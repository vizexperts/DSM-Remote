#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#include <direct.h>  // getcwd() definition

int main(int argc, char *argv[])
{
    std::ofstream file_op;
    file_op.open("gdal_warp_commands.bat");

    if(!file_op)
    {
        std::cout<<"could not create file"<<std::endl;
        return 0;
    }

    std::string command;
    
    int k=1;

    for(int i = 1; i < 3; i++)
    {
        for (int j = 1;j < 3;j++)
		{
            char a[10];
            itoa(i, a, 10);
            char b[10];
            itoa(j, b, 10);
            char c[10];
            itoa(k, c, 10);
            //gdalwarp -s_srs EPSG:4326 -t_srs EPSG:4326 -dstalpha -overwrite -multi -of GTiff -r cubicspline -co "TILED=YES" -srcnodata 0 -dstnodata 0
            //gdalwarp -s_srs EPSG:3857 -t_srs EPSG:4326 -of GTiff "D:\WB\WB_Mosaic[Define]_L18\WB_01-03.tif" "D:\WB\WB_Mosaic[Define]_L18\WB3.tif"
            //command = "gdalwarp -s_srs EPSG:4326 -t_srs EPSG:4326 -dstalpha -overwrite -multi -of GTiff -r cubicspline -co \"TILED=YES\" -srcnodata 0 -dstnodata 0 ";
            command = "gdalwarp -s_srs EPSG:3857 -t_srs EPSG:4326 -dstalpha -overwrite -multi -of GTiff -r cubicspline -co \"TILED=YES\" -srcnodata 0 -dstnodata 0 ";
            //command += "\\";
			command += "\"F:\\DATA_DOWNLOAD\\BSF_Gurdaspur\\bsf_gurdaspur_L19\\bsf_gurdaspur_";
			if(i<10)
			command+="0";
			command += a;
			if(j<10)
			command+="-0";
			else
			command+="-";
			command+=b;
	           
			command += ".tif\" \"F:\\DATA_DOWNLOAD\\BSF_Gurdaspur\\L19\\bsf_gurdaspur_";
			command += c;
			command += ".tif\"";

			file_op << command << "\n";
	        ++k;
		} 
    }
    
    file_op.close();
    
    
    return 0;
}