#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#include <direct.h>  // getcwd() definition

int main(int argc, char *argv[])
{
    std::ofstream file_op;
    file_op.open("gdal_warp_commands.bat");

    if(!file_op)
    {
        std::cout<<"could not create file"<<std::endl;
        return 0;
    }

    std::string command;
    
    int k=1;

    for(int i = 1; i < 3; i++)
    {
        for (int j = 1;j < 5;j++)
		{
            char a[4];
            itoa(i, a, 4);
            char b[10];
            itoa(j, b, 10);
            char c[4];
            itoa(k, c, 4);
            //gdalwarp -s_srs EPSG:4326 -t_srs EPSG:4326 -dstalpha -overwrite -multi -of GTiff -r cubicspline -co "TILED=YES" -srcnodata 0 -dstnodata 0
            //gdalwarp -s_srs EPSG:3857 -t_srs EPSG:4326 -of GTiff "D:\WB\WB_Mosaic[Define]_L18\WB_01-03.tif" "D:\WB\WB_Mosaic[Define]_L18\WB3.tif"
            command = "gdalwarp -s_srs EPSG:4326 -t_srs EPSG:4326 -dstalpha -overwrite -multi -of GTiff -r cubicspline -co \"TILED=YES\" -srcnodata 0 -dstnodata 0 ";
            command += "\"F:\\Guwahati\\Dhubri\\Dhubri_Mosaic[Define]_L20\\Dhubri_0";
			command += a;
	            
			command+="-0";
			command+=b;
	           
			command += ".tif\" \"F:\\Guwahati\\Dhubri\\Dhubri";
			command += c;
			command += ".tif\"";

			file_op << command << "\n";
	        ++k;
		} 
    }
    
    file_op.close();
    
    
    return 0;
}