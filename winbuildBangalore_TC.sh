#!/bin/bash
export OSSROOT=/root/Desktop/Consulting_VREALITY/OSS
echo $OSSROOT
#Set PlatForm Options
export OSSBUILD=$1
echo $OSSBUILD
export OSSARCH=$2
echo $OSSARCH
export TKPSDK_DIR=/root/Teamcity/TKP_SDK/trunk
export TERRAINSDK_DIR=/root/Teamcity/TKP_SDK/trunk
export DSM_DIR=/root/Teamcity/DSM



# Building Cmake and compiling code
# Generate files from scratch

rm CMakeCache.txt

/usr/bin/cmake -G "Unix Makefiles" -DUSE_NAMESPACE=viz .

make -j4

cd Package/Linux

# Making RPM Package

chmod a+x ./makeDSMDeps.sh
./makeDSMDeps.sh

chmod a+x ./makeDSM.sh
./makeDSM.sh