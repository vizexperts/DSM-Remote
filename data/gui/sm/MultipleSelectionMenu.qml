import QtQuick 2.1

Item{
    id: multipleSelectionMenuContainer
    objectName: "multipleSelectionMenu"

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
    }

    signal highlightItem(int index)
    signal selectItem(int index)
    signal openMenu(int Index)
    signal closeMenu()

    onSelectItem: unload()
    onOpenMenu: unload()
    onCloseMenu: unload()


    width: multipleSelectionView.width + 10
    height: multipleSelectionView.height + 35

    scale: 0.1

    visible: false

    property int posX: 0
    property int posY: 0

    // call this function after setting the list
    function loadAt(posX, posY){

        if((posX < 0) || (posY > root.width)){
            return
        }
        if((posY < 0) || (posY > root.height)){
            return
        }

        if(typeof(multipleSelectionModel) == "undefined"){
            return
        }

        x = posX + 5
        if(x < 0){
            x = 0
        }
        else if(x > root.width - width){
            x = root.width - width
        }

        y = (root.height - posY) - height/2
//        y = posY - height/2
        if(y < 0){
            y = 0
        }
        else if(y > root.height - height){
            y = root.height - height
        }
        visible = true
        scale = 1
        restOfArea.enabled = true
    }

    function unload(){
        scale = 0.0
        restOfArea.enabled = false
        visible=false
    }

    property int boxWidth: 10
    property int boxHeight: 30
    Image{
        width: 25
        height: 25
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 5
        anchors.rightMargin: 5
        source: rootStyle.getIconPath("add")
        rotation: 45
        MouseArea{
            anchors.fill: parent
            onClicked: closeMenu()
        }
    }

    ListView{
        id: multipleSelectionView
        x: 5
        y: 30
        model: (typeof(multipleSelectionModel) != "undefined") ? multipleSelectionModel : "undefined"
        width: boxWidth
        height: count*boxHeight
        snapMode: ListView.NoSnap
        boundsBehavior: Flickable.StopAtBounds
        highlightFollowsCurrentItem: true
        highlight: Rectangle{
            height: boxHeight
            width: multipleSelectionView.width
            color: "#64ffffff"
            radius: 2
            Behavior on y{
                NumberAnimation{
                    duration: 100
                }
            }
        }

        delegate: Rectangle{
            id: multipleSelectionDelegate
            height: boxHeight
            z: restOfArea.z+5
            width: multipleSelectionView.width
            color: "transparent"
            Component.onCompleted: {
                if(text.width +height+20 > multipleSelectionView.width){
                    multipleSelectionView.width = text.width + height + 20
                }
            }

            TextEdit{
                id: text
                color: multipleSelectionDelegate.ListView.isCurrentItem ? "black" : "white"
                text: modelData
                horizontalAlignment: TextEdit.AlignLeft
                font.pixelSize: rootStyle.fontSize*0.8
            }
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    multipleSelectionView.currentIndex = index
                    highlightItem(index)
                }
                onClicked: {
                    selectItem(index)
                }
            }

            Image{
                height: parent.height
                width: height
                anchors.right: parent.right
                source: rootStyle.getIconPath("right_dark")
                visible: multipleSelectionDelegate.ListView.isCurrentItem
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        openMenu(index)
                    }
                }
            }

        }
    }

    Behavior on scale {
        PropertyAnimation{
            duration: 500
            easing.type: Easing.OutBack
        }
    }
}
