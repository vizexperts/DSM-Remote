import QtQuick 2.1
import "VizComponents"

Item{
    id: statusBar
    width: Math.max(360, row.width + 30)
    height: 35

    Component.onCompleted: {
        visible = false
        y = -100
    }

    Component.onDestruction: {
        applicationBusy = false
    }

    objectName: "statusBar"

    property bool applicationBusy: false

    Image {
        id: name
        source: rootStyle.getIconPath("Picture1")
        x: -width
        transformOrigin: Item.TopRight
        rotation: -90
        width: parent.height
        height: parent.width
    }
    anchors.horizontalCenter: parent.horizontalCenter

    // to remove status bar call with message = ""
    function setStatus(message, busy){
        if(message === "close"){
            closeAnim.start()
            applicationBusy = false
        }
        else{
            statusMessageLabel.text = message
            progressbar.visible = false
            progressbar.value = 0
            applicationBusy = busy
            open.start()
        }
    }
    
    function setProgressStatus(message, busy,progressValue){
        if(message === ""){
            closeAnim.start()
            applicationBusy = false
        }
        else{
            applicationBusy = true
            statusMessageLabel.text = message
            progressbar.visible = true
            progressbar.value = progressValue
            applicationBusy = busy
            open.start()
        }
    }

    function quicKLoadBar(message){
            applicationBusy = true
            statusMessageLabel.text = message
            progressbar.visible = false
            quickOpen.start()
    }

    function close(){
        setStatus("close", false)
    }

    SequentialAnimation{
        id: closeAnim
        PropertyAnimation{target: statusBar; property: "y"; duration: 1200; to: -100}
        PropertyAction{target: statusBar; property: "visible"; value: false}
    }

    SequentialAnimation{
        id: open
        PropertyAction{target: statusBusy; property: "visible"; value: true}
        PropertyAction{target: statusBar; property: "visible"; value: true}
        PropertyAnimation{target: statusBar; property: "y"; duration: 1200; to: 0}
    }

    SequentialAnimation{
        id: quickOpen
        PropertyAction{target: statusBusy; property: "visible"; value: true}
        PropertyAction{target: statusBar; property: "visible"; value: true}
        PropertyAnimation{target: statusBar; property: "y"; duration: 0; to: 0}
    }

    Row{
        id: row
        anchors.centerIn: parent
        AnimatedImage{
            id: statusBusy
            source: rootStyle.getBusyIcon()
        }

        VizLabel{
            id: statusMessageLabel
            text: ""
        }
        
        VizProgressBar
        {
            id:progressbar
            visible:false
        }
    }
}

