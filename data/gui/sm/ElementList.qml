import QtQuick 2.0
import QtQuick.Layouts 1.0
import "VizComponents"

/*
     Element List
 */

Item {
    id: elementList

    width: minimumWidth
    height: root.height-50

    objectName: "elementList"

    enabled: !minimized
    x: minimized ? - width : toolBoxId.x + toolBoxId.width

    /*
        \property bool minimized
        \brief Minimized state.
    */
    property bool minimized: true

    /*
        \property int minimumWidth
        \brief Minimum width possible of element list
    */
    property int minimumWidth: 300*rootStyle.uiScale
    /*
        \property int maximumWidth
        \brief Maximum width possible of element list
    */
    property int maximumWidth: root.width/2

    onMinimumWidthChanged: if(width < minimumWidth) width = minimumWidth

    //! Internal properties
    QtObject{
        id: internal
        /*
            \readonly
            \property real opacity
            \brief Overall opacity of the element list
        */
        readonly property real opacity : 0.7
    }

    //! Aliases for external usage
    property alias treeViewHasActiveFocus: elementListTreeView.treeHasActiveFocus
    property alias treeViewHasFocus: elementListTreeView.treeHasFocus
    property alias searchText: searchField.text

    //!  Signals for C++
    /*
        \signal search(string text)
        \brief search the world and show suggestions for given search text
        \param text text to search for
      */
    signal search(string text)

    /*
        \signal selectPlace(int index)
        \brief select and go to place at suggestion#(index)
        \param index index to go to
      */
    signal selectPlace(int index)

    /*
        \signal addChildFolder(int index)
        \brief Add child folder/group to the element at index.
        \param index element index

        Requires the element at index to be a folder object. Or the top layer.
      */
    signal addChildFolder(int index)

    /*
        \signal openRenameItemPopup()
        \brief Open openRenameItemPopup window
    */
    signal openRenameItemPopup(int index)

    /*
        \function openRenamePopup()
        \brief Function to open openRenamePopup. Called from ElementListGUI::_openRenameItemPopup()
    */
    function openRenamePopup(){
        smpFileMenu.load("ItemRenamePopup.qml", root.width/2, root.height/2)
    }
    /*
        \function openAddFolderPopup()
        \brief Function to open openAddFolderPopup. Called from ElementListGUI::_opeAddFolderPopup()
    */
    function openAddFolderPopup(){
        smpFileMenu.load("AddFolderPopup.qml", root.width/2, root.height/2)
    }

    /*
        \function openVBVPopup()
        \brief Function to open ViewBasedVisibilityPopup. Called from ElementListGUI::_openViewBasedVisibilityPopup()
    */
    function openVBVPopup(){
        smpFileMenu.load("ViewBasedVisibilityPopup.qml", root.width/2, root.height/2)
    }

    function openItemReloadPopup(){
        smpFileMenu.load("ReloadObjectPopup.qml")
    }


    //! Add an event eating background mouse area
    VizBackgroundMouseArea{
        onClicked: {
            rightClickContextualConnection.target = null
            rightClickContextualMenu.closeContextualMenu()
        }
        onPressed: {
            treeViewHasFocus = true
        }
    }

    Behavior on x {
        NumberAnimation{
            duration: 500
            easing.type: Easing.OutExpo
        }
    }

    Rectangle{
        anchors.fill: parent
        border.color: rootStyle.textColor
        border.width: 1
        opacity: internal.opacity
        color: "#ee000000"
        Rectangle{
            anchors.fill: parent
            border.color: treeViewHasActiveFocus ? rootStyle.colorWhenSelected : "#ee000000"
            border.width: 10
            color: "transparent"
        }
    }

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        opacity: internal.opacity
    }

    ColumnLayout{
        anchors.fill: parent
        anchors.margins: 17
        spacing: 5
        VizButton{
            id: closeButton
            anchors.right: parent.right
            backGroundVisible: false
            iconSource: rootStyle.getIconPath("close")
            onClicked: elementList.minimized = true
        }

        VizTextField{
            id: searchField
            anchors.topMargin: 30
            Layout.fillWidth: true
            width: parent.width - 50
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: "Search..."
            Keys.onEscapePressed: {
                setFocus = false
                text = ""
            }
            onTextChanged: search(text)
        }

        states: State {
            name: "SEARCHING"
            when: (searchField.setFocus == true)
        }

        VizTreeView{
            id: elementListTreeView
            x: 5
            implicitWidth: parent.width
            Layout.fillHeight: true
            model: (typeof(elementListTreeModel) !== "undefined") ? elementListTreeModel : "undefined"
            objectName: "elementListTreeView"
            ListView{
                id: dummyListModel
                model: (typeof(RightClickListModel) !== "undefined") ? RightClickListModel : "undefined"

            }

            Connections{
                id: rightClickContextualConnection
                onSelectItem : {
                    rightClickContextualConnection.target = null
                    switch(index){
                    case 0:                                    // ADD_FOLDER_UID
                        openAddFolderPopup()
                        break
                    case 1:                                  // DELETE_UID
                        elementListTreeView.deleteKeyPressed(elementListTreeView.currentRow)
                        break
                    case 2:                                // RENAME_UID
                        openRenameItemPopup(elementListTreeView.currentRow);
                        break
                    case 3:                              // SCALE_VISIBILITY_UID
                        openVBVPopup()
                        break
                    case 4:                             // RELOAD_UID
                        openItemReloadPopup()
                        break
                    }
                }
                target: null
            }

            onClicked: {
                rightClickContextualConnection.target = null
                rightClickContextualMenu.closeContextualMenu()
            }

            onRightClicked: {
                rightClickContextualConnection.target = rightClickContextualMenu
                rightClickContextualMenu.openContextualMenu(dummyListModel.model)
            }

            VizTreeViewColumn{
                role: "iconPath"
                delegate: Image {
                    source: typeof styleData.value == "undefined" ? "" : styleData.value
                    width: rootStyle.buttonIconSize
                    height: rootStyle.buttonIconSize
                }
            }

            VizTreeViewColumn{
                role: "name"
                delegate: VizLabel{
                    text: typeof styleData.value == "undefined" ? "" : styleData.value
                    textColor: styleData.textColor
                    Connections{
                        target: elementList
                        onSearch: if(text === "") select("")
                    }
                    Component.onCompleted: {
                        if(searchField.text != "")
                            select(searchField.text)
                    }
                }
            }
            VizTreeViewColumn{
                role:"rightClick"
                visible: (multi_touch_interaction_active===true)
                delegate: Image {
                    id: rightClickImage
                    source: rootStyle.getIconPath("more_option")

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            rightClickContextualConnection.target = rightClickContextualMenu
                            rightClickContextualMenu.openContextualMenu(dummyListModel.model)
//                            elementListTreeView.clicked(dummyListModel.currentIndex)
//                            elementListTreeView.rightClicked(dummyListModel.currentIndex)
                        }
                    }
                }
            }
        }

        Item{
            id: searchTab
            width: parent.width
            visible: (parent.state == "SEARCHING")
            enabled: visible
            Layout.minimumHeight: root.height*0.4
            Rectangle{
                anchors.fill: parent
                gradient: rootStyle.gradientWhenDefault
                opacity:  internal.opacity
            }
            clip: true
            height: (parent.state == "SEARCHING")  ? searchTitleBar.height + searchListView.height + 20 : 0
            Rectangle{
                id: searchTitleBar
                width: parent.width
                height: childrenRect.height
                gradient: rootStyle.gradientWhenDefault
                VizLabel{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Location"
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {}
                }
            }
            ListView{
                id: searchListView
                anchors.top: searchTitleBar.bottom
                anchors.topMargin: 10
                clip: true
                VizScrollBar{
                    id: searchViewScrollBar
                    target: searchListView
                }
                width: parent.width
                height: Math.min(root.height*0.4, count*searchTitleBar.height)
                model: (typeof(placesModel) !== "undefined") ? placesModel : "undefined"
                boundsBehavior: Flickable.StopAtBounds
                delegate: VizLabel{
                    id: placesDelegate
                    text: name
                    width: searchListView.width - searchViewScrollBar.width
                    color: (ListView.view.currentIndex == index) ? rootStyle.colorWhenSelected : "transparent"
                    textColor: (ListView.view.currentIndex == index) ? rootStyle.colorWhenDefault : rootStyle.textColor
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            placesDelegate.ListView.view.currentIndex = index
                            selectPlace(index)
                        }
                    }
                }

            }
        }
    }

    MouseArea{
        id: resizeArea
        height: parent.height
        anchors.right: parent.right
        width: 10
        hoverEnabled: true
        property int mx
        states: State {
            name: "entered"
            when: resizeArea.containsMouse
            PropertyChanges { target: root; cursorShape: Qt.SizeHorCursor }
        }
        function handleMouse(mouse){
            if(mouse.buttons & Qt.LeftButton){
                parent.width = Math.min(Math.max(minimumWidth, parent.width + (mouse.x-mx)),maximumWidth)
            }
        }
        onPositionChanged: handleMouse(mouse)
        onPressed: {
            mx = mouse.x
        }

    }
}
