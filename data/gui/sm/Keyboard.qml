import QtQuick 2.1
import "VizComponents"

Item {
//    enabled: multi_touch_interaction_active
    property bool minimized : true
    visible : !minimized

    anchors.fill: parent

    id : keyBoard
    objectName : "keyboard"

//    MouseArea{
//        anchors.fill: parent
//        enabled: !minimized
//        onClicked: {
//            close()
//        }
//    }

    smooth: false

    function close(){
        minimized = true
        panel.focus = false
    }

    onMinimizedChanged: {
        if(minimized)
            closeAnimation.start()
        else
            openAnimation.start()
    }

    ParallelAnimation{
        id: openAnimation
        PropertyAction{
            target: rect
            property: "visible"
            value: true
        }
        NumberAnimation{
            id: anim
            target: clKeyboard
            property: "width"
            easing.type: Easing.OutCubic
            from: (panel)? panel.width : 0
            to : 650
            duration: 300
            //            easing.type: Easing.OutBack
        }
        NumberAnimation{
            target: clKeyboard
            property: "height"
            from : (panel)? panel.height : 0
            to : 250
            duration: anim.duration
            easing: anim.easing
        }
        NumberAnimation{
            target: rect
            property: "x"
            from : (panel) ? panel.mapToItem(null,0,0).x : 0
            to : (root.width - 700)/2
            duration: anim.duration
            easing: anim.easing
        }
        NumberAnimation{
            target: rect
            property: "y"
            from : (panel) ? panel.mapToItem(null,0,0).y : 0
            to : (root.height - 300)/2
            duration: anim.duration
            easing: anim.easing
        }
    }

    SequentialAnimation{
        id: closeAnimation
        ParallelAnimation{
            NumberAnimation{
                target: clKeyboard
                property: "width"
                from: 650
                to : (panel) ? panel.width : 0
                duration: anim.duration
                easing : anim.easing
            }
            NumberAnimation{
                target: clKeyboard
                property: "height"
                from : 250
                to : (panel) ? panel.height : 0
                duration: anim.duration
                easing: anim.easing
            }
            NumberAnimation{
                target: rect
                property: "x"
                to : (panel) ? panel.mapToItem(null,0,0).x : 0
                from : (root.width - 700)/2
                duration: anim.duration
                easing: anim.easing
            }
            NumberAnimation{
                target: rect
                property: "y"
                to : (panel) ? panel.mapToItem(null,0,0).y : 0
                from : (root.height - 300)/2
                duration: anim.duration
                easing: anim.easing
            }
        }

        PropertyAction{
            target: rect
            property: "visible"
            value: false
        }
    }

    property alias text: txtpnl.text


    function open(textEdit){
        panel = textEdit
        text = textEdit.text
        if(panel.focus)
            minimized = false
    }

    property var panel
    //    visible: true

    onTextChanged: {
        if(panel !== null){
            panel.text = text
        }
    }

    Rectangle{
        id: backgroundRect1
        //x:0
        //y:0
        width: root.width
        height: (panel) ? panel.mapToItem(null,0,0).y : root.height
        color: "grey"
        opacity: 0.2
        visible: !minimized
    }
    Rectangle{
        id: backgroundRect2
        //x:0
        y: backgroundRect1.height
        width: (panel) ? (panel.mapToItem(null, 0, 0).x) : 0
        height: (panel) ? panel.height : 0
        color: backgroundRect1.color
        opacity: backgroundRect1.opacity
        visible: backgroundRect1.visible
    }
    Rectangle{
        x: root.width - width
        width: (panel) ? (root.width - backgroundRect2.width - panel.width) : 0
        height: backgroundRect2.height
        y: backgroundRect2.y
        color: backgroundRect1.color
        opacity: backgroundRect1.opacity
        visible: backgroundRect1.visible
    }
    Rectangle{
        y: backgroundRect2.y+backgroundRect2.height
        width: root.width
        height: (root.width - y)
        color: backgroundRect1.color
        opacity: backgroundRect1.opacity
        visible: backgroundRect1.visible
    }

    Rectangle{
        id: rect
        anchors.bottom: parent.bottom
        MouseArea{
            anchors.fill: parent
        }
        width: clKeyboard.width + 20
        height: clKeyboard.height + textPanel.height + 25
        radius: 10
        color: "#961a1515"
        visible: false

        Rectangle{
            id: textPanel
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin:  10
            height: txtpnl.height+10
            width: clKeyboard.width - 40
            radius: 10
            color: "white"
            TextInput{
                id: txtpnl
                //x: 5
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.right: parent.right
                font.pixelSize: 30
                echoMode: /*(panel) ? panel.echoMode : */TextInput.Normal
                selectByMouse: true
                layer.enabled: true
            }
        }

        VizButton{
            anchors.right: rect.right
            anchors.rightMargin: 5
            anchors.top: rect.top
            anchors.topMargin: 10
            iconSource:  rootStyle.getIconPath("close")
            backGroundVisible: false
            height: 40
            width: 40
            opacity: 0.7
            onClicked:{
                close()
            }
        }
        VizKeyboard{
            id: clKeyboard
            anchors.top: textPanel.bottom
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            z: parent.z+1
            textInput: txtpnl
            showCancelCLButton: false
            showOkCLButton: false
            onLeftArrowClicked: {
                --textInput.cursorPosition
            }
            onRightArrowClicked: {
                ++textInput.cursorPosition
            }

            onEnterClicked: {
                close()
            }
        }
    }
}
