import QtQuick 2.1
import "VizComponents"

Item {
    id: elevationProfile
    width: bottomLoader.width
    height: 300*rootStyle.uiScale
    objectName: "elevationProfile"

    property int count: 0

    Component.onCompleted: {
        bottomLoaderWrapper.elevationProfileLoaded()
    }

    property bool calculating: false
    property bool showGraph : false
    property alias minValueText: minElevation.text
    property alias maxValueText: maxElevation.text
    property alias avgValueText: avgElevation.text

    property real yMin: 0
    property real yMax: 0
    property real yAvg: 0

    signal mark(bool value)
    signal selectLine(bool value)
    signal start()
    signal stop()

    signal showPointAtDistance(double disance)

    property real distance: 0
    property real altitude: 0

    onShowGraphChanged: {
        graph.requestPaint()
    }

    signal closed()

    onClosed: {
        //if(calculating)
            stop()
        //flag set false
        smpFileMenu.isPopUpAlreadyOpen=false;
        root.nowRemoveHighLight();
    }

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        opacity: 0.8
    }

    Component.onDestruction: closed()

    property bool busy: false

    function changeCurrentPosition(){

        busy = true

        var cHeight = graph.height
        var cWidth = graph.width

        var topPadding = graph.topPadding
        var bottomPadding = graph.bottomPadding
        var vPadding = topPadding + bottomPadding

        var leftPadding = graph.leftPadding
        var rightPadding = graph.rightPadding
        var hPadding = leftPadding + rightPadding

        var horizSpace = (cWidth - hPadding)

        var xMin = xData[0];
        var xMax = xData[count-1];
        var xRange = xMax - xMin
        var xPos = leftPadding;

        var vertSpace = (cHeight - vPadding)
        var yMin = elevationProfile.yMin
        var yMax = elevationProfile.yMax
        var yRange = yMax - yMin

        var curX = mousearea.mouseX

        if(curX < leftPadding)
            curX = leftPadding
        else if(curX > cWidth - rightPadding)
            curX = cWidth - rightPadding

        marker.x = curX + 2
        distance = xMin + (curX - leftPadding)*(xRange/horizSpace)

        line.x = curX

        var j = binarySearchClosest(distance)

        for(var i = j-1; i < j+1; i++){
            if((xData[i] <= distance) && (distance <= xData[i + 1])){
                var p = (distance - xData[i])/(xData[i+1] - xData[i])

                altitude = yData[i] + (yData[i+1] - yData[i])*p

                marker.text = "("+distance.toFixed()+","+altitude.toFixed()+")"
                marker.y = 50 + (cHeight - (bottomPadding + (altitude - yMin)*(vertSpace/yRange)))
            }
        }

        busy = false

        showPointAtDistance(distance)
    }

    function binarySearchClosest(key)
    {
        var first = 0;
        var last = count - 1;
        var mid = 0;

        if(key <= xData[first])
            return 0;

        if(key >= xData[last])
            return last;

        while (first <= last)
        {
            mid = Math.round((first + last) / 2);  // compute mid point.
            if (key > xData[mid])
                first = mid + 1;  // repeat search in top half.
            else if (key < xData[mid])
                last = mid - 1; // repeat search in bottom half.
            else
                break;
        }

        return mid;

    }

    Row{
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 100
        anchors.topMargin: 10
        spacing: 10
        VizButton{
            id: markButton
            text: "Mark"
            enabled: !calculating
            onClicked: {
                showGraph = false
                selectButton.checked = false                
                checked = !checked
            }
            onCheckedChanged: mark(checked)
        }
        VizButton{
            id: selectButton
            text: "Select Line"
            enabled: !calculating
            onClicked: {
                showGraph = false
                markButton.checked = false
                checked = !checked
            }
            onCheckedChanged: selectLine(checked)
        }
        VizButton{
            id: startButton
            text: calculating ? "Stop" : "Start"
            onClicked: {
                showGraph = false
                if(calculating){
                    stop()
                }
                else{
                    start()
                }
            }
        }
        AnimatedImage{
            anchors.verticalCenter: parent.verticalCenter
            source: rootStyle.getBusyIcon()
            visible: (calculating)
        }
    }

    Row{
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 100
        spacing: 20
        visible: showGraph
        VizLabel{
            id: maxElevation
            textColor: "red"
        }
        VizLabel{
            id: minElevation
            text: "4350 m @ 98.5 N, 76.4 E"
            textColor: "green"
        }
        VizLabel{
            id: avgElevation
            textColor: "yellow"
        }
    }

    VizButton{
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 10
        anchors.rightMargin: 10
        backGroundVisible: false
        iconSource: rootStyle.getIconPath("close")
        onClicked: bottomLoaderWrapper.unload()
    }


    Canvas{
        id: graph
        width: parent.width
        anchors.bottom: parent.bottom
//        renderTarget: Canvas.FramebufferObject
        height: 250*rootStyle.uiScale

        MouseArea {
            id:mousearea
            hoverEnabled:true
            anchors.fill: parent
//            onPositionChanged: {
//                line.x = mouseX
//            }

            onPositionChanged: {
                if(showGraph && !busy){
                    changeCurrentPosition()
                }
            }
        }


        onPaint: { draw(); }

        property int topPadding: 20
        property int bottomPadding: 50
        property int leftPadding: 80
        property int rightPadding: 30

        function draw() {

            var ctx = getContext("2d");

            var cHeight = graph.height
            var cWidth = graph.width

            var topPadding = graph.topPadding
            var bottomPadding = graph.bottomPadding
            var vPadding = topPadding + bottomPadding

            var leftPadding = graph.leftPadding
            var rightPadding = graph.rightPadding
            var hPadding = leftPadding + rightPadding


            // gradient
            var grad = ctx.createLinearGradient(0, 0, 0, cHeight);
            grad.addColorStop(0, "#3f3b3c");
            grad.addColorStop(1, "#010103");
            ctx.fillStyle = grad;
            ctx.fillRect(0, 0, graph.width, graph.height);


            // Draw Axis
            ctx.lineWidth = 2;
            ctx.strokeStyle = "#ffffff";
            ctx.beginPath();
            ctx.moveTo(leftPadding, topPadding);
            ctx.lineTo(leftPadding, cHeight - bottomPadding);
            ctx.lineTo(cWidth - rightPadding, cHeight - bottomPadding);
            ctx.stroke();
            ctx.closePath();

            // draw grid
            ctx.lineWidth = 0.5;
            ctx.strokeStyle = "#ffffff";

            // horizontal
            for(var y = topPadding; y < cHeight - bottomPadding; y += 10){
                ctx.beginPath()
                ctx.moveTo(leftPadding, y);
                ctx.lineTo(cWidth - rightPadding, y);
                ctx.stroke()
                ctx.closePath();
            }

            // vertical
            for(var x = leftPadding; x < cWidth - rightPadding; x+=50){
                ctx.beginPath()
                ctx.moveTo(x, topPadding);
                ctx.lineTo(x, cHeight - bottomPadding)
                ctx.stroke()
                ctx.closePath();
            }

            //  current Position Pointer
            ctx.lineWidth = 2;
            ctx.strokeStyle = "#ff79de";

            if(showGraph){
                // draw graph
                var horizSpace = (cWidth - hPadding)

                var xMin = xData[0];
                var xMax = xData[count-1];
                var xRange = xMax - xMin
                var xPos = leftPadding;

                var vertSpace = (cHeight - vPadding)
                var yMin = elevationProfile.yMin
                var yMax = elevationProfile.yMax
                var yRange = yMax - yMin

                ctx.font = "9px Helvetiva";
                ctx.fillStyle = "#ffffff";
                ctx.textAlign = "center";

                // Distance labels
                for(x = leftPadding; x < cWidth - rightPadding; x+=100){
                    var xLabel = xMin +  (x - leftPadding)*(xRange/horizSpace)
                    ctx.fillText(xLabel.toFixed(), x, (cHeight-bottomPadding + 18));
                }

                ctx.textAlign = "right";
                // elevation labels
                for(y = topPadding; y < cHeight - bottomPadding; y+=30){
                    var yLabel = yMin + (cHeight -(y+bottomPadding))*(yRange/vertSpace);
                    ctx.fillText(yLabel.toFixed(), leftPadding - 5, y);

                }

                // avg height marker
                ctx.lineWidth = 2;
                ctx.strokeStyle = "#74fafd"
                var avgLineY = (cHeight -(bottomPadding + (yAvg - yMin)*(vertSpace/yRange)))
                ctx.beginPath()
                ctx.moveTo(leftPadding - 5, avgLineY);
                ctx.lineTo(cWidth-rightPadding+5, avgLineY);
                ctx.stroke();
                ctx.closePath()

//                //  current Position Pointer
//                ctx.lineWidth = 2;
//                ctx.strokeStyle = "#ff0000";
//                var curX = mousearea.mouseX
//                if(curX < leftPadding)
//                    curX = leftPadding
//                else if(curX > cWidth - rightPadding)
//                    curX = cWidth - rightPadding
//                marker.x = curX + 2
//                distance = xMin + (curX - leftPadding)*(xRange/horizSpace)
//                ctx.beginPath();
//                ctx.moveTo(curX, 0);
//                ctx.lineTo(curX, cHeight)
//                ctx.stroke();
//                ctx.closePath();

                // line graph
                ctx.lineWidth = 2;
                var prevX , prevY;
                for(var i = 0; i < count; i++){
                    xPos = leftPadding + (xData[i] - xMin)*(horizSpace/xRange)
                    var yPos = (cHeight -  (bottomPadding +(yData[i] - yMin)*(vertSpace/yRange)))

//                    if((xData[i] < distance) && (distance <= xData[i+1])){
//                        var p = (distance - xData[i])/(xData[i+1] - xData[i])
//                        altitude = yData[i] + (yData[i+1] - yData[i])*p
//                        marker.text = "("+distance.toFixed()+","+altitude.toFixed()+")"
//                        marker.y = (cHeight - (bottomPadding + (altitude - yMin)*(vertSpace/yRange)))
//                    }

                    if(i == 0){
                        prevX = leftPadding + (xData[i] - xMin)*(horizSpace/xRange)
                        prevY = (cHeight -  (bottomPadding +(yData[i] - yMin)*(vertSpace/yRange)))
                    }
                    else{

                        ctx.strokeStyle = colorArray[i];
                        ctx.beginPath();
                        ctx.moveTo(prevX, prevY);
                        ctx.lineTo(xPos, yPos);
                        ctx.stroke();
                        ctx.closePath();
                        prevY = yPos;
                        prevX = xPos;
                        console.log(ctx.strokeStyle +"   "+ colorArray[i]);
                    }

                }

            }
        }
    }

    Rectangle{
        y: 50
        id: line
        width: 2
        color: "red"
        height: graph.height
        visible: showGraph
    }

    TextEdit{
        id: marker
        color: rootStyle.textColor
        visible: showGraph
        readOnly: true
        font.pixelSize: 14
    }
}
