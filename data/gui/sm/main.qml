import QtQuick 2.1
import QtQuick.Window 2.0
import "VizComponents"

/*
     Root QML file.
 */

FocusScope {
    id: root
    focus: true
    Keys.onPressed: {
        if(event.key === Qt.Key_QuoteLeft){
            opacity = (opacity == 0) ? 1: 0
        }
        else if(event.key === Qt.Key_BracketLeft){
            rootStyle.uiScale = (rootStyle.uiScale <= 1) ? 1 : rootStyle.uiScale - 0.05
        }
        else if(event.key === Qt.Key_BracketRight){
            rootStyle.uiScale = (rootStyle.uiScale >= 4) ? 4 : rootStyle.uiScale + 0.05
        }
    }

    Behavior on opacity{
        NumberAnimation{duration: 300}
    }

    signal nowRemoveHighLight();


    height: 1000
    width: 1000*aspectRatio
    property real aspectRatio: 16/9

    //! flag to check if the gui is running from QML or C++
    //! for development purposes only
    property bool running_from_qml: true

    QtObject {
            id: licenseManager
            objectName: "licenseManager"
            property bool georbIS_Vu : false
            property bool georbIS_Pro : false
            property bool georbIS_Strategem : false
            property bool georbIS_Intel : false
            property bool georbIS_Footprint : false
            property bool georbIS_Analyst : false
            property bool georbIS_Processor : false
    }

    // property to check which mode is application in
    property int application_mode : application_mode_PLAN

    // Enums for application mode
    readonly property int application_mode_NONE : 0
    readonly property int application_mode_PLAN: 2

    // type of user(Instructor/Syndicate/SandModelCreator)
    property int user_type : userType_NONE

    // Enums for user types
    readonly property int userType_NONE: 0
    readonly property int usertype_INSTRUCTOR: 1
    readonly property int usertype_MODELCREATOR: 2
    readonly property int userType_SYNDICATE: 3

    // flag if eventMode is on or not
    property bool eventModeOn: false

    //! is MultiTouchInteractionActive
    property bool multi_touch_interaction_active : false
    property bool hoverEnabled: false

    //! signal to change cursor
    signal changeCursor(int shape)
    signal appFontSizeChanged(int fontSize)

    //! Current cursor type
    property int cursorShape: Qt.ArrowCursor

    onApplication_modeChanged:
    {
        //Setting the current Tab
        // desktop.currentTabIndex = desktop.tabType_SMP
        desktop.tabType_SMP_Visbile = true
    }

    onCursorShapeChanged: {
        changeCursor(cursorShape)
        _cursorRestoreArea.enabled = (cursorShape == Qt.ArrowCursor)? false : true
    }

    FontLoader{
        id: lato
        source: "qrc:///fonts/lato/Lato-Regular.ttf"
    }

    FontLoader{
        id: latoLight
        source: "qrc:///fonts/lato/Lato-Light.ttf"
    }

    FontLoader{
        id:latoHairline
        source: "qrc:///fonts/lato/Lato-Hairline.ttf"
    }
    // Root style Component
    property VizStyle rootStyle: VizStyle{
        onFontSizeChanged: {
            // Write the change in a file
        }
    }

    DateTime{
        id: systemDateTime
        Timer{
            interval: 1000; repeat: true
            running: true
            onTriggered: systemDateTime.update()
        }
    }

    //! Mouse area to restore arrow cursor if cursor has been changed by any other mouse area
    MouseArea{
        id: _cursorRestoreArea
        anchors.fill: parent
        enabled: false
        visible: enabled
        hoverEnabled: enabled
        onPressed: cursorShape = Qt.ArrowCursor
    }

    MouseArea{
        id: restOfArea
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        enabled: false
        visible: enabled
    }

    FileContents{
        id: smpFileMenu
    }

    MyDesktop{
        id: desktop
    }

    LockScreen{
        id:loc
    }

    Keyboard{
        id: keyboard
    }

    ErrorLoader{
        id: errorLoader
        objectName: "errorLoader"
    }

    StatusBar{
        id: statusBar
    }

    RightClickContextualMenu{
        id: rightClickContextualMenu
    }

    //! Cursor object
    Item{
        id: cursor
        objectName: "cursorObject"

        //! Initial visibility is set to false: set to true if CursorImplementation is CURSORIMPL_QML
        visible: false

        //! Offsets for cursor icon
        property int imageVerticalOffset: 0
        property int imageHorizontalOffset: 0

        //! Source file for current cursor
        property string cursorSource

        //! Image: expected size (32,32)
        Image {
            id: cursorImage
            anchors.left: parent.left
            anchors.leftMargin: parent.imageHorizontalOffset
            anchors.top: parent.top
            anchors.topMargin: parent.imageVerticalOffset
            source: parent.cursorSource
        }
    }
}
