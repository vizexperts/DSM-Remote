import QtQuick 2.1
import QtQuick.Controls 1.0
import "VizComponents"
import PDFView 1.0

Rectangle {    
    width: 600*rootStyle.uiScale
    height: 800*rootStyle.uiScale
    radius: 5
    objectName: "pdfViewer"
    color: "#64ffffff"
    id: pdfViewer
    clip: true

    property bool titleBarVisible: true

    property string title
    property string path
    property bool saveButtonVisible

    MouseArea{
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton| Qt.RightButton
        onWheel: {
            if(wheel.angleDelta.y > 0){
                view.currentPage++
            }
            else{
                view.currentPage--
            }
        }
    }

    Component.onCompleted:{
        view.source = path
        pageLineEdit.text = (view.currentPage+1)+" / "+ (view.numPages)
    }

    Rectangle{
        id: titleBar
        width: parent.width
        height: 30*rootStyle.uiScale
        gradient: rootStyle.blueGradient
        radius: 5
        visible: titleBarVisible
        VizLabel{
            text: title
            width: parent.width
        }
        MouseArea{
            id: dragArea
            states: State {
                name: "entered"
                when: dragArea.containsMouse
                PropertyChanges { target: root; cursorShape: Qt.SizeAllCursor }
            }
            anchors.fill: parent
            drag.target: pdfViewer
            drag.axis: Drag.XandYAxis
            drag.minimumX: 0
            drag.maximumX: root.width - pdfViewer.width
            drag.minimumY: 0
        }
    }
    VizButton{
        anchors.top: parent.top
        anchors.right: parent.right
        visible: titleBarVisible
        backGroundVisible: false
        iconSource:  rootStyle.getIconPath("close")
        onClicked: {
            pdfViewer.destroy()
        }
    }

    Rectangle{
        id: buttonBar
        width: parent.width
        height: 50*rootStyle.uiScale
        anchors.top: titleBar.bottom
        color: "#640000ff"
        Row{
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 20
            VizButton{
                iconSource: rootStyle.getIconPath("save")
                backGroundVisible: false
                onClicked: {
                    enabled = false
                    saveButtonVisible = false
                    pdfLoader.saveFile(path)
                }
            }

//            Rectangle{
//                id: saveNameBar
//                color: "white"
//                border.color: "blue"
//                border.width: 2
//                radius: 4
//                anchors.verticalCenter: parent.verticalCenter
//                Behavior on width {
//                    NumberAnimation{
//                        duration: 100
//                    }
//                }
//                height: 30*rootStyle.uiScale
//                width: 0
//                VizTextEdit{

//                }

//                /*LineEdit{
//                    id: nameLineEdit
//                    color: "black"
//                    horizontalAlignment: TextEdit.AlignHCenter
//                    anchors.centerIn: parent
//                    maximumLength: 15
//                    width: parent.width
//                }*/
//            }
            VizButton{
                iconSource: rootStyle.getIconPath("time/slower")
                backGroundVisible: false
                onClicked: {
                    view.currentPage = 0
                }
            }

            VizButton{
                iconSource:  rootStyle.getIconPath("left_dark")
                backGroundVisible: false
                onClicked: {
                    view.currentPage--
                }
            }

            Rectangle{
                id: pageNumber
                color: "white"
                border.color: "blue"
                border.width: 2
                radius: 4
                anchors.verticalCenter: parent.verticalCenter
                height: 30*rootStyle.uiScale
                width: 60*rootStyle.uiScale
                TextEdit{
                    id: pageLineEdit                    
                    color: "black"
                    font.pixelSize: rootStyle.fontSize
                    horizontalAlignment: TextEdit.AlignHCenter
                    anchors.centerIn: parent
                    readOnly: true
                }
            }

            VizButton {
                iconSource: rootStyle.getIconPath("right_dark")
                backGroundVisible: false
                onClicked: {
                    view.currentPage++
                }
            }
            VizButton{
                iconSource: rootStyle.getIconPath("time/faster")
                backGroundVisible: false
                onClicked: {
                    view.currentPage = view.numPages-1
                }
            }

            VizButton{
                iconSource: rootStyle.getIconPath("time/zoomIn")
                backGroundVisible: false
                onClicked: {
                    view.zoom = view.zoom*1.1
                }
            }
            VizButton{
                iconSource: rootStyle.getIconPath("time/zoomOut")
                backGroundVisible: false
                onClicked: {
                    view.zoom = view.zoom/1.1
                }
            }
        }
    }
    Rectangle{
        id:pdfDoc
        anchors.top: buttonBar.bottom
        width: parent.width
        anchors.bottom: parent.bottom
        clip: true
        color: "transparent"
        PDFView{
            id: view
            width: parent.width
            height: parent.height
            onCurrentPageChanged: {
                x = 0
                y = 0
                pageLineEdit.text = (view.currentPage+1)+" / "+ (view.numPages)
            }
            onZoomChanged: {
                x = 0;
                y = 0;
            }

            onCorruptedPDFError:
            {
                pdfViewer.visible=false
                //PresentationGUI.showCorruptedPDFError()
            }

            onEncryptedPDFError:
            {
                pdfViewer.visible=false
                //PresentationGUI.showEncryptedPDFError()
            }

            visible: (source !== "")
            MouseArea{
                anchors.fill: parent
                drag.target: view
                drag.axis: Drag.XandYAxis
                drag.minimumX: 0
                drag.minimumY: 0
                drag.maximumX: pdfViewer.width - view.width
                drag.maximumY: pdfViewer.height - view.height
            }
        }
    }

    Rectangle{
        id: dragger
        color: "blue"
        x: pdfViewer.width - width/2
        y: pdfViewer.height - height/2
        width: 25*rootStyle.uiScale
        height: 25*rootStyle.uiScale
        rotation: 45
        states: State {
            name: "entered"
            when: draggerMouseArea.containsMouse
            PropertyChanges { target: root; cursorShape: Qt.SizeFDiagCursor }
        }
        MouseArea{
            id: draggerMouseArea
            z: 2
            anchors.fill: parent
            property int mx
            property int my
            function handleMouse(mouse){
                if(mouse.buttons & Qt.LeftButton){
                    pdfViewer.width = Math.max(200*rootStyle.uiScale, pdfViewer.width + (mouse.x-mx))
                    pdfViewer.height = Math.max(200*rootStyle.uiScale, pdfViewer.height + (mouse.y -my))
                }
            }
            onPositionChanged: handleMouse(mouse)
            onPressed: {
                mx = mouse.x
                my = mouse.y
            }
        }
    }
}
