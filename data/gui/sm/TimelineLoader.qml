import QtQuick 2.1
import "VizComponents"

Item {
    id: timelineLoaderWrapper
    objectName: "timelineLoader"
    anchors.left: elementList.minimized ? toolBoxId.right : elementList.right
    anchors.right: parent.right

    //! Signals
    signal timelineLoaded(bool state)

    property alias timelineObject:timelineLoader.item

    //!properties
    property bool minimized: true

    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    onMinimizedChanged: {
        root.eventModeOn = !minimized
        if(minimized){
            height = 0
            timelineLoader.source = ""
        }
        else{
            timelineLoader.source = "Timeline.qml"
            height = timelineLoader.height
        }
    }

    Rectangle{
        anchors.fill: parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
    }


    Behavior on height {
        NumberAnimation{
            duration: 500
            easing.type: Easing.OutExpo
        }
    }

    Loader{
        id: timelineLoader
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
    }
}
