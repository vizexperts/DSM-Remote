import QtQuick 2.1
Item{
    id: contextualContainer

    property int pointerOffset
    property string _type: ""
    property  int mouseButtonClicked: Qt.RightButton

    property alias currentContextualItem: loader.item
    //    transformOrigin: Item.Bottom
    // Pointer
    Item {
        width: contextualContainer.width
        height: 35
        anchors.top: parent.bottom
        anchors.topMargin: 0
        anchors.left: parent.left
        clip: true
        scale: rootStyle.uiScale
        Rectangle{
            width: 20
            height: 20
            color: "#641a1515"
            radius: 2
            anchors.top: parent.top
            anchors.topMargin: -10
            anchors.horizontalCenter: parent.left
            anchors.horizontalCenterOffset: pointerOffset
            rotation: 45
        }
    }

    property bool moved: false

    MouseArea{
        id: backgroundMouseArea
        anchors.fill: parent
        drag.target: contextualContainer
        drag.axis: Drag.XandYAxis
        drag.minimumX: 0
        drag.maximumX: root.width - contextualContainer.width
        drag.minimumY: 0
        drag.maximumY: root.height - contextualContainer.height
        onPositionChanged: {
            if(mouse.buttons & Qt.LeftButton){
                moved = true
            }
        }
    }

    // background
    Rectangle{
        color: "#641a1515"
        anchors.fill: parent
        radius: 2
    }
    Image{
        width: 25
        height: 25
        scale: rootStyle.uiScale
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 5
        anchors.rightMargin: 5
        source: rootStyle.getIconPath("add")
        rotation: 45
        MouseArea{
            anchors.fill: parent
            onClicked:{
                unload()
            }
        }
    }


    objectName: "contextualMenu"
    height: loader.height
    width: loader.width

    // Setting initial scale
    scale : 0.1

    // Initial visibility
    visible: false

    property int posX : 0
    property int posY : 0

    function reposition(){
        // Set position
        // Set pointer in the middle
        pointerOffset = width/2

        contextualContainer.x = posX - (width/2)

        // position inside the screen area
        if(contextualContainer.x < 0){
            pointerOffset += contextualContainer.x;
            contextualContainer.x = 0;
        }
        else if(contextualContainer.x > root.width - width){
            pointerOffset -= ((root.width - width) - contextualContainer.x)
            contextualContainer.x = root.width - width
        }

        contextualContainer.y = (root.height - posY) - height - 40

        // position inside the screen area
        if(contextualContainer.y < 0){
            contextualContainer.y = 0
        }
        else if(contextualContainer.y > root.height - height){
            contextualContainer.y = root.height - height
        }
    }

    // Loads file typeContextual.qml for object at (posX,posY)
    function load(type, clickLeft, posX, posY){
//        console.log("type="+type)
        if(statusBar.applicationBusy){
            return
        }

        // Do nothing if type is not specified
        if(type === ""){
            return
        }
        if(type === "MilitaryLayerPoint" || type === "PointLayer" || type === "LineLayer" || type === "PolygonLayer" || type === "ModelLayer"){
            return
        }
        mouseButtonClicked = clickLeft ? Qt.LeftButton : Qt.RightButton

        moved = false

        // Set visibility to false
        visible = false
        // Check if position specified is valid or not
        if((posX < 0) || (posX > root.width)){
            return
        }
        if((posY < 0) || (posY > root.height)){
            return
        }
        // Set visible if position is valid
        visible = true
        // Set source file based on type specified

        if(_type !== type){
            //refresh
            loader.source = ""
            _type = type

            if(root.eventModeOn){
                loader.source = "ContextualWindows/EventContextual.qml"
            }
            else{
                loader.source = "ContextualWindows/" + _type + "Contextual.qml"
            }
            scale = 1
        }

        contextualContainer.posX = posX
        contextualContainer.posY = posY
        reposition()
        smpFileMenu.enableResizeArea(contextualContainer)
    }

    // Unload file
    function unload(){
        //        loader.item.reset()
        _type = ""
        loader.source = ""
        moved = false
        height =0
        width =0
        smpFileMenu.resizeMouseArea.visible =false
        visible = false
        scale = 0.0
    }

    Connections{
        target: root
        onEventModeOnChanged:{
            var type = _type
            unload()
            //            load(type,posX, posY)
        }
    }

    Loader{
        id: loader
        source: ""
        //anchors.centerIn: parent
        anchors.fill: parent
        onStatusChanged: {
            if (loader.status == Loader.Ready) {
                    loader.item.mouseButtonClicked = contextualContainer.mouseButtonClicked
            }
        }
    }

    Behavior on scale{
        PropertyAnimation{
            duration: 800
            easing.type: Easing.OutBack
        }
    }

    property int minimumWidth
    property int minimumHeight

    function setMinWidthHeight(){
//        console.log("here")
        height = minimumHeight
        width = minimumWidth
    }

    function setInitialMinWidthHeight(minWidth,minHeight){
        minimumHeight =minHeight
        minimumWidth =minWidth
    }

    onMinimumHeightChanged: setMinWidthHeight()
    onMinimumWidthChanged: setMinWidthHeight()
}
