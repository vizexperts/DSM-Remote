import QtQuick 2.1
import "VizComponents"
import QtQuick.Layouts 1.0
//! LatLong bar
Rectangle{
    id: latLongBar
    z:10
    color: "transparent"
    property string dateFormat: "ddd MMM dd/yyyy hh:mm:ss"
    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    Rectangle{
        width: parent.height
        height: parent.width
        color: rootStyle.colorWhenDefault
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#00000000"
            }

            GradientStop {
                position: 1
                color: "#100000000"
            }
        }
        transform: [
            Rotation{
                origin.x: 0
                origin.y: 0
                angle: -90
            },
            Translate{
                y: height
            }
        ]
    }

    Timer{
        interval: 10
        running: (application_mode !== application_mode_NONE) && (!running_from_qml)
        repeat: true
        onTriggered: onPositionChanged()
    }

    width: rowlayout.width + 100//Width of parent window dependent on the Width of "latlong+MGRS" string.
    height: rowlayout.height
    RowLayout{
        id:rowlayout
        anchors.left: parent.left
        anchors.leftMargin: 90
        spacing: 4
        VizLabel{
            id: positionTextLabel
            fontSize: rootStyle.fontSize + 3
            text: "°"
            textAlignment: Qt.AlignLeft
            textColor: "#ffffff"
            fontStyle: lato.name
        }
        VizLabel{
            id: eyePositionTextLabel
            fontSize: rootStyle.fontSize + 3
            textColor: "#ffffff"
            textAlignment: Qt.AlignLeft
            fontStyle: lato.name
        }
        VizLabel{
            id: mgrsString
            visible: rootStyle.mgrsVisibility;
            fontSize: positionTextLabel.fontSize+ 3
            textColor: "#ffffff"
            textAlignment: Qt.AlignLeft
            fontStyle: lato.name
        }
        VizLabel{
            id: mapSheetID
            visible: rootStyle.showMapSheet;
            fontSize: positionTextLabel.fontSize+ 3
            textColor: "#ffffff"
            fontStyle: lato.name
            textAlignment: Qt.AlignLeft

        }
        VizLabel{
            id: gridReference
            visible: rootStyle.indianGrVisibility;
            fontSize: positionTextLabel.fontSize+ 3
            textColor: "#ffffff"
            fontStyle: lato.name
            textAlignment: Qt.AlignLeft

        }
    }


    function latLongToGlbUnit(latlng)
    {
        var st = latlng.split(" ");
        var str = rootStyle.latDegDecToGlobal(st[0])+"  "+rootStyle.longDegDecToGlobal(st[1])+"  ELE: "+ rootStyle.convertAltitudeToGlobal(st[2]);
        return str;
    }
    function altiToGlbUnit(alti)
    {
        var str ="  ALT: "+ rootStyle.convertAltitudeToGlobal(alti);
        return str;
    }
    function grToFixLength(gr)
    {
        if(gr === "NA")
            return gr;

        var st = gr.split(" ");
        var str = "";
        if(rootStyle.showZone )
        {
            str=st[0]+ " "
        }

        if(rootStyle.showGR)
        {
            str=str+st[1]+ " "
        }

        var east = st[2];
        var easting = east.substring(0, rootStyle.numOfGrDigits)
        str= str+ easting

        str= str+" ";
        var north = st[3]
        var northing = north.substring(0, rootStyle.numOfGrDigits)
        str= str+ northing

        return str;


    }

    function onPositionChanged(){
        if(application_mode !== application_mode_NONE)
        {
            positionTextLabel.text = latLongToGlbUnit(BottomPaneGUI.positionText);
            mgrsString.text = BottomPaneGUI.mgrsText

            if(rootStyle.indianGrVisibility)
            gridReference.text=grToFixLength(BottomPaneGUI.gridText)

            if(rootStyle.showMapSheet)
            mapSheetID.text = BottomPaneGUI.mapSheetText

            eyePositionTextLabel.text=altiToGlbUnit(BottomPaneGUI.eyePositionText)

        }
    }
}
