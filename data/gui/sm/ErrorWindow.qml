import QtQuick 2.1
import "VizComponents"
import "VizComponents/javascripts/functions.js" as Functions
import QtQuick.Layouts 1.0

Item{
    id: rectangle10
    objectName: "errorWindow"

    width:   root.width
    height:  root.height

    signal okButtonClicked()
    signal cancelButtonClicked()
    signal close()

    property string titleText : "defaultTitleText"
    property string messageText : "defaultMessageText"
    property bool  critical: false
    
    property alias okButtonVisible: okButton.visible
    property alias cancelButtonVisible: cancelButton.visible
    property alias closeButtonVisible: closeButton.visible
    
    property alias okButtonText: okButton.text
    property alias cancelButtonText: cancelButton.text
    property alias closeButtonText: closeButton.text

    MouseArea{
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        onClicked: {}
        onDoubleClicked: {}
        hoverEnabled: true
        onEntered: root.cursorShape = Qt.ArrowCursor
        onWheel: wheel.accepted = true
    }

    Component.onCompleted: {
        errorLoader.connectOkCancel()
    }

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        opacity: 0.0
    }

    Rectangle{
        anchors.fill: back
        gradient: rootStyle.gradientWhenDefault
        opacity: 0.8
        radius: 2
        border.color: rootStyle.colorWhenSelected
        border.width: rootStyle.borderWidth
    }

    Item{
        id: back
        anchors.centerIn: parent
        width: parent.width/3
        height: Math.max(titleRectangle.height+ message.height + buttonRow.height + 70, 180)

        Rectangle{
            id: titleRectangle
            anchors.top: parent.top
            width: parent.width
            height: title.height
            radius: 1
            gradient: rootStyle.gradientWhenSelected
            VizLabel{
                id: title
                text: titleText
                width: parent.width
                textColor: rootStyle.colorWhenDefault
                fontSize: message.fontSize*1.5
            }
            VizButton{
                anchors.right: parent.right
                iconSource: rootStyle.getIconPath("close")
                height: title.height
                width: height
                opacity: 0.7
                backGroundVisible: false
                onClicked: close()
            }
        }
        VizLabel{
            id: message
            text: messageText
            anchors.horizontalCenter: parent.horizontalCenter
            textWidth: parent.width - 50
            anchors.top: titleRectangle.bottom
            anchors.topMargin: 30
            wrapMode: TextEdit.Wrap
        }
        Image {
            id: errorImage
            source: rootStyle.getIconPath("error")
            anchors.left: message.left
            anchors.verticalCenter: parent.verticalCenter
            width: 35
            height: 35
            anchors.rightMargin: 30
            visible: critical
        }

        RowLayout{
            id:buttonRow
            anchors.top: message.bottom
            anchors.topMargin: 30
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: parent.width/6
            height: childrenRect.height

            VizButton
            {
                id:okButton
                text:"Ok"
                onClicked: {
                    okButtonClicked()
                    close()
                }
            }
            VizButton
            {
                id: cancelButton
                text: "Cancel"
                onClicked: {
                    cancelButtonClicked()
                    close()
                }
            }
            VizButton
            {
               id:closeButton
               text:"Cancel"
               visible:false
               onClicked:close()
            }
        }
    }
}
