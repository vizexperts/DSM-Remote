import QtQuick 2.1
import QtQuick.Layouts 1.0
import "VizComponents"

//! This menu gets opened when user plays an auto generated tour(GPS Track/Checkpoint Path)

Rectangle {
    id: cpTourMenu
    objectName: "CheckpointTourMenu"
    color: "transparent"
    width: bottomLoader.width
    height: 150

    Component.onCompleted: {
        bottomLoaderWrapper.cptourMenuLoaded()
    }

    Component.onDestruction: {
        closed()
    }

    //! Playback controls
    signal playTour()
    signal pauseTour()
    signal stop()
    signal increaseSpeedFactor()
    signal decreaseSpeedFactor()
    signal sliderValueChanged()
    signal closed()

    property int totalTime
    property alias elapsedTime: tourSlider.value
    property string tourName: ""
    property bool animationRunning: false
    property string speedFactor: ""


    Item{
        id: player
        width: parent.width
        height: parent.height

        Behavior on width {
            PropertyAnimation{ duration: 100}
        }

        Rectangle{
            anchors.fill: parent
            gradient: rootStyle.gradientWhenDefault
            opacity: 0.5
        }

        VizButton{
            id: closeButton
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 10
            anchors.rightMargin: 10
            iconSource: rootStyle.getIconPath("close")
            onClicked: bottomLoaderWrapper.unload()
            backGroundVisible: false
        }
        VizButton{
            id: minimizeButton
            anchors.top: closeButton.top
            anchors.right: closeButton.left
            anchors.rightMargin: 10
            iconSource: rootStyle.getIconPath("minimize")
            onClicked: parent.state = (parent.state == "MINIMIZED") ? "" : "MINIMIZED"
            backGroundVisible: false
        }

        states: State{
            name: "MINIMIZED"
            PropertyChanges{target: cpTourMenu; height: 50}
            PropertyChanges {target: bottomLoaderWrapper; height: tourMenu.height}
            PropertyChanges {target: minimizeButton; iconSource: rootStyle.getIconPath("maximize") }
            PropertyChanges {target: tourSlider; bottomMargin: 10}
            PropertyChanges {target: tourControls; spacing: 5}
            PropertyChanges {target: tourControls; horizontalCenterOffset : -(cpTourMenu.width - tourControls.width)/2}
            PropertyChanges {target: tourSlider; width: (minimizeButton.x - tourControls.width - 20)}
            PropertyChanges {target: tourSlider; horizontalCenterOffset: (5+tourControls.width - (cpTourMenu.width - tourSlider.width)/2)}
        }

        VizSlider{
            id: tourSlider
            minimumValue: 0
            maximumValue: totalTime
            width: parent.width

            property int bottomMargin: 25
            property int horizontalCenterOffset: 0
            Behavior on bottomMargin {NumberAnimation{duration: 100}}

            anchors.bottom: parent.bottom
            anchors.bottomMargin: bottomMargin
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: horizontalCenterOffset

            labelVisible: false
            value: 0

            Behavior on width {
                NumberAnimation{
                    duration: 100
                }
            }

            VizLabel{
                anchors.left: parent.left
                anchors.bottom: parent.top
                anchors.bottomMargin: 10
                text: "Elapsed : " + elapsedTime + " sec"
            }

            VizLabel{
                anchors.right: parent.right
                anchors.bottom: parent.top
                anchors.bottomMargin: 10
                text: "Remaining : " + (totalTime - elapsedTime) + " sec/ " + totalTime + " sec"
            }
            VizLabel{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.top
                anchors.bottomMargin: 10
                text: Qt.formatDateTime(currentMissionDateTime, "ddd MMM dd yyyy hh:mm:ss") + tourName
            }
        }

        Row{
            id: tourControls
            anchors.horizontalCenter: parent.horizontalCenter
            property int  horizontalCenterOffset: 0
            anchors.horizontalCenterOffset: horizontalCenterOffset
            Behavior on horizontalCenterOffset {NumberAnimation{duration: 200}}
            anchors.top: parent.top
            anchors.topMargin: 10
            spacing: 10
            VizButton{
                id: slowButton
                iconSource: rootStyle.getIconPath("time/slower")
                backGroundVisible: false
                onClicked: decreaseSpeedFactor()
            }
            VizButton{
                id: playPauseButton
                iconSource: animationRunning ?  rootStyle.getIconPath("time/pause") : rootStyle.getIconPath("time/play")
                backGroundVisible: false
                onClicked: {
                    if(animationRunning){
                        pauseTour()
                    }
                    else{
                        playTour()
                    }
                }
            }
            VizButton{
                id:stopButton
                iconSource: rootStyle.getIconPath("time/stop")
                backGroundVisible: false
                onClicked: stop()
            }
            VizButton{
                id:fastButton
                iconSource: rootStyle.getIconPath("time/faster")
                backGroundVisible: false
                onClicked: increaseSpeedFactor()
            }
            VizLabel{
                id: speedFactorLabel
                text: "Speed:"
            }
            VizLabel{
                id: speedFactorValueLabel
                width: 50
                text: speedFactor + "X"
                textAlign: "left"
            }

        }
    }
}
