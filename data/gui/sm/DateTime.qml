import QtQuick 2.1

Item {

    property alias hours: time.hours
    property alias minutes: time.minutes

    property alias date:date1.date
    property alias year: date1.year

    property alias month: date1.month
    property alias day: date1.day

    Component.onCompleted: update()

    function update(today){
        if(typeof(today)==='undefined')
            today = new Date();

        date1.update(today)
        time.update(today)
    }


    Date{
        id:date1
    }

    Time{
        id:time
    }

}
