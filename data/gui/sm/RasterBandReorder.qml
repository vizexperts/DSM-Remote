import QtQuick 2.1
import "../components"
import "VizComponents"

Item {
    id: rasterBandReorder

    objectName: "rasterBandReorderObject"

    width: 200
    height: 220

    property int bandNo: 1
	property bool rasterValid: false

    Component.onCompleted: {
        menuHolder.loaded("RasterBandReorder")
		if(!rasterValid){
            menuHolder.minimized = true
		}
    }

	// Collaboration
	signal handleApplyButton()
	signal handleCancelButton()

	
    VizLabel{
        id: close
        width: parent.height - 30
        height: 35
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 9
        color: "transparent"
        text: "Band Ordering"

        Rectangle{

            height: 30
            width: 30
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5
            color: "transparent"

            Image {
                id: closeIcon
                source: rootStyle.getIconPath("close")
                anchors.fill: parent
				anchors.right: parent.right
				anchors.top : parent.top				
            }

            MouseArea {
                anchors.fill: parent;
                onClicked:  {
                    menuHolder.minimized = true
					handleCancelButton()
                }
            }
        }
    }


    Rectangle{
        id: boundary
        anchors.margins: 10
        anchors.fill: parent
        color: "transparent"
        border.width: 2
        radius: 2
        border.color: "white"
    }

    Item{
        anchors.fill: boundary
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;

        VizLabel{
            id: band1Label
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 40
            text: "Band 1"
        }

        VizComboBox{
            id: band1ComboBox
            objectName: "band1ComboBox"
            anchors.left: band1Label.right
            anchors.leftMargin: 15
            anchors.top: band1Label.top
            listModel: subdatasetListModel //band1ListModel
        }


        VizLabel{
            id: band2Label
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: band1Label.bottom
            anchors.topMargin: 5
            text: "Band 2"
        }

        VizComboBox{
            id: band2ComboBox
            objectName: "band2ComboBox"
            anchors.left: band2Label.right
            anchors.leftMargin: 15
            anchors.top: band2Label.top
            listModel: subdatasetListModel//band2ListModel
        }

		VizLabel{
            id: band3Label
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: band2Label.bottom
            anchors.topMargin: 5
            text: "Band 3"
        }

        VizComboBox{
            id: band3ComboBox
            objectName: "band3ComboBox"
            anchors.left: band3Label.right
            anchors.leftMargin: 15
            anchors.top: band3Label.top
            listModel: subdatasetListModel//band3ListModel
        }
		
		VizButton{
			id: applyButton
            anchors.horizontalCenter: parent.horizontalCenter
			anchors.top: band3Label.bottom
			anchors.topMargin: 15				
            text:"Apply"
            MouseArea {
                anchors.fill: parent;
                onClicked:  {
					handleApplyButton()
                    menuHolder.minimized = true
                }
            }			
        }		

    }

    ListModel{
        id: band1ListModel
        ListElement{name:"1"; uID : "1"}
        ListElement{name:"2"; uID : "2"}
        ListElement{name:"3"; uID : "3"}
    }

    ListModel{
        id: band2ListModel
        ListElement{name:"1"; uID : "1"}
        ListElement{name:"2"; uID : "2"}
        ListElement{name:"3"; uID : "3"}
    }

    ListModel{
        id: band3ListModel
        ListElement{name:"1"; uID : "1"}
        ListElement{name:"2"; uID : "2"}
        ListElement{name:"3"; uID : "3"}
    }


    transitions: Transition{
        AnchorAnimation{
            duration: 100
        }
    }
}
