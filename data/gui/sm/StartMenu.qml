import QtQuick 2.1
import "VizComponents"

Item {
    id: startMenu
    objectName: "startMenu"
    height: currentUserLabel.height
    width: currentUserLabel.width + 30

    property alias name: currentUserLabel.text

    signal selectOption(string option)

    onSelectOption: {
        if(option === "Database Settings"){
            smpFileMenu.load("UserSettingsMenu.qml")
        }
        else if(option === "Log off"){
            desktop.logoff()
            loc.login(false)
        }
        else if(option == "Quit"){
            desktop.closeApplication()
        }
        startMenu.state = ""
    }

    VizLabel{
        id: currentUserLabel
        fontSize: rootStyle.fontSize*1.5
        text: "VizExperts"
        MouseArea{
            anchors.fill: parent
            onClicked: menuButton.clicked()
        }
    }
    ListModel{
        id: startMenuModel
        ListElement{optionName: "Database Settings"}
        ListElement{optionName: "Log off"}
        ListElement{optionName: "Quit"}
    }

    states: State{
        name: "ACTIVE"
        PropertyChanges{
            target: startMenu
            z: parent.z + 10
        }
        PropertyChanges{
            target: list
            height: currentUserLabel.height*(list.count)
        }
        PropertyChanges {
            target: list
            visible: true
        }
    }

    VizButton {
        id: menuButton
        iconSource: (parent.state == "") ? rootStyle.getIconPath("down") : rootStyle.getIconPath("up")
        anchors.right: parent.right
        anchors.rightMargin: 1
        anchors.verticalCenter: parent.verticalCenter
        scale: 0.5
        onClicked: parent.state = (parent.state == "") ? "ACTIVE" : ""
    }

    Rectangle{
        anchors.fill: list
        color: rootStyle.colorWhenDefault
        border.color: rootStyle.borderColor
        border.width: 2
    }

    ListView{
        Behavior on height{
            PropertyAnimation{
                duration: 600
                easing.type: Easing.OutBack
            }
        }
        id: list
        model: startMenuModel
        visible: false
        height: 0
        width: parent.width
        anchors.top: parent.bottom
        interactive: false
        delegate: VizLabel{
            text: optionName
            width: list.width
            textColor: mouseArea.containsMouse ? rootStyle.colorWhenDefault : rootStyle.textColor
            color: mouseArea.containsMouse ? rootStyle.colorWhenSelected : "transparent"
            MouseArea{
                id: mouseArea
                anchors.fill: parent
                onClicked: selectOption(optionName)
                hoverEnabled: true
            }
        }
    }
}
