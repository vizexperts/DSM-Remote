import QtQuick 2.1
import "VizComponents"

Rectangle {
    id: pdfMenu
    objectName: "pdfMenu"
    color: "transparent"
    height: 150*rootStyle.uiScale
    width:  bottomLoader.width

    Component.onCompleted: {
        bottomLoaderWrapper.pdfMenuLoaded()
        populatePDFList()
    }

    Component.onDestruction: {
        closed()
    }

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        opacity: 0.7
    }

    signal closed()

    // presentation signals
//    signal addNewPDF(url filePath)
    signal addNewPDF()
    signal openDocument(string name)
    signal deleteDocument(string name)
    signal populatePDFList()
    property string currentItemName

//    function browserAccepted(){
//        addNewPDF(fileBrowser.fileUrl)
//    }

    VizButton{
        id: closeButton
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 10
        anchors.rightMargin: 10
        iconSource: rootStyle.getIconPath("close")
        onClicked: bottomLoaderWrapper.unload()
        backGroundVisible: false
    }

    VizButton{
        id: addPdfButton
        anchors.left: parent.left
        anchors.leftMargin: 100
        anchors.top: parent.top
        anchors.topMargin: 10
        text: "Add New PDF"
        onClicked: {
            addNewPDF()
//            fileBrowser.openBrowser("Browse for Pdf", "*.pdf")
//            fileBrowser.accepted.connect(browserAccepted)
        }
    }

    ListView{
        id: listView
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        clip: true
        spacing: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.top: addPdfButton.bottom
        anchors.topMargin: 10
        model: (typeof(pdfListModel) != "undefined")? pdfListModel : "undefined"
        orientation: ListView.Horizontal
        boundsBehavior : Flickable.StopAtBounds
        delegate: Rectangle{
            id: wrapper
            color: "transparent"
            Rectangle{
                anchors.fill: parent
                gradient: if(name === currentItemName)
                              rootStyle.gradientWhenSelected
                          else
                              rootStyle.gradientWhenDefault
                radius: 5
                border.color: rootStyle.borderColor
                border.width: rootStyle.borderWidth
            }
            width: height*(4/3)
            height: listView.height
            radius: 5
            VizLabel{
                anchors.top: parent.top
                anchors.topMargin: 5
                textWidth: parent.width - 10
                text: name
                wrapMode: TextEdit.WrapAnywhere
            }
            MouseArea{
                anchors.fill: parent
                onClicked: currentItemName = name
                onDoubleClicked: openDocument(name)
            }
            Image{
                width: 30
                height: 30
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.margins: 5
                source: rootStyle.getIconPath("delete")
                MouseArea{
                    anchors.fill: parent
                    onClicked: deleteDocument(name)
                }
            }
        }
    }
}
