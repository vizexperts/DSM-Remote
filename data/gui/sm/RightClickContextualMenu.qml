import QtQuick 2.1
import "VizComponents"

Item{
    id: contextualMenu
    objectName: "rightContextualMenu"
    width: contextMenuListView.width
    height: contextMenuListView.height
    scale: 0

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
    }

    Behavior on scale {
        PropertyAnimation{
            easing.type: Easing.OutBack; duration: 100
        }
    }

    Connections{
        target: restOfArea
        onClicked: closeContextualMenu()
    }

    signal selectItem(int index)

    function openContextualMenu(listModel){
        x = cursor.x
        y = cursor.y

        contextMenuListView.model = listModel
        scale = 1

        if(x > root.width - width){
            x = root.width - width
        }

        if(y > root.height - height){
            y = root.height - height
        }

        restOfArea.enabled = true
    }

    function closeContextualMenu(){
        restOfArea.enabled = false
        contextMenuListView.model = "null"
        scale = 0
    }

    onSelectItem: closeContextualMenu()

    ListView{
        id: contextMenuListView

        height: childrenRect.height

        snapMode: ListView.NoSnap
        boundsBehavior: Flickable.StopAtBounds
        highlightFollowsCurrentItem: true
        highlight: Rectangle{
            width: contextMenuListView.width
            color: "#64ffffff"
            radius: 2
            Behavior on y{
                NumberAnimation{
                    duration: 100
                }
            }
        }

        delegate: VizLabel{
            id: contextMenuItemDelegate
            Component.onCompleted: {
                contextMenuListView.width = Math.max(contextMenuListView.width, width)
                width = contextMenuListView.width
            }
            Connections{
                target: contextMenuListView
                onWidthChanged: width = Math.max(contextMenuListView.width, width)
            }

            textAlignment: Qt.AlignLeft
            text: name
            textColor: contextMenuListView.currentIndex == index ? "black" : rootStyle.textColor
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onEntered: contextMenuListView.currentIndex = index
                onClicked:{
                    //uID is Bind With Enum so we can call fixed Switch case
                    selectItem(contextMenuListView.model[contextMenuListView.currentIndex].uID)
                }
            }
        }
    }
}
