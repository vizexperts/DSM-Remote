import QtQuick 2.1
import QtMultimedia 5.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import "VizComponents"

Item{
    id: infoEvent
    width: 500

    height:(videoPath!="")?column.height+videoRect.height+videoButtonRect.height+100:500
    property string title:""
    property string description:""

    property string audioPath:""
    property string videoPath:""
    property string imagePath:""
    property font descFont
    property color descColor
    property bool videoMute: true
    property bool audioMute:false

    Rectangle{
        id: infoEventDelegate

        color: "transparent"
        width: parent.width
        height: parent.height
        Rectangle{
            width: parent.height
            height: parent.width
            //color: rootStyle.colorWhenDefault
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#ee000000"
                }

                GradientStop {
                    position: 1
                    color: "#64000000"
                }
            }
            transform: [
                Rotation{
                    origin.x: 0
                    origin.y: 0
                    angle: -90
                },
                Translate{
                    y: height
                }
            ]
        }

        Column{
            id: column
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.right: attachedImage.left
            spacing: 20
            visible: (audioPath!="" || description!="")
            RowLayout{
                id: labelRow
                visible: (audioPath!="")

                VizLabel{
                    id: volumeLabel
                    text: "Mute audio"
                    fontSize: rootStyle.fontSize + 5
                    fontWeight: Font.Bold
                    textAlignment: Qt.AlignLeft
                }

                VizButton{
                    id: aMute
                    iconSource: (audioMute)?rootStyle.getIconPath("mute"):rootStyle.getIconPath("unmute")
                    tooltip: (audioMute===false)?"UnMute Audio":"Mute Audio"
                    //backGroundVisible: true
                    onClicked: {
                        //console.log("onclicked" + mediaplayer.muted)
                        if(audioMute){
                            audioMute=false
                            EventGUI.muteAudio(title,audioMute)
                        }
                        else{
                            audioMute=true
                            EventGUI.muteAudio(title,audioMute)
                        }
                    }
                }
            }

            VizLabel{
                id: descriptionLabel
                text: description
                textAlignment: Qt.AlignLeft
                //fontSize: rootStyle.fontSize + 5
                visible: (description!="")
                marquee: true
                width: parent.width
                fontSize: descFont.pixelSize
                fontStyle:descFont.family
                fontWeight: descFont.weight
                textColor: descColor
            }
        }
        Image {
            id: attachedImage
            anchors.right: parent.right
            //anchors.rightMargin: 5
            visible: (imagePath !== "")
            source: (visible) ?  "file:/" + imagePath : rootStyle.getIconPath("addImage")
            height: column.height
            width: visible? height*4/3 : 0
        }

        MouseArea{
            id: imageDrag

            anchors.fill: attachedImage
            drag.target: infoEvent
            drag.axis: Drag.XAndYAxis
            enabled: true
        }

        //Drag.active: preview

        MouseArea{
            id: videoDrag

            anchors.fill: videoRect
            drag.target: infoEvent
            drag.axis: Drag.XAndYAxis
            enabled: true
        }

        Rectangle{
            id: videoRect

            anchors.top: column.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: videoButtonRect.top
            visible:(videoPath!=="")
            color: "transparent"

            MediaPlayer {
                id: mediaplayer
                source: "file:/" + videoPath
                autoPlay: true
                muted: true
                loops: MediaPlayer.Infinite
            }

            VideoOutput {
                id: vidOutput
                anchors.fill: videoRect
                source: mediaplayer
                fillMode: VideoOutput.Stretch
                opacity: 0.2
            }
        }

        Rectangle{
            id: videoButtonRect

            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottomMargin: 3
            visible:(videoPath!=="")
            height: 40
            color:"black"

            RowLayout{
                id: handlers
                //height: 30
                //horizontalCenter: parent.horizontalCenter
                VizLabel{
                    id: videoVolumeLabel
                    text: "  Mute video"
                    fontSize: rootStyle.fontSize + 5
                    fontWeight: Font.Bold
                    textAlignment: Qt.AlignLeft
                }

                VizButton{
                    id: vMute
                    iconSource: (mediaplayer.muted)?rootStyle.getIconPath("mute"):rootStyle.getIconPath("unmute")
                    tooltip: (mediaplayer.muted===false)?"UnMute Video":"Mute Video"
                    //backGroundVisible: false
                    onClicked: {
                        //console.log("onclicked" + mediaplayer.muted)
                        if(mediaplayer.muted){
                            //console.log("video unmute " + mediaplayer.muted)
                            mediaplayer.muted=false
                        }
                        else{
                            //console.log("video mute " + mediaplayer.muted)
                            mediaplayer.muted=true
                        }
                    }

                }

                VizLabel{
                    id: blankVolumeLabel
                    text: "  "
                    fontSize: rootStyle.fontSize + 5
                    fontWeight: Font.Bold
                    textAlignment: Qt.AlignLeft
                }

                VizLabel{
                    id: transparencyVolumeLabel
                    text: "Transparency"
                    fontSize: rootStyle.fontSize + 5
                    fontWeight: Font.Bold
                    textAlignment: Qt.AlignLeft
                }

                VizSlider{
                    id: slider
                    visible: true
                    minimumValue: .2
                    maximumValue: 1
                    labelVisible: false
                    value: 1
                    onValueChanged: {
                        vidOutput.opacity=value
                    }
                }
            }
        }
    }

    Rectangle{
        id: closeRect
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: infoEventDelegate.top
        height: 30
        color: rootStyle.colorWhenSelected
        VizButton{
            anchors.right: parent.right
            anchors.rightMargin: 2
            anchors.top: parent.top
            anchors.topMargin: 2
            width: 20
            height: 20
            iconSource: rootStyle.getIconPath("close")
            backGroundVisible: false
            onClicked: {
                EventGUI.closePreview()
            }
        }
    }

    MouseArea{
        id: resizeArea
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: -width
        property int mx
        property int my
        states: State {
            name: "entered"
            when: resizeArea.containsMouse
            PropertyChanges { target: root; cursorShape: Qt.SizeFDiagCursor }
        }
        function handleMouse(mouse){
            if(mouse.buttons & Qt.LeftButton){
                parent.width = Math.max(width, parent.width + (mouse.x-mx))
                parent.height = Math.max(width, parent.height + (mouse.y -my))
            }
        }
        onPositionChanged: handleMouse(mouse)
        onPressed: {
            mx = mouse.x
            my = mouse.y
        }
        width: 10*rootStyle.uiScale
        height: width
        hoverEnabled: true
    }

    function playMedia(){
        //console.log("play media")
        mediaplayer.play()
    }

    function pauseMedia(){
        //console.log("pause media")
        mediaplayer.pause()
    }
}
