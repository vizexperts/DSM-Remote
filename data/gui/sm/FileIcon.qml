import QtQuick 2.1
import QtQuick.Layouts 1.0
import "VizComponents"

Rectangle{
    id: background

    color: "black"
    property bool selected : isSelected()

    function isSelected(){
        if(fileManager.state == "SEARCHING"){
            return (searchResults.currentIndex == index)
        }
        else{
            if(typeof(filesView) != "undefined"){
                return (filesView.currentIndex == index)
            }
        }

        return false
    }

    onSelectedChanged: {
        if(!selected){
            resetExportOption()
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    Behavior on height {
        NumberAnimation{
            duration: 100
        }
    }

    function resetExportOption(){
        secLayout.state = ""
    }

    MouseArea{
        id: area
        anchors.fill: parent
        onClicked: (fileManager.state == "SEARCHING") ?
                       (searchResults.currentIndex = index) : (filesView.currentIndex = index)
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        RowLayout{
            Image{
                source: rootStyle.getIconPath("sat")
                width: 30*rootStyle.uiScale
                height: width

                Layout.maximumHeight: 30*rootStyle.uiScale
                Layout.maximumWidth: 30*rootStyle.uiScale
            }
            VizLabel{
                id: project
                text: modelData
                wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
            }
        }
        RowLayout{
            spacing: 0
            VizButton {
                id: deleteButton
                iconSource: rootStyle.getIconPath("delete")
                backGroundVisible: false
                visible: (selected && (user_type == usertype_INSTRUCTOR))
                onClicked: desktop.deleteProject(modelData, tabFrame.currentTabType)
            }
            VizButton{
                id: openButton
                text: "Open"
                visible: background.selected
                onClicked: {
                    desktop.loadProject(modelData, tabFrame.currentTabType)
                }
            }
        }
    }
}
