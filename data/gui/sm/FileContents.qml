import QtQuick 2.1
import "VizComponents"
//import "ContextualWindows/PointContextual.qml"
/////////////////////////////////////////////////////////////////////////////////
//!
//! \file FileContents.qml
//!
//! \brief Contains all the UI components visible after Desktop and Login Screen
//!
/////////////////////////////////////////////////////////////////////////////////

Item{

    objectName: "smpFileMenu"
    id: fileContents

    width: root.width
    height: root.height

    //! Deafult image visible a background in place of earth when running from qml
    Image {
        source: rootStyle.getIconPath("backGroundImage")
        anchors.fill: parent
        visible: running_from_qml
        enabled: running_from_qml
    }

    property date currentMissionDateTime

    property Component popupContainer;
    Component.onCompleted: {

        currentMissionDateTime = new Date()
        popupContainer = Qt.createComponent("PopupContainer.qml")
    }
    function saveASFunction()
    {
        if(smpFileMenu.load("ProjectSaveAsPopup.qml"))
        {
            emit:saveAs();
            return true;
        }
        else
        {
            return false;
        }
    }

    //onSaveAs: smpFileMenu.load("ProjectSaveAsPopup.qml")

    signal save()
    signal saveAs()
    signal undo()
    signal redo()
    signal loaded(string menuType)
    signal closeProject()

    onCloseProject: {
        if(running_from_qml){
            desktop.minimized = false
        }
    }


    /////////
    onUndo: leftMenu.isRedoAvailable = true

    onRedo: leftMenu.isRedoAvailable = false
    /////////

    //! LatLong bar containing Home button

    LatLong{
        id: latLongBar
        objectName: "LatLongGUI"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        //anchors.bottom: parent.top
    }

    ElementList{
        id: elementList
        y: projectNameBar.height+10
    }

    ToolBox{
        id:toolBoxId
        anchors.top: projectNameBar.bottom
        anchors.topMargin: 10;
    }

    QuickLaunch{
        id: projectNameBar
        objectName: "QuickLaunchGUI"
        //anchors.left: elementList.right
    }

    //! Element to show Info event list
    InfoEventContainer{
        id: infoEventContainer
        anchors.top: projectNameBar.bottom
        anchors.topMargin: 10;
        anchors.left: elementList.minimized ? toolBoxId.right : elementList.right
    }


    MultipleSelectionMenu{
        id: multipleSelectionMenu
        Connections{
            target: restOfArea
            onClicked: multipleSelectionMenu.unload()
        }
    }

    /* ContextualMenu{
        id: contextualMenu
    }*/

    LeftMenu{
        id: leftMenu
        anchors.verticalCenter: parent.verticalCenter
    }

    TimelineLoader{
        id: timelineLoader
        anchors.bottom: bottomLoader.top
    }

    RightMenu{
        id: rightMenu
        anchors.verticalCenter: parent.verticalCenter
    }

    ContextualMenu{
        id: contextualMenu
        states: [
            State {
                name: "docked"
                when: rootStyle.docked
                AnchorChanges{
                    target: contextualMenu
                    anchors.bottom: elementList.bottom
                    anchors.left: elementList.left
                    anchors.right: elementList.right
                }
                PropertyChanges {
                    target: elementList
                    minimumWidth : contextualMenu.width
                }
                PropertyChanges {
                    target: contextualMenu
                    anchors.bottomMargin: 50
                }
            }
        ]
    }

    MouseArea{
        anchors.left: elementList.right
        anchors.right: parent.right
        height: parent.height
        acceptedButtons: Qt.AllButtons
        visible: enabled
        enabled: elementList.treeViewHasActiveFocus
        onPressed: {
            mouse.accepted = false
            elementList.treeViewHasFocus = false
        }
    }

    property alias resizeMouseArea:resizeAreamouse

    signal sizeChanged(int newWidth, int newHeight)

    MouseArea{
        id: resizeAreamouse
        visible: false

        z:10
        property int minHeight
        property int minWidth

        property int mx
        property int my
        property var popupObject
        property var contextualObject

        states: State {
            name: "entered"
            when: resizeAreamouse.containsMouse
            PropertyChanges { target: root; cursorShape: Qt.SizeFDiagCursor }
        }

        property var prevWidth
        property var prevHeight
        function handleMouse(mouse){
            if(mouse.buttons & Qt.LeftButton){
                if(popupObject){
                    prevWidth = popupObject.width
                    prevHeight = popupObject.height
                    popupObject.width = Math.max(minWidth, popupObject.width + (mouse.x-mx))
                    popupObject.height = Math.max(minHeight, popupObject.height + (mouse.y -my))
                    sizeChanged(popupObject.width-prevWidth, popupObject.height-prevHeight);
                }
            }
        }

        onPositionChanged: handleMouse(mouse)

        onPressed:{
            mx = mouse.x
            my = mouse.y
        }

        hoverEnabled: true

        Image {
            id: resizeImage
            source: rootStyle.getIconPath("resize")
            z:5
            anchors.left: parent.left
            anchors.top: parent.top
        }
    }

    property alias dateTimePicker: datePicker

    DatePicker{
        id: datePicker
        objectName: "datePicker"
        visible: false
    }

    property alias fontPickerAlias: fontPicker

    FontPicker{
        id: fontPicker
    }

    property alias colorPickerAlias: colorPicker

    ColorPicker{
        id: colorPicker
    }

    TimeDateRect{
        id: timeDate
        visible: false
    }

    TourRecorderLoader{
        id: tourRecorderLoader
    }
    
    ImagePreview{
        id: imagePreview
        anchors.centerIn: parent
    }

    PDFLoader{
        id: pdfLoader
    }

    Image{
        // To bring up the right menu
        source: rootStyle.getIconPath("Picture1")
        opacity: 0.5
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        width: 50
        height: 100
        visible: (desktop.minimized && rightMenu.minimized)
        Image {
            source: rootStyle.getIconPath("backLeft")
            width: 35
            height: 35
            anchors.top: parent.top
            anchors.topMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
        }
        MouseArea{
            anchors.fill: parent
            enabled: parent.visible
            onClicked: rightMenu.minimized = false
        }
    }

    Image {
        // to bring up the left menu
        source: rootStyle.getIconPath("Picture1")
        rotation: 180
        anchors.left: elementList.minimized?toolBoxId.right : elementList.right
        opacity: 0.5
        anchors.verticalCenter: parent.verticalCenter
        width: 50
        height: 100
        visible: (desktop.minimized && leftMenu.minimized)
        Image {
            source: rootStyle.getIconPath("backLeft")
            width: 35
            height: 35
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter
        }
        MouseArea{
            anchors.fill: parent
            enabled: parent.visible && !statusBar.applicationBusy
            onClicked: leftMenu.minimized = false
        }
    }

    BottomLoader{
        id: bottomLoader
        anchors.bottom: parent.bottom
    }


    // popup loader code

    signal popupLoaded(string type)

    property bool  isPopUpAlreadyOpen: false;
    property bool  isParentChild: false;

    property var popupObjectsList:[]

    property var popupObject

    function load(fileName){
        // Do nothing if type is not specified
        if(fileName=== ""){
            return
        }
        //isPopUpAlreadyOpen flag true means already a popup is opened so not to allow to open another popup
        //Exception:- allow Child Popup open
        //isParentChild flag is used to prevent parent popup Close
        if(isPopUpAlreadyOpen)
        {
            if((fileName==="VectorQueryBuilder.qml") ||(fileName==="QueryBuilderGIS.qml")|| (fileName==="CreateColorMap.qml")
                    || (fileName==="AddOrRenameBookmarksPopup.qml"))
            {
                isParentChild=true;
            }
            else
            {
                return false;
            }

        }

        popupObject = popupContainer.createObject(smpFileMenu , {"_fileName": fileName})

        if(fileName === "ColorLegendPopUp.qml")
        {
            popupObject.anchors.left=elementList.right
            popupObject.anchors.bottom=elementList.bottom
        }

        popupObjectsList.push(popupObject)
        if(fileName != "ColorLegendPopUp.qml")
        {
          enableResizeArea(popupObject)
          anchorPickers(popupObject)
        }
        visible = true
        return true;

    }

    // enable resize mouse area for popups and contextuals
    function enableResizeArea(popupObject){
        resizeAreamouse.visible =true
        resizeAreamouse.popupObject = popupObject
        resizeAreamouse.minHeight = popupObject.height
        resizeAreamouse.minWidth =popupObject.width

        resizeAreamouse.anchors.left= popupObject.right
        resizeAreamouse.anchors.leftMargin= -25
        resizeAreamouse.anchors.top= popupObject.bottom
        resizeAreamouse.anchors.topMargin= -25
        resizeAreamouse.anchors.right= popupObject.right
        resizeAreamouse.anchors.bottom= popupObject.bottom
        resizeAreamouse.anchors.bottomMargin= -100
        resizeAreamouse.anchors.rightMargin= -100
    }

    // anchor the DatePicker to the object now created
    function anchorPickers(popupObject){
        datePicker.anchors.left = popupObject.right
        datePicker.anchors.leftMargin = 10
        datePicker.anchors.verticalCenter = popupObject.verticalCenter

        fontPicker.anchors.left = popupObject.right
        fontPicker.anchors.leftMargin = 10
        fontPicker.anchors.verticalCenter = popupObject.verticalCenter

        colorPicker.anchors.left = popupObject.right
        colorPicker.anchors.leftMargin = 10
        colorPicker.anchors.verticalCenter = popupObject.verticalCenter
    }

    function unloadAll(){
        var numOfPopupObjects = popupObjectsList.length
        for(var i = 0; i < numOfPopupObjects; i++){
            var popupObject = popupObjectsList.pop()
            popupObject.destroy(200)
        }
        resizeMouseArea.visible =false
        colorPickerAlias.visible =false
        fontPickerAlias.visible =false
        //visible = false
    }

    // datePickers functions
    function openDatePicker(){
        datePicker.visible = true
    }

    function closeDatePicker(){
        datePicker.visible =false;
    }

    function setDateTime(dateTime){
        datePicker.setDateTime(dateTime)
    }

    function getDatePicker(){
        return datePicker;
    }

    // fontPicker Functions

    function getFontPicker(){
        return fontPicker
    }

    function setFont(fontFamily,fontSize,fontWeight){
        fontPicker.fontFamily =fontFamily
        fontPicker.fontSize =fontSize
        fontPicker.fontWeight =fontWeight
    }

    function openFontPicker(){
        fontPicker.visible =true;
    }
    function closeFontPicker(){
        fontPicker.visible = false;
    }

    // color Picker functions

    function openColorPicker(){
        colorPicker.visible =true
    }

    function closeColorPicker(){
        colorPicker.visible =false
    }

    function setColor(color){
        colorPicker.currentColor  =color
    }

    function getColorPicker(){
        return colorPicker;
    }

    function loadTimeDateRectAt(position , target){
        timeDate.anchors.horizontalCenter = timelineLoader.left
        timeDate.anchors.bottom = timelineLoader.top
        timeDate.loadAt(position,target)
    }

    function anchorContextualPickers(){
        datePicker.anchors.left = contextualMenu.right
        datePicker.anchors.leftMargin = 10
        datePicker.anchors.verticalCenter = contextualMenu.verticalCenter

        fontPicker.anchors.left =contextualMenu.right
        fontPicker.anchors.leftMargin = 10
        fontPicker.anchors.verticalCenter = contextualMenu.verticalCenter

        colorPicker.anchors.left =contextualMenu.right
        colorPicker.anchors.leftMargin = 10
        colorPicker.anchors.verticalCenter = contextualMenu.verticalCenter
    }
}
