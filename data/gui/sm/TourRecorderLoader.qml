import QtQuick 2.1

Rectangle {
    id: recorderLoader
    objectName: "recorderLoader"

    color: "transparent"
    radius: 5

    x: 150
    y: root.height - 500

    height: loader.height
    width: loader.width

    signal recorderLoaded()

    property bool load: false

    scale: load ? 1.0 : 0.0

    MouseArea{
        anchors.fill: recorderLoader
        drag.target: recorderLoader
        drag.axis: Drag.XandYAxis
        onWheel: wheel.accepted = true
        acceptedButtons: Qt.AllButtons
        drag.minimumX: 0
        drag.maximumX: root.width - width
        drag.minimumY: 0
        drag.maximumY: root.height - height
        enabled: load
    }

    Loader{
        id: loader
        source: load ? "TourRecorder.qml" : ""
        anchors.centerIn: parent
    }

    Behavior on scale {
        PropertyAnimation{
            duration: 200
        }
    }
}
