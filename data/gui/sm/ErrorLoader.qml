import QtQuick 2.1

Rectangle {
    id: errorLoader
    objectName: "errorLoader"
    visible:  load
    enabled: load

    color: "transparent"
    radius: 2

    height: loader.height /*+ 10*/
    width: loader.width /*+ 10*/

    property bool load: false

    signal connectOkCancel()

    scale: load ? 1.0 : 0.0

    Loader{
        id: loader
        source: load ? "ErrorWindow.qml" : ""
        anchors.centerIn: parent
    }

    Behavior on scale {
        PropertyAnimation{
            duration: 200
        }
    }
}
