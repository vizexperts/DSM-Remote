import QtQuick 2.1

Item{

    property int hours; property int minutes

    Component.onCompleted: update()

    function update(today){

        if(typeof(today)==='undefined')
            today = new Date();

        hours = today.getHours()
        minutes = today.getMinutes()
    }
}
