import QtQuick 2.1
import QtQuick.Layouts 1.0
import "VizComponents"

Rectangle{
    id: toolBoxId
    objectName:"toolBox"
    width: columnId.width
    height: root.height
    color: "#1a1a1a"

    enabled: !statusBar.applicationBusy


    property bool minimized: false
    x: minimized ? - width : 0

    Behavior on x {
        NumberAnimation{
            duration: 100
            easing.type: Easing.OutExpo
        }
    }

    //! Never turn this on.
    //! Tool tips wont be visible
    clip:false

    property VizButton currentSelectedObject:tempButtonId;
    property VizButton currentClickedObject : tempButtonId;

    property bool anyButtonClicked:false;

    signal openAttributeTablePopup()
    signal openContextualMenu()

    /*
        \signal rasterMove(int index, int positionOption)
        \brief Move raster object at index according to given option
        \param index raster to move
        \param positionOption where to move.
                    0 = Top
                    1 = Up
                    2 = Down
                    3 = Bottom

    */
    signal rasterMove(int positionOption)

    property string layerType;

    /*
        \function openATPopup()
        \brief Function to open openAttibuteTablePopup. Called from ElementListGUI::_openAttributeTablePopup()
    */
    function openATPopup(){
        smpFileMenu.load("OpenAttributeTablePopup.qml", root.width/2, root.height/2)
    }


    function closeReorderRasterPopup(){
        reorderRasterItemPopup.scale = 0;
        currentSelectedObject.checked = false;
    }

    function newRasterWindowButtonUncheck(){
        currentSelectedObject.checked = false;
    }

    signal disableAllButtons()

    function resetProperty(){
        if(currentSelectedObject.checked){
            if(smpFileMenu.isPopUpAlreadyOpen){
                smpFileMenu.unloadAll();
                smpFileMenu.isPopUpAlreadyOpen=false;
                currentSelectedObject.checked = false;
            }
        }
        else{
            //Nothing to do
        }

        disableAllButtons()

        currentSelectedObject=tempButtonId;
        currentClickedObject=tempButtonId;

        layerType = ""

        anyButtonClicked=false;

    }

    function handleExportLayerClicked(){
        if(!running_from_qml){
            if((layerType == "Point") || (layerType == "ModelFeatureObject")){
                FeatureExportGUI.setMode(10);
            }
            else{
                FeatureExportGUI.setMode(1);
            }
        }

        smpFileMenu.load("ExportWindow.qml")
    }

    function handleToExportWebserver(){
        if(rasterReorderId.enabled && !running_from_qml){
            FeatureExportGUI.setMode(3);
        }
        else if(!running_from_qml){
            FeatureExportGUI.setMode(1);
        }
        smpFileMenu.load("ExportGeoWindow.qml")
    }

    VizToolboxButton{
        id:tempButtonId
        visible: false;
    }

    Connections{
        target: root
        onNowRemoveHighLight:removeHighlight();
    }

    function removeHighlight(){
        currentSelectedObject.checked=false;
        anyButtonClicked=false;
    }

    function checkStateAndCallOptionClick(id){

        currentClickedObject=id;

        if(smpFileMenu.isPopUpAlreadyOpen && !anyButtonClicked ){
            currentClickedObject.checked=false;
        }
        else if((currentSelectedObject==currentClickedObject)&& smpFileMenu.isPopUpAlreadyOpen){
            currentSelectedObject.checked=true
            //currentClickedObject.checked=false;
            //RightMenu/leftMenu from Any Other Place Popup is already open so close before that
        }
        else if(smpFileMenu.isPopUpAlreadyOpen && anyButtonClicked){
            currentSelectedObject.checked=true //Dont Flip flop
            currentClickedObject.checked=false;
        }
        else if(currentSelectedObject!=currentClickedObject && currentSelectedObject.checked){
            currentClickedObject.checked=false;

            //flash button
        }
        else
        {
            rightMenu.minimized = true
            leftMenu.minimized=true
            timelineLoader.minimized = true
            currentSelectedObject = currentClickedObject
            anyButtonClicked=currentClickedObject.checked;
        }
    }

    onLayerTypeChanged: {
        if(layerType == "Line"){
            addPointId.iconSource = rootStyle.getIconPath("Features/addLine")
            addPointId.tooltip = "Add Lines To Selected Layer"
        }
        else if(layerType == "Polygon"){
            addPointId.iconSource = rootStyle.getIconPath("Features/addArea")
            addPointId.tooltip = "Add Area To Selected Layer"
        }
        else if(layerType == "ModelFeatureObject"){
            addPointId.iconSource = rootStyle.getIconPath("Add-3DModels-to-Layer")
            addPointId.tooltip = "Add Model To Selected Layer"
        }
        else{
            addPointId.iconSource = rootStyle.getIconPath("Features/addPoint")
            addPointId.tooltip = "Add PointsTo Selected Layer"
        }
    }


    ColumnLayout{
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 1
        id: columnId
        VizToolboxButton{
            id : addPointId
            objectName: "addFeatureToolboxButton"
            iconSource: rootStyle.getIconPath("Features/addPoint")
            tooltip: "Add Points To Selected Layer"
            visible: enabled
            onClicked:{
                if(!running_from_qml){
                    if(layerType === "Point"){
                        CreatePointGUI.addPointsToSelectedLayer(checked)
                        if(!checked){
                            CreatePointGUI._resetForDefaultHandling()
                        }
                    }
                    else if(layerType == "Line"){
                        CreateLineGUI.addLineToSelectedLayer(checked)
                        if(!checked){
                            CreateLineGUI._resetForDefaultHandling()
                        }
                    }
                    else if(layerType == "Polygon"){
                        CreatePolygonGUI.addPolygonToSelectedLayer(checked);
                        if(!checked){
                            CreatePolygonGUI._resetForDefaultHandling()
                        }
                    }
                    else if(layerType == "ModelFeatureObject"){
                        CreateLandmarkGUI.addModelsToSelectedLayer(checked)
                    }
                }
            }
        }
        VizToolboxButton{
            id : exportLayerId
            objectName: "exportLayerToolboxButton"
            iconSource: rootStyle.getIconPath("Features/export")
            tooltip: "Export Selected Layer"
            visible: enabled
            onClicked:{
                if(checked)
                    handleExportLayerClicked();
            }
        }

        VizToolboxButton{
            id : exportWebserverId
            objectName: "exportWebserverToolboxButton"
            iconSource: rootStyle.getIconPath("export to web")
            tooltip: "Export Selected Layer To GIS Server"
            visible: enabled && licenseManager.georbIS_Pro
            onClicked:{
                if(checked)
                    handleToExportWebserver();
            }
        }
        VizToolboxButton{
            id : queryselectedId
            objectName: "queryselectedToolboxButton"
            iconSource: rootStyle.getIconPath("list")
            tooltip: "Query Selected Data"
            visible: enabled
            onClicked: {
                if(checked)
                    smpFileMenu.load("QueryBuilder.qml")
            }
        }
        VizToolboxButton{
            id : tkNewRasterId
            objectName: "tkNewRasterToolboxButton"
            iconSource: rootStyle.getIconPath("tk/newRaster")
            tooltip: "Open In A New Window"
            visible: enabled && licenseManager.georbIS_Pro && rootStyle.showDemoFeatures
            onClicked:{
                //XXX special checked
                if(currentSelectedObject.checked){
                    RasterWindowGUI.rasterWindow();
                }
            }
        }
        VizToolboxButton{
            id : rasterReorderId
            objectName: "rasterReorderToolboxButton"
            iconSource: rootStyle.getIconPath("raster-reorder")
            tooltip: "Reorder Raster Item "
            visible: enabled
            onClicked:{
                if(checked){
                    reorderRasterItemPopup.scale = 1
                    reorderRasterItemPopup.x =toolBoxId.width
                   // reorderRasterItemPopup.y = (rasterReorderId.y + rasterReorderId.height/2) - reorderRasterItemPopup.height/2+reorderRasterItemPopup.height/10;
                    reorderRasterItemPopup.y = rasterReorderId.y +reorderRasterItemPopup.height/10;
                }
                else{
                    reorderRasterItemPopup.scale = 0
                }
            }
        }
        VizToolboxButton{
            id : opacitySliderId
            objectName: "opacitySliderToolboxButton"
            iconSource: rootStyle.getIconPath("tk/opacityslider")
            tooltip: "Opacity Slider"
            visible: enabled
            onClicked:{
                if(checked)
                    smpFileMenu.load("OpacitySliderTab.qml")
            }
        }
        VizToolboxButton{
            id : tkOverviewCreationId
            objectName: "tkOverviewCreationToolboxButton"
            iconSource: rootStyle.getIconPath("tk/overviewCreation")
            tooltip: "Generate Overviews For This Layer"
            visible: enabled && licenseManager.georbIS_Processor
            onClicked:{
                if(checked)
                    smpFileMenu.load("CreateOverviewTab.qml")
            }
        }
        VizToolboxButton{
            id : generalInfoId
            objectName: "generalInfoToolboxButton"
            iconSource: rootStyle.getIconPath("tk/info")
            tooltip: "General Information About File"
            visible: enabled
            onClicked:{
                if(checked)
                    smpFileMenu.load("InformationTab.qml")
            }
        }
        VizToolboxButton{
            id : createTmsId
            objectName: "createTmsToolboxButton"
            iconSource: rootStyle.getIconPath("tk/tms")
            tooltip: "Create TMS"
            visible: enabled && licenseManager.georbIS_Processor
            onClicked:{
                smpFileMenu.load("CreateTMS.qml")
            }
        }
        VizToolboxButton{
            id : tkPostgisId
            objectName: "tkPostgisToolboxButton"
            iconSource: rootStyle.getIconPath("tk/postgis")
            tooltip: "Export Selected Layer"
            visible: enabled && licenseManager.georbIS_Pro && rootStyle.showDemoFeatures
            onClicked:{
                if(checked)
                    smpFileMenu.load("PostGIS.qml")
            }
        }
        VizToolboxButton{
            id : routeCalculation
            objectName: "routeCalculationToolboxButton"
            iconSource: rootStyle.getIconPath("route-analysis")
            tooltip: "Route Calculation"
            visible: enabled && licenseManager.georbIS_Analyst
            onClicked:{
                if(checked)
                    smpFileMenu.load( "RoadNetwork.qml")
            }
        }
        VizToolboxButton{
            id : exportWebLayerId
            objectName: "exportWebLayerToolboxButton"
            iconSource: rootStyle.getIconPath("Features/export")
            tooltip: "Export Selected Layer To WebServer"
            visible: enabled && false
            onClicked:{
                smpFileMenu.load( "QueryBuilderGIS.qml")
            }
        }
        VizToolboxButton{
            id : previewTrackId
            objectName: "previewTrackToolboxButton"
            iconSource: rootStyle.getIconPath("preview")
            tooltip: "View Tracks"
            visible: enabled && licenseManager.georbIS_Footprint
            onClicked:{
                if(checked)
                    smpFileMenu.load("ShowGPSTracks.qml")
            }
        }
        VizToolboxButton{
            id : elementClassificationId
            objectName: "elementClassificationToolboxButton"
            iconSource: rootStyle.getIconPath("DGN-Classification")
            tooltip: "Dgn Classification"
            visible: enabled && licenseManager.georbIS_Strategem
            exclusiveButton: true
            onClicked:{
                if(checked)
                    smpFileMenu.load("ElementClassification.qml")
            }
        }
        VizToolboxButton{
            id : vectorStyleId
            objectName: "vectorStyleToolboxButton"
            iconSource: rootStyle.getIconPath("layer-style")
            tooltip: "Vector Style"
            visible: enabled
            onClicked:  {
                if(checked)
                    smpFileMenu.load(layerType + "LayerStylePopup.qml")
            }
        }
        VizToolboxButton{
            id : vectorAttributeTableId
            objectName: "vectorAttributeTableToolboxButton"
            iconSource: rootStyle.getIconPath("attribute-table")
            tooltip: "Open Vector Attribute Table"
            visible: enabled
            onClicked:{
                if(checked)
                    openAttributeTablePopup();
            }
        }
        VizToolboxButton{
            id : legendsId
            objectName: "legendsToolboxButton"
            iconSource: rootStyle.getIconPath("list")
            tooltip: "Show Legends"
            visible: enabled && licenseManager.georbIS_Intel
            onClicked:{
                if(checked)
                    smpFileMenu.load("HistogramLegendPopup.qml")
            }
        }
        VizToolboxButton{
            id: connectedFeaturesButton
            objectName: "connectedFeaturesToolboxButton"
            iconSource: rootStyle.getIconPath("3d-connected-features")
            tooltip: "Connected Features"
            visible: enabled && licenseManager.georbIS_Pro
            onClicked: {
                if(checked)
                    smpFileMenu.load("LineLayerStylePopup.qml")
            }
        }

        VizToolboxButton{
            id: converTO3dButton
            objectName: "convertTo3DToolboxButton"
            iconSource: rootStyle.getIconPath("2DTo3D")
            indicatorButton: true
            tooltip: "Convert to 3D"
            visible: enabled && licenseManager.georbIS_Strategem
            onClicked: {
                ElementClassificationGUI.setPrefer3D(checked)
            }
        }
        VizToolboxButton{
            id: flyaroundId
            objectName: "flyaroundToolboxButton"
            iconSource: rootStyle.getIconPath("flyaround")
            tooltip: "Fly around"
            visible: enabled
            onClicked: {
                if(checked)
                    smpFileMenu.load("FlyAroundPopup.qml")
            }
        }
        VizToolboxButton{
            id: styleTemplatesId
            objectName: "styleTemplatesToolboxButton"
            iconSource: rootStyle.getIconPath("style-editor")
            tooltip: "Style Template Editor"
            enabled: true
            visible: enabled
            exclusiveButton: true
            onClicked: {
                if(checked){
                    smpFileMenu.load("TemplateListPopup.qml")
                }
            }
        }

        VizToolboxButton{
            id: ditchProfileId
            objectName: "ditchProfileToolboxButton"
            iconSource: rootStyle.getIconPath("ditch_profile")
            tooltip: "Ditch Profile Editor"
            enabled: true
            visible: enabled
            exclusiveButton: true
            onClicked: {
                if(checked){
                    smpFileMenu.load("DitchProfile.qml")
                }
            }
        }

        VizToolboxButton{
            id: applyDitch
            objectName: "applyDitchToTerrainToolboxButton"
            iconSource: rootStyle.getIconPath("ditch")
            tooltip: "Apply dicth to terrain"
            visible: enabled
            onClicked: {
                if(checked)
                   smpFileMenu.load("Ditch.qml")
            }
        }

        VizToolboxButton{
            id: propertiessId
            objectName: "propertiesToolboxButton"
            iconSource: rootStyle.getIconPath("properties")
            tooltip: "Properties"
            enabled: true
            visible: enabled
            exclusiveButton: true
            onClicked: {
                if(checked){
                    ContextualTabGUI.openContextualMenuFromButton()
                }
            }
        }
        VizToolboxButton{
            id: colorLegendId
            objectName: "colorLegendToolboxButton"
            iconSource: rootStyle.getIconPath("legends")
            tooltip: "Color Legend"
            visible: enabled
            onClicked: {
                      smpFileMenu.load("ColorLegendPopUp.qml")
                    }
                }
    }
    Item {
        id: reorderRasterItemPopup
        objectName: "reorderRasterItemPopup"
        width: reorderRasterButtonRow.width
        height: reorderRasterButtonRow.height
        z:5;
        visible: true


        scale: 0

        Behavior on scale{
            NumberAnimation{
                duration: 100
            }
        }

        Rectangle{
            anchors.fill: parent
            gradient: rootStyle.gradientWhenDefault

        ColumnLayout{
            x: 1.5
            y:-5
            id: reorderRasterButtonRow
            height: rasterReorderButton.height*4
            scale: 0.8
            VizButton{
                id: rasterReorderButton
                iconSource: rootStyle.getIconPath("top")
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked:{rasterMove(0)}
            }
            VizButton{
                iconSource: rootStyle.getIconPath("up")
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked:{ rasterMove(1)}
            }
            VizButton{
                iconSource: rootStyle.getIconPath("down")
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked:{rasterMove(2)}
            }
            VizButton{
                iconSource: rootStyle.getIconPath("bottom")
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked:{rasterMove(3)}
            }
        }
        }
    }
}

