import QtQuick 2.1
import QtQuick.Layouts 1.0
import "VizComponents"

//! Quicklaunch bar containing Home button
Rectangle{
    id: quickLaunchBar

    //! TKP App launch signals
    //XXX: Connect these with appropriate slots in the application and hide them in default app mode.
    signal launchTerrainWizard()
    signal launchStereoWizard()
    signal launchStreamingWizard()


    // Point Attribute quickbar button enabled
    property bool pointAttributeActive : false

    // Point Hyperlink quickbar button enabled
    property bool pointHyperlinkActive : false

    color: "transparent"
    property string dateFormat: "ddd MMM dd/yyyy hh:mm:ss"

    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    /*Rectangle{
        width: parent.height
        height: parent.width
        color: rootStyle.colorWhenDefault
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#64000000"
            }

            GradientStop {
                position: 1
                color: "#00000000"
            }
        }
        transform: [
            Rotation{
                origin.x: 0
                origin.y: 0
                angle: -90
            },
            Translate{
                y: height
            }
        ]
    }*/

    width: parent.width //Width of parent window dependent on the Width of "latlong+MGRS" string.
    height: column1.height


    property string typeOfProject
    property string projectName

    onProjectNameChanged: {
        projectNameLabel.text = typeOfProject+" Name : " + projectName
    }

    ColumnLayout{
        id: column1
        anchors.left: parent.left
        anchors.leftMargin: 1
        y:10
        //spacing: -10

        VizLabel{
            id: curentTimeLabel
            fontSize: rootStyle.fontSize + 3
            visible: false
            text: Qt.formatDateTime(currentMissionDateTime, dateFormat)
        }
        VizLabel{
            fontSize: rootStyle.fontSize + 3
            id: projectNameLabel
            visible: false
        }
        VizLabel{
            id: blankLabel
            fontSize: rootStyle.fontSize + 5
            visible: false
            text: ""
        }

        RowLayout{
            id: column2
            anchors.left: parent.left
            //anchors.leftMargin: 10
            spacing: 0

            VizButton{
                id: homeButton
                iconSource: rootStyle.getIconPath("home")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Home"
                enabled: !statusBar.applicationBusy
                onClicked: {
                    toolBoxId.resetProperty()
                    smpFileMenu.unloadAll();
                    rightMenu.minimized = true
                    leftMenu.minimized=true
                    bottomLoader.unload()
                    timelineLoader.minimized = true
                    root.nowRemoveHighLight();
                    smpFileMenu.closeProject()
                }
            }
            /*VizButton{
                id: logoffButton
                iconSource: rootStyle.getIconPath("power")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Log Off"
                enabled: !statusBar.applicationBusy
                onClicked: {
                    toolBoxId.resetProperty()
                    smpFileMenu.unloadAll();
                    rightMenu.minimized = true
                    leftMenu.minimized=true
                    bottomLoader.unload()
                    timelineLoader.minimized = true
                    root.nowRemoveHighLight();
                    desktop.logoff()
                    loc.enableLoginText = false
                    loc.login(false)
                    //desktop.closeApplication()
                }
            }*/

            VizButton{
                id: undoButton
                iconSource: rootStyle.getIconPath("LeftMenu/undo")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Undo"
                visible: rootStyle.showDemoFeatures
                enabled: !statusBar.applicationBusy
                onClicked: {
                    rightMenu.minimized = true
                    smpFileMenu.undo()
                }
            }

            VizButton{
                id: redoButton
                iconSource: rootStyle.getIconPath("LeftMenu/redo")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Redo"
                visible: rootStyle.showDemoFeatures
                enabled: !statusBar.applicationBusy
                onClicked: {
                    rightMenu.minimized = true
                    smpFileMenu.redo()
                }
            }

            VizButton{
                id: saveButton
                iconSource: rootStyle.getIconPath("save")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Save"
                enabled: !statusBar.applicationBusy
                onClicked: {
                    //rightMenu.minimized = true
                    smpFileMenu.save()
                }
            }

            VizButton{
                id: saveAsButton
                iconSource: rootStyle.getIconPath("saveAs")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Save As"
                enabled: !statusBar.applicationBusy
                onClicked: {
                    if(smpFileMenu.saveASFunction())
                    {
                        rightMenu.minimized = true
                    }
                }
            }

            VizButton{
                id: screenshotButton
                iconSource: rootStyle.getIconPath("LeftMenu/screenShot")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "ScreenShot"
                enabled: !statusBar.applicationBusy
                onClicked: {

                    if(smpFileMenu.load("Screenshot.qml"))
                    {
                        rightMenu.minimized = true;
                    }
                }
            }

            VizButton{
                id: sideBarButton
                iconSource: rootStyle.getIconPath("Menu/elementList")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Element List"
                onClicked: {
                    rightMenu.minimized = true
                    elementList.minimized = !elementList.minimized
                }
            }

            VizToggleButton{
                id: pointAttributeButton
                iconSource: rootStyle.getIconPath("AttribInfo")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Point Attribute"
                onClicked: {
                    rightMenu.minimized = true
                    pointAttributeActive = !pointAttributeActive

                    if(pointHyperlinkActive == true){
                    pointHyperlinkButton.on = false
                    pointHyperlinkActive = false
                    }
                }
                enabled: !statusBar.applicationBusy
            }

            VizToggleButton{
                id: pointHyperlinkButton
                iconSource: rootStyle.getIconPath("hyperlink")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "Hyperlink Attribute"
                visible: rootStyle.showDemoFeatures
                onClicked: {
                    rightMenu.minimized = true
                    pointHyperlinkActive = !pointHyperlinkActive

                    if(pointAttributeActive == true){
                        pointAttributeButton.on = false
                        pointAttributeActive = false
                    }
                }
                enabled: !statusBar.applicationBusy
            }
            VizToggleButton{
                id: toolBarToggleButton
                iconSource: rootStyle.getIconPath("toolBox")
                scale: 0.9
                anchors.bottom: parent.bottom
                tooltip: "ToolBar"
                onClicked: {
                    toolBoxId.minimized=!toolBoxId.minimized
                }
                enabled: !statusBar.applicationBusy
            }
            VizToggleButton{
                id:userManual
                iconSource: rootStyle.getIconPath("help")
                scale:0.9
                anchors.bottom: parent.bottom
                tooltip: "UserManual"
                onClicked:
                {
                    smpFileMenu.load("UserManual.qml");
                    smpFileMenu.unloadAll();
                }
            }
        }
    }
    RowLayout{
        id: column3
        anchors.right: parent.right
        anchors.rightMargin: 10
        y:10
        spacing: 0
        VizButton{
            id: logoffButton
            iconSource: rootStyle.getIconPath("power")
            scale: 0.9
            visible: false
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            tooltip: "Log Off"
            enabled: !statusBar.applicationBusy
            onClicked: {
                toolBoxId.resetProperty()
                smpFileMenu.unloadAll();
                rightMenu.minimized = true
                leftMenu.minimized=true
                bottomLoader.unload()
                timelineLoader.minimized = true
                root.nowRemoveHighLight();
                desktop.logoff()
                loc.enableLoginText = false
                loc.login(false)
                //desktop.closeApplication()
            }
        }
    }

    VizButton{
        id: minimizeQuickLaunchBar
        iconSource: minimized ? rootStyle.getIconPath("down") :  rootStyle.getIconPath("up")
        scale: 0.7
        visible: false
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onClicked: minimized = !minimized
    }
    property bool minimized: false

    Behavior on y{
        NumberAnimation{duration: 300}
    }

    states: [
        State {
            name: "Minimized"
            when: minimized
            PropertyChanges {
                target: quickLaunchBar
                y: -85
            }
        }
    ]
}
