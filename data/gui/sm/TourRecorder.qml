import QtQuick 2.1
import "VizComponents"

Rectangle {
    id: recorder
    objectName: "tourRecorder"
    width: column.width + closeButton.width + 20
    height: column.height
    radius: 5
    color: "transparent"

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        border.color: rootStyle.borderColor
        border.width: rootStyle.borderWidth
//        opacity: 0.7
        radius: 5
    }

    property int recorderType: 0

    Component.onCompleted: recorderLoader.recorderLoaded()

    Behavior on width {
        NumberAnimation{
            duration: 100
        }
    }

    Behavior on height {
        NumberAnimation{
            duration: 100
        }
    }

    signal startRecording()
    signal pauseRecording()
    signal stopRecording()
    signal closed()
    signal saveTour()
    signal markKeyframe()

    onSaveTour: {
        if(running_from_qml)
            showSaver(false)
    }

    function showSaver(setVisible)
    {
        if(setVisible)
        {
            nameLineEdit.width  = 150
            saveButton.visible = true
        }
        else
        {
            nameLineEdit.width  = 0
            nameLineEdit.setFocus = false
            saveButton.visible = false
        }
    }

    property alias tourName: nameLineEdit.text

    onStopRecording: {
        if(running_from_qml)
            showSaver(true)
    }

    property bool recording: false

    Row{
        id: column
        anchors.verticalCenter : parent.verticalCenter
        height: (recorderType == 1) ? 70 : 50
        width: childrenRect.width
        property real iconScale: 0.9
        property bool backgroundVisible: false
        VizButton{
            id: recordButton
            anchors.verticalCenter: parent.verticalCenter
            iconSource: recording ? rootStyle.getIconPath("time/pause")
                                : rootStyle.getIconPath("time/record")
            backGroundVisible: parent.backgroundVisible
            scale: parent.iconScale
            onClicked: {
                if(recording)
                    pauseRecording()
                else
                    startRecording()
            }
        }
        VizButton{
            id: stopRecordingButton
            anchors.verticalCenter: parent.verticalCenter
            iconSource: rootStyle.getIconPath("time/recording")
            backGroundVisible: parent.backgroundVisible
            scale: parent.iconScale
            onClicked: stopRecording()
        }
        VizButton{
            id: keyframeButton
            anchors.verticalCenter: parent.verticalCenter
            iconSource: rootStyle.getIconPath("add")
            backGroundVisible: parent.backgroundVisible
            scale: parent.iconScale
            onClicked: markKeyframe()
            visible: ((recorderType == 1) && (!saveButton.visible))
        }
        Image {
            source: rootStyle.getIconPath("left")
            width: 25
            height: 25
            anchors.verticalCenter: parent.verticalCenter
            visible: keyframeButton.visible
        }
        VizLabel{
            id: keyframeTip
            textWidth: 130
            anchors.verticalCenter: parent.verticalCenter
            text: "Press 'X' or click here to mark keyframe."
            wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
            fontSize: 12
            visible: keyframeButton.visible
        }
        VizTextField{
            id: nameLineEdit
            anchors.verticalCenter: parent.verticalCenter
            placeholderText: "Enter tour tame..."
            width: 0
            maxLength: 64
            validator: rootStyle.nameValidator
            Behavior on width {
                NumberAnimation{
                    duration: 100
                }
            }
        }
        VizButton{
            id: saveButton
            anchors.verticalCenter: parent.verticalCenter
            iconSource: rootStyle.getIconPath("saveAs")
            backGroundVisible: parent.backgroundVisible
            scale: parent.iconScale
            onClicked:
            {
                saveTour()
                nameLineEdit.text = ""
            }
            visible: false
        }
    }

    property date elapsedTime

//    TextEdit{
//        id: elapsedTimeLabel
//        text: Qt.formatTime(elapsedTime, "hh:mm:ss")
//        readOnly: true
//        anchors.left: stopRecordingButton.right
//        anchors.leftMargin: 10
//        anchors.bottom: stopRecordingButton.bottom
//    }

    VizButton{
        id: closeButton
        anchors.top: parent.top
        anchors.right: parent.right
        iconSource: rootStyle.getIconPath("close")
        onClicked: closed()
        backGroundVisible: false
    }
}
