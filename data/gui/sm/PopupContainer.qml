import QtQuick 2.1
import "VizComponents"

Item{

    id: popupContainer

    objectName: "popupContainer"

    Component.onCompleted: {
        loader.source = "Popup/" + _fileName
        if(_fileName !== "")
            open.start()
    }

    function setTitle(title){
        label.text = title
        titleRectangle.visible = true
    }

    signal close()

    onClose: {
        if(smpFileMenu.isParentChild && (_fileName!=="VectorQueryBuilder.qml") && (_fileName!=="QueryBuilderGIS.qml") && (_fileName!=="CreateColorMap.qml")
                 && (_fileName!=="AddOrRenameBookmarksPopup.qml"))
        {
            //trying to close parent window :-not allow to close parent window without close child window)
            return;
        }


        unload()
    }

    function unload()
    {
        smpFileMenu.resizeMouseArea.visible =false
        closeAnimation.start()        
    }

    height: loader.height + titleRectangle.height + 10
    width: loader.width + 10

    Rectangle{
        anchors.fill: parent
        color: rootStyle.colorWhenDefault
        opacity: 0.9
        radius: 2
        border.color: rootStyle.colorWhenSelected
        border.width: rootStyle.borderWidth
    }

    MouseArea{
        anchors.fill: backgroundMouseArea
        hoverEnabled: true
        enabled: backgroundMouseArea.enabled
        visible: enabled
    }

    MouseArea{
        id: backgroundMouseArea
        anchors.top: titleRectangle.top
        onWheel: wheel.accepted = true
        acceptedButtons: Qt.AllButtons
        enabled: popupContainer.scale > 0.0
        visible: enabled
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
    }


    Rectangle{
        id: titleRectangle
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        border.width: rootStyle.borderWidth
        border.color: rootStyle.colorWhenSelected

        width: parent.width
        height: label.height
        color: rootStyle.colorWhenSelected
        radius: 1
        VizLabel{
            id: label
            width: parent.width
            textColor: rootStyle.colorWhenDefault
            text: ""
        }
        MouseArea{
            id: _titleBarMouseArea
            anchors.fill: parent
            states: State{
                name: "entered"
                when: _titleBarMouseArea.pressed
                PropertyChanges{target: root; cursorShape: Qt.SizeAllCursor}
            }
            enabled: popupContainer.scale > 0.0
            visible: enabled
            drag.target: popupContainer
            drag.axis: Drag.XAndYAxis
            drag.minimumX: 0
            drag.maximumX: root.width - width
            drag.minimumY: titleRectangle.height
            drag.maximumY:root.height - popupContainer.height
        }
        VizButton{
            anchors.right: parent.right
            iconSource:  rootStyle.getIconPath("close")
            height: label.height
            width: height
            opacity: 0.7
            backGroundVisible: false
            onClicked: {
                close()
                keyboard.close()
            }
        }
    }

    property string _fileName : ""

    Loader{
        id: loader
        source: ""
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: titleRectangle.bottom
    }

    SequentialAnimation{
        id: closeAnimation
        PropertyAnimation{target: popupContainer; property: "visible"; duration: 300; to: false}
        PropertyAction{target: popupContainer; property: "_fileName"; value: ""}
        PropertyAction{target: loader; property: "source"; value: ""}
        PropertyAction{target: titleRectangle; property: "visible"; value: false}
        PropertyAction{target: label; property: "text"; value: ""}
    }

    SequentialAnimation{
        id: open
        PropertyAnimation{target: popupContainer; property: "visible"; duration: 300;from: false; to: true}
    }

    property int minimumWidth
    property int minimumHeight

    function setMinWidthHeight(){
//        console.log("here")
        height = minimumHeight
        width = minimumWidth
    }

    function setInitialMinWidthHeight(minWidth,minHeight){
        minimumHeight =minHeight
        minimumWidth =minWidth
    }

    onMinimumHeightChanged: setMinWidthHeight()
    onMinimumWidthChanged: setMinWidthHeight()
}
