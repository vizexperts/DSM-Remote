import QtQuick 2.1
import "VizComponents"

Item{
    id: fileManager

    property alias filesList: fileView.model
    property alias recentFilesList: recentFilesGroup.model
    property alias searchResultsModel: searchResults.model

    clip: true

    GridView{
        id: searchResults
        cellWidth: 190*rootStyle.uiScale
        cellHeight: 135*rootStyle.uiScale
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        clip: true
        visible: false
        anchors.leftMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 5
        flow: GridView.LeftToRight
        boundsBehavior: Flickable.StopAtBounds
        delegate: FileIcon{}
        VizScrollBar{
            target: searchResults
        }
    }

    states: [
        State {
            name: "SEARCHING"
            PropertyChanges { target: searchResults; visible: true}
            PropertyChanges { target: recentFilesGroup; visible: false}
            PropertyChanges { target: line; visible: false }
            PropertyChanges { target: fileView; visible: false }
        }
    ]

    FileGroup{
        id: recentFilesGroup
        anchors.top: parent.top
        anchors.topMargin: 10
        label: "Recent Files"
        state: "OPENED"
    }

    Rectangle{
        id: line
        anchors.top: recentFilesGroup.bottom
        anchors.topMargin: 20
        width: parent.width
        height: 3
        color: "white"
    }

    function resetGroupStates(){
        recentFilesGroup.state = ""
        for(var i = 0; i < fileView.count; i++){
            fileView.currentIndex = i
            if(fileView.currentItem)
                fileView.currentItem.state = ""
        }
    }

    ListView{
        anchors.top: line.bottom
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        id: fileView
        clip: true
        width: parent.width + 20
        height: parent.height
        boundsBehavior: Flickable.StopAtBounds
        spacing: 10
        delegate: FileGroup{
            label: groupName
            model: groupContents
            width: parent.width - 20
        }
        VizScrollBar{
            target: fileView
        }
    }
}
