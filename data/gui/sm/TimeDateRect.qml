import QtQuick 2.1
import "VizComponents"

DatePicker{
    id: dateRect

    objectName: "timeDatePicker"

    function setSelectedDate(selectedDate){
        var t
        if(target){
            t = target
        }
        else{
            t = previousTarget
        }
        if((t.objectName === "startTimeTextEdit") || (t.objectName === "startTimeDelegate")){
            timelineLoader.timelineObject.setStartDateTimeInternal(selectedDate)
        }
        else if((t.objectName === "endTimeTextEdit") || (t.objectName === "endTimeDelegate")){
            timelineLoader.timelineObject.setEndDateTimeInternal(selectedDate)
        }
    }
        
    //opacity: 0
    property int offset: root.width/2
    Behavior on offset {
        NumberAnimation{
            duration: 200
        }
    }/*
    Behavior on visible{
        NumberAnimation{
            duration: 200
        }
    }*/
    
    property Item target
    
    // kept so as to assign property changes after timeDateCmponent has been destroyed
    property Item previousTarget
    
    // checks weather the passed object is a textEdit or not
    function isTextEdit(object){
        if(object){
            if(typeof object.font != "undefined"){
                return true
            }
        }
        return false
    }
    
    
    // loads the timeDate dialog at specified position on timeline
    // if already open the shifts to the given position
    // targetString is the target textEdit that should display the selected Date/Time
    function loadAt(position, newTarget){
        var d
        if(target === newTarget){
            //set the target value to the selected value
            offset = root.width/2
            //opacity = 0
            if(isTextEdit(target)){
                target.font.bold = false
                d = new Date(year, month, dayOfMonth, hours, minutes, seconds)
                setSelectedDate(d)
            }
            previousTarget = target
            //visible= false
            dateRect.visible =false
            //console.log("position : "+position + " target : "+ newTarget +" date Visible un load : " +dateRect.visible)
            target = null
        }
        else{
            // open the dialog
            offset = position
            previousTarget = target

            //opacity = 1
            dateRect.visible =true
            //console.log("position : "+position + " target : "+ newTarget +" date Visible  load: " +dateRect.visible)
            
            target = newTarget
            
            if(isTextEdit(previousTarget))
                previousTarget.font.bold = false
            
            if(isTextEdit(target)){
                target.font.bold = true
                if(target.objectName == "startTimeDelegate"){
                    d = timelineLoader.timelineObject.startDate
                }
                else if(target.objectName == "endTimeDelegate"){
                    d = timelineLoader.timelineObject.endDate
                }
                else{
                    d = new Date()
                }
                setDateTime(d)
            }
        }
    }
    anchors.horizontalCenterOffset: offset
}
