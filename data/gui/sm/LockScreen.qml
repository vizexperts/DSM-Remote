import QtQuick 2.1
import QtQuick.Layouts 1.0
import "VizComponents"

/**
 *  Login Screen.
 */

Item{

    id: lockScreen
    anchors.left: parent.left
    objectName: "loginScreen"

    width: root.width
    height: root.height

    property string background : rootStyle.getIconPath("loginScreen")

    property bool locked: true

    property bool enableLoginText: true

    property alias status : loggingInLabel.text

    property bool isLocal: true

    property bool localChecked: true

    property string ipAddress: ""

    signal check(string userName, string password)

    onCheck: {
        userNameTextEdit.setFocus = false
        passwordTextEdit.setFocus = false
        ip.setFocus = false

        if(running_from_qml){
            if((userName == "") && password == ""){
                login(true)
                user_type = usertype_INSTRUCTOR
            }
            else if((userName == "creator")&& password == ""){
                login(true)
                user_type = usertype_MODELCREATOR
            }
            else if((userName == "syndicate") && password == ""){
                login(true)
                user_type = userType_SYNDICATE
            }
        }
        else{
            loggingInLabel.text = qsTr("Logging in ..")
            loggingInLabel.visible = true
            LoginGUI.checkPassword(userName, password)
        }
    }

    function login(result){
        if(result){
            desktop.loadProject("", 3, application_mode)
            if(running_from_qml){
                locked = false
            }
            visible=true
            keyboard.close()
            userNameTextEdit.focus = false
            passwordTextEdit.focus = false
            passwordTextEdit.enabled = false
            userNameTextEdit.enabled = false
        }
        else{
            locked  = true
            if(enableLoginText)
            {

                loggingInLabel.text = qsTr(status)
                loggingInLabel.visible = true
            }
            else
            {
                loggingInLabel.visible = false
            }
            userNameTextEdit.focus = true
            passwordTextEdit.focus = true
            passwordTextEdit.enabled = true
            userNameTextEdit.enabled = true
         }
      }

    y : locked ? 0 : -root.height*1.5

    Behavior on y {NumberAnimation{duration: 800; easing.type: Easing.OutCurve}}

    Image{
        anchors.fill: parent
        source: background
    }


    Rectangle {
        id: rectangle1
        anchors.fill: mainLayout
        anchors.margins: -20
        color: "#64000000"
    }

    ColumnLayout{
        id: mainLayout
        anchors.centerIn: parent
        RowLayout{
            anchors.left: parent.left
            VizLabel {
                id: userNameLabel
                text: qsTr("Username:")
                fontSize: rootStyle.fontSize
            }

            VizTextField{
                id: userNameTextEdit
                Layout.minimumWidth: root.width*(rootStyle.fontSize/14)*0.1
                placeholderText: "username..."
                Keys.onTabPressed: {
                    passwordTextEdit.setFocus = true
                }
            }
        }
        RowLayout{
            anchors.left: parent.left
            VizLabel {
                id: passwordLabel
                text: qsTr(" Password:")
                fontSize: rootStyle.fontSize
            }

            VizTextField {
                anchors.right: parent.right
                id: passwordTextEdit
                placeholderText: "password..."
                Layout.minimumWidth: root.width*(rootStyle.fontSize/14)*0.1
                echoMode: TextInput.Password
                onAccepted: {
                    focus = false
                    check(userNameTextEdit.text, text)
                    userNameTextEdit.text = ""
                    text = ""
                }
                }
            }
		
        RowLayout{
            VizLabel{
                anchors.left: parent.left
                id:checkLabel
                text: qsTr(" Use Local :")
            }

            VizCheckBox{
                anchors.right: parent.right
                id:localCheck
                checked: isLocal
                onCheckedChanged: {
                    isLocal=checked;
                    LoginGUI.checkLocalProject(checked);
                }
            }

        }
        RowLayout{
            VizLabel{
                anchors.left: parent.left
                id:saveLabel
                text: qsTr(" Save IP :")
                visible:!(isLocal)
            }
            VizButton{
                anchors.right: parent.right
                id:saveButton
                visible :!(isLocal)
                text: "Save"
                onClicked: {
                    LoginGUI.saveIPAddress(ip.text);
                }
            }
            VizTextField {
                anchors.right: saveButton.left
                id: ip
                visible:!(isLocal)
                text:ipAddress
                placeholderText: "IP"
                Layout.minimumWidth: root.width*(rootStyle.fontSize/14)*0.1

            }

        }

        RowLayout{
            id: actionButtons
            anchors.right: parent.right
            spacing: 20

            VizButton{
                id: loginButton
                text: "Login"
                onClicked: {
                    userNameTextEdit.focus = false
                    passwordTextEdit.focus = false
                    passwordTextEdit.enabled = false
                    userNameTextEdit.enabled = false
                    enableLoginText = true
                    loggingInLabel.visible = true
                    check(userNameTextEdit.text, passwordTextEdit.text)
                }
            }

            /*VizButton{
                id: cancelButton
                text: "Cancel"
                onClicked: {
                    desktop.closeApplication()
                }
            }*/
        }
        RowLayout{
            anchors.left: parent.left
            VizLabel {
                id: loggingInLabel
                text: qsTr("Logging in..")
                fontSize: rootStyle.fontSize
                visible: false
            }
        }
    }

    Image {
        id: vizLogo
        source: rootStyle.getIconPath("logo")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        anchors.right: parent.right
        anchors.rightMargin: 50
        height: 30
        width: 200
    }

    /*Clock{
        id: clock
        anchors.left: parent.left
        //anchors.top: rectangle1.top
        anchors.leftMargin: 50
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
    }*/

    VizLabel {
        id: productLabel
        text: "GeorbIS"
        fontSize: rootStyle.fontSize * 3
        anchors.top: parent.top
        anchors.left: parent.left
        height: 100
    }

//        property Component spriteComponent

//        Component.onCompleted: {
//            spriteComponent = Qt.createComponent("Sprite.qml")
//        }

//        MouseArea{
//            anchors.fill: canvas
//            onPositionChanged: {
//                if(pressed){
//                    spriteComponent.createObject(canvas, {"x":mouseX, "y":mouseY, "color":canvas.spriteColor})
//                }
//            }
//        }
//        Rectangle{
//            id: canvas
//            property string spriteColor: "black"
//            anchors.fill: parent
//        }

//        Row{
//            VizButton{
//                text: "Clear"
//                onClicked: {
//                    for(var i = 0; i < canvas.children.length; ++i){
//                        canvas.children[i].destroy(100)
//                    }
//                }
//            }
//            VizButton{
//                property string colorName: "black"
//                text: colorName
//                checked: canvas.spriteColor == colorName
//                onClicked: canvas.spriteColor = colorName
//            }
//            VizButton{
//                property string colorName: "red"
//                text: colorName
//                checked: canvas.spriteColor == colorName
//                onClicked: canvas.spriteColor = colorName
//            }
//            VizButton{
//                property string colorName: "green"
//                text: colorName
//                checked: canvas.spriteColor == colorName
//                onClicked: canvas.spriteColor = colorName
//            }
//            VizButton{
//                property string colorName: "grey"
//                text: colorName
//                checked: canvas.spriteColor == colorName
//                onClicked: canvas.spriteColor = colorName
//            }
//            VizButton{
//                property string colorName: "blue"
//                text: colorName
//                checked: canvas.spriteColor == colorName
//                onClicked: canvas.spriteColor = colorName
//            }
//            VizButton{
//                property string colorName: "gold"
//                text: colorName
//                checked: canvas.spriteColor == colorName
//                onClicked: canvas.spriteColor = colorName
//            }
//        }
}
