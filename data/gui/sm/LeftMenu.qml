import QtQuick 2.1
import "VizComponents"

Item {
    id: menuHolder
    objectName: "smpLeftMenu"
    width: menuLoader.width
    height: menuLoader.height
    //anchors.left: elementList.right
    anchors.left: elementList.minimized ? toolBoxId.right : elementList.right

    property int leftMargin: minimized ? -width : 0

    anchors.leftMargin: leftMargin

    Behavior on leftMargin {
        NumberAnimation{
            duration: 500
            easing.type: Easing.OutExpo
        }
    }

    visible: !minimized
    enabled: !minimized

    x:minimized ? - width : 0

    signal loaded(string type)

    property bool isRedoAvailable: false

    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    Connections{
        target: root
        onApplication_modeChanged: {
            minimized = true
            menuHolder.setSource("")
        }
    }

    signal connectFovMenu()
    signal connectCacheSettingsMenu()
    signal connectStereoMenu()
    signal connectWeatherMenu()
    signal connectProjectSettingsMenu()
    signal browseForTextToSpeech()
    signal connectLandmarkMenu()

    Behavior on width {
        NumberAnimation{
            duration: 500
            easing.type: Easing.OutExpo
        }
    }

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        opacity: 0.7
    }

    property bool minimized: true

    onMinimizedChanged: {
        if(!minimized){
            loadMainMenu()
        }
    }

    function loadMainMenu(){
        timelineLoader.minimized = true
        if(application_mode == application_mode_NONE){
            setSource("")
        }
        else{
            setSource("ProLeftPanel.qml")
        }
    }

    //! Ex. Filename = "ProLeftPanel.qml"
    function setSource(fileName){
        menuLoader.source = "SideMenu/" + fileName
    }

    Loader{
        id: menuLoader
        source: ""
    }
}
