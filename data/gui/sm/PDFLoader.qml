import QtQuick 2.1

Item{

    id: pdfLoader
//    width: root.width
//    height: root.height

    objectName: "mainPDFLoader"

    signal saveFile(string document)

    property Component pdfViewerComponent

    Component.onCompleted: {
        //console.log(pdfViewerComponent.status + "" + errorString() )

        //pdfViewerComponent = Qt.createComponent("PDFViewer.qml")
    }

    clip: false

    function loadPDF(paramTitle, paramPath, isNewFile){
        if(pdfViewerComponent)
            pdfViewerComponent.destroy()
        pdfViewerComponent = Qt.createComponent("PDFViewer.qml")
        if( pdfViewerComponent.status != Component.Ready )
        {
            if( pdfViewerComponent.status == Component.Error )
                console.debug("Error:"+ pdfViewerComponent.errorString() );
            return; // or maybe throw
        }
        pdfViewerComponent.createObject(stack, {"x": 50, "y": 50, "title": paramTitle, "path":paramPath, "saveButtonVisible":isNewFile})
    }

    function closeAll(){
        for(var i = 0; i < stack.children.length; ++i){
            stack.children[i].destroy()
        }
    }

    Item {
        id: stack
    }

}

