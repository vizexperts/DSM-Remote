import QtQuick 2.1
import "VizComponents"

/**
 * Desktop. Contains LockScreen and FileManagers.
 */

FocusScope{
    id: desktop
    objectName: "desktop"

    /*
        \property bool minimized
        \brief Minimized state.
    */
    property bool minimized: true

    //! Used when ndesktop was dropping down from above, occupyinh the whole window
    //    y: minimized ? -root.height*1.1 : 0

    scale: minimized ? 0 : 1

    visible : !minimized

    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    /*
        \transition
        \brief for smoothening falling down from above
    */
    Behavior on y {NumberAnimation{duration: 800; easing.type: Easing.OutCurve}}

    /*
        \transition
        \brief for smoothening scaling
    */
    Behavior on scale {NumberAnimation{duration: 400; easing.type: Easing.OutCurve}}

    width: root.width*0.75
    height: root.height*0.75

    anchors.centerIn: parent

    //Types of the tabs Present
    readonly property int tabType_SMP: 0

    //For setting the Visibility of the tabs
    property bool tabType_SMP_Visbile: true

    //For setting the current Tab of the TabFrame
    property alias currentTabIndex: tabFrame.current

    /*
      \signal loadFiles
      \brief Load list of files/projects available to load and populate in the desktop project list model.
      \sa ProjectManagerGUI.loadFiles()
    */
    signal loadFiles()

    signal loadProject(string name, int type)
    signal deleteProject(string name, int type)
    signal createNewProject(string name)
    signal closeApplication()
    signal logoff()
    signal searchProjects(string name, int type)
    signal activeTab(int tabIndex)

    signal fileLoaded()

    onMinimizedChanged: {
        if(minimized){
            fileLoaded()
        }
        else{
            // load project list
            loadFiles();
        }
    }

    //! Background wallpaper
    Image {
        id: wallpaper
        anchors.fill: parent
        source: rootStyle.getIconPath("desktop")
        opacity: 0.7
    }

    //! Close Button
    VizButton{
        anchors.right: parent.right
        iconSource: rootStyle.getIconPath("close")
        backGroundVisible: false
        opacity: 0.7
        onClicked: minimized = true
    }

    VizTabFrame{
        id: tabFrame
        anchors.fill: parent
        tabHeight: root.height*0.05
        tabWidth: tabHeight*4
        anchors.margins: root.height*0.05

        property int currentTabType:tabType_SMP

        onCurrentChanged:{
            switch(tabs[current].title){
            case "Plans":
                currentTabType = tabType_SMP
                break
            }
            activeTab(currentTabType)
        }

        VizTab{
            title: "Projects"
            visible: tabType_SMP_Visbile
			height: tabFrame.height
            width: tabFrame.width

            Rectangle{
                id: planOptionRect

                enabled: tabFrame.currentTabType == tabType_SMP
                width: parent.width - 30
				//To Ensure width of the Rectangle is always associated with parent's width
                onWidthChanged: {
                    if (width >= 0)
                        width = parent.width - 30
                }
                anchors.top: parent.top
                anchors.topMargin: 15
                height: Math.max(100, rootStyle.buttonIconSize*2)
                color: "transparent"
                border.color: "white"
                border.width: 2
                anchors.horizontalCenter: parent.horizontalCenter
                VizButton{
                    id: createNewPlanButton
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    anchors.verticalCenter: parent.verticalCenter
                    iconSource: rootStyle.getIconPath("add")
                    onClicked: {
                        parent.state = (parent.state == "NEW") ? "" : "NEW"
                        planFilesManager.state = ""
                    }
                }

                VizButton{
                    id: planSearchButton
                    anchors.left: createNewPlanButton.left
                    anchors.leftMargin: 60
                    anchors.verticalCenter: parent.verticalCenter
                    iconSource: rootStyle.getIconPath("search")
                    onClicked: {
                        parent.state = (parent.state == "SEARCH") ? "" : "SEARCH"
                        planFilesManager.state = (planFilesManager.state == "SEARCHING") ? "" : "SEARCHING"
                        if(running_from_qml){
                            application_mode = application_mode_PLAN
                            desktop.minimized = true
                        }
                    }
                }
                states: [
                    State{
                        name: "NEW"
                        PropertyChanges{target: planTextField; width: root.width*0.2}
                        PropertyChanges{target: okButton; scale: 0.8}
                        PropertyChanges{target: createNewPlanButton; checked: true}
                        PropertyChanges{target: planTextField; placeholderText: "Create New Project..."}
                    },
                    State {
                        name: "SEARCH"
                        PropertyChanges{target: planTextField; width: root.width*0.2}
                        PropertyChanges{target: planSearchButton; checked: true}
                        PropertyChanges{target: planTextField; placeholderText: "Search Projects..."}
                    }
                ]
                VizTextField{
                    id: planTextField
                    Behavior on width {
                        NumberAnimation{duration: 300}
                    }
                    anchors.left: planSearchButton.right
                    placeholderText: ""
                    anchors.leftMargin: 15
                    anchors.verticalCenter: createNewPlanButton.verticalCenter
                    width: 0
                    Keys.onEscapePressed: {
                        setFocus = false
                        text = ""
                    }
                    onTextChanged: {
                        if(parent.state == "SEARCH"){
                            searchProjects(text, tabType_SMP)
                        }
                    }
                }
                VizButton{
                    Behavior on scale {
                        NumberAnimation{duration: 100}
                    }

                    id: okButton
                    iconSource: rootStyle.getIconPath("accept")
                    scale: 0
                    anchors.left: planTextField.right
                    anchors.verticalCenter: planTextField.verticalCenter
                    anchors.leftMargin: 10
                    onClicked: {
                        createNewProject(planTextField.text)
                        planTextField.setFocus = false
                        planTextField.text = ""
                        parent.state = ""
                    }
                }
            }
            FileManager{
                id: planFilesManager
                objectName: "planFileManager"
                width: parent.width - 30
				//To Ensure width of the FileManager is always associated with parent's width
                onWidthChanged: {
                    if (width >= 0)
                        width = parent.width - 30
                }
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: planOptionRect.bottom
                anchors.bottom: parent.bottom

                filesList: (typeof(planFiles) === "undefined")? "undefined" : planFiles
                recentFilesList: (typeof(recentPlanFiles) === "undefined") ? "undefined" : recentPlanFiles
                searchResultsModel: (typeof(plannerSearcheResult) === "undefined") ? "undefined" : plannerSearcheResult
            }
        }
    }
}



