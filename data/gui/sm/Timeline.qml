import QtQuick 2.1
import "VizComponents"

Item {
    id: timeline
    width: timelineLoader.width
    height: 150*rootStyle.uiScale
    objectName: "timeline"

    //! Properties
    // date/time format used
    property string dateTimeFormat: "ddd MMM dd/yyyy \n hh:mm:ss"
    property string dateFormat: "ddd MMM dd/yyyy"
    property string timeFormat: "hh:mm:ss"

    // StartDateTime of mission
    property date startDate

    // endDateTime of mission
    property date endDate

    property bool animationRunning: false

    //! signal to set Start/End date time
    signal setStartEndDateTime(date startDate, date endDate)

    signal slower()
    signal play()
    signal pause()
    signal stop()
    signal faster()

    onPlay: {
        if(running_from_qml){
            timeSlider.profileType = (timeSlider.profileType == timeSlider.profileType_Movement)
                    ? timeSlider.profileType_None: timeSlider.profileType_Movement
        }
    }

    signal timeSliderValueChanged(int value)

    //! function to check weather startDate is greater than or equal to the endDate
    function checkValidity(sd, ed){
        if(sd && ed){
            if(sd >= ed){
                // XXX: show error
                return false
            }
        }
        return true
    }

    function setStartDateTimeInternal(sd){

        // check validity of new start date
        if(!checkValidity(sd, endDate)){
            return
        }

        // set start date
        startDate = sd
        setStartEndDateTime(startDate,endDate)
    }

    function setEndDateTimeInternal(ed){

        // check validity of new end date
        if(!checkValidity(startDate, ed)){
            return
        }

        //set end date
        endDate = ed
        setStartEndDateTime(startDate,endDate)
    }


    Component.onCompleted: {
        startDate = endDate = currentMissionDateTime
        timelineLoaderWrapper.timelineLoaded(true)
    }

    Component.onDestruction: {
        timelineLoaderWrapper.timelineLoaded(false)
    }


    VizButton{
        id: createGlobalEventButton
        anchors.left: headerDelegate.right
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 10
        text: "Add Event"
        onClicked: {
            smpFileMenu.load("AddEventGUI.qml")
        }
    }


    Item{
        id: headerDelegate
        width: 200*rootStyle.uiScale
        height: timeline.height
        Rectangle{
            anchors.fill: parent
            gradient: rootStyle.gradientWhenDefault
            opacity: 0.7
        }
        anchors.left: parent.left
        enabled: !animationRunning
        TextEdit{
            id: startTimeDelegate
            objectName: "startTimeDelegate"
            text: Qt.formatDateTime(startDate, dateTimeFormat)
            width: parent.width
            horizontalAlignment: TextEdit.AlignHCenter
            color: "white"
            font.family: rootStyle.fontFamily
            font.pointSize: 16*rootStyle.uiScale
            readOnly: true
        }        
        MouseArea{
            anchors.fill: parent
            onClicked: {
                smpFileMenu.loadTimeDateRectAt(headerDelegate.x + headerDelegate.width/2 + 50, startTimeDelegate)
            }
        }
    }

    Item{
        id: footerDelegate
        width: 200*rootStyle.uiScale
        height: timeline.height
        Rectangle{
            anchors.fill: parent
            gradient: rootStyle.gradientWhenDefault
            opacity: 0.7
        }
        anchors.right: parent.right
        enabled: !animationRunning
        TextEdit{
            id: endTimeDelegate
            objectName: "endTimeDelegate"
            text: Qt.formatDateTime(endDate, dateTimeFormat)
            width: parent.width
            horizontalAlignment: TextEdit.AlignHCenter
            color: (startDate >= endDate) ? "red" :"white"
            font.family: rootStyle.fontFamily
            font.pointSize: 16*rootStyle.uiScale
            readOnly: true
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                smpFileMenu.loadTimeDateRectAt(footerDelegate.x + footerDelegate.width/2 - 50, endTimeDelegate)
            }
        }
    }

    TimelineSlider{
        id: timeSlider
        anchors.left: headerDelegate.right
        anchors.right: footerDelegate.left
        minimumValue: 0
        maximumValue: 100
        value: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 45
        onValueChanged: {
            if(pressed){
                timeSliderValueChanged(value)
            }
        }
    }

    Row{
        id: timeControls
        anchors.horizontalCenter: parent.horizontalCenter
        property int horizontalCenterOffset: 0
        anchors.horizontalCenterOffset: horizontalCenterOffset
        Behavior on horizontalCenterOffset {
            NumberAnimation{duration: 300}
        }

        anchors.top: parent.top
        anchors.topMargin: 10
        VizButton{
            iconSource: rootStyle.getIconPath("time/slower")
            backGroundVisible: false
            onClicked: slower()
        }
        VizButton{
            iconSource: animationRunning ?  rootStyle.getIconPath("time/pause") : rootStyle.getIconPath("time/play")
            backGroundVisible: false
            onClicked: {
                if(animationRunning){
                    pause()
                }
                else{
                    play()
                }
                animationRunning = !animationRunning
            }
        }
        VizButton{
            iconSource: rootStyle.getIconPath("time/stop")
            backGroundVisible: false
            onClicked: {
                stop()
                animationRunning = false
            }
        }
        VizButton{
            iconSource: rootStyle.getIconPath("time/faster")
            backGroundVisible: false
            onClicked: faster()
        }
    }

    VizButton{
        id: minimizeButton
        anchors.top: closeButton.top
        anchors.right: closeButton.left
        anchors.rightMargin: 10
        iconSource: rootStyle.getIconPath("minimize")
        onClicked: parent.state = (parent.state == "MINIMIZED") ? "" : "MINIMIZED"
        backGroundVisible: false
    }

    VizButton{
        id: closeButton
        anchors.top: parent.top
        anchors.right: footerDelegate.left
        anchors.topMargin: 10
        anchors.rightMargin: 10
        iconSource: rootStyle.getIconPath("close")
        onClicked: timelineLoaderWrapper.minimized = true
        backGroundVisible: false
    }

    states: State {
        name: "MINIMIZED"
        PropertyChanges { target: timeline; height: 50*rootStyle.uiScale}
        PropertyChanges { target: timelineLoaderWrapper; height: timeline.height}
        PropertyChanges { target: minimizeButton; iconSource: rootStyle.getIconPath("maximize")}
        PropertyChanges { target: headerDelegate; visible: false}
        PropertyChanges { target: footerDelegate; visible: false}
        AnchorChanges { target: closeButton; anchors.right: timeline.right}
        PropertyChanges {target: timeControls; horizontalCenterOffset: -(timeline.width - timeControls.width)/2}
        PropertyChanges {target: createGlobalEventButton; visible: false}
        PropertyChanges {target: timeSlider; anchors.bottomMargin: 20}
        PropertyChanges {target: timeSlider; timeLabelVisible: false}
    }

    ListModel{
        id: dummy
        ListElement{ posX : 0.0; moving : false; deltaX: 0.20; distance : 0; profileColor: "red"}
        ListElement{ posX : 0.2; moving : false; deltaX: 0.02; distance : 0; profileColor: "green"}
        ListElement{ posX : 0.22; moving : true; deltaX: 0.08; distance: 0 ; profileColor: "blue"}
        ListElement{ posX : 0.3; moving : false; deltaX: 0.06; distance: 25; profileColor: "red"}
        ListElement{ posX : 0.36; moving : true; deltaX: 0.14; distance: 25; profileColor: "pink"}
        ListElement{ posX : 0.5; moving : false; deltaX: 0.08; distance: 50; profileColor: "gold"}
        ListElement{ posX : 0.58; moving : true; deltaX: 0.12; distance: 50; profileColor: "magenta"}
        ListElement{ posX : 0.7; moving : true; deltaX: 0.10; distance: 75; profileColor: "brown"}
        ListElement{ posX : 0.8; moving : false; deltaX: 0.20; distance: 100; profileColor: "red"}
    }
}
