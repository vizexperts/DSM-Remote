import QtQuick 2.1
import QtMultimedia 5.0
import "VizComponents"

Item{

    id: infoParent

    //infoEvent item is not fixed now, it can be dragged
    /*width: latLongBar.width
    height: 1000*/

    objectName: "infoContainer"

    Item {
        id: infoEventContainer
        anchors.fill: parent
    }


    property Component infoComponent;

    Component.onCompleted: {
        infoComponent=Qt.createComponent("InfoEventItem.qml");
    }

    function addEvent(title,description,audioPath,videoPath,imagePath,xP,yP,font,color){
        //console.log("add Event" + title)
        infoComponent.createObject(infoEventContainer,{"objectName":title,"title":title,
                                       "description":description,"audioPath":audioPath,
                                       "videoPath":videoPath,"imagePath":imagePath,
                                       "x":xP,"y":yP,"descFont":font,"descColor":color})
    }

    function removeEvent(title){
        //console.log("remove Event" + title)
        for(var i = 0; i < infoEventContainer.children.length; ++i){

            if(infoEventContainer.children[i].title===title){
                infoEventContainer.children[i].destroy();
            }
        }
    }
}

