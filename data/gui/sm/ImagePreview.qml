import QtQuick 2.1
import "VizComponents"

Item {
    clip: true
    function setImage(fileName){
        height = root.height
        width = root.width
        preview.source = fileName
    }

    Behavior on height{
        NumberAnimation{duration: 200}
    }
    Behavior on width{
        NumberAnimation{duration: 200}
    }

    Image{
        id: preview
        anchors.fill: parent
        anchors.margins: 20
    }

    VizButton{
        iconSource: rootStyle.getIconPath("minimize")
        anchors.left: preview.left
        anchors.leftMargin: 50
        anchors.top: preview.top
        anchors.topMargin: 50
        onClicked: {
            parent.height = 0
            parent.width = 0
            preview.source = ""

        }
    }
}
