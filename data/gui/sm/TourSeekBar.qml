import QtQuick 2.1
import QtQuick.Layouts 1.0
import "VizComponents"

VizSlider{
    id: seekBar

    property bool pointersVisible: true
    property bool pointersSelectable: true
    property int activeCheckPointIndex: -1
    property real speedAtCurrentCheckPoint: 1.0
    property int timeAtCurrentCheckPoint: 0
    property bool contextualVisible: false

    // signals
    signal modifyCP(int newTime, real newSpeed)
    signal deleteCP()
    signal resetActiveCP()
    signal setActiveCP(int idx)
    signal setActiveProperties()
    signal cpPositionChanged(int time)

    Component.onCompleted: {
        if( running_from_qml ){
            minimumValue = 0
            maximumValue = 5
        }
    }

    onActiveCheckPointIndexChanged: {
        if(activeCheckPointIndex == -1){
            contextualVisible = false
        }
        else{
            contextualVisible = true
        }
    }

    onSetActiveCP: {
        activeCheckPointIndex = idx
        timeAtCurrentCheckPoint = checkPointList[idx]
        setActiveProperties()
    }

    onResetActiveCP: {
        activeCheckPointIndex = -1
    }

    // contextual of a checkpoint
    // shows the checkpoint's time stamp and speed at the checkpoint
    // check point and its properties can be manipulated through this

    Rectangle{
        id: cpContextual
        color: "transparent"

        width: infoGrid.implicitWidth+ 20
        height: infoGrid.implicitHeight+ 20
        radius: 5

        visible: contextualVisible
        focus: true

        Component.onCompleted: {
            populateSpeeds()
        }

        Keys.onEscapePressed: {
            resetActiveCP()
        }

        onVisibleChanged: {
            speedAdjust.visible = false
        }

        anchors.bottom: parent.top
        anchors.bottomMargin: 10

        x: parent.width * ( timeAtCurrentCheckPoint / ( maximumValue - minimumValue )) - width/2
        z : 5

        Rectangle{
            id: bigBG
            color: rootStyle.colorWhenSelected
            opacity: 0.5
            anchors.fill: parent
            radius: 5
        }

        Rectangle{
            id: smallBG
            color: rootStyle.colorWhenDefault
            opacity: 1.0
            height: parent.height - 10
            width: parent.width - 10
            anchors.centerIn: parent
            radius: 5
        }

        GridLayout{
            id: infoGrid
            columns: 2
            anchors.centerIn: parent

            VizLabel{
                text: "Time"
            }
            VizTextField{
                width: 100
                readOnly: true
                text: (timeAtCurrentCheckPoint/1000000).toFixed(2)
                Layout.fillWidth: true
            }
            VizLabel{
                text: "Speed"
            }
            VizTextField{
                id: speedBox
                width: 100
                text: speedAtCurrentCheckPoint + "X"
                readOnly: true
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        speedAdjust.visible = !speedAdjust .visible
                    }
                }
                Layout.fillWidth: true
            }
            VizButton{
                id: deleteButton

                implicitHeight: 35
                implicitWidth: implicitHeight

                Layout.alignment: Qt.AlignHCenter

                iconSource: rootStyle.getIconPath("delete")
                onClicked: {
                    deleteCP()
                    resetActiveCP()
                }
            }
            VizButton{
                id:saveButton
                text: "Save"

                Layout.alignment: Qt.AlignHCenter

                onClicked: {
                    modifyCP(timeAtCurrentCheckPoint, speedAtCurrentCheckPoint)
                    resetActiveCP()
                }
            }
        }

        function populateSpeeds(){
            speedModel.clear()
            for(var i = -3; i <= 4 ; i++)
                speedModel.append({"speed" : Math.pow(2,i)})
            speedAdjust.currentIndex = 3
        }

        function getIndex(val){
            for(var i= -3; i<=4; i++){
                if(Math.pow(2,i) === val)
                    return i+3
            }
        }

        ListModel{
            id:speedModel
        }

        Spinner{
            id: speedAdjust
            model: speedModel
            width: 40
            height: 100
            itemHeight: 30

            visible: false

            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: -5
            anchors.right: parent.right
            anchors.rightMargin: 10

            delegate: VizLabel{
                text: speed
                scale: (index == speedAdjust.currentIndex) ? 1 : 0.8
                textColor: (index == speedAdjust.currentIndex) ? "white" : rootStyle.textColor
            }

            onVisibleChanged: {
                if(visible)
                    currentIndex = parseInt(parent.getIndex(speedAtCurrentCheckPoint))
            }

            onCurrentIndexChanged: {
                speedAtCurrentCheckPoint = speedModel.get(currentIndex).speed
            }
        }
    }

    // to show check points over the slider
    // check point list is given as model to the repeater
    // repeater delegates each check point in the form of a tag

    Repeater{
        id: checkPoints

        anchors.left: parent.left
        anchors.right: parent.right

        model: (running_from_qml)? 4 : checkPointList

        delegate:
            Item {
            id: pointer
            height: 1
            width: 1

            visible: pointersVisible

            x: (running_from_qml)? seekBar.width * ((index+1)/( maximumValue - minimumValue )) : seekBar.width * ( checkPointList[index] / ( maximumValue - minimumValue ))
            y: -5

            onXChanged: {
                if( pointerArea.drag.active ){
                    timeAtCurrentCheckPoint = (maximumValue - minimumValue)*x/seekBar.width
                    cpPositionChanged(timeAtCurrentCheckPoint)
                }
            }

            MouseArea{
                id: pointerArea
                anchors.fill: pointerHead
                visible: pointersSelectable
                onClicked: {
                    if(activeCheckPointIndex != index){
                        setActiveCP(index)
                    }
                    else
                        resetActiveCP()
                }
                onReleased: {
                    if(pointerArea.drag.active == false){
                        modifyCP(timeAtCurrentCheckPoint,speedAtCurrentCheckPoint)
                    }
                }

                drag.target: contextualVisible? pointer : null
                drag.axis: Drag.XAxis
                drag.minimumX: 0
                drag.maximumX: seekBar.width
            }

            Rectangle{
                id: pointerHead

                color: "transparent" //(activeCheckPointIndex == index) ? rootStyle.colorWhenSelected : "yellow"
                height:16
                width: height
                radius: height/2

                border.width: 2
                border.color: (activeCheckPointIndex == index) ? rootStyle.colorWhenSelected : "yellow"

                z:1

                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.verticalCenter
            }

            Rectangle{
                id: pointerTail

                height: 12
                width: 2
                color: "yellow"
                anchors.top : parent.verticalCenter
            }
        }
    }
}
