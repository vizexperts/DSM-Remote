import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: vectorQueryBuilder
    objectName: "VectorQueryBuilder"
    title: "Query Builder"

    height: mainLayout.implicitHeight+2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+2*mainLayout.anchors.margins

    minimumHeight: mainLayout.Layout.minimumHeight+10
    minimumWidth: mainLayout.Layout.minimumWidth+10


    property alias queryName : queryNameTextBox.text


    property string shapeFileName

    property alias sqlWhereText : sqlWhereTextId.text

    //Signal defined for buttons
    signal addToWhere(int index)

    signal operatorButtonClicked(string operator)
    signal queryButtonClicked()
    signal clearButtonClicked()
    signal cancelButtonClicked()
    signal okButtonClicked()
    signal addValueToWhere(int index)
    signal changeText(string all)
    signal showAllValues(int index)
    signal showSampleValues(int index)

    signal populateFieldDefinitions()

    Component.onCompleted:
    {
        if(!running_from_qml){
            VectorLayerQueryBuilderGUI.setActive(true);
        }

    }
    Component.onDestruction:
    {
        if(!running_from_qml){
            VectorLayerQueryBuilderGUI.setActive(false);
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id: queryName
            spacing: 5
            Layout.alignment: Qt.AlignRight


            VizLabel{
                text: "Query Name"
            }
            VizTextField{
                id: queryNameTextBox
                Layout.minimumWidth: 300
                width: 300
                text: ""
                placeholderText: "Query Name..."
                Layout.fillWidth: true
            }
        }
        RowLayout{
            Layout.alignment: Qt.AlignCenter
            spacing: 5
            ColumnLayout{
                id: allFieldsColumnId

                VizLabel{
                    text: "All Fields"
                }

                Rectangle{
                    color: "transparent"
                    border.width: 2
                    border.color: rootStyle.colorWhenSelected
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 150*rootStyle.uiScale
                    Layout.minimumHeight: 150*rootStyle.uiScale
                    ListView{
                        id: allFieldsListView
                        currentIndex: -1
                        anchors.fill: parent
                        model: (typeof(allFieldsList) != "undefined") ? allFieldsList : "undefined"
                        spacing: 4
                        VizScrollBar{ target: allFieldsListView }
                        clip: true
                        Keys.onUpPressed: if(currentIndex > 0) currentIndex--
                        Keys.onDownPressed: if(currentIndex < count) currentIndex++
                        focus: true
                        delegate: VizLabel{
                            id: label
                            text: modelData
                            textAlignment: Qt.AlignLeft
                            width: allFieldsListView.width
                            Behavior on color {
                                ColorAnimation { duration: 200 }
                            }
                            Behavior on textColor {
                                ColorAnimation { duration: 200 }
                            }

                            textColor: (allFieldsListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                            color: (allFieldsListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                            MouseArea{
                                anchors.fill: parent
                                onClicked: allFieldsListView.currentIndex = index
                                onDoubleClicked: addToWhere(allFieldsListView.currentIndex)
                            }
                        }
                    }
                }
            }
            ColumnLayout{
                id: distinctValuesColumnId

                VizTextField{
                    id: values
                    anchors.right: parent.right
                    placeholderText: "Values"
                    Layout.fillWidth: true
                    Keys.onEscapePressed: {
                        setFocus = false
                        text = ""
                    }
                    onTextChange:
                    {
                        changeText(values.text)
                        showAllValues(allFieldsListView.currentIndex)
                    }
                }
                Rectangle{
                    color: "transparent"
                    border.width: 2
                    border.color: rootStyle.colorWhenSelected
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 150*rootStyle.uiScale
                    Layout.minimumHeight: 150*rootStyle.uiScale
                    ListView{
                        id: distinctValuesListView
                        currentIndex: -1
                        anchors.fill: parent
                        clip: true
                        model: (typeof(distinctValuesList) != "undefined") ? distinctValuesList : "undefined"
                        spacing: 4
                        Keys.onUpPressed: if(currentIndex > 0) currentIndex--
                        Keys.onDownPressed: if(currentIndex < count) currentIndex++
                        focus: true
                        VizScrollBar{
                            target: distinctValuesListView
                        }

                        delegate: VizLabel{
                            text: modelData
                            textAlignment: Qt.AlignLeft
                            width: distinctValuesListView.width
                            Behavior on color {
                                ColorAnimation { duration: 200 }
                            }
                            Behavior on textColor {
                                ColorAnimation { duration: 200 }
                            }

                            textColor: (distinctValuesListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                            color: (distinctValuesListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                            MouseArea{
                                anchors.fill: parent
                                onClicked:  distinctValuesListView.currentIndex = index
                                onDoubleClicked: addValueToWhere(distinctValuesListView.currentIndex)
                            }
                        }
                    }
                }

                RowLayout{
                    spacing: 5
                    Layout.alignment: Qt.AlignCenter
                    VizButton{
                        text: "Sample"
                        onClicked: showSampleValues(allFieldsListView.currentIndex)
                    }
                    VizButton{
                        text: " All "
                        visible: false
                        onClicked: showAllValues(allFieldsListView.currentIndex)
                    }
                }
            }
        }



        VizGroupBox{
            id: operatorsId
            title: "Operators"
            Layout.fillWidth: true
            Layout.fillHeight: true

            GridLayout{
                id: operatorsGridId
                columns: 8

                VizButton{
                    text: "     =     "
                    onClicked: operatorButtonClicked("= ");
                }
                VizButton{
                    text: "     <     "
                    onClicked: operatorButtonClicked("< ");
                }
                VizButton{
                    text: "     >     "
                    onClicked: operatorButtonClicked("> ");
                }
                VizButton{
                    text: "   LIKE   "
                    onClicked: operatorButtonClicked("LIKE ");
                }
                VizButton{
                    text: "     %      "
                    onClicked: operatorButtonClicked("% ");
                }
                VizButton{
                    text: "     IN    "
                    onClicked: operatorButtonClicked("IN ");
                }
                VizButton{
                    text: " NOT IN  "
                    onClicked: operatorButtonClicked("NOT IN ");
                }
                VizButton{
                    text: "    <=    "
                    onClicked: operatorButtonClicked("<= ");
                }
                VizButton{
                    text: "    >=    "
                    onClicked: operatorButtonClicked(">= ");
                }
                VizButton{
                    text: "     !=    "
                    onClicked: operatorButtonClicked("!= ");
                }
                VizButton{
                    text: "   ILIKE  "
                    onClicked: operatorButtonClicked("ILIKE ");
                }
                VizButton{
                    text: "     AND   "
                    onClicked: operatorButtonClicked("AND ");
                }
                VizButton{
                    text: "    OR   "
                    onClicked: operatorButtonClicked("OR ");
                }
                VizButton{
                    text: "   NOT    "
                    onClicked: operatorButtonClicked("NOT ");
                }
                VizButton{
                    text: "    (    "
                    onClicked: operatorButtonClicked("( ");
                }
                VizButton{
                    text: "    )    "
                    onClicked: operatorButtonClicked(") ");
                }
            }
        }

        VizGroupBox{
            id: sqlWhereGroupId
            title: "SQL Where Clause"
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout{
                anchors.fill: parent
                VizTextEdit{
                    id: sqlWhereTextId
                    text: ""
                    //anchors.fill: parent
                    z:155
                    anchors.margins: 10
                    Layout.minimumHeight: 100
                    Layout.fillWidth: true
                    onTextChanged: cursorPosition = sqlWhereTextId.text.length
                }
            }
        }

        RowLayout{
            id: controlCommandId
            spacing: 5
            Layout.alignment: Qt.AlignRight

            VizButton{
                text: "    Clear    "
                onClicked: clearButtonClicked()
            }
            VizButton{
                text: "  Cancel  "
                onClicked:{
                    cancelButtonClicked()
                    closePopup()
                }
            }
            VizButton{
                text: "     OK     "
                onClicked:{
                    okButtonClicked()
                }
            }
        }
    }
}
