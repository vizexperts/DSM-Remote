import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "changeDetection"
    title: "Change Detection"
    id:cvaID

    Component.onCompleted: {
        if(!running_from_qml){
            CVAGUI.setActive(true)
        }
    }

    onClosed: {
        if(!running_from_qml){
            CVAGUI.setActive(false)
        }
    }
    Component.onDestruction:
    {
        if(!running_from_qml){
            CVAGUI.setActive(false)
        }

    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal browseInput1Image()
    signal browseInput2Image()
    signal browseDistanceImage()
    signal browseAngleImage()

    signal viewImage(string imagePath)

    signal calculate()
    signal stopButtonClicked()


    property alias input1ImagePath : input1TextFeildID.text
    property alias input2ImagePath : input2TextFieldID.text

    property alias startButtonEnabled: okButton.enabled
    property alias stopButtonEnabled: stopButton.enabled

    property alias input1ButtonEnabled: input1OpenID.enabled
    property alias input2ButtonEnabled: input2OpenID.enabled
    property alias outputDButtonEnabled: outputDOpenID.enabled
    property alias outputAButtonEnabled: outputAOpenID.enabled

    property alias outputDistance: outputDTextFeildID.text
    property alias outputAngle: outputATextFieldID.text
    property bool calculating: false


    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizGroupBox {
            title: "Input files"

            Layout.fillWidth: true
            Layout.fillHeight: true

            GridLayout{
                id: inputGrid
                columns: 4
                anchors.fill: parent

                VizLabel {
                    id : input1LabelID
                    text: "Input1 Image"
                }
                VizTextField {
                    id: input1TextFeildID
                    enabled:!calculating
                    text: ""
                    placeholderText : "Browse Input1 Image file..."
                    Layout.fillWidth: true
                    validator:rootStyle.nameValidator
                }
                VizButton {
                    id : input1BrowseID
                    enabled:!calculating
                    text : "Browse"
                    onClicked:browseInput1Image()
                }
                VizButton {
                    id : input1OpenID
                    text : "View"
                    onClicked:viewImage(input1ImagePath)
                }
                VizLabel {
                   id : input2LabelID
                   text: "Input2 Image"
                }
                VizTextField {
                   id: input2TextFieldID
                   enabled:!calculating
                   text: ""
                   Layout.fillWidth: true
                   placeholderText : "Browse Input2 Image file..."
                   validator:rootStyle.nameValidator
                }
                VizButton {
                    id : input2BrowseID
                    enabled:!calculating
                    text : "Browse"
                    onClicked:browseInput2Image()
                }
                VizButton {
                    id : input2OpenID
                    text : "View"
                    onClicked:viewImage(input2ImagePath)
                }
             }
        }

        VizGroupBox {
            id : outputPathGroupBoxId
            title: "output files"

            height: outputGrid.implicitHeight+50
            width: outputGrid.implicitWidth+10

            Layout.fillWidth: true

            GridLayout{
                id: outputGrid
                columns: 4
                anchors.fill: parent

                VizLabel {
                    id : outputDLabelID
                    text: "Output(Distance)"
                }
                VizTextField {
                    id: outputDTextFeildID
                    enabled:!calculating
                    text: ""
                    placeholderText : "Enter OutputDistance Image Name"
                    Layout.fillWidth: true
                    validator:rootStyle.nameValidator
                }
                VizButton {
                    id : outputDBrowseID
                    enabled:!calculating
                    text : "Browse"
                    onClicked:browseDistanceImage()
                }
                VizButton {
                    id : outputDOpenID
                    text : "View"
                    onClicked:viewImage(outputDistance)
                }

                VizLabel {
                   id : outputALabelID
                   text:  "Output(Angle)"
                }
                VizTextField {
                   id: outputATextFieldID
                   enabled:!calculating
                   text: ""
                   Layout.fillWidth: true
                   placeholderText :"Enter Output Angle Image Name"
                   validator:rootStyle.nameValidator
                }
                VizButton {
                    id : outputABrowseID
                    enabled:!calculating
                    text : "Browse"
                    onClicked:browseAngleImage()
                }
                VizButton {
                    id : outputAOpenID
                    text : "View"
                    onClicked:viewImage(outputAngle)
                }
             }
        }

        RowLayout{
            id:commandButtonRect
            spacing: 10

            AnimatedImage{
                id:progressBarId
                Layout.fillWidth: true
                source: rootStyle.getBusyIcon()
                visible: calculating
            }

            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id:okButton
                text: "   Calculate   "
                onClicked: calculate()
            }

            VizButton{
                id:stopButton
                text: "  Stop  "
                onClicked: stopButtonClicked()
            }
        }
    }
}


