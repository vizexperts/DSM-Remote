import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ProfileObject 1.0
import ".."

Popup{

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins


    minimumWidth: mainLayout.Layout.minimumWidth + 200
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias profileWidth: profileGrid.width

    objectName: "ditchProfile"
    title: "Extrusion Profile"

    Component.onCompleted: {
        graph.startDrawing(true)
        graph.editGraph(false)
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    property bool startDrawing : false
    property alias name : profileName.text
    property alias sideTextureFilePath : textureFilePath.text
    property string graphXProp : "10"
    property string graphYProp : "10"
    property var points : []
    property alias profileCreatedText : profileCreated.text
    property alias editProfileTicked : editProfile.checked
    signal browseButtonClicked()
    signal showGUIMessage(string message)
    signal saveProfile(var pointList)
    signal updateProfileText(string x, string y)

    property alias profileYText : profileY.text
    property alias profileXText : profileX.text

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        Layout.minimumWidth: 350
        Layout.fillHeight: true
        Layout.fillWidth: true
        RowLayout{

            VizLabel{
                text: "Name"
                width : 75
            }

            VizTextField{
                id:profileName
                Layout.minimumWidth: 300
                Layout.fillWidth: true
                placeholderText: " Give profile name for saving... "
                validator:rootStyle.nameValidator
            }

            Rectangle{
                Layout.fillWidth: true
            }
        }

        RowLayout{
            id:textureRow
            VizLabel{
                text: "Texture"
                width : 75
            }

            VizTextField{
                id:textureFilePath
                Layout.minimumWidth: 300
                Layout.fillWidth: true
                placeholderText: " Browse texture file... "

            }

            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id:browse
                text: "Browse"
                onClicked:{
                    browseButtonClicked()
                }
            }

            Rectangle{
                Layout.fillWidth: true
            }
        }


        RowLayout{
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.top:textureRow.bottom
            anchors.bottom: drawRow.top
            anchors.margins: 5


            GridLayout{
                id: profileGrid
                Layout.minimumWidth:450
                Layout.minimumHeight: 450

                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.top: parent.top
//To make the square grid resize with popup size
                onHeightChanged: {
                    if(height > width){
                        height = width
                    }
                    else{
                        width = height
                    }
                }

                onWidthChanged: {
                    if(height > width){
                        height = width
                    }
                    else{
                        width = height
                    }
                }



                ProfileObject{
                    id : graph
                    objectName: "ditchProfilePopup"
                    anchors.fill: parent
                    anchors.margins: 10
                }
            }
            ColumnLayout
            {
                Layout.maximumWidth: 120
                spacing: 5
                anchors.left:profileGrid.right
                anchors.verticalCenter: parent.verticalCenter

                RowLayout{
                    VizLabel{
                        text: "X"
                        width : 15
                    }

                    VizTextField{
                        id:profileX
                        width: 60
                        text : graphXProp
                    }
                }


                RowLayout{
                    VizLabel{
                        text: "Y"
                        width : 15
                    }

                    VizTextField{
                        id:profileY
                        width: 60
                        text : graphYProp
                    }
                }
                VizButton{
                    id:setProfileXYID
                    width: 50
                    text: " Set "
                    onClicked:{
                        if(editProfileTicked)
                        {
                            graphXProp = profileX.text
                            graphYProp = profileY.text
                            graph.setProfileXY(profileX.text, profileY.text)
                        }
                    }
                }
            }
        }

        RowLayout{
            id:drawRow
            VizButton{
                id:addProfile
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 50
                text: "Draw"
                checkable: true
                checked: true
                onClicked:{
                    profileCreatedText = ""
                    graph.startDrawing(checked)
                    graph.editGraph(!checked)
                    editProfile.checked = !checked
                }
            }

            Rectangle{
                width : 8
            }

            VizButton{
                id:editProfile
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 50
                checkable: true
                text: "Edit"
                onClicked:{
                    profileCreatedText = ""
                    graph.startDrawing(!checked)
                    graph.editGraph(checked);
                    addProfile.checked = !checked
                }
            }

            Rectangle{
                width : 8
            }

            VizButton{
                id:eraseRectangle
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 50
                text: "Erase"
                onClicked:{
                    profileCreatedText = ""
                    graph.clearGraph()
                }
            }

            Rectangle{
                Layout.fillWidth: true
            }
            Rectangle{
                Layout.fillWidth: true
            }
            VizLabel{
                id: profileCreated
                text: ""
                width : 100
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id:done
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 50
                text: "Create"
                onClicked:{
                    points = graph.getLineData()
                    saveProfile(points)
                }
            }
        }
    }
}
