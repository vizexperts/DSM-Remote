import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"
import ".."


//! XXX: Only being used for 3d connected features now(Fence feature)
Popup {

    id: lineLayerStylePopup
    objectName: "lineLayerStylePopup"
    title: "3D connected Feature"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth + 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 15
    minimumHeight: mainLayout.Layout.minimumHeight +15
    property alias editTemplateButton:editButton.checked;
    property bool open3DConnectedFeatureParameterGrid: false
    property alias selectedTemplateName: connected3DFeatureBoxId.value
    signal deleteSelectedTemplate(string value)
    signal editConnectedFeature(string value)

    signal saveTemplate();
    property alias templateName:templateNameTextBox.text;

    property alias type: geometryTypeComboBox.value
    property alias numberOfPillar: numberOfPillarId.value
    property alias pillarModelPath1: filePathPillar1.text
    property alias pillarModelPath2: filePathPillar2.text
    property alias pillarModelPath3: filePathPillar3.text

    property alias interPillarDistance: interPillarDistId.text
    property alias featureWidth: featureWidthId.text
    property alias sideTexturePath: sideTexture.text
    property alias topSideTexturePath: topSideTexture.text
    property alias bottomSideTexturePath: bottomSideTexture.text
    property alias inner3DModelPath: inner3DModelPathId.text

    property alias sideOffset: sideOffset.text
    property alias topSideOffset: topSideOffset.text
    property alias bottomSideOffset: bottomSideOffset.text
    property alias numberOfInnerModel: numberOfInnerModelId.text
    property alias innerModelOffset: innerModelOffset.text
    property alias featureModelPath: featureModelPathId.text
    property alias featureOffsetVal: featureOffsetId.text
    property alias sidesOfFeatureVal: sidesOfFeatureID.value

    property bool sideImageTextureVisible:sideTexture.visible;
    property bool topSideImageTextureVisible:topSideTexture.visible;
    property bool bottomSideImageTextureVisible:bottomSideTexture.visible;
    property bool inner3DModelVisible:inner3DModelPathId.visible;
    property bool featureModelVisible:featureModelPathId.visible;

    signal applyButtonClick();
    signal okButtonPressed()
    property alias busyBarVisible : busyBar.visible

    //! disable the UI when doing this
    enabled: !busyBarVisible

    signal browseButtonClicked(string value,bool filter);
    property alias geometryType: geometryTypeComboBox.selectedUID

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ListModel{
        id: sidesOfFeatureTypeModel
        ListElement{name:"Left Side";uID : "0"}
        ListElement{name:"Right Side" ; uID : "1"}
        ListElement{name:"Both Side";uID : "2"}

    }
    ListModel{
        id: numberOfPillarTypeModel
        ListElement{name:"1";uID : "0"}
        ListElement{name:"2";uID : "1"}
        ListElement{name:"3" ; uID : "2"}
    }
    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id:layerNameRow
            spacing: 5
            z: 60
            VizLabel{
                text: "Choose "
                width: 130
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: connected3DFeatureBoxId
                    Layout.minimumWidth: 200
                    Layout.fillWidth: true
                    listModel: typeof(connected3DFeatureList)== "undefined" ? "undefined" : connected3DFeatureList
                    value: connected3DFeatureList.get(selectedUID).name
                    onSelectOption: {
                        editConnectedFeature(value)
                    }

            }

            VizButton{
                id:editButton
                Layout.minimumWidth: 50
                checkable: true
                text: "Edit"
                onClicked:
                        editConnectedFeature(connected3DFeatureBoxId.value)
            }

            VizButton{
                id:deleteButton
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 50
                text: " Delete "
                onClicked:
                {
                    deleteSelectedTemplate(connected3DFeatureBoxId.value)
                }
            }
        }
        RowLayout{
            id:queryGroupCol
            spacing: 5
            visible: open3DConnectedFeatureParameterGrid
            VizLabel{
                width: 130
                text: "Template name"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: templateNameTextBox
                Layout.minimumWidth: 300
                width: 300
                text: ""
                placeholderText: "Template Name..."
                Layout.fillWidth: true
            }
        }
        RowLayout{
            id: drawTechniqueRow
            visible: open3DConnectedFeatureParameterGrid
            spacing: 5
            z: 40
            VizLabel{
                text: "Type"
                width: 130
                textAlignment: Qt.AlignLeft
            }
            VizComboBox{
                id: geometryTypeComboBox
                Layout.fillWidth: true
                Layout.minimumWidth: 200*rootStyle.uiScale
                onSelectOption: {
                 //   changeDrawTechniqueType(drawPropertyTypeModel.get(uID).drawPropertyName)
                      //applyButton.enabled = (uID!= 1)
                }
                listModel: ListModel{
                    id: drawPropertyTypeModel
                    ListElement{name:"Fence"; drawPropertyName: "FenceDrawProperty" ; uID : "0"}
                    ListElement{name:"Bridge"; drawPropertyName: "FenceDrawProperty" ; uID : "1"}
                    ListElement{name:"Power Line"; drawPropertyName: "FenceDrawProperty" ; uID : "2"}
                    ListElement{name:"Custom"; drawPropertyName: "FenceDrawProperty" ; uID : "3"}
                }
            }
        }
        VizGroupBox{
            title: "Pillars"
            visible: open3DConnectedFeatureParameterGrid
            z:5
            Layout.fillWidth: true
            ColumnLayout{
                id: pillar
                anchors.fill: parent
                anchors.margins: 1

                GridLayout{
                    id: pillarId
                    Layout.fillWidth: true
                    z: 20

                    VizLabel{
                        text:"Number of pillars"
                        width: 130
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox{
                        id: numberOfPillarId
                        Layout.minimumWidth: 200
                        listModel: numberOfPillarTypeModel
                        value: numberOfPillarTypeModel.get(selectedUID).name
                        onSelectOption: {

                        }


                    }


                }
                GridLayout{
                    id:pillarModeId

                    //anchors.fill: parent
                    anchors.margins: 5
                    columns: 1
                    RowLayout{
                        VizLabel{
                            text: "First pillar model"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: filePathPillar1
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked: browseButtonClicked("pillarModelPath1",true);
                        }
                    }
                    RowLayout{
                        visible: !(numberOfPillarId.value==='1')
                        VizLabel{
                            text: "Second Pillar model"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: filePathPillar2
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked: browseButtonClicked("pillarModelPath2",true);
                        }
                    }
                    RowLayout{
                        visible: numberOfPillarId.value==='3'
                        VizLabel{
                            text: "Third pillar model"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: filePathPillar3
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked: browseButtonClicked("pillarModelPath3",true);
                        }
                    }
                }
                GridLayout{
                    id:inputParameterRect

                    //anchors.fill: parent
                    anchors.margins: 5
                    columns: 1
                    RowLayout{

                        VizLabel{
                            text:"Inter pillar dist"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: interPillarDistId
                            text: ""
                            Layout.minimumWidth: 200
                            validator:  DoubleValidator { bottom : 0;top:999999;notation: DoubleValidator.StandardNotation;}
                            placeholderText: "Enter Inter pillar distance"
                        }
                        VizLabel{
                            text:"Feature width"
                            textAlignment: Qt.AlignLeft
                            width: 130
                        }
                        VizTextField{
                            id: featureWidthId
                            text: ""
                            Layout.fillWidth: true
                            validator:  DoubleValidator { bottom : 0;top:999999;notation: DoubleValidator.StandardNotation;}
                            placeholderText: "Enter Feature width"
                        }
                    }
                }
            }
        }
        VizGroupBox{
            title: "Side Texture"
            z:5
            visible: open3DConnectedFeatureParameterGrid && geometryType !== '1'
            Layout.fillWidth: true
            ColumnLayout{
                id: sideTextureId
                anchors.fill: parent
                anchors.margins: 1
                GridLayout{
                    id:sideTextureGridId

                    //anchors.fill: parent
                    anchors.margins: 5
                    columns: 1
                    RowLayout{
                        visible: geometryType === '3'||geometryType === '0'
                        VizLabel{
                            text: "Side image"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: sideTexture
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            //Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked: browseButtonClicked("sideTexturePath",false);
                        }

                        VizLabel{
                            text:"Side image offset"
                            textAlignment: Qt.AlignLeft
                            Layout.minimumWidth: 150
                        }
                        VizTextField{
                            id: sideOffset
                            text: ""
                            Layout.alignment: Qt.AlignLeft
                            Layout.fillWidth: true
                            validator:  DoubleValidator { bottom : 0;top:999999;notation: DoubleValidator.StandardNotation;}
                            placeholderText: "Enter side offset"
                        }
                    }
                    RowLayout{
                        visible: geometryType === '3'
                        VizLabel{
                            text: "Top image"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: topSideTexture
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            //Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked:browseButtonClicked("topSideTexturePath",false);
                        }
                        VizLabel{
                            text:"Top image offset"
                            Layout.minimumWidth: 150
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: topSideOffset
                            text: ""
                            Layout.fillWidth: true
                            validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation;}
                            placeholderText: "Enter top side offset"
                        }
                    }

                    RowLayout{
                        visible: geometryType === '3'
                        VizLabel{
                            text: "Bottom image"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: bottomSideTexture
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            //Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked: browseButtonClicked("bottomSideTexturePath",false);
                        }
                        VizLabel{
                            text:"Bottom image offset"
                            Layout.minimumWidth: 150
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: bottomSideOffset
                            text: ""
                            Layout.fillWidth: true
                            validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation;}
                            placeholderText: "Enter Bottom side offset"
                        }
                    }
                    RowLayout{
                        visible: geometryType === '3'||geometryType === '0'||geometryType === '2'
                        VizLabel{
                            text: "Inner 3D model"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: inner3DModelPathId
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            //Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked: browseButtonClicked("inner3DModelPath",true);
                        }

                        VizLabel{
                            text:"Inner model offset"
                            Layout.minimumWidth: 150
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: innerModelOffset
                            Layout.fillWidth: true
                            validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation;}
                            placeholderText: "Enter offset"
                        }
                    }
                    RowLayout{
                        visible: geometryType === '3'||geometryType === '0'||geometryType === '2'
                        VizLabel{
                            text:"No. of inner 3D models"
                            Layout.minimumWidth: 150
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: numberOfInnerModelId
                            Layout.fillWidth: true
                            validator:   IntValidator { bottom : 1; top:10;}
                            placeholderText: "Enter number of 3D model"
                        }
                    }
                }

            }
        }
        VizGroupBox{
            title: "Additional side feature"
            z:5
            visible: open3DConnectedFeatureParameterGrid
            Layout.fillWidth: true
            Layout.minimumHeight: additionalId.implicitHeight + margins
            Layout.minimumWidth: additionalId.implicitWidth + 20
            ColumnLayout{
                id: additionalId
                anchors.fill: parent
                anchors.margins: 1
                GridLayout{
                    id:additionalGridId

                    //anchors.fill: parent
                    anchors.margins: 5
                    columns: 1

                    RowLayout{
                        VizLabel{
                            text: "Feature model"
                            textAlignment: Qt.AlignLeft
                            width: 130
                        }
                        VizTextField{
                            id: featureModelPathId
                            Layout.minimumWidth: 200
                            text: ""
                            placeholderText: "Browse files..."
                            //Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 50
                            onClicked: browseButtonClicked("featureModelPath",true);
                        }
                        VizLabel{
                            text:"Feature offset"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: featureOffsetId
                            text: ""
                            Layout.fillWidth: true
                            validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation;}
                            placeholderText: "Enter feature offset"
                        }
                    }
                    RowLayout{

                        VizLabel{
                            text:"Sides of feature"
                            width: 130
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox{
                            id: sidesOfFeatureID
                            Layout.fillWidth: true
                            Layout.minimumWidth: 200*rootStyle.uiScale
                            listModel: sidesOfFeatureTypeModel
                            value: sidesOfFeatureTypeModel.get(selectedUID).name
                            onSelectOption: {

                            }

                        }
                    }
                }
            }
        }
        RowLayout{
            id: okCancelRow
            visible: open3DConnectedFeatureParameterGrid
            Layout.fillWidth: true
            Rectangle{
                Layout.fillWidth: true
            }
            VizButton{
                id: applyButton
                text: "   Save   "
                Layout.alignment: Qt.AlignRight
                Layout.minimumWidth: 50
                onClicked:{
                    saveTemplate()
                    //changeDrawTechniqueType(drawPropertyTypeModel.get(geometryTypeComboBox.selectedUID).drawPropertyName)
                }
            }


            VizButton{
                text: "  Cancel  "
                Layout.alignment: Qt.AlignRight
                Layout.minimumWidth: 50
                onClicked:
                {
                    open3DConnectedFeatureParameterGrid=false;
                    editTemplateButton=false;
                }
            }

        }
        RowLayout{
            id:commandButtonRect
            spacing: 10
            Layout.minimumHeight: okButton.implicitHeight
            visible: !open3DConnectedFeatureParameterGrid
            Rectangle{
                width: parent.width - busyBar.width-okButton.width - cancelButton.width -20
                Layout.fillWidth: true
            }
            AnimatedImage{
                id: busyBar
                source: rootStyle.getBusyBar()
                Layout.minimumWidth: 100
                visible: false
            }
            VizButton{
                id:okButton
                text: "   Apply   "
                width: 130
                onClicked:{
                    applyButtonClick()
                }

            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                width: 130
                onClicked: {
                    closePopup()
                }
            }
        }
    }
}
