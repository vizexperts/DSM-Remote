import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup {
    id: databseSettings

    objectName: "databaseSettingsMenu"
    title: "Database Settings"

    height: mainLayout.implicitHeight + 15
    width: mainLayout.implicitWidth+ 10

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal saveDatabaseParameters()
    signal deleteDatabase(string value)
    signal comboBoxClicked(string value)
    signal testDatabaseConnection()

    property alias value : databaseBox.value
    property alias connectionName: connectionNameLineEdit.text
    property alias databaseName: databaseNameLineEdit.text
    property alias userName: databaseUsernameLineEdit.text
    property alias password:databasePasswordLineEdit.text
    property alias host:databaseHostLineEdit.text
    property alias port:databasePortLineEdit.text
    property alias databaseTypeValue: databaseTypeBox.value

    property bool testResultVisible : false
    property alias status : statusButton.text

    Component.onCompleted: {

        if(!running_from_qml){
           DatabaseSettingsGUI.setActive(true)
        }

    }

    Component.onDestruction: {
        if(!running_from_qml){
           DatabaseSettingsGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{

            id:databseNameRow
            spacing: 5
            z: 150

            VizLabel{
                text: "Choose "
            }

            VizComboBox{
                id: databaseBox
                Layout.minimumWidth: 250
                listModel: typeof(databaseList)== "undefined" ? "undefined" : databaseList
                value: databaseBox.get(selectedUID).name

                onSelectOption: {
                    comboBoxClicked(value)
                    testResultVisible = false
                }

            }

            VizButton{
                id:deleteButton
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 35
                text: " Delete "
                onClicked:
                {
                    deleteDatabase(value)
                    testResultVisible = false
                }
            }
        }



        VizGroupBox{
            id:databaseParameterGroupBox
            title: "New"
            visible: databaseBox.value != "Select Database Instance"
            clip: true
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true

            GridLayout{
                Layout.fillWidth: true
                Layout.fillHeight: true
                columns: 2

                VizLabel{
                    text: "Connection Name"
                }
                VizTextField{
                    id: connectionNameLineEdit
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200

                    Keys.onTabPressed: databaseNameLineEdit.giveFocus()
                }

                VizLabel{
                    text: "Database Type "
                }

                VizComboBox{
                    id: databaseTypeBox
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200

                    listModel:ListModel{
                        ListElement{name: "postgres"; uID: "0"}
                        ListElement{name: "odbc"; uID: "1"}
                    }

                    value: "postgres"
                }

                VizLabel{
                    text: "Database Name"
                    visible: databaseTypeValue == "odbc"
                }
                VizTextField{
                    id: databaseNameLineEdit
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    Keys.onTabPressed: databaseUsernameLineEdit.giveFocus()
                    visible: databaseTypeValue == "odbc"
                }

                VizLabel{
                    text: "Username"
                }
                VizTextField{
                    id: databaseUsernameLineEdit
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    Keys.onTabPressed: databasePasswordLineEdit.giveFocus()
                }
                VizLabel{
                    text: "Password"
                }
                VizTextField{
                    id: databasePasswordLineEdit
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    Keys.onTabPressed: databaseHostLineEdit.giveFocus()
                    echoMode: TextInput.Password
                }
                VizLabel{
                    text: "Host"
                    visible: databaseTypeValue == "postgres"
                }
                VizTextField{
                    id: databaseHostLineEdit
                    visible: databaseTypeValue == "postgres"
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    Keys.onTabPressed: databasePortLineEdit.giveFocus()
                }
                VizLabel{
                    text: "Port"
                    visible: databaseTypeValue == "postgres"
                }
                VizTextField{
                    id: databasePortLineEdit
                    visible: databaseTypeValue == "postgres"
                    validator: IntValidator{ bottom: 0 }
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                }

                VizButton{
                    id: saveButton
                    anchors.left: parent.left
                    anchors.leftMargin: 50
                    Layout.minimumWidth: 100
                    text:" Save "
                    onClicked: {
						testResultVisible = false
                        saveDatabaseParameters()
                    }
                }

                VizButton{
                    id: testButton
                    anchors.right: parent.right
                    anchors.rightMargin: 50
                    Layout.minimumWidth: 100
                    text:" Test "
                    onClicked: {
                        testResultVisible = false
                        testDatabaseConnection()

                    }
                }
            }
        }

        VizLabel{
            id: statusButton
            Layout.alignment: Qt.AlignHCenter
            text: status
            visible: testResultVisible
        }

    }
}
