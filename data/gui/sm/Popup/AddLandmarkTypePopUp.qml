import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{

    title: "Add New Landmark Type"
    objectName: "addLandmarkTypePopup"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth + 2*mainLayout.anchors.margins

    minimumHeight: mainLayout.implicitHeight + 10
    minimumWidth: mainLayout.implicitWidth + 10

    signal browseButtonClicked()
    signal addNewCategory()
    signal addLandmarkType(string path)
    signal showGUIError(string title, string message)

    property alias modelFilePath: modelPath.text
    property alias modelCategory: categoryComboBox.value

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{

        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            id: browseRow
            VizLabel{
                text: "Model"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: modelPath
                text: ""
                placeholderText: "Browse a model file..."
                readOnly: true
                Layout.fillWidth: true
                Layout.minimumWidth: 200
            }
            VizButton{
                id: browseButton
                text: " Browse "
                onClicked: browseButtonClicked()
            }
        }

        RowLayout{
            id: categoryRow
            z :1

            VizLabel{
                text: "Category"
                textAlignment: Qt.AlignLeft
            }
            VizComboBox{
                id: categoryComboBox
                Layout.fillWidth: true
                Layout.minimumWidth: 150
                listModel: typeof(landmarkCategoryList)== "undefined" ? "undefined" : landmarkCategoryList
                value: "Misc"
            }
        }
        RowLayout{
            id: commandButtonRect
            Layout.alignment: Qt.AlignRight
            VizButton{
                id: okButton
                text:"    Ok    "
                onClicked: {
                    if( modelFilePath == "" )
                    {
                        showGUIError("Insufficient data","Landmark name or model file path is not specified.");
                    }
                    else
                    {
                        addLandmarkType(modelFilePath)
                    }

                    closePopup()
                }
            }
            VizButton{
                id: cancelButton
                text:" Cancel "
                onClicked: {
                    closePopup()
                }
            }
        }
    }
}
