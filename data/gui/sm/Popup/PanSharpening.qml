import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "panSharpening"
    title: "Pan-Sharpening"

    Component.onCompleted: {
        if(!running_from_qml){
            PanSharpenGUI.setActive(true)
        }
    }

    onClosed: {
        if(!running_from_qml){
            PanSharpenGUI.setActive(false)
        }
    }
    Component.onDestruction:
    {
        if(!running_from_qml){
            PanSharpenGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal fuseImages(string panImage, string mSImage, string band1Image, string band2Image ,string band3Image)

    signal browsePanImage()
    signal browseMSImage()
    signal browseBand1Image()
    signal browseBand2Image()
    signal browseBand3Image()
    signal browseOutputImage()

    signal stopButtonClicked()
    signal showMSImage()
    signal viewImage(string imagePath)

    property alias panImagePath : panImage.text
    property alias msImagePath: msImage.text
    property alias band1ImagePath: band1.text
    property alias band2ImagePath: band2.text
    property alias band3ImagePath: band3.text
    property alias outputNamePath: outputName.text

    property alias startButtonLabel : fuseButton.text
    property alias stopButtonLabel : stopButton.text

    property alias startButtonEnabled: fuseButton.enabled
    property alias stopButtonEnabled: stopButton.enabled
    property alias panButtonEnabled : panOpenID.enabled
    property alias msButtonEnabled: msImageOpenID.enabled
    property alias band1ImageButtonEnabled: band1OpenID.enabled
    property alias band2ImageButtonEnabled: band2OpenID.enabled
    property alias band3ImageButtonEnabled: band3OpenID.enabled
    property alias msImageEnabled: msimageButton.enabled
    property alias outputButtonEnabled: outputOpenID.enabled


    property bool calculating: false

    property bool showAdvancedOptions: false

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{
            id:layerNameRow
            spacing: 5

            VizLabel{
                text: "Load Pan Image"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: panImage
                enabled: false
                Layout.minimumWidth: 200*rootStyle.uiScale
                text: ""
                validator:rootStyle.nameValidator
                placeholderText:"Browse Pan Image file..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browsePanImage()
                }
            }
            VizButton {
                id : panOpenID
                text : "View"
                onClicked:viewImage(panImagePath)
            }
        }

        RowLayout{
            id:msImageRow
            spacing: 5
            visible: !showAdvancedOptions
            VizLabel{
                text: "Load MS Image"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: msImage
                enabled: false
                text: ""
                placeholderText:"Browse MS Image file..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browseMSImage()
                }
            }
            VizButton {
                id : msImageOpenID
                text : "View"
                onClicked:viewImage(msImagePath)
            }
        }


        RowLayout{
            id:band1Image
            spacing: 5
            visible: showAdvancedOptions

            VizLabel{
                text: "Load band1 Image"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: band1
                enabled: false
                text: ""
                placeholderText:"Browse band1 Image file..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browseBand1Image()
                }
            }
            VizButton {
                id : band1OpenID
                text : "View"
                onClicked:viewImage(band1ImagePath)
            }
        }


        RowLayout{
            id:band2Image
            spacing: 5
            visible: showAdvancedOptions

            VizLabel{
                text: "Load band2 Image"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: band2
                enabled: false
                text: ""
                placeholderText:"Browse band2 Image file..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browseBand2Image()
                }
            }
            VizButton {
                id : band2OpenID
                text : "View"
                onClicked:viewImage(band2ImagePath)
            }
        }

        RowLayout{
            id:band3Image
            spacing: 5
            visible: showAdvancedOptions

            VizLabel{
                text: "Load band3 Image"
            }
            VizTextField{
                id: band3
                enabled: false
                text: ""
                placeholderText:"Browse band3 Image file..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browseBand3Image()
                }
            }
            VizButton {
                id : band3OpenID
                text : "View"
                onClicked:viewImage(band3ImagePath)
            }
        }

        RowLayout {
            id : advancedOptionsRect
            VizButton{
                id: advancedOptionsCheckBox
                enabled: !calculating
                text: showAdvancedOptions? "Hide Advanced Options" : "Show Advanced Options"
                onClicked: {
                    showAdvancedOptions = !showAdvancedOptions
                }
            }
        }

        RowLayout{
            id:opFileName
            VizLabel{
                id: opFileNameLabel
                text: "Output Name"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: outputName
                enabled: !calculating
                Layout.fillWidth: true
                text: ""
                placeholderText: "Enter Output Name"
                validator: rootStyle.nameValidator
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browseOutputImage()
                }
            }
            VizButton {
                id : outputOpenID
                text : "View"
                onClicked:viewImage(outputNamePath)
            }

        }

        RowLayout{
            id:commandButtonRect
            spacing: 10

            AnimatedImage{
                id:progressBarId
                Layout.fillWidth: true
                source: rootStyle.getBusyIcon()
                visible: calculating
            }

            Rectangle{
                Layout.fillWidth: true
            }
            VizButton{
                id:msimageButton
                visible: showAdvancedOptions
                text: "MS Image"
                onClicked: showMSImage()
            }

            VizButton{
                id:fuseButton
                text: "  Fuse Images "
                onClicked: fuseImages(panImagePath, msImagePath, band1ImagePath, band2ImagePath, band3ImagePath)
            }

            VizButton{
                id:stopButton
                text: "Stop"
                onClicked: stopButtonClicked()
            }
        }
    }
}

