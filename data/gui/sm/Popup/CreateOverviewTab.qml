import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: createOverviewPopUp
    objectName: "CreateOverviewPopUp"
    title: "Create Overview"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    clip: true

    signal okButtonClicked()
    property alias levelNumber: levelNumberText.text

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id:mainLayout
        anchors.margins: 5
        anchors.fill: parent
        RowLayout{
            VizLabel{
                text: "Number of levels"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id:levelNumberText
                text:""
                placeholderText: ""
            }
        }

        VizButton {
            Layout.alignment: Qt.AlignCenter
            text: "Ok"
            onClicked: {
                closePopup()
                okButtonClicked()
            }
        }
    }
}
