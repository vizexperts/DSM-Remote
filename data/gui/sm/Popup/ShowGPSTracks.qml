import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: showGPSTracks
    objectName: "ShowGPSTracks"
    title: "GPS Tracks"
    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal playSegmentTour(int index)

    property alias busyBarVisible: busyBar.visible

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizLabel{
            text: "Track List"
        }

        Rectangle{
            id: trackListTreeViewRow
            clip: false
            Layout.minimumHeight: 300
            Layout.minimumWidth: 450
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: "transparent"
            VizTreeView{
                id: trackListTreeView
                objectName: "trackListTreeView"
                anchors.fill: parent
                model:(typeof(trackListTreeModel)!== "undefined") ? trackListTreeModel : "undefined"
                VizTreeViewColumn{
                    role: "name"
                    delegate: VizLabel{
                        text: styleData.value
                        textColor: rootStyle.textColor
                    }
                }
                VizTreeViewColumn{
                    role: "type"
                    delegate: VizButton{
                        visible: styleData.value === "segment"
                        iconSource: rootStyle.getIconPath("play")
                        backGroundVisible: false
                        onClicked: {
                            playSegmentTour(styleData.row)
                        }
                    }
                }
            }
        }
        RowLayout{
            id: busyBar
            visible: false
            VizLabel{
                text: "Preparing Path..."
            }
            AnimatedImage{
                source: rootStyle.getBusyBar()
            }
        }
    }
}
