import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: rasterInfoId
    objectName: "RasterInfoObject"
    title: "Basic Information"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    // for displaying of list populated data
    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5
        VizTableView{
            id: informationTabId
            model: listInfoModel
            Layout.minimumWidth:  650
            Layout.minimumHeight: 300
            Layout.fillWidth: true
            Layout.fillHeight: true
            focus: true
            clip: true
            TableViewColumn {
                role: "name"
                title: "Name"
                elideMode: Text.ElideMiddle
            }
            TableViewColumn {
                role: "type"
                title: "Description"
                elideMode: Text.ElideMiddle
            }
        }
    }
}
