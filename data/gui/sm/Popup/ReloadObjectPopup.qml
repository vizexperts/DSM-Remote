import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{

    id: reloadObjectPopup
    objectName: "reloadObjectPopup"
    title: "Reload Object"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias previousDataSource: previousDataSourceTextField.text
    property alias newDataSource: newDataSourceTextField.text
    property string fileSuffix : ""

    //! This flag says that the layer comes from a query result, and can only be reloaded, not recpecified.
    property bool queryDatasource: false

    Component.onCompleted: {
        if(!running_from_qml){
            ElementListGUI.connectItemReloadPopup()
        }
    }

    states: State {
        name: "databaseDatasource"
        when: queryDatasource
        PropertyChanges { target: datasourceTextBoxGridLayout; visible: false }
        PropertyChanges { target: okCancelButtonRowLayout; visible: false }
        PropertyChanges { target: reloadDatabaseLayerRowLayout; visible: true }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        GridLayout{
            id: datasourceTextBoxGridLayout
            Layout.fillWidth: true
            columns: 3
            z:1

            VizLabel{
                text: "Previous Data Source"
            }

            VizTextField{
                Layout.fillWidth: true
                id: previousDataSourceTextField
                Layout.minimumWidth : 200
                readOnly: true
                text: ""
                placeholderText: ""
             }

            Item{
                width: 10
                height: 10
            }

            VizLabel{
                text: "New Data Source"
            }

            VizTextField{
                id: newDataSourceTextField
                Layout.minimumWidth : 200
                Layout.fillWidth: true
                text: ""
                placeholderText: ""
             }
            VizButton{
                text: "Browse"
                onClicked: ElementListGUI.browseNewDataSourceButtonClicked()
            }
        }
        RowLayout{
            id: okCancelButtonRowLayout
            Layout.fillWidth: true
            Rectangle{
                Layout.fillWidth  : true
            }
            VizButton{
                id: okButtonId
                text: "   OK   "
                onClicked: {
                    ElementListGUI.reloadCurrentObject();
                    closePopup()
                }
                Layout.alignment: Qt.AlignCenter
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: cancelButtonId
                text: " Cancel "
                onClicked: closePopup()
                Layout.alignment: Qt.AlignCenter

            }
            Rectangle{
                Layout.fillWidth: true
            }
        }
        RowLayout{
            id: reloadDatabaseLayerRowLayout
            Layout.fillWidth: true
            visible: false
            VizLabel{
                text: "Database Layer"
            }
            VizButton{
                text: "Reload"
                onClicked: {
                    ElementListGUI.reloadCurrentObject();
                    closePopup()
                }
            }
        }
    }
}
