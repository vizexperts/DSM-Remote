import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "almanacAnalysis"
    title: "Almanac Analysis"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Component.onCompleted:{

        if(!running_from_qml){
            AlmanacGUI.setActive(true)
        }

        dateAndTime = new Date()

        smpFileMenu.setDateTime(dateAndTime)
    }

    Component.onDestruction:{
        if(!running_from_qml){
            AlmanacGUI.setActive(false)
        }
        smpFileMenu.closeDatePicker()
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    signal markPosition(bool mark)
    signal timeZoneChanged(string uID)
    signal positionEdited()
    signal dateTimeChanged(date dateAndTime)

    // date/time format used
    property string dateFormat: " dd MMM yyyy ddd\thh : mm : ss"
    property date dateAndTime

    property alias moonPhase: moonPhase.text
    property alias moonPhaseImage: phaseImage.source
    property alias moonRiseTime: moonRiseTime.text
    property alias moonSetTime: moonSetTime.text
    property alias sunRiseTime: sunRiseTime.text
    property alias sunSetTime: sunSetTime.text
    property alias longitude: longitude.text
    property alias latitude: latitude.text

    property bool dateTimeFlickableVisible:false

    onDateTimeFlickableVisibleChanged: {
        if(dateTimeFlickableVisible ===true){
            smpFileMenu.setDateTime(dateAndTime)
            smpFileMenu.openDatePicker()
        }
        else{
            timeDateChanged()
            smpFileMenu.closeDatePicker()
        }
    }

    function timeDateChanged()
    {
        if(dateTimeFlickableVisible===true){
            var obj = smpFileMenu.getDatePicker()
            dateAndTime = new Date(obj.year, obj.month, obj.dayOfMonth, obj.hours, obj.minutes, obj.seconds)
            dateTimeChanged(dateAndTime)
        }
    }

    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5

        GridLayout{
            id:positionRow
            Layout.fillWidth: true
            columns: 4

            VizLabel{
                text: "Position       "
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id:latitude
                Layout.fillWidth: true
                placeholderText: "Latitude"
                text:""
                validator: rootStyle.latitudeValidator
                onTextEdited: positionEdited()
            }
            VizTextField{
                id:longitude
                Layout.fillWidth: true
                placeholderText: "Longitude"
                text:""
                validator: rootStyle.longitudeValidator
                onTextEdited: positionEdited()
            }
            VizButton{
                id: markButton
                text: "   Mark   "
                checkable: true
                onCheckedChanged: {
                    dateTimeFlickableVisible =false
                    markPosition(checked)
                }
            }
        }

        GridLayout{
            id: dateTimeRow
            Layout.fillWidth: true
            columns: 3

            VizLabel{
                text:"Date & Time"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                readOnly: true
                Layout.fillWidth: true
                placeholderText: ""
                Layout.minimumWidth: 320
                text: Qt.formatDateTime(dateAndTime, dateFormat)
            }
            VizButton{
                id:showDateAndTime
                iconSource: rootStyle.getIconPath("calendar")
                backGroundVisible: false
                onClicked: {
                    if(dateTimeFlickableVisible === false){
                        dateTimeFlickableVisible =true
                    }
                    else{
                        smpFileMenu.setDateTime(dateAndTime)
                        timeDateChanged()
                        dateTimeFlickableVisible =false
                    }
                    markButton.checked=false
                }
            }
        }

        GridLayout{
            id:timeZoneRow
            Layout.fillWidth: true
            z:1
            columns: 2

            VizLabel{
                text:"Time Zone   "
                textAlignment: Qt.AlignLeft
            }
            VizComboBox{
                id:timeZoneComboBox
                Layout.fillWidth: true
                listElementAlignment: Qt.AlignLeft
                listModel: (typeof(timeZonesList)=="undefined")? "undefined": timeZonesList
                onSelectOption:timeZoneChanged(uID)
            }
        }

        VizGroupBox{
            id:almanacGroupbox
            title: "Almanac"
            checkable: false

            width: layout.implicitWidth+10
            height: layout.implicitHeight + 50

            implicitHeight: layout.implicitHeight+50

            Layout.fillWidth: true
            ColumnLayout{
                id:layout
                anchors.fill: parent
                GridLayout{
                    id:sunRow
                    columns: 4
                    VizLabel{
                        text:"Sunrise      "
                        textAlignment:  Qt.AlignLeft
                    }
                    VizTextField{
                        id:sunRiseTime
                        Layout.fillWidth: true
                        readOnly: true
                        text: ""
                        placeholderText: ""
                    }
                    VizLabel{
                        text:"Sunset"
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id:sunSetTime
                        Layout.fillWidth: true
                        readOnly: true
                        text: ""
                        placeholderText: ""
                    }
                    VizLabel{
                        text:"Moonrise    "
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id:moonRiseTime
                        Layout.fillWidth: true
                        readOnly: true
                        text: ""
                        placeholderText: ""
                    }
                    VizLabel{
                        text:"Moonset"
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id:moonSetTime
                        Layout.fillWidth: true
                        readOnly: true
                        text: ""
                        placeholderText: ""
                    }
                }

                RowLayout{
                    id:moonPhaseRow

                    VizLabel{
                        text: "Moonphase"
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id: moonPhase
                        text:""
                        Layout.minimumWidth:moonSetTime.width
                        placeholderText: ""
                        readOnly: true
                    }
                    VizLabel{
                        text: "View"
                        textAlignment: Qt.AlignLeft
                        visible: phaseImage.visible
                    }

                    Image {
                        id: phaseImage
                        source: ""
                        height: 64*rootStyle.uiScale
                        width: 64*rootStyle.uiScale
                        visible: source==""? false : true
                    }
                }
            }
        }
    }
}
