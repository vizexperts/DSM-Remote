import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

/*
     \popup AddElevationLayerPopUp.qml
     \author Nitish Puri
     \brief Browse and Add Elevation file
 */

Popup{
    objectName: "addElevationLayerPopup"
    title: "Add New Elevation"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10


    signal addLayer(string name, string filePath)
    signal fileTypeCheckBoxChanged(string textActive)
    signal browseButtonClicked(bool checked)

    property alias selectedFileName: filePath.text
    property alias layerName: layerNameField.text
    property alias layerNameReadOnly: layerNameField.readOnly
    property alias coordSystem: coordSys.text

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent

        RowLayout{
            id:layerNameRow
            spacing: 5
            VizLabel{
                text: "Layer Name"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: layerNameField
                text: ""
                placeholderText: "Give a name to this layer..."
                validator:rootStyle.nameValidator
                Layout.minimumWidth: 250
                Layout.fillWidth: true
            }
        }

        RowLayout{
            id:filePathRow
            spacing: 5
            VizLabel{
                text: "Path"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: filePath
                Layout.minimumWidth: 250
                text: ""
                placeholderText:checkBox.checked? "Browse directory..." : "Browse files..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                onClicked: {
                    browseButtonClicked(checkBox.checked)
                }
            }
        }

        RowLayout{
            spacing: 5
            Item{
                Layout.fillWidth: true
            }

            VizCheckBox{
                id: checkBox
                text: "Add whole directory"
                checked: false
            }
        }
        RowLayout{
            id:commandButtonRect
            spacing: 10

            Rectangle{
                width: parent.width - okButton.width - cancelButton.width -20
                Layout.fillWidth: true
            }

            VizLabel{
                id: coordSys
                text: ""
                anchors.left: parent.left
                anchors.leftMargin: 20
            }

            VizButton{
                id:okButton
                text: "   Ok   "
                width: 100
                onClicked:addLayer(layerName, selectedFileName)
            }
         
            VizButton{
                id:cancelButton
                text: "Cancel"
                width: 100
                onClicked: closePopup()
            }
        }
    }
}

