import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: sensorRangeAnalysisId

    objectName: "SensorRangeAnalysisObject"
    title: "Sensor range analysis"

    Component.onCompleted: {
        if(!running_from_qml){
            SensorRangeAnalysisGUI.setActive(true)
        }

        smpFileMenu.setColor(rectColor)
    }

    onClosed: {
        if(!running_from_qml){
            SensorRangeAnalysisGUI.setActive(false)
        }
    }
    Component.onDestruction:
    {
        if(!running_from_qml){
            SensorRangeAnalysisGUI.setActive(false)
        }
        smpFileMenu.closeColorPicker()
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //signals for UI
    signal qmlHandleAddButtonClicked()
    signal qmlHandleRemoveButtonClicked(int currentIndex)
    signal qmlHandleStartButtonClicked()
    signal qmlHandleStopButtonClicked()
    signal qmlHandleMarkButtonClicked(bool pressed)


    property alias leLat: leLatId.text
    property alias leLong: leLongId.text
    property alias mapsheet: mapsheetTextField.text
    property alias gr: grTextField.text
    property alias leHeight: leHeightId.text
    property alias leRange: leRangeId.text
    property alias leName: leNameId.text
    property alias rectColor : colorRectangle.backgroundColor
    property alias startButtonLabel : pbStartButtonId.text
    //property alias stopButtonLabel : pbStopButtonId.text
    property alias startButtonEnabled: pbStartButtonId.enabled
    //property alias stopButtonEnabled: pbStopButtonId.enabled
    property bool calculating: false

    property string selectedRect

    property bool colorPickerVisible:false

    onColorPickerVisibleChanged: {
        if(colorPickerVisible === true){
            smpFileMenu.openColorPicker()
        }
        else{
            smpFileMenu.closeColorPicker()
        }
    }

    function colorChanged(){
        var fontObj = smpFileMenu.getColorPicker();

        if(selectedRect === "defaultRect"){
            rectColor = fontObj.currentColor
        }
        else{
            colorListModel[colorListView.currentIndex].cellColor = fontObj.currentColor
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizGroupBox {
            id:observerId
            title: "Add Observers"

            ColumnLayout{
                id:contentLayout
                anchors.fill: parent
                RowLayout{
                    id: positionRow
                    VizLabel {
                        id: labelPositionId
                        text: "Position"
                        textAlignment: Qt.AlignLeft
                    }

                    VizTextField {
                        id: leLatId
                        Layout.fillWidth: true
                        placeholderText: "Latitude"
                        validator: rootStyle.latitudeValidator
                    }
                    VizTextField {
                        id: leLongId
                        Layout.fillWidth: true
                        placeholderText: "Longitude"
                        validator: rootStyle.longitudeValidator
                    }

                    VizButton {
                        id:pbMarkPositionId
                        text:"   Mark   "
                        checkable: true
                        onClicked: qmlHandleMarkButtonClicked(checked)

                    }
                }

                RowLayout{
                    id: mapsheetRow
                    VizLabel {
                        id: labelReferenceId
                        text: "Mapsheet / GR"
                        textAlignment: Qt.AlignLeft
                    }

                    VizTextField {
                        id: mapsheetTextField
                        Layout.fillWidth: true
                        placeholderText: "Mapsheet Number"
                    }
                    VizTextField {
                        id: grTextField
                        Layout.fillWidth: true
                        placeholderText: "Grid Reference"
                    }
                }

                GridLayout{
                    Layout.fillWidth: true
                    columns: 4

                    VizLabel {
                        id:labelHeightId
                        text: "Height (m)"
                        textAlignment: Qt.AlignLeft
                    }

                    VizTextField{
                        id: leHeightId
                        Layout.fillWidth: true
                        placeholderText: "Enter height in meters"
                        validator: DoubleValidator{
                            bottom: 0;
                            top:9999
                            decimals: 2;
                            notation: DoubleValidator.StandardNotation;
                        }
                    }

                    VizLabel {
                        id:labelRangeId
                        text: "Range (m)"
                        textAlignment: Qt.AlignLeft
                    }

                    VizTextField{
                        id: leRangeId
                        Layout.fillWidth: true
                        placeholderText: "Enter range in meters"
                        validator: DoubleValidator{
                            bottom: 0;
                            top:99999
                            decimals: 2;
                            notation: DoubleValidator.StandardNotation;
                        }
                    }

                    VizLabel {
                        id:labelNameId
                        text: "Name"
                        textAlignment: Qt.AlignLeft
                    }

                    VizTextField{
                        id: leNameId
                        Layout.fillWidth: true
                        placeholderText: "Enter name ";
                        validator: rootStyle.nameValidator;
                    }

                    VizLabel {
                        id:labelColorId
                        text: "Color"
                    }

                    VizButton{
                        id: colorRectangle
                        flatButton: true
                        onClicked : {
                            if(colorPickerVisible == false){
                                smpFileMenu.setColor(rectColor)
                                colorPickerVisible =true
                                selectedRect = "defaultRect"
                            }
                            else{
                                colorChanged()
                                colorPickerVisible =false
                            }
                        }
                    }
                }

                RowLayout {
                    id: observerButtonId ;
                    spacing: 20;
                    Layout.fillWidth: true

                    Rectangle{
                        Layout.fillWidth: true
                    }

                    VizButton {
                        id: pbAddButtonId
                        text: "    Add    "
                        onClicked: {
                            qmlHandleAddButtonClicked();
                        }
                    }
                    VizButton {
                        id: pbRemoveButtonId;
                        text: "   Remove   "
                        onClicked: {
                            qmlHandleRemoveButtonClicked(twObserverListId.currentIndex);
                        }
                    }
                }

                VizTableView {
                    id: twObserverListId;
                    model:(typeof(observerModel) !== "undefined") ? observerModel: "undefined"

                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    focus: true;
                    clip: true;
                    onCurrentRowChanged: {
                        if(currentRow >= 0)
                        {
                            if(typeof(observerModel) !== "undefined"){
                                leName  = observerModel[currentRow].name
                                leLong  = observerModel[currentRow].longi
                                leLat = observerModel[currentRow].lat
                                leHeight = observerModel[currentRow].height
                                leRange = observerModel[currentRow].range
                                rectColor = observerModel[currentRow].color
                            }
                        }
                    }

                    TableViewColumn{
                        role: "name";
                        title: "Name";
                    }
                    TableViewColumn{
                        role: "longi";
                        title: "Longitude";
                    }
                    TableViewColumn{
                        role: "lat";
                        title: "Latitude"
                    }
                    TableViewColumn{
                        role: "height";
                        title: "Height";
                    }
                    TableViewColumn{
                        role: "range";
                        title: "Range";
                    }
                    TableViewColumn{
                        role: "color";
                        title: "Color";
                        delegate: VizButton{
                            backgroundColor: modelData.color
                            flatButton: true
                        }
                    }
                }
            }
        }
        RowLayout {
            id: controlButtonId

            AnimatedImage{
                id:progressBarId
                Layout.fillWidth: true
                source: rootStyle.getBusyIcon()
                visible: calculating
            }
            Rectangle{
                Layout.fillWidth: true
            }
            VizButton {
                id: pbStartButtonId
                text: "   Calculate   "
                onClicked: {
                    qmlHandleStartButtonClicked()
                }
            }
            /*VizButton {
                id: pbStopButtonId;
                text: "    Stop    "
                onClicked: {
                    qmlHandleStopButtonClicked()
                }
            }*/
        }
    }
}
