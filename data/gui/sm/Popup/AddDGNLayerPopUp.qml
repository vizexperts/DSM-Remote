import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"
import ".."

/*
     \popup AddDGNLayerPopUp.qml
     \author Nitish Puri
     \brief Browse and Add DGN file
 */

Popup{
    objectName: "addDGNLayerPopup"
    title: "Add DGN"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal addLayer(string name, string filePath, string orclFilePath, bool extentsSpecified, bool reloadCache)
    signal browseButtonClicked()
    signal orclBrowseButtonClicked()
    signal mapsheetnumberchanged(string mapSheetNumber)

    property alias selectedFileName: filePath.text
    property alias orclSelectedFileName: orclFilePath.text
    property alias layerName: layerNameField.text
    property alias latitude: latitude.text
    property alias longitude: longitude.text
    property alias mapSheetNumber: mapSheetNumber.text
    property alias layerNameReadOnly: layerNameField.readOnly

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }
    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            id:layerNameRow
            spacing: 5

            VizLabel{
                text: "Layer Name"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: layerNameField
                text: ""
                validator:rootStyle.nameValidator
                placeholderText: "Give a name to this layer..."
                Layout.fillWidth: true
            }
        }

        RowLayout{
            id:filePathRow
            spacing: 5
            VizLabel{
                text: "Path"
                 textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: filePath
                text: ""
                Layout.minimumWidth: 250
                placeholderText:"Browse files..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                onClicked: browseButtonClicked()
            }
        }
        RowLayout{
            id:orclPathRow
            spacing: 5
            //Oracle Dump File Functionality not implemented
            //So making it invisibile till the we implement it
            visible: false
            VizLabel{
                text :"Oracle File"
                 textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: orclFilePath
                text: ""
                Layout.minimumWidth: 250
                placeholderText: "Browse oracle dump file..."
                Layout.fillWidth: true
            }
            VizButton{
                text: "Browse"
                onClicked: orclBrowseButtonClicked()
            }
        }

        VizGroupBox{
            id:mapSheetGroupBox
            width:parent.width
            title: "Map Sheet"
            checkable: true
            checked: true
            Layout.fillWidth: true
            onClicked: {
                if(checked==true) {
                    geoExtentsGroupBox.checked= false
                }
                else {
                    checked= true
                }
            }

            RowLayout{
                spacing: 5
                width: parent.width- 10
                anchors.horizontalCenter: parent.horizontalCenter

                VizLabel{ text: "Map sheet Number" }
                VizTextField{
                    id:mapSheetNumber
                    Layout.minimumWidth:  200
                    placeholderText: "Enter Map sheet Number..."
                    text: ""
                    Layout.fillWidth: true
                    onTextChange:  mapsheetnumberchanged(mapSheetNumber.text)
                }
            }
        }
        VizGroupBox{
            id:geoExtentsGroupBox
            width:parent.width
            title: "Geographical Extents"
            checkable: true
            checked: false
            Layout.fillWidth: true
            onClicked: {
                if(checked==true) {
                    mapSheetGroupBox.checked= false
                }
                else {
                    checked = true
                }
            }
            RowLayout{
                spacing: 5
                width: parent.width-10
                anchors.horizontalCenter: parent.horizontalCenter

                VizLabel{ text: "Map center" }
                VizTextField{
                    id:latitude
                    placeholderText: "Latitude.."
                    text: ""
                    Layout.fillWidth: true
                    validator: rootStyle.latitudeValidator
                }
                VizTextField{
                    id:longitude
                    placeholderText: "Longitude.."
                    text: ""
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }
            }
        }

        RowLayout{
            Layout.fillWidth: true
            width: parent.width

            Item{
                Layout.minimumWidth: 100
                Layout.fillWidth: true
            }
            VizCheckBox{
                id: reloadCacheCheckBox
                text: "Reload Cache"
                checked: false
            }

        }

        RowLayout{
            id:commandButtonRect
            spacing: 10

            Item{
                Layout.fillWidth: true
            }

            VizButton{
                id:okButton
                text: "   Ok   "
                width: 100
                onClicked: {
                    addLayer(layerName, selectedFileName,orclSelectedFileName, geoExtentsGroupBox.checked, reloadCacheCheckBox.checked)
                }
            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                width: 100
                onClicked: {
                    closePopup()
                }
            }
        }
    }
}
