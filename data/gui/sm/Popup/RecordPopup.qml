import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: recordPopup
    objectName: "recordMenu"
    title: "Record"
    Component.onDestruction: {
        //flag set false
        smpFileMenu.isPopUpAlreadyOpen=false;
    }
    height: mainLayout.implicitHeight+2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+2*mainLayout.anchors.margins

    minimumHeight: mainLayout.Layout.minimumHeight+10
    minimumWidth: mainLayout.Layout.minimumWidth+10


    transformOrigin: Item.Center
    clip: true

    signal browseButtonClicked()
    signal startCaptureClicked(string filename)
    signal stopCaptureClicked()
    signal changeResolution(string selectedResolution)

    property alias selectedFile: filePath.text
    property alias isStartButtonEnabled: startCaptureButton.enabled

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ListModel{
        id: resolutionId
        ListElement{name:"1920 x 1080"; uID : "0"}
        ListElement{name:"1680 x 1050"; uID : "1"}
        ListElement{name:"1600 x 1024"; uID : "2"}
        ListElement{name:"1600 x 900"; uID : "3"}
        ListElement{name:"1366 x 768"; uID : "4"}
        ListElement{name:"1360 x 768"; uID : "5"}
        ListElement{name:"1280 x 1024"; uID : "6"}
        ListElement{name:"1280 x 960"; uID : "7"}
        ListElement{name:"1280 x 800"; uID : "8"}
        ListElement{name:"1280 x 768"; uID : "9"}
        ListElement{name:"1280 x 720"; uID : "10"}
        ListElement{name:"1152 x 864"; uID : "11"}
        ListElement{name:"1024 x 768"; uID : "12"}
        ListElement{name:"800 x 600"; uID : "13"}
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            VizLabel{
                text: "Save as"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: filePath
                Layout.minimumWidth: 250
                text: ""
                readOnly: true
                placeholderText: "Browse file..."
                Layout.fillWidth: true
            }

            VizButton{
                text:"Browse"
                onClicked: {
                    browseButtonClicked()
                }
            }
        }

        VizGroupBox{
            title: "Resolution"
            Layout.fillWidth: true
            Layout.fillHeight: true
            RowLayout{
                id:resolutionSpecificationRow
                Layout.alignment: Qt.AlignCenter

                VizLabel{
                    text: "Width"
                    textAlignment: Qt.AlignLeft
                }

                VizComboBox {
                    id: losId
                    listModel: resolutionId
                    z:1
                    value: resolutionId.get(selectedUID).name
                    Layout.minimumWidth: 150
                    Layout.fillWidth: true
                    onSelectedUIDChanged:{
                        changeResolution(resolutionId.get(selectedUID).name)
                    }
                }

            }
        }

        RowLayout{
            Layout.alignment: Qt.AlignCenter
            VizButton{
                id: startCaptureButton
                text:"Start Capture"
                onClicked: {
                    startCaptureClicked(selectedFile)
                }
            }
            VizButton{
                text:"Stop and Write"
                onClicked: {
                    stopCaptureClicked()
                    closePopup()
                }
            }
        }
    }
}
