import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: queryBuilder
    objectName: "QueryBuilderGIS"
    title: "Query Builder"

    height: mainLayout.implicitHeight+2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+2*mainLayout.anchors.margins

    minimumHeight: mainLayout.Layout.minimumHeight+10
    minimumWidth: mainLayout.Layout.minimumWidth+10

    //properties
    property alias bottomLeftLongitude : bottomLeftLongitude.text
    property alias bottomLeftLatitude : bottomLeftLatitude.text
    property alias topRightLongitude : topRightLongitude.text
    property alias topRightLatitude : topRightLatitude.text
    property alias isMarkSelected : markButton.checked

    property string userspaceName
    property string tableName
    property string uiType
    property string incidentName

    property alias sqlWhereText : sqlWhereTextId.text

    //Signal defined for buttons
    signal addToWhere(int index)
    signal markPosition(bool checked)
    signal operatorButtonClicked(string operator)
    signal queryButtonClicked()
    signal clearButtonClicked()
    signal cancelButtonClicked()
    signal okButtonClicked()
    signal addValueToWhere(int index)
    signal changeText(string all)
    signal showAllValues(int index)
    signal showSampleValues(int index)

    onTableNameChanged:{
        populateFieldDefinitions()
    }

    signal populateFieldDefinitions()

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            Layout.alignment: Qt.AlignCenter
            spacing: 5
            ColumnLayout{
                id: allFieldsColumnId

                VizLabel{
                    text: "All Fields"
                }

                Rectangle{
                    color: "transparent"
                    border.width: 2
                    border.color: rootStyle.colorWhenSelected
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 150*rootStyle.uiScale
                    Layout.minimumHeight: 150*rootStyle.uiScale
                    ListView{
                        id: allFieldsListView
                        currentIndex: -1
                        anchors.fill: parent
                        model: (typeof(allFieldsList) != "undefined") ? allFieldsList : "undefined"
                        spacing: 4
                        VizScrollBar{ target: allFieldsListView }
                        clip: true
                        Keys.onUpPressed: if(currentIndex > 0) currentIndex--
                        Keys.onDownPressed: if(currentIndex < count) currentIndex++
                        focus: true
                        delegate: VizLabel{
                            id: label
                            text: modelData
                            textAlignment: Qt.AlignLeft
                            width: allFieldsListView.width
                            Behavior on color {
                                ColorAnimation { duration: 200 }
                            }
                            Behavior on textColor {
                                ColorAnimation { duration: 200 }
                            }

                            textColor: (allFieldsListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                            color: (allFieldsListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                            MouseArea{
                                anchors.fill: parent
                                onClicked: allFieldsListView.currentIndex = index
                                onDoubleClicked: addToWhere(allFieldsListView.currentIndex)
                            }
                        }
                    }
                }
            }
            ColumnLayout{
                id: distinctValuesColumnId

                VizTextField{
                    id: values
                    anchors.right: parent.right
                    placeholderText: "Values"
                    Layout.fillWidth: true
                    Keys.onEscapePressed: {
                        setFocus = false
                        text = ""
                    }
                    onTextChange:
                    {
                        changeText(values.text)
                        showAllValues(allFieldsListView.currentIndex)
                    }
                }
                Rectangle{
                    color: "transparent"
                    border.width: 2
                    border.color: rootStyle.colorWhenSelected
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 150*rootStyle.uiScale
                    Layout.minimumHeight: 150*rootStyle.uiScale
                    ListView{
                        id: distinctValuesListView
                        currentIndex: -1
                        anchors.fill: parent
                        clip: true
                        model: (typeof(distinctValuesList) != "undefined") ? distinctValuesList : "undefined"
                        spacing: 4
                        Keys.onUpPressed: if(currentIndex > 0) currentIndex--
                        Keys.onDownPressed: if(currentIndex < count) currentIndex++
                        focus: true
                        VizScrollBar{
                            target: distinctValuesListView
                        }

                        delegate: VizLabel{
                            text: modelData
                            textAlignment: Qt.AlignLeft
                            width: distinctValuesListView.width
                            Behavior on color {
                                ColorAnimation { duration: 200 }
                            }
                            Behavior on textColor {
                                ColorAnimation { duration: 200 }
                            }

                            textColor: (distinctValuesListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                            color: (distinctValuesListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                            MouseArea{
                                anchors.fill: parent
                                onClicked:  distinctValuesListView.currentIndex = index
                                onDoubleClicked: addValueToWhere(distinctValuesListView.currentIndex)
                            }
                        }
                    }
                }

                RowLayout{
                    spacing: 5
                    Layout.alignment: Qt.AlignCenter
                    VizButton{
                        text: "Sample"
                        onClicked: showSampleValues(allFieldsListView.currentIndex)
                    }
                    VizButton{
                        text: " All "
                        onClicked: showAllValues(allFieldsListView.currentIndex)
                    }
                }
            }
        }

        VizGroupBox{
            id : areaGroupBox
            Layout.fillWidth: true
            Layout.fillHeight: true
            title: "Extent"

            GridLayout{
                id: areaGrid
                columns: 3
                VizLabel{
                    text:"Bottom-Left"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: bottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "bottom"
                    validator: rootStyle.longitudeValidator
                }
                VizTextField{
                    id: bottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "left"
                    validator:rootStyle.latitudeValidator
                }
                VizLabel{
                    text:"Top-Right"
                    textAlignment: Qt.AlignLeft
                }

                VizTextField{
                    id: topRightLongitude
                    text: ""
                    placeholderText: "top"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }

                VizTextField{
                    id: topRightLatitude
                    text: ""
                    placeholderText: "right"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                VizButton{
                    id: markButton
                    objectName: "markArea"
                    text:"    Mark    "
                    backGroundVisible: true
                    checkable : true
                    Layout.alignment: Qt.AlignRight
                    onClicked: markPosition(checked)
                }
            }
        }

        VizGroupBox{
            id: operatorsId
            title: "Operators"
            Layout.fillWidth: true
            Layout.fillHeight: true

            GridLayout{
                id: operatorsGridId
                columns: 8

                VizButton{
                    text: "     =     "
                    onClicked: operatorButtonClicked("= ");
                }
                VizButton{
                    text: "     <     "
                    onClicked: operatorButtonClicked("< ");
                }
                VizButton{
                    text: "     >     "
                    onClicked: operatorButtonClicked("> ");
                }
                VizButton{
                    text: "   LIKE   "
                    onClicked: operatorButtonClicked("LIKE ");
                }
                VizButton{
                    text: "     %      "
                    onClicked: operatorButtonClicked("% ");
                }
                VizButton{
                    text: "     IN    "
                    onClicked: operatorButtonClicked("IN ");
                }
                VizButton{
                    text: " NOT IN  "
                    onClicked: operatorButtonClicked("NOT IN ");
                }
                VizButton{
                    text: "    <=    "
                    onClicked: operatorButtonClicked("<= ");
                }
                VizButton{
                    text: "    >=    "
                    onClicked: operatorButtonClicked(">= ");
                }
                VizButton{
                    text: "     !=    "
                    onClicked: operatorButtonClicked("!= ");
                }
                VizButton{
                    text: "   ILIKE  "
                    onClicked: operatorButtonClicked("ILIKE ");
                }
                VizButton{
                    text: "     AND   "
                    onClicked: operatorButtonClicked("AND ");
                }
                VizButton{
                    text: "    OR   "
                    onClicked: operatorButtonClicked("OR ");
                }
                VizButton{
                    text: "   NOT    "
                    onClicked: operatorButtonClicked("NOT ");
                }
                VizButton{
                    text: "    (    "
                    onClicked: operatorButtonClicked("( ");
                }
                VizButton{
                    text: "    )    "
                    onClicked: operatorButtonClicked(") ");
                }
            }
        }

        VizGroupBox{
            id: sqlWhereGroupId
            title: "SQL Where Clause"
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout{
                anchors.fill: parent
                VizTextEdit{
                    id: sqlWhereTextId
                    text: ""
                    //anchors.fill: parent
                    z:155
                    anchors.margins: 10
                    Layout.minimumHeight: 100
                    Layout.fillWidth: true
                    onTextChanged: cursorPosition = sqlWhereTextId.text.length
                }
            }
        }

        RowLayout{
            id: controlCommandId
            spacing: 5
            Layout.alignment: Qt.AlignRight

            VizButton{
                text: "    Clear    "
                onClicked: clearButtonClicked()
            }
            VizButton{
                text: "  Cancel  "
                onClicked:{
                    cancelButtonClicked()
                    closePopup()
                }
            }
            VizButton{
                text: "     OK     "
                onClicked:{
                    okButtonClicked()
                    closePopup()
                }
            }
        }
    }
}
