import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."
Popup {
    id : opacitySliderId
    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins
    title:"Opacity Slider"
    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

//    height: 0   // intially the height is set to zero for the animation. Set the height to 400 to disable animation
//    width : 100*rootStyle.uiScale
    objectName: "opacitySliderObject"

    Component.onCompleted: {
        CreateOpacitySliderGUI.setActive(true);
        //menuHolder.loaded("OpacitySliderTab")
        //opacitySliderId.height = 400*rootStyle.uiScale    // for the animation
    }

    Component.onDestruction: {
        CreateOpacitySliderGUI.setActive(false);
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    signal changeSliderValue(double value)
    signal okButtonChangeSliderValue(double value)

    // alias used to set the text in the slider
    property alias sliderValue: opacitySliderObjectId.value

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        Item {
            height: 15
        }
        RowLayout{
            Layout.fillWidth: true
            z:1
            VizSlider{
                id: opacitySliderObjectId
                Layout.fillWidth: true
                labelVisible: true
                anchors.horizontalCenter: parent.horizontalCenter
                orientation: Qt.Horizontal
                minimumValue: 0;
                maximumValue: 100;
                labelText: parseInt(value, 10);
                value : 100
                labelMovable:true
                onValueChanged: {
                    opacitySliderId.changeSliderValue( value/100 )
                }
            }

        }
        RowLayout{
            Layout.fillWidth: true
            Rectangle{
                Layout.fillWidth  : true
            }
            VizButton{
                id: okButtonId
                text: "   OK   "
                onClicked: {
                     //normalize
                    okButtonChangeSliderValue( opacitySliderObjectId.value/100 )
                }
                Layout.alignment: Qt.AlignCenter
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: cancelButtonId
                text: " Cancel "
                onClicked: {
                    //timelineLoader.minimized = true
                    //menuHolder.minimized = true
                    closePopup()
                }
                Layout.alignment: Qt.AlignCenter

            }
            Rectangle{
                Layout.fillWidth: true
            }
        }
    }
}

    // back icon present at the bottom of the slider
    // if pressed it loads the main menu
//    VizButton {
//        id : closeIcon
//        iconSource: rootStyle.getIconPath("close")
//        backGroundVisible: false
//        anchors.right: parent.right
//        anchors.top : parent.top
//        onClicked: {
//            timelineLoader.minimized = true
//            menuHolder.minimized = true
//        }
//    }

