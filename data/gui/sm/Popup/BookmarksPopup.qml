import QtQuick 2.1
import "../VizComponents"
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import ".."


Popup{
    id: bookmarks
    title: "Bookmarks"
    objectName: "smpBookmarksMenu"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 20
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal addNewBookmark()
    signal deleteBookmark()
    signal editBookmark()

    Component.onCompleted: {
        if(!running_from_qml){
            AddBookmarksGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            AddBookmarksGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    RowLayout {
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        Rectangle{
            id: modelsRec
            Layout.minimumWidth: 300
            Layout.minimumHeight: 480
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: "transparent"
            border.color: rootStyle.colorWhenSelected


            VizTreeView{
                id: bookmarkDirTreeView
                objectName: "bookmarkDirTreeView"
                anchors.fill: parent
                anchors.topMargin: 10
                Layout.fillWidth: true
                Layout.fillHeight: true
                model:(typeof(bookmarkListModel) !== "undefined") ? bookmarkListModel : "undefined"
                VizTreeViewColumn{
                    role: "isDir"
                    delegate: Image {
                        source: styleData.value ? rootStyle.getIconPath("folder") : rootStyle.getIconPath("model")
                        width: rootStyle.buttonIconSize
                        height: width
                    }
                }

                VizTreeViewColumn{
                    role: "name"
                    delegate: VizLabel {
                        id: nameDelegate
                        text: styleData.value
                        textColor: styleData.textColor
                    }
                }
            }
        }
    }


    VizButton{
        id:addBookmarkButton
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        iconSource: rootStyle.getIconPath("add")
        onClicked: {
            addNewBookmark()
        }
    }

    VizButton{
        id:deleteBookmarkButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        iconSource: rootStyle.getIconPath("delete")
        onClicked: {
            deleteBookmark()
        }
    }

    VizButton{
        id:editBookmarkButton
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        iconSource: rootStyle.getIconPath("edit")
        onClicked: {
            editBookmark()
        }
    }

}
