import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: addFolderPopup
    objectName: "addFolderPopup"

    title: "Add Folder Popup"
    signal addChildFolder(string name)

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias itemName: renameFieldId.text

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            Layout.fillWidth: true
            z:1

            VizLabel{
                text: "Folder Name"
            }

            VizTextField{
                id: renameFieldId
                Layout.minimumWidth : 200
                Layout.fillWidth: true
                text: "New Group"
                placeholderText: ""
             }
        }
        RowLayout{
            Layout.fillWidth: true
            Rectangle{
                Layout.fillWidth  : true
            }
            VizButton{
                id: okButtonId
                text: "   OK   "
                onClicked: {
                    ElementListGUI.createChildFolder(itemName);
                }
                Layout.alignment: Qt.AlignCenter
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: cancelButtonId
                text: " Cancel "
                onClicked: closePopup()
                Layout.alignment: Qt.AlignCenter

            }
            Rectangle{
                Layout.fillWidth: true
            }
        }
    }
}

