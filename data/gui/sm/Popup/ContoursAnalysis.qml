import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: contoursAnalysisid
    objectName: "ContoursAnalysis"
    title: "Contours Analysis"
    //clip : true

    Component.onCompleted: {
        if(!running_from_qml){
            ContoursGenerationGUI.setActive(true)
        }
    }
    Component.onDestruction:  {
        if(!running_from_qml){
            ContoursGenerationGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //properties
    property alias outputName : outputName.text
    property alias elevationInterval : elevationIntervalId.text
    property alias offSetValue : offsetId.text

    //property alias rectColor : colorRectangle.backgroundColor

    property alias bottomLeftLongitude : bottomLeftLongitude.text
    property alias bottomLeftLatitude : bottomLeftLatitude.text
    property alias topRightLongitude : topRightLongitude.text
    property alias topRightLatitude : topRightLatitude.text

    //states of buttons
    property alias isStartButtonEnabled : startButton.enabled
    //property alias isStopButtonEnabled: stopButton.enabled
    property alias isMarkSelected : markButton.checked
    property alias isProgressBarVisible : pb2.visible
    property alias progressValue: pb2.value
    property string selectedRect

    //signal generated by buttons
    signal changeAnalysisAspect(string aspect)
    signal startAnalysis()
    signal stopAnalysis()
    signal markPosition(bool value)
    signal getprogressBar()
    signal setStyle(string value)

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizGroupBox{
            title: "Analysis parameters"
            z:5

            GridLayout{
                id: parameterGrid
                columns: 2
                anchors.fill: parent
                VizLabel{
                    text:"Layer name"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: outputName
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "Layer name"
                    validator: rootStyle.nameValidator
                }
                VizLabel{
                    text:"Elevation interval"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: elevationIntervalId
                    text: ""
                    Layout.fillWidth: true
                    validator:  DoubleValidator { bottom : 0;notation: DoubleValidator.StandardNotation; }
                    placeholderText: "Enter elevation in meters"
                }
                VizLabel{
                    text:"Offset "
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: offsetId
                    text: ""
                    Layout.fillWidth: true
                    validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation; }
                    placeholderText: "Enter offset in meters"
                }

                VizLabel{
                    id: styleType
                    text: "Style "
                    textAlignment: Qt.AlignLeft
                }

                VizComboBox{
                    id: styleTypeBox
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    listModel: typeof(styleList)== "undefined" ? "undefined" : styleList
                    value: styleTypeBox.get(selectedUID).name

                    onSelectOption: {
                        setStyle(value)
                    }
                }
            }
        }
        VizGroupBox{
            id : areaGroupBox
            title: "Mark area"
            GridLayout{
                id: areaGrid
                columns: 3
                anchors.fill: parent
                VizLabel{
                    text:"Bottom left          "
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: bottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "Latitude"
                    validator:rootStyle.latitudeValidator
                }
                VizTextField{
                    id: bottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "Longitude"
                    validator: rootStyle.longitudeValidator
                }
                VizLabel{
                    text:"Top right            "
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: topRightLatitude
                    text: ""
                    placeholderText: "Latitude"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                VizTextField{
                    id: topRightLongitude
                    text: ""
                    placeholderText: "Longitude"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                VizButton{
                    id: markButton
                    objectName: "markArea"
                    text:"    Mark    "
                    backGroundVisible: true
                    checkable : true
                    Layout.alignment: Qt.AlignRight
                    onClicked: markPosition(checked)
                }
            }
        }

        RowLayout{
            id:commandRect
            Layout.fillWidth: true
            VizProgressBar{
                id : pb2
                value: 0
                minimum: 0
                maximum: 100
                indeterminate: false
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                visible: false
                onValueChanged:{
                    if( value == 100)
                    {
                        startButton.enabled = true
                        //stopButton.enabled = false
                    }
                }
            }
            Item{
                Layout.fillWidth: true
            }

            VizButton{
                id: startButton
                objectName: "startButton"
                text: "   Calculate   "
                onClicked:{
                    if((outputName.text=="")||(elevationInterval=="")||(offSetValue=="")||(bottomLeftLongitude.text=="")||(bottomLeftLatitude.text=="")||(topRightLongitude.text=="")||(topRightLatitude.text==""))
                    {
                        startAnalysis()
                        startButton.enabled = true
                    }
                    else
                    {

                        //stopButton.enabled = true
                        startButton.enabled = false
                        pb2.visible = true
                        startAnalysis()
                    }
                }
            }
            /*VizButton{
                id : stopButton
                text: "   Cancel   "
                enabled: false
                onClicked:{
                    stopAnalysis()
                    stopButton.enabled = false
                    startButton.enabled = true

                }
            }*/
        }
    }
}
