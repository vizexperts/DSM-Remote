import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: viewshedAnalysisId

    objectName: "ViewshedAnalysisObject"
    title: "Viewshed Analysis"

    Component.onCompleted: {
        if(!running_from_qml){
            ViewshedAnalysisGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            ViewshedAnalysisGUI.setActive(false)
        }
        smpFileMenu.closeColorPicker()
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +150

    //signals for UI
    signal qmlHandleAddButtonClicked()
    signal qmlHandleRemoveButtonClicked(int currentIndex)
    signal qmlHandleStartButtonClicked()
    signal qmlHandleStopButtonClicked()
    signal qmlHandleMarkButtonClicked(bool pressed)
    signal qmlHandlePointButtonClicked(bool pressed)
    signal qmlBrowseButtonClicked()
    signal qmlBuidingBrowseButtonClicked()
    signal qmlSaveTifPathButton()
    signal qmlnameChanged(string columnName)
    signal qmlheightChanged(string columnHeight)
    signal qmlrangeChanged(string columnWidth)
    signal qmlfixedfov(bool checked)


    signal qmllookatLatChanged(string columnlookatLat)
    signal qmllookatLongChanged(string columnlookatLong)
    signal qmllookatEleChanged(string columnlookatEle)
    signal qmlverticalFOVChanged(string columnverticalFOV)
    signal qmlhorizontalFOVChanged(string columnhorizontalFOV)
    signal qmlSensorChanged()
    signal qmlHandleMarklookatLatLongButtonClicked(bool pressed)
    signal qmltabChanged(bool value)

    property alias selectedFileName: filePath.text
    property alias fixedfov: fixedFOVCheckbox.checked
    //property alias selectedBuidingFileName: buidingFilePath.text
    property alias saveTifFilePath: tiffilePath.text
    property alias leLat: leLatId.text
    property alias leLong: leLongId.text
    property alias leHeight: leHeightId.text
    property alias leRange: leRangeId.text
    property alias leName: leNameId.text
    property alias leHfov: horizontalFOVText.text
    property alias leVfov: verticalFOVText.text
    property bool isFilePathValid: false

    property alias leSensorType: sensor.value

    property alias leLookAtLat: lookatLat.text
    property alias leLookAtLong: lookatLong.text
    property alias leLookAtEle: elevationText.text



    property alias rectColor : colorRectangle.backgroundColor
    property alias rectColorFile : colorRectangleFile.backgroundColor
    property alias startButtonLabel : pbStartButtonId.text
    property alias markButtonLabel : pbMarkPositionId.enabled
    property alias markviewerButton : pbMarkPositionId.checked
    property alias markLookatButton :  pbMarklookat.checked
    property alias checkPickPosition :pbPointPositionId.checked

    //property alias stopButtonLabel : pbStopButtonId.text
    property alias startButtonEnabled: pbStartButtonId.enabled
    //property alias stopButtonEnabled: pbStopButtonId.enabled
    property bool calculating: false
    property string projectValid
    property string selectedRect

    property alias nameValue: nameComboBox.value
    property alias heightValue: heightComboBox.value
    property alias rangeValue: rangeComboBox.value
    property alias verticalFOVValue: verticalFOVComboBox.value
    property alias horizontalFOVValue: horizontalFOVComboBox.value
    property alias lookatLatValue: lookatLatComboBox.value
    property alias lookatLongValue:lookatLongComboBox.value
    property alias lookatEleValue:lookatEleComboBox.value

    property bool colorPickerVisible:false

    onColorPickerVisibleChanged: {
        if(colorPickerVisible === true){
            smpFileMenu.openColorPicker()
        }
        else{
            smpFileMenu.closeColorPicker()
        }
    }

    function colorChanged(){
        var fontObj = smpFileMenu.getColorPicker();
        if(selectedRect === "defaultRect"){
            rectColor = fontObj.currentColor
            rectColorFile = fontObj.currentColor
        }
        else{
            colorListModel[colorListView.currentIndex].cellColor = fontObj.currentColor
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizTabFrame{
            id : vizTabFrameId
            onCurrentChanged: qmltabChanged(current)

            VizTab{
                title: "Manual selection"
                id: tabPPCTabID

                ColumnLayout{
                    id:contentLayout

                    RowLayout{
                        Layout.fillWidth: true;
                        id: positionRow
                        VizLabel {
                            id: labelPositionId
                            textAlignment: Qt.AlignLeft
                            text: "Position:             "
                        }

                        VizTextField {
                            id: leLatId
                            placeholderText: "Latitude"
                            Layout.fillWidth: true
                            validator: rootStyle.latitudeValidator
                        }
                        VizTextField {
                            id: leLongId
                            placeholderText: "Longitude"
                            Layout.fillWidth: true
                            validator: rootStyle.longitudeValidator
                        }

                        VizButton {
                            id:pbMarkPositionId
                            text:"   Mark   "
                            checkable: true
                            onClicked: {
                                pbMarklookat.checked=false;
                                qmlHandleMarklookatLatLongButtonClicked(false);

                                pbPointPositionId.checked = false;
                                qmlHandlePointButtonClicked(false);
                                qmlHandleMarkButtonClicked(checked);

                            }
                        }

                        VizButton {
                            id:pbPointPositionId
                            text:"   Pick   "
                            checkable: true
                            onClicked: {
                                pbMarklookat.checked=false;
                                qmlHandleMarklookatLatLongButtonClicked(false);

                                pbMarkPositionId.checked = false;
                                qmlHandleMarkButtonClicked(false);
                                qmlHandlePointButtonClicked(checked);
                            }
                        }
                    }

                    RowLayout{
                        id: sensortype
                        z: 150
                        VizLabel{
                            text: "Sensor type:       "
                            textAlignment: Qt.AlignLeft
                        }


                        VizComboBox{
                            id: sensor
                            Layout.fillWidth: true
                            listModel: (typeof(sensorListModel)=="undefined")?"undefined":sensorListModel
                            value:sensor.get(selectedUID).name

                            onSelectOption: qmlSensorChanged()
                        }


                        VizLabel{
                            Image {
                                source:   rootStyle.getIconPath("ElementListIcons/error")
                                visible: (projectValid.valueOf() == "valid")? false: true
                            }
                            id: projectValidId
                            textAlignment: Qt.AlignLeft
                            text: (projectValid.valueOf() == "valid")?" ":projectValid.valueOf()
                        }
                    }

                    ColumnLayout{
                        Layout.fillWidth: true
                        //columns: 4

                        RowLayout{
                            id: nameRow

                            VizLabel {
                                id:labelNameId
                                text: "Name                  "
                                textAlignment: Qt.AlignLeft

                            }

                            VizTextField{
                                id: leNameId
                                Layout.fillWidth: true
                                placeholderText: "Enter observer name ";
                                validator: rootStyle.nameValidator;
                            }
                        }

                        RowLayout{
                            id: heightRow
                            VizLabel {
                                id:labelHeightId
                                text: "Height                 "
                                textAlignment: Qt.AlignLeft
                            }

                            VizTextField{
                                id: leHeightId
                                Layout.fillWidth: true
                                placeholderText: "Enter height in meters"
                                validator: DoubleValidator{
                                    bottom: 0;
                                    top:9999
                                    decimals: 2;
                                    notation: DoubleValidator.StandardNotation;
                                }
                            }
                        }

                        RowLayout{
                            id: rangeRow

                            VizLabel {
                                id:labelRangeId
                                text: "Range                 "
                                textAlignment: Qt.AlignLeft
                            }

                            VizTextField{
                                id: leRangeId
                                Layout.fillWidth: true
                                placeholderText: "Enter range in meters"
                                validator: DoubleValidator{
                                    bottom: 0;
                                    top:99999
                                    decimals: 2;
                                    notation: DoubleValidator.StandardNotation;
                                }
                            }
                        }

                        RowLayout{
                            id: colorRow

                            VizLabel {
                                id:labelColorId
                                text: "Visible area color"
                                textAlignment: Qt.AlignLeft

                            }

                            VizButton{
                                id: colorRectangle
                                flatButton: true
                                Layout.fillWidth: true
                                backgroundColor: "green"
                                onClicked : {

                                    if(colorPickerVisible == false){
                                        smpFileMenu.setColor(rectColor)
                                        colorPickerVisible =true
                                        selectedRect = "defaultRect"
                                    }
                                    else{
                                        colorChanged()
                                        colorPickerVisible =false
                                    }
                                }
                            }
                        }
                    }

                    VizGroupBox{
                        id: fixedFOVCheckbox
                        Layout.fillWidth: true;
                        title: "Fixed field of view"
                        checkable : true
                        onClicked: {
                            qmlfixedfov(checked)
                        }

                        ColumnLayout{
                            RowLayout{
                                VizLabel{
                                    text: "Vertical FOV      "
                                    textAlignment: Qt.AlignLeft
                                }

                                VizTextField{
                                    id: verticalFOVText
                                    Layout.fillWidth: true
                                    placeholderText: "Enter vertical FOV"
                                    validator: DoubleValidator{bottom: 0.1;top: 180;notation: DoubleValidator.StandardNotation;
                                    }
                                }
                            }

                            RowLayout{
                                VizLabel{
                                    text: "Horizontal FOV "
                                    textAlignment: Qt.AlignLeft
                                }

                                VizTextField{
                                    id: horizontalFOVText
                                    Layout.fillWidth: true
                                    placeholderText: "Enter horizontal FOV"
                                    validator: DoubleValidator{bottom: 0.1;top: 360;notation: DoubleValidator.StandardNotation;
                                    }
                                }
                            }

                            RowLayout{
                                VizLabel{
                                    text: "Vertical angle    "
                                    textAlignment: Qt.AlignLeft
                                }

                                VizTextField{
                                    id: elevationText
                                    Layout.fillWidth: true
                                    placeholderText: "Vertical angle"
                                    validator: DoubleValidator{bottom: -90;top:90;notation: DoubleValidator.StandardNotation;
                                    }
                                }
                            }

                            RowLayout{
                                VizLabel {
                                    id: positionId
                                    text: "Look at point     "
                                    textAlignment: Qt.AlignLeft
                                }

                                VizTextField {
                                    id: lookatLat
                                    Layout.fillWidth: true
                                    placeholderText: "Latitude"
                                    validator: rootStyle.latitudeValidator
                                }
                                VizTextField {
                                    id: lookatLong
                                    Layout.fillWidth: true
                                    placeholderText: "Longitude"
                                    validator: rootStyle.longitudeValidator
                                }

                                VizButton {
                                    id:pbMarklookat
                                    text:"   Mark   "
                                    checkable: true
                                    onClicked: {
                                        pbPointPositionId.checked = false;
                                        pbMarkPositionId.checked = false;

                                        qmlHandlePointButtonClicked(false);
                                        qmlHandleMarklookatLatLongButtonClicked(checked);
                                    }
                                }
                            }
                        }
                    }

                }

            }

            VizTab{
                title: "Load from file"
                id: tabPPCTabID2
                //                height: contentLayout2.implicitHeight
                //                width: contentLayout2.implicitWidth

                ColumnLayout{
                    id:contentLayout2
                    Layout.fillWidth: true;
                    RowLayout{
                        VizLabel{
                            text: "Point file                               "
                            textAlignment: Qt.AlignLeft
                        }
                        VizTextField{
                            id: filePath
                            text: ""
                            placeholderText: "Browse files..."
                            Layout.fillWidth: true
                        }
                        VizButton{
                            text:"Browse"
                            onClicked: {
                                qmlBrowseButtonClicked()
                            }
                        }
                    }
                    RowLayout{
                        VizLabel{
                            text: "Name atrribute                     "
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox{
                            id: nameComboBox
                            inverted: true
                            Layout.fillWidth: true
                            listModel: (typeof(layerColumnNameListModel) == "undefined") ? "undefined" : layerColumnNameListModel
                            value: "<None>"

                            onSelectOption:
                                if (isFilePathValid)
                                {
                                    qmlnameChanged(value)
                                }
                        }
                    }

                    RowLayout{
                        VizLabel{
                            text: "Height attribute                    "
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox{
                            id: heightComboBox
                            inverted: true
                            Layout.fillWidth: true
                            listModel: (typeof(layerColumnHeightListModel) == "undefined") ? "undefined" : layerColumnHeightListModel
                            value: "<None>"
                            onSelectOption:
                                if (isFilePathValid)
                                {
                                    qmlheightChanged(value)
                                }
                        }
                    }

                    RowLayout{
                        VizLabel{
                            text: "Range attribute                    "
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox{
                            id: rangeComboBox
                            inverted: true
                            Layout.fillWidth: true
                            listModel: (typeof(layerColumnRangeListModel) == "undefined") ? "undefined" : layerColumnRangeListModel
                            value: "<None>"
                            onSelectOption:
                                if (isFilePathValid)
                                {
                                    qmlrangeChanged(value)
                                }
                        }
                    }

                    RowLayout{

                        VizLabel{
                            text: "Visible area color                 "
                            textAlignment: Qt.AlignLeft
                        }

                        VizButton{
                            id: colorRectangleFile
                            flatButton: true
                            backgroundColor: "green"
                            Layout.fillWidth: true
                            onClicked : {
                                if(colorPickerVisible == false){
                                    smpFileMenu.setColor(rectColorFile)
                                    colorPickerVisible =true
                                    selectedRect = "defaultRect"
                                }
                                else{
                                    colorChanged()
                                    colorPickerVisible =false
                                }
                            }
                        }
                    }
                }

                VizGroupBox{
                    id: fixedFOVCheckbox1
                    title: "Fixed field of view"
                    Layout.fillWidth: true;
                    checkable: true
                    onClicked: {
                        qmlfixedfov(checked)
                    }

                    ColumnLayout{


                        RowLayout
                        {
                            VizLabel{
                                text: "Vertical FOV atrrtibute      "
                                textAlignment: Qt.AlignLeft
                            }
                            VizComboBox{
                                id: verticalFOVComboBox
                                Layout.fillWidth: true
                                inverted: true
                                listModel: (typeof(layerColumnverticalFOVListModel) == "undefined") ? "undefined" : layerColumnverticalFOVListModel
                                value: "180"
                                onSelectOption:
                                    if (isFilePathValid)
                                    {
                                        qmlverticalFOVChanged(value)
                                    }

                            }
                        }

                        RowLayout{
                            VizLabel{
                                text: "Horizontal FOV attribute   "
                                textAlignment: Qt.AlignLeft
                            }
                            VizComboBox{
                                id: horizontalFOVComboBox
                                Layout.fillWidth: true
                                inverted: true
                                listModel: (typeof(layerColumnhorizontalFOVListModel) == "undefined") ? "undefined" : layerColumnhorizontalFOVListModel
                                value: "360"
                                onSelectOption:
                                    if (isFilePathValid)
                                    {
                                        qmlhorizontalFOVChanged(value)
                                    }
                            }
                        }

                        RowLayout{
                            VizLabel{
                                width: 50
                                text: "Look at latitude attribute       "
                                textAlignment: Qt.AlignLeft
                            }
                            VizComboBox{
                                id: lookatLatComboBox
                                Layout.fillWidth: true
                                Layout.minimumWidth: 100
                                inverted: true
                                listModel: (typeof(layerColumnlookatLatListModel) == "undefined") ? "undefined" : layerColumnlookatLatListModel
                                value: "0"
                                onSelectOption:
                                    if (isFilePathValid)
                                    {
                                        qmllookatLatChanged(value)
                                    }

                            }
                        }
                        RowLayout{
                            VizLabel{
                                text: "Look at longitude attribute"
                                textAlignment: Qt.AlignLeft
                            }
                            VizComboBox{
                                id: lookatLongComboBox
                                Layout.fillWidth: true
                                inverted: true
                                listModel: (typeof(layerColumnlookatLongListModel) == "undefined") ? "undefined" : layerColumnlookatLongListModel
                                value: "0"
                                onSelectOption:
                                    if (isFilePathValid)
                                    {
                                        qmllookatLongChanged(value)
                                    }

                            }
                        }
                        RowLayout{
                            VizLabel{
                                text: "Vertical angle attribute      "
                                textAlignment: Qt.AlignLeft
                            }
                            VizComboBox{
                                id: lookatEleComboBox
                                Layout.fillWidth: true
                                inverted: true
                                listModel: (typeof(layerColumnlookatEleListModel) == "undefined") ? "undefined" : layerColumnlookatEleListModel
                                value: "0"
                                onSelectOption:
                                    if (isFilePathValid)
                                    {
                                        qmllookatEleChanged(value)
                                    }
                            }
                        }
                    }
                }
            }
        }

        VizTableView {
            id: twObserverListId;
            model:(typeof(observerModel) !== "undefined") ? observerModel: "undefined"
            Layout.fillWidth: true
            Layout.fillHeight: true
            focus: true;
            clip: true;
            onCurrentRowChanged: {
                if((currentRow >= 0 )&& (rowCount > 0))  {
                    if(typeof(observerModel) !== "undefined"){
                        leName  = observerModel[currentRow].name
                        leLong  = observerModel[currentRow].longi
                        leLat = observerModel[currentRow].lat
                        leHeight = observerModel[currentRow].height
                        leRange = observerModel[currentRow].range
                        leLookAtLat = observerModel[currentRow].lookatlat
                        leLookAtLong  = observerModel[currentRow].lookatlongi
                        leLookAtEle = observerModel[currentRow].lookatele
                        leHfov = observerModel[currentRow].horizontalFOV
                        leVfov = observerModel[currentRow].verticalFOV
                    }
                }
            }

            TableViewColumn{
                role: "name";
                title: "Name";
            }

            TableViewColumn{
                role: "lat";
                title: "Latitude"
            }

            TableViewColumn{
                role: "longi";
                title: "Longitude";
            }

            TableViewColumn{
                role: "height";
                title: "Height";
            }
            TableViewColumn{
                role: "range";
                title: "Range";
            }
            TableViewColumn{
                role: "lookatlat";
                title: "Look at latitude";
            }

            TableViewColumn{
                role: "lookatlongi";
                title: "Look at longitude";
            }

            TableViewColumn{
                role: "lookatele";
                title: "Look at vertical angle";
            }

            TableViewColumn{
                role: "horizontalFOV";
                title: "Horizontal FOV";
            }

            TableViewColumn{
                role: "verticalFOV";
                title: "Vertical FOV";
            }

            TableViewColumn{
                role: "color";
                title: "Color";
                delegate: VizButton{
                    flatButton: true
                    backgroundColor:  modelData.color
                }
            }
        }


        RowLayout {

            VizButton {
                id: pbAddButtonId
                text: "    Add    "
                onClicked: {
                    qmlHandleAddButtonClicked();
                }
            }
            VizButton {
                id: pbRemoveButtonId;
                text: "   Remove   "
                onClicked: {
                    qmlHandleRemoveButtonClicked(twObserverListId.currentIndex);
                }
            }

        }

        RowLayout {
            id: observerButtonId ;

            VizLabel{
                //width: 50
                id : save
                text: "Output tiff path"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: tiffilePath
                text: ""
                Layout.fillWidth: true
                placeholderText: "Set Path..."
            }
            VizButton{
                text:"Browse"
                onClicked: {
                    qmlSaveTifPathButton()
                }
            }
        }

        RowLayout {
            Rectangle{
                //Layout.fillWidth: true
            }
        }

        RowLayout {
            id: controlButtonId

            AnimatedImage{
                id:progressBarId
                Layout.fillWidth: true
                source: rootStyle.getBusyIcon()
                visible: calculating
            }
            Rectangle{
                Layout.fillWidth: true
            }
            VizButton {
                id: pbStartButtonId
                text: "   Calculate   "
                onClicked: {
                    qmlHandleStartButtonClicked()
                }
            }

            /*VizButton {
                id: pbStopButtonId;
                text: "    Cancel    "
                onClicked: {
                    qmlHandleStopButtonClicked()
                }
            }*/
        }
    }
}
