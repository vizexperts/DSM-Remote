import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup {
    id: userSettingsMenu

    objectName: "userSettingsMenu"
    title: "Server Settings"

    height: mainLayout.implicitHeight + 15
    width: mainLayout.implicitWidth+ 10

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal okButtonClicked()
    signal cancelButtonClicked()
    signal browseRasterData()
    signal browseElevationData()

    onCancelButtonClicked: closePopup()

    property alias isProjectLocal: localProjectCheckbox.checked

    // project database properties
    property alias projectDatabaseName : projectDatabaseNameLineEdit.text
    property alias projectDatabaseUsername : projectDatabaseUsernameLineEdit.text
    property alias projectDatabasePassword : projectDatabasePasswordLineEdit.text
    property alias projectDatabaseHost : projectDatabaseHostLineEdit.text
    property alias projectDatabasePort : projectDatabasePortLineEdit.text

    // layer database properties
    property alias layerDatabaseName : layerDatabaseNameLineEdit.text
    property alias layerDatabaseUsername : layerDatabaseUsernameLineEdit.text
    property alias layerDatabasePassword: layerDatabasePasswordLineEdit.text
    property alias layerDatabaseHost : layerDatabaseHostLineEdit.text
    property alias layerDatabasePort: layerDatabasePortLineEdit.text

    // webserver properties
    property alias webserverServer: webserverServerLineEdit.text
    property alias webserverUsername: webserverUsernameLineEdit.text
    property alias webserverPassword: webserverPasswordLineEdit.text

    //Local Data Properties
    property alias localRasterLocation: localDataRasterLineEdit.text
    property alias localElevationLocation: localDataElevationLineEdit.text
    //property alias localVectorLocation: localDataVectorLineEdit.text

    //properties of buttons
    property alias browseRasterDataEnabled:browseRaster.enabled
    property alias browseElevationDataEnabled:browseElevation.enabled

    property bool reloadProjectsList: false

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5
        VizCheckBox{
            id: localProjectCheckbox
            text: "Enable Local Project"
            onClicked: reloadProjectsList = true
            onCheckedChanged: {
                if(checked){
                    databaseTabFrame.current = 1
                }
                else{
                    databaseTabFrame.current = 0
                }
            }
        }
        VizTabFrame{
            id: databaseTabFrame
            Layout.minimumHeight: 300*rootStyle.uiScale
            Layout.fillHeight: true
            VizTab{
                id:  projectDatabaseTab
                visible: !localProjectCheckbox.checked                
                contentMargin: 10
                title: "Project"
                GridLayout{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    columns: 2
                    VizLabel{
                        text: "Database Name"
                    }
                    VizTextField{
                        id: projectDatabaseNameLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: projectDatabaseUsernameLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Username"
                    }
                    VizTextField{
                        id: projectDatabaseUsernameLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: projectDatabasePasswordLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Password"
                    }
                    VizTextField{
                        id: projectDatabasePasswordLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: projectDatabaseHostLineEdit.giveFocus()
                        echoMode: TextInput.Password
                    }
                    VizLabel{
                        text: "Host"
                    }
                    VizTextField{
                        id: projectDatabaseHostLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: projectDatabasePortLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Port"
                    }
                    VizTextField{
                        id: projectDatabasePortLineEdit
                        onTextChanged: reloadProjectsList = true
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
//                        Keys.onTabPressed: projectDatabaseUsernameLineEdit.giveFocus()
                    }
                }
            }
            VizTab{
                id: layerDatabaseTab
                contentMargin: 10
                title: "Layer"
                GridLayout{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    columns: 2
                    VizLabel{
                        text: "Database Name"
                    }
                    VizTextField{
                        id: layerDatabaseNameLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: layerDatabaseUsernameLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Username"
                    }
                    VizTextField{
                        id: layerDatabaseUsernameLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: layerDatabasePasswordLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Password"
                    }
                    VizTextField{
                        id: layerDatabasePasswordLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: layerDatabaseHostLineEdit.giveFocus()
                        echoMode: TextInput.Password
                    }
                    VizLabel{
                        text: "Host"
                    }
                    VizTextField{
                        id: layerDatabaseHostLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: layerDatabasePortLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Port"
                    }
                    VizTextField{
                        id: layerDatabasePortLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                    }
                }
            }
            VizTab{
                id: webserverTab
                visible: !localProjectCheckbox.checked
                contentMargin: 10
                title: "Webserver"
                GridLayout{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    columns: 2
                    VizLabel{
                        text: "Server"
                    }
                    VizTextField{
                        id: webserverServerLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: webserverUsernameLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Username"
                    }
                    VizTextField{
                        id: webserverUsernameLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        Keys.onTabPressed: webserverPasswordLineEdit.giveFocus()
                    }
                    VizLabel{
                        text: "Password"
                    }
                    VizTextField{
                        id: webserverPasswordLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        onTextChanged: reloadProjectsList = true
                        echoMode: TextInput.Password
                    }
                }
            }
            VizTab{
                id: localDataTab
                contentMargin: 10
                title: "Local Data"
                GridLayout{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    columns: 3
                    VizLabel{
                        text: "Raster"
                    }
                    VizTextField{
                        id: localDataRasterLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        placeholderText: "Add Raster Data"
                        Keys.onTabPressed: localDataElevationLineEdit.giveFocus()
                    }
                    VizButton{
                        id: browseRaster
                        text: "Add"
                        onClicked: browseRasterData()
                    }
                    VizLabel{
                        text: "Elevation"
                    }
                    VizTextField{
                        id: localDataElevationLineEdit
                        Layout.fillWidth: true
                        Layout.minimumWidth: 200
                        placeholderText: "Add Elevation Data"
                        Keys.onTabPressed: localDataElevationLineEdit.giveFocus()
                    }
                    VizButton{
                        id: browseElevation
                        text: "Add"
                        onClicked: browseElevationData()
                    }

                }
            }
        }
        OkCancelButtonRow{
            anchors.horizontalCenter: parent.horizontalCenter
            onOkButtonPressed: {
                userSettingsMenu.enabled = false
                okButtonClicked()
            }
            onCancelButtonPressed: cancelButtonClicked()
        }
    }
}
