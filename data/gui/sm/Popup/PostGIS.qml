import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: postgisPopUp
    objectName: "postgisPopUp"
    title: "Upload PostGIS"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth + 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal okButtonClicked()
    signal cancelButtonClicked()
    signal selectDataType()
    signal setParent()

    property alias tileWidth: tileWidthSpace.text
    property alias tileHeight: tileHeightSpace.text
    property alias selectedTableName: tableNameText.text
    property alias dataTypeSelected: dataTypeDropDown.value
    property alias createCheckBox: createCheckBox.checked
    property alias appendCheckBox: appendCheckBox.checked
    property alias dataCategory: categoryText.text
    property alias databaseText: databaseText.text

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id:database
            spacing: 5
            VizLabel{
                text: "Database"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id:databaseText
                Layout.fillWidth: true
                placeholderText: ""
                readOnly: true
            }
        }

        RowLayout{
            id:tableName
            spacing: 5
            VizLabel{
                text: "Table Name"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id:tableNameText
                Layout.fillWidth: true
                placeholderText: "Enter Table Name"
            }
        }

        RowLayout{
            id:dataType
            spacing: 5
            z:parent.z + 10
            VizLabel{
                text: "Data Type"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                id: dataTypeDropDown
                Layout.fillWidth: true
                listModel: (typeof(dataTypeList) !== "undefined") ? dataTypeList: "undefined"
                onSelectOption: selectDataType()
            }

            VizButton{
                id:createDataTypeButton
                text: "Create Data Type"
                onClicked: {
                    smpFileMenu.load("CreateDataType.qml")
                    setParent()
                }
            }
        }

        RowLayout{
            id:categoryDataType
            spacing: 5
            VizLabel{
                text: "Category of selected Data Type"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id:categoryText
                Layout.fillWidth: true
                placeholderText: ""
                readOnly: true
            }
        }

        VizGroupBox{
            id:upload
            Layout.fillWidth: true
            Layout.fillHeight: true
            title: "Upload Type"
            checkable: false
            RowLayout{
                id: row1
                spacing: 5
                height: childrenRect.height
                ExclusiveGroup{id: uploadGroup}
                VizCheckBox{
                    id: createCheckBox
                    text: "Create"
                    checked: true
                    exclusiveGroup: uploadGroup
                }

                VizCheckBox{
                    id: appendCheckBox
                    text: "Append"
                    checked: false
                    exclusiveGroup: uploadGroup
                    onClicked: appendCheckClicked()
                }
            }
        }
        RowLayout{
            id: tileWidth
            spacing: 5

            VizLabel{
                text: "Tile width"
            }
            VizTextField{
                id:tileWidthSpace
                Layout.fillWidth: true
                readOnly: appendCheckBox.checked
                text:"64"
            }
        }
        RowLayout{
            id: tileHeight
            spacing: 5

            VizLabel{
                text: "Tile height"
            }
            VizTextField{
                id:tileHeightSpace
                Layout.fillWidth: true
                readOnly: appendCheckBox.checked
                text:"64"
            }
        }

        RowLayout{
            id: commandButtons
            spacing: 10
            Layout.alignment: Qt.AlignRight
            VizButton{
                id:okButton
                text: "OK"
                onClicked: {
                    okButtonClicked()
                    closePopup()
                }
            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                onClicked: {
                    closePopup()
                }
            }
        }
    }
}


