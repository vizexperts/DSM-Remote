import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: versionId
    objectName: "VersionInfoObject"
    title: "Version Information"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    // for displaying of list populated data
    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5
        VizTableView{
            id: versionTabId
            model: listInfoModel
            Layout.minimumWidth:  480
            Layout.minimumHeight: 200
            Layout.fillWidth: true
            Layout.fillHeight: true
            focus: true
            clip: true
            TableViewColumn {
                width: 150
                role: "name"
                title: "Name"
                elideMode: Text.ElideLeft
            }
            TableViewColumn {
                width: 300
                role: "type"
                title: "Description"
                elideMode: Text.ElideLeft
            }
        }
    }
}
