import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: queryBuilderTableId
    objectName: "QueryBuilderTable"
    title: "Query Results"
    width: 400
    height: 525

    signal tableOkButtonClicked()


    // Creation of table view
    function addTableViewColumn(paramRole, paramTitle, paramId, paramText )
    {
        var columnToAdd = Qt.createComponent(tableViewColumnComponent);
        if(columnToAdd.status === Component.Ready)
        {
            queryTableViewId.addColumn(columnToAdd.createObject(queryTableViewId,{"role":paramRole
                                                                   , "title":paramTitle
//                                                                   , "columnId":paramId
//                                                                   , "columnText":paramText
                                                                   }))
        }
    }

    // for displaying of table populated data

    Component{
        id: tableViewColumnComponent
        TableViewColumn{

        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }


    TableView{
        id: queryTableViewId
        objectName: "queryBuilderTableView"
        model: (typeof(queryTableModel) !== "undefined") ? queryTableModel : "undefined";             //queryTableModel
        anchors.right: parent.right
        anchors.bottom: backtoQueryButtonId.top
        anchors.bottomMargin: 10
        anchors.top: parent.top
        anchors.left: parent.left
        //implicitWidth: 400
        //implicitHeight: 525
        Layout.fillWidth: true
        Layout.fillHeight: true
        focus: true
        clip: true
    }

    VizButton
    {
        id: okbuttonId
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.bottomMargin: 5
        text: "     OK     "
        onClicked:{
            tableOkButtonClicked()
            closePopup()
        }
    }

    VizButton
    {
        id:backtoQueryButtonId
        anchors.right: okbuttonId.left
        anchors.rightMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        text: "Back to Query"
        onClicked:
        {
             smpFileMenu.load("QueryBuilder.qml")
        }
    }

}

