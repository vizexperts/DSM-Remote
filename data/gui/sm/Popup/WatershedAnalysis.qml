import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: watershedAnalysisid
    objectName: "WatershedAnalysis"
    title: "Watershed analysis"
    clip : true

    Component.onCompleted: {
        if(!running_from_qml){
            WatershedGenerationGUI.setActive(true)
        }
    }
    Component.onDestruction:  {
        if(!running_from_qml){
            WatershedGenerationGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //properties
    property alias outputNameStream : outputNameStreamId.text
    property alias outputNameWater : outputNameWaterShedId.text

    property alias thresholdVal : thresholdId.text

    property alias bottomLeftLongitude : bottomLeftLongitude.text
    property alias bottomLeftLatitude : bottomLeftLatitude.text
    property alias topRightLongitude : topRightLongitude.text
    property alias topRightLatitude : topRightLatitude.text

    property alias outletBottomLeftLongitude : outletBottomLeftLongitude.text
    property alias outletBottomLeftLatitude : outletBottomLeftLatitude.text
    property alias outletTopRightLongitude : outletTopRightLongitude.text
    property alias outletTopRightLatitude : outletTopRightLatitude.text
    //states of buttons
    property alias isStartButtonEnabled : startButton.enabled
    //property alias isStopButtonEnabled: stopButton.enabled
    property alias isMarkSelected : markButton.checked
    property alias isOutletMarkSelected : outLetMarkButton.checked
    property alias isProgressBarVisible : pb2.visible
    property alias progressValue: pb2.value


    //signal generated by buttons
    signal changeAnalysisAspect(string aspect)
    signal startAnalysis()
    signal stopAnalysis()
    signal markPosition(bool value)
    signal markOutletPosition(bool value)
    signal getprogressBar()

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizGroupBox{
            title: "Analysis parameters"
            z:5

            GridLayout{
                id: parameterGrid
                columns: 2
                anchors.fill: parent
                VizLabel{
                    text:"Threshold"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: thresholdId
                    text: ""
                    Layout.fillWidth: true
                    validator:  DoubleValidator { bottom : 0;notation: DoubleValidator.StandardNotation; }
                    placeholderText: "Enter threshold"
                }
                VizLabel{
                    text:"Output(Stream)"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: outputNameStreamId
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "Enter Output Name"
                    validator: rootStyle.nameValidator
                }
                VizLabel{
                    text:"Output(Watershed)"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: outputNameWaterShedId
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "Enter Output Name"
                    validator: rootStyle.nameValidator
                }
            }
        }
        VizGroupBox{
            id : areaGroupBox
            title: "Area"
            GridLayout{
                id: areaGrid
                columns: 3
                anchors.fill: parent
                VizLabel{
                    text:"Bottom-Left"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: bottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "bottom"
                    validator: rootStyle.longitudeValidator
                }
                VizTextField{
                    id: bottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "left"
                    validator:rootStyle.latitudeValidator
                }
                VizLabel{
                    text:"Top-Right"
                    textAlignment: Qt.AlignLeft
                }

                VizTextField{
                    id: topRightLongitude
                    text: ""
                    placeholderText: "top"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }

                VizTextField{
                    id: topRightLatitude
                    text: ""
                    placeholderText: "right"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                VizButton{
                    id: markButton
                    objectName: "markArea"
                    text:"    Mark    "
                    backGroundVisible: true
                    checkable : true
                    Layout.alignment: Qt.AlignRight
                    onClicked: markPosition(checked)
                }
            }
        }
        VizGroupBox{
            id : outletAreaGroupBox
            title: "Outlet Region"
            GridLayout{
                id: outletAreaGrid
                columns: 3
                anchors.fill: parent
                VizLabel{
                    text:"Bottom-Left"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: outletBottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "bottom"
                    validator: rootStyle.longitudeValidator
                }
                VizTextField{
                    id: outletBottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "left"
                    validator:rootStyle.latitudeValidator
                }
                VizLabel{
                    text:"Top-Right"
                    textAlignment: Qt.AlignLeft
                }

                VizTextField{
                    id: outletTopRightLongitude
                    text: ""
                    placeholderText: "top"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }

                VizTextField{
                    id: outletTopRightLatitude
                    text: ""
                    placeholderText: "right"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                Rectangle{
                    Layout.fillWidth: true
                }
                VizButton{
                    id: outLetMarkButton
                    objectName: "markArea"
                    text:"    Mark    "
                    backGroundVisible: true
                    checkable : true
                    Layout.alignment: Qt.AlignRight
                    onClicked: markOutletPosition(checked)
                }
            }
        }

        RowLayout{
            id:commandRect
            Layout.fillWidth: true
            VizProgressBar{
                id : pb2
                value: 0
                minimum: 0
                maximum: 100
                indeterminate: false
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                onValueChanged:{
                    if( value == 100)
                    {
                        startButton.enabled = true
                        //stopButton.enabled = false
                    }
                }
            }
            Item{
                Layout.fillWidth: true
            }

            VizButton{
                id: startButton
                objectName: "startButton"
                text: "   Calculate   "
                onClicked:{
                    if((outputNameStreamId.text == "")||(outputNameWaterShedId.text == "")||(thresholdId.text == "")||(bottomLeftLongitude.text=="")||(bottomLeftLatitude.text=="")||(topRightLongitude.text=="")||(topRightLatitude.text=="")
                            ||(outletBottomLeftLongitude.text=="")||(outletBottomLeftLatitude.text=="")||(outletTopRightLongitude.text=="")||(outletTopRightLatitude.text==""))
                    {
                        startAnalysis()
                        startButton.enabled = true
                    }
                    else
                    {

                        //stopButton.enabled = true
                        startButton.enabled = false
                        startAnalysis()
                    }
                }
            }
            /*VizButton{
                id : stopButton
                objectName: "stopButton"
                text: "   Cancel   "
                enabled: false
                onClicked:{
                    stopAnalysis()
                    stopButton.enabled = false
                    startButton.enabled = true

                }
            }*/
        }
    }
}

