import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"
import ProfileObject 1.0
import ".."

Popup{

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 200
    minimumHeight: mainLayout.Layout.minimumHeight +10

    objectName: "ditch"
    title: "Extrusion"

    Component.onCompleted: {
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    property alias profileName : profileCombobox.value
    property string templateName : "Default"
    property alias topTextureFile : textureFilePath.text
    property alias profileEditorXProp : profileEditor.xProp
    property alias profileEditorYProp : profileEditor.yProp
    property alias profileAppliedText : profileApplied.text

    property alias profileYText : profileY.text
    property alias profileXText : profileX.text

    property var currentPosition: []
    property var points : []

    signal browseButtonClicked()
    signal apply(string profile,var positionList)
    signal applyProfileToTerrain(var pointProfileMap)
    signal loadTemplate(string templateName, bool refreshTemplateList)
    signal saveTemplate(string templateName, var pointList)
    signal addNewTemplate(string templateName)
    signal deleteTemplate(string templateName)
    signal deleteProfile(string profile)

    function clearGraphs()
    {
        graph.clearGraph()
        profileEditor.clearGraph()
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 10
        Layout.minimumWidth: 380


        RowLayout{
            VizLabel{
                text: "Profile"
                width : 50
            }

            VizComboBox{
                id: profileCombobox
                Layout.minimumWidth: 300
                Layout.fillWidth: true
                listModel: typeof(profileList)== "undefined" ? "undefined" : profileList
                value: profileCombobox.get(selectedUID).name

                onSelectOption: {
                    loadTemplate("Default", true);
                    graph.setVizProfileObject("Default","red",true);
                    profileEditor.setVizProfileObject("Default","red",true);
                    profileEditor.setLineData(points)
                }
            }

            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id:deleteProfileID
                text: "Delete Profile"
                onClicked:{
                    deleteProfile(profileName)
                }
            }

            Rectangle{
                Layout.fillWidth: true
            }
        }

        RowLayout {

        }

        RowLayout{

            VizLabel{
                text: "Texture"
                width : 50
            }

            VizTextField{
                id:textureFilePath
                Layout.minimumWidth: 300
                Layout.fillWidth: true
                placeholderText: " Browse texture file... "

            }

            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id:browse
                text: "Browse"
                onClicked:{
                    browseButtonClicked()
                }
            }

            Rectangle{
                Layout.fillWidth: true
            }

        }

        VizGroupBox{
            title: "Line Profile"

            ColumnLayout{
                anchors.fill:parent
                RowLayout{
                    id:firstrow
                    anchors.top:parent.top
                    anchors.bottom: secondrow.top
                    anchors.right: parent.right
                    anchors.left: parent.left
                    RowLayout{
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        Layout.fillWidth: true
                        anchors.right: col.left
                        anchors.left: parent.left
                        Layout.minimumHeight:  200
                        Layout.minimumWidth:   400

                        ProfileObject{
                            id : graph
                            objectName: "ditchPopup"
                            anchors.fill: parent
                            anchors.margins: 20
                        }
                    }

                    ColumnLayout{
                        id:col
                        anchors.right: parent.right

                        RowLayout{
                            VizLabel{
                                text: "X"
                                width : 15
                            }
                            VizTextField{
                                id:graphx
                                text: graph.xProp
                                width: 60
                            }
                        }
                        VizButton{
                            id:setgraphXID
                            width: 70
                            text: "Set"
                            onClicked:{
                                graph.xProp = graphxText
                                graph.setGraphX(graphxText)
                            }
                        }
                    }
                }
                RowLayout{
                    id:secondrow
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10
                    width:30
                    Rectangle{
                        width: 30
                    }
                    VizButton{
                        id:applyToTerrain
                        Layout.alignment: Qt.AlignLeft
                        Layout.minimumWidth: 35
                        text: "Apply to terrain"
                        onClicked:{
                            var profileMapLocal = graph.getPointProfileMap();
                            applyProfileToTerrain(profileMapLocal);
                        }
                    }

                    VizButton{
                        id:cancel
                        Layout.alignment: Qt.AlignLeft
                        Layout.minimumWidth: 35
                        text: "Clear"
                        onClicked:{
                            graph.clearGraph()
                            profileAppliedText = ""
                        }
                    }

                    VizButton{
                        id: editButton
                        text: "   Edit   "
                        checkable: true
                        onCheckedChanged: {
                            graph.editGraph(checked);
                            profileAppliedText = ""
                        }
                    }

                    Rectangle{
                        width: 30
                    }

                    VizLabel{
                        id: profileApplied
                        text: ""
                        width : 120
                    }

                }

                Rectangle{
                    width: 30
                }

            }
        }

        VizGroupBox{
            title: "Profile Templates"

            ColumnLayout
            {
                anchors.fill:parent
                Layout.fillHeight: true
                Layout.fillWidth: true
                RowLayout{
                    Layout.fillWidth: true
                    id:row1
                    Layout.fillHeight: true
                    anchors.top: parent.top
                    anchors.bottom: row2.top

                    ColumnLayout
                    {
                        id:rectcol
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        Layout.fillWidth: true
                        Layout.minimumHeight:  200
                        Layout.minimumWidth:   160

                        Rectangle
                        {
                            anchors.fill:parent
                            anchors.margins: 5
                            color: rootStyle.colorWhenDefault
                            opacity: 0.7
                            border.width: 2
                            border.color:rootStyle.colorWhenSelected

                            ListView{
                                id: templateListView
                                anchors.fill: parent
                                clip: true
                                model: typeof(profileTemplateList)== "undefined" ? "undefined" : profileTemplateList
                                delegate:

                                    RowLayout{

                                    width: parent.width - 5
                                    Rectangle{
                                        width: 0.2
                                    }
                                    VizCheckBox{

                                        checked: model.active
                                        onCheckedChanged: {
                                            graph.setVisible(model.name, model.color, checked)
                                        }
                                    }

                                    VizLabel{

                                        text: model.name
                                        textColor : model.color
                                        width: parent.width - 30
                                        color: (index === templateListView.currentIndex)? rootStyle.colorWhenSelected : "transparent"

                                        border.width: 1
                                        border.color: rootStyle.colorWhenSelected
                                        anchors.right: parent.right
                                        MouseArea{
                                            anchors.fill: parent
                                            hoverEnabled: true
                                            onClicked: {
                                                templateListView.currentIndex = index
                                                templateName = model.name;
                                                loadTemplate(templateName, false);
                                                graph.setVizProfileObject(model.name,model.color, model.active);
                                                profileEditor.setVizProfileObject(model.name,model.color, model.active);
                                                profileEditor.setLineData(points)
                                                profileEditorXProp = "0"
                                                profileEditorYProp = "0"
                                                profileAppliedText = ""
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }

                    ColumnLayout
                    {
                        id:gridcolumn
                        Layout.fillWidth:  true
                        anchors.left: rectcol.right
                        anchors.right: xycolumn.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        GridLayout{
                            anchors.fill: parent
                            Layout.minimumHeight:260
                            Layout.minimumWidth:  260
                            onHeightChanged: {
                                if(height > width){
                                    height = width
                                }
                                else{
                                    width = height
                                }
                            }

                            onWidthChanged: {
                                if(height > width){
                                    height = width
                                }
                                else{
                                    width = height
                                }
                            }



                            ProfileObject{
                                id : profileEditor
                                objectName: "profileEditorPopup"
                                xProp : "0"
                                yProp : "0"
                                anchors.fill: parent
                                anchors.margins: 20
                            }
                        }
                    }
                    ColumnLayout
                    {
                        id:xycolumn
                        anchors.right: parent.right
                        anchors.rightMargin: 20
                        Rectangle{
                            anchors.fill: parent
                            color:"black"
                        }

                        RowLayout{
                            VizLabel{
                                text: "X"
                                width : 15
                            }
                            VizTextField{
                                id:profileX
                                width: 60
                                text : profileEditorXProp
                            }
                        }
                        RowLayout{
                            VizLabel{
                                text: "Y"
                                width : 15
                            }
                            VizTextField{
                                id:profileY
                                width: 60
                                text : profileEditorYProp
                            }
                        }
                        VizButton{
                            id:setProfileXYID
                            width: 70
                            text: " Set "
                            onClicked:{
                                profileEditorXProp = profileX.text
                                profileEditorYProp = profileY.text
                                profileEditor.setProfileXY(profileEditorXProp, profileEditorYProp)
                                profileAppliedText = ""
                            }
                        }
                    }

                }

                RowLayout{
                    id:row2
                    anchors.bottom: parent.bottom
                    width: parent.width
                    height: 30
                    Rectangle{
                        width: 30
                    }


                    VizButton{
                        id:editTemplateID
                        Layout.alignment: Qt.AlignLeft
                        Layout.minimumWidth: 35
                        text: "Add"
                        onClicked:{
                            addNewTemplate(templateName)
                            profileAppliedText = ""
                        }
                    }

                    VizButton{
                        id:saveTemplateID
                        Layout.alignment: Qt.AlignLeft
                        Layout.minimumWidth: 35
                        text: "Save"
                        onClicked:{
                            points = profileEditor.getLineData()
                            saveTemplate(templateName, points)
                            profileAppliedText = ""
                        }
                    }

                    VizButton{
                        id: deleteTemplateID
                        Layout.alignment: Qt.AlignLeft
                        Layout.minimumWidth: 35
                        text: "Delete"
                        onClicked:{
                            deleteTemplate(templateName)
                            if(templateName != "Default")
                            {
                                graph.deleteTemplate(templateName)
                                templateName = "Default"
                                loadTemplate("Default", false);
                                graph.setVizProfileObject("Default","red",true);
                                profileEditor.setVizProfileObject("Default","red",true);
                                profileEditor.setLineData(points)
                                profileAppliedText = ""
                            }
                        }
                    }
                }

            }
        }
    }
}
