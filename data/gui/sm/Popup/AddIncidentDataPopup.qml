import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "addIncidentDataPopup"
    title: "Add Incident Data Vector Layer"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Component.onCompleted: {
        smpFileMenu.setDateTime(fromTime)
    }
    Component.onDestruction: {
        smpFileMenu.closeDatePicker()
    }

    signal addIncidentLayer()
    //signal showLegends()
    signal addIncidentType(string tableName)
    signal plottingTypeChanged(string plottingType)
    signal addAllIncidents()
    signal openQueryBuilder()

    // property to get the current state of datetime option
    property int optionNone : 0
    property int optionTimeStart: 1
    property int optionTimeEnd: 2
    property int selectedOption: optionNone

    signal addGroupedQuery()
    signal removeGroupedQuery(int index)
    signal renameQuery()
    signal groupedQuerySelected(int index)

    function timeDateChanged()
    {
        var obj = smpFileMenu.getDatePicker()
        var d = new Date(obj.year, obj.month, obj.dayOfMonth, obj.hours, obj.minutes, obj.seconds)
        if(selectedOption === optionTimeStart){
            fromTime = d
            if(fromTime >= toTime)
                toTime = d
        }
        else if(selectedOption === optionTimeEnd){
            toTime = d
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    property alias queryName: queryNameTextField.text
    property alias layerName: layerNameTextBox.text
    property alias annotation: annotationComboBox.value
    property alias incidentType: incidentTypeComboBox.value
    property alias whereClause: whereClauseTextField.text
    property alias plotType: plotTypeComboBox.selectedUID
    property alias groupBy: groupByComboBox.value
    property int previousGroupedQueryIndex: 0
    property string bottomLeftLong
    property string bottomLeftLat
    property string topRightLong
    property string topRightLat

    property bool show : false

    onPreviousGroupedQueryIndexChanged: multipleQueryListView.currentIndex = previousGroupedQueryIndex

    onOpenQueryBuilder: {
        smpFileMenu.load("QueryBuilderGIS.qml")
    }

    property date fromTime
    property date toTime
    property string dateFormat: "ddd MMM dd/yyyy \n hh:mm:ss"

    ListModel{
        id: plotTypeId
        ListElement{name:"Point/Scatter"; uID : "0"}
        ListElement{name:"Point/Frequency"; uID : "1"}
        ListElement{name:"Bar/Frequency"; uID : "2"}
    }
    ListModel{
        id: groupByListModel
        ListElement {name: "Frontier"; uID: "0"}
        ListElement {name: "Sector"; uID: "1"}
        ListElement {name: "BOP"; uID: "2"}
        ListElement {name: "Incident"; uID: "3"}
    }


    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        z:10;
        RowLayout{
            id:layerNameCol
            spacing: 5
            VizLabel{
                width: 100
                text: "Layer Name"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: layerNameTextBox
                Layout.minimumWidth: 300
                width: 300
                text: ""
                placeholderText: "Layer Name..."
                Layout.fillWidth: true
            }
        }

        VizGroupBox{
            title: "Choose Incident Filter"
            z:100
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: sourceGrp.implicitHeight + margins
            Layout.minimumWidth: groupedQueryRow.implicitWidth + 20
            RowLayout{
                id: groupedQueryRow
                ColumnLayout{
                    id: groupedQueryCol
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    visible: plotTypeComboBox.selectedUID === '2'
                    Rectangle{
                        color: "transparent"
                        border.width: 2
                        border.color: rootStyle.colorWhenSelected
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.minimumWidth: 150*rootStyle.uiScale
                        ListView{
                            id: multipleQueryListView
                            currentIndex: 0
                            anchors.fill: parent
                            model: (typeof(queryNameList)=="undefined")?"undefined":queryNameList
                            spacing: 4
                            clip: true
                            focus: true
                            VizScrollBar{
                                target: multipleQueryListView
                                color: "transparent"
                                gradient: null
                            }
                            delegate: VizLabel{
                                id: label
                                text: modelData
                                textAlignment: Qt.AlignLeft
                                width: multipleQueryListView.width
                                Behavior on color {
                                    ColorAnimation { duration: 200 }
                                }
                                Behavior on textColor {
                                    ColorAnimation { duration: 200 }
                                }

                                textColor: (multipleQueryListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                                color: (multipleQueryListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                                MouseArea{
                                    anchors.fill: parent
                                    onClicked: {
                                        groupedQuerySelected(index)
                                    }
                                }
                            }
                        }
                    }
                    RowLayout{
                        id: groupQueryButtonRow
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        VizButton{
                            text: "Add"
                            onClicked: addGroupedQuery()
                        }
                        VizButton{
                            text: "Remove"
                            visible: multipleQueryListView.count > 0
                            onClicked: removeGroupedQuery(multipleQueryListView.currentIndex)
                        }
                    }
                }


                ColumnLayout{
                    id: sourceGrp
                    //width: parent.width

                    RowLayout{
                        id: incidentTypeCol
                        spacing: 5
                        z:10
                        VizLabel{
                            text: "Incident Type"
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox {
                            id: incidentTypeComboBox
                            listModel: (typeof(incidentTypeList)=="undefined")?"undefined":incidentTypeList
                            Layout.fillWidth: true
                            onSelectOption: addIncidentType(value)
                        }
                    }

                    RowLayout{
                        id : queryName
                        spacing : 5
                        visible : plotTypeComboBox.selectedUID == '2'

                        VizLabel{
                            text: "Query Name:"
                            textAlignment: Qt.AlignLeft
                        }

                        VizTextField{
                            id: queryNameTextField
                            text: ""
                            placeholderText: "Query name"
                            Layout.minimumWidth: 150*rootStyle.uiScale
                            Layout.fillWidth: true
                        }
                    }

                    RowLayout{
                        id: whereClauseRow
                        spacing: 5
                        z:5
                        VizButton{
                            text: "Query Builder"
                            onClicked: openQueryBuilder()
                        }

                        VizTextField{
                            id: whereClauseTextField
                            text: ""
                            placeholderText: "Enter Where Clause..."
                            Layout.minimumWidth: 150*rootStyle.uiScale
                            Layout.fillWidth: true
                        }

                        VizButton{
                            text: "Add All"
                            visible: plotTypeComboBox.selectedUID === '2'
                            onClicked: addAllIncidents()
                        }
                    }
                    GridLayout{
                        id: startTime
                        columns: 3
                        z: 5
                        VizLabel{
                            text: "From : "
                        }
                        VizLabel{
                            id: arrivalTimeTextEdit
                            text: Qt.formatDateTime(fromTime, dateFormat)
                        }
                        VizButton{
                            id : pbArrivalCalenderButtonID

                            onClicked:{
                                if(selectedOption === optionTimeStart){
                                    timeDateChanged()
                                    selectedOption = optionNone
                                }
                                else{
                                    selectedOption = optionTimeStart
                                    setDateTime(fromTime)
                                }
                            }
                            iconSource: rootStyle.getIconPath("calendar")
                            backGroundVisible: false
                        }
                        VizLabel{
                            text: " To: "
                        }
                        VizLabel{
                            id: departureTimeTextEdit
                            text: Qt.formatDateTime(toTime, dateFormat)
                        }
                        VizButton{
                            id : pbDepartureCalenderButtonID

                            onClicked:{
                                if(selectedOption === optionTimeEnd){
                                    timeDateChanged()
                                    selectedOption = optionNone
                                }
                                else{
                                    selectedOption = optionTimeEnd
                                    setDateTime(toTime)
                                }
                            }
                            iconSource: rootStyle.getIconPath("calendar")
                            backGroundVisible: false
                        }
                    }
                }
            }
        }
        VizGroupBox{
            title: "Style"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: styleCol.height + margins
            z:30
            ColumnLayout{
                id: styleCol
                width: parent.width

                RowLayout{
                    id: plotTypeRow
                    spacing: 5
                    z:11
                    VizLabel{
                        width: 100
                        text: "Plotting Type"
                        textAlignment: Qt.AlignLeft
                    }

                    VizComboBox {
                        id: plotTypeComboBox
                        width: 250
                        listModel:plotTypeId
                        value: plotTypeId.get(selectedUID).name
                        Layout.fillWidth: true
                        onSelectOption: plottingTypeChanged(value)
                    }
                }
                RowLayout{
                    id: groupByRow
                    spacing: 5
                    z: 8
                    visible: plotTypeComboBox.selectedUID !== '0'
                    VizLabel{
                        width: 100
                        text: "Group By"
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox{
                        id: groupByComboBox
                        width: 250
                        listModel: groupByListModel
                        value: groupByListModel.get(selectedUID).name
                        Layout.fillWidth: true
                    }
                }

                RowLayout{
                    id: styleRow
                    spacing: 5
                    z: 9
                    visible: false

                    VizLabel{
                        width: 100
                        text: "Annotation"
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox {
                        id: annotationComboBox
                        width: 250
                        listModel:(typeof(annotationListModel)=="undefined")?"undefined":annotationListModel
                        value: "<None>"
                        Layout.fillWidth: true
                        z:10
                    }
                }
            }
        }

        RowLayout{
            id:commandButtonRect
            spacing: 10
            Layout.minimumHeight: okButton.implicitHeight

            Rectangle{
                width: parent.width - okButton.width - cancelButton.width -20
                Layout.fillWidth: true
            }

            VizButton{
                id:okButton
                text: "   Ok   "
                width: 100
                onClicked:{
                    addIncidentLayer()
                    multipleQueryListView.currentIndex = 0
                    // showLegends();
                    //   if(show){
                    //     smpFileMenu.load("HistogramLegendPopup.qml")
                    // }
                }

            }

            /* VizButton{
                text: "Show Legends"
                onClicked: smpFileMenu.load("HistogramLegendPopup.qml")
            }*/

            VizButton{
                id:cancelButton
                text: "Cancel"
                width: 100
                onClicked: {
                    multipleQueryListView.currentIndex = 0
                    closePopup()
                }
            }
        }
    }
    onSelectedOptionChanged: {
        if(selectedOption === optionTimeEnd || selectedOption === optionTimeStart){
            openDatePicker()
        }
        else{
            closeDatePicker()
        }
    }

    function openDatePicker(){
        smpFileMenu.openDatePicker()
    }

    function closeDatePicker(){
        smpFileMenu.closeDatePicker()
    }
}
