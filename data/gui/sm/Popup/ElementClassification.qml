import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

//! Popup for DGN classification

Popup{
    id: elementClassificationId
    objectName: "elementClassification"
    title: "DGN Classification"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal okButtonClicked();
    //signal treeViewClicked();
    Component.onCompleted: {
        if(!running_from_qml){
            ElementClassificationGUI.setActive(true)
        }
    }

    onClosed: {
        if(!running_from_qml){
            ElementClassificationGUI.setActive(false)
        }
    }
    Component.onDestruction: {

        if(!running_from_qml){
            ElementClassificationGUI.setActive(false)
        }

    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }
    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        z:10
        VizGroupBox{
            id:elementClassificationGroupId
            title: "Classification"
            Layout.fillWidth: true
            Layout.fillHeight: true
            VizTreeView{
                id: elementClassificationTreeViewId
                anchors.fill: parent
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumWidth: 300*rootStyle.uiScale
                Layout.minimumHeight: 250*rootStyle.uiScale
                model:(typeof(elementClassificationTreeModel) !== "undefined") ? elementClassificationTreeModel : "undefined"
                VizTreeViewColumn{
                    role: "iconPath"
                    delegate: Image {
                        source: styleData.value
                        width: rootStyle.buttonIconSize
                        height: width
                    }
                }

                VizTreeViewColumn{
                    role: "name"
                    delegate: VizLabel {
                        id: nameDelegate
                        text: styleData.value
                        textColor: styleData.textColor
                    }
                }
            }
        }

        VizButton{
            id: okButtonId
            Layout.alignment: Qt.AlignCenter
            text: "    OK     "
            onClicked: {
                okButtonClicked();
                closePopup()
            }
        }
    }
}
