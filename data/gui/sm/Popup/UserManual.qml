import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: userManualID
    objectName: "UserManualObject"
    title: "User Manual"

    Component.onCompleted:
    {
        UserManualGUI.openUserManual()
    }
}

