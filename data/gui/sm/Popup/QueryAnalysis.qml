import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "VectorLayerQueryPopup"
    title: "Spatial Filter"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal changeLayerType(string value);
    signal addGroupedQuery()
    signal removeQueryFromList(int index)
    signal groupedQuerySelected(int index)
    signal editQueryGroup(string value)
    signal deleteSelectedQueryGroup(string value)
    signal apply();
    signal saveQueryGroup();
    signal runButtonClick();
    signal selectOrEditQuery(string value);
    signal deleteSelectedQuery(string value);
    signal okButtonClicked();
    signal clearArea()
    signal markPosition(bool checked)

    function openQueryBuilderPopup()
    {
        smpFileMenu.load("VectorQueryBuilder.qml")

    }
    property alias outputName: outputNameTextBox.text
    property alias queryGroupName:queryNameTextBox.text;
    property alias selectedLayerType:layerTypeComboBox.value;
    property alias selectedLayerName:layerComboBox.value;
    property alias selectedLayerUid:layerComboBox.selectedUID;
    property alias styleForSelectedLayer:styleTypeComboBox.value;
    property alias editQueryGroupButton:editButton.checked;
    property alias editQueryButton:editButtonId.checked;
    property alias selectedQueryName:queryNameComboBox.value;
    property alias selectedQueryGroupName: queryGroupBoxId.value

    property string whereClause
    property int previousGroupedQueryIndex: 0
    property string createdQueryName
    property bool openQueryGroupParameterGrid: false

    //properties
    property alias bottomLeftLongitude : bottomLeftLongitude.text
    property alias bottomLeftLatitude : bottomLeftLatitude.text
    property alias topRightLongitude : topRightLongitude.text
    property alias topRightLatitude : topRightLatitude.text
    property alias isMarkSelected : markButton.checked

    function queryBuilderOkClicked()
    {
        okButtonClicked();
    }
    onOpenQueryGroupParameterGridChanged:
    {
        if(!openQueryGroupParameterGrid)
        {
            markPosition(false);
        }
    }

    onPreviousGroupedQueryIndexChanged: multipleQueryListView.currentIndex = previousGroupedQueryIndex

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ListModel{
        id: layerTypeId
        ListElement{name:"Point"; uID : "0"}
        ListElement{name:"Line"; uID : "1"}
        ListElement{name:"Polygon"; uID : "2"}
        ListElement{name:"Unknown"; uID : "2"}
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        RowLayout{
            id:layerNameCol
            spacing: 5
            visible: !openQueryGroupParameterGrid
            VizLabel{
                width: 100
                text: "Output Name"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: outputNameTextBox
                Layout.minimumWidth: 300
                width: 300
                text: ""
                placeholderText: "Output Name..."
                Layout.fillWidth: true
                validator: rootStyle.nameValidator
            }
        }
        RowLayout{
            id:layerNameRow
            spacing: 5
            z: 60

            VizLabel{
                text: "Choose "
                width: 100
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: queryGroupBoxId
                    Layout.minimumWidth: 200
                    Layout.fillWidth: true
                    listModel: typeof(queryGroupList)== "undefined" ? "undefined" : queryGroupList
                    value: queryGroupList.get(selectedUID).name
                    onSelectOption: {
                        editQueryGroup(value)
                    }

            }

            VizButton{
                id:editButton
                Layout.minimumWidth: 50
                checkable: true
                text: "Edit"
                onClicked:
                        editQueryGroup(queryGroupBoxId.value)
            }

            VizButton{
                id:deleteButton
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 50
                text: " Delete "
                onClicked:
                {
                    deleteSelectedQueryGroup(queryGroupBoxId.value)
                }
            }
        }
        RowLayout{
            id:queryGroupCol
            spacing: 5
            visible: openQueryGroupParameterGrid
            VizLabel{
                width: 100
                text: "Query Group"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: queryNameTextBox
                Layout.minimumWidth: 300
                width: 300
                text: ""
                placeholderText: "Query Group Name..."
                Layout.fillWidth: true
                validator: rootStyle.nameValidator
            }
        }
        VizGroupBox{
            id : areaGroupBox
            Layout.fillWidth: true
            Layout.fillHeight: true
            title: "Extent"
            visible: openQueryGroupParameterGrid

            GridLayout{
                id: areaGrid
                columns: 3
                VizLabel{
                    text:"Bottom-Left"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: bottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "Latitude"
                    validator: rootStyle.longitudeValidator
                }
                VizTextField{
                    id: bottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "Longitude"
                    validator:rootStyle.latitudeValidator
                }
                VizLabel{
                    text:"Top-Right"
                    textAlignment: Qt.AlignLeft
                }

                VizTextField{
                    id: topRightLongitude
                    text: ""
                    placeholderText: "Latitude"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }

                VizTextField{
                    id: topRightLatitude
                    text: ""
                    placeholderText: "Longitude"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                Rectangle{
                    Layout.fillWidth: true
                }

                VizButton{
                    id: markButton
                    objectName: "markArea"
                    text:"Mark"
                    width: 100
                    backGroundVisible: true
                    checkable : true
                    anchors.right: clearButton.left
                    anchors.rightMargin: 10
                    onClicked: markPosition(checked)
                }
                VizButton{
                    id: clearButton
                    objectName: "clearArea"
                    text:"Clear"
                    backGroundVisible: true
                    width: 100
                    Layout.alignment: Qt.AlignRight
                    onClicked: {
                        clearArea();
                    }
                }

            }
        }
        VizGroupBox{
            title: "Vector Query Builder"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: sourceGrp.implicitHeight + margins
            Layout.minimumWidth: groupedQueryRow.implicitWidth + 20
            visible: openQueryGroupParameterGrid
            RowLayout{
                id: groupedQueryRow
                ColumnLayout{
                    id: groupedQueryCol
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Rectangle{
                        color: "transparent"
                        border.width: 2
                        border.color: rootStyle.colorWhenSelected
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.minimumWidth: 150*rootStyle.uiScale
                        ListView{
                            id: multipleQueryListView
                            currentIndex: 0
                            anchors.fill: parent
                            model: (typeof(queryNameList)=="undefined")?"undefined":queryNameList
                            spacing: 4
                            clip: true
                            focus: true
                            VizScrollBar{
                                target: multipleQueryListView
                                color: "transparent"
                                gradient: null
                            }
                            delegate: VizLabel{
                                id: label
                                text: modelData
                                textAlignment: Qt.AlignLeft
                                width: multipleQueryListView.width
                                Behavior on color {
                                    ColorAnimation { duration: 200 }
                                }
                                Behavior on textColor {
                                    ColorAnimation { duration: 200 }
                                }

                                textColor: (multipleQueryListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                                color: (multipleQueryListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                                MouseArea{
                                    anchors.fill: parent
                                    onClicked: {
                                        groupedQuerySelected(index)
                                    }
                                }
                            }
                        }
                    }
                    RowLayout{
                        id: groupQueryButtonRow
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        VizButton{
                            text: "Add"
                            onClicked: addGroupedQuery()
                        }
                        VizButton{
                            text: "Remove"
                            visible: multipleQueryListView.count > 0
                            onClicked: removeQueryFromList(multipleQueryListView.currentIndex)
                        }
                    }
                }


                ColumnLayout{
                    id: sourceGrp
                    RowLayout{
                        id: layerTypeCol
                        spacing: 5
                        z:50
                        VizLabel{
                            text: "Layer Type"
                            width: 100
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox {
                            id: layerTypeComboBox
                            listModel: (typeof(layerTypeId)=="undefined")?"undefined":layerTypeId
                            Layout.fillWidth: true
                            value: layerTypeId.get(selectedUID).name
                            onSelectedUIDChanged:changeLayerType(value)
                        }
                    }
                    RowLayout{
                        id: layerCol
                        spacing: 5
                        z:40
                        VizLabel{
                            text: "Layer"
                            width: 100
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox {
                            id: layerComboBox
                            listModel: (typeof(vectorLayerList)=="undefined")?"undefined":vectorLayerList
                            Layout.fillWidth: true
                        }
                    }
                    RowLayout{
                        id : queryName
                        spacing : 5
                        z:30;
                        VizLabel{
                            text: "Query Name:"
                            width: 100
                            textAlignment: Qt.AlignLeft
                        }

                        VizComboBox {
                            id: queryNameComboBox
                            listModel: (typeof(createdQueryNameList)=="undefined")?"undefined":createdQueryNameList
                            Layout.fillWidth: true
                            onSelectOption: selectOrEditQuery(value)
                        }
                        VizButton{
                            id:editButtonId
                            Layout.minimumWidth: 40
                            checkable: true
                            text: "Edit"
                            onClicked: {
                                if(checked)
                                selectOrEditQuery(queryNameComboBox.value)
                            }
                        }

                        VizButton{
                            id:deleteButtonid
                            Layout.alignment: Qt.AlignLeft
                            Layout.minimumWidth: 40
                            text: " Delete "
                            onClicked:
                            {
                                deleteSelectedQuery(queryNameComboBox.value)
                            }
                        }
                   }

                    RowLayout{
                        id: styleTypeCol
                        spacing: 5
                        z:25
                        VizLabel{
                            text: "Style"
                            width: 100
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox {
                            id: styleTypeComboBox
                            listModel: (typeof(styleSheetList)=="undefined")?"undefined":styleSheetList
                            Layout.fillWidth: true
                            value:styleSheetList.get(selectedUID).name
                        }
                    }

                }
            }
        }
        RowLayout{
            id:commandButtonRectid
            visible: openQueryGroupParameterGrid && multipleQueryListView.count > 0
            spacing: 10
            Layout.minimumHeight: okButton.implicitHeight
            z:-5

            Rectangle{
                width: parent.width - okButton.width - cancelButton.width -20
                Layout.fillWidth: true
            }

            VizButton{
                id:okButtonid
                text: "   Save   "
                width: 100
                onClicked:{
                    saveQueryGroup()
                    //multipleQueryListView.currentIndex = 0
                }

            }

            VizButton{
                id:cancelButtonid
                text: "Cancel"
                width: 100
                onClicked: {
                    multipleQueryListView.currentIndex = 0
                    editQueryButton=false;
                    openQueryGroupParameterGrid=false;
                    editQueryGroupButton=false;

                    //closePopup()
                }
            }
        }
        RowLayout{
            id:commandButtonRect
            spacing: 10
            Layout.minimumHeight: okButton.implicitHeight
            visible: !openQueryGroupParameterGrid
            Rectangle{
                width: parent.width - okButton.width - cancelButton.width -20
                Layout.fillWidth: true
            }

            VizButton{
                id:okButton
                text: "   Run   "
                width: 100
                onClicked:{
                    runButtonClick()
                    multipleQueryListView.currentIndex = 0
                }

            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                width: 100
                onClicked: {
                    multipleQueryListView.currentIndex = 0
                    closePopup()
                }
            }
        }
    }
}
