import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup {
    id: addExcelPopUp
    objectName: "AddExcelPopUp"

    title: "Add Excel/CSV File"

    width: mainLayout.implicitWidth+2*mainLayout.anchors.margins
    height: mainLayout.implicitHeight+2*mainLayout.anchors.margins

    minimumWidth: mainLayout.implicitWidth+10
    minimumHeight: mainLayout.implicitHeight+100

    property alias layerName:nameLabel.text
    property alias pathName :pathText.text
    property alias latitudeValue : latitudeList.value
    property alias longitudeValue : longitudeList.value
    property alias unitIndex : latlongUnit.selectedUID
    property alias hyperlinkvalue : hyperlink.value
    property alias hyperlinkListIndex : multipleHyperlinkListView.currentIndex
    property bool showAttributes : false
    property bool showHyperlinkOptions : false
    property string selectedFileName;
    property bool closeWindow: false
    signal addExcelFile(string pathName)
    signal browseButtonClicked()
    signal latitudeComboboxClicked()
    signal longitudeComboboxClicked()
    signal hyperlinkComboboxClicked(string value,bool add)
    signal selectHyperlinkFromList(int index)
    signal removeHyperlinkFromList(int index)

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            VizLabel {
                text: "Layer Name"
                width : 100
                textAlignment: Qt.AlignLeft
            }
            VizTextField {
                id: nameLabel
                Layout.minimumWidth: 300
                validator:rootStyle.nameValidator
                Layout.fillWidth: true
                color: "transparent"
                text: ""
                placeholderText:"Give a name to this layer..."
            }
        }

        RowLayout{
            VizLabel {
                id: pathLabel
                width : 100
                text: "Path"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id:pathText
                text:selectedFileName
                placeholderText: "Browse for data files..."
                Layout.fillWidth: true
            }
            VizButton{
                id:browse
                text: "Browse"
                onClicked:browseButtonClicked()
            }
        }

        RowLayout{
            id:longlat
            spacing:5
            z : 150
            visible : showAttributes
            VizLabel{
                text: "Latitude"
                width : 100
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: latitudeList
                    Layout.minimumWidth: 150
                    height: 28
                    listModel: typeof(attributeList)== "undefined" ? "undefined" : attributeList
                    value: latitudeList.get(selectedUID).name
                    onSelectOption: latitudeComboboxClicked()
            }

            VizLabel{
                text: "Longitude"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: longitudeList
                    Layout.minimumWidth: 150
                    height: 28
                    listModel: typeof(attributeList)== "undefined" ? "undefined" : attributeList
                    value: longitudeList.get(selectedUID).name
                    onSelectOption: longitudeComboboxClicked()
            }
        }

        RowLayout{
            visible : showAttributes
			z : 100
            VizLabel{
                width : 100
                text: "Unit"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: latlongUnit
                    Layout.minimumWidth: 150
                    height: 28
                    listModel: ListModel{
                        id: unitListModel
                        ListElement{name: "Degree" ; uID: 1}
                        ListElement{name: "Deg Min" ;  uID: 2}
                        ListElement{name: "Deg Min Sec" ; uID: 3}
                    }
                    value : "Degree"
            }
        }

        VizGroupBox{
            id:hyperlinkGroupbox
            title: "Hyperlink"
            checkable: false
            visible : showHyperlinkOptions && showAttributes
            width: mainLayout.implicitWidth+10
            height: mainLayout.implicitHeight + 50

            implicitHeight: mainLayout.implicitHeight + 10

            Layout.fillWidth: true

            RowLayout{
                id:hyperLinkRow
                spacing:5
                z : 100
                anchors.top : parent.top

                VizLabel{
                    text: "Hyperlinks"
                    width : 100
                    textAlignment: Qt.AlignLeft
                }

                VizComboBox{
                        id: hyperlink
                        Layout.minimumWidth: 150
                        listModel: typeof(hyperlinkList)== "undefined" ? "undefined" : hyperlinkList
                        value: hyperlink.get(selectedUID).name
                       // onSelectOption: hyperlinkComboboxClicked(value)
                }
            }

            ColumnLayout{
                id: addHyperlink
                Layout.fillHeight: true
                Layout.fillWidth: true
                Rectangle{
                    color: "transparent"
                    border.width: 2
                    border.color: rootStyle.colorWhenSelected
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 150*rootStyle.uiScale
                    ListView{
                        id: multipleHyperlinkListView
                        anchors.fill: parent
                        model: (typeof(hyperlinkNameList)=="undefined")?"undefined":hyperlinkNameList
                        spacing: 4
                        clip: true
                        focus: true
                        VizScrollBar{
                            target: multipleHyperlinkListView
                            color: "transparent"
                            gradient: null
                        }
                        delegate: VizLabel{
                            id: label
                            text: modelData
                            textAlignment: Qt.AlignLeft
                            width: multipleHyperlinkListView.width
                            Behavior on color {
                                ColorAnimation { duration: 200 }
                            }
                            Behavior on textColor {
                                ColorAnimation { duration: 200 }
                            }

                            textColor: (multipleHyperlinkListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                            color: (multipleHyperlinkListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                            MouseArea{
                                anchors.fill: parent
                                onClicked: {
                                    selectHyperlinkFromList(index)
                                }
                            }
                        }
                    }
                }
                RowLayout{
                    id: groupQueryButtonRow
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    VizButton{
                        text: "Add"
                        onClicked: hyperlinkComboboxClicked(hyperlink.value,true)
                    }
                    VizButton{
                        text: "Remove"
                        visible: multipleHyperlinkListView.count > 0
                        onClicked: removeHyperlinkFromList(hyperlinkListIndex)
                    }
                }
            }
        }

        RowLayout{
            Rectangle{
                Layout.fillWidth: true
            }
            VizButton
            {
                id:okButton
                text:"    Ok    "
                onClicked:addExcelFile(pathText.text)
            }
            VizButton
            {
                id:cancel
                text:"Cancel"
                onClicked:closePopup()
            }
        }
    }
}
