import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: queryBuilder
    objectName: "QueryBuilder"
    title: "Query Builder"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //properties
    property alias bottomLeftLongitude : bottomLeftLongitude.text
    property alias bottomLeftLatitude : bottomLeftLatitude.text
    property alias topRightLongitude : topRightLongitude.text
    property alias topRightLatitude : topRightLatitude.text
    property alias isMarkSelected : markButton.checked
    property alias sqlWhereText : sqlWhereTextId.text

    //Signal defined for buttons
    signal addToSelectedFields(int index)
    signal addToWhere(int index)
    signal markPosition(bool checked)
    signal operatorButtonClicked(string operator)
    signal queryButtonClicked()
    signal clearButtonClicked()
    signal cancelButtonClicked()
    signal okButtonClicked()
    signal selectedFieldDeletion(int index)

    function displayTable() {
        smpFileMenu.load("QueryBuilderTable.qml")
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            Layout.alignment: Qt.AlignCenter
            spacing: 5
            ColumnLayout{
                id: allFieldsColumnId
                VizLabel{
                    text: "All Fields"
                }

                Rectangle{
                    color: "transparent"
                    border.width: 2
                    border.color: rootStyle.colorWhenSelected
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 100*rootStyle.uiScale
                    Layout.minimumHeight: 150*rootStyle.uiScale
                    ListView{
                        id: allFieldsListView
                        objectName: "allFieldsListView"
                        currentIndex: -1
                        anchors.fill: parent
                        model: (typeof(allFieldsList) != "undefined") ? allFieldsList : "undefined"
                        spacing: 4
                        VizScrollBar{ target: allFieldsListView }
                        delegate: VizLabel{
                            id: label
                            text: modelData
                            Behavior on color {
                                ColorAnimation { duration: 200 }
                            }
                            Behavior on textColor {
                                ColorAnimation { duration: 200 }
                            }

                            textColor: (allFieldsListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                            color: (allFieldsListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                            MouseArea{
                                anchors.fill: parent
                                onClicked: allFieldsListView.currentIndex = index
                            }
                        }
                    }
                }
            }
            ColumnLayout{
                id: selectAndWhereButtonColumnId
                VizButton{
                    id: selectButtonId
                    text: "Add to SELECT"
                    onClicked: addToSelectedFields(allFieldsListView.currentIndex)
                }
                VizButton{
                    id: whereButtonId
                    text: "Add to WHERE "
                    onClicked: addToWhere(allFieldsListView.currentIndex)
                }
            }
            ColumnLayout{
                id: selectedFieldsColumnId
                VizLabel{
                    text: "Selected Fields"
                }
                Rectangle{
                    color: "transparent"
                    border.width: 2
                    border.color: rootStyle.colorWhenSelected
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 100*rootStyle.uiScale
                    Layout.minimumHeight: 150*rootStyle.uiScale
                    ListView{
                        id: selectedFieldsListView
                        objectName: "selectedFieldsListView"
                        currentIndex: -1
                        anchors.fill: parent
                        model: (typeof(selectedFieldsList) != "undefined") ? selectedFieldsList : "undefined"
                        spacing: 4
                        VizScrollBar{ target: selectedFieldsListView }
                        delegate: VizLabel{
                            text: modelData
                            Behavior on color {
                                ColorAnimation { duration: 200 }
                            }
                            Behavior on textColor {
                                ColorAnimation { duration: 200 }
                            }

                            color: (allFieldsListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"

                            textColor: (allFieldsListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                            MouseArea{
                                anchors.fill: parent
                                onClicked: allFieldsListView.currentIndex = index
                            }
                        }
                    }
                }
            }
        }

        VizGroupBox{
            id : areaGroupBox
            objectName: "Area"
            Layout.fillWidth: true
            Layout.fillHeight: true
            title: "Extent"
            GridLayout{
                id: areaGrid
                columns: 3
                VizLabel{
                    text:"Bottom-Left"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: bottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "bottom"
                    validator: rootStyle.longitudeValidator
                }
                VizTextField{
                    id: bottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "left"
                    validator:rootStyle.latitudeValidator
                }
                VizLabel{
                    text:"Top-Right"
                    textAlignment: Qt.AlignLeft
                }

                VizTextField{
                    id: topRightLongitude
                    text: ""
                    placeholderText: "top"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }

                VizTextField{
                    id: topRightLatitude
                    text: ""
                    placeholderText: "right"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                Item{
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                }
                VizButton{
                    id: markButton
                    objectName: "markArea"
                    text:"    Mark    "
                    backGroundVisible: true
                    checkable : true
                    Layout.alignment: Qt.AlignRight
                    onClicked: if(checked ) markPosition(true)
                }
            }
        }

        VizGroupBox{
            id: operatorsId
            title: "Operators"
            Layout.fillWidth: true
            Layout.fillHeight: true

            GridLayout{
                id: operatorsGridId
                columns: 7
                VizButton{
                    text: "     =     "
                    onClicked: operatorButtonClicked("= ");
                }
                VizButton{
                    text: "     <     "
                    onClicked: operatorButtonClicked("< ");
                }
                VizButton{
                    text: "     >     "
                    onClicked: operatorButtonClicked("> ");
                }
                VizButton{
                    text: "   LIKE   "
                    onClicked: operatorButtonClicked("LIKE ");
                }
                VizButton{
                    text: "     %      "
                    onClicked: operatorButtonClicked("% ");
                }
                VizButton{
                    text: "     IN    "
                    onClicked: operatorButtonClicked("IN ");
                }
                VizButton{
                    text: " NOT IN  "
                    onClicked: operatorButtonClicked("NOT IN ");
                }
                VizButton{
                    text: "    <=    "
                    onClicked: operatorButtonClicked("<= ");
                }
                VizButton{
                    text: "    >=    "
                    onClicked: operatorButtonClicked(">= ");
                }
                VizButton{
                    text: "     !=    "
                    onClicked: operatorButtonClicked("!= ");
                }
                VizButton{
                    text: "   ILIKE  "
                    onClicked: operatorButtonClicked("ILIKE ");
                }
                VizButton{
                    text: "     AND   "
                    onClicked: operatorButtonClicked("AND ");
                }
                VizButton{
                    text: "    OR   "
                    onClicked: operatorButtonClicked("OR ");
                }
                VizButton{
                    text: "   NOT    "
                    onClicked: operatorButtonClicked("NOT ");
                }
            }
        }

        VizGroupBox{
            id: sqlWhereGroupId
            title: "SQL Where Clause"
            Layout.fillWidth: true
            Layout.fillHeight: true
            VizTextEdit{
                id: sqlWhereTextId
                placeholderText: "enter where clause"
                anchors.fill: parent
                anchors.margins: 10
                Layout.minimumHeight: 100
                onTextChanged: cursorPosition = sqlWhereTextId.text.length
            }
        }

        RowLayout{
            id: controlCommandId
            spacing: 5
            Layout.alignment: Qt.AlignRight
            VizButton{
                text: "Run Query"
                onClicked:queryButtonClicked()
            }

            VizButton{
                text: "    Clear    "
                onClicked: clearButtonClicked()
            }
            VizButton{
                text: "  Cancel  "
                onClicked:{
                    cancelButtonClicked()
                    closePopup()
                }
            }
            VizButton{
                text: "     OK     "
                onClicked:{
                    okButtonClicked()
                    closePopup()
                }
            }
        }
    }
}
