import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: skippedFileList
    objectName: "SkippedFileList"
    title: "Skipped File List"
    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight + 10

    property bool selectedSkippedFiles: false

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout
    {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        //Layout.minimumHeight: 50
        Layout.minimumHeight : labels.Layout.minimumHeight
        RowLayout{
            id:labels
            ColumnLayout
            {
                VizLabel
                {
                    text:"Some files skipped because of different DataSet value"
                }
                RowLayout{
                    VizLabel
                    {
                        text:"Show These Files"
                    }
                    VizCheckBox{
                        id:showSkippedFiles
                        checked: false
                        onCheckedChanged: {
                            selectedSkippedFiles = checked
                            if(checked)
                                mainLayout.Layout.minimumHeight = skippedLayerID.Layout.minimumHeight + labels.Layout.minimumHeight
                            else
                                mainLayout.Layout.minimumHeight = labels.Layout.minimumHeight
                        }
                    }
                }
            }
        }
        VizTableView {
            id: skippedLayerID
            model: typeof( skippedlayerModel) !== "undefined" ? skippedlayerModel : "undefined"
            Layout.fillWidth: true
            Layout.fillHeight: true
            focus: true
            clip: true
            visible:selectedSkippedFiles
            Layout.minimumHeight: 300
            Layout.minimumWidth: 300
            onVisibleChanged: {

            }
            TableViewColumn{
                title: "Sr. No.";
                role: "srno";
                elideMode: Text.ElideMiddle
            }
            TableViewColumn{
                title: "Name";
                role: "name";
                elideMode: Text.ElideMiddle
            }
        }
    }
}
