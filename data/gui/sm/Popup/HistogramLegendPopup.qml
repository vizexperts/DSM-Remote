import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."


Popup{
    id:  histogramLegendPopup
    objectName: "histogramLegendPopup"
    title: "Layer Legend"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal changeHistColor(color newColor, int index)

    function colorChanged(){
        var obj = smpFileMenu.getColorPicker()

        changeHistColor(obj.currentColor, legendListModel[legendListView.currentIndex].legendIndex)
        //legendColor = obj.currentColor

        if(legendListModel !== "undefined"){
            legendListModel[legendListView.currentIndex].legendColor = obj.currentColor
        }

        legendListView.currentIndex = -1
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        ListView{
            id:  legendListView
            clip: true
            model: (typeof(legendListModel)=="undefined")?"undefined": legendListModel
            currentIndex: -1
            VizScrollBar{ target: legendListView }
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 200*rootStyle.uiScale
            Layout.minimumWidth: 200*rootStyle.uiScale

            delegate: Row{
                height: children.implicitHeight
                width: legendListView.width
                Layout.fillWidth: true
                VizButton{
                    backgroundColor: legendColor
                    flatButton: true
                    onClicked: {
                        if(legendListView.currentIndex == index){
                            colorChanged()
                            //colorPicker.scale = 0
                            smpFileMenu.closeColorPicker()
                            legendListView.currentIndex = -1
                        }
                        else{
                            //colorPicker.currentColor = legendColor
                            smpFileMenu.setColor(legendColor)
                            //colorPicker.scale = 1
                            smpFileMenu.openColorPicker()
                            legendListView.currentIndex = index
                        }
                    }
                }
                VizLabel{
                    text: legendName
                    Layout.fillWidth: true
                    Behavior on color {
                        ColorAnimation { duration: 200 }
                    }
                    Behavior on textColor {
                        ColorAnimation { duration: 200 }
                    }

                    textColor: (legendListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                    color: (legendListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                }
            }
        }
    }
}
