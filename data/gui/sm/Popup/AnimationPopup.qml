import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQml.Models 2.1
import QtQuick.Dialogs 1.2
import "../VizComponents"
import ".."
Popup {
    id: animationGUI
    objectName: "AnimationGUI"
    title: "Add Animation Data"
    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins
    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property bool animationRunning : false
    property string currentItemName : "Dummy Item Name"
    property string rampFile
    property double speedFactor : 1
    property bool generateColor : !(rampFilesComboB.value == "SELECT")
    signal addToAnimationList(int index)
    signal addToRampFileList(string filename)
    signal removeFromAnimationList(int index)
    signal moveInAnimationList(int from, int to)
    signal playAnimation()
    signal pauseAnimation()
    signal stopAnimation()
    signal loopAnimation(bool checked)
    signal changeAnimationSpeed(double factor)
    signal saveAnimation(string name)
    signal loadAnimation(string name)
    Component.onDestruction: {
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }
    FileDialog {
            id: fileDialog
            title: "Choose a file"
            nameFilters: [ "Ramp files (*.txt)" ]
            onAccepted: {
                console.log("Accepted: " + fileUrl)
                var path = fileUrl.toString();
                // remove prefixed "file:///"
                path= path.replace(/^(file:\/{3})|(qrc:\/{2})|(http:\/{2})/,"");
                // unescape html codes like '%23' for '#'
                var cleanPath = decodeURIComponent(path);
                addToRampFileList(cleanPath);
            }
            onRejected: { console.log("Rejected") }
        }

    Component {
        id: dragDelegate
        MouseArea {
            id: dragArea
            property bool held: false
            anchors { left: parent.left; right: parent.right }
            height: content.height
            drag.target: held ? content : undefined
            drag.axis: Drag.YAxis
            onPressed: animationListView.currentIndex = index
            onPressAndHold: {
                held = true
                from = dragArea.DelegateModel.itemsIndex
            }
            onReleased: {
                if(held){
                    held = false
                    if(from !== dragArea.DelegateModel.itemsIndex){
                        moveInAnimaitonList(from, dragArea.DelegateModel.itemsIndex)
                    }
                }
            }
            //!
            property int from: 0
            Rectangle {
                id: content
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                width: dragArea.width; height: label.height + 4
                border.width: 1
                border.color: "lightsteelblue"
                color: dragArea.held ? "lightsteelblue" : (animationListView.currentIndex == index)
                                       ? rootStyle.selectionColor : "transparent"
                Behavior on color { ColorAnimation { duration: 100 } }
                radius: 2
                Drag.active: dragArea.held
                Drag.source: dragArea
                Drag.hotSpot.x: width / 2
                Drag.hotSpot.y: height / 2
                states: State {
                    when: dragArea.held
                    ParentChange { target: content; parent: root }
                    AnchorChanges {
                        target: content
                        anchors { horizontalCenter: undefined; verticalCenter: undefined }
                    }
                }
                VizLabel{
                    id: label
                    text: modelData
                    textColor: (animationListView.currentIndex == index) ? rootStyle.selectedTextColor : rootStyle.textColor
                }
            }
            DropArea {
                anchors { fill: parent; margins: 10 }

                onEntered: {
                    visualModel.items.move(
                                drag.source.DelegateModel.itemsIndex,
                                dragArea.DelegateModel.itemsIndex)
                }
            }
        }
    }
    DelegateModel{
        id: visualModel
        objectName: "delegateModel"
        model: (typeof(animationList) != "undefined")? animationList : "undefined"
        delegate: dragDelegate
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5
        RowLayout{
            id: openAnimation

            VizLabel {
                id : addNewAnimationLabel
                text: "Create "

            }
            VizButton {
                id : addNewAnimation

                text: "+"
                onClicked: {
                    animationNameLayout.visible = true
                }
            }

            VizLabel {
                id : openAnimationLabel
                text: "Open "
                anchors.leftMargin: 20
                anchors.left : addNewAnimation
            }

            VizComboBox {
                id: previouslySaved
                anchors.leftMargin: 20
                anchors.right : parent.right
                listModel: (typeof(savedAnimationFiles) != "undefined")? savedAnimationFiles : "undefined"
                Layout.fillWidth: true
                onSelectedUIDChanged:
                {
                    animationNameLayout.visible = false
                    loadAnimation(selectedUID)
                }
            }
        }

        RowLayout{
            id: animationNameLayout
            spacing: 5
            visible: false
            VizLabel{
                id : createNewLabel
                text: "Create New"
                width : 100
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: animationName
                width : 200
                anchors.left : createNewLabel.right
                anchors.rightMargin: 20
                text: ""
                validator: rootStyle.nameValidator
                placeholderText: "Name"
                Layout.fillWidth: true
            }
            VizLabel{
                id : rampFileLabel
                text: "Ramp File"
                anchors.left : animationName.right
                anchors.leftMargin: 20
                width : 100
                textAlignment: Qt.AlignLeft
            }
            VizComboBox {
                objectName: "rampFilesComboBoxObject"
                id: rampFilesComboB
                anchors.right : addNewRamp.left
                anchors.leftMargin: 20
                value : "SELECT"
                Layout.fillWidth: true
                onFocusChanged: {
                    if (typeof(rampFiles) != "undefined"){
                        rampFilesComboB.listModel = rampFiles
                    }
                    else{
                        rampFilesComboB.listModel =  "undefined"
                    }
                }
                onSelectedUIDChanged: {

                    rampFile = selectedUID
                    console.log("value : "+ rampFile)
                }
            }
            VizButton {
                id : addNewRamp
                anchors.right: parent.right
                anchors.leftMargin: 20
                text: "+"
                onClicked: fileDialog.open()
            }


        }
        RowLayout{
            id: packageRow
            Layout.fillWidth: true
            spacing: 5
            anchors.bottom: animationControlsGB.top
            anchors.top: (animationNameLayout.visible) ? animationNameLayout.bottom : openAnimation.bottom

            ColumnLayout{
                id: residualColumn
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                VizLabel{
                    id: availText
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Available"
                }
                Rectangle{
                    Layout.fillWidth: true
                    anchors.top: availText.bottom
                    anchors.bottom: parent.bottom
                    color: "black"
                    ListView{
                        id: listView
                        currentIndex: -1
                        objectName: "listview"
                        anchors.fill: parent
                        model: (typeof(availableList) != "undefined")? availableList : "undefined"
                        spacing: 4
                        highlight: Rectangle{
                            width: 200
                            height: label.height
                            color: rootStyle.selectionColor
                        }
                        delegate: VizLabel{
                            id: label
                            text: modelData
                            textColor: (listView.currentIndex == index) ? rootStyle.selectedTextColor : rootStyle.textColor
                            MouseArea{
                                anchors.fill: parent
                                onClicked: listView.currentIndex = index
                            }
                        }
                    }
                }
            }
            ColumnLayout{
                id: buttonLayout
                anchors.verticalCenter: parent.verticalCenter
                VizButton{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "<"
                    enabled: (animationListView.currentIndex != -1)
                    onClicked: removeFromAnimationList(animationListView.currentIndex)
                }
                VizButton{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: ">"
                    onClicked: addToAnimationList(listView.currentIndex)
                }
                VizButton{
                    anchors.horizontalCenter: parent.horizontalCenter
                    iconSource: rootStyle.getIconPath("accept")
                    scale: 0.72
                    onClicked: {
                        saveAnimation(animationName.text)
                    }
                }
            }
            ColumnLayout{
                id: animationColumn
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                VizLabel{
                    id: animText
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Animation"
                }
                Rectangle{
                    Layout.fillWidth: true
                    anchors.top: animText.bottom
                    anchors.bottom: parent.bottom
                    color: "black"
                    ListView{
                        id: animationListView
                        anchors.fill: parent
                        currentIndex: -1
                        model: visualModel
                        spacing: 4
                    }
                }
            }
        }
        VizGroupBox{
            id : animationControlsGB
            title: "controls"
            height: 100
            onHeightChanged: {
                height = 100
            }

            anchors.bottom: parent.bottom

            Row{
                id: animationControls
                anchors.horizontalCenter: parent.horizontalCenter
                property int  horizontalCenterOffset: 10
                anchors.horizontalCenterOffset: horizontalCenterOffset
                Behavior on horizontalCenterOffset {NumberAnimation{duration: 200}}
                visible: (currentItemName != "")
                anchors.top: parent.top
                anchors.topMargin: 10
                spacing: 10
                VizButton{
                    id: slowButton
                    iconSource: rootStyle.getIconPath("time/slower")
                    backGroundVisible: false
                    onClicked: {
                        if (speedFactor == 1)
                        {
                            speedFactor = -2
                            changeAnimationSpeed(speedFactor)
                        }
                        else
                        {
                            speedFactor = speedFactor -1
                            changeAnimationSpeed(speedFactor)
                        }

                    }
                }
                VizButton{
                    id: playPauseButton
                    iconSource: animationRunning ?  rootStyle.getIconPath("time/pause") : rootStyle.getIconPath("time/play")
                    backGroundVisible: false
                    checked : false
                    onClicked: {
                        if(animationRunning){
                            animationRunning = false
                            //console.log("pause()")
                            pauseAnimation()
                        }
                        else{
                            animationRunning = true
                            //console.log("play()")
                            playAnimation()
                        }
                    }
                }
                VizButton{
                    id:stopButton
                    iconSource: rootStyle.getIconPath("time/stop")
                    backGroundVisible: false
                    onClicked: {
                        stopAnimation()
                    }
                }
                VizButton{
                    id:fastButton
                    iconSource: rootStyle.getIconPath("time/faster")
                    backGroundVisible: false
                    onClicked: {
                        if (speedFactor == -2)
                        {
                            speedFactor = 1
                            changeAnimationSpeed(speedFactor)
                        }
                        else
                        {
                            speedFactor = speedFactor + 1
                            changeAnimationSpeed(speedFactor)
                        }
                    }
                }
                VizLabel{
                    id: loopLabel
                    text: "Loop"
                }
                VizCheckBox{
                    id:loopCheckBox
                    checked : false
                    onCheckedChanged:
                    {
                        loopAnimation(checked)
                    }
                }
                VizLabel{
                    id: speedFactorLabel
                    text: "Speed:"
                }
                VizLabel{
                    id: speedFactorValueLabel
                    width: 50
                    text: speedFactor + "X"
                    textAlignment: Qt.AlignLeft
                }
            }
        }
    }
}
