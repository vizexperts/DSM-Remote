import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup {
    id: roadNetworkPopup
    objectName: "roadNetworkPopup"
    title: "Road Network Analysis"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 50
    minimumHeight: mainLayout.Layout.minimumHeight + 50

    property alias startText: startTextLabel.text
    property alias endText: endTextLabel.text
    property alias outputLayer: layerId.text
    property alias styleName: styleTypeBox.value

    signal prepare()
    signal markStart(bool value)
    signal markEnd(bool value)
    signal selectDatabase(string value)
    signal setStyle(string value)
    signal compute()

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            id: outputLayer
            spacing: 5
            VizLabel{
                text: "Output Layer"
                Layout.alignment: Qt.AlignLeft
            }

            VizTextField{
                id: layerId
                Layout.fillWidth: true
                text: ""
                placeholderText: "Route Result Name"
            }
        }

        RowLayout{
            id: startRow
            spacing: 5
            VizLabel{
                text: "Start Point   "
                Layout.alignment: Qt.AlignLeft
            }
            VizButton{
                id: startMarkButton
                text: "Mark"
                Layout.alignment: Qt.AlignLeft
                onClicked: {
                    if(endMarkButton.checked){
                        endMarkButton.checked = false
                        markEnd(false)
                    }
                    checked = !checked
                    markStart(checked)
                }
            }

            VizLabel{
                id: startTextLabel
                Layout.alignment: Qt.AlignLeft
                text: "No Value"
            }
        }
        RowLayout{
            id: endRow
            spacing: 5
            VizLabel{
                id: endLabel
                text: "End Point    "
                Layout.alignment: Qt.AlignLeft
            }
            VizButton{
                id: endMarkButton
                text: "Mark"
                Layout.alignment: Qt.AlignLeft
                onClicked: {
                    if(startMarkButton.checked){
                        startMarkButton.checked = false
                        markStart(false)
                    }
                    checked = !checked
                    markEnd(checked)
                }
            }
            VizLabel{
                id: endTextLabel
                Layout.alignment: Qt.AlignLeft
                text: "No Value"
            }
        }

        RowLayout{
            id: database
            spacing: 5
            z: 100
            VizLabel{
                id: databaseType
                text: "Database    "
                Layout.alignment: Qt.AlignLeft
            }

            VizComboBox{
                id: databaseTypeBox
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                listModel: typeof(databaseList)== "undefined" ? "undefined" : databaseList
                value: databaseTypeBox.get(selectedUID).name

                onSelectOption: {
                    selectDatabase(value)
                }
            }

        }

        RowLayout{
            id: style
            spacing: 5
            z : 10
            VizLabel{
                id: styleType
                text: "Styling        "
                Layout.alignment: Qt.AlignLeft
            }

            VizComboBox{
                id: styleTypeBox
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                listModel: typeof(styleList)== "undefined" ? "undefined" : styleList
                value: styleTypeBox.get(selectedUID).name

                onSelectOption: {
                    setStyle(value)
                }
            }

        }

        RowLayout{
            id: prepareRow
            spacing: 25
            z : -10
            Layout.alignment: Qt.AlignHCenter
            VizButton{
                text: "Compute"
                onClicked:
                {
                    if(endMarkButton.checked){
                        endMarkButton.checked = false
                        markEnd(false)
                    }

                    if(startMarkButton.checked){
                        startMarkButton.checked = false
                        markStart(false)
                    }
                    compute()
                }
            }
        }
    }
}
