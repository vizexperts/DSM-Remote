import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."


Popup {
    id:getTiffFromExtentsPopUp
    objectName: "getTiffFromExtent"
    title: "Download Raster"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 40
    minimumHeight: mainLayout.Layout.minimumHeight +150


    //Properties associted with parameters
    property alias value: serverList.value

    //Properties associated with buttons
    property alias connectButtonEnabled: connectButton.enabled
    property alias addButtonEnabled: addButton.enabled
    property alias wmsTreeVisible:layerTreeView.visible
    property alias outputDirectory:outputDirectoryField.text
    property bool treeVisible

    // Area Properties
    //properties
    property alias bottomLeftLongitude : bottomLeftLongitude.text
    property alias bottomLeftLatitude : bottomLeftLatitude.text
    property alias topRightLongitude : topRightLongitude.text
    property alias topRightLatitude : topRightLatitude.text
    property alias isMarkSelected : markButton.checked

    signal markPosition(bool checked)
    signal clearArea()

    signal connectButtonClicked()
    signal exportButtonClicked()
    signal browseButtonClicked()

    Component.onDestruction: {
        if(!running_from_qml){
            DownloadRasterGUI.reset()
            treeVisible=false
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{
            id:pathRow
            spacing:5
            VizLabel{
                text: "Choose Server"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                id: serverList
                Layout.minimumWidth: 150
                Layout.fillWidth: true
                listModel: typeof(georbISServerList)== "undefined" ? "undefined" : georbISServerList
                value: serverList.get(selectedUID).name
            }

            VizButton{
                id:connectButton
                visible : serverList.value != "Custom"
                text:"Connect"
                Layout.alignment: Qt.AlignRight
                onClicked:connectButtonClicked();
            }
        }

        VizTreeView{
            id: layerTreeView
            z : -10
            objectName: "layerTreeView"
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: treeVisible
            width: parent.width
            height: 200
            model:(typeof(getTiffTreeModel)!== "undefined") ? getTiffTreeModel : "undefined"

            onModelChanged: {
                if((model === null)||(model === "undefined"))
                    treeVisible = false
                else
                    treeVisible = true
            }

            VizTreeViewColumn{
                role: "name"
                delegate:VizLabel{
                    text:styleData.value
                    textColor:rootStyle.textColor
                }
            }
            VizTreeViewColumn{
                role: "title"
                delegate:VizLabel{
                    text:styleData.value
                    textColor:rootStyle.textColor
                }
            }
            VizTreeViewColumn{
                role: "abstraction"
                delegate:VizLabel{
                    text:styleData.value
                    textColor:rootStyle.textColor
                }
            }
        }

        VizGroupBox{
            id : areaGroupBox
            title: "Extent"
            GridLayout{
                id: areaGrid
                columns: 3
                VizLabel{
                    text:"Bottom-Left"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: bottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "Latitude"
                    validator: rootStyle.longitudeValidator
                }
                VizTextField{
                    id: bottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "Longitude"
                    validator:rootStyle.latitudeValidator
                }
                VizLabel{
                    text:"Top-Right"
                    textAlignment: Qt.AlignLeft
                }

                VizTextField{
                    id: topRightLongitude
                    text: ""
                    placeholderText: "Latitude"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }

                VizTextField{
                    id: topRightLatitude
                    text: ""
                    placeholderText: "Longitude"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                Rectangle{
                    Layout.fillWidth: true
                }

                VizButton{
                    id: markButton
                    objectName: "markArea"
                    text:"Mark"
                    width: 100
                    backGroundVisible: true
                    checkable : true
                    anchors.right: clearButton.left
                    anchors.rightMargin: 10
                    onClicked: markPosition(checked)
                }
                VizButton{
                    id: clearButton
                    objectName: "clearArea"
                    text:"Clear"
                    backGroundVisible: true
                    width: 100
                    Layout.alignment: Qt.AlignRight
                    onClicked: {
                        clearArea();
                    }
                }
            }
        }

        RowLayout{
            id: filePathRow
            spacing: 10

            VizLabel{
                text: "Output Directory"
            }
            VizTextField{
                id: outputDirectoryField
                placeholderText: "Output Directory Path"
                Layout.fillWidth: true
            }

            VizButton{
                text: "Browse"

                onClicked: {
                    browseButtonClicked()
                }
            }
        }

        RowLayout{
            id:controlButton
            spacing:10
            Layout.alignment: Qt.AlignRight
            VizButton{
                id:addButton
                text: "Export"
                onClicked: exportButtonClicked()
            }
        }
    }
}
