import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"
import ".."

Popup {
    id: saveAsProject
    objectName: "saveAsProject"
    title: "Save As"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth + 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.implicitWidth +10
    minimumHeight: mainLayout.implicitHeight +10

    signal projectSaveAs(string name)

    property alias projectName:nameText.text
    property alias fileNameToFill: nameText.text

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            VizLabel {
                id:nameLabel
                text:"Name"
            }
            VizTextField {
                id:nameText
                text:""
                Layout.minimumWidth: 300
                placeholderText: "Type project name here"
                validator: rootStyle.nameValidator
                Layout.fillWidth: true
            }
        }

        RowLayout {
            id:buttonRow
            Layout.alignment: Qt.AlignCenter
            VizButton {
                id: button
                text:"Save"
                visible: true
                onClicked: projectSaveAs(nameText.text)
            }

            VizButton {
                id: cancelButton
                text:"Cancel"
                onClicked: closePopup()
            }
        }
    }
}
