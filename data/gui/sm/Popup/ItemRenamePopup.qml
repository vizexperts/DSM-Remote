import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: itemRenamePopup
    objectName: "itemRenamePopup"

    title: "Rename Object"

    /*
        \signal renameObject(int index, string name)
        \brief Rename object at index to given name.
        \param index element index
        \param name New name
      */
    signal renameObject(int index, string name)

     height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10
    property alias itemName: renameFieldId.text
    property int currentIndex: -1;

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            Layout.fillWidth: true
            z:1

            VizLabel{
                text: "Item Name"
            }

            VizTextField{
                id: renameFieldId
                Layout.minimumWidth : 200
                Layout.fillWidth: true
                text: ""
                placeholderText: ""
             }

        }
        RowLayout{
            Layout.fillWidth: true
            Rectangle{
                Layout.fillWidth  : true
            }
            VizButton{
                id: okButtonId
                text: "   OK   "
                onClicked: {
                    renameObject(currentIndex,itemName);
                }
                Layout.alignment: Qt.AlignCenter
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: cancelButtonId
                text: " Cancel "
                onClicked: closePopup()
                Layout.alignment: Qt.AlignCenter

            }
            Rectangle{
                Layout.fillWidth: true
            }
        }
    }
}

