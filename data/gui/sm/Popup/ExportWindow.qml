import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id:exportLayer
    objectName: "ExportWindow"
    title: "Export Layer"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //property associated with parameters

    property alias fileName:sourceFile.text

    //Signal defined for buttons
    signal browseButtonClicked()
    signal exportButtonClicked()
    signal cancelButtonClicked()

    //states for button
    property alias isexportEnabled : exportButton.enabled
    property alias isCancelEnabled : cancel.enabled
    property alias isBrowseEnabled : browse.enabled

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            spacing: 5
            VizLabel{ text: "FilePath" }
            VizTextField{
                id:sourceFile
                Layout.minimumWidth:  300
                placeholderText: "Browse for filename "
                text : ""
                Layout.fillWidth: true
            }
            VizButton{
                id:browse
                text: "Browse"
                onClicked:browseButtonClicked()
              }

        }

        RowLayout{
            id: lastrow
            Layout.alignment: Qt.AlignRight
            VizButton{
                id: exportButton
                text: " Export "
                onClicked: exportButtonClicked()
            }
            VizButton{
                id:cancel
                text: " Cancel "
                onClicked: closePopup()
            }
        }
    }
}
