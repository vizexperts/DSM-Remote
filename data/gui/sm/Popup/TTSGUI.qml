import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: talkToSpeechMenuId;
    objectName: "textToSpeechMenuObject";

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal qmlHandleBrowseButtonClicked();
    signal qmlHandlePlayButtonClicked();
    signal qmlHandleStopButtonClicked();
    signal qmlHandleCloseButtonClicked();

    property alias ttsFileLE: ttsFileLEId.text
    property alias stopPEnabled:ttsStopPBId.enabled
    property alias browsePBEnabled: ttsBrowsePBId.enabled
    property alias playPBtext: ttsPlayPBId.text

    title:"Text To Speech";
    Component.onDestruction:
    {
        //flag set false
        smpFileMenu.isPopUpAlreadyOpen=false;
        qmlHandleStopButtonClicked();
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    function browserAccepted()
    {
        qmlHandleBrowseButtonClicked(fileBrowser.fileUrl)
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout {
            VizLabel{
                text:"Text File"
            }
            VizTextField{
                id: ttsFileLEId
                Layout.minimumWidth: 250
                placeholderText: "Browse for file";
                color: "transparent"
                Layout.fillWidth: true
            }
            VizButton{
                id: ttsBrowsePBId
                text: "Browse"
                onClicked: qmlHandleBrowseButtonClicked()
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            VizButton {
                id: ttsPlayPBId
                text: "    Play    ";
                onClicked: {
                    qmlHandlePlayButtonClicked();
                }
            }
            VizButton {
                id: ttsStopPBId;
                text: "    Stop    ";
                enabled:  false
                onClicked: {
                    qmlHandleStopButtonClicked();
                }
            }
            VizButton {
                id: ttsClosePBId
                text: "   Close   ";
                onClicked: {
                    qmlHandleCloseButtonClicked();
                    closePopup()
                }
            }
        }
    }
}




