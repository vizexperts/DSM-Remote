import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "addDatabaseLayerPopup"
    title: "Add New Database Vector Layer"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10


    signal userspaceChanged(string userspaceName)
    signal addOracleTable()
    signal openQueryBuilder()

    onOpenQueryBuilder: {
        smpFileMenu.load("QueryBuilderGIS.qml")
    }

    property alias selectedUserspaceName: userspaceComboBox.value
    property alias selectedTableName: tablespaceComboBox.value
    property alias uidColumnName: columnspaceComboBox.value
    property alias whereClause: whereClauseTextField.text

    property string bottomLeftLong
    property string bottomLeftLat
    property string topRightLong
    property string topRightLat

    property alias layerName: layerNameTextField.text

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        RowLayout{
            id:layerNameRow
            spacing: 5
            VizLabel{
                text: "Layer Name"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: layerNameTextField
                Layout.minimumWidth: 250*rootStyle.uiScale
                text: ""
                placeholderText: "Enter Layer Name..."
                Layout.fillWidth: true
            }
        }

        VizGroupBox{
            title: "Data source"
            Layout.fillWidth: true
            z:200
            ColumnLayout{
                width: parent.width
                Layout.fillWidth: true
                RowLayout{
                    id: userSpaceRow
                    spacing: 5
                    z: 250
                    Layout.fillWidth: true
                    VizLabel{
                        text: "Userspace "
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox{
                        id: userspaceComboBox
                        Layout.fillWidth: true
                        listModel: (typeof(dbUserspaceListModel)=="undefined")?"undefined":dbUserspaceListModel
                        onSelectOption: userspaceChanged(value)
                    }
                }

                RowLayout{
                    id: tablenameRow
                    spacing: 5
                    z: 240
                    Layout.fillWidth: true
                    VizLabel{
                        text: "Table Name "
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox{
                        id: tablespaceComboBox
                        Layout.fillWidth: true
                        listModel: (typeof(dbTablenameListModel)=="undefined")?"undefined":dbTablenameListModel
                        onSelectOption: tablenameChanged(value)
                    }
                }
                RowLayout{
                    id: columnnameRow
                    spacing: 5
                    z: 230
                    Layout.fillWidth: true
                    VizLabel{
                        text: "Unique ID"
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox{
                        id: columnspaceComboBox
                        Layout.fillWidth: true
                        listModel: (typeof(dbColumnNameListModel)=="undefined")?"undefined":dbColumnNameListModel
                        value: "<None>"
                    }
                }
            }
        }
        VizGroupBox{
            title: "Filter"
            Layout.fillWidth: true
            RowLayout{
                id: whereClauseRow
                spacing: 5
                VizButton{
                    text: "Query Builder"
                    onClicked: openQueryBuilder()
                }
                VizTextField{
                    id: whereClauseTextField
                    text: ""
                    placeholderText: "Enter Where Clause..."
                    Layout.fillWidth: true
                }
            }
        }

        RowLayout{
            id:commandButtonRect
            spacing: 10

            Rectangle{
                width: parent.width - okButton.width - cancelButton.width -20
                Layout.fillWidth: true
            }

            VizButton{
                id:okButton
                text: "   Ok   "
                width: 100
                onClicked:addOracleTable()
            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                width: 100
                onClicked: closePopup()
            }
        }
    }
}

