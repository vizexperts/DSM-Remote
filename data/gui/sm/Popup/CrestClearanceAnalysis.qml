import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id : crestClearanceMenuId
    objectName: "CrestClearanceMenuObject"
    title: "Firing Range Analysis"

    Component.onCompleted: {
        if(!running_from_qml){
            CrestClearanceGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            CrestClearanceGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    // signal for the UI
    signal qmlHandlePPCLPMarkButtonClicked( bool value )
    signal qmlHandlePPCDirMarkButtonClicked( bool value )
    signal qmlHandleVACLPMarkButtonClicked( bool value )
    signal qmlHandleVACTPMarkButtonClicked( bool value )

    signal qmlHandlePPCStartButtonClicked()
    signal qmlHandlePPCStopButtonClicked()
    signal qmlHandleVACStartButtonClicked()
    signal qmlHandleVACStopButtonClicked()

    // for projectile path calculation
    property alias lePPCLaunchPointLong : lePPCLaunchPointLongID.text
    property alias lePPCLaunchPointLat : lePPCLaunchPointLatID.text
    //   property alias lePPCLaunchPointAlt:lePPCLaunchPointAltID.text
    property alias lePPCLaunchPointHeight : lePPCLaunchPointHeightID.text

    property alias lePPCDirectionX : lePPCDirectionXID.text
    property alias lePPCDirectionY : lePPCDirectionYID.text
    property alias lePPCDirectionZ : lePPCDirectionZID.text

    property alias lePPCAngle : lePPCAngleID.text
    property alias lePPCVelocity : lePPCVelocityID.text

    // for Velocity and Angle Calculation
    property alias leVACLaunchPointLong : leVACLaunchPointLongID.text
    property alias leVACLaunchPointLat : leVACLaunchPointLatID.text
    //    property alias leVACLaunchPointAlt : leVACLaunchPointAltID.text
    property alias leVACLaunchPointHeight : leVACLaunchPointHeightID.text

    property alias leVACTargetPointX : leVACTargetPointXID.text
    property alias leVACTargetPointY : leVACTargetPointYID.text
    //    property alias leVACTargetPointZ : leVACTargetPointZID.text
    property alias leVACTargetPointHeight : leVACTargetPointHeightID.text

    property alias leVACMinAngle : leVACMinAngleID.text
    property alias leVACMaxAngle : leVACMaxAngleID.text
    property alias leVACAngleStepSize : leVACAngleStepSizeID.text

    property alias leVACMaxSpeed : leVACMaxSpeedID.text
    property alias leVACMinSpeed : leVACMinSpeedID.text

    property alias crestClearanceState : crestClearanceMenuId.state
    property alias crestClearanceProgressBarPPC : progressBarPPCID.visible
    property alias crestClearanceProgressBarVAC : progressBarVACID.visible

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        VizTabFrame{
            id : vizTabFrameId
            anchors.fill: parent
            anchors.margins: 5

            Layout.fillHeight: true
            Layout.fillWidth: true

            VizTab{
                title: "Projectile path calculation"
                id: tabPPCTabID
                ColumnLayout{
                    id:tab1
                    VizGroupBox {
                        id : ppcLaunchPointGroupBoxId
                        title: "Launch point"
                        ColumnLayout{
                            GridLayout{
                                id: launchPointGrid
                                columns: 4
                                anchors.fill: parent
                                VizLabel {
                                    id : ppcLaunchPointPositionLabelID
                                    text: "Location"
                                }
                                VizTextField {
                                    id: lePPCLaunchPointLatID
                                    placeholderText : "Latitude"
                                    Layout.fillWidth: true
                                    validator:  rootStyle.latitudeValidator
                                }
                                VizTextField {
                                    id: lePPCLaunchPointLongID
                                    placeholderText : "Longitude"
                                    Layout.fillWidth: true
                                    validator:  rootStyle.longitudeValidator
                                }
                                VizButton {
                                    id : pbPPCLaunchPointMarkID
                                    text : "   Mark   "
                                    checkable: true
                                    Layout.alignment: Qt.AlignRight
                                    onClicked: {
                                        if ( checked ) {
                                            pbPPCDirectionMarkID.checked = false
                                            qmlHandlePPCLPMarkButtonClicked( true )
                                        }
                                        else {
                                            crestClearanceMenuId.state = "defaultPPC"
                                            qmlHandlePPCLPMarkButtonClicked( false )
                                        }
                                    }
                                }
                            }

                            RowLayout{
                                VizLabel {
                                    id : ppcLaunchPointHeightLabelID
                                    text: "Height"
                                }
                                // Launch point altitude text field
                                VizTextField {
                                    id: lePPCLaunchPointHeightID
                                    placeholderText : "Height"
                                    text: "2"
                                    Layout.fillWidth: true
                                    validator:  DoubleValidator { bottom : 0;notation: DoubleValidator.StandardNotation; }
                                }
                            }
                        }
                    }
                }

                // Target point for PPC
                VizGroupBox {
                    id : ppcTargetPointGroupBoxId
                    title: "Target position"

                    RowLayout {
                        id: targetPointRow
                        anchors.fill: parent

                        VizLabel {
                            text: "Location"
                        }
                        VizTextField {
                            id: lePPCDirectionYID
                            Layout.fillWidth: true
                            placeholderText : "Latitude"
                            validator:  DoubleValidator {bottom: -90; top: 90;notation: DoubleValidator.StandardNotation;}
                        }
                        VizTextField {
                            id: lePPCDirectionXID
                            placeholderText : "Longitude"
                            Layout.fillWidth: true
                            validator:  DoubleValidator {bottom: -180; top: 180; notation: DoubleValidator.StandardNotation;}
                        }
                        VizTextField {
                            id: lePPCDirectionZID
                            placeholderText : "Altitude"
                            width : 0
                            visible: false
                        }

                        VizButton {
                            id : pbPPCDirectionMarkID
                            text : "   Mark   "
                            checkable: true
                            onClicked: {
                                if ( checked) {
                                    pbPPCLaunchPointMarkID.checked = false
                                    qmlHandlePPCDirMarkButtonClicked( true )
                                }
                                else {
                                    crestClearanceMenuId.state = "defaultPPC"
                                    qmlHandlePPCDirMarkButtonClicked( false )
                                }
                            }
                        }
                    }
                }

                RowLayout{
                    id:angleAndVelocityRow
                    VizLabel { text : "Shot angle" }
                    // angle text field
                    VizTextField {
                        id: lePPCAngleID
                        placeholderText: "degrees ( 0-90 )"
                        validator:  DoubleValidator {bottom: 0; top: 90; notation: DoubleValidator.StandardNotation;}
                        Layout.fillWidth: true
                    }

                    VizLabel {
                        text : "Speed"
                    }
                    // Velocity text field
                    VizTextField {
                        id: lePPCVelocityID
                        placeholderText: "m/s"
                        validator:  DoubleValidator {bottom: 0; notation: DoubleValidator.StandardNotation;}
                        Layout.fillWidth: true
                    }
                }

                RowLayout {
                    id : ppcStartAndStopButtonsID

                    AnimatedImage
                    {
                        id : progressBarPPCID
                        source: rootStyle.getBusyBar()
                        visible: false
                    }
                    Rectangle{
                        Layout.fillWidth: true
                    }
                    VizButton {
                        id : pbPPCStartID
                        objectName : "PBPPCStart"
                        text : "    Calculate    "

                        function handlePPCStartClicked()
                        {
                            crestClearanceMenuId.state = "pbPPCStartClicked"
//                            pbPPCStopID.focus = true
                            qmlHandlePPCDirMarkButtonClicked(false)
                            qmlHandlePPCLPMarkButtonClicked(false)
                            qmlHandlePPCStartButtonClicked()
                        }

                        onClicked: handlePPCStartClicked()
                        Keys.onEnterPressed: handlePPCStartClicked()
                        Keys.onReturnPressed: handlePPCStartClicked()
                    }

//                    VizButton
//                        {
//                            id : pbPPCStopID
//                            text : "    Cancel    "
//                            width : 95
//                            onClicked:{
//                                crestClearanceMenuId.state = "defaultPPC"
//                                qmlHandlePPCStopButtonClicked()
//                            }
//                            Keys.onEnterPressed: {
//                                crestClearanceMenuId.state = "defaultPPC"
//                                qmlHandlePPCStopButtonClicked()
//                            }
//                            Keys.onReturnPressed: {
//                                crestClearanceMenuId.state = "defaultPPC"
//                                qmlHandlePPCStopButtonClicked()
//                            }
//                            enabled: false
//                        }
                }
                states:
                    [
                    State
                    {
                        name : "allPPCTextFieldDisable"
                        PropertyChanges { target: lePPCLaunchPointLongID; textFieldEnable: false }
                        PropertyChanges { target: lePPCLaunchPointLatID; textFieldEnable: false }
                        PropertyChanges { target: lePPCLaunchPointHeightID; textFieldEnable: false }
                        PropertyChanges { target: lePPCDirectionXID; textFieldEnable: false }
                        PropertyChanges { target: lePPCDirectionYID; textFieldEnable: false }
                        PropertyChanges { target: lePPCDirectionZID; textFieldEnable: false }
                        PropertyChanges { target: lePPCAngleID; textFieldEnable: false }
                        PropertyChanges { target: lePPCVelocityID; textFieldEnable: false }
                    },
                    State
                    {
                        name : "allPPCTextFieldEnable"
                        PropertyChanges { target: lePPCLaunchPointLongID; textFieldEnable: true }
                        PropertyChanges { target: lePPCLaunchPointLatID; textFieldEnable: true }
                        PropertyChanges { target: lePPCLaunchPointHeightID; textFieldEnable: true }
                        PropertyChanges { target: lePPCDirectionXID; textFieldEnable: true }
                        PropertyChanges { target: lePPCDirectionYID; textFieldEnable: true }
                        PropertyChanges { target: lePPCDirectionZID; textFieldEnable: true }
                        PropertyChanges { target: lePPCAngleID; textFieldEnable: true }
                        PropertyChanges { target: lePPCVelocityID; textFieldEnable: true }
                    }
                ]
            }

            VizTab{
                title: "Firing speed and angle prediction"
                id: tabVACTabID
                clip : true

                height: tab2.implicitHeight
                width: tab2.implicitWidth

                ColumnLayout{

                    id:tab2
                    anchors.fill: parent

                    // Launch point text fields and mark button
                    VizGroupBox{
                        id : vacLaunchPointGroupBoxId
                        title: "Launch point"

                        height: launchPointGrid2.implicitHeight +50
                        width: launchPointGrid2.implicitWidth+10

                        Layout.fillWidth: true

                        GridLayout{
                            id: launchPointGrid2
                            columns: 4
                            anchors.fill: parent

                            VizLabel{
                                id : vacLaunchPointPositionLabelID
                                text: "Location"
                            }
                            VizTextField {
                                id: leVACLaunchPointLatID
                                placeholderText : "Latitude"
                                Layout.fillWidth: true
                                validator:  rootStyle.latitudeValidator
                            }
                            VizTextField{
                                id: leVACLaunchPointLongID
                                placeholderText : "Longitude"
                                Layout.fillWidth: true
                                validator:  rootStyle.longitudeValidator
                            }
                            VizButton {
                                id : pbVACLaunchPointMarkID
                                objectName: "PBVACLaunchPointMark"
                                text : "   Mark   "
                                checkable : true
                                Layout.alignment: Qt.AlignRight
                                onClicked: {
                                    if ( checked) {
                                        pbVACTargetPointMarkID.checked = false
                                        qmlHandleVACLPMarkButtonClicked( true )
                                    }
                                    else {
                                        crestClearanceMenuId.state = "defaultPPC"
                                        qmlHandleVACLPMarkButtonClicked( false )
                                    }
                                }
                            }
                            VizLabel {
                                id : vacLaunchPointHeightLabelID
                                text: "Height"
                            }
                            VizTextField {
                                id: leVACLaunchPointHeightID
                                text: "2"
                                Layout.fillWidth: true
                                validator : DoubleValidator { bottom : 2; notation: DoubleValidator.StandardNotation; }
                            }
                        }
                    }

                    VizGroupBox {
                        id : vacTargetPointGroupBoxId
                        title: "Target location"

                        height: targetPointGrid2.implicitHeight + 50
                        width: targetPointGrid2.implicitWidth +10

                        Layout.fillWidth: true

                        GridLayout {
                            id:targetPointGrid2
                            columns: 4

                            anchors.fill: parent

                            VizLabel {
                                id : vacTargetPointPositionLabelID
                                text: "Location"
                            }

                            VizTextField {
                                id: leVACTargetPointXID
                                placeholderText : "Latitude"
                                Layout.fillWidth: true
                                validator:  rootStyle.longitudeValidator
                            }

                            VizTextField {
                                id: leVACTargetPointYID
                                placeholderText : "Longitude"
                                Layout.fillWidth: true
                                validator:  rootStyle.latitudeValidator
                            }

                            VizButton {
                                id : pbVACTargetPointMarkID
                                objectName: "PBVACTargetPointMark"
                                text : "   Mark   "
                                checkable: true
                                onClicked:
                                {
                                    if ( checked)
                                    {
                                        pbVACLaunchPointMarkID.checked = false
                                        qmlHandleVACTPMarkButtonClicked( true )
                                    }
                                    else
                                    {
                                        crestClearanceMenuId.state = "defaultPPC"
                                        qmlHandleVACTPMarkButtonClicked( false )
                                    }
                                }
                            }

                            VizLabel{
                                id : vacTargetPointHeightLabelID
                                text: "Height"
                            }

                            VizTextField{
                                id: leVACTargetPointHeightID
                                text: "2"
                                Layout.fillWidth: true
                                validator : DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation; }
                            }
                        }
                    }

                    VizGroupBox{
                        id : angleGroupBox
                        title: "Shooting angle range"

                        height: angleGrid.implicitHeight +50
                        width: angleGrid.implicitWidth + 10

                        Layout.fillWidth: true

                        GridLayout{
                            id: angleGrid
                            columns: 3

                            anchors.fill: parent

                            VizLabel{
                                id : rangeId
                                text : "Range"
                            }

                            VizTextField{
                                id: leVACMinAngleID
                                placeholderText : "Minimum"
                                Layout.fillWidth: true
                                validator:  DoubleValidator {bottom: 0; top: 90; notation: DoubleValidator.StandardNotation;}
                            }

                            VizTextField{
                                id: leVACMaxAngleID
                                placeholderText : "Maximum"
                                Layout.fillWidth: true
                                validator:  DoubleValidator {bottom: 0; top: 90; notation: DoubleValidator.StandardNotation;}
                            }

                            VizLabel{
                                id: stepSizeID
                                text: "Angle change by"
                            }

                            VizTextField{
                                id: leVACAngleStepSizeID
                                placeholderText : "Step size"
                                Layout.fillWidth: true
                                validator:  DoubleValidator {bottom: 0; top: 90; notation: DoubleValidator.StandardNotation;}
                            }
                        }
                    }

                    // left pane containing text
                    RowLayout{
                        id: rectangle2

                        VizLabel{
                            text : "Speed range"
                        }

                        VizTextField{
                            id: leVACMinSpeedID
                            placeholderText : "Minimum"
                            Layout.fillWidth: true
                            validator : DoubleValidator {notation: DoubleValidator.StandardNotation;}
                        }

                        VizTextField{
                            id: leVACMaxSpeedID
                            placeholderText : "Maximum"
                            Layout.fillWidth: true
                            validator : DoubleValidator {notation: DoubleValidator.StandardNotation;}
                        }
                    }

                    RowLayout{
                        id : vacStartAndStopButtonsID

                        AnimatedImage
                        {
                            id : progressBarVACID
                            source: rootStyle.getBusyIcon()
                            visible: false
                        }

                        Rectangle{
                            Layout.fillWidth: true
                        }

                        VizButton
                        {
                            id : pbVACStartID
                            objectName: "PBVACStart"
                            text : "   Calculate   "

                            function handleVACStartClicked()
                            {
                                crestClearanceMenuId.state = "pbVACStartClicked"
//                                pbVACStopID.focus = true
                                qmlHandleVACTPMarkButtonClicked( false )
                                qmlHandleVACLPMarkButtonClicked( false )
                                qmlHandleVACStartButtonClicked()
                            }

                            Keys.onReturnPressed: handleVACStartClicked()
                            Keys.onEnterPressed: handleVACStartClicked()
                            onClicked: handleVACStartClicked()
                        }

                        /*VizButton{
                            id : pbVACStopID
                            text : "   Cancel   "

                            onClicked:{
                                crestClearanceMenuId.state = "defaultVAC"
                                qmlHandleVACStopButtonClicked()
                            }

                            Keys.onReturnPressed:{
                                crestClearanceMenuId.state = "defaultVAC"
                                qmlHandleVACStopButtonClicked()
                            }

                            Keys.onEnterPressed:{
                                crestClearanceMenuId.state = "defaultVAC"
                                qmlHandleVACStopButtonClicked()
                            }

                            enabled: false
                        }*/
                    }


                    states:[
                        State
                        {
                            name : "allVACTextFieldDisable"
                            PropertyChanges { target: leVACLaunchPointLongID; textFieldEnable: false }
                            PropertyChanges { target: leVACLaunchPointLatID; textFieldEnable: false }
                            PropertyChanges { target: leVACLaunchPointHeightID; textFieldEnable: false }
                            PropertyChanges { target: leVACTargetPointXID; textFieldEnable: false }
                            PropertyChanges { target: leVACTargetPointYID; textFieldEnable: false }
                            PropertyChanges { target: leVACTargetPointZID; textFieldEnable: false }
                            PropertyChanges { target: leVACTargetPointHeightID; textFieldEnable: false }
                            PropertyChanges { target: leVACMinAngleID; textFieldEnable: false }
                            PropertyChanges { target: leVACMaxAngleID; textFieldEnable: false }
                            PropertyChanges { target: leVACAngleStepSizeID; textFieldEnable: false }
                            PropertyChanges { target: leVACMinSpeedID; textFieldEnable: false }
                            PropertyChanges { target: leVACMaxSpeedID; textFieldEnable: false }
                        },
                        State
                        {
                            name : "allVACTextFieldEnable"
                            PropertyChanges { target: leVACLaunchPointLongID; textFieldEnable: true }
                            PropertyChanges { target: leVACLaunchPointLatID; textFieldEnable: true }
                            PropertyChanges { target: leVACLaunchPointHeightID; textFieldEnable: true }
                            PropertyChanges { target: leVACTargetPointXID; textFieldEnable: true }
                            PropertyChanges { target: leVACTargetPointYID; textFieldEnable: true }
                            PropertyChanges { target: leVACTargetPointZID; textFieldEnable: true }
                            PropertyChanges { target: leVACTargetPointHeightID; textFieldEnable: true }
                            PropertyChanges { target: leVACMinAngleID; textFieldEnable: true }
                            PropertyChanges { target: leVACMaxAngleID; textFieldEnable: true }
                            PropertyChanges { target: leVACAngleStepSizeID; textFieldEnable: true }
                            PropertyChanges { target: leVACMinSpeedID; textFieldEnable: true }
                            PropertyChanges { target: leVACMaxSpeedID; textFieldEnable: true }
                        }
                    ]
                }
            }
        }
    }

    states:[
        State
        {
            name : "pbPPCStartClicked"
            PropertyChanges { target : pbPPCLaunchPointMarkID; enabled : false  }
            PropertyChanges { target : pbPPCDirectionMarkID; enabled : false  }
            PropertyChanges { target : pbPPCStartID; enabled : false }
//            PropertyChanges { target : pbPPCStopID; enabled : true }
            PropertyChanges { target : vizTabFrameId; tabEnabled : false }
            PropertyChanges { target : tabPPCTabID; state: "allPPCTextFieldDisable" }
        },

        State
        {
            name : "defaultPPC"
            PropertyChanges { target : pbPPCLaunchPointMarkID; enabled : true; checked : false }
            PropertyChanges { target : pbPPCDirectionMarkID; enabled : true; checked : false }
            PropertyChanges { target : pbPPCStartID; enabled: true}
//            PropertyChanges { target : pbPPCStopID; enabled : false}
            PropertyChanges { target : vizTabFrameId; tabEnabled : true }
            PropertyChanges { target : tabPPCTabID; state: "allPPCTextFieldEnable"  }
        },

        State
        {
            name : "pbVACStartClicked"
            PropertyChanges { target : pbVACLaunchPointMarkID; enabled: false}
            PropertyChanges { target : pbVACTargetPointMarkID; enabled: false}
            PropertyChanges { target : pbVACStartID; enabled: false}
//            PropertyChanges { target : pbVACStopID; enabled: true}
            PropertyChanges { target : vizTabFrameId; tabEnabled : false }
            PropertyChanges { target : tabVACTabID; state: "allVACTextFieldDisable" }
        },

        State
        {
            name : "defaultVAC"
            PropertyChanges { target : pbVACLaunchPointMarkID; enabled : true; checked: false }
            PropertyChanges { target : pbVACTargetPointMarkID; enabled: true; checked: false }
            PropertyChanges { target : pbVACStartID; enabled: true}
//            PropertyChanges { target : pbVACStopID; enabled: false}
            PropertyChanges { target : vizTabFrameId; tabEnabled : true }
            PropertyChanges { target : tabVACTabID; state: "allVACTextFieldEnable" }
        }
    ]
}
