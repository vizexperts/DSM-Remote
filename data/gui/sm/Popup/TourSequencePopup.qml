import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQml.Models 2.1
import "../VizComponents"
import ".."

Popup {
    id: tourSequenceGUI
    objectName: "tourSequenceGUI"
    title: "Create Tour Sequence"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal addToSequenceList(int index)
    signal removeFromSequence(int index)
    signal moveInSequence(int from, int to)
    signal saveSequence(string name)
    signal openBottomLoader()

    Component.onDestruction: {
        openBottomLoader()
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    Component {
        id: dragDelegate

        MouseArea {
            id: dragArea

            property bool held: false

            anchors { left: parent.left; right: parent.right }
            height: content.height

            drag.target: held ? content : undefined
            drag.axis: Drag.YAxis

            onPressed: sequenceListView.currentIndex = index
            onPressAndHold: {
                held = true
                from = dragArea.DelegateModel.itemsIndex
            }
            onReleased: {
                if(held){
                    held = false
                    if(from !== dragArea.DelegateModel.itemsIndex){
                        moveInSequence(from, dragArea.DelegateModel.itemsIndex)
                    }
                }
            }

            //!
            property int from: 0

            Rectangle {
                id: content
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                width: dragArea.width; height: label.height + 4

                border.width: 1
                border.color: "lightsteelblue"

                color: dragArea.held ? "lightsteelblue" : (sequenceListView.currentIndex == index)
                                       ? rootStyle.selectionColor : "transparent"

                Behavior on color { ColorAnimation { duration: 100 } }

                radius: 2
                Drag.active: dragArea.held
                Drag.source: dragArea
                Drag.hotSpot.x: width / 2
                Drag.hotSpot.y: height / 2
                states: State {
                    when: dragArea.held

                    ParentChange { target: content; parent: root }
                    AnchorChanges {
                        target: content
                        anchors { horizontalCenter: undefined; verticalCenter: undefined }
                    }
                }

                VizLabel{
                    id: label
                    text: modelData
                    textColor: (sequenceListView.currentIndex == index) ? rootStyle.selectedTextColor : rootStyle.textColor
                }
            }
            DropArea {
                anchors { fill: parent; margins: 10 }

                onEntered: {
                    visualModel.items.move(
                            drag.source.DelegateModel.itemsIndex,
                            dragArea.DelegateModel.itemsIndex)
                }
            }
        }
    }
    DelegateModel{
        id: visualModel
        objectName: "delegateModel"
        model: (typeof(tourSequenceList) != "undefined")? tourSequenceList : "undefined"
        delegate: dragDelegate
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        spacing: 5

        RowLayout{
            id: sequenceNameLayout
            spacing: 5
            VizLabel{
                width: 100
                text: "Sequence Name"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: sequenceNameTextField
                width: 400
                text: ""
                validator: rootStyle.nameValidator
                placeholderText: "Give a name to this sequence..."
                Layout.fillWidth: true
            }
        }
        RowLayout{
            id: packageRow
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 5
            ColumnLayout{
                id: sequenceColumn
                VizLabel{
                    text: "Current Sequence"
                }
                Rectangle{
                    width: 200
                    height: 200
                    color: "black"
                    ListView{
                        id: sequenceListView
                        anchors.fill: parent
                        currentIndex: -1
                        model: visualModel
                        spacing: 4
                    }
                }
                VizButton{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Remove"
                    enabled: (sequenceListView.currentIndex != -1)
                    onClicked: removeFromSequence(sequenceListView.currentIndex)
                }
            }
            ColumnLayout{
                id: residualColumn
                VizLabel{
                    text: "Remaining Tours"
                }
                Rectangle{
                    width: 200
                    height: 200
                    color: "black"
                    ListView{
                        id: listView
                        currentIndex: -1
                        objectName: "listview"
                        anchors.fill: parent
                        model: (typeof(tourSequenceAvailableList) != "undefined")? tourSequenceAvailableList : "undefined"
                        spacing: 4
                        highlight: Rectangle{
                            width: 200
                            height: label.height
                            color: rootStyle.selectionColor
                        }
                        delegate: VizLabel{
                            id: label
                            text: modelData
                            textColor: (listView.currentIndex == index) ? rootStyle.selectedTextColor : rootStyle.textColor
                            MouseArea{
                                anchors.fill: parent
                                onClicked: listView.currentIndex = index
                            }
                        }
                    }
                }
                VizButton{
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Add To Sequence"
                    visible: (listView.currentIndex != -1)
                    onClicked: addToSequenceList(listView.currentIndex)
                }
            }
        }
        RowLayout{
            id: commndButtonRect
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10
            VizButton{
                id: okButton
                text: "  Ok  "
                onClicked: {
                    saveSequence(sequenceNameTextField.text)
                }
            }
            VizButton{
                id: cancelButton
                text: "Cancel"
                onClicked: {
                    closePopup()
                }
            }
        }
    }

}
