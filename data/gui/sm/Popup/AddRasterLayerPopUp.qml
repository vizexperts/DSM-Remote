import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "addRasterLayerPopup"
    title: "Add New Raster"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal addLayer(string name, string filePath, int minLOD, int maxLOD, bool overrideExtents, string enhanceType,string projectionType)
    signal browseButtonClicked(bool checked)
    signal lodTypeCheckBoxChanged(bool checked, string selectedFileName)
    signal computeMinMaxLod(string selectedFileName)
    signal minMarkClicked( bool value )
    signal maxMarkClicked( bool value )
    signal selectSubdataset(bool checked)
    signal createCRSCombobox()

    property alias xMin: xminEdit.text
    property alias yMin: yminEdit.text
    property alias xMax: xmaxEdit.text
    property alias yMax: ymaxEdit.text
    property alias subdatasetSelectedUid : subdatasetComboBox.selectedUID
    property alias subdataseName : subdatasetComboBox.value

    property alias selectedFileName: filePath.text
    property alias layerName: layerNameField.text
    property alias minimumLOD: minLOD.text
    property alias maximumLOD: maxLOD.text
    property alias layerNameReadOnly: layerNameField.readOnly
    property alias lodEditable: lodGroupBox.checkable
    property alias lodEnabled: lodGroupBox.checked
    property alias coordSystem: coordSys.text
    property alias userWholeFolder : checkBox.checked

    property bool showAdvancedOptions: false
    property bool showGeoExtents: false
    property bool showCRS:false

    Component.onDestruction:
    {
        smpFileMenu.isPopUpAlreadyOpen=false;
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{
            id:layerNameRow
            spacing: 5

            VizLabel{
                text: "Layer Name"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: layerNameField
                text: ""
                validator:rootStyle.nameValidator
                placeholderText: "Give a name to this layer..."
                Layout.fillWidth: true
            }
        }

        RowLayout{
            id:filePathRow
            spacing: 5
            VizLabel{
                text: "Path"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: filePath
                Layout.minimumWidth: 300
                text: ""
                placeholderText:checkBox.checked? "Browse directory..." : "Browse files..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                onClicked: browseButtonClicked(checkBox.checked)
            }
        }

        RowLayout {
            id : advancedOptionsRect
            VizButton{
                id: advancedOptionsCheckBox
                text: showAdvancedOptions? "Hide Advanced Options" : "Show Advanced Options"
                onClicked: showAdvancedOptions = !showAdvancedOptions
            }

            Item{
                Layout.fillWidth: true
            }

            VizCheckBox{
                id: checkBox
                text: "Add whole directory"
                checked: false
            }
        }

        VizGroupBox{
            id:subdatasetrow
            title: "Select subdataset"
            checkable: false
            visible: showAdvancedOptions
            clip: false
            Layout.fillWidth: true
            Layout.minimumHeight: subdatasetComboBox.height + margins
            z:1000
            VizComboBox{
                id: subdatasetComboBox
                Layout.fillWidth: true
                listModel:(typeof(subdatasetListModel) !== "undefined") ? subdatasetListModel: "undefined"
                onSelectOption: selectSubdataset()
            }
        }

        VizGroupBox{
            id:coordinatesGroupBox
            title: "Position"
            checkable: true
            checked: showGeoExtents
            visible: showAdvancedOptions|| showGeoExtents
            clip: true
            Layout.fillWidth: true
            Layout.minimumHeight: positionGrid.implicitHeight + margins
            Layout.minimumWidth: positionGrid.implicitWidth + 20

            GridLayout{
                id: positionGrid
                Layout.fillWidth: true
                columns: 2
                RowLayout{
                    spacing: 5
                    VizLabel{
                        id: vizlabel1
                        text: "Left Longitude"
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id:xminEdit
                        text:""
                        placeholderText: "[-180 to 180]"
                        validator: rootStyle.longitudeValidator
                        Layout.fillWidth: true
                        Layout.minimumWidth: 75
                    }
                }
                RowLayout{
                spacing: 5
                    VizLabel{
                        text: "Right Longitude"
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id:xmaxEdit
                        text:""
                        placeholderText: "[-180 to 180]"
                        validator: rootStyle.longitudeValidator
                        Layout.fillWidth: true
                        Layout.minimumWidth: 75
                    }
                }
                RowLayout{
                    spacing: 5
                    VizLabel{
                        text: "Bottom Latitude"
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id:yminEdit
                        text:""
                        placeholderText: "[-90 to 90]"
                        validator: rootStyle.latitudeValidator
                        Layout.minimumWidth: 75
                        Layout.fillWidth: true
                    }
                }
                RowLayout{
                    spacing: 5
                    VizLabel{
                        text: "Top Latitude"
                        textAlignment: Qt.AlignLeft
                    }
                    VizTextField{
                        id:ymaxEdit
                        text:""
                        placeholderText: "[-90 to 90]"
                        validator: rootStyle.latitudeValidator
                        Layout.fillWidth: true
                        Layout.minimumWidth: 75
                    }
                }

                VizButton{
                    id:minMarkButton
                    text: "Mark"
                    checkable: true
                    Layout.alignment: Qt.AlignHCenter
                    onClicked: {
                        minMarkClicked( checked )
                        if ( checked ) {
                            maxMarkButton.checked = false
                        }
                    }
                }

                VizButton {
                    id:maxMarkButton
                    text: "Mark"
                    Layout.alignment: Qt.AlignHCenter
                    checkable: true
                    onClicked: {
                        maxMarkClicked( checked )
                        if ( checked ){
                            minMarkButton.checked = false
                        }
                    }
                }
            }
            onCheckableChanged: {
                if(checkable== false){
                    visible= false
                    checked= false
                }
                else{
                    visible= true
                }
            }
        }

        VizGroupBox{
            id:lodGroupBox
            title: "Level Of Detail"
            Layout.minimumHeight: lodLayout.implicitHeight + margins
            Layout.minimumWidth: lodLayout.implicitWidth + 20
            checkable: true
            checked: false
            visible: showAdvancedOptions
            clip: true
            Layout.fillWidth: true

            ColumnLayout{
                id: lodLayout
                spacing: 5
                RowLayout{
                    id: detailsRow
                    Layout.fillWidth: true
                    spacing: 5
                    VizLabel{
                        text: "Minimum LOD"
                    }
                    VizTextField{
                        id:minLOD
                        text:""
                        placeholderText: "Enter valid LOD value"
                        validator: IntValidator{ bottom: 0 }
                        Layout.fillWidth: true
                    }

                    VizLabel{
                        text: "Maximum LOD"
                    }
                    VizTextField{
                        id:maxLOD
                        text: ""
                        placeholderText: "Enter valid LOD value"
                        validator: IntValidator{ bottom: 0 }
                        Layout.fillWidth: true
                    }
                }
                RowLayout{
                    Item{
                        width: parent.width - computeButton.width
                        Layout.fillWidth: true
                    }
                    VizButton{
                        id:computeButton
                        text: "Compute"
                        onClicked: {
                                computeMinMaxLod(selectedFileName)
                        }
                    }
                }
            }
            onCheckableChanged: {
                if(checkable== false)
                {
                    visible= false
                    checked= false
                }
                else
                {
                    visible= true
                }
            }
        }

        VizGroupBox{
            id:enhanceGroupBox
            title: "Image Enhancement"
            height: imgLayout.Layout.minimumHeight+200
            visible: showAdvancedOptions
            width: parent.width
            Layout.fillWidth: true
            z: 10

            ColumnLayout{
                id: imgLayout
                spacing: 5
                width: parent.width-20
                anchors.horizontalCenter: parent.horizontalCenter

                RowLayout{
                    id: layerTypeRow
                    VizLabel{
                        text: "Enhancement"
                        width: 100
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox{
                        id: enhanceTypeComboBox
                        width: 200
                        Layout.fillWidth: true
                        Layout.minimumWidth: 150
                        listModel: typeListModel
                        property string enhanceType: ""
                        value: "None"
                        onSelectOption: {
                            switch(uID){
                            case '0': enhanceType = "None"; break;
                            case '1': enhanceType = "MinMax"; break;
                            case '2': enhanceType = "StandardDeviation"; break;
                            case '3': enhanceType = "HistogramEqualization"; break;
                            case '4': enhanceType = "MinMaxConstrast"; break;
                            }
                        }
                    }

                    ListModel{
                        id: typeListModel
                        ListElement{name:"None"; uID : 0}
                        ListElement{name:"Min Max"; uID : 1}
                        ListElement{name:"Standard Deviation"; uID : 2}
                        ListElement{name:"Histogram Equalization"; uID : 3}
                        ListElement{name:"Min Max Contrast"; uID : 4}
                    }
                }
            }
        }

        RowLayout
        {
            id:crsOptionsRect
            VizButton{
                id: coordSys
                text: "CRS: Unknown"
                anchors.left: parent.left
                anchors.leftMargin: 20
                onClicked:
                {
                showCRS=!showCRS
                //createCRSCombobox()
                }
            }
            Item{
                Layout.fillWidth: true
            }

        }
        VizGroupBox{
            id:changeCRS
            title: "Current "+coordSys.text
            checkable: false
            visible:showCRS
            clip: false
            Layout.fillWidth: true
            Layout.minimumHeight: subdatasetComboBox.height + margins
            z:1000
//            VizComboBox{
//                id: selectNewCRS
//                Layout.fillWidth: true
//                 listModel:(typeof(crsListModel) !== "undefined") ? crsListModel: "undefined"
//            }
            VizTextField
            {
                id:projectionType
                text: ""
                placeholderText: "Give Projection parameter"
                Layout.fillWidth: true
            }
        }

        RowLayout{
            id:commandButtonRect
            spacing: 10
            z: 5
            Item{
                width: parent.width-okButton.width-cancelButton.width
                Layout.fillWidth: true
            }
            VizButton{
                id:okButton
                text: "   Ok   "
                onClicked:
                {
                    minMarkClicked( false )
                    maxMarkClicked( false )
                    addLayer(layerName, selectedFileName, minimumLOD, maximumLOD, coordinatesGroupBox.checked, enhanceTypeComboBox.enhanceType,projectionType.text )
                }
            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                onClicked:
                {
                    minMarkClicked( false )
                    maxMarkClicked( false )
                    closePopup()
                }
            }
        }
    }
}

