import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."


Popup{
    id: createDataType
    objectName: "createDataType"
    title: "Create DataType"

    signal createButtonClicked()

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias createdDatabase: createdDatabase.text
    property alias newDataType: newDataType.text
    property alias rasterCheckBox: rasterCheckBox.checked
    property alias vectorCheckBox: vectorCheckBox.checked
    property alias elevationCheckBox: elevationCheckBox.checked
    property string rootQml: "raster"

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizTextField{
            id:createdDatabase
            text:""
            Layout.fillWidth: true
            placeholderText: ""
        }
        VizLabel{
            text: "Existing Data Types"
            textAlignment: Qt.AlignLeft
        }

        ListView {
            id: list
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip:true
            VizScrollBar{
                target: list
            }
            model:  (typeof(dataTypeListed) !== "undefined") ? dataTypeListed : "undefined"
            delegate:VizLabel{
                textAlignment: Qt.AlignLeft
                text: modelData
            }
        }
        RowLayout{
            spacing: 5
            VizLabel{
                text: "New Data Types"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id:newDataType
                text:""
                Layout.minimumWidth: 200
                placeholderText: "Enter New Data Type Name"
            }
        }

        VizGroupBox{
            id : dataCategory
            title: "Data Category"
            Layout.fillWidth: true
            Layout.fillHeight: true
            GridLayout{
                columns: 2
                VizLabel{
                    text: "Raster"
                    textAlignment: Qt.AlignLeft
                }
                VizCheckBox{
                    id: rasterCheckBox
                    checked: true
                    onClicked:{
                        vectorCheckBox.checked = false;
                        elevationCheckBox.checked = false;
                    }
                }
                VizLabel{
                    text: "Vector"
                    textAlignment: Qt.AlignLeft
                }

                VizCheckBox{
                    id: vectorCheckBox
                    checked: false
                    onClicked:{
                        rasterCheckBox.checked = false;
                        elevationCheckBox.checked = false;
                    }
                }
                VizLabel{
                    text: "Elevation"
                    textAlignment: Qt.AlignLeft
                }

                VizCheckBox{
                    id: elevationCheckBox
                    checked: false
                    onClicked:{
                        rasterCheckBox.checked = false;
                        vectorCheckBox.checked = false;
                    }
                }

            }

        }

        VizButton{
            id:createButton
            text: "Create"
            onClicked: {
                createButtonClicked()         //addLayer(selectedFileName, xMin, yMin, xMax, yMax, rotation)
                closePopup()
                if(rootQml==="raster")
                {
                    smpFileMenu.load("PostGIS.qml")
                }
                else if(rootQml==="vector")
                {
                    smpFileMenu.load("PostGISVector.qml")
                }
            }
        }
    }

    Connections{
        target: popupContainer
        onClose:{
            if(rootQml==="raster")
            {
                smpFileMenu.load("PostGIS.qml")
            }
            else if(rootQml==="vector")
            {
                smpFileMenu.load("PostGISVector.qml")
            }
        }
    }
}


















