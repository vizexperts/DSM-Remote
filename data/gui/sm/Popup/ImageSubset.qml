import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "imagesubset"
    title: "Extract ROI"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal viewImage(string imagePath)
    signal calculate()
    signal cancelButtonClicked()
    signal browseInputImage()
    signal browseOutputImage()

    property alias inputImagePath : inputImage.text
    property alias outputImagePath: outputImage.text

    property alias okButtonEnabled: okButton.enabled
    property alias cancelButtonEnabled: cancelButton.enabled

    property alias startX: xpos.text
    property alias startY: ypos.text
    property alias sizeX: xsize.text
    property alias sizeY: ysize.text

    property alias inputButtonEnabled: inputOpenID.enabled
    property alias outputButtonEnabled: outputOpenID.enabled
    property bool calculating: false


    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id:imageRow
            spacing: 5
            VizLabel{
                text: "Input Image"
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: inputImage
                enabled: !calculating
                Layout.minimumWidth: 200
                text: ""
                validator:rootStyle.nameValidator
                placeholderText:"Browse Input Image file..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: browseInputImage()
            }
            VizButton {
                id : inputOpenID
                text : "View"
                onClicked:viewImage(inputImagePath)
            }
        }
        RowLayout{
            id:opRow
            spacing: 5
            VizLabel{
                text: "Output Name"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: outputImage
                enabled: !calculating
                Layout.minimumWidth: 200
                text: ""
                validator:rootStyle.nameValidator
                placeholderText:"Enter output Image fileName..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: browseOutputImage()
            }
            VizButton {
                id : outputOpenID
                text : "View"
                onClicked:viewImage(outputImagePath)
            }
        }

        VizGroupBox{
            id:roiGroupbox
            title: "ROI"
            Layout.fillWidth: true
            Layout.fillHeight: true
            GridLayout{
                id:roiRow
                columns: 4
                VizLabel{
                    text:"StartX"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:xpos
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: ""
                    validator:  IntValidator {bottom: 0; top: 99999;}
                }
                VizLabel{
                    text:"StartY"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:ypos
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: ""
                    validator:  IntValidator {bottom: 0; top: 99999;}
                }
                VizLabel{
                    text:"SizeX"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:xsize
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: ""
                    validator:  IntValidator {bottom: 0; top: 9999;}
                }
                VizLabel{
                    text:"SizeY"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:ysize
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: ""
                    validator:  IntValidator {bottom: 0; top: 9999;}
                }
            }
        }
        RowLayout{
            id:commandButtonRect
            spacing: 15

            AnimatedImage{
                id:progressBarId
                source: rootStyle.getBusyIcon()
                visible: calculating
            }

            Item{
                Layout.fillWidth: true
            }

            VizButton{
                id:okButton
                text: "  Execute  "
                onClicked: calculate()
            }

            VizButton{
                id:cancelButton
                text: "  Clear  "
                onClicked: cancelButtonClicked()
            }
        }
    }
}
