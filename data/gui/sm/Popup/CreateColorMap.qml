import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id : createColorMap

    objectName: "ColorMap"
    title: "Create Color Map "

    property bool colorPickerVisible: false
    property int currentSelectedIndex: -1
    property alias defaultColor: defaultColorRectangle.backgroundColor
    property bool isClampChecked: false


    Component.onCompleted: {
        if(!running_from_qml){
            ColorPaletteGUI.setActive(true)
        }
        smpFileMenu.anchorContextualPickers()
    }

    onClosed: {
        if(!running_from_qml){
            ColorPaletteGUI.setActive(false)
        }
    }
    Component.onDestruction: {

        if(!running_from_qml){
            ColorPaletteGUI.setActive(false)
        }
     smpFileMenu.closeColorPicker()
    }
    Connections{
           target: smpFileMenu
           onSizeChanged : {
               minimumWidth = minimumWidth + newWidth
               minimumHeight =minimumHeight +newHeight
           }
       }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias rowIndex :colorListView.currentRow

    // mapsize
    property alias mapSize : mapSize.text

    //minimum value of color range
    property alias minmValue : minimumValue.text

    //maximum value of color range
    property alias maxmValue : maximumValue.text

    // alphaValue
    property alias alphaValue : alphaValue.text


    //colorMap Name
    property alias colorMapName : colorMapNameTextEdit.text
    property alias templateSelectedUid : templateColorMapComboBox.selectedUID
    property alias templateName : templateColorMapComboBox.value
    property string selectedRect

    signal generate()
    signal rowInsert()
    signal rowDelete()
    signal save()
    signal cancel()
    signal defaultColorButton(color rectColor)
    signal selectTemplateList()
    signal isClamp(bool checked)
    signal deleteColorMap()


    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            spacing: 5
            z: 10
            VizLabel{
                text: "Template"
                textAlignment: Qt.AlignLeft
            }
            VizComboBox{
                id: templateColorMapComboBox
                listModel: (typeof(templateColorMapListModel) !== "undefined") ? templateColorMapListModel: "undefined"
                Layout.fillWidth: true
                onSelectOption: selectTemplateList()
            }
        }
        RowLayout{
            spacing: 5
            z: 5
            VizLabel{
                text:"Name     "
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id : colorMapNameTextEdit
                Layout.fillWidth: true
                text:""
                placeholderText:""
                Layout.minimumWidth: 150
            }
        }
        RowLayout{
                   spacing: 5
                   VizCheckBox{
                       id: clamp
                       text :  "Clamp Values"
                       checked: isClampChecked
                       onCheckedChanged:
                       {
                           isClamp(checked);
                       }
                   }
                   Item{
                       Layout.minimumWidth: 50
                   }
                   VizLabel{
                       id : colorLabel
                       text:  "Default Color"
                   }
                   VizButton{
                       id: defaultColorRectangle
                       backgroundColor: "White"
                       flatButton: true
                       width : 50
                       onClicked: {
                           if(colorPickerVisible=== false){
                               smpFileMenu.setColor(backgroundColor)
                               colorPicker.currentColor = backgroundColor
                               colorPickerVisible =true
                           }
                           else{
                               smpFileMenu.getColorPicker();
                               defaultColorRectangle.backgroundColor = colorPicker.currentColor
                               defaultColorButton(defaultColorRectangle.backgroundColor)
                               colorPickerVisible =false
                           }
                       }
                   }
                   //Handle After deciding where to store the colorMap
//                   VizButton{
//                       id:deleteColorMapId
//                       text:"Delete ColorMap"
//                       width:50
//                       onClicked:
//                       {
//                           deleteColorMap();
//                       }
//                   }
               }

        VizGroupBox{
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: autoGenerateGridLayout.height + margins
            Layout.minimumWidth: autoGenerateGridLayout.width + 20
            title: "Auto Generate"
            GridLayout{
                id: autoGenerateGridLayout
                Layout.fillWidth: true
                columns: 2
                VizLabel{
                    text:"Step Size     "
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:mapSize
                    objectName: "mapSize"
                    Layout.minimumWidth : 270
                    validator: IntValidator{bottom:0; top: 99999}
                    text:""
                    placeholderText: "Map Size"
                    Keys.onTabPressed: {
                        minimumValue.setFocus = true
                        minimumValue.selectTheText()
                    }
                }
                VizLabel{
                    text:"Minimum Value"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:minimumValue
                    objectName: "minimumValue"
                    Layout.minimumWidth : 270
                    validator : DoubleValidator{bottom:-999999; top: 999999; decimals: 4; notation: DoubleValidator.StandardNotation; }
                    text:""
                    placeholderText: "Min Value"
                    Keys.onTabPressed: {
                        maximumValue.setFocus = true
                        maximumValue.selectTheText()
                    }
                    Keys.onBacktabPressed:{
                        mapSize.setFocus = true
                        mapSize.selectTheText()
                    }
                }
                VizLabel{
                    text:"Maximum Value"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:maximumValue
                    objectName: "maximumValue"
                    Layout.minimumWidth : 270
                    validator : DoubleValidator{ bottom:-999999;top: 999999; decimals:4 ;  notation: DoubleValidator.StandardNotation;}
                    text:""
                    placeholderText: "Max Value"
                    Keys.onTabPressed: {
                        alphaValue.setFocus = true
                        alphaValue.selectTheText()
                    }
                    Keys.onBacktabPressed: {
                        minimumValue.setFocus = true
                        minimumValue.selectTheText()
                    }
                }
                VizLabel{
                    text:"Transparency(0-255)"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id:alphaValue
                    objectName: "alphaValue"
                    Layout.minimumWidth : 270
                    validator : IntValidator{bottom:0; top:255 }
                    text:""
                    placeholderText: "Alpha Value"
                    Keys.onTabPressed:{
                        generateButton.focus = true
                    }

                    Keys.onBacktabPressed:{
                        maximumValue.setFocus = true
                        maximumValue.selectTheText()
                    }
                }
                VizButton{
                    id: generateButton
                    Layout.alignment: Qt.AlignHCenter
                    Layout.columnSpan: 2
                    text:"Generate"
                    onClicked: generate()
                }
            }
        }

        VizTableView {
            id:colorListView
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 300
            model:(typeof(colorListModel) !== "undefined") ? colorListModel : "undefined"


            TableViewColumn {
                role: "minVal"
                title: "Min Value"
                delegate: VizTextField{
                    text:modelData.minVal
                    onTextChanged:
                {
                    colorListModel[styleData.row].minVal = text
                }
                Layout.fillWidth: true
                }
            }
            TableViewColumn {
                role: "maxVal"
                title: "Max Value"
                delegate: VizTextField{
                    text:modelData.maxVal
                    onTextChanged:
                {
                    colorListModel[styleData.row].maxVal = text
                }
                Layout.fillWidth: true
                }
            }
            Component.onCompleted: {
            smpFileMenu.setColor(modelData.cellColor)
            }
            Component.onDestruction: {
                smpFileMenu.closeColorPicker()
                //visible =false
            }
            TableViewColumn {
                role: "cellColor"
                title: "Color Value"
                delegate:VizButton{
                    id:colorRectangle
                    backgroundColor : modelData.cellColor
                    flatButton: true
                    onClicked : {
                        if(colorPickerVisible=== false){
                            smpFileMenu.setColor(backgroundColor)
                            colorPicker.currentColor = colorListModel[styleData.row].cellColor
                            currentSelectedIndex =styleData.row
                            colorPickerVisible =true
                        }
                        else{
                            smpFileMenu.getColorPicker();
                            if(currentSelectedIndex===styleData.row)
                            {
                                colorListModel[styleData.row].cellColor = colorPicker.currentColor
                                selectedRect = "delegateRect"
                                colorPickerVisible =false
                                currentSelectedIndex =-1
                            }
                            else
                            {
                                colorPickerVisible=false;
                                smpFileMenu.openColorPicker()
                                smpFileMenu.setColor(backgroundColor)
                                colorPicker.currentColor = colorListModel[styleData.row].cellColor
                                currentSelectedIndex =styleData.row
                                colorPickerVisible =true
                            }

                        }
                    }
                }
            }
        }
        RowLayout{
            spacing: 5
            VizButton{
                text: "Insert Row"
                onClicked: rowInsert()
            }
            VizButton{
                text: "Delete Row"
                onClicked: rowDelete()
            }
            VizButton{
                text: "Save"
                onClicked: save()
            }
            VizButton{
                text: "Clear"
                onClicked: cancel()
            }
            VizButton{
                text: "Cancel"
                onClicked: closePopup()
            }
        }
    }
    onColorPickerVisibleChanged: {
        if(colorPickerVisible === true){
            smpFileMenu.openColorPicker()
        }
        else{
            smpFileMenu.closeColorPicker()
        }
    }

}






