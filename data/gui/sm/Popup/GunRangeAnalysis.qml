import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "GunRangeMenuObject"
    title: "Weapon Range Analysis"
    id : gunRangeMenuId

    Component.onCompleted: {
        if(!running_from_qml){
            GunRangeGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            GunRangeGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    // signal for the UI
    signal qmlHandleGPMarkButtonClicked( bool value )
    signal qmlHandleDirMarkButtonClicked( bool value )
    signal qmlHandleStartButtonClicked()
    signal qmlHandleStopButtonClicked()

    property alias leGunPointLongID : gunPointLongID.text
    property alias leGunPointLatId : gunPointLatID.text
    property alias leGunPointHeightId : gunPointHeightID.text
    property alias leGunType: attrTypeId.value

    property alias leDirectionX : leDirectionXID.text
    property alias leDirectionY : leDirectionYID.text
    property alias leDirectionZ : leDirectionZID.text

    property alias leStepSizeId : stepSize.text

    property alias progressBar : progressBarID.visible
    property alias gunRangeState : gunRangeMenuId.state


    ColumnLayout{
        id:mainLayout
        anchors.fill: parent

        VizGroupBox {
            id : gunPositionGroupBoxId
            title: "Weapon Position"

            GridLayout{
                id: gunPointGrid
                columns: 4

                anchors.fill: parent

                VizLabel {
                    id : gunPointPositionLabelID
                    text: "Position"
                }
                VizTextField {
                    id: gunPointLatID
                    placeholderText : "Latitude"
                    Layout.fillWidth: true
                    validator:  rootStyle.latitudeValidator
                }
                VizTextField {
                    id: gunPointLongID
                    placeholderText : "Longitude"
                    Layout.fillWidth: true
                    validator:  rootStyle.longitudeValidator
                }
                VizButton {
                    id : pbGunPointMarkID
                    text : "   Mark   "
                    checkable: true
                    Layout.alignment: Qt.AlignRight
                    onClicked: {
                        if ( checked ) {
                            pbDirectionMarkID.checked = false
                            qmlHandleGPMarkButtonClicked( true )
                        }
                        else {
                            gunRangeMenuId.state = "defaultPPC"
                            qmlHandleGPMarkButtonClicked( false )
                        }
                    }
                }
                VizLabel {
                    id : gunPointHeightLabelID
                    text: "Height"
                }
                // Launch point altitude text field
                VizTextField {
                    id: gunPointHeightID
                    text: "0"
                    placeholderText : "Height"
                    Layout.fillWidth: true
                    validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation; }
                }
            }
        }
        // Target point

        VizGroupBox {
            id : targetPointGroupBoxId
            title: "Target Direction"

            height:targetPointRow.implicitHeight+50
            width: targetPointRow.implicitWidth+10

            Layout.fillWidth: true

            RowLayout {
                id: targetPointRow
                anchors.fill: parent

                VizLabel {
                    text: "Position"
                }
                VizTextField {
                    id: leDirectionYID
                    Layout.fillWidth: true
                    placeholderText : "Latitude"
                    validator:  DoubleValidator {bottom: -90; top: 90; notation: DoubleValidator.StandardNotation;}
                }
                VizTextField {
                    id: leDirectionXID
                    placeholderText : "Longitude"
                    Layout.fillWidth: true
                    validator:  DoubleValidator {bottom: -180; top: 180; notation: DoubleValidator.StandardNotation;}
                }
                VizTextField {
                    id: leDirectionZID
                    placeholderText : "Altitude"
                    width : 0
                    visible: false
                }

                VizButton {
                    id : pbDirectionMarkID
                    text : "   Mark   "
                    checkable: true
                    onClicked: {
                        if ( checked) {
                            pbGunPointMarkID.checked = false
                            qmlHandleDirMarkButtonClicked( true )
                        }
                        else {
                            gunRangeMenuId.state = "default"
                            qmlHandleDirMarkButtonClicked( false )
                        }
                    }
                }
            }
        }

        RowLayout{

            id:angleAndVelocityRow
            anchors.leftMargin: 360
            z:250

            VizLabel {
                text: "Weapon Type"
            }

            VizComboBox {
                id: attrTypeId
                Layout.minimumWidth: 320*rootStyle.uiScale
                listModel: (typeof(WeaponListModel)=="undefined")?"undefined":WeaponListModel
                value: attrTypeId.get(selectedUID).name
            }

            VizLabel {
                text : "Angle Change"
            }

            VizTextField {
                id: stepSize
                text: "6"
                 Layout.minimumWidth: 1*rootStyle.uiScale
                validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation; }
            }
        }




        RowLayout {
            id : startAndStopButtonsID

            AnimatedImage
            {
                id : progressBarID
                source: rootStyle.getBusyIcon()
                visible: false
            }
            Rectangle{
                Layout.fillWidth: true
            }
            VizButton {
                id : pbStartID
                objectName : "PBStart"
                text : "    Calculate    "

                function handleStartClicked()
                {
                    gunRangeMenuId.state = "pbStartClicked"
//                    pbStopID.focus = true
                    qmlHandleDirMarkButtonClicked(false)
                    qmlHandleGPMarkButtonClicked(false)
                    qmlHandleStartButtonClicked()

                }


                Keys.onEnterPressed: handleStartClicked()
                Keys.onReturnPressed: handleStartClicked()
                onClicked: handleStartClicked()
            }

            /*VizButton{
                id : pbStopID
                text : "    Cancel    "
                onClicked:{
                    gunRangeMenuId.state = "default"
                    qmlHandleStopButtonClicked()
                }
                Keys.onEnterPressed: {
                    gunRangeMenuId.state = "default"
                    qmlHandleStopButtonClicked()
                }
                Keys.onReturnPressed: {
                    gunRangeMenuId.state = "default"
                    qmlHandleStopButtonClicked()
                }
                enabled: false
            }*/
        }
    }
    states:[
        State {
            name: "default"
            PropertyChanges { target: gunPointLatID; textFieldEnable: true }
            PropertyChanges { target: gunPointLongID; textFieldEnable: true }
            PropertyChanges { target: gunPointHeightID; textFieldEnable: true }
            PropertyChanges { target: leDirectionXID; textFieldEnable: true }
            PropertyChanges { target: leDirectionYID; textFieldEnable: true }
            PropertyChanges { target: leDirectionZID; textFieldEnable: true }
            PropertyChanges {target: pbGunPointMarkID; enabled : true; checked : false }
            PropertyChanges {target: pbDirectionMarkID;enabled : true; checked : false  }
            PropertyChanges {target: pbStartID; enabled : true}
//            PropertyChanges {target: pbStopID; enabled : false}

        },
        State {
            name: "pbStartClicked"
            PropertyChanges { target: gunPointLatID; textFieldEnable: false }
            PropertyChanges { target: gunPointLongID; textFieldEnable: false }
            PropertyChanges { target: gunPointHeightID; textFieldEnable: false }
            PropertyChanges { target: leDirectionXID; textFieldEnable: false }
            PropertyChanges { target: leDirectionYID; textFieldEnable: false }
            PropertyChanges { target: leDirectionZID; textFieldEnable: false }
            PropertyChanges {target: pbGunPointMarkID; enabled : false }
            PropertyChanges {target: pbDirectionMarkID;enabled : false  }
            PropertyChanges {target: pbStartID; enabled : false }
//            PropertyChanges {target: pbStopID; enabled : true}

        }
    ]
}
