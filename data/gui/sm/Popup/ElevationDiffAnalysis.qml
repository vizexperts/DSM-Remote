import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: eleDiffAnalysis
    objectName: "eleDiffAnalysis"

    title: "Elevation difference between two points"

    Component.onCompleted: {
        if(!running_from_qml){
             ElevationDiffGUI.setEleDiffEnable(true)
        }
    }

    onClosed: {
        if(!running_from_qml){
            ElevationDiffGUI.setEleDiffEnable(false)
        }
    }
    Component.onDestruction: {
        if(!running_from_qml){
            ElevationDiffGUI.setEleDiffEnable(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias isMarkViewPointSelected: markViewPoint.checked
    property alias isMarkLookAtSelected: markLookAt.checked
    property alias value: resultRect.text
    signal markLookAtPosition(bool value)
    signal markViewPointPosition(bool value)

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            Layout.fillWidth: true
            z:1

            VizLabel{
                text: "Height Difference"
            }

            VizTextField{
                id: resultRect
                readOnly: true
                Layout.minimumWidth : 200
                Layout.fillWidth: true
                text: ""
                placeholderText: ""
             }

        }
        RowLayout{
            Layout.fillWidth: true
            Rectangle{
                Layout.fillWidth  : true
            }
            VizButton{
                id: markViewPoint
                text: "   Point 1   "
                checkable: true
                onCheckedChanged: markViewPointPosition(checked)
                Layout.alignment: Qt.AlignCenter
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: markLookAt
                text: "   Point 2   "
                 //enabled: !calculating
                checkable: true
                onCheckedChanged: markLookAtPosition(checked)
                Layout.alignment: Qt.AlignCenter

            }
            Rectangle{
                Layout.fillWidth: true
            }
        }
    }
}

