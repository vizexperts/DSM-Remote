import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup
{

    id: appSettings
    objectName: "appSettings"
    title: "Application Settings"

    signal jsonParsing();
    property string jsonContent;
    property string jsonSchemaContent;
    property string jsonOutput;
    // This is the list of all editing items that we will have to expose in Gui
    property var editList : [];

    property var curJsonObj;

    Component.onCompleted:
    {
        ApplicationSettingsGUI.connectSlots()
        jsonParsing();
        showJsonData();
    }

    Component.onDestruction:
    {
        if(!running_from_qml)
        {
            ApplicationSettingsGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }


    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property bool pointClamping: false




    function pushEditItem(name,value,schemaObj)
    {

        var newObj = {}
        newObj["name"] = name
        newObj["displayName"] = schemaObj.displayName
        newObj["value"] = value.toString()
        newObj["type"] = schemaObj.type
        newObj["visible"]=schemaObj.visible
        editList.push(newObj);
    }



    ColumnLayout
    {
        id: mainLayout
        Layout.minimumWidth: 500
        Layout.minimumHeight: 550

        spacing: 20
        Layout.fillWidth: true
        anchors.fill: parent
        anchors.margins: 10
        Rectangle{
            id:mainRect
            Layout.minimumWidth: 500
            Layout.minimumHeight: 500
            anchors.left: parent.left
            anchors.right: parent.right
            color: "transparent"
            border.color: rootStyle.colorWhenSelected

            // List Model for editor
            ListModel {
                id: editingItemsListModel
            }

            // ListView for Editing Items
            // It displays the complete editor
            ListView  {
                id: editItemList
                Layout.minimumWidth: 450
                Layout.minimumHeight: 450
                clip: true
                anchors.margins: 10
                anchors.fill: parent

                model: editingItemsListModel;

                Text{
                    color: "#EEE"
                    visible: editItemList.model.count === 0;
                    font.bold: true
                    font.pixelSize: 25
                    text: "No Editing Items to display !";
                    anchors.centerIn: parent
                }

                delegate:  Component{

                    Item{
                        id: editListItem                       
                        width: mainRect.width
                        height: mainRect.height/10

                        property string checkType: type;
                        property var enumValues :  [];
                        property bool isTextInputDisplayed: false;
                        property bool isEnumComboBoxDisplayed:false;
                        property bool isCheckBoxDisplayed:false;
                        property bool enableNumberValidation: type === "int" || type === "float"
                        VizLabel{
                            id:label
                            width:editListItem.width/2
                            text:displayName
                            textAlignment: 10
                            anchors.left: parent.left
                        }


                        VizComboBox{
                            id: enumComboBox
                            visible: isEnumComboBoxDisplayed
                            width : editListItem.width/2
                            anchors.right: parent.right
                            anchors.margins: 20
                            inverted: true
                            listModel:eval(name)
                            onSelectOption:{
                                curJsonObj[name]= value;
                            }
                        }

                        VizTextField{
                            id: userInput
                            visible: isTextInputDisplayed
                            width:editListItem.width/2
                            anchors.right: parent.right
                            anchors.margins: 20
                            text:value

                            onTextChanged:{
                                curJsonObj[name]=userInput.text;
                            }
                        }
                        VizCheckBox
                        {
                            id:checkbox
                            width: editListItem.width/2
                            visible: isCheckBoxDisplayed
                            anchors.right:parent.right
                            anchors.margins: 20
                            onCheckedChanged: {
                                var valueString = checked ? "true": "false";
                                curJsonObj[name]=valueString;
                            }

                        }

                        Component.onCompleted:
                        {
                            if(checkType=="enum")
                            {
                                console.log(name);
                                ApplicationSettingsGUI.createComboBoxItem(name);
                                isEnumComboBoxDisplayed=true;
                                enumComboBox.value=value;
                            }
                            else if(checkType=="float" || checkType=="int" || checkType=="double")
                            {
                                isTextInputDisplayed=true;

                                userInput.validator = rootStyle.generalDoubleValidator
                                userInput.maximumLength=8;
                                userInput.bottom=0;
                                userInput.top=20;
                            }
                            else
                            {
                                isCheckBoxDisplayed=true;
                                if(value=="true")
                                    checkbox.checked="true";

                            }
                        }

                    }
                }
            }
        }

        VizButton{
            id: save
            text:"Save"
            anchors.top:mainRect.bottom
            anchors.horizontalCenter:mainRect.horizontalCenter
            clip:true
            onClicked : {
                createJSON();
                ApplicationSettingsGUI.saveJsonFile();
            }
        }


    }



    function showJsonData()
    {
        curJsonObj = JSON.parse(jsonContent);

        //console.log(jsonContent);
        //console.log(jsonSchemaContent);

        var jsonSchemaObj=JSON.parse(jsonSchemaContent);

        for(var i in curJsonObj)
        {
            var schemaObj = jsonSchemaObj[i];
            pushEditItem(i,curJsonObj[i],schemaObj);
        }
        editingItemsListModel.clear();
        for(var i=0; i<editList.length; i++)
        {
            editingItemsListModel.append(editList[i]);
        }
    }

    function createJSON()
    {
        jsonOutput=JSON.stringify(curJsonObj, null, 2);
    }

}
