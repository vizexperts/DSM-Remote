import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import Qt.labs.folderlistmodel 2.0
import "../VizComponents"
import ".."

Popup{
    id: tacticalSymbolsPopup
    objectName: "tacticalSymbolsPopup"
    title: "Tactical Symbols"

    height: mainLayout.implicitHeight + 15
    width: mainLayout.implicitWidth+ 10

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal mark(bool value, string name)

    property string  tacticalSymbolsDir
    property string sourceImageName
    property alias symbolAffiliation: affiliationComboBox.selectedUID
    property alias symbolName : symbolNameTextField.text
    property alias  symbolStatus: planStatusComboBox.selectedUID

    Component.onCompleted: {
        if(!running_from_qml)
        {
            CreateTacticalSymbolsGUI.popupLoaded()
        }
    }

    Component.onDestruction: {
        mark(false, sourceImageName)
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent.fill
        anchors.right: parent.right
        anchors.top:parent.top
        anchors.bottom:parent.bottom
        anchors.left:parent.left

        RowLayout{
            anchors.left: parent.left
            anchors.right: parent.right
            VizLabel{
                text: "Symbol name"
            }
            VizTextField{
                id: symbolNameTextField
                text: ""
                Layout.fillWidth: true
                placeholderText: "Symbol name"
            }
        }

        RowLayout{
            id: symbolsRow
            spacing: 5
            anchors.left: parent.left
            anchors.right: parent.right
            GridView{
                id: symbolsListView
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumWidth: 550*rootStyle.uiScale
                Layout.minimumHeight: 400*rootStyle.uiScale
                cellWidth: 170*rootStyle.uiScale
                cellHeight: cellWidth
                clip: true
                anchors.margins: 20
                currentIndex: -1
                FolderListModel{
                    id: folderModel
                    nameFilters: ["*.svg"]
                    folder: "file:/" +tacticalSymbolsDir
                    showDirs: false
                }

                model : folderModel

                VizScrollBar{
                    target: symbolsListView
                }
                onCurrentIndexChanged:{
                    symbolNameTextField.text = sourceImageName.substring(0, sourceImageName.length-4).replace(/_/g, ' ')
                }

                delegate: Rectangle{
                    id: listDelegate
                    color: "transparent"
                    border.color: "black"
                    border.width: 2
                    width: 170*rootStyle.uiScale
                    height: width
                    //                    radius: 10
                    Column{
                        id: column
                        anchors.centerIn: parent
                        Image {
                            id: gridIcon
                            width: rootStyle.uiScale*100
                            height: width
                            source: "file:/" +  tacticalSymbolsDir + fileName
                        }
                        VizLabel{
                            id: nameLabel
                            text: fileName.substring(0, fileName.length-4).replace(/_/g, ' ')
                            textWidth:  gridIcon.width
                            wrapMode: TextEdit.Wrap
                        }
                    }

                    MouseArea{
                        id: mousePad
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: {
                            sourceImageName = fileName
                            listDelegate.GridView.view.currentIndex = index
                            //                            symbolInfo.text = description
                            mark(markButton.checked, sourceImageName)
                        }
                    }
                    states: [
                        State {
                            name: "selectedX"
                            when: (listDelegate.GridView.view.currentIndex === index)
                            PropertyChanges {
                                target: listDelegate
                                gradient: rootStyle.gradientWhenSelected
                            }
                            PropertyChanges {
                                target: nameLabel
                                textColor: rootStyle.colorWhenDefault
                            }
                        },
                        State {
                            name: "hovered"
                            when: mousePad.containsMouse
                            PropertyChanges {
                                target: listDelegate
                                color: rootStyle.colorWhenHovered
                                gradient: rootStyle.gradientWhenHovered
                            }
                        }
                    ]
                }
            }

        }

        VizLabel{
            id: symbolInfo
            anchors.horizontalCenter: parent.horizontalCenter
            textWidth: symbolsRow.width
            textAlignment: Qt.AlignLeft
            text: ""
            wrapMode: TextEdit.Wrap
        }
        RowLayout{
            anchors.left: parent.left
            anchors.right: parent.right
            VizLabel{
                text:  "Affiliation"
            }
            VizComboBox{
                id: affiliationComboBox
                Layout.minimumWidth: 150
                Layout.fillWidth: true
                listModel:ListModel{
                    ListElement{name: "Friend"; uID: "0"}
                    ListElement{name: "Hostile"; uID: "1"}
                    ListElement{name: "Neutral"; uID: "2"}
                    ListElement{name: "Unknown"; uID: "3"}
                }
                value: "Friend"
            }
            VizLabel{
                text: "Status"
            }
            VizComboBox{
                id: planStatusComboBox
                Layout.minimumWidth: 150
                Layout.fillWidth: true
                listModel:ListModel{
                    ListElement{name: "Planned"; uID: "0"}
                    ListElement{name: "Current"; uID: "1"}
                }
                value: "Planned"
            }
            VizButton{
                id: markButton
                text: "Mark"
                checkable: true
                onClicked: mark(checked, sourceImageName)
            }
        }
    }
}
