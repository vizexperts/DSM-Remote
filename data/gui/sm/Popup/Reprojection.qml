import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: imageReprojectionId
    objectName: "ReprojectImage"

    title: "Image Reprojection"

    height: mainLayout.implicitHeight+2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+2*mainLayout.anchors.margins

    minimumHeight: mainLayout.Layout.minimumHeight+10
    minimumWidth: mainLayout.Layout.minimumWidth+10

    // signals for UI
    signal okButtonClicked()
    signal viewImage(string imagePath)
    signal browseInputImage()
    signal browseOutputImage()

    property alias inputImagePath : inputImage.text
    property alias outputImagePath: outputImage.text
    property alias projAttrType : attrTypeId.value ;
    property alias leEPSGCode : epsgCodeId.text
    property alias okButtonEnabled: okButtonId.enabled

    property bool calculating: false

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ListModel{
        id: listAttrTypeId
        ListElement{name:"WGS84"; uID : "0"}
        ListElement{name:"WGS72"; uID : "1"}
        ListElement{name:"NAD27"; uID : "2"}
        ListElement{name:"NAD83"; uID : "3"}
        ListElement{name:"EPSG"; uID : "4"}
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{
            id:imageRow
            spacing: 5

            VizLabel {
                text: "Input Image"
            }
            VizTextField {
                id: inputImage
                enabled: !calculating
                Layout.minimumWidth: 200
                text: ""
                placeholderText: "Enter Input Image";
                validator: rootStyle.nameValidator
                Layout.fillWidth: true
            }
            VizButton {
                id : inputBrowseID
                text : "Browse"
                onClicked: browseInputImage()
            }
            VizButton {
                id : inputOpenID
                text : "View"
                onClicked:viewImage(inputImagePath)
            }
        }

        RowLayout{
            spacing: 5

            VizLabel {
                text: "Output Image"
            }
            VizTextField {
                id: outputImage
                enabled: !calculating
                placeholderText: "Enter output Image";
                Layout.minimumWidth: 200
                text: ""
                validator: rootStyle.nameValidator
                Layout.fillWidth: true
            }
            VizButton {
                id : outputBrowseID
                text : "Browse"
                onClicked: browseOutputImage()
            }
            VizButton {
                id : outputOpenID
                text : "View"
                onClicked:viewImage(outputImagePath)
            }
        }

        RowLayout{
            id: layout

            VizLabel {
                text: "Type"
            }
            VizComboBox {
                id: attrTypeId
                Layout.minimumWidth: 150
                Layout.fillWidth: true
                listModel: listAttrTypeId
                value: listAttrTypeId.get(selectedUID).name
            }
            VizLabel{
                visible : epsgCodeId.visible
                text: "EPSG code"
            }
            VizTextField{
                id: epsgCodeId
                visible : false
                validator:IntValidator {bottom: 0; top: 99999;}
                Layout.fillWidth: true
            }
            // for displaying of list populated data
        }

        // setting the default state
        state : "typeInteger"

        states: [
            //                State {
            //                    name: "typeFloat";
            //                    when: attrTypeId.value == "NAD27"
            //                    PropertyChanges { target: epsgCodeId; text: "4326" }
            //                    PropertyChanges { target: epsgCodeId; visible: false }
            //                },
            //                State {
            //                    name: "typeFloat";
            //                    when: attrTypeId.value == "NAD27"
            //                    PropertyChanges { target: epsgCodeId; text: "4326" }
            //                    PropertyChanges { target: epsgCodeId; visible: false }
            //                },
            State {
                name: "typeInteger";
                when: attrTypeId.value == "WGS84"

                PropertyChanges { target: epsgCodeId; text: "4326" }
                PropertyChanges { target: epsgCodeId; visible: false }
            },
            //                State {
            //                    name: "typeInteger";
            //                    when: attrTypeId.value == "WGS84"

            //                    PropertyChanges { target: epsgCodeId; text: "4326" }
            //                    PropertyChanges { target: epsgCodeId; visible: false }
            //                },
            State {
                name: "typeText";
                when: attrTypeId.value == "EPSG"

                PropertyChanges { target: epsgCodeId; text: "4326" }
                PropertyChanges { target: epsgCodeId; visible: true }
            }
        ]




        RowLayout {
            id: mainButtonId

            AnimatedImage{
                id:progressBarId
                source: rootStyle.getBusyIcon()
                visible: calculating
            }

            Rectangle{
                Layout.fillWidth: true
            }

            VizButton {
                id: okButtonId
                text: "    Ok    ";
                onClicked: {
                    okButtonClicked();
                }
            }
            VizButton {
                id: cancelButtonId
                text: "Cancel"

                onClicked: {
                    smpFileMenu.close()
                }
            }
        }
    }



}
