import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "flyAroundPopup"
    title: "Fly around"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth
    minimumHeight: mainLayout.Layout.minimumHeight

    signal startExistingFlyAround(string value)
    signal saveFlyAround()
    signal stopFlyAround()
    signal previewFlyAround()
    signal pauseFlyAround()
    signal editFlyAround(string value,bool checked)
    signal toggleOpenFlyaroundParameterGrid(string value)
    signal visualise(string value)
    signal increaseSpeed()
    signal decreaseSpeed()
    signal stopFlyAroundPreview()
    signal apply()
    signal deleteSelectedFlyaround(string value)

    property alias flyaroundNameString : flyaroundName.text
    property alias distanceString: distance.text
    property alias heightString: height.text
    property alias lookAtHeightString: lookAtHeight.text
    property alias arcAngleString: arcAngle.text
    property alias startAngleString : startAngle.text
    property alias preview: previewButton.enabled
    property alias start: startButton.enabled
    property alias stop: stopButton.enabled
    property alias value: flyAroundBox.value
    property alias loop: loopButton.checked
    property alias editFlyAroundButton: editButton.checked
    property alias playVisible: startButton.visible
    property alias pauseVisible: pauseButton.visible
    property bool openFlyaroundParameterGrid: false
    property alias reverseChecked :reverseButton.checked

    Component.onCompleted: {

        if(!running_from_qml){
           FlyAroundGUI.setActive(true)
        }

    }

    Component.onDestruction: {
        if(!running_from_qml){
           FlyAroundGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent

        RowLayout{
            id:layerNameRow
            spacing: 5
            z: 10

            VizLabel{
                text: "Choose "
                textAlignment: Qt.AlignLeft
                Layout.alignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: flyAroundBox
                    Layout.minimumWidth: 200
                    listModel: typeof(flyAroundList)== "undefined" ? "undefined" : flyAroundList
                    value: flyAroundBox.get(selectedUID).name
                    onSelectOption: {
                        if (editButton.checked)
                        {
                            editFlyAround(flyAroundBox.value,editButton.checked)
                        }

                        toggleOpenFlyaroundParameterGrid(value)
                        visualise(value)
                    }

            }

            VizButton{
                id:editButton
                Layout.minimumWidth: 60
                checkable: true
                text: "Edit"
                onClicked: editFlyAround(flyAroundBox.value,editButton.checked)
            }

            VizButton{
                id:deleteButton
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 50
                text: " Delete "
                onClicked:
                {
                    deleteSelectedFlyaround(flyAroundBox.value)
                    if (editButton.checked)
                    {
                        editButton.checked = false
                    }
                }
            }
        }

        VizGroupBox{
                    id:flyAroundParameterGroupBox
                    title: "New"
                    visible: flyAroundBox.value == "Create New Fly around" || openFlyaroundParameterGrid
                    clip: true
                    Layout.alignment: Qt.AlignLeft
                    Layout.fillWidth: true

                    GridLayout{
                        id: parametersGrid
                        columns: 3
                        anchors.fill: parent

                        VizLabel{
                            text:"Name"
                            textAlignment: Qt.AlignLeft
                            Layout.minimumWidth: 80
                        }
                        VizTextField{
                            id: flyaroundName
                            Layout.fillWidth: true
                            Layout.minimumWidth: 120
                            text: ""
                            placeholderText: "Name of Flyaround"
//                            validator: RegExpValidator{
//                                // for restricting space in name string
//                                regExp: /^[a-zA-Z0-9_]*$/
//                            }

                        }

                        Rectangle{
                            Layout.fillWidth: true
                        }


                        VizLabel{
                            text:"Distance"
                            textAlignment: Qt.AlignLeft
                            Layout.minimumWidth: 80
                        }

                        VizTextField{
                            id: distance
                            text: ""
                            Layout.fillWidth: true
                            placeholderText: "Distance"
                            validator: DoubleValidator{ bottom: 0;top: 99999999; decimals: 6; notation: DoubleValidator.StandardNotation; }
                        }

                        Rectangle{
                            Layout.fillWidth: true
                        }

                        VizLabel{
                            text:"Viewing Height"
                            textAlignment: Qt.AlignLeft
                            Layout.minimumWidth: 80
                        }
                        VizTextField{
                            id: height
                            text: ""
                            placeholderText: "Height from ground"
                            Layout.fillWidth: true
                            validator: DoubleValidator{ bottom: 0;top: 99999999; decimals: 6; notation: DoubleValidator.StandardNotation; }
                        }

                        Rectangle{
                            Layout.fillWidth: true
                        }

                        VizLabel{
                            text:"Look At Height"
                            textAlignment: Qt.AlignLeft
                            Layout.minimumWidth: 80
                        }
                        VizTextField{
                            id: lookAtHeight
                            text: ""
                            placeholderText: "Height from ground"
                            Layout.fillWidth: true
                            validator: DoubleValidator{ bottom: 0;top: 99999999; decimals: 6; notation: DoubleValidator.StandardNotation; }
                        }

                        Rectangle{
                            Layout.fillWidth: true
                        }

                        VizLabel{
                            text:"Arc Angle"
                            textAlignment: Qt.AlignLeft
                            Layout.minimumWidth: 80
                        }

                        VizTextField{
                            id: arcAngle
                            text: ""
                            placeholderText: "[0-360] degrees"
                            Layout.fillWidth: true
                            validator: DoubleValidator{ bottom: 0; top: 360; decimals: 6; notation: DoubleValidator.StandardNotation; }
                        }

                        VizCheckBox{
                            id: reverseButton
                            text: "Reverse"
                            checked: reverseChecked
                        }

                        VizLabel{
                            text:"Start Angle"
                            textAlignment: Qt.AlignLeft
                            Layout.minimumWidth: 80
                        }

                        VizTextField{
                            id: startAngle
                            text: ""
                            placeholderText: "[0-360] degrees"
                            Layout.fillWidth: true
                            validator: DoubleValidator{ bottom: 0;top: 360; decimals: 6; notation: DoubleValidator.StandardNotation; }
                        }

                        Rectangle{
                            Layout.fillWidth: true
                        }

                        VizLabel{
                            text: "Loop     "
                            textAlignment: Qt.AlignLeft
                        }
                        VizCheckBox{
                            id: loopButton
                            checked: loop
                        }

                        Rectangle{
                            Layout.fillWidth: true
                        }

                        VizButton{
                            id:previewButton
                            Layout.alignment: Qt.AlignHCenter
                            Layout.minimumWidth: 50
                            text: " Preview "
                            onClicked: previewFlyAround()
                        }

                        VizButton{
                            id:stopPreviewButton
                            Layout.alignment: Qt.AlignHCenter
                            Layout.minimumWidth: 50
                            text: " Stop "
                            onClicked: stopFlyAroundPreview()
                        }

                        VizButton{
                            id: saveButton
                            Layout.alignment: Qt.AlignHCenter
                            Layout.minimumWidth: 50
                            text:" Save "
                            onClicked: {
                                if(editButton.checked)
                                {
                                  apply()
                                }

                                else
                                {
                                    saveFlyAround()
                                }
                            }
                        }
                }

            }

        RowLayout{
            id:commandButtonRect
            spacing: 10
            Layout.alignment: Qt.AlignHCenter
            visible: flyAroundBox.value != "Create New Fly around" && flyAroundBox.value != "Select Fly around" && !openFlyaroundParameterGrid
			
            VizButton{
                id:decreaseSpeedButton
                iconSource: rootStyle.getIconPath("time/slower")
                Layout.minimumWidth: 50
                backGroundVisible: false
                onClicked: decreaseSpeed()
            }

            VizButton{
                id:startButton
                iconSource: rootStyle.getIconPath("time/play")
                Layout.minimumWidth: 50
                backGroundVisible: false
                visible: true
                onClicked:
                {
                    visible = false
                    pauseButton.visible = true
                    startExistingFlyAround(flyAroundBox.value)

                }
            }

            VizButton{
                id:pauseButton
                iconSource: rootStyle.getIconPath("time/pause")
                Layout.minimumWidth: 50
                backGroundVisible: false
                visible: false
                onClicked:
                {
                    visible = false
                    startButton.visible = true
                    pauseFlyAround()

                }
            }

            VizButton{
                id:stopButton
                iconSource: rootStyle.getIconPath("time/stop")
                Layout.minimumWidth: 50
                backGroundVisible: false
                onClicked: stopFlyAround()
            }

            VizButton{
                id:increaseSpeedButton
                iconSource: rootStyle.getIconPath("time/faster")
                Layout.minimumWidth: 50
                backGroundVisible: false
                onClicked: increaseSpeed()
            }

        }

     }
}



