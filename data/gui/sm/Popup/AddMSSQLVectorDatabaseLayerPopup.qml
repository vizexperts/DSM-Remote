import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "addMSSQLDatabaseLayerPopup"
    title: "Add New Database Vector Layer"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal addMSSQLTable()
    signal dbNameChanged(string tablenameName)
    signal msTablenameChanged(string tablenameName)

    property alias schema: dbNameComboBox.value
    property alias annotation: annotationComboBox.value
    //    property alias startDate: startDateTextBox.text
    //    property alias endDate: endDateTextBox.text
    property alias tableName: tablenNameComboBox.value
    property alias plotType: plotTypeComboBox.value

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ListModel{
        id: listAttrTypeId
        ListElement{name:"Sitrep"; uID : "0"}
        ListElement{name:"BFSNew"; uID : "1"}
        //        ListElement{name:"FN MAG"; uID : "2"}
        //        ListElement{name:"AK-103"; uID : "3"}
        //        ListElement{name:"Dragunov"; uID : "4"}
        //        ListElement{name:"Vidhwansak"; uID : "5"}
    }
    ListModel{
        id: plotTypeId
        ListElement{name:"Mapsheet/GR"; uID : "0"}
        ListElement{name:"Longitude/Latitude"; uID : "1"}
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        RowLayout{
            id:layerNameRow
            spacing: 5
            z:60
            VizLabel{
                text: "Database Name"
                textAlignment: Qt.AlignLeft
            }
            VizComboBox {
                id: dbNameComboBox
                Layout.minimumWidth: 250*rootStyle.uiScale
                listModel:(typeof(dbNameListModel)=="undefined")?"undefined":dbNameListModel
                value: "<None>"
                Layout.fillWidth: true
                onSelectOption: dbNameChanged(value)
                z:50
            }
        }

        VizGroupBox{
            title: "Data Source"
            Layout.fillWidth: true
            z:40
            ColumnLayout{
                id: sourceGrp
                width: parent.width

                RowLayout{
                    id: dataSourceCol
                    spacing: 5
                    z:20
                    VizLabel{
                        text: "Table Name"
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox {
                        id: tablenNameComboBox
                        width: 300
                        listModel:(typeof(tableNameListModel)=="undefined")?"undefined":tableNameListModel
                        value: "<None>"
                        onSelectOption: msTablenameChanged(value)
                        Layout.fillWidth: true
                        z:10
                    }
                }
            }
        }

        VizGroupBox{
            title: "Style"
            Layout.fillWidth: true
            z:30
            ColumnLayout{
                id: dateFilterGrp
                width: parent.width

                RowLayout{
                    id: plotTypeRow
                    spacing: 5
                    z:11
                    VizLabel{
                        text: "Plotting Type"
                        textAlignment: Qt.AlignLeft
                    }

                    VizComboBox {
                        id: plotTypeComboBox
                        width: 300
                        listModel:plotTypeId
                        value: plotTypeId.get(selectedUID).name
                        Layout.fillWidth: true
                         onSelectOption: msTablenameChanged(tablenNameComboBox.value)
                    }
                }

                RowLayout{
                    id: annotationCol
                    spacing: 5
                    z:10
                    VizLabel{
                        text: "Annotation"
                        textAlignment: Qt.AlignLeft
                    }
                    VizComboBox {
                        id: annotationComboBox
                        width: 300
                        listModel:(typeof(colNameListModel)=="undefined")?"undefined":colNameListModel
                        value: "<None>"
                        Layout.fillWidth: true
                        z:10
                    }
                }

                RowLayout{
                    id: info
                    spacing: 5
                    z: 5
                    VizLabel{
                        text: ""
                        textAlignment: Qt.AlignLeft
                    }
                }
            }
        }

        RowLayout{
            id:commandButtonRect
            spacing: 10

            Rectangle{
                width: parent.width - okButton.width - cancelButton.width -20
                Layout.fillWidth: true
            }

            VizButton{
                id:okButton
                text: "   Ok   "
                width: 100
                onClicked:addMSSQLTable()
            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                width: 100
                onClicked: closePopup()
            }
        }
    }
}

