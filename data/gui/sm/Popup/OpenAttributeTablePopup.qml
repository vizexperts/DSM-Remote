import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: openAttributeTablePopupId
    objectName: "openAttributeTablePopup"
    title: "Attribute Table : "+fileName

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    //Signal defined for buttons
    signal browseButtonClicked()
    signal saveButtonClicked();
    property bool readOnlyFlag:true;
    property string fileName:"";
    function addTableViewColumn(paramRole,fieldType,index)
    {
        var columnToAdd = Qt.createComponent("AddColumn.qml");
        if(columnToAdd.status === Component.Ready)
        {

            queryTableViewId.addColumn(columnToAdd.createObject(queryTableViewId))
            queryTableViewId.getColumn(index).role=paramRole;
            queryTableViewId.getColumn(index).title=paramRole+" "+"("+fieldType+")";
            queryTableViewId.getColumn(index).elideMode =Text.ElideLeft
        }
    }


//    Component{
//        id: tableViewColumnComponent
//        TableViewColumn{

//        }
//    }
    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent

        VizTableView{
            id: queryTableViewId
            model: (typeof(queryTableModel) !== "undefined") ? queryTableModel : "undefined"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth:  400
            Layout.minimumHeight: 450
            highlightOnFocus: false
            clip: true;
            objectName: "openAttributeTableView"
            itemDelegate: VizTextField {
                    id:textId
                    text: styleData.value
                    readOnly: readOnlyFlag
                    Layout.fillWidth: true
                    placeholderText: "Null"
                    onTextEdited:
                    {
                        parent.forceActiveFocus()
                        if(!readOnlyFlag)
                        {
                            if(!queryTableViewId.model.setData(styleData.row,styleData.column,text))
                            {

                                textId.text=queryTableViewId.model.getData(styleData.row,styleData.column)

                            }
                        }

                    }
             }

            onClicked: {
                    //handle Selection
                }




        }

        RowLayout{
            id: commandButtons
            spacing: 10
            Layout.alignment: Qt.AlignRight

            VizButton{
                id:saveButton
                text: "Edit"
                checked: false
                onClicked: {
                    checked = !checked
                    readOnlyFlag=!readOnlyFlag

                }
            }
            VizButton{
                id:saveButtonId
                text: "Save"
                onClicked:
                {
                    readOnlyFlag=true;
                    saveButton.checked=false;
                    saveButtonClicked()
                }
              }
            VizButton{
                id:saveAsButtonId
                text: "Save As"
                onClicked:
                {
                    readOnlyFlag=true;
                    saveButton.checked=false;
                    browseButtonClicked()
                }
              }

        }
    }

}

