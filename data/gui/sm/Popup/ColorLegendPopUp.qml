import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup {

    id : colorLegend
    objectName: "colorLegend"
    title: "Legend"
    property bool colorPickerVisible: false
    property int currentSelectedIndex: -1

    property string colorMapValue: ""

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Component.onCompleted: {
        ColorLegendGUI.handleSelectColorMap();
    }
    Component.onDestruction: {
         // colorLegend.setActive(false);
    }

    Connections{
           target: smpFileMenu
           onSizeChanged : {
               minimumWidth = minimumWidth + newWidth
               minimumHeight =minimumHeight +newHeight
           }
       }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
    VizTableView {
        id:colorListView
        headerVisible: false
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.minimumWidth: 200
        Layout.minimumHeight: 300
        model:(typeof(colorLegendListModel) !== "undefined") ? colorLegendListModel : "undefined"


        TableViewColumn {
            role: "cellColor"
            width:66
            delegate:VizButton{
                id:colorRectangle
                backgroundColor : modelData.cellColor
                flatButton: true
            }
        }
        TableViewColumn {
            role: "minVal"
            width:66
        }
        TableViewColumn {
            role: "maxVal"
            width:66
        }
    }
  }
    onColorMapValueChanged:{
        colorLegend.handleChangeColorMap(colorMapValue);
    }
}

