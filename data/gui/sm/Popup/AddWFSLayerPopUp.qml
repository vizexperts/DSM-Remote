import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."


Popup {
    id:addWfsLayerPopUp
    objectName: "addWFSLayerPopUp"
    title: "Add WFS Layer"


    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 40
    minimumHeight: mainLayout.Layout.minimumHeight +150

    signal wfsConnectButtonClicked()
    signal addButtonClicked()
    signal enteredByUser(string name)

    //Properties associted with parameters
    property alias layerName:layerNameField.text
    property alias value: serverList.value
    property alias serverAddress:pathTextFeild.text

    //Properties associated with buttons
    property alias connectButtonEnabled: connectButton.enabled
    property alias browseButtonEnabled: browseButton.enabled
    property alias addButtonEnabled: addButton.enabled
    property alias wfsTreeVisible:wfsTreeView.visible
    property bool treeVisible

    Component.onDestruction: {
        if(!running_from_qml){
           AddServerLayerGUI.reset()
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ListModel{
        id: modelOptions

        ListElement{
            name:"json"
            value: "json"
        }
        ListElement{
            name:"gml"
            value: "gml"
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{
            id:layerRow
            spacing:5
            VizLabel{
                text: "Layer Name   "
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: layerNameField
                Layout.minimumWidth: 300
                text: ""
                validator:rootStyle.nameValidator
                placeholderText: "Give a name to this layer..."
                Layout.fillWidth: true
                onTextEdited: enteredByUser(text)
           }
        }

        RowLayout{
            id:pathRow
            spacing:5
            z : 100
            VizLabel{
                text: "Choose Server"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: serverList
                    Layout.minimumWidth: 150
                    Layout.fillWidth: true
                    listModel: typeof(georbISServerList)== "undefined" ? "undefined" : georbISServerList
                    value: serverList.get(selectedUID).name
            }

            VizButton{
                id:connectButton
                visible : serverList.value != "Custom"
                text:"Connect"
                Layout.alignment: Qt.AlignRight
                onClicked:wfsConnectButtonClicked();
            }
        }

        RowLayout{
            id:customPath
            spacing:5
            z : -10
            visible: serverList.value == "Custom"
            VizLabel{
                text:"Path             "
                textAlignment: Qt.AlignLeft
            }

           VizTextField{
               id:pathTextFeild
               placeholderText:"Enter URL for WFS data"
               text:""
               Layout.minimumWidth: 250
            }

           VizButton{
               id:browseButton
               text:"Connect"
               onClicked:wfsConnectButtonClicked();
               Layout.alignment: Qt.AlignRight
           }
        }

        VizTreeView{
            id:wfsTreeView
            z : -10
            objectName: "wfsTreeView"
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: treeVisible
            model:(typeof(wfsTreeModel)!== "undefined") ? wfsTreeModel : "undefined"
            onModelChanged:{
                if((model === null)||(model === "undefined"))
                    treeVisible = false
                else
                    treeVisible = true
            }
            VizTreeViewColumn{
                role: "name"
                delegate:VizLabel{
                    text:styleData.value
                    textColor:rootStyle.textColor
                }
            }
            VizTreeViewColumn{
                role: "title"
                delegate:VizLabel{
                    text:styleData.value
                    textColor:rootStyle.textColor
                }
            }
            VizTreeViewColumn{
                role: "abstraction"
                delegate:VizLabel{
                    text:styleData.value
                    textColor:rootStyle.textColor
                }
            }
        }

        RowLayout{
            id:controlButton
            spacing:10
            Layout.alignment: Qt.AlignRight
            VizButton{
                id:addButton
                text: "Add"
                onClicked: addButtonClicked()
            }
        }
    }
}
