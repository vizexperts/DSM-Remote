import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import "../VizComponents"
import ".."

Image{
    id: resources
    objectName: "smpResourcesMenu"

    property string symbology: "Military"

    Component.onCompleted: {
        popupContainer.setTitle("Symbology")
        popupContainer.x = root.width/2 - width/2
        popupContainer.y = root.height/2 - height/2

        smpFileMenu.popupLoaded(objectName)
        smpFileMenu.isPopUpAlreadyOpen=true;
    }
    Component.onDestruction: {
        smpFileMenu.isPopUpAlreadyOpen=false;
        root.nowRemoveHighLight();
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }


    signal showAffiliation(string affiliation)
    signal showCategory(string affiliation, string category)
    signal addSymbol(string affiliation, string category, string symbolName)
    signal setSymbolSize(string symbolSize)

    height: root.height*0.75
    width :  root.width/2

    source:rootStyle.getIconPath("loginScreen")
    property string selectedAffiliation
    property string selectedCategory
    property string selectedSymbol
    property bool valueSet: false
    property alias symbolSize : symbolResourcesId.value
    property string overlayerColor : "blue"
    clip: true

    TextEdit {
        id: titleText
        anchors.left: parent.left; anchors.top: parent.top
        anchors.leftMargin: 37; anchors.topMargin: 37
        color: "#ffffff"
        text: symbology+" Symbology"
        font.family: rootStyle.fontFamily
        readOnly: true
        font.pixelSize: 36*rootStyle.uiScale
    }

    Image{
        id: image
        source: rootStyle.getIconPath("Menu/resource")
        anchors.verticalCenter: titleText.verticalCenter; anchors.left: parent.left
        anchors.leftMargin: titleText.width + 100
        fillMode: Image.PreserveAspectFit
        width: 70*rootStyle.uiScale; height: 70*rootStyle.uiScale
    }

    RowLayout{
        anchors.right : parent.right
        anchors.rightMargin: 20
        anchors.top : parent.top
        anchors.topMargin:35
        height :30

        VizLabel {
            id: symbolSizeLabel
            text : "Symbol Size"
        }

        VizComboBox {
            id: symbolResourcesId

            listModel:ListModel{

                ListElement{name:"16"; uID : "16"}
                ListElement{name:"32"; uID : "32"}
                ListElement{name:"48"; uID : "48"}
                ListElement{name:"64"; uID : "64"}
                ListElement{name:"80"; uID : "80"}
                ListElement{name:"96"; uID : "96"}
                ListElement{name:"112"; uID : "112"}
                ListElement{name:"128"; uID : "128"}
            }
            value: symbolSize
            onSelectOption: {
                if (value != "")
                {
                    //console.log(value)
                    setSymbolSize(value)
                }
                else
                {
                    //do nothing
                }
            }
        }
    }

    ListView{
        id: affiliationListView
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        model: affiliationModel
        anchors.top: titleText.bottom
        anchors.topMargin: 30
        height: 50*rootStyle.uiScale
        orientation: ListView.Horizontal
        boundsBehavior : Flickable.StopAtBounds
        delegate: VizButton{
            text: modelData
            width: affiliationListView.width/affiliationModel.count
            height: 50*rootStyle.uiScale
            checked: (ListView.view.currentIndex == index)
            onClicked: {
                if(!checked){
                    ListView.view.currentIndex = index
                    affiliationModel.move(index, 0, 1)
                    selectedAffiliation = modelData
                    showAffiliation(modelData)
                    if(text == "Hostile")
                    {
                        overlayerColor = "red"
                    }else if(text == "Friend")
                    {
                        overlayerColor = "blue"
                    }
                    else if(text == "Neutral")
                    {
                        overlayerColor = "green"
                    }else
                    {
                        overlayerColor = "yellow"
                    }
                }
            }
        }
    }

    ListView{
        id: categoriesListView
        width: affiliationListView.width/affiliationModel.count
        anchors.left: affiliationListView.left
        anchors.top: affiliationListView.bottom
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        boundsBehavior : Flickable.StopAtBounds
        model: (typeof(categoriesModel) !== "undefined") ? categoriesModel : "undefined"
        clip: true
        highlightFollowsCurrentItem: true
        VizScrollBar{
            target: categoriesListView
        }
        delegate: Rectangle{
            id: delegate
            radius: 10
            color: "black"
            width: categoriesListView.width
            height: 50*rootStyle.uiScale
            gradient: rootStyle.gradientWhenDefault
            TextEdit{
                anchors.fill: parent
                horizontalAlignment: TextEdit.AlignHCenter
                text: modelData
                readOnly: true
                font.pointSize: 11*rootStyle.uiScale
                font.family: rootStyle.fontFamily
                font.weight: Font.DemiBold
                wrapMode: TextEdit.WrapAnywhere
                color: "white"
            }
            MouseArea{
                id: mousePad
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    if(delegate.ListView.view.currentIndex !== index){
                        delegate.ListView.view.currentIndex = index
                        selectedCategory = modelData
                        showCategory(selectedAffiliation, selectedCategory)
                    }
                }
            }
            states: [
                State {
                    name: "selectedX"
                    when: (delegate.ListView.view.currentIndex === index)
                    PropertyChanges {
                        target: delegate
                        color: rootStyle.colorWhenSelected
                        gradient: rootStyle.blueGradient
                    }
                },
                State {
                    name: "hovered"
                    when: mousePad.containsMouse
                    PropertyChanges {
                        target: delegate
                        color: rootStyle.colorWhenHovered
                        gradient: rootStyle.gradientWhenHovered
                    }
                }
            ]
        }
    }

    GridView{
        id: symbolsGridView
        anchors.left: categoriesListView.right
        anchors.right: parent.right
        anchors.top: affiliationListView.bottom
        anchors.bottom: parent.bottom
        clip: true
        anchors.margins: 20
        cellHeight: 110*rootStyle.uiScale
        cellWidth: 110*rootStyle.uiScale
        model: (typeof(symbolsModel) !== "undefined") ? symbolsModel : "undefined"
        VizScrollBar{
            target: symbolsGridView
        }
        delegate: Item{
            id: gridDelegate
            width: 110*rootStyle.uiScale
            height: 110*rootStyle.uiScale
            Rectangle{
                id: rect
                width: 100*rootStyle.uiScale
                height: 100*rootStyle.uiScale
                radius: 5
                anchors.centerIn: parent
                gradient: Gradient {
                        GradientStop { position: 0.0; color: "#cccccc" }
                        GradientStop { position: 1.0; color: "#666666" }
                    }
                Image {
                    id: im
                    source: path
                    sourceSize.width: 50*rootStyle.uiScale
                    sourceSize.height: 50*rootStyle.uiScale
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                    visible : (im.source.toString().indexOf(".svg") >= 0)? false : true
                }
                ColorOverlay {
                    anchors.fill: im
                    source: im
                    color: (im.source.toString().indexOf(".svg") >= 0)? overlayerColor : "transparent"
                }
                TextEdit{
                    id: label
                    anchors.top: im.bottom
                    text: name
                    readOnly: true
                    font.pointSize: 9*rootStyle.uiScale
                    font.family: rootStyle.fontFamily
                    font.weight: Font.DemiBold
                    color: "white"
                    width: parent.width
                    horizontalAlignment: TextEdit.AlignHCenter
                    wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
                }
                MouseArea{
                    id: mousePad2
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        gridDelegate.GridView.view.currentIndex = index
                        if(selectedSymbol == name){
                            addSymbol(selectedAffiliation, selectedCategory, selectedSymbol)
                        }
                        else{
                            selectedSymbol = name
                        }
                    }
                }
            }
            states: [
                State {
                    name: "selectedX"
                    when: (gridDelegate.GridView.view.currentIndex === index)
                    PropertyChanges {
                        target: rect
                        color: rootStyle.colorWhenSelected
                        gradient: rootStyle.greyGradient
                    }
                    PropertyChanges{
                        target: label
                        color: "black"
                    }
                },
                State {
                    name: "hovered"
                    when: mousePad.containsMouse
                    PropertyChanges {
                        target: rect
                        color: rootStyle.colorWhenHovered
                        gradient: rootStyle.gradientWhenHovered
                    }
                }
            ]
        }
    }

    ListModel{
        id: affiliationModel
        ListElement{name: "Friend"}
        ListElement{name: "Hostile"}
        ListElement{name: "Neutral"}
        ListElement{name: "Unknown"}
    }
}
