import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: minmaxAnalysis
    objectName: "MinMaxElev"
    title: "Min/Max elevation analysis"

    Component.onCompleted: {
        if(!running_from_qml){
            MinMaxElevGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            MinMaxElevGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //properties
    //property alias outputName : outputName.text
    property alias minLatitude : minLat.text
    property alias minLongitude : minLong.text
    property alias minAltitude : minAlt.text

    property alias maxLatitude : maxLat.text
    property alias maxLongitude : maxLong.text
    property alias maxAltitude : maxAlt.text

    property alias bottomLeftLongitude : bottomLeftLongitude.text
    property alias bottomLeftLatitude : bottomLeftLatitude.text
    property alias topRightLongitude : topRightLongitude.text
    property alias topRightLatitude : topRightLatitude.text

    //states of buttons
    property alias isStartButtonEnabled : startButton.enabled
    //property alias isStopButtonEnabled: stopButton.enabled
    property alias isMarkSelected : markButton.checked
    property bool calculating: false

    // property alias isProgressBarVisible : pb2.visible
    //property alias progressValue: pb2.value

    signal startAnalysis()
    signal stopAnalysis()
    signal markPosition(bool value)
    signal _updateMinMax()
    signal _resetMinMax()
    //signal getprogressBar()
    //signal populateAnalysisListMenu()
    //signal populateColorMapMenu()
    //signal selectColorListMap()
    //signal selectAnalysisList()



    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        VizGroupBox{
            id : areaGroupBox
            title: "Area to be considered"

            GridLayout{
                id: areaGrid
                columns: 3
                anchors.fill: parent
                VizLabel{
                    text:"Bottom left"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: bottomLeftLatitude
                    Layout.fillWidth: true
                    text: ""
                    placeholderText: "Latitude"
                    validator:rootStyle.latitudeValidator
                }
                VizTextField{
                    id: bottomLeftLongitude
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: "Longtude"
                    validator: rootStyle.longitudeValidator
                }                
                VizLabel{
                    text:"Top right"
                    textAlignment: Qt.AlignLeft
                }

                VizTextField{
                    id: topRightLatitude
                    text: ""
                    placeholderText: "Latitude"
                    Layout.fillWidth: true
                    validator:rootStyle.latitudeValidator
                }
                VizTextField{
                    id: topRightLongitude
                    text: ""
                    placeholderText: "Longitude"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                }

                Rectangle{
                    Layout.fillWidth: true
                }
                VizButton{
                    id: markButton
                    objectName: "markArea"
                    text:"    Mark    "
                    backGroundVisible: true
                    checkable : true
                    Layout.alignment: Qt.AlignRight
                    onClicked: markPosition(checked)
                }
            }
        }

        VizGroupBox{
            id : analysisParameters
            title: "Results"
            z:5

            GridLayout{
                id: parameterGrid
                columns: 4
                anchors.fill: parent
                VizLabel{
                    id : minimum
                    text:"Lowest point"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: minLat
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: ""
                    validator: rootStyle.nameValidator
                }
                VizTextField{
                    id: minLong
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: ""
                    validator: rootStyle.nameValidator
                }
                VizTextField{
                    id: minAlt
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: ""
                    validator: rootStyle.nameValidator
                }
                VizLabel{
                    id : maximum
                    text:"Highest point"
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: maxLat
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: ""
                    validator: rootStyle.nameValidator
                }
                VizTextField{
                    id: maxLong
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: ""
                    validator: rootStyle.nameValidator
                }
                VizTextField{
                    id: maxAlt
                    text: ""
                    Layout.fillWidth: true
                    placeholderText: ""
                    validator: rootStyle.nameValidator
                }
            }
        }

        /*RowLayout{
            id:commandRect
            Layout.fillWidth: true
            VizProgressBar{
                id : pb2
                value: 0
                minimum: 0
                maximum: 100
                indeterminate: false
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                onValueChanged:{
                    if( value == 100)
                    {
                        startButton.enabled = true
                        stopButton.enabled = false
                    }
                }
            }
            Rectangle{
                Layout.fillWidth: true
            }*/

        RowLayout {
            id:rowControl

            AnimatedImage{
                id:progressBarId
                Layout.fillWidth: true
                source: rootStyle.getBusyIcon()
                visible: calculating
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: startButton
                objectName: "startButton"
                text: "   Calculate   "
                onClicked:{
                    if((bottomLeftLongitude.text=="")||(bottomLeftLatitude.text=="")||(topRightLongitude.text=="")||(topRightLatitude.text==""))
                    {
                        startAnalysis()
                        startButton.enabled = true
                    }
                    else
                    {
                        //stopButton.enabled = true
                        startButton.enabled = false
                        startAnalysis()
                    }
                }
            }
            /*VizButton{
                id : stopButton
                objectName: "stopButton"
                text: "   Cancel   "
                enabled: false
                onClicked:{
                    stopAnalysis()
                    stopButton.enabled = false
                    startButton.enabled = true

                }
            }*/
        }
    }
}


