import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"
import ".."

Popup {
    id: saveAsTemplate
    objectName: "saveAsTemplate"
    title:  "Save As Template"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal projectSaveAsTemplate()

    onProjectSaveAsTemplate: if(!running_from_qml) ProjectManagerGUI.projectSaveAsTemplate(nameText.text)

    property alias templateName : nameText.text


    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            VizLabel {
                text:"Name"
            }
            VizTextField {
                id:nameText
                text:""
                Layout.minimumWidth: 250
                placeholderText: "Type template name here"
                validator: rootStyle.nameValidator
                Layout.fillWidth: true
            }
        }

        RowLayout {
            id:buttonRow
            Layout.alignment: Qt.AlignCenter
            VizButton {
                id: button
                text:"Save"
                onClicked: projectSaveAsTemplate()
            }

            VizButton {
                id: cancelButton
                text:"Cancel"
                onClicked: closePopup()
            }
        }
    }
}
