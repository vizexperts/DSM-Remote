import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."
import "../VizComponents/javascripts/functions.js" as Functions

/*
     \popup AddEventGUI.qml
     \author Nitish Puri
     \brief Add timed events to the scene
 */

Popup {
    id: addEventGUI
    objectName: "addEventGUI"

    title: "Add Event"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Behavior on height {
        PropertyAnimation{
            easing.type: Easing.OutExpo
            duration: 400
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    Component.onCompleted: {
        if(running_from_qml){
            arrivalTime = new Date()
            departureTime = arrivalTime
        }
        smpFileMenu.setDateTime(arrivalTime)
        smpFileMenu.setFont(fontFamily,fontSize,fontWeight)
        smpFileMenu.setColor(descColor)
    }
    Component.onDestruction: {
        smpFileMenu.closeDatePicker()
        smpFileMenu.closeFontPicker()
        smpFileMenu.closeColorPicker()
    }

    // this function is called from cpp to
    // change the windowtitle to Edit event
    // as the same window is used for editing also.
    function changeWindowTitle( name ){
        smpFileMenu.setTitle(name)
    }

    function setTimeDate( startTime, endTime )
    {
        arrivalTime = startTime
        departureTime = endTime
    }

    property string dateFormat: "ddd MMM dd/yyyy \n hh:mm:ss"

    // properties for the window

    property alias eventName : leEventName.text
    property alias eventType: typeComboBox.selectedUID
    property bool eventTypeVisible : true

    property date arrivalTime
    property date departureTime

    property alias markButtonSelected : pbMarkButtonID.checked
    property alias previewButtonSelected : pbPreviewButtonID.checked

    // property to get the current state of datetime option
    property int optionNone : 0
    property int optionTimeStart: 1
    property int optionTimeEnd: 2
    property int selectedOption: optionNone

    // properties for info event
    property alias infoDescriptionPresent: infoEventDescriptionCheckbox.checked
    property alias infoEventDescription: infoEventDescriptionTextEdit.text
    property string iconPath: ""
    property string audioPath:""
    property string videoPath:""
    property color descColor:"blue"
    property string fontWeight:"Light"
    property string fontFamily:"Arial"
    property int fontSize:40

    property bool fontPickerVisible: false;

    onFontPickerVisibleChanged: {
        if(fontPickerVisible === true){
            smpFileMenu.openFontPicker()
        }
        else{
            smpFileMenu.closeFontPicker()
        }
    }

    property bool colorPickerVisible:false;

    onColorPickerVisibleChanged: {
        if(colorPickerVisible === true){
            smpFileMenu.openColorPicker()
        }
        else{
            smpFileMenu.closeColorPicker()
        }
    }

    // properties for explosion event
    property alias explosionScale : scaleTextField.text

    // slot for info event
    signal attachFile()
    signal handleOkButtonClicked()
    signal handleCancelButtonClicked()
    signal handleMarkButtonClicked( bool value )
    signal handlePreviewButtonClicked( bool value )
    signal handleExplosionScaleChanged( double value )
    signal handleEventTypeChanged()
    signal attachAudioFile()
    signal attachVideoFile()

    onEventTypeChanged:
    {
        handleEventTypeChanged()
    }

    function timeDateChanged()
    {
        var obj = smpFileMenu.getDatePicker();
        var d = new Date(obj.year, obj.month, obj.dayOfMonth, obj.hours, obj.minutes, obj.seconds)
        if(selectedOption=== optionTimeStart){
            arrivalTime = d
            if(arrivalTime >= departureTime)
                departureTime = d
        }
        else if(selectedOption=== optionTimeEnd){
            departureTime = d
        }
    }

    function fontChanged(){
        var fontObj = smpFileMenu.getFontPicker();

        fontFamily = fontObj.fontFamily
        fontSize = fontObj.fontSize
        fontWeight =fontObj.fontWeight
    }

    function colorChanged(){
        var fontObj = smpFileMenu.getColorPicker();
        descColor = fontObj.currentColor
    }

    states: [
        State{
            name : "InfoEvent"
            when: typeComboBox.selectedUID == "Info"
            PropertyChanges{ target: infoEventGroupBox; visible: true }
            PropertyChanges{ target: explosionEventGroupBox; visible: false }
            //PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 250+infoEventGroupBox.height+50 : 250+infoEventGroupBox.height+10 }
            PropertyChanges{ target: pbMarkButtonID; visible: false}
            PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        },

        State{
            name : "explosionEvent"
            when: typeComboBox.selectedUID == "Explosion"
            PropertyChanges{ target: infoEventGroupBox; visible: false }
            PropertyChanges{ target: explosionEventGroupBox; visible: true }
            //PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 250+explosionEventGroupBox.height+50 : 250 + explosionEventGroupBox.height +10 }
            PropertyChanges{ target: pbMarkButtonID; visible: eventTypeVisible}
            PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        },

        State{
            name : "gunfireEvent"
            when: typeComboBox.selectedUID == "Gunfire"
            PropertyChanges{ target: infoEventGroupBox; visible: false }
            PropertyChanges{ target: explosionEventGroupBox; visible: false }
           // PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 240+50 : 240+10 }
            PropertyChanges{ target: pbMarkButtonID; visible: eventTypeVisible}
            PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        },

        State{
            name : "alarmEvent"
            when: typeComboBox.selectedUID == "Alarm"
            PropertyChanges{ target: infoEventGroupBox; visible: false }
            PropertyChanges{ target: explosionEventGroupBox; visible: false }
           // PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 240+50 : 240+10 }
            PropertyChanges{ target: pbMarkButtonID; visible: eventTypeVisible}
            PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        }
    ]

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent
        RowLayout{
            spacing: 5
            visible: eventTypeVisible
            z: 10
            VizLabel { text: "Type"  }
            VizComboBox{
                id: typeComboBox
                Layout.minimumWidth: 150
                Layout.fillWidth: true
                listModel:ListModel{
                    ListElement{name: "InfoEvent"; uID: "Info"}
                    ListElement{name: "Alarm"; uID: "Alarm"}
                    ListElement{name: "Explosion"; uID: "Explosion"}
                    ListElement{name: "Gunfire"; uID: "Gunfire"}
                }
                onSelectOption: setMinWidthHeight()
                value: "Select Event type"
            }
        }
        RowLayout{
            spacing: 5
            z: 5
            VizLabel { text: "Name" }
            VizTextField{
                id: leEventName
                Layout.minimumWidth: 150
                Layout.fillWidth: true
                placeholderText : "Name of the event"
                validator: rootStyle.nameValidator
            }
        }

        // shows the start and end time of the event
        VizGroupBox{
            id: timeGroupBox
            title: "Time"
            Layout.fillWidth: true
            Layout.minimumHeight: startTime.implicitHeight + margins
            Layout.minimumWidth: startTime.implicitWidth + 20

            GridLayout{
                id: startTime
                columns: 3
                z: 5
                VizLabel{
                    text: "Start Time: "
                }
                VizLabel{
                    id: arrivalTimeTextEdit
                    text: Qt.formatDateTime(arrivalTime, dateFormat)
                }
                VizButton{
                    id : pbArrivalCalenderButtonID

                    onClicked:{
                        if(selectedOption === optionTimeStart){
                            timeDateChanged()
                            selectedOption = optionNone
                        }
                        else{
                        	if(fontPickerVisible == true)
                                fontPickerVisible =false
                            if(colorPickerVisible == true)
                                colorPickerVisible =false
                            smpFileMenu.setDateTime(arrivalTime)
                            selectedOption = optionTimeStart
                            //datePicker.setDateTime(arrivalTime)
                        }
                    }
                    iconSource: rootStyle.getIconPath("calendar")
                    backGroundVisible: false
                }
                VizLabel{
                    text: " End Time: "
                }
                VizLabel{
                    id: departureTimeTextEdit
                    text: Qt.formatDateTime(departureTime, dateFormat)
                }
                VizButton{
                    id : pbDepartureCalenderButtonID

                    onClicked:{
                        if(selectedOption === optionTimeEnd){
                            timeDateChanged()
                            selectedOption = optionNone
                        }
                        else{
                            if(fontPickerVisible == true)
                                fontPickerVisible =false
                            if(colorPickerVisible == true)
                                colorPickerVisible =false
                            smpFileMenu.setDateTime(departureTime)
                            selectedOption = optionTimeEnd
                            //datePicker.setDateTime(departureTime)
                        }
                    }
                    iconSource: rootStyle.getIconPath("calendar")
                    backGroundVisible: false
                }
            }
        }

        // show the parameter for info event
        // shown only when selected in the combo box
        VizGroupBox{
            id: infoEventGroupBox
            visible: false
            title: "InfoEvent Parameters"
            Layout.fillWidth: true
            Layout.minimumHeight: infoCol.implicitHeight + margins
            Layout.minimumWidth: infoCol.implicitWidth + 20
            ColumnLayout{
                id: infoCol
                Layout.fillWidth: true
                spacing: 5
                RowLayout{
                    id: rowLayout
                    Layout.fillWidth: true
                    VizCheckBox{
                        id: infoEventDescriptionCheckbox
                        text: "Description"
                        onCheckedChanged:
                        {
                            if ( checked == false )
                            {
                                infoEventDescriptionTextEdit.text = ""
                            }
                        }
                    }
                    VizButton{
                        id: fontIcon
                        iconSource: rootStyle.getIconPath("font-size")
                        onClicked: {
                            if(fontPickerVisible === true){
                                fontChanged()
                                fontPickerVisible =false;
                            }
                            else{
                                if(colorPickerVisible ==true)
                                    colorPickerVisible = false;
                                if(selectedOption != optionNone)
                                    selectedOption = optionNone

                                smpFileMenu.setFont(fontFamily,fontSize,fontWeight)
                                fontPickerVisible=true;
                            }
                        }

                        backGroundVisible: false;
                    }

                    VizButton{
                        id: colorIcon
                        iconSource: rootStyle.getIconPath("font-color")
                        onClicked: {
                            if(colorPickerVisible === true){
                                colorChanged()
                                colorPickerVisible =false;
                            }
                            else{
                                if(fontPickerVisible ==true)
                                    fontPickerVisible = false;
                                if(selectedOption != optionNone)
                                    selectedOption = optionNone

                                smpFileMenu.setColor(descColor)
                                colorPickerVisible=true;
                            }
                        }
                        backGroundVisible: false
                    }
                }

                // font Dialog
                // Color Dialog
                VizTextField{
                    id: infoEventDescriptionTextEdit
                    Layout.fillWidth: true
                    Layout.minimumHeight: 60
                    visible: infoEventDescriptionCheckbox.checked
                    placeholderText: "Enter Description..."
                }
                RowLayout{
                    id: imageRow
                    anchors.top: (infoEventDescriptionCheckbox.checked)?infoEventDescriptionTextEdit.bottom:rowLayout.bottom

                    VizLabel{
                        id: imageLabel
                        text: "Image"
                    }
                    VizTextField{
                        id: imageField
                        text: iconPath
                    }

                    VizButton {
                        id: addImage
                        onClicked: {
                            attachFile()
                        }
                        //backGroundVisible: false
                        text: "Browse"
                    }
                }

                RowLayout{
                    id: audioRow
                    VizLabel{
                        id: audioLabel
                        text: "Audio"
                    }
                    VizTextField{
                        id: audioField
                        text: audioPath
                    }

                    VizButton {
                        id: addAudio
                        onClicked: attachAudioFile()
                        //backGroundVisible: false
                        text: "Browse"
                    }

                }
                RowLayout{
                    id: videoRow
                    VizLabel{
                        id: videoLabel
                        text: "Video"
                    }
                    VizTextField{
                        id: videoField
                        text: videoPath
                    }

                    VizButton {
                        id: addVideo
                        onClicked: attachVideoFile()
                        //backGroundVisible: false
                        text: "Browse"
                    }
                }
            }
        }

        // show the parameter for explosion event
        // shown only when selected in the combo box
        VizGroupBox{
            id: explosionEventGroupBox
            visible: false
            title: "Explosion Event Parameters"
            Layout.fillWidth: true
            Layout.minimumHeight: explosionEventRowLayout.implicitHeight + margins
            Layout.minimumWidth: explosionEventRowLayout.implicitWidth + 20
            RowLayout{
                id:explosionEventRowLayout
                spacing: 5
                VizLabel { text : "Scale" }
                VizSpinBox {
                    id : scaleTextField
                    value : 4.0
                    minimumValue: 4
                    maximumValue: 1000
                    validator: IntValidator{
                        top: scaleTextField.maximumValue
                        bottom: scaleTextField.minimumValue
                    }
                    onValueChanged: handleExplosionScaleChanged( value )
                }
            }
        }

        RowLayout{
            spacing: 5
            anchors.horizontalCenter: parent.horizontalCenter
            VizButton{
                id : pbMarkButtonID
                text : "Mark"
                checkable: true
                onCheckedChanged: handleMarkButtonClicked(checked)
            }

            VizButton{
                text: "Ok"
                onClicked: handleOkButtonClicked()
            }
            VizButton{
                text: "Cancel"
                onClicked: {
                    handleCancelButtonClicked()
                    //closePopup()
                }
            }
            VizButton
            {
                id : pbPreviewButtonID
                text : "Preview"
                checkable: true
                onCheckedChanged: handlePreviewButtonClicked(checked)
            }
        }
    }

    onSelectedOptionChanged: {
        if(selectedOption === optionTimeEnd || selectedOption === optionTimeStart){
            openDatePicker()
        }
        else{
            closeDatePicker()
        }
    }

    function openDatePicker(){
        smpFileMenu.openDatePicker()
    }

    function closeDatePicker(){
        smpFileMenu.closeDatePicker()
    }
}
