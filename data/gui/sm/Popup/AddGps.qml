import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id:addGps
    objectName: "AddGps"
    title: "Add GPS log"

    height: mainLayout.implicitHeight+2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias sourceFile:gpsDataFile.text

    property alias isVisible:addGps.visible

    //Signal defined for buttons
    signal browseButtonClicked()
    signal okButtonClicked()
    signal cancelButtonClicked()

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id: row
            VizLabel{
                text: "Path"
            }
            VizTextField{
                id:gpsDataFile
                Layout.minimumWidth: 300
                placeholderText: " Browse source data file... "
                Layout.fillWidth: true
            }
            VizButton{
                id:browse
                text: "Browse"
                onClicked:browseButtonClicked()
              }

        }
        RowLayout{
            id: lastrow
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: ok
                text: "   Ok   "
                onClicked: okButtonClicked()
            }
            VizButton{
                id:cancel
                text: "Cancel"
                onClicked: closePopup()
            }
        }
    }
}


