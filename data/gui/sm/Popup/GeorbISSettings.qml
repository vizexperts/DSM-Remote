import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup {
    id: georbISSettings

    objectName: "georbISSettingsMenu"
    title: "GeorbIS Server Settings"

    height: mainLayout.implicitHeight + 15
    width: mainLayout.implicitWidth+ 10

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal saveGeorbISServerParameters()
    signal deleteGeorbISServer(string value)
    signal comboBoxClicked(string value)
    signal testGeorbISServerConnection()

    property alias value : georbISServerBox.value
    property alias georbISServerName: georbISServerNameLineEdit.text
    property alias host:georbISServerHostLineEdit.text
    property bool testResultVisible : false
    property alias status : statusButton.text

    Component.onCompleted: {

        if(!running_from_qml){
           GeorbISServersSettingsGUI.setActive(true)
        }

    }

    Component.onDestruction: {
        if(!running_from_qml){
           GeorbISServersSettingsGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{

            id:databseNameRow
            spacing: 5
            z: 150

            VizLabel{
                text: "Choose "
            }

            VizComboBox{
                id: georbISServerBox
                Layout.minimumWidth: 250
                Layout.fillWidth: true
                listModel: typeof(georbISServerList)== "undefined" ? "undefined" : georbISServerList
                value: georbISServerBox.get(selectedUID).name

                onSelectOption: {
                    comboBoxClicked(value)
                    testResultVisible = false
                }

            }

            VizButton{
                id:deleteButton
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 35
                text: " Delete "
                onClicked:
                {
                    deleteGeorbISServer(value)
                    testResultVisible = false
                }
            }
        }



        VizGroupBox{
            id:georbISServerParameterGroupBox
            title: "New"
            visible: georbISServerBox.value != "Select GeorbIS Server Instance"
            clip: true
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true

            GridLayout{
                Layout.fillWidth: true
                Layout.fillHeight: true
                columns: 2

                VizLabel{
                    text: "Server Name"
                }
                VizTextField{
                    id: georbISServerNameLineEdit
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    Keys.onTabPressed: georbISServerHostLineEdit.giveFocus()
                }
                VizLabel{
                    text: "Enter Url"
                }
                VizTextField{
                    id: georbISServerHostLineEdit
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    Keys.onTabPressed: saveButton.giveFocus()
                }

                VizButton{
                    id: saveButton
                    anchors.left: parent.left
                    anchors.leftMargin: 50
                    Layout.minimumWidth: 100
                    text:" Save "
                    onClicked: {
						testResultVisible = false
                        saveGeorbISServerParameters()
                    }
                }

                VizButton{
                    id: testButton
                    anchors.right: parent.right
                    anchors.rightMargin: 50
                    Layout.minimumWidth: 100
                    text:" Test "
                    onClicked: {
                        testResultVisible = false
                        testGeorbISServerConnection()

                    }
                }
            }
        }

        VizLabel{
            id: statusButton
            Layout.alignment: Qt.AlignHCenter
            text: status
            visible: testResultVisible
        }

    }
}
