import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: createLayerMenuId
    objectName: "CreateLayerMenuObject"

    title: "Creation of Custom Layer"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.implicitWidth+ 10
    minimumHeight: mainLayout.implicitHeight + 10

    // signals for UI
    signal qmlHandleAddButtonClicked()
    signal qmlHandleUpdateButtonClicked(int currentRow)
    signal qmlHandleRemoveButtonClicked(int currentRow)
    signal qmlHandleOkButtonClicked()
    signal qmlHandleCancelButtonClicked()

    //property alias
    property alias cbLayerTemplate: templateId.value;
    property alias leLayerName : layerId.text;
    property alias leAttrName: attrNameId.text
    property alias cbAttrType : attrTypeId.value ;
    property alias leAttrWidth : attrWidthId.text
    property alias leAttrPrecision : attrPrecisionId.text

    property bool popupClosed : true

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ListModel{
        id: listTemplateId
        ListElement{name:"Point"; uID : "0"}
        ListElement{name:"Line"; uID : "1"}
        ListElement{name:"Polygon"; uID : "2"}
        ListElement{name:"Model"; uID : "3"}
    }

    ListModel{
        id: listAttrTypeId
        ListElement{name:"Integer"; uID : "0"}
        ListElement{name:"Decimal"; uID : "1"}
        ListElement{name:"Text"; uID : "2"}
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        GridLayout{
            columns: 2
            z : 10

            VizLabel { text: "Template" }
            VizComboBox {
                id: templateId
                listModel: listTemplateId
                value: listTemplateId.get(selectedUID).name
                Layout.fillWidth: true
                z : 10
            }
            VizLabel { text: "Layer Name"  }
            VizTextField {
                id: layerId
                Layout.minimumWidth: 250
                placeholderText: "enter layer name"
                validator: rootStyle.nameValidator
                Layout.fillWidth: true
            }
        }

        VizGroupBox {
            id: attributeListGroupBoxId
            Layout.fillWidth: true
            Layout.fillHeight: true
            title: "Attributes"

            ColumnLayout{
                id: layout
                anchors.margins: 5
                GridLayout {
                    columns: 4
                    z: 10

                    VizLabel{ text: "Name" }
                    VizTextField{
                        id: attrNameId
                        placeholderText: "Enter attribute name"
                        validator: rootStyle.nameValidator
                        Layout.fillWidth: true
                        Layout.minimumWidth: 250
                    }
                    VizLabel { text: "Type" }
                    VizComboBox {
                        id: attrTypeId
                        listModel: listAttrTypeId
                        value: listAttrTypeId.get(selectedUID).name
                        Layout.fillWidth: true
                        z:10
                    }
                    VizLabel{
                        visible : attrWidthId.visible
                        text: "Width"
                    }
                    VizTextField{
                        id: attrWidthId
                        validator:IntValidator {bottom: 1; top: 9999;}
                        Layout.fillWidth: true
                    }

                    VizLabel {
                        visible : attrPrecisionId.visible
                        text: "Precision"
                    }
                    VizTextField {
                        id: attrPrecisionId;
                        validator: IntValidator {bottom: 1; top: 99;}
                        Layout.fillWidth: true
                    }
                }
                RowLayout {
                    id: attrListButtonId
                    Layout.alignment: Qt.AlignCenter
                    VizButton {
                        id: attrListAddButtonId
                        text: "   Add   "
                        onClicked: qmlHandleAddButtonClicked()
                    }
                    VizButton {
                        id: attrListUpdateButtonId;
                        text: " Update ";
                        enabled: (listViewId.rowCount !== 0)
                        onClicked: qmlHandleUpdateButtonClicked(listViewId.currentRow);
                    }
                    VizButton {
                        id: attrListRemoveButtonId
                        text: " Remove "
                        enabled: (listViewId.rowCount !== 0)
                        onClicked: qmlHandleRemoveButtonClicked(listViewId.currentRow);
                    }
                }

                // for displaying of list populated data

                VizTableView {
                    id: listViewId
                    model: typeof( layerModel) !== "undefined" ? layerModel : "undefined"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    focus: true
                    clip: true
                    onCurrentRowChanged: {
                        if(currentRow >= 0 && listViewId.rowCount > 0){
                            leAttrName  = layerModel[currentRow].name;
                            cbAttrType  = layerModel[currentRow].type;
                            leAttrWidth = layerModel[currentRow].width;
                            leAttrPrecision = layerModel[currentRow].precision;
                        }
                    }

                    TableViewColumn{
                        role: "name";
                        title: "Name";
                        elideMode: Text.ElideMiddle
                    }
                    TableViewColumn{
                        role: "type";
                        title: "Type";
                        elideMode: Text.ElideMiddle
                        horizontalAlignment: Text.AlignHCenter
                    }
                    TableViewColumn{
                        role: "width";
                        title: "Width";
                        elideMode: Text.ElideMiddle
                    }
                    TableViewColumn{
                        role: "precision";
                        title: "Precision";
                        elideMode: Text.ElideMiddle
                    }
                }
            }
        }

        RowLayout {
            id: mainButtonId
            Layout.alignment: Qt.AlignRight
            VizButton {
                id: okButtonId
                text: "    Ok    ";
                onClicked: {
                    qmlHandleOkButtonClicked();
                    if(popupClosed)
                        closePopup()
                }
            }
            VizButton {
                id: cancelButtonId
                text: "Cancel"
                onClicked: {
                    qmlHandleCancelButtonClicked();
                    closePopup()
                }
            }
        }
    }

    // setting the default state
    state : "typeInteger"

    states: [
        State {
            name: "typeFloat";
            when: attrTypeId.value == "Decimal"
            PropertyChanges { target: attrPrecisionId; text: "1" }
            PropertyChanges { target: attrPrecisionId; visible: true }

            PropertyChanges { target: attrWidthId; text: "1" }
            PropertyChanges { target: attrWidthId; visible: false }
        },
        State {
            name: "typeInteger";
            when: attrTypeId.value == "Integer"
            PropertyChanges { target: attrPrecisionId; text: "1" }
            PropertyChanges { target: attrPrecisionId; visible: false }

            PropertyChanges { target: attrWidthId; text: "1" }
            PropertyChanges { target: attrWidthId; visible: false }
        },
        State {
            name: "typeText";
            when: attrTypeId.value == "Text"
            PropertyChanges { target: attrPrecisionId; text: "1" }
            PropertyChanges { target: attrPrecisionId; visible: false }

            PropertyChanges { target: attrWidthId; text: "1" }
            PropertyChanges { target: attrWidthId; visible: true }
        }
    ]
}
