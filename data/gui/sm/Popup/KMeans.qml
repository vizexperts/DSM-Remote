import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "KMeanGUI"
    title: "k-Means Clustering"

    Component.onCompleted: {
        if(!running_from_qml){
            KMeanGUI.setActive(true)
        }
    }

    onClosed: {
        if(!running_from_qml){
            KMeanGUI.setActive(false)
        }
    }
    Component.onDestruction:
    {
        if(!running_from_qml){
            KMeanGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal viewImage(string imagePath)
    signal calculate()
    signal browseInputImage()

    signal cancelButtonClicked()
    signal browseOutputImage()


    property alias inputImagePath : inputImage.text
    property alias outputImagePath: outputImage.text
    property alias okButtonLabel : okButton.text
    property alias cancelButtonLabel : cancelButton.text
    property alias okButtonEnabled: okButton.enabled
    property alias cancelButtonEnabled: cancelButton.enabled

    property alias inputButtonEnabled: inputOpenID.enabled
    property alias outputButtonEnabled: outputOpenID.enabled

    property alias classes: classesfield.text
    property alias iterations: iterationsfield.text
    property bool calculating: false



    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        RowLayout{
            id:imageRow
            spacing: 5

            VizLabel{
                text: "Input Image"
                textAlignment: Qt.AlignLeft
            }

            VizTextField{
                id: inputImage
                enabled: !calculating
                Layout.minimumWidth: 250
                Layout.fillWidth: true
                text: ""
                validator:rootStyle.nameValidator
                placeholderText:"Browse Input Image file..."
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browseInputImage()
                }
            }
            VizButton {
                id : inputOpenID
                text : "View"
                onClicked:viewImage(inputImagePath)
            }
        }
        RowLayout{
            id:opRow
            spacing: 5

            VizLabel{
                text: "output Name"
                textAlignment:Qt.AlignLeft
            }

            VizTextField{
                id: outputImage
                enabled: !calculating
                Layout.minimumWidth: 250
                Layout.fillWidth: true
                text: ""
                validator:rootStyle.nameValidator
                placeholderText:"Enter output Image fileName..."
            }
            VizButton{
                text:"Browse"
                enabled: !calculating
                onClicked: {
                    browseOutputImage()
                }
            }
            VizButton {
                id : outputOpenID
                text : "View"
                onClicked:viewImage(outputImagePath)
            }
        }


        RowLayout{
            id:classes
            VizLabel{
                id: classeslabel
                text: "Classes"
                textAlignment:  Qt.AlignLeft
            }
            VizTextField{
                id: classesfield
                enabled: !calculating
                Layout.fillWidth: true
                text: ""
                placeholderText: "Enter Number of Classes"
                validator:  IntValidator {bottom: 1; top: 10;}
            }
        }
        RowLayout{
            id:iterations
            VizLabel{
                id: iterationslabel
                text: "Iterations"
                width: 100
                textAlignment:Qt.AlignLeft
            }
            VizTextField{
                id: iterationsfield
                enabled: !calculating
                Layout.fillWidth: true
                text: ""
                placeholderText: "Enter Number of Iterations"
                validator:  IntValidator {bottom: 1; top: 30;}
            }
        }

        RowLayout{
            id:commandButtonRect
            spacing: 15



            AnimatedImage{
                id:progressBarId
                source: rootStyle.getBusyIcon()
                visible: calculating
            }

            Rectangle{
                Layout.fillWidth: true
            }


            VizButton{
                id:okButton
                text: "  Calculate  "
                onClicked: calculate()
            }

            VizButton{
                id:cancelButton
                text: "  Cancel  "
                onClicked: cancelButtonClicked()
            }
        }
    }



}

