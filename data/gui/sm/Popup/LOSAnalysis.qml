import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{

    Component.onCompleted: {
        if(!running_from_qml){
            LOSGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            LOSGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 200
    minimumHeight: mainLayout.Layout.minimumHeight +10

    objectName: "LOSAnalysis"
    title: "Line Of Sight Analysis"

    // ViewPoint and LookAt position properties
    property alias viewPointLatitude: viewPointLatitude.text
    property alias viewPointLongitude: viewPointLongitude.text
    property alias viewPointAltitude: viewPointAltitude.text
    property alias lookAtLatitude: lookAtLatitude.text
    property alias lookAtLongitude: lookAtLongitude.text
    property alias lookAtAltitude: lookAtAltitude.text
    property alias heightOffset: heightValue.text
    property alias lookAtHeightOffset: lookAtHeightValue.text
    property alias aerialDistance: distance.text
    property alias isMarkViewSelected: markViewPoint.checked
    property alias isMarkLookAtSelected: markLookAt.checked

    property alias losType:losId.value
    // signals generated by buttons
    signal markLookAtPosition(bool value)
    signal markViewPointPosition(bool value)
    signal changeLosType( int selectedLOSType )
    signal circularSectorAngle(double value)
    signal circularFill(bool value)
    signal transperancyValue(double value)
    signal viewerHeight(double value)
    signal lookAtHeight(double value)
    signal viewerLat(double value)
    signal viewerLong(double value)
    signal lookAtLat(double value)
    signal lookAtLong(double value)
    signal numberOfSpokes(int value)


    ListModel{
        id: losTypeId
        ListElement{name:"Point to point"; uID : "0"}
        ListElement{name:"Circular"; uID : "1"}
    }
    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        Layout.minimumWidth: 400

        GridLayout{
            id: lostype
            Layout.fillWidth: true
            z: 150

            VizLabel{
                id: losType
                text:"Analysis type   "
                textAlignment: Qt.AlignLeft

            }
            VizComboBox {
                id: losId
                listModel: losTypeId
                value: losTypeId.get(selectedUID).name
                Layout.fillWidth: true
                onSelectedUIDChanged:{
                    changeLosType(selectedUID)
                }
            }

        }
        GridLayout{
            id:inputParameterRect

            //anchors.fill: parent
            anchors.margins: 5
            columns: 1

            RowLayout{
                VizLabel{
                    text:"Viewers point   "
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: viewPointLatitude
                    text: ""
                    //Layout.minimumWidth: 85*rootStyle.uiScale
                    placeholderText: "Latitude"
                    Layout.fillWidth: true
                    validator: rootStyle.latitudeValidator
                    onTextChange: viewerLat(text)
                }
                VizTextField{
                    id: viewPointLongitude
                    text: ""
                    //Layout.minimumWidth: 85*rootStyle.uiScale
                    placeholderText: "Longitude"
                    validator: rootStyle.longitudeValidator
                    Layout.fillWidth: true
                    onTextChange: viewerLong(text)
                }
                VizTextField{
                    id: viewPointAltitude
                    //Layout.minimumWidth: 85*rootStyle.uiScale
                    text: ""
                    placeholderText: "Altitude"
                    Layout.fillWidth: true
                    visible:false
                }
                VizButton{
                    id: markViewPoint
                    objectName: "markViewPoint"
                    height: 30
                    text:"   Mark   "
                    checkable: true
                    onCheckedChanged: {
                        if(checked){
                            markLookAt.checked =false
                        }

                        markViewPointPosition(checked)
                    }
                    Layout.alignment: Qt.AlignCenter
                }
            }

            RowLayout{

                VizLabel{
                    text:"Look at point    "
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: lookAtLatitude
                    text: ""
                    //Layout.minimumWidth: 85*rootStyle.uiScale
                    placeholderText: "Latitude"
                    validator: rootStyle.latitudeValidator
                    Layout.fillWidth: true
                    onTextChange: lookAtLat(text)
                }
                VizTextField{
                    id: lookAtLongitude
                    text: ""
                    //Layout.minimumWidth: 85*rootStyle.uiScale
                    placeholderText: "Longitude"
                    Layout.fillWidth: true
                    validator: rootStyle.longitudeValidator
                    onTextChange: lookAtLong(text)
                }
                VizTextField{
                    id: lookAtAltitude
                    //Layout.minimumWidth: 85*rootStyle.uiScale
                    text: ""
                    placeholderText: "Altitude"
                    Layout.fillWidth: true
                    visible:false
                }
                VizButton{
                    id: markLookAt
                    objectName: "markLookAt"
                    text:"   Mark   "
                    height: 30
                    checkable: true
                    onCheckedChanged: {
                        if(checked){
                            markViewPoint.checked =false
                        }
                        markLookAtPosition(checked)
                    }
                    Layout.alignment: Qt.AlignCenter
                }
            }

            RowLayout{

                VizLabel{
                    id: heightLabel
                    text:"Viewer's height "
                    textAlignment: Qt.AlignLeft
                }
                VizTextField{
                    id: heightValue
                    text: "1.67"
                    placeholderText: "Viewer's height in meters"
                    Layout.fillWidth: true
                    validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation; }
                    onTextChange: viewerHeight(text)
                }
                VizLabel{
                    id: metersLableViewer
                    text:"meters"
                    textAlignment: Qt.AlignLeft
                }

                VizLabel{
                    id: sectorAngle
                    text:"       Angle "
                    textAlignment: Qt.AlignLeft
                    visible :  losId.selectedUID === "1"
                }
                VizTextField{
                    id: sectorAngleValue
                    text: "120"
                    placeholderText: "Sector Angle"
                    Layout.fillWidth: true
                    validator:  DoubleValidator {bottom: 0; top: 360; notation: DoubleValidator.StandardNotation;}
                    onTextChange: circularSectorAngle(text)
                    visible :  losId.selectedUID === "1"
                }

                VizLabel{
                    id: lookAtHeightLabel
                    text:"    Look at's height "
                    textAlignment: Qt.AlignLeft
                    visible: losId.selectedUID !== "1"
                }
                VizTextField{
                    id: lookAtHeightValue
                    text: "0"
                    visible: losId.selectedUID !== "1"
                    placeholderText: "Viewer's height in meters"
                    Layout.fillWidth: true
                    validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation; }
                    onTextChange: lookAtHeight(text)
                }

                VizLabel{
                    id: metersLableLookat
                    text:"meters"
                    textAlignment: Qt.AlignLeft
                    visible :  losId.selectedUID !== "1"
                }
            }

        }

        RowLayout{
            visible: losId.selectedUID === "1"

            VizLabel{
                id: fillTransparencyLable
                text: "Transparency   "
                textAlignment: Qt.AlignLeft
            }

            VizSlider {
                id: fillTransparency
                minimumValue: 0
                maximumValue: 1
                width: 100
                labelVisible: false
                value: 0
                onValueChanged: transperancyValue(value)
            }

            Rectangle{
                Layout.fillWidth: true
            }


            VizLabel{
                id: stepLable
                text: "Step Size  "
            }

            VizSlider {
                id: stepSlider
                minimumValue: 1
                maximumValue: 360
                width: 100
                stepSize: 1
                value: 32
                labelVisible: false
                onValueChanged: numberOfSpokes(value)
            }

            Rectangle{
                Layout.fillWidth: true
            }

            VizLabel{
                id: circularFillLable
                text: "Fill"
                textAlignment: Qt.AlignLeft
            }

            VizCheckBox{
                id: circularFillLOS
                checked: true
                onClicked: circularFill(checked)
            }

            Rectangle{
                Layout.fillWidth: true
            }


        }


        RowLayout{

            VizLabel{
                text:"Aerial Distance "
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: distance
                text: "0"
                placeholderText: "Distance"
                validator:  DoubleValidator { bottom : 0; notation: DoubleValidator.StandardNotation; }
            }

        }
    }
}
