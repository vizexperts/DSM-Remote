import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"
import ".."

Popup {
    id: uacSettingsMenu

    objectName: "uacSettingsMenu"
    title: "User Account Control"

    height: mainLayout.implicitHeight + 15
    width: mainLayout.implicitWidth+ 10

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal okButtonClicked()
    signal cancelButtonClicked()

    signal addToActiveUsers(int index)
    signal removeFromActiveUsers(int index)
    signal markRowDirty(int index)

    signal analysisRoleChanged(int index, bool value)
    signal planningRoleChanged(int index, bool value)
    signal addDataRoleChanged(int index, bool value)
    signal adminUserRoleChanged(int index, bool value)

//    signal browseRasterData()
//    signal browseElevationData()

    Component.onCompleted: {
        if(!running_from_qml)
            LoginGUI.connectUacPopup()
    }

    Component.onDestruction: {
        if(!running_from_qml)
            LoginGUI.disconnectUacPopup()
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    onCancelButtonClicked: closePopup()

    property bool reloadProjectsList: false

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5
        VizCheckBox{
            id: enableUserAuthCheckbox
            text: "Enable User Authentication"
            checked: true
        }
        VizTabFrame{
            id: uacTabFrame
            Layout.fillHeight: true
            VizTab{
                id:  usersTab
                contentMargin: 10
                title: "Users"
                ColumnLayout{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    RowLayout{
                        ColumnLayout{
                            VizLabel{
                                text: "All Users"
                            }
                            Rectangle{
                                color: "transparent"
                                border.width: 2
                                border.color: rootStyle.colorWhenSelected
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                Layout.minimumWidth: 100*rootStyle.uiScale
                                Layout.minimumHeight: 150*rootStyle.uiScale
                                ListView{
                                    id: allUsersListView
                                    currentIndex: -1
                                    anchors.fill: parent
                                    model: (typeof(nonActiveUsersList) != "undefined") ? nonActiveUsersList : "undefined"
                                    spacing: 4
                                    VizScrollBar{ target: allUsersListView }
                                    clip: true
                                    Keys.onUpPressed: if(currentIndex > 0) currentIndex--
                                    Keys.onDownPressed: if(currentIndex < count) currentIndex++
                                    focus: true
                                    delegate: VizLabel{
                                        id: label
                                        text: modelData
                                        width: allUsersListView.width
                                        Behavior on color {
                                            ColorAnimation { duration: 200 }
                                        }
                                        Behavior on textColor {
                                            ColorAnimation { duration: 200 }
                                        }

                                        textColor: (allUsersListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                                        color: (allUsersListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                                        MouseArea{
                                            anchors.fill: parent
                                            onClicked: allUsersListView.currentIndex = index
                                            onDoubleClicked: addToActiveUsers(allUsersListView.currentIndex)
                                        }
                                    }
                                }
                            }
                        }
                        ColumnLayout{
//                            Layout.alignment: Layout.verticalCenter
                            VizButton{
//                                iconSource: rootSty.
                                Layout.fillWidth: true
                                text: "Add"
                                onClicked: addToActiveUsers(allUsersListView.currentIndex)
                            }
                            VizButton{
                                Layout.fillWidth: true
                                Layout.minimumWidth: 100
                                text: "Delete"
                                onClicked: removeFromActiveUsers(activeUsersListView.currentIndex)
                            }
                        }
                        ColumnLayout{
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            VizLabel{
                                text: "Active Users"
                            }
                            Rectangle{
                                color: "transparent"
                                border.width: 2
                                border.color: rootStyle.colorWhenSelected
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                Layout.minimumWidth: 100*rootStyle.uiScale
                                Layout.minimumHeight: 150*rootStyle.uiScale
                                ListView{
                                    id: activeUsersListView
                                    currentIndex: -1
                                    anchors.fill: parent
                                    model: (typeof(permissionsListModel) != "undefined") ? permissionsListModel : "undefined"
                                    spacing: 4
                                    VizScrollBar{ target: activeUsersListView }
                                    clip: true
                                    Keys.onUpPressed: if(currentIndex > 0) currentIndex--
                                    Keys.onDownPressed: if(currentIndex < count) currentIndex++
                                    focus: true
                                    delegate: VizLabel{
                                        text: username
                                        width: activeUsersListView.width
                                        Behavior on color {
                                            ColorAnimation { duration: 200 }
                                        }
                                        Behavior on textColor {
                                            ColorAnimation { duration: 200 }
                                        }

                                        textColor: (activeUsersListView.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
                                        color: (activeUsersListView.currentIndex == index ) ? rootStyle.selectionColor : "transparent"
                                        MouseArea{
                                            anchors.fill: parent
                                            onClicked: activeUsersListView.currentIndex = index
                                            onDoubleClicked: removeFromActiveUsers(activeUsersListView.currentIndex)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            VizTab{
                id: permissionsTab
                title: "Permissions"
                VizTableView{
                    id: permissionsTableView
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.minimumHeight: 300
                    Layout.minimumWidth: 300*rootStyle.uiScale
                    model:(typeof(permissionsListModel) !== "undefined") ? permissionsListModel : "undefined"
//                    function
                    TableViewColumn{
                        role: "username"
                        title: "User"
                    }
                    TableViewColumn{
                        role: "analysis"
                        title: "Analysis"
                        delegate: VizCheckBox{
                            Layout.alignment: Qt.AlignHCenter
                            checked: styleData.value
                            onCheckedChanged: {
                                analysisRoleChanged(styleData.row, checked)
                            }
                        }
                    }
                    TableViewColumn{
                        role: "add_data"
                        title: "Add Data"
                        delegate: VizCheckBox{
                            checked: styleData.value
                            onCheckedChanged: {
                                addDataRoleChanged(styleData.row, checked)
                            }
                        }
                    }
                    TableViewColumn{
                        role: "planning"
                        title: "Planning"
                        delegate: VizCheckBox{
                            checked: styleData.value
                            onCheckedChanged: {
                                planningRoleChanged(styleData.row, checked)
                            }
                        }
                    }
                    TableViewColumn{
                        role: "adminUser"
                        title: "Admin"
                        delegate: VizCheckBox{
                            checked: styleData.value
                            onCheckedChanged: {
                                adminUserRoleChanged(styleData.row, checked)
                            }
                        }
                    }
                }
            }
        }
        OkCancelButtonRow{
            visible: permissionsTab.isCurrent
            anchors.horizontalCenter: parent.horizontalCenter
            onOkButtonPressed: {
                okButtonClicked()
            }
            onCancelButtonPressed: cancelButtonClicked()
        }

    }
}
