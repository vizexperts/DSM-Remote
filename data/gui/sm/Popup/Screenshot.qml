import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: screenshotMenuID
    objectName: "screenshotMenuObject"

    title: "Screenshot"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal qmlHandleBrowseButtonClicked();
    signal qmlHandleCaptureButtonClicked();

    property alias fileName: screenshotTextId.text

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            id: screenshotrowId
            height: Math.max(screenshotBrowseId.height, screenshotTextId.height)
            VizLabel{
                id: screenshotLabelId
                text: "Save as"
            }
            VizTextField{
                id: screenshotTextId;
                placeholderText: "Browse an output file..."
                text: ""
                readOnly: true
                Layout.fillWidth: true
                Layout.minimumWidth: 250
            }
            VizButton{
                id: screenshotBrowseId
                text: "Browse";
                onClicked: qmlHandleBrowseButtonClicked();
            }
        }

        VizButton{
            id: screenshotOkId
            text: "Take Screenshot"
            onClicked: qmlHandleCaptureButtonClicked();
            Layout.alignment: Qt.AlignCenter
        }
    }
}
