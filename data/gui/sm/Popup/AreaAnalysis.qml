import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: areaAnalysis
    objectName: "areaAnalysis"
    title: "Calculate Area"

    Component.onCompleted: {
        if(!running_from_qml){
            MeasureAreaGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            MeasureAreaGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal changeUnit(string unit)
    signal mark(bool state)
    signal selectArea(bool state)
    signal start()
    signal stop()

    property alias projected: calculationTypeCheckbox.checked
    property bool calculating: false
    property alias value: resultRect.text

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            Layout.fillWidth: true
            z:1

            VizLabel{ text: "Area" }

            VizTextField{
                id: resultRect
                readOnly: true
                Layout.fillWidth: true
                text: ""
                placeholderText: ""
            }

            ListModel{
                id: unitsModel
                ListElement{name: "sq m"; uID: "m"}
                ListElement{name: "sq km"; uID: "km"}
                ListElement{name: "sq miles"; uID: "miles"}
            }

            VizComboBox{
                id: unitComboBox
                listModel: unitsModel
                Layout.minimumWidth: 100
                value: "sq m"
                onSelectedUIDChanged: {
                    changeUnit(selectedUID)
                }
            }
        }
        RowLayout{
            VizLabel{ text: "Area on Ground" }

            VizCheckBox{
                id: calculationTypeCheckbox
                text: ""
                enabled: !calculating
            }
            AnimatedImage{
                source: rootStyle.getBusyBar()
                visible: calculating
                Layout.fillWidth: true
            }
        }
        RowLayout{
            Layout.alignment: Qt.AlignRight
            spacing: 5
            VizButton{
                id: markButton
                text: "   Mark   "
                enabled: !calculating
                onClicked: {
                    checked = !checked
                    if(selectButton.checked){
                        selectButton.checked = false
                        selectArea(false)
                    }
                    mark(checked)
                }
            }
            VizButton{
                id: selectButton
                text: "Select Area"
                enabled: !calculating
                onClicked: {
                    checked = !checked
                    if(markButton.checked){
                        markButton.checked = false
                        mark(false)
                    }
                    selectArea(checked)
                }
            }
            VizButton{
                id: startButton
                text: calculating ? "   Stop   " : "  Start  "
                onClicked: {
                    if(calculating)
                        stop()
                    else
                        start()
                }
            }
        }
    }
}
