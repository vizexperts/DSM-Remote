import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: computeServicesPopup
    objectName: "ComputeServicesPopup"
    title: "Analysis Browser"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.implicitWidth+ 10
    minimumHeight: mainLayout.implicitHeight + 10

    Component.onCompleted: {
        if(!running_from_qml)
        {
            ComputeServicesGUI.popupLoaded()
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            Rectangle{
                Layout.minimumWidth: 250
                Layout.minimumHeight: 400
                Layout.fillWidth: true
                Layout.fillHeight: true
                color: "transparent"
                border.color: rootStyle.colorWhenSelected
                VizTreeView{
                    id: computeServicesListView
                    anchors.fill: parent
                    anchors.topMargin: 10
                    model:(typeof(analysisListModel) !== "undefined") ? analysisListModel : "undefined"
//                    delegate: VizLabel{
//                        text: modelData
//                    }
                    VizTreeViewColumn{
                        role: "name"
                        delegate:VizLabel{
                            text:styleData.value
                            textColor:rootStyle.textColor
                        }
                    }
                    VizScrollBar{
                        target: computeServicesListView
                    }
                }
            }
        }
    }
}
