import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id:exportGeoLayer
    objectName: "ExportGeoWindow"
    title: "Export Layer To GeorbIS Server"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //property associated with parameters
    property string initialLayerName

    //Signal defined for buttons
    signal exportGeoButtonClicked()
    signal cancelButtonClicked()
    signal publishingServerChange(string serverName);
    //states for button
    property alias isexportEnabled : exportButton.enabled
    property alias isCancelEnabled : cancel.enabled

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id:mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id:layerNameRow
            spacing: 5
            z: 60

            VizLabel{
                text: "Choose Server "
                width: 100
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                    id: publishingServerBoxId
                    Layout.minimumWidth: 200
                    Layout.fillWidth: true
                    listModel: typeof(publishingServerList)== "undefined" ? "undefined" : publishingServerList
                    value: publishingServerList.get(selectedUID).name
                    onSelectOption: {
                        publishingServerChange(value)
                    }

            }
        }
        RowLayout{
            id: lastrow
            Layout.alignment: Qt.AlignRight
            VizButton{
                id: exportButton
                text: " Export "
                onClicked: exportGeoButtonClicked()
            }
            VizButton{
                id:cancel
                text: " Cancel "
                onClicked: closePopup()
            }
        }
    }
}
