import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: distanceAnalysis
    objectName: "distanceAnalysis"

    title: "Calculate Distance"

    Component.onCompleted: {
        if(!running_from_qml){
            MeasureDistanceGUI.setActive(true)
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            MeasureDistanceGUI.setActive(false)
        }
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal changeUnit(string unit)
    signal mark(bool state)
    signal selectLine(bool state)
    signal start()
    signal stop()

    property alias projected: calculationTypeCheckbox.checked
    property bool calculating: false
    property alias value: resultRect.text

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            Layout.fillWidth: true
            z:1

            VizLabel{
                text: "Distance"
            }

            VizTextField{
                id: resultRect
                readOnly: true
                Layout.fillWidth: true
                text: ""
                placeholderText: ""
            }

            ListModel{
                id: unitsModel
                ListElement{name: "m"; uID: "m"}
                ListElement{name: "km"; uID: "km"}
                ListElement{name: "miles"; uID: "miles"}
            }

            VizComboBox{
                id: unitComboBox
                listModel: unitsModel
                Layout.minimumWidth: 100
                value: unitsModel.get(selectedUID).name
                onSelectedUIDChanged: {
                    changeUnit(selectedUID)
                }
            }
        }
        RowLayout{
            Layout.fillWidth: true
            VizLabel{
                text: "Distance on Ground"
            }

            VizCheckBox{
                id: calculationTypeCheckbox
                text: ""
                enabled: !calculating
            }
            AnimatedImage{
                source: rootStyle.getBusyBar()
                visible: calculating
                Layout.fillWidth: true
            }

        }
        RowLayout{
            Layout.fillWidth: true
            Rectangle{
                Layout.fillWidth: true
            }
            VizButton{
                id: markButton
                text: "   Mark   "
                enabled: !calculating
                onClicked: {
                    checked = !checked
                    if(selectButton.checked){
                        selectButton.checked = false
                        selectLine(false)
                    }
                    mark(checked)
                }
            }
            Rectangle{
                Layout.fillWidth: true
            }

            VizButton{
                id: selectButton
                text: "Select Line"
                enabled: !calculating
                onClicked: {
                    checked = !checked
                    if(markButton.checked){
                        markButton.checked = false
                        mark(false)
                    }
                    selectLine(checked)
                }
            }
            Rectangle{
                Layout.fillWidth: true
            }
            VizButton{
                id: startButton
                text: calculating ? "   Stop   " : "  Start  "
                onClicked: {
                    if(calculating)
                        stop()
                    else
                        start()
                }
            }
            Rectangle{
                Layout.fillWidth: true
            }
        }
    }
}

