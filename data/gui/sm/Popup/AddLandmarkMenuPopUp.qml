import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{

    title: "Add New Landmark Directory"
    objectName: "addLandmarkDirectoryPopup"

    width: mainLayout.implicitWidth + 10
    height: mainLayout.implicitHeight + 10

    minimumHeight: mainLayout.implicitHeight + 10
    minimumWidth: mainLayout.implicitWidth + 10

    signal browseButtonClicked()
    signal onClickedOk(string path)
    signal showGUIError(string title, string message)

    property alias userDirectoryPath: modelPath.text
    property bool calculating: false
    property alias isOkButtonEnabled : okButton.enabled

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{

        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5

        RowLayout{
            id: browseRow
            VizLabel{
                text: "Model"
                width: 100
                textAlignment: Qt.AlignLeft
            }
            VizTextField{
                id: modelPath
                width: 300
                text: ""
                placeholderText: "Browse user directory..."
                readOnly: true
                Layout.fillWidth: true
                Layout.minimumWidth: 150
            }
            VizButton{
                id: browseButton
                text: " Browse "
                onClicked: {
                    browseButtonClicked()
                }
            }
        }

        RowLayout{
            id: commandButtonRect
            Layout.alignment: Qt.AlignRight
            AnimatedImage{
                id:progressBarId
                Layout.fillWidth: true
                source: rootStyle.getBusyIcon()
                visible: calculating
            }

            VizButton{
                id: okButton
                text:"    OK    "
                onClicked: {
                    onClickedOk(userDirectoryPath);
                    closePopup()
                }
            }
            VizButton{
                id: cancelButton
                text:" Cancel "
                onClicked: {
                    closePopup()
                }
            }
        }
    }
}
