import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "addVectorLayerPopup"
    title: "Add New Vector"

    height: popupLayout.implicitHeight + 2*popupLayout.anchors.margins
    width: popupLayout.implicitWidth+ 2*popupLayout.anchors.margins

    minimumWidth: popupLayout.Layout.minimumWidth + 10
    minimumHeight: popupLayout.Layout.minimumHeight +10



    signal addLayer(string layerName, string selectedFileName, bool editable)
    signal browseButtonClicked()

    property alias selectedFileName: filePath.text
    property alias layerName: layerNameField.text
    property alias layerNameReadOnly: layerNameField.readOnly
    property alias coordSystem: coordSys.text
    //property alias styleSheet: styleSheetBox.value
    property bool showAdvancedOptions: false
    property bool editable: false

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: popupLayout
        anchors.fill:parent
        anchors.margins: 5
        spacing: 5

        RowLayout{
            id:layerNameTag
            spacing: 5

            VizLabel{
                text: "Name"
                textAlignment: Qt.AlignLeft
                //anchors.left: parent.left
            }

            VizTextField{
                id: layerNameField
                Layout.fillWidth: true
                validator:rootStyle.nameValidator
                text: ""
                Layout.minimumWidth: 350
                placeholderText: "Give a name to this layer..."
            }
        }

        RowLayout{
            id:blank
            spacing: 5
        }


        RowLayout{
            id:filePathTag
            spacing: 5

            VizLabel{
                text: "Path "
                textAlignment: Qt.AlignLeft
            }


            VizTextField{
                id: filePath
                text: ""
                Layout.minimumWidth: 350
                placeholderText: "Browse files..."
                Layout.fillWidth: true
            }
            VizButton{
                text:"Browse"
                Layout.minimumWidth: 100
                onClicked: {
                    browseButtonClicked()
                }
            }
        }


        RowLayout{
            id:blank1
            spacing: 5
        }

        RowLayout{
            id:commandButtonRect
            spacing: 20
            Item{
                width:parent.width-okButton.width-cancelButton.width
                Layout.fillWidth: true
            }

            VizCheckBox{
                id: editableCheckbox
                text: "Editable"
                anchors.left: parent.left
                onClicked:{ editable = !editable
                }
            }

            VizLabel{
                id: coordSys
                text: ""
                anchors.left: editableCheckbox.right
                anchors.leftMargin: 20
            }

            VizButton{
                id:okButton
                text: "Ok"
                onClicked: addLayer(layerName, selectedFileName, editable)
            }

            VizButton{
                id:cancelButton
                text: "Cancel"
                onClicked: closePopup()
            }
        }
    }
}


