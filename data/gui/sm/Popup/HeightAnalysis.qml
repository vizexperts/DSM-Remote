import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    id: heightAnalysis
    objectName: "HeightAnalysis"
    title: "Height Analysis"

    Component.onCompleted: {
        if(!running_from_qml){
            MeasureHeightGUI.setActive(true)
        }
        smpFileMenu.setColor(featureColor)
    }

    Component.onDestruction: {
        if(!running_from_qml){
            MeasureHeightGUI.setActive(false)
        }
        smpFileMenu.closeColorPicker()
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal markPosition(bool value)
    signal calculationTypeChanged()
    signal currentIndexChanged()
    signal updatePosition()
    signal fontSizeChanged(string value)
    signal fontColorChanged(color x)

    property bool colorPickerVisible:false;

    onColorPickerVisibleChanged: {
        if(colorPickerVisible === true){
            smpFileMenu.openColorPicker()
        }
        else{
            smpFileMenu.closeColorPicker()
        }
    }

    function colorChanged(){
        var fontObj = smpFileMenu.getColorPicker();
        featureColor = fontObj.currentColor
    }

    property alias longitude : longitude.text
    property alias latitude : latitude.text
    property alias isMarkSelected : markButton.checked
    property alias isUpdateSelected : updateButton.checked
    property alias heightvalue : height.text
    property alias calType : typeComboBox.value
    property alias unitType : unitComboBox.value
    property alias featureColor : colorRectangle.backgroundColor
    property string selectedBox

    onFeatureColorChanged:fontColorChanged(featureColor)

    function setFontSize(value){
        fontsizeSpinBox.setValue(value)
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            Layout.fillWidth: true
            z:1

            VizLabel{
                text: "Position"
            }

            VizTextField {
                id: latitude
                Layout.fillWidth: true
                placeholderText: "Latitude"
                validator: rootStyle.latitudeValidator
            }


            VizTextField {
                id: longitude
                Layout.fillWidth: true
                placeholderText: "Longitude"
                validator: rootStyle.longitudeValidator
            }


            VizTextField {
                id: height
                Layout.fillWidth: true
                placeholderText: "Height"
                validator:  DoubleValidator {notation: DoubleValidator.StandardNotation; }


            }



            VizButton{
                id: updateButton
                objectName: "update"
                text:"    Mark    "
                backGroundVisible: true
                checkable : true
                Layout.alignment: Qt.AlignRight
                onClicked: updatePosition()
            }
        }

        RowLayout{
            z : 160

            VizButton{
                id: markButton
                objectName: "markArea"
                text:"    Enable Marker    "
                backGroundVisible: true
                checkable : true
                Layout.alignment: Qt.AlignRight
                onClicked: markPosition(checked)
            }

            Rectangle{
                Layout.fillWidth: true
            }

            VizLabel{
                id: calculationTypeLabel
                text: "Calculation Type"
            }

            VizComboBox{
                id: typeComboBox
                Layout.fillWidth: true
                listModel: typeModel              

                onSelectOption: calculationTypeChanged()

                ListModel{
                    id: typeModel
                    ListElement{name: "Mean Sea Level"; uID: "Sea"}
                    ListElement{name: "Relative to Terrain"; uID: "Terrain"}

                }

                value : typeModel.get(0).name
            }

            VizLabel{
                id: unitTypeLabel
                text: "Unit Type"
            }

            VizComboBox{
                id: unitComboBox
                Layout.fillWidth: true
                listElementAlignment: Qt.AlignRight
                listModel: unitModel              

                onSelectOption: currentIndexChanged()

                ListModel{
                    id: unitModel
                    ListElement{name: "m"; uID: "M"}
                    ListElement{name: "km"; uID: "KM"}
                    //ListElement{name: "miles"; uID: "MILES"}

                }
                value : unitModel.get(0).name
            }
        }

        RowLayout{
            id: fontSizeColumnRow
            visible: true
            spacing: 5
            z: 100
            VizLabel{
                width: 100
                text: "Font_Size"
                textAlignment: Qt.AlignLeft
            }

            VizSpinBox{
                id: fontsizeSpinBox
                Layout.minimumWidth: 50
                placeholderText: "Fontsize"
                minimumValue:0
                maximumValue: 50
                value : 25
                validator: IntValidator{
                    bottom: 1
                    top: 50
                }
                onValueChanged: fontSizeChanged(value)
            }

            VizLabel{
                width: 100
                text: "Font Color"
                textAlignment: Qt.AlignLeft
            }
            VizButton{
                id: colorRectangle
                flatButton: true
                onClicked: {
                    if(selectedBox === "textColor"){
                        //featureColor = colorPicker.currentColor
                        colorChanged()
                        //colorPicker.scale = 0
                        colorPickerVisible = false
                        selectedBox = ""
                    }
                    else{
                        //colorPicker.currentColor = featureColor
                        smpFileMenu.setColor(featureColor)
                        colorPickerVisible = true
                        //colorPicker.scale = 1
                        selectedBox = "textColor"
                    }
                }
            }
        }

    }
}


