import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"
import ".."

//!XXX: Only being used for histogram styling now
Popup {

    objectName: "pointLayerStylePopup"
    title: "Point Style"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth + 2*mainLayout.anchors.margins

    //! disable the UI when doing this
    enabled: !busyBarVisible

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    //! Draw technique
    function setDrawPropertyType(drawPropertyType, model){
        for(var i = 0; i < drawPropertyTypeModel.count ; ++i){
            if(drawPropertyTypeModel.get(i).drawPropertyName === drawPropertyType){
                if(drawPropertyTypeModel.get(i).model === model){
                    geometryTypeComboBox.selectedUID = drawPropertyTypeModel.get(i).uID
                    geometryTypeComboBox.value = drawPropertyTypeModel.get(i).name
                }
            }
        }
    }

    Component.onCompleted: {
        smpFileMenu.setColor(featureColor)
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    signal changeDrawTechniqueType(string drawTechnique, string modelName)

    signal showIconMenu(bool value)
    signal annotationChanged(string columnName)
    signal fontSizeChanged(string size)
    signal fontColorChanged(color x)
    signal fontStyleChanged(int fontfile)
    signal changeColorTransparency(int alphaValue)

    signal okButtonPressed()

    property alias iconPath: iconButton.iconSource
    property alias styleColumnVisible: mainLayout.visible
    property alias selectedColumnName: annotationComboBox.value

    property alias fontstyle: unitComboBox2.value
    property alias fontsize : fontsizeSpinBox.value

    property alias histogramWidth: histogramWidthSpinBox.value
    property alias colorTransparency: colorTransparencySlider.value

    property int numHistoramValues: 1

    onColorTransparencyChanged: changeColorTransparency(colorTransparency*100)

    function setHistogramWidth(value){
        histogramWidthSpinBox.setValue(value)
    }

    signal changeHistWidth(double width)

    onHistogramWidthChanged: changeHistWidth(histogramWidth)

    //! Property to hide/show busy bar
    property alias busyBarVisible : busyBar.visible
    property alias featureColor : colorRectangle.backgroundColor

    onFeatureColorChanged:fontColorChanged(featureColor)

    function setFontSize(value){
        fontsizeSpinBox.setValue(value)
    }

    property bool colorPickerVisible:false

    onColorPickerVisibleChanged: {
        if(colorPickerVisible === true){
            smpFileMenu.openColorPicker()
        }
        else{
            smpFileMenu.closeColorPicker()
        }
    }

    function colorChanged(){
        var fontObj = smpFileMenu.getColorPicker();
        featureColor = fontObj.currentColor
    }

    PointLayerProperties{
        id: iconSelectionGrid
        anchors.fill: parent
        visible: false
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id: drawTechniqueRow
            spacing: 5
            z: 170
            VizLabel{
                text: "Load as:"
                textAlignment: Qt.AlignLeft
            }
            VizComboBox{
                id: geometryTypeComboBox
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                enabled: false
                onSelectOption: {
                    changeDrawTechniqueType(drawPropertyTypeModel.get(uID).drawPropertyName, drawPropertyTypeModel.get(uID).model)
                }
                listModel: ListModel{
                    id: drawPropertyTypeModel
                    ListElement{name:"Point"; drawPropertyName: "MarkerDrawProperty" ; model: ""; uID : "0"}
                    ListElement{name:"Border Pillar"; drawPropertyName: "PillarDrawProperty" ; model: "bp" ; uID : "1"}
                    ListElement{name:"Border Outpost"; drawPropertyName: "PillarDrawProperty" ; model: "bop"; uID : "2"}
                    ListElement{name:"Village"; drawPropertyName: "PillarDrawProperty" ; model: "village"; uID : "3"}
                    ListElement{name:"BarChart"; drawPropertyName: "HistogramDrawProperty" ; model: ""; uID : "4"}
                    ListElement{name:"WeaponRange"; drawPropertyName: "WeaponRangeProperty" ; model: ""; uID : "5"}
                }
            }
        }
        RowLayout{
            id: iconRow
            spacing: 5
            visible: geometryTypeComboBox.selectedUID === '0'
            VizLabel{
                text: "Icon"
                textAlignment: Qt.AlignLeft
            }
            VizButton{
                id: iconButton
                anchors.margins: 10
                objectName: "iconButton"
                onClicked: {
                    showIconMenu(true)
                    iconSelectionGrid.visible = true
                    mainLayout.visible = false
                }
            }
        }

        RowLayout{
            id: fontSizeColumnRow
            visible: geometryTypeComboBox.selectedUID === '0' || geometryTypeComboBox.selectedUID === '4'
            spacing: 5
            z: 100
            VizLabel{
                text: "Font Size"
                textAlignment: Qt.AlignLeft
            }

            VizSpinBox{
                id: fontsizeSpinBox
                Layout.minimumWidth: 50
                placeholderText: "Fontsize"
                minimumValue:0
                maximumValue: 50
                value : 30
                validator: IntValidator{
                    bottom: 1
                    top: 50
                }
                onValueChanged: fontSizeChanged(value)
            }
        }

        RowLayout{
            id: fontColorRow
            spacing: 5
            visible: geometryTypeComboBox.selectedUID === '0' || geometryTypeComboBox.selectedUID === '4'
            VizLabel{
                text: "Font Color"
                textAlignment: Qt.AlignLeft
            }
            VizButton{
                id: colorRectangle
                backgroundColor: "Black"
                flatButton: true
                onClicked: {
                    if(selectedBox === "textColor"){
                        colorChanged()
                        colorPickerVisible = false
                        selectedBox = ""
                    }
                    else{
                        smpFileMenu.setColor(featureColor)
                        colorPickerVisible = true
                        selectedBox = "textColor"
                    }
                }
            }
        }

        RowLayout{
            id: fontStyleRow
            visible: geometryTypeComboBox.selectedUID === '0' || geometryTypeComboBox.selectedUID === '4'
            spacing: 5
            z: 150
            VizLabel{
                text: "Font Type"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                id: unitComboBox2
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                listModel: unitModel
                value:"<None>"

                onSelectOption: fontStyleChanged(uID)

                ListModel{
                    id: unitModel
                    ListElement{name: "Arial"; uID: "1"}
                    ListElement{name: "Impact"; uID: "2"}
                    ListElement{name: "Verdana"; uID: "3"}
                    ListElement{name: "Calibri"; uID: "4"}
                    ListElement{name: "Times New Roman"; uID: "5"}
                    ListElement{name: "Vani"; uID: "6"}
                    ListElement{name: "Courier New"; uID: "7"}
                }
            }
        }

        RowLayout{
            id: tranperancyRow
            spacing: 5
            visible: true
            VizLabel{
                text: "Color Opacity"
                textAlignment: Qt.AlignLeft
            }
            VizSlider{
                id: colorTransparencySlider
                visible: true
                labelVisible: false
                Layout.fillWidth: true
                value: 100
            }
        }
        RowLayout{
            id: annotationColumnRow
            visible: geometryTypeComboBox.selectedUID === '0'
            spacing: 5
            z: 100
            VizLabel{
                text: "Annotation"
                textAlignment: Qt.AlignLeft
            }
            VizComboBox{
                id: annotationComboBox
                Layout.fillWidth: true
                Layout.minimumWidth: 200
                listModel: (typeof(layerColumnNameListModel) == "undefined") ? "undefined" : layerColumnNameListModel
                value: "<None>"
                onSelectOption: annotationChanged(value)
            }
        }

        RowLayout{
            id: histogrmWidthRow
            visible: geometryTypeComboBox.selectedUID === '4'
            spacing: 5
            VizLabel{
                text: "Width"
                textAlignment: Qt.AlignLeft
            }
            VizSpinBox{
                id: histogramWidthSpinBox
                Layout.minimumWidth: 50
                placeholderText: "bar width"
                minimumValue: 1
                maximumValue: 51
                singlestep: 5
            }
        }

        RowLayout{
            id: okCancelRow
            spacing: 5

            VizButton{
                text: " Ok "
                onClicked: {
                    okButtonPressed()
                    closePopup()
                }
            }
            VizButton{
                text: " Cancel "
                onClicked: closePopup()
            }
            Rectangle{
                Layout.fillWidth: true
            }
            AnimatedImage{
                id: busyBar
                source: rootStyle.getBusyBar()
                Layout.minimumWidth: 100
                visible: false
            }
        }
    }

    property string selectedBox
}




