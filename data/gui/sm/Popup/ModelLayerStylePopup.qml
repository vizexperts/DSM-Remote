import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup{
    objectName: "modelLayerStylePopup"
    title: "Style Templates"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    signal reloadModelObject(string templateName)

    Component.onCompleted:{
        //ElementListGUI.populateTemplateList()
         StyleTemplateGUI.populateStyleTemplateList();
    }

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.margins: 5
        anchors.fill: parent


        Rectangle{
            id:dirRect
            Layout.minimumWidth: 250
            Layout.minimumHeight: 400
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "transparent"
            border.color: rootStyle.colorWhenSelected

            VizTreeView{
                id: styleReloadDirTreeView
                objectName: "styleReloadDirTreeView"
                anchors.fill: parent
                anchors.topMargin: 10
                Layout.fillWidth: true
                Layout.fillHeight: true

                model:(typeof(StyleListModel) !== "undefined") ? StyleListModel : "undefined"
                VizTreeViewColumn{
                    role: "isDir"
                    delegate: Image {
                        source: styleData.value ? rootStyle.getIconPath("folder") : rootStyle.getIconPath("Menu/file")
                        width: rootStyle.buttonIconSize
                        height: width
                    }
                }
                VizTreeViewColumn{
                    role: "name"
                    delegate: VizLabel {
                        id: nameDelegate
                        text: styleData.value
                        textColor: styleData.textColor
                    }
                }
            }
        }
//        Rectangle{
//            color: "transparent"
//            border.width: 2
//            border.color: rootStyle.colorWhenSelected
//            Layout.preferredHeight: 300
//            Layout.preferredWidth: 250

//            ListView{
//                id: templateList
//                currentIndex: 0
//                anchors.fill: parent
//                model: (typeof(styleSheetList)=="undefined")?"undefined":styleSheetList
//                anchors.margins: 5
//                spacing: 4
//                clip: true
//                focus: true
//                VizScrollBar{
//                    target: templateList
//                    color: "transparent"
//                    gradient: null
//                }
//                delegate: VizLabel{
//                    id: label
//                    text: modelData
//                    textAlignment: Qt.AlignLeft
//                    width: templateList.width
//                    Behavior on color {
//                        ColorAnimation { duration: 200 }
//                    }
//                    Behavior on textColor {
//                        ColorAnimation { duration: 200 }
//                    }

//                    textColor: (templateList.currentIndex == index ) ? rootStyle.selectedTextColor : rootStyle.textColor
//                    color: (templateList.currentIndex == index ) ? rootStyle.colorWhenSelected : "transparent"
//                    MouseArea{
//                        anchors.fill: parent
//                        onClicked: {
//                            templateList.currentIndex = index
//                        }
//                    }
//                }
//            }
//        }


        RowLayout{
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            VizButton{
                text: "Apply"
                onClicked: ElementListGUI.reloadModelObject()
            }
            VizButton{
                text: "Ok"
                onClicked:{
                    ElementListGUI.reloadModelObject()
                    closePopup()
                }
            }
            VizButton{
                text: "Cancel"
                onClicked: closePopup()
            }
        }

    }
}
