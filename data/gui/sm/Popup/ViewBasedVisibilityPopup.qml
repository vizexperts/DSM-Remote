import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Popup {
    id: viewBasedVisibilityPopup
    objectName: "viewBasedVisibilityPopup"
    title: "Set view based visibility"

    height: mainLayout.implicitHeight+40
    width: mainLayout.implicitWidth+40

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property real currentLOD: 0

    property alias propEnabled : visualizationGroup.checked

    property int spinnerStepSize: 5000
    property int maxSpinnerValue: 15000000

    signal setLOD(string type, int lod)
    signal goToLod(int lod, bool zoomin)
    signal togleProperty(bool value)

    property real originalMinLOD : 0
    property real originalMaxLOD : 0
    property bool originalPropEnabled : false

    Connections{
        target: smpFileMenu
        onSizeChanged : {
            minimumWidth = minimumWidth + newWidth
            minimumHeight =minimumHeight +newHeight
        }
    }

    function validateLOD(val){
        if(val < 10000){
            return val + " m"
        }
        else{
            return (val/1000.0).toFixed(2) + "km"
        }
    }

    function setMinLod(lod){
        originalMinLOD = lod
        minLodSpinBox.text = lod
    }

    function setMaxLod(lod){
        originalMaxLOD = lod
        maxLodSpinBox.text = lod
    }

    function setPropEnabled(prop){
        propEnabled = prop
        originalPropEnabled = prop
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        VizGroupBox{
            id:visualizationGroup
            title:"Visualization"
            checkable: true
            checked: false
            Layout.fillHeight: true
            Layout.fillWidth: true
            onCheckedChanged: {
                togleProperty(checked)
                if(checked){
                    setLOD("min", minLodSpinBox.text)
                    setLOD("max", maxLodSpinBox.text)
                }
            }
            ColumnLayout{
                id: column
                RowLayout{
                    id:lodLabelRow
                    spacing:20
                    VizLabel{
                        id:minLod
                        text: "Current LOD"
                    }
                    Item{
                        Layout.fillWidth: true
                    }
                    VizLabel{
                        id: curLod
                        text: validateLOD(currentLOD)
                    }
                }
                RowLayout{
                    id:lodRow
                    spacing:20
                    ColumnLayout{
                        RowLayout{
                            spacing: 20
                            VizLabel{
                                text: "Min LOD"
                            }

                            VizTextField{
                                id: minLodSpinBox
                                placeholderText: "0"
                                validator: IntValidator{bottom:maxLodSpinBox.text; top:maxSpinnerValue}
                                onAccepted: {
                                    setLOD("min", text)
                                }
                            }
                        }
                        VizButton{
                            id:minLodButton
                            text: "Set Current as Min"
                            anchors.horizontalCenter: parent.horizontalCenter
                            enabled: ((currentLOD > maxLodSpinBox.text) || (maxLodSpinBox.text === "0"))
                            onClicked:{
                                minLodSpinBox.text = currentLOD
                                setLOD("min", currentLOD)
                            }
                        }
                    }

                    Item{
                        width: 50
                        Layout.fillWidth: true
                    }
                    ColumnLayout{
                        RowLayout{
                            spacing: 20
                            height: children.height
                            VizLabel{
                                text: "Max LOD"
                            }

                            VizTextField{
                                id: maxLodSpinBox
                                placeholderText: "0"
                                validator: IntValidator{bottom:0; top:minLodSpinBox.text}
                                onAccepted: {
                                    setLOD("max", text)
                                }
                            }
                        }
                        VizButton{
                            id:maxLodButton
                            text: "Set current as Max"
                            anchors.horizontalCenter: parent.horizontalCenter
                            enabled: ((currentLOD < minLodSpinBox.text) || (minLodSpinBox.text === "0"))
                            onClicked:{
                                maxLodSpinBox.text = currentLOD
                                setLOD("max", currentLOD)
                            }
                        }
                    }
                }
            }
        }
        RowLayout{
            Item{
                Layout.fillWidth: true
            }
            VizButton{
                text: " Ok "
                onClicked: closePopup()
            }
            VizButton{
                text: " Cancel "
                onClicked: {

                    //! Reset property value

                    if(originalPropEnabled !== propEnabled){
                        togleProperty(originalPropEnabled)
                    }

                    if(originalPropEnabled){
                        setLOD("min", originalMinLOD)
                        setLOD("max", originalMaxLOD)
                    }

                    closePopup()
                }
            }
        }
    }
}
