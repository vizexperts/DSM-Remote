import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "areaFeaturesMenu"

    signal setFeatureEnable(string type)

    property int selectedIndex : -1

    Component.onDestruction: setFeatureEnable("")

    onOptionClicked:{
        if(option === "Back") {
            menuHolder.setSource("Features.qml")
        }
        else{

            if(selectedIndex != currentIndex){
                if(selectedIndex != -1){
                    listModel.get(selectedIndex).selected = false
                }
                listModel.get(currentIndex).selected = true
                selectedIndex = currentIndex
                setFeatureEnable(option)
            }
            else {
                listModel.get(currentIndex).selected = false
                selectedIndex = -1
                setFeatureEnable("")
            }
        }
    }

    function resetAreaFeatures()
    {
        listModel.get(currentIndex).selected = false
        selectedIndex = -1
        setFeatureEnable("")
    }

    VizButtonColumnItemData{
        icon:"Features/DSM_Lake"
        toolTip: "Add Lakes"
        label: "Lakes"
        selected: false
        name: "Lakes"
    }
    VizButtonColumnItemData{
        icon: "Features/DSM_Pond"
        toolTip: "Add Ponds"
        label: "Ponds"
        selected: false
        name: "Ponds"
    }
    VizButtonColumnItemData{
        icon:  "Features/DSM_Forest"
        toolTip: "Add Forests"
        label: "Forests"
        selected: false
        name: "Forests"
    }
}
