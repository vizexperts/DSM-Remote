import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "markingFeaturesMenu"

    property string selectedOption : ""

    signal setPointEnable(bool value)
    signal setLineEnable(bool value)
    signal setPolygonEnable(bool value)
    signal setFenceEnable(bool value)
    signal setLabelEnable(bool value)

    Component.onDestruction: setState(selectedOption, false)

    function setState(type, state){
        if(type === "Points"){
            setPointEnable(state)
            listModel.get(0).selected = state
        }
        else if(type === "Lines"){
            setLineEnable(state)
            listModel.get(1).selected = state
        }
        else if(type === "Area"){
            setPolygonEnable(state)
            listModel.get(2).selected = state
        }
        else if(type === "Label"){
            setLabelEnable(state)
            listModel.get(4).selected = state
        }

        //        else if(type === "CustomLayers"){
        //            CreateLayerGUI.setActive(state)
        ////            listModel.get(3).selected = state
        //        }
    }

    onOptionClicked:{
        if(option === "Back") {
            menuHolder.loadMainMenu()
        }
        else if(option == "Tactical"){
            smpFileMenu.load("CreateTacticalSymbols.qml")
        }
        else{
            if(selectedOption !== option){
                setState(selectedOption, false)
                setState(option, true)
                selectedOption = option
            }
            else{
                setState(option, false)
                selectedOption = ""
            }
        }
    }

    VizButtonColumnItemData{
        icon: "Features/addPoint"
        toolTip: "Add Points"
        label: "Points"
        selected: false
        name: "Points"
    }
    VizButtonColumnItemData{
        icon: "Features/addLine"
        toolTip: "Add Lines"
        label: "Lines"
        selected: false
        name: "Lines"
    }
    VizButtonColumnItemData{
        icon: "Features/addArea"
        toolTip: "Add Area"
        label: "Area"
        selected: false
        name: "Area"
    }
    VizButtonColumnItemData{
        icon: "Features/addPoint"
        toolTip: "Add Area"
        label: "Tactical"
        selected: false
        name: "Tactical"
        menuEnabled: licenseManager.georbIS_Strategem
    }
    VizButtonColumnItemData{
        icon: "Features/addPoint"
        toolTip: "Add Label"
        label: "Label"
        selected: false
        name: "Label"
    }
}
