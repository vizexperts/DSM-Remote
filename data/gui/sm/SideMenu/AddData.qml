import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "AddLayerTab"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.loadMainMenu()
        }
        else if(option === "WebData"){
            menuHolder.setSource("AddWebData.qml")
            smpFileMenu.unloadAll()
        }
        else if(option == "CreateLayer"){
            if(! smpFileMenu.load("CreateLayer.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }

        }
        else if(option == "TerrainExport"){
            if(! smpFileMenu.load("TerrainExport.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }

        }
        else if(option == "SpatialQuery"){
            if(! smpFileMenu.load("AddIncidentDataPopup.qml", root.width/2, root.height/2))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else {
            menuHolder.setSource(option + ".qml")
        }
    }

    VizButtonColumnItemData{
        icon: "Menu/satellite"
        toolTip: "Add Local or DB data"
        label: "Satellite"
        name: "SatLayers"
    }
    VizButtonColumnItemData{
        icon:  "Menu/CustomLayer"
        toolTip: "Add Custom Layers"
        label: "Custom Layers"
        name: "CreateLayer"
    }
    VizButtonColumnItemData{
        icon: "Menu/addContent"
        toolTip: "Add Log data"
        label: "Sensor Logs"
        name: "CustomLayers"
        menuEnabled: licenseManager.georbIS_Footprint
    }
    VizButtonColumnItemData{
        icon: "AddData/webData"
        toolTip:  "Internet Data Sources"
        label:  "Internet"
        name: "WebData"
    }
    VizButtonColumnItemData{
        icon: "Menu/SpatialQuery"
        toolTip: "Perform Spatial query on database"
        label: "Spatial Query"
        selected: false
        name: "SpatialQuery"
        menuEnabled: licenseManager.georbIS_Intel
    }
}
