import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

Item{
    id: fovSettings
    objectName: "ViewSettings"

    width: mainLayout.implicitWidth + 40
    height: mainLayout.implicitHeight + closeButton.implicitHeight + 30

    Component.onCompleted: {
        menuHolder.connectFovMenu()
    }

    property alias fovValue:fovSpinBox.value
    property alias scaleValue: verticalScaleSpinBox.value
    property alias useAOR: useAorCB.checked
    property alias markButtonChecked: markButton.checked
    property alias useOrtho: orthoViewCB.checked
    property alias selection: selectionCB.checked
    property alias dispayGrid: gridCB.checked
    property alias dispayIndianGrid: indianGridCB.checked

    onUseAORChanged: {
        changeAorSetting(useAOR)
    }

    onUseOrthoChanged: {
        changeToOrtho(useOrtho)
    }


    signal changeFov(double fovValue)
    signal changeAorSetting(bool value)
    signal changeToOrtho(bool value)
    signal markAOR(bool value)
    signal save()
    signal changeSelectionState(bool value)
    signal displayGrid(bool value)
    signal displayIndianGrid(bool value)
    signal verticalScale(double scaleValue)

    Rectangle{
        id: boundary
        anchors.margins: 5
        anchors.fill : parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
        border.width: 2
        radius: 2
        border.color:"DeepSkyBlue"
    }

    VizLabel{
        id: titleLabel
        width: parent.width - 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: rootStyle.colorWhenDefault
        height: closeButton.height
        text: "View Settings"
    }

    VizButton{
        id: closeButton
        anchors.right: boundary.right
        anchors.verticalCenter: titleLabel.verticalCenter
        backGroundVisible: false
        iconSource: rootStyle.getIconPath("backLeft")
        onClicked: leftMenu.setSource("SettingsMenu.qml")
    }

    GridLayout{
        id: mainLayout
        anchors.fill: boundary
        anchors.margins: 2
        anchors.topMargin: closeButton.height
        columns:2

        VizLabel{ text:"Field of View" }

        VizSpinBox{
            id: fovSpinBox
            minimumValue: 0
            maximumValue: 90
            singlestep: 1
            validator: IntValidator{
                top:fovSpinBox.maximumValue
                bottom:fovSpinBox.minimumValue
            }
            onValueChanged:changeFov(value)
        }
        VizLabel{ text:"Vertical Scale" }

        VizSpinBox{
            id: verticalScaleSpinBox
            minimumValue: 0
            maximumValue: 10
            singlestep: 0.01
            validator: DoubleValidator{
                top:verticalScaleSpinBox.maximumValue
                bottom:verticalScaleSpinBox.minimumValue
            }
            onValueChanged:verticalScale(value)
        }

        VizLabel{
            text: "2D view"
        }

        VizCheckBox{
            id: orthoViewCB
            text: ""
        }

        VizLabel{
            text: "Object Selection"
        }

        VizCheckBox{
            id: selectionCB
            text: ""
            onClicked: changeSelectionState(checked)
        }

        VizLabel{
            text: "Use AOR"
            visible: licenseManager.georbIS_Strategem
        }

        VizCheckBox{
            id: useAorCB
            text: ""
            visible: licenseManager.georbIS_Strategem
        }

        VizLabel{
            text: "Grid"
        }

        VizCheckBox{
            id: gridCB
            text: ""
            onClicked: displayGrid(checked)
        }
        VizLabel{
            text: "Indian Grid"
            visible: licenseManager.georbIS_Strategem
        }

        VizCheckBox{
            id: indianGridCB
            text: ""
            onClicked: displayIndianGrid(checked)
            visible: licenseManager.georbIS_Strategem
        }
        VizButton{
            text: "Mark"
            id: markButton
            checkable: true
            checked: false
            onClicked: markAOR(checked)
        }

        VizButton{
            id:saveButton
            text:"   Save    "
            Layout.alignment: Qt.AlignHCenter
            onClicked:save();
        }
    }
}


