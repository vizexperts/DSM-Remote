import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "AddLayerTab"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.loadMainMenu()
        }
        else if(option == "TerrainExport"){
            if(! smpFileMenu.load("TerrainExport.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }

        }
        else if(option == "SnowConvert"){
            if(! smpFileMenu.load("SnowConvert.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
    }

    VizButtonColumnItemData{
        icon:"terrain_export"
        toolTip: "Terrain Export"
        label: "Terrain Export"
        name: "TerrainExport"
    }
    VizButtonColumnItemData{
        icon:"terrain_export"
        toolTip: "Snow Convert"
        label: "Snow Convert"
        name: "SnowConvert"
    }
}
