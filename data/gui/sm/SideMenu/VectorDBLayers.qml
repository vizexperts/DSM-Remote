import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "AddVectorDBLayers"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.setSource("VectorLayers.qml")
        }
        else if(option === "Oracle"){
            smpFileMenu.load("AddOracleVectorDatabaseLayerPopup.qml")
        }
        else if(option == "MSSQL"){
            smpFileMenu.load("AddMSSQLVectorDatabaseLayerPopup.qml")
        }
        else if(option == "Incidents"){
            smpFileMenu.load("AddIncidentDataPopup.qml", root.width/2, root.height/2)
        }
    }

    VizButtonColumnItemData{
        icon: "Menu/Oracle"
        toolTip: "Add oracle vector layer"
        label: "Oracle"
        selected: false
        name: "Oracle"
    }
    VizButtonColumnItemData{
        icon: "Menu/mssql"
        toolTip: "Add mssql vector layer"
        label: "MSSQL"
        selected: false
        name: "MSSQL"
    }
    VizButtonColumnItemData{
        icon: "Menu/textlist"
        toolTip: "Add Incidents vector layer"
        label: "Incidents"
        selected: false
        name: "Incidents"
    }
}
