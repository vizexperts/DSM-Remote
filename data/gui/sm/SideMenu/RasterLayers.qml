import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "AddRasterLayer"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.setSource("SatLayers.qml")
        }
        else if(option === "File"){
            if(!smpFileMenu.load("AddRasterLayerPopUp.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else if(option === "WMS"){
            if(!smpFileMenu.load("AddWMSLayerPopUp.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else if(option === "Download Raster"){
            if(!smpFileMenu.load("DownloadRasterPopup.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
    }

    VizButtonColumnItemData{
        icon: "Menu/file"
        toolTip: "Add raster layer"
        label: "File"
        selected: false
        name: "File"
    }
    VizButtonColumnItemData{
        icon: "Menu/wms-wfs"
        toolTip: "Add WMS layer"
        label: "WMS"
        selected: false
        name: "WMS"
    }
    VizButtonColumnItemData{
        icon: "Menu/wms"
        toolTip: "Download Tiff From WMS Layer"
        label: "Download Raster"
        selected: false
        name: "Download Raster"
    }
}
