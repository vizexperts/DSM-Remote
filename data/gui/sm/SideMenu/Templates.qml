import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

Item{
    id: weatherSettings

    objectName: "smpTemplatesMenu"

    width: mainLayout.implicitWidth + 30
    height: mainLayout.implicitHeight + closeButton.height + 30

    property string templateName;
    property alias curTemplateComboBoxValue: defTemplateComboBox.value

    signal deleteTemplate(string name);


    Component.onCompleted: {
        ProjectManagerGUI.loadsmpTemplatesMenu();
    }

    function templateNameValue(){
        templateName=ProjectManagerGUI.getDefaultTemplate();
        return templateName;
    }

    Rectangle{
        id: boundary
        anchors.margins: 5
        anchors.fill : parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
        border.width: 2
        radius: 2
        border.color:"DeepSkyBlue"
    }

    VizLabel{
        id: titleLabel
        width: parent.width - 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: rootStyle.colorWhenDefault
        height: closeButton.height
        text: "Default Template"
    }

    VizButton{
        id: closeButton
        anchors.right: boundary.right
        anchors.verticalCenter: titleLabel.verticalCenter
        backGroundVisible: false
        iconSource: rootStyle.getIconPath("backLeft")
        onClicked: menuHolder.setSource("SettingsMenu.qml")
    }

    RowLayout{
        id: mainLayout
        anchors.fill: boundary
        anchors.margins: 2
        anchors.topMargin: closeButton.height
        VizComboBox{
            id: defTemplateComboBox
            objectName: "TemplateComboBoxList"
            Layout.minimumWidth: 200
            listModel:(typeof(templateProjectsList) !== "undefined") ? templateProjectsList: "undefined"
            Layout.fillWidth: true
            value:templateNameValue()
            onSelectOption:{
                if(!running_from_qml)
                {
                    templateName=value;
                    ProjectManagerGUI.setDefaultTemplate(value)

                }
            }
        }
        anchors.horizontalCenter: parent.horizontalCenter
        VizButton{

            anchors.verticalCenter:   parent.verticalCenter
            iconSource: rootStyle.getIconPath("add")
            onClicked: smpFileMenu.load("SaveAsTemplatePopup.qml")
            Layout.minimumWidth: 60
            implicitHeight: defTemplateComboBox.height

        }
        VizButton{

            anchors.verticalCenter:   parent.verticalCenter
            iconSource: rootStyle.getIconPath("delete")
            enabled: curTemplateComboBoxValue != ""

            onClicked:
            {

                deleteTemplate(curTemplateComboBoxValue);
            }
            Layout.minimumWidth: 60
            implicitHeight: defTemplateComboBox.height
        }
    }
}
