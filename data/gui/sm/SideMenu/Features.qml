import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "featuresMenu"

    onOptionClicked:{
        if(option === "Back") {
            menuHolder.loadMainMenu()
        }
        else if (option === "Bookmarks")
        {
            if(! smpFileMenu.load("BookmarksPopup.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }

        else{
            menuHolder.setSource(option + ".qml")
        }
    }

    VizButtonColumnItemData{
        icon: "Menu/landmark"
        toolTip: "Add Landmarks"
        label: "3D Models"
        selected: false
        name: "Landmarks"
    }
    /*VizButtonColumnItemData{
        icon: "Features/DSM_Road"
        toolTip: "Line Features"
        label: "Roads/Rivers"
        selected: false
        name: "LineFeatures"
    }*/
    VizButtonColumnItemData{
        icon: "Features/DSM_Lake"
        toolTip: "Area Features"
        label: "Lakes/Forests"
        selected: false
        name: "AreaFeatures"
    }

    VizButtonColumnItemData{
        icon: "Menu/bookmarks"
        toolTip: "Bookmarks"
        label: "Bookmarks"
        selected: false
        name: "Bookmarks"
    }
}
