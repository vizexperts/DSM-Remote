import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "pointLayerTab"

    Component.onDestruction:
    {
        CreatePointGUI._resetForDefaultHandling()
    }

    onOptionClicked:{
        if(option === "Back"){
            CreatePointGUI.addPointsToSelectedLayer(false)
            menuHolder.minimized = true
        }
        else if(option === "AddPoints"){
            listModel.get(0).selected = !listModel.get(0).selected
            CreatePointGUI.addPointsToSelectedLayer(listModel.get(0).selected)
        }
        else if(option === "Export"){
            listModel.get(0).selected = false
            CreatePointGUI.addPointsToSelectedLayer(false)
            FeatureExportGUI.setMode(10);
            smpFileMenu.load("ExportWindow.qml")
        }
        else if(option === "ExportGeo"){
            listModel.get(0).selected = false
            CreatePolygonGUI.addPolygonToSelectedLayer(false)
            FeatureExportGUI.setMode(1);
            smpFileMenu.load("ExportGeoWindow.qml")
        }
        else if(option === "Query"){
            //            listModel.get(0).selected = false
            //            CreatePolygonGUI.addPolygonToSelectedLayer(false)
            smpFileMenu.load("QueryBuilder.qml")
        }
    }

    VizButtonColumnItemData{
        icon: "Features/addPoint"
        toolTip: "Add Points To Selected Layer"
        label: "Add points"
        selected: false
        name: "AddPoints"
    }
    VizButtonColumnItemData{
        icon: "Features/export"
        toolTip: "Export Selected Layer"
        label: "Export Layer"
        selected: false
        name: "Export"
    }
    VizButtonColumnItemData{
        icon: "Features/export"
        toolTip: "Export Selected Layer to WebServer"
        label: "WebServer"
        selected: false
        name: "ExportGeo"
    }
    VizButtonColumnItemData{
        icon: "list"
        toolTip: "Query Selected Data"
        label: "Query Builder"
        selected: false
        name: "Query"
    }
}
