import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "AddElevationLayer"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.setSource("SatLayers.qml")
        }
        else if(option === "File"){
            if(!smpFileMenu.load("AddElevationLayerPopUp.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
    }

    VizButtonColumnItemData{
        icon: "Menu/file"
        toolTip: "Add elevation layer"
        label: "File"
        selected: false
        name: "File"
    }
}
