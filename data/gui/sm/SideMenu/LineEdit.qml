import QtQuick 2.1

// Component to make Editable line edit that brings up the onscreen keyboard

//!NP: Used only in RightPane search box, To be removed. Do not use.

TextInput{
    id: textInput
    font.family: rootStyle.fontFamily
    font.pixelSize: rootStyle.fontSize
    color: rootStyle.textColor
    width: TextInput.width
    onFocusChanged: {
        if(!focus){
            cursorPosition = 0
        }
        if(multi_touch_interaction_active){
            if(focus && !readOnly)
                keyboard.open(textInput)
            else{
                accepted()
            }
        }
    }
    onTextChanged: {
        if(multi_touch_interaction_active){
            if(keyboard.text !== text){
                keyboard.text = text
            }
        }
    }

    Keys.onEscapePressed: {
        focus = false
        if(multi_touch_interaction_active){
            keyboard.close()
        }
    }
    Keys.onReturnPressed: {
        focus = false
        if(multi_touch_interaction_active){
            keyboard.close()
        }
        accepted()
    }

    Keys.onEnterPressed: {
        focus = false
        if(multi_touch_interaction_active){
            keyboard.close()
        }
        accepted()
    }

    Keys.onLeftPressed: {
        cursorPosition--
        accepted()
    }
    Keys.onRightPressed: {
        cursorPosition++
        accepted()
    }
    Keys.onUpPressed: {
        accepted()
    }
    Keys.onDownPressed: {
        accepted()
    }

    Keys.onReleased: {
        event.accepted = true
    }
}
