import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    id: buttonColumn
    objectName: "addWebData"

    // declarations of signals
    signal addYahooMaps(bool value)
    signal addBingMaps(bool value)
    signal addOSMaps(bool value)
    signal addNASAWW(bool value)
    signal addGoogleStreets(bool value)
    signal addGoogleHybrid(bool value)

    property bool yahooSelected: false
    property bool bingSelected: false
    property bool opsSelected: false
    property bool worldWindSelected: false
    property bool googleStreetSelected: false
    property bool googleHybridSelected: false

    function syncModel(){
        listModel.get(0).selected = yahooSelected
        listModel.get(1).selected = bingSelected
        listModel.get(2).selected = opsSelected
        listModel.get(3).selected = worldWindSelected
//        listModel.get(4).selected = googleStreetSelected
//        listModel.get(5).selected = googleHybridSelected
    }

        /*VizButtonColumnItemData{
            name: "YahooMapsData"
            icon: "maplogos/yahooLogo"
            toolTip: "Add data from Yahoo!Maps"
            label: "Yahoo! Maps"
            selected : false
        }*/
        VizButtonColumnItemData{
            name: "BingMapsData"
            icon: "maplogos/bngLogo"
            toolTip: "Add data from BingMaps"
            label: "Bing Maps"
            selected : false
        }
        VizButtonColumnItemData{
            name: "OSMapsData"
            icon: "maplogos/OSMLogo"
            toolTip: "Add data from OpenStreetMaps"
            label: "OpenStreet"
            selected : false
        }
        VizButtonColumnItemData{
            name: "NasaWorldWindData"
            icon: "maplogos/nasaLogo"
            toolTip: "Add data from NASA's WorldWind"
            label: "WorldWind"
            selected : false
        }
//        VizButtonColumnItemData{
//            name: "GoogleStreetMapsData"
//            icon: "maplogos/GSMLogo"
//            toolTip: "Add data from Google street maps"
//            label: "GoogleStreet Maps"
//            selected : false
//        }
//        VizButtonColumnItemData{
//            name: "GoogleHybridMapsData"
//            icon: "maplogos/GHMLogo"
//            toolTip: "Add data from Google hybrid maps"
//            label: "GoogleHybrid Maps"
//            selected : false
//        }


    onOptionClicked: {
        if(option == "Back"){
            menuHolder.setSource("AddData.qml")
        }
        else {
            listModel.get(currentIndex).selected = !listModel.get(currentIndex).selected
            if(option === "YahooMapsData") { addYahooMaps(listModel.get(currentIndex).selected) }
            else if(option === "OSMapsData") { addOSMaps(listModel.get(currentIndex).selected) }
            else if(option === "BingMapsData") { addBingMaps(listModel.get(currentIndex).selected) }
            else if(option === "NasaWorldWindData") { addNASAWW(listModel.get(currentIndex).selected) }
            else if(option === "GoogleStreetMapsData") { addGoogleStreets(listModel.get(currentIndex).selected) }
            else if(option ==="GoogleHybridMapsData") { addGoogleHybrid(listModel.get(currentIndex).selected) }
        }
    }
}
