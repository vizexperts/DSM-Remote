import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

Item {
    id: guiSettingsMenu
    objectName: "guiSettingsMenu"

    width: mainLayout.implicitWidth + 40
    height: mainLayout.implicitHeight + closeButton.implicitHeight + 30

    Component.onCompleted: {
        if(!running_from_qml)
            GuiSettingsGUI.connectGuiSettingsMenu()
    }


    function latLongUnitValue(){
        for(var i = 0; i < unitListModel.count; i++){
            if(unitListModel.get(i).uID === rootStyle.latlongUnit){
                return unitListModel.get(i).name
            }
        }

    }

    function docModeValue(){
        var dockModeState=(rootStyle.docked===true)?1:2;
        for(var i = 0; i < unitListModel1.count; i++){
            if(unitListModel1.get(i).uID === dockModeState){
                return unitListModel1.get(i).name
            }
        }
    }

    function grVisibilityValue(){
        var grVisibilityState;
        if(rootStyle.mgrsVisibility && !rootStyle.indianGrVisibility)
        {
            grVisibilityState=0;
        }
        else if(!rootStyle.mgrsVisibility && rootStyle.indianGrVisibility)
        {
            grVisibilityState=1;
        }
        else if(rootStyle.mgrsVisibility && rootStyle.indianGrVisibility)
        {
            grVisibilityState=2;
        }
        else if(!rootStyle.mgrsVisibility && !rootStyle.indianGrVisibility)
        {
            grVisibilityState=3;
        }

        for(var i = 0; i < referenceListModel.count; i++){
            if(referenceListModel.get(i).uID === grVisibilityState){
                 return referenceListModel.get(i).name
            }
        }
    }

    function uiValue(){
                return rootStyle.uiScale

    }
    //Function to on/off mgrs/GR visibility
    function mgrsGrVisibilitychange(uid)
    {
        if(uid==="0")
        {
            rootStyle.mgrsVisibility=true;
            rootStyle.indianGrVisibility=false;
        }
        else if(uid==="1")
        {
            rootStyle.mgrsVisibility=false;
            rootStyle.indianGrVisibility=true;
        }
        else if(uid==="2")
        {        
            rootStyle.mgrsVisibility=true;
            rootStyle.indianGrVisibility=true;
        }
        else if(uid==="3")
        {
            rootStyle.mgrsVisibility=false;
            rootStyle.indianGrVisibility=false;
        }

    }

    signal setLatLongUnit(int unit)
    signal setUIDockedState(bool value)
    signal setUIScale(double value)
    signal setGRVisibilityState(int value)
    signal toggleMultiTouchMode(bool value)
    signal setShowZone(bool value)
    signal setShowLetter(bool value)
    signal setGRNumOfDigits(int num)
    signal setMapSheet(bool value)

    Rectangle{
        id: boundary
        anchors.margins: 5
        anchors.fill : parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
        border.width: 2
        radius: 2
        border.color:"DeepSkyBlue"
    }

    VizLabel{
        id: titleLabel
        width: parent.width - 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: rootStyle.colorWhenDefault
        height: closeButton.height
        text: "UI Settings"
    }

    VizButton{
        id: closeButton
        anchors.right: boundary.right
        anchors.verticalCenter: titleLabel.verticalCenter
        backGroundVisible: false
        iconSource: rootStyle.getIconPath("backLeft")
        onClicked: menuHolder.setSource("SettingsMenu.qml")
    }

    GridLayout{
        id: mainLayout
        anchors.fill: boundary
        anchors.margins: 2
        anchors.topMargin: closeButton.height
        columns: 2

        VizLabel{ text: "Coordinates Unit" }

        VizComboBox{
            id: unitList
            Layout.fillWidth: true
            value:latLongUnitValue()
            listModel: ListModel{
                id: unitListModel
                ListElement{name: "Decimal Degrees" ; uID: 1}
                ListElement{name: "Deg Min" ;  uID: 2}
                ListElement{name: "Deg Min Sec" ; uID: 3}
            }

            onSelectOption: {
                setLatLongUnit(uID)
                rootStyle.latlongUnit = uID
            }
        }

        VizLabel{
            text: "Dock Mode"
        }

        VizComboBox{
            id: unitList1
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter
            value:docModeValue();
            listModel: ListModel{
                id: unitListModel1
                ListElement{name: "Dock" ; uID: 1}
                ListElement{name: "Undock" ;  uID: 2}
            }

            onSelectOption: {
                rootStyle.docked = (uID === "1")
                setUIDockedState((uID === "1"))
            }
        }

        VizLabel{ text: "MapSheet"
        }

        VizCheckBox{

            checked : rootStyle.showMapSheet
            onClicked: {
                rootStyle.showMapSheet = checked
                setMapSheet(checked)
            }
        }

        VizLabel{
            text: "GR"
            visible : licenseManager.georbIS_Strategem
        }

        VizComboBox{
            id: referenceList
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter
            value:grVisibilityValue();
            visible : licenseManager.georbIS_Strategem
            listModel: ListModel{
                id: referenceListModel
                ListElement{name: "MGRS" ; uID: 0}
                ListElement{name: "Indian GR" ; uID: 1}
                ListElement{name: "Both" ;  uID: 2}
                ListElement{name: "None" ;  uID: 3}
                }
                onSelectOption: {
                    mgrsGrVisibilitychange(uID)
                    setGRVisibilityState(uID)
                }

            }

        VizLabel{ text: "Show Zones "
            visible: referenceList.value == "Indian GR" || referenceList.value == "Both"
        }

        VizCheckBox{
            visible: referenceList.value == "Indian GR" || referenceList.value == "Both"

            checked : rootStyle.showZone
            onClicked: {
                rootStyle.showZone = checked
                setShowZone(checked)
            }
        }

        VizLabel{ text: "Show GR letter"
            visible: referenceList.value == "Indian GR" || referenceList.value == "Both"
        }

        VizCheckBox{
            visible: referenceList.value == "Indian GR" || referenceList.value == "Both"
            checked : rootStyle.showGR
            onClicked: {
                rootStyle.showGR = checked
                setShowLetter(checked)
            }
        }

        VizLabel{
            text: "No. of Digits "
            visible: referenceList.value == "Indian GR" || referenceList.value == "Both"
        }

        VizSpinBox{
            //            Layout.fillWidth: true
            visible: referenceList.value == "Indian GR" || referenceList.value == "Both"
            minimumValue: 1
            maximumValue: 5
            singlestep: 1
            value: rootStyle.numOfGrDigits

            validator: DoubleValidator{
                bottom: 0.0
                top: 4.0
            }
            onValueChanged:
            {
                rootStyle.numOfGrDigits = value;
                setGRNumOfDigits(value);
            }
        }

        VizLabel{
            text: "UI Scale"
        }
        VizSpinBox{
            //            Layout.fillWidth: true
            minimumValue: 1
            maximumValue: 4
            singlestep: 0.1
            value: uiValue()

            validator: DoubleValidator{
                bottom: 0.0
                top: 4.0
            }
            onValueChanged:
            {
                rootStyle.uiScale = value
                setUIScale(value)
            }
        }

        VizLabel{ text: "Touch " }

        // by default root.multi_touch_interaction_active = false
        VizCheckBox{
            id: touchModeToggle
            checked : root.multi_touch_interaction_active
            onClicked: {
                root.multi_touch_interaction_active = checked
                toggleMultiTouchMode(checked)
            }
        }
    }
}
