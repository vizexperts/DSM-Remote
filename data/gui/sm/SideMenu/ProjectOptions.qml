import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

Item {
    id: projectSettingsMenu

    objectName: "projectSettingsMenu"

    width: mainLayout.implicitWidth + 30
    height: mainLayout.implicitHeight + 70

    Component.onCompleted: {
        menuHolder.connectProjectSettingsMenu()
        modeList.value = militarySymbolModeValue()
    }

    // MilitarySymbolMode
    property int militarySymbolMode
    property alias symbolSize : symbolSizeSelectionId.value


    function militarySymbolModeValue(){
        for(var i = 0; i < modeListModel.count; i++){
            if(modeListModel.get(i).uID === militarySymbolMode){
                return modeListModel.get(i).name
            }
        }
    }

    signal setMilitaryMode(int mode)
    signal setMilSymbolSize(string symbolSize)

    Rectangle{
        id: boundary
        anchors.margins: 5
        anchors.fill : parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
        border.width: 2
        radius: 2
        border.color:"DeepSkyBlue"
    }
    VizLabel{
        id: close
        width: parent.width - 20
        height: 35
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: rootStyle.colorWhenDefault
        text: "Project Settings"
        Layout.alignment: Qt.AlignLeft
        leftMargin: 5
        fontSize: 16
        fontWeight: 4
    }
    Rectangle{
        height: 30
        width: 30
        anchors.right: boundary.right
        anchors.rightMargin: 5
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: "transparent"
        Image {
            id: backButton
            source: rootStyle.getIconPath("backLeft")
            anchors.fill: parent
        }
        MouseArea {
            anchors.fill: parent;
            onClicked:  {
                menuHolder.setSource("SettingsMenu.qml")
            }
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: boundary
        anchors.margins: 10
        anchors.topMargin: 35

        RowLayout
        {
            id: milSymModeRL
            anchors.topMargin: 10
            anchors.top: parent.top
            anchors.right : parent.right
            anchors.left : parent.left
            height:35
            VizLabel{
                 id: militarySymbolModeLabel
                 anchors.left: parent.left
                 width: parent.width/2
                 text: "Military Symbol Mode"
             }

             VizComboBox{
                 id: modeList
                 anchors.leftMargin: 10
                 anchors.left: militarySymbolModeLabel.right
                 anchors.right: parent.right
                 Layout.alignment: Qt.AlignCenter
                 listModel: ListModel{
                     id: modeListModel
                     ListElement{name: "Icon and Text" ; uID: 4}
                     ListElement{name: "3D Model" ;  uID: 2}
                     ListElement{name: "Auto" ; uID: 5}
                 }

                 onSelectOption: {
                     militarySymbolMode = uID
                     setMilitaryMode(uID)
                 }
                 value: setMilitarySymbolModeValue()
             }
        }

        RowLayout{
             id : symbolSizeRL
             anchors.topMargin: 10
             anchors.top:milSymModeRL.bottom
             anchors.right : parent.right
             anchors.left : parent.left
             height : 35
             VizLabel{
                 id: symbolSizeLabel
                 anchors.left: parent.left
                 width: parent.width/2
                 text: "Symbol Size"
             }

             VizComboBox {
                 id: symbolSizeSelectionId
                 listModel:ListModel{

                     ListElement{name:"16"; uID : "16"}
                     ListElement{name:"32"; uID : "32"}
                     ListElement{name:"48"; uID : "48"}
                     ListElement{name:"64"; uID : "64"}
                     ListElement{name:"80"; uID : "80"}
                     ListElement{name:"96"; uID : "96"}
                     ListElement{name:"112"; uID : "112"}
                     ListElement{name:"128"; uID : "128"}
                 }
                 Layout.fillWidth: true
                 value: symbolSize
                 onValueChanged: {
                       //console.log("value changed" + value)
                 }
                 onSelectedUIDChanged: {
                     setMilSymbolSize(value)
                 }
             }
        }
    }
}
