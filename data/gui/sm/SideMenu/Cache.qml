import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

Item{
    id: cacheSettings
    objectName: "cacheSettings"

    width: mainLayout.implicitWidth + 40
    height: mainLayout.implicitHeight + closeButton.implicitHeight + 30

    Component.onCompleted: {
       menuHolder.connectCacheSettingsMenu()
    }
    property alias dgnCacheCBchecked: dgnCacheCB.checked;
    signal clearDGNCache();

    function clear()
    {
        if (dgnCacheCB.checked)
            clearDGNCache();
    }


    Rectangle{
        id: boundary
        anchors.margins: 5
        anchors.fill : parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
        border.width: 2
        radius: 2
        border.color:"DeepSkyBlue"
    }

    VizLabel{
        id: titleLabel
        width: parent.width - 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: rootStyle.colorWhenDefault
        height: closeButton.height
        text: "Cache Settings"
    }

    VizButton{
        id: closeButton
        anchors.right: boundary.right
        anchors.verticalCenter: titleLabel.verticalCenter
        backGroundVisible: false
        iconSource: rootStyle.getIconPath("backLeft")
        onClicked: leftMenu.setSource("SettingsMenu.qml")
    }

    GridLayout{
        id: mainLayout
        anchors.fill: boundary
        anchors.margins: 2
        anchors.topMargin: closeButton.height
        columns:2
        VizLabel{
            text: "DGN Cache"
        }
        VizCheckBox{
            id: dgnCacheCB
            text: ""
        }
        Item{
            Layout.fillWidth: true
        }
        VizButton{
            id:clearButton
            text:"   Clear    "
            Layout.alignment: Qt.AlignRight
            onClicked: clear();
        }
    }
}


