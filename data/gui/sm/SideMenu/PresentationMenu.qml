import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "presentationMenu"
    position: "left"

    onOptionClicked: {
        if(option == "Back"){
            menuHolder.loadMainMenu()
        }
        else if(option == "TextToSpeechMenu"){
            smpFileMenu.load("TTSGUI.qml")
            //setState(selectedOption, false)
            //menuHolder.browseForTextToSpeech()
        }
        else if(option == "RecordMenu"){
            if(smpFileMenu.load("RecordPopup.qml"))
            {
                menuHolder.minimized = true
            }
        }
        else {

            if(bottomLoader.load(option +".qml"))
            {
               menuHolder.minimized = true
            }


        }
    }

    VizButtonColumnItemData{
        icon: "Presentation/TourIcon"
        toolTip: "Tour Menu"
        label: "Tour"
        selected: false
        name: "TourMenu"
    }
    VizButtonColumnItemData{
        icon: "Presentation/PDF"
        toolTip: "PDF"
        label: "PDF"
        selected: false
        name: "PDFMenu"
        menuEnabled: licenseManager.georbIS_Pro
    }
    VizButtonColumnItemData{
        icon: "Presentation/TextToSpeech"
        toolTip: "Text To Speech"
        label: "Text To Speech"
        selected: false
        name: "TextToSpeechMenu"
        menuEnabled: rootStyle.showDemoFeatures && licenseManager.georbIS_Strategem
    }
    VizButtonColumnItemData{
        icon: "Presentation/TourIcon"
        toolTip: "Record Menu"
        label: "Record"
        selected: false
        name: "RecordMenu"
        menuEnabled: licenseManager.georbIS_Pro
    }
}
