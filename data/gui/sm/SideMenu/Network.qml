import QtQuick 2.1

RightPane{
    id: path

    objectName: "networkMenu"

    titleText: "Network"
    titleImageSource: rootStyle.getIconPath("Menu/network")

    ListModel{
        id: categories
        ListElement{name:"Publish"}
        ListElement{name:"Join"}
        ListElement{name:"Camera Properties"}
    }

    onSetMode: {
        if(mode=="Publish")
            NetworkPublishGUI.setActive(true);
        else if(mode=="Join")
            NetworkJoinGUI.setActive(true);
        else if(mode=="Camera Properties")
            ScreenShareGUI.setActive(true);
    }

}
