import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "analysisMenu"

    property string selectedOption: ""

    onOptionClicked:{
        //analysisLoader.unload()
        bottomLoader.unload()

        if(option === "Back"){
            menuHolder.loadMainMenu()
        }
        else if(option === "ElevationProfile"){
            if(!bottomLoader.load("ElevationProfileMenu.qml"))
            {
                removeHighlight();
            }
            else
            {
                disableListOption(option)
            }


            //menuHolder.minimized = true
        }
        else{

            if(! smpFileMenu.load(option + "Analysis.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
         }
    }

    VizButtonColumnItemData{
        icon:"Analysis/MeasureAreaNormal"
        toolTip: "Area"
        label: "Area"
        selected: false
        name: "Area"
    }

    VizButtonColumnItemData{
        icon:"Analysis/MeasureDistanceNormal"
        toolTip: "Distance"
        label: "Distance"
        selected: false
        name: "Distance"
    }

    VizButtonColumnItemData{
        icon:"Analysis/ElevationProfile"
        toolTip: "Elevation Profile"
        label: "Elevation Graph"
        selected: false
        name: "ElevationProfile"
    }

    VizButtonColumnItemData{
        icon:"Analysis/Height"
        toolTip: "Height"
        label: "Height"
        selected: false
        name: "Height"
    }

    VizButtonColumnItemData{
        icon:"Analysis/MeasureElevationDistance"
        toolTip: "Elevation Difference between Points"
        label: "Height Diff"
        selected: false
        name: "ElevationDiff"
    }

    VizButtonColumnItemData{
        icon:"Analysis/MinMax"
        toolTip: "Min/Max Height Measurement"
        label: "Min/Max Height"
        selected: false
        name: "MinMaxElev"
    }

    VizButtonColumnItemData{
        icon:"Analysis/MeasurePeriMeter"
        toolTip: "Perimeter of Selected Area"
        label: "Perimeter"
        selected: false
        name: "Perimeter"
    }

    VizButtonColumnItemData{
        icon:"Analysis/MeasureSlopeNormal"
        toolTip: "Slope"
        label: "Slope"
        selected: false
        name: "Slope"
    }

}
