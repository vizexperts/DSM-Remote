import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    position: "left"
    objectName: "leftMenuSource"

    onOptionClicked: {
        if(option == "Undo"){
            smpFileMenu.undo()
        }
        else if(option == "Redo"){
            smpFileMenu.redo()
        }
        else if(option == "Save"){
            smpFileMenu.save()
        }
        else if(option == "SaveAs"){
            smpFileMenu.saveAs()
        }
        else if(option == "Back"){
            menuHolder.minimized = true
        }
        else if(option == "Settings"){
            menuHolder.setSource("SettingsMenu.qml")
        }
        else if(option == "Screenshot"){
            smpFileMenu.load("Screenshot.qml");
        }
        else if(option == "Webcam"){
            WebCamGUI.setActive(true)
        }
        else if(option == "Presenter"){
            menuHolder.setSource("PresentationMenu.qml")
        }
        else if(option == "Weather"){
            menuHolder.setSource("Weather.qml")
        }
        else if(option == "ApplicationSettings"){
            smpFileMenu.load("ApplicationSettings.qml")
        }
    }

    VizButtonColumnItemData{
        icon: "LeftMenu/settings"
        toolTip: "Settings"
        label: "Settings"
        selected: false
        name: "Settings"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/Presentation"
        toolTip:  "Tour and Presentation options"
        label: "Presentation"
        selected: false
        name: "Presenter"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/weather"
        toolTip: "Environment Options"
        label: "Effects"
        selected: false
        name: "Weather"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/settings"
        toolTip: "Application Settings"
        label: "Application Settings"
        selected: false
        name: "ApplicationSettings"
    }
}
