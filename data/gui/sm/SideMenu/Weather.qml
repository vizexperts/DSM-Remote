import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import "../VizComponents"

Item{
    id: weatherSettings

    objectName: "smpWeatherMenu"

    width: mainLayout.implicitWidth + 30
    height: mainLayout.implicitHeight + closeButton.height + 30

    Component.onCompleted: menuHolder.connectWeatherMenu()

    // environment
    property alias sunMoonStartsEnabled: envCheckmark.checked
    property alias lightEnabled: lightCheckmark.checked
    signal toggleSunMoonStars(bool active)

    signal toggleLight(bool active)

    // clouds
    property alias cloudsEnabled: cloudsCheckmark.checked
    property alias cloudsDensity: cloudsSlider.value
    signal toggleCloudsEnable(bool active)
    signal setCloudsDensity(int density)

    onCloudsDensityChanged: setCloudsDensity(cloudsDensity)

    // precipitation
    property alias snowEnabled: snowCheckbox.checked
    property alias rainEnabled: rainCheckmark.checked
    signal toggleSnow(bool active)
    signal toggleRain(bool active)

    // fog
    property alias fogEnabled: fogCheckmark.checked
    property alias dustEnabled: dustCheckmark.checked
    property alias fogDensity: fogSlider.value
    signal toggleFog(bool active)
    signal toggleDust(bool active)
    signal setFogDensity(int density)

    onFogDensityChanged: setFogDensity(fogDensity)

    Rectangle{
        id: boundary
        anchors.margins: 5
        anchors.fill : parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
        border.width: 2
        radius: 2
        border.color:"DeepSkyBlue"
    }

    VizLabel{
        id: titleLabel
        width: parent.width - 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: rootStyle.colorWhenDefault
        height: closeButton.height
        text: "Environment"
    }

    VizButton{
        id: closeButton
        anchors.right: boundary.right
        anchors.verticalCenter: titleLabel.verticalCenter
        backGroundVisible: false
        iconSource: rootStyle.getIconPath("backLeft")
        onClicked: menuHolder.setSource("ProLeftPanel.qml")
    }

    GridLayout{
        id: mainLayout
        anchors.fill: boundary
        anchors.margins: 2
        anchors.topMargin: closeButton.height
        columns: 2

        VizLabel{ text: "Sun Moon Stars"}

        VizCheckBox{
            id: envCheckmark
            onClicked: toggleSunMoonStars(checked)
        }

        VizLabel{ text: "Light on Terrain"  }

        VizCheckBox{
            id: lightCheckmark
            enabled:sunMoonStartsEnabled
            onClicked: toggleLight(checked)
        }

        VizLabel{ text: "Clouds" }

        VizCheckBox{
            id: cloudsCheckmark
            onClicked: toggleCloudsEnable(checked)
        }

        VizLabel{
            text: "Clouds Density"
            visible: cloudsEnabled
        }
        VizSlider{
            id: cloudsSlider
            visible: cloudsEnabled
            minimumValue:  0
            maximumValue: 2
            labelVisible: false
            Layout.fillWidth: true
            value: 0
        }

        VizLabel{  text: "Snow"  }

        VizCheckBox{
            id: snowCheckbox
            onClicked: {
                toggleSnow(checked)
                if(checked)
                    rainEnabled = false
            }
        }

        VizLabel{ text: "Rain" }

        VizCheckBox{
            id: rainCheckmark
            onClicked: {
                toggleRain(checked)
                if(checked)
                    snowEnabled = false
            }
        }

        VizLabel{
            visible: true
            text: "Fog"
        }

        VizCheckBox{
            id: fogCheckmark
            visible: true
            onClicked: {
                toggleFog(checked)
                if(checked)
                    dustEnabled = false
            }
        }
        VizLabel{
            visible: rootStyle.showDemoFeatures
            text: "Dust"
        }

        VizCheckBox{
            id: dustCheckmark
            visible: rootStyle.showDemoFeatures
            onClicked:{
                toggleDust(checked)
                if(checked)
                    fogEnabled = false
            }
        }
        VizLabel{
            text: fogEnabled ? "Fog Density": "Dust Density"
            visible: (fogEnabled || dustEnabled)
        }
        VizSlider{
            id: fogSlider
            visible: (fogEnabled || dustEnabled)
            minimumValue: 0
            maximumValue: 99
            labelVisible: false
            value: 50
            Layout.fillWidth: true
        }
    }

    transitions: Transition{
        AnchorAnimation{
            duration: 100
        }
    }
}
