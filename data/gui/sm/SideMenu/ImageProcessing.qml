import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "imageProcessingTab"

    onOptionClicked:{

        //analysisLoader.unload()
        bottomLoader.unload()

        if(option === "Back"){
            menuHolder.loadMainMenu()
        }
        else if(option === "Reprojection"){
            smpFileMenu.load("Reprojection.qml")
        }
        else if(option === "ImageSubset"){
            smpFileMenu.load("ImageSubset.qml")
        }

        else{
            smpFileMenu.load(option + ".qml")
        }
    }

    VizButtonColumnItemData{
        icon: "ImageProcessing/panSharpen"
        toolTip: "Pan Sharpening"
        label: "Pan Sharpen"
        selected: false
        name: "PanSharpening"
    }
    VizButtonColumnItemData{
        icon: "ImageProcessing/chang"
        toolTip: "Change Detection"
        label: "Change\ndetection"
        selected: false
        name: "ChangeDetection"
    }
    VizButtonColumnItemData{
        icon: "ImageProcessing/cluster"
        toolTip: "k-Means"
        label: "K-Means"
        selected: false
        name: "KMeans"
    }
    //        VizButtonColumnItemData{
    //            icon: "ImageProcessing/change"
    //            toolTip: "SVM"
    //            label: "SVM Analysis"
    //            selected: false
    //            name: "SVM"
    //        }

    VizButtonColumnItemData{
        icon: "ImageProcessing/pan"
        toolTip: "Reprojection"
        label: "Re-projection"
        selected: false
        name: "Reprojection"
    }
    VizButtonColumnItemData{
        icon: "ImageProcessing/subset"
        toolTip: "Image subset"
        label: "Image Subset"
        selected: false
        name: "ImageSubset"
    }
}
