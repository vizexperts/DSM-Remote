import QtQuick 2.1
import "../VizComponents"

RightPane{
    id: landmarks

    objectName: "smpLandmarksMenu"

    signal menuDestroyed()
    signal addNewLandmarkType()
    //signal addLandmarkDirectory()

    onAddNewLandmarkType: {
        if(smpFileMenu.load("AddLandmarkTypePopUp.qml"))
        {
          disableLandmark()
        }


    }

    /*onAddLandmarkDirectory: {
        if(smpFileMenu.load("AddLandmarkMenuPopUp.qml"))
        {
            disableLandmark()
        }
    }*/
    Connections{
        target: root
        onNowRemoveHighLight:enableLandmark();
    }


    function disableLandmark()
    {
        enabled=false;
    }

    function enableLandmark()
    {
        enabled=true;
    }

    Component.onDestruction: menuDestroyed()

    titleText: "3D Models"
    titleImageSource: rootStyle.getIconPath("Menu/landmark")

    categoriesEnabled: false

    VizTreeView{
        id: landmarkDirTreeView
        objectName: "landmarkDirTreeView"
        x:5
        implicitWidth: parent.width
        anchors.left: parent.left
        anchors.right: parent.right
        y: startY
        height: (addLandmarkTypeButton.y - y - 20)
        model:(typeof(landmarkListModel) !== "undefined") ? landmarkListModel : "undefined"
        VizTreeViewColumn{
            role: "isDir"
            delegate: Image {
                source: styleData.value ? rootStyle.getIconPath("folder") : rootStyle.getIconPath("model")
                width: rootStyle.buttonIconSize
                height: width
            }
        }

        VizTreeViewColumn{
            role: "name"
            delegate: VizLabel {
                id: nameDelegate
                text: styleData.value
                textColor: styleData.textColor
            }
        }
    }



    /*VizButton{
        id: addDirectoryButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        text: "   Set Models Directory   "
        onClicked:{
            addLandmarkDirectory()

        }
    }*/
    VizButton{
        id:addLandmarkTypeButton
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        //text: "Add New Models"
        iconSource: rootStyle.getIconPath("add")
        onClicked: {
            addNewLandmarkType()
        }
    }

}
