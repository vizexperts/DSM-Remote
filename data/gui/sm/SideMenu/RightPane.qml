import QtQuick 2.1
import "../VizComponents"

Item{
    id: pane
    height: root.height

    width: 400*rootStyle.uiScale

    property int minimumWidth: 400*rootStyle.uiScale
    property int maximumWidth: root.width/2

    Component.onCompleted: menuHolder.loaded(objectName)
    Component.onDestruction: menuHolder.loaded("")

    signal filter(string keyWord)
    signal setMode(string mode)

    property alias titleText: title.text
    property string titleImageSource
    property int startY: lineImage.y + 20
    property bool categoriesEnabled: true
    property bool searchVisible: true
    property int resultsListCount: 0

    TextEdit {
        id: title
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 37
        anchors.topMargin: 37
        width: 109
        height: 43
        color: "#ffffff"
        font.family: rootStyle.fontFamily
        readOnly: true
        font.pixelSize: 37
    }

    Image {
        id: backImage
        width: 40;
        height: 40
        rotation: 180
        anchors.top: parent.top
        anchors.left: parent.left
        fillMode: Image.PreserveAspectFit
        source: rootStyle.getIconPath("backLeft")
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(titleText == "3D Models"){
                    menuHolder.setSource("Features.qml")
                }
                else{
                        menuHolder.setSource("ProRightPanel.qml")
                }
            }
        }
    }

    Image{
        source: titleImageSource;
        anchors.verticalCenter: title.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 50
        fillMode: Image.PreserveAspectFit
        width: 70; height: 70
    }


   TextEdit {
        id: searchLabel
        anchors.left: title.left
        anchors.top: title.bottom
        anchors.topMargin: 30;
        width: 116
        height: 26
        color: "#ffffff"
        text: qsTr("Search")
        font.family: rootStyle.fontFamily
        readOnly: true
        font.pixelSize: 20
        visible: searchVisible
    }

    Rectangle {
        id: searchBox
        anchors.top: searchLabel.bottom
        anchors.left: searchLabel.left
        anchors.right: searchIcon.left
        anchors.rightMargin: 10
        anchors.topMargin: 10
//        width: 305
        height: 37
        color: "#ffffff"
        visible: searchVisible
        LineEdit {
            id: text_edit1
            anchors.fill:parent
            clip: true
            font.pointSize: 15
            autoScroll: true
            onAccepted: focus = false
            onTextChanged: filter(text)
            readOnly: false
            font.family: rootStyle.fontFamily
            color: "black"
        }
    }

    Image {
        id: searchIcon
        anchors.verticalCenter: searchBox.verticalCenter; anchors.right: parent.right
        anchors.rightMargin: 37
        width: 45; height: 45
        fillMode: Image.PreserveAspectFit
        source: rootStyle.getIconPath("search")
        visible: searchVisible
    }
    ListView{
        id:categoryList
        enabled: categoriesEnabled
        width: parent.width
        anchors.top: searchBox.bottom
        anchors.topMargin: 30
        height: contentHeight
        currentIndex: -1
        highlight: Rectangle{
            width: pane.width
            height: 70
            color: "#64ffffff"
        }
        contentHeight: 70
        highlightFollowsCurrentItem: true
        boundsBehavior: Flickable.StopAtBounds
        model: (categoriesEnabled) ? categories : null
        delegate: Item{
            id: categoryDelegate
            height: category.height
            width:400
            TextEdit{
                id: category
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 37
                anchors.right: parent.right
                anchors.rightMargin: 53
                height: 50
                color: "#ffffff"
                text: name
                font.family: rootStyle.fontFamily
                readOnly: true
                font.pixelSize: 20
                TextEdit {
                    id: noOfResults
                    visible: (categoryDelegate.ListView.view.currentIndex === index)
                    anchors.right: parent.right
                    anchors.top: category.top
                    color: "#ffffff"
                    font.pixelSize: 20
                    text: resultsListCount
                    horizontalAlignment: TextEdit.AlignRight
                    font.family: rootStyle.fontFamily
                    readOnly: true
                }
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    setMode(name)
                    categoryList.currentIndex = index
                }
            }
        }
    }
    Image {
        id: lineImage
        height: 10
        anchors.left: categoryList.left
        anchors.leftMargin: 30
        anchors.right: parent.right
        anchors.rightMargin: 30
        anchors.top: (categoriesEnabled)? categoryList.bottom : searchBox.bottom
        anchors.topMargin : 10
        source: rootStyle.getIconPath("seperator")
    }

    MouseArea{
        id: resizeArea
        height: parent.height
        anchors.left: parent.left
        width: 10
        hoverEnabled: true
        property int mx
        states: State {
            name: "entered"
            when: resizeArea.containsMouse
            PropertyChanges { target: root; cursorShape: Qt.SizeHorCursor }
        }
        function handleMouse(mouse){
            if(mouse.buttons & Qt.LeftButton){
                parent.width = Math.min(Math.max(minimumWidth, parent.width + (mx-mouse.x)),maximumWidth)
            }
        }
        onPositionChanged: handleMouse(mouse)
        onPressed: {
            mx = mouse.x
        }
    }
}
