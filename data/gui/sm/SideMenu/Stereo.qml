import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

Item{
    id: stereo
    objectName: "smpStereoMenu"

    width: mainLayout.implicitWidth + 30
    height: mainLayout.implicitHeight + closeButton.height + 30

    property alias stereoEnabled: checkBox.checked
    property alias toggleEyes:toggleEyesCheckBox.checked
    property alias screenDistance: screenDistance.value
    property alias screenWidth: screenWidth.value
    property alias screenHeight: screenHeight.value
    property alias eyeSeperation: eyeSeparation.value
    property int mode: 0

    onModeChanged: {
        for(var i = 0; i < modeListModel.count; i++){
            if(modeListModel.get(i).uID == mode){
                modeList.value = modeListModel.get(i).name
                break
            }
        }
    }

    signal setMode(int mode)
    signal save()
    signal setStereo(bool state)
    signal setToggeEyes(bool state)
    signal updateEyeSeparation(double value)
    signal updateScreenDistance(double value)
    signal updateScreenHeight(double value)
    signal updateScreenWidth(double value)


    Component.onCompleted: {
        menuHolder.connectStereoMenu()
    }

    Rectangle{
        id: boundary
        anchors.margins: 5
        anchors.fill : parent
        color: rootStyle.colorWhenDefault
        opacity: 0.7
        border.width: 2
        radius: 2
        border.color:"DeepSkyBlue"
    }

    VizLabel{
        id: titleLabel
        width: parent.width - 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: boundary.top
        anchors.topMargin: 5
        color: rootStyle.colorWhenDefault
        height: closeButton.height
        text: "Stereo Settings"
    }

    VizButton{
        id: closeButton
        anchors.right: boundary.right
        anchors.verticalCenter: titleLabel.verticalCenter
        backGroundVisible: false
        iconSource: rootStyle.getIconPath("backLeft")
        onClicked: menuHolder.setSource("SettingsMenu.qml")
    }

    GridLayout{
        id: mainLayout
        anchors.fill: boundary
        anchors.margins: 2
        anchors.topMargin: closeButton.height
        columns: 2

        VizLabel{ text: "Stereo Enabled" }

        VizCheckBox{
            id: checkBox
            text: ""
            onClicked:{
                setStereo(checked)
                if(checked === false)
                    modeList.state = "INACTIVE"
            }
        }

        VizLabel{ text: "Swap Eyes" }

        VizCheckBox{
            id: toggleEyesCheckBox
            text: ""
            enabled: stereoEnabled
            onClicked: setToggeEyes(checked)
        }

        VizLabel { text: "Stereo Mode" }

        VizComboBox{
            id: modeList
            listModel: modeListModel
            value: modeListModel.get(mode).name
            enabled: stereoEnabled
            Layout.fillWidth: true
            onSelectOption:{
                stereo.mode = uID
                setMode(mode)
            }
        }

        VizLabel { text: "Eye Separation (cm)" }

        VizSpinBox{
            id: eyeSeparation;
            minimumValue: 0
            maximumValue: 10
            singlestep: 0.01
            validator: DoubleValidator{
                top: eyeSeparation.maximumValue
                bottom: eyeSeparation.minimumValue
                decimals: 2
                notation: DoubleValidator.StandardNotation;
            }
            onValueChanged: updateEyeSeparation(value);
            enabled: stereoEnabled
        }

        VizLabel { text: "Screen Distance (m)" }

        VizSpinBox {
            id: screenDistance
            minimumValue: 0;
            maximumValue: 100;
            singlestep: 0.01;
            validator: DoubleValidator{
                top: screenDistance.maximumValue
                bottom: screenDistance.minimumValue
                decimals: 2
                notation: DoubleValidator.StandardNotation;
            }
            onValueChanged: updateScreenDistance(value);
            enabled: stereoEnabled
        }

        VizLabel { text: "Screen Width (m)" }

        VizSpinBox{
            id: screenWidth;
            minimumValue: 0;
            maximumValue: 100;
            singlestep: 0.01;
            validator: DoubleValidator{
                top: screenWidth.maximumValue
                bottom: screenWidth.minimumValue
                decimals: 2
                notation: DoubleValidator.StandardNotation;
            }
            onValueChanged: updateScreenWidth(value);
            enabled: stereoEnabled
        }

        VizLabel { text: "Screen Height (m)" }

        VizSpinBox{
            id: screenHeight
            minimumValue: 0;
            maximumValue: 100;
            singlestep: 0.01;
            validator: DoubleValidator{
                top: screenHeight.maximumValue
                bottom: screenHeight.minimumValue
                decimals: 2
                notation: DoubleValidator.StandardNotation;
            }
            onValueChanged: updateScreenHeight(value);
            enabled: stereoEnabled
        }

        Item{ /*Empty placeholfer item on grid*/ }

        VizButton{
            text: "   Save   "
            Layout.alignment: Qt.AlignHCenter
            onClicked:save();
        }

        ListModel{
            id: modeListModel
            ListElement{name:"Quad Buffer"; uID : "0"}
            ListElement{name:"Anaglyphic"; uID : "1"}
            ListElement{name:"Horizontal Split"; uID : "2"}
            ListElement{name:"Vertical Split"; uID : "3"}
            ListElement{name:"Horizontal Interlace"; uID : "6"}
            ListElement{name:"Vertical Interlace"; uID : "7"}
        }
    }
}
