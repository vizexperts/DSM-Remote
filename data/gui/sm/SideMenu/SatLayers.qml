import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "AddSatData"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.setSource("AddData.qml")
        }
        else if(option === "Raster"){
            menuHolder.setSource("RasterLayers.qml")
        }
        else if(option === "Elevation"){
            menuHolder.setSource("ElevationLayers.qml")
        }
        else if(option === "Vector"){
            menuHolder.setSource("VectorLayers.qml")
        }
    }

    VizButtonColumnItemData{
        icon: "ElementListIcons/raster"
        toolTip: "Add raster layer"
        label: "Raster"
        selected: false
        name: "Raster"
    }
    VizButtonColumnItemData{
        icon: "ElementListIcons/elevation"
        toolTip: "Add raster layer"
        label: "Elevation"
        selected: false
        name: "Elevation"
    }
    VizButtonColumnItemData{
        icon: "ElementListIcons/shapeFile"
        toolTip: "Add raster layer"
        label: "Vector"
        selected: false
        name: "Vector"
    }
}
