import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "settingsMenu"
    position: "left"

    onOptionClicked: {
        if(option == "Back"){
            menuHolder.loadMainMenu()
        }
        else if(option == "DatabaseSettings"){
            smpFileMenu.load("DatabaseSettings.qml")
        }
        else if(option == "GeorbISSettings"){
            smpFileMenu.load("GeorbISSettings.qml")
        }
        else if(option == "UACSettings"){
            smpFileMenu.load("UACSettings.qml")
        }
        else if(option == "ModelsDirectory"){
            smpFileMenu.load("AddLandmarkMenuPopUp.qml")
        }
        else{
            menuHolder.setSource(option + ".qml")
        }
    }

    VizButtonColumnItemData{
        icon: "LeftMenu/camera"
        toolTip: "View Options"
        label: "View"
        selected: false
        name: "View"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/database"
        toolTip: "Database Options"
        label: "Database"
        selected: false
        name: "DatabaseSettings"
        menuEnabled: licenseManager.georbIS_Pro
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/database"
        toolTip: "georbIS Options"
        label: "GeorbIS Setting"
        selected: false
        name: "GeorbISSettings"
        menuEnabled: licenseManager.georbIS_Pro
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/stereo"
        toolTip: "Stereo Options"
        label: "Stereo"
        selected: false
        name: "Stereo"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/ui_settings"
        toolTip: "UI Options"
        label: "GUI"
        selected: false
        name: "GuiOptions"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/landmark"
        toolTip: "Set 3D models directory"
        label: "Models Source"
        selected: false
        name: "ModelsDirectory"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/uacSettings"
        toolTip: "User Account Control"
        label: "UAC Settings"
        selected: false
        name: "UACSettings"
        menuEnabled: licenseManager.georbIS_Pro
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/templates"
        toolTip: "Template Settings"
        label: "Templates"
        selected: false
        name: "Templates"
    }
    VizButtonColumnItemData{
        icon: "LeftMenu/ClearCache"
        toolTip: "Clear Cache Options"
        label: "Cache"
        selected: false
        name: "Cache"
        menuEnabled: licenseManager.georbIS_Strategem
    }
    VizButtonColumnItemData{
        icon: "Menu/layers"
        toolTip: "Project Options"
        label: "Project"
        name: "ProjectOptions"
        menuEnabled: licenseManager.georbIS_Strategem
    }
}
