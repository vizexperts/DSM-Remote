import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "lineFeaturesMenu"

    signal setFeatureEnable(string type)

    property int selectedIndex : -1

    onOptionClicked:{
        if(option === "Back") {
            menuHolder.setSource("Features.qml")
        }
        else{
            if(selectedIndex != currentIndex){
                if(selectedIndex != -1){
                    listModel.get(selectedIndex).selected = false
                }
                listModel.get(currentIndex).selected = true
                selectedIndex = currentIndex
                setFeatureEnable(option)
            }
            else {
                listModel.get(currentIndex).selected = false
                selectedIndex = -1
                setFeatureEnable("")
            }
        }
    }

    function resetLineFeatures()
    {
        listModel.get(currentIndex).selected = false
        selectedIndex = -1
        setFeatureEnable("")
    }

    Component.onDestruction: setFeatureEnable("")

    VizButtonColumnItemData{
        icon:"Features/DSM_Road"
        toolTip: "Add Roads"
        label: "Roads"
        selected: false
        name: "Roads"
    }
    VizButtonColumnItemData{
        icon: "Features/DSM_River"
        toolTip: "Add Rivers"
        label: "Rivers"
        selected: false
        name: "Rivers"
    }
    VizButtonColumnItemData{
        icon:  "Features/DSM_Pond"
        toolTip: "Add Canals"
        label: "Canals"
        selected: false
        name: "Canals"
    }
}
