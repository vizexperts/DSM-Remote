import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "customLayerTab"


    onOptionClicked:{
        if(option === "Back"){
            menuHolder.setSource("AddData.qml")
        }
        else if(option === "GPS"){
            if(!smpFileMenu.load("AddGps.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
    }

    VizButtonColumnItemData{
        icon: "AddData/addGps"
        toolTip: "Add GPS Data"
        label: "GPS Data"
        selected: false
        name: "GPS"
    }
}
