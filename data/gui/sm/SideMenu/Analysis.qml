import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "analysisMenu"
    onOptionClicked:{
        //analysisLoader.unload()
        bottomLoader.unload()

        if(option === "Back"){
            menuHolder.loadMainMenu()
        }
        else{
            if(!smpFileMenu.load(option + "Analysis.qml"))
            {
                removeHighlight();
            }
            else
            {
                disableListOption(option)
            }
        }
    }

    VizButtonColumnItemData{
        icon:"Analysis/AlmanacNormal"
        toolTip: "Almanac Computation"
        label: "Almanac"
        selected: false
        name: "Almanac"
    }
    VizButtonColumnItemData{
        icon:"Analysis/Contours"
        toolTip: "Contours Generation"
        label: "Contours"
        selected: false
        name: "Contours"
    }
    VizButtonColumnItemData{
        icon:"Analysis/CutAndFill"
        toolTip: "Cut & Fill"
        label: "Cut & Fill"
        selected: false
        name: "CutAndFill"
    }
    VizButtonColumnItemData{
        icon:"Analysis/CrestClearance"
        toolTip: "Crest Clearance Analysis"
        label: "Firing Range"
        selected: false
        name: "CrestClearance"
    }
    VizButtonColumnItemData{
        icon:"Analysis/Flooding"
        toolTip: "Flooding"
        label: "Flood"
        selected: false
        name: "Flooding"
    }
    VizButtonColumnItemData{
        icon:"Analysis/Hillshade"
        toolTip: "Hillshade"
        label: "Hillshade"
        selected: false
        name: "Hillshade"
    }
    VizButtonColumnItemData{
        icon:"Analysis/LOSNormal"
        toolTip: "Line of Sight"
        label: "LOS"
        selected: false
        name: "LOS"
    }

    VizButtonColumnItemData{
        icon:"Analysis/RadioLOS"
        toolTip: "Radio Line of Sight"
        label: "Radio LOS"
        selected: false
        name: "RadioLOS"
    }
    VizButtonColumnItemData{
        icon:"Analysis/SlopeAspectNormal"
        toolTip: "Slope/Aspect Calculation"
        label: "Slope/Aspect"
        selected: false
        name: "SlopeAspect"
    }
    VizButtonColumnItemData{
        icon:"Analysis/spatial_query"
        toolTip: "Spatial Filter"
        label: "Spatial Filter"
        selected: false
        name: "Query"
    }
    VizButtonColumnItemData{
        icon:"Analysis/SteepestPath"
        toolTip: "Steepest Path"
        label: "Steepest Path"
        selected: false
        name: "SteepestPath"
    }
    VizButtonColumnItemData{
        icon:"Analysis/ColorCodeEle"
        toolTip: "Color Coded Elevation"
        label: "Terrain Profile"
        selected: false
        name: "ColorElevation"
    }
    VizButtonColumnItemData{
        icon:"Analysis/ViewshedAnalysisHover"
        toolTip: "Viewshed Analysis"
        label: "Viewshed"
        selected: false
        name: "Viewshed"
    }
    VizButtonColumnItemData{
        icon:"Analysis/Watershed"
        toolTip: "Watershed"
        label: "Watershed"
        selected: false
        name: "Watershed"
        menuEnabled: rootStyle.showDemoFeatures
    }
    VizButtonColumnItemData{
        icon:"Analysis/gun"
        toolTip: "Weapon Range"
        label: "Weapon Range"
        selected: false
        name: "GunRange"
    }
}
