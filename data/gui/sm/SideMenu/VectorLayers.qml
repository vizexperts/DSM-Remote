import QtQuick 2.1
import "../VizComponents"

VizButtonColumn{
    objectName: "AddVectorLayers"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.setSource("SatLayers.qml")
        }
        else if(option === "File"){
            if(!smpFileMenu.load("AddVectorLayerPopUp.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else if(option === "DGN"){
            if(!smpFileMenu.load("AddDGNLayerPopUp.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else if(option === "ExcelCSV"){
            if(!smpFileMenu.load("AddExcelPopUp.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }

        }
        else if(option === "WFS"){
            if(!smpFileMenu.load("AddWFSLayerPopUp.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else if(option == "Database"){
            menuHolder.setSource("VectorDBLayers.qml")
        }
    }

    VizButtonColumnItemData{
        icon: "Menu/file"
        toolTip: "Add local layer"
        label: "File"
        selected: false
        name: "File"
    }
    VizButtonColumnItemData{
        icon: "ElementListIcons/shapeFile"
        toolTip: "Add dgn layer"
        label: "DGN"
        selected: false
        name: "DGN"
        menuEnabled: licenseManager.georbIS_Strategem
    }
    VizButtonColumnItemData{
        icon: "AddData/addExcel"
        toolTip: "Add Exel/CSV Data"
        label: "Excel"
        selected: false
        name: "ExcelCSV"
        menuEnabled: licenseManager.georbIS_Intel
    }
    VizButtonColumnItemData{
        icon: "Menu/wms-wfs"
        toolTip: "Add WFS layer"
        label: "WFS"
        selected: false
        name: "WFS"
    }
}
