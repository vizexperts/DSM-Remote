import QtQuick 2.1
import "../VizComponents"

VizButtonColumn {
    objectName: "plannerMenu"

    onOptionClicked:{
        if(option === "Back"){
            menuHolder.minimized = true
        }
        /*else if(option === "ElementsList"){
            menuHolder.minimized = true
            elementList.minimized = !elementList.minimized
        }*/
        else if(statusBar.applicationBusy){
            return
        }
        else if(option === "Event"){
            menuHolder.minimized = true
            timelineLoader.minimized = false
        }
        else if (option == "Animation")
        {
            if(!smpFileMenu.load("AnimationPopup.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else if(option ==="ElementClassification"){
            if(!smpFileMenu.load("ElementClassification.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }

        }
        else if(option === "Version"){
            if(!smpFileMenu.load("VersionTab.qml"))
            {
                removeHighlight();

            }
            else
            {
                disableListOption(option)
            }
        }
        else {
            menuHolder.setSource(option + ".qml")
        }
    }

    VizButtonColumnItemData{
        icon: "Menu/layers"
        toolTip: "Add Data Layers"
        label: "Add Layers"
        name: "AddData"
    }
    VizButtonColumnItemData{
        icon: "Menu/layers"
        toolTip: "Add Data Layers"
        label: "Export Data"
        name: "ExportData"
    }
    VizButtonColumnItemData{
        icon: "Menu/features"
        toolTip: "Add Landmarks"
        label: "Features"
        name: "Features"
    }
    VizButtonColumnItemData{
        icon: "Menu/path"
        toolTip: "Add Marking Features"
        label: "Markings"
        name: "MarkingFeatures"
    }
    VizButtonColumnItemData{
        icon: "Menu/analysis"
        toolTip: "Analysis Options"
        label: "Analysis"
        name: "Analysis"
        menuEnabled: licenseManager.georbIS_Analyst
    }
    VizButtonColumnItemData{
        icon: "tk/imageAnalysis"
        toolTip: "Image Processing"
        label: "Imagyst"
        name: "ImageProcessing"
        menuEnabled: licenseManager.georbIS_Processor
    }
    VizButtonColumnItemData{
        icon: "Menu/measure"
        toolTip: "Measurements"
        label: "Measure"
        name: "Measurements"
    }
    VizButtonColumnItemData{
        icon: "Menu/deploy"
        toolTip: "Allocated Resources"
        label: "Resources"
        name: "Available"
        menuEnabled: licenseManager.georbIS_Strategem
    }
    VizButtonColumnItemData{
        icon: "Menu/event"
        toolTip: "View Timeline"
        label: "Event Mode"
        name: "Event"
        //menuEnabled: licenseManager.georbIS_Pro
    }
//    VizButtonColumnItemData{
//        icon: "Menu/event"
//        toolTip: "Animation"
//        label: "Add Animation"
//        name: "AddAnimation"

//        //menuEnabled: licenseManager.georbIS_Pro
//    }
    VizButtonColumnItemData{
        icon: "Menu/event"
        toolTip: "View Animation"
        label: "Raster Animation"
        name: "Animation"
        //menuEnabled: licenseManager.georbIS_Pro
    }
    VizButtonColumnItemData{
        icon: "Menu/network"
        toolTip: "Networking Options"
        label: "Networking"
        name: "Network"
        menuEnabled: licenseManager.georbIS_Pro
    }
    VizButtonColumnItemData{
        icon: "about"
        toolTip: "Version info about software"
        label: "About"
        selected: false
        name: "Version"
    }
}
