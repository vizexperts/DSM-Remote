import QtQuick 2.1
import "../VizComponents"

RightPane{
    id: availableMenu

    objectName: "smpAvailableResourceMenu"

    titleText: "Available"
    titleImageSource: rootStyle.getIconPath("Menu/deploy")

    //! signals for available resources managemant
    signal selectResource(string uID)
    signal deselectResource(string uID)
    signal deleteResource(string uID)

    //! Signals for Group Managemant
    signal selectGroup(string uID)
    signal deselectGroup(string uID)
    signal changeGroupName(string uID, string newName)
    signal createNewGroup()
    property alias resourcesButtonChecked: resourcesButton.checked;
    Component.onDestruction: smpFileMenu.unloadAll()

    property string selectedResource : ""
    property string selectedGroupUID : ""

    categoriesEnabled: false

    ListModel{
        id: categories
        ListElement{name:"Military Symbols"}
    }
    function unCheckedButton()
    {
        resourcesButtonChecked=false
    }
    Connections{
        target: root
        onNowRemoveHighLight:unCheckedButton();
    }

   Component {
        id: iconItem
        FocusScope{
            width: childrenRect.width; height: childrenRect.height
            x:childrenRect.x; y: childrenRect.y


            Rectangle {
                id: item1
                width: 400; height: 70
                opacity: (allocated == deployed) ? 0.7 : 1.0
                property string fullName: affiliation+"/"+category+"/"+type
                color: (selectedResource == fullName)? "#64ffffff" : "transparent"

                MouseArea{
                    anchors.fill: parent;
                    enabled: (allocated != deployed)
                    onClicked: {
                        if(selectedResource == item1.fullName){
                            selectedResource = ""
                            deselectResource(item1.fullName)
                        }
                        else{
                            selectResource(item1.fullName)
                            selectedResource = item1.fullName
                        }
                    }
                }

                Image {
                    id: icon
                    anchors.left: parent.left; anchors.verticalCenter: parent.verticalCenter
                    anchors.leftMargin: 37
                    sourceSize.width: 40
                    sourceSize.height: 40
                    source: path
                    Rectangle{
                        width: parent.width+10
                        height: width
                        anchors.centerIn: parent
                        color:"transparent"
                        border.width: 1
                        border.color:/* deployed ? "red" :*/ "white"
                    }
                }

                Text{
                    anchors.right: parent.right
                    anchors.rightMargin: 40
                    anchors.verticalCenter: deleteButton.verticalCenter
                    text: (allocated - deployed)+"/"+allocated
                    color: "white"
                    font.pixelSize: 15
                }

                Image {
                    id: deleteButton
                    source: rootStyle.getIconPath("delete")
                    visible: false
                    height: 30
                    width: 30
                    anchors.right: parent.right
                    anchors.rightMargin: 50
                    anchors.top: parent.top
                    anchors.topMargin: 20
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            deleteResource(fullName)
                        }
                    }
                }
                Text{
                    id: resourceName
                    anchors.left: icon.right;
                    anchors.top:  icon.top
                    anchors.leftMargin: 10;
                    horizontalAlignment: TextEdit.AlignLeft
                    anchors.topMargin: 5
                    color: "#ffffff";
                    font.pixelSize: 18
                    text:type
                    font.family: rootStyle.fontFamily;
                }
                Text{
                    id: resourceType
                    anchors.left: icon.right
                    anchors.top: resourceName.bottom
                    anchors.topMargin: 5
                    anchors.leftMargin: 10
                    width: 200
                    height: 15
                    color: "#ffffff"
                    font.pixelSize: 14
                    text: affiliation + " - " + category
                    font.family: rootStyle.fontFamily
                }
            }
        }
    }


    ListView {
        id: resultsList;
        anchors.right: parent.right;
        anchors.left: parent.left;
        y:  startY
        height: resourcesButton.y - y
        clip:true
        currentIndex: -1
        boundsBehavior: Flickable.StopAtBounds
        model: (typeof(resourceListModel) != "undefined") ? resourceListModel : "undefined"
        delegate: iconItem
    }

    VizButton{
        id: resourcesButton
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Allocate Resources"
        checkable: true
        visible: (user_type === usertype_INSTRUCTOR)
        onCheckedChanged: {
            if(checked){
                if(!smpFileMenu.load("Resources.qml"))
                {
                    checked=false;
                }
            }
            else{
                smpFileMenu.unloadAll()
            }
        }
    }
}
