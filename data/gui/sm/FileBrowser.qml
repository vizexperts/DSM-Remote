import QtQuick 2.1
import QtQuick.Dialogs 1.0

FileDialog{

    id: fileBrowser
    
    function openBrowser(caption, filters){
        title = caption
        nameFilters = filters
        open()
    }
    
    function openBrowserMultipleFile(caption,filters,fileSelect)
    {
        title = caption
        nameFilters = filters
        selectMultiple = fileSelect
        open()
    }

    function openBrowserFolder(caption,filters,folderSelect)
    {
        title = caption
        nameFilters = filters
        selectFolder = folderSelect
        open()

    }

    width: 100
    height: 62
}
