import QtQuick 2.1
import "../components"
import "VizComponents"

Item {
    id: rasterEnhancementChanger

    objectName: "rasterEnhancementChangerObject"

    width: 250
    height: 260

    property bool rasterValid: false

    // No Enhancement
    property alias noneEnabled: noneCheckmark.checked

    // Min Max stretching
    property alias minmaxEnabled: minmaxCheckmark.checked

    //Standard Deviation Stretching
    property alias sdEnabled: sdCheckmark.checked

    //Histogram Equalization
    property alias histeqEnabled: histeqCheckmark.checked

    Component.onCompleted: {
        menuHolder.loaded("RasterEnhancementChanger")
		if(!rasterValid){
			menuHolder.minimized = true
		}
    }

	// Collaboration
	signal handleApplyButton()
	signal handleCancelButton()

	
    VizLabel{
        id: close
        width: parent.height - 30
        height: 35
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 9
        color: "transparent"
        text: "Enhancement Type"

        Rectangle{

            height: 30
            width: 30
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5
            color: "transparent"

            Image {
                id: closeIcon
                source: rootStyle.getIconPath("close")
                anchors.fill: parent
				anchors.right: parent.right
				anchors.top : parent.top				
            }

            MouseArea {
                anchors.fill: parent;
                onClicked:  {
                    menuHolder.minimized = true
					handleCancelButton()
                }
            }
        }
    }


    Rectangle{
        id: boundary
        anchors.margins: 10
        anchors.fill: parent
        color: "transparent"
        border.width: 2
        radius: 2
        border.color: "white"
    }

    Item{
        anchors.fill: boundary
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;

        VizCheckBox{
            id: noneCheckmark
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 45
            checked: true
            onClicked: {
                if(checked)
                {
                    minmaxEnabled = false
                    sdEnabled = false
                    histeqEnabled = false
                }
            }

        }

        VizLabel{
            id: noneLabel
            anchors.left: noneCheckmark.right
            anchors.leftMargin: 2
            anchors.top: parent.top
            anchors.topMargin: 45
            text: "None"
        }

        VizCheckBox{
            id: minmaxCheckmark
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: noneCheckmark.bottom
            anchors.topMargin: 2
            onClicked: {
                if(checked)
                {
                    noneEnabled = false
                    sdEnabled = false
                    histeqEnabled = false
                }
                else
                {
                    noneEnabled = true
                }

            }

        }

        VizLabel{
            id: minmaxLabel
            anchors.left: minmaxCheckmark.right
            anchors.leftMargin: 2
            anchors.top: noneCheckmark.bottom
            anchors.topMargin: 2
            text: "Min Max"
        }

       /* VizLabel{
            id: minLabel
            anchors.left: minmaxCheckmark.right
            anchors.leftMargin: 2
            anchors.top: minmaxCheckmark.bottom
            anchors.topMargin: 2
            text: "Min"
        }

        VizTextField{
            id: minValueField
            width: 70
            text: "0"
            readOnly: true
            textFieldEnable: minmaxEnabled
            anchors.left: minLabel.right
            anchors.top: minmaxCheckmark.bottom
            anchors.topMargin: 2
        }

        VizLabel{
            id: maxLabel
            anchors.left: minValueField.right
            anchors.leftMargin: 2
            anchors.top: minmaxCheckmark.bottom
            anchors.topMargin: 2
            text: "Max"
        }

        VizTextField{
            id: maxValueField
            width: 70
            text: "255"
            readOnly: true
            textFieldEnable: minmaxEnabled
            anchors.left: maxLabel.right
            anchors.top: minmaxCheckmark.bottom
            anchors.topMargin: 2
        }*/

        VizCheckBox{
            id: sdCheckmark
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: minmaxLabel.bottom
            anchors.topMargin: 2
            onClicked: {
                if(checked)
                {
                    noneEnabled = false
                    minmaxEnabled = false
                    histeqEnabled = false
                }
                else
                {
                    noneEnabled = true
                }
            }

        }

        VizLabel{
            id: sdLabel
            anchors.left: sdCheckmark.right
            anchors.leftMargin: 2
            anchors.top: minmaxLabel.bottom
            anchors.topMargin: 2
            text: "Standard Deviation"
        }

        /*VizLabel{
            id: meanLabel
            anchors.left: sdCheckmark.right
            anchors.leftMargin: 2
            anchors.top: sdCheckmark.bottom
            anchors.topMargin: 2
            text: "Mean"
        }

        VizTextField{
            id: meanValueField
            width: 70
            text: "0"
            readOnly: true
            textFieldEnable: sdEnabled
            anchors.left: meanLabel.right
            anchors.top: sdCheckmark.bottom
            anchors.topMargin: 2
        }

        VizLabel{
            id: sdevLabel
            anchors.left: meanValueField.right
            anchors.leftMargin: 2
            anchors.top: sdCheckmark.bottom
            anchors.topMargin: 2
            text: "SD"
        }

        VizTextField{
            id: sdevValueField
            width: 70
            text: "0"
            readOnly: true
            textFieldEnable: sdEnabled
            anchors.left: sdevLabel.right
            anchors.top: sdCheckmark.bottom
            anchors.topMargin: 2
        }*/

        VizCheckBox{
            id: histeqCheckmark
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: sdLabel.bottom
            anchors.topMargin: 2
            onClicked: {
                if(checked)
                {
                    noneEnabled = false
                    sdEnabled = false
                    minmaxEnabled = false
                }
                else
                {
                    noneEnabled = true
                }
            }

        }

        VizLabel{
            id: histeqLabel
            anchors.left: histeqCheckmark.right
            anchors.leftMargin: 2
            anchors.top: sdLabel.bottom
            anchors.topMargin: 2
            text: "Histogram Equalization"
        }
		
		VizButton{
			id: applyButton
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: histeqLabel.bottom
			anchors.topMargin: 15				
            text:"Apply"
            MouseArea {
                anchors.fill: parent;
                onClicked:  {
					handleApplyButton()
                    menuHolder.minimized = true
                }
            }			
        }		

    }

    transitions: Transition{
        AnchorAnimation{
            duration: 100
        }
    }
}
