import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."
import "../VizComponents/javascripts/functions.js" as Functions

/*
     \popup AddEventGUI.qml
     \author Nitish Puri
     \brief Add timed events to the scene
 */

ContextualItem {
    id: editEventGUI
    objectName: "editEvent"

    //title: "Add Event"
    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: Math.max(mainLayout.Layout.minimumWidth + 10, 300*rootStyle.uiScale)
    minimumHeight: mainLayout.Layout.minimumHeight +10


    Behavior on height {
        PropertyAnimation{
            easing.type: Easing.OutExpo
            duration: 400
        }
    }

    Component.onCompleted: {
        if(running_from_qml){
            arrivalTime = new Date()
            departureTime = arrivalTime
        }
        EventGUI.setActive(true);
        EventGUI.editEvent(true)
        smpFileMenu.setDateTime(arrivalTime)
        smpFileMenu.setFont(fontFamily,fontSize,fontWeight)
        smpFileMenu.setColor(descColor)
        smpFileMenu.anchorContextualPickers()
    }
    Component.onDestruction:
    {
        EventGUI.showEventPreview(false)
        EventGUI.editEvent(false)
        smpFileMenu.closeDatePicker()
        smpFileMenu.closeFontPicker()
        smpFileMenu.closeColorPicker()
    }

    // this function is called from cpp to
    // change the windowtitle to Edit event
    // as the same window is used for editing also.
    function changeWindowTitle( name ){
        if(editEventGUI.mouseButtonClicked === Qt.RightButton)
        {
            tabId.title=name;
        }
        //smpFileMenu.setTitle(name)
    }

    function setTimeDate( startTime, endTime )
    {
        arrivalTime = startTime
        departureTime = endTime
    }

    property string dateFormat: "ddd MMM dd/yyyy \n hh:mm:ss"

    // properties for the window

    property alias eventName : leEventName.text
    property alias eventType: typeComboBox.selectedUID
    property bool eventTypeVisible : true

    property date arrivalTime
    property date departureTime

    property alias markButtonSelected : pbMarkButtonID.checked
    property alias previewButtonSelected : pbPreviewButtonID.checked

    // option properties
    property int optionNone : 0
    property int optionTimeStart: 1
    property int optionTimeEnd: 2
    property int selectedOption: optionNone


    // properties for info event
    property alias infoDescriptionPresent: infoEventDescriptionCheckbox.checked
    property alias infoEventDescription: infoEventDescriptionTextEdit.text
    property string iconPath: ""
    property string audioPath:""
    property string videoPath:""
    property color descColor
    property string fontWeight:"Light"
    property string fontFamily:"Arial"
    property int fontSize:40

    property bool fontPickerVisible: false;

    onFontPickerVisibleChanged: {
        if(fontPickerVisible === true){
            smpFileMenu.openFontPicker()
        }
        else{
            smpFileMenu.closeFontPicker()
        }
    }

    property bool colorPickerVisible:false;

    onColorPickerVisibleChanged: {
        if(colorPickerVisible === true){
            smpFileMenu.openColorPicker()
        }
        else{
            smpFileMenu.closeColorPicker()
        }
    }

    // properties for explosion event
    property alias explosionScale : scaleTextField.text

    //    onIconPathChanged: {
    //        addImageItem.iconSource = "file:/" + iconPath
    //    }

    // slot for info event
    signal attachFile()
    signal handleOkButtonClicked()
    signal handleCancelButtonClicked()
    signal handleMarkButtonClicked( bool value )
    signal handlePreviewButtonClicked( bool value )
    signal handleExplosionScaleChanged( double value )
    signal handleEventTypeChanged()
    signal attachAudioFile()
    signal attachVideoFile()

    onEventTypeChanged:
    {
        handleEventTypeChanged()
    }

    function timeDateChanged()
    {
        var obj = smpFileMenu.getDatePicker()
        var d = new Date(obj.year, obj.month, obj.dayOfMonth, obj.hours, obj.minutes, obj.seconds)
        if(selectedOption=== optionTimeStart){
            arrivalTime = d
            if(arrivalTime >= departureTime)
                departureTime = d
        }
        else if(selectedOption=== optionTimeEnd){
            departureTime = d
        }
    }

    function fontChanged(){
        var fontObj = smpFileMenu.getFontPicker();

        fontFamily = fontObj.fontFamily
        fontSize = fontObj.fontSize
        fontWeight =fontObj.fontWeight
    }

    states: [
        State{
            name : "InfoEvent"
            when: typeComboBox.selectedUID == "Info"
            PropertyChanges{ target: infoEventGroupBox; visible: true }
            PropertyChanges{ target: explosionEventGroupBox; visible: false }
            //PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 250+infoEventGroupBox.height+50 : 250+infoEventGroupBox.height+10 }
            PropertyChanges{ target: pbMarkButtonID; visible: false}
            //PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        },

        State{
            name : "explosionEvent"
            when: typeComboBox.selectedUID == "Explosion"
            PropertyChanges{ target: infoEventGroupBox; visible: false }
            PropertyChanges{ target: explosionEventGroupBox; visible: true }
            //PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 250+explosionEventGroupBox.height+50 : 250 + explosionEventGroupBox.height +10 }
            PropertyChanges{ target: pbMarkButtonID; visible: eventTypeVisible}
            //PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        },

        State{
            name : "gunfireEvent"
            when: typeComboBox.selectedUID == "Gunfire"
            PropertyChanges{ target: infoEventGroupBox; visible: false }
            PropertyChanges{ target: explosionEventGroupBox; visible: false }
            // PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 240+50 : 240+10 }
            PropertyChanges{ target: pbMarkButtonID; visible: eventTypeVisible}
            //PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        },

        State{
            name : "alarmEvent"
            when: typeComboBox.selectedUID == "Alarm"
            PropertyChanges{ target: infoEventGroupBox; visible: false }
            PropertyChanges{ target: explosionEventGroupBox; visible: false }
            // PropertyChanges{ target: addEventGUI; height : eventTypeVisible ? 240+50 : 240+10 }
            PropertyChanges{ target: pbMarkButtonID; visible: eventTypeVisible}
            //PropertyChanges{ target: pbPreviewButtonID; visible: eventTypeVisible}
        }
    ]
    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5
        VizTabFrame{
            id: eventTabFrame
            anchors.fill: parent
            anchors.margins: 0
            onCurrentChanged: {
                visibilityTimeButton.checked = false
                arrivalTimeButton.checked = false
                departureTimeButton.checked = false

                tabChanged(tabs[current].title)
            }
            VizTab{
                id:tabId
                title: (editEventGUI.mouseButtonClicked === Qt.RightButton)?"Event Edit":"Information"
                //contentMargin: 20
                ColumnLayout{
                    id: eventEditContextual
                    spacing: 5
                    RowLayout{
                        spacing: 5
                        visible: eventTypeVisible
                        z: 10
                        VizLabel { text: "Type"  }
                        VizComboBox{
                            id: typeComboBox
                            Layout.minimumWidth: 150
                            Layout.fillWidth: true
                            listModel:ListModel{
                                ListElement{name: "InfoEvent"; uID: "Info"}
                                ListElement{name: "Alarm"; uID: "Alarm"}
                                ListElement{name: "Explosion"; uID: "Explosion"}
                                ListElement{name: "Gunfire"; uID: "Gunfire"}
                            }
                            onSelectOption: setMinWidthHeight()
                            value: "Select Event type"
                        }
                    }
                    RowLayout{
                        spacing: 5
                        z: 5
                        anchors.rightMargin: 5
                        VizLabel { text: "Name" }
                        VizTextField{
                            readOnly: !(editEventGUI.mouseButtonClicked === Qt.RightButton)
                            id: leEventName
                            Layout.minimumWidth: 150
                            Layout.fillWidth: true
                            placeholderText : "Name of the event"
                            validator: rootStyle.nameValidator
                        }
                    }

                    // shows the start and end time of the event
                    VizGroupBox{
                        id: timeGroupBox
                        title: "Time"
                        Layout.fillWidth: true
                        Layout.minimumHeight: startTime.implicitHeight + margins
                        Layout.minimumWidth: startTime.implicitWidth + 20

                        GridLayout{
                            id: startTime
                            columns: 3
                            z: 5
                            VizLabel{
                                text: "Start Time: "
                            }
                            VizLabel{
                                id: arrivalTimeTextEdit
                                text: Qt.formatDateTime(arrivalTime, dateFormat)
                            }
                            VizButton{
                                id : pbArrivalCalenderButtonID
                                enabled: (editEventGUI.mouseButtonClicked === Qt.RightButton)
                                onClicked:{
                                    if(selectedOption === optionTimeStart){
                                        timeDateChanged()
                                        selectedOption = optionNone
                                    }
                                    else{
                                        if(fontPickerVisible == true)
                                            fontPickerVisible =false
                                        if(colorPickerVisible == true)
                                            colorPickerVisible =false
                                        smpFileMenu.setDateTime(arrivalTime)
                                        selectedOption = optionTimeStart
                                        //datePicker.setDateTime(arrivalTime)
                                    }
                                }
                                iconSource: rootStyle.getIconPath("calendar")
                                backGroundVisible: false
                            }
                            VizLabel{
                                text: " End Time: "
                            }
                            VizLabel{
                                id: departureTimeTextEdit
                                text: Qt.formatDateTime(departureTime, dateFormat)
                            }
                            VizButton{
                                id : pbDepartureCalenderButtonID
                                enabled: (editEventGUI.mouseButtonClicked === Qt.RightButton)
                                onClicked:{
                                    if(selectedOption === optionTimeEnd){
                                        timeDateChanged()
                                        selectedOption = optionNone
                                    }
                                    else{
                                        if(fontPickerVisible == true)
                                            fontPickerVisible =false
                                        if(colorPickerVisible == true)
                                            colorPickerVisible =false
                                        smpFileMenu.setDateTime(departureTime)
                                        selectedOption = optionTimeEnd
                                        //datePicker.setDateTime(departureTime)
                                    }
                                }
                                iconSource: rootStyle.getIconPath("calendar")
                                backGroundVisible: false
                            }
                        }
                    }

                    // show the parameter for info event
                    // shown only when selected in the combo box
                    VizGroupBox{
                        id: infoEventGroupBox
                        visible: (false)
                        title: "InfoEvent Parameters"
                        Layout.fillWidth: true
                        Layout.minimumHeight: infoCol.implicitHeight + margins
                        Layout.minimumWidth: infoCol.implicitWidth + 20
                        ColumnLayout{
                            id: infoCol
                            Layout.fillWidth: true
                            spacing: 5
                            RowLayout{
                                id: rowLayout
                                Layout.fillWidth: true
                                VizCheckBox{
                                    id: infoEventDescriptionCheckbox
                                    text: "Description"
                                    enabled: (editEventGUI.mouseButtonClicked === Qt.RightButton)
                                    onCheckedChanged:
                                    {
                                        if ( checked == false )
                                        {
                                            infoEventDescriptionTextEdit.text = ""
                                        }
                                    }
                                }
                                VizButton{
                                    id: fontIcon
                                    iconSource: rootStyle.getIconPath("font-size")
                                    onClicked: {
                                        if(fontPickerVisible === true){
                                            fontChanged()
                                            fontPickerVisible =false;
                                        }
                                        else{
                                            if(colorPickerVisible ==true)
                                                colorPickerVisible = false;
                                            if(selectedOption != optionNone)
                                                selectedOption = optionNone

                                            smpFileMenu.setFont(fontFamily,fontSize,fontWeight)
                                            fontPickerVisible=true;
                                        }
                                    }
                                    backGroundVisible: false;
                                }

                                VizButton{
                                    id: colorIcon
                                    iconSource: rootStyle.getIconPath("font-color")
                                    onClicked: {
                                        if(colorPickerVisible === true){
                                            colorChanged()
                                            colorPickerVisible =false;
                                        }
                                        else{
                                            if(fontPickerVisible ==true)
                                                fontPickerVisible = false;
                                            if(selectedOption != optionNone)
                                                selectedOption = optionNone

                                            smpFileMenu.setColor(descColor)
                                            colorPickerVisible=true;
                                        }
                                    }
                                    backGroundVisible: false
                                }
                            }

                            // font Dialog
                            // Color Dialog
                            VizTextEdit{
                                id: infoEventDescriptionTextEdit
                                Layout.fillWidth: true
                                Layout.minimumHeight: 60
                                visible: infoEventDescriptionCheckbox.checked
                                placeholderText: "Enter Description..."
                            }
                            RowLayout{
                                id: imageRow
                                anchors.top: (infoEventDescriptionCheckbox.checked)?infoEventDescriptionTextEdit.bottom:rowLayout.bottom
                                anchors.left: parent.left
                                anchors.right: parent.right
                                height: 50
                                VizLabel{
                                    id: imageLabel
                                    anchors.left: parent.left
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    width: 50
                                    text: "Image"
                                }
                                VizTextField{
                                    id: imageField
                                    anchors.left: imageLabel.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    text: iconPath
                                }

                                VizButton {
                                    id: addImage
                                    anchors.left: imageField.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    anchors.right: parent.right
                                    onClicked: {
                                        EventGUI.attachInfoEventImageFile()
                                    }
                                    //backGroundVisible: false
                                    text: "Browse"
                                }

                            }

                            RowLayout{
                                id: audioRow
                                anchors.top: imageRow.bottom
                                anchors.left: parent.left
                                anchors.right: parent.right
                                height: 50
                                VizLabel{
                                    id: audioLabel
                                    anchors.left: parent.left
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    width: 50
                                    text: "Audio"
                                }
                                VizTextField{
                                    id: audioField
                                    anchors.left: audioLabel.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    text: audioPath
                                }

                                VizButton {
                                    id: addAudio
                                    anchors.left: audioField.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    anchors.right: parent.right
                                    onClicked: {
                                        EventGUI.attachInfoEventAudioFile()
                                    }
                                    //backGroundVisible: false
                                    text: "Browse"
                                }

                            }
                            RowLayout{
                                id: videoRow
                                anchors.top: audioRow.bottom
                                anchors.left: parent.left
                                anchors.right: parent.right
                                height: 50
                                VizLabel{
                                    id: videoLabel
                                    anchors.left: parent.left
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    width: 50
                                    text: "Video"
                                }
                                VizTextField{
                                    id: videoField
                                    anchors.left: videoLabel.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    text: videoPath
                                }

                                VizButton {
                                    id: addVideo
                                    anchors.left: videoField.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    anchors.right: parent.right
                                    onClicked: {
                                        EventGUI.attachInfoEventVideoFile()
                                    }
                                    //backGroundVisible: false
                                    text: "Browse"
                                }
                            }
                        }
                    }

                    // show the parameter for explosion event
                    // shown only when selected in the combo box
                    VizGroupBox{
                        id: explosionEventGroupBox
                        visible: false
                        title: "Explosion Event Parameters"
                        Layout.fillWidth: true
                        Layout.minimumHeight: explosionEventRowLayout.implicitHeight + margins
                        Layout.minimumWidth: explosionEventRowLayout.implicitWidth + 20
                        RowLayout{
                            id:explosionEventRowLayout
                            spacing: 5
                            VizLabel { text : "Scale" }
                            VizSpinBox {
                                id : scaleTextField
                                value : 4.0
                                minimumValue: 4
                                maximumValue: 1000
                                enabled: (editEventGUI.mouseButtonClicked === Qt.RightButton)
                                validator: IntValidator{
                                    top: scaleTextField.maximumValue
                                    bottom: scaleTextField.minimumValue
                                }
                                onValueChanged: handleExplosionScaleChanged( value )
                            }
                        }
                    }

                    RowLayout{
                        spacing: 5
                        anchors.bottomMargin: 10
                        anchors.horizontalCenter: parent.horizontalCenter
                        visible: true
                        VizButton{
                            id : pbMarkButtonID
                            text : "Mark"
                            checkable: true
                            onCheckedChanged: handleMarkButtonClicked(checked)
                        }

                        VizButton{
                            text: "Ok"
                            onClicked: {
                                EventGUI.handleOkButtonClicked()
                                ContextualTabGUI.closeContextualTab()
                            }
                        }
                        VizButton{
                            text: "Delete"
                            onClicked: {
                                EventGUI.deleteEvent()
                            }
                        }
                        VizButton
                        {
                            id : pbPreviewButtonID
                            text : "Preview"
                            checkable: true
                            onCheckedChanged:
                            {
                                EventGUI.showEventPreview(checked)
                            }
                        }
                    }
                }
            }
        }
    }

    onSelectedOptionChanged: {
        if(selectedOption === optionTimeEnd || selectedOption === optionTimeStart){
            openDatePicker()
        }
        else{
            closeDatePicker()
        }
    }

    function openDatePicker(){
        smpFileMenu.openDatePicker(addEventGUI)
    }

    function closeDatePicker(){
        smpFileMenu.closeDatePicker()
    }
}
