import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"


ContextualItem{
    id: hyperLinkContextual
    objectName: "hyperLinkContextual"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: mainLayout.Layout.minimumWidth + 10
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Component.onCompleted: {
        if(!running_from_qml){
            CreatePointGUI.connectContextualMenu()
            PointLayerPropertiesGUI.connectContextualMenu()
            mainLayout.height = mainLayout.implicitHeight
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            CreatePointGUI.disconnectContextualMenu()
            PointLayerPropertiesGUI.disconnectContextualMenu()
        }
    }

    // play Audio in external player
    signal playAudioOrVideo(string fileName)

    // show Image in external viewer
    signal showImage(string fileName)

    signal openUrl(string url)

    ColumnLayout{
        id : mainLayout
        anchors.margins: 5
        anchors.fill: parent

        Rectangle{
            height: 100
            width: 200

            visible: (docListView.count == 0 && imageListView.count == 0 && urlListView.count == 0)
            color: "transparent"
            border.color: rootStyle.colorWhenSelected
            border.width: rootStyle.borderWidth
            VizLabel{
                text: "No HyperLinks Found!"
                textColor: rootStyle.colorWhenSelected
                fontSize: 15
                anchors.horizontalCenter : parent.horizontalCenter
                anchors.verticalCenter : parent.verticalCenter
            }
        }

        Rectangle{
            height: 100
            width: 200
            visible: (docListView.count != 0)
            color: "transparent"

            VizGroupBox {
                title: "Docs"
                id : vizDocs
                anchors.fill: parent
                anchors.margins: 2

                GridView{
                    id: docListView
                    anchors.fill: parent
                    clip: true
                    cellWidth: 50
                    cellHeight: 40
                    model: if(typeof(docList) != "undefined") docList
                    delegate: Rectangle{
                        id: docListDelegate
                        color: "transparent"
                        height: 37
                        width: 47
                        border.color: rootStyle.borderColor
                        border.width: rootStyle.borderWidth
                        Image{
                            id: docImage
                            source: rootStyle.getIconPath("save")
                            height: parent.height/1.10
                            width: height*4/3
                        }
                        MouseArea{
                            anchors.fill: parent
                            onClicked:{ playAudioOrVideo(modelData)
                                //console.log(" imagePath.source "+modelData)
                            }
                        }
                    }
                }
            }
        }
        Rectangle{
            height: 100
            width: 200

            color: "transparent"
            visible: (imageListView.count != 0)

            VizGroupBox {
                title: "Images"
                id: vizImages
                anchors.fill: parent
                anchors.margins: 2

                GridView{
                    id: imageListView
                    anchors.fill: parent
                    anchors.margins: 2
                    clip: true
                    cellWidth: 50
                    cellHeight: 40

                    model: if(typeof(imageList) != "undefined") imageList
                    delegate: Rectangle{
                        id: imageListDelegate
                        color: "transparent"
                        height: 37
                        width: 47
                        border.color: rootStyle.borderColor
                        border.width: rootStyle.borderWidth
                        Image{
                            id: imagePath
                            source: modelData
                            height: parent.height/1.10
                            width: parent.width*0.9
                            anchors.centerIn: parent
                        }
                        MouseArea{
                            anchors.fill: parent
                            onClicked:{ showImage(imagePath.source)
                                //console.log(" imagePath.source "+imagePath.source)
                            }
                        }
                    }
                }
            }
        }
        Rectangle{
            height: 100
            width: 200
            color: "transparent"
            visible: (urlListView.count != 0)

            VizGroupBox {
                title: "Urls"
                id: vizUrls
                anchors.fill: parent
                anchors.margins: 2

                GridView{
                    id: urlListView
                    anchors.fill: parent
                    anchors.margins: 2
                    clip: true
                    cellWidth: 50
                    cellHeight: 40

                    model: if(typeof(urlList) != "undefined") urlList
                    delegate: Rectangle{
                        id: urlListDelegate
                        color: "transparent"
                        height: 37
                        width: 47
                        border.color: rootStyle.borderColor
                        border.width: rootStyle.borderWidth
                        Image{
                            id: urlPath
                            source: rootStyle.getIconPath("hyperlink")
                            height: parent.height/1.10
                            width: height*4/3
                            anchors.centerIn: parent
                        }
                        MouseArea{
                            anchors.fill: parent
                            onClicked:{ openUrl(modelData)
                            }
                        }
                    }
                }
            }
        }
    }
}












