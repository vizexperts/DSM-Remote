import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"

ContextualItem{
    id: tacticalSymbolsContextual
    objectName: "tacticalSymbolsContextual"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: Math.max(mainLayout.Layout.minimumWidth + 10, 300*rootStyle.uiScale)
    minimumHeight: mainLayout.Layout.minimumHeight +10

    property alias name : nameLineEdit.lineEditText

    Component.onCompleted: {
        if(!running_from_qml){
            CreateTacticalSymbolsGUI.connectContextualMenu()
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            CreateTacticalSymbolsGUI.disconnectContextualMenu()
        }
    }

    signal rename(string newName)
    signal deleteSymbol()
    signal editSymbol(bool value)

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        VizTabFrame{
            anchors.fill: parent
            anchors.margins: 5
            current: (mouseButtonClicked == Qt.RightButton)? 0 : 3
            VizTab{
                title: "Symbol"
                visible:(mouseButtonClicked == Qt.RightButton)
                contentMargin: 20
                ColumnLayout{
                    spacing: 5
                    VizLabelTextEdit{
                        id: nameLineEdit
                        labelText: "Name "
                        onChangeApplied: rename(lineEditText)
                        validator: rootStyle.nameValidator
                    }
                    RowLayout{
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: 10
                        VizButton{
                            id: editButton
                            checkable: true
                            onClicked: editSymbol(checked)
                            text: "Edit"
                        }

                        VizButton{
                            id: deleteButton
                            scale: 0.8
                            onClicked: deleteSymbol()
                            iconSource: rootStyle.getIconPath("delete")
                        }
                    }
                }
            }
        }
    }
}
