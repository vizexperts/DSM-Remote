import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"

ContextualItem{
    id: eventContextual
    objectName: "eventContextual"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: Math.max(mainLayout.Layout.minimumWidth + 100, 300*rootStyle.uiScale)
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Component.onCompleted: {
        if(!running_from_qml){
            MilitaryPathAnimationGUI.connectContextualMenu()

            VisibilityControllerGUI.connectContextualMenu()
        }

        smpFileMenu.setDateTime(arrivalTime)
        smpFileMenu.anchorContextualPickers()
    }

    Component.onDestruction: {
        if(!running_from_qml){
            MilitaryPathAnimationGUI.disconnectContextualMenu()

            VisibilityControllerGUI.disconnectContextualMenu()
        }
        smpFileMenu.closeDatePicker()
    }

    property bool movementTabVisible: true
    property bool propertiesTabVisible: true
    property alias pathLength : positionSlider.maximumValue // in metres
    property alias position: positionSlider.value // in metres
    property date arrivalTime
    property date departureTime
    property date visibilityEventTime
    property date currentTime
    property string checkpointMode: "Add Checkpoint"
    property string visibilityMode: "Add Control Point"
    property bool pathAttached: false
    property int movementBlockHeight: 160
    property alias trailVisible: trailCheckbox.checked
    property alias visibilityState: visibilityCheckbox.checked
    property alias facingDirection: facingCombo.value
    property alias normalDirection: normalCombo.value
    property int stages : -2
    property alias checkpointComboBoxList : checkpointComboBox.value

    property alias animationType: animationTypeComboBox.value

    property bool animatedPath : false

    // datePicker properties

    property int optionNone:0
    property int optionVisibility:1
    property int optionArrival:2
    property int optionDeparture:3
    property int selectedOption:optionNone

    onSelectedOptionChanged: {
        if(selectedOption === optionVisibility || selectedOption === optionArrival || selectedOption === optionDeparture){
            openDatePicker()
        }
        else{
            closeDatePicker()
        }
    }

    function openDatePicker(){
        smpFileMenu.openDatePicker()
    }

    function closeDatePicker(){
        smpFileMenu.closeDatePicker()
    }

    onTrailVisibleChanged: showTrailChanged(trailVisible)

    function setPositionOnPath(pos){
        position = pos
    }

    onPositionChanged: {
        showGhostAt(position)
    }

    onCurrentTimeChanged: {
        smpFileMenu.setDateTime(currentTime)
        arrivalTime = currentTime
        departureTime = currentTime
        visibilityEventTime = currentTime
    }

    onPathAttachedChanged: {
        attachRoute.checked = false
    }

    property string dateFormat: "ddd MMM dd/yyyy \n hh:mm:ss"

    // movement signals
    signal attachPath(bool value)
    signal removePath()
    signal addCheckpoint(double position, date arrivalTime, date departureTime, string animationType)
    signal showGhostAt(double position)
    signal deleteCheckpoint(string cpID)
    signal editCheckpoint(string cpID, double position, date arrivalTime, date departureTime, string animationType)
    signal selectCheckpoint(string cpID)
    signal showTrailChanged(bool state)

    // visibility signals
    signal selectControlPoint(string cpID)
    signal addControlPoint(date cpTime, bool visibilityState)
    signal editControlPoint(string cpID, date cpTime, bool visibilityState)
    signal deleteControlPoint(string cpID)
    signal setEnableAxis(bool val)

    signal tabChanged(string name)

    signal selectAnimationType(string name)

    signal selectTimestampingMethod(string selectedOption)
    signal selectType(string selectedOption)
    signal selectSegment(string selectedOption)
    signal selectCheckPointFrmList(string selectedOption)
    onAttachPath: {
        if(running_from_qml){
            pathAttached = true
        }
    }

    onRemovePath: {
        if(running_from_qml)
            pathAttached = false
    }

    function okButtonClicked() {
        if(eventTabFrame.current == 0){
            if(visibilityMode == "Add Control Point"){
                addControlPoint(visibilityEventTime, visibilityCheckbox.checked)
            }
            else{
                editControlPoint(visibilityControlPointsComboBox.selectedUID, visibilityEventTime,
                                 visibilityCheckbox.checked)
            }
        }
        else{
            if(checkpointMode == "Add Checkpoint"){
                addCheckpoint(position, arrivalTime, departureTime, animationType)
            }
            else{
                editCheckpoint(checkpointComboBox.selectedUID, position, arrivalTime, departureTime, animationType)
            }
        }
    }

    onPathLengthChanged: {
        if(pathLength > 10000){
            // if path length is greater than 10km show the value in km
            positionSliderTextField.unit = "km"
        }
        else{
            positionSliderTextField.unit = "m"
        }
    }

    function populateMenu(uID){
        if(eventTabFrame.current == 0){         // visibility tab
            if(uID === "0"){
                visibilityMode = "Add Control Point"
            }
            else{
                selectControlPoint(uID)
                visibilityMode = "Edit Control Point"
            }
        }
        else if(eventTabFrame.current == 1){    // movement tab
            if(uID === "0")
            {
                checkpointMode = "Add Checkpoint"
            }
            else
            {
                checkpointMode = "Edit Checkpoint"
            }
            selectCheckpoint(uID);
        }
    }
    function processStages(  fcheckpointComboBox )
    {
        if( stages == -1)
        {
           populateMenu(fcheckpointComboBox.selectedUID);
        }
        else
        if( stages == -2)
        {
            selectTimestampingMethod(fcheckpointComboBox.value)
        }
        else if( stages == 1)
        {
            selectType(fcheckpointComboBox.value)
        }
        else if( stages == 2)
        {
            selectSegment(fcheckpointComboBox.value)
            //populateMenu(0);
        }
        else if( stages == 3)
        {
            selectCheckpoint(fcheckpointComboBox.selectedUID)
        }
    }

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        VizTabFrame{
            id: eventTabFrame
            anchors.fill: parent
            anchors.margins: 5
            Layout.fillHeight: true
            Layout.fillWidth: true
            onCurrentChanged: {
                visibilityTimeButton.checked = false
                arrivalTimeButton.checked = false
                departureTimeButton.checked = false

                tabChanged(tabs[current].title)
            }

            VizTab{
                title: "Visibility"
                ColumnLayout{
                    id:visiblityLayout
                    spacing:10
                    width:parent.width
                    anchors.fill:parent
                    RowLayout{
                        id:visiblityControlPointRow
                        spacing:7
                        z: 1
                        VizLabel{
                            text: "Control Points"
                            textAlignment: Qt.AlignLeft
                        }
                        VizComboBox{
                            id: visibilityControlPointsComboBox
                            Layout.minimumWidth: 250
                            z:1
                            listModel: (typeof(visibilityListModel) !== "undefined") ? visibilityListModel : "undefined"
                            value: "New"
                            onSelectOption: populateMenu(uID)
                        }
                        VizButton{
                            id: deleteControlPointButton
                            visible: (visibilityMode == "Edit Control Point")
                            iconSource: rootStyle.getIconPath("delete")
                            onClicked: deleteControlPoint(visibilityControlPointsComboBox.selectedUID)
                            scale: 0.7
                        }
                    }

                    RowLayout{
                        id:visibilityCheckRow
                        spacing:7
                        VizCheckBox{
                            id: visibilityCheckbox
                            text: "Visibility"
                        }
                    }

                    RowLayout{
                        spacing: 5
                        VizLabel{
                            text: "Time"
                        }
                        VizLabel{
                            id: timeTextEdit
                            text: Qt.formatDateTime(visibilityEventTime, dateFormat)
                        }
                        VizButton{
                            id: visibilityTimeButton
                            iconSource: rootStyle.getIconPath("calendar")
                            backGroundVisible: false
                            onClicked: {

                                if(selectedOption===optionVisibility){
                                    timeDateChanged()
                                    selectedOption = optionNone
                                }
                                else{
                                    smpFileMenu.setDateTime(visibilityEventTime)
                                    //departureTimeButton.checked = false
                                    //timeDateRect.setDateTime(arrivalTime)
                                    selectedOption = optionVisibility
                                }
                            }
                        }
                    }

                    VizButton{
                        id: addVisibilityControlPointButton
                        Layout.alignment: Qt.AlignHCenter
                        text: visibilityMode
                        onClicked: okButtonClicked()
                    }
                }
            }
            VizTab{
                title: "Movement"
                contentMargin: 20
                visible: movementTabVisible
                ColumnLayout{
                    spacing:10
                    anchors.fill:parent
                    RowLayout{
                        id:attachRouteRow
                        spacing:7
                        VizButton{
                            id: attachRoute
                            visible: !pathAttached
                            text: "Attach Path"
                            onClicked: {
                                checked = !checked
                                attachPath(checked)
                            }
                        }
                        VizCheckBox{
                            id: trailCheckbox
                            text: "Show Trail"
                            visible: pathAttached
                        }
                    }
                    RowLayout{
                        id:checkPointMovementRow
                        spacing:5
                        z: 1
                        VizLabel{
                            id: checkpointComboBoxLabel
                            visible: pathAttached
                            text: "Checkpoints"
                        }
                        VizComboBox{
                            id: checkpointComboBox
                            visible: pathAttached
                            listModel: (typeof(checkpointListModel) !== "undefined") ? checkpointListModel : "undefined"
                            Layout.fillWidth: true
                            value: "New"
                            objectName: "checkpointComboBoxNme"
                            onSelectOption: {
                                processStages(checkpointComboBox)
                            }
                        }

                        VizButton{
                            id: deleteCheckpointButton
                            visible: (pathAttached && ( stages == -1 ) && (checkpointMode == "Edit Checkpoint"))
                            iconSource: rootStyle.getIconPath("delete")
                            onClicked:{
                                deleteCheckpoint(checkpointComboBox.selectedUID)
                            }
                        }
                    }
                    RowLayout
                    {
                        GridLayout{
                            columns: 3
                            visible: pathAttached && (  stages == -1 )
                            VizLabel{
                                text : "Arrival Time : "
                            }
                            VizLabel{
                                id: arrivalTimeTextEdit
                                text: Qt.formatDateTime(arrivalTime, dateFormat)
                            }
                            VizButton{
                                id: arrivalTimeButton
                                iconSource: rootStyle.getIconPath("calendar")
                                backGroundVisible: false
                                onClicked: {
                                    if(selectedOption===optionArrival){
                                        timeDateChanged()
                                        selectedOption = optionNone
                                    }
                                    else{
                                        smpFileMenu.setDateTime(arrivalTime)
                                        //departureTimeButton.checked = false
                                        //timeDateRect.setDateTime(arrivalTime)
                                        selectedOption = optionArrival
                                    }
                                }
                            }
                            VizLabel{
                                text : "Departure\nTime : "
                            }
                            VizLabel{
                                id: departureTimeTextEdit
                                text: Qt.formatDateTime(departureTime, dateFormat)
                            }
                            VizButton{
                                id: departureTimeButton
                                iconSource: rootStyle.getIconPath("calendar")
                                backGroundVisible: false
                                onClicked: {
                                    if(selectedOption===optionDeparture){
                                        timeDateChanged()
                                        selectedOption = optionNone
                                    }
                                    else{
                                        smpFileMenu.setDateTime(departureTime)
                                        //departureTimeButton.checked = false
                                        //timeDateRect.setDateTime(arrivalTime)
                                        selectedOption = optionDeparture
                                    }
                                }
                            }
                        }
                        ColumnLayout
                        {
                            visible: pathAttached && animatedPath && (  stages == -1 )
                            VizLabel
                            {
                                text : "Animation Type :"
                            }
                            VizComboBox{
                                id: animationTypeComboBox
                                listModel: (typeof(animationTypeListModel) !== "undefined") ? animationTypeListModel : "undefined"
                                Layout.fillWidth: true
                                value: "Select Type"
                                width: 200
                                onSelectOption: selectAnimationType(value)
                            }

                        }
                    }

                    RowLayout{
                        id:positionSliderRow
                        Layout.fillWidth:true
                        spacing:5
                        VizLabel{
                            id: positionSliderTextField
                            visible: pathAttached && (  stages == -1 )
                            property string unit: "m"
                            property real value
                            text: "Distance From Start : " + value + " " + unit
                        }
                    }
                    RowLayout{
                        id:sliderRow
                        anchors.top:positionSliderRow.bottom
                        anchors.topMargin: 10
                        Layout.fillWidth: true
                        VizSlider{
                            id: positionSlider
                            Layout.fillWidth: true
                            visible: pathAttached && ( stages == -1 )
                            minimumValue : 0
                            maximumValue : 1000
                            labelVisible: false
                            anchors.horizontalCenter: parent.horizontalCenter
                            value: 0
                            onValueChanged:{
                                if(positionSliderTextField.unit === "m"){
                                    positionSliderTextField.value = value.toFixed(3)
                                }
                                else if(positionSliderTextField.unit === "km"){
                                    positionSliderTextField.value = (value/1000).toFixed(3)
                                }
                            }
                        }
                    }
                    RowLayout{
                        id: row2
                        spacing:5
                        width:parent.width
                        anchors.top: sliderRow.bottom
                        anchors.topMargin: 10
                        visible: pathAttached
                        VizButton{
                            id: addCheckpointButton
                            anchors.left:parent.left
                            anchors.leftMargin:20
                            text: checkpointMode
                            visible: pathAttached && ( stages == -1 )
                            onClicked: okButtonClicked()
                        }
                        VizButton{
                            id: removePathButton
                            anchors.left:addCheckpointButton.right
                            anchors.leftMargin:10
                            text: "Remove Path"
                            onClicked: removePath()
                        }
                    }
                }
            }

            VizTab{
                title: "Properties"
                contentMargin: 20
                visible: propertiesTabVisible

                ColumnLayout{
                    spacing:10
                    anchors.fill:parent
                    RowLayout{
                        VizButton{
                            id: setOrientationButton
                            text: "Enable Axis"
                            Layout.alignment: Qt.AlignLeft
                            checkable: true
                            onClicked: {
                                //show Manipulator
                                setEnableAxis(checked)
                            }
                        }
                    }

                    ColumnLayout{
                        spacing: 5
                        visible: setOrientationButton.checked
                        Layout.fillWidth: true
                        RowLayout{
                            spacing : 5
                            Layout.fillWidth: true
                            VizLabel{
                                id: forwardLabel
                                Layout.alignment: Qt.AlignLeft
                                text: "Forward Direction"
                            }

                            VizComboBox{
                                id: facingCombo
                                Layout.alignment: Qt.AlignRight
                                Layout.fillWidth: true
                                listModel: ListModel{
                                    id: facingAxis
                                    ListElement{name:"  X"; uID : "x"}
                                    ListElement{name:"  Y"; uID : "y"}
                                    ListElement{name:"  Z"; uID : "z"}
                                    ListElement{name:"- X"; uID : "-x"}
                                    ListElement{name:"- Y"; uID : "-y"}
                                    ListElement{name:"- Z"; uID : "-z"}
                                }
                                color: (value === "  X" || value=== "- X")?"red":
                                                                            ((value === "  Y" || value=== "- Y")
                                                                             ?"green":"blue")
                                value: facingAxis.get(selectedUID).name

                                //                                onValueChanged: {
                                //                                    facingDirection= value
                                //                                }
                            }
                        }
                        RowLayout{
                            spacing: 5
                            Layout.fillWidth: true
                            VizLabel{
                                id : normalLabel
                                Layout.alignment: Qt.AlignLeft
                                text: "Normal Direction"
                            }

                            VizComboBox{
                                id: normalCombo
                                Layout.alignment: Qt.AlignRight
                                Layout.fillWidth: true
                                listModel: ListModel{
                                    id: normalAxis
                                    ListElement{name:"  X"; uID : "x"}
                                    ListElement{name:"  Y"; uID : "y"}
                                    ListElement{name:"  Z"; uID : "z"}
                                    ListElement{name:"- X"; uID : "-x"}
                                    ListElement{name:"- Y"; uID : "-y"}
                                    ListElement{name:"- Z"; uID : "-z"}
                                }
                                color: (value === "  X" || value=== "- X")?"red":
                                                                            ((value === "  Y" || value=== "- Y")
                                                                             ?"green":"blue")
                                value: normalAxis.get(selectedUID).name
                                //                                onValueChanged: {
                                //                                    normalDirection= value
                                //                                }
                            }
                        }

                        VizButton{
                            text: "Set Axis"
                            Layout.alignment: Qt.AlignRight
                            onClicked: {
                                MilitaryPathAnimationGUI.setAxis(facingDirection,normalDirection);
                            }
                        }
                    }
                }
            }
        }
    }


    function timeDateChanged(){
        var obj = smpFileMenu.getDatePicker()
        var d = new Date(obj.year, obj.month, obj.dayOfMonth, obj.hours, obj.minutes, obj.seconds)
        if(selectedOption===optionVisibility){
            visibilityEventTime = d
        }
        else if(selectedOption===optionArrival){
            arrivalTime = d
            if(arrivalTime >= departureTime)
                departureTime = d
        }
        else if(selectedOption===optionDeparture){
            departureTime = d
        }
    }
}
