import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

ContextualItem {
    id: landmarkContextual

    objectName: "landmarkContextual"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: Math.max(mainLayout.Layout.minimumWidth + 10, 300*rootStyle.uiScale)
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Component.onCompleted: {
        if(!running_from_qml){
            CreateLandmarkGUI.connectContextualMenu()
            CreateLandmarkGUI.handleClippingGUI()
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            //Deleting the clipped model and replacing it with the real one if clipping is still active
            closeClippingFromGUI()
            CreateLandmarkGUI.disconnectContextualMenu()
            rightMenu.setSource("")
            rightMenu.minimized = true
        }
    }

    //Properties
    property alias name: nameLineEdit.lineEditText

    property alias latitude: latitudeLineEdit.latLongText

    property alias longitude: longitudeLineEdit.latLongText

    property alias altitude: altitudeLineEdit.lineEditText

    property alias scale: scaleLineEdit.lineEditText

    property bool attributesTabVisible: true

    // signals
    signal rename(string newName)

    signal handleLatLongAltScaleChanged(string attribute)

    signal snapToGround()

    signal reorient()

    signal deleteLandmark()

    signal crossSection()

    signal closeClippingFromGUI()

    property alias xPlaneEnabled: cXplaneCheckmark.checked
    property alias xPlaneOppositeEnabled: cXplaneOppositeCheckmark.checked

    property alias xPlaneDistance: xPlaneSlider.value
    property alias xPlaneMinExtent: xPlaneSlider.minimumValue
    property alias xPlaneMaxExtent: xPlaneSlider.maximumValue

    property alias xPlaneDistanceOpposite: xPlaneSliderOpposite.value
    property alias xPlaneMinExtentOpposite: xPlaneSliderOpposite.minimumValue
    property alias xPlaneMaxExtentOpposite: xPlaneSliderOpposite.maximumValue

    property alias xPlaneSpinBoxMinValue: xPlaneSpinBox.minimumValue
    property alias xPlaneSpinBoxMaxValue: xPlaneSpinBox.maximumValue
    property alias xPlaneSpinBoxDistance: xPlaneSpinBox.value

    property alias xPlaneSpinBoxMinValueOpposite: xPlaneSpinBoxOpposite.minimumValue
    property alias xPlaneSpinBoxMaxValueOpposite: xPlaneSpinBoxOpposite.maximumValue
    property alias xPlaneSpinBoxDistanceOpposite: xPlaneSpinBoxOpposite.value

    signal toggleXPlane(bool active)
    signal toggleXPlaneOpposite(bool active)
    signal setFrontBackPlaneDistance(double xplane)
    signal setFrontBackPlaneDistanceOpposite(double xplaneOpposite)

    onXPlaneDistanceChanged:setFrontBackPlaneDistance(xPlaneDistance)
    onXPlaneSpinBoxDistanceChanged:  xPlaneSpinBox.setValue(xPlaneSpinBoxDistance)

    onXPlaneDistanceOppositeChanged:setFrontBackPlaneDistanceOpposite(xPlaneDistanceOpposite)
    onXPlaneSpinBoxDistanceOppositeChanged:  xPlaneSpinBoxOpposite.setValue(xPlaneSpinBoxDistanceOpposite)

    property alias yPlaneEnabled: cYplaneCheckmark.checked
    property alias yPlaneOppositeEnabled: cYplaneOppositeCheckmark.checked

    property alias yPlaneDistance: yPlaneSlider.value
    property alias yPlaneMinExtent: yPlaneSlider.minimumValue
    property alias yPlaneMaxExtent: yPlaneSlider.maximumValue

    property alias yPlaneDistanceOpposite: yPlaneSliderOpposite.value
    property alias yPlaneMinExtentOpposite: yPlaneSliderOpposite.minimumValue
    property alias yPlaneMaxExtentOpposite: yPlaneSliderOpposite.maximumValue

    property alias yPlaneSpinBoxMinValue: yPlaneSpinBox.minimumValue
    property alias yPlaneSpinBoxMaxValue: yPlaneSpinBox.maximumValue
    property alias yPlaneSpinBoxDistance: yPlaneSpinBox.value

    property alias yPlaneSpinBoxMinValueOpposite: yPlaneSpinBoxOpposite.minimumValue
    property alias yPlaneSpinBoxMaxValueOpposite: yPlaneSpinBoxOpposite.maximumValue
    property alias yPlaneSpinBoxDistanceOpposite: yPlaneSpinBoxOpposite.value


    signal toggleYPlane(bool active)
    signal toggleYPlaneOpposite(bool active)
    signal setLeftRightPlaneDistance(double yPlane)
    signal setLeftRightPlaneDistanceOpposite(double yplaneOpposite)

    onYPlaneDistanceChanged:setLeftRightPlaneDistance(yPlaneDistance)
    onYPlaneSpinBoxDistanceChanged:  yPlaneSpinBox.setValue(yPlaneSpinBoxDistance)

    onYPlaneDistanceOppositeChanged:setLeftRightPlaneDistanceOpposite(yPlaneDistanceOpposite)
    onYPlaneSpinBoxDistanceOppositeChanged:  yPlaneSpinBoxOpposite.setValue(yPlaneSpinBoxDistanceOpposite)


    property alias zPlaneEnabled: cZplaneCheckmark.checked
    property alias zPlaneOppositeEnabled: cZplaneOppositeCheckmark.checked

    property alias zPlaneDistance: zPlaneSlider.value
    property alias zPlaneMinExtent: zPlaneSlider.minimumValue
    property alias zPlaneMaxExtent: zPlaneSlider.maximumValue

    property alias zPlaneDistanceOpposite: zPlaneSliderOpposite.value
    property alias zPlaneMinExtentOpposite: zPlaneSliderOpposite.minimumValue
    property alias zPlaneMaxExtentOpposite: zPlaneSliderOpposite.maximumValue

    property alias zPlaneSpinBoxMinValue: zPlaneSpinBox.minimumValue
    property alias zPlaneSpinBoxMaxValue: zPlaneSpinBox.maximumValue
    property alias zPlaneSpinBoxDistance: zPlaneSpinBox.value

    property alias zPlaneSpinBoxMinValueOpposite: zPlaneSpinBoxOpposite.minimumValue
    property alias zPlaneSpinBoxMaxValueOpposite: zPlaneSpinBoxOpposite.maximumValue
    property alias zPlaneSpinBoxDistanceOpposite: zPlaneSpinBoxOpposite.value


    signal toggleZPlane(bool active)
    signal toggleZPlaneOpposite(bool active)
    signal setTopBottomPlaneDistance(double zplane)
    signal setTopBottomPlaneDistanceOpposite(double zplaneOpposite)

    onZPlaneDistanceChanged:setTopBottomPlaneDistance(zPlaneDistance,false)
    onZPlaneSpinBoxDistanceChanged:  zPlaneSpinBox.setValue(zPlaneSpinBoxDistance)

    onZPlaneDistanceOppositeChanged:setTopBottomPlaneDistanceOpposite(zPlaneDistanceOpposite)
    onZPlaneSpinBoxDistanceOppositeChanged:  zPlaneSpinBoxOpposite.setValue(zPlaneSpinBoxDistanceOpposite)

    property alias opacityValue: opacitySlider.value
    signal setOpacity(double value)
    onOpacityValueChanged: setOpacity(opacityValue/100)

    property alias opacitySpinBoxValue: opacitySpinBox.value
    signal setOpacitySpinBox(double value)
    onOpacitySpinBoxValueChanged:{
        opacitySpinBox.setValue(opacitySpinBoxValue.toFixed(1))
        opacityValue = opacitySpinBoxValue
    }

    property alias quadsBoundaryEnabled: quadsBoundaryCheckmark.checked
    property alias clippingPersistantEnabled: persistentCheckmark.checked
    property bool persistentEnabledOnOtherModel: false

    signal toggleQuadsboundary(bool value)
    signal togglePersistanceCheck(bool value)

    property bool isReadOnlyMode:(mouseButtonClicked == Qt.LeftButton)?true:false
    signal setEnableDragger(bool value)

    signal openModelSelectionPane(bool value)

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        ColumnLayout{
        spacing: 5
        id: mainColumnLayout

        VizTabFrame{
            id: landmarkContextualTabFrame
            current: (landmarkContextual.mouseButtonClicked== Qt.RightButton)?0:1
            anchors.fill: parent
            anchors.margins: 0

            VizTab{
                title: "LandMark"
                visible: (landmarkContextual.mouseButtonClicked == Qt.RightButton)
                ColumnLayout{
                    VizLabelTextEdit{
                        id: nameLineEdit
                        labelText: "Name "
                        onChangeApplied: rename(lineEditText)
                        labelWidth: 80*rootStyle.uiScale
                        Keys.onTabPressed: {
                            latitudeLineEdit.giveFocus()
                        }
                        validator: rootStyle.nameValidator
                        placeholderText: "Name"
                        isReadOnly:isReadOnlyMode
                    }
                    VizLatLongTextEdit{
                        id: latitudeLineEdit
                        labelWidth: 80*rootStyle.uiScale
                        labelText: "Latitude"
                        type:type_LATITUDE
                        onLatLongChanged: handleLatLongAltScaleChanged("Latitude")
                        //onChangeApplied: handleLatLongAltScaleChanged("Latitude")
                        Keys.onTabPressed: longitudeLineEdit.giveFocus()
                        //validator: rootStyle.latitudeValidator
                        placeholderText: "Latitude"
                        isReadOnly: isReadOnlyMode
                    }
                    VizLatLongTextEdit{
                        id: longitudeLineEdit
                        type:type_LONGITUDE
                        labelWidth: 80*rootStyle.uiScale
                        labelText: "Longitude"
                        onLatLongChanged: handleLatLongAltScaleChanged("Longitude")
                        //onChangeApplied: handleLatLongAltScaleChanged("Longitude")
                        Keys.onTabPressed: altitudeLineEdit.giveFocus()
                        //validator: rootStyle.longitudeValidator
                        placeholderText: "Longitude"
                        isReadOnly: isReadOnlyMode
                    }
                    VizLabelDoubleEdit{
                        id: altitudeLineEdit
                        labelText: "Altitude"
                        labelWidth: 80*rootStyle.uiScale
                        onChangeApplied: handleLatLongAltScaleChanged("Altitude")
                        Keys.onTabPressed: nameLineEdit.giveFocus()
                        validator: rootStyle.altitudeValidator
                        placeholderText: "Altitude"
                        isReadOnly: isReadOnlyMode
                    }
                    VizLabelDoubleEdit{
                        id: scaleLineEdit
                        labelText: "Scale"
                        validator:rootStyle.scaleValidator
                        labelWidth: 80*rootStyle.uiScale
                        placeholderText: "Scale"
                        onChangeApplied: handleLatLongAltScaleChanged("Scale")
                        isReadOnly: isReadOnlyMode
                    }
                }
            }

            VizTab{
                title: "Attributes"
                visible: attributesTabVisible || (mouseButtonClicked == Qt.LeftButton)

                ShowAttributes{
                    id: attributes
                    objectName: "landmarkAttributes"
                    mouseButtonClicked: landmarkContextual.mouseButtonClicked
                    anchors.fill: parent
                    Layout.fillHeight: true
                }
            }
        }
       }
        RowLayout{
            id: row1
            anchors.top: mainColumnLayout.bottom
            anchors.left: parent.bottom
            spacing: 5
            visible: !isReadOnlyMode;
            VizButton{
                id: deleteButton
                iconSource: rootStyle.getIconPath("delete")
                scale: 0.8
                onClicked: deleteLandmark()
            }
            VizButton{
                id : setModelButton
                text: "Set Model"
                checkable: true
                objectName: "setModelButton"
                onClicked:
                {
                    if (crossSectionButton.checked || manipulatorsButton.checked)
                    {
                        if (crossSectionButton.checked)
                        {
                            crossSection()
                            crossSectionButton.checked = false

                        }

                        else if (manipulatorsButton.checked)
                        {
                            setEnableDragger(false)
                            manipulatorsButton.checked = false
                        }
                    }

                    openModelSelectionPane(checked)

                    if(checked){
                        rightMenu.setSource("")
                        rightMenu.minimized = false
                        rightMenu.setSource("Landmarks.qml")
                    }

                    else{
                        rightMenu.setSource("")
                        rightMenu.minimized = true
                    }
                }
            }



            VizButton{
                id: manipulatorsButton
                text: "Manipulator"
                checkable: true
                onClicked:
                {
                    if(crossSectionButton.checked || setModelButton.checked)
                    {
                        if (crossSectionButton.checked)
                        {
                            crossSection()
                            crossSectionButton.checked = false

                        }

                        else if (setModelButton.checked)
                        {
                            openModelSelectionPane(false)
                            rightMenu.setSource("")
                            rightMenu.minimized = true
                            setModelButton.checked = false
                        }
                    }

                    setEnableDragger(checked)
                }
            }

            VizButton{
                id: crossSectionButton
                text: "Cross section"
                checkable: true
                onClicked:
                {
                    if (manipulatorsButton.checked || setModelButton.checked)
                    {
                        if (setModelButton.checked)
                        {
                            openModelSelectionPane(false)
                            rightMenu.setSource("")
                            rightMenu.minimized = true
                            setModelButton.checked = false
                        }

                        else if (manipulatorsButton.checked)
                        {
                            setEnableDragger(false)
                            manipulatorsButton.checked = false
                        }
                    }
                    crossSection()
                }

            }

        }

        ColumnLayout{

            visible:crossSectionButton.checked && !persistentEnabledOnOtherModel

            RowLayout{

                VizLabel{ text: "Front-Back " }

                VizCheckBox{
                    id: cXplaneCheckmark
                    onClicked: toggleXPlane(checked)
                }

                VizSpinBox{
                    id: xPlaneSpinBox
                    visible: xPlaneEnabled
                    validator: DoubleValidator{
                        top: xPlaneSpinBoxMaxValue
                        bottom: xPlaneSpinBoxMinValue
                        notation: DoubleValidator.StandardNotation;
                    }

                    onValueChanged:{
                        setFrontBackPlaneDistance(value)
                    }
                }

                VizSlider{
                    id: xPlaneSlider
                    visible: xPlaneEnabled
                    labelVisible: false
                    Layout.fillWidth: true
                }



            }

            RowLayout{

                VizLabel{ text: "Back-Front " }

                VizCheckBox{
                    id: cXplaneOppositeCheckmark
                    onClicked: toggleXPlaneOpposite(checked)
                }

                VizSpinBox{
                    id: xPlaneSpinBoxOpposite
                    visible: xPlaneOppositeEnabled
                    validator: DoubleValidator{
                        top: xPlaneSpinBoxMaxValueOpposite
                        bottom: xPlaneSpinBoxMinValueOpposite
                        notation: DoubleValidator.StandardNotation;
                    }

                    onValueChanged:{
                        setFrontBackPlaneDistanceOpposite(value)
                    }
                }

                VizSlider{
                    id: xPlaneSliderOpposite
                    visible: xPlaneOppositeEnabled
                    labelVisible: false
                    Layout.fillWidth: true
                }



            }

            RowLayout{

                VizLabel{ text: "Left-Right   " }

                VizCheckBox{
                    id: cYplaneCheckmark
                    onClicked: toggleYPlane(checked)
                }

                VizSpinBox{
                    id: yPlaneSpinBox
                    visible: yPlaneEnabled
                    validator: DoubleValidator{
                        top: yPlaneSpinBoxMaxValue
                        bottom: yPlaneSpinBoxMinValue
                        notation: DoubleValidator.StandardNotation;
                    }

                    onValueChanged:{
                        setLeftRightPlaneDistance(value)
                    }
                }


                VizSlider{
                    id: yPlaneSlider
                    visible: yPlaneEnabled
                    labelVisible: false
                    Layout.fillWidth: true
                }

            }

            RowLayout{

                VizLabel{ text: "Right-Left   " }

                VizCheckBox{
                    id: cYplaneOppositeCheckmark
                    onClicked: toggleYPlaneOpposite(checked)
                }

                VizSpinBox{
                    id: yPlaneSpinBoxOpposite
                    visible: yPlaneOppositeEnabled
                    validator: DoubleValidator{
                        top: yPlaneSpinBoxMaxValueOpposite
                        bottom: yPlaneSpinBoxMinValueOpposite
                        notation: DoubleValidator.StandardNotation;
                    }

                    onValueChanged:{
                        setLeftRightPlaneDistanceOpposite(value)
                    }
                }

                VizSlider{
                    id: yPlaneSliderOpposite
                    visible: yPlaneOppositeEnabled
                    labelVisible: false
                    Layout.fillWidth: true
                }



            }

            RowLayout{

                VizLabel{ text: "Top-Bottom" }

                VizCheckBox{
                    id: cZplaneCheckmark
                    onClicked: toggleZPlane(checked)
                }

                VizSpinBox{
                    id: zPlaneSpinBox
                    visible: zPlaneEnabled
                    validator: DoubleValidator{
                        top: zPlaneSpinBoxMaxValue
                        bottom: zPlaneSpinBoxMinValue
                        notation: DoubleValidator.StandardNotation;
                    }

                    onValueChanged:{
                        setTopBottomPlaneDistance(value)
                    }
                }


                VizSlider{
                    id: zPlaneSlider
                    visible: zPlaneEnabled
                    labelVisible: false
                    Layout.fillWidth: true
                }


            }

            RowLayout{

                VizLabel{ text: "Bottom-Top" }

                VizCheckBox{
                    id: cZplaneOppositeCheckmark
                    onClicked: toggleZPlaneOpposite(checked)
                }

                VizSpinBox{
                    id: zPlaneSpinBoxOpposite
                    visible: zPlaneOppositeEnabled
                    validator: DoubleValidator{
                        top: zPlaneSpinBoxMaxValueOpposite
                        bottom: zPlaneSpinBoxMinValueOpposite
                        notation: DoubleValidator.StandardNotation
                    }

                    onValueChanged:{
                        setTopBottomPlaneDistanceOpposite(value)
                    }
                }

                VizSlider{
                    id: zPlaneSliderOpposite
                    visible: zPlaneOppositeEnabled
                    labelVisible: false
                    Layout.fillWidth: true
                }



            }
        }

        ColumnLayout{
            visible:  crossSectionButton.checked && (xPlaneEnabled || yPlaneEnabled || zPlaneEnabled || xPlaneOppositeEnabled || yPlaneOppositeEnabled || zPlaneOppositeEnabled ) && !persistentEnabledOnOtherModel
            RowLayout {

                VizLabel{
                    id:opacity
                    text: "Opacity                    "
                }

                VizSpinBox{
                    id: opacitySpinBox
                    validator: DoubleValidator{
                        top: 100
                        bottom: 0
                        notation: DoubleValidator.StandardNotation
                    }
                    textBoxWidth: 74
                    value: 30
                    singlestep: 0.1

                    onValueChanged:{
                        setOpacitySpinBox(value/100)
                    }
                }

                VizSlider{
                    id: opacitySlider
                    labelVisible: false
                    minimumValue: 0
                    maximumValue: 100
                    value : 30

                    Layout.fillWidth: true
                    onValueChanged: {
                        opacitySpinBox.value = value
                    }

                }

            }

            RowLayout{

                VizLabel{ text: "Remove Boundary  " }

                VizCheckBox{
                    id: quadsBoundaryCheckmark
                    checked: false
                    onClicked: toggleQuadsboundary(checked)
                }

                VizLabel{ text: "Persistent" }

                VizCheckBox{
                    id: persistentCheckmark
                    checked: false
                    onClicked: togglePersistanceCheck(checked)
                }
            }
        }

    ColumnLayout{
        visible: crossSectionButton.checked && persistentEnabledOnOtherModel
        VizLabel{ text:"Clipping enabled on other model. Please turn off clipping in that model before switching this on" }
     }
    }
}
