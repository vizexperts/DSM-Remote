import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"

ContextualItem{
    id: militaryContextual
    objectName: "militarySymbolPointContextual"

    height: mainLayout.implicitHeight + 2*mainLayout.anchors.margins
    width: mainLayout.implicitWidth+ 2*mainLayout.anchors.margins

    minimumWidth: Math.max(mainLayout.Layout.minimumWidth + 10, 300*rootStyle.uiScale)
    minimumHeight: mainLayout.Layout.minimumHeight +10

    Component.onCompleted: {
        if(!running_from_qml){
            ResourceManagerGUI.connectContextualMenu()
        }
    }

    Component.onDestruction: {
        if(!running_from_qml){
            ResourceManagerGUI.disconnectContextualMenu()
        }
    }

    //! Signals
    //! Mil symbol
    signal rename(string newName)
    signal handleLatLongAltChanged(string attribute)
    signal deleteSymbol()
    signal clampingChanged(bool value)
    signal textVisibilityChanged(bool value)
    signal sphereVisibilityChanged(bool value)
    signal sphereRangeChanged( double value )
    signal handleUserIdChanged(string userid)
    signal selectGroup(string uID)
    signal changeSymbolSize(string symbolSize)

    // Properties
    //! Mil symbol
    property string iconPath : rootStyle.getIconPath("error")
    property alias symbolName: symbolNameLabel.lineEditText
    property string symbolType: "AD_BTY"
    property alias latitude: latitudeLineEdit.latLongText
    property alias longitude: longitudeLineEdit.latLongText
    property alias altitude: altitudeLineEdit.lineEditText
    property alias clampingEnabled: clampingCheckbox.checked
    property alias sphereVisible : sphereVisibilityID.checked
    property alias sphereRadius : sphereRangeID.text
    property alias textVisible: textVisibilityCheckBox.checked

    signal editEnabled(bool value)

    property bool isReadOnlyMode:(mouseButtonClicked == Qt.LeftButton)?true:false

    ColumnLayout{
        id: mainLayout
        anchors.fill: parent
        anchors.margins: 5
        spacing: 5

        VizTabFrame{
            anchors.fill: parent
            anchors.margins: 10
            VizTab{
                title: "Symbol"
                ColumnLayout{
                    spacing: 5
                    RowLayout{
                        id: symbolTypeRow
                        VizLabel{
                            id: symbolTypeLabel
                            text: symbolType
                            Layout.minimumWidth: 200*rootStyle.uiScale
                        }
                        Rectangle{
                            color: "#64eed7d7"
                            width: 30*rootStyle.uiScale
                            height: 30*rootStyle.uiScale
                            Image{
                                id: symbolIcon
                                anchors.fill: parent
                                source: iconPath
                            }
                        }
                    }
                    VizLabelTextEdit{
                        id: symbolNameLabel
                        labelWidth: 80*rootStyle.uiScale
                        labelText: "Name  "
                        isReadOnly: isReadOnlyMode
                        onChangeApplied: rename(lineEditText)
                        Keys.onTabPressed: {
                            latitudeLineEdit.giveFocus()
                        }
                        validator: rootStyle.nameValidator
                    }
                    VizLatLongTextEdit{
                        id: latitudeLineEdit
                        type:type_LATITUDE
                        labelText: "Latitude "
                        isReadOnly: isReadOnlyMode
                        labelWidth: 80*rootStyle.uiScale
                        onLatLongChanged: handleLatLongAltChanged("Latitude");
                        Keys.onTabPressed: {
                            longitudeLineEdit.giveFocus()
                        }
                    }
                    VizLatLongTextEdit{
                        id: longitudeLineEdit
                        type:type_LONGITUDE
                        labelWidth: 80*rootStyle.uiScale
                        isReadOnly: isReadOnlyMode
                        labelText: "Longitude"
                        onLatLongChanged: handleLatLongAltChanged("Longitude");
                        Keys.onTabPressed: {
                            altitudeLineEdit.giveFocus()
                        }
                    }
                    VizLabelTextEdit{
                        id: altitudeLineEdit
                        labelText: "Altitude "
                        isReadOnly: isReadOnlyMode
                        onChangeApplied: handleLatLongAltChanged("Altitude")
                        labelWidth: 80*rootStyle.uiScale
                        enabled: !clampingEnabled
                        Keys.onTabPressed: symbolNameLabel.giveFocus()
                        validator: rootStyle.altitudeValidator
                    }
                    VizLabelTextEdit{
                        id: userIdEdit
                        visible: (rootStyle.showDemoFeatures && !isReadOnlyMode)
                        labelText: "User Id "
                        labelWidth: 80*rootStyle.uiScale
                        onChangeApplied: handleUserIdChanged(lineEditText)
                    }
                    RowLayout{
                        Layout.alignment: Qt.AlignCenter
                        VizCheckBox{
                            id: clampingCheckbox
                            visible: !isReadOnlyMode
                             text: "Clamp"
                            onCheckedChanged: {
                                clampingChanged(checked)
                            }
                        }
                        VizCheckBox{
                            id: textVisibilityCheckBox
                            visible: !isReadOnlyMode
                            text: "Text Visibility"
                            onCheckedChanged: {
                                textVisibilityChanged(checked)
                            }
                        }
                    }
                    RowLayout{
                        visible: !isReadOnlyMode
                        VizCheckBox{
                            id: sphereVisibilityID
                            text: "Range"
                            onCheckedChanged: {
                                sphereVisibilityChanged(checked)
                            }
                        }

                        VizSpinBox{
                            id : sphereRangeID
                            minimumValue: 0
                            enabled: sphereVisible
                            maximumValue: 1000
                            validator: IntValidator{
                                top: 1000
                                bottom: 0
                            }
                            onValueChanged:{
                                sphereRangeChanged( value )
                            }
                        }
                    }
                    RowLayout{
                        id: bottomRow
                        anchors.horizontalCenter: parent.horizontalCenter
                        visible:!isReadOnlyMode
                        spacing: 10
                        VizComboBox {
                            id: sizeConId
                            listModel:ListModel{

                                ListElement{name:"16"; uID : "16"}
                                ListElement{name:"32"; uID : "32"}
                                ListElement{name:"48"; uID : "48"}
                                ListElement{name:"64"; uID : "64"}
                                ListElement{name:"80"; uID : "80"}
                                ListElement{name:"96"; uID : "96"}
                                ListElement{name:"112"; uID : "112"}
                                ListElement{name:"128"; uID : "128"}
                            }
                            Layout.fillWidth: true
                            value: "Symbol Size"
                            onSelectOption: {
                                if (value != "Symbol Size")
                                {
                                    changeSymbolSize(value)
                                    //console.log(value)

                                }
                                else
                                {
                                    //do nothing
                                }


                            }
                        }
                        VizButton{
                            id: deleteButton
                            scale: 0.8
                            iconSource: rootStyle.getIconPath("delete")
                            onClicked: deleteSymbol()
                        }
                        VizButton{
                            id: editPointButton
                            text: "Reposition"
                            anchors.verticalCenter: deleteButton.verticalCenter
                            checkable: true
                            onCheckedChanged: editEnabled(checked)
                        }
                    }
                }
            }
        }
    }
}
