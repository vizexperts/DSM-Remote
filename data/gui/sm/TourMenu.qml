import QtQuick 2.1
import QtQuick.Layouts 1.0
import "VizComponents"

Rectangle {
    id: tourMenu
    objectName: "tourMenu"
    color: "transparent"
    height: 150*rootStyle.uiScale
    width:  bottomLoader.width

    Component.onCompleted: {
        bottomLoaderWrapper.tourMenuLoaded()
        populateTourList()
    }

    Component.onDestruction: {
        //flag set false
        smpFileMenu.isPopUpAlreadyOpen=false;
        stopTour()
        closed()
    }

    signal closed()

    // Tour related signals
    signal addNewTour()
    signal playTour(string name)
    signal pauseTour()
    signal stopTour()
    signal deleteTour(string name)
    signal changeTourName( string oldName, string newName )
    signal populateTourList()
    signal micClicked(string tour)
    signal deleteAudio(string tour)
    signal enableMicInModel(string tour)
    signal muteNarration(bool checked)

    // Tour recorder related signals
    signal changeRecorderType(int type)
    signal editCurrentTourSpeed()

    // checkpoints related signals
    signal createCheckPoint(int elapsedTime)
    signal modifyCheckPoint(int index, int newTime, real newSpeed)
    signal deleteCheckPoint(int index)
    signal resetSpeedProfile()
    signal saveSpeedProfile()
    signal cpContextualOpened(int index)
    signal previewAtCp(int elapsedTime)

    // Player speed related signals
    signal decreaseSpeedFactor()
    signal increaseSpeedFactor()
    signal tourSliderValueChanged(int value)

    signal changeMenuType(int menuType)

    property bool narrationPresent : false
    property bool animationRunning: false
    property bool recordingRunning: false
    property bool showSlider: false
    property string currentItemName
    property alias elapsedTime: tourSlider.value
    property int totalTime
    property string speedFactor: ""
    property int recorderType: 0

    //! Menu shows Tour list/sequence list
    //! 0: Tour Menu
    //! 1: Sequence Menu
    property int menuType: 0
	
	//! considering tour mode by default
    property int tourToPlay : 1
    onMenuTypeChanged: changeMenuType(menuType)

    property string tourName
    property alias tourPlayerState: tourMenu.state
    property alias activeCheckPointIndex: tourSlider.activeCheckPointIndex
    property alias speedAtActiveCheckPoint: tourSlider.speedAtCurrentCheckPoint

	// will be changed from cpp side
    onElapsedTimeChanged: {
        if( elapsedTime === 0 && tourToPlay === 0){
            animationRunning = false
        }
    }

    Rectangle{
        id:player
        width: parent.width
        height: parent.height
        color: "transparent"

        Behavior on width{
            PropertyAnimation{ duration: 100 }
        }

        Rectangle{
            anchors.fill: parent
            gradient: rootStyle.gradientWhenDefault
            opacity: 0.5
        }

        VizButton{
            id: closeButton
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 10
            anchors.rightMargin: 10
            iconSource: rootStyle.getIconPath("close")
            onClicked: bottomLoaderWrapper.unload()
            backGroundVisible: false
        }

        VizButton{
            id: minimizeButton
            anchors.top: closeButton.top
            anchors.right: closeButton.left
            anchors.rightMargin: 10
            iconSource: rootStyle.getIconPath("minimize")
            onClicked: parent.state = (parent.state == "MINIMIZED") ? "" : "MINIMIZED"
            backGroundVisible: false
        }

        states: State {
            name: "MINIMIZED"
            PropertyChanges {target: tourMenu; height: 100*rootStyle.uiScale}
            PropertyChanges {target: bottomLoaderWrapper; height: tourMenu.height}
            PropertyChanges {target: minimizeButton; iconSource: rootStyle.getIconPath("maximize") }
            PropertyChanges {target: addTourItem; visible: !tourControls.visible}
            PropertyChanges {target: createCPButton; visible: !tourControls.visible}
            PropertyChanges {target: resetCPButton; visible: !tourControls.visible}
            PropertyChanges {target: tourSlider; bottomMargin: 20}
            PropertyChanges {target: tourControls; spacing: 5}
            PropertyChanges {target: tourControls; horizontalCenterOffset : -(tourMenu.width - tourControls.width)/2}
            PropertyChanges {target: tourSlider; width: (minimizeButton.x - tourControls.width - 20)}
            PropertyChanges {target: tourSlider; horizontalCenterOffset: (5+tourControls.width - (tourMenu.width - tourSlider.width)/2)}
        }

        VizComboBox{
            id: tourMenuTypeComboBox
            anchors.left: parent.left
            width: 200*rootStyle.uiScale
            enabled: !animationRunning
            visible: addTourItem.visible
            anchors.leftMargin: 100
            anchors.verticalCenter: tourControls.verticalCenter
            listModel: ListModel{
                ListElement{name: "Tour Options"; uID: 0}
                ListElement{name: "Sequence Options"; uID: 1}
            }
            value: listModel.get(menuType).name
            onSelectOption: tourMenu.menuType = uID
        }

        VizButton{
            id: addTourItem
            enabled: !animationRunning
            anchors.left: tourMenuTypeComboBox.right
            anchors.leftMargin: 20
            anchors.verticalCenter: tourControls.verticalCenter
            text: (menuType == 0) ? "Add New Tour" : "Add New Sequence"
            onClicked: {
                if(running_from_qml){
                    animationRunning = true
                    showSlider = true
                    currentItemName = "dummy"
                }
                else if(menuType == 1){
                    bottomLoaderWrapper.unload()
                    smpFileMenu.load("TourSequencePopup.qml")
                }
                else{
                    addNewTour()
                    tourRecorderLoader.load = true
                    bottomLoaderWrapper.unload()
                }
            }
        }

        VizComboBox{
            id: recorderTypeComboBox
            anchors.left: addTourItem.right
            visible: addTourItem.visible && (menuType == 0)
            enabled: addTourItem.enabled
            width: 200*rootStyle.uiScale
            anchors.leftMargin: 20
            anchors.top: addTourItem.top
            value: listModel.get(recorderType).name
            listModel: ListModel{
                ListElement{name:"Movement Recorder"; uID: 0}
                ListElement{name:"Keyframe Recorder"; uID: 1}
            }
            onSelectOption: tourMenu.changeRecorderType(uID)
        }

        VizButton{
            id: createCPButton
            text: "Create Checkpoint"
            visible: showSlider && (menuType == 0)
            anchors.right: resetCPButton.left
            anchors.rightMargin: 15
            anchors.top: recorderTypeComboBox.top
            onClicked: {
                createCheckPoint(elapsedTime)
            }
        }

        VizButton{
            id: resetCPButton
            text: "Reset"
            visible: showSlider && (menuType == 0)
            anchors.right: minimizeButton.left
            anchors.rightMargin: 15
            anchors.top: recorderTypeComboBox.top
            onClicked: {
                resetSpeedProfile()
            }
        }

        TourSeekBar{
            id: tourSlider

            width: showSlider ? parent.width - 200 : 0
            visible: showSlider
            z : 1

            minimumValue: 0
            maximumValue: totalTime

            onSetActiveProperties: {
                cpContextualOpened(activeCheckPointIndex)
                pauseTour()
            }
            onModifyCP: {
                modifyCheckPoint(activeCheckPointIndex, newTime, newSpeed)
            }
            onDeleteCP: {
                deleteCheckPoint(activeCheckPointIndex)
            }
            onCpPositionChanged: {
                previewAtCp(time)
            }

            property int bottomMargin: 25
            property int horizontalCenterOffset: 0
            Behavior on bottomMargin {NumberAnimation{duration: 100}}

            anchors.bottom: parent.bottom
            anchors.bottomMargin: bottomMargin + 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: horizontalCenterOffset

            labelVisible: false

            unit: "ms"
            value: 0

            Behavior on width {
                NumberAnimation{
                    duration: 100
                }
            }
            onValueChanged: {
                if(pressed)
                    tourSliderValueChanged(value)
            }
            VizLabel{
                anchors.left: parent.left
                anchors.bottom: parent.top
                anchors.bottomMargin: 10
                text: "Elapsed : " + (elapsedTime/1000000).toFixed(2) + " sec"
            }

            VizLabel{
                anchors.right: parent.right
                anchors.bottom: parent.top
                anchors.bottomMargin: 10
                text: "Remaining : " + ((totalTime - elapsedTime)/1000000).toFixed(2) + " sec/" + (totalTime/1000000).toFixed(2) + " sec"
            }

            VizLabel{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.top
                anchors.bottomMargin: 10
                text: Qt.formatDateTime(currentMissionDateTime, "ddd MMM dd yyyy hh:mm:ss") + tourName
            }
        }


        Row{
            id: tourControls
            anchors.horizontalCenter: parent.horizontalCenter
            property int  horizontalCenterOffset: 60
            anchors.horizontalCenterOffset: horizontalCenterOffset
            Behavior on horizontalCenterOffset {NumberAnimation{duration: 200}}
            visible: (currentItemName != "")
            anchors.top: parent.top
            anchors.topMargin: 10
            spacing: 10
            VizButton{
                id: slowButton
                iconSource: rootStyle.getIconPath("time/slower")
                backGroundVisible: false
                onClicked: decreaseSpeedFactor()
            }
            VizButton{
                id: playPauseButton
                iconSource: animationRunning ?  rootStyle.getIconPath("time/pause") : rootStyle.getIconPath("time/play")
                backGroundVisible: false
                onClicked: {
                    if(animationRunning){
                        pauseTour()
                    }
                    else{
                        playTour(currentItemName)
                    }
                }
            }
            VizButton{
                id:stopButton
                iconSource: rootStyle.getIconPath("time/stop")
                backGroundVisible: false
                onClicked: stopTour()
            }
            VizButton{
                id:fastButton
                iconSource: rootStyle.getIconPath("time/faster")
                backGroundVisible: false
                onClicked: increaseSpeedFactor()
            }
            VizLabel{
                id: speedFactorLabel
                text: "Speed:"
            }
            VizLabel{
                id: speedFactorValueLabel
                width: 50
                text: speedFactor + "X"
                textAlignment: Qt.AlignLeft
            }

            VizButton{
                id:mute
                iconSource: rootStyle.getIconPath("mic_crossed")
                height: stopButton.height
                visible : tourMenuTypeComboBox.value === "Sequence Options" && !narrationPresent && animationRunning && recordingRunning
                backGroundVisible: checked
                checkable: true
                onClicked: muteNarration(checked)
            }
        }

        ListView{
            id: listView
            visible: !showSlider
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.right: parent.right
            anchors.rightMargin: 20
            clip: true
            spacing: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.top: tourControls.bottom
            anchors.topMargin: 10
            model: (typeof(tourListModel) != "undefined")? tourListModel : "undefined"
            orientation: ListView.Horizontal
            boundsBehavior : Flickable.StopAtBounds
            delegate: Rectangle{
                id: wrapper
                color: "transparent"
                Rectangle{
                    anchors.fill: parent
                    gradient: if(name === currentItemName)
                                  rootStyle.gradientWhenSelected
                              else
                                  rootStyle.gradientWhenDefault
                    radius: 5
                    border.color: rootStyle.borderColor
                    border.width: rootStyle.borderWidth
                }
                width: height*5/3
                height: listView.height
                radius: 5
                VizLabel
                {
                    id : tourLabel
                    anchors.top: parent.top
                    anchors.topMargin: 5
                    textWidth: parent.width - 10
                    text: name
                    wrapMode: TextEdit.WrapAnywhere
                    visible : true
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        currentItemName = name
                        if (tourMenuTypeComboBox.value === "Sequence Options")
                        {
                            enableMicInModel(currentItemName)
                        }
                    }
                    onDoubleClicked:
                    {
                        playTour(name)
                    }
                }

                VizLabelTextEdit{
                    id : tourLabelEdit
                    visible : false
                    labelVisible : false
                    width : parent.width
                    validator: rootStyle.nameValidator
                    anchors.left: parent.left
                    placeholderText: "Tour Name"
                    onChangeApplied :
                    {
                        if ( lineEditText !== "" )
                        {
                            visible = false
                            tourLabel.visible = true
                            changeTourName( tourLabel.text, lineEditText )
                            tourLabel.text = lineEditText
                            lineEditText = ""
                        }
                        else
                        {
                            visible = false
                            tourLabel.visible = true
                        }
                    }
                }
                RowLayout{

                    anchors.fill: parent
                    anchors.bottom: parent.bottom

                    VizButton
                    {
                        id : edit
                        anchors.bottom: parent.bottom

                        backGroundVisible: false
                        iconSource: rootStyle.getIconPath("edit")
                        onClicked :
                        {
                            tourLabel.visible = false
                            tourLabelEdit.visible = true
                        }

                        scale: 0.8
                    }

                    VizButton
                    {
                        visible: tourMenuTypeComboBox.value === "Sequence Options"
                        anchors.bottom: parent.bottom
                        iconSource: audioPresent ? rootStyle.getIconPath("mic_crossed") : rootStyle.getIconPath("mic")
                        backGroundVisible: false
                        enabled: micEnable
                        onClicked :{
                            if (!audioPresent) {
                                micClicked(name)
                            }
                            else
                            {
                                deleteAudio(name)
                            }
                        }

                        scale: 0.8
                    }

                    VizButton
                    {
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        backGroundVisible: false
                        iconSource: rootStyle.getIconPath("delete")
                        scale: 0.8
                        onClicked: deleteTour(name)
                    }
                }
            }
        }
    }

}
