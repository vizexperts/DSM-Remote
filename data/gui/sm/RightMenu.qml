import QtQuick 2.1
import QtGraphicalEffects 1.0
import "VizComponents"

Item {
    id: menuHolder
    objectName: "smpMenu"
    width: menuLoader.width
    height: minimized ? 0 : menuLoader.height

    visible: !minimized

    x: minimized? root.width : root.width - width

    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        opacity: 0.7
    }

    Behavior on x {
        NumberAnimation{
            duration: 500
            easing.type: Easing.OutExpo
        }
    }

    Behavior on height {
        PropertyAnimation{
            easing.type: Easing.OutBack
            duration: 800
        }
    }


    property bool minimized: true

    onMinimizedChanged: {
        //if(!minimized){
            loadMainMenu()
        //}
    }

    signal changeOption(string type)
    signal loaded(string type)
    signal loadMainMenu()

    onLoadMainMenu: {
        timelineLoader.minimized = true
        if(application_mode == application_mode_NONE){
            setSource("")
		}
        else{
            setSource("") //Reset then set ProRightPanel
            setSource("ProRightPanel.qml")
        }
    }

    Connections{
        target: root
        onApplication_modeChanged: {
            loadMainMenu()
            timelineLoader.minimized = true
            elementList.minimized = true
            tourRecorderLoader.load = false
            contextualMenu.unload()
            bottomLoader.unload()
            pdfLoader.closeAll()
            tourRecorderLoader.load = false
            multipleSelectionMenu.unload()
            smpFileMenu.unloadAll()
        }
    }

    //! Ex. Filename = "CreatorMenu.qml"
    function setSource(fileName){
        if(fileName === ""){
            menuLoader.source = ""
        }
        else{
            menuLoader.source = "SideMenu/" + fileName
        }
    }

    Loader{
        id: menuLoader
        anchors.horizontalCenter: parent.horizontalCenter;
        source: ""
    }
}
