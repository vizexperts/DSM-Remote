import QtQuick 2.1
import "VizComponents"

Rectangle{
    id: fileGroup
    height: filesLabelTextEdit.height + 10
    width: parent.width
    color: "transparent"
    border.color: "white"
    border.width: 2

    clip:true

    Behavior on height {
        NumberAnimation{
            duration: 300
        }
    }

    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    property alias label: filesLabelTextEdit.text
    property alias model: filesView.model

    function open(){
        var toOpen = true

        if(fileGroup.state == "OPENED")
            toOpen = false

        fileManager.resetGroupStates()

        if(label !== "Recent Files"){
            fileView.currentIndex = index
        }

        if(toOpen)
            fileGroup.state = "OPENED"
    }

    MouseArea{
        anchors.fill: parent
        enabled: (fileGroup.state == "")
        onClicked: open()
    }

    Image {
        id: icon
        source: rootStyle.getIconPath("down")
        anchors.right: parent.right
        anchors.rightMargin: 5
        height: filesLabelTextEdit.height
        anchors.verticalCenter: filesLabelTextEdit.verticalCenter
        width: height
        MouseArea{
            anchors.fill: parent
            onClicked: open()
        }
    }

    states: [
        State {
            name: "OPENED"
            PropertyChanges { target: fileGroup; height: filesLabelTextEdit.height + filesView.height + 20 }
            PropertyChanges { target: icon; source: rootStyle.getIconPath("up") }
        }
    ]

    VizLabel{
        id: filesLabelTextEdit
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
    }

    GridView{
        id: filesView
        cellWidth: 190*rootStyle.uiScale
        cellHeight: 135*rootStyle.uiScale
        anchors.left: parent.left
        anchors.top: filesLabelTextEdit.bottom
        anchors.topMargin: 10
        cacheBuffer: 200
        height: cellHeight*(Math.min(Math.ceil(count/2), 2))
        clip: true
        anchors.leftMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 5
        flow: GridView.LeftToRight
        boundsBehavior: Flickable.StopAtBounds
        delegate: FileIcon{}
        VizScrollBar{
            target: filesView
        }
    }
}
