import QtQuick 2.1
import "VizComponents"
Rectangle {
    id: bottomLoaderWrapper
    objectName: "bottomLoader"
    anchors.right: parent.right
    anchors.left: elementList.minimized ? toolBoxId.right : elementList.right


    color: "transparent"
    radius: 5

    //! Signals
    signal elevationProfileLoaded()
    signal tourMenuLoaded()
    signal pdfMenuLoaded()

    //!properties
    property bool minimized: true

    //! Add an event eating background mouse area
    VizBackgroundMouseArea{}

    function load(fileName){

        if(fileName === ""){
            return
        }
        if(smpFileMenu.isPopUpAlreadyOpen)
        {
            return false;
        }
        smpFileMenu.isPopUpAlreadyOpen=true;
        bottomLoader.source = ""
        bottomLoader.source = fileName
        height = bottomLoader.height
        return true;

    }

    function unload(){
        if(bottomLoader.source!="")
        {
             smpFileMenu.isPopUpAlreadyOpen=false;
        }
        height = 0
        bottomLoader.source = ""


    }

    Rectangle{
        anchors.fill: parent
        gradient: rootStyle.gradientWhenDefault
        radius: parent.radius
        opacity: 0.3
    }


    Behavior on height {
        NumberAnimation{
            duration: 500
            easing.type: Easing.OutExpo
        }
    }

    Loader{
        id: bottomLoader
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
    }
}
