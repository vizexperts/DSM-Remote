import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "VizComponents"
import "VizComponents/javascripts/functions.js" as Functions

Slider{
    id: slider
    objectName: "timelineSlider"
    height: (profileType == profileType_None) ? 8 : 20

    style: SliderStyle{
        groove: Rectangle{
            width: slider.width
            Behavior on height {
                NumberAnimation{
                    duration: 100
                }
            }
            height:slider.height
            color: rootStyle.colorWhenDefault
            gradient: rootStyle.gradientWhenDefault
            radius: Functions.countRadius(rootStyle.roundness, width, height, 0, 1)
            Rectangle{
                id: left
                anchors.verticalCenter: parent.verticalCenter
                visible: (slider.profileType === slider.profileType_None)
                x: 2
                height: parent.height - 4
                width: (value - minimumValue)/(maximumValue - minimumValue) * (parent.width) - 2
                gradient: rootStyle.blueGradient
            }

            ListView{
                id: profile
                anchors.fill: parent
                visible: (slider.profileType !== slider.profileType_None)
                orientation: ListView.Horizontal
                currentIndex: -1
                interactive: false
                clip: true
                model : (slider.profileType !== slider.profileType_None) ? timelineModel : dummy
                delegate: Rectangle{
                    id: profileDelegate
                    opacity: 0.7
                    color: profileColor
                    width: deltaX*profile.width
                    height: profile.height
                    Rectangle{
                        id: startMark
                        anchors.left: parent.left
                        width: 1
                        height: parent.height
                        color: "yellow"
                    }
                    Rectangle{
                        id: endMark
                        anchors.right: parent.right
                        width: 2
                        height: parent.height
                        color: "yellow"
                    }
                }
            }
            Rectangle{
                id: handle
                width: 3
                height: parent.height
                anchors.horizontalCenter: left.right
                anchors.verticalCenter: parent.verticalCenter
                gradient: rootStyle.gradientWhenSelected
            }

        }

        handle: Item{
            anchors.top: parent.bottom
            Rectangle{
                color: "transparent"
                x: -15
                width: 30
                height: 10
                clip: true
                Rectangle{
                    x: 15
                    width: 15
                    height: 15
                    radius: 2
                    transformOrigin: Item.TopLeft
                    rotation: 45
                    color: rootStyle.colorWhenSelected
                }
            }
        }
    }

    property alias timeLabelVisible: dateLabel.visible

    property string name: value

    property int profileType : profileType_None
    property int profileType_None: 0
    property int profileType_Visibility: 1
    property int profileType_Movement: 2

    VizLabel{
        id: dateLabel
        text: Qt.formatDateTime(currentMissionDateTime, dateFormat)
        anchors.bottom: timeLabel.top
        anchors.bottomMargin: -10
        anchors.horizontalCenter: parent.horizontalCenter
    }
    VizLabel{
        id: timeLabel
        visible: dateLabel.visible
        text: Qt.formatDateTime(currentMissionDateTime, timeFormat)
        anchors.bottom: parent.top
        anchors.bottomMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter

    }
}
