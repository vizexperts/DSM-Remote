import QtQuick 2.1
import "VizComponents"

/**
 * Component to show clock on screen
 */

Item{
    width: timeb.width + 20
    height: time.height+timeb.height+20
    VizLabel{
        id: time
        text: qsTr(((systemDateTime.hours<10)?"0":"")+systemDateTime.hours+":"+((systemDateTime.minutes<10)? "0":"")+systemDateTime.minutes)
        fontSize: rootStyle.fontSize * 4
        fontWeight: Font.Light
    }

    VizLabel{
        id: timeb
        anchors.left: time.left
        anchors.top: time.bottom
        text: qsTr(systemDateTime.day+", "+systemDateTime.month+" "+systemDateTime.date)
        fontSize: rootStyle.fontSize * 2
        fontWeight: Font.Light
    }
}
