/**
 * Helpful functions for QtQuick-library
 */


 /**
  * Keeps number in given area. If value isn't between min and max value
  *
  * @param value Value
  * @param min Minimum value for value
  * @param max Maximun value for value
  * @return value if value is betwewwn min and max. otherwise returns min or max value.
  */
function numberParser(value,min,max) {
    if (value < min) return min;
    if (value > max) return max;
    return value;
}

/**
 * Count radius for qml element. Function is used to get relative radius
 * to different size of items.
 *
 * @param roundness wanted roundness value
 * @param elementWidth Width of element
 * @param elementHeight Height of element
 * @param minValue
 * @param maxValue
 * @return Radius value
 */
function countRadius(roundness,elementWidth,elementHeight,minValue,maxValue) {
    roundness = numberParser(roundness,minValue,maxValue);
    if (elementWidth < elementHeight) return (elementWidth/2)*roundness;
    return (elementHeight/2)*roundness;
}
