import QtQuick 2.1
import "javascripts/keyboard.js" as KeyboardEngine
import "includes/"

/**
 * More simple keyboard. Loading time optimized.
 */
Rectangle {
    id: keyboard

    property VizStyle style : VizStyle{ }

    //    property string text
    property TextInput textInput
    property real buttonWidth: ((keyboard.width-buttonSpacing*11)/11) - (keyboard.width*0.05)/11 ;
    property real buttonHeight: (keyboard.height-buttonSpacing*4)/4
    property int buttonSpacing: width*0.005

    readonly property alias abcOn: internal.abcOn// read-only
    readonly property alias numbersOn: internal.numbersOn; // read-only

    readonly property bool shiftOn: internal.shiftOn // read-only
    readonly property alias capsOn: internal.capsOn // read-only

    QtObject{
        id: internal
        property bool capsOn: false  // internal
        property bool shiftOn: false  // internal
        property bool abcOn: true  // internal
        property bool numbersOn: false  // internal
    }

    property bool showOkCLButton: true
    property bool showCancelCLButton: true

    property real shiftTextFontSize : style.fontSize*0.7
    property string shiftTextFontFamily : style.fontFamily
    property real shiftTextFontWeight : style.fontWeight
    property color shiftTextColor : style.textColor


    property real imageWidth : buttonWidth
    property real imageHeight : buttonHeight

    property Image shiftImage : Image { source: "icons/shift_50x50.png"; scale: buttonHeight*0.01; smooth: true}
    property Image enterImage : Image { source: "icons/enter_50x70.png"; scale: buttonWidth*0.01; smooth: true}
    property Image backspaceImage : Image { source: "icons/backspace_50x70.png"; scale: buttonWidth*0.01; smooth: true}
    property Image leftArrowImage : Image { source: "icons/arrow_left_50x50.png"; scale: buttonHeight*0.008; smooth: true}
    property Image rightArrowImage : Image { source: "icons/arrow_right_50x50.png"; scale: buttonHeight*0.008; smooth: true}
    property Image upArrowImage : Image { source: "icons/arrow_up_50x50.png"; scale: buttonHeight*0.008; smooth: true}
    property Image downArrowImage : Image { source: "icons/arrow_down_50x50.png"; scale: buttonHeight*0.008; smooth: true}
    property Image okImage : Image { source: "icons/ok_50x50.png"; scale: buttonHeight*0.01; smooth: true}
    property Image cancelImage : Image { source: "icons/delete_50x50.png"; scale: buttonHeight*0.01; smooth: true}
    property Image capsImage : nullImage

    /* Properties for background images
     * --------------------------------
     * This solution is temporary. Remember performance.
     */
    property Image nullImage : Image { //this property is "private" don't write it to documentation
        id: nullImageID
        source: ""
        width: buttonWidth
        height: buttonHeight
        fillMode: Image.PreserveAspectCrop
    }

    property Image buttonBackgroundImage : nullImage

    property Image buttonBackgroundImageWhenHovered : nullImage
    property Image buttonBackgroundImageWhenPressed : nullImage

    /* \internal */
    property Image buttonCurrentImage : buttonBackgroundImage //this property is "private" don't write it to documentation

    property string buttonTextAlign: "center"
    property real buttonLeftMargin: 0
    property real buttonRightMargin: 0


    signal leftArrowClicked()
    signal rightArrowClicked()
    signal upArrowClicked()
    signal downArrowClicked()
    signal okCLButtonClicked()
    signal cancelCLButtonClicked()
    signal clicked()
    signal backspaceClicked()
    signal enterClicked()


    /**
     * Function shows number buttons.
     */
    function showNumbers() {
        if (!numbersOn) {
            numbers.checked = true
            abc.checked= false

            changeCharacters(KeyboardEngine.numbers)
            internal.numbersOn = true;
            internal.abcOn = false
        }
    }

    /**
     * Function shows abc buttons
     */
    function showAbc() {
        if (!abcOn) {
            abc.checked = true
            numbers.checked = false

            changeCharacters(KeyboardEngine.abc)

            internal.abcOn = true
            internal.numbersOn = false
        }
    }

    /**
     * This function changes character text in buttons when "state" is changed.
     * Changes characters between abc characters, number characaters and european special characters (���)
     *
     * @param buttons in array
     * @return nothing
     */
    function changeCharacters(characters) {

        for (var i=0; i<row1.children.length; i++) {
            row1.children[i].value = characters[i][0];
            if(characters[i][1] != null) row1.children[i].shiftValue = characters[i][1];
            else row1.children[i].shiftValue = characters[i][0].toUpperCase()
            if ((shiftOn || capsOn) && row1.children[i].shiftValue != "") {
                row1.children[i].text = row1.children[i].shiftValue
                row1.children[i].shiftText = row1.children[i].value
            } else {
                row1.children[i].text = row1.children[i].value
                if (row1.children[i].shiftValue != "") row1.children[i].shiftText = row1.children[i].shiftValue
                else row1.children[i].shiftText = ""
            }
        }

        for (var i=0; i<row2.children.length; i++) {
            row2.children[i].value = characters[KeyboardEngine.row1Length + i][0];
            if (characters[KeyboardEngine.row1Length - 1 + i][1] != null) {
                row2.children[i].shiftValue = characters[KeyboardEngine.row1Length + i][1];
            } else  row2.children[i].shiftValue = characters[KeyboardEngine.row1Length + i][0].toUpperCase();

            //if shift or caps is on and character has a shift value
            if ((shiftOn || capsOn) && row2.children[i].shiftValue != "") {
                row2.children[i].text = row2.children[i].shiftValue
                row2.children[i].shiftText = row2.children[i].value
            } else {
                row2.children[i].text = row2.children[i].value
                if (row2.children[i].shiftValue != "") row2.children[i].shiftText = row2.children[i].shiftValue
                else row2.children[i].shiftText = ""
            }
        }

        for (var i=0; i<row3.children.length; i++) {
            row3.children[i].value = characters[KeyboardEngine.row1Length+KeyboardEngine.row2Length + i][0];
            if (characters[KeyboardEngine.row1Length+KeyboardEngine.row2Length + i][1] != null)row3.children[i].shiftValue = characters[KeyboardEngine.row1Length+KeyboardEngine.row2Length + i][1];
            else row3.children[i].shiftValue = characters[KeyboardEngine.row1Length+KeyboardEngine.row2Length + i][0].toUpperCase();

            if ((shiftOn || capsOn) && row3.children[i].shiftValue != "") {
                row3.children[i].text = row3.children[i].shiftValue
                row3.children[i].shiftText = row3.children[i].value

            } else {
                row3.children[i].text = row3.children[i].value
                if(row3.children[i].shiftValue != "") row3.children[i].shiftText = row3.children[i].shiftValue
                else row3.children[i].shiftText = ""
            }
        }
    }

    /**
     * Function which will be performed when button is clicked. In this moment
     * only used by buttons with shift value.
     *
     * @param active text(character) of button which is clicked
     * @return nothing
     */
    function keyboardCLButtonClicked(character) {
        var selection = textInput.selectedText
        var selectionStart = textInput.selectionStart
        var selectionEnd = textInput.selectionEnd
        if(selection === ""){
            var cursor = textInput.cursorPosition
            var start = textInput.text.substring(0, cursor)
            var end = textInput.text.substring(cursor, textInput.text.length)
            var resultText = ""+start+character+end
            textInput.text = resultText
            textInput.cursorPosition = cursor+1
        }
        else{
            var start = textInput.text.substring(0, selectionStart)
            var end = textInput.text.substring(selectionEnd, textInput.text.length)
            var resultText = ""+start+character+end
            textInput.text = resultText
            textInput.cursorPosition = selectionStart+1
        }

        keyboard.clicked();
        if(shiftOn) shift();
    }

    /**
     * This function will be executed if shift- or caps-button is clicked on
     */
    function setShiftOn() {
        shiftOnSingleRow(row1);
        shiftOnSingleRow(row2);
        shiftOnSingleRow(row3);

        //if there is background image for up arrow button
        if (upArrowImage.source != "") {
            leftArrow.text = ""
            leftArrow.specialImage = upArrowImage
        } else {
            leftArrow.text = leftArrow.shiftValue
            leftArrow.specialImage = nullImage
        }

        leftArrow.shiftText = leftArrow.value

        //if there is background image for down arrow button
        if (downArrowImage.source != "") {
            rightArrow.text = ""
            rightArrow.specialImage = downArrowImage
        } else {
            rightArrow.text = rightArrow.shiftValue
            rightArrow.specialImage = nullImage
        }

        rightArrow.shiftText = rightArrow.value

        apostrophe.text = apostrophe.shiftValue
        apostrophe.shiftText = apostrophe.value
    }

    /**
     * Change button text to shift value in single row
     *
     * @param row Row of buttons.
     * @return nothing
     */
    function shiftOnSingleRow(row) {
        for (var i=0; i<row.children.length; i++) {
            if (row.children[i].shiftValue != "") {
                row.children[i].text = row.children[i].shiftValue;
                row.children[i].shiftText = row.children[i].value;
            }
        }
    }

    /**
     * This function will be executed if shift- or caps-button is clicked off
     */
    function setShiftOff() {
        shiftOffSingleRow(row1);
        shiftOffSingleRow(row2);
        shiftOffSingleRow(row3);

        if (leftArrowImage.source != "") {
            leftArrow.text = ""
            leftArrow.specialImage = leftArrowImage
        } else {
            leftArrow.text = leftArrow.value
            leftArrow.specialImage = nullImage
        }

        leftArrow.shiftText = leftArrow.shiftValue

        if (rightArrowImage.source != "") {
            leftArrow.text = ""
            rightArrow.specialImage = rightArrowImage
        } else {
            rightArrow.text = rightArrow.value
            rightArrow.specialImage = nullImage
        }

        rightArrow.shiftText = rightArrow.shiftValue

        apostrophe.text = apostrophe.value
        apostrophe.shiftText = apostrophe.shiftValue

    }

    /**
     * Sets shift off in single row of buttons.
     *
     * @param row of buttons
     * @return nothing
     */
    function shiftOffSingleRow(row) {
        for (var i=0; i<row.children.length; i++) {
            if (row.children[i].shiftValue != "") {
                row.children[i].text = row.children[i].value;
                row.children[i].shiftText = row.children[i].shiftValue;
            }
        }
    }

    /**
     * Function is called when shift-button is pressed...
     */
    function shift() {
        if (shiftOn) {
            //if shift is on, let's put it off
            setShiftOff();
            internal.shiftOn = false;
            capsCLButton.enabled = true;
            shiftCLButton.checked = false
        } else {
            //if shift is off, let's put it on
            setShiftOn();
            internal.shiftOn = true;
            capsCLButton.enabled = false;
            shiftCLButton.checked = true
        }
    }

    // Function is called when backspace button is pressed
    function handleBackspaceClicked(){
        var selection = textInput.selectedText
        var selectionStart = textInput.selectionStart
        var selectionEnd = textInput.selectionEnd
        if(selection === ""){
            var cursor = textInput.cursorPosition
            var start = textInput.text.substring(0, cursor-1)
            var end = textInput.text.substring(cursor, textInput.text.length)
            var resultText = ""+start+end
            textInput.text = resultText
            textInput.cursorPosition = cursor-1
        }
        else{
            var start = textInput.text.substring(0, selectionStart)
            var end = textInput.text.substring(selectionEnd, textInput.text.length)
            var resultText = ""+start+end
            textInput.text = resultText
            textInput.cursorPosition = selectionStart
        }

        backspaceClicked()
    }

    function deleteTextIncrementally(){
        var textVal = textInput.text
        var selection = textInput.selectedText
        var selectionStart = textInput.selectionStart
        var selectionEnd = textInput.selectionEnd

        var cursorPos = textInput.cursorPosition
            if(textVal.length >0){
                textInput.text = textVal.substring(0,textVal.length-1)
            }
    }

    // Function is called when enter key is pressed
    function handleEnterClicked(){
        enterClicked()
    }

    /**
     * Function is called when caps-button is pressed...
     */
    function caps() {
        if (capsOn) {
            //if caps is on, let's put if off
            setShiftOff();
            internal.capsOn = false;
            capsCLButton.checked = false
            shiftCLButton.enabled = true;
        } else {
            //if caps is off, let's put it on
            setShiftOn();
            internal.capsOn = true
            capsCLButton.checked = true
            shiftCLButton.enabled = false;
        }
    }

    Component.onCompleted: {
        abc.checked = true
        //if special buttons are using images in background texts has to removed
        if (backspaceImage.source != "") backspace.text = "";
        if (enterImage.source != "") enter.text = "";
        if (leftArrowImage.source != "") leftArrow.text = "";
        if (rightArrowImage.source != "") rightArrow.text = "";
        if (okImage.source != "") ok.text = "";
        if (cancelImage.source != "") cancel.text = "";
        if (capsImage.source != "") caps.text = "";
        if (shiftImage.source != "") shiftCLButton.text = "";

        if (!showOkCLButton) ok.visible = false
        if (!showCancelCLButton)  cancel.visible = false
    }

    clip: true;
    smooth: true
    width: 500
    height: 250
    color: "transparent"

    Column {
        id: qwerty

        spacing: buttonSpacing

        Column {
            id: abcCLButtons

            spacing: buttonSpacing

            Row {
                x: buttonWidth*0.5
                spacing: buttonSpacing

                Row {
                    id: row1

                    spacing: buttonSpacing

                    KeyboardButton { text: value; value: KeyboardEngine.abc[0][0]; shiftValue: KeyboardEngine.abc[0][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[1][0]; shiftValue: KeyboardEngine.abc[1][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[2][0]; shiftValue: KeyboardEngine.abc[2][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[3][0]; shiftValue: KeyboardEngine.abc[3][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[4][0]; shiftValue: KeyboardEngine.abc[4][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[5][0]; shiftValue: KeyboardEngine.abc[5][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[6][0]; shiftValue: KeyboardEngine.abc[6][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[7][0]; shiftValue: KeyboardEngine.abc[7][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[8][0]; shiftValue: KeyboardEngine.abc[8][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[9][0]; shiftValue: KeyboardEngine.abc[9][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                }

                KeyboardButton { id: backspace; shiftValue: "";
                    text:"<--";
                    width: buttonWidth; height: buttonHeight;
                    specialImage: backspaceImage;
                    onClicked: handleBackspaceClicked()
                    shiftTextFontSize: keyboard.shiftTextFontSize;
                    shiftTextFontFamily: keyboard.shiftTextFontFamily;
                    shiftTextFontWeight: keyboard.shiftTextFontWeight;
                    shiftTextColor: keyboard.shiftTextColor;
                    onPressAndHold: {

                        deleteTextIncrementally();
                    }
                }
            }

            Row {
                spacing: buttonSpacing
                KeyboardButton { id: capsCLButton; text:"caps"; shiftValue: ""; width: buttonWidth; height: buttonHeight; onClicked: caps(); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor; specialImage: capsImage}
                Row {
                    id: row2

                    spacing: buttonSpacing

                    KeyboardButton { text: value; value: KeyboardEngine.abc[10][0]; shiftValue: KeyboardEngine.abc[10][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[11][0]; shiftValue: KeyboardEngine.abc[11][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[12][0]; shiftValue: KeyboardEngine.abc[12][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[13][0]; shiftValue: KeyboardEngine.abc[13][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[14][0]; shiftValue: KeyboardEngine.abc[14][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[15][0]; shiftValue: KeyboardEngine.abc[15][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[16][0]; shiftValue: KeyboardEngine.abc[16][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[17][0]; shiftValue: KeyboardEngine.abc[17][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }
                    KeyboardButton { text: value; value: KeyboardEngine.abc[18][0]; shiftValue: KeyboardEngine.abc[18][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor }

                }
                KeyboardButton { id: enter; text: value; value: "enter";
                    shiftValue: ""; width: buttonWidth*1.5;
                    height: buttonHeight;
                    onClicked: handleEnterClicked();
                    shiftTextFontSize: keyboard.shiftTextFontSize;
                    shiftTextFontFamily: keyboard.shiftTextFontFamily;
                    shiftTextFontWeight: keyboard.shiftTextFontWeight;
                    shiftTextColor: keyboard.shiftTextColor;
                    specialImage: enterImage}
            }

            Row {
                id: row3
                x: buttonWidth*0.5
                spacing: buttonSpacing
                KeyboardButton { text: value; value: KeyboardEngine.abc[19][0]; shiftValue: KeyboardEngine.abc[19][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[20][0]; shiftValue: KeyboardEngine.abc[20][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[21][0]; shiftValue: KeyboardEngine.abc[21][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[22][0]; shiftValue: KeyboardEngine.abc[22][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[23][0]; shiftValue: KeyboardEngine.abc[23][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[24][0]; shiftValue: KeyboardEngine.abc[24][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[25][0]; shiftValue: KeyboardEngine.abc[25][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[26][0]; shiftValue: KeyboardEngine.abc[26][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[27][0]; shiftValue: KeyboardEngine.abc[27][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[28][0]; shiftValue: KeyboardEngine.abc[28][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
                KeyboardButton { text: value; value: KeyboardEngine.abc[29][0]; shiftValue: KeyboardEngine.abc[29][1]; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            id: row4
            spacing: buttonSpacing
            KeyboardButton { id: shiftCLButton; text: "shift"; width: buttonWidth; height: buttonHeight; onClicked: shift(); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor; specialImage: shiftImage}
            KeyboardButton { id: abc; text:"abc"; width: buttonWidth; height: buttonHeight; onClicked: showAbc(); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
            KeyboardButton { id: numbers; text:"123@"; width: buttonWidth; height: buttonHeight; onClicked: showNumbers(); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
//            KeyboardButton { id: europeans; text:value; value: "<html>&auml;&ouml;&aring;</html>"; width: buttonWidth; height: buttonHeight; onClicked: showEuropeans(); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
            KeyboardButton { id: space; text:""; shiftValue: ""; width: buttonWidth*2; height: buttonHeight; onClicked: keyboardCLButtonClicked(" "); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
            KeyboardButton { id: leftArrow; text:value; value: eval("<html>&larr;</html>");
                shiftValue: eval("<html>&uarr;</html>");
                width: buttonWidth; height: buttonHeight;
                onClicked: {
                    if (shiftOn) {
                        keyboard.upArrowClicked();
                        shift();
                    } else if (capsOn) {
                        keyboard.upArrowClicked();
                    } else {
                        keyboard.leftArrowClicked();
                    }
                }
                shiftTextFontSize: keyboard.shiftTextFontSize;
                shiftTextFontFamily: keyboard.shiftTextFontFamily;
                shiftTextFontWeight: keyboard.shiftTextFontWeight;
                shiftTextColor: keyboard.shiftTextColor; specialImage: leftArrowImage
            }
            KeyboardButton { id: rightArrow; text: value; value: eval("<html>&rarr;</html>");
                shiftValue: eval("<html>&darr;</html>"); width: buttonWidth;
                height: buttonHeight;
                onClicked: {
                    if (shiftOn) {
                        keyboard.downArrowClicked();
                        shift();
                    } else if (capsOn) {
                        keyboard.downArrowClicked();
                    } else {
                        keyboard.rightArrowClicked();
                    }
                }
                shiftTextFontSize: keyboard.shiftTextFontSize;
                shiftTextFontFamily: keyboard.shiftTextFontFamily;
                shiftTextFontWeight: keyboard.shiftTextFontWeight;
                shiftTextColor: keyboard.shiftTextColor; specialImage: rightArrowImage
            }
            KeyboardButton { id: apostrophe; text: value; value: "'"; shiftValue: "\""; width: buttonWidth; height: buttonHeight; onClicked: keyboardCLButtonClicked(text); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor}
            KeyboardButton { id: ok; text:"ok"; width: buttonWidth;  height: buttonHeight; onClicked: keyboard.okCLButtonClicked(); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor; specialImage: okImage}
            KeyboardButton { id: cancel; text:"cancel"; width: buttonWidth;  height: buttonHeight; onClicked: keyboard.cancelCLButtonClicked(); shiftTextFontSize: keyboard.shiftTextFontSize; shiftTextFontFamily: keyboard.shiftTextFontFamily; shiftTextFontWeight: keyboard.shiftTextFontWeight; shiftTextColor: keyboard.shiftTextColor; specialImage: cancelImage}
        }
    }
}
