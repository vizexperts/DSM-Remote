import QtQuick 2.1
import QtQuick.Layouts 1.0

MouseArea{

    //! Component that eats any events that are not processed by any other
    //! any element in the parent so that they dont go to earth

    id: backgroundMouseArea
    anchors.fill: parent
    acceptedButtons:  Qt.AllButtons
    onWheel: wheel.accepted = true
    onClicked: {
        //! do nothing
    }
}
