import QtQuick 2.1

Rectangle {
    id: flickabelTimeRect
    property int hours: hourList.currentIndex
    property int minutes: minuteList.currentIndex
    property int seconds: secondList.currentIndex

    signal changed()

    onHoursChanged: if(!changingTime) changed()
    onMinutesChanged: if(!changingTime) changed()
    onSecondsChanged: if(!changingTime) changed()

    height: displayRect.height+10
    width: displayRect.width+10
    radius: 5
    color: rootStyle.colorWhenDefault

    property bool changingTime: false

    function setTime(dateTime){
        changingTime = true
        hourList.currentIndex = dateTime.getHours()
        minuteList.currentIndex = dateTime.getMinutes()
        secondList.currentIndex = dateTime.getSeconds()
        changingTime = false
    }

    Rectangle{
        id:displayRect
        width: hourList.width + minuteList.width + secondList.width +10
        height: hourList.height+30*rootStyle.uiScale
        color: "transparent"
        anchors.centerIn: parent

        VizLabel{
            text: "h"
            anchors.horizontalCenter: hourList.horizontalCenter
            anchors.bottom: hourList.top
            fontSize: rootStyle.fontSize + 3
        }

        Spinner{
            id: hourList
            width: 30*rootStyle.uiScale
            height: 100*rootStyle.uiScale
            model: 24
            itemHeight: 26*rootStyle.uiScale
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            delegate: VizLabel{
                text: index
                scale: (index == hourList.currentIndex) ? 1 : 0.8
                textColor: (index == hourList.currentIndex) ? "white" : rootStyle.textColor
            }
        }

        VizLabel {
            text: "m"
            anchors.horizontalCenter: minuteList.horizontalCenter
            anchors.bottom: minuteList.top
            fontSize: rootStyle.fontSize + 3
        }
        Spinner{
            id: minuteList
            model: 60
            width: 30*rootStyle.uiScale
            height: 100*rootStyle.uiScale
            anchors.left: hourList.right
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            itemHeight: 26*rootStyle.uiScale
            delegate: VizLabel{
                text: index
                scale: (index == minuteList.currentIndex) ? 1 : 0.8
                textColor: (index == minuteList.currentIndex) ? "white" : rootStyle.textColor
            }
        }


        VizLabel{
            text: "s"
            anchors.horizontalCenter: secondList.horizontalCenter
            anchors.bottom: secondList.top
            fontSize: rootStyle.fontSize + 3
        }

        Spinner{
            id: secondList
            model: 60
            width: 30*rootStyle.uiScale
            height: 100*rootStyle.uiScale
            anchors.left: minuteList.right
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            itemHeight: 26*rootStyle.uiScale
            delegate: VizLabel{
                text: index
                scale: (index == secondList.currentIndex) ? 1 : 0.8
                textColor:  (index == secondList.currentIndex) ? "white" : rootStyle.textColor
            }
        }
    }
}
