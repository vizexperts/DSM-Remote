import QtQuick 2.1
import QtQuick.Layouts 1.0
//import QtWebKit 1.0

/**
  * Convenience Component to show Attributes Tab for objects
 **/
ColumnLayout {
    id: container
    spacing: 5


    // Properties
    // html description of object
    property string description

    property  int mouseButtonClicked: Qt.RightButton
    property int webHeight
    property int webWidth

    signal okButtonPressed()
    signal cancelButtonPressed()

    onDescriptionChanged: {
        webWidth = web.contentsSize.width
        webHeight = web.contentsSize.height
    }

    VizLabel{
        id: message
        text: "No associated attributes."
        anchors.horizontalCenter: parent.horizontalCenter
        visible: ((description == "") && (attributesList.count == 0))
    }

    ListView{
        id: attributesList
        width: parent.width
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.minimumHeight: 35*rootStyle.uiScale*5
        model: if(typeof(attributesModel) != "undefined") attributesModel
        clip: true
        spacing: 5
        boundsBehavior: Flickable.StopAtBounds
        VizScrollBar{
            target: attributesList
        }

        delegate:VizLabelTextEdit{
            id: labelTextEdit
            isReadOnly: !(mouseButtonClicked == Qt.RightButton)
            width: parent.width-15
            anchors.left: parent.left
            okButtonVisible: false
            Component.onCompleted: {
                if(name == "Latitude")
                {
                    value = rootStyle.latDegDecToGlobal(value);
                }
                else if(name == "Longitude")
                {
                    value = rootStyle.longDegDecToGlobal(value);
                }
                else if(type == "Integer"){
                    labelTextEdit.validator = intValidator
                }
                else if(type == "Double"){
                    labelTextEdit.validator = doubleValidator
                    doubleValidator.decimals = precision
                }
                else{
                    maximumLength = precision
                }
            }
            //labelText: name + "("+type+")"
            labelText: name + " "
            lineEditText: value
            labelWidth: 80*rootStyle.uiScale
            onLineEditTextChanged: {
                okCancelButtonRow.enabled = true
                if(value != lineEditText){
                    value = lineEditText
                }
            }

            Keys.onTabPressed: {
                if(index == attributesList.count-1){
                    attributesList.currentIndex = 0
                }else{
                    attributesList.currentIndex = index + 1
                }
                attributesList.currentItem.giveFocus()
            }

            IntValidator{
                id: intValidator
            }
            DoubleValidator{
                id: doubleValidator
                decimals: 4
                notation: DoubleValidator.ScientificNotation
            }
        }
    }

    OkCancelButtonRow{
        id: okCancelButtonRow
        visible: (!message.visible && (mouseButtonClicked == Qt.RightButton))
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.right: parent.right
        onOkButtonPressed: {
            enabled = false
            container.okButtonPressed()
        }
        onCancelButtonPressed: {
            enabled = false
            container.cancelButtonPressed()
        }
    }

//    Rectangle {
//        id: contents
//        anchors.top: attributesList.bottom
//        height: web.height
//        width: web.width

//        Flickable {
//            id: flickable1
//            width: web.width
//            height: web.height
//            flickableDirection: Flickable.VerticalFlick
//            clip: true

//            WebView {
//                id: web
//                preferredWidth: webWidth;
//                preferredHeight: webHeight
//                smooth: true
//                settings.pluginsEnabled: true
//                html: description
//            }
//        }
//    }
}
