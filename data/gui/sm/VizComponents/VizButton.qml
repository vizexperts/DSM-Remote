import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "javascripts/functions.js" as Functions
import QtGraphicalEffects 1.0

/*
    \Component VizButton
    \brief button with some defined and globally bound style
    \example
        VizButton{
            id: button
            iconSource: rootStyle.getIconPath("circle")
        }
*/

Button{

    /*
        \property bool backGroundVisible
        \brief Set/Get Is button background visible
    */
    property bool backGroundVisible: true
    property color backgroundColor: rootStyle.colorWhenDefault
    property bool flatButton: false
    style: ButtonStyle{
        background: Item{
            implicitWidth: rootStyle.buttonIconSize
            implicitHeight: rootStyle.buttonIconSize

            Rectangle{
                id: bgrect
                anchors.fill: parent
                visible: backGroundVisible
                color: backgroundColor
                radius: Functions.countRadius(rootStyle.roundness, width, height, 0, 1)
                border.color: rootStyle.borderColor
                border.width: rootStyle.borderWidth
                gradient: if(!flatButton)  rootStyle.gradientWhenDefault
            }
            states: [
                State {
                    name: "pressed"
                    when: control.pressed && control.enabled
                    PropertyChanges {target: bgrect; gradient: if(!flatButton) rootStyle.gradientWhenPressed}
                    PropertyChanges{target: bgrect; border.color: rootStyle.borderColorInnerWhenPressed}
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenPressed}
                    //                    PropertyChanges{target: root; cursorShape: Qt.PointingHandCursor}
                },
                State {
                    name: "selected"
                    when: control.checked && control.enabled
                    PropertyChanges {target: bgrect; gradient: if(!flatButton) rootStyle.gradientWhenSelected}
                    PropertyChanges { target: bgrect; border.color: rootStyle.borderColorWhenSelected }
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenSelected}
                },
                State {
                    name: "selectedAndEntered"
                    when: control.checked && control.enabled && control.hovered
                    PropertyChanges {target: bgrect; gradient: if(!flatButton) rootStyle.gradientWhenSelected}
                    PropertyChanges { target: bgrect; border.color: rootStyle.borderColorWhenSelected }
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenSelected}
                    //                    PropertyChanges{target: root; cursorShape: Qt.PointingHandCursor}
                },/*
                State{
                    name: "entered"
                    when: control.hovered && control.enabled
                    PropertyChanges{target: bgrect; gradient: if(!flatButton) rootStyle.gradientWhenHovered}
                    //                    PropertyChanges{target: root; cursorShape: Qt.PointingHandCursor}
                    PropertyChanges { target: bgrect; border.color: rootStyle.borderColorWhenHovered }
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenHovered}
                },*/
                State{
                    name: "focused"
                    when: control.activeFocus && control.enabled
                },
                State{
                    name: "disabled"
                    when: !control.enabled
                    PropertyChanges{target: bgrect; opacity: 0.6}
                    PropertyChanges{target: bgrect; gradient: "undefined"}
                }
            ]
        }
        label: Item{
            implicitWidth: row.implicitWidth + 15
            implicitHeight: row.implicitHeight + 10
            Row{
                id: row
                anchors.centerIn: parent
                spacing: 5
                Image{
                    id: image
                    antialiasing: true
                    source: control.iconSource
                    anchors.verticalCenter: parent.verticalCenter
                    width: (iconSource == "") ? 0 :rootStyle.buttonIconSize
                    height: width
                }
                TextEdit{
                    id: labelText
                    readOnly: true
                    renderType: TextEdit.NativeRendering
                    anchors.verticalCenter: parent.verticalCenter
                    text: control.text
                    color: rootStyle.textColor
                    font.family: rootStyle.fontFamily
                    font.pixelSize: rootStyle.fontSize
                    font.weight: rootStyle.fontWeight
                }
            }

            Glow{
                id: glow
                anchors.fill: row
                visible: false
                enabled: visible
                anchors.centerIn: parent
                source: row
                radius: 5
                samples: 17
                color: rootStyle.colorWhenSelected
            }
            states: [
                State {
                    name: "pressed"
                    when: control.pressed && control.enabled
                },
                State {
                    name: "selected"
                    when: control.checked && control.enabled
                    PropertyChanges{target: labelText; color: rootStyle.colorWhenDefault}
                },
//                State{
//                    name: "entered"
//                    when: control.hovered && control.enabled
//                    PropertyChanges{target: glow; visible: true}
//                },
                State{
                    name: "focused"
                    when: control.activeFocus && control.enabled
                },
                State{
                    name: "disabled"
                    when: !control.enabled
                    PropertyChanges{target: row; opacity: 0.6}
                }

            ]
        }
    }

    signal pressAndHold()

    Timer {
        id: longPressTimer

        interval: 100 // press and hold interval
        repeat: true
        running: false

        onTriggered: {
            pressAndHold()
        }
    }


    onPressedChanged: {
        if ( pressed ) {
            longPressTimer.running = true;
        } else {
            longPressTimer.running = false;
        }
    }
}
