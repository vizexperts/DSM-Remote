import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "javascripts/functions.js" as Functions


Slider {
    id: slider
    width: 150
    height: 8

    style: SliderStyle{
        handle: Rectangle{
            width: 24
            height: 24
            radius: width/2
            gradient: rootStyle.gradientWhenDefault
            Rectangle{
                height: 12
                width: 12
                radius: width/2
                gradient: rootStyle.gradientWhenChecked
                anchors.centerIn: parent
            }
        }

        groove: Item {
            implicitWidth : 100
            implicitHeight: 8
            Rectangle{
                anchors.fill: parent
                color: rootStyle.colorWhenDefault
                gradient: rootStyle.gradientWhenDefault
                radius: Functions.countRadius(rootStyle.roundness, width, height, 0, 1)
                Rectangle{
                    id: left
                    anchors.verticalCenter: parent.verticalCenter
                    x: 2
                    height: parent.height - 4
                    width: (control.value - control.minimumValue)/(control.maximumValue - control.minimumValue) * (parent.width - 4)
                    gradient: rootStyle.blueGradient
                }
            }
        }

    }

    property string unit
    property alias labelVisible: label.visible
    property alias labelText:label.text;
    property bool  labelMovable: false
    function labelMovableFunc()
    {
        if(labelMovable)
        {
            if(orientation==Qt.Horizontal)
            {
                return (slider.x+4+(slider.width-24)*(value/(maximumValue-minimumValue)))
            }
        }
        return 0;
    }

    TextEdit{
        id: label
        color: rootStyle.textColor
        anchors.bottom: parent.top
        anchors.bottomMargin:10
        text: value + " " + unit
        readOnly: true
        font.family: rootStyle.fontFamily
        font.pixelSize:  rootStyle.fontSize
        font.weight: rootStyle.fontWeight
        x:labelMovableFunc();
    }
}
