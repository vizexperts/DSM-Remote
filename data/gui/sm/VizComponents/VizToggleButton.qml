import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "javascripts/functions.js" as Functions

Button{
    property bool backGroundVisible: true

    id:but
    property bool on:false

    style: ButtonStyle{
        //property VizStyle vizStyle: VizStyle {}

        background: Item{
            implicitWidth: 25
            implicitHeight: 25

            id: bg
            Rectangle{
                id: bgrect
                anchors.fill: parent
                visible: backGroundVisible
                color: rootStyle.colorWhenDefault
                radius: Functions.countRadius(rootStyle.roundness, width, height, 0, 1)
                border.color: rootStyle.borderColor
                border.width: rootStyle.borderWidth
                gradient: rootStyle.gradientWhenDefault

            }
            states: [
                State {
                    name: "pressed"
                    when: but.on && control.enabled
                    PropertyChanges {target: bgrect; gradient: rootStyle.gradientWhenPressed}
                    PropertyChanges{target: bgrect; border.color: rootStyle.borderColorInnerWhenPressed}
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenPressed}
                    //PropertyChanges{target: root; cursorShape: Qt.PointingHandCursor}
                },
                State {
                    name: "selected"
                    when: control.checked && control.enabled
                    PropertyChanges {target: bgrect; gradient: rootStyle.gradientWhenSelected}
                    PropertyChanges { target: bgrect; border.color: rootStyle.borderColorWhenSelected }
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenSelected}
                },
                State {
                    name: "selectedAndEntered"
                    when: control.checked && control.enabled && control.hovered
                    PropertyChanges {target: bgrect; gradient: rootStyle.gradientWhenSelected}
                    PropertyChanges { target: bgrect; border.color: rootStyle.borderColorWhenSelected }
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenSelected}
                    //PropertyChanges{target: root; cursorShape: Qt.PointingHandCursor}
                },/*
                State{
                    name: "entered"
                    when: control.hovered && control.enabled
                    PropertyChanges{target: bgrect; gradient: rootStyle.gradientWhenHovered}
                    //PropertyChanges{target: root; cursorShape: Qt.PointingHandCursor}
                    PropertyChanges { target: bgrect; border.color: rootStyle.borderColorWhenHovered }
                    PropertyChanges { target: bgrect; color: rootStyle.colorWhenHovered}
                },*/
                State{
                    name: "focused"
                    when: control.activeFocus && control.enabled
                },
                State{
                    name: "disabled"
                    when: !control.enabled
                    PropertyChanges{target: bgrect; opacity: 0.6}
                }
            ]
        }
        label: Item{
            implicitWidth: row.implicitWidth + 15
            implicitHeight: row.implicitHeight + 10
            Row{
                id: row
                anchors.centerIn: parent
                spacing: 5
    //            implicitWidth: childrenRect.width
    //            implicitHeight: childrenRect.height
                Image{
                    id: image
                    antialiasing: true
                    source: control.iconSource
                    anchors.verticalCenter: parent.verticalCenter
                    width: (iconSource == "") ? 0 :rootStyle.buttonIconSize
                    height: width
                }
                TextEdit{
                    id: labelText
                    readOnly: true
                    renderType: TextEdit.NativeRendering
                    anchors.verticalCenter: parent.verticalCenter
                    text: control.text
                    color: rootStyle.textColor
                    font.family: rootStyle.fontFamily
                    font.pixelSize: rootStyle.fontSize
                    font.weight: rootStyle.fontWeight
                }
            }
            states: [
                State {
                    name: "pressed"
                    when: control.pressed && control.enabled
                },
                State {
                    name: "selected"
                    when: control.checked && control.enabled
                    PropertyChanges{target: labelText; color: rootStyle.colorWhenDefault}
                }/*,
                State{
                    name: "entered"
                    when: control.hovered && control.enabled
                }*/,
                State{
                    name: "focused"
                    when: control.activeFocus && control.enabled
                },
                State{
                    name: "disabled"
                    when: !control.enabled
                    PropertyChanges{target: row; opacity: 0.6}
                }

            ]
        }

    }

    onClicked: {
        but.on=!but.on;
    }


}
