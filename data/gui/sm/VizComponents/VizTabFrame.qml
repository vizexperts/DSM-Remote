import QtQuick 2.1
import QtQuick.Layouts 1.0
import "javascripts/functions.js" as Functions

/*
     \Component VizTabFrame
     \biref Create a tab view.
 */
Item {
    id: tabWidget
    Layout.minimumWidth: tabbarItem.width
    Layout.minimumHeight: tabbarItem.height
    Layout.fillWidth: true
    height: 100
    property int current: 0
    property int count: stack.children.length
    property bool frame: true
    property bool tabsVisible: true

    property int position: position_North

    readonly property int position_North : 0
    readonly property int position_South : 1

    default property alias tabs : stack.children
    property Item tabBar: tabbarItem
    property alias tabBarAlignment: tabbarItem.tabBarAlignment
    property alias tabEnabled: tabbarItem.enabled
    property alias tabHeight: tabbarItem.tabHeight
    property alias tabWidth: tabbarItem.tabItemWidth

    onCurrentChanged: __setOpacities()
    Component.onCompleted: __setOpacities()

    function __setOpacities() {
        for (var i = 0; i < stack.children.length; ++i) {
            stack.children[i].opacity = (i == current ? 1 : 0)
            stack.children[i].enabled = (i == current ? true : false)
            stack.children[i].isCurrent = (i == current ? true : false)
        }

        Layout.minimumHeight = tabbarItem.height + stack.children[current].Layout.minimumHeight + 40
        Layout.minimumWidth = Math.max(tabbarItem.width, stack.children[current].Layout.minimumWidth + 20)
    }

    Component {
        id: tabcomp
        VizTab {}
    }

    function addTab(component, title) {
        var tab = tabcomp.createObject(this);
        component.createObject(tab)
        tab.parent = stack
        tab.title = title
        __setOpacities()
        return tab
    }

    function removeTab(id) {
        var tab = tabs[id]
        tab.destroy()
        if (current > 0)
            current-=1
    }

    VizTabBar {
        id: tabbarItem
        tabFrame: tabWidget
        anchors.top: tabWidget.top
        anchors.left: tabWidget.left
    }
    Rectangle {
        id: frameitem
        border.width: 2
        border.color: rootStyle.colorWhenSelected
        width : parent.width
        anchors.top: tabbarItem.bottom
        anchors.bottom: parent.bottom
        color: "transparent"

        property int frameWidth: 5

        Item {
            id: stack
            anchors.fill: parent
            anchors.margins: (frame ? frameitem.frameWidth : 0)
        }

//        anchors.topMargin: tabbarItem && tabsVisible && position == position_North ? Math.max(0, tabbarItem.height ) : 0
        anchors.topMargin: 17

        states: [
            State {
                name: "South"
                when: position == position_South && tabbarItem!= undefined
                PropertyChanges {
                    target: frameitem
                    anchors.topMargin: 0
                    anchors.bottomMargin: tabbarItem ? tabbarItem.height : 0
                }
                AnchorChanges {
                    target: tabbarItem
                    anchors.top: frameitem.bottom
                    anchors.bottom: undefined
                }
            }
        ]
    }

}
