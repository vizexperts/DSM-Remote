import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0

Rectangle{
    id: comboBox
    height: label.height
    Layout.minimumWidth: label.width + 20
    color: rootStyle.colorWhenDefault
    border.color: rootStyle.borderColor
    border.width: 2
    state : "INACTIVE"
    focus: false

    onActiveFocusChanged: {
        if(!activeFocus)
            state = "INACTIVE"
    }

    onFocusChanged: {
        if(!focus)
            state = "INACTIVE"
    }

    Connections{
        target: restOfArea
        onClicked: {
            (comboBox.state = "INACTIVE")
        }
    }

    states: [
        State
        {
            name: "ACTIVE"
//            when: comboBox.focus
            PropertyChanges{
                target: comboBox
                focus: true
            }
            PropertyChanges{
                target: list
                height: (list.count>6)? label.height*7 : label.height*(list.count)
            }
            PropertyChanges{
                target: restOfArea
                enabled: true
            }
            PropertyChanges {
                target: list
                visible: true
            }
            ParentChange{
                target: listBackground
                parent: {
                    var parentT = comboBox
                    while(parentT.parent){
                        parentT = parentT.parent
                    }
                    return parentT
                }
            }
            ParentChange{
                target: list
                parent: {
                    var parentT = comboBox
                    while(parentT.parent){
                        parentT = parentT.parent
                    }
                    return parentT
                }
            }
        },

        State
        {
            name: "INACTIVE"
            when: !comboBox.focus

            PropertyChanges{
                target: comboBox
                z: 0
            }

            PropertyChanges{
                target: list
                height: 0
            }
            PropertyChanges{
                target: restOfArea
                enabled: false
            }
            PropertyChanges {
                target: list
                visible: false
            }
        }
    ]

    signal selectOptionPrivate(string name, string uID)

    onSelectOptionPrivate:  {
        label.text = name
        label.focus = false
        comboBox.state = "INACTIVE"
        selectedUID = uID
        selectOption(uID)
    }

    signal selectOption(string uID)
    property alias listModel : list.model
    property alias listContentWidth : list.contentWidth
    property string headerText : ""
    property alias value : label.text
    property string selectedUID
    property bool inverted: false

    property bool modelDefined: false

    onListModelChanged: {
        if(listModel !== "undefined" && !modelDefined && list.count > 0){
            if(typeof listModel['get'] === 'function'){
                value = listModel.get(0).name
                selectedUID = listModel.get(0).uID
            }
            else{
                value = listModel[0].name
                selectedUID = listModel[0].uID
            }
            modelDefined = true
        }
    }
    function getDefaultVal()
    {
        //For Setting Default First Value in ComboBox and Call automatically selectOptionPrivate
        var FirstVal, FirstId;
        if(listModel !== "undefined" && !modelDefined && list.count > 0)
        {
            if(typeof listModel['get'] === 'function')
            {
                FirstVal = listModel.get(0).name
                FirstId = listModel.get(0).uID
            }
            else
            {
                FirstVal = listModel[0].name
                FirstId = listModel[0].uID
            }
            selectOptionPrivate(FirstVal,FirstId)
        }
    }

    //for aligning the text of list element in the dropdown list
    property int listElementAlignment: Qt.AlignCenter

    Behavior on width{
        PropertyAnimation{
            duration: 600
            easing.type: Easing.OutBack
        }
    }

    Rectangle{
        id: labelRect
        height: label.height
        width: parent.width
        anchors.top: label.top
        visible: list.visible
        color: rootStyle.colorWhenSelected
    }

    VizLabel{
        id: label
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        color: (comboBox.state == "INACTIVE")? "transparent" : labelRect.color
        textColor: (comboBox.state == "INACTIVE") ? rootStyle.textColor : rootStyle.colorWhenDefault
        textAlignment: listElementAlignment
    }

    Image {
        id: icon
        source: (comboBox.state == "INACTIVE") ? rootStyle.getIconPath("down") : rootStyle.getIconPath("up")
        width:  15*rootStyle.uiScale
        height: 10*rootStyle.uiScale
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.verticalCenter: label.verticalCenter
    }

    MouseArea{
        anchors.fill: labelRect
        onClicked: comboBox.state = (comboBox.state == "INACTIVE") ? "ACTIVE" : "INACTIVE"
    }

    Rectangle{
        id: listBackground
        anchors.fill: list
        color: rootStyle.colorWhenDefault
        border.color: rootStyle.borderColor
        border.width: 2
    }

    ListView{
        Behavior on height{
            PropertyAnimation{
                duration: 600
                easing.type: Easing.OutBack
            }
        }
        id: list
        visible: false
        width: comboBox.width
        states: State {
            when: inverted
            AnchorChanges{
                target: list
                anchors.bottom: label.top
                anchors.top: undefined
            }
        }

        anchors.top: labelRect.bottom
        anchors.left: labelRect.left
        clip: true
        snapMode: ListView.SnapToItem
        VizScrollBar{
            target: list
        }

        boundsBehavior: Flickable.StopAtBounds
        delegate: VizLabel{
            Component.onCompleted: {
                textAlignment: listElementAlignment
                comboBox.width = Math.max(textconWidth + 10,comboBox.width)
            }

            text: name
            width: ListView.view.width
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    selectOptionPrivate(name, uID)
                }
            }
        }
    }
}
