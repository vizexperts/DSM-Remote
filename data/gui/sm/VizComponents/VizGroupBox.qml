import QtQuick 2.1
import QtQuick.Layouts 1.0

Item {
    id: groupbox

    width: adjustToContentSize ? Math.max(200, contentWidth + background.leftMargin + background.rightMargin) : 100
//    height: adjustToContentSize ? contentHeight + background.topMargin + background.bottomMargin : 100
    Layout.preferredHeight: contentHeight + margins
    default property alias data: content.data

    property string title
    property bool checkable: false
    property alias contentWidth: content.implicitWidth
    property alias contentHeight: content.implicitHeight
    property double contentOpacity: (checkable && !check.checked)  ? 0.6 : 1

    Layout.minimumHeight: contentHeight + margins
    Layout.minimumWidth: contentWidth + 20
    Layout.fillHeight: true
    Layout.fillWidth: true

    //! Margins
    readonly property int margins: background.topMargin + background.bottomMargin

    signal clicked()

    Rectangle{
        anchors.left: background.left
        height: background.height
        anchors.top: background.top
        width: 2
        color: rootStyle.colorWhenSelected
    }
    Rectangle{
        anchors.right: background.right
        anchors.top: background.top
        height: background.height
        width: 2
        color: rootStyle.colorWhenSelected
    }
    Rectangle{
        anchors.bottom: background.bottom
        anchors.left: background.left
        width: background.width
        height: 2
        color: rootStyle.colorWhenSelected
    }
    Rectangle{
        anchors.top: background.top
        anchors.left: background.left
        anchors.right: label.left
        height: 2
        color: rootStyle.colorWhenSelected
    }
    Rectangle{
        anchors.top: background.top
        anchors.left: label.right
        anchors.right: background.right
        height: 2
        color: rootStyle.colorWhenSelected
    }

    Item{
        id: background
        anchors.fill: parent
        anchors.margins: 5
        anchors.topMargin: title.length > 0 || checkable ? 15*rootStyle.uiScale : 4*rootStyle.uiScale
        property int topMargin: title.length > 0 || checkable ? 35*rootStyle.uiScale : 4*rootStyle.uiScale
        property int bottomMargin: 10*rootStyle.uiScale
        property int leftMargin: 4*rootStyle.uiScale
        property int rightMargin: 4*rootStyle.uiScale
    }

    property Item checkbox: check
    property alias checked: check.checked
    property bool adjustToContentSize: false // Resizes groupbox to fit contents.
    // Note when using this, you cannot anchor children

    VizCheckBox{
        id: check
        checked: true
        anchors.top:  parent.top
        anchors.left: parent.left
        anchors.leftMargin: 20*rootStyle.uiScale
        width: parent.checkable ?  rootStyle.buttonIconSize : 0
        visible: parent.checkable
        text: ""
        onClicked: {
            groupbox.clicked()
        }
    }

    VizLabel{
        id: label
        anchors.left: check.right
        anchors.leftMargin: 10
        anchors.verticalCenter: check.verticalCenter
        text: title
    }

    GridLayout {
        id:content
        z: 1
        focus: true
        opacity: contentOpacity
        anchors.topMargin: background.topMargin
        anchors.leftMargin: 8
        anchors.rightMargin: 8
        anchors.bottomMargin: 8
        anchors.fill: parent
        enabled: (!checkable || checkbox.checked)
    }
}
