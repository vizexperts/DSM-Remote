import QtQuick 2.1
import "javascripts/functions.js" as Functions

Rectangle {
    id: scrollBar
    property variant target
    property bool interactive: true

    clip: true
    color: rootStyle.colorWhenDefault
    gradient: rootStyle.gradientWhenDefault
    width: 15
    radius: 4
    smooth: true

    anchors.bottom: target.bottom
    anchors.left: target.left
    anchors.right: target.right
    anchors.bottomMargin: 5

    //visible: (track.height == slider.height) ? false : true //TODO: !visible -> width: 0 (but creates a binding loop)


    states: [
        State { name: "nothing"; },
        State { name: "disabled"; when: track.height == slider.height }
    ]
    transitions: [
        Transition { to: "disabled"; //reversible: true;
            SequentialAnimation {
                NumberAnimation { target: scrollBar; property: "opacity"; to: 0; duration: 500; }
                PropertyAction { target: scrollBar; property: "visible"; value: false; }
            }
        },
        Transition { to: "*";
            PropertyAction { target: scrollBar; property: "visible"; value: true; }
            NumberAnimation { target: scrollBar; property: "opacity"; to: 1; duration: 500; }
        }
    ]

    Timer {
        property int scrollAmount

        id: timer
        repeat: true
        interval: 20
        onTriggered: {
            target.contentY = Math.max(0, Math.min(target.contentY + scrollAmount,
                                          target.contentHeight - target.height));
            scrollAmount = 0;
        }
    }

    Item {
        id: track
        anchors.top: parent.top
        anchors.bottom: parent.bottom;
        width: parent.width

        MouseArea {
            anchors.fill: parent
            enabled: interactive
            visible: interactive
            onPressed: {
                timer.scrollAmount = target.height * (mouseY < slider.y ? -1 : 1) // scroll by a page
                timer.running = true;
            }
            onReleased: {
                timer.running = false;
            }
        }

        Rectangle {
            id:slider
            color: rootStyle.colorWhenSelected
            smooth: true
            width: parent.width - 8
            anchors.horizontalCenter: parent.horizontalCenter
            radius: 4

            anchors.bottom: (target.visibleArea.yPosition > 1)? parent.bottom: undefined
            height: {
                if (target.visibleArea.yPosition<0)         // Oberer Rand
                    Math.max(30, Math.min(target.height / target.contentHeight * track.height, track.height-y) +target.height * target.visibleArea.yPosition)
                else                                        // Mittelbereich
                    Math.min(target.height / target.contentHeight * track.height, track.height-y)
                }
            y: Math.max(0,Math.min(track.height-30, target.visibleArea.yPosition * track.height));

            MouseArea {
                anchors.fill: parent
                drag.target: parent
                drag.axis: Drag.YAxis
                drag.minimumY: 0
                drag.maximumY: track.height - height
                enabled: interactive
                visible: interactive

                onPositionChanged: {
                    if (pressedButtons == Qt.LeftButton) {
                        target.contentY = slider.y * target.contentHeight / track.height
                    }
                }
            }
        }
    }
}
