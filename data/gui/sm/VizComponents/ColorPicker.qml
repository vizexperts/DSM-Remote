import QtQuick 2.1
import QtQuick.Layouts 1.0
import "javascripts/functions.js" as Functions

Rectangle {
    id: topRect

    objectName: "colorPicker"
    width: mainLayout.implicitWidth + 10
    height: mainLayout.implicitHeight + 20
    color: rootStyle.colorWhenDefault
    border.color: rootStyle.borderColor
    border.width: rootStyle.borderWidth
    radius: Functions.countRadius(rootStyle.roundness, width, height, 0, 1)

    signal colorSelected(color selectedColor, double alpha)

    visible: false

    property color currentColor

//    Behavior on width{
//        NumberAnimation{
//            duration: 100
//        }
//    }

//    Behavior on height{
//        NumberAnimation{
//            duration: 100
//        }
//    }

    ListModel{
        id: colorModel
        ListElement{name : "red"}
        ListElement{name : "green"}
        ListElement{name : "yellow"}
        ListElement{name : "pink"}
        ListElement{name : "black"}
        ListElement{name : "orange"}
        ListElement{name : "aqua"}
        ListElement{name : "brown"}
        ListElement{name : "blue"}
        ListElement{name : "white"}
        ListElement{name : "lime"}
        ListElement{name : "lightskyblue"}
        ListElement{name : "grey"}
        ListElement{name : "crimson"}
        ListElement{name : "seagreen"}
        ListElement{name : "fuchsia"}
    }

    ColumnLayout{
        id:mainLayout
        anchors{
            left: parent.left
            margins: 5
            verticalCenter: parent.verticalCenter
        }

        GridLayout{
            id: grid
            columns: 4
            Layout.alignment: Qt.AlignCenter

            Repeater{
                model: colorModel
                delegate:
                    Rectangle {
                    width: rootStyle.buttonIconSize
                    height: width
                    color: name
                    property int borderWidth: (currentColor == color)? 5 : 0
                    border.width: borderWidth
                    border.color: rootStyle.colorWhenSelected
                    Behavior on borderWidth{
                        NumberAnimation{
                            duration: 100
                        }
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: currentColor = parent.color
                        onDoubleClicked: {
                            colorSelected(currentColor, advancedPicker.colorValue)
                            //topRect.scale = 0.0
                        }
                    }
                }
            }
        }

        VizButton{
            id: advancedButton
            //        width: grid.width
            text: "Advanced Picker"
            checkable:true
            Layout.alignment: Qt.AlignCenter
        }

//        RowLayout{
//            Layout.fillWidth: true
//            Layout.alignment: Qt.AlignCenter
//            VizButton{
//                id: okButton
//                text: "  Ok  "
//                onClicked: {
//                    colorSelected(currentColor, advancedPicker.opacityValue)
//                    topRect.scale = 0.0
//                }
//            }
//            VizButton{
//                id: cancelButton
//                text: "Cancel"
//                onClicked: {
//                    topRect.scale = 0.0
//                }
//            }
//        }
    }


    AdvancedColorPicker{
        id: advancedPicker
        anchors.left: mainLayout.right
        anchors.leftMargin: 10
        anchors.top: mainLayout.top
        enabled: false
        onColorValueChanged: {
//            console.log("onColorValueChanged : " + colorValue)
//            console.log("onColorValueChanged : " + colorValue.opacity)
            currentColor = colorValue
        }
        onEnabledChanged: {
            if(enabled){
                setColor(currentColor, currentColor.a)
            }
        }
    }


    states: [
        State {
            name: "ADVANCED_PICKER"
            when: advancedButton.checked
            PropertyChanges {
                target: topRect
                width : mainLayout.width + 330
                //                height : 220
            }
            PropertyChanges{
                target: advancedPicker
                enabled: true
            }
        }
    ]
}
