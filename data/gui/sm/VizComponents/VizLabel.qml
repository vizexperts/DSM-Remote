import QtQuick 2.1
import QtQuick.Layouts 1.0

Rectangle {
    id: rectangle

    property alias text: text.text

    property int textAlignment: Qt.AlignCenter
    Layout.minimumWidth: textWidth
    Layout.minimumHeight: text.height

    property real leftMargin: 0
    property real rightMargin: 15
    property alias textColor: text.color
    property real fontSize: rootStyle.fontSize
    property string fontStyle: rootStyle.fontFamily
    property real fontWeight: rootStyle.fontWeight
    property alias wrapMode: text.wrapMode
    property alias textWidth : text.width
    property alias textconWidth : text.contentWidth

    function select(selection){

        if(selection === ""){
            text.deselect()
            return
        }

        var sel = selection.toLowerCase()
        var selStart = text.text.toString().toLowerCase().search(sel)
        if(selStart !== -1){
            var selEnd = selStart + sel.length
            text.select(selStart, selEnd)
        }
        else{
            text.deselect()
        }
    }

    width: text.width + 15
    height: text.height + 15
    color: "transparent"
    smooth: true
    clip: true
    property bool marquee: false

    Component.onCompleted: {
        if(rectangle.width < (text.width + 15)){
            if((textAlignment == Qt.AlignLeft) && marquee){
                marqueeAnimation.target = rectangle
                marqueeAnimation.property = "leftMargin"
                marqueeAnimation.from = 15
                marqueeAnimation.to = -(text.width - rectangle.width + 15)
                marqueeAnimation.duration = (marqueeAnimation.from - marqueeAnimation.to)*30
                marqueeAnimation.start()
            }
        }
    }

    NumberAnimation{
        id: marqueeAnimation
        loops : Animation.Infinite
    }


    TextEdit{
        id: text
        enabled: false
        readOnly: true
        text: "Label"
        anchors.horizontalCenter: if(textAlignment == Qt.AlignCenter) rectangle.horizontalCenter
        anchors.left: if(textAlignment == Qt.AlignLeft) rectangle.left
        anchors.right: if(textAlignment == Qt.AlignRight ) rectangle.right
        anchors.rightMargin: rightMargin
        anchors.leftMargin: leftMargin
        anchors.verticalCenter: rectangle.verticalCenter
        horizontalAlignment: if(textAlignment == Qt.AlignCenter) Text.AlignHCenter
        font.family: fontStyle
        font.pixelSize:  fontSize
        color: rootStyle.textColor
        font.weight: fontWeight
    }
}
