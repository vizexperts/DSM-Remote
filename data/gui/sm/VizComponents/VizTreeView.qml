import QtQuick 2.1
import QtQuick.Controls 1.0
//import
/*
     \Component VizTreeView
     \biref Create a tree view. See ElementList.qml for example
 */
Flickable{
    id: treeView

    /*
        \property var model
        \brief TreeModel to be set from C++ code.

        Required to be derived from ElementListTreeModel class.
    */
    property var model

    /*
      \internal
      \property Component itemDelegate
      \brief Dummy delegate
    */
    property Component itemDelegate: VizLabel{
        text: styleData.value
        textColor: styleData.textColor
    }

    clip: false
    boundsBehavior: Flickable.StopAtBounds
    interactive: false

    //! Easy access alias
    property alias treeHasActiveFocus: listView.activeFocus
    property alias treeHasFocus: listView.focus

    default property alias __columns: treeView.data

    readonly property alias rowCount: listView.count
    readonly property alias columnCount: columnModel.count

    /*
        \property alias currentRow
        \brief Get/Set current row of the tree view
    */
    property alias currentRow: listView.currentIndex

    property alias __currentRowItem: listView.currentItem

    signal activated(int index)
    signal clicked(int index)
    signal doubleClicked(int index)
    signal rightClicked(int index)
    signal deleteKeyPressed(int index)

    onDoubleClicked: {
        listView.model.toggleItem(index)
    }

    function positionViewAtRow(row, mode){
        listView.positionViewAtIndex(row, mode)
    }

    function rowAt(x, y){
        var obj = treeView.mapToItem(listView.contentItem, x, y)
        return listView.indexAt(obj.x, obj.y)
    }

    function addColumn(column){
        insertColumn(columnCount, column)
    }

    function insertColumn(index, column){
        var object = column
        if(typeof column['createObject'] === 'function'){
            object = column.createObject(treeView)
        }
        else if(object.__view){
            console.log("TreeView::insertColumn(): you cannot add a column to multiple views")
            return null
        }

        if(index >= 0 && index <= columnCount && object.Accessible.role === Accessible.ColumnHeader){
            object.__view = treeView
            columnModel.insert(index, {columnItem: object})
            return object
        }

        if(object !== column)
            object.destroy()
        return null
    }

    function removeColumn(index){
        if(index < 0 || index >= columnCount){
            return
        }
        var column = columnModel.get(index).columnItem
        columnModel.remove(index, 1)
        column.destroy()
    }

    function moveColumn(from, to){
        if(from < 0 || from >= columnCount || to < 0 || to >= columnCount){
            return
        }
        columnModel.move(from, to, 1)
    }

    function getColumn(index){
        if(index < 0 || index >= columnCount){
            return null
        }
        return columnModel.get(index).columnItem
    }

    Component.onCompleted: {
        for(var i = 0; i < treeView.data.length; ++i){
            var column = treeView.data[i]
            if(typeof column !== "undefined" && (column.Accessible.role === Accessible.ColumnHeader)){
                addColumn(column)
            }
        }
    }

    function __decrementCurrentIndex(){
        listView.decrementCurrentIndex();
    }

    function __incrementCurrentIndex() {
        listView.incrementCurrentIndex();
    }

    VizScrollView{
        anchors.fill: parent
        ListView{
            id: listView
            focus: true
            activeFocusOnTab: true
            currentIndex: -1
            visible: columnCount > 0
            interactive: true
            cacheBuffer: 100000*rootStyle.uiScale*5
            model: treeView.model
            boundsBehavior: Flickable.StopAtBounds
            highlightFollowsCurrentItem: true
            anchors.fill: parent
            MouseArea {
                id: mousearea

                z: -1
                anchors.fill: listView
                propagateComposedEvents: true
                acceptedButtons: Qt.AllButtons

                property bool autoincrement: false
                property bool autodecrement: false

                onReleased: {
                    autoincrement = false
                    autodecrement = false
                }

                // Handle vertical scrolling whem dragging mouse outside boundraries
                Timer { running: mousearea.autoincrement ; repeat: true; interval: 20 ; onTriggered: __incrementCurrentIndex()}
                Timer { running: mousearea.autodecrement ; repeat: true; interval: 20 ; onTriggered: __decrementCurrentIndex()}

                onWheel: {
                    if(listView.contentHeight < treeView.height)
                        return

                    if(wheel.angleDelta.y < 0){
                        listView.contentY = (listView.contentY < listView.contentHeight - treeView.height) ? listView.contentY + 10 : listView.contentHeight - treeView.height
                    }
                    else{
                        listView.contentY = (listView.contentY > 10) ? listView.contentY - 10 : 0
                    }
                }

                onPositionChanged: {
                    if (mouseY > listView.height && pressed) {
                        if (autoincrement) return;
                        autodecrement = false;
                        autoincrement = true;
                    } else if (mouseY < 0 && pressed) {
                        if (autodecrement) return;
                        autoincrement = false;
                        autodecrement = true;
                    } else  {
                        autoincrement = false;
                        autodecrement = false;
                    }

                    if (pressed) {
                        var newIndex = listView.indexAt(0, mouseY + listView.contentY)
                        if (newIndex >= 0)
                            listView.currentIndex = newIndex;
                    }
                }

                onClicked: {
                    var clickIndex = listView.indexAt(0, mouseY + listView.contentY)
                    if (clickIndex > -1) {
                        if (treeView.__activateItemOnSingleClick)
                            treeView.activated(clickIndex)
                        if(mouse.button == Qt.LeftButton){
                            treeView.clicked(clickIndex)
                        }
                        else if(mouse.button == Qt.RightButton){
                            treeView.clicked(clickIndex)
                            treeView.rightClicked(clickIndex)
                        }
                    }
                }

                onPressed: {
                    var newIndex = listView.indexAt(0, mouseY + listView.contentY)
                    listView.forceActiveFocus()
                    if (newIndex > -1) {
                        listView.currentIndex = newIndex
                    }
                }

                onDoubleClicked: {
                    var clickIndex = listView.indexAt(0, mouseY + listView.contentY)
                    if (clickIndex > -1) {
                        if (!treeView.__activateItemOnSingleClick)
                            treeView.activated(clickIndex)
                        treeView.doubleClicked(clickIndex)
                    }
                }

                // Note:  with boolean preventStealing we are keeping the flickable from
                // eating our mouse press events
                preventStealing: true

            }

            /*VizScrollBar{
                id: verticalScrollBar
                target: listView

            }*/

            ListModel{
                id: columnModel
            }

            Keys.onUpPressed: {
                treeView.__decrementCurrentIndex()
            }

            Keys.onDownPressed: {
                treeView.__incrementCurrentIndex()
            }

            Keys.onLeftPressed: model.closeItem(currentIndex)

            Keys.onDeletePressed: treeView.deleteKeyPressed(currentIndex)

            Keys.onRightPressed: model.openItem(currentIndex)

            Keys.onPressed: {
                if (event.key === Qt.Key_PageUp) {
                    listView.contentY = listView.contentY - listView.height
                } else if (event.key === Qt.Key_PageDown)
                    listView.contentY = listView.contentY + listView.height
            }

            Keys.onSpacePressed: model.toggleCheckedState(currentIndex)

            Keys.onReturnPressed: {
                if (currentRow > -1)
                    treeView.activated(currentRow);
            }

            delegate: Item{
                id: rowItem
                clip: false
                width: treeView.width - 20
                height: itemRow.height

                ListView.onAdd: ParallelAnimation{
                    NumberAnimation{target: rowItem; property: "height"; from: 10; to: rootStyle.buttonIconSize; duration: 200}
                    NumberAnimation{target: rowItem; property: "opacity"; from: 0; to: 1; duration: 200}
                }
                ListView.onRemove: SequentialAnimation{
                    PropertyAction{target: rowItem; property: "ListView.delayRemove"; value: true}
                    NumberAnimation{target: rowItem; property: "height"; to: 0; duration: 200}
                    PropertyAction { target: rowItem; property: "ListView.delayRemove"; value: false}
                }

                readonly property int rowIndex: model.index
                readonly property var itemModelData: typeof modelData == "undefined" ? null : modelData
                readonly property var itemModel: model
                readonly property bool itemSelected: ListView.isCurrentItem
                readonly property color itemTextColor: itemSelected ? rootStyle.colorWhenDefault : rootStyle.textColor
                readonly property int itemCheckedStateInternal: typeof itemCheckedState == "undefined" ? 0 : itemCheckedState

                onItemCheckedStateInternalChanged: {
                    checkBox.checkedState = itemCheckedStateInternal
                }

                Rectangle{
                    anchors.fill: parent
                    color: itemSelected ? (dragArea.held & !isItemDraggable ? rootStyle.colorWhenInvalid : rootStyle.colorWhenSelected) : "transparent"
                    opacity: 0.6
                }

                MouseArea{
                    id: dragArea
                    anchors.fill: parent
                    property int positionStarted: 0
                    property int positionEnded: 0
                    property int positionsMoved: Math.round((positionEnded - positionStarted)/parent.height)
                    property int newPosition: index + positionsMoved
                    property bool held: false
                    drag.axis: Drag.YAxis

                    onClicked: {
                        if (treeView.__activateItemOnSingleClick)
                            treeView.activated(index)
                        treeView.clicked(index)
                    }

                    onPressed: {
                        listView.currentIndex = index
                        listView.forceActiveFocus()
                    }

                    onDoubleClicked: {
                        if (!treeView.__activateItemOnSingleClick)
                            treeView.activated(index)
                        treeView.doubleClicked(index)
                    }

                    onPressAndHold: {
                        parent.z = 2
                        positionStarted = parent.y
                        dragArea.drag.target = parent
                        parent.opacity = 0.5
                        held = true
                        rowItem.ListView.view.model.closeItem(index)
                        drag.maximumY = (listView.count -1)*parent.height + 10 + listView.contentY
                        //drag.maximumY = (listView.count -1)
                        drag.minimumY = -10
                    }
                    onPositionChanged: {
                        positionEnded = parent.y
                    }
                    onReleased: {
                        if(!held)
                            return

                        parent.opacity = 1
                        parent.z = 1
                        held = false
                        dragArea.drag.target = null

                        if(Math.abs(positionsMoved) < 1 ){
                            parent.y = positionStarted
                        }
                        else {
                            var newPos = Math.min( Math.max(0, newPosition), listView.count - 1)
                            if(isItemDraggable)
                                rowItem.ListView.view.model.move(index, newPos)
                        }
                    }
                }
                onActiveFocusChanged: {
                    if (activeFocus)
                        listView.currentIndex = rowIndex
                }


                Row{
                    id: itemRow
                    Item {
                        id: levelMarginElement
                        width: level*rootStyle.buttonIconSize + 5
                        height: 25
                    }
                    Rectangle{
                        id: nodeOpenElement
                        enabled:(typeof(hasChildren) == "undefined") ? false : hasChildren
                        width: 25*rootStyle.uiScale
                        height: width
                        color: enabled && msArea.containsMouse ? rootStyle.colorWhenSelected :"transparent"
                        radius: 2
                        Image {
                            id: nodeOpenElementImage
                            visible: nodeOpenElement.enabled
                            source: isOpened ? rootStyle.getIconPath("down") : rootStyle.getIconPath("right")
                            anchors.centerIn: parent
                            width: 20*rootStyle.uiScale
                            height: width
                        }
                        MouseArea{
                            id: msArea
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                if(nodeOpenElement.enabled){
                                    if(isOpened){
                                        rowItem.ListView.view.model.closeItem(index)
                                    }
                                    else{
                                        rowItem.ListView.view.model.openItem(index)
                                    }
                                }
                            }
                        }
                    }
                    VizCheckBox{
                        id: checkBox
                        text: ""
                        visible: typeof isItemCheckable == "undefined" ? false : isItemCheckable
                        checkedState: itemCheckedStateInternal
                        onClicked: {
                            rowItem.ListView.view.model.toggleCheckedState(index)
                        }
                        scale: visible ? 0.7 : 0.0
                    }
                    Repeater{
                        id: repeater
                        model: columnModel
                        Loader{
                            id: itemDelegateLoader
                            readonly property VizTreeViewColumn __column: columnItem
                            width: item.width
                            height: item.height
                            visible: __column.visible
                            sourceComponent: __column.delegate ? __column.delegate : itemDelegate

                            // these properties are exposed to the item delegate
                            readonly property var model: listView.model
                            readonly property var modelData: itemModelData

                            property QtObject styleData: QtObject{
                                readonly property var value: itemModel[role]
                                readonly property int row: rowItem.rowIndex
                                readonly property int column: index
                                readonly property bool selected: rowItem.itemSelected
                                readonly property color textColor: rowItem.itemTextColor
                                readonly property string role: __column.role
                            }
                        }
                    }
                    onWidthChanged: listView.contentWidth = width
                }
            }

        }

    }

}
