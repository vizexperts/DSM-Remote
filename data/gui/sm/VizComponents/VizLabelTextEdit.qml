import QtQuick 2.1
import QtQuick.Layouts 1.0

/**
  Custom Component to show a label and a line of editable text
**/

RowLayout {
    property alias labelText: label.text
    property alias labelVisible : label.visible
    property alias lineEditText: lineEdit.text
    property alias isReadOnly: lineEdit.readOnly
    property alias validator : lineEdit.validator
    property alias maximumLength: lineEdit.maximumLength
    property alias echoMode: lineEdit.echoMode
    property alias textColor: lineEdit.textColor
    property alias placeholderText: lineEdit.placeholderText
    property int labelWidth: 0

    Layout.minimumHeight: lineEdit.height
    height: lineEdit.height

    property bool okButtonVisible: true

    onLineEditTextChanged: {
        if(lineEdit.setFocus && okButtonVisible){
            okButton.visible = true
        }
    }

    signal changeApplied()

    function giveFocus(){
        lineEdit.setFocus = true
    }

    VizLabel{
        id: label
        textAlignment: Qt.AlignLeft
        Layout.minimumWidth: labelWidth * 1.5
    }

    VizTextField{
        id: lineEdit
        opacity: enabled ? 1.0 : 0.3
        Keys.forwardTo: [parent]
        Layout.fillWidth: true
        onAccepted: {
            changeApplied()
            okButton.visible = false
            lineEdit.setFocus = false
        }
        onTextChange: {
            if(text.charAt(0)=="_"){
                text=""
            }
        }
    }

    VizButton{
        id: okButton
        scale: 0.7
        visible: false
        iconSource: rootStyle.getIconPath("accept")
        onClicked: {
            changeApplied()
            visible = false
            lineEdit.setFocus = false
        }
    }
}
