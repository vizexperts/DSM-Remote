import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import "../VizComponents"
import ".."

Rectangle{
    id: fontPopup
    objectName: "FontPicker"
    color: "#1a1a1a"
    clip: true
    border.color: rootStyle.borderColor
    border.width: rootStyle.borderWidth
    radius: 10
    width: 200
    height: 150

    property alias fontFamily: fontFamilyCombo.value
    property alias fontSize : fontsizeSpinBox.value
    property alias fontWeight : weightCOmbo.value

    visible: false

//    signal okClicked(string family,int size, string weight)
//    signal cancelClicked()
    ColumnLayout{
        id: colLayout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout{
            id: fontSizeColumnRow
            visible: true
            spacing: 5
            VizLabel{
                text: "Size"
                textAlignment: Qt.AlignLeft
            }

            VizSpinBox{
                id: fontsizeSpinBox
                Layout.minimumWidth: 50
                placeholderText: "Fontsize"
                Layout.alignment: Qt.AlignRight
                minimumValue:0
                maximumValue: 99
                value : 40
                validator: IntValidator{
                    bottom: 0
                    top: 99
                }
            }
        }

        RowLayout{
            id: fontStyleRow
            spacing: 5
            VizLabel{
                text: "Style"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                id: fontFamilyCombo
                Layout.minimumWidth: 130
                //Layout.fillWidth: true
                listModel: unitModel
                value:listModel.get(selectedUID).name

                ListModel{
                    id: unitModel
                    ListElement{name: "Arial"; uID: "1"}
                    ListElement{name: "Impact"; uID: "2"}
                    ListElement{name: "Verdana"; uID: "3"}
                    ListElement{name: "Calibri"; uID: "4"}
                    ListElement{name: "Times New Roman"; uID: "5"}
                    ListElement{name: "Vani"; uID: "6"}
                    ListElement{name: "Courier New"; uID: "7"}
                }
            }
        }
        RowLayout{
            id: weightLayout

            spacing: 5
            VizLabel{
                text: "Weight"
                textAlignment: Qt.AlignLeft
            }

            VizComboBox{
                id: weightCOmbo
                Layout.minimumWidth: 130
                //Layout.fillWidth: true
                listModel: weightModel
                value:listModel.get(selectedUID).name

                ListModel{
                    id: weightModel
                    ListElement{name: "Light"; uID: "1"}
                    ListElement{name: "Normal"; uID: "2"}
                    ListElement{name: "DemiBold"; uID: "3"}
                    ListElement{name: "Bold"; uID: "4"}
                    ListElement{name: "Black"; uID: "5"}
                }
            }
        }

//        RowLayout{
//            id: buttonRow
//            spacing: 5
//            VizButton{
//                id: cancelButton
//                anchors.left: parent.left
//                anchors.leftMargin: 50
//                anchors.top: parent.top
//                anchors.bottom: parent.bottom

//                text: "Cancel"

//                onClicked: {
//                    cancelClicked()
//                }
//            }

//            VizButton{
//                id: saveButton
//                anchors.left:cancelButton.right
//                anchors.top: parent.top
//                anchors.bottom: parent.bottom

//                text: "Ok"

//                onClicked: {
//                    fontFamily=fontFamilyCombo.value
//                    fontSize=fontsizeSpinBox.value
//                    fontWeight=weightCOmbo.value
//                    //okClicked(fontFamily,fontSize,fontWeight)
//                }
//            }
//        }
    }
}
