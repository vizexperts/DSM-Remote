import QtQuick 2.1
import QtQuick.Layouts 1.0
import "javascripts/functions.js" as Functions

Item {
    id: spinbox

    width: Math.max(input.width + buttons.width, 50)
    height: input.height

    Layout.minimumHeight: input.height
    Layout.minimumWidth: input.width + buttons.width + 10

    smooth: true

    property real value: 0.0
    property real maximumValue: 99
    property real minimumValue: 0
    property real singlestep: 1
    property bool upEnabled: value != maximumValue;
    property bool downEnabled: value != minimumValue;

    property alias upPressed: mouseUp.pressed
    property alias downPressed: mouseDown.pressed
    property alias upHovered: mouseUp.containsMouse
    property alias downHovered: mouseDown.containsMouse
    property alias validator: input.validator
    property alias showButtons: input.setFocus
    property alias placeholderText: input.placeholderText
    property alias textBoxWidth:input.width

    // use this property to change the text in the spinbox.  <--------
    property alias text : input.text

    function increment() {
        value += singlestep
        if (value > maximumValue)
            value = maximumValue
        input.text = value
    }

    function decrement() {
        value -= singlestep
        if (value < minimumValue)
            value = minimumValue
        input.text = value
    }

    function setValue(v) {
        var newval = parseFloat(v)
        if (newval > maximumValue)
            newval = maximumValue
        else if (value < minimumValue)
            newval = minimumValue
        value = newval
        input.text = value
    }

    signal accepted()
    onShowButtonsChanged:{
        if(!showButtons)
            accepted()
    }

    Component.onCompleted: {
        setValue(value)
    }

    VizTextField {
        id: input
//        anchors.left: parent.left
        text: value
        opacity: parent.enabled ? 1 : 0.5
        //readOnly: multi_touch_interaction_active
        width: 75
        onTextChanged:{
            if(acceptableInput){
                value = text
            }
            else{
                text = value;
            }
        }
        Keys.onUpPressed: increment()
        Keys.onDownPressed: decrement()
   }

    MouseArea {
        id: mouseArea
        enabled: multi_touch_interaction_active
        anchors.fill: parent
        hoverEnabled: true
        z: 1
        onClicked: showButtons = !showButtons
    }

    Rectangle{
        id: buttons
        anchors.right: parent.right
        color: rootStyle.colorWhenDefault
        gradient: rootStyle.gradientWhenDefault
        border.width: rootStyle.borderWidth
        border.color: rootStyle.borderColor
        radius: showButtons ? Functions.countRadius(rootStyle.roundness,width,height,0,1) : 0
        width: (showButtons & multi_touch_interaction_active) ?  spinbox.height*0.8 : spinbox.height/2
        height: 2*width
        Behavior on width {
            PropertyAnimation{easing.type: Easing.OutBack; duration: 600}
        }

        anchors.verticalCenter: parent.verticalCenter
        Item {
            id: upButton
            property alias pressed : spinbox.upPressed
            property alias hover : spinbox.upHovered
            property alias enabled : spinbox.upEnabled
            MouseArea {
                id: mouseUp
                anchors.fill: upImage
                onClicked: increment()

                property bool autoincrement: false;
                onReleased: autoincrement = false
                Timer { running: mouseUp.pressed; interval: 350 ; onTriggered: mouseUp.autoincrement = true }
                Timer { running: mouseUp.autoincrement; interval: 60 ; repeat: true ; onTriggered: increment() }
            }
            anchors.right: parent.right
            anchors.top: parent.top
            width: parent.width
            height: width
            Image {
                id: upImage
                anchors.fill: parent

                opacity: (upEnabled && enabled) ? (upPressed ? 1 : 0.8) : 0.3
                source: "icons/up_arrow-24-xxl.png"
            }
        }

        Item {
            id: downButton
            property alias pressed : spinbox.downPressed
            property alias hover : spinbox.downHovered
            property alias enabled : spinbox.downEnabled
            MouseArea {
                id: mouseDown
                anchors.fill: downImage
                onClicked: decrement()

                property bool autoincrement: false;
                onReleased: autoincrement = false
                Timer { running: mouseDown.pressed; interval: 350 ; onTriggered: mouseDown.autoincrement = true }
                Timer { running: mouseDown.autoincrement; interval: 60 ; repeat: true ; onTriggered: decrement() }
            }
            anchors.right: parent.right
            anchors.top: upButton.bottom
            width: parent.width
            height: width
            x: spinbox.width - width
            y: spinbox.height -height
            Image {
                id: downImage
                anchors.fill: parent
                opacity: (downEnabled && enabled) ? (downPressed ? 1 : 0.8) : 0.3
                source: "icons/down_arrow-24-xxl.png"
            }
        }
    }
}
