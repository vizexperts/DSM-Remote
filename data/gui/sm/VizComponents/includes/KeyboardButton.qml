import QtQuick 2.1
import "../"

VizButton {
    id: structure

    property string value:  ""
    property string shiftValue: value.toUpperCase()
    property string shiftTextFontFamily : style.fontFamily
    property string shiftTextFontWeight : style.fontWeight
    property real shiftTextFontSize: structure.height*0.2
    property color shiftTextColor: "red"
    property string shiftText: shiftValue
    property Image nullImage : Image { //this property is "private" don't write it to documentation
        id: "null"
        source: ""
        width: buttonWidth
        height: buttonHeight
        fillMode: Image.PreserveAspectCrop
        smooth: false
        scale: 1
    }
    property Image specialImage : nullImage

    /**
     * Returns HTML auml tag to uppercase
     * "<html>&auml;</html>" -> "<html>&Auml;</html>"
     */
    function htmlToUpperCase(htmlString) {
        return (htmlString.replace(htmlString.substring(7,8),htmlString.substring(7,8).toUpperCase()));
    }

    /**
     * Checks if word has html-tag in start.
     * "<html>&auml;</html>" -> true
     *  "<html>" -> true
     *  "<htm" -> false
     *  "dog" -> false
     */
    function isHtml(string) {
        return (string.substring(0,6) == "<html>")
    }

    Image {
        source: specialImage.source
        width: if(source != "") specialImage.width
        height: if(source != "") specialImage.height
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        smooth: specialImage.smooth
        scale: specialImage.scale
    }

    /** Shows shift mark in upleft corner**/
    TextEdit {
        anchors.left: parent.left;
        anchors.leftMargin: structure.width*0.1;
        anchors.top: parent.top;
        anchors.topMargin: structure.height*0.1;
        text: shiftText;
        color: shiftTextColor
        font.pixelSize: 0.001+shiftTextFontSize
        readOnly: true
    }
}
