import QtQuick 2.1

Item{

    id: contextualItem

    anchors.fill: parent
    Rectangle{
        anchors.fill: parent
        opacity: 0.7
        gradient: rootStyle.gradientWhenDefault
    }

    Behavior on height {
        NumberAnimation{duration: 200}
    }

    Component.onCompleted: {
        mouseButtonClicked =  contextualContainer.mouseButtonClicked
    }

    property int minimumWidth
    property int minimumHeight

    //! Mouse button used to open the contextual menu
    property int mouseButtonClicked : Qt.RightButton


    // pass these values to contextual menu
    onMinimumHeightChanged: contextualContainer.setInitialMinWidthHeight(minimumWidth,minimumHeight)
    onMinimumWidthChanged: contextualContainer.setInitialMinWidthHeight(minimumWidth,minimumHeight)


}
