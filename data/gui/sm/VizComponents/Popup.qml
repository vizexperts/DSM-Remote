import QtQuick 2.1

Item {
    id: popup
    property string title

    onTitleChanged: {
        if(title != "")
            popupContainer.setTitle(title)
    }

    Rectangle{
        anchors.fill: parent
        opacity: 0.7
        gradient: rootStyle.gradientWhenDefault
    }

    Component.onCompleted: {
        smpFileMenu.isPopUpAlreadyOpen=true;
        if(title !== ""){
            setMinWidthHeight()
            popupContainer.setTitle(title)
            popupContainer.x = root.width/2 - width/2
            popupContainer.y = root.height/2 - height/2

        }
        smpFileMenu.popupLoaded(objectName)
    }
    Component.onDestruction:
    {
        if(objectName=="QueryBuilderGIS" ||  objectName=="ColorMap" || objectName=="VectorQueryBuilder"  ||  objectName=="addOrRenameBookmarksPopup")
        {
            smpFileMenu.isParentChild=false;

        }
        else
        {
            smpFileMenu.isPopUpAlreadyOpen=false;
        }
    }

    function closePopup() {
        popupContainer.close()
        smpFileMenu.isPopUpAlreadyOpen = false;
    }

    signal closed()
    onClosed: {
        if(objectName!="QueryBuilderGIS" && objectName!="ColorMap" && objectName!="VectorQueryBuilder"  && objectName!="addOrRenameBookmarksPopup")
        {
            if(!smpFileMenu.isParentChild)
            {
                root.nowRemoveHighLight();
            }


        }
    }

    Connections{
        target: popupContainer
        onClose: closed()
    }

    property int minimumWidth
    property int minimumHeight

    function setMinWidthHeight(){
        height = minimumHeight
        width = minimumWidth
    }

    onMinimumHeightChanged: setMinWidthHeight()
    onMinimumWidthChanged: setMinWidthHeight()
}
