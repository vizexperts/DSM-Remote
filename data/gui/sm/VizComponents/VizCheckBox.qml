import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0

/*
    \Component VizCheckBox
*/

CheckBox{
    style: CheckBoxStyle {
        indicator: Rectangle{
            implicitWidth: rootStyle.buttonIconSize
            implicitHeight: rootStyle.buttonIconSize
            radius: 3
            border.color: control.activeFocus ? rootStyle.colorWhenSelected : rootStyle.borderColor
            border.width: rootStyle.borderWidth
            color: control.checkedState !== Qt.PartiallyChecked ? rootStyle.colorWhenDefault : rootStyle.colorWhenSelected
            Image {
                anchors.fill: parent
                anchors.margins: 1
                visible: control.checked || (control.checkedState === Qt.Checked)
                source: rootStyle.getIconPath("tick")
            }
        }
        label: VizLabel{
                anchors.verticalCenter: parent.verticalCenter
                text: control.text
        }
    }
}
