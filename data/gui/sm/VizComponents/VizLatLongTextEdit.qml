import QtQuick 2.1

VizLabelTextEdit{
    id: latlogn
    property string latLongText

    //Enum for latitude and longitude type
    readonly property int type_LATITUDE:0
    readonly property int type_LONGITUDE:1
    readonly property int type_GR:2
    property int type:type_Latitude

    signal latLongChanged()

    onChangeApplied: {
        handleChange();
        latLongChanged();
    }

    onLatLongTextChanged: {
        if(type == type_LATITUDE)lineEditText = rootStyle.latDegDecToGlobal(latLongText);
        else if(type == type_LONGITUDE)lineEditText = rootStyle.longDegDecToGlobal(latLongText);
        else if(type==type_GR)lineEditText=latLongText;
    }



    function handleChange()
    {
        //First Convert in DegDec then check validation *** deegree in ? format here
        var tempLineEditText=lineEditText;
        if(type == type_LATITUDE)
        {
            if((lineEditText.indexOf("S")>-1)||(lineEditText.indexOf("s")>-1)||(lineEditText.indexOf("N")>-1)||(lineEditText.indexOf("n")>-1))
            {
                lineEditText=rootStyle.toDegDec(tempLineEditText);
            }

            if(rootStyle.valLatFormat(lineEditText))latLongText = rootStyle.toDegDec(lineEditText);
            else lineEditText = rootStyle.latDegDecToGlobal(latLongText);
        }
        else if(type == type_LONGITUDE)
        {
            if((lineEditText.indexOf("E")>-1)||(lineEditText.indexOf("e")>-1)||(lineEditText.indexOf("W")>-1)||(lineEditText.indexOf("w")>-1))
            {
                lineEditText=rootStyle.toDegDec(tempLineEditText);
            }
            if(rootStyle.valLongFormat(lineEditText))latLongText = rootStyle.toDegDec(lineEditText);
            else lineEditText = rootStyle.longDegDecToGlobal(latLongText);
        }
        else if(type == type_GR)
        {
            if(rootStyle.valGrFormat(lineEditText))latLongText = lineEditText;
            else lineEditText = latLongText;

        }

    }

}

