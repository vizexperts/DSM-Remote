import QtQuick 2.1

Item {
    SystemPalette {id: palette}
    objectName: "vizStyle"

    property string iconSetPath: "../../IconsSets/DSM_iconSet_Dark/"

    function getIconPath(iconType){
        return Qt.resolvedUrl(iconSetPath + iconType + ".png")
    }

    function getBusyIcon(){
        return Qt.resolvedUrl(iconSetPath + "busy.gif")
    }
    function getBusyBar(){
        return Qt.resolvedUrl(iconSetPath + "busyBar.gif")
    }

    //! contextual menu to be docked in elememt list or not
    property bool docked: false

    //! Flag whether to show demo/incomplete features or not.
    property bool showDemoFeatures: false

    //This function validate latitude entered in any of the three formats.
    function valLatFormat(lat)
    {
        //Both degreeMinuteFormat degreeMinuteSecondFormat will not validate in any case because first converting in degreedec then validation
        var degreeDecimalFormat = /^([-]{0,1}([0-9](\.[0-9]{1,5}){0,1}|[1-8][0-9](\.[0-9]{1,5}){0,1}|90((\.[0]{1}){0,1})))$/;
        var degreeMinuteFormat = /^[0-8]{0,1}[0-9]{1}\s[0-5]{0,1}[0-9]{1}(\.[0-9]{1,5}){0,1}\'[NSns]$/;
        var degreeMinuteSecondFormat = /^[0-8]{0,1}[0-9]{1}\s[0-5]{0,1}[0-9]{1}\'\s[0-5]{0,1}[0-9]{1}(\.[0-9]{1,5}){0,1}\"[NSns]$/;
        if(degreeDecimalFormat.test(lat))
        {
            return true;
        }
        else if(degreeMinuteFormat.test(lat))
        {
            return true;

        }
        else if(degreeMinuteSecondFormat.test(lat))
        {
            return true;

        }
        else
        {
            return false;
        }

    }

    //This function validate longitude entered in any of the three formats.
    function valLongFormat(lon)
    {
        var degreeDecimalFormat = /^([-]{0,1}([0-9]{0,2}(\.[0-9]{1,5}){0,1}|1[0-7][0-9](\.[0-9]{1,5}){0,1}|180((\.[0]{1}){0,1})))$/;
        var degreeMinuteFormat = /^([0-9]{0,2}|1[0-7][0-9]|180)\s[0-5]{0,1}[0-9]{1}(\.[0-9]{1,5}){0,1}\'[EWew]$/;
        var degreeMinuteSecondFormat = /^([0-9]{0,2}|1[0-7][0-9]|180)\s[0-5]{0,1}[0-9]{1}\'\s[0-5]{0,1}[0-9]{1}(\.[0-9]{1,5}){0,1}\"[EWew]$/;

        if(degreeDecimalFormat.test(lon))
        {
            return true;
        }
        else if(degreeMinuteFormat.test(lon))
        {
            return true;

        }
        else if(degreeMinuteSecondFormat.test(lon))
        {
            return true;

        }
        else
        {
            return false;
        }


    }
    function  valGrFormat(gr)
    {
        var indianGrValidator = /^([0-9]{1,3})[a-p]{1}([1-9]{1}|0[1-9]|1[0-6]){0,1}\\040([0-9]{6}|[0-9]{8}|[0-9]{10})$/;
        if(indianGrValidator.test(gr))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //This function convert latitude from degree decimal to global format.
    function latDegDecToGlobal(lat)
    {

        var a = parseFloat(lat);
        var sign = "N";
        if(a<0)
        {
            a = a * -1;
            sign = "S";
        }
        a=a.toFixed(4);

        if(latlongUnit === degDecUnit)
            return a + "° " + sign

        var deg = parseInt(a);
        a = a - deg;
        a = a * 60;
        if(latlongUnit === degMinUnit )
        {
            a=a.toFixed(2);
            return deg+"° "+a+"'"+sign;
        }
        var min = parseInt(a);
        a = a - min;
        var sec = a*60;
        sec=sec.toFixed(2);
        return deg+"° "+min+"' "+sec+"\""+sign;
    }

    //This function convert longitude from degree decimal to global format.
    function longDegDecToGlobal(lon)
    {
        var a = parseFloat(lon);
        var sign = "E";
        if(a<0)
        {
            a = a * -1;
            sign = "W";
        }
        a=a.toFixed(4);

        if(latlongUnit === degDecUnit)
            return a + "° " + sign

        var deg = parseInt(a);
        a = a - deg;
        a = a * 60;
        if(rootStyle.latlongUnit === degMinUnit )
        {
            a=a.toFixed(2);
            return deg+"° "+a+"'"+sign;
        }
        var min = parseInt(a);
        a = a - min;
        var sec = a*60;
        sec=sec.toFixed(2);
        return deg+"° "+min+"' "+sec+"\""+sign;
    }

    function convertAltitudeToGlobal(alt)
    {
        var a=parseFloat(alt)
        var leftZeroPadding=""
        if(a<=10){
            leftZeroPadding="000"
        }
        else if(a<=100){
            leftZeroPadding="00"
        }
        else if(a<=1000){
            leftZeroPadding="0"
        }

        a=a.toFixed(4)

        return  leftZeroPadding + a + "m"
    }


    //This function convert latitude/longitude entered in any format to degree decimal.
    function toDegDec(latlon)
    {
        var str = latlon.split(" ");
        var deg=0.0,min=0.0,sec=0.0;
        var sign=1;
        if((latlon.indexOf("S")>-1)||(latlon.indexOf("s")>-1)||(latlon.indexOf("W")>-1)||(latlon.indexOf("w")>-1))sign = -1;

        deg = parseFloat(str[0]);

        if(str.length > 1)min = parseFloat(str[1]);
        if(isNaN(parseFloat(str[1])))
        {
            min=0.0;
        }

        if(str.length > 2)sec = parseFloat(str[2]);
        if(isNaN(parseFloat(str[2])))
        {
            sec=0.0;
        }

        var result = (((sec/60)+min)/60)+deg;
        result = result.toFixed(5)*sign;
        return result.toString();
    }
    
    property real uiScale : 1
//    property real uiMultiplier: value
    property color colorWhenDefault: "#202020"
    property color colorWhenPressed: palette.shadow
    property color colorWhenHovered: palette.mid
    property color colorWhenSelected: "#52B1FF"
    property color colorWhenInvalid: "#FF1111"
    property color textColor: "#D4D0C8"
    property int buttonIconSize: 25*uiScale
    property real roundness: 0.2 // 0-1
    property real fontSize: 14*uiScale
    property string fontFamily: "Helvetica"
    property real fontWeight: Font.DemiBold
    property int borderWidth: 2
    property color borderColor: "black"
    property color borderColorWhenHovered: "black"
    property color borderColorWhenPressed: "black"
    property color borderColorWhenSelected: "black"
    property int borderWidthInner: 1
    property color borderColorInner: "#747275"
    property color borderColorInnerWhenHovered: "#747275"
    property color borderColorInnerWhenPressed: "#747275"
    property color bgColor: "#E0E0E0"
    property real itemRoundness: 0.00001
    property color selectionColor: "#D4D0C8"
    property color selectedTextColor: "black"
    property bool gradientActiveOn: true
    property bool gradientDefaultOn: true
    property bool gradientHoveredOn: true
    property bool gradientPressedOn: true
    property bool gradientBgOn: true
    property bool gradientCheckedOn: true;
    property bool gradientSelectedOn: true;
    property bool activeStateOn: true
    property bool hoveredStateOn: true
    property bool pressedStateOn: true
    property color borderColorWhenChecked: "black"
    property color colorWhenChecked: "#52B1FF"
    property color borderColorWhenActive: "#52B1FF"
    property color colorWhenActive: palette.shadow
    property bool  indianGrVisibility: false
    property bool  mgrsVisibility: false
    //Enum for latlongUnit
    property int degDecUnit:1
    property int degMinUnit:2
    property int degSecUnit:3
    property int latlongUnit:degDecUnit
    property bool showZone: true
    property bool showGR: true
    property bool showMapSheet: false
    property int numOfGrDigits: 3



    property string buttonMode: "textOnly"
    // available modes are : "textOnly", "iconOnly", "iconAndText"

    property Gradient gradientBg: Gradient {
        GradientStop { position: 0; color: "#C2C2C2" }
        GradientStop { position: 1; color: "#E0E0E0" }
    }
    property Gradient gradientWhenDefault: Gradient {
        GradientStop { position: 0; color: "#404040" }
//        GradientStop { position: 0.5; color: "#010103"}
        GradientStop { position: 1; color: "#202020" }
    }
    property Gradient gradientWhenHovered:  Gradient {
        GradientStop { position: 0; color: "#4c4849" }
        GradientStop { position: 1; color: "#09091c" }
    }
    property Gradient gradientWhenPressed: Gradient {
        GradientStop { position: 0; color: "#3f3b3c" }
        GradientStop { position: 1; color: "#010103" }
    }
    property Gradient nullGradient: Gradient{}
    property Gradient gradientWhenSelected: Gradient{
        GradientStop { position: 0; color: "#00A6FF" }
        GradientStop { position: 1; color: "#52B1FF" }
    }

    property Gradient blueGradient: Gradient {
        GradientStop { position: 0; color: "#00A6FF" }
        GradientStop { position: 0.5; color: "#52B1FF" }
        GradientStop { position: 0.5000000000001; color: "#0088C3" }
        GradientStop { position: 1; color: "#52B1FF" }
    }


    property Gradient greyGradient: Gradient  {
        GradientStop { position: 0; color: "#C2C2C2" }
        GradientStop { position: 1; color: "#E0E0E0" }
   }

    property Gradient gradientWhenChecked: gradientWhenSelected
    property Gradient gradientWhenActive: gradientWhenDefault


    // validators for Name, Longitude, Latitude
    property RegExpValidator nameValidator: RegExpValidator{
        regExp: /^[_a-zA-Z]{1}([_ ]{0,1}[a-zA-Z0-9]){0,30}([_ ]){0,1}$/
    }
    property RegExpValidator ipValidator: RegExpValidator{
        regExp: /\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/
    }
    property RegExpValidator grValidator: RegExpValidator{
        regExp: /^([0-9]{1,3})[a-p]{1}([1-9]{1}|0[1-9]|1[0-6]){0,1}\\040([0-9]{6}|[0-9]{8}|[0-9]{10})$/
    }
    property DoubleValidator longitudeValidator: DoubleValidator{
        bottom: -180; top:180; decimals: 6; notation: DoubleValidator.StandardNotation
    }
    property DoubleValidator latitudeValidator: DoubleValidator{
        bottom: -90; top:90; decimals: 6; notation: DoubleValidator.StandardNotation
    }
    property DoubleValidator altitudeValidator: DoubleValidator{
        bottom: 0; top: 8850; decimals: 4; notation: DoubleValidator.StandardNotation
    }
    property DoubleValidator scaleValidator: DoubleValidator{
        bottom: 0.1; top:3000; decimals: 2; notation: DoubleValidator.StandardNotation
    }
    property DoubleValidator angleInDegreesValidator: DoubleValidator{
        bottom: 0; top: 360; decimals: 4; notation: DoubleValidator.StandardNotation
    }
    property IntValidator generalIntValidator: IntValidator{    }
    property DoubleValidator generalDoubleValidator: DoubleValidator{  notation: DoubleValidator.StandardNotation  }
}
