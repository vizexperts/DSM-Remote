import QtQuick 2.1
import "Date.js" as DateFunctions

Rectangle {
    id:flickableDateRect

    width: displayRect.width+10
    height: displayRect.height + 10
    radius: 5

    color: rootStyle.colorWhenDefault

    property int date:(dateList.currentIndex == -1) ? -1 : dateList.currentIndex + 1
    property int month:(monthList.currentIndex == -1) ? -1 : monthList.currentIndex
    property int year: (yearList.currentIndex == -1) ? -1 : yearList.currentIndex + startYear

    property int startYear: 1900
    property int endYear: 2099

    property bool changingDate: false

    function setDate(date){
        changingDate = true
        yearList.currentIndex = date.getFullYear() - startYear
        monthList.currentIndex = date.getMonth()
        dateList.currentIndex = date.getDate() - 1
        changingDate = false
    }

    function populateDates(model,endDate){
        model.clear()
        for(var i = 1; i <= endDate ; i++)
            model.append({"date": i})
        dateList.currentIndex = 0
    }

    ListModel{
        id: dateModel
    }

    signal changed()

    onDateChanged: if(!changingDate) changed()
    onMonthChanged: {
        populateDates(dateModel,DateFunctions.noOfDaysIn(month,year))
        if(!changingDate)
            changed()
    }
    onYearChanged: {
        populateDates(dateModel,DateFunctions.noOfDaysIn(month,year))
        if(!changingDate)
            changed()
    }

    ListModel{
        id: monthModel
        ListElement{month: "Jan"}
        ListElement{month: "Feb"}
        ListElement{month: "Mar"}
        ListElement{month: "Apr"}
        ListElement{month: "May"}
        ListElement{month: "Jun"}
        ListElement{month: "Jul"}
        ListElement{month: "Aug"}
        ListElement{month: "Sep"}
        ListElement{month: "Oct"}
        ListElement{month: "Nov"}
        ListElement{month: "Dec"}
    }


    Rectangle{
        id:displayRect
        width: dateList.width + monthList.width + yearList.width +10
        height: dateList.height+30*rootStyle.uiScale
        color: "transparent"
        anchors.centerIn: parent

        VizLabel{
            text: "Y"
            anchors.horizontalCenter: yearList.horizontalCenter
            anchors.bottom: yearList.top
            fontSize: rootStyle.fontSize + 3
        }
        VizLabel{
            text: "M"
            anchors.horizontalCenter: monthList.horizontalCenter
            anchors.bottom: monthList.top
            fontSize: rootStyle.fontSize + 3
        }
        VizLabel{
            text: "D"
            anchors.horizontalCenter: dateList.horizontalCenter
            anchors.bottom: dateList.top
            fontSize: rootStyle.fontSize + 3
        }

        Spinner{
            id: yearList
            model : (endYear - startYear + 1)
            width: 60*rootStyle.uiScale
            height: 100*rootStyle.uiScale
            itemHeight: 26*rootStyle.uiScale
            anchors.bottom: parent.bottom
            offset: startYear
            anchors.left: parent.left
            delegate: VizLabel{
                text: (index + startYear)
                fontSize: rootStyle.fontSize + 3
                scale: (index == yearList.currentIndex) ? 1 : 0.8
                textColor: (index == yearList.currentIndex) ? "white" : rootStyle.textColor
            }
        }
        Spinner{
            id: monthList
            model : monthModel
            width: 40*rootStyle.uiScale
            height: 100*rootStyle.uiScale
            itemHeight: 26*rootStyle.uiScale
            anchors.left: yearList.right
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            isSpecial: true

            Timer{
                id: monthTimer
                interval: 300
                onTriggered: parent.temp = ""
            }

            property string temp

            Keys.onPressed: {
                if(!monthTimer.running){
                    monthTimer.start()
                    switch(event.key){
                    case Qt.Key_J:
                        temp = "j"
                        currentIndex = 0    // Jan
                        break
                    case Qt.Key_F:
                        currentIndex = 1    // Feb
                        break
                    case Qt.Key_M:
                        temp = "m"
                        currentIndex = 2    // March
                        break
                    case Qt.Key_A:
                        temp = "a"
                        currentIndex = 3    // April
                        break
                    case Qt.Key_S:
                        currentIndex = 8    // Sept
                        break
                    case Qt.Key_O:
                        currentIndex = 9    // October
                        break
                    case Qt.Key_N:
                        currentIndex = 10   //November
                        break
                    case Qt.Key_D:
                        currentIndex = 11   // December
                        break
                    default:
                        monthTimer.stop()
                        break
                    }
                }
                else{
                    switch(event.key){
                    case Qt.Key_A:
                        if(temp == "m"){
                            temp = "ma"
                            monthTimer.start()
                        }
                        break
                    case Qt.Key_Y:
                        if(temp == "ma"){
                            currentIndex = 4    // May
                        }
                        break
                    case Qt.Key_U:
                        if(temp == "j"){
                            temp = "ju"
                            monthTimer.start()
                            currentIndex = 5    // June
                        }
                        else if(temp == "a"){
                            currentIndex = 7    // Aug
                        }
                        break
                    case Qt.Key_L:
                        if(temp == "ju"){
                            currentIndex = 6    // July
                        }
                        break
                    }
                }
                event.accepted = true
            }
            delegate: VizLabel{
                text: month
                fontSize: rootStyle.fontSize + 3
                scale: (index == monthList.currentIndex) ? 1 : 0.8
                textColor: (index == monthList.currentIndex) ? "white" : rootStyle.textColor
            }
        }
        Spinner{
            id: dateList
            model : dateModel
            width: 30*rootStyle.uiScale
            height: 100*rootStyle.uiScale
            itemHeight: 26*rootStyle.uiScale
            offset: 1
            anchors.left: monthList.right
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            delegate: VizLabel{
                text: date
                scale: (index == dateList.currentIndex) ? 1 : 0.8
                textColor: (index == dateList.currentIndex)  ? "white" : rootStyle.textColor
                fontSize: rootStyle.fontSize + 3
            }
        }

    }
}
