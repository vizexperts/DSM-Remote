import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "javascripts/functions.js" as Functions
import "../VizComponents"
import QtGraphicalEffects 1.0

Button{
//    height: cl.height + 10
//    width: root.width*(rootStyle.fontSize/15)*0.06

    property string iconPath
    property string toolTipText
    property string labelText
    property bool isSelected: false
    property string optionName

    style: ButtonStyle{
        background: Item{
            implicitWidth: rootStyle.buttonIconSize
            implicitHeight: rootStyle.buttonIconSize

            Rectangle{
                id: bgrect
                anchors.fill: parent
                color: "transparent"
            }
        }
        label: Item{
            implicitWidth: row.implicitWidth + 15
            implicitHeight: row.implicitHeight + 10
//            RadialGradient{
//                anchors.fill: parent
//                gradient: Gradient{
//                    GradientStop{ position: 0.0; color: "#52B1FFAA"}
//                    GradientStop{ position: 0.5; color: "transparent"}
//                }
//            }

            Column{
                id: row
                anchors.centerIn: parent
                spacing: 5
                Image{
                    id: image
                    antialiasing: true
                    source: control.iconSource
                    anchors.horizontalCenter : parent.horizontalCenter
                    width: (iconSource == "") ? 0 :rootStyle.buttonIconSize
                    height: width
                }
                TextEdit{
                    id: labelText
                    readOnly: true
                    renderType: TextEdit.NativeRendering
                    anchors.horizontalCenter : parent.horizontalCenter
                    text: control.text
                    color: rootStyle.textColor
                    font.family: rootStyle.fontFamily
                    font.pixelSize: rootStyle.fontSize
                    font.weight: rootStyle.fontWeight
                }
            }
            Glow{
                id: glow
                anchors.fill: row
                visible: false
                enabled: visible
                anchors.centerIn: parent
                source: row
                radius: 8
                samples: 17
                color: rootStyle.colorWhenSelected
            }

            ColorOverlay{
                id: colorOverlay
                anchors.fill: row
                anchors.centerIn: parent
                source: row
                visible: false
                enabled: visible
                color:  rootStyle.colorWhenSelected
            }
            states: [
                State {
                    name: "pressed"
                    when: control.pressed && control.enabled
                },
                State {
                    name: "selected"
                    when: control.checked && control.enabled || isSelected
                    PropertyChanges{target: labelText; color: rootStyle.colorWhenSelected}
                    PropertyChanges{target: colorOverlay; visible: true}
//                    PropertyChanges{target: glow; visible: true}
                },
                State{
                    name: "entered"
                    when: control.hovered && control.enabled
                    PropertyChanges{target: glow; visible: true}
                },
                State{
                    name: "focused"
                    when: control.activeFocus && control.enabled
                },
                State{
                    name: "disabled"
                    when: !control.enabled
                    PropertyChanges{target: row; opacity: 0.6}
                }

            ]
        }
    }

    height: width/1.5
    width: root.width*(rootStyle.fontSize/15)*0.06

    //
    iconSource: iconPath
    tooltip: toolTipText
    text: labelText

    //! flag whether menu item to be removed based on flags.
    property bool toRemoveMenu : false

    Component.onCompleted: {
        if(toRemoveMenu)
            column.model.remove(index)
    }

    state: isSelected ? "selected" : ""

    onClicked: {
        if(optionName !== "Back"){
            column.currentIndex = index
        }
        buttonColumn.optionClicked(optionName)
    }
}
