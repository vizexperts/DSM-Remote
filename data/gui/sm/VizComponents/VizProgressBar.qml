import QtQuick 2.1

Rectangle {
    id: progressbar
    radius:5
    property real minimum: 0
    property real maximum: 100
    property int value: 0
    property bool indeterminate: false
    property int leftMargin : 0
    property int rightMargin : 0
    property int topMargin: 0
    property int bottomMargin: 0

    width: 100
    height: 25
    //clip: true
    //color: "white"
    //border.width:5
    //border.color:"black"

    onValueChanged: {
        highlight.gradient = rootStyle.blueGradient;
   }

    Timer{
        id: timer
        interval:200
        repeat:true
        running:true
        }

    Rectangle {
        id: highlight
        property int widthDest: ((progressbar.width * (value - minimum)) / (maximum - minimum) - 6)
        width: highlight.widthDest
        visible:true
        clip:true
        TextEdit{
            text:value+"%"
            color:"black"
            anchors.centerIn: parent
        }
        Behavior on color{ColorAnimation{duration: 200}}
        Behavior on width{NumberAnimation{duration: 500}}
        anchors { left: parent.left; top: parent.top; bottom: parent.bottom; margins: 3 }
        radius: 1
    }

}
