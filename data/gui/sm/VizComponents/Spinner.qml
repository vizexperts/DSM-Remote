import QtQuick 2.1

Rectangle{
    id: spinner
    property alias model: view.model
    property alias delegate: view.delegate
    property alias currentIndex: view.currentIndex
    property real itemHeight: 30

    // set "isSpecial" to true for custom key processing
    property bool isSpecial: false
    property int offset : 0

    property int temp : -1
    Timer{
        id: timer
        interval: 300
        onTriggered: parent.temp = -1
    }

    Keys.onPressed: {

        if(isSpecial)
            return

        if(!timer.running){
            switch(event.key){
            case Qt.Key_0: temp = 0; break;
            case Qt.Key_1: temp = 1; break;
            case Qt.Key_2: temp = 2; break;
            case Qt.Key_3: temp = 3; break;
            case Qt.Key_4: temp = 4; break;
            case Qt.Key_5: temp = 5; break;
            case Qt.Key_6: temp = 6; break;
            case Qt.Key_7: temp = 7; break;
            case Qt.Key_8: temp = 8; break;
            case Qt.Key_9: temp = 9; break;
            }
            if(temp != -1){
                timer.start()
                currentIndex = temp-offset
            }
        }
        else{
            var t  = -1
            switch(event.key){
            case Qt.Key_0: t = 0; break;
            case Qt.Key_1: t = 1; break;
            case Qt.Key_2: t = 2; break;
            case Qt.Key_3: t = 3; break;
            case Qt.Key_4: t = 4; break;
            case Qt.Key_5: t = 5; break;
            case Qt.Key_6: t = 6; break;
            case Qt.Key_7: t = 7; break;
            case Qt.Key_8: t = 8; break;
            case Qt.Key_9: t = 9; break;
            }
            if(t != -1){
                temp = temp*10+t
                if(temp < view.count){
                    currentIndex = temp-offset
                    timer.start()
                }
            }
        }
        event.accepted = true
    }

    color: rootStyle.colorWhenDefault
    border.color: view.activeFocus ? rootStyle.colorWhenSelected :  rootStyle.borderColor
    border.width: rootStyle.borderWidth

    clip: true

    PathView {
        id: view
        anchors.fill: parent

        pathItemCount: height/itemHeight
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        highlight: Rectangle {
            gradient: rootStyle.gradientWhenDefault
            Rectangle{
                anchors.top: parent.top
                width: parent.width
                height: 2
                gradient: rootStyle.blueGradient
            }
            Rectangle{
                anchors.bottom: parent.bottom
                width: parent.width
                height: 2
                gradient: rootStyle.blueGradient
            }

            width: view.width
            height: itemHeight+4
            opacity: 0.8
        }
        dragMargin: view.width/2

        path: Path {
            startX: view.width/2
            startY: -itemHeight/2
            PathLine {
                x: view.width/2
                y: view.pathItemCount*itemHeight + itemHeight
            }
        }
        Keys.onDownPressed: {
            view.incrementCurrentIndex()
        }
        Keys.onUpPressed: {
            view.decrementCurrentIndex()
        }
        Keys.onEscapePressed:{
            view.focus = false
        }
        activeFocusOnTab: true
        MouseArea{
            anchors.fill: parent
            acceptedButtons: Qt.NoButton
            onEntered: view.forceActiveFocus()
            hoverEnabled: true
            onWheel: {
                if(wheel.angleDelta.y > 0.0){
                    view.incrementCurrentIndex()
                }
                else
                    view.decrementCurrentIndex()
            }
        }
    }
}
