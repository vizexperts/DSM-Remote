import QtQuick 2.0

QtObject {
    property string icon : "Menu/layers"
    property string toolTip : "Tooltip"
    property string label : "Label"
    property bool selected : false
    property string name: "Button"
    property bool menuEnabled: true
}
