import QtQuick 2.1
import QtQuick.Layouts 1.0
import "javascripts/functions.js" as Functions

/* TODO:
 * Next releases of Qt will change the usage of selectionStart and selectionEnd properties.
 * Properties are to be change to read only properties.
 * For selection functionalities, there will be select (start, end) -function.
 * When these changes are made, this component's selectionStart & selectionEnd- functionalities
 * has to be changed corresponding selection funtionality.
 */

Rectangle {
    id: rectangle

    property int maxLength: -1
    property bool cursorVisible: false
    property bool dirty: false
    property alias selectionStart: lineEdit.selectionStart
    property alias selectionEnd: lineEdit.selectionEnd
    property alias cursorPosition: lineEdit.cursorPosition
    property alias setFocus: lineEdit.focus
    property alias textFieldEnable : lineEdit.enabled
    property alias text: lineEdit.text
    property alias textColor: lineEdit.color
    property alias placeholderText: placeholderTextInput.text
    property Gradient nullGradient: Gradient{}
    property alias validator: lineEdit.validator
    property alias acceptableInput : lineEdit.acceptableInput
    property alias readOnly : lineEdit.readOnly
    property alias maximumLength: lineEdit.maximumLength
    property alias echoMode: lineEdit.echoMode

    property string fieldValueTemp: ""

    signal clicked()
    signal textChange()
    signal accepted()
    signal textEdited()

    clip: true
    width: 150*rootStyle.uiScale

    Layout.minimumHeight: rootStyle.buttonIconSize
    Layout.maximumHeight: rootStyle.buttonIconSize

    Layout.preferredHeight: rootStyle.buttonIconSize
    height: rootStyle.buttonIconSize

    color: rootStyle.colorWhenDefault
    smooth: false
    border.color: rootStyle.borderColor
    border.width: rootStyle.borderWidth

    onAcceptableInputChanged:
    {
        if ( acceptableInput == false )
        {
            // removing the last entered char if it is not acceptable
            fieldValueTemp = text
            text= fieldValueTemp.replace(fieldValueTemp.substring(0, cursorPosition),
                                         fieldValueTemp.substring(0, cursorPosition-1))
        }
    }

    function selectTheText ()
    {
        lineEdit.selectAll()
    }

    Component.onCompleted: {
        if (!rootStyle.hoveredStateOn) stateHovered.destroy();
        if (!rootStyle.activeStateOn) stateActive.destroy();
        if (maxLength != -1 && lineEdit.text.length >= maxLength) {
            lineEdit.text = lineEdit.text.slice(0,maxLength);
            lineEdit.cursorPosition = maxLength;
        }
    }

    /**
     * Function which can be used to insert text in lineEdit.
     *
     * @param insert Text to insert.
     * @return nothing
     */
    function insertText(insert) {
        var start, end, resultText
        var selection = lineEdit.selectedText;
        var selectionStart = lineEdit.selectionStart;
        var selectionEnd = lineEdit.selectionEnd;
        if (selection == "") {
            var cursor = lineEdit.cursorPosition;
            start = lineEdit.text.substring(0,cursor);
            end = lineEdit.text.substring(cursor,lineEdit.text.length);
            resultText = ""+start+insert+end;
            lineEdit.text = resultText;
            lineEdit.cursorPosition = cursor+1;
        }
        else {
            start = lineEdit.text.substring(0,selectionStart);
            end = lineEdit.text.substring(selectionEnd,lineEdit.text.length);
            resultText = ""+start+insert+end;
            lineEdit.text = resultText;
            lineEdit.cursorPosition = selectionEnd;
        }
    }

    /**
     * Function which is used to remove text in text lineEdit.
     */
    function removeText() {
        var start, end, resultText

        var selection = lineEdit.selectedText;
        var selectionStart = lineEdit.selectionStart;
        var selectionEnd = lineEdit.selectionEnd;
        if (selection == "") {
            var cursor = lineEdit.cursorPosition;
            if (cursor > 0) {
                start = lineEdit.text.substring(0,cursor-1);
                end = lineEdit.text.substring(cursor,lineEdit.text.length);
                resultText = ""+start+end;
                lineEdit.text = resultText;
                lineEdit.cursorPosition = cursor-1;
            }
            else {
                lineEdit.cursorPosition = 0;
            }
        }
        else {
            start = lineEdit.text.substring(0,selectionStart);
            end = lineEdit.text.substring(selectionEnd,lineEdit.text.length);
            resultText = ""+start+end;
            lineEdit.text = resultText;
            lineEdit.cursorPosition = selectionStart;
        }
    }

    /**
     * Makes text selection between given parameters
     *
     * @param start where selection starts.
     * @param end where selection ends.
     *
     */
    function selectText (start,end){
        lineEdit.cursorPosition = start;
        lineEdit.moveCursorSelection(end);
        return;
    }

    MouseArea {
        id: mouseArea

        anchors.fill: rectangle
        onClicked: {
            lineEdit.focus = true
            rectangle.clicked()
        }
        hoverEnabled: true
        onEntered: root.cursorShape = Qt.IBeamCursor
        onExited: root.cursorShape = Qt.ArrowCursor
    }

    TextInput{
        id: placeholderTextInput
        anchors.fill: lineEdit
        opacity: !lineEdit.text.length && !lineEdit.activeFocus ? 1 : 0
        color: "gray"
        text: "Enter text"
        clip: true
        font.family: rootStyle.fontFamily
        font.weight: Math.round(rootStyle.fontWeight*0.8)
        font.pixelSize: lineEdit.font.pixelSize*0.8

        Behavior on opacity {
            NumberAnimation{
                duration: 90
            }
        }
    }

    TextInput {
        id: lineEdit

        Keys.onPressed: {
            dirty= true
            if(event.key === Qt.Key_F10){
                if(focus){
                    focus =false
                }
            }
        }

        onTextChanged: {
            rectangle.textChange()
            if(rootStyle.multiTouchInteractionActive){
                if(keyboard.text != text){
                    keyboard.text = text
                }
            }
            if (maxLength != -1 && lineEdit.text.length >= maxLength) {
                lineEdit.text = lineEdit.text.slice(0,maxLength);
                lineEdit.cursorPosition = maxLength;
                keyboard.text = lineEdit.text
            }
            if(focus && dirty ) {
                dirty = false
                textEdited()
            }
            if(text =="_"){
                text =""
                keyboard.text = text
            }
//            if(text.charAt(text.length-1)=="@" ||
//                    text.charAt(text.length-1)=="#" ||
//                    text.charAt(text.length-1)=="~" ||
//                    text.charAt(text.length-1)=="!" ||
//                    text.charAt(text.length-1)=="$" ||
//                    text.charAt(text.length-1)=="&" ||
//                    text.charAt(text.length-1)=="^" ||
//                    text.charAt(text.length-1)=="*" ||
//                    //text.charAt(text.length-1)=="-" ||
//                    text.charAt(text.length-1)=="+" ||
//                    text.charAt(text.length-1)==";" ||
//                    text.charAt(text.length-1)==":" ||
//                    //text.charAt(text.length-1)=="." ||
//                    text.charAt(text.length-1)=="," ||
//                    text.charAt(text.length-1)=="\"" ||
//                    text.charAt(text.length-1)=="|" ||
//                    text.charAt(text.length-1)=="\\" ||
//                    text.charAt(text.length-1)=="[" ||
//                    text.charAt(text.length-1)=="]" ||
//                    text.charAt(text.length-1)=="{" ||
//                    text.charAt(text.length-1)=="}" ||
//                    text.charAt(text.length-1)=="/" ||
//                    text.charAt(text.length-1)=="?"){
//                text= text.substring(0,text.length-1)

//            }
            keyboard.text = text
        }
        text: ""
        x: rootStyle.roundness*5+8
        cursorVisible: rectangle.cursorVisible
        width: rectangle.width - x *2
        font.family: rootStyle.fontFamily
        font.weight: rootStyle.fontWeight
        font.pixelSize : rootStyle.fontSize*0.9
        color: rootStyle.textColor
        anchors.verticalCenter: rectangle.verticalCenter
        selectedTextColor: rootStyle.selectedTextColor
        selectionColor: rootStyle.selectionColor
        selectByMouse:true

        onFocusChanged: {
            if(!focus){
                cursorPosition = 0
            }
            if((typeof(root) != "undefined") && root.multi_touch_interaction_active){
                if(focus && !readOnly){
                    keyboard.open(lineEdit)
                }
            }
            if(focus == false) dirty = false
        }

        Keys.forwardTo: [rectangle]

        Keys.onEnterPressed: {
            focus = false
            if((typeof(root) != "undefined") && root.multi_touch_interaction_active){
                keyboard.close()
            }
            accepted()
        }
        onAccepted: {
            rectangle.accepted()
        }

        Keys.onEscapePressed: {
            focus = false
            if((typeof(root) != "undefined") && root.multi_touch_interaction_active){
                keyboard.close()
            }
            accepted()
        }
        Keys.onLeftPressed: {
            cursorPosition--
        }
        Keys.onRightPressed: {
            cursorPosition++
        }
        Keys.onUpPressed: {
        }
        Keys.onDownPressed: {
        }
        Keys.onReleased: {
            event.accepted = true
        }
    }

    states: [
        State {
            id: stateActive
            name: "active"; when: lineEdit.activeFocus && !lineEdit.readOnly
            PropertyChanges { target: rectangle; border.color: rootStyle.borderColorWhenActive }
            PropertyChanges { target: rectangle; color: rootStyle.colorWhenDefault}
//            PropertyChanges { target: root; cursorShape: Qt.IBeamCursor }
        },
        State {
            id: stateHovered
            name: "entered"; when: mouseArea.containsMouse && !lineEdit.readOnly
            PropertyChanges { target: rectangle; border.color: rootStyle.borderColorWhenHovered }
//            PropertyChanges { target: root; cursorShape: Qt.IBeamCursor }
        }
    ]
}
