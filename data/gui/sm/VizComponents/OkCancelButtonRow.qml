import QtQuick 2.1

/**
 * Convenience component for Ok/Cancel button Row
 */

Row{
    height: childrenRect.height
    width: childrenRect.width
    spacing: 30

    signal okButtonPressed()
    signal cancelButtonPressed()

    VizButton{
        iconSource: rootStyle.getIconPath("accept")
        onClicked: {
            okButtonPressed()
        }
    }
    VizButton{id: cancelButton
        iconSource: rootStyle.getIconPath("cancel")
        onClicked: {
            cancelButtonPressed()
        }
    }
}

