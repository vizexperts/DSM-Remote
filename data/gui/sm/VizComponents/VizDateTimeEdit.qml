import QtQuick 2.1

Rectangle{
    id: arrivalTimeRect
    width: parent.width
    height: childrenRect.height
    color: "transparent"
    border.width: 2
    border.color: (timeGroupBox.selectedOption === timeGroupBox.optionArrivalTime) ?
                                          rootStyle.colorWhenSelected : "transparent"
    radius: 15
    VizLabel{
        text: "Start Time"
    }
    VizLabel{
        id: arrivalTimeTextEdit
        anchors.left: parent.horizontalCenter
        anchors.leftMargin: -15
        text: Qt.formatDateTime(arrivalTime, dateFormat)
    }

    VizButton
    {
        id : pbArrivalCalenderButtonID
        anchors.left : arrivalTimeTextEdit.right
        anchors.leftMargin: -5

        onClicked:
        {
            if(timeGroupBox.selectedOption === timeGroupBox.optionArrivalTime)
            {
                timeGroupBox.selectedOption = timeGroupBox.optionNone
            }
            else
            {
                timeGroupBox.selectedOption = timeGroupBox.optionArrivalTime
                date.setDate(arrivalTime)
                time.setTime(arrivalTime)
            }
        }
        iconSource: rootStyle.getIconPath("calendar")
        backGroundVisible: false
    }
}
