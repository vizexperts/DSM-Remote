import QtQuick 2.1
import "javascripts/functions.js" as Functions

Rectangle {
    id: datePickerRect
    //objectName: "datePicker"
    width: dateItem.width+timeItem.width+60
    clip: true

    signal timeChanged()
    signal dateChanged()

    property alias dayOfMonth: dateItem.date
    property alias month: dateItem.month
    property alias year: dateItem.year
    property alias hours: timeItem.hours
    property alias minutes: timeItem.minutes
    property alias seconds: timeItem.seconds

    function setDateTime(dateTime){
        dateItem.setDate(dateTime)
        timeItem.setTime(dateTime)
    }

    visible: false

    Behavior on visible {
        PropertyAnimation{
            easing.type: Easing.OutBack; duration: 100
        }
    }

    //visible: false
    Rectangle{
        anchors.fill: parent
        color: rootStyle.colorWhenDefault
        opacity: 0.8
        radius: parent.radius
    }
    MouseArea{
        anchors.fill: parent
        onWheel: wheel.accepted = true
        hoverEnabled: true
        acceptedButtons: Qt.AllButtons
    }
    height: timeItem.height + 20
    color: "transparent"
    border.color: rootStyle.borderColor
    border.width: rootStyle.borderWidth
    radius: Functions.countRadius(rootStyle.roundness, width, height, 0, 1)
    FlickableTime{
        id: timeItem
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20
        onChanged: datePickerRect.timeChanged()
    }

    FlickableDate{
        id: dateItem
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20
        onChanged: datePickerRect.dateChanged()
    }
}
