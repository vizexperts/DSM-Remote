import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Controls.Styles 1.0

TableView{
    style: TableViewStyle{
        backgroundColor : rootStyle.colorWhenDefault
        alternateBackgroundColor : Qt.darker(backgroundColor, 1.1)
        headerDelegate: VizButton{
            text: styleData.value
        }
        rowDelegate: Rectangle {
            height: rootStyle.buttonIconSize
            property color selectedColor: styleData.hasActiveFocus ? "#38d" : "#999"
            gradient: Gradient {
                GradientStop {
                    color: styleData.selected ? Qt.lighter(selectedColor, 1.3) :
                                                styleData.alternate ? alternateBackgroundColor : backgroundColor
                    position: 0
                }
                GradientStop {
                    color: styleData.selected ? Qt.lighter(selectedColor, 1.0) :
                                                styleData.alternate ? alternateBackgroundColor : backgroundColor
                    position: 1
                }
            }
            Rectangle {
                anchors.bottom: parent.bottom
                width: parent.width
                height: 1
                color: styleData.selected ? Qt.darker(selectedColor, 1.4) : "transparent"
            }
            Rectangle {
                anchors.top: parent.top
                width: parent.width ; height: 1
                color: styleData.selected ? Qt.darker(selectedColor, 1.1) : "transparent"
            }
        }
        itemDelegate: VizLabel{
            text: styleData.value
        }
    }
}
