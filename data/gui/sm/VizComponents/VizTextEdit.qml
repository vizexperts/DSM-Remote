import QtQuick 2.1
import "javascripts/functions.js" as Functions

/* TODO:
 * Next releases of Qt will change the usage of selectionStart and selectionEnd properties.
 * Properties are to be change to read only properties.
 * For selection functionalities, there will be select (start, end) -function.
 * When these changes are made, this component's selectionStart & selectionEnd- functionalities
 * has to be changed corresponding selection funtionality.
 */

Rectangle {
    id: rectangle

    property int maxLength: -1
    property bool cursorVisible: false
    property alias selectionStart: lineEdit.selectionStart
    property alias selectionEnd: lineEdit.selectionEnd
    property alias cursorPosition: lineEdit.cursorPosition
    property alias setFocus: lineEdit.focus
    property alias textFieldEnable : lineEdit.enabled
    property alias text: lineEdit.text
    property alias textColor: lineEdit.color
    property alias placeholderText: placeholderTextInput.text
    property Gradient nullGradient: Gradient{}
    property alias readOnly : lineEdit.readOnly
    clip: true

    signal clicked()
    signal textChange()
    signal accepted()

    width: 150
    height: 30
    MouseArea {
        id: mouseArea
        anchors.fill: rectangle
        onClicked: rectangle.clicked()
        hoverEnabled: true
    }
    Rectangle {
        id: gradientContainer
        anchors.fill: rectangle
        color: Qt.lighter(rootStyle.colorWhenDefault)
        smooth: false
        border.color: rootStyle.borderColor
        border.width: rootStyle.borderWidth
    }

    VizScrollBar{
        target: flickable
    }

    Flickable{
        id: flickable
        width: parent.width
        height: parent.height


        function ensureVisible(r)
        {
            if (contentX >= r.x)
                contentX = r.x
            else if (contentX+width <= r.x+r.width)
                contentX = r.x+r.width-width
            if (contentY >= r.y)
                contentY = r.y
            else if (contentY+height <= r.y+r.height)
                contentY = r.y+r.height-height
        }

        contentWidth: lineEdit.paintedWidth
        contentHeight: lineEdit.paintedHeight
        TextInput{
            id: placeholderTextInput
            anchors.fill: lineEdit
            font.italic: true
            opacity: !lineEdit.text.length && !lineEdit.activeFocus ? 1 : 0
            color: "gray"
            text: "Enter text"
            clip: true

            Behavior on opacity {
                NumberAnimation{
                    duration: 90
                }
            }
        }

        TextEdit {
            id: lineEdit
            anchors.top: parent.top
            anchors.topMargin: 5
            onTextChanged: {
                rectangle.textChange()
                if(rootStyle.multiTouchInteractionActive){
                    if(keyboard.text !== text){
                        keyboard.text = text
                    }
                }
                if (maxLength != -1 && lineEdit.text.length >= maxLength) {
                    lineEdit.text = lineEdit.text.slice(0,maxLength);
                    lineEdit.cursorPosition = maxLength;
                }

                if(text =="_"){
                    text =""
                }

//                if(text.charAt(text.length-1)=="@" ||
//                        text.charAt(text.length-1)=="#" ||
//                        text.charAt(text.length-1)=="~" ||
//                        text.charAt(text.length-1)=="!" ||
//                        text.charAt(text.length-1)=="$" ||
//                        text.charAt(text.length-1)=="&" ||
//                        text.charAt(text.length-1)=="^" ||
//                        text.charAt(text.length-1)=="*" ||
//                        text.charAt(text.length-1)=="-" ||
//                        text.charAt(text.length-1)=="+" ||
//                        text.charAt(text.length-1)==";" ||
//                        text.charAt(text.length-1)==":" ||
//                        text.charAt(text.length-1)=="." ||
//                        text.charAt(text.length-1)=="," ||
//                        text.charAt(text.length-1)=="\"" ||
//                        text.charAt(text.length-1)=="|" ||
//                        text.charAt(text.length-1)=="\\" ||
//                        text.charAt(text.length-1)=="[" ||
//                        text.charAt(text.length-1)=="]" ||
//                        text.charAt(text.length-1)=="{" ||
//                        text.charAt(text.length-1)=="}" ||
//                        text.charAt(text.length-1)=="/" ||
//                        text.charAt(text.length-1)=="?"
//                        )
//                {
//                text=text.substring(0,text.length-1)
//                }
                keyboard.text = text
            }
            text: ""
            x: rootStyle.roundness*5+8
            cursorVisible: rectangle.cursorVisible
            width: rectangle.width - x *2
            wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
            height: rectangle.height*0.9
            font.family: rootStyle.fontFamily
            font.pixelSize: rootStyle.fontSize
            font.weight: rootStyle.fontWeight
            color: rootStyle.textColor
            onCursorRectangleChanged: flickable.ensureVisible(cursorRectangle)
    //        anchors.verticalCenter: rectangle.verticalCenter
            selectedTextColor: rootStyle.selectedTextColor
            selectionColor: rootStyle.selectionColor
            selectByMouse:true
            onFocusChanged: {
                if(!focus){
                    cursorPosition = 0
                }
                if((typeof(root) != "undefined") && root.multi_touch_interaction_active){
                    if(focus && !readOnly){
                        keyboard.open(lineEdit)
                    }
                    else{
                        accepted()
                    }
                }
            }

            Keys.forwardTo: [rectangle]
            Keys.onEnterPressed: {
                focus = false
                if((typeof(root) != "undefined") && root.multi_touch_interaction_active){
                    keyboard.close()
                }
                accepted()
            }
    //        onAccepted: {
    //            rectangle.accepted()
    //        }

            Keys.onEscapePressed: {
                focus = false
                if((typeof(root) != "undefined") && root.multi_touch_interaction_active){
                    keyboard.close()
                }
                accepted()
            }
            Keys.onLeftPressed: {
                cursorPosition--
            }
            Keys.onRightPressed: {
                cursorPosition++
            }
            Keys.onUpPressed: {
            }
            Keys.onDownPressed: {
            }
    //        Keys.onDeletePressed: {
    //            removeText()
    //        }

            Keys.onReleased: {
                event.accepted = true
            }
        }
    }


    function selectTheText ()
    {
        lineEdit.selectAll()
    }

    Component.onCompleted: {
        if (!rootStyle.hoveredStateOn) stateHovered.destroy();
        if (!rootStyle.activeStateOn) stateActive.destroy();
        if (maxLength != -1 && lineEdit.text.length >= maxLength) {
            lineEdit.text = lineEdit.text.slice(0,maxLength);
            lineEdit.cursorPosition = maxLength;
        }
    }

    /**
     * Function which can be used to insert text in lineEdit.
     *
     * @param insert Text to insert.
     * @return nothing
     */
    function insertText(insert) {
        var start, end, resultText
        var selection = lineEdit.selectedText;
        var selectionStart = lineEdit.selectionStart;
        var selectionEnd = lineEdit.selectionEnd;
        if (selection == "") {
            var cursor = lineEdit.cursorPosition;
            start = lineEdit.text.substring(0,cursor);
            end = lineEdit.text.substring(cursor,lineEdit.text.length);
            resultText = ""+start+insert+end;
            lineEdit.text = resultText;
            lineEdit.cursorPosition = cursor+1;
        }
        else {
            start = lineEdit.text.substring(0,selectionStart);
            end = lineEdit.text.substring(selectionEnd,lineEdit.text.length);
            resultText = ""+start+insert+end;
            lineEdit.text = resultText;
            lineEdit.cursorPosition = selectionEnd;
        }
    }

    /**
     * Function which is used to remove text in text lineEdit.
     */
    function removeText() {
        var start, end, resultText

        var selection = lineEdit.selectedText;
        var selectionStart = lineEdit.selectionStart;
        var selectionEnd = lineEdit.selectionEnd;
        if (selection == "") {
            var cursor = lineEdit.cursorPosition;
            if (cursor > 0) {
                start = lineEdit.text.substring(0,cursor-1);
                end = lineEdit.text.substring(cursor,lineEdit.text.length);
                resultText = ""+start+end;
                lineEdit.text = resultText;
                lineEdit.cursorPosition = cursor-1;
            }
            else {
                lineEdit.cursorPosition = 0;
            }
        }
        else {
            start = lineEdit.text.substring(0,selectionStart);
            end = lineEdit.text.substring(selectionEnd,lineEdit.text.length);
            resultText = ""+start+end;
            lineEdit.text = resultText;
            lineEdit.cursorPosition = selectionStart;
        }
    }

    /**
     * Makes text selection between given parameters
     *
     * @param start where selection starts.
     * @param end where selection ends.
     *
     */
    function selectText (start,end){
        lineEdit.cursorPosition = start;
        lineEdit.moveCursorSelection(end);
        return;
    }


    states: [
        State {
            id: stateActive
            name: "active"; when: lineEdit.focus
//            PropertyChanges { target: gradientContainer; gradient: (rootStyle.gradientActiveOn)?rootStyle.gradientWhenActive:rectangle.nullGradient;}
            PropertyChanges { target: gradientContainer; border.color: rootStyle.borderColorWhenActive }
            PropertyChanges { target: gradientContainer; color: rootStyle.colorWhenDefault}

        },
        State {
            id: stateHovered
            name: "entered"; when: mouseArea.containsMouse
//            PropertyChanges { target: gradientContainer; gradient: (rootStyle.gradientHoveredOn)?rootStyle.gradientWhenHovered:rectangle.nullGradient;}
            PropertyChanges { target: gradientContainer; border.color: rootStyle.borderColorWhenHovered }
//            PropertyChanges { target: gradientContainer; color: rootStyle.colorWhenDefault}
        }
    ]
}
