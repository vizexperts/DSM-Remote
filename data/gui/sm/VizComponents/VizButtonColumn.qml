import QtQuick 2.1

Item{
    id: buttonColumn
    width: column.width
    height: column.height
    clip:true

    Component.onCompleted: {
        menuHolder.loaded(objectName)

        //! Run a process on the model to remove unwanted menus
        //! Too bad i have to write this, since QML ListElement does not support specifying a script in the definition
        for(var i = 0; i < buttonColumn.data.length ; ++i){
            var child = buttonColumn.data[i]
            if(typeof( child.name ) !== "undefined"){
                if(child.menuEnabled){
                    column.model.append({
                                         "icon": child.icon,
                                         "toolTip" :  child.toolTip,
                                         "label" : child.label,
                                         "selected" : child.selected,
                                         "name" : child.name,
                                         "menuEnabled" : child.menuEnabled
                        }
                    )
                 }
            }
        }
    }

    Connections{
        target: root
        onNowRemoveHighLight:removeHighlight();
    }

    function removeHighlight()
    {
        listModel.get(currentIndex).selected = false
        islistEnabled=true
    }
    function disableListOption(option)
    {
        islistEnabled=false
        listModel.get(currentIndex).selected = true;
    }

    property alias listModel: column.model
    property alias currentIndex: column.currentIndex
    property alias columnHeight: column.height
    property alias columnWidth: column.width
    property alias listDelegate: column.delegate
    property alias islistEnabled: column.enabled

    signal optionClicked(string option)

    property string position: "right"

    VizMenuOption{
        id: dummyOption
        visible: false
    }

    ListView{
        id: column
        anchors.centerIn: parent
        enabled: true
        width: dummyOption.width + scrollBar.width+2
        height: Math.min(dummyOption.height*(count+1), root.height*0.6)
        boundsBehavior: Flickable.StopAtBounds
        cacheBuffer :0
        model: ListModel{}
        VizScrollBar{
            id:scrollBar
            target: column
            interactive: false
        }
        header: VizMenuOption{
            iconPath: (position === "right") ? rootStyle.getIconPath("backRight") : rootStyle.getIconPath("backLeft")
            toolTipText: "Back"
            labelText: "Back"
            optionName: "Back"
        }

        delegate: VizMenuOption{
            iconPath: rootStyle.getIconPath(icon)
            toolTipText: toolTip
            labelText: label
            optionName: name
            isSelected: selected
            toRemoveMenu: !menuEnabled
        }
    }
}
