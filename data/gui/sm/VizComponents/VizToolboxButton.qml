import QtQuick 2.1
import QtQuick.Layouts 1.0
//import "VizComponents"

VizButton {
    enabled: false
    scale: 0.9
    checkable: true
    checked: false

    visible: true

    property bool demoFeature : false

    //! Set to true if this button should not listen to disableAllButtons signal from parent
    property bool exclusiveButton: false

    //! Set to true if this button should not be toggled off when other buttons are pressed
    property bool indicatorButton: false

    Connections{
        target: toolBoxId
        onDisableAllButtons: {

            if(checked){
                checked = false

                if(!indicatorButton){
                    clicked()
                }
            }

            if(!exclusiveButton) {
                enabled = false
            }
        }

    }

    signal toolboxButtonClicked()

    onClicked: {
        checkStateAndCallOptionClick(this)
    }
}
