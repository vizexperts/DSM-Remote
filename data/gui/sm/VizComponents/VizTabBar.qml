import QtQuick 2.1
import "javascripts/functions.js" as Functions

Item {
    id: tabbar
    property real tabHeight: tabrow.height
    property alias tabWidth: tabrow.width
    property int tabItemWidth: 0

    Keys.onRightPressed: {
        if (tabFrame && tabFrame.current < tabFrame.count - 1)
            tabFrame.current = tabFrame.current + 1
    }
    Keys.onLeftPressed: {
        if (tabFrame && tabFrame.current > 0)
            tabFrame.current = tabFrame.current - 1
    }

    height: tabrow.height
    width: tabrow.width

    property Item tabFrame
    onTabFrameChanged:parent = tabFrame
    visible: tabFrame ? tabFrame.tabsVisible : true
    property string tabBarAlignment: "leftAligned"
    property int tabHSpace: 5
    property int tabVSpace: 5

    function tab(index) {
        for (var i = 0; i < tabrow.children.length; ++i) {
            if (tabrow.children[i].tabindex === index) {
                return tabrow.children[i]
            }
        }
        return null;
    }

    VizLabel{
        visible: false
        id: dummyLabel
        text: "dummy"
    }

    Row {
        id: tabrow
        property int paintMargins: 1
        states:[
            State {
                when: tabBarAlignment == "center"
                name: "centered"
                AnchorChanges {
                    target:tabrow
                    anchors.horizontalCenter: tabbar.horizontalCenter
                }
            },
            State {
                name: "rightAligned"
                when: tabBarAlignment == "rightAligned"
                AnchorChanges {
                    target: tabrow
                    anchors.right: tabBar.right
                }
            }
        ]

       height: repeater.height

        Repeater {
            id:repeater
            focus:true
            model: tabFrame ? tabFrame.tabs.length : null
            height: dummyLabel.height
            delegate: Item {
                id:tab
                focus:true
                property int tabindex: index
                property bool selected : tabFrame.current === index
                visible: tabFrame.tabs[index].visible
                z: selected ? 1 : -1
                width: Math.max(textitem.width + 15, tabItemWidth)
                height: Math.max(textitem.height + 15, tabHeight)
                Rectangle {
                    id: rect
                    color: rootStyle.colorWhenDefault
                    gradient: tab.selected ? rootStyle.gradientWhenSelected : mousearea.containsMouse ? rootStyle.gradientWhenHovered : rootStyle.gradientWhenDefault
                    Rectangle{
                        visible: !tab.selected
                        anchors.bottom: parent.bottom
                        width: parent.width
                        height: 2
                        color: rootStyle.colorWhenSelected
                    }

                    smooth: true
                    property bool first: index === 0
                    property string tabType: tabFrame.count === 1 ? "only" : index === 0 ? "start" :
                                                                index == tabFrame.count-1 ? "end" : "middle"

                    states: [
                        State {
                            name: "OnlyElement"
                            when: rect.tabType === "only"
                        },
                        State {
                            name: "StartElement"
                            when: rect.tabType === "start"
                        },
                        State {
                            name: "EndElement"
                            when: rect.tabType == "end"
                        },
                        State {
                            name: "MiddleElement"
                            when: rect.tabType == "middle"
                        }
                    ]

                    anchors.fill: parent
                    anchors.margins: -tabrow.paintMargins
                    VizLabel{
                        id: textitem
                        text:  tabFrame.tabs[index].title
                        textColor: tab.selected ? rootStyle.colorWhenDefault : rootStyle.textColor
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                MouseArea {
                    id: mousearea
                    anchors.fill: parent
                    hoverEnabled: true
                    onPressed: tabFrame.current = index
                }
            }
        }
    }
}
