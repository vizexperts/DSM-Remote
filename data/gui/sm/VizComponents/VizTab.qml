import QtQuick 2.1
import QtQuick.Layouts 1.0

ColumnLayout{
    id:tab
    onImplicitHeightChanged: {
        parent.parent.parent.__setOpacities()
    }

    property bool isCurrent: false
    anchors.fill: parent
    property string title
    property int contentMargin
    Behavior on opacity {
        NumberAnimation{
            duration: 200
        }
    }
}
