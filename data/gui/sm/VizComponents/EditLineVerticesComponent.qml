import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../VizComponents"

ColumnLayout {
    id: editLineVerticesComponent

    spacing: 5

    property alias latitude: latitudeLineEdit.latLongText
    property alias longitude: longitudeLineEdit.latLongText
    property bool editable: false
    property alias enableDelete :deleteButtonID.enabled
    signal deleteVertex()
    signal handleLatChanged()
    signal handleLongChanged()
    signal setEditMode( bool mode )

    VizLatLongTextEdit{
        id: latitudeLineEdit
        type: type_LATITUDE
        enabled: editable
        Layout.minimumWidth: 250*rootStyle.uiScale
        labelText: "Latitude"
        placeholderText: "Enter Latitude"
        onLatLongChanged: handleLatChanged();
        Keys.onTabPressed: longitudeLineEdit.giveFocus()
    }
    VizLatLongTextEdit{
        id: longitudeLineEdit
        type: type_LONGITUDE
        enabled: editable
        labelText: "Longitude"
        placeholderText: "Enter Longitude"
        onLatLongChanged: handleLongChanged();
        Keys.onTabPressed: latitudeLineEdit.giveFocus()
    }
    RowLayout{
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 30

        VizButton{
            text: "Edit Mode"
            checkable: true
            onClicked: checked ? setEditMode( true ) : setEditMode( false )
        }

        VizButton{
            id : deleteButtonID
            iconSource: rootStyle.getIconPath("delete")
            scale: 0.8
            onClicked: deleteVertex()
            enabled : false
        }
    }
}
