import QtQuick 2.1

/**
  Custom Component to show a label and a Number.
**/

VizLabelTextEdit {

    property bool valid: false
    property real minimum: 0
    property real maximum: 10000
    property int decimals: 4

    onLineEditTextChanged: {
        if(lineEditText > maximum || lineEditText < minimum){
            valid = false
            textColor = "red"
        }
        else{
            valid = true
            textColor = rootStyle.textColor
        }
    }
}
