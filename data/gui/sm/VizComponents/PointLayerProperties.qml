import QtQuick 2.1

Item{
    id : pointIcon
    objectName : "PointLayerProperties"
    clip: true

    //signals
    signal onOkButtonClicked()
    signal onCancelButtonClicked()
    signal onApplyButtonClicked()
    signal onBrowseButtonClicked()

    //Properties
    property string selectedIcon

    property alias  buttonRowVisible : pointIcon.visible

    GridView{
        id: symbolsGridView
        anchors.left : parent.left
        anchors.top  : parent.top
        anchors.right : parent.right
        anchors.bottom : buttonRow.top
        clip: true
        anchors.margins  : 10
        model: (typeof(symbolModel) !== "undefined") ? symbolModel : "undefined"
        VizScrollBar{
            target: symbolsGridView
            visible: pointIcon.visible
        }

        delegate: Rectangle{
            id : iconDelegate
            width : 60
            height : 60
            radius  : 4
            color : (selectedIcon === modelData.name)? "white" : "transparent"
            Image {
                id : iconImage
                source : "file:/" + path
                width: 40
                height: 40
                anchors.centerIn: parent
            }
            MouseArea {
                id : mouseArea
                anchors.fill: parent
                hoverEnabled : true
                onClicked : {
                    iconDelegate.GridView.view.currentIndex = index
                    if(selectedIcon === modelData.name){
                        selectedIcon = ""
                    }
                    else{
                        selectedIcon = modelData.name
                    }
                }
                onDoubleClicked: {
                    selectedIcon = modelData.name
                    onOkButtonClicked()
                }
            }
        }
    }
    Row{
        id : buttonRow
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        spacing: 20
        VizButton{
            text : "Ok"
            id   : button1
            onClicked : onOkButtonClicked()
        }
        VizButton{
            id   : button2
            text : "Apply"
            onClicked : onApplyButtonClicked()
        }
        VizButton{
            id  : button4
            text: "Browse"
            onClicked : onBrowseButtonClicked()

        }
        VizButton{
            id  : button3
            text: "Cancel"
            onClicked : onCancelButtonClicked()

        }

    }
}
