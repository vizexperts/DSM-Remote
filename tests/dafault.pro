#Ascii Object 
#Generator VizSimulation 
World {
  DSMComponents 4 {
TourComponent {
      TourMap 0 {
}
SequenceMap 0 {
}
SpeedFactor 1 
Name "TourComponent" 
UID fe31ce8d-e3f0-4035-a0aa-7c3f41752c84 
}
    PresentationComponent {
      PresentationMap 0 {
}
Name "PresentationComponent" 
UID d6fdac7a-c622-437a-80b6-f35fb2d899ff 
}
    ResourceManagerComponent {
      ResourceMap 0 {
}
Name "ResourceManagerComponent" 
UID e15b274a-75b3-435a-a3e9-38567404be52 
}
    UserFolderComponent {
      Folders {  UserFolders.pro }
Name "UserFolderComponent" 
UID d06af667-9412-49ab-a26e-2fd286105c9e 
}
  }
Components 3 {
AnimationComponent {
      StartTime 20170525T174505 
EndTime 20170525T174505 
Name "AnimationComponent" 
UID e9fb754e-6c68-4af6-a4e8-3ed997741173 
}
    WeatherComponent {
      Effects 6 {
FogEffect {
          Effect FALSE 
FogDensity 6.25e-005 
FogColor 1 1 1 0 
FogSphereHeight 7.65376e+006 
EffectType FOG 
}
        CloudEffect {
          Effect FALSE 
CloudDensity MEDIUM 
}
        PrecipitationEffect {
          EffectType RAIN 
Effect FALSE 
}
        OceanEffect {
          Effect FALSE 
}
        SunMoonLightEffect {
          Effect TRUE 
}
        LightEffect {
Effect TRUE 
}
}
Name "WeatherComponent" 
UID 42329674-974f-49bd-85ec-0951f9504872 
}
    ProjectSettingsComponent {
      ProjectSettings {
"MilitarySymbolRepresentationMode"   5 
"MilitarySymbolSize"   32 
}
Name "ProjectSettingsComponent" 
UID 02c5a074-bb68-4afb-a646-d7111453141e 
}
  }
Objects 1 {
RasterObject {
      URL "../../data/RasterImages/world.tif" 
Connection "" 
MinLevel 0 
MaxLevel 99 
Subdataset -1 
MinX 0 
MinY 0 
MaxX 0 
MaxY 0 
OverrideValue FALSE 
HasNoDataValue FALSE 
NoDataValue -1 
Opacity 1 
RasterIndex 0 
EnhancementType NONE 
BandOrder 1 2 3 
Visibility TRUE 
Selected FALSE 
Parent 8d9dd456-a18f-42c4-bfc1-80ca1f4cf652 
Name "Base" 
UID 63e906a2-4265-4406-846c-e97375f7288a 
}
  }
Property {  property.pro }
Name "World" 
UID 8d9dd456-a18f-42c4-bfc1-80ca1f4cf652 
}
