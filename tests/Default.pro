#Ascii Object 
#Generator VizSimulation 
World {

Components 1 {
    WeatherComponent {
      Effects 6 {
FogEffect {
          Effect FALSE 
FogDensity 0.2 
FogColor 1 1 1 0 
FogSphereHeight 7.65376e+006 
EffectType FOG 
}
        CloudEffect {
          Effect FALSE 
CloudDensity MEDIUM 
}
        PrecipitationEffect {
          EffectType RAIN 
Effect FALSE 
}
        OceanEffect {
          Effect FALSE 
}
        SunMoonLightEffect {
          Effect TRUE 
}
        LightEffect {
Effect TRUE 
}
}
Name "WeatherComponent" 
UID c25fcaa3-ce56-4520-96e9-701d739cc1e2 
}

  }
Objects 1 {
RasterObject {
      URL "../../data/RasterImages/world.tif" 
      RasterOrderIndex 0 
Visibility TRUE 
      Selected TRUE 
      Name "world" 
      UID b68dc24d-0be9-4e25-b355-77d9794341bf 
}
  }
Name "World" 
UID 8d9dd456-a18f-42c4-bfc1-80ca1f4cf652 
}
