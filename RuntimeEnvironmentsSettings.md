Environment variable and settings decription

- Environment variaables

-- SDK/DSM Specific

--- TERRAINSDK_APP_FOLDER : (directory url) Set the directory to be used for daving user project, styles and settings data. If not set, APPDATA is used.
e.g. set TERRAINSDK_APP_FOLDER=C:/path/to/some/directory
Note: Only Forward Slash("/") should be used as a path seperator.

--- TKP_SDK_TILE_RANGE_FACTOR : (int) , 2- 8 , default : 4

--- TERRAINSDK_LOG_LEVEL : (enum) Same as osg log level variable,
ALWAYS
ERROR
WARN
INFO
DEBUG

-- OSG specific variables

--- OSG_COMPUTE_NEAR_FAR_MODE : (enum) Specify near far computation mode for culling
DO_NOT_COMPUTE_NEAR_FAR
COMPUTE_NEAR_FAR_USING_BOUNDING_VOLUMES
COMPUTE_NEAR_FAR_USING_PRIMITIVES

--- OSG_NEAR_FAR_RATIO : Double value specifying near far ratio , default : 0.000001

--- OSG_DISPLAY_TYPE : (enum)
MONITOR
POWERWALL
REALITY_CENTER
HEAD_MOUNTED_DISPLAY

--- OSG_STEREO_MODE (enum)
OSG_STEREO_MODE
QUAD_BUFFER
ANAGLYPHIC
HORIZONTAL_SPLIT
VERTICAL_SPLIT
LEFT_EYE
RIGHT_EYE
HORIZONTAL_INTERLACE
VERTICAL_INTERLACE

--- OSG_STEREO (enum)
OFF
ON

--- OSG_EYE_SEPARATION : (float)

--- OSG_SCREEN_WIDTH : (float)

--- OSG_SCREEN_HEIGHT : (float)

--- OSG_SCREEN_DISTANCE : (float)

--- OSG_SPLIT_STEREO_HORIZONTAL_EYE_MAPPING : (enum)
LEFT_EYE_LEFT_VIEWPORT
LEFT_EYE_RIGHT_VIEWPORT

--- OSG_SPLIT_STEREO_HORIZONTAL_SEPARATION : (integer)

--- OSG_SPLIT_STEREO_VERTICAL_EYE_MAPPING : (enum)
LEFT_EYE_TOP_VIEWPORT
LEFT_EYE_BOTTOM_VIEWPORT

--- OSG_SPLIT_STEREO_AUTO_ADJUST_ASPECT_RATIO : (enum)
OFF
ON

--- OSG_SPLIT_STEREO_VERTICAL_SEPARATION : (integer)

--- OSG_MAX_NUMBER_OF_GRAPHICS_CONTEXTS : (integer)

--- OSG_DEFAULT_BIN_SORT_MODE : (enum)
SORT_BY_STATE
SORT_BY_STATE_THEN_FRONT_TO_BACK
SORT_FRONT_TO_BACK
SORT_BACK_TO_FRONT

--- OUTPUT_THREADLIB_SCHEDULING_INFO :(boolean)

--- OSG_DATABASE_PAGER_GEOMETRY or OSG_DATABASE_PAGER_DRAWABLE : (enum)
DoNotModify
DisplayList or DL
VBO
VertexArrays or VA

--- OSG_DO_PRE_COMPILE : (enum)
yes, YES, on, ON

--- OSG_MINIMUM_COMPILE_TIME_PER_FRAME : (float)

--- OSG_MAXIMUM_OBJECTS_TO_COMPILE_PER_FRAME : (integer)

--- OSG_NOTIFY_LEVEL or OSGNOTIFYLEVEL : (enum)
ALWAYS
FATAL
WARN
NOTICE
DEBUG_INFO
DEBUG_FP
DEBUG
INFO


--- OSGEARTH Specific Variables


--- OSGEARTH_CACHE_PATH : Sets up a cache at the specified folder (path)

--- OSGEARTH_CACHE_ONLY : Directs osgEarth to ONLY use the cache and no data sources (set to 1)
 	
--- OSGEARTH_NO_CACHE : Directs osgEarth to NEVER use the cache (set to 1)

--- OSGEARTH_CACHE_DRIVER : Sets the name of the plugin to use for caching (default is “filesystem”)

--- OSG_NUM_DATABASE_THREADS : Sets the total number of threads that the OSG DatabasePager will use to load terrain tiles and feature data tiles.

--- OSG_NUM_HTTP_DATABASE_THREADS : Sets the number of threads in the Pager’s thread pool (see above) that should be used for “high-latency” operations.
		(Usually this means operations that do not read data from the cache, or are expected to take more time than average.)

--- OSGEARTH_NOTIFY_LEVEL : (enum)
DEBUG
INFO
NOTICE(default)
WARN

--- OSGEARTH_MP_PROFILE : Dumps verbose profiling and timing data about the terrain engine’s tile generator to the console. Set to 1 for detailed per-tile timings; Set to 2 for average tile load time calculations.

--- OSGEARTH_MP_DEBUG : Draws tile bounding boxes and tilekey labels atop the map.

--- OSGEARTH_MERGE_SHADERS : Consolidate all shaders within a single shader program; this is required for GLES (mobile devices) and is therefore useful for testing. (set to 1).

--- OSGEARTH_DUMP_SHADERS : Prints composed shader programs to the console (set to 1).

--- OSGEARTH_DEFAULT_FONT : Name of the default font to use for text symbology.

--- OSGEARTH_MIN_STAR_MAGNITUDE : Smallest star magnitude to use in SkyNode.

--- OSGEARTH_USE_PBUFFER_TEST : Directs the osgEarth platform Capabilities analyzer to create a PBUFFER-based graphics context for collecting GL support information. (set to 1)