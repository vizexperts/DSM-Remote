#ifndef PDFPLUGIN_PLUGIN_H
#define PDFPLUGIN_PLUGIN_H

#include <QtQml/QQmlExtensionPlugin>

class PDFPluginPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")
#endif
    
public:
    void registerTypes(const char *uri);
};

#endif // PDFPLUGIN_PLUGIN_H

