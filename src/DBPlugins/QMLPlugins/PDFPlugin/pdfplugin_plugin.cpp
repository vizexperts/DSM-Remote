#include "pdfplugin_plugin.h"
#include "pdfview.h"

#include <QtQml/qqml.h>

void PDFPluginPlugin::registerTypes(const char *uri)
{
    // @uri Viz.sandModel.pdfView
    qmlRegisterType<PDFView>(uri, 1, 0, "PDFView");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(PDFPlugin, PDFPluginPlugin)
#endif

