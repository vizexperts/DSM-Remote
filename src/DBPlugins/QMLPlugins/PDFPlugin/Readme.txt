1. Install visual studio plugin for QT5 (after installing you can see QT5 option in tool bar for VS)
2. Click on Qt5 and set Qt5 options (set the qmake path of the qt you want to use)
3. Now open .pro file present in the folder
4. set include path for poppler and qt
5. set linker path for poppler-qt5 and qt's libs
6. set working directory to be the folder containing poppler's dll
7. Build your plugin for release and debug

