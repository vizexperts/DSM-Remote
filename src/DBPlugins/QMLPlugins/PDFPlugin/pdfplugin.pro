TEMPLATE = lib
TARGET = PDFPlugin
QT += qml quick widgets
CONFIG += plugin

TARGET = $$qtLibraryTarget($$TARGET)
uri = Viz.sandModel.pdfView

# Input
SOURCES += \
    pdfplugin_plugin.cpp \
    pdfview.cpp

HEADERS += \
    pdfplugin_plugin.h \
    pdfview.h

OTHER_FILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    maemo5 | !isEmpty(MEEGO_VERSION_MAJOR) {
        installPath = /usr/lib/qt4/imports/$$replace(uri, \\., /)
    } else {
        installPath = $$[QT_INSTALL_IMPORTS]/$$replace(uri, \\., /)
    }
    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir
}


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../../../tree/consulting/OSS/poppler/poppler-0.24.1/lib/win32/release/ -lpoppler-qt5
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../../../tree/consulting/OSS/poppler/poppler-0.24.1/lib/win32/debug/ -lpoppler-qt5

INCLUDEPATH += $$PWD/../../../../../../../../../tree/consulting/OSS/poppler/poppler-0.24.1/include
DEPENDPATH += $$PWD/../../../../../../../../../tree/consulting/OSS/poppler/poppler-0.24.1/include
