#include <SMCElements/IProjectSettingsComponent.h>

#include <Elements/IRepresentation.h>
#include <DB/ObjectWrapper.h>
#include <DB/InputStream.h>
#include <DB/OutputStream.h>

#include <osg/Vec4d>

static bool writeProjectSettings( DB::OutputStream& os, 
						 const SMCElements::IProjectSettingsComponent &projectSettingsComponent)
{
    // MilitarySymbol representation mode
    ELEMENTS::IRepresentation::RepresentationMode mode = projectSettingsComponent.getMilitarySymbolRepresentationMode();

	os <<  DB::VIZ_BEGIN_BRACKET << std::endl;

    os.writeWrappedString("MilitarySymbolRepresentationMode");
    os << " ";
    os << int(mode);
    os << std::endl;

	os << DB::VIZ_END_BRACKET << std::endl;

	return true;
}

static bool readProjectSettings( DB::InputStream& is, 
						 SMCElements::IProjectSettingsComponent &projectSettingsComponent)
{
	unsigned int numEntriesMap = 0;
	is >> DB::VIZ_BEGIN_BRACKET;

    std::string settingName;
    is.readWrappedString(settingName);

    if(settingName == "MilitarySymbolRepresentationMode")
    {
        unsigned int representationMode;
        is >> representationMode;
        projectSettingsComponent.setMilitarySymbolRepresentationMode((ELEMENTS::IRepresentation::RepresentationMode)(representationMode));
    }

	is.advanceToCurrentEndBracket();
	return true;
}

VIZ_REGISTER_COMPONENT_WRAPPER(ProjectSettingsComponent,
							   SMCElements::IProjectSettingsComponent,
							   "ProjectSettingsComponent Component Base"
							  )
{
	ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(ProjectSettings);
}