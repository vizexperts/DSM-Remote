#include <SMCElements/IUserFolderItem.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>

static bool writeChildren(DB::OutputStream& os, const SMCElements::IUserFolderItem& folder)
{
    unsigned int size = 0;
    const SMCElements::IUserFolderItem::UserFolderCollection::UIDBaseMap& folderMap = folder.getChildrenList();

    size = folderMap.size();

    os << size << DB::VIZ_BEGIN_BRACKET << std::endl;

    SMCElements::IUserFolderItem::UserFolderCollection::UIDBaseMap::const_iterator iter = folderMap.begin();
    while(iter != folderMap.end())
    {
        os << iter->second->getInterface<CORE::IBase>();
        ++iter;
    }

    os << DB::VIZ_END_BRACKET << std::endl;
    return true;
}
static bool readChildren(DB::InputStream& is, SMCElements::IUserFolderItem& folder)
{
    unsigned int size = 0;
    is >> size >> DB::VIZ_BEGIN_BRACKET;

    for(unsigned int i = 0; i < size; ++i)
    {
        CORE::IBase* object = is.readBase();
        if(object && object->getInterface<CORE::IObject>())
        {
            folder.addChild(object->getInterface<CORE::IObject>());
        }
    }

    is.advanceToCurrentEndBracket();

    return true;
}

VIZ_REGISTER_OBJECT_WRAPPER(UserFolder, 
                            SMCElements::IUserFolderItem,
                            "UserFolder Visibility Base"
                            )
{
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(Children);
}