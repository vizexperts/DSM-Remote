#include <SMCElements/IPresentationComponent.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>

#include <osg/Vec4d>
#include <osgDB/FileNameUtils>

#include <Core/IWorldMaintainer.h>
#include <Core/WorldMaintainer.h>

#include <Util/FileUtils.h>

static bool writePresentationMap( DB::OutputStream& os, 
                         const SMCElements::IPresentationComponent &presentationComponent)
{
    //Serializing the map
    const SMCElements::IPresentationComponent::pdfMap &presentationMap
        = presentationComponent.getPdfMapToSave();

    // removing all the unnecessary files
    CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
    std::string strProjectName = worldMaintainer->getProjectFile();
    std::string strProjectPath = osgDB::getFilePath( strProjectName );
    if(strProjectPath.empty())
    {
        throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
            "Unable to locate project directory",
            __FILE__, __LINE__);
    }

    std::vector<std::string> files;
    std::vector<std::string> vstrExts;
    vstrExts.push_back("pdf");

    UTIL::getFilesFromFolder( strProjectPath, files, vstrExts );

    std::vector<std::string>::iterator directoryIterator = files.begin();
    SMCElements::IPresentationComponent::pdfMap::const_iterator pdfIterator;

    for( ; directoryIterator != files.end(); directoryIterator++ )
    {
        std::string fileName = UTIL::getFileName(*directoryIterator);

        for ( pdfIterator=presentationMap.begin(); pdfIterator!=presentationMap.end(); pdfIterator++ )
        {
            if ( (*pdfIterator).second.compare(fileName) == 0 )
            {
                break;
            }
        }

        if ( pdfIterator == presentationMap.end() )
        {
            UTIL::deleteFile( (*directoryIterator).c_str() );  
        }
    }

    unsigned int numOfValues = presentationMap.size();
    os <<  numOfValues <<DB::VIZ_BEGIN_BRACKET << std::endl;

    SMCElements::IPresentationComponent::pdfMap::const_iterator itr = presentationMap.begin();
    for(;itr != presentationMap.end(); ++itr)
    {
        os.writeWrappedString(itr->first);
        os << " ";
        os.writeWrappedString(itr->second);
        os << std::endl;
    }

    os << DB::VIZ_END_BRACKET << std::endl;

    return true;
}

static bool readPresentationMap( DB::InputStream& is, 
                         SMCElements::IPresentationComponent &presentationComponent)
{
    unsigned int numEntriesMap = 0;
    is >> numEntriesMap >> DB::VIZ_BEGIN_BRACKET;

    for(unsigned int currEntry = 0; currEntry < numEntriesMap; ++currEntry)
    {
        std::string presentationName;
        std::string fileName;


        is.readWrappedString(presentationName);
        is.readWrappedString(fileName);

        presentationComponent.addPdfDocument(presentationName, fileName);
    }

    is.advanceToCurrentEndBracket();
    return true;
}

VIZ_REGISTER_COMPONENT_WRAPPER(PresentationComponent,
                               SMCElements::IPresentationComponent,
                               "PresentationComponent Component Base"
                              )
{
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(PresentationMap);
}
