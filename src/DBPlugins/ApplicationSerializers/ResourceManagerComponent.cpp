#include <SMCElements/IResourceManagerComponent.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>

#include <osg/Vec4d>

static bool writeResourceMap( DB::OutputStream& os,
                             const SMCElements::IResourceManagerComponent &resourceComponent)
{
    //Serializing the map
    const SMCElements::IResourceManagerComponent::MapResourceToInstances &mapResource
        = resourceComponent.getResourceMap();

    unsigned int numOfValues = mapResource.size();
    os <<  numOfValues <<DB::VIZ_BEGIN_BRACKET << std::endl;

    SMCElements::IResourceManagerComponent::MapResourceToInstances::const_iterator itr
        = mapResource.begin();

    for(;itr!=mapResource.end(); ++itr)
    {
        os.writeWrappedString(itr->first);
        os << " " << itr->second << std::endl;
    }

    os << DB::VIZ_END_BRACKET << std::endl;

    return true;
}

static bool readResourceMap( DB::InputStream& is,
                            SMCElements::IResourceManagerComponent &resourceComponent)
{
    unsigned int numEntriesMap = 0;
    is >> numEntriesMap >> DB::VIZ_BEGIN_BRACKET;

    std::string resourceFullName("");
    unsigned int numInstances=0;

    for(unsigned int currEntry=0; currEntry<numEntriesMap; ++currEntry)
    {
        is.readWrappedString(resourceFullName);
        is >> numInstances;


        for(unsigned int currInstance=0; currInstance<numInstances; ++currInstance)
        {
            resourceComponent.addResource(resourceFullName);
        }

        resourceFullName.clear();
    }

    is.advanceToCurrentEndBracket();
    return true;
}

VIZ_REGISTER_COMPONENT_WRAPPER(ResourceManagerComponent,
                               SMCElements::IResourceManagerComponent,
                               "ResourceManagerComponent Component Base"
                               )
{
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(ResourceMap);
}
