#include <SMCElements/IUserFolderItem.h>

#include <Elements/ElementsUtils.h>
#include <Core/ICompositeObject.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>

static bool writeRefObjectID(DB::OutputStream& os, const SMCElements::IUserFolderObjectWrapper& objectWrapper)
{
    CORE::IObject* referencedObject = objectWrapper.getReferencedObject();
    if(referencedObject != NULL)
    {
        os << referencedObject->getInterface<CORE::IBase>()->getUniqueID().toString() << std::endl;
    }

    return true;
}
static bool readRefObjectID(DB::InputStream& is, SMCElements::IUserFolderObjectWrapper& objectWrapper)
{
    std::string uid;
    is >> uid;

    CORE::RefPtr<CORE::ICompositeObject> composite = ELEMENTS::GetFirstWorldFromMaintainer()->getInterface<CORE::ICompositeObject>();
    CORE::RefPtr<CORE::UniqueID> uniqueID = new CORE::UniqueID(uid);
    CORE::RefPtr<CORE::IObject> object = composite->getObjectByID(uniqueID.get());

    if(object.valid())
    {
        objectWrapper.setReferencedObject(object.get());
    }

    return true;
}

VIZ_REGISTER_OBJECT_WRAPPER(UserFolderObjectWrapper, 
                            SMCElements::IUserFolderObjectWrapper,
                            "UserFolderObjectWrapper Base"
                            )
{
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(RefObjectID);
}