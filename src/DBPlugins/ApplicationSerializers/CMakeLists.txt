SET(LIB_NAME SMCdb_application_serializer)

SET(HEADER_PATH ${DSM_SOURCE_DIR}/src/DBPlugins/ApplicationSerializers)
SET(SOURCE_PATH ${DSM_SOURCE_DIR}/src/DBPlugins/ApplicationSerializers)

file(GLOB LIB_PUBLIC_HEADERS_1
    )

file(GLOB LIB_SOURCES_1
        ${SOURCE_PATH}/TourComponent.cpp
        ${SOURCE_PATH}/PresentationComponent.cpp
        ${SOURCE_PATH}/ResourceManagerComponent.cpp
        ${SOURCE_PATH}/World.cpp
        ${SOURCE_PATH}/UserFolderComponent.cpp
        ${SOURCE_PATH}/UserFolder.cpp
        ${SOURCE_PATH}/UserFolderObjectWrapper.cpp
    )

# Defining all headers and sources
SET(LIB_PUBLIC_HEADERS
    ${LIB_PUBLIC_HEADERS_1}
    )

SET(LIB_SOURCES
    ${LIB_SOURCES_1}
    )

ADD_LIBRARY(${LIB_NAME} SHARED
    ${LIB_PUBLIC_HEADERS}
    ${LIB_SOURCES}
)

LINK_WITH_VARIABLES(${LIB_NAME}
                    OSG_LIBRARY
                    OSGDB_LIBRARY
                    
                    BOOST_SYSTEM_LIBRARY
                    BOOST_FILESYSTEM_LIBRARY
                    
                    TKDB_LIBRARY
                    TKSERIALIZATION_LIBRARY
                    TKAPP_LIBRARY
                    )


INCLUDE_DIRECTORIES(${DSM_SOURCE_DIR}/include
                    ${DSM_SOURCE_DIR}/src
                    ${GDAL_INCLUDE_DIR}
                    ${OSG_EARTH_INCLUDE_DIR}
                    ${OSG_INCLUDE_DIR}
                    ${SPATIALITE_INCLUDE_DIR}
                    ${BOOST_INCLUDE_DIR}
                    ${CURL_INCLUDE_DIR}
                    ${TERRAINSDK_INCLUDE_DIR}
                    )

TARGET_LINK_LIBRARIES(${LIB_NAME}
                       ${SMCELEMENTS_LIBRARY}
                      )

TARGET_LINK_LIBRARIES(${LIB_NAME}
                        ${SMCELEMENTS_LIBRARY}
                      )

#INCLUDE(ModuleInstall OPTIONAL)
