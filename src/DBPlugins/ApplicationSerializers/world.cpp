#include <Core/IWorld.h>
#include <Core/IComponent.h>

#include <Elements/ElementsUtils.h>

#include <Serialization/ObjectWrapper.h>
#include <Serialization/InputStream.h>
#include <Serialization/OutputStream.h>

#include <osg/Vec4d>

static bool writeDSMComponents( DB::OutputStream& os, 
                               const CORE::IWorld& world)
{

    int n=0;
    CORE::IComponent *tourComponent,*presentationComponent,*resourceManagerComponent, *userFolderComponent;

    tourComponent = ELEMENTS::GetComponentFromMaintainer("TourComponent");
    if(tourComponent) n++;

    presentationComponent = ELEMENTS::GetComponentFromMaintainer("PresentationComponent");
    if(presentationComponent) n++;

    resourceManagerComponent = ELEMENTS::GetComponentFromMaintainer("ResourceManagerComponent");
    if(resourceManagerComponent) n++;

    userFolderComponent = ELEMENTS::GetComponentFromMaintainer("UserFolderComponent");
    if(userFolderComponent) n++;

    //Writting the starting bracket with size(XXXX Components size is hardCoded)
    os << n << DB::VIZ_BEGIN_BRACKET << std::endl;

    if(tourComponent)
    {
        os << tourComponent->getInterface<CORE::IBase>();
    }
    if(presentationComponent)
    {
        os << presentationComponent->getInterface<CORE::IBase>();
    }
    if(resourceManagerComponent)
    {
        os << resourceManagerComponent->getInterface<CORE::IBase>();
    }
    if(userFolderComponent)
    {
        os << userFolderComponent->getInterface<CORE::IBase>();
    }

    os << DB::VIZ_END_BRACKET << std::endl;
    return true;
}

static bool readDSMComponents( DB::InputStream& is, 
                              CORE::IWorld& world)
{
    unsigned int size = 0; is >> size >> DB::VIZ_BEGIN_BRACKET;

    for ( unsigned int i=0; i<size; ++i )
    {
        CORE::IBase *base = is.readBase();
    }

    is >> DB::VIZ_END_BRACKET;

    return true;
}


//XXX Make sure you don't add serailizer with same name 
//as the wrapper you are updating
VIZ_REGISTER_UPDATE_WRAPPER(World,
                            CORE::IWorld)
{
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK_FRONT(DSMComponents);
}
