#include <SMCElements/ITourComponent.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>

#include <osg/Vec4d>
#include <osgDB/FileNameUtils>

#include <Core/IWorldMaintainer.h>
#include <Core/WorldMaintainer.h>

#include <Util/FileUtils.h>


static bool writeTourMap( DB::OutputStream& os,
                         const SMCElements::ITourComponent &tourComponent)
{
    //Serializing the map
    const SMCElements::ITourComponent::MapTourToFile &mapTour
        = tourComponent.getTourMap();

    // removing all the unnecessary files
    CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
    std::string strProjectName = worldMaintainer->getProjectFile();
    std::string strProjectPath = osgDB::getFilePath( strProjectName );
    if(strProjectPath.empty())
    {
        throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
            "Unable to locate project directory",
            __FILE__, __LINE__);
    }

    std::vector<std::string> files;
    std::vector<std::string> vstrExts;
    vstrExts.push_back("tour");

    UTIL::getFilesFromFolder( strProjectPath, files, vstrExts );

    std::vector<std::string>::iterator directoryIterator = files.begin();
    SMCElements::ITourComponent::MapTourToFile::const_iterator mapIterator;

    for( ; directoryIterator != files.end(); directoryIterator++ )
    {
        std::string fileName = UTIL::getFileName(*directoryIterator);

        for ( mapIterator=mapTour.begin(); mapIterator!=mapTour.end(); mapIterator++ )
        {
            if ( (*mapIterator).second.compare(fileName) == 0 )
            {
                break;
            }
        }

        if ( mapIterator == mapTour.end() )
        {
            UTIL::deleteFile( (*directoryIterator).c_str());
            UTIL::deleteFile( (*directoryIterator + "cp").c_str());
        }
    }

    unsigned int numOfValues = mapTour.size();
    os <<  numOfValues <<DB::VIZ_BEGIN_BRACKET << std::endl;

    SMCElements::ITourComponent::MapTourToFile::const_iterator itr
        = mapTour.begin();
    for(;itr!=mapTour.end(); ++itr)
    {
        os.writeWrappedString(itr->first);
        os << " ";
        os.writeWrappedString(itr->second);
        os << std::endl;
    }

    os << DB::VIZ_END_BRACKET << std::endl;

    return true;
}

static bool writeSequenceMap(DB::OutputStream& os, const SMCElements::ITourComponent & tourComponent)
{
    // Serializing the map
    const SMCElements::ITourComponent::MapSequenceToTourList & mapSequence = tourComponent.getSequenceList();

    unsigned int numOfSequences = mapSequence.size();
    os << numOfSequences << DB::VIZ_BEGIN_BRACKET << std::endl;

    SMCElements::ITourComponent::MapSequenceToTourList::const_iterator iter = mapSequence.begin();
    for(; iter != mapSequence.end(); ++iter)
    {
        os.writeWrappedString(iter->first);

        const std::vector<std::string>& sequence = iter->second;
        unsigned int numOfValues = sequence.size();

        os << " " << numOfValues << DB::VIZ_BEGIN_BRACKET << " ";
        for(unsigned int currEntry = 0; currEntry < numOfValues; currEntry++)
        {
            std::string str = sequence[currEntry];
            os.writeWrappedString(str);
        }

        os << DB::VIZ_END_BRACKET << std::endl;
    }

        os << DB::VIZ_END_BRACKET << std::endl;
    return true;
}

static bool readSequenceMap(DB::InputStream& is, SMCElements::ITourComponent &tourComponent)
{
    unsigned int numEntriesMap = 0;
    is >> numEntriesMap >> DB::VIZ_BEGIN_BRACKET;
    CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
    std::string strProjectName = worldMaintainer->getProjectFile();
    std::string strProjectPath = osgDB::getFilePath(strProjectName);
    if (strProjectPath.empty())
    {
        throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
            "Unable to locate project directory",
            __FILE__, __LINE__);
    }


    for(unsigned int currEntry = 0; currEntry < numEntriesMap; ++currEntry)
    {
        std::string sequenceName;
        std::vector<std::string> sequence;


        is.readWrappedString(sequenceName);
        unsigned int numOfTours;
        is >> numOfTours >> DB::VIZ_BEGIN_BRACKET;

        for(unsigned int i = 0; i < numOfTours; ++i)
        {
            std::string tourName;
            is.readWrappedString(tourName);
            sequence.push_back(tourName);
        }
#if 1
        //This code has to be removed after sometime
        if (!fs::exists(fs::path(strProjectPath) / "sequence.json"))
        {
            tourComponent.reloadOldSequence(sequenceName, sequence);
        }
#endif
        tourComponent.reloadSequence(sequenceName, sequence);
        is.advanceToCurrentEndBracket();
    }

    is.advanceToCurrentEndBracket();
    return true;
}

static bool readTourMap( DB::InputStream& is, 
                        SMCElements::ITourComponent &tourComponent)
{
    unsigned int numEntriesMap = 0;
    is >> numEntriesMap >> DB::VIZ_BEGIN_BRACKET;

    for(unsigned int currEntry=0; currEntry<numEntriesMap; ++currEntry)
    {
        std::string tourName;
        std::string fileName;

        is.readWrappedString(tourName);
        is.readWrappedString(fileName);

        tourComponent.addExternalTour(tourName,  
            fileName,
            true);
    }

    is.advanceToCurrentEndBracket();
    return true;
}

VIZ_REGISTER_COMPONENT_WRAPPER(TourComponent,
                               SMCElements::ITourComponent,
                               "TourComponent Component Base"
                               )
{
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(TourMap);
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(SequenceMap);
    ADD_VIZ_FLOAT_SERIALIZER(SpeedFactor, 1.0f);
}
