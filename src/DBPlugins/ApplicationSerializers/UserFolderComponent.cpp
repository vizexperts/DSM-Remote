#include <SMCElements/IUserFolderComponent.h>
#include <SMCElements/IUserFolderItem.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>
#include <serialization/StreamOperator.h>
#include <serialization/AsciiStreamOperator.h>

#include <Core/WorldMaintainer.h>

#include <osgDB/FileNameUtils>

static bool writeFolders(DB::OutputStream& os, const SMCElements::IUserFolderComponent& userFolderComponent)
{
    const SMCElements::IUserFolderItem* rootItem = userFolderComponent.getRootFolder();

    if(rootItem == NULL)
        return false;

    std::string filename = userFolderComponent.getFoldersFilename();

    os << DB::VIZ_BEGIN_BRACKET << " ";
    os << filename << DB::VIZ_END_BRACKET << std::endl;

    filename = osgDB::getFilePath(CORE::WorldMaintainer::instance()->getProjectFile()) + "/" + filename;

    std::ios::openmode mode = std::ios::out;
    std::ofstream fout(filename.c_str(), mode);

    if(!fout)
        return false;

    fout << std::string("#Ascii") << ' ';
    CORE::RefPtr<DB::OutputIterator> oi = new AsciiOutputIterator(&fout);

    DB::OutputStream osf(NULL);
    osf.start(oi.get(), DB::OutputStream::WRITE_EVENT);

    osf << rootItem->getInterface<CORE::IBase>();
    
    oi->flush();
}

static bool readFolders(DB::InputStream& is, SMCElements::IUserFolderComponent& userFolderComponent)
{
    std::string filename;
    is >> DB::VIZ_BEGIN_BRACKET >> filename >> DB::VIZ_END_BRACKET;

    return true;
}

VIZ_REGISTER_COMPONENT_WRAPPER(UserFolderComponent,
                               SMCElements::IUserFolderComponent,
                               "UserFolderComponent Component Base"
                               )
{
    ADD_VIZ_USER_SERIALIZER_WITHOUT_CHECK(Folders);
}
