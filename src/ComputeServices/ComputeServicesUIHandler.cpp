#include <ComputeServices/ComputeServicesUIHandler.h>
#include <ComputeServices/ComputeServicesPlugin.h>

#include <Core/InterfaceUtils.h>
#include <Core/WorldMaintainer.h>

namespace ComputeServices
{
    DEFINE_META_BASE(ComputeServices, ComputeServicesUIHandler);
    DEFINE_IREFERENCED(ComputeServicesUIHandler, VizUI::UIHandler);

    ComputeServicesUIHandler::ComputeServicesUIHandler()
    {
        _addInterface(ComputeServices::IComputeServicesUIHandler::getInterfaceName());
        setName(getClassname());

        _httpInteractor = new HTTPInteractor();
        _thriftInteractor = new ThriftInteractor();
    }

    ComputeServicesUIHandler::~ComputeServicesUIHandler()
    {
        delete _httpInteractor;
        delete _thriftInteractor;
    }

    void ComputeServicesUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizUI::UIHandler::onAddedToManager();
    }

    void ComputeServicesUIHandler::setFocus(bool value)
    {
        VizUI::UIHandler::setFocus(value);
    }

    void ComputeServicesUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();

            //! Do whatever,.
        }
        //else

        VizUI::UIHandler::update(messageType, message);
    }

    void ComputeServicesUIHandler::setConnection(const ComputeServices::IComputeServicesUIHandler::Connection& connection)
    {
        _currentConnection = connection;

        testConnection();
    }

    bool ComputeServicesUIHandler::testConnection()
    {
        return true;
    }

    void ComputeServicesUIHandler::listServices(std::string& response)
    {
        std::string serviceRequest = "{     \"request\" : \"list_services\"    }";

        switch(_currentConnection.connectionMode)
        {
        case IComputeServicesUIHandler::GEORBIS_SERVER :
            _httpInteractor->sendRequest(serviceRequest, response, _currentConnection);
            break;
        case IComputeServicesUIHandler::GEORBIS_ENGINE :
            _thriftInteractor->sendRequest(serviceRequest, response, _currentConnection);
        }
    }
}
