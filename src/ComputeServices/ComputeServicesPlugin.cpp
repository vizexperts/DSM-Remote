/*****************************************************************************
*
* File             : ComputeServicesPlugin.cpp
* Description      : ComputeServicesPlugin class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <ComputeServices/ComputeServicesPlugin.h>
#include <ComputeServices/export.h>

#include <Util/FileUtils.h>

#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>

#include <App/IUIHandlerType.h>
#include <App/ManagerType.h>
#include <App/ManagerUtils.h>
#include <App/UndoTransactionType.h>
#include <App/ApplicationRegistry.h>

#include <Core/IBaseVisitor.h>
#include <Core/MessageType.h>
#include <Core/CoreRegistry.h>
#include <Core/MessageUtils.h>
#include <Core/CoreMessage.h>
#include <Core/NamedAttributeMessage.h>

#include <ComputeServices/ComputeServicesUIHandler.h>

//----------------------------------------------------------------------------
// Register all interfaces
//----------------------------------------------------------------------------

DEFINE_META_INTERFACE(ComputeServices, IComputeServicesUIHandler);


REGISTER_APPREGISTRY_PLUGIN(COMPUTESERVICES_DLL_EXPORT, ComputeServices, ComputeServicesRegistryPlugin);
//REGISTER_PLUGIN(COMPUTESERVICES_DLL_EXPORT, ComputeServices, ComputeServicesCoreRegistryPlugin);

namespace ComputeServices
{
    const std::string ComputeServicesRegistryPlugin::LibraryName = UTIL::getPlatformLibraryName("ComputeServices");
    const std::string ComputeServicesRegistryPlugin::PluginName = "ComputeServices";

    const CORE::RefPtr<APP::IUIHandlerType>
        ComputeServicesRegistryPlugin::ComputeServicesUIHandlerType(new VizUI::UIHandlerType("ComputeServicesUIHandler", ComputeServicesRegistryPlugin::ComputeServicesRegistryPluginStartId + 104));

    ComputeServicesRegistryPlugin::~ComputeServicesRegistryPlugin(){}

    void ComputeServicesRegistryPlugin::load(const APP::ApplicationRegistry& ar)
    {
        registerUIHandlers(*(ar.getUIHandlerFactory()));
    }

    void ComputeServicesRegistryPlugin::unload(const APP::ApplicationRegistry& ar)
    {
        deregisterUIHandlers(*(ar.getUIHandlerFactory()));
    }

    void ComputeServicesRegistryPlugin::registerUIHandlers(APP::IUIHandlerFactory& uf)
    {
        uf.registerUIHandler(*ComputeServicesRegistryPlugin::ComputeServicesUIHandlerType, new CORE::Proxy<APP::IUIHandler, ComputeServices::ComputeServicesUIHandler>());
    }


    void ComputeServicesRegistryPlugin::deregisterUIHandlers(APP::IUIHandlerFactory& uf)
    {
        uf.deregisterUIHandler(*ComputeServicesRegistryPlugin::ComputeServicesUIHandlerType);
    }

    const std::string& ComputeServicesRegistryPlugin::getLibraryName() const
    {
        return ComputeServicesRegistryPlugin::LibraryName;
    }

    const std::string& ComputeServicesRegistryPlugin::getPluginName() const
    {
        return ComputeServicesRegistryPlugin::PluginName;
    }
}