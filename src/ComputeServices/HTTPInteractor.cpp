#include <ComputeServices/HTTPInteractor.h>

#include <curl/curl.h>

namespace ComputeServices
{

    void HTTPInteractor::sendRequest(const std::string& request, std::string& response, const IComputeServicesUIHandler::Connection& connection)
    {
        if (connection.connectionMode != IComputeServicesUIHandler::GEORBIS_SERVER)
            return;

        std::string url = connection.ip + ":" + UTIL::ToString(connection.port) + "/compute";

        LOG_DEBUG("Performing request to GeorbIS Server at " + url);
        LOG_DEBUG(request);

        //! initialize curl
        CURL* curl;
        CURLcode res;

        //! curl global init
        curl_global_init(CURL_GLOBAL_ALL);

        //! get a curl handle
        curl = curl_easy_init();
        if (curl)
        {
            //! set the url that is about to receive the POST
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            //! specify write handler
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
            //! specify write location
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
            //! specify content type header
            struct curl_slist *headers = curl_slist_append(NULL, "Content-Type:application/json");
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
            //! Now specify the POST data
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request.c_str());

            //! perform the request, 
            res = curl_easy_perform(curl);

            //! check for errors
            if (res != CURLE_OK)
            {
                LOG_ERROR("Curl POST failed");
            }

            //! cleanup
            curl_easy_cleanup(curl);
        }
        curl_global_cleanup();

    }

    size_t HTTPInteractor::write_to_string(void *ptr, size_t size, size_t count, void *stream) {
        ((std::string*)stream)->append((char*)ptr, 0, size*count);
        return size*count;
    }

}