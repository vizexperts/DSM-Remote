namespace cpp Georbis
namespace java com.vizexperts.georbis
namespace py georbis

/*
 * Exception thrown when there is an error internal to the Compute Engine
 */
exception InternalException{
    /*
     * Details about the exception that occured
     */
    1: required string details;
}

/*
 * Exception thrown when there is an invalid input to any of the services
 */
exception InvalidInputException{
    /*
     * Details about the exception that occured
     */
    1: required string details;
}

/*
 * Exception thrown when there is an unexpected error, internal to the engine
 */
exception RuntimeException{
    /*
     * Details about the exception that occured
     */
    1: required string details;
}

service Service {
   /*
    * Common service entry point for all Thrift based communication with GeorbisEngine
    * Accepts a single string input which must contain the service input data that conforms
    * to JSON service schema for that service. 
    * Returns the output as JSON as well
    */
   string execute(1: required string serviceInputJson)
                  throws
                  (1: InternalException internalException,
                   2: InvalidInputException invalidInputException,
                   3: RuntimeException runtimeException);
}