#include <SMCUI/MinMaxElevUIHandler.h>

#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IMinMaxElevCalcVisitor.h>
#include <GISCompute/IFilterStatusMessage.h>

#include <Core/ITerrain.h>
#include <Core/IPoint.h>
#include <Core/IPenStyle.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IRepresentation.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/ICompositeObject.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataField.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IMetadataRecord.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataRecordHolder.h>

#include <Core/InterfaceUtils.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <Core/IText.h>

#include <VizUI/VizUIPlugin.h>
#include <Core/IFeatureLayer.h>
#include <sstream>


namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI,MinMaxElevUIHandler);

    DEFINE_IREFERENCED(MinMaxElevUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    void MinMaxElevUpdateCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
    {
        traverse(node, nv);

        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(mutex);
        if(object)
        {
            _world->addObject(object.get());
            object = NULL;
        }
    }

    MinMaxElevUIHandler::MinMaxElevUIHandler()
                            : _minMaxCallback(NULL)
    {
        _addInterface(IMinMaxElevUIHandler::getInterfaceName());
        setName(getClassname());

        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _extents = new osg::Vec2Array;

        _reset();
    }

    MinMaxElevUIHandler::~MinMaxElevUIHandler(){}

    void 
    MinMaxElevUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void 
    MinMaxElevUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void 
    MinMaxElevUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if(value && world)
            {
                ////
                
            }
            else
            {
                if(_minMaxCallback.valid() && world)
                {
                    _minMaxCallback = NULL;
                }
            }

            UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool
    MinMaxElevUIHandler::getFocus() const
    {
        return false;
    }

    void 
    MinMaxElevUIHandler::computeMinMaxElevation(bool multiThreaded)
    {
        if(_extents->size() < 2)
        {
            return;
        }

        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(vfactory)
        {
            CORE::IWorld* world = getWorldInstance();
            if(world)
            {
                _minmaxVisitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::MinMaxElevCalcVisitorType);

                _setArea();

                _minmaxVisitor->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>()->setMultiThreaded(multiThreaded);

                CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                CORE::IBase* terrainBase = terrain->getInterface<CORE::IBase>();
                _subscribe(_minmaxVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                terrainBase->accept(*_minmaxVisitor.get());
            }
        }
    }

    void
    MinMaxElevUIHandler::_setArea()
    {
        GISCOMPUTE::IMinMaxElevCalcVisitor* iVisitor  = 
            _minmaxVisitor->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>();

        iVisitor->setExtents(*_extents);
    }

    void 
    MinMaxElevUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check for message type
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                            message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_PENDING)
                {
                }
                else
                {
                    if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                    {

                        CORE::RefPtr<CORE::IBase> base = message.getSender();
                        CORE::RefPtr<GISCOMPUTE::IMinMaxElevCalcVisitor> minmaxVisitor = base->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>();
                       
                        if(minmaxVisitor.valid())
                        {
                            // to be safe side, remove already existing feature layer, for every successfull case, 
                            // new feature layer must be created
                            _featureLayer = NULL;
                            _addMinMaxPoints(minmaxVisitor->getMinElevOutput(), minmaxVisitor->getMaxElevOutput());
                            _minPoint = minmaxVisitor->getMinElevOutput()->getValue();
                            _maxPoint = minmaxVisitor->getMaxElevOutput()->getValue();

                            if(_featureLayer.valid())
                            {
                                // add the layer to the world
                               
                                getWorldInstance()->addObject(_featureLayer.get());

                                _featureLayer = NULL;
                            }
                            else
                            {
                                throw UTIL::Exception("Min Max elevation output not created", __FILE__, __LINE__);
                            }
                        }
                    }
                }
            }
             catch(...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage
                (*GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            msg->getInterface<GISCOMPUTE::IFilterStatusMessage>()->setStatus(GISCOMPUTE::FILTER_SUCCESS);
            notifyObservers(*GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType, *msg);

            // notify GUI
            //notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }

    }

    void 
    MinMaxElevUIHandler::_addMinMaxPoints(CORE::IPoint* min, CORE::IPoint* max)
    {
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
                CORE::CoreRegistry::instance()->getObjectFactory();

        if(!_featureLayer.valid())
        {
            if(!objectFactory.valid())
            {
                LOG_ERROR("Invalid Object Factory");
                return;
            }

            _featureLayer = 
                objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);

            _createLayer();
        }
        
        CORE::RefPtr<CORE::ICompositeObject> composite = 
            _featureLayer->getInterface<CORE::ICompositeObject>();

        if(!composite.valid())
        {
            LOG_ERROR("IComposite interface not found in FeatureLayer");
            return;
        }

        // create tow feature objects
        CORE::RefPtr<CORE::IObject> minObject = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

        CORE::RefPtr<CORE::IObject> maxObject = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

        if(!minObject.valid() && !maxObject.valid())
        {
            LOG_ERROR("Failed to create MinPoint and MaxPoint from object factory");
            return;
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }


        CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
            _featureLayer->getInterface<CORE::IMetadataTableHolder>();

        if(!holder.valid())
        {
            LOG_ERROR("IMetadataHolder interface not found in Feature Layer");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = holder->getMetadataTable();

        if(!metadataTable.valid())
        {
            LOG_ERROR("Invalid IMetadataTable instance");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataTable->getMetadataTableDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Invalid IMetadataTableDefn instance");
            return;
        }

        //XXX - hard coded icon 
        CORE::RefPtr<CORE::IComponent> iconLoadercomponent = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        if(iconLoadercomponent.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
                component->getInterface<ELEMENTS::IIconLoader>();

            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = 
                minObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolder.valid() && iconLoader.valid())
            {
                iconHolder->setIcon(iconLoader->getIconByName("circle"));
            }

            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolderMax = 
                maxObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolderMax.valid() && iconLoader.valid())
            {
                iconHolderMax->setIcon(iconLoader->getIconByName("circle"));
            }
        }

        //set the representation to icon mode
        CORE::RefPtr<ELEMENTS::IRepresentation> representation = 
            minObject->getInterface<ELEMENTS::IRepresentation>();

        if(representation.valid())
        {
            representation->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
        }

        CORE::RefPtr<ELEMENTS::IRepresentation> representationMax = 
            maxObject->getInterface<ELEMENTS::IRepresentation>();

        if(representationMax.valid())
        {
            representationMax->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
        }

        //double offset = 10.0f;

        CORE::RefPtr<CORE::IPoint> minPoint = minObject->getInterface<CORE::IPoint>();
        CORE::RefPtr<CORE::IPoint> maxPoint = maxObject->getInterface<CORE::IPoint>();

        CORE::RefPtr<CORE::IPenStyle> minStyle = minObject->getInterface<CORE::IPenStyle>();
        CORE::RefPtr<CORE::IPenStyle> maxStyle = maxObject->getInterface<CORE::IPenStyle>();

        CORE::RefPtr<CORE::IText> minText = minObject->getInterface<CORE::IText>();
        CORE::RefPtr<CORE::IText> maxText = maxObject->getInterface<CORE::IText>();

        if(minText.valid() && maxText.valid())
        {
            double x = 0.0, y = 0.0, z = 0.0;
            {
                std::stringstream ssMin;
                x = min->getX();
                y = min->getY();
                z = min->getZ();
                if( y < 0.0)
                {
                    ssMin << "Min: " << std::abs(y) << "S, ";
                }
                else
                {
                    ssMin << "Min: " << y << "N, ";
                }
                if( x < 0.0)
                {
                    ssMin << std::abs(x) << "W, " << z;
                }
                else
                {
                    ssMin << x << "E, " << z;
                }
                minText->setText(ssMin.str());
                minText->setTextActive(true);
            }

            {
                std::stringstream ssMax;
                x = max->getX();
                y = max->getY();
                z = max->getZ();
                if( y < 0.0)
                {
                    ssMax << "Max: " << std::abs(y) << "S, ";
                }
                else
                {
                    ssMax << "Max: " << y << "N, ";
                }
                if( x < 0.0)
                {
                    ssMax << std::abs(x) << "W, " << z;
                }
                else
                {
                    ssMax << x << "E, " << z;
                }
                maxText->setText(ssMax.str());
                maxText->setTextActive(true);
            }
        }

        
        //maxText->setFont(std::string("College Halo.ttf"));

        if(maxStyle.valid())
        {
            maxStyle->setPenWidth(3.0);
            maxStyle->setPenColor(osg::Vec4(1.0, 0.0, 0.0, 1.0));
        }

        if(minStyle.valid())
        {
            minStyle->setPenWidth(3.0);
            minStyle->setPenColor(osg::Vec4(0.0, 0.0, 1.0, 1.0));
        }

        // set the coordinate for the min and max feature objects
        minPoint->setX(min->getX());
        minPoint->setY(min->getY());
        //minPoint->setZ(min->getZ() + offset);
        minPoint->setZ(min->getZ());
        maxPoint->setX(max->getX());
        maxPoint->setY(max->getY());
        //maxPoint->setZ(max->getZ() + offset);
        maxPoint->setZ(max->getZ());

        //create metadata record for the min max point
        CORE::RefPtr<CORE::IMetadataRecord> minRecord = 
            metadataCreator->createMetadataRecord();
        CORE::RefPtr<CORE::IMetadataRecord> maxRecord = 
            metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        minRecord->setTableDefn(tableDefn.get());
        maxRecord->setTableDefn(tableDefn.get());

        // populate the fields of the records
        CORE::RefPtr<CORE::IMetadataField> minNamefield;
        minNamefield = minRecord->getFieldByName("Name");
        minNamefield->fromString("Minimum Elevation Point");

        CORE::RefPtr<CORE::IMetadataField> maxNamefield;
        maxNamefield = maxRecord->getFieldByName("Name");
        maxNamefield->fromString("Maximum Elevation Point");

        CORE::RefPtr<CORE::IMetadataField> minLatitudefield;
        minLatitudefield = minRecord->getFieldByName("Latitude");
        minLatitudefield->fromDouble(minPoint->getY());

        CORE::RefPtr<CORE::IMetadataField> maxLatitudefield;
        maxLatitudefield = maxRecord->getFieldByName("Latitude");
        maxLatitudefield->fromDouble(maxPoint->getY());

        CORE::RefPtr<CORE::IMetadataField> minLongitudefield;
        minLongitudefield = minRecord->getFieldByName("Longitude");
        minLongitudefield->fromDouble(minPoint->getX());

        CORE::RefPtr<CORE::IMetadataField> maxLongitudefield;
        maxLatitudefield = maxRecord->getFieldByName("Longitude");
        maxLatitudefield->fromDouble(maxPoint->getX());

        CORE::RefPtr<CORE::IMetadataField> minHeightfield;
        minHeightfield = minRecord->getFieldByName("Height");
        minHeightfield->fromDouble(minPoint->getZ());

        CORE::RefPtr<CORE::IMetadataField> maxHeightfield;
        maxHeightfield = maxRecord->getFieldByName("Height");
        maxHeightfield->fromDouble(maxPoint->getZ());

        CORE::RefPtr<CORE::IMetadataRecordHolder> minRecordHolder = 
            minObject->getInterface<CORE::IMetadataRecordHolder>();

        CORE::RefPtr<CORE::IMetadataRecordHolder> maxRecordHolder = 
            maxObject->getInterface<CORE::IMetadataRecordHolder>();

        if(!minRecordHolder.valid() && maxRecordHolder.valid())
        {
            LOG_ERROR("IMetadataRecordHolder interface not found in FeatureObject");
            return;
        }

        //set metadata record to the point feature objects
        minRecordHolder->setMetadataRecord(minRecord.get());
        maxRecordHolder->setMetadataRecord(maxRecord.get());

        //add the min and max point feature to the feature layer
        composite->addObject(minObject.get());
        composite->addObject(maxObject.get());
    }

    void 
    MinMaxElevUIHandler::_removeMinMaxPoints()
    {
    }

    void
    MinMaxElevUIHandler::_resetMinMaxPoints()
    {
        
    }

    void
    MinMaxElevUIHandler::_createLayer()
    {
        CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
                _featureLayer->getInterface<CORE::IMetadataTableHolder>();

        if(!tableHolder.valid())
        {
            LOG_ERROR("IMetadataTableHolder interface not found in FeatureLayer");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = 
            tableHolder->getMetadataTable();

        if(!metadataTable.valid())
        {
            LOG_ERROR("Invalid IMetadataTable instance");
            return;
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        // create a new table definition
        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataCreator->createMetadataTableDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Unable to create a IMetadataTableDefn instance");
            return;
        }

        // create fields to store name, latitude, longitude and height
        CORE::RefPtr<CORE::IMetadataFieldDefn> nameField = 
            metadataCreator->createMetadataFieldDefn();
        CORE::RefPtr<CORE::IMetadataFieldDefn> latitudeField = 
            metadataCreator->createMetadataFieldDefn();
        CORE::RefPtr<CORE::IMetadataFieldDefn> longitudeField = 
            metadataCreator->createMetadataFieldDefn();
        CORE::RefPtr<CORE::IMetadataFieldDefn> heightField= 
            metadataCreator->createMetadataFieldDefn();

        nameField->setName("Name");
        nameField->setType(CORE::IMetadataFieldDefn::STRING);
        nameField->setLength(32);

        latitudeField->setName("Latitude");
        latitudeField->setType(CORE::IMetadataFieldDefn::DOUBLE);
        latitudeField->setPrecision(8);

        longitudeField->setName("Longitude");
        longitudeField->setType(CORE::IMetadataFieldDefn::DOUBLE);
        longitudeField->setPrecision(8);

        heightField->setName("Height");
        heightField->setType(CORE::IMetadataFieldDefn::DOUBLE);
        heightField->setPrecision(8);

        // add the fields to the table definition
        tableDefn->addFieldDefn(nameField.get());
        tableDefn->addFieldDefn(latitudeField.get());
        tableDefn->addFieldDefn(longitudeField.get());
        tableDefn->addFieldDefn(heightField.get());

        CORE::RefPtr<CORE::IBase> base = _featureLayer->getInterface<CORE::IBase>();
        if(base.valid())
        {
            base->setName("MinMaxElevationPoints");
        }

        CORE::RefPtr<CORE::IFeatureLayer> layer = 
            _featureLayer->getInterface<CORE::IFeatureLayer>();

        if(layer.valid())
        {
            layer->setFeatureLayerType(CORE::IFeatureLayer::POINT);
        }

        // set the field definition to the MetadataTable
        metadataTable->setMetadataTableDefn(tableDefn.get());
    }



    void
    MinMaxElevUIHandler::_reset()
    {
        _extents->clear();
        _minPoint = osg::Vec3d(0, 0, 0);//(75.54874f, 33.146325f);
        _maxPoint = osg::Vec3d(0, 0, 0);//(75.6490056f, 33.24655278f);

    }

    void
    MinMaxElevUIHandler::setExtents(osg::Vec4d extents)
    {
        _extents->clear();
        _extents->push_back(osg::Vec2d(extents.x(), extents.y()));
        _extents->push_back(osg::Vec2d(extents.z(), extents.w()));
    }

    osg::Vec4d 
    MinMaxElevUIHandler::getExtents() const
    {
        osg::Vec4d extents(0.0, 0.0, 0.0, 0.0);
        osg::Vec2d first = _extents->at(0);
        osg::Vec2d second = _extents->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    osg::Vec3d 
    MinMaxElevUIHandler::getMinimumElevationPoint() const
    {
        return _minPoint;
    }

    osg::Vec3d 
    MinMaxElevUIHandler::getMaximumElevationPoint() const
    {
        return _maxPoint;
    }

    void
    MinMaxElevUIHandler::stopFilter()
    {
        if(_minmaxVisitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
            _minmaxVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }

} // namespace SMCUI

