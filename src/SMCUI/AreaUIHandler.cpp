
#include <SMCUI/AreaUIHandler.h>

#include <Core/Point.h>
#include <Core/Line.h>
#include <Core/CoreRegistry.h>
#include <Core/IBrushStyle.h>
#include <Core/IPenStyle.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IFeatureObject.h>
#include <Core/IFeatureLayer.h>
#include <Core/ICompositeObject.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataRecord.h>
#include <Core/IGeometry.h>
#include <Core/ITemporary.h>
#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITerrain.h>
#include <Core/IDeletable.h>

#include <Transaction/ICompositeAddObjectTransaction.h>
#include <Transaction/IPolygonSetExteriorRingTransaction.h>
#include <Transaction/IMultiPointEditPointTransaction.h>
#include <Transaction/IMultiPointInsertPointTransaction.h>
#include <Transaction/IMultiPointRemovePointTransaction.h>
#include <Transaction/IMultiPointAddPointTransaction.h>
#include <Transaction/ILineAddPointTransaction.h>
#include <Transaction/ILineInsertPointTransaction.h>
#include <Transaction/ILineRemovePointTransaction.h>
#include <Transaction/ILineEditPointTransaction.h>
#include <Transaction/IDeleteObjectTransaction.h>
#include <Transaction/TransactionPlugin.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IMouse.h>

#include <Util/CoordinateConversionUtils.h>

#include <Elements/MultiPoint.h>
#include <Elements/IDrawTechnique.h>
#include <Elements/IPolygonDrawTechnique.h>
#include <Elements/IClamper.h>
#include <Elements/ILineDrawTechnique.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/ElementsUtils.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <App/IUndoTransactionFactory.h>
#include <App/IUndoTransaction.h>
#include <App/ApplicationRegistry.h>

#include <Util/DistanceBearingCalculationUtils.h>
#include <SMCUI/SMCUIPlugin.h>

#include <SMCElements/ILayerComponent.h>
#include <Core/IProperty.h>

#include <osgEarth/GeoMath>
#include <Util/GdalUtils.h>
#include <Util/CalculateExtentUtils.h>
#include <Util/StyleFileUtil.h>
#include <Elements/FeatureObject.h>
#include <serialization/StyleSheetParser.h>
namespace SMCUI
{
    void PolygonUpdateCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(mutex);

        if (dirty)
        {
            area->setExteriorRing(line.get());
            dirty = false;
        }

        traverse(node, nv);
    }

    DEFINE_META_BASE(SMCUI, AreaUIHandler);

    DEFINE_IREFERENCED(AreaUIHandler, VizUI::UIHandler);

    AreaUIHandler::AreaUIHandler()
        :_offset(50.0f)
        , _visitor(NULL)
        , _temporary(false)
        , _currentSelectedPoint(-1)
        , _dragged(false)
        , _processMouseEvents(false)
        , _handleButtonRelease(false)
    {
        _addInterface(SMCUI::IAreaUIHandler::getInterfaceName());
        setName(getClassname());

        _updateCallback = new PolygonUpdateCallback;
        _updateCallback->dirty = false;
        _selectedPointList.clear();
    }

    void AreaUIHandler::_resetMultiPoints()
    {
        if (!(_area.valid() && _line.valid()))
        {
            return;
        }
        if (!_multiPoint.valid())
        {
            _multiPoint = new ELEMENTS::MultiPoint;
        }
        if (_multiPoint.valid())
        {
            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
            _multiPoint->clearPoints();
            _multiPoint->showCntrlPoints(true);
            _currentSelectedPoint = -1;
            _selectedPointList.clear();

            CORE::RefPtr<ELEMENTS::IDrawTechnique> drawTechnique =
                _line->getInterface<ELEMENTS::IDrawTechnique>();

            double offset = 0.0;
            if (drawTechnique.valid())
            {
                offset = drawTechnique->getHeightOffset();
            }

            ELEMENTS::IPolygonDrawTechnique* pdt =
                _area->getInterface<ELEMENTS::IPolygonDrawTechnique>();

            if (pdt)
            {
                for (unsigned int i = 0; i < _line->getPointNumber(); i++)
                {
                    osg::Vec3d position = pdt->getExteriorRingECEFValue(i);
                    _multiPoint->add(position);
                }
            }
            else
            {
                for (unsigned int i = 0; i < _line->getPointNumber(); i++)
                {
                    CORE::RefPtr<CORE::IPoint> point = _line->getPoint(i);
                    osg::Vec3d posLongLatAlt = point->getValue();
                    osg::Vec3d position =
                        UTIL::CoordinateConversionUtils::GeodeticToECEF
                        (osg::Vec3d(posLongLatAlt.y(), posLongLatAlt.x(), posLongLatAlt.z() + offset));
                    _multiPoint->add(position);
                }
            }

            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->addChild(_multiPoint->getOSGGroupNode());
        }
    }
    bool AreaUIHandler::setMode(AreaMode mode)
    {

        if (_mode == mode)
        {
            return false;
        }

        bool cleanup = false;
        if (_mode == AREA_MODE_CREATE_RECTANGLE || _mode == AREA_MODE_FIXED_RECTANGLE)
            cleanup = true;

        _mode = mode;



        // Subscribe for TerrainPick messages
        _processMouseEvents = true;

        switch (_mode)
        {
        case AREA_MODE_NO_OPERATION:
        {
            setSelectedAreaAsCurrentArea();
            if (!_area.valid())
            {
                return false;
            }
            if (!_line.valid())
            {
                return false;
            }
            if (_multiPoint.valid())
            {
                _multiPoint->showCntrlPoints(false);
            }

            // Query TerrainUIHandler interface
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
            if (tph.valid())
            {
                CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                if (observable)
                {
                    observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

                    observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                    observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                    observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                }
            }
        }
        break;
        case AREA_MODE_CREATE_AREA:
        {
            _createLine();
            _createPolygon();
        }
        case AREA_MODE_EDIT:
        {
            // Subscribe to TerrainUIHandler
            try
            {
                if (_mode != AREA_MODE_CREATE_AREA)
                    setSelectedAreaAsCurrentArea();

                if (!_area.valid())
                {
                    _createPolygon();
                }
                if (!_line.valid())
                {
                    _createLine();
                }


                _resetMultiPoints();

                CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());

                if (tph.valid())
                {
                    CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                    if (observable)
                    {
                        observable->registerObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                        observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

                        observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                        observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                    }
                }

                CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

                selectionUIHandler->setMouseClickSelectionState(false);
            }
            catch (...)
            {
                // XXX - Throw a fatal error
            }
        }
        break;
        case AREA_MODE_FIXED_RECTANGLE:
        case AREA_MODE_CREATE_RECTANGLE:
        {
            if (!_area.valid())
            {
                _createPolygon();
            }
            if (!_line.valid())
            {
                _createLine();
            }

            _firstVertexSet = false;

            _resetMultiPoints();

            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());

            if (tph.valid())
            {
                CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                if (observable)
                {
                    observable->registerObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                    observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                    observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

                    observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                }
            }

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if (selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(false);
            }

        }
        break;

        case AREA_MODE_NONE:
        {
            _handleButtonRelease = false;
            _selectedPointList.clear();
            // Unsubscribe from TerrainUIHandler
            try
            {
                // Query TerrainUIHandler interface
                CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
                if (tph.valid())
                {
                    CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                    if (observable)
                    {
                        observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                        observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                        observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                        observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
                    }
                }

                if (cleanup)
                {
                    removeCreatedArea();
                }

                _line = NULL;
                _area = NULL;

                if (_multiPoint.valid())
                {
                    getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
                    _multiPoint->clearPoints();
                    _multiPoint->showCntrlPoints(false);
                    _currentSelectedPoint = -1;
                }

                CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

                if (selectionUIHandler.valid())
                {
                    selectionUIHandler->clearCurrentSelection();
                    selectionUIHandler->setMouseClickSelectionState(true);
                }
            }
            catch (...)
            {
                // XXX - Throw a fatal error
            }
        }
        break;
        }

        return true;
    }

    void AreaUIHandler::addToLayer(CORE::IFeatureLayer *featureLayer)
    {
        _addToLayer(featureLayer);
    }

    void AreaUIHandler::_addToLayer(CORE::IFeatureLayer *featureLayer)
    {
        if (!featureLayer)
            return;

        if (featureLayer->getFeatureLayerType() != CORE::IFeatureLayer::POLYGON)
            return;

        if (!_area.valid())
            return;

        try
        {
            CORE::RefPtr<CORE::ICompositeObject> composite =
                featureLayer->getInterface<CORE::ICompositeObject>(true);

            CORE::RefPtr<CORE::IBase> polygonObject = _area->getInterface<CORE::IBase>(true);
            getWorldInstance()->removeObjectByID(&polygonObject->getUniqueID());

            CORE::RefPtr<CORE::ITemporary> temp = _area->getInterface<CORE::ITemporary>();
            if (temp.valid())
            {
                temp->setTemporary(false);
            }

            // create a transaction to add the point to the layer
            APP::IUndoTransactionFactory* transactionFactory =
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction =
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::CompositeAddObjectTransactionType);

            CORE::RefPtr<TRANSACTION::ICompositeAddObjectTransaction> compositeAddObjectTransaction =
                transaction->getInterface<TRANSACTION::ICompositeAddObjectTransaction>();

            compositeAddObjectTransaction->setCompositeObject(composite.get());
            compositeAddObjectTransaction->setObject(_area->getInterface<CORE::IObject>());

            jsonFile = UTIL::StyleFileUtil::getObjectFolder() + "/" + _area->getInterface<CORE::IBase>()->getUniqueID().toString() + ".json";
            if (!osgDB::fileExists(jsonFile))
            {
                osgDB::copyFile(UTIL::StyleFileUtil::getDefaultPolygonStyle(), jsonFile);
            }
            CORE::RefPtr<SYMBOLOGY::IStyle> styleFile = DB::StyleSheetParser::readStyle(jsonFile);
            ELEMENTS::FeatureObject* featureNodeHolder = dynamic_cast<ELEMENTS::FeatureObject*>(_area->getInterface<CORE::IObject>());
            if (featureNodeHolder != nullptr)
            {
                featureNodeHolder->setVizPlaceNodeStyle(styleFile);
                featureNodeHolder->setBillboard(true);
            }

            // execute the transaction 
            addAndExecuteUndoTransaction(transaction.get());
        }
        catch (UTIL::Exception &e)
        {
            e.LogException();
        }
    }

    AreaUIHandler::AreaMode AreaUIHandler::getMode() const
    {
        return _mode;
    }

    void AreaUIHandler::setExtents(osg::Vec4d extents)
    {
        _fixedRectExtents = extents;
        _handleRectangleMode(_fixedRectExtents);
    }

    osg::Vec4d AreaUIHandler::getExtentsUTM()
    {
        return _fixedRectExtentsUTM;
    }

    osg::Vec4d AreaUIHandler::getExtents()
    {
        return _fixedRectExtents;
    }
    
    osg::Vec4d AreaUIHandler::_calculateExtents(osg::Vec3d center)
    {
        const osgEarth::SpatialReference* utm = osgEarth::SpatialReference::get(UTIL::GDALUtils::UTM_ZONE43_SREF);
        const osgEarth::SpatialReference* wgs84 = osgEarth::SpatialReference::get(UTIL::GDALUtils::WGS84_SREF);

        osgEarth::GeoPoint centerPt(wgs84, center.x(), center.y());
        osgEarth::GeoPoint centerPtUTM = centerPt.transform(utm);
        //osgEarth::GeoPoint centerwg = centerPtUTM.transform(wgs84);
        
        osg::Vec4d extentsUTM;
        extentsUTM.x() = centerPtUTM.x() - std::abs(_fixedRectangle.x()) / 2 - 0.5;  //upper left x
        extentsUTM.y() = centerPtUTM.y() + std::abs(_fixedRectangle.y()) / 2 + 0.5; //upper left y
        extentsUTM.z() = centerPtUTM.x() + std::abs(_fixedRectangle.x()) / 2 + 0.5;  //lower right x
        extentsUTM.w() = centerPtUTM.y() - std::abs(_fixedRectangle.y()) / 2 - 0.5; //lower right y
        
        _fixedRectExtentsUTM = extentsUTM;
        osg::Vec4d extents = UTIL::CalculateExtentUtils::transform(extentsUTM, utm, wgs84);
        //osg::Vec4d extents1 = UTIL::CalculateExtentUtils::transform(extents, wgs84, utm);
        return extents;
    }

    void AreaUIHandler::setFixedRectangleWidthHeight(osg::Vec2d widthHeihgt)
    {
        _fixedRectangle = widthHeihgt;
    }
    osg::Vec2d AreaUIHandler::getFixedRectangleWidthHeight()
    {
        return _fixedRectangle;
    }

    void AreaUIHandler::setColor(osg::Vec4 color)
    {
        //XXX - TBD
    }

    osg::Vec4 AreaUIHandler::getColor() const
    {
        //XXX - TBD, hardcoded for the moment for compilation purpose
        return osg::Vec4(0.0f, 0.0f, 0.0f, 0.0f);
    }

    std::vector<int>& AreaUIHandler::getSelectedVertexList()
    {
        return _selectedPointList;
    }

    void AreaUIHandler::setSelectedVertexList(std::vector<int> selectedPointList)
    {
        _selectedPointList.clear();
        _selectedPointList = selectedPointList;
    }

    void AreaUIHandler::_updateArea()
    {
        osg::ref_ptr<osg::Node> areaNode = _area->getInterface<CORE::IObject>()->getOSGNode();
        if (areaNode.valid())
        {
            areaNode->setUpdateCallback(_updateCallback.get());
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_updateCallback->mutex);
            _updateCallback->dirty = true;
            _updateCallback->area = _area;
            _updateCallback->line = _line;
        }
    }

    void AreaUIHandler::_handleMouseReleased(int button)
    {
        if (button & VizUI::IMouse::RIGHT_BUTTON)
        {
            switch (_mode)
            {

            case AREA_MODE_CREATE_AREA:
            {
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaCreatedMessageType);
                notifyObservers(*IAreaUIHandler::AreaCreatedMessageType, *msg);

                CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());
                if (selectionUIHandler.valid())
                {
                    selectionUIHandler->clearCurrentSelection();
                }
            }
            break;
            case AREA_MODE_EDIT:
            case AREA_MODE_NO_OPERATION:
            {
                if (_area->getInterface<CORE::ISelectable>()->getSelected())
                {
                    _area->getInterface<CORE::ISelectable>()->setSelected(false);
                }

                reset();
            }

            }
            return;
        }

        else if (button & VizUI::IMouse::LEFT_BUTTON)
        {
            if (_mode == AREA_MODE_NO_OPERATION)
            {
                if (_area->getInterface<CORE::ISelectable>()->getSelected())
                {
                    _area->getInterface<CORE::ISelectable>()->setSelected(false);
                }

                reset();
            }
            if (_currentSelectedPoint > -1)
            {
                std::vector<int>::iterator pos = find(_selectedPointList.begin(),
                    _selectedPointList.end(), _currentSelectedPoint);

                // toggling the selections
                if (pos == _selectedPointList.end())
                {
                    _selectedPointList.push_back(_currentSelectedPoint);
                    _multiPoint->setHighlightFlagAtPoint(_currentSelectedPoint, true);
                }
                else
                {
                    _selectedPointList.erase(pos);
                    _multiPoint->setHighlightFlagAtPoint(_currentSelectedPoint, false);
                }

                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
                notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
            }
        }
    }

    void AreaUIHandler::_handleMouseClickedIntersection(int button, const osg::Vec3d& longLatAlt)
    {
        if (!_processMouseEvents)
        {
            return;
        }

        if (!(button & VizUI::IMouse::LEFT_BUTTON) || (AREA_MODE_NONE == _mode))
        {
            return;
        }

        switch (_mode)
        {
        case AREA_MODE_CREATE_AREA:
        {
            if (!_line.valid())
            {
                // first point is to be added 
                _createLine();
            }

            if (!_area.valid())
            {
                _createPolygon();
                _resetMultiPoints();
            }

            double offset = 0.0f;
            CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
                _area->getInterface<ELEMENTS::IDrawTechnique>();
            if (draw.valid())
            {
                offset = draw->getHeightOffset();
            }
            osg::Vec3d offsetPoint = UTIL::CoordinateConversionUtils::
                GeodeticToECEF(osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z() + offset));

            _currentSelectedPoint = _multiPoint->findNearestCntrlPoint(offsetPoint);

            if (_currentSelectedPoint == -1)
            {
                CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
                if (tph.valid())
                {
                    CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                    if (observable)
                    {
                        observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                    }
                }

                _addPoint(longLatAlt);

            }
            else
            {
                CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
                if (tph.valid())
                {
                    CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                    if (observable)
                    {
                        observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                    }
                }
            }
        }
        break;
        case AREA_MODE_EDIT:
        {
            if (_area.valid() && _line.valid())
            {
                double offset = 0.0f;
                CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
                    _area->getInterface<ELEMENTS::IDrawTechnique>();
                if (draw.valid())
                {
                    offset = draw->getHeightOffset();
                }
                osg::Vec3d offsetPoint = UTIL::CoordinateConversionUtils::
                    GeodeticToECEF(osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z() + offset));

                _currentSelectedPoint = _multiPoint->findNearestCntrlPoint(offsetPoint);

                if (_currentSelectedPoint > -1)
                {
                    CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                        APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
                    if (tph.valid())
                    {
                        CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                        if (observable)
                        {
                            observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                        }
                    }
                    _createMoveUndoTransaction(_currentSelectedPoint);
                }
                else
                {
                    CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                        APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
                    if (tph.valid())
                    {
                        CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                        if (observable)
                        {
                            observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                        }
                    }
                    _currentSelectedPoint = -1;

                    if (_selectedPointList.size() == 2)
                    {
                        if ((_selectedPointList[0] + 1 == _selectedPointList[1]) ||  // adjacent points
                            (_selectedPointList[0] - 1 == _selectedPointList[1]))
                        {
                            int first = -1;

                            if (_selectedPointList[0] < _selectedPointList[1])
                            {
                                first = _selectedPointList[0];
                            }
                            else
                            {
                                first = _selectedPointList[1];
                            }
                            _insertPoint(first + 1, longLatAlt);
                            _multiPoint->setHighlightFlagAtPoint(first + 1, true);
                            _selectedPointList.push_back(first + 2);

                            osg::Vec3 point0 = _line->getValueAt(first);
                            point0 = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                osg::Vec3d(point0.y(), point0.x(), point0.z()));

                            osg::Vec3 point1 = _line->getValueAt(first + 2);
                            point1 = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                osg::Vec3d(point1.y(), point1.x(), point1.z()));

                            osg::Vec3 pointm = _line->getValueAt(first + 1);
                            pointm = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                osg::Vec3d(pointm.y(), pointm.x(), pointm.z()));

                            double d0 = (point0 - pointm).length();
                            double d1 = (point1 - pointm).length();

                            if (d0 < d1)
                            {
                                _multiPoint->setHighlightFlagAtPoint(first, false);
                                std::vector<int>::iterator pos = find(_selectedPointList.begin(),
                                    _selectedPointList.end(), first);
                                _selectedPointList.erase(pos);
                            }
                            else
                            {
                                _multiPoint->setHighlightFlagAtPoint(first + 2, false);
                                std::vector<int>::iterator pos = find(_selectedPointList.begin(),
                                    _selectedPointList.end(), first + 2);
                                _selectedPointList.erase(pos);
                            }

                            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
                            notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
                        }
                        // for first and last point
                        else if (_selectedPointList[0] + _selectedPointList[1] == _line->getPointNumber() - 1)
                        {
                            int start = 0, end = -1;

                            if (_selectedPointList[0] != 0)
                            {
                                end = _selectedPointList[0];
                            }
                            else
                            {
                                end = _selectedPointList[1];
                            }

                            _addPoint(longLatAlt);
                            _multiPoint->setHighlightFlagAtPoint(end + 1, true);
                            _selectedPointList.push_back(end + 1);

                            osg::Vec3 point0 = _line->getValueAt(start);
                            point0 = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                osg::Vec3d(point0.y(), point0.x(), point0.z()));

                            osg::Vec3 point1 = _line->getValueAt(end);
                            point1 = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                osg::Vec3d(point1.y(), point1.x(), point1.z()));

                            osg::Vec3 pointm = _line->getValueAt(end + 1);
                            pointm = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                osg::Vec3d(pointm.y(), pointm.x(), pointm.z()));

                            double d0 = (point0 - pointm).length();
                            double d1 = (point1 - pointm).length();

                            if (d0 < d1)
                            {
                                _multiPoint->setHighlightFlagAtPoint(start, false);
                                std::vector<int>::iterator pos = find(_selectedPointList.begin(),
                                    _selectedPointList.end(), start);
                                _selectedPointList.erase(pos);
                            }
                            else
                            {
                                _multiPoint->setHighlightFlagAtPoint(end, false);
                                std::vector<int>::iterator pos = find(_selectedPointList.begin(),
                                    _selectedPointList.end(), end);
                                _selectedPointList.erase(pos);
                            }

                            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
                            notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
                        }
                    }
                }
            }
        }
        break;
        case AREA_MODE_CREATE_RECTANGLE:
        {
            _handleRectangleMode(longLatAlt);

            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
            notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
        }
        break;

        case AREA_MODE_FIXED_RECTANGLE:
        {
            _fixedRectExtents = _calculateExtents(longLatAlt);
            _handleRectangleMode(_fixedRectExtents);

            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
            notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
        }
        break;

        }
        return;
    }

    void AreaUIHandler::_createLine()
    {
        _line = new CORE::Line();
    }

    bool AreaUIHandler::_deletePointAtIndex(int index)
    {
        if (index <= -1)
        {
            return false;
        }


        if (!_area.valid())
        {
            return  false;
        }

        if (!_line.valid())
        {
            return  false;
        }

        if ((unsigned)index >= getNumPoints())
        {
            return false;
        }

        if (_mode == AREA_MODE_EDIT)
        {
            if (_line->getPointNumber() <= 3)
            {
                return false;
            }
        }

        APP::IUndoTransactionFactory* transactionFactory =
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> macroTransaction =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction1 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin
            ::PolygonSetExteriorRingTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction3 = transactionFactory->createUndoTransaction
            (*TRANSACTION::TransactionRegistryPlugin::LineRemovePointTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction2 = transactionFactory->createUndoTransaction
            (*TRANSACTION::TransactionRegistryPlugin::MultiPointRemovePointTransactionType);

        CORE::RefPtr<TRANSACTION::IPolygonSetExteriorRingTransaction> polygonSetExteriorRingTransaction =
            transaction1->getInterface<TRANSACTION::IPolygonSetExteriorRingTransaction>();

        polygonSetExteriorRingTransaction->setPolygon(_area.get());
        polygonSetExteriorRingTransaction->setExteriorRing(_line.get());

        CORE::RefPtr<TRANSACTION::ILineRemovePointTransaction> lineRemovePointTransaction =
            transaction3->getInterface<TRANSACTION::ILineRemovePointTransaction>();

        lineRemovePointTransaction->setLine(_line.get());
        lineRemovePointTransaction->setIndex(index);

        CORE::RefPtr<TRANSACTION::IMultiPointRemovePointTransaction> multiPointRemovePointTransaction =
            transaction2->getInterface<TRANSACTION::IMultiPointRemovePointTransaction>();

        multiPointRemovePointTransaction->setMultiPoint(_multiPoint.get());
        multiPointRemovePointTransaction->setIndex(index);

        macroTransaction->addChild(transaction3.get());

        //XXX - NS : Need better API support in multipoint
        //check if multipoint is initialized or not
        if (_multiPoint.valid() && _multiPoint->getNumPoints() > (unsigned)index)
        {
            macroTransaction->addChild(transaction2.get());
        }

        macroTransaction->addChild(transaction1.get());

        addAndExecuteUndoTransaction(macroTransaction.get());

        _selectedPointList.erase(_selectedPointList.begin());

        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
        notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);

        return true;
    }

    void AreaUIHandler::_createMoveUndoTransaction(unsigned int index)
    {
        if (!_area.valid())
        {
            return;
        }

        if (!_line.valid())
        {
            return;
        }

        if (index >= getNumPoints())
        {
            return;
        }

        osg::Vec3d longLatAlt = _line->getPoint(index)->getValue();

        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z()));

        APP::IUndoTransactionFactory* transactionFactory =
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> macroTransaction =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction1 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::
            PolygonSetExteriorRingTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction2 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::
            MultiPointEditPointTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction3 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::LineEditPointTransactionType);

        CORE::RefPtr<TRANSACTION::IPolygonSetExteriorRingTransaction> polygonSetExteriorRingTransaction =
            transaction1->getInterface<TRANSACTION::IPolygonSetExteriorRingTransaction>();

        polygonSetExteriorRingTransaction->setPolygon(_area.get());
        polygonSetExteriorRingTransaction->setExteriorRing(_line.get());

        CORE::RefPtr<TRANSACTION::IMultiPointEditPointTransaction> multiPointEditPointTransaction =
            transaction2->getInterface<TRANSACTION::IMultiPointEditPointTransaction>();

        multiPointEditPointTransaction->setMultiPoint(_multiPoint.get());
        multiPointEditPointTransaction->setIndex(index);
        multiPointEditPointTransaction->setPosition(point);

        CORE::RefPtr<TRANSACTION::ILineEditPointTransaction> lineEditPointTransaction =
            transaction3->getInterface<TRANSACTION::ILineEditPointTransaction>();

        lineEditPointTransaction->setLine(_line.get());
        lineEditPointTransaction->setIndex(index);
        lineEditPointTransaction->setPosition(longLatAlt);

        macroTransaction->addChild(transaction3.get());
        macroTransaction->addChild(transaction1.get());
        macroTransaction->addChild(transaction2.get());

        addAndExecuteUndoTransaction(macroTransaction.get());

        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
        notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);

    }

    void AreaUIHandler::_movePointAtIndex(unsigned int index, osg::Vec3 longLatAlt)
    {
        if (!_area.valid())
        {
            return;
        }

        if (!_line.valid())
        {
            return;
        }

        if (index >= getNumPoints())
        {
            return;
        }

        _line->editPoint(index, longLatAlt);
        _area->setExteriorRing(_line);

        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z()));

        if (_multiPoint.valid())
        {
            _multiPoint->editPointAtIndex(index, point);
        }


        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
        notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
    }

    void AreaUIHandler::_addPoint(osg::Vec3 longLatAlt)
    {
        if (!_area.valid())
        {
            return;
        }

        if (!_line.valid())
        {
            return;
        }

        double offset = 0.0f;
        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
            _line->getInterface<ELEMENTS::IDrawTechnique>();
        if (draw.valid())
        {
            offset = draw->getHeightOffset();
        }

        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z() + offset));

        APP::IUndoTransactionFactory* transactionFactory =
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> macroTransaction =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction1 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::PolygonSetExteriorRingTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction2 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::LineAddPointTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction3 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MultiPointAddPointTransactionType);

        CORE::RefPtr<TRANSACTION::IPolygonSetExteriorRingTransaction> polygonSetExteriorRingTransaction =
            transaction1->getInterface<TRANSACTION::IPolygonSetExteriorRingTransaction>();

        polygonSetExteriorRingTransaction->setPolygon(_area.get());
        polygonSetExteriorRingTransaction->setExteriorRing(_line.get());

        CORE::RefPtr<TRANSACTION::ILineAddPointTransaction> lineAddPointTransaction =
            transaction2->getInterface<TRANSACTION::ILineAddPointTransaction>();

        lineAddPointTransaction->setLine(_line.get());
        lineAddPointTransaction->setPosition(longLatAlt);

        CORE::RefPtr<TRANSACTION::IMultiPointAddPointTransaction> multiPointAddPointTransaction =
            transaction3->getInterface<TRANSACTION::IMultiPointAddPointTransaction>();

        multiPointAddPointTransaction->setMultiPoint(_multiPoint.get());
        multiPointAddPointTransaction->setPosition(point);

        macroTransaction->addChild(transaction2.get());
        macroTransaction->addChild(transaction1.get());
        macroTransaction->addChild(transaction3.get());

        addAndExecuteUndoTransaction(macroTransaction.get());

        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
        notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
    }

    void AreaUIHandler::_insertPoint(unsigned int index, osg::Vec3 longLatAlt)
    {
        if (!_area.valid())
        {
            return;
        }

        if (!_line.valid())
        {
            return;
        }

        if (index >= getNumPoints())
        {
            return;
        }

        double offset = 0.0f;
        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
            _line->getInterface<ELEMENTS::IDrawTechnique>();
        if (draw.valid())
        {
            offset = draw->getHeightOffset();
        }

        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z() + offset));

        APP::IUndoTransactionFactory* transactionFactory =
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> macroTransaction =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction1 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::PolygonSetExteriorRingTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction2 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::LineInsertPointTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction3 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MultiPointInsertPointTransactionType);

        CORE::RefPtr<TRANSACTION::IPolygonSetExteriorRingTransaction> polygonSetExteriorRingTransaction =
            transaction1->getInterface<TRANSACTION::IPolygonSetExteriorRingTransaction>();

        polygonSetExteriorRingTransaction->setPolygon(_area.get());
        polygonSetExteriorRingTransaction->setExteriorRing(_line.get());

        CORE::RefPtr<TRANSACTION::ILineInsertPointTransaction> lineInsertPointTransaction =
            transaction2->getInterface<TRANSACTION::ILineInsertPointTransaction>();

        lineInsertPointTransaction->setLine(_line.get());
        lineInsertPointTransaction->setIndex(index);
        lineInsertPointTransaction->setPosition(longLatAlt);

        CORE::RefPtr<TRANSACTION::IMultiPointInsertPointTransaction> multiPointInsertPointTransaction =
            transaction3->getInterface<TRANSACTION::IMultiPointInsertPointTransaction>();

        multiPointInsertPointTransaction->setMultiPoint(_multiPoint.get());
        multiPointInsertPointTransaction->setIndex(index);
        multiPointInsertPointTransaction->setPosition(point);

        macroTransaction->addChild(transaction2.get());
        macroTransaction->addChild(transaction1.get());
        macroTransaction->addChild(transaction3.get());

        addAndExecuteUndoTransaction(macroTransaction.get());
    }

    void AreaUIHandler::_createPolygon()
    {
        CORE::RefPtr<CORE::IObject> areaObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject
            (*ELEMENTS::ElementsRegistryPlugin::PolygonFeatureObjectType);

        //set the current temporary state
        CORE::RefPtr<CORE::ITemporary> temp = areaObject->getInterface<CORE::ITemporary>();
        if (temp.valid())
        {
            temp->setTemporary(true);
        }

        CORE::RefPtr<ELEMENTS::IDrawTechnique> dt = areaObject->getInterface<ELEMENTS::IDrawTechnique>();
        if (dt.valid())
        {
            dt->setHeightOffset(_offset);
        }

        // Add the line to the world
        getWorldInstance()->addObject(areaObject.get());

        _area = areaObject->getInterface<CORE::IPolygon>();
        CORE::RefPtr<CORE::IBrushStyle> brush = areaObject->getInterface<CORE::IBrushStyle>();
        CORE::RefPtr<CORE::IPenStyle> pen = areaObject->getInterface<CORE::IPenStyle>();
        brush->setBrushState(CORE::IBrushStyle::BrushState(CORE::IBrushStyle::ON | CORE::IBrushStyle::PROTECTED));
        brush->setBrushFillColor(osg::Vec4(0.0, 0.0, 1.0, 0.4));
        pen->setPenWidth(1.0f);

        // Query TerrainUIHandler interface
        CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
        if (tph.valid())
        {
            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
            if (observable)
            {
                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);

                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
            }
        }
    }

    void AreaUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            if (wm.valid())
            {
                _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
            }
        }
        if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
        }
        if (messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            // Read intersections from Terrain UIHandler
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tpu =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());

            if (tpu.valid())
            {
                // Get the IMouseMessage interface
                CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();


                // get terrain position
                osg::Vec3d pos;
                if (tpu->getMousePickedPosition(pos, true))
                {
                    _clickedVertex = pos;
                    _handleMouseClickedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }

            }
            _dragged = false;
        }
        else if (messageType == *VizUI::IMouseMessage::HandledMouseDraggedMessageType)
        {
            _dragged = true;
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
            if (tph.valid())
            {
                osg::Vec3d pos;
                if (tph->getMousePickedPosition(pos, true))
                {
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleMouseDragged(mmsg->getMouse()->getButtonMask(), pos);
                }

                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaUpdatedMessageType);
                notifyObservers(*IAreaUIHandler::AreaUpdatedMessageType, *msg);
            }
        }
        else if (messageType == *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType)
        {
            // double click has problems in touch pad
        }
        else if (messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

            if (!_handleButtonRelease)
            {
                _handleButtonRelease = true;
                if (mmsg->getMouse()->getButtonMask() & (VizUI::IMouse::RIGHT_BUTTON | VizUI::IMouse::LEFT_BUTTON))
                {
                    return;
                }
            }

            if (!_dragged)
            {
                _handleMouseReleased(mmsg->getMouse()->getButtonMask());
            }
            else
            {
                CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
                if (tph.valid())
                {
                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAreaUIHandler::AreaCreatedMessageType);
                    notifyObservers(*IAreaUIHandler::AreaCreatedMessageType, *msg);
                }
            }
            _dragged = false;
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }


    void AreaUIHandler::_handleMouseDragged(int button, const osg::Vec3d& longLatAlt)
    {
        if (!_processMouseEvents)
        {
            return;
        }

        if (!(button & VizUI::IMouse::LEFT_BUTTON) || (AREA_MODE_NONE == _mode))
        {
            return;
        }
        switch (_mode)
        {
        case AREA_MODE_CREATE_AREA:
        case AREA_MODE_EDIT:
        {
            if (_area.valid() && _line.valid())
            {
                double offset = 0.0f;
                CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
                    _line->getInterface<ELEMENTS::IDrawTechnique>();
                if (draw.valid())
                {
                    offset = draw->getHeightOffset();
                }

                osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                    osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z() + offset));
                if (_currentSelectedPoint > -1)
                {
                    _movePointAtIndex(_currentSelectedPoint, longLatAlt);
                }
            }
        }
        break;
        case AREA_MODE_CREATE_RECTANGLE:
        {
            _handleRectangleMode(longLatAlt);
        }
        break;
        case AREA_MODE_FIXED_RECTANGLE:
        {
            _fixedRectExtents = _calculateExtents(longLatAlt);
            _handleRectangleMode(_fixedRectExtents);
        }
        break;

        }
    }
    void AreaUIHandler::_setArea(osg::Vec4d _extents)
    {
        osg::ref_ptr<osg::Vec3dArray> polygonPoints = new osg::Vec3dArray;
        polygonPoints->clear();
        polygonPoints->push_back(osg::Vec3d(_extents.x(), _extents.y(), ELEMENTS::GetAltitudeAtLongLat(_extents.x(), _extents.y())));
        polygonPoints->push_back(osg::Vec3d(_extents.x(), _extents.w(), ELEMENTS::GetAltitudeAtLongLat(_extents.x(), _extents.w())));
        polygonPoints->push_back(osg::Vec3d(_extents.z(), _extents.w(), ELEMENTS::GetAltitudeAtLongLat(_extents.z(), _extents.w())));
        polygonPoints->push_back(osg::Vec3d(_extents.z(), _extents.y(), ELEMENTS::GetAltitudeAtLongLat(_extents.z(), _extents.y())));

        _line->clear();
        osg::Vec3dArray::iterator iter = polygonPoints->begin();
        for (; iter != polygonPoints->end(); ++iter)
        {
            osg::Vec3d p = *iter;
            CORE::RefPtr<CORE::IPoint> point = new CORE::Point();
            point->setValue(p.x(), p.y(), p.z());
            _line->addPoint(point.get());
        }

        // Push list to polygon
        _area->setExteriorRing(_line.get());
    }

    void AreaUIHandler::_handleRectangleMode(osg::Vec3d longLatAlt)
    {
        if (!_firstVertexSet)
        {
            _line->clear();
            _firstVertex = longLatAlt;
            _firstVertexSet = true;
        }
        else
        {
            _secondVertex = longLatAlt;

            osg::Vec2d f = osg::Vec2d(_firstVertex.x(), _firstVertex.y());
            osg::Vec2d s = osg::Vec2d(_secondVertex.x(), _secondVertex.y());

            // Check min-max and swap values, if not proper
            if (f.x() > s.x())
            {
                double tmp = f.x();
                f.x() = s.x();
                s.x() = tmp;
            }
            if (f.y() > s.y())
            {
                double tmp = f.y();
                f.y() = s.y();
                s.y() = tmp;
            }

            _setArea(osg::Vec4d(f.x(), f.y(), s.x(), s.y()));

        }

    }

    void AreaUIHandler::_handleRectangleMode(osg::Vec4d extents)
    {
        if (_fixedRectExtents.x() != 0 && _fixedRectExtents.y() != 0 && _fixedRectExtents.z() != 0 && _fixedRectExtents.w() != 0)
        {
			if (_line.valid())
			{
				_line->clear();
			}

            _fixedRectExtents = extents;
            _setArea(_fixedRectExtents);
        }
    }

    void AreaUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizUI::UIHandler::onAddedToManager();
    }

    void AreaUIHandler::setFocus(bool value)
    {
        if (!value)
        {
            reset();
            _area = NULL;
        }

        _processMouseEvents = value;

        UIHandler::setFocus(value);
    }

    void AreaUIHandler::setSelectedAreaAsCurrentArea()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if (iter != map.end())
        {
            _area = iter->second->getInterface<CORE::IPolygon>();
            if (_area.valid())
            {
                _line = _area->getExteriorRing();
            }
        }
    }

    void AreaUIHandler::setMetadataRecord(CORE::IMetadataRecord* record)
    {
        if (_area.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder =
                _area->getInterface<CORE::IMetadataRecordHolder>();

            if (holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataRecord> oldrecord = holder->getMetadataRecord();
                if (!oldrecord.valid())
                {
                    holder->setMetadataRecord(record);
                }
            }
        }
    }

    CORE::RefPtr<CORE::IMetadataRecord>
        AreaUIHandler::getMetadataRecord() const
    {
        if (_area.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder =
                _area->getInterface<CORE::IMetadataRecordHolder>();

            if (holder.valid())
            {
                return holder->getMetadataRecord();
            }
        }
        return NULL;
    }

    void AreaUIHandler::update()
    {
        if (_area.valid())
        {
            CORE::RefPtr<CORE::IFeatureObject> feature =
                _area->getInterface<CORE::IFeatureObject>();
            if (feature.valid())
            {
                feature->update();
            }
        }
    }

    void AreaUIHandler::getDefaultLayerAttributes(std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecesions) const
    {
        attrNames.push_back("NAME");
        attrTypes.push_back("Text");
        attrWidths.push_back(128);
        attrPrecesions.push_back(1);
    }

    CORE::RefPtr<CORE::IFeatureLayer>
        AreaUIHandler::getOrCreateAreaLayer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer =
            CORE::WorldMaintainer::instance();
        if (!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return NULL;
        }

        CORE::RefPtr<CORE::IComponent> component =
            worldMaintainer->getComponentByName("LayerComponent");

        if (!component.valid())
        {
            LOG_ERROR("Layer Component is not found");
            return NULL;
        }

        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent =
            component->getInterface<SMCElements::ILayerComponent>();

        if (!layerComponent.valid())
        {
            return NULL;
        }
        CORE::RefPtr<CORE::IFeatureLayer> layer = layerComponent->getFeatureLayer("Area");

        if (!layer.valid())
        {
            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecesions;

            getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecesions);

            layer = layerComponent->getOrCreateFeatureLayer("Area", "Polygon", attrNames,
                attrTypes, attrWidths, attrPrecesions);
        }

        return layer.get();
    }


    void AreaUIHandler::addAreaToCurrentSelectedLayer()
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateAreaLayer();
        _addToLayer(layer.get());
    }

    CORE::RefPtr<CORE::IMetadataTableDefn>
        AreaUIHandler::getCurrentSelectedLayerDefn()
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateAreaLayer();

        //XXX - using the first element in the selection
        if (layer.valid())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder =
                layer->getInterface<CORE::IMetadataTableHolder>();

            if (holder.valid())
            {
                return holder->getMetadataTable()->getMetadataTableDefn();
            }
        }
        return NULL;
    }

    void AreaUIHandler::removeCreatedArea()
    {
        if (_area.valid())
        {
            getWorldInstance()->removeObjectByID(&(_area->getInterface<CORE::IBase>()->getUniqueID()));
            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
            _area = NULL;
            _line = NULL;
        }
    }

    void AreaUIHandler::deleteLastPointFromArea()
    {
        if (_line.valid() && _area.valid() && _line->getPointNumber() > 0)
        {
            _line->deleteLastPoint();
            _area->setExteriorRing(_line.get());
        }
    }

    CORE::IPolygon* AreaUIHandler::getCurrentArea() const
    {
        return _area.get();
    }

    void AreaUIHandler::setTemporaryState(bool state)
    {
        _temporary = state;
    }

    bool AreaUIHandler::getTemporaryState() const
    {
        return _temporary;
    }

    void AreaUIHandler::reset()
    {
        setMode(AREA_MODE_NONE);
        _area = NULL;

        _temporary = false;
        _processMouseEvents = false;
        _dragged = false;
    }

    void AreaUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "AreaUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("Offset", "Offset",
            CORE::DoubleAttribute::SetFuncType(this, &AreaUIHandler::setOffset),
            CORE::DoubleAttribute::GetFuncType(this, &AreaUIHandler::getOffset),
            "Offset name",
            groupName));
    }

    void AreaUIHandler::setOffset(double offset)
    {
        _offset = offset;
    }

    double AreaUIHandler::getOffset() const
    {
        return _offset;
    }

    void AreaUIHandler::_setProcessMouseEvents(bool value)
    {
        _processMouseEvents = value;
    }

    bool AreaUIHandler::_getProcessMouseEvents() const
    {
        return _processMouseEvents;
    }

    unsigned int AreaUIHandler::getNumPoints() const
    {
        if (_line.valid())
        {
            return _line->getPointNumber();
        }

        return 0;
    }
    void AreaUIHandler::_deleteCurrentSelectedArea()
    {
        if (_area.valid())
        {
            APP::IUndoTransactionFactory* transactionFactory =
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction =
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::
                DeleteObjectTransactionType);

            CORE::RefPtr<TRANSACTION::IDeleteObjectTransaction> deleteObjectTransaction =
                transaction->getInterface<TRANSACTION::IDeleteObjectTransaction>();

            deleteObjectTransaction->setObject(_area->getInterface<CORE::IDeletable>());

            addAndExecuteUndoTransaction(transaction.get());

            if (_multiPoint.valid())
            {
                getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
                _multiPoint->clearPoints();
                _multiPoint->showCntrlPoints(false);
            }
        }
    }

    // Get Clicked Point
    osg::Vec3d AreaUIHandler::getClickedPoint()
    {
        return _clickedVertex;
    }

} // namespace SMCUI
