/*****************************************************************************
*
* File             : DistanceCalculationUIHandler.cpp
* Description      : DistanceCalculationUIHandler class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCUI/DistanceCalculationUIHandler.h>
#include <App/IApplication.h>
#include <Core/CoreRegistry.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <App/AccessElementUtils.h>
#include <Core/ITerrain.h>
#include <Util/BaseExceptions.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI,  DistanceCalculationUIHandler);
    DEFINE_IREFERENCED(DistanceCalculationUIHandler, UIHandler);

    DistanceCalculationUIHandler::DistanceCalculationUIHandler()
        : _visitor(NULL)
    {
        // TBD initiliaze the member variables in initialization list
        _addInterface(SMCUI::IDistanceCalculationUIHandler::getInterfaceName());

        setName(getClassname());
    }

    DistanceCalculationUIHandler::~DistanceCalculationUIHandler()
    {}

    void DistanceCalculationUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void DistanceCalculationUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void DistanceCalculationUIHandler::setComputationType(ComputationType type)
    {
        if(_visitor.valid())
        {
            switch(type)
            {
            case IDistanceCalculationUIHandler::AERIAL:
                _visitor->setComputationType(GISCOMPUTE::IDistanceCalculationVisitor::AERIAL);
                break;
            case IDistanceCalculationUIHandler::PROJECTED:
                _visitor->setComputationType(GISCOMPUTE::IDistanceCalculationVisitor::PROJECTED);
                break;
            }
        }
    }

    IDistanceCalculationUIHandler::ComputationType 
        DistanceCalculationUIHandler::getComputationType()
    {
        IDistanceCalculationUIHandler::ComputationType type = IDistanceCalculationUIHandler::INVALID;
        if(_visitor.valid())
        {
            switch(_visitor->getComputationType())
            {
            case GISCOMPUTE::IDistanceCalculationVisitor::AERIAL:
                type = IDistanceCalculationUIHandler::AERIAL;
                break;
            case GISCOMPUTE::IDistanceCalculationVisitor::PROJECTED:
                type = IDistanceCalculationUIHandler::PROJECTED;
                break;
            }            
        }
        return type;
    }

    void DistanceCalculationUIHandler::setPoints(osg::Vec3dArray* points)
    {
        if(_visitor.valid())
        {
            _visitor->setPoints(points);
        }
    }

    osg::Vec3dArray* 
        DistanceCalculationUIHandler::getPoints()
    {
        osg::Vec3dArray* points = NULL;
        if(_visitor.valid())
        {
            points = _visitor->getPoints();
        }
        return points;
    }

    void DistanceCalculationUIHandler::setFocus(bool value)
    {
        UIHandler::setFocus(value);
        if(_visitor.valid())
        {
            if(value)
            {
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
        }
    }

    void DistanceCalculationUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::
                    DistanceCalculationVisitorType)->getInterface<GISCOMPUTE::IDistanceCalculationVisitor>();                
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::FilterStatus status = message.getInterface<GISCOMPUTE::IFilterStatusMessage>()->getStatus();
            if(status == GISCOMPUTE::FILTER_IDLE || status == GISCOMPUTE::FILTER_SUCCESS)
            {
                // XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage
                    (*SMCUI::IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType);
                notifyObservers(*SMCUI::IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType, *msg);
            }
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }

    double DistanceCalculationUIHandler::getDistance()
    {
        double area = 0.0;
        if(_visitor.valid())
        {
            area = _visitor->getDistance();
        }
        return area;
    }

    void DistanceCalculationUIHandler::execute()
    {
        try
        {
            GISCOMPUTE::IFilterVisitor* fv = _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            if(fv == NULL)
                return;

            if(fv->getFilterStatus() != GISCOMPUTE::FILTER_PENDING)
            {
                // Get the terrain handle and pass the visitor to terrain
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
                CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                CORE::RefPtr<CORE::IBase> terrainobj = terrain->getInterface<CORE::IBase>();
                terrainobj->accept(*_visitor->getInterface<CORE::IBaseVisitor>());
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void DistanceCalculationUIHandler::stop()
    {
        if(_visitor.valid())
        {
            _visitor->stop();
        }
    }

} // namespace SMCUI

