/*****************************************************************************
*
* File             : PerimeterCalcUIHandler.cpp
* Description      : PerimeterCalcUIHandler class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCUI/PerimeterCalcUIHandler.h>
#include <SMCUI/SurfaceAreaCalcUIHandler.h>
#include <App/IApplication.h>
#include <Core/CoreRegistry.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <App/AccessElementUtils.h>
#include <Core/ITerrain.h>
#include <Util/BaseExceptions.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, PerimeterCalcUIHandler);
    DEFINE_IREFERENCED(PerimeterCalcUIHandler, UIHandler);

    PerimeterCalcUIHandler::PerimeterCalcUIHandler()
        : _visitor(NULL)
    {
        // TBD initiliaze the member variables in initialization list
        _addInterface(SMCUI::IPerimeterCalcUIHandler::getInterfaceName());

        setName(getClassname());
    }

    PerimeterCalcUIHandler::~PerimeterCalcUIHandler()
    {}

    void PerimeterCalcUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void PerimeterCalcUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void PerimeterCalcUIHandler::setComputationType(ComputationType type)
    {
        if(_visitor.valid())
        {
            switch(type)
            {
            case IPerimeterCalcUIHandler::AERIAL:
                _visitor->setComputationType(GISCOMPUTE::IPerimeterCalcVisitor::AERIAL);
                break;
            case IPerimeterCalcUIHandler::PROJECTED:
                _visitor->setComputationType(GISCOMPUTE::IPerimeterCalcVisitor::PROJECTED);
                break;
            }
        }
    }

    IPerimeterCalcUIHandler::ComputationType 
        PerimeterCalcUIHandler::getComputationType()
    {
        IPerimeterCalcUIHandler::ComputationType type = IPerimeterCalcUIHandler::INVALID;
        if(_visitor.valid())
        {
            switch(_visitor->getComputationType())
            {
            case GISCOMPUTE::IPerimeterCalcVisitor::AERIAL:
                type = IPerimeterCalcUIHandler::AERIAL;
                break;
            case GISCOMPUTE::IPerimeterCalcVisitor::PROJECTED:
                type = IPerimeterCalcUIHandler::PROJECTED;
                break;
            }            
        }
        return type;
    }

    void PerimeterCalcUIHandler::setPoints(osg::Vec3dArray* points)
    {
        if(_visitor.valid())
        {
            _visitor->setPoints(points);
        }
    }

    osg::Vec3dArray* PerimeterCalcUIHandler::getPoints()
    {
        osg::Vec3dArray* points = NULL;
        if(_visitor.valid())
        {
            points = _visitor->getPoints();
        }
        return points;
    }

    void PerimeterCalcUIHandler::setFocus(bool value)
    {
        UIHandler::setFocus(value);
        if(_visitor.valid())
        {
            if(value)
            {
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
        }
    }

    void PerimeterCalcUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::
                    PerimeterCalcVisitorType)->getInterface<GISCOMPUTE::IPerimeterCalcVisitor>();                
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::FilterStatus status = message.getInterface<GISCOMPUTE::IFilterStatusMessage>()->getStatus();
            if(status == GISCOMPUTE::FILTER_IDLE || status == GISCOMPUTE::FILTER_SUCCESS)
            {
                // XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*SMCUI::ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType);
                notifyObservers(*SMCUI::ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType, *msg);
            }
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }

    double PerimeterCalcUIHandler::getPerimeter()
    {
        double perimeter = 0.0;
        if(_visitor.valid())
        {
            perimeter = _visitor->getPerimeter();
          
        }
        return perimeter;
    }

    void PerimeterCalcUIHandler::execute()
    {
        try
        {
            // Get the terrain handle and pass the visitor to terrain
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
            CORE::RefPtr<CORE::IBase> terrainobj = terrain->getInterface<CORE::IBase>();
            terrainobj->accept(*_visitor->getInterface<CORE::IBaseVisitor>());
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void PerimeterCalcUIHandler::stop()
    {
        if(_visitor.valid())
        {
            _visitor->stop();
        }
    }

} // namespace SMCUI

