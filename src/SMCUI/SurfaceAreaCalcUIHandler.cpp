/*****************************************************************************
*
* File             : SurfaceAreaCalcUIHandler.cpp
* Description      : SurfaceAreaCalcUIHandler class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCUI/SurfaceAreaCalcUIHandler.h>
#include <App/IApplication.h>
#include <Core/CoreRegistry.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <App/AccessElementUtils.h>
#include <Core/ITerrain.h>
#include <Util/BaseExceptions.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, SurfaceAreaCalcUIHandler);
    DEFINE_IREFERENCED(SurfaceAreaCalcUIHandler, UIHandler);

    SurfaceAreaCalcUIHandler::SurfaceAreaCalcUIHandler()
        : _visitor(NULL)
    {
        // TBD initiliaze the member variables in initialization list
        _addInterface(SMCUI::ISurfaceAreaCalcUIHandler::getInterfaceName());

        setName(getClassname());
    }

    SurfaceAreaCalcUIHandler::~SurfaceAreaCalcUIHandler()
    {}

    void SurfaceAreaCalcUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void SurfaceAreaCalcUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void SurfaceAreaCalcUIHandler::setComputationType(ComputationType type)
    {
        if(_visitor.valid())
        {
            switch(type)
            {
            case ISurfaceAreaCalcUIHandler::AERIAL:
                _visitor->setComputationType(GISCOMPUTE::ISurfaceAreaCalcVisitor::AERIAL);
                break;
            case ISurfaceAreaCalcUIHandler::PROJECTED:
                _visitor->setComputationType(GISCOMPUTE::ISurfaceAreaCalcVisitor::PROJECTED);
                break;
            }
        }
    }

    ISurfaceAreaCalcUIHandler::ComputationType 
        SurfaceAreaCalcUIHandler::getComputationType()
    {
        ISurfaceAreaCalcUIHandler::ComputationType type = ISurfaceAreaCalcUIHandler::INVALID;
        if(_visitor.valid())
        {
            switch(_visitor->getComputationType())
            {
            case GISCOMPUTE::ISurfaceAreaCalcVisitor::AERIAL:
                type = ISurfaceAreaCalcUIHandler::AERIAL;
                break;
            case GISCOMPUTE::ISurfaceAreaCalcVisitor::PROJECTED:
                type = ISurfaceAreaCalcUIHandler::PROJECTED;
                break;
            }            
        }
        return type;
    }

    void SurfaceAreaCalcUIHandler::setPoints(osg::Vec3dArray* points)
    {
        if(_visitor.valid())
        {
            _visitor->setPoints(points);
        }
    }

    osg::Vec3dArray* SurfaceAreaCalcUIHandler::getPoints()
    {
        osg::Vec3dArray* points = NULL;
        if(_visitor.valid())
        {
            points = _visitor->getPoints();
        }
        return points;
    }

    void SurfaceAreaCalcUIHandler::setFocus(bool value)
    {
        UIHandler::setFocus(value);
        if(_visitor.valid())
        {
            if(value)
            {
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
        }
    }

    void SurfaceAreaCalcUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::
                    SurfaceAreaCalcVisitorType)->getInterface<GISCOMPUTE::ISurfaceAreaCalcVisitor>();                
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::FilterStatus status = message.getInterface<GISCOMPUTE::IFilterStatusMessage>()->getStatus();
            if(status == GISCOMPUTE::FILTER_IDLE || status == GISCOMPUTE::FILTER_SUCCESS)
            {
                // XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*SMCUI::ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType);
                notifyObservers(*SMCUI::ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType, *msg);
            }
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }

    double SurfaceAreaCalcUIHandler::getArea()
    {
        double area = 0.0;
        if(_visitor.valid())
        {
            area = _visitor->getArea();
        }
        return area;
    }

    void SurfaceAreaCalcUIHandler::execute()
    {
        try
        {
            // Get the terrain handle and pass the visitor to terrain
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
            CORE::RefPtr<CORE::IBase> terrainobj = terrain->getInterface<CORE::IBase>();
            terrainobj->accept(*_visitor->getInterface<CORE::IBaseVisitor>());
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void SurfaceAreaCalcUIHandler::stop()
    {
        if(_visitor.valid())
        {
            _visitor->stop();
        }
    }

} // namespace SMCUI

