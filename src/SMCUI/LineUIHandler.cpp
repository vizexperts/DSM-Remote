
#include <SMCUI/LineUIHandler.h>
#include <SMCUI/SMCUIPlugin.h>

#include <Core/IFeatureObject.h>
#include <Core/InterfaceUtils.h>
#include <Core/IPenStyle.h>
#include <Core/AttributeTypes.h>
#include <Core/ISelectionComponent.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataRecord.h>
#include <Core/IFeatureLayer.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataTable.h>
#include <Core/ICompositeObject.h>
#include <Core/ITemporary.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITerrain.h>
#include <Core/CoreRegistry.h>
#include <Core/IEditable.h>
#include <Core/IText.h>

#include <Elements/MultiPoint.h>
#include <Elements/IDrawTechniqueHolder.h>
#include <Elements/IClamper.h>
#include <Elements/ILineDrawTechnique.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/ElementsUtils.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouse.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IModelPickUIHandler.h>


#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <App/IUndoTransaction.h>

#include <Util/CoordinateConversionUtils.h>

#include <Transaction/TransactionPlugin.h>
#include <Transaction/ILineAddPointTransaction.h>
#include <Transaction/ILineRemoveLastPointTransaction.h>
#include <Transaction/ILineRemovePointTransaction.h>
#include <Transaction/ICompositeAddObjectTransaction.h>
#include <Transaction/ILineEditPointTransaction.h>
#include <Transaction/ILineInsertPointTransaction.h>
#include <Transaction/IMultiPointEditPointTransaction.h>
#include <Transaction/IMultiPointInsertPointTransaction.h>
#include <Transaction/IMultiPointAddPointTransaction.h>
#include <Transaction/IMultiPointRemovePointTransaction.h>
#include <Transaction/IDeleteObjectTransaction.h>

#include <SMCElements/LayerComponent.h>

#include <osg/LineWidth>
#include <osgText/Text>
#include <osg/ShapeDrawable>
#include <osgText/Text3D>

#include <sstream>
#include <Util/StyleFileUtil.h>
#include <serialization/StyleSheetParser.h>
#include <Elements/FeatureObject.h>

namespace SMCUI 
{
    //setting default pen color to off white along with pen width
    const osg::Vec4 LineUIHandler::DefaultColor = osg::Vec4(0.98, 0.92, 0.84, 1.0);
    const double LineUIHandler::DefaultWidth = 2.0;

    DEFINE_META_BASE(SMCUI, LineUIHandler);
    DEFINE_IREFERENCED(LineUIHandler, VizUI::UIHandler);

    LineUIHandler::LineUIHandler()
        : _mode(LINE_MODE_NONE)
        , _offset(50.0f)
        , _lineColor(DefaultColor)
        , _lineWidth(DefaultWidth)
        , _temporary(false)
        , _clamping(true)
        , _currentSelectedPoint(-1)
        , _dragged(false)
        , _processMouseEvents(false)
        ,_handleButtonRelease(false)
    {
        _addInterface(SMCUI::ILineUIHandler::getInterfaceName());
        _selectedPointList.clear();
        setName(getClassname());
    }

    void LineUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizUI::UIHandler::onAddedToManager();
    }

    void LineUIHandler::_resetMultiPoints()
    {
        if(!_line.valid())
        {
            return;
        }
        if(!_multiPoint.valid())
        {
            _multiPoint = new ELEMENTS::MultiPoint;
        }
        if(_multiPoint.valid())
        {
            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
            _multiPoint->clearPoints();
            _multiPoint->showCntrlPoints(true);
            _currentSelectedPoint = -1;
            _selectedPointList.clear();

            CORE::RefPtr<ELEMENTS::IDrawTechnique> drawTechnique =
                _line->getInterface<ELEMENTS::IDrawTechnique>();

            double offset = 0.0;
            if(drawTechnique.valid())
            {
                offset = drawTechnique->getHeightOffset();
            }

            ELEMENTS::ILineDrawTechnique* ldt =
                _line->getInterface<ELEMENTS::ILineDrawTechnique>();

            if(ldt)
            {
                for(unsigned int i = 0; i < _line->getPointNumber(); i++)
                {
                    osg::Vec3d position = ldt->getECEFValue(i);
                    _multiPoint->add(position);
                }
            }
            else
            {
                for(unsigned int i = 0; i < _line->getPointNumber(); i++)
                {
                    CORE::RefPtr<CORE::IPoint> point = _line->getPoint(i);
                    osg::Vec3d posLongLatAlt = point->getValue();
                    osg::Vec3d position =
                        UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(posLongLatAlt.y(), posLongLatAlt.x(), posLongLatAlt.z() + offset));
                    _multiPoint->add(position);
                }
            }

            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->addChild(_multiPoint->getOSGGroupNode());
        }

    }
    bool LineUIHandler::setMode(LineMode mode)
    {

        if(_mode == mode)
        {
            return true;
        }
        _mode = mode;

        // Subscribe for TerrainPick messages
        _processMouseEvents = true;


        switch(_mode)
        {
        case LINE_MODE_CREATE_LINE:
            _setCreateLineMode();
            break;

        case LINE_MODE_EDIT:
            _setEditMode();
            break;

        case LINE_MODE_NONE:
            _handleButtonRelease = false;
            _setModeNone();
            break;

        case LINE_MODE_NO_OPERATION:
            _setModeNoOperation();
            break;
        }

        return true;
    }

    void LineUIHandler::_setModeNoOperation()
    {
        setSelectedLineAsCurrentLine();
        if(!_line.valid())
        {
            return;
        }

        if(_multiPoint.valid())
        {
            _multiPoint->showCntrlPoints(false);
        }

        // register for mouse release.
        CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
        if(tph.valid())
        {
            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
            if(observable)
            {   
                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
            }
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }
    }
    void LineUIHandler::_setModeNone()
    {
        _processMouseEvents = false;
        // Query ModelPickUIHandler interface
        CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
        if(tph.valid())
        {
            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
            if(observable)
            {   
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }
        }

        _line = NULL;

        if(_multiPoint.valid())
        {
            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
            _multiPoint->clearPoints();
            _multiPoint->showCntrlPoints(false);
            _currentSelectedPoint = -1;
            _selectedPointList.clear();
        }

        _lineColor = DefaultColor;
        _lineWidth = DefaultWidth;

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
            selectionUIHandler->setMouseClickSelectionState(true);
        }
    }

    void LineUIHandler::_setEditMode()
    {
        if(!_line.valid())
        {
            return;
        }
        setSelectedLineAsCurrentLine();
        _resetMultiPoints();

        // Query ModelPickUIHandler interface
        CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
        if(tph.valid())
        {
            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
            if(observable)
            {   

                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
            }
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }

        return;
    }

    bool LineUIHandler::_deletePointAtIndex(unsigned int index)
    {
        if(!_line.valid())
        {
            return false;
        }

        if(index >= _line->getPointNumber())
        {
            return false;
        }

        if(_line->getPointNumber() <= 2)
        {
            if(_mode == LINE_MODE_EDIT)
            {
                return false;
            }
        }

        APP::IUndoTransactionFactory* transactionFactory = 
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        //XXX - NS : Need better API support in multipoint
        //check if multipoint is initialized or not
        if(!_multiPoint.valid() || _multiPoint->getNumPoints() <= index)
        {
            CORE::RefPtr<APP::IUndoTransaction> transaction =
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::
                LineRemovePointTransactionType);

            CORE::RefPtr<TRANSACTION::ILineRemovePointTransaction> lineRemovePointTransaction = 
                transaction->getInterface<TRANSACTION::ILineRemovePointTransaction>();

            lineRemovePointTransaction->setLine(_line.get());
            lineRemovePointTransaction->setIndex(index);

            addAndExecuteUndoTransaction(transaction.get());

            return true;
        }
        else
        {
            CORE::RefPtr<APP::IUndoTransaction> macroTransaction = 
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);

            CORE::RefPtr<APP::IUndoTransaction> transaction1 =
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::
                LineRemovePointTransactionType);

            CORE::RefPtr<APP::IUndoTransaction> transaction2 =
                transactionFactory->createUndoTransaction(*TRANSACTION::
                TransactionRegistryPlugin::MultiPointRemovePointTransactionType);

            CORE::RefPtr<TRANSACTION::ILineRemovePointTransaction> lineRemovePointTransaction = 
                transaction1->getInterface<TRANSACTION::ILineRemovePointTransaction>();

            lineRemovePointTransaction->setLine(_line.get());
            lineRemovePointTransaction->setIndex(index);

            CORE::RefPtr<TRANSACTION::IMultiPointRemovePointTransaction> multiPointRemovePointTransaction = 
                transaction2->getInterface<TRANSACTION::IMultiPointRemovePointTransaction>();

            multiPointRemovePointTransaction->setMultiPoint(_multiPoint.get());
            multiPointRemovePointTransaction->setIndex(index);

            macroTransaction->addChild(transaction1.get());
            macroTransaction->addChild(transaction2.get());

            addAndExecuteUndoTransaction(macroTransaction.get());

            _selectedPointList.erase( _selectedPointList.begin() );

            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILineUIHandler::LineUpdatedMessageType);
            notifyObservers(*ILineUIHandler::LineUpdatedMessageType, *msg);

            return true;
        }
    }

    void LineUIHandler::_movePointAtIndex(unsigned int index, osg::Vec3d longLatAlt)
    {
        if(!_line.valid())
        {
            return;
        }

        if(index >= _line->getPointNumber())
        {
            return;
        }

        _line->editPoint(index, longLatAlt);

        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), std::abs(longLatAlt.z())));
        if(_multiPoint.valid())
        {
            _multiPoint->editPointAtIndex(index, point);
        }

    }

    void LineUIHandler::_addPoint(osg::Vec3d longLatAlt)
    {
        if(!_line.valid())
        {
            return;
        }

        double offset = 0.0f;
        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
            _line->getInterface<ELEMENTS::IDrawTechnique>();
        if(draw.valid())
        {
            offset = draw->getHeightOffset();
        }

        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), std::abs(longLatAlt.z()) + offset));

        APP::IUndoTransactionFactory* transactionFactory = 
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> macroTransaction = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);


        CORE::RefPtr<APP::IUndoTransaction> transaction1 = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::LineAddPointTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction2 = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MultiPointAddPointTransactionType);

        CORE::RefPtr<TRANSACTION::ILineAddPointTransaction> lineAddPointTransaction = 
            transaction1->getInterface<TRANSACTION::ILineAddPointTransaction>();

        lineAddPointTransaction->setLine(_line.get());
        lineAddPointTransaction->setPosition(longLatAlt);

        CORE::RefPtr<TRANSACTION::IMultiPointAddPointTransaction> multiPointAddPointTransaction =
            transaction2->getInterface<TRANSACTION::IMultiPointAddPointTransaction>();

        multiPointAddPointTransaction->setMultiPoint(_multiPoint.get());
        multiPointAddPointTransaction->setPosition(point);

        macroTransaction->addChild(transaction1.get());
        macroTransaction->addChild(transaction2.get());

        addAndExecuteUndoTransaction(macroTransaction.get());

    }

    void LineUIHandler::_insertPoint(unsigned int index, osg::Vec3 longLatAlt)
    {
        if(!_line.valid())
        {
            return;
        }

        if(index >= _line->getPointNumber())
        {
            return;
        }

        double offset = 0.0f;
        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
            _line->getInterface<ELEMENTS::IDrawTechnique>();
        if(draw.valid())
        {
            offset = draw->getHeightOffset();
        }

        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), std::abs(longLatAlt.z() + offset)));

        APP::IUndoTransactionFactory* transactionFactory = 
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> macroTransaction = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction1 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::LineInsertPointTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction2 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MultiPointInsertPointTransactionType);

        CORE::RefPtr<TRANSACTION::ILineInsertPointTransaction> lineInsertPointTransaction = 
            transaction1->getInterface<TRANSACTION::ILineInsertPointTransaction>();

        lineInsertPointTransaction->setLine(_line.get());
        lineInsertPointTransaction->setIndex(index);
        lineInsertPointTransaction->setPosition(longLatAlt);

        CORE::RefPtr<TRANSACTION::IMultiPointInsertPointTransaction> multiPointInsertPointTransaction = 
            transaction2->getInterface<TRANSACTION::IMultiPointInsertPointTransaction>();

        multiPointInsertPointTransaction->setMultiPoint(_multiPoint.get());
        multiPointInsertPointTransaction->setIndex(index);
        multiPointInsertPointTransaction->setPosition(point);

        macroTransaction->addChild(transaction1.get());
        macroTransaction->addChild(transaction2.get());

        addAndExecuteUndoTransaction(macroTransaction.get());
    }

    void LineUIHandler::_setCreateLineMode()
    {
        createLineFeatureObject();

        _resetMultiPoints();

        // Query ModelPickUIHandler interface
        CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
        if(tph.valid())
        {
            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
            if(observable)
            {   

                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMousePressedMessageType);
                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
            }
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }
    }

    void LineUIHandler::_setProcessMouseEvents(bool value)
    {
        _processMouseEvents = value;
    }

    bool LineUIHandler::_getProcessMouseEvents() const
    {
        return _processMouseEvents;
    }

    LineUIHandler::LineMode LineUIHandler::getMode() const
    {
        return _mode;
    }

    void LineUIHandler::setColor(osg::Vec4 color)
    {
        _lineColor = color;
        if(_line.valid())
        {
            try
            {
                _line->getInterface<CORE::IPenStyle>()->setPenColor(_lineColor);
            }
            catch(...){}
        }
    }

    osg::Vec4 LineUIHandler::getColor() const
    {
        //XXX - TBD, ILine doesn't have any color property
        if(_line.valid())
        {
            try
            {
                return _line->getInterface<CORE::IPenStyle>()->getPenColor();
            }
            catch(...){}
        }

        return _lineColor;
    }


    void LineUIHandler::setWidth(double width)
    {
        //XXX - TBD, ILine doesn't have any width property
        _lineWidth = width;
        if(_line.valid())
        {
            try
            {
                _line->getInterface<CORE::IPenStyle>()->setPenWidth(_lineWidth);
            }
            catch(...){}
        }
    }

    double LineUIHandler::getWidth() const
    {
        //XXX - TBD, ILine doesn't have any width property
        if(_line.valid())
        {
            try
            {
                return _line->getInterface<CORE::IPenStyle>()->getPenWidth();
            }
            catch(...)
            {}
        }
        return 0;
    }

    std::vector<int>& LineUIHandler::getSelectedVertexList()
    {
        return _selectedPointList;
    }

    void LineUIHandler::setSelectedVertexList( std::vector<int> selectedPointList )
    {
        _selectedPointList.clear();
        _selectedPointList = selectedPointList;
    }

    CORE::IObject* LineUIHandler::createFolderObject()
    {
        CORE::IObject* folderObject = 
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::FolderType);

        return folderObject;
    }

    CORE::ILine* LineUIHandler::createLineFeatureObject()
    {
        CORE::RefPtr<CORE::IObject> lineObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->
            createObject(*ELEMENTS::ElementsRegistryPlugin::LineFeatureObjectType);

        _line = lineObject->getInterface<CORE::ILine>();

        CORE::RefPtr<ELEMENTS::IDrawTechnique> lineDrawTech = _line->getInterface<ELEMENTS::IDrawTechnique>();

        if(lineDrawTech.valid())
        {
            //lineDrawTech->setHeightOffset(_offset);
        }

        CORE::RefPtr<CORE::ITemporary> temp = 
            lineObject->getInterface<CORE::ITemporary>();

        if(temp.valid())
        {
            // XXX - not using _temporary 
            temp->setTemporary(true);
        }

        CORE::RefPtr<ELEMENTS::IClamper> clamp = lineObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            //clamp->setClampOnPlacement(_clamping);
            //clamp->setClampOnTileLoading(_clamping);
        }

        // Add the line to the world
        getWorldInstance()->addObject(lineObject.get());

        //_line->getInterface<CORE::IPenStyle>()->setPenColor(_lineColor);
        //_line->getInterface<CORE::IPenStyle>()->setPenWidth(_lineWidth);

        return _line.get();
    }

    CORE::ILine* LineUIHandler::getCurrentLine() const
    {
        return _line.get();
    }

    void LineUIHandler::setFocus(bool value)
    {
        if(!value)
        {
            _reset();
        }

        _processMouseEvents = value;

        VizUI::UIHandler::setFocus(value);
    }

    void LineUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            if(wm.valid())
            {
                _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
            }
        }
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);
        }
        if (messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            // Read intersections from ModelPickUIHandler
            CORE::RefPtr<VizUI::IModelPickUIHandler> tpu =
                CORE::InterfaceUtils::getFirstOf<APP::IUIHandler, VizUI::IModelPickUIHandler>(
                getManager()->getUIHandlerMap());
            if(tpu.valid())
            {
                // get terrain position
                osg::Vec3d pos;
                if (tpu->getMousePickedPosition(pos, false, true))
                {
                    // Get the IMouseMessage interface
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleMouseClickedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }
            }
            _dragged = false;
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseDraggedMessageType)
        {
            _dragged = true;
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                 CORE::InterfaceUtils::getFirstOf<APP::IUIHandler, VizUI::IModelPickUIHandler>(
                getManager()->getUIHandlerMap());
            if(tph.valid())
            {
                osg::Vec3d pos;
                if (tph->getMousePickedPosition(pos, false, true))
                {
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleMouseDragged(mmsg->getMouse()->getButtonMask(), pos);
                }

                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILineUIHandler::LineUpdatedMessageType);
                notifyObservers(*ILineUIHandler::LineUpdatedMessageType, *msg);
            }

        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType)
        {
            // double click has problems in touch pad
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

            if(!_handleButtonRelease)
            {
                _handleButtonRelease = true;
                if(mmsg->getMouse()->getButtonMask() & (VizUI::IMouse::RIGHT_BUTTON | VizUI::IMouse::LEFT_BUTTON))
                {
                    return;
                }
            }

            if( !_dragged )
            {
                _handleMouseReleased(mmsg->getMouse()->getButtonMask());
            }
            else
            {
                CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                    CORE::InterfaceUtils::getFirstOf<APP::IUIHandler, VizUI::IModelPickUIHandler>(
                    getManager()->getUIHandlerMap());
                if(tph.valid())
                {
                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILineUIHandler::LineUpdatedMessageType);
                    notifyObservers(*ILineUIHandler::LineUpdatedMessageType, *msg);
                }

            }
            _dragged = false;
        }
        else 
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }

    void LineUIHandler::_handleMouseDragged(int button, const osg::Vec3d& longLatAlt)
    {
        if(!_processMouseEvents)
        {
            return;
        }

        if(!(button & VizUI::IMouse::LEFT_BUTTON) || (LINE_MODE_NONE == _mode))
        {
            return;
        }
        switch(_mode)
        {
        case LINE_MODE_CREATE_LINE:
        case LINE_MODE_EDIT:
            {
                if(_line.valid())
                {
                    if(_currentSelectedPoint > -1)
                    {
                        _movePointAtIndex(_currentSelectedPoint, longLatAlt);

                    }
                }
            }
            break;
        }
    }
    void LineUIHandler::_handleMouseReleased(int button)
    {
        if(!_processMouseEvents)
        {
            return;
        }

        if( (button & VizUI::IMouse::RIGHT_BUTTON) )
        {
            switch(_mode)
            {
            case LINE_MODE_CREATE_LINE:
                {
                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILineUIHandler::LineCreatedMessageType);

                    notifyObservers(*ILineUIHandler::LineCreatedMessageType, *msg);
                    if(_line.valid())
                    {
                        if(_line->getInterface<CORE::ISelectable>()->getSelected())
                        {
                            _line->getInterface<CORE::ISelectable>()->setSelected(false);
                        }
                    }
                }
                break;

            case LINE_MODE_NO_OPERATION:
            case LINE_MODE_EDIT:
                {
                    _selectedPointList.clear();
                    _reset();
                }
            }
        }
        else if ( button & VizUI::IMouse::LEFT_BUTTON )
        {
            switch( _mode )
            {
                case LINE_MODE_EDIT:
                {
                    if( _currentSelectedPoint > -1)
                    {
                        std::vector<int>::iterator pos = find( _selectedPointList.begin(),
                            _selectedPointList.end(), _currentSelectedPoint);

                        // toggling the selections
                        if( pos == _selectedPointList.end() )
                        {
                            _selectedPointList.push_back( _currentSelectedPoint );
                            _multiPoint->setHighlightFlagAtPoint( _currentSelectedPoint, true );
                        }
                        else
                        {
                            _selectedPointList.erase( pos );
                            _multiPoint->setHighlightFlagAtPoint( _currentSelectedPoint, false );
                        }

                        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILineUIHandler::LineUpdatedMessageType);
                        notifyObservers(*ILineUIHandler::LineUpdatedMessageType, *msg);
                    }
                }
                break;
                
                case LINE_MODE_NO_OPERATION:
                {
                    _selectedPointList.clear();
                    _reset();
                } 
                
            }
            
        }
    }

    void LineUIHandler::_createEditPointUndoTransaction(unsigned int index)
    {
        if(!_line.valid())
            return;

        if(index>=_line->getPointNumber())
            return;

        osg::Vec3d longLatAlt = _line->getPoint(index)->getValue();
        osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
            osg::Vec3d(longLatAlt.y(), longLatAlt.x(), std::abs(longLatAlt.z())));

        APP::IUndoTransactionFactory* transactionFactory = 
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> macroTransaction = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MacroTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction1 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::LineEditPointTransactionType);

        CORE::RefPtr<APP::IUndoTransaction> transaction2 =
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MultiPointEditPointTransactionType);

        CORE::RefPtr<TRANSACTION::ILineEditPointTransaction> lineEditPointTransaction = 
            transaction1->getInterface<TRANSACTION::ILineEditPointTransaction>();

        lineEditPointTransaction->setLine(_line.get());
        lineEditPointTransaction->setIndex(index);
        lineEditPointTransaction->setPosition(longLatAlt);

        CORE::RefPtr<TRANSACTION::IMultiPointEditPointTransaction> multiPointEditPointTransaction = 
            transaction2->getInterface<TRANSACTION::IMultiPointEditPointTransaction>();

        multiPointEditPointTransaction->setMultiPoint(_multiPoint.get());
        multiPointEditPointTransaction->setIndex(index);
        multiPointEditPointTransaction->setPosition(point);

        macroTransaction->addChild(transaction1.get());
        macroTransaction->addChild(transaction2.get());

        addAndExecuteUndoTransaction(macroTransaction.get());

    }

    void LineUIHandler::addLineToDefaultLayer()
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateLineLayer();
        _addLineToLayer(layer.get());
    }

    void LineUIHandler::addToLayer(CORE::IFeatureLayer *featureLayer)
    {
        _addLineToLayer(featureLayer);
    }

    void LineUIHandler::_addLineToLayer(CORE::IFeatureLayer *featureLayer)
    {
        if(!featureLayer)
            return;

        if(featureLayer->getFeatureLayerType()!=CORE::IFeatureLayer::LINE)
            return; //Not adding to wrong layer

        if(!_line.valid())
            return; //Object is not valid

        try
        {
            CORE::RefPtr<CORE::ICompositeObject> composite = featureLayer->getInterface<CORE::ICompositeObject>(true);

            CORE::RefPtr<CORE::IBase> lineObject = _line->getInterface<CORE::IBase>(true);
            getWorldInstance()->removeObjectByID(&lineObject->getUniqueID());

            CORE::RefPtr<CORE::ITemporary> temp = _line->getInterface<CORE::ITemporary>();
            if(temp.valid())
            {
                temp->setTemporary(false);
            }

            CORE::RefPtr<CORE::IText> text = _line->getInterface<CORE::IText>();
            if(text.valid())
            {
                text->setTextActive(true);
            }

            // create a transaction to add the line to the layer
            APP::IUndoTransactionFactory* transactionFactory = 
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction = 
                transactionFactory->createUndoTransaction(*TRANSACTION::
                TransactionRegistryPlugin::CompositeAddObjectTransactionType);

            CORE::RefPtr<TRANSACTION::ICompositeAddObjectTransaction> compositeAddObjectTransaction = 
                transaction->getInterface<TRANSACTION::ICompositeAddObjectTransaction>();

            compositeAddObjectTransaction->setCompositeObject(composite.get());
            compositeAddObjectTransaction->setObject(_line->getInterface<CORE::IObject>());

            CORE::RefPtr<CORE::IPenStyle> linePen = _line->getInterface<CORE::IPenStyle>();
            if(linePen.valid())
            {
                linePen->setPenState(CORE::IPenStyle::PenStyleState(CORE::IPenStyle::PROTECTED | CORE::IPenStyle::ON));
            }
            jsonFile = UTIL::StyleFileUtil::getObjectFolder() + "/" + _line->getInterface<CORE::IBase>()->getUniqueID().toString() + ".json";
            if (!osgDB::fileExists(jsonFile))
            {
                osgDB::copyFile(UTIL::StyleFileUtil::getDefaultLineStyle(), jsonFile);

            }
            CORE::RefPtr<SYMBOLOGY::IStyle> styleFile = DB::StyleSheetParser::readStyle(jsonFile);
            ELEMENTS::FeatureObject* featureNodeHolder = dynamic_cast<ELEMENTS::FeatureObject*>(_line->getInterface<CORE::IObject>());
            if (featureNodeHolder != nullptr)
            {
                featureNodeHolder->setVizPlaceNodeStyle(styleFile);
                featureNodeHolder->setBillboard(true);
            }
            // execute the transaction 
            addAndExecuteUndoTransaction(transaction.get());
        }
        catch(UTIL::Exception &e)
        {
            e.LogException();
        }
    }

    void LineUIHandler::_handleMouseClickedIntersection(int button, const osg::Vec3d& longLatAlt)
    {
        if(!_processMouseEvents)
        {
            return;
        }

        if(!(button & VizUI::IMouse::LEFT_BUTTON) || (LINE_MODE_NONE == _mode))
        {
            return;
        }

        switch(_mode)
        {
        case LINE_MODE_CREATE_LINE:
            {

                if(!_line.valid())
                {
                    // first point is to be added 
                    createLineFeatureObject();
                    _resetMultiPoints();
                }

                double offset = 0.0f;
                CORE::RefPtr<ELEMENTS::IDrawTechnique> draw =
                    _line->getInterface<ELEMENTS::IDrawTechnique>();

                if(draw.valid())
                {
                    offset = draw->getHeightOffset();
                }

                osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                    osg::Vec3d(longLatAlt.y(), longLatAlt.x(), std::abs(longLatAlt.z() + offset)));
                _currentSelectedPoint = _multiPoint->findNearestCntrlPoint(point);

                if( _currentSelectedPoint == -1)
                {
                    CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                        APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
                    if(tph.valid())
                    {
                        CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                        if(observable)
                        {
                            observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                        }
                    }
                    _addPoint(longLatAlt);
                }
                else
                {
                    CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                        APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
                    if(tph.valid())
                    {
                        CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                        if(observable)
                        {
                            observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                        }
                    }
                }
            }
            break;
        case LINE_MODE_EDIT:
            {
                if(_line.valid())
                {
                    double offset = 0.0f;
                    CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = 
                        _line->getInterface<ELEMENTS::IDrawTechnique>();
                    if(draw.valid())
                    {
                        offset = draw->getHeightOffset();
                    }

                    osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                        osg::Vec3d(longLatAlt.y(), longLatAlt.x(), std::abs(longLatAlt.z()) + offset));
                    _currentSelectedPoint = _multiPoint->findNearestCntrlPoint(point);

                    if( _currentSelectedPoint > -1)
                    {
                        CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
                        if(tph.valid())
                        {
                            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                            if(observable)
                            {
                                observable->registerObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                            }
                        }

                        _createEditPointUndoTransaction( _currentSelectedPoint );
                    }
                    else 
                    {
                        CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
                        if(tph.valid())
                        {
                            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
                            if(observable)
                            {
                                observable->unregisterObserver(this, *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                            }
                        }
                        _currentSelectedPoint = -1;

                        if( _selectedPointList.size() == 1 )
                        {
                            int numOfPoints = getCurrentLine()->getPointNumber();
                            int currentpoint = _selectedPointList[0];
                            if ( currentpoint == 0 || currentpoint == numOfPoints-1 )
                            {
                                _selectedPointList.erase( _selectedPointList.begin() );
                                _multiPoint->setHighlightFlagAtPoint( currentpoint, false );

                                if ( currentpoint == 0 )
                                {
                                    _insertPoint( 0, longLatAlt );
                                    _selectedPointList.push_back( 0 );
                                    _multiPoint->setHighlightFlagAtPoint( 0, true );
                                }
                                else
                                {
                                    _addPoint( longLatAlt );
                                    _selectedPointList.push_back( numOfPoints );
                                    _multiPoint->setHighlightFlagAtPoint( numOfPoints, true );
                                }

                                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILineUIHandler::LineUpdatedMessageType);
                                notifyObservers(*ILineUIHandler::LineUpdatedMessageType, *msg);
                            }
                        }
                        else if ( _selectedPointList.size() == 2 )
                        {
                            if ( (_selectedPointList[0]+1 == _selectedPointList[1]) ||
                                (_selectedPointList[0]-1 == _selectedPointList[1]) )
                            {
                                int first = -1;

                                if ( _selectedPointList[0] < _selectedPointList[1] )
                                {
                                    first = _selectedPointList[0];
                                }
                                else
                                {
                                    first = _selectedPointList[1];
                                }
                                _insertPoint( first+1, longLatAlt);
                                _multiPoint->setHighlightFlagAtPoint( first+1, true );
                                _selectedPointList.push_back( first+2 );

                                osg::Vec3 point0 = _line->getValueAt( first );
                                point0 = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                    osg::Vec3d(point0.y(), point0.x(), point0.z()));

                                osg::Vec3 point1 = _line->getValueAt( first+2 );
                                point1 = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                    osg::Vec3d(point1.y(), point1.x(), point1.z()));

                                osg::Vec3 pointm = _line->getValueAt( first+1 );
                                pointm = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                                    osg::Vec3d(pointm.y(), pointm.x(), pointm.z()));

                                double d0 = (point0 - pointm).length();
                                double d1 = (point1 - pointm).length();

                                if( d0 < d1 )
                                {
                                    _multiPoint->setHighlightFlagAtPoint( first, false);
                                    std::vector<int>::iterator pos = find( _selectedPointList.begin(),
                                        _selectedPointList.end(), first );
                                    _selectedPointList.erase( pos );
                                }
                                else
                                {
                                    _multiPoint->setHighlightFlagAtPoint( first+2, false);
                                    std::vector<int>::iterator pos = find( _selectedPointList.begin(),
                                        _selectedPointList.end(), first+2 );
                                    _selectedPointList.erase( pos );
                                }

                                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILineUIHandler::LineUpdatedMessageType);
                                notifyObservers(*ILineUIHandler::LineUpdatedMessageType, *msg);
                            }
                        }
                    }
                }
                break;
            }

        default:
            return;
        }
    }

    void LineUIHandler::setOffset(double offset)
    {
        _offset = offset;
    }

    double LineUIHandler::getOffset() const
    {
        return _offset;
    }

    void LineUIHandler::initializeAttributes()
    {
        VizUI::UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "LineUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("Offset", "Offset",
            CORE::DoubleAttribute::SetFuncType(this, &LineUIHandler::setOffset),
            CORE::DoubleAttribute::GetFuncType(this, &LineUIHandler::getOffset),
            "Offset name",
            groupName));
    }

    void LineUIHandler::setSelectedLineAsCurrentLine()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            _line = iter->second->getInterface<CORE::ILine>();
        }
    }

    void LineUIHandler::setMetadataRecord(CORE::IMetadataRecord* record)
    {
        if(_line.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _line->getInterface<CORE::IMetadataRecordHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataRecord> oldrecord = holder->getMetadataRecord();
                if(!oldrecord.valid())
                {
                    holder->setMetadataRecord(record);
                }
            }
        }
    }

    CORE::RefPtr<CORE::IMetadataRecord> 
        LineUIHandler::getMetadataRecord() const
    {
        if(_line.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _line->getInterface<CORE::IMetadataRecordHolder>();

            if(holder.valid())
            {
                return holder->getMetadataRecord();
            }
        }
        return NULL;
    }

    void LineUIHandler::update()
    {
        if(_line.valid())
        {
            CORE::RefPtr<CORE::IFeatureObject> feature = 
                _line->getInterface<CORE::IFeatureObject>();
            if(feature.valid())
            {
                feature->update();
            }
        }
    }

    void LineUIHandler::getDefaultLayerAttributes(std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecesions) const
    {
        attrNames.push_back("NAME");
        attrTypes.push_back("Text");
        attrWidths.push_back(128);
        attrPrecesions.push_back(1);

#if 0      //These are now handled through OGRFeature StyleString       
        attrNames.push_back("ColorR");
        attrTypes.push_back("Integer");
        attrWidths.push_back(1);
        attrPrecesions.push_back(1);

        attrNames.push_back("ColorG");
        attrTypes.push_back("Integer");
        attrWidths.push_back(1);
        attrPrecesions.push_back(1);

        attrNames.push_back("ColorB");
        attrTypes.push_back("Integer");
        attrWidths.push_back(1);
        attrPrecesions.push_back(1);

        attrNames.push_back("ColorA");
        attrTypes.push_back("Integer");
        attrWidths.push_back(1);
        attrPrecesions.push_back(1);

        attrNames.push_back("LineWidth");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecesions.push_back(2);

        attrNames.push_back("_VIZ_PenState");
        attrTypes.push_back("Integer");
        attrWidths.push_back(1);
        attrPrecesions.push_back(1);

        attrNames.push_back("_VIZ_TextVisibility");
        attrTypes.push_back("Integer");
        attrWidths.push_back(1);
        attrPrecesions.push_back(0);

        attrNames.push_back("_VIZ_GUID");
        attrTypes.push_back("Text");
        attrWidths.push_back(128);
        attrPrecesions.push_back(0);
#endif

    }

    CORE::RefPtr<CORE::IFeatureLayer> 
        LineUIHandler::getOrCreateLineLayer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();
        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return NULL;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("LayerComponent");

        if(!component.valid())
        {
            LOG_ERROR("Layer Component is not found");
            return NULL;
        }

        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
            component->getInterface<SMCElements::ILayerComponent>();

        if(!layerComponent.valid())
        {
            return NULL;
        }
        CORE::RefPtr<CORE::IFeatureLayer> layer = layerComponent->getFeatureLayer("Line");

        if(!layer.valid())
        {
            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecesions;

            getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecesions);

            layer = layerComponent->getOrCreateFeatureLayer("Line", "Line",attrNames, attrTypes, attrWidths, attrPrecesions);
        }

        return layer.get();
    }

    void LineUIHandler::addLineToCurrentSelectedLayer()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            CORE::RefPtr<CORE::IFeatureLayer> layer = 
                iter->second->getInterface<CORE::IFeatureLayer>();
            if(layer.valid())
            {
                _addLineToLayer(layer.get());
            }
        }
    }

    CORE::RefPtr<CORE::IMetadataTableDefn> 
        LineUIHandler::getCurrentSelectedLayerDefn()
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateLineLayer();

        //XXX - using the first element in the selection
        if(layer.valid())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                return holder->getMetadataTable()->getMetadataTableDefn();
            }
        }
        return NULL;
    }

    void LineUIHandler::removeCreatedLine()
    {
        if(_line.valid())
        {
            getWorldInstance()->removeObjectByID(&(_line->getInterface<CORE::IBase>()->getUniqueID()));
            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
            _line = NULL;
        }
    }

    void LineUIHandler::deleteLastPointFromLine()
    {
        if(_line.valid() && (_line->getPointNumber() > 0))
        {
            APP::IUndoTransactionFactory* transactionFactory = 
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction = 
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::LineRemoveLastPointTransactionType);

            CORE::RefPtr<TRANSACTION::ILineRemoveLastPointTransaction> lineRemoveLastPointTransaction = 
                transaction->getInterface<TRANSACTION::ILineRemoveLastPointTransaction>();

            lineRemoveLastPointTransaction->setLine(_line.get());

            addAndExecuteUndoTransaction(transaction.get());
        }
    }

    void LineUIHandler::setTemporaryState(bool state)
    {
        _temporary = state;
    }

    bool LineUIHandler::getTemporaryState() const
    {
        return _temporary;
    }

    void LineUIHandler::setClampingState(bool state)
    {
        _clamping = state;
    }

    bool LineUIHandler::getClampingState() const
    {
        return _clamping;
    }

    void LineUIHandler::_reset()
    {
        setMode(LINE_MODE_NONE);

        _line = NULL;

        _temporary = false;
        _clamping = true;
        _processMouseEvents = false;
    }

    void LineUIHandler::setCurrentLine(CORE::ILine* line)
    {
        _line = line;
    }

    void LineUIHandler::_deleteCurrentSelectedLine()
    {
        if(_line.valid())
        {
            APP::IUndoTransactionFactory* transactionFactory = 
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction = 
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::DeleteObjectTransactionType);

            CORE::RefPtr<TRANSACTION::IDeleteObjectTransaction> deleteObjectTransaction = 
                transaction->getInterface<TRANSACTION::IDeleteObjectTransaction>();

            deleteObjectTransaction->setObject(_line->getInterface<CORE::IDeletable>());

            addAndExecuteUndoTransaction(transaction.get());

            if(_multiPoint.valid())
            {
                getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(_multiPoint->getOSGGroupNode());
                _multiPoint->clearPoints();
                _multiPoint->showCntrlPoints(false);
            }
        }

        /*_line = NULL;*/
    }
} // namespace SMCUI

