#include <SMCUI/ColorElevationUIHandler.h>

#include <Terrain/IRasterObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <Core/ITerrain.h>

#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>

#include <Elements/IColorMap.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <Core/IPoint.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, ColorElevationUIHandler);

    DEFINE_IREFERENCED(ColorElevationUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    ColorElevationUIHandler::ColorElevationUIHandler()
    {
        _addInterface(IColorElevationUIHandler::getInterfaceName());
        setName(getClassname());
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _extents = new osg::Vec2Array;
        CORE::IComponent* comp = (CORE::WorldMaintainer::instance())->getComponentByName("ColorMapComponent");
        if (comp)
        {
            _colorMapComponent = comp->getInterface<ELEMENTS::IColorMapComponent>();
        }
        _reset();
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    ColorElevationUIHandler::~ColorElevationUIHandler(){}

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ColorElevationUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void ColorElevationUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }
    void ColorElevationUIHandler::setColorMapType(ELEMENTS::IColorMap::COLORMAP_TYPE colorMapType)
    {
        _colorMapType = colorMapType;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ColorElevationUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool ColorElevationUIHandler::getFocus() const
    {
        return false;
    }
    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ColorElevationUIHandler::execute()
    {
        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(vfactory)
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if(world)
            {
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::ColorCodedElevFilterVisitorType);

                if(!_visitor)
                {
                    LOG_ERROR("Failed to create filter class");
                    return;
                }

                GISCOMPUTE::IColorCodedElevFilterVisitor* iVisitor  = 
                    _visitor->getInterface<GISCOMPUTE::IColorCodedElevFilterVisitor>();

                if(_extents->size() >1)
                {
                    //set extent on visitor
                    iVisitor->setExtents(_extents.get());
                }
                if (_minPoint.valid() && _maxPoint.valid())
                {
                   // _populateColorMap(_minPoint.z(), _maxPoint.z());
                }
                   // set color map Type
                iVisitor->setColorMap(_colorMap.get());

                // set computation type
              //  iVisitor->setComputationType(_computeType);

                // set output name
                iVisitor->setOutputName(_name);

                CORE::IBase* applyNode = NULL;
                if(!_elevObj)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    applyNode = terrain->getInterface<CORE::IBase>();
                }
                else
                {
                    applyNode = _elevObj->getInterface<CORE::IBase>();
                }

                //subscribe for filter status
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                applyNode->accept(*_visitor);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ColorElevationUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                
                CORE::RefPtr<CORE::IBase> base = message.getSender();
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::IColorCodedElevFilterVisitor> visitor = 
                        base->getInterface<GISCOMPUTE::IColorCodedElevFilterVisitor>();

                    CORE::RefPtr<GISCOMPUTE::IMinMaxElevCalcVisitor> minmaxVisitor = base->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>();
                    if (minmaxVisitor.valid())
                    {
                        
                        _minPoint = minmaxVisitor->getMinElevOutput()->getValue();
                        _maxPoint = minmaxVisitor->getMaxElevOutput()->getValue();
                       
                    }
                    else if(visitor.valid())
                    {
                        CORE::RefPtr<TERRAIN::IRasterObject> object = visitor->getOutput();

                        if(object)
                        {
                            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                            if(world)
                            {
                                world->addObject(object->getInterface<CORE::IObject>(true));
                                /*CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                                terrain->addRasterObject(object);*/
                            }
                        }
                        else
                        {
                            throw UTIL::Exception("Failed to create raster object", __FILE__, __LINE__);
                        }
                    }
                }
            }
            catch(...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void ColorElevationUIHandler::_reset()
    {
        _extents->clear();
        _elevObj = NULL;
        _visitor = NULL;
        _name = "";
        _mapSize = 100;
        _colorMapType = ELEMENTS::IColorMap::RGB_TYPE;
        _colorMap = NULL;
        _minPoint = osg::Vec3d(0.0,0.0,1.0); //Default value Z=1;
        _maxPoint = osg::Vec3d(0.0, 0.0, 100000.0); //need to discuss
    }

    void ColorElevationUIHandler::setExtents(osg::Vec4d extents)
    {
        _extents->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _extents->push_back(osg::Vec2d(first.x(), first.y()));
        _extents->push_back(osg::Vec2d(second.x(), second.y()));
    }

    osg::Vec4d ColorElevationUIHandler::getExtents() const
    {
        osg::Vec4d extents(0.0, 0.0, 0.0, 0.0);
        osg::Vec2d first = _extents->at(0);
        osg::Vec2d second = _extents->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    void ColorElevationUIHandler::setColorMap(ELEMENTS::IColorMap* colorMap)
    {
        _colorMap = colorMap;
    }

    void ColorElevationUIHandler::setMapSize(int mapSize)
    {
        _mapSize = mapSize;
    }


    void ColorElevationUIHandler::setElevationObject(TERRAIN::IElevationObject* obj)
    {
        _elevObj = obj;
    }

    void ColorElevationUIHandler::setOutputName(std::string& name)
    {
        _name = name;
    }

    void ColorElevationUIHandler::stopFilter()
    {
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }
    void ColorElevationUIHandler::computeMinMaxElevation()
    {
        if (_extents->size() < 2)
        {
            return;
        }

        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if (vfactory)
        {
            CORE::RefPtr<CORE::IWorld> world = &(*(CORE::WorldMaintainer::instance()->getWorldMap().begin())->second);
            if (world)
            {
                _minmaxVisitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::MinMaxElevCalcVisitorType);

                GISCOMPUTE::IMinMaxElevCalcVisitor* iVisitor =
                    _minmaxVisitor->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>();

                iVisitor->setExtents(*_extents);

                _minmaxVisitor->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>()->setMultiThreaded(true);

                CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                CORE::IBase* terrainBase = terrain->getInterface<CORE::IBase>();
                _subscribe(_minmaxVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                terrainBase->accept(*_minmaxVisitor.get());
            }
        }
    }
    void
        ColorElevationUIHandler::_populateColorMap(float minSize, float maxSize)
    {

        int alphaValue = 255;
        _colorMap = _colorMapComponent->createColorMap();
        ELEMENTS::IColorMap::RangeColorMap& map = _colorMap->getMap();
        float colorstep = 2.0f / (float)_mapSize;
        float step = (maxSize - minSize) / (float)_mapSize;
        unsigned int j;
        for (j = 0; j < _mapSize / 2; ++j)
        {
            float colorval = colorstep * j;
            map[osg::Vec2(minSize + j * step, minSize + (j + 1) * step)] =
                osg::Vec4(colorval, 1, 0, (float)alphaValue / 255.0);
        }
        for (; j < _mapSize; ++j)
        {
            float colorval = colorstep * (j - _mapSize / 2);
            map[osg::Vec2(minSize + j * step, minSize + (j + 1) * step)] =
                osg::Vec4(1, 1 - colorval, 0, (float)alphaValue / 255.0);
        }

    }

} // namespace SMCUI

