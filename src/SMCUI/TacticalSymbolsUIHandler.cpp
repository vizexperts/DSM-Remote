
#include <SMCUI/TacticalSymbolsUIHandler.h>
#include <SMCUI/SMCUIPlugin.h>

#include <Core/IFeatureObject.h>
#include <Core/InterfaceUtils.h>
#include <Core/ISelectionComponent.h>
#include <Core/IFeatureLayer.h>
#include <Core/ICompositeObject.h>
#include <Core/ITemporary.h>
#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>

#include <Elements/MultiPoint.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/ElementsUtils.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/ICameraUIHandler.h>

#include <App/ApplicationRegistry.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Util/CoordinateConversionUtils.h>

#include <SMCElements/LayerComponent.h>

#include <TacticalSymbols/TacticalSymbolsPlugin.h>

#include <osgDB/FileNameUtils>

#include <sstream>

#include <osg/PolygonMode>
#include <osg/LineWidth>

namespace SMCUI
{

    DEFINE_META_BASE(SMCUI, TacticalSymbolsUIHandler);
    DEFINE_IREFERENCED(TacticalSymbolsUIHandler, VizUI::UIHandler);

    osg::Node* SelectionDecoration::getOSGNode()
    {
        return _group;
    }

    void SelectionDecoration::setSelectable(CORE::RefPtr<CORE::ISelectable> selectable)
    {
        _selection = selectable;
    }
    
    void SelectionDecoration::initialize()
    {
        if (!_selection.valid())
            return;

        if (!_group.valid())
        {
            _group = new osg::Group;
            CORE::WorldMaintainer::instance()->getOSGNode()->asGroup()->addChild(_group);
        }

        CORE::RefPtr<TACTICALSYMBOLS::ITacticalSymbol> tacticalSymbols = 
            _selection->getInterface<TACTICALSYMBOLS::ITacticalSymbol>();

        const TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds = tacticalSymbols->getBounds();            

        CORE::RefPtr<ELEMENTS::IDrawTechnique> drawTechnique =
            _selection->getInterface<ELEMENTS::IDrawTechnique>();

        osg::Vec2d topMiddle = (bounds.topLeft + bounds.topRight) / 2;
        osg::Vec2d rightMiddle = (bounds.bottomRight + bounds.topRight) / 2;
        osg::Vec2d bottomMiddle = (bounds.bottomLeft+ bounds.bottomRight) / 2;
        osg::Vec2d leftMiddle = (bounds.topLeft + bounds.bottomLeft) / 2;

        double topLeftAlt = ELEMENTS::GetAltitudeAtLongLat(bounds.topLeft.y(), bounds.topLeft.x());
        double topMiddleAlt = ELEMENTS::GetAltitudeAtLongLat(topMiddle.y(), topMiddle.x());
        double topRightAlt = ELEMENTS::GetAltitudeAtLongLat(bounds.topRight.y(), bounds.topRight.x());
        double rightMiddleAlt = ELEMENTS::GetAltitudeAtLongLat(rightMiddle.y(), rightMiddle.x());
        double bottomRightAlt = ELEMENTS::GetAltitudeAtLongLat(bounds.bottomRight.y(), bounds.bottomRight.x());
        double bottomMiddleAlt = ELEMENTS::GetAltitudeAtLongLat(bottomMiddle.y(), bottomMiddle.x());
        double bottomLeftAlt = ELEMENTS::GetAltitudeAtLongLat(bounds.bottomLeft.y(), bounds.bottomLeft.x());
        double leftMiddleAlt = ELEMENTS::GetAltitudeAtLongLat(leftMiddle.y(), leftMiddle.x());

        osg::Vec3d topLeftECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(bounds.topLeft, topLeftAlt));
        osg::Vec3d topMiddleECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(topMiddle, topMiddleAlt));
        osg::Vec3d topRightECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(bounds.topRight, topRightAlt));
        osg::Vec3d rightMiddleECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(rightMiddle, rightMiddleAlt));
        osg::Vec3d bottomRightECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(bounds.bottomRight, bottomRightAlt));
        osg::Vec3d bottomMiddleECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(bottomMiddle, bottomMiddleAlt));
        osg::Vec3d bottomLeftECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(bounds.bottomLeft, bottomLeftAlt));
        osg::Vec3d leftMiddleECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(leftMiddle, leftMiddleAlt));

        osg::Vec2d xDirGeodetic = bounds.topRight - bounds.topLeft;
        osg::Vec2d yDirGeodetic = bounds.bottomLeft - bounds.topLeft;

        osg::Vec3 xDirECEF = topRightECEF - topLeftECEF;
        osg::Vec3 yDirECEF = bottomLeftECEF - topLeftECEF;
        //double xSize = xDir.normalize();
        //double ySize = yDir.normalize();

        //! Boundary draggers
        //_currentSelectedPoint = -1;
        if (!_boundaryControlPoints.valid())
        {
            _boundaryControlPoints = new ELEMENTS::MultiPoint;
            _group->addChild(_boundaryControlPoints->getOSGGroupNode());
        }

        _boundaryControlPoints->clearPoints();
        _boundaryControlPoints->showCntrlPoints(true);

        _boundaryControlPoints->add(topLeftECEF);
        _boundaryControlPoints->add(topMiddleECEF);
        _boundaryControlPoints->add(topRightECEF);
        _boundaryControlPoints->add(rightMiddleECEF);
        _boundaryControlPoints->add(bottomRightECEF);
        _boundaryControlPoints->add(bottomMiddleECEF);
        _boundaryControlPoints->add(bottomLeftECEF);
        _boundaryControlPoints->add(leftMiddleECEF);

#if 0
        //! Decoration geode
        if (!_pat.valid())
        {
            _pat = new osg::PositionAttitudeTransform;
            _group->addChild(_pat);
        }

        if (!_geode.valid())
        {
            _geode = new osg::Geode;
            _pat->addChild(_geode);
        }

        //! Boundary quad
        if (!_boundaryQuad.valid())
        {
            _boundaryQuad = new osg::Geometry;
            _geode->addDrawable(_boundaryQuad.get());
            osg::ref_ptr<osg::Vec3Array> quadVertices = new osg::Vec3Array(4);
            (*quadVertices)[0] = topLeftECEF;
            (*quadVertices)[1] = topRightECEF;
            (*quadVertices)[2] = bottomRightECEF;
            (*quadVertices)[3] = bottomLeftECEF;
            //(*quadVertices)[0] = osg::Vec3(0, 0, 0);
            //(*quadVertices)[1] = osg::Vec3(1, 0, 0);
            //(*quadVertices)[2] = osg::Vec3(1, 1, 0);
            //(*quadVertices)[3] = osg::Vec3(0, 1, 0);
            _boundaryQuad->setVertexArray(quadVertices.get());

            osg::Vec4Array* colors = new osg::Vec4Array(1);
            (*colors)[0].set(1.0f, 1.0f, 1.0f, 1.0f);
            _boundaryQuad->setColorArray(colors, osg::Array::BIND_OVERALL);

            osg::Vec3 normal = topLeftECEF;
            normal.normalize();

            osg::Vec3Array* normals = new osg::Vec3Array(1);
            (*normals)[0] = normal;
            _boundaryQuad->setNormalArray(normals, osg::Array::BIND_OVERALL);

            _boundaryQuad->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4));

            osg::PolygonMode* polymode = new osg::PolygonMode;
            polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE);

            //_geode->getOrCreateStateSet()->setAttributeAndModes(polymode, osg::StateAttribute::OVERRIDE
            //    | osg::StateAttribute::ON);

            _geode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
            _geode->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
            _geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);

            _geode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(2.0f), osg::StateAttribute::ON);
        }

        //! Rotation Axis
        if (!_rotationAxis.valid())
        {
            _rotationAxis = new osg::Geometry;
            _geode->addDrawable(_rotationAxis.get());

            osg::ref_ptr<osg::Vec3Array> quadVertices = new osg::Vec3Array(2);
            (*quadVertices)[0] = (topLeftECEF + topRightECEF) / 2 - yDir*0.2;
            (*quadVertices)[1] = (bottomLeftECEF + bottomRightECEF) / 2 + yDir*0.2;
            //(*quadVertices)[0] = osg::Vec3(0, 0, 0);
            //(*quadVertices)[1] = osg::Vec3(1, 0, 0);
            _rotationAxis->setVertexArray(quadVertices.get());

            osg::Vec4Array* colors = new osg::Vec4Array(1);
            (*colors)[0].set(1.0f, 1.0f, 1.0f, 1.0f);
            _rotationAxis->setColorArray(colors, osg::Array::BIND_OVERALL);

            //osg::Vec3 normal = topLeftECEF;
            //normal.normalize();

            //osg::Vec3Array* normals = new osg::Vec3Array(1);
            //(*normals)[0] = normal;
            //_boundaryQuad->setNormalArray(normals, osg::Array::BIND_OVERALL);

            _rotationAxis->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 0, 2));

            //osg::PolygonMode* polymode = new osg::PolygonMode;
            //polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE);

            //_geode->getOrCreateStateSet()->setAttributeAndModes(polymode, osg::StateAttribute::OVERRIDE
            //    | osg::StateAttribute::ON);

            _geode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
            _geode->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
            _geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);

            _geode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(2.0f), osg::StateAttribute::ON);

        }
#endif 

        //! Rotation Dragger
        if (!_rotationPoint.valid())
        {
            _rotationPoint = new ELEMENTS::MultiPoint;
            _rotationPoint->setPointColor(osg::Vec4d(0, 1, 0, 1));

            _group->addChild(_rotationPoint->getOSGGroupNode());
        }

        _rotationPoint->clearPoints();
        _rotationPoint->showCntrlPoints(true);

        osg::Vec2d rotationLatlong = (bounds.topLeft + bounds.topRight) / 2 - yDirGeodetic*0.1;
        double rotationAlt = ELEMENTS::GetAltitudeAtLongLat(rotationLatlong.y(), rotationLatlong.x());

        osg::Vec3d rotationECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(rotationLatlong, rotationAlt));

        _rotationPoint->add(rotationECEF);

        //! Translation Dragger
        if (!_translationPoint.valid())
        {
            _translationPoint = new ELEMENTS::MultiPoint;
            _translationPoint->setPointColor(osg::Vec4d(0, 0, 1, 1));

            _group->addChild(_translationPoint->getOSGGroupNode());
        }

        _translationPoint->clearPoints();
        _translationPoint->showCntrlPoints(true);

        osg::Vec2d translationLatlong = (bounds.topLeft + bounds.topRight + bounds.bottomLeft + bounds.bottomRight) / 4;
        double translationAlt = ELEMENTS::GetAltitudeAtLongLat(translationLatlong.y(), translationLatlong.x());

        osg::Vec3d translationECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(translationLatlong, translationAlt));

        _translationPoint->add(translationECEF);

    }

    void SelectionDecoration::moveCurrentPointTo(osg::Vec3 longLatAlt)
    {
        if (!_selection.valid())
        {
            return;
        }

        if (_currentSelectedPoint == -1)
            return;


        CORE::RefPtr<TACTICALSYMBOLS::ITacticalSymbol> tacticalSymbols =
            _selection->getInterface<TACTICALSYMBOLS::ITacticalSymbol>();

        TACTICALSYMBOLS::ITacticalSymbol::Bounds newBounds = tacticalSymbols->getBounds();

        if (_currentSelectedPoint < _boundaryControlPoints->getNumPoints())
        { 
            resizeBounds(newBounds, longLatAlt);
        }
        else if ((_currentSelectedPoint - _boundaryControlPoints->getNumPoints()) < _rotationPoint->getNumPoints())
        {
            doRotate(newBounds, longLatAlt);
        }
        else
        {
            moveBounds(newBounds, longLatAlt);
        }

        tacticalSymbols->setBounds(newBounds);

        tacticalSymbols->getInterface<CORE::IFeatureObject>()->update();

        initialize();

        //_currentSelectedPoint = index;

        _boundaryControlPoints->setHighlightFlagAtPoint(_currentSelectedPoint, true);
    }

    void SelectionDecoration::moveBounds(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3 longLatAlt)
    {
        osg::Vec2d newPoint(longLatAlt.y(), longLatAlt.x());
        osg::Vec2d oldPoint = (bounds.topLeft + bounds.topRight + bounds.bottomLeft + bounds.bottomRight) / 4;
        osg::Vec2 top = bounds.topRight - bounds.topLeft;
        osg::Vec2 left = bounds.bottomLeft - bounds.topLeft;
        osg::Vec2 bottom = bounds.bottomRight - bounds.bottomLeft;
        osg::Vec2 right = bounds.bottomRight - bounds.topRight;

        osg::Vec2 xDir = top;
        osg::Vec2 yDir = left;
        xDir.normalize();
        yDir.normalize();

        osg::Vec2d displacement = newPoint - oldPoint;
        double xDisplacement = displacement*xDir;
        double yDisplacement = displacement*yDir;

        bounds.topLeft += top*xDisplacement + left*yDisplacement;
        bounds.topRight += top*xDisplacement + right*yDisplacement;
        bounds.bottomRight += bottom*xDisplacement + right*yDisplacement;
        bounds.bottomLeft += bottom*xDisplacement + left*yDisplacement;
    }

    void SelectionDecoration::resizeBounds(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3 longLatAlt)
    {
        osg::Vec2d newPoint(longLatAlt.y(), longLatAlt.x());
        osg::Vec2d oldPoint;
        osg::Vec2 xDir;
        osg::Vec2 yDir;

        switch ((int)_currentSelectedPoint)
        {
        case 0:             //! Top-Left
            oldPoint = bounds.topLeft;
            xDir = bounds.topRight - bounds.topLeft;
            yDir = bounds.bottomLeft - bounds.topLeft;
            break;
        case 1:             //! Top
            oldPoint = (bounds.topLeft + bounds.topRight) / 2;
            xDir = bounds.topRight - bounds.topLeft;
            yDir = bounds.bottomLeft - bounds.topLeft;
            break;
        case 2:             //! Top-Right
            oldPoint = bounds.topRight;
            xDir = bounds.topRight - bounds.topLeft;
            yDir = bounds.bottomRight - bounds.topRight;
            break;
        case 3:             //! Right
            oldPoint = (bounds.topRight+ bounds.bottomRight) / 2;
            xDir = bounds.topRight - bounds.topLeft;
            yDir = bounds.bottomRight- bounds.topRight;
            break;
        case 4:             //! Bottom-Right
            oldPoint = bounds.bottomRight;
            xDir = bounds.bottomRight - bounds.bottomLeft;
            yDir = bounds.bottomRight - bounds.topRight;
            break;
        case 5:             //! Bottom
            oldPoint = (bounds.bottomLeft+ bounds.bottomRight) / 2;
            xDir = bounds.bottomRight - bounds.bottomLeft;
            yDir = bounds.bottomRight - bounds.topRight;
            break;
        case 6:             //! Bottom-Left
            oldPoint = bounds.bottomLeft;
            xDir = bounds.bottomRight - bounds.bottomLeft;
            yDir = bounds.bottomLeft - bounds.topLeft;
            break;
        case 7:             //! Left
            oldPoint = (bounds.bottomLeft+ bounds.topLeft) / 2;
            xDir = bounds.bottomRight - bounds.bottomLeft;
            yDir = bounds.bottomLeft - bounds.topLeft;
            break;
        default:
            break;
        }

        xDir.normalize();
        yDir.normalize();
        
        osg::Vec2d displacement = newPoint - oldPoint;

        double xDisplacement = displacement*xDir;
        double yDisplacement = displacement*yDir;
        
        switch ((int)_currentSelectedPoint)
        {
        case 0:             //! Top-Left
            bounds.topLeft = newPoint;
            bounds.bottomLeft += xDir*xDisplacement;
            bounds.topRight += yDir*yDisplacement;
            break;
        case 1:             //! Top
            bounds.topLeft += yDir*yDisplacement;
            bounds.topRight += yDir*yDisplacement;
            break;
        case 2:             //! Top-Right
            bounds.topRight = newPoint;
            bounds.bottomRight += xDir*xDisplacement;
            bounds.topLeft += yDir*yDisplacement;
            break;
        case 3:             //! Right
            bounds.bottomRight += xDir*xDisplacement;
            bounds.topRight += xDir*xDisplacement;
            break;
        case 4:             //! Bottom-Right
            bounds.bottomRight = newPoint;
            bounds.topRight += xDir*xDisplacement;
            bounds.bottomLeft += yDir*yDisplacement;
            break;
        case 5:             //! Bottom
            bounds.bottomLeft += yDir*yDisplacement;
            bounds.bottomRight += yDir*yDisplacement;
            break;
        case 6:             //! Bottom-Left
            bounds.bottomLeft = newPoint;
            bounds.topLeft += xDir*xDisplacement;
            bounds.bottomRight += yDir*yDisplacement;
            break;
        case 7:             //! Left
            bounds.topLeft += xDir*xDisplacement;
            bounds.bottomLeft += xDir*xDisplacement;
            break;
        default:
            break;
        }
    }
    
    void SelectionDecoration::doRotate(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3 longLatAlt)
    {
        osg::Vec2 yDir = bounds.bottomLeft - bounds.topLeft;
        yDir.normalize();

        osg::Vec2d center = (bounds.topLeft + bounds.topRight + bounds.bottomLeft + bounds.bottomRight) / 4;
        osg::Vec2d rotatedPoint(longLatAlt.y(), longLatAlt.x());
        osg::Vec2d rotatedVec = (center - rotatedPoint );
        rotatedVec.normalize();

        double theta = acos(yDir*rotatedVec);
        double determinant = (yDir.x()*rotatedVec.y() - yDir.y()*rotatedVec.x());
        if (determinant < 0)
        {
            theta = -theta;
        }

        LOG_DEBUG_VAR("angle: %f", theta);

        rotatePoint(bounds.topLeft, center, theta);
        rotatePoint(bounds.topRight, center, theta);
        rotatePoint(bounds.bottomLeft, center, theta);
        rotatePoint(bounds.bottomRight, center, theta);
    }

    void SelectionDecoration::rotatePoint(osg::Vec2d& point, const osg::Vec2d& center, double angle)
    {
        osg::Vec2d temp = point - center;

        point.x() = temp.x()*cos(angle) - temp.y()*sin(angle);
        point.y() = temp.x()*sin(angle) + temp.y()*cos(angle);

        point += center;
    }

    void SelectionDecoration::reset()
    {
        CORE::WorldMaintainer::instance()->getOSGNode()->asGroup()->removeChild(_group);

        _rotationPoint = nullptr;
        _translationPoint = nullptr;
        _geode = nullptr;
        _rotationAxis = nullptr;
        _boundaryQuad = nullptr;
        _boundaryControlPoints = nullptr;
        _boundaryControlPoints = nullptr;
        _currentSelectedPoint = -1;
        _group = nullptr;
        _selection = nullptr;
    }

    CORE::RefPtr<ELEMENTS::IMultiPoint> SelectionDecoration::boundaryControlPoints()
    {
        return _boundaryControlPoints.get();
    }

    CORE::RefPtr<ELEMENTS::IMultiPoint> SelectionDecoration::rotationPoint()
    {
        return _rotationPoint.get();
    }

    int SelectionDecoration::getCurrentSelectedPointIndex()
    {
        return _currentSelectedPoint;
    }

    bool SelectionDecoration::findNearestControlPoint(osg::Vec3d point)
    {
        _currentSelectedPoint = -1;
        if (_boundaryControlPoints.valid())
        {
            _currentSelectedPoint = _boundaryControlPoints->findNearestCntrlPoint(point);
        }

        if (_currentSelectedPoint != -1)
        {
            _boundaryControlPoints->setHighlightFlagAtPoint(_currentSelectedPoint, true);

            return true;
        }

        if (_rotationPoint.valid())
        {
            _currentSelectedPoint = _rotationPoint->findNearestCntrlPoint(point);
        }

        if (_currentSelectedPoint != -1)
        {
            _rotationPoint->setHighlightFlagAtPoint(_currentSelectedPoint, true);
            _currentSelectedPoint += _boundaryControlPoints->getNumPoints();

            return true;
        }

        if (_translationPoint.valid())
        {
            _currentSelectedPoint = _translationPoint->findNearestCntrlPoint(point);
        }

        if (_currentSelectedPoint != -1)
        {
            _translationPoint->setHighlightFlagAtPoint(_currentSelectedPoint, true);
            _currentSelectedPoint += _boundaryControlPoints->getNumPoints() + _rotationPoint->getNumPoints();

            return true;
        }

        return false;
    }

    TacticalSymbolsUIHandler::TacticalSymbolsUIHandler()
        : _mode(MODE_NONE)
        , _dragged(false)
        , _processMouseEvents(false)
        , _handleButtonRelease(false)
    {
        _addInterface(SMCUI::ITacticalSymbolsUIHandler::getInterfaceName());
        setName(getClassname());
    }

    void TacticalSymbolsUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizUI::UIHandler::onAddedToManager();
    }

    void TacticalSymbolsUIHandler::_resetDecoration()
    {
        if (!_tacticalSymbol.valid())
        {
            return;
        }

        _selectionDecoration.setSelectable(_tacticalSymbol->getInterface<CORE::ISelectable>());
        _selectionDecoration.initialize();

    }
    bool TacticalSymbolsUIHandler::setMode(Mode mode)
    {

        if (_mode == mode)
        {
            return true;
        }
        _mode = mode;

        // Subscribe for TerrainPick messages
        _processMouseEvents = true;

        switch (_mode)
        {
        case MODE_CREATE_TACTICALSYMBOL:
            _setCreateSymbolMode();
            break;

        case MODE_EDIT:
            _setEditMode();
            break;

        case MODE_NONE:
            _handleButtonRelease = false;
            _setModeNone();
            break;

        case MODE_NO_OPERATION:
            _setModeNoOperation();
            break;
        }

        return true;
    }

    void TacticalSymbolsUIHandler::_setModeNoOperation()
    {
        setSelectedSymbolAsCurrentSymbol();

        _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMousePressedMessageType);
        _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseReleasedMessageType);
        _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);

        _selectionDecoration.reset();
    }

    void TacticalSymbolsUIHandler::_setModeNone()
    {
        _processMouseEvents = false;
        // Query TerrainUIHandler interface

        _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMousePressedMessageType);
        _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);
        _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseReleasedMessageType);

        _tacticalSymbol = NULL;

        _selectionDecoration.reset();

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if (selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
            selectionUIHandler->setMouseClickSelectionState(true);
        }
    }

    void TacticalSymbolsUIHandler::_setEditMode()
    {
        setSelectedSymbolAsCurrentSymbol();
        _resetDecoration();

        _subscribeToMouseMessage(*VizUI::IMouseMessage::HandledMousePressedMessageType);
        _subscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseReleasedMessageType);

        _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if (selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }

        return;
    }

    void TacticalSymbolsUIHandler::setSymbolPath(const std::string& symbolPath)
    {
        _symbolFilePath = symbolPath;
    }

    void TacticalSymbolsUIHandler::_setCreateSymbolMode()
    {
        // Query TerrainUIHandler interface
        CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());
        if (tph.valid())
        {
            CORE::RefPtr<CORE::IObservable> observable = tph->getInterface<CORE::IObservable>();
            if (observable)
            {

                _subscribeToMouseMessage(*VizUI::IMouseMessage::HandledMousePressedMessageType);
                _subscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseReleasedMessageType);

                _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);
            }
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if (selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }
    }

    void TacticalSymbolsUIHandler::_setProcessMouseEvents(bool value)
    {
        _processMouseEvents = value;
    }

    bool TacticalSymbolsUIHandler::_getProcessMouseEvents() const
    {
        return _processMouseEvents;
    }

    TacticalSymbolsUIHandler::Mode TacticalSymbolsUIHandler::getMode() const
    {
        return _mode;
    }

    void TacticalSymbolsUIHandler::setAffiliation(TACTICALSYMBOLS::ITacticalSymbol::Affiliation affiliation)
    {
        _affiliation = affiliation;
    }

    TACTICALSYMBOLS::ITacticalSymbol::Affiliation TacticalSymbolsUIHandler::getAffiliation()const
    {
        return _affiliation;
    }

    void TacticalSymbolsUIHandler::setPlanStatus(TACTICALSYMBOLS::ITacticalSymbol::PlanStatus status)
    {
        _planStatus = status;
    }

    TACTICALSYMBOLS::ITacticalSymbol::PlanStatus TacticalSymbolsUIHandler::getPlanStatus()const
    {
        return _planStatus;
    }

    void TacticalSymbolsUIHandler::setSymbolName(const std::string& name)
    {
        _name = name;
        if (_tacticalSymbol.valid())
        {
            _tacticalSymbol->getInterface<CORE::IBase>()->setName(_name);
        }
    }

    std::string TacticalSymbolsUIHandler::getSymbolName() const
    {
        return _name;
    }


    CORE::IObject* TacticalSymbolsUIHandler::createFolderObject()
    {
        CORE::IObject* folderObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::FolderType);

        return folderObject;
    }

    TACTICALSYMBOLS::ITacticalSymbol* TacticalSymbolsUIHandler::createTacticalSymbolObject()
    {
        CORE::RefPtr<CORE::IObject> tactObj = CORE::CoreRegistry::instance()->getObjectFactory()->
            createObject(*TACTICALSYMBOLS::TacticalSymbolsPlugin::TacticalSymbolType);

        _tacticalSymbol = tactObj->getInterface<TACTICALSYMBOLS::ITacticalSymbol>();

        std::string filename = osgDB::getSimpleFileName(_symbolFilePath);

        _tacticalSymbol->getInterface<CORE::IBase>()->setName(_name);
        _tacticalSymbol->setUrl(_symbolFilePath);
        _tacticalSymbol->setAffiliation(_affiliation);
        _tacticalSymbol->setPlanStatus(_planStatus);

        return _tacticalSymbol.get();
    }

    TACTICALSYMBOLS::ITacticalSymbol* TacticalSymbolsUIHandler::getCurrentSymbol() const
    {
        return _tacticalSymbol.get();
    }

    void TacticalSymbolsUIHandler::setFocus(bool value)
    {
        if (!value)
        {
            _reset();
        }

        _processMouseEvents = value;

        VizUI::UIHandler::setFocus(value);
    }

    void TacticalSymbolsUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            if (wm.valid())
            {
                _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
            }

        }
        if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_NONE);
        }
        if (messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            // Read intersections from Terrain UIHandler
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tpu =
                CORE::InterfaceUtils::getFirstOf<APP::IUIHandler, VizUI::ITerrainPickUIHandler>(
                getManager()->getUIHandlerMap());
            if (tpu.valid())
            {
                // get terrain position
                osg::Vec3d pos;
                if (tpu->getMousePickedPosition(pos, true))
                {
                    // Get the IMouseMessage interface
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleMouseClickedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }
            }
            _dragged = false;
        }
        else if (messageType == *VizUI::IMouseMessage::HandledMouseDraggedMessageType)
        {
            _dragged = true;
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                CORE::InterfaceUtils::getFirstOf<APP::IUIHandler, VizUI::ITerrainPickUIHandler>(
                getManager()->getUIHandlerMap());
            if (tph.valid())
            {
                osg::Vec3d pos;
                if (tph->getMousePickedPosition(pos, true))
                {
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleMouseDragged(mmsg->getMouse()->getButtonMask(), pos);
                }
            }

        }
        else if (messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

            if (!_handleButtonRelease)
            {
                _handleButtonRelease = true;
                if (mmsg->getMouse()->getButtonMask() & (VizUI::IMouse::RIGHT_BUTTON | VizUI::IMouse::LEFT_BUTTON))
                {
                    return;
                }
            }

            if (!_dragged)
            {
                _handleMouseReleased(mmsg->getMouse()->getButtonMask());
            }
            else
            {
                CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                    CORE::InterfaceUtils::getFirstOf<APP::IUIHandler, VizUI::ITerrainPickUIHandler>(
                    getManager()->getUIHandlerMap());
                if (tph.valid())
                {
                    //CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    //CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITacticalSymbolsUIHandler::LineUpdatedMessageType);
                    //notifyObservers(*ITacticalSymbolsUIHandler::LineUpdatedMessageType, *msg);
                }

            }
            _dragged = false;
        }
        else
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }

    void TacticalSymbolsUIHandler::_handleMouseDragged(int button, const osg::Vec3d& longLatAlt)
    {
        if (!_processMouseEvents)
        {
            return;
        }

        if (!(button & VizUI::IMouse::LEFT_BUTTON) || (MODE_NONE == _mode))
        {
            return;
        }
        switch (_mode)
        {
        case MODE_CREATE_TACTICALSYMBOL:
        case MODE_EDIT:
        {
            if (_tacticalSymbol.valid())
            {
                _selectionDecoration.moveCurrentPointTo(longLatAlt);
                //_movePointAtIndex(_selectionDecoration.getCurrentSelectedPointIndex(), longLatAlt);
            }
        }
        break;
        }
    }
    void TacticalSymbolsUIHandler::_handleMouseReleased(int button)
    {
        if (!_processMouseEvents)
        {
            return;
        }

        if ((button & VizUI::IMouse::RIGHT_BUTTON))
        {
            switch (_mode)
            {
            case MODE_CREATE_TACTICALSYMBOL:
            {
                if (_tacticalSymbol.valid())
                {
                    if (_tacticalSymbol->getInterface<CORE::ISelectable>()->getSelected())
                    {
                        _tacticalSymbol->getInterface<CORE::ISelectable>()->setSelected(false);
                    }

                    _tacticalSymbol = NULL;
                }
            }
            break;
            case MODE_EDIT:
            {
                _reset();
            }
            break;
            }
        }
        else if (button & VizUI::IMouse::LEFT_BUTTON)
        {
            switch (_mode)
            {
            case MODE_EDIT:
            {
                _selectionDecoration.boundaryControlPoints()
                    ->setHighlightFlagAtPoint(_selectionDecoration.getCurrentSelectedPointIndex(), false);
            }
            break;
            }
        }
    }

    void TacticalSymbolsUIHandler::_initializeBoundsToView(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3d longLatAlt)
    {
        //! Get camera uihandler
        CORE::RefPtr<VizUI::ICameraUIHandler> camUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getManager());

        //! Get camera view point
        osg::Vec3d cameraViewpointLongLatAlt;
        double cameraRange = 0, cameraHeading = 0, cameraPitch = 0;
        camUIHandler->getViewPoint(cameraViewpointLongLatAlt, cameraRange, cameraHeading, cameraPitch);

        cameraHeading = osg::DegreesToRadians(cameraHeading);
        osg::Vec2d yDir(sin(cameraHeading), cos(cameraHeading));
        osg::Vec2d xDir(sin(cameraHeading - osg::PI_2), cos(cameraHeading - osg::PI_2));

        double extent = cameraRange*0.000002;

        osg::Vec2d latLong(longLatAlt.y(), longLatAlt.x());

        bounds.topLeft = latLong - xDir*extent - yDir*extent;
        bounds.topRight = latLong + xDir*extent - yDir*extent;
        bounds.bottomRight = latLong + xDir*extent + yDir*extent;
        bounds.bottomLeft = latLong - xDir*extent + yDir*extent;

    }

    void TacticalSymbolsUIHandler::_handleMouseClickedIntersection(int button, const osg::Vec3d& longLatAlt)
    {
        if (!_processMouseEvents)
        {
            return;
        }

        if (!(button & VizUI::IMouse::LEFT_BUTTON) || (MODE_NONE == _mode))
        {
            return;
        }

        switch (_mode)
        {
        case MODE_CREATE_TACTICALSYMBOL:
        {
            if (!_tacticalSymbol.valid())
            {
                createTacticalSymbolObject();
                //_resetDecoration();

                _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);

                //! Initialize bounds according to current view.
                TACTICALSYMBOLS::ITacticalSymbol::Bounds bounds;
                _initializeBoundsToView(bounds, longLatAlt);

                _tacticalSymbol->setBounds(bounds);

                _tacticalSymbol->getInterface<CORE::ITemporary>()->setTemporary(false);

                _resetDecoration();

                getWorldInstance()->addObject(_tacticalSymbol->getInterface<CORE::IObject>());

                //_addPoint(longLatAlt);
            }
            else
            {
                osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                    osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z() ));

                if (_selectionDecoration.findNearestControlPoint(point))
                {
                    _subscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                }

            }

        }
        break;
        case MODE_EDIT:
        {
            if (_tacticalSymbol.valid())
            {
                osg::Vec3 point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                    osg::Vec3d(longLatAlt.y(), longLatAlt.x(), longLatAlt.z() ));

                if (_selectionDecoration.findNearestControlPoint(point))
                {
                    _subscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                }
                else
                {
                    _unsubscribeToMouseMessage(*VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                }
            }
            break;
        }

        default:
            return;
        }
    }

    void TacticalSymbolsUIHandler::setSelectedSymbolAsCurrentSymbol()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if (iter != map.end())
        {
            _tacticalSymbol = iter->second->getInterface<TACTICALSYMBOLS::ITacticalSymbol>();
        }
    }

    CORE::RefPtr<CORE::IFeatureLayer>
        TacticalSymbolsUIHandler::getOrCreateSymbolLayer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer =
            CORE::WorldMaintainer::instance();
        if (!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return NULL;
        }

        CORE::RefPtr<CORE::IComponent> component =
            worldMaintainer->getComponentByName("LayerComponent");

        if (!component.valid())
        {
            LOG_ERROR("Layer Component is not found");
            return NULL;
        }

        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent =
            component->getInterface<SMCElements::ILayerComponent>();

        if (!layerComponent.valid())
        {
            return NULL;
        }
        CORE::RefPtr<CORE::IFeatureLayer> layer = layerComponent->getFeatureLayer("Line");

        if (!layer.valid())
        {
            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecesions;

            layer = layerComponent->getOrCreateFeatureLayer("Line", "Line", attrNames, attrTypes, attrWidths, attrPrecesions);
        }

        return layer.get();
    }

    void TacticalSymbolsUIHandler::_reset()
    {
        setMode(MODE_NONE);

        _tacticalSymbol = NULL;

        _processMouseEvents = false;

        _selectionDecoration.reset();
    }

    void TacticalSymbolsUIHandler::_subscribeToMouseMessage(const CORE::IMessageType& messageType)
    {
        if (!_terrainPickUIHandler.valid())
        {
            _terrainPickUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());

        }

        if (_terrainPickUIHandler.valid())
        {
            _subscribe(_terrainPickUIHandler.get(), messageType);
        }

    }

    void TacticalSymbolsUIHandler::_unsubscribeToMouseMessage(const CORE::IMessageType& messageType)
    {
        if (!_terrainPickUIHandler.valid())
        {
            _terrainPickUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());

        }

        if (_terrainPickUIHandler.valid())
        {
            _unsubscribe(_terrainPickUIHandler.get(), messageType);
        }
    }

} // namespace SMCUI

