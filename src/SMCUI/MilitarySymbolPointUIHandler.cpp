/*****************************************************************************
*
* File             : MilitarySymbolPointUIHandler.cpp
* Description      : MilitarySymbolPointUIHandler class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

//Header files
#include <SMCUI/MilitarySymbolPointUIHandler.h>
#include <SMCUI/IOverlayUIHandler.h>
#include <SMCUI/IMilitaryPathAnimationUIHandler.h>
#include <SMCUI/SMCUIPlugin.h>

#include <Core/IPoint.h>
#include <Core/Point.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITemporary.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataRecord.h>
#include <Core/IFeatureObject.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/ICompositeObject.h>
#include <Core/IWorld.h>
#include <Core/IMessage.h>
#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/IDeletable.h>
#include <Core/AttributeTypes.h>
#include <Core/InterfaceUtils.h>
#include <Core/IObjectMessage.h>
#include <Core/IWorldMaintainer.h>

#include <VizUI/IModelPickUIHandler.h>
#include <VizUI/IMouse.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/VizUIPlugin.h>
#include <VizUI/IMouseMessage.h>

#include <App/IApplication.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransaction.h>
#include <App/AccessElementUtils.h>

#include <Util/CoordinateConversionUtils.h>
#include <Util/CoordinateConversionUtils.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IClamper.h>
#include <Elements/IMilitarySymbol.h>
#include <Elements/IMilitaryRange.h>
#include <Elements/MilitarySymbolType.h>
#include <Elements/ISymbolTypeLoader.h>
#include <Elements/IOverlay.h>
#include <Elements/IRepresentation.h>
#include <Elements/IIcon.h>
#include <Elements/IDrawTechnique.h>

#include <Transaction/TransactionPlugin.h>
#include <Transaction/IMovePointTransaction.h>
#include <Transaction/IDeleteObjectTransaction.h>
#include <Transaction/ICompositeAddObjectTransaction.h>

#include <SMCElements/ILayerComponent.h>

#include <osgDB/FileNameUtils> 


namespace SMCUI 
{

    DEFINE_META_BASE(SMCUI, MilitarySymbolPointUIHandler);
    DEFINE_IREFERENCED(MilitarySymbolPointUIHandler, UIHandler);

    MilitarySymbolPointUIHandler::MilitarySymbolPointUIHandler()
        :_offset(0.0)
        ,_mode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE)
        ,_temporary(false)
        ,_dragged(false)
        ,_handleButtonRelease(false)
        ,_clampingFromConfig(false)
        ,_offsetFromConfig(false)
    {
        _addInterface(SMCUI::IMilitarySymbolPointUIHandler::getInterfaceName());
    }

    MilitarySymbolPointUIHandler::~MilitarySymbolPointUIHandler(){}


    CORE::IFeatureLayer*
        MilitarySymbolPointUIHandler::getOrCreateMilitarySymbolLayer(const std::string &layerType, bool addToWorld)
    {
        static const std::string milLayerType = "MilitaryLayerPoint";

        _layerName = layerType;

        std::string name = layerType;


        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();
        if(!worldMaintainer.valid())
        {
            return NULL;
        }

        CORE::RefPtr<CORE::IComponent> component =
            worldMaintainer->getComponentByName("LayerComponent");
        if(!component.valid())
        {
            return NULL;
        }
        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
            component->getInterface<SMCElements::ILayerComponent>();
        CORE::RefPtr<CORE::IFeatureLayer> layer = 
            layerComponent->getFeatureLayer(_layerName);

        if(!layer.valid())
        {
            CORE::RefPtr<ELEMENTS::IMilitarySymbolType> milSymbolType; /*= _unit->getMilitarySymbolType();*/

            CORE::RefPtr<ELEMENTS::ISymbolTypeLoader> symbolLoaderComp;

            CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());
            if(wmain)
            {
                CORE::IComponent* comp = wmain->getComponentByName("SymbolTypeLoader");
                if(comp)
                {
                    symbolLoaderComp = comp->getInterface<ELEMENTS::ISymbolTypeLoader>();

                    if(symbolLoaderComp->isUniformAcrossAffiliation())
                    {
                        std::vector<std::string> tokens;
                        UTIL::StringTokenizer<UTIL::IsSlash>::tokenize(tokens, name, UTIL::IsSlash());
                        if(tokens.size() < 3)
                        {
                            return NULL;
                        }
                        name = "Common/"+tokens[1]+"/"+tokens[2];
                    }

                    milSymbolType = symbolLoaderComp->getMilitarySymbolTypeByFullName(name);
                    if(!milSymbolType.valid())
                    {
                        return NULL;
                    }

                }
            }
            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecisions;

            attrNames.push_back("NAME");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(1);

            attrNames.push_back("Affiliation");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("Type");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(1);

            // symbol color
            attrNames.push_back("ColorR");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("ColorG");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("ColorB");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("ColorA");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("LineWidth");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(1);
            attrPrecisions.push_back(2);

            attrNames.push_back("Representation");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("_VIZ_AttachedPath");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(1);

            attrNames.push_back("_VIZ_CheckPointList");
            attrTypes.push_back("Text");
            attrWidths.push_back(1024);
            attrPrecisions.push_back(1);

            attrNames.push_back("_VIZ_TrailVisibility");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("_VIZ_GUID");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(0);

            attrNames.push_back("Range");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(10);
            attrPrecisions.push_back(4);

            //! XXX: To be removed
            attrNames.push_back("UserId");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(1);

            attrNames.push_back("Visibility");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(1);

            attrNames.push_back("FacingDirectionX");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(1);
            attrPrecisions.push_back(2);

            attrNames.push_back("FacingDirectionY");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(1);
            attrPrecisions.push_back(2);

            attrNames.push_back("FacingDirectionZ");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(1);
            attrPrecisions.push_back(2);

            attrNames.push_back("NormalDirectionX");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(1);
            attrPrecisions.push_back(2);

            attrNames.push_back("NormalDirectionY");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(1);
            attrPrecisions.push_back(2);

            attrNames.push_back("NormalDirectionZ");
            attrTypes.push_back("Decimal");
            attrWidths.push_back(1);
            attrPrecisions.push_back(2);


            const std::vector<ELEMENTS::IMilitarySymbolType::Attribute>& attributes = 
                milSymbolType->getAttributeVector();

            std::vector<ELEMENTS::IMilitarySymbolType::Attribute>::const_iterator attrIter = attributes.begin();
            for(; attrIter != attributes.end(); ++attrIter)
            {
                attrNames.push_back(attrIter->name);
                attrTypes.push_back(attrIter->type);
                attrWidths.push_back(attrIter->length);
                attrPrecisions.push_back(10);
            }

            layer = layerComponent->getOrCreateFeatureLayer(_layerName, milLayerType, attrNames, 
                attrTypes, attrWidths, attrPrecisions, addToWorld);

        }

        if(addToWorld)
        {
            _subscribe(layer.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);
            _subscribe(layer.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
        }

        return layer.get();
    }

    void MilitarySymbolPointUIHandler::setMilitarySymbol(ELEMENTS::IMilitarySymbol* unit)
    {
        _unit = unit;
    }

    void MilitarySymbolPointUIHandler::setMilitaryRange(ELEMENTS::IMilitaryRange* sphere )
    {
        _sphere = sphere;
    }

    void MilitarySymbolPointUIHandler::setMilitarySymbolType(std::string milSymbol)
    {
        _layerName = milSymbol;
    }

    std::string MilitarySymbolPointUIHandler::getMilitarySymbolType() const
    {
        return _layerName ;
    }

    ELEMENTS::IMilitarySymbol* MilitarySymbolPointUIHandler::getMilitarySymbol() const    
    {
        return _unit.get();
    }

    ELEMENTS::IMilitaryRange* MilitarySymbolPointUIHandler::getMilitaryRange() const    
    {
        return _sphere.get();
    }

    void MilitarySymbolPointUIHandler::setFocus(bool value)
    {
        if(!value)
        {
            reset();
        }
        VizUI::UIHandler::setFocus(value);
    }

    //! Function called when added to UI Manager
    void MilitarySymbolPointUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
        reset();
    }

    void MilitarySymbolPointUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {

        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                CORE::WorldMaintainer::instance();
            if(wmain.valid())
            {
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            // take all of the military layers and subscribe to its object added/removed messages.
            CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                CORE::WorldMaintainer::instance();
            if(wmain.valid())
            {
                const CORE::WorldMap& worldMap = wmain->getWorldMap();
                CORE::RefPtr<CORE::IWorld> iworld = NULL;
                if(worldMap.size() > 0)
                {
                    iworld = worldMap.begin()->second;
                }
                if(iworld.valid())
                {
                    const CORE::ObjectMap& objMap = iworld->getObjectMap();
                    CORE::ObjectMap::const_iterator iter = objMap.begin();
                    for(; iter != objMap.end(); ++iter)
                    {
                        CORE::RefPtr<CORE::IFeatureLayer> layer = 
                            iter->second.get()->getInterface<CORE::IFeatureLayer>();
                        if(layer.valid())
                        {
                            if(layer->getFeatureLayerType() == CORE::IFeatureLayer::MILITARY_UNIT_POINT)
                            {
                                _subscribe(layer.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
                                _subscribe(layer.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);
                            }
                        }
                    }

                }
            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            if(_mode == MILITARY_MODE_CREATE_UNIT)
            {
                return;
            }

#if 0
            // Query ModelPickUIHandler interface
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());                
            if(tph.valid())
            {
                setSelectedUnitAsCurrentUnit();
                const CORE::IntersectionList& ilist = tph->getCurrentIntersections();
                if(!ilist.empty())
                {
                    CORE::RefPtr<CORE::IObject> object = ilist.front()->object;
                    if(object.valid())
                    {
                        if(_unit.valid())
                        {
                            if(_unit->getInterface<CORE::IBase>()->getUniqueID() == 
                                object->getInterface<CORE::IBase>()->getUniqueID())
                            {
                                _createUndoMoveTransaction();
                                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                            }
                        }
                        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = 
                            object->getInterface<ELEMENTS::IMilitarySymbol>();
                        if(unit.valid())
                        {
                            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>
                                (getManager())->setObjectAsSelected(object->getInterface<CORE::ISelectable>());
                            setSelectedUnitAsCurrentUnit();
                            _createUndoMoveTransaction();
                            _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                        }
                    }
                }
            }
            _dragged = false;
#else
            _dragged = true;
#endif
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseDraggedMessageType)
        {
            // Query ModelPickUIHandler interface
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());                
            if(tph.valid())
            {
                osg::Vec3d pos;
                if(!_dragged)
                {
                    _dragged = true;
                }

                if (tph->getMousePickedPosition(pos, true, false))
                {
                    // Get the IMouseMessage interface
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleButtonDraggedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }
            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                const CORE::IntersectionList& ilist = tph->getCurrentIntersections();
                if(!ilist.empty())
                {
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleButtonDoubleClickedIntersection(mmsg->getMouse()->getButtonMask(), ilist.front());
                }

            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            // Get the IMouseMessage interface
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

            if(!_handleButtonRelease)
            {
                _handleButtonRelease = true;
                if(mmsg->getMouse()->getButtonMask() & VizUI::IMouse::RIGHT_BUTTON)
                {
                    return;
                }
            }

            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                osg::Vec3d pos;


                if (tph->getMousePickedPosition(pos, true, false))
                {
                    if(_dragged)
                        _handleButtonDraggedIntersection(mmsg->getMouse()->getButtonMask(), pos);

                    _handleButtonReleasedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }

                if(_dragged)
                {
                    _dragged = false;
                }
            }
        }
        else if(messageType == *CORE::IObjectMessage::ObjectAddedMessageType)
        {
            CORE::RefPtr<CORE::IObjectMessage> omsg = message.getInterface<CORE::IObjectMessage>();
            CORE::RefPtr<const CORE::IObject> object = omsg->getObject();

            _unit = object->getInterface<ELEMENTS::IMilitarySymbol>();
            if(!_unit.valid())
            {
                return;
            }

            _sphere = object->getInterface<ELEMENTS::IMilitaryRange>();
            if(!_sphere.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IBase> base = message.getSender();
            CORE::RefPtr<CORE::IFeatureLayer> featureLayer = base->getInterface<CORE::IFeatureLayer>();
            if(featureLayer.valid())
            {

                // XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = 
                    mf->createMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType);
                notifyObservers(*IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType, *msg);

            }

        }
        else if(messageType == *CORE::IObjectMessage::ObjectRemovedMessageType)
        {
            CORE::RefPtr<CORE::IObjectMessage> omsg = message.getInterface<CORE::IObjectMessage>();
            CORE::RefPtr<const CORE::IObject> object = omsg->getObject();

            CORE::RefPtr<CORE::IBase> base = message.getSender();
            CORE::RefPtr<CORE::IFeatureLayer> featureLayer = base->getInterface<CORE::IFeatureLayer>();
            if(featureLayer.valid())
            {
                _layerName = base->getName();

                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();

                CORE::RefPtr<CORE::IMessage> msg = 
                    mf->createMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType);

                notifyObservers(*IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType, *msg);

            }

        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void MilitarySymbolPointUIHandler::_createUndoMoveTransaction()
    {
        if(!_unit.valid())
            return;

        APP::IUndoTransactionFactory* transactionFactory = 
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> transaction = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MovePointTransactionType);

        CORE::RefPtr<TRANSACTION::IMovePointTransaction> movePointTransaction = 
        transaction->getInterface<TRANSACTION::IMovePointTransaction>();

        movePointTransaction->setPoint(_unit->getInterface<CORE::IPoint>());
        movePointTransaction->setPosition(_unit->getInterface<CORE::IPoint>()->getValue());

        // execute the transaction 
        addAndExecuteUndoTransaction(transaction.get());
    }

    void MilitarySymbolPointUIHandler::_handleButtonDraggedIntersection(int button, const osg::Vec3d& longLatAlt)
    {
        if((_mode == MILITARY_MODE_EDIT_POSITION) && (button & VizUI::IMouse::LEFT_BUTTON))
        {
            if(_unit.valid())
            {
                _unit->getInterface<CORE::IPoint>()->setValue(longLatAlt);

                // XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();

                CORE::RefPtr<CORE::IMessage> msg =
                    mf->createMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType);

                notifyObservers(*IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType, *msg);
            }
        }
    }

    void MilitarySymbolPointUIHandler::
        _handleButtonDoubleClickedIntersection(int button, CORE::RefPtr<CORE::Intersection> intersection)
    {
        if(!(button & VizUI::IMouse::LEFT_BUTTON) || (MILITARY_MODE_NONE == _mode))
        {
            return;
        }
        switch(_mode)
        {
        case MILITARY_MODE_CREATE_UNIT:
        case MILITARY_MODE_EDIT_POSITION:
            {
                if(_unit.valid())
                {
                    CORE::RefPtr<CORE::IObject> object = intersection->object;
                    if(object.valid())
                    {
                        CORE::RefPtr<ELEMENTS::IMilitarySymbol> symbol =
                            object->getInterface<ELEMENTS::IMilitarySymbol>();

                        if(symbol.valid())
                        {

                            CORE::RefPtr<CORE::IDeletable> deletable = symbol->getInterface<CORE::IDeletable>();

                            APP::IUndoTransactionFactory* transactionFactory = 
                                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

                            //Create a deleteObjectTransaction to delete the object
                            CORE::RefPtr<APP::IUndoTransaction> transaction = 
                                transactionFactory->createUndoTransaction(
                                *TRANSACTION::TransactionRegistryPlugin::DeleteObjectTransactionType);

                            CORE::RefPtr<TRANSACTION::IDeleteObjectTransaction> deleteObjectTransaction = 
                                transaction->getInterface<TRANSACTION::IDeleteObjectTransaction>();

                            deleteObjectTransaction->setObject(deletable.get());

                            if(transaction.valid())
                            {
                                addAndExecuteUndoTransaction(transaction.get());
                            }
                        }
                    }
                }
            }
            break;
        }
    }

    void MilitarySymbolPointUIHandler::_handleButtonReleasedIntersection(int button, const osg::Vec3d& longLatAlt)
    {
        if(button & VizUI::IMouse::LEFT_BUTTON)
        {
            if (_mode == SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_CREATE_UNIT)
            {
                createUnit();

                if(_unit.valid())
                {
                    _unit->getInterface<CORE::IPoint>()->setValue(longLatAlt);

                    addUnitToCurrentSelectedLayer();
                }
            }
        }
        else if(button & VizUI::IMouse::RIGHT_BUTTON)
        {
            if (_mode == SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_POSITION ||
                _mode == SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_ATTRIBUTE)
            {

                if(_unit.valid())
                {
                    if(_unit->getInterface<CORE::ISelectable>()->getSelected())
                    {
                        _unit->getInterface<CORE::ISelectable>()->setSelected(false);
                    }
                    reset();
                }

                // deactivate the militaryPathAnimationUIHandler
                CORE::RefPtr<SMCUI::IMilitaryPathAnimationUIHandler> milPathAnimationUIHandler =
                    APP::AccessElementUtils::
                    getUIHandlerUsingManager<SMCUI::IMilitaryPathAnimationUIHandler>(getManager());
                if(milPathAnimationUIHandler.valid())
                {
                    milPathAnimationUIHandler->setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_NONE);
                }

                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");
                if(component.valid())
                {
                    CORE::RefPtr<CORE::ISelectionComponent> selectionComponent = 
                        component->getInterface<CORE::ISelectionComponent>();
                    selectionComponent->clearCurrentSelection();
                }
            }
        }
    }

    void MilitarySymbolPointUIHandler::createUnit(bool addToWorld)
    {
        _unit = createSymbol(_layerName);
    }

    CORE::RefPtr<ELEMENTS::IMilitarySymbol> MilitarySymbolPointUIHandler::createSymbol(const std::string &type) 
    {
        CORE::RefPtr<CORE::IObject> pointObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(
            *ELEMENTS::ElementsRegistryPlugin::MilitarySymbolPointType);

        if(!pointObject.valid())
        {
            return NULL;
        }

        //! set icon
        ELEMENTS::IMilitarySymbol* milSymbol = pointObject->getInterface<ELEMENTS::IMilitarySymbol>();
        if(!milSymbol)
        {
            return NULL;
        }

        milSymbol->setMilitaryType(type);

        //! set temporary state
        CORE::RefPtr<CORE::ITemporary> temp = pointObject->getInterface<CORE::ITemporary>();

        if(temp.valid())
        {
            temp->setTemporary(true);
        }

        CORE::RefPtr<ELEMENTS::IRepresentation> represent = pointObject->getInterface<ELEMENTS::IRepresentation>();
        if(represent.valid())
        {
            represent->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
        }

        CORE::RefPtr<ELEMENTS::IClamper> clamp = pointObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            //checking whether the clamping attribute is present in config or not
            if (!_clampingFromConfig)
            {
                 //if it is not setting from config , then it will set from setting component
                _setDefaultClamping();
            }
            clamp->setClampOnPlacement(_clamping);
        }

        CORE::RefPtr<ELEMENTS::IDrawTechnique>  drawTechnique = pointObject->getInterface<ELEMENTS::IDrawTechnique>();
        if(drawTechnique.valid())
        {
            //checking whether the offset attribute is present in config or not
            if (!_offsetFromConfig)
            {
                //if it is not setting from config , then it will set from setting component
                _setDefaultOffset();
            }
            drawTechnique->setHeightOffset(_offset);
        }

        getWorldInstance()->addObject(pointObject.get());

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = pointObject->getInterface<ELEMENTS::IMilitarySymbol>();

        _sphere = pointObject->getInterface<ELEMENTS::IMilitaryRange>();

        return unit;
    }


    void MilitarySymbolPointUIHandler::reset()
    {
        _unit = NULL;
        _sphere = NULL;
        setMode(IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE);
        _dragged = false;
    }

    // setting default offset value using setting component
    void MilitarySymbolPointUIHandler::_setDefaultOffset()
    {
       CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
       APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());

       if(wmain.valid())
       {
            CORE::IComponent* component = wmain->getComponentByName("SettingComponent");
            if(component)
            {
                  _settingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
                  _offset = _settingComponent->getHeightOffset();
            }
       }   
    }

    // setting default clamp state using config
    void MilitarySymbolPointUIHandler::setOffsetFromConfig(double offset)
    {
        _offset = offset;
        _offsetFromConfig = true;
    }

    double MilitarySymbolPointUIHandler::getOffsetFromConfig() const
    {
        return _offset;
    }

    // setting default clamp state using setting component
    void MilitarySymbolPointUIHandler::_setDefaultClamping()
    {
         CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
         APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());

         if(wmain.valid())
         {
            CORE::IComponent* component = wmain->getComponentByName("SettingComponent");
            if(component)
            {
                  _settingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
                  _clamping = _settingComponent->getDefaultClampingState();
            }
         }   
    }

    // setting default clamp state using config
    void MilitarySymbolPointUIHandler::setClampingStateFromConfig(bool clamp)
    {
        _clamping = clamp;
        _clampingFromConfig = true;
    }

    bool MilitarySymbolPointUIHandler::getClampingStateFromConfig() const
    {
        return _clamping;
    }

     void MilitarySymbolPointUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "MilitarySymbolPointUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("Offset", "Offset",
            CORE::DoubleAttribute::SetFuncType(this, &MilitarySymbolPointUIHandler::setOffsetFromConfig),
            CORE::DoubleAttribute::GetFuncType(this, &MilitarySymbolPointUIHandler::getOffsetFromConfig),
            "Offset name",
            groupName));

          _addAttribute(new CORE::BooleanAttribute("Clamp", "Clamp",
            CORE::BooleanAttribute::SetFuncType(this, &MilitarySymbolPointUIHandler::setClampingStateFromConfig),
            CORE::BooleanAttribute::GetFuncType(this, &MilitarySymbolPointUIHandler::getClampingStateFromConfig),
            "Clamp",
            groupName));
    }

    void MilitarySymbolPointUIHandler::setMode(IMilitarySymbolPointUIHandler::MilitarySymbolMode mode)
    {
        if(_mode == mode)
            return;

        _mode = mode;
        _handleButtonRelease = false;

        if(_mode == IMilitarySymbolPointUIHandler::MILITARY_MODE_CREATE_UNIT)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(false);
            }
        }
        else if(mode == IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_ATTRIBUTE)
        {
            setSelectedUnitAsCurrentUnit();
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);  
            }
            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = 
                mf->createMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType);
            notifyObservers(*IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType, *msg);
        }
        else if(mode == IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_POSITION)
        {
            setSelectedUnitAsCurrentUnit();
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);  
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);  
            }

            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = 
                mf->createMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType);
            notifyObservers(*IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType, *msg);
        }
        else if(_mode == IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(true);
            }
        }
    }

    IMilitarySymbolPointUIHandler::MilitarySymbolMode 
        MilitarySymbolPointUIHandler::getMode()
    {
        return _mode;
    }

    void MilitarySymbolPointUIHandler::setSelectedUnitAsCurrentUnit()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            _unit = iter->second->getInterface<ELEMENTS::IMilitarySymbol>();
            _sphere = iter->second->getInterface<ELEMENTS::IMilitaryRange>();
        }
    }

    void MilitarySymbolPointUIHandler::setMetadataRecord(CORE::IMetadataRecord* record)
    {
        if(_unit.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _unit->getInterface<CORE::IMetadataRecordHolder>();
            if(holder.valid())
            {
                return holder->setMetadataRecord(record);
            }
        }
    }

    CORE::RefPtr<CORE::IMetadataRecord>
        MilitarySymbolPointUIHandler::getMetadataRecord() const
    {
        if(_unit.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _unit->getInterface<CORE::IMetadataRecordHolder>();
            if(holder.valid())
            {
                return holder->getMetadataRecord();
            }
        }
        return NULL;
    }

    void MilitarySymbolPointUIHandler::update()
    {
        if(_unit.valid())
        {
            CORE::RefPtr<CORE::IFeatureObject> feature = 
                _unit->getInterface<CORE::IFeatureObject>();
            if(feature.valid())
            {
                return feature->update();
            }
        }
    }

    void MilitarySymbolPointUIHandler::addUnitToCurrentSelectedLayer()
    {
        addSymbolToLayer(_unit);
    }

    void MilitarySymbolPointUIHandler::addSymbolToLayer(CORE::RefPtr<ELEMENTS::IMilitarySymbol> symbol) 
    {
        if(symbol.valid())
        {
            CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateMilitarySymbolLayer(symbol->getMilitaryType());

            if(layer.valid())
            {
                CORE::RefPtr<CORE::ICompositeObject> composite = 
                    layer->getInterface<CORE::ICompositeObject>();

                CORE::RefPtr<CORE::IBase> pointObject = 
                    symbol->getInterface<CORE::IBase>();

                getWorldInstance()->removeObjectByID(&pointObject->getUniqueID());

                CORE::RefPtr<CORE::ITemporary> temp = 
                    pointObject->getInterface<CORE::ITemporary>();

                if(temp.valid())
                {
                    temp->setTemporary(false);
                }

                // create a transaction to add the model to the layer
                APP::IUndoTransactionFactory* transactionFactory = 
                    APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

                CORE::RefPtr<APP::IUndoTransaction> transaction = 
                    transactionFactory->createUndoTransaction(
                    *TRANSACTION::TransactionRegistryPlugin::CompositeAddObjectTransactionType);

                CORE::RefPtr<TRANSACTION::ICompositeAddObjectTransaction> compositeAddObjectTransaction = 
                    transaction->getInterface<TRANSACTION::ICompositeAddObjectTransaction>();

                compositeAddObjectTransaction->setCompositeObject(composite.get());
                compositeAddObjectTransaction->setObject(symbol->getInterface<CORE::IObject>());

                // execute the transaction 
                addAndExecuteUndoTransaction(transaction.get());
            }
        }
    }

    CORE::IFeatureLayer*
        MilitarySymbolPointUIHandler::_getSelectedLayer()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if(selectionUIHandler.valid())
        {
            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                CORE::IFeatureLayer* layer = iter->second->getInterface<CORE::IFeatureLayer>();
                return layer;
            }
        }

        return NULL;
    }

    CORE::RefPtr<CORE::IMetadataTableDefn>
        MilitarySymbolPointUIHandler::getCurrentSelectedLayerDefn()
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateMilitarySymbolLayer(_layerName);
        if(layer.valid())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                return holder->getMetadataTable()->getMetadataTableDefn();
            }
        }

        return NULL;
    }

    void MilitarySymbolPointUIHandler::removeCreatedUnit()
    {
        if(_unit.valid())
        {
            getWorldInstance()->removeObjectByID(&(_unit->getInterface<CORE::IBase>()->getUniqueID()));
            _unit = NULL;
            _sphere = NULL;
        }
    }

    void MilitarySymbolPointUIHandler::setTemporaryState(bool value)
    {
        _temporary = value;
    }

    bool MilitarySymbolPointUIHandler::getTemporaryState() const
    {
        return _temporary;
    }

    std::string MilitarySymbolPointUIHandler::getQMLIconPath
        (CORE::RefPtr<ELEMENTS::IMilitarySymbolType> milSymbolType) const
    {
        if(milSymbolType.valid())
        {
            std::string iconFileName = milSymbolType->getIconFilename();

            std::string absoluteIconPath = "file:///" + osgDB::convertFileNameToUnixStyle(osgDB::getRealPath(iconFileName));

            return absoluteIconPath;
        }

        return std::string("");
    }

} // namespace SMCUI

