/*****************************************************************************
*
* File             : ElevationDiffCalculationUIHandler.cpp
* Description      : ElevationDiffCalculationUIHandler class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <Core/CoreRegistry.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <Util/CoordinateConversionUtils.h>
#include <Util/StringUtils.h>
#include <Util/BaseExceptions.h>
#include <SMCUI/ElevationDiffUIHandler.h>

#include <boost/lexical_cast.hpp>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, ElevationDiffUIHandler);
    DEFINE_IREFERENCED(ElevationDiffUIHandler, UIHandler);

    ElevationDiffUIHandler::ElevationDiffUIHandler()
        :_eleDiffInString("")
    {
         //TBD initiliaze the member variables in initialization list
        _addInterface(SMCUI::IElevationDiffUIHandler::getInterfaceName());

        setName(getClassname());
    }

    ElevationDiffUIHandler::~ElevationDiffUIHandler()
    {
    }

    void ElevationDiffUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void ElevationDiffUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    
    void ElevationDiffUIHandler::setPoint1(CORE::IPoint* point1)
    {
        _point1 = point1;
    }

    CORE::IPoint* ElevationDiffUIHandler::getPoint1()
    {
        return _point1;
    }

    void ElevationDiffUIHandler::setPoint2(CORE::IPoint* point2)
    {
        _point2 = point2;
    }

        
    CORE::IPoint* ElevationDiffUIHandler::getPoint2()
    {
        return _point2;
    }

    void ElevationDiffUIHandler::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
         // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::FilterStatus status = message.getInterface<GISCOMPUTE::IFilterStatusMessage>()->getStatus();
            if(status == GISCOMPUTE::FILTER_IDLE || status == GISCOMPUTE::FILTER_SUCCESS)
            {
                 //XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage
                    (*SMCUI::IElevationDiffUIHandler::ElevationDiffCalculationCompletedMessageType);
                notifyObservers(*SMCUI::IElevationDiffUIHandler::ElevationDiffCalculationCompletedMessageType, *msg);
            }
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }
    
    std::string ElevationDiffUIHandler::getEleDiffText()
    {
        return _eleDiffInString;

    }
    void ElevationDiffUIHandler::reset()
    {
        _point1 = NULL;
        _point2 = NULL;

    }

    void ElevationDiffUIHandler::execute()
    {
        osg::Vec3d point1_latLongAlt = UTIL::CoordinateConversionUtils::longLatHeightToLatLongHeight(_point1->getValue());
        osg::Vec3d point2_latLongAlt = UTIL::CoordinateConversionUtils::longLatHeightToLatLongHeight(_point2->getValue());
        
        //calculating elevation difference between 2 points
        double rise = point2_latLongAlt.z() - point1_latLongAlt.z();
        _eleDiffInString =  UTIL::ToString(rise,3);
        _eleDiffInString += " metres" ;

    }

}  // namespace SMCUI
