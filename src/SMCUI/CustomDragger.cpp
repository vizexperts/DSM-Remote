#include <SMCUI/CustomDragger.h>

#include <osg/ShapeDrawable>
#include <osg/Geometry>
#include <osg/LineWidth>
#include <osg/PolygonMode>
#include <osg/CullFace>
#include <osg/Quat>


#include <Util/OSGGeometryUtils.h>
#include <SMCUI/DraggerContainer.h>

#include <SMCUI/DraggerUIHandler.h>

#include <iostream>


// NP:-Screen space scaling not working properly
#define USE_SCREEN_COORD_FOR_SCALING 0

namespace SMCUI
{
    // Utility function to create a circular geometry
    osg::Geometry* createCircleGeometry(float radius, unsigned int numSegments, bool fill)
    {
        const float angleDelta = 2.0f*osg::PI/(float)numSegments;
        const float r= radius;
        float angle = 0.0f;
        osg::Vec3Array* vertexArray = new osg::Vec3Array(numSegments);
        osg::Vec3Array* normalArray= new osg::Vec3Array(numSegments);
        for(unsigned int i = 0; i < numSegments; ++i, angle += angleDelta)
        {
            float c = cosf(angle);
            float s =  sinf(angle);
            (*vertexArray)[i].set(c*r, s*r, 0.0f);
            (*normalArray)[i].set(c, s, 0.0f);
        }
        osg::Geometry* geometry = new osg::Geometry();
        geometry->setVertexArray(vertexArray);
        geometry->setNormalArray(normalArray);
        geometry->setNormalBinding(osg::Geometry::BIND_PER_VERTEX);
        if(fill)
        {
            geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POLYGON,0,vertexArray->size()));
        }
        else
        {
            geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP,0,vertexArray->size()));
        }
        return geometry;
    }


    void CustomTranslateAxisDragger::setupTranslateDragger(osg::ref_ptr<osgManipulator::Translate1DDragger> dragger)
    {
        // Create a line.
        osg::Geode* lineGeode = new osg::Geode;
        {
            osg::Geometry* geometry = new osg::Geometry();

            osg::Vec3Array* vertices = new osg::Vec3Array(2);
            (*vertices)[0] = osg::Vec3(0.0f,0.0f,0.0f);
            (*vertices)[1] = osg::Vec3(0.0f,0.0f,1.0f);

            geometry->setVertexArray(vertices);
            geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES,0,2));

            lineGeode->addDrawable(geometry);
        }

        // Turn of lighting for line and set line width.
        {
            osg::LineWidth* linewidth = new osg::LineWidth();
            linewidth->setWidth(12.0f);
            lineGeode->getOrCreateStateSet()->setAttributeAndModes(linewidth, osg::StateAttribute::ON);
        }

        dragger->addChild(lineGeode);

        osg::Geode* geode = new osg::Geode;

        osg::Cone* cone = new osg::Cone (osg::Vec3(0.0f, 0.0f, 1.0f), 0.045f, 0.10f);
        osg::ShapeDrawable* coneDrawable = new osg::ShapeDrawable(cone);
        coneDrawable->setColor(COLORS_BLACK);

        osg::Cylinder* cylinder = new osg::Cylinder (osg::Vec3(0.0f,0.0f,0.5f), 0.04f, 1.0f);
        osg::ShapeDrawable* cylinderDrawable = new osg::ShapeDrawable(cylinder);
        cylinderDrawable->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        //osgManipulator::setDrawableToAlwaysCull(*cylinderDrawable);
        cylinderDrawable->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 0.0f));
        geode->addDrawable(coneDrawable);
        geode->addDrawable(cylinderDrawable);

        dragger->addChild(geode);
    }

    void setGeodeToAlwaysCull(osg::Geode* geode)
    {
        if(!geode)
            return;

        for(unsigned int i=0; i< geode->getNumDrawables(); ++i)
        {
            osgManipulator::setDrawableToAlwaysCull(*geode->getDrawable(i));
        }
    }

    //! CustomTrackballDragger
    CustomTrackballDragger::CustomTrackballDragger()
    {
        _xDragger = new osgManipulator::RotateCylinderDragger();
        addChild(_xDragger.get());
        addDragger(_xDragger.get());

        _yDragger = new osgManipulator::RotateCylinderDragger();
        addChild(_yDragger.get());
        addDragger(_yDragger.get());

        _zDragger = new osgManipulator::RotateCylinderDragger();
        addChild(_zDragger.get());
        addDragger(_zDragger.get());

        setParentDragger(getParentDragger());
    }

    void CustomTrackballDragger::setupDefaultGeometry()
    {
        osg::Geode* circleGeode = new osg::Geode;
        circleGeode->addDrawable(createCircleGeometry(1.0f, 100));

       // Draw in line mode.
        {
            osg::PolygonMode* polymode = new osg::PolygonMode;
            polymode->setMode(osg::PolygonMode::FRONT_AND_BACK,osg::PolygonMode::LINE);

            circleGeode->getOrCreateStateSet()->setAttributeAndModes(polymode,osg::StateAttribute::OVERRIDE
                | osg::StateAttribute::ON);

            circleGeode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(8.0f),osg::StateAttribute::ON);

#if !defined(OSG_GLES2_AVAILABLE)
            circleGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
#endif
        }
        _xDragger->addChild(circleGeode);
        _yDragger->addChild(circleGeode);
        _zDragger->addChild(circleGeode);

        double radius = 1.0;
        double pipeRadius = 0.025;
        double innerRadius = radius - pipeRadius;
        double outerRadius = radius + pipeRadius;

        osg::Geode* torusGeode = UTIL::OSGGeometryUtils::GetTorusGeode(innerRadius, outerRadius, 20, 30);
        setGeodeToAlwaysCull(torusGeode);
        _xDragger->addChild(torusGeode);
        _yDragger->addChild(torusGeode);
        _zDragger->addChild(torusGeode);

        // Rotate X-axis dragger appropriately.
        {
            osg::Quat rotation; rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), osg::Vec3(1.0f, 0.0f, 0.0f));
            _xDragger->setMatrix(osg::Matrix(rotation));
        }

        // Rotate Y-axis dragger appropriately.
        {
            osg::Quat rotation; rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), osg::Vec3(0.0f, 1.0f, 0.0f));
            _yDragger->setMatrix(osg::Matrix(rotation));
        }

        // Send different colors for each dragger.
        _xDragger->setColor(COLORS_RED);
        _yDragger->setColor(COLORS_GREEN);
        _zDragger->setColor(COLORS_BLUE);

    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////
    //! CustomScaleDragger
    CustomScaleDragger::CustomScaleDragger()
    {
        _projector = new osgManipulator::CylinderPlaneProjector();
        setColor(COLORS_GREEN);
        setPickColor(COLORS_YELLOW);
        _minScale = 0.001;
        _maxScale = 50.0;
    }
    double CustomScaleDragger::computeScale(const osg::Vec3d& startProjectedPoint, 
        const osg::Vec3d& projectedPoint,
        const osg::Vec3d& scaleCenter)
    {
        double denom = (startProjectedPoint - scaleCenter).length();
        double scale = denom ? (projectedPoint - scaleCenter).length() / denom : 0.0;

        //std::cout<< (projectedPoint - scaleCenter).length() << '/' << denom << '=' << scale << std::endl;
        return scale;
    }

    double CustomScaleDragger::computeScaleInScreenSpace(const osg::Vec3d& startProjectedPoint, 
        const osg::Vec3d& projectedPoint,
        const osg::Vec3d& scaleCenter)
    {
        if(!_camera.valid())
        {
            _getCamera();
        }

        if(!_camera.valid())
        {
            return computeScale(startProjectedPoint, projectedPoint, scaleCenter);
        }

        osg::Matrix projectionMatrix = _camera->getProjectionMatrix();
        osg::Matrix viewMatrix = _camera->getViewMatrix();
        osg::Matrix windowMatrix = _camera->getViewport()->computeWindowMatrix();

        osg::Matrix conversionMatrix = osg::Matrix(viewMatrix * projectionMatrix * windowMatrix);

        osg::Vec3 startProjectedPoint_onScreen = startProjectedPoint*conversionMatrix;
        osg::Vec3 projectedPoint_onScreen = projectedPoint * conversionMatrix;
        osg::Vec3 scaleCenter_onScreen = scaleCenter * conversionMatrix;

        double denom = (osg::Vec2(startProjectedPoint_onScreen.x(), startProjectedPoint_onScreen.y())
            - osg::Vec2(scaleCenter_onScreen.x(), scaleCenter_onScreen.y())).length();

        double num = (osg::Vec2(projectedPoint_onScreen.x(), projectedPoint_onScreen.y()) - 
            osg::Vec2(scaleCenter_onScreen.x(), scaleCenter_onScreen.y())).length();

        double scale = denom ? (num/denom) : 0.0;

        return scale;
    }

    void CustomScaleDragger::_getCamera()
    {
        SMCUI::DraggerContainer* draggerContainer = dynamic_cast<SMCUI::DraggerContainer*>(getParentDragger());

        if(!draggerContainer)
            return;
            
        SMCUI::DraggerUIHandler* draggerUIHandler = draggerContainer->getDraggerUIHandler();

        if(!draggerUIHandler)
            return;

        _camera = draggerUIHandler->getCamera();

    }

    bool CustomScaleDragger::handle(const osgManipulator::PointerInfo &pointer, 
        const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
    {
        //Check if the dragger node is in the nodepath.
        if(!pointer.contains(this)) return false;

        switch (ea.getEventType())
        {
            //Pick start.
        case(osgGA::GUIEventAdapter::PUSH):
            {
                //Get the LocalToWorld matrix fot this node and set it for the projector.
                osg::NodePath nodePathToRoot;
                computeNodePathToRoot(*this, nodePathToRoot);
                osg::Matrix localToWorld = osg::computeLocalToWorld(nodePathToRoot);
                _projector->setLocalToWorld(localToWorld);

                if(_projector->project(pointer, _startProjectedPoint))
                {
                    _scaleCenter = _projector->getCylinder()->getCenter();

#if USE_SCREEN_COORD_FOR_SCALING
                    _startProjectedPoint = _startProjectedPoint*localToWorld;
                    _scaleCenter = _scaleCenter* localToWorld;
#endif
                    //Generate the motion command.
                    osg::ref_ptr<osgManipulator::ScaleUniformCommand> cmd = new osgManipulator::ScaleUniformCommand();
                    cmd->setStage(osgManipulator::MotionCommand::START);
                    cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(), _projector->getWorldToLocal());

                    //Dispatch command.
                    dispatch(*cmd);

                    //Set color to pick color.
                    osgManipulator::setMaterialColor(_pickColor, *this);

                    aa.requestRedraw();
                }
                return true;
            }
            //Pick Move
        case (osgGA::GUIEventAdapter::DRAG):
            {
                osg::Vec3d projectedPoint;
                if(_projector->project(pointer, projectedPoint))
                {
                    //Generate the motion command
                    osg::ref_ptr<osgManipulator::ScaleUniformCommand> cmd = new osgManipulator::ScaleUniformCommand();

                    //Compute Scale
#if USE_SCREEN_COORD_FOR_SCALING
                    projectedPoint = projectedPoint*_projector->getLocalToWorld();
                    double scale = computeScaleInScreenSpace(_startProjectedPoint, projectedPoint, _scaleCenter);
#else
                    double scale = computeScale(_startProjectedPoint, projectedPoint, _scaleCenter);
#endif

                    if(scale < getMinScale())
                    {
                        scale = getMinScale();
                    }
                    if(scale > getMaxScale())
                    {
                        scale = getMaxScale();
                    }

                    static double oldScale = 0.0;
                    static bool decreasing = false;
                    if(decreasing)
                    {
                        if(scale > oldScale)
                        {
                            decreasing = false;
                        }
                    }
                    if(scale < oldScale)
                    {
                        decreasing = true;
                    }
                    oldScale = scale;

                    cmd->setStage(osgManipulator::MotionCommand::MOVE);
                    cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(), _projector->getWorldToLocal());
                    cmd->setScale(scale);
                    cmd->setScaleCenter(_scaleCenter);

                    //Dispatch command.
                    dispatch(*cmd);

                    aa.requestRedraw();
                }
                return true;
            }

            //Pick finish.
        case(osgGA::GUIEventAdapter::RELEASE):
            {
                osg::ref_ptr<osgManipulator::ScaleUniformCommand> cmd = new osgManipulator::ScaleUniformCommand();

                cmd->setStage(osgManipulator::MotionCommand::FINISH);
                cmd->setLocalToWorldAndWorldToLocal(_projector->getLocalToWorld(), _projector->getWorldToLocal());

                dispatch(*cmd);

                //Reset color.
                osgManipulator::setMaterialColor(_color, *this);

                aa.requestRedraw();

                return true;
            }
        default:
            return false;
        }
    }

    void CustomScaleDragger::setupDefaultGeometry()
    {
        osg::Geode* circleGeode = new osg::Geode;
        circleGeode->addDrawable(createCircleGeometry(1.0f, 100));

       // Draw in line mode.
        {
            osg::PolygonMode* polymode = new osg::PolygonMode;
            polymode->setMode(osg::PolygonMode::FRONT_AND_BACK,osg::PolygonMode::LINE);
            circleGeode->getOrCreateStateSet()->setAttributeAndModes(polymode,osg::StateAttribute::OVERRIDE
                | osg::StateAttribute::ON);
            circleGeode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(8.0f),osg::StateAttribute::ON);

#if !defined(OSG_GLES2_AVAILABLE)
            circleGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
#endif
        }

        this->addChild(circleGeode);

        osg::Geode* planeGeode = new osg::Geode;
        osg::Geometry* planeGeom = createCircleGeometry(1.0f, 100, true);

        osg::Vec4Array* colors = new osg::Vec4Array;
        colors->push_back(COLORS_TRANSPARENT_BLUE);
        planeGeom->setColorArray(colors);
        planeGeom->setColorBinding(osg::Geometry::BIND_OVERALL);

        planeGeode->addDrawable(planeGeom);

        {
            osg::PolygonMode* polymode = new osg::PolygonMode;
            polymode->setMode(osg::PolygonMode::FRONT_AND_BACK,osg::PolygonMode::FILL);

            planeGeode->getOrCreateStateSet()->setAttributeAndModes(polymode,osg::StateAttribute::OVERRIDE
                | osg::StateAttribute::ON);

            planeGeode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
            planeGeode->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

#if !defined(OSG_GLES2_AVAILABLE)
            planeGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
#endif
        }

        this->addChild(planeGeode);

        double radius = 1.0;
        double pipeRadius = 0.025;
        double innerRadius = radius - pipeRadius;
        double outerRadius = radius + pipeRadius;

        osg::Geode* torusGeode = UTIL::OSGGeometryUtils::GetTorusGeode(innerRadius, outerRadius, 20, 30);
        setGeodeToAlwaysCull(torusGeode);
        this->addChild(torusGeode);

        osg::Quat rotation; 
        rotation.makeRotate(osg::Vec3(0.0f, 1.0f, 1.0f), osg::Vec3(1.0f, 0.0f, 0.0f));

        this->setMatrix(osg::Matrix(rotation));
    }

    /////////////////////////////////////////////////////////////////////////////

    void CustomTranslateAxisDragger::setupDefaultGeometry()
    {
        setupTranslateDragger(_xDragger);
        setupTranslateDragger(_yDragger);
        setupTranslateDragger(_zDragger);

        // Rotate X-axis dragger appropriately.
        {
            osg::Quat rotation;
            rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), osg::Vec3(1.0f, 0.0f, 0.0f));

            osg::Matrix translation;
            translation.makeTranslate(0, 0, 0.05);

            _xDragger->setMatrix(osg::Matrix(rotation)* translation);
        }

        // Rotate Y-axis dragger appropriately.
        {
            osg::Quat rotation;
            rotation.makeRotate(osg::Vec3(0.0f, 0.0f, 1.0f), osg::Vec3(0.0f, 1.0f, 0.0f));

            osg::Matrix translation;
            translation.makeTranslate(0, 0, 0.05);

            _yDragger->setMatrix(osg::Matrix(rotation)*translation);
        }

        // translate z dragger above the origin 
        {
            osg::Matrix translation;
            translation.makeTranslate(0, 0, 0.05);
            _zDragger->setMatrix(translation);
        }

        _xDragger->setColor(COLORS_RED);
        _yDragger->setColor(COLORS_GREEN);
        _zDragger->setColor(COLORS_BLUE);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    CustomTranslatePlaneDragger::CustomTranslatePlaneDragger()
    {
        if(_projector.valid())
        {
            _projector->setPlane(osg::Plane(0.0, 0.0, 1.0, 0.0));
        }
        else
        {
            _projector = new osgManipulator::PlaneProjector(osg::Plane(0.0,0.0,1.0,0.0));
        }

        setColor(COLORS_RED);
    }

    void CustomTranslatePlaneDragger::setupDefaultGeometry()
    {
        //osgManipulator::Translate2DDragger::setupDefaultGeometry();

        osg::Geode* circleGeode = new osg::Geode;
        circleGeode->addDrawable(createCircleGeometry(1.0f, 100));

       // Draw in line mode.
        {
            osg::PolygonMode* polymode = new osg::PolygonMode;
            polymode->setMode(osg::PolygonMode::FRONT_AND_BACK,osg::PolygonMode::LINE);
            circleGeode->getOrCreateStateSet()->setAttributeAndModes(polymode,osg::StateAttribute::OVERRIDE
                | osg::StateAttribute::ON);
            circleGeode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(8.0f),osg::StateAttribute::ON);

#if !defined(OSG_GLES2_AVAILABLE)
            circleGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
#endif
        }

        this->addChild(circleGeode);

        osg::Geode* planeGeode = new osg::Geode;
        osg::Geometry* planeGeom = createCircleGeometry(1.0f, 100, true);

        osg::Vec4Array* colors = new osg::Vec4Array;
        colors->push_back(COLORS_TRANSPARENT_BLUE);
        planeGeom->setColorArray(colors);
        planeGeom->setColorBinding(osg::Geometry::BIND_OVERALL);

        planeGeode->addDrawable(planeGeom);

       // Draw in line mode.
        {
            osg::PolygonMode* polymode = new osg::PolygonMode;
            polymode->setMode(osg::PolygonMode::FRONT_AND_BACK,osg::PolygonMode::FILL);

            planeGeode->getOrCreateStateSet()->setAttributeAndModes(polymode,osg::StateAttribute::OVERRIDE
                | osg::StateAttribute::ON);

            planeGeode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
            planeGeode->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

#if !defined(OSG_GLES2_AVAILABLE)
            planeGeode->getOrCreateStateSet()->setMode(GL_NORMALIZE, osg::StateAttribute::ON);
#endif
        }

        this->addChild(planeGeode);

        osg::Quat rotation; 
        rotation.makeRotate(osg::Vec3(0.0f, 1.0f, 1.0f), osg::Vec3(1.0f, 0.0f, 0.0f));

        this->setMatrix(osg::Matrix(rotation));
    }
}
