#include <SMCUI/CutAndFillUIHandler.h>

#include <Terrain/IRasterObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <Core/ITerrain.h>

#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, CutAndFillUIHandler);

    DEFINE_IREFERENCED(CutAndFillUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    CutAndFillUIHandler::CutAndFillUIHandler()
    {
        _addInterface(ICutAndFillUIHandler::getInterfaceName());
        setName(getClassname());
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _areaExtent     = new osg::Vec2Array;
        _transparency=0.0;
        _color=GISCOMPUTE::ICutAndFillFilterVisitor::RGB;
        _name="";
        _reset();
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    CutAndFillUIHandler::~CutAndFillUIHandler(){}

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void CutAndFillUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void CutAndFillUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void CutAndFillUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool CutAndFillUIHandler::getFocus() const
    {
        return false;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void CutAndFillUIHandler::execute()
    {
        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(vfactory)
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if(world)
            {
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::CutAndFillFilterVisitorType);

                if(!_visitor)
                {
                    LOG_ERROR("Failed to create filter class");
                    return;
                }

                GISCOMPUTE::ICutAndFillFilterVisitor* iVisitor  = 
                    _visitor->getInterface<GISCOMPUTE::ICutAndFillFilterVisitor>();


                //set threshold val
                iVisitor->setTransparency(_transparency);
                //set color val
                iVisitor->setColor(_color);

                if(_areaExtent->size() >1)
                {
                    //set extent on visitor
                    iVisitor->setExtents(_areaExtent.get());
                }

                // set stream output name
                iVisitor->setOutputName(_name);

                if(_firstInputElevObj &&  _secondInputElevObj)
                {
                    iVisitor->setElevationObjects(_firstInputElevObj,_secondInputElevObj);
                }
                else
                {
                    LOG_ERROR("Failed Input Not Proper");
                    return;
                }

                CORE::IBase* applyNode = NULL;
                if(!_elevObj)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    applyNode = terrain->getInterface<CORE::IBase>();
                }
                else
                {
                    applyNode = _elevObj->getInterface<CORE::IBase>();
                }

                //subscribe for filter status
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                applyNode->accept(*_visitor);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void CutAndFillUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::ICutAndFillFilterVisitor> visitor = 
                        base->getInterface<GISCOMPUTE::ICutAndFillFilterVisitor>();

                    if(visitor.valid())
                    {
                        CORE::RefPtr<TERRAIN::IRasterObject> object = visitor->getOutput();

                        if(object)
                        {
                            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                            if(world)
                            {
                                //osg::ref_ptr<osg::Node> node = object->getInterface<CORE::IObject>()->getOSGNode();
                                //if(node.valid())
                                //{
                                //    osg::ref_ptr<osg::StateSet> stateSet = node->getOrCreateStateSet();
                                //    stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::PROTECTED);
                                //}

                                world->addObject(object->getInterface<CORE::IObject>(true));
                                /*CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                                terrain->addRasterObject(object);*/
                            }
                        }
                        else
                        {
                            throw UTIL::Exception("Failed to create raster object", __FILE__, __LINE__);
                        }
                    }
                }
            }
            catch(...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void CutAndFillUIHandler::_reset()
    {
        _areaExtent->clear();
        _elevObj = NULL;
        _firstInputElevObj = NULL;
        _secondInputElevObj=NULL;
        _visitor = NULL;
        _transparency=0.0;
        _color=GISCOMPUTE::ICutAndFillFilterVisitor::RGB;
        _name="";
    }

    void CutAndFillUIHandler::setExtents(osg::Vec4d extents)
    {
        _areaExtent->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _areaExtent->push_back(osg::Vec2d(first.x(), first.y()));
        _areaExtent->push_back(osg::Vec2d(second.x(), second.y()));
    }

    void CutAndFillUIHandler::setColor(GISCOMPUTE::ICutAndFillFilterVisitor::COLOR color)
    {
        _color=color;

    }
    /*
    GISCOMPUTE::ICutAndFillFilterVisitor::COLOR CutAndFillUIHandler::getColor() const
    {
        return _color;

    }*/
    void CutAndFillUIHandler::setTransparency(double transparency)
    {
        _transparency=transparency;

    }
    double CutAndFillUIHandler::getTransparency() const
    {
        return _transparency;

    }
    void CutAndFillUIHandler::setOutputName(std::string& name)
    {
        _name = name;
    }
    osg::Vec4d CutAndFillUIHandler::getExtents() const
    {
        osg::Vec2d first = _areaExtent->at(0);
        osg::Vec2d second = _areaExtent->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    void CutAndFillUIHandler::setElevationObject(TERRAIN::IElevationObject* obj)
    {
        _elevObj = obj;
    }

    void CutAndFillUIHandler::setInputElevationObject(TERRAIN::IElevationObject* objFirst,TERRAIN::IElevationObject* objSecond)
    {
        _firstInputElevObj=objFirst;
        _secondInputElevObj=objSecond;
    }
    void CutAndFillUIHandler::stopFilter()
    {
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }

} // namespace SMCUI

