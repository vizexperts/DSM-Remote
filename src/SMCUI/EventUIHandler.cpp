/*****************************************************************************
*
* File             : EventUIHandler.cpp
* Description      : EventUIHandler class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

//Header files
#include <SMCUI/EventUIHandler.h>

#include <Core/IPoint.h>
#include <Core/IMessage.h>
#include <Core/IWorld.h>
#include <Core/CoreRegistry.h>
#include <Core/InterfaceUtils.h>
#include <Core/ITerrain.h>
#include <Core/AttributeTypes.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataRecord.h>
#include <Core/IFeatureObject.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/ICompositeObject.h>
#include <Core/ITemporary.h>
#include <Core/WorldMaintainer.h>
#include <Core/Point.h>
#include <Core/IBase.h>
#include <Core/IVisibility.h>
#include <Core/IMetadataCreator.h>
#include <Core/IDeletable.h>

#include <App/IApplication.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransaction.h>
#include <App/AccessElementUtils.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/IModelPickUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>
#include <VizUI/VizUIPlugin.h>
#include <VizUI/IMouse.h>

#include <Util/CoordinateConversionUtils.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIcon.h>
#include <Elements/IDrawTechnique.h>
#include <Elements/IRepresentation.h>
#include <Elements/IClamper.h>

#include <Transaction/TransactionPlugin.h>
#include <Transaction/IMovePointTransaction.h>
#include <Transaction/IDeleteObjectTransaction.h>
#include <Transaction/ICompositeAddObjectTransaction.h>

#include <SMCElements/ILayerComponent.h>

#include <Effects/IEvent.h>
#include <Elements/IEventFeature.h>
#include <Effects/IEventComponent.h>

#include <Effects/EffectPlugin.h>
#include <Core/ICompositeObject.h>
#include <SMCElements/IDataSourceComponent.h>

#include <Effects/IEventPlayer.h>

namespace SMCUI 
{

    DEFINE_META_BASE(SMCUI, EventUIHandler);
    DEFINE_IREFERENCED(EventUIHandler, UIHandler);

    EventUIHandler::EventUIHandler()
        :_offset(0.0f)
        ,_mode(SMCUI::IEventUIHandler::EVENT_MODE_NONE)
        ,_temporary(true)
        , _clamping(true)
        ,_dragged(false)
    {
        _addInterface(SMCUI::IEventUIHandler::getInterfaceName());
    }

    EventUIHandler::~EventUIHandler(){}

    void EventUIHandler::setFocus(bool value)
    {
        if(!value)
        {
            reset();
        }
        VizUI::UIHandler::setFocus(value);

    }

    //! Function called when added to UI Manager
    void EventUIHandler::onAddedToManager()
    {
        reset();
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void EventUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            if(wm.valid())
            {
                _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
            }

            CORE::RefPtr<CORE::IComponent> component = wm->getComponentByName("EventComponent");
            if(component.valid())
            {
                _eventComponent = component->getInterface<EFFECTS::IEventComponent>();
            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            _performHardReset();

        }
        else if(messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            if(_mode == EVENT_MODE_CREATE_POINT)
            {
                return;
            }

            _dragged = true;
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseDraggedMessageType)
        {
            // Query TerrainUIHandler interface
            _dragged = true;
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                osg::Vec3d pos;
                if (tph->getMousePickedPosition(pos, true, false))
                {
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleButtonDraggedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }
            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                const CORE::IntersectionList& ilist = tph->getCurrentIntersections();
                if(!ilist.empty())
                {
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                    _handleButtonDoubleClickedIntersection(mmsg->getMouse()->getButtonMask(), ilist.front());
                }

            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                osg::Vec3d pos;
                if (tph->getMousePickedPosition(pos, true,false))
                {
                    // Get the IMouseMessage interface
                    CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                    CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

                    if(_dragged)
                        _handleButtonDraggedIntersection(mmsg->getMouse()->getButtonMask(), pos);

                    _handleButtonReleasedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }
            }
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void EventUIHandler::_performHardReset()
    {
        reset();
    }

    void EventUIHandler::_createUndoMoveTransaction()
    {
        APP::IUndoTransactionFactory* transactionFactory = 
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> transaction = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MovePointTransactionType);

        CORE::RefPtr<TRANSACTION::IMovePointTransaction> movePointTransaction = 
        transaction->getInterface<TRANSACTION::IMovePointTransaction>();

        movePointTransaction->setPoint(_point.get());
        movePointTransaction->setPosition(_point->getValue());

        // execute the transaction 
        addAndExecuteUndoTransaction(transaction.get());
    }

    void EventUIHandler::_handleButtonDraggedIntersection(int button,const osg::Vec3d& longLatAlt)
    {
        if((_mode == EVENT_MODE_EDIT_POINT) && (button & VizUI::IMouse::LEFT_BUTTON))
        {
            if(_point.valid())
            {
                _point->setValue(longLatAlt);
            
                // XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg  = mf->createMessage(*IEventUIHandler::EventUpdatedMessageType);

                notifyObservers(*IEventUIHandler::EventUpdatedMessageType, *msg);
            }
        }
    }

    void EventUIHandler::_handleButtonDoubleClickedIntersection(int button, CORE::RefPtr<CORE::Intersection> intersection)
    {
        return;
    }

    void EventUIHandler::_handleButtonReleasedIntersection(int button, const osg::Vec3d& longLatAlt)
    {
        if((button & VizUI::IMouse::LEFT_BUTTON))
        {
            if (_mode == SMCUI::IEventUIHandler::EVENT_MODE_CREATE_POINT)
            {
                if(!_point.valid())
                {
                    create(true);

                    //Notify all current observers about the intersections found message
                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IEventUIHandler::EventCreatedMessageType);
                    notifyObservers(*IEventUIHandler::EventCreatedMessageType, *msg);
                }

                if(_point.valid())
                {
                    // Add the point to the line
                    _point->setValue(longLatAlt);
                }
            }            
        }
    }

    void EventUIHandler::create(bool addToWorld)
    {
        CORE::RefPtr<CORE::IObject> pointObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*EFFECTS::EffectRegistryPlugin::EventFeature);

        //XXX - hard coded icon 
        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
                component->getInterface<ELEMENTS::IIconLoader>();

            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = 
                pointObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolder.valid() && iconLoader.valid())
            {
                iconHolder->setIcon(iconLoader->getIconByName("circle"));
            }
        }

        //set the representation to icon mode
        CORE::RefPtr<ELEMENTS::IRepresentation> representation = 
            pointObject->getInterface<ELEMENTS::IRepresentation>();

        if(representation.valid())
        {
            representation->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
        }


        //set the current temporary state
        CORE::RefPtr<CORE::ITemporary> temp = pointObject->getInterface<CORE::ITemporary>();
        if(temp.valid())
        {
            temp->setTemporary(_temporary);
        }

        CORE::RefPtr<ELEMENTS::IClamper> clamp = pointObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            clamp->setClampOnPlacement(_clamping);
        }

        // Add the point to the world
        if ( addToWorld )
            getWorldInstance()->addObject(pointObject.get());

        _point = pointObject->getInterface<CORE::IPoint>();
        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = _point->getInterface<ELEMENTS::IDrawTechnique>();
        if(draw.valid())
        {
            draw->setHeightOffset(_offset);
        }


    }

    void EventUIHandler::reset()
    {
        setMode(EVENT_MODE_NONE);

        _point = NULL;
        _dragged = false;
        _temporary = true;

        _clamping = true;
    }

    void EventUIHandler::setOffset(double offset)
    {
        _offset = offset;
    }

    double EventUIHandler::getOffset() const
    {
        return _offset;
    }

    void EventUIHandler::initializeAttributes()
    {
        //UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "EventUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("Offset", "Offset",
            CORE::DoubleAttribute::SetFuncType(this, &EventUIHandler::setOffset),
            CORE::DoubleAttribute::GetFuncType(this, &EventUIHandler::getOffset),
            "Offset name",
            groupName));
    }

    //Function to set the current mode
    void EventUIHandler::setMode(IEventUIHandler::EventMode mode)
    {
        if(_mode == mode)
        {
            return;
        }

        _mode = mode;

        //if mode is create mode then disable the selection else enable selection on terrain
        if(_mode == EVENT_MODE_CREATE_POINT )
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
//                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(false);
            }
        }
        else if(_mode == EVENT_MODE_EDIT_ATTRIBUTES)
        {
            setSelectedEventAsCurrentPoint();
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);  
            }

        }
        else if(_mode == EVENT_MODE_EDIT_POINT)
        {
            setSelectedEventAsCurrentPoint();
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                //_subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);  
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);  
            }
        }
        else if(_mode == EVENT_MODE_NONE)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

            }

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(true);
            }
        }
    }

    IEventUIHandler::EventMode EventUIHandler::getMode()
    {
        return _mode;
    }

    void EventUIHandler::setSelectedEventAsCurrentPoint()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            _point = iter->second->getInterface<CORE::IPoint>();
        }
    }


    void EventUIHandler::setMetadataRecord(CORE::IMetadataRecord* record)
    {
        if(_point.valid())
        {

            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _point->getInterface<CORE::IMetadataRecordHolder>();
            if(holder.valid())
            {
                holder->setMetadataRecord(record);
                return;
            }
        }
    }

    CORE::IMetadataRecord*
    EventUIHandler::getMetadataRecord() const
    {
        if(_point.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _point->getInterface<CORE::IMetadataRecordHolder>();
            if(holder.valid())
            {
                return holder->getMetadataRecord();
            }
        }

        return NULL;
    }

    void 
    EventUIHandler::update()
    {
        if(_point.valid())
        {
            CORE::RefPtr<CORE::IFeatureObject> feature = 
                _point->getInterface<CORE::IFeatureObject>();
            if(feature.valid())
            {
                return feature->update();
            }
        }
    }

    void EventUIHandler::_addToLayer(CORE::IFeatureLayer *featureLayer)
    {
        if(!featureLayer)
            return;

        if(!_point.valid())
            return;

        try
        {
            CORE::RefPtr<CORE::ICompositeObject> composite = 
                featureLayer->getInterface<CORE::ICompositeObject>(true);

            _point->getInterface<CORE::IDeletable>()->remove();

            CORE::RefPtr<CORE::ITemporary> temp = _point->getInterface<CORE::ITemporary>();
            if(temp.valid())
            {
                temp->setTemporary(false);
            }

            // create a transaction to add the point to the layer
            APP::IUndoTransactionFactory* transactionFactory = 
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction = transactionFactory->createUndoTransaction
                (*TRANSACTION::TransactionRegistryPlugin::CompositeAddObjectTransactionType);

            CORE::RefPtr<TRANSACTION::ICompositeAddObjectTransaction> compositeAddObjectTransaction = 
                transaction->getInterface<TRANSACTION::ICompositeAddObjectTransaction>();

            compositeAddObjectTransaction->setCompositeObject(composite.get());
            compositeAddObjectTransaction->setObject(_point->getInterface<CORE::IObject>());

            // execute the transaction 
            addAndExecuteUndoTransaction(transaction.get());
        }
        catch(UTIL::Exception &e)
        {
            e.LogException();
        }

    }

    void EventUIHandler::getDefaultLayerAttributes(std::vector<std::string> &attrNames, 
        std::vector<std::string> &attrTypes, std::vector<int> &attrWidths, std::vector<int> &attrPrecisions) const
    {
            attrNames.push_back("NAME");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(1);
        
    }
    CORE::IFeatureLayer* EventUIHandler::getOrCreateEventLayer(const std::string& name)
    {
        CORE::RefPtr<CORE::IFeatureLayer> featureLayer = NULL;

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("LayerComponent");

        CORE::RefPtr<SMCElements::IDataSourceComponent> datasourceComponent = 
            component->getInterface<SMCElements::IDataSourceComponent>();

        CORE::RefPtr<ELEMENTS::IFeatureDataSource> datasource = datasourceComponent->getOrCreateFeatureDataSource("Events");

        if(datasource.valid())
        {
            CORE::RefPtr<CORE::ICompositeObject> composite = datasource->getInterface<CORE::ICompositeObject>();

            if(composite.valid())
            {
                const CORE::ObjectMap& objectMap = composite->getObjectMap();

                CORE::ObjectMap::const_iterator iter = objectMap.begin();

                while(iter != objectMap.end())
                {
                    CORE::RefPtr<CORE::IBase> base = iter->second->getInterface<CORE::IBase>();

                    if(base.valid())
                    {
                        if(base->getName() == name)
                        {
                            featureLayer = base->getInterface<CORE::IFeatureLayer>();
                            return featureLayer.get();
                        }
                    }

                    iter++;
                }
            }

            //if not found create a layer with the given name
            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecisions;

            getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecisions);

            CORE::RefPtr<CORE::IObject> object = 
                _createFeatureLayer(name, attrNames, attrTypes, attrWidths, attrPrecisions, composite);
            featureLayer = object->getInterface<CORE::IFeatureLayer>();
        }

        return featureLayer.get();
    }

    CORE::IMetadataTableDefn*
    EventUIHandler::getCurrentSelectedLayerDefn()
    {

        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateEventLayer(_eventTypeName);

        if(layer.valid())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                return holder->getMetadataTable()->getMetadataTableDefn();
            }

        }

        return NULL;
    }

    void EventUIHandler::removeCreatedEvent()
    {
        if(_point.valid())
        {
            _point->getInterface<CORE::IDeletable>()->remove();
            reset();
        }
    }

    void EventUIHandler::setTemporaryState(bool state)
    {
        _temporary = state;
    }

    bool EventUIHandler::getTemporaryState() const
    {
        return _temporary;
    }

    void EventUIHandler::setClampingState(bool state)
    {
        _clamping = state;
    }

    bool EventUIHandler::getClampingState() const
    {
        return _clamping;
    }

    CORE::RefPtr<CORE::IObject> 
        EventUIHandler::_createFeatureLayer(const std::string& layerName,
        std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes,
        std::vector<int>& attrWidths,
        std::vector<int>& attrPrecisions,
        CORE::ICompositeObject* composite)
    {
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
            CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IObject> featureLayer = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);

        CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
            featureLayer->getInterface<CORE::IMetadataTableHolder>();

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = 
            tableHolder->getMetadataTable();

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        // create a new table definition
        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataCreator->createMetadataTableDefn();

        for(unsigned int i = 0; i < attrNames.size(); i++)
        {

            // create fields to store name, latitude, longitude and height
            CORE::RefPtr<CORE::IMetadataFieldDefn> field = 
                metadataCreator->createMetadataFieldDefn();

            field->setName(attrNames.at(i));
            std::string fieldType = attrTypes.at(i);
            if(fieldType == "Text")
            {
                field->setType(CORE::IMetadataFieldDefn::STRING);
            }
            else if(fieldType == "Decimal")
            {
                field->setType(CORE::IMetadataFieldDefn::DOUBLE);
            }
            else if(fieldType == "Integer")
            {
                field->setType(CORE::IMetadataFieldDefn::INTEGER);
            }

            field->setLength(attrWidths.at(i));
            field->setPrecision(attrPrecisions.at(i));

            // add the field to the table definition
            tableDefn->addFieldDefn(field.get());
        }

        CORE::IFeatureLayer::FeatureLayerType featureLayerType = 
            CORE::IFeatureLayer::EVENT;
        

        metadataTable->setFeatureLayerType(featureLayerType);

        CORE::RefPtr<CORE::IBase> base = featureLayer->getInterface<CORE::IBase>();
        if(base.valid())
        {
            base->setName(layerName);
        }

        // set the field definition to the MetadataTable
        metadataTable->setMetadataTableDefn(tableDefn.get(),featureLayer->getInterface<CORE::IBase>()->getUniqueID().toString());

        composite->addObject(featureLayer.get());

        return featureLayer;

    }

    CORE::RefPtr<EFFECTS::IEvent>
        EventUIHandler::addEvent(const std::string &name, const std::string &type,
        const boost::posix_time::ptime &startTime, const boost::posix_time::ptime &endTime)
    {
        CORE::RefPtr<EFFECTS::IEvent> addedEvent;

        CORE::RefPtr<EFFECTS::IEventType> eventType = _eventComponent->getEventType(type);
        if(!eventType.valid())
        {
            return addedEvent;
        }

        CORE::RefPtr<CORE::IFeatureLayer> featureLayer;
        
        if(eventType->getAttachmentType() == EFFECTS::IEventType::GLOBAL)
        {
            create(false);
            _point->getInterface<ELEMENTS::IRepresentation>()->setRepresentationMode(ELEMENTS::IRepresentation::NONE);
        }

        if(!_point.valid())
        {
            return addedEvent;
        }

        featureLayer = getOrCreateEventLayer(type);

        _point->getInterface<CORE::IDeletable>()->remove();

        if(featureLayer.valid())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                featureLayer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = holder->getMetadataTable()->getMetadataTableDefn();

                CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder = _point->getInterface<CORE::IMetadataRecordHolder>();

                CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                    CORE::WorldMaintainer::instance();

                CORE::RefPtr<CORE::IComponent> component = 
                    worldMaintainer->getComponentByName("DataSourceComponent");

                CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
                    component->getInterface<CORE::IMetadataCreator>();

                //create metadata record for the min max point
                CORE::RefPtr<CORE::IMetadataRecord> record = 
                    metadataCreator->createMetadataRecord();

                // set the table Definition to the records
                record->setTableDefn(tableDefn.get());

                recordHolder->setMetadataRecord(record.get());
            }

        }

        _addToLayer(featureLayer.get());

        _point->getInterface<CORE::IBase>()->setName(name);
        _point->getInterface<CORE::ITemporary>()->setTemporary(false);

        CORE::RefPtr<ELEMENTS::IEventFeature> eventFeature = _point->getInterface<ELEMENTS::IEventFeature>();
        CORE::RefPtr<EFFECTS::IEventPlayer> eventPlayer   = eventFeature->getEventPlayer();

        addedEvent = _eventComponent->getEvent(type);

        eventPlayer->addEvent(addedEvent, startTime, endTime);

        return addedEvent;

    }
} // namespace SMCUI

