#include <osgEarthAnnotation/AnnotationNode>
#include <osgEarthAnnotation/LabelNode>
#include <osgEarthAnnotation/AnnotationData>
#include <osgEarthAnnotation/FeatureNode>
#include <osgEarthAnnotation/PlaceNode>
#include <osgEarthDrivers/model_feature_geom/FeatureGeomModelOptions>
#include <osgEarthDrivers/feature_ogr/OGRFeatureOptions>
#include <osgEarthDrivers/kml/KMLOptions>
#include <osgEarthDrivers/kml/KML>
#include <osgEarthDrivers/feature_ogr/OGRFeatureOptions>

#include <Core/CoreRegistry.h>
#include <Core/Point.h>
#include <Core/Line.h>
#include <Core/IPolygon.h>
#include <Core/IWorld.h>
#include <Core/CoreRegistry.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataRecordHolder.h>

#include <Elements/ElementsUtils.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IOSGEarthDrawTechnique.h>


#include <SMCUI/AddVectorUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <Terrain/IVectorObject.h>
#include <Terrain/VectorObject.h>
#include <Terrain/TerrainPlugin.h>
#include <Terrain/ModelObject.h> 

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>

#include <Util/Exception.h>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <Core/IText.h>
#include <Util/FileUtils.h>
#include <Util/CoordinateConversionUtils.h> 

#include <GIS/IDynamicProfileObject.h> 
#include <GIS/GISPlugin.h>

namespace SMCUI
{
    CORE::IObject* 
    AddVectorUIHandler::_createLineObjectfromFeature(osgEarth::Features::Feature *feature)
    {
        if(!feature)
            return NULL;

        osgEarth::Features::Geometry *geometry = feature->getGeometry();
        if(!geometry)
            return NULL;

        CORE::ILine* line = NULL;

        //Getting the line Feature Object
        CORE::IObject *object = _createObjectOfTypeFromFactory(*ELEMENTS::ElementsRegistryPlugin::LineFeatureObjectType.get());

        line = object->getInterface<CORE::ILine>(true);

        unsigned int numPoints = geometry->size();
        
        //Configure the points
        for(unsigned int currPoint = 0; currPoint < numPoints; ++currPoint)
        {
            line->addPoint(geometry->at(currPoint));
        }

        ELEMENTS::IOSGEarthDrawTechnique* drawTechnique = object->getInterface<ELEMENTS::IOSGEarthDrawTechnique>();

        if(drawTechnique)
        {
            osgEarth::Symbology::Style style = feature->style().value();
            osgEarth::Symbology::LineSymbol* lineSymbol = style.getSymbol<osgEarth::Symbology::LineSymbol>();
            if(lineSymbol)
            {
                osg::Vec4f color = lineSymbol->stroke()->color();
                float width = lineSymbol->stroke()->width().value();

                CORE::IPenStyle* penStyle = object->getInterface<CORE::IPenStyle>();

                if(penStyle)
                {        
                    penStyle->setPenColor(color);
                    penStyle->setPenWidth(width);
                }
            }
        }

        return object;
    }

    CORE::IObject* 
    AddVectorUIHandler::_createPolygonObjectFromFeature(osgEarth::Features::Feature *feature)
    {
        if(!feature)
            return NULL;

        osgEarth::Features::Geometry *geometry = feature->getGeometry();
        if(!geometry)
            return NULL;

        if(geometry->getType()!=osgEarth::Symbology::Geometry::TYPE_POLYGON)
            return NULL;

        CORE::IPolygon* polygonFeatureObject;

        //Getting the line Feature Object
        CORE::IObject *object = _createObjectOfTypeFromFactory(*ELEMENTS::ElementsRegistryPlugin::PolygonFeatureObjectType.get());
        polygonFeatureObject = object->getInterface<CORE::IPolygon>();

        osgEarth::Symbology::Polygon *polygon = dynamic_cast<osgEarth::Symbology::Polygon*>(geometry);

        //Configuring the outer ring of the polyon
        CORE::RefPtr<CORE::ILine> line = new CORE::Line();

        unsigned int numPointsRing = polygon->size();

        for(unsigned int currPoint=0; currPoint<numPointsRing; ++currPoint)
        {
            line->addPoint(polygon->at(currPoint));
        }

        polygonFeatureObject->setExteriorRing(line);

        //configuring the rings
        //external ring is already configured
        
        osgEarth::Symbology::RingCollection& ringCollection = polygon->getHoles();
        unsigned int numRings = ringCollection.size();

        
        for(unsigned int currRing = 0; currRing < numRings; ++currRing)
        {
            osg::ref_ptr<osgEarth::Symbology::Ring> ring = ringCollection[currRing];
            CORE::RefPtr<CORE::ILine> line = new CORE::Line();
            unsigned int numPoints = ring->size();
            for(unsigned int currPoint = 0; currPoint < numPoints; ++currPoint)
            {
                line->addPoint(ring->at(currPoint));
            }

            polygonFeatureObject->addInteriorRing(line);
        }

        ELEMENTS::IOSGEarthDrawTechnique* drawTechnique = object->getInterface<ELEMENTS::IOSGEarthDrawTechnique>();

        if(drawTechnique)
        {
            osgEarth::Symbology::Style style = feature->style().value();
            osgEarth::Symbology::LineSymbol* lineSymbol = style.getSymbol<osgEarth::Symbology::LineSymbol>();
            CORE::IPenStyle* penStyle = object->getInterface<CORE::IPenStyle>();
            if(penStyle)
            {
                if(lineSymbol)
                {
                    osg::Vec4f color = lineSymbol->stroke()->color();
                    float width = lineSymbol->stroke()->width().value();

                    penStyle->setPenColor(color);
                    penStyle->setPenWidth(width);
                    penStyle->setPenState((CORE::IPenStyle::PenStyleState)(CORE::IPenStyle::ON | CORE::IPenStyle::PROTECTED));
                }
            }
            else
            {
                penStyle->setPenState((CORE::IPenStyle::PenStyleState)(CORE::IPenStyle::OFF | CORE::IPenStyle::PROTECTED));
            }

            osgEarth::Symbology::PolygonSymbol* polygonSymbol = style.getSymbol<osgEarth::Symbology::PolygonSymbol>();
            CORE::IBrushStyle* brushStyle = object->getInterface<CORE::IBrushStyle>();
            if(brushStyle)
            {
                if(polygonSymbol)
                {
                    osg::Vec4f color = polygonSymbol->fill()->color();
                    brushStyle->setBrushFillColor(color);
                    brushStyle->setBrushState((CORE::IBrushStyle::BrushState)(CORE::IBrushStyle::ON | CORE::IBrushStyle::PROTECTED));
                }
                else
                {
                    brushStyle->setBrushState((CORE::IBrushStyle::BrushState)(CORE::IBrushStyle::OFF | CORE::IBrushStyle::PROTECTED));
                }
            }
        }

        return object;
    }

    CORE::IObject* 
    AddVectorUIHandler::_createPointObjectFromPlaceNode(osgEarth::Annotation::AnnotationNode *annotationNode)
    {
        CORE::RefPtr<CORE::IObject> object=NULL;

        if(!annotationNode)
            return NULL;

        try
        {
            osgEarth::Annotation::PlaceNode *placeNode = dynamic_cast<osgEarth::Annotation::PlaceNode*>(annotationNode);
            if(placeNode)
            {
                //Getting the line Feature Object
                object = _createObjectOfTypeFromFactory(*ELEMENTS::ElementsRegistryPlugin::OSGEarthPointObjectType);
                if(object.valid())
                {
                    object->getInterface<ELEMENTS::IOSGEarthDrawTechnique>(true)->setAnnotationNode(placeNode);

                    object->getInterface<CORE::IPoint>(true)->setValue(osg::Vec3d(placeNode->getPosition().x(), 
                                                                   placeNode->getPosition().y(),
                                                                   placeNode->getPosition().z()));

                    object->getInterface<CORE::IBase>(true)->setName(placeNode->getText());
                    object->getInterface<CORE::IText>()->setTextActive(true);
                }
            }

#if 0 //Will include support for label nodes in future 
            osgEarth::Annotation::LabelNode *labelNode = dynamic_cast<osgEarth::Annotation::LabelNode*>(annotationNode);
            if(labelNode)
            {
                object = _createObjectOfTypeFromFactory(*ELEMENTS::ElementsRegistryPlugin::OSGEarthPointObjectType);
                if(object.valid())
                {
                    object->getInterface<ELEMENTS::IOSGEarthDrawTechnique>(true)->setAnnotationNode(labelNode);
                
                    object->getInterface<CORE::IPoint>(true)->setValue(osg::Vec3d(labelNode->getPosition().x(), 
                                                                   labelNode->getPosition().y(),
                                                                   labelNode->getPosition().z()));

                    object->getInterface<CORE::IBase>(true)->setName(labelNode->text());
                }
            }
#endif
        }
        catch(UTIL::Exception &e)
        {
            e.LogException();
            return NULL;
        }

        return object.release();
    }

    CORE::IObject* 
    AddVectorUIHandler::_createObjectOfTypeFromFactory(CORE::IObjectType &objectType)
    {
        if(_objectFactory.valid())
        {
            return _objectFactory->createObject(objectType);
        }

        return NULL;
    }

    DEFINE_META_BASE(SMCUI, AddVectorUIHandler);
    DEFINE_IREFERENCED(AddVectorUIHandler, VizUI::UIHandler);

    AddVectorUIHandler::AddVectorUIHandler()
        : _iconFileName(""), _verticalOffset(50.0), _isClamp(false), _isEditable(false)
    {
        _addInterface(IAddVectorUIHandler::getInterfaceName());
        setName(getClassname()); 
    }

    AddVectorUIHandler::~AddVectorUIHandler(){}

    void
    AddVectorUIHandler::onAddedToManager()
    {
        try
        {
            //Subscribing to World loaded Message type to
            CORE::RefPtr<CORE::IWorldMaintainer> wm = 
                APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IUIHandlerManager>(getManager());

            _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);

        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void
    AddVectorUIHandler::onRemovedFromManager()
    {
        try
        {
            //Subscribing to World loaded Message type to
            CORE::RefPtr<CORE::IWorldMaintainer> wm = 
                APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IUIHandlerManager>(getManager());

            _unsubscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void
    AddVectorUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            //Subscribing to postTerrainMessage type to
            CORE::IWorld *world = ELEMENTS::GetFirstWorldFromMaintainer();
            if(world)
            {
                if(world->getTerrain())
                {
                    _subscribe(world->getTerrain(), *CORE::ITerrain::PostTerrainChangeMessageType);
                }
            }
        }
        if(messageType == *CORE::ITerrain::PostTerrainChangeMessageType)
        {
            _configureFeatureQueryTool();
        }
    }

    void
    AddVectorUIHandler::addVectorData(const std::string &url, osgEarth::Symbology::Style &style)
    {

        if(osgDB::getFileExtension(url)=="kml")
        {
            _addKMLToWorld(url);
            return;
        }

        //Just handling KML only right now
        return;
    }

    void
    AddVectorUIHandler::setOffset(double offset)
    {
        _verticalOffset = offset;
    }

    double
    AddVectorUIHandler::getOffset() const
    {
        return _verticalOffset;
    }

    void
    AddVectorUIHandler::setClamping(bool clamping)
    {
        _isClamp = clamping;
    }

    bool
    AddVectorUIHandler::getClamping() const
    {
        return _isClamp;;
    }

    void
    AddVectorUIHandler::setEditable(bool editable)
    {
        _isEditable = editable;
    }

    bool
    AddVectorUIHandler::getEditable() const
    {
        return _isEditable;;
    }

    void
    AddVectorUIHandler::setIconFileName(std::string iconFileName)
    {
        _iconFileName = iconFileName;
    }

    std::string
    AddVectorUIHandler::getIconFileName() const
    {
        return _iconFileName;
    }

    void 
    AddVectorUIHandler::_configureFeatureQueryTool()
    {
        //Configuring the creator
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
        if(worldMaintainer.valid())
        {
            CORE::RefPtr<CORE::IComponent> component = 
                worldMaintainer->getComponentByName("DataSourceComponent");
            if(component.valid())
            {
                _metaDataCreator = component->getInterface<CORE::IMetadataCreator>();
            }
        }

        //Getting the object factory
        _objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
    }
    
    void 
    AddVectorUIHandler::_updateNodeMask(osg::Node *node)
    {
        if(!node)
            return;

        osg::Group *group = node->asGroup();
        if(group)
        {
            for(unsigned int numChild=0; numChild<group->getNumChildren(); ++numChild)
            {
                _updateNodeMask(group->getChild(numChild));
            }
        }
        else
        {
            node->setName("kml");
        }
        
        node->setNodeMask(-1);
    }

    bool 
    AddVectorUIHandler::_createFeatureObjectFromGroupNode(osg::Node *node, const std::string& folderName, CORE::ICompositeObject* featureDatasource)
    {
        if(!node)
            return true;

        //If the node is group node we are creating the new feature layer for that
        //with description and name as the metadata
        osgEarth::Annotation::AnnotationNode *an = dynamic_cast<osgEarth::Annotation::AnnotationNode*>(node);
        if(an)
        {
            //Means we have to create the object from the node and add to the layer
            CORE::IObject *object = _createObjectFromNode(node);

            CORE::IFeatureLayer::FeatureLayerType type  =  _getFeatureLayerType(node);
            CORE::IFeatureLayer* featureLayer            =  _getOrCreateFeatureLayer(folderName, type, featureDatasource);

            if(object && featureLayer)
            {
                //Copy the value of the metaData
                _updateMetaDataObject(object, featureLayer, node);
                featureLayer->getInterface<CORE::ICompositeObject>()->addObject(object);
                if(type == CORE::IFeatureLayer::POINT)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        else
        {
            osg::Group *group = node->asGroup();
            if(group)
            {
                osg::Referenced* referenced = group->getUserData();
                
                if(referenced)
                {
                    osgEarth::Annotation::AnnotationData* annotationData = dynamic_cast<osgEarth::Annotation::AnnotationData*>(referenced);
                    if(annotationData)
                    {
                        std::string dataSourceName = annotationData->getName();

                        CORE::RefPtr<CORE::IObjectFactory> objectFactory =  
                            CORE::CoreRegistry::instance()->getObjectFactory();
                        CORE::RefPtr<CORE::IObject> folderDataSource = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureDataSourceType);

                        folderDataSource->getInterface<CORE::IBase>()->setName(dataSourceName);
                        featureDatasource->addObject(folderDataSource);


                        //Checking if the group has children
                        unsigned int numChildren = group->getNumChildren();
                        //Getting the child of each Layer
                        for(unsigned int currChild = 0; currChild<numChildren; ++currChild)
                        {
                            _createFeatureObjectFromGroupNode(group->getChild(currChild), dataSourceName, folderDataSource->getInterface<CORE::ICompositeObject>());
                        }
                    }
                }
                else
                {
                    //Checking if the group has children
                    unsigned int numChildren = group->getNumChildren();
                    //Getting the child of each Layer
                    for(unsigned int currChild = 0; currChild<numChildren; ++currChild)
                    {
                        bool result = _createFeatureObjectFromGroupNode(group->getChild(currChild), folderName, featureDatasource);
                        if(result)
                        {
                            break;
                        }
                    }
                }
            }
        }
        return false;
    }

    CORE::IFeatureLayer::FeatureLayerType
    AddVectorUIHandler::_getFeatureLayerType(osg::Node *node)
    {
        osgEarth::Annotation::PlaceNode *placeNode = dynamic_cast<osgEarth::Annotation::PlaceNode*>(node);
        if(placeNode)
        {
            return CORE::IFeatureLayer::POINT;
        }
#if 0 //Not supported 
        osgEarth::Annotation::LabelNode *labelNode = dynamic_cast<osgEarth::Annotation::LabelNode*>(node);
        if(labelNode)
        {
            return CORE::IFeatureLayer::POINT;
        }
#endif

        osgEarth::Annotation::FeatureNode *featureNode = dynamic_cast<osgEarth::Annotation::FeatureNode*>(node);
        if(featureNode)
        {
            osgEarth::Features::Feature *feature = featureNode->getFeature();
            if(feature)
            {
                osgEarth::Features::Geometry *geometry = feature->getGeometry();
                if(geometry)
                {
                    osgEarth::Features::Geometry::Type type = geometry->getType();
                    switch(type)
                    {
                        case osgEarth::Features::Geometry::TYPE_POINTSET:
                        {
                            return CORE::IFeatureLayer::POINT;
                        }
                        break;

                        case osgEarth::Features::Geometry::TYPE_POLYGON:
                        {
                            return CORE::IFeatureLayer::POLYGON;

                        }
                        break;

                        case osgEarth::Features::Geometry::TYPE_LINESTRING:
                        {
                            return CORE::IFeatureLayer::LINE;
                        }
                        break;
                    }
                }
            }
        }

        return CORE::IFeatureLayer::UNKNOWN;
    }

    CORE::IFeatureLayer*
    AddVectorUIHandler::_getOrCreateFeatureLayer(const std::string& folderName, CORE::IFeatureLayer::FeatureLayerType layerType, CORE::ICompositeObject* featureDatasource)
    {
        CORE::IObject* featureLayerObject = NULL;
        CORE::RefPtr<CORE::IFeatureLayer> featureLayer = NULL;

        if(_kmlFolderMap.find(folderName) != _kmlFolderMap.end())
        {
            RasterLayerMap& rasterLayerMap = _kmlFolderMap[folderName];
            if(layerType == CORE::IFeatureLayer::POINT)
            {
                if(rasterLayerMap.find("Placemarks") != rasterLayerMap.end())
                {
                    featureLayer = rasterLayerMap["Placemarks"];
                    return featureLayer;
                }
            }
            else if(layerType == CORE::IFeatureLayer::LINE)
            {
                if(rasterLayerMap.find("Paths") != rasterLayerMap.end())
                {
                    featureLayer = rasterLayerMap["Paths"];
                    return featureLayer;
                }
            }
            else if(layerType == CORE::IFeatureLayer::POLYGON)
            {
                if(rasterLayerMap.find("Polygons") != rasterLayerMap.end())
                {
                    featureLayer = rasterLayerMap["Polygons"];
                    return featureLayer;
                }
            }
        }

        CORE::RefPtr<CORE::IObjectFactory> objectFactory =  
            CORE::CoreRegistry::instance()->getObjectFactory();

        featureLayerObject = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);
        if(featureLayerObject == NULL)
        {
            LOG_ERROR("Failed to create FeatureLayer instance");
            return NULL;
        }

        featureLayer = featureLayerObject->getInterface<CORE::IFeatureLayer>();

        {

            CORE::RefPtr<CORE::IMetadataTableHolder> metadataTableHolder = featureLayerObject->getInterface<CORE::IMetadataTableHolder>();
            CORE::RefPtr<CORE::IMetadataTable> metadataTable = metadataTableHolder->getMetadataTable();
            if(!metadataTable.valid())
            {
                LOG_ERROR("IMetadataTable interface not found in FeatureLayer");
                return NULL;
            }

            // set the layer type
            CORE::IFeatureLayer::FeatureLayerType featureLayerType = layerType;
            featureLayerObject->getInterface<CORE::IFeatureLayer>()->setFeatureLayerType(featureLayerType);

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = _metaDataCreator->createMetadataTableDefn();
            //Setting just the name and description as the table attribute
            if(!tableDefn.valid())
            {
                LOG_ERROR("Unable to create a IMetadataTableDefn instance");
                return NULL;
            }

            // set the layer name
            std::string layerName;

            if(layerType == CORE::IFeatureLayer::POINT)
            {
                layerName = "Placemarks";
            }
            else if(layerType == CORE::IFeatureLayer::LINE)
            {
                layerName = "Paths";
            }
            else if(layerType == CORE::IFeatureLayer::POLYGON)
            {
                layerName = "Polygons";
            }

            featureLayerObject->getInterface<CORE::IBase>()->setName(layerName);

            // set the fields
            CORE::RefPtr<CORE::IMetadataFieldDefn> nameField = 
             _metaDataCreator->createMetadataFieldDefn();

            CORE::RefPtr<CORE::IMetadataFieldDefn> descriptionField = 
                _metaDataCreator->createMetadataFieldDefn();

            nameField->setType(CORE::IMetadataFieldDefn::STRING);
            descriptionField->setType(CORE::IMetadataFieldDefn::STRING);

            nameField->setName("Name");
            descriptionField->setName("Description");

            nameField->setLength(64);
            descriptionField->setLength(1024);

            tableDefn->addFieldDefn(nameField);
            tableDefn->addFieldDefn(descriptionField);

            metadataTable->setMetadataTableDefn(tableDefn.get(),featureLayerObject->getInterface<CORE::IBase>()->getUniqueID().toString());

            //add the layer to KMLFolderMap
            _kmlFolderMap[folderName][layerName] = featureLayerObject->getInterface<CORE::IFeatureLayer>();

            //add the layer to datasource
            featureDatasource->addObject(featureLayerObject);
        }

        return featureLayer.release();
    }

    void 
    AddVectorUIHandler::_updateMetaDataObject(CORE::IObject *object, CORE::IFeatureLayer *featureLayer, osg::Node *node)
    {
        if(!object || !featureLayer || !node)
            return;

        try
        {
            //Getting the table defination from the layer
            CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = featureLayer->getInterface<CORE::IMetadataTableHolder>(true);
            CORE::RefPtr<CORE::IMetadataTableDefn> tableDef      = tableHolder->getMetadataTableDefn();
            if(!tableDef.valid())
                return;

            CORE::RefPtr<CORE::IMetadataRecord> record = _metaDataCreator->createMetadataRecord();
            record->setTableDefn(tableDef.get());

            osg::Referenced *referenced =  node->getUserData();
            osgEarth::Annotation::AnnotationData *anntotationData = dynamic_cast<osgEarth::Annotation::AnnotationData*>(referenced);
            if(anntotationData)
            {
                std::string name = anntotationData->getName();
                std::string description = anntotationData->getDescription();

                //Setting the field
                CORE::RefPtr<CORE::IMetadataField> nfield = record->getFieldByName("Name");
                CORE::RefPtr<CORE::IMetadataField> dfield = record->getFieldByName("Description");
                if(nfield.valid())
                    nfield->fromString(name);
                if(dfield.valid())
                    dfield->fromString(description);

                object->getInterface<CORE::IMetadataRecordHolder>(true)->setMetadataRecord(record);
            }
        }
        catch(...)
        {
        }
    }

    CORE::IObject*
    AddVectorUIHandler::_createObjectFromNode(osg::Node *node)
    {
        CORE::IObject *object = NULL;
        if(!node)
            return NULL;

        //getting type of the node from the feature layer type funtion which
        //checks for the node type
        CORE::IFeatureLayer::FeatureLayerType type = _getFeatureLayerType(node);
        if(type==CORE::IFeatureLayer::UNKNOWN)
            return NULL;

        if(type==CORE::IFeatureLayer::POINT)
        {
            osgEarth::Annotation::AnnotationNode *annotationNode = dynamic_cast<osgEarth::Annotation::AnnotationNode*>(node);
            object = _createPointObjectFromPlaceNode(annotationNode);
        }
        else if(type==CORE::IFeatureLayer::LINE)
        {
            osgEarth::Annotation::FeatureNode *featureNode = dynamic_cast<osgEarth::Annotation::FeatureNode*>(node);
            if(featureNode != NULL)
            {
                object = _createLineObjectfromFeature(featureNode->getFeature());
            }
        }
        else if(type==CORE::IFeatureLayer::POLYGON)
        {
            osgEarth::Annotation::FeatureNode *featureNode = dynamic_cast<osgEarth::Annotation::FeatureNode*>(node);
            if(featureNode != NULL)
            {
                object = _createPolygonObjectFromFeature(featureNode->getFeature());
            }
        }

        return object;
    }

    void 
    AddVectorUIHandler::_addKMLToWorld(const std::string &url)
    {
        osgEarth::Drivers::KMLOptions kml_options;

        kml_options.iconMaxSize() = 8;
        kml_options.declutter()      = true;
        std::string dataDir = UTIL::getDataDirPath();

        bool exist = osgDB::fileExists(dataDir + "/Symbols/hcircle.png");
        if(exist)
        {
            osg::ref_ptr<osgEarth::IconSymbol> icon = new osgEarth::IconSymbol; 
            osg::Image* image = new osg::Image;
            image->setFileName(dataDir + "/Symbols/circle.png");
            icon->setImage(image);
            kml_options.defaultIconSymbol() = icon;
            icon = NULL;
        }

        osgEarth::MapNode *mapNode            = ELEMENTS::GetMapNodeFromTerrain();
        osg::ref_ptr<osg::Node> kml            = osgEarth::Drivers::KML::load(osgEarth::URI(url), mapNode, kml_options);
        
        if(!kml.valid())
            return; //Unable to parse the KML file

        

        //Updating the node mask for(for setting the visiblility always on
        _updateNodeMask(kml.get());

        CORE::RefPtr<CORE::IObjectFactory> objectFactory =  
            CORE::CoreRegistry::instance()->getObjectFactory();


        CORE::RefPtr<CORE::IObject> featureDataSource = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureDataSourceType);

        std::string dataSourceName = osgDB::getStrippedName(url);
        featureDataSource->getInterface<CORE::IBase>()->setName(dataSourceName);
        
        //Adding each to the world
        osg::Group *topGroup = kml->asGroup();
        if(topGroup)
        {
            osg::Group *group = topGroup->getChild(0)->asGroup();
            for(unsigned int currChild=0; currChild<group->getNumChildren(); ++currChild)
            {
                _createFeatureObjectFromGroupNode(group->getChild(currChild), dataSourceName, featureDataSource->getInterface<CORE::ICompositeObject>());
            }
        }

        //Now adding the feature data source to the world
        CORE::IWorld *world = ELEMENTS::GetFirstWorldFromMaintainer();
        world->addObject(featureDataSource.get());
    }

    void AddVectorUIHandler::createProfileFromVector(std::vector<osg::Vec2d> relPointVec)
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());
        const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();
        CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();

        TERRAIN::IModelObject* modelObject;

        if (iter != map.end())
        {
            modelObject = iter->second->getInterface<TERRAIN::IModelObject>();
        }

        std::string url = modelObject->getUrl();

        CORE::RefPtr<CORE::IObject>  modelObjectObject = modelObject->getInterface<CORE::IObject>();

        CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IObject> dynamicProfile = objectFactory->createObject(*GIS::GISRegistryPlugin::DynamicProfileObjectType);
        CORE::IBase* dynamicProfileBase = dynamicProfile->getInterface<CORE::IBase>();

        dynamicProfileBase->setName(osgDB::getNameLessExtension(_profileName));

        CORE::RefPtr<GIS::IDynamicProfileObject> dynamicProfileObject = dynamicProfile->getInterface<GIS::IDynamicProfileObject>();

        dynamicProfileObject->setProfile(_profileName);
        dynamicProfileObject->setTexture(_textureFile);
        dynamicProfileObject->setBaseVectorFile(url);
        dynamicProfileObject->setDistanceProfileMap(_distanceProfileObject);

        CORE::RefPtr<CORE::ICompositeObject> modelObjectParentComp = modelObjectObject->getParent()->getInterface<CORE::ICompositeObject>();
        CORE::IBase* modelObjectParentBase = modelObjectParentComp->getInterface<CORE::IBase>();
        std::string name = modelObjectParentBase->getName(); 

        if (name == "Folder Object")
        {
            modelObjectParentComp->addObject(dynamicProfile); 
        }
        else
        {
            CORE::RefPtr<CORE::IObject> folderObject = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FolderType);
            CORE::IBase* folderbase = folderObject->getInterface<CORE::IBase>();
            folderbase->setName("Folder Object");

            CORE::RefPtr<CORE::ICompositeObject> compositeObject = folderObject->getInterface<CORE::ICompositeObject>();

            compositeObject->addObject(dynamicProfile);

            modelObjectParentComp->removeObjectByID(&(modelObjectObject->getInterface<CORE::IBase>()->getUniqueID()));

            compositeObject->addObject(modelObjectObject.get());

            modelObjectParentComp->addObject(compositeObject->getInterface<CORE::IObject>());

        }
    }

    void AddVectorUIHandler::setDistanceProfileObject(std::string profileName, std::map<double, std::string> distanceProfileObject)
    {
        _distanceProfileObject = distanceProfileObject; 
        _profileName = profileName; 
    }

    void AddVectorUIHandler::setProfileTexture(std::string texturefile)
    {
        _textureFile = texturefile; 
    }

} // namespace SMCUI

