#include <SMCUI/ContoursGenerationUIHandler.h>

#include <Terrain/IRasterObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <Core/ITerrain.h>

#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, ContoursGenerationUIHandler);

    DEFINE_IREFERENCED(ContoursGenerationUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    ContoursGenerationUIHandler::ContoursGenerationUIHandler()
    {
        _addInterface(IContoursGenerationUIHandler::getInterfaceName());
        setName(getClassname());
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _extents = new osg::Vec2Array;
        _elevationInterval=0.0;
        _offSetVal=0.0;

        _reset();
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    ContoursGenerationUIHandler::~ContoursGenerationUIHandler(){}

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ContoursGenerationUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void ContoursGenerationUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ContoursGenerationUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool ContoursGenerationUIHandler::getFocus() const
    {
        return false;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ContoursGenerationUIHandler::execute()
    {
        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(vfactory)
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if(world)
            {
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::ContourFilterVisitorType);

                if(!_visitor)
                {
                    LOG_ERROR("Failed to create filter class");
                    return;
                }

                GISCOMPUTE::IContourFilterVisitor* iVisitor  = 
                    _visitor->getInterface<GISCOMPUTE::IContourFilterVisitor>();


                //set Elevation Interval
                iVisitor->setElevationInterval(_elevationInterval);
                //set offset val
                iVisitor->setOffset(_offSetVal);
                //set color 
                iVisitor->setColor(_color);

                if(_extents->size() >1)
                {
                    //set extent on visitor
                    iVisitor->setExtents(_extents.get());
                }
                // set output name
                iVisitor->setOutputName(_name);

                //set style
                iVisitor->setStyle(_styleJsonPath);

                CORE::IBase* applyNode = NULL;
                if(!_elevObj)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    applyNode = terrain->getInterface<CORE::IBase>();
                }
                else
                {
                    applyNode = _elevObj->getInterface<CORE::IBase>();
                }

                //subscribe for filter status
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                applyNode->accept(*_visitor);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ContoursGenerationUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::IContourFilterVisitor> visitor = 
                        base->getInterface<GISCOMPUTE::IContourFilterVisitor>();

                    if(visitor.valid())
                    {
                         //CORE::RefPtr<CORE::IFeatureObject> object = visitor->getOutput();
                        CORE::RefPtr<CORE::IObject> object = visitor->getOutput();

                        if(object)
                        {
                            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                            if(world)
                            {
                                osg::ref_ptr<osg::Node> node = object->getInterface<CORE::IObject>()->getOSGNode();
                                if(node.valid())
                                {
                                    osg::ref_ptr<osg::StateSet> stateSet = node->getOrCreateStateSet();
                                    stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::PROTECTED);
                                }

                                world->addObject(object->getInterface<CORE::IObject>(true));
                                /*CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                                terrain->addRasterObject(object);*/
                            }
                        }
                        else
                        {
                            throw UTIL::Exception("Failed to create raster object", __FILE__, __LINE__);
                        }
                    }
                }
            }
            catch(...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void ContoursGenerationUIHandler::_reset()
    {
        _extents->clear();
        _elevObj = NULL;
        _visitor = NULL;
        _elevationInterval=0.0;
        _offSetVal=0.0;
        _name = "";
    }

    void ContoursGenerationUIHandler::setExtents(osg::Vec4d extents)
    {
        _extents->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _extents->push_back(osg::Vec2d(first.x(), first.y()));
        _extents->push_back(osg::Vec2d(second.x(), second.y()));
    }
    void ContoursGenerationUIHandler::setElevationInterval(double ElevationIntVal)
    {
        _elevationInterval=ElevationIntVal;
    }
    void ContoursGenerationUIHandler::setOffset(double offset)
    {
        _offSetVal=offset;

    }
    void ContoursGenerationUIHandler::setUserDefineColor(osg::Vec4d ocolor)
    {
        _color=ocolor;

    }
    double ContoursGenerationUIHandler::getElevationInterval() const 
    {
        return _elevationInterval;
    }
    double ContoursGenerationUIHandler::getOffset() const 
    {
        return _offSetVal;

    }
    osg::Vec4d ContoursGenerationUIHandler::getUserDefineColor() const 
    {
        return _color;

    }
    osg::Vec4d ContoursGenerationUIHandler::getExtents() const
    {
        osg::Vec4d extents(0.0, 0.0, 0.0, 0.0);
        osg::Vec2d first = _extents->at(0);
        osg::Vec2d second = _extents->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    void ContoursGenerationUIHandler::setElevationObject(TERRAIN::IElevationObject* obj)
    {
        _elevObj = obj;
    }

    void ContoursGenerationUIHandler::setOutputName(std::string& name)
    {
        _name = name;
    }

    void ContoursGenerationUIHandler::stopFilter()
    {
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }
    
    void ContoursGenerationUIHandler::setStyle(const std::string &value)
    {
        _styleJsonPath = value;
    }
    
    std::string ContoursGenerationUIHandler::getStyle()
    {
        return _styleJsonPath;
    }

} // namespace SMCUI

