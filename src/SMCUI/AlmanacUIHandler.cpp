/****************************************************************************
 *
 * File             : AlmanacGUI.h
 * Description      : AlmanacGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCUI/AlmanacUIHandler.h>

#include <Util/MoonPhaseCalculator.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, AlmanacUIHandler);
    DEFINE_IREFERENCED(SMCUI::AlmanacUIHandler, VizUI::UIHandler);

    AlmanacUIHandler::AlmanacUIHandler()
        : _longlatAlt(osg::Vec3(0, 0, 0))
    {
        _addInterface(IAlmanacUIHandler::getInterfaceName());
    }

    AlmanacUIHandler::~AlmanacUIHandler()
    {}

    void 
    AlmanacUIHandler::setPosition(const osg::Vec3& longlatAlt)
    {
        _longlatAlt = longlatAlt;
    }

    const osg::Vec3 
    AlmanacUIHandler::getPosition() const
    {
        return _longlatAlt;
    }

    void
    AlmanacUIHandler::setDateTime(const boost::posix_time::ptime& stime)
    {
        _dateTime = stime;
    }

    const boost::posix_time::ptime& 
    AlmanacUIHandler::getDateTime() const
    {
        return _dateTime;
    }

    void 
    AlmanacUIHandler::computeAlmanac()
    {
        // Get Local time
        boost::posix_time::ptime::date_type date = _dateTime.date();
        boost::posix_time::ptime::time_duration_type time = _dateTime.time_of_day();
        double hours = time.hours();
        double minutes = time.minutes();
        double seconds = time.seconds();
        double localTime = hours + minutes / 60.0 + seconds / 3600.0;

        // Get Date
        double day = date.day();
        double month = date.month();
        double year = date.year();
        double localDate = year * 10000 + month * 100 + day;

        // Get time zone
        double timeZone = _gmtTime;

        //! Pass received input values to MoonPhasecalculator for getting MoonPhase values
        UTIL::MoonPhaseCalculator::MoonData* moonData = 
            _mpc.generateMoonData(_longlatAlt.y(), _longlatAlt.x(), 
                                    localDate, localTime, 
                                    timeZone);

        //! Pass received input values to SunData for getting SunData
        const UTIL::SunData& sunData = 
            _srsc.generateSunData(_longlatAlt.y(), _longlatAlt.x(), 
                                    localDate, localTime, 
                                    timeZone);

        _populateAlmanacData(moonData, sunData);
    }

    void
    AlmanacUIHandler::_populateAlmanacData(const UTIL::MoonPhaseCalculator::MoonData* const moonData,
                                            const UTIL::SunData& sunData)
    {
        // Populate moonrise time
        _almanacData.moonriseTime = 
            UTIL::MoonPhaseCalculator::timeToBoostTime(moonData->RiseTime);

        // Populate moonset time
        _almanacData.moonsetTime = 
            UTIL::MoonPhaseCalculator::timeToBoostTime(moonData->SetTime);

        // Populate moon phase
        _almanacData.moonPhase = moonData->MoonPhase;

        // Populate sunrise and sunset time
        _almanacData.sunriseTime =
            UTIL::MoonPhaseCalculator::timeToBoostTime(sunData.sunriseTime);
        _almanacData.sunsetTime =
            UTIL::MoonPhaseCalculator::timeToBoostTime(sunData.sunsetTime);

        // Copy Time to New Moon
        _almanacData.timeToNewMoon = moonData->NewMoon;
    }

    const AlmanacData& 
    AlmanacUIHandler::getAlmanacData() const
    {
        return _almanacData;
    }

    void 
    AlmanacUIHandler::setGMTTime(const double& gmtTime)
    {
        _gmtTime = gmtTime;
    }

    double
    AlmanacUIHandler::getGMTTime()
    {
        return _gmtTime;
    }
}
