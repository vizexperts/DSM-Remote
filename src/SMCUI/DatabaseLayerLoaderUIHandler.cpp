#include <SMCUI/DatabaseLayerLoaderUIHandler.h>

#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>
#include <Core/AttributeTypes.h>
#include <Core/ICompositeObject.h>

#include <Util/CoordinateConversionUtils.h>
#include <Util/DistanceBearingCalculationUtils.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Database/DatabaseMaintainer.h>
#include <Database/ISQLQuery.h>

#include <Elements/ElementsUtils.h>
#include <Elements/ElementsPlugin.h>

#include <DB/ReaderWriter.h>
#include <DB/ReadFile.h>

#include <Core/GroupAttribute.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, DatabaseLayerLoaderUIHandler);
    DEFINE_IREFERENCED(DatabaseLayerLoaderUIHandler, VizUI::UIHandler);

    int DatabaseLayerLoaderUIHandler::_progressValue = 0;
    bool DatabaseLayerLoaderUIHandler::_progressChanged = false;

    const std::string DatabaseLayerLoaderUIHandler::IncidentLayerName = "Incidents";

    DatabaseLayerLoaderUIHandler::DatabaseLayerLoaderUIHandler()
        : _isInitializedOracle(false),_isInitializedMssql(false)
    {

        _addInterface(SMCUI::IDatabaseLayerLoaderUIHandler::getInterfaceName());
        _databaseOracle = NULL;
       // _databaseMssql = NULL;
    }

    DatabaseLayerLoaderUIHandler::~DatabaseLayerLoaderUIHandler()
    {
    }

    void DatabaseLayerLoaderUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "DatabaseLayerLoaderUIHandler attributes";

        _addAttribute(new CORE::StringAttribute("OracleDB", "OracleDB",
            CORE::StringAttribute::SetFuncType(this, &DatabaseLayerLoaderUIHandler::setOracleDBName),
            CORE::StringAttribute::GetFuncType(this, &DatabaseLayerLoaderUIHandler::getOracleDBName),
            "OracleDB name",
            groupName));
        
        _addAttribute(new CORE::StringAttribute("MSSQLDB", "MSSQLDB",
            CORE::StringAttribute::SetFuncType(this, &DatabaseLayerLoaderUIHandler::setMssqlDBName),
            CORE::StringAttribute::GetFuncType(this, &DatabaseLayerLoaderUIHandler::getMssqlDBName),
            "MSSQLDB name",
            groupName));
    }

    void DatabaseLayerLoaderUIHandler::setOracleDBName(const std::string& dbName)
    {
        _serverNameOracle = dbName;
    }

    std::string DatabaseLayerLoaderUIHandler::getOracleDBName() const
    {
        return _serverNameOracle;
    }

    void DatabaseLayerLoaderUIHandler::setMssqlDBName(const std::string& dbName)
    {
        _serverNameMssql = dbName;
    }

    std::string DatabaseLayerLoaderUIHandler::getMssqlDBName() const
    {
        return _serverNameMssql;
    }

    CORE::RefPtr<DATABASE::ISQLQueryResult> DatabaseLayerLoaderUIHandler::executeSQLQuery(const std::string &queryStatement, const IDatabaseLayerLoaderUIHandler::DBType dbType)
    {
        connectToDB(dbType);

        CORE::RefPtr<DATABASE::ISQLQueryResult> result = NULL;
        CORE::RefPtr<DATABASE::ISQLQuery> query;
        if(dbType == 1) //oracle Type
        {
            if(!_databaseOracle.valid())
                return result;
            query = _databaseOracle->createSQLQuery();
        }
        else if(dbType == 2) //sql server type
        {
            CORE::RefPtr<DATABASE::IDatabase> _databaseMssql = DATABASE::GetDatabaseFromMaintainer(_serverNameMssql);
            if(!_databaseMssql.valid())
                return result;
            query = _databaseMssql->createSQLQuery();
        }


        if(query.valid())
        {
            if(query->exec(queryStatement))
            {
                result = query->getNextResult();
            }
        }

        return result;
    }

    bool DatabaseLayerLoaderUIHandler::connectToDB(const IDatabaseLayerLoaderUIHandler::DBType dbType)
    {
        if(dbType == IDatabaseLayerLoaderUIHandler::DBType_Oracle)
        {
            if(_isInitializedOracle)
                return true;

            _databaseOracle = DATABASE::GetDatabaseFromMaintainer(_serverNameOracle);
            if(_databaseOracle.valid())
            {
                _isInitializedOracle = true;
                std::cout << "Connected to Oracle" << std::endl;
            }

            return _isInitializedOracle;
        }
        else if(dbType == IDatabaseLayerLoaderUIHandler::DBType_MSSQL)
        {
            if(_isInitializedMssql)
                return true;

            CORE::RefPtr<DATABASE::IDatabase> _databaseMssql = DATABASE::GetDatabaseFromMaintainer(_serverNameMssql);
            if(_databaseMssql.valid())
            {
                _isInitializedMssql = true;
                std::cout << "Connected to MSSQL" << std::endl;
            }

            return _isInitializedMssql;

        }
    }

    void DatabaseLayerLoaderUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            //! Try to add object to world
            if(_featuresToAddMutex.trylock() == 0)
            {   
                if(_progressValue < 0)
                    _notifyStatusToObservers(IAddContentStatusMessage::FAILURE, _progressValue);

                if(_progressValue < 100)
                    _notifyStatusToObservers(IAddContentStatusMessage::PENDING, _progressValue);

                if(_objectToAdd.valid())
                {
                    CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
                    _unsubscribe(wm, *CORE::IWorldMaintainer::TickMessageType);

                    //! Add to Incidents folder object if one exists in the world
                    CORE::RefPtr<CORE::IWorld> world = ELEMENTS::GetFirstWorldFromMaintainer();
                    if(world.valid())
                    {
                        CORE::RefPtr<CORE::IObject> incidentsFolderObject = world->getObjectByName(IncidentLayerName);
                        if(!incidentsFolderObject.valid())
                        {
                            //! Create a folder object and add it to world
                            CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
                            incidentsFolderObject = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FolderType);
                            if(incidentsFolderObject.valid())
                            {
                                incidentsFolderObject->getInterface<CORE::IBase>()->setName(IncidentLayerName);
                                world->addObject(incidentsFolderObject.get());
                            }
                        }

                        if(incidentsFolderObject.valid() && (incidentsFolderObject->getInterface<CORE::ICompositeObject>() != NULL))
                        {
                            incidentsFolderObject->getInterface<CORE::ICompositeObject>()->addObject(_objectToAdd);
                        }
                        else
                        {
                            ELEMENTS::GetFirstWorldFromMaintainer()->addObject(_objectToAdd);
                        }

                    }

                    _objectToAdd = NULL;

                    _notifyStatusToObservers(IAddContentStatusMessage::SUCCESS, _progressValue);
                }
                _featuresToAddMutex.unlock();
            }
        }
        else
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }

    void DatabaseLayerLoaderUIHandler::_notifyStatusToObservers(IAddContentStatusMessage::AddContentStatus status, unsigned int progress)
    {
        if(_progressChanged)
        {
            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAddContentStatusMessage::AddContentStatusMessageType);
            msg->setSender(this);
            IAddContentStatusMessage* addContentStatusMsg = msg->getInterface<IAddContentStatusMessage>();
            if(addContentStatusMsg)
            {
                addContentStatusMsg->setStatus(status);
                addContentStatusMsg->setProgress(progress);
            }
            notifyObservers(*IAddContentStatusMessage::AddContentStatusMessageType, *msg);

            _progressChanged = false;
        }
    }


    void DatabaseLayerLoaderUIHandler::loadLayer(CORE::RefPtr<DB::ReaderWriter::Options> options, const IDatabaseLayerLoaderUIHandler::DBType dbType)
    {
        if(connectToDB(dbType))
        {
            UTIL::ThreadPool* tp = CORE::WorldMaintainer::instance()->getComputeThreadPool();
            if(tp)
            {
                switch(dbType)
                {
                case IDatabaseLayerLoaderUIHandler::DBType_Oracle :
                    tp->invoke(boost::bind(&DatabaseLayerLoaderUIHandler::_mtLoadBPLayer, this,options));
                    break;
                case IDatabaseLayerLoaderUIHandler::DBType_MSSQL :
                    tp->invoke(boost::bind(&DatabaseLayerLoaderUIHandler::_mtLoadBPLayerMssql, this,options));
                    break;
                };
            }
            else
            {
                switch(dbType)
                {
                case IDatabaseLayerLoaderUIHandler::DBType_Oracle :
                    _mtLoadBPLayer(options);
                    break;
                case IDatabaseLayerLoaderUIHandler::DBType_MSSQL :
                    _mtLoadBPLayerMssql(options);
                    break;
                };
            }
        }
    }

    void DatabaseLayerLoaderUIHandler::_mtLoadBPLayer(CORE::RefPtr<DB::ReaderWriter::Options> options)
    {
        std::string userspaceName;
        if(!options->getMapValue("UserspaceName", userspaceName))
            return;

        std::string tableName;
        if(!options->getMapValue("Tablename", tableName))
            return;

        std::string filename = userspaceName + (userspaceName.empty() ? "" : "." ) + tableName + ".orcl";

        options->setMapValue("Database", _serverNameOracle);

         CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
        _subscribe(wm, *CORE::IWorldMaintainer::TickMessageType);

        _progressFunctPtr = NULL;
        _progressFunctPtr = &DatabaseLayerLoaderUIHandler::setProgress;

        CORE::RefPtr<CORE::IObject> object = DB::readFeature(filename, options.get(), _progressFunctPtr);
        if(object.valid())
        {
            _featuresToAddMutex.lock();
            _objectToAdd = object;
            _featuresToAddMutex.unlock();
        }
    }

    void DatabaseLayerLoaderUIHandler::_mtLoadBPLayerMssql(CORE::RefPtr<DB::ReaderWriter::Options> options)
    {
        std::string tableNameMssql;
        std::string filename;
        if(options->getMapValue("Tablename", tableNameMssql))
        {
            filename = tableNameMssql + ".mssql";
        }
        else if(options->getMapValue("LayerName", filename))
        {
            filename += ".mssql";
        }

        options->setMapValue("Database", _serverNameMssql);

         CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
        _subscribe(wm, *CORE::IWorldMaintainer::TickMessageType);

        _progressFunctPtr = NULL;
        _progressFunctPtr = &DatabaseLayerLoaderUIHandler::setProgress;

        CORE::RefPtr<CORE::IObject> object = DB::readFeature(filename, options.get(), _progressFunctPtr);
        if(object.valid())
        {
            _featuresToAddMutex.lock();
            _objectToAdd = object;
            _featuresToAddMutex.unlock();
        }
    }


    void DatabaseLayerLoaderUIHandler::setProgress(int value)
    {
        if(_progressValue != value)
        {
            _progressValue = value;
            _progressChanged = true;
        }
    }
}