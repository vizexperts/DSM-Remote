#include <SMCUI/ExportToGeoserverUIHandler.h>

#include <SMCUI/SMCUIPlugin.h>

#include <Core/InterfaceUtils.h>
#include <Core/WorldMaintainer.h>
#include <Core/IFeatureLayer.h>
#include <Core/IFeatureObject.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataCreator.h>
#include <Core/ICompositeObject.h>
#include <Core/IMetadataField.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IRasterData.h>

#include <VizUI/ISelectionUIHandler.h>

#include <GIS/IFeatureLayer.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>

#include <Util/FileUtils.h>

#include <Elements/ISettingComponent.h>
#include <DB/WriteFile.h>
#include <DB/ReaderWriter.h>

#include <curl/curl.h>
#include <ogrsf_frmts.h>
#include <gdal.h>
#include <ogr_api.h>
#include <ogr_srs_api.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, ExportToGeoserverUIHandler);
    DEFINE_IREFERENCED(ExportToGeoserverUIHandler, VizUI::UIHandler);

    ExportToGeoserverUIHandler::ExportToGeoserverUIHandler()
    {
        _addInterface(SMCUI::IExportToGeoserverUIHandler::getInterfaceName());
        setName(getClassname());
    }

    void ExportToGeoserverUIHandler::checkVecStore(std::string& path, std::string& ws, std::string& user, std::string& pass) {
        CURL *curl; CURLcode res;
        curl = curl_easy_init();
        if (curl) {
            std::string url(path + "/rest/workspaces/" + ws + "/datastores/vec");

            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(curl, CURLOPT_USERNAME, user.c_str());
            curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.c_str());
            curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);

            struct curl_slist *headers = curl_slist_append(NULL, "Content-Type: application/xml");
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
            curl_easy_setopt(curl, CURLOPT_HEADER, true);
            curl_easy_setopt(curl, CURLOPT_NOBODY, true);
            res = curl_easy_perform(curl);
            long code;
            res = curl_easy_getinfo(curl, CURLINFO_HTTP_CODE, &code);
            if (res == CURLE_OK) {
                if (code == 404) {
                    curl_slist_free_all(headers);
                    curl_easy_cleanup(curl);
                    curl = curl_easy_init();

                    url = path + "/geoserver/rest/workspaces/" + ws + "/datastores";
                    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
                    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
                    curl_easy_setopt(curl, CURLOPT_USERNAME, "admin");
                    curl_easy_setopt(curl, CURLOPT_PASSWORD, "geoserver");

                    std::string postFields = "<dataStore><name>vec</name><connectionParameters>"\
                        "<host>" + pghost + "</host>"\
                        "<port>" + port + "</port>"\
                        "<database>" + dbname + "</database>"\
                        "<user>" + user + "</user>"\
                        "<passwd>" + pass + "</passwd>"\
                        "<dbtype>postgis</dbtype></connectionParameters></dataStore>";
                    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postFields.c_str());

                    headers = curl_slist_append(NULL, "Content-Type: application/xml");
                    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
                    res = curl_easy_perform(curl);
                    if (res != CURLE_OK) fprintf(stderr, "Cannot make vec store:%s", curl_easy_strerror(res));
                }
            }
            curl_slist_free_all(headers);
            curl_easy_cleanup(curl);
        }
    }

    void ExportToGeoserverUIHandler::pushFeatureLayerToGeoserver(std::string& path, std::string& ws, std::string& user, std::string& pass, std::string& featureName, std::string& title) {
        checkVecStore(path, ws, user, pass);
        CURL *curl;
        CURLcode res;

        curl = curl_easy_init();
        if (curl)
        {
            //std::string url("http://192.168.0.89:8080/geoserver/");
            std::string url(path + "/rest/workspaces/" + ws + "/datastores/vec/featuretypes");

            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(curl, CURLOPT_USERNAME, user.c_str());
            curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.c_str());

            struct curl_slist *headers = NULL;
            headers = curl_slist_append(headers, "Content-Type: application/xml");
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

            std::string postField = "";
            postField.append("<featureType><name>");
            postField.append(featureName);
            postField.append("</name><title>" + title + "</title><advertised>true</advertised><srs>EPSG:4326</srs></featureType>");
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postField.c_str());

            res = curl_easy_perform(curl);

            /* Check for errors */
            if (res != CURLE_OK)
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));

            /* free the header list */
            curl_slist_free_all(headers);

            curl_easy_cleanup(curl);
        }
    }

    bool ExportToGeoserverUIHandler::exportGeoFeature(std::string& exportLayerName)
    {
        CORE::RefPtr<ELEMENTS::ISettingComponent> settingComp = CORE::WorldMaintainer::instance()->getComponentByName("SettingComponent")->getInterface<ELEMENTS::ISettingComponent>();

        if (settingComp.valid())
        {
            ELEMENTS::ISettingComponent::SettingsAttributes att =
                settingComp->getGlobalSetting(ELEMENTS::ISettingComponent::LAYERDATABASECONN);

            dbname = att.keyValuePair["Database"];
            pghost = att.keyValuePair["Host"];
            port = att.keyValuePair["Port"];
            user = att.keyValuePair["Username"];
            pass = att.keyValuePair["Password"];
        }

        CORE::RefPtr<CORE::IObject> object = _getCurrentSelectedItem<CORE::IObject>();
        CORE::RefPtr<CORE::IBase> base = object->getInterface<CORE::IBase>();
        const CORE::UniqueID& uid = base->getUniqueID();
        std::string fileName = uid.toString();
        /*std::string conn="PG:dbname=\'"+dbname+"\' host=\'"+pghost+"\' port=\'"+port+"\' user='"+user+"' password='"+pass+"'";
        OGRDataSource* ogrDS = OGRSFDriverRegistrar::Open( conn.c_str(), 1 );
        size_t pos = 0;
        while ((pos = fileName.find("-", pos)) != std::string::npos) {
        fileName.replace(pos, 1, "_");
        pos += 1;
        }
        for(int i=0;i<ogrDS->GetLayerCount();i++) {
        if(std::string(ogrDS->GetLayer(i)->GetName()).find(fileName)!=std::string::npos) {
        ogrDS->DeleteLayer(i);break;}
        }
        ogrDS->DestroyDataSource(ogrDS);*/

        DB::ReaderWriter::Options* options = new DB::ReaderWriter::Options();
        options->setMapValue("dbnname", dbname);
        options->setMapValue("hostname", pghost);
        options->setMapValue("port", port);
        options->setMapValue("user", user);
        options->setMapValue("password", pass);
        options->setMapValue("fileName", fileName);

        CORE::RefPtr<CORE::IFeatureLayer> featureLayer = object->getInterface<CORE::IFeatureLayer>();
        if (featureLayer.valid()) {
            DB::writeFeatureFile(featureLayer->getInterface<CORE::IFeatureObject>(), fileName + ".psg", options);
        }
        CORE::RefPtr<GIS::IFeatureLayer> gfl = object->getInterface<GIS::IFeatureLayer>();
        if (gfl.valid()) {
            options->setMapValue("Editable", UTIL::ToString(false));
            DB::writeObjectFile(object.get(), "test.pggis", options);
        }


        std::string title = exportLayerName;
        std::string name = fileName;

        std::string path = "http://localhost:8080/geoserver", ws = "vizexperts";
        std::string user = "admin", pass = "geoserver";
        if (settingComp.valid())
        {
            ELEMENTS::ISettingComponent::SettingsAttributes att = settingComp->getGlobalSetting(ELEMENTS::ISettingComponent::GEOSERVER);
            std::string host, un, p;
            host = att.keyValuePair["Host"];
            un = att.keyValuePair["Username"];
            p = att.keyValuePair["Password"];
            if (!un.empty() && !p.empty()) {
                user = un; pass = p;
            }
            if (!host.empty())
                path = host;
        }
        pushFeatureLayerToGeoserver(path, ws, user, pass, name, title);

        if (featureLayer.valid()) {
            CORE::RefPtr<CORE::IMetadataTable> table = featureLayer->getInterface<CORE::IMetadataTableHolder>()->getMetadataTable();
            std::string styleName = "MilPointStyle";
            if (table->getFeatureLayerType() == CORE::IFeatureLayer::MILITARY_UNIT_POINT)
                changeFeatureLayerStyle(path, ws, user, pass, name, styleName);//pushMilPointStyle(path,ws);
        }

        delete options;

        return false;
    }


    bool ExportToGeoserverUIHandler::exportGeoRaster(std::string& exportLayerName) {

        CORE::RefPtr<TERRAIN::IRasterObject> rObject = _getCurrentSelectedItem<TERRAIN::IRasterObject>();
        if (rObject.valid()) {
            CORE::RefPtr<CORE::IRasterData> data = rObject->getRasterData();
            if (data.valid()) {
                UTIL::TemporaryFolder* temp = UTIL::TemporaryFolder::instance();
                std::string name = rObject->getInterface<CORE::IBase>()->getUniqueID().toString();
                std::string filename = temp->getPath() + "/" + name + ".tiff";
                bool b = DB::writeRasterFile(data.get(), filename);
                if (!b) return false;

                CURL *curl; CURLcode res;
                curl_global_init(CURL_GLOBAL_ALL);
                curl = curl_easy_init();
                if (curl) {
                    std::string path = "http://localhost:8080/geoserver", ws = "vizexperts";
                    std::string user = "admin", pass = "geoserver";
                    CORE::RefPtr<ELEMENTS::ISettingComponent> settingComp = CORE::WorldMaintainer::instance()->getComponentByName("SettingComponent")->getInterface<ELEMENTS::ISettingComponent>();
                    if (settingComp.valid()) {
                        ELEMENTS::ISettingComponent::SettingsAttributes att = settingComp->getGlobalSetting(ELEMENTS::ISettingComponent::GEOSERVER);
                        std::string host, un, p;
                        host = att.keyValuePair["Host"];
                        un = att.keyValuePair["Username"];
                        p = att.keyValuePair["Password"];
                        if (!un.empty() && !p.empty()) {
                            user = un; pass = p;
                        }
                        if (!host.empty())
                            path = host;
                    }
                    std::string url(path + "/rest/workspaces/" + ws + "/coveragestores/" + name + "/file.geotiff");

                    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
                    curl_easy_setopt(curl, CURLOPT_USERNAME, user.c_str());
                    curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.c_str());

                    struct curl_slist *headers = curl_slist_append(NULL, "Content-Type: image/tiff");
                    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

                    /*std::string postField="<coverageStore><name>"+name+"</name><workspace>"+ws+"</workspace><enabled>true</enabled></coverageStore>";
                    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postField);
                    curl_easy_setopt(curl, CURLOPT_HEADER  , true);
                    curl_easy_setopt(curl, CURLOPT_NOBODY  , true);
                    */

                    FILE *hd_src = fopen(filename.c_str(), "rb");
                    struct stat file_info;
                    int rc = stat(filename.c_str(), &file_info);
                    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
                    curl_easy_setopt(curl, CURLOPT_PUT, 1L);
                    curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
                    curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);
                    curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)file_info.st_size);//in.tellg());

                    res = curl_easy_perform(curl);

                    curl_slist_free_all(headers);
                    curl_easy_cleanup(curl);
                    fclose(hd_src);

                    if (res == CURLE_OK) {
                        curl = curl_easy_init();
                        if (curl) {
                            url = path + "/rest/workspaces/" + ws + "/coveragestores/" + name + "/coverages/" + name;

                            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
                            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
                            curl_easy_setopt(curl, CURLOPT_USERNAME, user.c_str());
                            curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.c_str());

                            struct curl_slist *headers = NULL;
                            headers = curl_slist_append(headers, "Content-Type: application/xml");
                            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
                            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

                            std::string postField = "";
                            postField.append("<coverage><title>" + exportLayerName + "</title><enabled>true</enabled></coverage>");
                            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postField.c_str());

                            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "put");

                            res = curl_easy_perform(curl);

                            if (res != CURLE_OK)
                                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                                curl_easy_strerror(res));

                            curl_slist_free_all(headers);
                        }

                        curl_easy_cleanup(curl);
                    }
                }

                curl_global_cleanup();
            }
        }
        return false;
    }

    size_t ExportToGeoserverUIHandler::write_to_string(void *ptr, size_t size, size_t count, void *stream) {
        ((std::string*)stream)->append((char*)ptr, 0, size*count);
        return size*count;
    }

    template <class T>
    T* ExportToGeoserverUIHandler::_getCurrentSelectedItem()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if (iter != map.end())
        {
            return iter->second->getInterface<T>();
        }
        return NULL;
    }

    void ExportToGeoserverUIHandler::changeFeatureLayerStyle(std::string& path, std::string& ws, std::string& user, std::string& pass, std::string& featureName, std::string& styleName) {
        CURL *curl = NULL;
        CURLcode res;

        /* In windows, this will init the winsock stuff */
        curl_global_init(CURL_GLOBAL_ALL);

        /* get a curl handle */
        curl = curl_easy_init();
        if (curl) {

            std::string url(path + "/rest/layers/" + ws + ":" + featureName);
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_USERNAME, user.c_str());
            curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.c_str());

            struct curl_slist *headers = NULL;
            headers = curl_slist_append(headers, "Content-Type: application/xml");
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

            std::string s = "<layer><defaultStyle><name>" + styleName + "</name></defaultStyle></layer>";
            std::string file = "stylechange.xml";
            std::ofstream myfile; myfile.open(file.c_str());
            myfile << s;
            myfile.close();
            struct stat file_info;
            int rc = stat(file.c_str(), &file_info);
            if (rc) return;
            FILE *hd_src = fopen(file.c_str(), "rb");
            curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
            curl_easy_setopt(curl, CURLOPT_PUT, 1L);
            curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
            curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);
            curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,
                (curl_off_t)file_info.st_size);

            res = curl_easy_perform(curl);
            if (res != CURLE_OK)
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));

            curl_easy_cleanup(curl);
            fclose(hd_src); /* close the local file */
        }

        curl_global_cleanup();
    }

    size_t ExportToGeoserverUIHandler::read_callback(void *ptr, size_t size, size_t nmemb, void *stream) {
        size_t retcode;
        curl_off_t nread;

        /* in real-world cases, this would probably get this data differently
        as this fread() stuff is exactly what the library already would do
        by default internally */
        retcode = fread(ptr, size, nmemb, (FILE*)stream);

        nread = (curl_off_t)retcode;

        fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T
            " bytes from file\n", nread);

        return retcode;
    }


}