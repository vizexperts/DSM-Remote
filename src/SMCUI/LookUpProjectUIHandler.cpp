#include <SMCUI/LookUpProjectUIHandler.h>

#include <App/AccessElementUtils.h>
#include <App/ApplicationRegistry.h>

#include <Database/IDatabaseMaintainer.h>
#include <Database/IDBRecord.h>
#include <Database/IDBField.h>
#include <Database/IDBTable.h>
#include <Database/ISQLQueryResult.h>
#include <Database/ISQLQuery.h>

#include <osgDB/FileNameUtils>
#include <iomanip> 
#include <stdio.h>
#include <Database/DBUtils.h>

//TODO :- To move some redundant SQL related functions in single place

namespace SMCUI 
{

    DEFINE_META_BASE(SMCUI, LookUpProjectUIHandler);
    DEFINE_IREFERENCED(LookUpProjectUIHandler, VizUI::UIHandler);

    LookUpProjectUIHandler::LookUpProjectUIHandler()
    {
        _addInterface(SMCUI::ILookUpProjectUIHandler::getInterfaceName());
        setName(getClassname());
    }

    LookUpProjectUIHandler::~LookUpProjectUIHandler()
    {
        
    }

    bool
        LookUpProjectUIHandler::makeConnectionWithDatabase(const SMCUI::ILookUpProjectUIHandler::DatabaseCredential& credential)
    {

        //This contain the Map of the credentail of the Database connected
        if (_cpSchemaTableMap.find(credential.database) == _cpSchemaTableMap.end())
        {
            SMCUI::LookUpProjectUIHandler::CreatorPlannerSchemaTable* schemaTableData = new CreatorPlannerSchemaTable;
            schemaTableData->creatorSchemaName = credential.creatorSchemaName;
            schemaTableData->creatortableName = credential.creatortableName;
            schemaTableData->plannerSchemaName = credential.plannerSchemaName;
            schemaTableData->plannertableName = credential.plannertableName;

            schemaTableData->tkSchemaName = credential.tkSchemaName;
            schemaTableData->tkTableName = credential.tkTableName;

            schemaTableData->templateSchemaName = credential.templateSchemaName;
            schemaTableData->templateTableName = credential.templateTableName;

            _cpSchemaTableMap[credential.database] = schemaTableData;
        }

        _setDatabaseCredential(credential);
        _presentDatabaseConnection = credential.database;
        bool databaseConnectionStatus = _checkConnectionWithDatabase();

        return databaseConnectionStatus;
    }

    void
    LookUpProjectUIHandler::closeDatabaseConnection()
    {
        // There is not existing Database connection then return;
        if(_cpSchemaTableMap.find(_presentDatabaseConnection) != _cpSchemaTableMap.end())
        {
            delete _cpSchemaTableMap[_presentDatabaseConnection];
            _cpSchemaTableMap.erase(_presentDatabaseConnection);
        }
    }

    void
    LookUpProjectUIHandler::_setDatabaseCredential(const SMCUI::ILookUpProjectUIHandler::DatabaseCredential& credential)
    {
        _dbOptions.clear();
        _dbOptions.insert(std::pair<std::string, std::string>("Name", ""));
        _dbOptions.insert(std::pair<std::string, std::string>("Database", credential.database));
        _dbOptions.insert(std::pair<std::string, std::string>("Host", credential.hostname));
        _dbOptions.insert(std::pair<std::string, std::string>("Port", credential.port));
        _dbOptions.insert(std::pair<std::string, std::string>("Username", credential.username));
        _dbOptions.insert(std::pair<std::string, std::string>("Password", credential.password));
    }

    std::vector<std::pair<std::string, std::string> >
    LookUpProjectUIHandler::getProjectListFromProjectMap(const LookUpProjectUIHandler::ProjectType& type)
    {
        //std::map<std::string, std::string> list;
        std::vector<std::pair<std::string, std::string> > list;
        if(_cpSchemaTableMap.find(_presentDatabaseConnection) != _cpSchemaTableMap.end() )
        {
            std::string schemaName;
            std::string tableName;
            if(type == LookUpProjectUIHandler::CREATOR)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatorSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatortableName;
            }
            else if(type == LookUpProjectUIHandler::PLANNER)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannerSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannertableName;
            }
            else if(type == LookUpProjectUIHandler::TK)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkTableName;
            }
            else if(type == LookUpProjectUIHandler::TEMPLATE)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateTableName;
            }
            list = _getPresentProjects(schemaName, tableName);
         }
        return list;
    }

    bool
    LookUpProjectUIHandler::_checkConnectionWithDatabase()
    {
        if(_dbOptions.size() < 5)
            return false;

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(dbUIHandler == NULL)
            return false;

        dbUIHandler->initialize(_dbOptions);
        
        std::string status;

        if(dbUIHandler->testConnection(_dbOptions))
        {
            status = "Connected";
            _dbOptions.insert(std::pair<std::string, std::string>("Status", status));
            dbUIHandler->initialize(_dbOptions);
            return true;
        }
        else
        {
            status = "Not Connected";
            _dbOptions.insert(std::pair<std::string, std::string>("Status", status));
            dbUIHandler->initialize(_dbOptions);
            return false;
        }

        return false;
    }

    void
    LookUpProjectUIHandler::_getPresentSchemaAndTable(std::string& schemaName, std::string& tableName, const LookUpProjectUIHandler::ProjectType& type)
    {
        if(_cpSchemaTableMap.find(_presentDatabaseConnection) != _cpSchemaTableMap.end() )
        {
            if(type == LookUpProjectUIHandler::CREATOR)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatorSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatortableName;
            }
            else if(type == LookUpProjectUIHandler::PLANNER)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannerSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannertableName;
            }
            else if(type == LookUpProjectUIHandler::TK)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkTableName;
            }
            else if (type == LookUpProjectUIHandler::TEMPLATE){
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateTableName;
            }
        }
    }

    std::string 
    LookUpProjectUIHandler::getProjectNameByUID(const std::string& uid, const LookUpProjectUIHandler::ProjectType& type)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(dbUIHandler == NULL)
            return false;

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        std::string schemaName;
        std::string tableName;

        _getPresentSchemaAndTable(schemaName, tableName, type);

        int count = 0;
        if(query.valid())
        {
             std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);;
             std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);;

            std::string queryStatement = "SELECT projectname FROM %s.%s WHERE uid = '%s'";
            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), uid.c_str()); 

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();

                if(result.valid())
                {
                    CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                    std::string projectName="";
                    if(DBUtils::getString(resultTable,projectName) && !projectName.empty())
                    {
                        return projectName;
                    }

                }
            }
        }
        return std::string("");
    }

    bool 
    LookUpProjectUIHandler::_checkForExistingSchema(const std::string& schemaName)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(dbUIHandler == NULL)
            return false;

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        int count = 0;
        if(query.valid())
        {
            std::string toLowerForm = osgDB::convertToLowerCase(schemaName);

            std::string queryStatement = "SELECT count(schema_name) FROM information_schema.schemata WHERE schema_name = '%s'";
            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toLowerForm.c_str());

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                if(result.valid())
                {
                    CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                    if(DBUtils::getInt(resultTable,count) && (count >= 1))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    std::vector<std::string>
    LookUpProjectUIHandler::getRecentProjectsList(const ILookUpProjectUIHandler::ProjectType& type)
    {
        std::vector<std::string> list;
        if(_cpSchemaTableMap.find(_presentDatabaseConnection) != _cpSchemaTableMap.end() )
        {
            std::string schemaName;
            std::string tableName;
            if(type == LookUpProjectUIHandler::CREATOR)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatorSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatortableName;
            }
            else if(type == LookUpProjectUIHandler::PLANNER)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannerSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannertableName;
            }
            else if(type == LookUpProjectUIHandler::TK)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkTableName;
            }
            else if(type == LookUpProjectUIHandler::TEMPLATE)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateTableName;
            }
            list = _getRecentProjects(schemaName, tableName);
         }
        return list;

    }

    std::vector<std::string>
    LookUpProjectUIHandler::_getRecentProjects(const std::string &schemaName, const std::string &tableName)
    {
        std::vector<std::string> projectNames;
        projectNames.clear();

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::
            getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(!dbUIHandler)
            return projectNames;

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        int count = 0;
        int recentProjectsCount = 20;
        if(query.valid())
        {
            std::string queryStatement = "SELECT projectname FROM %s.%s ORDER BY datemodified DESC LIMIT " + 
                UTIL::ToString(recentProjectsCount);

            std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);;
            std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);;

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str());
            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                if(result.valid())
                {
                    CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                    if(resultTable.valid())
                    {
                        if(count > recentProjectsCount)
                            count = recentProjectsCount;
                        CORE::RefPtr<DATABASE::IDBRecord> record = resultTable->getNextRecord();
                        while(record.valid() && count <= recentProjectsCount )
                        {
                            DATABASE::IDBField* field1 =  record->getFieldByIndex(0);
                            std::string name = field1->toString();
                            projectNames.push_back(name);
                            count++;
                            record = resultTable->getNextRecord();
                        }
                    }
                }
            }
        }

        return projectNames;
    }


    std::vector<std::pair<std::string, std::string> >
    LookUpProjectUIHandler::getProjectListWithCreationTime(const SMCUI::ILookUpProjectUIHandler::ProjectType &type)
    {
        std::vector<std::pair<std::string, std::string> > list;
        if(_cpSchemaTableMap.find(_presentDatabaseConnection) != _cpSchemaTableMap.end() )
        {
            std::string schemaName;
            std::string tableName;
            if(type == LookUpProjectUIHandler::CREATOR)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatorSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->creatortableName;
            }
            else if(type == LookUpProjectUIHandler::PLANNER)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannerSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->plannertableName;
            }
            else if(type == LookUpProjectUIHandler::TK)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->tkTableName;
            }
            else if(type == LookUpProjectUIHandler::TEMPLATE)
            {
                schemaName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateSchemaName;
                tableName = _cpSchemaTableMap.find(_presentDatabaseConnection)->second->templateTableName;
            }
            list = _getPresentProjectsWithCreationTime(schemaName, tableName);
         }
        return list;        
    }

    std::vector<std::pair<std::string, std::string> >
    LookUpProjectUIHandler::_getPresentProjectsWithCreationTime(const std::string &schemaName, 
        const std::string &tableName)
    {
        std::vector<std::pair<std::string, std::string> > projectNames;
        projectNames.clear();

        VizDataUI::IDatabaseUIHandler* dbUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(!dbUIHandler)
            return projectNames;

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        int count = 0;
        if(query.valid())
        {
            // uid is not really needed as projectName is UNIQUE
            std::string queryStatement = "SELECT projectname, datecreated FROM %s.%s";

            std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);;
            std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);;

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str());

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                if(result.valid())
                {
                    CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                    if(resultTable.valid())
                    {
                        CORE::RefPtr<DATABASE::IDBRecord> record = resultTable->getNextRecord();
                        while(record.valid())
                        {
                            DATABASE::IDBField* field1 =  record->getFieldByIndex(0);
                            std::string name = field1->toString();
                            DATABASE::IDBField* field2 =  record->getFieldByIndex(1);
                            std::string dateCreated = field2->toString();
                            projectNames.push_back(std::make_pair(name, dateCreated));
                            record = resultTable->getNextRecord();
                        }
                    }
                }
            }
        }
        return projectNames;
    }

    std::vector<std::pair<std::string, std::string> >
    LookUpProjectUIHandler::_getPresentProjects(const std::string& schemaName, const std::string& tableName)
    {
        std::vector<std::pair<std::string, std::string> > projectName;
        projectName.clear();

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(!dbUIHandler)
            return projectName;

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        int count = 0;
        if(query.valid())
        {
            std::string queryStatement = "SELECT projectname, uid FROM %s.%s";

            std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);;
            std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);;

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str());

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                if(result.valid())
                {
                    CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                    if(resultTable.valid())
                    {
                        CORE::RefPtr<DATABASE::IDBRecord> record = resultTable->getNextRecord();
                        while(record.valid())
                        {
                            DATABASE::IDBField* field1 =  record->getFieldByIndex(0);
                            std::string name = field1->toString();
                            DATABASE::IDBField* field2 =  record->getFieldByIndex(1);
                            std::string uid = field2->toString();
                            projectName.push_back(std::make_pair(name, uid));
                            record = resultTable->getNextRecord();
                        }
                    }
                }
            }
        }
        return projectName;
    }

    bool 
    LookUpProjectUIHandler::_createProjectSchema(const std::string& schemaName, const std::string& tableName)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(!dbUIHandler)
            return false;

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        if(_checkForExistingSchema(schemaName))
            return true;

        if(query.valid())
        {
            std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);
            std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);

            _tableNameSaveLoad = toTableLowerForm;

            std::string queryStatement 
                = "CREATE SCHEMA %s CREATE TABLE %s ( projectname varchar NOT NULL UNIQUE, uid varchar UNIQUE, usertype varchar, datecreated varchar, datemodified timestamp)";

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str());

            if(query->exec(buffer))
            {
                 return true;
            }
            else
            {
                //Failed to execute the command
                return false;
            }
        }
        return false;
    }

    std::string
    LookUpProjectUIHandler::getProjectUIDByName(const std::string& projectName, const LookUpProjectUIHandler::ProjectType& type)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(dbUIHandler == NULL)
            return "";

        std::string schemaName;
        std::string tableName;
        std::string projectUID = "";
        _getPresentSchemaAndTable(schemaName, tableName, type);

        std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);
        std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        if(query.valid())
        {
            std::string queryStatement;
            char buffer[1000] = {'\0'};
            queryStatement = "SELECT uid FROM %s.%s where UPPER(projectname) = UPPER('%s')";
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), projectName.c_str());

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                if(result.valid())
                {

                    CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                    if(resultTable.valid())
                    {
                        CORE::RefPtr<DATABASE::IDBRecord> record = resultTable->getNextRecord();
                        if(record.valid())
                        {

                                DATABASE::IDBField* field1 =  record->getFieldByIndex(0);
                                projectUID = field1->toString();
                                return projectUID;
                        }
                    }
                    return projectUID;
                }
            }
        }
        return projectUID;
    }

    //Populate the database with the project path
    // We are executing too may SQL Query at the same time so that the Projects are saved in Sorted order of Recently modified date
    void
    LookUpProjectUIHandler::addOrUpdateProjectName(const std::string& projectName, const std::string& uid,
                                const SMCUI::LookUpProjectUIHandler::NewOrSaveType& newOrSave, const LookUpProjectUIHandler::ProjectType& type)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(!dbUIHandler)
            return;

        std::string schemaName;
        std::string tableName;

        _getPresentSchemaAndTable(schemaName, tableName, type);

        if(!_checkForExistingSchema(schemaName))
        {
            if(!_createProjectSchema(schemaName, tableName))
                return;
        }

        std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);
        std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        int bitType = 0;
        if( type == LookUpProjectUIHandler::CREATOR)
            bitType |= 1;
        else if (type == LookUpProjectUIHandler::PLANNER)
            bitType |= 2;
        else if (type == LookUpProjectUIHandler::TEMPLATE)
            bitType = 3;
        else if( type == LookUpProjectUIHandler::TK)
            bitType |= 4;

        std::stringstream ss;
        ss<<bitType;
        std::string userType = ss.str();

        if(query.valid())
        {
            std::string queryStatement;

            char buffer[1000] = {'\0'};
            
            //For New pro
            if(newOrSave == LookUpProjectUIHandler::NEWPROJECT)
            {
                std::string uid = CORE::UniqueID::CreateUniqueID()->toString();
                queryStatement = "INSERT INTO %s.%s (projectname, uid, usertype, datecreated, datemodified) VALUES (UPPER('%s'), '%s','%s', to_char(current_timestamp, 'DD Month YYYY HH24:MI:SS.MS'), current_timestamp)";
                sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), projectName.c_str(), uid.c_str(), userType.c_str());
            }
            // For saving the existing
            else if(newOrSave == LookUpProjectUIHandler::SAVEPROJECT)
            {
                memset(buffer, '/0', sizeof(buffer));
                queryStatement = "SELECT datecreated FROM %s.%s where uid = '%s'";
                sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), uid.c_str());
                std::string createdDate;

                if(query->exec(buffer))
                {
                    CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                    if(result.valid())
                    {
                        CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                        DBUtils::getString(resultTable,createdDate);
                    }
                }

                if(createdDate.empty())
                    return;

                queryStatement = "DELETE FROM %s.%s where uid = '%s'";
                memset(buffer, '/0', sizeof(buffer));
                sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), uid.c_str());
                if(query->exec(buffer))
                {
                    CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                }

                queryStatement = "INSERT INTO %s.%s (projectname, uid, usertype, datecreated, datemodified) VALUES (UPPER('%s'), '%s','%s', '%s', current_timestamp)";
                memset(buffer, '/0', sizeof(buffer));
                sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), projectName.c_str(), uid.c_str(), userType.c_str(), createdDate.c_str());
            }
            else if(newOrSave == LookUpProjectUIHandler::SAVEASPROJECT)
            {
                
            }

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
            }
        }
    }

    bool 
    LookUpProjectUIHandler::isProjectAlreadyPresent(const std::string& projectName, const ILookUpProjectUIHandler::ProjectType& type)
    {
        if( !getProjectUIDByName(projectName, type).empty() )
            return true;

        return false;
    }

    //Caution :- UID is unique 
    void 
    LookUpProjectUIHandler::deleteProjectByUID(const std::string& uid, const ILookUpProjectUIHandler::ProjectType& type)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(dbUIHandler == NULL)
            return;

        std::string schemaName;
        std::string tableName;

        _getPresentSchemaAndTable(schemaName, tableName, type);

        std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);
        std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        if(query.valid())
        {
            std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);
            std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);

            _tableNameSaveLoad = toTableLowerForm;

            std::string queryStatement = "DELETE FROM %s.%s WHERE uid = '%s'";

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), uid.c_str());

            query->exec(buffer);
        }
    }

    //Caution :- Project Name may not be Unique , So LOOKOUT
    void 
    LookUpProjectUIHandler::deleteProjectByName(const std::string& projectName, const ILookUpProjectUIHandler::ProjectType& type)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(dbUIHandler == NULL)
            return ;


        std::string schemaName;
        std::string tableName;

        _getPresentSchemaAndTable(schemaName, tableName, type);

        std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);
        std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);

        CORE::RefPtr<DATABASE::ISQLQuery> query = dbUIHandler->createSQLQuery();

        if(query.valid())
        {
            std::string toSchemaLowerForm = osgDB::convertToLowerCase(schemaName);
            std::string toTableLowerForm =  osgDB::convertToLowerCase(tableName);

            _tableNameSaveLoad = toTableLowerForm;

            std::string queryStatement = "DELETE FROM %s.%s WHERE UPPER(projectname) = UPPER('%s')";

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str(), projectName.c_str());

            query->exec(buffer);
        }
    }

    std::string
    LookUpProjectUIHandler::getNextProjectName(const LookUpProjectUIHandler::ProjectType& type)
    {

        VizDataUI::IDatabaseUIHandler* dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getManager());

        if(!dbUIHandler)
            return std::string("");;

        if(!_globalSettingComponent.valid())
            return std::string("");

        // There is not existing Database connection then return;
        ELEMENTS::ISettingComponent::SettingsAttributes presentDatabaseSetting = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::PROJECTDATABASECONN);

        std::string schemaName;
        std::string tableName;

        _getPresentSchemaAndTable(schemaName, tableName, type);

        // This will create the Schema and table in case it is creating it the First Time or Else it will simply Return
        _createProjectSchema(schemaName, tableName);

        std::vector<std::pair<std::string, std::string> > projectList = _getPresentProjects(schemaName, tableName);
        if(projectList.empty())
        {
            if(type == LookUpProjectUIHandler::CREATOR)
                return "Project1";
            else if(type == LookUpProjectUIHandler::PLANNER)
                return "Planner1";
            else if(type == LookUpProjectUIHandler::TK)
                return "tk1";
        }

        std::string nextProject;;
        std::ostringstream ss ;
        ss<<projectList.size() + 1;
        if(type == LookUpProjectUIHandler::CREATOR)
        {
            nextProject = "Project" + ss.str();
        }
        else if(type == LookUpProjectUIHandler::PLANNER)
        {
            nextProject = "Planner" + ss.str();
        }
        else if(type == LookUpProjectUIHandler::TK)
        {
            nextProject = "TK" + ss.str();
        }

        return nextProject;

    }

    void 
    LookUpProjectUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void 
    LookUpProjectUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
         // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
            CORE::RefPtr<CORE::IComponent> component = worldMaintainer->getComponentByName("SettingComponent");
           _globalSettingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
        }
        else
        {
             UIHandler::update(messageType, message);
        }            
    }

}// namespace SMCUI
