#include <SMCUI/SlopeAspectUIHandler.h>

#include <Terrain/IRasterObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <Core/ITerrain.h>

#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>

#include <Elements/IColorMap.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, SlopeAspectUIHandler);

    DEFINE_IREFERENCED(SlopeAspectUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    SlopeAspectUIHandler::SlopeAspectUIHandler()
        : _computeType(GISCOMPUTE::ISlopeAspectFilterVisitor::COMPUTE_SLOPE_DEGREE)
        , _mapSize(100)
    {
        _addInterface(ISlopeAspectUIHandler::getInterfaceName());
        setName(getClassname());
        CORE::IComponent* comp = (CORE::WorldMaintainer::instance())->getComponentByName("ColorMapComponent");
        if (comp)
        {
            _colorMapComponent = comp->getInterface<ELEMENTS::IColorMapComponent>();
        }
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _extents = new osg::Vec2Array;

        _reset();
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    SlopeAspectUIHandler::~SlopeAspectUIHandler(){}

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void SlopeAspectUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void SlopeAspectUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void SlopeAspectUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }
    void SlopeAspectUIHandler::setColorMapType(ELEMENTS::IColorMap::COLORMAP_TYPE colorMapType) 
    {
        _colorMapType = colorMapType;
    }


    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool SlopeAspectUIHandler::getFocus() const
    {
        return false;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void SlopeAspectUIHandler::execute()
    {
        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(vfactory)
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if(world)
            {
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::SlopeAspectFilterVisitorType);

                if(!_visitor)
                {
                    LOG_ERROR("Failed to create filter class");
                    return;
                }

                GISCOMPUTE::ISlopeAspectFilterVisitor* iVisitor  = 
                    _visitor->getInterface<GISCOMPUTE::ISlopeAspectFilterVisitor>();

                if(_extents->size() >1)
                {
                    //set extent on visitor
                    iVisitor->setExtents(_extents.get());
                }
                //check type and set minval and maxval
                ////Min and Max Value of Color Map will -90 to 90 in Slope case
                int minVal = 0, maxVal = 0;
                if (_computeType == GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType::COMPUTE_SLOPE_DEGREE)
                {
                    minVal = 0;
                    maxVal = 90;
                }
                else if (_computeType == GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType::COMPUTE_SLOPE_PERCENT)
                {
                    minVal = 0;
                    maxVal = 200;
                }
                else if (_computeType == GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType::COMPUTE_ASPECT)// calculate aspect
                {
                    minVal = 0;
                    maxVal = 360;
                }
                //_populateColorMap(minVal, maxVal);
                // set color map type
                if (_colorMap.valid())
                {
                    iVisitor->setColorMap(_colorMap.get());
                }

                // set computation type
                iVisitor->setComputationType(_computeType);

                // set output name
                iVisitor->setOutputName(_name);

                CORE::IBase* applyNode = NULL;
                if(!_elevObj)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    applyNode = terrain->getInterface<CORE::IBase>();
                }
                else
                {
                    applyNode = _elevObj->getInterface<CORE::IBase>();
                }

                CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                CORE::IBase* terrainBase = terrain->getInterface<CORE::IBase>();

                //subscribe for filter status
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                applyNode->accept(*_visitor);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void SlopeAspectUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::ISlopeAspectFilterVisitor> visitor = 
                        base->getInterface<GISCOMPUTE::ISlopeAspectFilterVisitor>();

                    if(visitor.valid())
                    {
                        CORE::RefPtr<TERRAIN::IRasterObject> object = visitor->getOutput();
                        if(object)
                        {
                            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                            if(world)
                            {
                                world->addObject(object->getInterface<CORE::IObject>(true));
                                /*CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                                terrain->addRasterObject(object);*/
                            }
                        }
                        else
                        {
                            throw UTIL::Exception("Failed to create raster object", __FILE__, __LINE__);
                        }
                    }
                }
            }
            catch(...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void SlopeAspectUIHandler::_reset()
    {
        _extents->clear();
        _elevObj = NULL;
        _visitor = NULL;
        _name = "";
        _mapSize = 100;
        _colorMapType = ELEMENTS::IColorMap::RGB_TYPE;
        _colorMap = NULL;
    }

    void SlopeAspectUIHandler::setExtents(osg::Vec4d extents)
    {
        _extents->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _extents->push_back(osg::Vec2d(first.x(), first.y()));
        _extents->push_back(osg::Vec2d(second.x(), second.y()));
    }

    osg::Vec4d SlopeAspectUIHandler::getExtents() const
    {
        osg::Vec4d extents(0.0, 0.0, 0.0, 0.0);
        osg::Vec2d first = _extents->at(0);
        osg::Vec2d second = _extents->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    void SlopeAspectUIHandler::setColorMap(ELEMENTS::IColorMap* colorMap)
    {
        _colorMap = colorMap;
    }

    void SlopeAspectUIHandler::setComputeType(GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType type)
    {
        _computeType = type;
    }


    void SlopeAspectUIHandler::setElevationObject(TERRAIN::IElevationObject* obj)
    {
        _elevObj = obj;
    }

    void SlopeAspectUIHandler::setOutputName(std::string& name)
    {
        _name = name;
    }
    void SlopeAspectUIHandler::setMapSize(int mapSize)
    {
        _mapSize = mapSize;
    }
    void SlopeAspectUIHandler::stopFilter()
    {
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }
    void
        SlopeAspectUIHandler::_populateColorMap(float minSize, float maxSize)
    {

        int alphaValue = 255;
        _colorMap = _colorMapComponent->createColorMap();
        ELEMENTS::IColorMap::RangeColorMap& map = _colorMap->getMap();
        float colorstep = 2.0f / (float)_mapSize;
        float step = (maxSize - minSize) / (float)_mapSize;
        unsigned int j;
        for (j = 0; j < _mapSize / 2; ++j)
        {
            float colorval = colorstep * j;
            map[osg::Vec2(minSize + j * step, minSize + (j + 1) * step)] =
                osg::Vec4(colorval, 1, 0, (float)alphaValue / 255.0);
        }
        for (; j < _mapSize; ++j)
        {
            float colorval = colorstep * (j - _mapSize / 2);
            map[osg::Vec2(minSize + j * step, minSize + (j + 1) * step)] =
                osg::Vec4(1, 1 - colorval, 0, (float)alphaValue / 255.0);
        }

    }
} // namespace SMCUI

