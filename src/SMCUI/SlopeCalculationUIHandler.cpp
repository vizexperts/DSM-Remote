/*****************************************************************************
*
* File             : SlopeCalculationUIHandler.cpp
* Description      : SlopeCalculationUIHandler class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <Core/IFeatureObject.h>
#include <Core/InterfaceUtils.h>
#include <Core/IPenStyle.h>
#include <Core/AttributeTypes.h>
#include <Core/ISelectionComponent.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataRecord.h>
#include <Core/IFeatureLayer.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataTable.h>
#include <Core/ICompositeObject.h>
#include <Core/ITemporary.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITerrain.h>
#include <Core/CoreRegistry.h>
#include <Core/IEditable.h>
#include <Core/IText.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IMetadataCreator.h>
#include <Core/IDeletable.h>
#include <Core/ILine.h>
#include <Core/IWorld.h>
#include <Core/IPoint.h>
#include <Core/IBase.h>
#include <Core/IWorld.h>
#include <Core/Point.h>

#include <Elements/MultiPoint.h>
#include <Elements/IDrawTechnique.h>
#include <Elements/IClamper.h>
#include <Elements/ILineDrawTechnique.h>
#include <Elements/OSGEarthLineDrawTechnique.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IRepresentation.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouse.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>
#include <VizUI/IModelPickUIHandler.h>

#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <App/IUndoTransaction.h>
#include <App/IUIHandler.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>

#include <Util/CoordinateConversionUtils.h>
#include <Util/StringUtils.h>
#include <Util/BaseExceptions.h>

#include <SMCUI/IFeatureExportUIHandler.h>
#include <SMCUI/SMCUIPlugin.h>

#include <SMCUI/SlopeCalculationUIHandler.h>

#include <boost/lexical_cast.hpp>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, SlopeCalculationUIHandler);
    DEFINE_IREFERENCED(SlopeCalculationUIHandler, UIHandler);

    SlopeCalculationUIHandler::SlopeCalculationUIHandler()
    {
         //TBD initiliaze the member variables in initialization list
        _addInterface(SMCUI::ISlopeCalculationUIHandler::getInterfaceName());

        setName(getClassname());
    }

    SlopeCalculationUIHandler::~SlopeCalculationUIHandler()
    {
    }

    void SlopeCalculationUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void SlopeCalculationUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    
    void SlopeCalculationUIHandler::setPoint1(CORE::IPoint* point1)
    {
        _point1 = point1;
    }

    CORE::IPoint* SlopeCalculationUIHandler::getPoint1()
    {
        return _point1;
    }

    void SlopeCalculationUIHandler::setPoint2(CORE::IPoint* point2)
    {
        _point2 = point2;
    }

        
    CORE::IPoint* SlopeCalculationUIHandler::getPoint2()
    {
        return _point2;
    }

    void SlopeCalculationUIHandler::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
         // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::FilterStatus status = message.getInterface<GISCOMPUTE::IFilterStatusMessage>()->getStatus();
            if(status == GISCOMPUTE::FILTER_IDLE || status == GISCOMPUTE::FILTER_SUCCESS)
            {
                 //XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage
                    (*SMCUI::ISlopeCalculationUIHandler::SlopeCalculationCompletedMessageType);
                notifyObservers(*SMCUI::ISlopeCalculationUIHandler::SlopeCalculationCompletedMessageType, *msg);
            }
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }
    std::string SlopeCalculationUIHandler::getSlopeText()
    {
        return _slopeInString;

    }

    void SlopeCalculationUIHandler::_deleteLine()
    {
        if(_line.valid())
        {
            getWorldInstance()->removeObjectByID(&(_line->getInterface<CORE::IBase>()->getUniqueID()));
        }
    }

    void SlopeCalculationUIHandler::reset()
    {
        if(_line.valid())
        {
            getWorldInstance()->removeObjectByID(&(_line->getInterface<CORE::IBase>()->getUniqueID()));
        }
        _point1=NULL;
        _point2=NULL;
    }


    void SlopeCalculationUIHandler::_createLine()
    {
        CORE::RefPtr<CORE::IObject> lineObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::LineFeatureObjectType);

        _line = lineObject->getInterface<CORE::ILine>();

        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = _line->getInterface<ELEMENTS::IDrawTechnique>();

        if(draw.valid())
        {
            draw->setHeightOffset(50.0f);
        }

        CORE::RefPtr<CORE::ITemporary> temp = 
            lineObject->getInterface<CORE::ITemporary>();

        if(temp.valid())
        {
            // XXX - not using _temporary 
            temp->setTemporary(true);
        }

        CORE::RefPtr<ELEMENTS::IClamper> clamp = lineObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            clamp->setClampOnPlacement(false);
        }

        // Add the line to the world
        getWorldInstance()->addObject(lineObject.get());

        _line->getInterface<CORE::IPenStyle>()->setPenColor(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
        _line->getInterface<CORE::IPenStyle>()->setPenWidth(3.0f);

    }

    void SlopeCalculationUIHandler::execute()
    {
        if(_line.valid())
        {
             _deleteLine();
        }
        _createLine();
        _line->addPoint(_point1);
        _line->addPoint(_point2);

        osg::Vec3d point1_latLongAlt = UTIL::CoordinateConversionUtils::longLatHeightToLatLongHeight(_point1->getValue());
        osg::Vec3d point2_latLongAlt = UTIL::CoordinateConversionUtils::longLatHeightToLatLongHeight(_point2->getValue());
        
        double rise = point2_latLongAlt.z() - point1_latLongAlt.z();
        point2_latLongAlt.z() = point1_latLongAlt.z();

        osg::Vec3d ecefPoint1 = UTIL::CoordinateConversionUtils::GeodeticToECEF( osg::Vec3d(point1_latLongAlt.y(), point1_latLongAlt.x(), point1_latLongAlt.z()) );
        osg::Vec3d ecefPoint2 = UTIL::CoordinateConversionUtils::GeodeticToECEF( osg::Vec3d(point2_latLongAlt.y(), point2_latLongAlt.x(), point2_latLongAlt.z()) );        
        
        double run  = (ecefPoint2 - ecefPoint1).length();

        if(run != 0)
        {
             double Slope = rise/run; 
            _slopeInString =  UTIL::ToString(osg::RadiansToDegrees(atan(Slope)),3);
            _slopeInString += " degree" ;

        }

    }

}  // namespace SMCUI
