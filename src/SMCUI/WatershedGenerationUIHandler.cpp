#include <SMCUI/WatershedGenerationUIHandler.h>

#include <Terrain/IRasterObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <Core/ITerrain.h>

#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, WatershedGenerationUIHandler);

    DEFINE_IREFERENCED(WatershedGenerationUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    WatershedGenerationUIHandler::WatershedGenerationUIHandler()
    {
        _addInterface(IWatershedGenerationUIHandler::getInterfaceName());
        setName(getClassname());
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _areaExtent     = new osg::Vec2Array;
        _outletExtents  = new osg::Vec2Array;
        _threshold=0.0;
        _watershedname="";
        _streamname="";
        _reset();
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    WatershedGenerationUIHandler::~WatershedGenerationUIHandler(){}

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void WatershedGenerationUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void WatershedGenerationUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void WatershedGenerationUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool WatershedGenerationUIHandler::getFocus() const
    {
        return false;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void WatershedGenerationUIHandler::execute()
    {
        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(vfactory)
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if(world)
            {
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::TauDEMWatershedVisitorType);

                if(!_visitor)
                {
                    LOG_ERROR("Failed to create filter class");
                    return;
                }

                GISCOMPUTE::ITauDEMWatershedVisitor* iVisitor  = 
                    _visitor->getInterface<GISCOMPUTE::ITauDEMWatershedVisitor>();


                //set threshold val
                iVisitor->setThreshold(_threshold);

                if(_areaExtent->size() >1)
                {
                    //set extent on visitor
                    iVisitor->setExtents(_areaExtent.get());
                }
                if(_outletExtents->size() >1)
                {
                    //set extent on visitor
                    iVisitor->setOutletExtents(_outletExtents.get());
                }

                // set stream output name
                iVisitor->setStreamOutputName(_streamname);
                // set watershed output name
                iVisitor->setWatershedOutputName(_watershedname);

                CORE::IBase* applyNode = NULL;
                if(!_elevObj)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    applyNode = terrain->getInterface<CORE::IBase>();
                }
                else
                {
                    applyNode = _elevObj->getInterface<CORE::IBase>();
                }

                //subscribe for filter status
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                applyNode->accept(*_visitor);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void WatershedGenerationUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::ITauDEMWatershedVisitor> visitor = 
                        base->getInterface<GISCOMPUTE::ITauDEMWatershedVisitor>();

                    //if(visitor.valid())
                    //{
                    //     CORE::RefPtr<CORE::IFeatureObject> object = visitor->getOutput();

                    //    if(object)
                    //    {
                    //        CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                    //        if(world)
                    //        {
                    //            osg::ref_ptr<osg::Node> node = object->getInterface<CORE::IObject>()->getOSGNode();
                    //            if(node.valid())
                    //            {
                    //                osg::ref_ptr<osg::StateSet> stateSet = node->getOrCreateStateSet();
                    //                stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::PROTECTED);
                    //            }

                    //            world->addObject(object->getInterface<CORE::IObject>(true));
                    //            /*CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    //            terrain->addRasterObject(object);*/
                    //        }
                    //    }
                    //    else
                    //    {
                    //        throw UTIL::Exception("Failed to create raster object", __FILE__, __LINE__);
                    //    }
                    //}
                }
            }
            catch(...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void WatershedGenerationUIHandler::_reset()
    {
        _areaExtent->clear();
        _outletExtents->clear();
        _elevObj = NULL;
        _visitor = NULL;
        _threshold=0.0;
        _watershedname="";
        _streamname="";
    }

    void WatershedGenerationUIHandler::setExtents(osg::Vec4d extents)
    {
        _areaExtent->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _areaExtent->push_back(osg::Vec2d(first.x(), first.y()));
        _areaExtent->push_back(osg::Vec2d(second.x(), second.y()));
    }
    void WatershedGenerationUIHandler::setOutletExtents(osg::Vec4d extents)
    {
        _outletExtents->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _outletExtents->push_back(osg::Vec2d(first.x(), first.y()));
        _outletExtents->push_back(osg::Vec2d(second.x(), second.y()));
    }


    void WatershedGenerationUIHandler::setThreshold(double value)
    {
        _threshold=value;

    }
    double WatershedGenerationUIHandler::getThreshold() const
    {
        return _threshold;

    }
    void WatershedGenerationUIHandler::setStreamOutputName(std::string& name)
    {
        _streamname = name;
    }
    void WatershedGenerationUIHandler::setWatershedOutputName(std::string& name)
    {
        _watershedname = name;
    }
    osg::Vec4d WatershedGenerationUIHandler::getExtents() const
    {
        osg::Vec2d first = _areaExtent->at(0);
        osg::Vec2d second = _areaExtent->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    osg::Vec4d WatershedGenerationUIHandler::getOutletExtents() const
    {
        osg::Vec2d first  = _outletExtents->at(0);
        osg::Vec2d second = _outletExtents->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }
    void WatershedGenerationUIHandler::setElevationObject(TERRAIN::IElevationObject* obj)
    {
        _elevObj = obj;
    }


    void WatershedGenerationUIHandler::stopFilter()
    {
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }

} // namespace SMCUI

