/*****************************************************************************
*
* File             : TourUIHandler.cpp
* Description      : TourUIHandler class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

//Header files
#include <Core/WorldMaintainer.h>
#include <Core/IAnimationComponent.h>
#include <Core/TickMessage.h>

#include <SMCUI/TourUIHandler.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <SMCUI/ILineUIHandler.h>
#include <serialization/IProjectLoaderComponent.h>
#include <Elements/ISettingComponent.h>
#include <Elements/ElementsUtils.h>
#include <VizUI/IKeyboard.h>

#include <osgViewer/CompositeViewer>
#include <osgText/Text>
#include <osg/ShapeDrawable>

namespace SMCUI 
{

    DEFINE_META_BASE(SMCUI, TourUIHandler);
    DEFINE_IREFERENCED(TourUIHandler, UIHandler);


    TourUIHandler::TourUIHandler() : _tourComponent(NULL), _handlePlaybackControls(true)
    {
        _addInterface(SMCUI::ITourUIHandler::getInterfaceName());
    }

    TourUIHandler::~TourUIHandler(){}

    void TourUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
        VizUI::UIHandler::onAddedToManager();
    }

    void TourUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
        VizUI::UIHandler::onRemovedFromManager();
    }

    void TourUIHandler::_createHUD()
    {
        CORE::RefPtr<APP::IManager> uiManager = _manager->getInterface<APP::IManager>(true);
        osg::ref_ptr<osgViewer::View> view = uiManager->getApplication()->getCompositeViewer()->getView(0);

        osgViewer::ViewerBase::Contexts contexts;

        if(!view.valid())
            return;

        osg::ref_ptr<osgViewer::ViewerBase> viewerBase = view->getViewerBase();

        if(!viewerBase.valid())
            return;

        viewerBase->getContexts(contexts);

        if(contexts.empty())
            return;

        osg::ref_ptr<osg::GraphicsContext> gc;
        gc = contexts[0];

        if(!gc.valid())
            return;

        const osg::GraphicsContext::Traits* traits = gc->getTraits();

        _hudCamera = new osg::Camera;
        _hudCamera->setName("HUDCamera");

        _hudCamera->setProjectionMatrix(osg::Matrix::ortho2D(traits->x, 
            int(traits->width*1.5), traits->y, int(traits->height*1.5)));

        _hudCamera->setGraphicsContext(gc.get());
        _hudCamera->setClearMask(0);

        _hudCamera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
        _hudCamera->setViewMatrix(osg::Matrix::identity());

        _hudCamera->setViewport(traits->x, traits->y, traits->width, traits->height);
        _hudCamera->setRenderOrder(osg::Camera::POST_RENDER);
        _hudCamera->setCullingActive(false);
        _hudCamera->setCullingMode(osg::CullSettings::NO_CULLING);
        //_hudCamera->setDisplaySettings(osg::DisplaySettings::instance());
        _hudCamera->setAllowEventFocus(false);

        //view->getCamera()->addChild(_hudCamera.get());
        view->addSlave(_hudCamera.get(), false);

        //! create text subgraph
        osg::Geode* geode = new osg::Geode;
        osg::ref_ptr<osgText::Font> font = osgText::readFontFile("verdana.ttf");
        //! turn lighting off for the text and disable depth test to ensure it's always on top.
        osg::StateSet* stateset = geode->getOrCreateStateSet();
        stateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

        osg::Vec3 position(10, int(traits->height*1.5) - 100, 0.0);
        osg::Vec3 delta(0.0f, -40.0f, 0.0f);

        _tourNameText = new osgText::Text;
        _tourNameText->setPosition(position);
        if(_tourComponent->getTourNameToPlayOnStart() != "")
        {
            _tourNameText->setText(_tourComponent->getTourNameToPlayOnStart() + "       LoadingProject....");
        }
        else
        {
            _tourNameText->setText("Loading Project ....");
        }

        _tourNameText->setFontResolution(10, 10);
        _tourNameText->setFont(font.get());
        position += delta;
        geode->addDrawable(_tourNameText);

        _tourTimeText = new osgText::Text;
        _tourTimeText->setPosition(position);
        _tourTimeText->setText("Time : 0.0s/0.0s        Speed: 1x");
        _tourTimeText->setFont(font.get());
        position+=delta;
        geode->addDrawable(_tourTimeText);

        _helpText = new osgText::Text;
        _helpText->setPosition(position);
        _helpText->setText("P: Play/Pause   <-/->: Decrease/Increase playback speed     Esc: Quit");
        _helpText->setFont(font.get());
        position+=delta;
        geode->addDrawable(_helpText);

        ////Create buttons
        //osg::ref_ptr<osg::Geometry> pictureQuad = osg::createTexturedQuadGeometry(osg::Vec3(800,800,-1.0),
        //                                   osg::Vec3(30,0.0f,-1.0f),
        //                                   osg::Vec3(0.0, 30,-1.0f),
        //                                   0.0f, 0.0, 1.0, 1.0);

        //osg::ref_ptr<osg::Image> image = osgDB::readImageFile("../../data/gui/IconsSets/DSM_iconSet_Dark/time/play.png");
        //osg::ref_ptr<osg::Texture2D> texture = new osg::Texture2D(image.get());
        //texture->setResizeNonPowerOfTwoHint(true);
        //texture->setWrap(osg::Texture::WRAP_R, osg::Texture::CLAMP_TO_EDGE);
        //texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
        //pictureQuad->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture.get(), osg::StateAttribute::ON);
        //geode->addDrawable(pictureQuad.get());


        //{
        //    osg::BoundingBox bb;
        //    for(unsigned int i =0; i<geode->getNumDrawables(); ++i)
        //    {
        //        bb.expandBy(geode->getDrawable(i)->getBound());
        //    }

        //    osg::Geometry* geom = new osg::Geometry;
        //    osg::Vec3Array* vertices = new osg::Vec3Array;
        //    float depth = bb.zMin() - 0.1;
        //    vertices->push_back(osg::Vec3(bb.xMin(),bb.yMax(),depth));
        //    vertices->push_back(osg::Vec3(bb.xMin(),bb.yMin(),depth));
        //    vertices->push_back(osg::Vec3(bb.xMax(),bb.yMin(),depth));
        //    vertices->push_back(osg::Vec3(bb.xMax(),bb.yMax(),depth));
        //    geom->setVertexArray(vertices);
        //    osg::Vec3Array* normals = new osg::Vec3Array;
        //    normals->push_back(osg::Vec3(0.0f,0.0f,1.0f));
        //    geom->setNormalArray(normals, osg::Array::BIND_OVERALL);

        //    osg::Vec4Array* colors = new osg::Vec4Array;
        //    colors->push_back(osg::Vec4(1.0f,1.0,0.8f,0.2f));
        //    geom->setColorArray(colors, osg::Array::BIND_OVERALL);

        //    geom->addPrimitiveSet(new osg::DrawArrays(GL_QUADS,0,4));

        //    osg::StateSet* stateset = geom->getOrCreateStateSet();
        //    stateset->setMode(GL_BLEND,osg::StateAttribute::ON);
        //    //stateset->setAttribute(new osg::PolygonOffset(1.0f,1.0f),osg::StateAttribute::ON);
        //    stateset->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

        //    geode->addDrawable(geom);
        //}


        _hudCamera->addChild(geode);

        //! Bounding box

        _handlePlaybackControls = true;
    }

    void TourUIHandler::saveSequence(const std::string &sequenceName, const std::vector<std::string> &sequence)
    {
        if(!_tourComponent.valid())
            return;

        _tourComponent->saveSequence(sequenceName, sequence);
    }

    void TourUIHandler::deleteSequence(const std::string& tourName)
    {
        if(!_tourComponent.valid())
            return;

        _tourComponent->deleteSequence(tourName);
    }

    void TourUIHandler::playSequence(const std::string& tourName)
    {
        if(!_tourComponent.valid())
            return;

        _tourComponent->playSequence(tourName);
    }

    void TourUIHandler::playNextTourInCurrentSequence()
    {
        if(!_tourComponent.valid())
            return;

        _tourComponent->playNextTourInCurrentSequence();
    }

    void TourUIHandler::playPreviousTourInCurrentSequence()
    {
        if(!_tourComponent.valid())
            return;

        _tourComponent->playPreviousTourInCurrentSequence();
    }

    void TourUIHandler::setSequenceName(const std::string& oldSequenceName, const std::string& newSequenceName)
    {
        if(_tourComponent.valid())
        {
            _tourComponent->setSequenceName(oldSequenceName, newSequenceName);
        }
    }

    const SMCElements::ITourComponent::MapSequenceToTourList& TourUIHandler::getSequenceList() const
    {
        static SMCElements::ITourComponent::MapSequenceToTourList map;

        if(!_tourComponent.valid())
            return map;

        return _tourComponent->getSequenceList();
    }

    void TourUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            //Getting the tour component from the worldMaintainer 
            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                CORE::WorldMaintainer::instance();

            CORE::RefPtr<CORE::IComponent> tourcomponent             = 
                worldMaintainer->getComponentByName("TourComponent");
            if(tourcomponent.valid())
            {
                _tourComponent  = tourcomponent->getInterface<SMCElements::ITourComponent>();
            }

            //subscribing to the world changes message type
            _subscribe(worldMaintainer, *CORE::IWorld::WorldLoadedMessageType);
        }
        else if(messageType == *CORE::WorldMaintainer::TickMessageType)
        {
            if(_tourComponent.valid())
            {
                if(!_createHud)
                    return;

                if(!_tourNameText.valid())
                {
                    _createHUD();
                }

                if(!_projectLoaded)
                {
                    static int t = 0;
                    int i = ((t/20)%4);
                    std::string dots;
                    for(; i > 0; i--)
                    {
                        dots+=".";
                    }

                    t++;

                    _tourNameText->setText(_tourComponent->getTourNameToPlayOnStart() + "    : Loading Project" + dots);
                }
                else
                {
                    _tourNameText->setText(_tourComponent->getTourNameToPlayOnStart());

                    std::string totalTime = UTIL::ToString(getTourDuration()/100000);
                    std::string elapsedTime = UTIL::ToString(getTourCurrentTime()/100000);
                    std::string speed = UTIL::ToString(getSpeedFactor());

                    std::string timeText = "Time : " + elapsedTime + "/" + totalTime + "s    Playback Speed : " + speed + "x";
                    _tourTimeText->setText(timeText);
                }
            }
        }
        else if(messageType==*CORE::IWorld::WorldLoadedMessageType)
        {
            //we have to reset the handler
            _reset();

            CORE::RefPtr<CORE::IComponent> projectLoaderComponent =
                ELEMENTS::GetComponentFromMaintainer("ProjectLoaderComponent");
            if(projectLoaderComponent.valid())
            {
                _subscribe(projectLoaderComponent.get(), *DB::IProjectLoaderComponent::ProjectLoadedMessageType);
            }

            //!
            _projectLoaded = false;
            _createHud = false;
            CORE::RefPtr<ELEMENTS::ISettingComponent> settingComponent = 
                dynamic_cast<ELEMENTS::ISettingComponent*>(ELEMENTS::GetComponentFromMaintainer("SettingComponent"));
            if(!settingComponent.valid())
            {
                return;
            }

            if(settingComponent->getAppMode() == ELEMENTS::ISettingComponent::APP_MODE_QT)
            {
                return;
            }

            //! Subscribe to tick message to update HUD
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            _subscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);

            //! Create HUD
            _createHud = true;

        }
        else if(messageType == *DB::IProjectLoaderComponent::ProjectLoadedMessageType)
        {
            _projectLoaded = true;
        }
        else
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }

    void TourUIHandler::reset()
    {
        if(_tourComponent.valid())
        {
            _tourComponent->reset();
        }
    }

    void TourUIHandler::resetAll()
    {
        if(_tourComponent.valid())
        {
            _tourComponent->resetAll();
        }
    }

    bool TourUIHandler::startTourRecording()
    {
        if(!_tourComponent.valid())
            return false; //No component for handling the tour related stuff

        return _tourComponent->startRecording();
    }

    bool TourUIHandler::stopTourRecording()
    {
        if(!_tourComponent.valid())
            return false; //No component for handling the tour related stuff

        return _tourComponent->stopRecording();
    }

    bool TourUIHandler::pauseTourRecording()
    {
        if(!_tourComponent.valid())
            return false; //No component for handling the tour related stuff

        return _tourComponent->pauseRecording();
    }

    bool TourUIHandler::saveCurrentRecordedTour(const std::string &tourName, bool overwrite)
    {
        if(!_tourComponent.valid())
            return false; //No component for handling the tour related stuff

        //Now we are saving the tour
        _tourComponent->saveCurrentRecorded(tourName, overwrite);
        return true;
    }

    float TourUIHandler::getSpeedFactor() const
    {
        if(!_tourComponent.valid())
            return 1.0;

        return _tourComponent->getSpeedFactor();
    }

    void TourUIHandler::setSpeedFactor(float speedFactor)
    {
        if(_tourComponent.valid())
        {
            _tourComponent->setSpeedFactor(speedFactor);
        }
    }

    void TourUIHandler::increaseSpeedFactor()
    {
        if(_tourComponent.valid())
        {
            _tourComponent->increaseSpeedFactor();
        }
    }

    void TourUIHandler::decreaseSpeedFactor()
    {
        if(_tourComponent.valid())
        {
            _tourComponent->decreaseSpeedFactor();
        }
    }

    bool TourUIHandler::playTour(const std::string &tourName)
    {
        if(!_tourComponent.valid())
            return false; //No component for handling the tour related stuff

        _tourComponent->playTour(tourName);
        return true;
    }

    bool TourUIHandler::deleteTour(const std::string & tourName)
    {
        if(!_tourComponent.valid())
            return false; //No component for handling the tour related stuff

        _tourComponent->deleteTour(tourName);
        return true;
    }

    SMCElements::ITourComponent::TourMode
        TourUIHandler::getTourState() const
    {
        if(!_tourComponent.valid())
            return SMCElements::ITourComponent::Tour_None;

        return _tourComponent->getTourMode();
    }


    SMCElements::ITourComponent::PlayerState
        TourUIHandler::getPlayerState() const
    {
        if(!_tourComponent.valid())
            return SMCElements::ITourComponent::Player_None;

        return _tourComponent->getPlayerStatus();
    }

    SMCElements::ITourComponent::RecorderState
        TourUIHandler::getRecordState() const
    {
        if(!_tourComponent.valid())
            return SMCElements::ITourComponent::Recorder_None;

        return _tourComponent->getRecorderStatus();
    }

    const SMCElements::ITourComponent::MapTourToFile& 
        TourUIHandler::getTourMap() const
    {
        static SMCElements::ITourComponent::MapTourToFile map;
        if(!_tourComponent.valid())
            return map;

        return _tourComponent->getTourMap();
    }

    // Pause current playing tour
    bool TourUIHandler::pauseTour()
    {
        if(!_tourComponent.valid())
            return false;

        return _tourComponent->pauseTour();
    }

    // Stop the current playing tour
    bool TourUIHandler::stopTour()
    {
        if(!_tourComponent.valid())
            return false;

        return _tourComponent->stopTour();
    }

    // get the duration of current loaded tour of tour component
    unsigned int TourUIHandler::getTourDuration()
    {
        if(!_tourComponent.valid())
            return 0;

        return _tourComponent->getTourDuration();
    }

    // set the tour duration(To provide seek functionality while tour is playing)
    void TourUIHandler::setTourCurrTime(unsigned int currTime)
    {
        if(!_tourComponent.valid())
            return;

        _tourComponent->setTourCurrentTime(currTime);
    }

    // to show a snap of tour at the time duration specified
    void TourUIHandler::previewAtCheckPoint(unsigned int duration)
    {
        if(!_tourComponent.valid())
            return;

        _tourComponent->previewSnap(duration);
    }


    void TourUIHandler::_reset()
    {
        if(_tourComponent.valid())
        {
            _tourComponent->reset();
        }
    }

    std::string TourUIHandler::getCurrentTourName() const
    {
        std::string result("");
        if(_tourComponent.valid())
        {
            result =_tourComponent->getCurrentTourName();
        }

        return result;
    }

    std::string TourUIHandler::getCurrentSequenceName() const
    {
        std::string result("");
        if(_tourComponent.valid())
        {
            result =_tourComponent->getCurrentSequenceName();
        }

        return result;
    }

    bool TourUIHandler::setTourName( std::string oldTourName, std::string newTourName)
    {
        if( !_tourComponent.valid() )
        {
            return false;
        }

        return _tourComponent->setTourName( oldTourName, newTourName );
    }

    unsigned int
        TourUIHandler::getTourCurrentTime() const
    {
        unsigned int result = 0;
        if(_tourComponent.valid())
        {
            result = _tourComponent->getTourCurrentTime();
        }

        return result;
    }

    void TourUIHandler::setRecorderType(SMCElements::ITourComponent::RecorderType type)
    {
        if(_tourComponent.valid())
        {
            _tourComponent->setRecorderType(type);
        }
    }

    SMCElements::ITourComponent::RecorderType TourUIHandler::getRecorderType() const
    {
        if(_tourComponent.valid())
        {
            return _tourComponent->getRecorderType();
        }
        // XXX -- handle return value properly if component not found
        return SMCElements::ITourComponent::CONTINUOUS;
    }

    void TourUIHandler::recordKeyframe()
    {
        _tourComponent->recordKeyframe();
    }

    bool 
        TourUIHandler::handleKeyPressed(VizUI::IKeyboard* keyboard, int key)
    {

        /*        if(key == 'c' || key == 'C')
        {
        _tourComponent->setRecorderType(SMCElements::ITourComponent::CONTINUOUS);
        }
        else if(key == 'v' || key == 'V')
        {
        _tourComponent->setRecorderType(SMCElements::ITourComponent::USER_KEYFRAME);
        }
        else */if(key == 'x' || key == 'X')
        {
            recordKeyframe();
        }
        else if(_handlePlaybackControls)
        {
            //if(keyboard->getModKeyMask() & VizUI::IKeyboard::MODKEY_CTRL)
            {
                if(key == 'p' || key == 'P')
                {
                    if(getPlayerState() == SMCElements::ITourComponent::Player_Playing)
                    {
                        pauseTour();
                    }
                    else if(getPlayerState() == SMCElements::ITourComponent::Player_Pause)
                    {
                        playTour("");
                    }
                    else
                    {
                        playTour(_tourComponent->getTourNameToPlayOnStart());
                    }
                }
                else if(key == VizUI::IKeyboard::KEY_Left)
                {
                    decreaseSpeedFactor();
                }
                else if(key == VizUI::IKeyboard::KEY_Right)
                {
                    increaseSpeedFactor();
                }
            }
        }
        /*else if(key == 'l' || key == 'L')
        {
        CORE::RefPtr<SMCUI::ILineUIHandler> lineUIHandler = 
        APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getManager());
        if(lineUIHandler.valid())
        {
        if(lineUIHandler->getMode() == SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE)
        {
        lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);
        }
        else
        {
        lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE);
        }
        }
        }*/

        return false;
    }

    bool 
        TourUIHandler::handleKeyReleased(VizUI::IKeyboard* keyboard, int key)
    {
        return false;
    }

    void TourUIHandler::createNewCheckPoint(unsigned int duration)
    {
        if( !_tourComponent.valid() )
        {
            return;
        }
        _tourComponent->createCheckPoint(duration, getSpeedFactor());
        _tourComponent->saveSpeedProfile();
    }

    void TourUIHandler::editCheckPoint(unsigned int oldDuration, unsigned int newDuration, double newSpeed)
    {
        if( !_tourComponent.valid() )
        {
            return;
        }
        _tourComponent->deleteCheckPoint(oldDuration);
        _tourComponent->createCheckPoint(newDuration, newSpeed);
        _tourComponent->saveSpeedProfile();
    }

    void TourUIHandler::deleteCheckPoint(unsigned int duration)
    {
        if( !_tourComponent.valid() )
        {
            return;
        }
        _tourComponent->deleteCheckPoint(duration);
        _tourComponent->saveSpeedProfile();
    }

    void TourUIHandler::resetSpeedProfile()
    {
        if( !_tourComponent.valid() )
        {
            return;
        }
        _tourComponent->resetSpeedProfile();
        _tourComponent->saveSpeedProfile();
    }

    void TourUIHandler::saveSpeedProfile()
    {
        if( !_tourComponent.valid() )
        {
            return;
        }
        _tourComponent->saveSpeedProfile();
    }


    SMCElements::ITourComponent::MapTimeToSpeed TourUIHandler::getCheckPoints()
    {
        SMCElements::ITourComponent::MapTimeToSpeed map;
        if(!_tourComponent.valid())
            return map;

        return _tourComponent->getCurrentCheckpointMap();
    }


} // namespace SMCUI
