/*****************************************************************************
 *
 * File             : RadioLOSUIHandler.cpp
 * Description      : RadioLOSUIHandler class definition
 *
 *****************************************************************************
 * Copyright 2010, VizExperts India Private Limited (unpublished)
 *****************************************************************************/

#include <SMCUI/RadioLOSUIHandler.h>

#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>
#include <Core/ITerrain.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IRadioLOSVisitor.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <Core/IWorldMaintainer.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <App/AccessElementUtils.h>
#include <Util/Log.h>

#include <Core/WorldMaintainer.h>
#include <Core/AttributeTypes.h>





namespace SMCUI 
{

    void RadioLOSCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
    {
        traverse(node, nv);

        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(mutex);

        if(obj.valid())
        {
            CORE::RefPtr<CORE::IWorld> world = 
                    &(*(CORE::WorldMaintainer::instance()->getWorldMap().begin())->second);

            world->addObject(obj.get());
            
            obj = NULL;
        }
    }

    DEFINE_META_BASE(SMCUI, RadioLOSUIHandler);
    DEFINE_IREFERENCED(RadioLOSUIHandler, UIHandler);

    RadioLOSUIHandler::RadioLOSUIHandler()
        : _radioLOSVisitor(NULL)
        , _radioLOSCallback(NULL)
        ,  _transmitterPos(osg::Vec3d(0.0, 0.0, 0.0))
        , _receiverPos(osg::Vec3d(0.0, 0.0, 0.0))
        , _transObj(NULL)
        , _recvObj(NULL)
        , _txPower(0.0)
        , _txAntennaGain(0.0)
        , _rxAntennaGain(0.0)
        , _cableLoss(0.0)
        , _rxSensitivity(0.0)
        , _frequency(0.0)
        , _kFactor(4.0/3.0)
        , _rangeStep(0.0)
        , _outputName("")
        , _heightIncrementStep(10.0)
    {
        _addInterface(IRadioLOSUIHandler::getInterfaceName());
        setName(getClassname());
        //_reset();
    }

    void 
    RadioLOSUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void 
    RadioLOSUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }
    
    void
    RadioLOSUIHandler::setFocus(bool value)
    {



        try
        {
            UIHandler::setFocus(value);
            if(value)
            {
                // Reset value
                _reset();

                CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                if(NULL == world)
                {
                    throw UTIL::Exception("Failed to get instance from world", __FILE__, __LINE__);
                }

                _radioLOSCallback = new RadioLOSCallback();
                OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_radioLOSCallback->mutex);
                world->getOSGGroup()->addUpdateCallback(_radioLOSCallback.get());
            }
            else if (_radioLOSCallback.valid())
            {
                CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                if(NULL == world)
                {
                    throw UTIL::Exception("Failed to get instance from world", __FILE__, __LINE__);
                }

                {
                    OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_radioLOSCallback->mutex);
                    world->getOSGGroup()->removeUpdateCallback(_radioLOSCallback.get());
                }
                _radioLOSCallback = NULL;

                // Reset value
                _reset();
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void
    RadioLOSUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "FireEffectUIHandler attributes";

        // step size in degrees
        _addAttribute(new CORE::DoubleAttribute("HeightIncrementStep", "HeightIncrementStep",
                    CORE::DoubleAttribute::SetFuncType(this, &RadioLOSUIHandler::setHeightIncrementStep),
                    CORE::DoubleAttribute::GetFuncType(this, &RadioLOSUIHandler::getHeightIncrementStep),
                    "Height Increment Step",
                    groupName));

    }

    void 
    RadioLOSUIHandler::setHeightIncrementStep(double heightIncrementStep)
    {
        _heightIncrementStep = heightIncrementStep;
    }

    double 
    RadioLOSUIHandler::getHeightIncrementStep()
    {
        return _heightIncrementStep;
    }


    void 
    RadioLOSUIHandler::setTransmitterPosition(const osg::Vec3d& pos)
    {
        _transmitterPos = pos;
    }


    void 
    RadioLOSUIHandler::setReceiverPosition(const osg::Vec3d& pos)
    {
        _receiverPos = pos;
    }

    void
    RadioLOSUIHandler::setTransmitterHeight(double transHeight)
    {
        _transHeight = transHeight;
    }

    void
    RadioLOSUIHandler::setReceiverHeight(double recvHeight)
    {
        _recvHeight = recvHeight;
    }


    void
    RadioLOSUIHandler::setTrasmitterIcon(CORE::IObject* transObj)
    {
        _transObj = transObj;
    }


    void
    RadioLOSUIHandler::setReceiverIcon(CORE::IObject* recvObj)
    {
        _recvObj = recvObj;
    }


    void
    RadioLOSUIHandler::setWaveFrequency(double frequency)
    {
        _frequency = frequency;
    }


    void
    RadioLOSUIHandler::setEffectiveEarthRadiusFactor(double kFactor)
    {
        _kFactor = kFactor;
    }


    void
    RadioLOSUIHandler::setOutputName(std::string& name)
    {
        _outputName = name;
    }


    void
    RadioLOSUIHandler::setTransmitterPower(double txPower)
    {
        _txPower = txPower;
    }

    void
    RadioLOSUIHandler::setAntennaGain(double txGain, double rxGain)
    {
        _txAntennaGain = txGain;
        _rxAntennaGain = rxGain;
    }


    void
    RadioLOSUIHandler::setReceiverSensitivity(double rxSensitivity)
    {
        _rxSensitivity = rxSensitivity;
    }

    void
    RadioLOSUIHandler::setCableLoss(double cableLoss)
    {
        _cableLoss = cableLoss;
    }

    void
    RadioLOSUIHandler::setRangeStep(double rangeStep)
    {
        _rangeStep = rangeStep;
    }

    bool
    RadioLOSUIHandler::losClearance()
    {
        if(_radioLOSVisitor.valid())
        {
            return _radioLOSVisitor->losClearance();
        }
        else
        {
            throw UTIL::Exception("Invalid Filter Visitor", __FILE__, __LINE__);
        }
    }

    double
    RadioLOSUIHandler::getOutputReceiverPower()
    {
        if(_radioLOSVisitor.valid())
        {
            return _radioLOSVisitor->getOutputReceiverPower();
        }
        else
        {
            throw UTIL::Exception("Invalid Filter Visitor", __FILE__, __LINE__);
        }
    }

    double
    RadioLOSUIHandler::getOutputSOM()
    {
        if(_radioLOSVisitor.valid())
        {
            return _radioLOSVisitor->getOutputSOM();
        }
        else
        {
            throw UTIL::Exception("Invalid Filter Visitor", __FILE__, __LINE__);
        }
    }

    double
    RadioLOSUIHandler::getOutputFSL()
    {
        if(_radioLOSVisitor.valid())
        {
            return _radioLOSVisitor->getOutputFSL();
        }
        else
        {
            throw UTIL::Exception("Invalid Filter Visitor", __FILE__, __LINE__);
        }
    }

    double
    RadioLOSUIHandler::getOutputEIRP()
    {
        if(_radioLOSVisitor.valid())
        {
            return _radioLOSVisitor->getOutputEIRP();
        }
        else
        {
            throw UTIL::Exception("Invalid Filter Visitor", __FILE__, __LINE__);
        }
    }

    std::string&
    RadioLOSUIHandler::getDecisionSupport()
    {
        if(_radioLOSVisitor.valid())
        {
            return _radioLOSVisitor->getDecisionSupport();
        }
        else
        {
            throw UTIL::Exception("Invalid Filter Visitor", __FILE__, __LINE__);
        }
    }



    void 
    RadioLOSUIHandler::execute(bool multiThreaded)
    {
        CORE::IWorld* world = getWorldInstance();
        if(NULL == world)
        {
            throw UTIL::Exception("invalid world instance", __FILE__, __LINE__);
        }

        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(NULL == vfactory)
        {
            throw UTIL::Exception("unable to create visitor factor", __FILE__, __LINE__);
        }

        CORE::RefPtr<CORE::IBaseVisitor> visitor =
            vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::RadioLOSVisitorType);

        if(!visitor)
        {
            throw UTIL::Exception("invalid visitor", __FILE__, __LINE__);
        }

        _radioLOSVisitor = visitor->getInterface<GISCOMPUTE::IRadioLOSVisitor>();

        if(!_radioLOSVisitor.valid())
        {
            throw UTIL::Exception("invalid visitor", __FILE__, __LINE__);
        }

        CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

        if(!terrain.valid())
        {
            throw UTIL::Exception("invalid terrain instance", __FILE__, __LINE__);
        }


        // set input params
        _radioLOSVisitor->setTransmitterPosition(_transmitterPos);
        _radioLOSVisitor->setReceiverPosition(_receiverPos);
        _radioLOSVisitor->setTrasmitterIcon(_transObj);
        _radioLOSVisitor->setReceiverIcon(_recvObj);
        _radioLOSVisitor->setWaveFrequency(_frequency);
        _radioLOSVisitor->setEffectiveEarthRadiusFactor(_kFactor);
        _radioLOSVisitor->setOutputName(_outputName);
        _radioLOSVisitor->setTransmitterPower(_txPower);
        _radioLOSVisitor->setAntennaGain(_txAntennaGain, _rxAntennaGain);
        _radioLOSVisitor->setReceiverSensitivity(_rxSensitivity);
        _radioLOSVisitor->setCableLoss(_cableLoss);
        _radioLOSVisitor->setRangeStep(_rangeStep);
        _radioLOSVisitor->setHeightIncrementStep(_heightIncrementStep);
        _radioLOSVisitor->setTransmitterHeight(_transHeight);
        _radioLOSVisitor->setReceiverHeight(_recvHeight);
        _radioLOSVisitor->setMultiThreaded(multiThreaded);

        CORE::IBase* terrainBase = terrain->getInterface<CORE::IBase>();

        //subscribe for filter status
        _subscribe(_radioLOSVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

        terrainBase->accept(*visitor.get());
    }



    void 
    RadioLOSUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                            message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::IRadioLOSVisitor> visitor = 
                            base->getInterface<GISCOMPUTE::IRadioLOSVisitor>();
                   
                    // only message intended for radio coverage is to be handled, or its to verify the sender
                    if(visitor.valid())
                    {
                        CORE::IObject* object = visitor->getOutput();
                        /*if(NULL == object)
                        {
                            throw UTIL::Exception("Failed to create radio LOS output", __FILE__, __LINE__);
                        }*/

                        if( (NULL != object) )
                        {
                            if(!_radioLOSCallback.valid())
                            {
                                throw UTIL::Exception("radio LOS call back instance is invalid", __FILE__, __LINE__);
                            }
                            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_radioLOSCallback->mutex);
                            _radioLOSCallback->obj = object;
                        }
                    }
                }
            }
            catch(UTIL::Exception& e)
            {
                e.LogException();
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }
            catch(...)
            {
                LOG_ERROR("Unknown Error")
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }



    void
    RadioLOSUIHandler::stopFilter()
    {
        if(_radioLOSVisitor.valid())
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _radioLOSVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
            iVisitor->stopFilter();
        }
        else
        {

            throw UTIL::Exception("Filter Stop Fail", __FILE__, __LINE__);
        }

    }

    void
    RadioLOSUIHandler::_reset()
    {
        _radioLOSVisitor = NULL;
        _radioLOSCallback = NULL;
        _transmitterPos.set(0.0, 0.0, 0.0);
        _receiverPos.set(0.0, 0.0, 0.0);
        _transObj = NULL;
        _recvObj = NULL;
        _txPower = 0.0;
        _txAntennaGain = 0.0;
        _rxAntennaGain = 0.0;
        _cableLoss = 0.0;
        _rxSensitivity = 0.0;
        _frequency = 0.0;
        _kFactor = 4.0/3.0;
        _rangeStep = 0.0;
        _outputName = "";
        _heightIncrementStep = 10.0;
    }


} // namespace SMCUI

