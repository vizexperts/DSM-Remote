#include <SMCUI/DraggerContainer.h>
#include <SMCUI/DraggerUIHandler.h>

#include <osg/AutoTransform>
#include <osg/ShapeDrawable>
#include <osg/LineWidth>

#include <osgViewer/View>

#include <Core/IObject.h>
#include <Elements/IModelHolder.h>
#include <Util/CoordinateConversionUtils.h>
#include <osg/Texture2D>
#include <osgDB/ReadFile>

//////////////////////////////////////////////////////////////////////////////////
//!
//! \file DraggerContainer.cpp
//!
//! \brief DraggerContainer class definition
//!
//////////////////////////////////////////////////////////////////////////////////
namespace SMCUI
{
    //! Dragger container
    DraggerContainer::DraggerContainer(DraggerUIHandler* draggerUIHandler)
        : _draggerSize(240.0f)
        , _fixedInScreen(true)
        , _draggerUIHandler(draggerUIHandler)
        ,_selection(new osg::MatrixTransform)
    {
        //! Initialize child draggers
        _translatePlaneDragger = new CustomTranslatePlaneDragger;
        initializeDragger(_translatePlaneDragger);

        _translateAxisDragger = new CustomTranslateAxisDragger;
        initializeDragger(_translateAxisDragger);

        _rotateDragger = new CustomTrackballDragger;
        initializeDragger(_rotateDragger);

        _scaleDragger = new CustomScaleDragger;
        initializeDragger(_scaleDragger);

        _selection->setNodeMask(0);

        //! Create button node
        createButtons();
    }

    void DraggerContainer::initializeDragger(osg::ref_ptr<osgManipulator::Dragger> dragger)
    {
        addChild(dragger);
        addDragger(dragger);
        dragger->setNodeMask(0);
        dragger->addDraggerCallback(new DraggerUpdateCallback(this));
        dragger->addTransformUpdating(_selection.get());
        dragger->setHandleEvents(true);
    }

    void DraggerContainer::setDraggerSize(float size)
    {
        _draggerSize = size;
    }

    float DraggerContainer::getDraggerSize() const
    {
        return _draggerSize;
    }

    void DraggerContainer::setFixedInScreen(bool value)
    {
        _fixedInScreen = value;
    }

    bool DraggerContainer::getFixedInScreen() const
    {
        return _fixedInScreen;
    }

    DraggerUIHandler* DraggerContainer::getDraggerUIHandler()
    {
        return _draggerUIHandler;
    }

    osgManipulator::Dragger* DraggerContainer::getTranslateAxisDragger()
    {
        return _translateAxisDragger.get();
    }

    osgManipulator::Dragger* DraggerContainer::getTranslatePlaneDragger()
    {
        return _translatePlaneDragger.get();
    }

    osgManipulator::Dragger* DraggerContainer::getRotateDragger()
    {
        return _rotateDragger.get();
    }

    osgManipulator::Dragger* DraggerContainer::getScaleDragger()
    {
        return _scaleDragger.get();
    }

    osg::MatrixTransform* DraggerContainer::getSelectionMatrix()
    {
        return _selection.get();
    }

    osg::MatrixTransform* DraggerContainer::getButtonsTransform()
    {
        return _buttonsTransform.get();
    }
    double DraggerContainer::getScale()
    {
        return _scale;
    }

    ELEMENTS::IModelHolder* DraggerContainer::getAssociatedModel()
    {
        return _associatedModel.get();
    }

    void DraggerContainer::createButtons()
    {
        _buttonsTransform = new osg::MatrixTransform;
        _buttonsTransform->setNodeMask(0);

        //! Auto transform to restrict rotation along Up Axis
        osg::AutoTransform* autoTransform = new osg::AutoTransform;
        autoTransform->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_AXIS);
        autoTransform->setAxis(osg::Vec3(0, 0, 1));

        //! Add autoTransform to _buttonTransform
        _buttonsTransform->addChild(autoTransform);

        //! Path for Dragger button icons
        std::string imagePathLocation = "../../data/ManipulatorButtons/";

        std::string translateImagePath = imagePathLocation + "translate.png";
        std::string rotateImagePath = imagePathLocation + "rotate.png";
        std::string scaleImagePath = imagePathLocation + "scale.png";

        //! Create Buttons
        _translateButton = createButton(COLORS_RED, 0, "TranslateButton", translateImagePath);
        _rotateButton = createButton(COLORS_BLUE, 20, "RotateButton", rotateImagePath);
        _scaleButton = createButton(COLORS_GREEN, -20, "ScaleButton", scaleImagePath);

        //! Add buttons to autoTransform
        autoTransform->addChild(_rotateButton);
        autoTransform->addChild(_scaleButton);
        autoTransform->addChild(_translateButton);

        //! Add _buttonsTransform to DraggerContainer
        addChild(_buttonsTransform);
    }

    void DraggerContainer::setDraggerMode(SMCUI::IDraggerUIHandler::DraggerMode mode)
    {
        _draggerMode = mode;

        //! Scale of button when selected
        float selectedScale = 1.1f;

        switch(mode)
        {
        case IDraggerUIHandler::DRAGGER_MODE_TRANSLATE:
            {
                setAssociatedModel(_associatedModel);
                _buttonsTransform->setNodeMask(1);

                _translateAxisDragger->setNodeMask(1);
                _translatePlaneDragger->setNodeMask(1);
                _translateButton->setMatrix(osg::Matrix::scale(osg::Vec3(selectedScale, 
                    selectedScale, selectedScale)));

                _rotateDragger->setNodeMask(0);
                _rotateButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));

                _scaleDragger->setNodeMask(0);
                _scaleButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));
                break;
            }
        case IDraggerUIHandler::DRAGGER_MODE_ROTATE:
            {
                setAssociatedModel(_associatedModel);
                _buttonsTransform->setNodeMask(1);

                _translateAxisDragger->setNodeMask(0);
                _translatePlaneDragger->setNodeMask(0);
                _translateButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));

                _rotateDragger->setNodeMask(1);
                _rotateButton->setMatrix(osg::Matrix::scale(osg::Vec3(selectedScale, 
                    selectedScale, selectedScale)));

                _scaleDragger->setNodeMask(0);
                _scaleButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));
                break;
            }
        case IDraggerUIHandler::DRAGGER_MODE_SCALE:
            {
                setAssociatedModel(_associatedModel);
                _buttonsTransform->setNodeMask(1);

                _translateAxisDragger->setNodeMask(0);
                _translatePlaneDragger->setNodeMask(0);
                _translateButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));

                _rotateDragger->setNodeMask(0);
                _rotateButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));

                _scaleDragger->setNodeMask(1);
                _scaleButton->setMatrix(osg::Matrix::scale(osg::Vec3(selectedScale, 
                    selectedScale, selectedScale)));
                break;
            }
        default:
            {
                _buttonsTransform->setNodeMask(0);

                _translateAxisDragger->setNodeMask(0);
                _translatePlaneDragger->setNodeMask(0);
                _translateButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));

                _rotateDragger->setNodeMask(0);
                _rotateButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));

                _scaleDragger->setNodeMask(0);
                _scaleButton->setMatrix(osg::Matrix::scale(osg::Vec3(1.0f, 1.0f, 1.0f)));

                _associatedModel = NULL;
            }
        }
    }

    osg::MatrixTransform* DraggerContainer::createButton(osg::Vec4 color, double offsetAngle, 
        std::string objectName, std::string imageFilePath)
    {
        //! button node
        osg::MatrixTransform* buttonNode = new osg::MatrixTransform;

        //! button Geode
        osg::Geode* button = new osg::Geode;
        button->setName(objectName);

        //! Cylinder
        {
            osg::Cylinder* cylinder = new osg::Cylinder;
            cylinder->setHeight(0.05f);
            cylinder->setRadius(0.1f);
            osg::ShapeDrawable* cylinderDrawable = new osg::ShapeDrawable(cylinder);
            cylinderDrawable->setColor(color);
            button->addDrawable(cylinderDrawable);
        }

        //! outline circle
        {
            osg::Geometry* circleGeometry = createCircleGeometry(0.1f, 20);
            osg::Vec4Array* circleGeometryColorArray = new osg::Vec4Array;
            circleGeometryColorArray->push_back(COLORS_BLACK);
            circleGeometry->setColorArray(circleGeometryColorArray);
            circleGeometry->setColorBinding(osg::Geometry::BIND_OVERALL);
            circleGeometry->getOrCreateStateSet()->
                setAttributeAndModes(new osg::LineWidth(3.0f), osg::StateAttribute::ON);

            button->addDrawable(circleGeometry);
        }

        //! button texture
        if(imageFilePath != "")
        {
            osg::Texture2D* buttonTexture = new osg::Texture2D;
            buttonTexture->setDataVariance(osg::Object::DYNAMIC);

            osg::Image* image = osgDB::readImageFile(imageFilePath);
            buttonTexture->setImage(image);

            osg::Geometry* texGeom = new osg::Geometry;
            button->addDrawable(texGeom);

            // 
            osg::Vec3Array* textureGeometryArray = new osg::Vec3Array;
            float halfSize = 0.07f;
            textureGeometryArray->push_back(osg::Vec3(-halfSize, -halfSize, 0.0));
            textureGeometryArray->push_back(osg::Vec3(halfSize, -halfSize, 0.0));
            textureGeometryArray->push_back(osg::Vec3(halfSize, halfSize, 0.0));
            textureGeometryArray->push_back(osg::Vec3(-halfSize, halfSize, 0.0));
            texGeom->setVertexArray(textureGeometryArray);

            osg::Vec3Array* normals = new osg::Vec3Array;
            normals->push_back(osg::Vec3(0.0f, 0.0f, 1.0f));
            texGeom->setNormalArray(normals);
            texGeom->setNormalBinding(osg::Geometry::BIND_OVERALL);

            osg::Vec4Array* textureColorArray = new osg::Vec4Array;
            textureColorArray->push_back(osg::Vec4(0.0, 0.0, 0.0, 1.0));
            texGeom->setColorArray(textureColorArray);
            texGeom->setColorBinding(osg::Geometry::BIND_OVERALL);

            osg::Vec2Array* texCoordArray = new osg::Vec2Array;
            texCoordArray->push_back(osg::Vec2(0.f, 0.f));
            texCoordArray->push_back(osg::Vec2(1.0, 0.f));
            texCoordArray->push_back(osg::Vec2(1.0, 1.0));
            texCoordArray->push_back(osg::Vec2(0.0, 1.0));
            texGeom->setTexCoordArray(0, texCoordArray);

            texGeom->getOrCreateStateSet()->setTextureAttributeAndModes(0, buttonTexture);
            texGeom->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
            texGeom->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
            texGeom->addPrimitiveSet(new osg::DrawArrays(GL_QUADS, 0, 4));
        }

        //! autoTransform to restrict button rotation on screen
        osg::AutoTransform* buttonAutoTransform = new osg::AutoTransform;
        buttonAutoTransform->setAutoRotateMode(osg::AutoTransform::ROTATE_TO_AXIS);
        buttonAutoTransform->setAxis(osg::Vec3(1, 0, 0));

        osg::MatrixTransform* matTransform = new osg::MatrixTransform;
        matTransform->setMatrix(osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0), 
            osg::DegreesToRadians(offsetAngle), osg::Vec3(0, 0, 1), 0, osg::Vec3(0, 1, 0)));
        matTransform->addChild(button);

        buttonAutoTransform->addChild(matTransform);

        buttonNode->addChild(buttonAutoTransform);

        double angle = osg::DegreesToRadians(offsetAngle);
        buttonNode->setMatrix(osg::Matrix::translate(-sin(angle), -cos(angle), 0));


        //! wrapper used for scaling 
        osg::MatrixTransform* wrapperNode = new osg::MatrixTransform;
        wrapperNode->addChild(buttonNode);

        wrapperNode->setName(objectName);

        return wrapperNode;
    }

    void DraggerContainer::setAssociatedModel(CORE::RefPtr<vizElements::IModelHolder> object)
    {
        if(!object.valid())
            return;

        if(_draggerMode == IDraggerUIHandler::DRAGGER_MODE_NONE)
        {
            setDraggerMode(IDraggerUIHandler::DRAGGER_MODE_TRANSLATE);
        }

        _associatedModel = object;

        CORE::RefPtr<CORE::IPoint> point = object->getInterface<CORE::IPoint>();
        if(!point.valid())
        {
            return;
        }

        // get geodetic position
        osg::Vec3d geoPos = point->getValue();

        CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = object->getInterface<ELEMENTS::IModelHolder>();
        if(!modelHolder.valid())
        {
            return;
        }

        // get model size in world
        double size = modelHolder->getInterface<CORE::IObject>()->getOSGNode()->getBound().radius()*1.4;

        // get model scale
        osg::Vec3d geoScale = modelHolder->getModelScale();

        if (geoScale == osg::Vec3d(0.0f, 0.0f, 0.0f)){
            geoScale = osg::Vec3d(1.0f, 1.0f, 1.0f);
        }
        // set initial scale base
        _scale = size/geoScale.x();

        // get ECEF position
        osg::Vec3d posECEF = UTIL::CoordinateConversionUtils::
            GeodeticToECEF(osg::Vec3d(geoPos.y(), geoPos.x(), geoPos.z()));

        // north direction
        osg::Quat northing(osg::DegreesToRadians(geoPos.x() + 90), osg::Vec3d(0, 0, 1));

        // up Vector
        osg::Quat up;
        up.makeRotate(osg::Vec3d(0, 0, 1), posECEF);

        osg::Matrix globalRot(northing*up);

        _translateAxisDragger->setMatrix(globalRot* osg::Matrix::scale(geoScale* _scale)*
            osg::Matrix::translate(posECEF));

        _translatePlaneDragger->setMatrix(globalRot* osg::Matrix::scale(geoScale* _scale)*
            osg::Matrix::translate(posECEF));

        _scaleDragger->setMatrix(globalRot* osg::Matrix::scale(geoScale* _scale)*
            osg::Matrix::translate(posECEF));

        _buttonsTransform->setMatrix(globalRot* osg::Matrix::scale(geoScale* _scale)*
            osg::Matrix::translate(posECEF));

        // orientation
        osg::Quat geoRotate = modelHolder->getModelOrientation();        

        osg::Matrix rotationMatrix(geoRotate);
        _rotateDragger->setMatrix(rotationMatrix* osg::Matrix::scale(geoScale*_scale) *
            osg::Matrix::translate(posECEF));

        _selection->setMatrix(osg::Matrix::scale(geoScale*_scale));
    }

    bool DraggerContainer::handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
    {
        if(ea.getHandled())
        {
            return false;
        }

        osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
        if (!view) return false;

        bool handled = false;

        switch(ea.getEventType())
        {
        case(osgGA::GUIEventAdapter::PUSH):
            {
                osgUtil::LineSegmentIntersector::Intersections intersections;
                if(view->computeIntersections(ea.getX(), ea.getY(), intersections, _intersectionMask) )
                {
                    for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
                        hitr != intersections.end();
                        ++hitr)
                    {
                        if(!hitr->nodePath.empty() && !(hitr->nodePath.back()->getName().empty()))
                        {
                            if(hitr->nodePath.back()->getName() == "TranslateButton")
                            {
                                setDraggerMode(IDraggerUIHandler::DRAGGER_MODE_TRANSLATE);
                                handled = true;
                                break;
                            }
                            else if(hitr->nodePath.back()->getName() == "RotateButton")
                            {
                                setDraggerMode(IDraggerUIHandler::DRAGGER_MODE_ROTATE);
                                handled = true;
                                break;
                            }
                            else if(hitr->nodePath.back()->getName() == "ScaleButton")
                            {
                                setDraggerMode(IDraggerUIHandler::DRAGGER_MODE_SCALE);
                                handled = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if(handled)
        {
            return true;
        }
        else
        {
            return Dragger::handle(ea, aa);
        }
    }

    bool DraggerContainer::DraggerUpdateCallback::receive(const osgManipulator::MotionCommand &command)
    {
        std::string commandName;
        if(dynamic_cast<const osgManipulator::TranslateInLineCommand*>(&command))
        {
            commandName = "TranslateInLineCommand";

            osg::NodePath nodePathToRoot;
            osgManipulator::computeNodePathToRoot(*_draggerContainer->getTranslateAxisDragger(), nodePathToRoot);
            osg::Matrix localToWorld = osg::computeLocalToWorld(nodePathToRoot);

            osg::Vec3d translation, scale;
            osg::Quat rotation, so;
            localToWorld.decompose(translation, rotation, scale, so);

            osg::Matrix mat = _draggerContainer->getSelectionMatrix()->getMatrix();
            osg::Vec3d selectionScale = mat.getScale();

            // convert to (lat, long, height)
            osg::Vec3d position = UTIL::CoordinateConversionUtils::ECEFToGeodetic(translation);

            // convert to (long, lat, height)
            position = osg::Vec3d(position.y(), position.x(), position.z());

            // get scale value from selection as the dragger might have been scaled due to fixed size in screen
            osg::Vec3d geoScale = selectionScale/_draggerContainer->getScale();

            double elevation = _draggerContainer->getDraggerUIHandler()->getElevationAt
                (osg::Vec2d(position.x(), position.y()));

            position.z() = (position.z() < elevation) ? elevation :position.z();

            osg::Quat northing(osg::DegreesToRadians(position.x() + 90), osg::Vec3d(0, 0, 1));
            osg::Quat up;
            up.makeRotate(osg::Vec3d(0, 0, 1), translation);
            osg::Matrix nt(northing*up);
            translation = UTIL::CoordinateConversionUtils::
                GeodeticToECEF(osg::Vec3d(position.y(), position.x(), position.z()));

            _draggerContainer->getTranslateAxisDragger()->setMatrix
                (nt* osg::Matrix::scale(scale)* osg::Matrix::translate(translation));

            _draggerContainer->getTranslatePlaneDragger()->setMatrix
                (nt* osg::Matrix::scale(scale)* osg::Matrix::translate(translation));

            _draggerContainer->getButtonsTransform()->setMatrix
                (nt* osg::Matrix::scale(scale)* osg::Matrix::translate(translation));

            osg::Quat orientation = _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->getModelOrientation();

            _draggerContainer->getAssociatedModel()->getInterface<CORE::IPoint>()->setValue(position);
            _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->setModelScale(geoScale);

            // this is done as on OverlayObject::reorient()is called on setModelScale() 
            //recalculates modelOrientation based on its position and Euler rotation values which 
            //are not being changed so the model will reset to upright position
            _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()
                ->setModelOrientation(orientation);

            osg::Matrix rot;
            orientation.get(rot);
            _draggerContainer->getRotateDragger()->setMatrix(rot*osg::Matrix::scale(scale)*
                osg::Matrix::translate(translation));

            _draggerContainer->getDraggerUIHandler()->sendDraggerUpdatedMessage();

        }
        else if(dynamic_cast<const osgManipulator::TranslateInPlaneCommand*>(&command))
        {
            commandName = "TranslateInPlaneCommand";
            osg::NodePath nodePathToRoot;
            osgManipulator::computeNodePathToRoot(*_draggerContainer->getTranslatePlaneDragger(), nodePathToRoot);
            osg::Matrix localToWorld = osg::computeLocalToWorld(nodePathToRoot);

            osg::Vec3d translation, scale;
            osg::Quat rotation, so;
            localToWorld.decompose(translation, rotation, scale, so);

            osg::Matrix mat = _draggerContainer->getSelectionMatrix()->getMatrix();
            osg::Vec3d selectionScale = mat.getScale();

            // convert to (lat, long, height)
            osg::Vec3d position = UTIL::CoordinateConversionUtils::ECEFToGeodetic(translation);

            // convert to (long, lat, height)
            position = osg::Vec3d(position.y(), position.x(), position.z());

            // get scale value from selection as the dragger might have been scaled due to fixed size in screen
            osg::Vec3d geoScale = selectionScale/_draggerContainer->getScale();

            //Prevent dragger from going below the terrain
            position.z() = _draggerContainer->getDraggerUIHandler()->getElevationAt
                (osg::Vec2d(position.x(), position.y()));


            osg::Quat northing(osg::DegreesToRadians(position.x() + 90), osg::Vec3d(0, 0, 1));
            osg::Quat up;
            up.makeRotate(osg::Vec3d(0, 0, 1), translation);
            osg::Matrix nt(northing*up);
            translation = UTIL::CoordinateConversionUtils::
                GeodeticToECEF(osg::Vec3d(position.y(), position.x(), position.z()));

            _draggerContainer->getTranslateAxisDragger()->setMatrix
                (nt* osg::Matrix::scale(scale)* osg::Matrix::translate(translation));

            _draggerContainer->getTranslatePlaneDragger()->setMatrix
                (nt* osg::Matrix::scale(scale)* osg::Matrix::translate(translation));

            _draggerContainer->getButtonsTransform()->setMatrix
                (nt* osg::Matrix::scale(scale)* osg::Matrix::translate(translation));

            osg::Quat orientation = _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->getModelOrientation();

            _draggerContainer->getAssociatedModel()->getInterface<CORE::IPoint>()->setValue(position);
            _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->setModelScale(geoScale);

            // this is done as on OverlayObject::reorient()is called on setModelScale() 
            //recalculates modelOrientation based on its position and Euler rotation values which 
            //are not being changed so the model will reset to upright position
            _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->setModelOrientation(orientation);

            osg::Matrix rot;
            orientation.get(rot);
            _draggerContainer->getRotateDragger()->setMatrix(rot*osg::Matrix::scale(scale)*
                osg::Matrix::translate(translation));

            _draggerContainer->getDraggerUIHandler()->sendDraggerUpdatedMessage();
        }
        else if(dynamic_cast<const osgManipulator::ScaleUniformCommand*>(&command))
        {
            commandName = "ScaleUniformCommand";

            osg::NodePath nodePathToRoot;
            osgManipulator::computeNodePathToRoot(*_draggerContainer->getScaleDragger(), nodePathToRoot);
            osg::Matrix localToWorld = osg::computeLocalToWorld(nodePathToRoot);

            osg::Vec3d translation, scale;
            osg::Quat rotation, so;
            localToWorld.decompose(translation, rotation, scale, so);

            osg::Matrix mat = _draggerContainer->getSelectionMatrix()->getMatrix();
            osg::Vec3d selectionScale = mat.getScale();

            // convert to (lat, long, height)
            osg::Vec3d position = UTIL::CoordinateConversionUtils::ECEFToGeodetic(translation);

            // convert to (long, lat, height)
            position = osg::Vec3d(position.y(), position.x(), position.z());

            // get scale value from selection as the dragger might have been scaled due to fixed size in screen
            osg::Vec3d geoScale = selectionScale/_draggerContainer->getScale();

            //Prevent dragger from going below the terrain
            double elevation = _draggerContainer->getDraggerUIHandler()->getElevationAt
                (osg::Vec2d(position.x(), position.y()));

            position.z() = (position.z() < elevation) ? elevation :position.z();

            osg::Quat northing(osg::DegreesToRadians(position.x() + 90), osg::Vec3d(0, 0, 1));
            osg::Quat up;
            up.makeRotate(osg::Vec3d(0, 0, 1), translation);
            osg::Matrix nt(northing*up);

            translation = UTIL::CoordinateConversionUtils::
                GeodeticToECEF(osg::Vec3d(position.y(), position.x(), position.z()));

            _draggerContainer->getScaleDragger()->setMatrix(nt* osg::Matrix::scale(scale)*
                osg::Matrix::translate(translation));

            _draggerContainer->getButtonsTransform()->setMatrix
                (nt* osg::Matrix::scale(scale)* osg::Matrix::translate(translation));

            osg::Quat orientation = _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->getModelOrientation();

            // restrciting dragger scale to 50
            if(geoScale.x() > 50.0)
            {
                geoScale = osg::Vec3d(50.0, 50.0, 50.0);
            }


            _draggerContainer->getAssociatedModel()->getInterface<CORE::IPoint>()->setValue(position);

            _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->setModelScale(geoScale);

            // this is done as on OverlayObject::reorient()is called on setModelScale() recalculates modelOrientation based
            // on its position and Euler rotation values which are not being changed
            // so the model will reset to upright position
            _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->setModelOrientation(orientation);

            osg::Matrix rot;
            orientation.get(rot);
            _draggerContainer->getRotateDragger()->setMatrix(rot*osg::Matrix::scale(scale)* 
                osg::Matrix::translate(translation));

            _draggerContainer->getDraggerUIHandler()->sendDraggerUpdatedMessage();

        }
        else if(dynamic_cast<const osgManipulator::Rotate3DCommand*>(&command))
        {
            commandName = "Rotate3DCommand";

            osg::NodePath nodePathToRoot;
            osgManipulator::computeNodePathToRoot(*_draggerContainer->getRotateDragger(), nodePathToRoot);

            osg::Matrix localToWorld = osg::computeLocalToWorld(nodePathToRoot);

            osg::Vec3d translation, scale;
            osg::Quat rotation, so;

            localToWorld.decompose(translation, rotation, scale, so);

            /////////
            // convert to (lat, long, height)
            osg::Vec3d position = UTIL::CoordinateConversionUtils::ECEFToGeodetic(translation);
            // convert to (long, lat, height)
            position = osg::Vec3d(position.y(), position.x(), position.z());
            osg::Quat northing(osg::DegreesToRadians(position.x() + 90), osg::Vec3d(0, 0, 1));
            osg::Quat up;
            up.makeRotate(osg::Vec3d(0, 0, 1), translation);
            osg::Matrix nt(northing*up);

            _draggerContainer->getTranslateAxisDragger()->setMatrix(nt* osg::Matrix::scale(scale)*
                osg::Matrix::translate(translation));

            _draggerContainer->getTranslatePlaneDragger()->setMatrix(nt* osg::Matrix::scale(scale)*
                osg::Matrix::translate(translation));
            ///////////////

            _draggerContainer->getAssociatedModel()->getInterface<ELEMENTS::IModelHolder>()->setModelOrientation(rotation);

            _draggerContainer->getDraggerUIHandler()->sendDraggerUpdatedMessage();
        }
        else if(dynamic_cast<const osgManipulator::Scale1DCommand*>(&command))
        {
            commandName = "Scale1DCommand";
        }
        else if(dynamic_cast<const osgManipulator::Scale2DCommand*>(&command))
        {
            commandName = "Scale2DCommand";
        }

        switch(command.getStage())
        {
        case osgManipulator::MotionCommand::START:
            if((commandName == "Scale1DCommand") ||(commandName == "Scale2DCommand")||
                (commandName == "ScaleUniformCommand"))
            {
                _draggerContainer->setFixedInScreen(false);
            }
            _draggerContainer->getDraggerUIHandler()->setDragStage(IDraggerUIHandler::START);
            break;
        case osgManipulator::MotionCommand::MOVE:
            _draggerContainer->getDraggerUIHandler()->setDragStage(IDraggerUIHandler::MOVE);
            break;
        case osgManipulator::MotionCommand::FINISH:
            if((commandName == "Scale1DCommand") ||(commandName == "Scale2DCommand")||
                (commandName == "ScaleUniformCommand"))
            {
                _draggerContainer->setFixedInScreen(_draggerContainer->getDraggerUIHandler()->getFixedInScreen());
            }
            _draggerContainer->getDraggerUIHandler()->setDragStage(IDraggerUIHandler::FINISH);
            break;
        }
        return true;
    }
    void DraggerContainer::setupDefaultGeometry()
    {
        _translateAxisDragger->setupDefaultGeometry();
        _translatePlaneDragger->setupDefaultGeometry();
        _rotateDragger->setupDefaultGeometry();
        _scaleDragger->setupDefaultGeometry();
    }

    void DraggerContainer::traverse(osg::NodeVisitor& nv)
    {
        if(_fixedInScreen && (nv.getVisitorType() == osg::NodeVisitor::CULL_VISITOR))
        {
            traverse(nv, _translateAxisDragger.get());
            traverse(nv, _translatePlaneDragger.get());
            traverse(nv, _rotateDragger.get());
            traverse(nv, _scaleDragger.get());
            traverse(nv, _buttonsTransform.get());
        }

        osgManipulator::CompositeDragger::traverse(nv);
    }

    void DraggerContainer::traverse(osg::NodeVisitor& nv, osg::MatrixTransform* matrixTransform)
    {
        if(!matrixTransform)
            return;

        osgUtil::CullVisitor* cv = static_cast<osgUtil::CullVisitor*>(&nv);

        float pixelSize = cv->pixelSize(matrixTransform->getBound().center(), 0.48f);
        if(pixelSize != _draggerSize)
        {
            float pixelScale = pixelSize > 0.0f ? _draggerSize/pixelSize : 1.0f;
            osg::Vec3d scaleFactor(pixelScale, pixelScale, pixelScale);

            osg::Vec3 trans, scale;
            osg::Quat rot, so;
            matrixTransform->getMatrix().decompose(trans, rot, scale, so);
            matrixTransform->setMatrix(osg::Matrix::rotate(rot) * osg::Matrix::scale(scaleFactor) 
                * osg::Matrix::translate(trans));
        }
    }


}