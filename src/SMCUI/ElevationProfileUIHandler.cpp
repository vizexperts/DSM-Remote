/*****************************************************************************
*
* File             : ElevationProfileUIHandler.cpp
* Description      : ElevationProfileUIHandler class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <Core/IDeletable.h>
#include <Core/ICompositeObject.h>

#include <SMCUI/ElevationProfileUIHandler.h>
#include <SMCUI/IMilitaryPathAnimationUIHandler.h>
#include <App/IApplication.h>
#include <Core/CoreRegistry.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <SMCUI/ILineUIHandler.h>
#include <App/AccessElementUtils.h>
#include <Core/WorldMaintainer.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/ITerrain.h>
#include <fstream>
#include <limits.h>
#include <Util/CoordinateConversionUtils.h>
#include <Elements/IDrawTechnique.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIconLoader.h>
#include <Elements/IRepresentation.h>

#include <Terrain/OEFeatureObjectAdapter.h>
#include <osgEarthSymbology/Geometry>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI,ElevationProfileUIHandler);
    DEFINE_IREFERENCED(ElevationProfileUIHandler, UIHandler);

    ElevationProfileUIHandler::ElevationProfileUIHandler()
        : _clampingVisitor(NULL)
        , _filename("")
        , _minPoint(osg::Vec3d(0.0, 0.0, 0.0))
        , _maxPoint(osg::Vec3d(0.0, 0.0, 0.0))
        , _avgHeight(0.0)
        , _distanceAltitudePoints (NULL)
    {
        // TBD initiliaze the member variables in initialization list
        _addInterface(SMCUI::IElevationProfileUIHandler::getInterfaceName());
        setName(getClassname());
    }

    ElevationProfileUIHandler::~ElevationProfileUIHandler()
    {}

    void ElevationProfileUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void ElevationProfileUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }


    void ElevationProfileUIHandler::setPoints(osg::Vec3dArray* points)
    {
        if(_clampingVisitor.valid())
        {
            _clampingVisitor->setPoints(points);
        }
    }

    const osg::Vec3dArray* ElevationProfileUIHandler::getPoints() const
    {
        return _pointList.get();
    }

    const osg::Vec2dArray* ElevationProfileUIHandler::getDistanceAltitudePoints() const
    {
        return _distanceAltitudePoints.get();
    }

    const std::string&  ElevationProfileUIHandler::getGraphFilename()
    {
        return _filename;
    }

    void ElevationProfileUIHandler::setFocus(bool value)
    {
        UIHandler::setFocus(value);

        // Subscribe to utility UI Handler
        try
        {
            if(value)
            {
                _reset();

                //CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                //_clampingVisitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::
                //    PointListClampingVisitorType)->getInterface<GISCOMPUTE::IPointListClampingVisitor>();


                // Subscribe to clamping visitor
                if(_clampingVisitor.valid())
                {
                    _subscribe(_clampingVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                }
            }
            else
            {
                if(_clampingVisitor.valid())
                {
                    _unsubscribe(_clampingVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                }

                _reset();
            }
        }
        catch(...)
        {
            // XXX - Throw a fatal error on logcomponent
        }
    }

    void ElevationProfileUIHandler::stop()
    {
        if(_clampingVisitor.valid())
        {
            _clampingVisitor->stop();
        }
        if (selectedObjectVector.size()){

            for (int s = 0; s < selectedObjectVector.size(); s++){
                selectedObjectVector[s]->setVisibility(true); 
            }
            selectedObjectVector.clear(); 
        }
    }

    void ElevationProfileUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {

            CORE::RefPtr<CORE::IComponent> component = 
                CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

            if(component.valid())
            {
                _selectionComponent = component->getInterface<CORE::ISelectionComponent>();
            }

            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _clampingVisitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::
                    ColorTerrainProfileVisitorType)->getInterface<GISCOMPUTE::IColorTerrainProfileVisitor>();
            }            
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::FilterStatus status = message.getInterface<GISCOMPUTE::IFilterStatusMessage>()->getStatus();
            if(status == GISCOMPUTE::FILTER_IDLE || status == GISCOMPUTE::FILTER_SUCCESS)
            {   
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
                CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
                osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();

                tp->invoke(boost::bind(&ElevationProfileUIHandler::_generateGraph, this));

                //_generateGraph();
            }
        }
        else if(messageType == *CORE::ISelectionComponent::ObjectClickedMessageType)
        {
            const CORE::ISelectionComponent::SelectionMap& map = 
                _selectionComponent->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();
            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                CORE::ILine* line = iter->second->getInterface<CORE::ILine>();
                if(line)
                {
                    _line = line;
                    CORE::IVisibility* visiblityInterface = iter->second->getInterface<CORE::IVisibility>();
                    selectedObjectVector.push_back(visiblityInterface); 
                }

                CORE::RefPtr< TERRAIN::OEFeatureObjectAdapter> featureAdapter = dynamic_cast < TERRAIN::OEFeatureObjectAdapter*>(iter->second.get());
                if (featureAdapter.valid()){

                    CORE::RefPtr<TERRAIN::IModelObject> modelObject = featureAdapter->getModelObject();

                    CORE::RefPtr< const osgEarth::Features::Feature> feature = featureAdapter->getOEFeature();
                    CORE::RefPtr<CORE::IObject> lineObject;
                    if (feature.valid())
                    {
                        CORE::RefPtr<SMCUI::IMilitaryPathAnimationUIHandler> militaryPathAnimationUIHandler =
                            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IMilitaryPathAnimationUIHandler>(getManager());

                        lineObject = militaryPathAnimationUIHandler->createLineObjectfromFeature(feature.get());
                        if (lineObject){
                            _line = lineObject->getInterface<CORE::ILine>();
                            CORE::IVisibility* visiblityInterface = iter->second->getInterface<CORE::IVisibility>();
                            selectedObjectVector.push_back(visiblityInterface);
                        }
                    }
                }
            }
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }

    void ElevationProfileUIHandler::execute()
    {        
        try
        {
            if (selectedObjectVector.size()){

                for (int s = 0; s < selectedObjectVector.size(); s++){
                    selectedObjectVector[s]->setVisibility(false);
                }
            }

            // Query TerrainUIHandler interface
            if(!_line.valid() || _line->getPointNumber() < 2)
            {
                throw UTIL::Exception("Line is required for generating elevation profile", __FILE__, __LINE__);
                return;
            }

            CORE::RefPtr<SMCUI::ILineUIHandler> lineUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getManager());

            if(lineUIHandler.valid())
            {
                lineUIHandler->_setProcessMouseEvents(false);
            }

            // Clamp using point clamping filter
            _pointList = new osg::Vec3dArray;

            /*
            double step = 0.001;

            for(unsigned int i = 0; i < _line->getPointNumber() - 1; i++)
            {
                // Get first point
                osg::Vec3d p1 = _line->getPoint(i)->getValue();

                osg::Vec3d p2 = _line->getPoint(i + 1)->getValue();

                osg::Vec2d p1v2 = osg::Vec2d(p1.x(), p1.y());
                osg::Vec2d p2v2 = osg::Vec2d(p2.x(), p2.y());

                osg::Vec2d origp1v2 = p1v2;

                _pointList->push_back(p1);
                           
                //XXX - possible division by zero error
//                double m = ((p2v2.y() - p1v2.y()) / (p2v2.x() - p1v2.x()));
                double theta = atan2((p2v2.y() - p1v2.y()),(p2v2.x() - p1v2.x()));

                while((p2v2 - origp1v2).length() > ((p1v2 - origp1v2).length() + step))
                {
                    // Equation of line between first and second
                    osg::Vec3d ptmp;

                    ptmp.x() = p1v2.x() + step*cos(theta);
                    ptmp.y() = p1v2.y() + step*sin(theta);

                    p1v2 = osg::Vec2(ptmp.x(), ptmp.y());

                    _pointList->push_back(ptmp);
                }                
            }

            _pointList->push_back(osg::Vec3d(_line->getPoint(_line->getPointNumber()-1)->getValue()));
            */

            for (int itr = 0; itr < _line->getPointNumber(); itr++){
                _pointList->push_back(_line->getPoint(itr)->getValue());
            }

            _clampingVisitor->setPoints(_pointList.get());
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
            CORE::RefPtr<CORE::IBase> terrainBase = terrain->getInterface<CORE::IBase>();
            terrainBase->accept(*(_clampingVisitor->getInterface<CORE::IBaseVisitor>()));
        }
        catch(const UTIL::Exception& e)
        {
            throw e;
        }        
    }

// Test function
#if 0
    void ElevationProfileUIHandler::_createPointsAt(osg::ref_ptr<osg::Vec3dArray> pointList)
    {
        osg::Vec3dArray::const_iterator citer = pointList->begin();
        for(; citer != pointList->end(); citer++)
        {
            osg::Vec3d position = *citer;
            createPointAt(position);
        }
    }

    void ElevationProfileUIHandler::createPointAt(osg::Vec3d position)
    {
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
            CORE::CoreRegistry::instance()->getObjectFactory();
        CORE::RefPtr<CORE::IObject> cpObject = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");
        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
                component->getInterface<ELEMENTS::IIconLoader>();

            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = 
                cpObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolder.valid() && iconLoader.valid())
            {
                iconHolder->setIcon(iconLoader->getIconByFilename("../../data/Symbols/hcircle.png"));
            }
        }

        if(cpObject.valid())
        {
            CORE::RefPtr<ELEMENTS::IRepresentation> representation = 
                cpObject->getInterface<ELEMENTS::IRepresentation>();
            if(representation.valid())
            {
                representation->setRepresentationMode(ELEMENTS::IRepresentation::ICON);
            }
        }

        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IUIHandlerManager>(getManager());
        world->addObject(cpObject.get());

        cpObject->getInterface<CORE::IBase>()->setName("");
        cpObject->getInterface<CORE::IPoint>()->setValue(position);
    }


#endif

    void ElevationProfileUIHandler::_generateGraph()
    {
        _pointList = _clampingVisitor->getClampedPoints(); 
        osg::Vec3dArray::const_iterator citer = _pointList->begin();
        if(citer != _pointList->end())
        {
            osg::Vec3d last = *(citer++);
            osg::Vec3d elast = UTIL::CoordinateConversionUtils::GeodeticToECEF(last);
            double distance = 0.0;
            _avgHeight = 0.0;
            _minPoint = _maxPoint = osg::Vec3d(0.0, 0.0, 0.0);
            double minElevation = (double)UINT_MAX;
            double maxElevation = -(double)UINT_MAX;

            _distanceAltitudePoints = new osg::Vec2dArray;

            _distanceAltitudePoints->push_back(osg::Vec2d(distance, last.z()));

            for(; citer != _pointList->end(); ++citer)
            {
                // Get the point
                osg::Vec3d current = *citer;
                double elevation = current.z();
                osg::Vec3d ecurrent = UTIL::CoordinateConversionUtils::GeodeticToECEF(current);
                distance += (ecurrent - elast).length();
                last = current;
                elast = UTIL::CoordinateConversionUtils::GeodeticToECEF(last);
                osg::Vec2d distElevElement = osg::Vec2d(distance, elevation);
                _distanceAltitudePoints->push_back(distElevElement);

                // Check for minimum elevation
                if(minElevation > elevation)
                {
                    _minPoint = current;
                    minElevation = elevation;
                }

                // Check for minimum elevation
                if(maxElevation < elevation)
                {
                    _maxPoint = current;
                    maxElevation = elevation;
                }

                _avgHeight += elevation;
            }

            double dis = (UTIL::CoordinateConversionUtils::GeodeticToECEF((*_pointList)[_pointList->size() - 1]) - UTIL::CoordinateConversionUtils::GeodeticToECEF((*_pointList)[0])).length();

            _avgHeight /= _pointList->size();
        }


        // XXX - Notify all current observers about the intersections found message
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*SMCUI::IElevationProfileUIHandler::
            ProfileCalculatedMessageType);
        notifyObservers(*SMCUI::IElevationProfileUIHandler::ProfileCalculatedMessageType, *msg);
    }
    std::vector<std::string> ElevationProfileUIHandler::getColorVector(){
        return _clampingVisitor->getColorVector(); 
    }

    osg::Vec3d ElevationProfileUIHandler::getMinimumElevationPoint() const
    {
        return _minPoint;
    }

    osg::Vec3d ElevationProfileUIHandler::getMaximumElevationPoint() const
    {
        return _maxPoint;
    }

    double ElevationProfileUIHandler::getAverageElevation() const
    {
        return _avgHeight;
    }

    CORE::RefPtr<CORE::ILine> ElevationProfileUIHandler::getLine() const
    {
        return _line;
    }

    void ElevationProfileUIHandler::_reset()
    {
        _filename = "";
        _pointList = NULL;;
        _distanceAltitudePoints = NULL;
        //_graphGenerationScript = "";
        _minPoint.set(0.0, 0.0, 0.0);
        _maxPoint.set(0.0, 0.0, 0.0);
        _avgHeight = 0.0;

        if(_line.valid())
        {
            _removeObject(_line->getInterface<CORE::IObject>());
            _line = NULL;
        }
    }

    void ElevationProfileUIHandler::clearLine()
    {
        try
        {
            if(_line.valid())
            {
                _line->clear();
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ElevationProfileUIHandler::setMarking(bool value)
    {
        if(getFocus())
        {
            CORE::RefPtr<SMCUI::ILineUIHandler> lineUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getManager());

            if(value)
            {
                bool temporaryState = lineUIHandler->getTemporaryState();
                lineUIHandler->setTemporaryState(true);
                lineUIHandler->setColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
                lineUIHandler->setWidth(3.0);
                lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE);
                lineUIHandler->setTemporaryState(temporaryState);
                lineUIHandler->getInterface<IUIHandler>(true)->setFocus(true);

                _line = lineUIHandler->getCurrentLine();
                CORE::IVisibility* visiblityInterface = _line->getInterface<CORE::IVisibility>();
                if (visiblityInterface != NULL)
                selectedObjectVector.push_back(visiblityInterface);
                //CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = _line->getInterface<ELEMENTS::IDrawTechnique>();
                //if(draw.valid())
                //{
                //    draw->setHeightOffset(1.0);
                //    draw->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);
                //}
            }
            else
            {
                lineUIHandler->getInterface<APP::IUIHandler>(true)->setFocus(false);
                lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);

                if(_line.valid())
                {
                    _removeObject(_line->getInterface<CORE::IObject>());
                    _line = NULL;
                }
            }
        }
    }

    void ElevationProfileUIHandler::_removeObject(CORE::IObject *object)
    {
        if(!object)
            return;

        if(object->getInterface<CORE::IDeletable>())
        {
            object->getInterface<CORE::IDeletable>()->remove();
        }
        else
        {
            try
            {
                CORE::IObject *parent = object->getInterface<CORE::IObject>(true)->getParent();
                if(parent)
                {
                    parent->getInterface<CORE::ICompositeObject>(true)->
                        removeObjectByID(&(object->getInterface<CORE::IBase>(true)->getUniqueID()));
                }
            }
            catch(UTIL::Exception &e)
            {
                e.LogException();
            }
        }
    }

    void ElevationProfileUIHandler::selectLine(bool value)
    {
        if(getFocus())
        {
            if(!_selectionComponent.valid())
            {
                throw UTIL::Exception("Selection Component Not Found", __FILE__, __LINE__);
                return;
            }
            
            _selectionComponent->clearCurrentSelection();
            if(value)
            {
                _selectionComponent->setIntermediateSelection(true);
                _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
            }
            else
            {
                _selectionComponent->setIntermediateSelection(false);
                _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
                _line = NULL;
            }            
        }
    }

} // namespace SMCUi
