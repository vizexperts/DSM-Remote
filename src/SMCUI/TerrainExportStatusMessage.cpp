/*****************************************************************************
 *
 * File             : TerrainExportStatusMessage.h
 * Description      : TerrainExportStatusMessage class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCUI/TerrainExportStatusMessage.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, TerrainExportStatusMessage);
    DEFINE_IREFERENCED(TerrainExportStatusMessage, CORE::CoreMessage);

    TerrainExportStatusMessage::TerrainExportStatusMessage()
        : _status(IDLE),
        _progress(0),
        _layername("")
    {
        _addInterface(ITerrainExportStatusMessage::getInterfaceName());
    }

    void
        TerrainExportStatusMessage::setStatus(const LayerStatus status)
    {
        _status = status;
    }

    TerrainExportStatusMessage::LayerStatus
        TerrainExportStatusMessage::getStatus() const
    {
        return _status;
    }

    void
        TerrainExportStatusMessage::setProgress(unsigned int progress)
    {
        _progress = progress;
    }

    unsigned int
        TerrainExportStatusMessage::getProgress()
    {
        return _progress;
    }

    void TerrainExportStatusMessage::setLayerName(std::string layername)
    {
        _layername = layername;
    }
    std::string TerrainExportStatusMessage::getLayerName()
    {
        return _layername;
    }

    TerrainExportStatusMessage::~TerrainExportStatusMessage(){}
}
