


#include <SMCUI/AddContentUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>

#include <Elements/ElementsPlugin.h>
#include <Terrain/TerrainPlugin.h>
#include <Terrain/IRasterObject.h>
#include <Terrain/IMultiBandRasterObject.h>
#include <Terrain/IElevationObject.h>
#include <Terrain/IVectorObject.h>
#include <Terrain/Terrain.h>
#include <Elements/OverlayImage.h>
#include <Elements/ElementsUtils.h>
#include <Core/CommonUtilityFunctions.h>

#include <Util/CoordinateConversionUtils.h>
#include <Util/BaseExceptions.h>
#include <Util/HeightFieldUtils.h>
#include <Util/ThreadPool.h>
#include <Util/FileUtils.h>
#include<Util/StyleFileUtil.h>


#include <App/ApplicationRegistry.h>
#include <App/AppPlugin.h>
#include <App/TaskMaintainer.h>
#include <App/ITask.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>

#include <DB/ReadFile.h>

#include <App/AccessElementUtils.h>

#include <Util/GdalUtils.h>
#include <Terrain/VRTBuilder.h>

#include <osgEarth/Map>
#include <osgEarth/MapNode>
#include <osgEarthDrivers/tms/TMSOptions>
#include <osgEarthDrivers/gdal/GDALOptions>
#include <osgEarthDrivers/yahoo/YahooOptions>
#include <osgEarthDrivers/model_feature_geom/FeatureGeomModelOptions>
#include <Core/IObjectMessage.h>

#include<Terrain\IModelObject.h>
#include<serialization\StyleSheetParser.h>

#include <Core/IGISData.h>
#include <Core/IRasterData.h>
#include <Elements/VizPlaceNode.h>
#include <Elements/FeatureObject.h>

#if USE_OSGEARTH < 22
#include <osgEarth/Caching>
#endif 

#include <iostream>

#define DGN_RENDERBIN_NUMBER 1000

namespace SMCUI
{
    void setProgress(int progressValue)
    {
        _progressValue = progressValue;
    }

    int getProgress()
    {
        return _progressValue;
    }

    DEFINE_META_BASE(SMCUI, AddContentUIHandler);

    DEFINE_IREFERENCED(AddContentUIHandler, VizUI::UIHandler);

    AddContentUIHandler::AddContentUIHandler()
        : _url(""), _offset(10.0), _clamping(true), isGoogleStreetDataAdded(false), isGoogleHybridDataAdded(false)
    {
        _addInterface(IAddContentUIHandler::getInterfaceName());
        setName(getClassname());
    }

    AddContentUIHandler::~AddContentUIHandler(){}

    void AddContentUIHandler::onAddedToManager()
    {
        try
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm =
                APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IUIHandlerManager>(getManager());

            _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void AddContentUIHandler::onRemovedFromManager()
    {
        try
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm =
                APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IUIHandlerManager>(getManager());

            _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void AddContentUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);

            int progress = getProgress();
            setProgressValue(progress);

            if (progress < 100)
                _notifyStatusToObservers(IAddContentStatusMessage::PENDING, progress);

            if (_object.valid())
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
                _unsubscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);

                CORE::RefPtr<CORE::IWorld> world = ELEMENTS::GetFirstWorldFromMaintainer();

                world->addObject(_object.get());
                _object = NULL;
                _notifyStatusToObservers(IAddContentStatusMessage::SUCCESS, progress);
            }
        }

        VizUI::UIHandler::update(messageType, message);
    }

    void AddContentUIHandler::addMultibandRasterData(std::string layerName, std::string url1, std::string url2, std::string url3, osg::Vec4d &extents, bool override_extents, unsigned int minLevel,
        unsigned int maxLevel, bool hasNoData, double noDataValue)
    {
        if (url1.empty() || url2.empty() || url3.empty())
        {
            std::string errstring = "No filename specified";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        // Check whether the filenames are valid or not
        if (!osgDB::fileExists(url1))
        {
            std::string errstring = "Unable to load " + url1 + ". Check whether the file exists.";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }
        if (!osgDB::fileExists(url2))
        {
            std::string errstring = "Unable to load " + url2 + ". Check whether the file exists.";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }
        if (!osgDB::fileExists(url3))
        {
            std::string errstring = "Unable to load " + url3 + ". Check whether the file exists.";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            /*CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddRasterData, this, url,extents,override_extents, minLevel, maxLevel));*/

            GDALAllRegister();
            GDALDataset* _srcDS = NULL;

            _srcDS = (GDALDataset*)GDALOpen(url1.c_str(), GA_ReadOnly);

            TERRAIN::IRasterObject::EnhancementType enhanceType = TERRAIN::IRasterObject::MIN_MAX;

            if (_srcDS)
            {
                if (_srcDS->GetRasterBand(1)->GetRasterDataType() == GDT_Byte)
                {
                    enhanceType = TERRAIN::IRasterObject::NONE;
                }
            }

            _mtAddMultiBandRasterData(layerName, url1, url2, url3, enhanceType, extents, override_extents, minLevel, maxLevel, -1, hasNoData, noDataValue);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }

    }


    void
        AddContentUIHandler::addRasterData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel,
        unsigned int maxLevel, int subdataset, const std::string &enhanceType, const std::string &projectionType)
    {
        // Create a new raster data object
        if (url.empty())
        {
            std::string errstring = "No filename specified";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        // Check whether the filenames are valid or not
        if (!osgDB::fileExists(url))
        {
            std::string errstring = "Unable to load " + url + ". Check whether the file exists.";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        if (osgDB::fileType(url) == osgDB::DIRECTORY)
        {
            osgDB::DirectoryContents contents = osgDB::getDirectoryContents(url);

            // a empty folder has "." and ".." as elements
            if (contents.size() <= 2)
            {
                std::string errstring = url + "is an empty folder. No files to load";
                throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
            }
        }
        if (boost::filesystem::is_empty(url)){
            throw AddRasterException("File Is Empty", __FILE__, __LINE__);
        }
        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            /*CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddRasterData, this, url,extents,override_extents, minLevel, maxLevel));*/
            _mtAddRasterData(layerName, url, extents, override_extents, minLevel, maxLevel, subdataset, enhanceType,projectionType);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void AddContentUIHandler::setProgressValue(int value)
    {
        _progressValueForGUI = value;
    }

    int AddContentUIHandler::getProgressValue()
    {
        return _progressValueForGUI;
    }

    void
        AddContentUIHandler::addMultibandHDFData(const std::string &layerName,
        const std::string &url,
        osg::Vec4d &extents,
        bool override_extents,
        unsigned int minLevel,
        unsigned int maxLevel,
        std::vector<int> subdatasetVector, bool hasNoData, double noDataValue)
    {

        if (!osgDB::fileExists(url))
        {
            LOG_ERROR("File not found");
        }

        std::string urlVector[3];

        GDALAllRegister();
        GDALDataset* _srcDS = NULL;

        _srcDS = (GDALDataset*)GDALOpen(url.c_str(), GA_ReadOnly);

        if (_srcDS == NULL)
        {
            LOG_ERROR("File not found!!");
        }


        for (int i = 0; i < subdatasetVector.size(); i++)
        {

            if (subdatasetVector[i] > 0)
            {
                if (_srcDS)
                {
                    char **subDatasets = _srcDS->GetMetadata("SUBDATASETS");
                    int numSubDatasets = CSLCount(subDatasets);

                    if (numSubDatasets > 0)
                    {
                        if (subdatasetVector[i] > numSubDatasets)
                        {
                            subdatasetVector[i] = 1;
                        }

                        std::stringstream buf;
                        buf << "SUBDATASET_" << subdatasetVector[i] << "_NAME";
                        char *pszSubdatasetName = CPLStrdup(CSLFetchNameValue(subDatasets, buf.str().c_str()));


                        //set filename to sub dataset name
                        urlVector[i] = pszSubdatasetName;
                        CPLFree(pszSubdatasetName);
                    }
                }
            }
        }

        GDALClose(_srcDS);



        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            /*CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddRasterData, this, url,extents,override_extents, minLevel, maxLevel));*/

            GDALAllRegister();
            GDALDataset* _srcDS = NULL;

            _srcDS = (GDALDataset*)GDALOpen(urlVector[0].c_str(), GA_ReadOnly);

            TERRAIN::IRasterObject::EnhancementType enhanceType = TERRAIN::IRasterObject::MIN_MAX;

            if (_srcDS)
            {
                if (_srcDS->GetRasterBand(1)->GetRasterDataType() == GDT_Byte)
                {
                    enhanceType = TERRAIN::IRasterObject::NONE;
                }
            }

            _mtAddMultiBandRasterData(layerName, urlVector[0], urlVector[1], urlVector[2], enhanceType, extents, override_extents, minLevel, maxLevel, -1, hasNoData, noDataValue);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void
        AddContentUIHandler::_mtAddRasterData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel,
        unsigned int maxLevel, int subdataset, const std::string &enhanceType, const std::string &projectionType, bool hasNoData, double noDataValue)
    {
        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> rasterLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(rasterLoadTask.get());
        rasterLoadTask->isProgressable(false);
        rasterLoadTask->isCancelable(false);

        try
        {
            // Enhancement Type
            std::string strMinMax("MinMax");
            std::string strStdDev("StandardDeviation");
            std::string strHistEqu("HistogramEqualization");
            std::string strConstst("MinMaxConstrast");

            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);

            if (enhanceType.compare(strMinMax) == 0)
                irobject->setEnhancementType(TERRAIN::IRasterObject::MIN_MAX);
            else if (enhanceType.compare(strStdDev) == 0)
                irobject->setEnhancementType(TERRAIN::IRasterObject::STANDARD_DEVIATION);
            else if (enhanceType.compare(strHistEqu) == 0)
                irobject->setEnhancementType(TERRAIN::IRasterObject::HISTOGRAM_EQUALIZATION);
            else if (enhanceType.compare(strConstst) == 0)
                irobject->setEnhancementType(TERRAIN::IRasterObject::CONSTRAST_ADJUSTMENT);

            if (projectionType != "")
                irobject->setProjectionType(projectionType);
            irobject->setMinLevel(minLevel);
            irobject->setMaxLevel(maxLevel);
            irobject->setSubdataset(subdataset);
            if (hasNoData)
            {
                irobject->setHasNoDataValue(hasNoData);
                irobject->setNoDataValue(noDataValue);
            }
            /*irobject->setEnhancementType(enhanceType);*/

            // if a tms file is added - append extension ".xml" at the end.
            std::string extension = osgDB::getLowerCaseFileExtension(url);
            std::string fileName;

            if (extension == "xml")
            {
                std::string modifiedUrl = url + ".tms";
                irobject->setURL(modifiedUrl);

                fileName = osgDB::getSimpleFileName(osgDB::getFilePath(url)) + "_tms";
            }
            else
            {
                irobject->setURL(url);
                fileName = osgDB::getSimpleFileName(url);
            }

            irobject->getInterface<CORE::IBase>(true)->setName(layerName);
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            if (override_extents)
            {
                irobject->setExtents(extents, override_extents);
            }

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            rasterLoadTask->setObjectNames(objBase->getName());

            //start the task
            rasterLoadTask->start();

            // Get the world interface and add the raster object
            //_subscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);
            iworld->addObject(iobject.get());
            //_object = iobject;

            //finish task
            rasterLoadTask->finish();
            CORE::IRasterData* rasterData = nullptr;
            if (irobject && (rasterData = irobject->getRasterData()))
            {
                if (rasterData)
                {
                    CORE::IGISData* gisData = rasterData->getInterface<CORE::IGISData>();
                    int vecSize = 0;
                    if (gisData)
                    {
                        std::vector<std::string> skippedFiles = gisData->getSkippedFilesFolder();
                        vecSize = skippedFiles.size();
                        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAddContentStatusMessage::AddContentStatusMessageType);
                        msg->setSender(this);
                        IAddContentStatusMessage* addContentStatusMsg = msg->getInterface<IAddContentStatusMessage>();
                        if (addContentStatusMsg)
                        {
                            addContentStatusMsg->setStatus(IAddContentStatusMessage::SKIPPED_FILES);
                            addContentStatusMsg->setProgress(-1);
                            addContentStatusMsg->setSkippedFileList(skippedFiles);
                        }
                        notifyObservers(*IAddContentStatusMessage::AddContentStatusMessageType, *msg);
                    }
                }
            }

        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            rasterLoadTask->terminate();
            //if(iobject.valid())
            //{
            //    // Remove the object from the world
            //    CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
            //    iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            //}

            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void
        AddContentUIHandler::_mtAddMultiBandRasterData(const std::string &layerName, const std::string &url1, const std::string &url2, const std::string &url3, TERRAIN::IRasterObject::EnhancementType enhanceType,
        osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel, int subdataset, bool hasNoData, double noDataValue)
    {
        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> rasterLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(rasterLoadTask.get());
        rasterLoadTask->isProgressable(false);
        rasterLoadTask->isCancelable(false);

        try
        {
            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::MultiBandRasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);
            CORE::RefPtr<TERRAIN::IMultiBandRasterObject> imbrobject = iobject->getInterface<TERRAIN::IMultiBandRasterObject>(true);
            irobject->setMinLevel(minLevel);
            irobject->setMaxLevel(maxLevel);
            irobject->setSubdataset(subdataset);
            if (hasNoData)
            {
                irobject->setHasNoDataValue(hasNoData);
                irobject->setNoDataValue(noDataValue);
            }
            irobject->setEnhancementType(enhanceType);

            // if a tms file is added - append extension ".xml" at the end.
            //std::string extension1 = osgDB::getLowerCaseFileExtension(url1);
            std::string fileName;

            //if(extension1 == "xml")
            //{
            //    std::string modifiedUrl = url + ".tms";
            //    irobject->setURL(modifiedUrl);

            //    fileName = osgDB::getSimpleFileName(osgDB::getFilePath(url)) + "_tms";
            //}
            //else
            {
#if 0
                std::stringstream ss;
                ss<< "gdalenhance -equalize "<<url<<" "<<"temp.tif";
                std::string command = ss.str();
                system(command.c_str());

                irobject->setURL(/*url*/"temp.tif");
#else 
                imbrobject->setBand1URL(url1);
                imbrobject->setBand2URL(url2);
                imbrobject->setBand3URL(url3);
#endif 
                fileName = osgDB::getSimpleFileName(url1);
            }

            irobject->getInterface<CORE::IBase>(true)->setName(layerName);
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            if (override_extents)
            {
                irobject->setExtents(extents, override_extents);
            }

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            rasterLoadTask->setObjectNames(objBase->getName());

            //start the task
            rasterLoadTask->start();

            // Get the world interface and add the raster object
            //_subscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);
            iworld->addObject(iobject.get());
            //_object = iobject;

            //finish task
            rasterLoadTask->finish();
        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            rasterLoadTask->terminate();
            //if(iobject.valid())
            //{
            //    // Remove the object from the world
            //    CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
            //    iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            //}

            e.LogException();
            /*throw AddRasterException(e.What(), __FILE__, __LINE__);*/
        }
    }

    void
        AddContentUIHandler::addRasterData(const std::string &layerName, const std::string &url)
    {
        // Create a new raster data object
        if (url.empty())
        {
            std::string errstring = "No filename specified";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        // Check whether the filenames are valid or not
        if (!osgDB::fileExists(url))
        {
            std::string errstring = "Unable to load " + url + ". Check whether the file exists.";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            /*CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager.get());
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddRasterData, this));*/
            _mtAddRasterData(layerName, url);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void
        AddContentUIHandler::_mtAddRasterData(const std::string &layerName, const std::string &url)
    {
        CORE::RefPtr<CORE::IObject> iobject = NULL;
        try
        {
            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);

            // if a tms file is added - append extension ".xml" at the end.
            std::string extension = osgDB::getLowerCaseFileExtension(url);
            if (extension == "xml")
            {
                std::string modifiedUrl = url + ".tms";
                irobject->setURL(modifiedUrl);
            }
            else
            {
                irobject->setURL(url);
            }

            irobject->getInterface<CORE::IBase>(true)->setName(layerName);

            // Get the world interface and add the raster object
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
            iworld->addObject(iobject.get());
            /*CORE::RefPtr<CORE::ITerrain> terrain = iworld->getTerrain();
            if(terrain.valid())
            {
            terrain->addRasterObject(irobject.get());
            }*/
        }
        catch (const UTIL::Exception& e)
        {
            if (iobject.valid())
            {
                // Remove the object from the world
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
                iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            }

            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void
        AddContentUIHandler::addElevationData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel)
    {
        // Create a new raster data object
        if (url.empty())
        {
            std::string errstring = url + " is an invalid file or does not exist";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        if (!osgDB::fileExists(url))
        {
            std::string errstring = url + " file does not exist";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        if (osgDB::fileType(url) == osgDB::DIRECTORY)
        {
            osgDB::DirectoryContents contents = osgDB::getDirectoryContents(url);

            // a empty folder has "." and ".." as elements
            if (contents.size() <= 2)
            {
                std::string errstring = url + "is an empty folder. No files to load";
                throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
            }
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            /*CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager.get());
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddElevationData, this));*/
            _mtAddElevationData(layerName, url, extents, override_extents, minLevel, maxLevel);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void
        AddContentUIHandler::_mtAddElevationData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel
        , unsigned int maxLevel)
    {

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> elevationLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(elevationLoadTask.get());
        elevationLoadTask->isProgressable(false);
        elevationLoadTask->isCancelable(false);

        try
        {
            // Load the rasterdata and add to world and terrain
            CORE::RefPtr<CORE::IObject> iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::ElevationObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IElevationObject> ieobject = iobject->getInterface<TERRAIN::IElevationObject>(true);
            ieobject->setMinLevel(minLevel);
            ieobject->setMaxLevel(maxLevel);


            // if a tms file is added - append extension ".xml" at the end.
            std::string extension = osgDB::getLowerCaseFileExtension(url);
            std::string fileName;

            if (extension == "xml")
            {
                std::string modifiedUrl = url + ".tms";
                ieobject->setURL(modifiedUrl);
                fileName = osgDB::getSimpleFileName(osgDB::getFilePath(url)) + "_tms";
            }
            else
            {
                ieobject->setURL(url);
                fileName = osgDB::getSimpleFileName(url);
            }

            ieobject->getInterface<CORE::IBase>(true)->setName(layerName);

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            elevationLoadTask->setObjectNames(objBase->getName());

            // Get the world interface and add the raster object
            // XXX - Acquire lock for world
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            if (override_extents)
            {
                ieobject->setExtents(extents, override_extents);
            }

            //start the task
            elevationLoadTask->start();

            // Get the world interface and add the raster object
            iworld->addObject(iobject.get());

            //Finish task
            elevationLoadTask->finish();

        }
        catch (const UTIL::Exception& e)
        {
            //Terminate the task on exception
            elevationLoadTask->terminate();
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }


    void
        AddContentUIHandler::addElevationData(const std::string &layerName, const std::string &url)
    {
        // Create a new raster data object
        if (url.empty())
        {
            std::string errstring = url + " is an invalid file or does not exist";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            /*CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager.get());
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddElevationData, this));*/
            _mtAddElevationData(layerName, url);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void
        AddContentUIHandler::_mtAddElevationData(const std::string &layerName, const std::string &url)
    {
        try
        {
            // Load the rasterdata and add to world and terrain
            CORE::RefPtr<CORE::IObject> iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::ElevationObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IElevationObject> ieobject = iobject->getInterface<TERRAIN::IElevationObject>(true);

            // if a tms file is added - append extension ".xml" at the end.
            std::string extension = osgDB::getLowerCaseFileExtension(url);
            if (extension == "xml")
            {
                std::string modifiedUrl = url + ".tms";
                ieobject->setURL(modifiedUrl);
            }
            else
            {
                ieobject->setURL(url);
            }

            ieobject->getInterface<CORE::IBase>(true)->setName(layerName);

            // Get the world interface and add the raster object
            // XXX - Acquire lock for world
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
            iworld->addObject(iobject.get());
            /*CORE::RefPtr<CORE::ITerrain> terrain = iworld->getTerrain();
            if(terrain.valid())
            {
            terrain->addElevationObject(ieobject.get());
            }*/
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }
    void
        AddContentUIHandler::addVectorData(const std::string &layerName, const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options)
    {
        // Create a new raster data object
        if (url.empty())
        {
            std::string errstring = "Please enter a valid string";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        if (!osgDB::fileExists(url))
        {
            std::string errstring = url + " file does not exist";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add vector data method
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            _subscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            //tp->invoke(boost::bind(&AddContentUIHandler::_mtAddVectorData, this,layerName, url, options));
            //add LayerName to options
            options->setMapValue("LayerName", layerName);
            //tp->invoke(boost::bind(&AddContentUIHandler::_addNewOsgEarthVectorData, this, url, options));
            
            std::string symbologyStringFlag;
            if (options->getMapValue("Symbology", symbologyStringFlag))
            {
                bool symbologyFlag = UTIL::ToType<bool>(symbologyStringFlag);
                
                if (!symbologyFlag){
                    _mtAddVectorData(layerName, url, options);
                }
                else{
                    _addNewOsgEarthVectorData(url, options);
                }
            }
            
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw e;
        }
    }

    void
        AddContentUIHandler::addDGNData(const std::string &layerName, const std::string &url, const std::string &orclUrl, osg::Vec2d extent, bool reloadCache)
    {
        // Create a new raster data object
        if (url.empty())
        {
            std::string errstring = url + " is an invalid file or does not exist";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddDGNData, this, layerName, url, orclUrl, extent, reloadCache));
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw e;
        }
    }

    void AddContentUIHandler::_addNewOsgEarthVectorData(const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options)
    {

        IAddContentStatusMessage::AddContentStatus status = IAddContentStatusMessage::PENDING;
        try
        {
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);

            progressFunctPtr = NULL;
            progressFunctPtr = &setProgress;

            //options->setMapValue("Symbology", "1");
            CORE::RefPtr<CORE::IObject> modelPtr = DB::readFeature(url, options.get(), progressFunctPtr);
            if (modelPtr.valid())
            {
                iworld->addObject(modelPtr.get());
            }
            if (getProgress() == 100)
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
                _unsubscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);
                _notifyStatusToObservers(IAddContentStatusMessage::SUCCESS, 100);
            }
        }
        catch (UTIL::Exception& e)
        {
            e.LogException();

            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }
        catch (...)
        {
            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }

        // notify gui for status
        _notifyStatusToObservers(status, 0);
    }

    void
        AddContentUIHandler::_addOsgEarthVectorData(const std::string &layerName, const std::string &url, bool clamping, std::string iconFileName)
    {
        IAddContentStatusMessage::AddContentStatus status = IAddContentStatusMessage::PENDING;
        try
        {
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IObject> iobject = NULL;

            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::VectorObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IVectorObject> ivobject = iobject->getInterface<TERRAIN::IVectorObject>(true);
            ivobject->getInterface<CORE::IBase>(true)->setName(layerName);
            osgEarth::Drivers::FeatureModelSourceOptions *geomOptions = new osgEarth::Drivers::FeatureGeomModelOptions;
            ivobject->setURL(url);
            if (clamping)
            {
                ivobject->setClampType(TERRAIN::IVectorObject::CLAMP_RELATIVE_TO_TERRAIN);
            }
            ivobject->setModelOptions(geomOptions);
            iworld->addObject(iobject.get());
            status = IAddContentStatusMessage::SUCCESS;
            _notifyStatusToObservers(status, 100);
        }
        catch (UTIL::Exception& e)
        {
            e.LogException();

            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }
        catch (...)
        {
            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }

        // notify gui for status
        _notifyStatusToObservers(status, 0);
    }

    void AddContentUIHandler::computeMinMaxLOD(std::string fileName, unsigned int minLevel, unsigned int maxLevel)
    {
        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtComputeMinMaxLOD, this, fileName, minLevel, maxLevel));
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }


    }

    void AddContentUIHandler::_mtComputeMinMaxLOD(std::string fileName, unsigned int minLevel, unsigned int maxLevel)
    {
        IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODStatus status = IAddContentComputeMinMaxLODMessage::PENDING;

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

        CORE::RefPtr<CORE::ITerrain> terrain = iworld->getTerrain();
        if (terrain.valid())
        {
            terrain->computeMinMaxLOD(fileName, minLevel, maxLevel);
            status = IAddContentComputeMinMaxLODMessage::SUCCESS;
            _notifyComputeMinMaxToObservers(status, minLevel, maxLevel, 0);
        }
    }

    void
        AddContentUIHandler::_mtAddVectorData(const std::string &layerName, const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options)
    {
        IAddContentStatusMessage::AddContentStatus status = IAddContentStatusMessage::PENDING;
        try
        {
            options->setMapValue("Offset", UTIL::ToString(_offset));

            progressFunctPtr = NULL;
            progressFunctPtr = &setProgress;
            // Load the feature data and add to world
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            _subscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);


            CORE::RefPtr<CORE::IObject> object = DB::readFeature(url, options.get(), progressFunctPtr);
            if (!object.valid())
            {
                _unsubscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);
                _object = NULL;
                _notifyStatusToObservers(IAddContentStatusMessage::FAILURE, 0);

                std::string errstring = std::string("Error loading file. Check whether the file is valid or not.");
                throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);

            }

            object->getInterface<CORE::IBase>(true)->setName(layerName);

            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            _object = object;

            return;
        }
        catch (UTIL::Exception& e)
        {
            e.LogException();

            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }
        catch (...)
        {
            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }

        // notify gui for status
        _notifyStatusToObservers(status, 0);
    }

    void
        AddContentUIHandler::_mtAddDGNData(const std::string &layerName, const std::string &url, const std::string &orclUrl, osg::Vec2d extent, bool reloadCache)
    {
        IAddContentStatusMessage::AddContentStatus status = IAddContentStatusMessage::PENDING;

        double latitude = extent.y();
        double longitude = extent.x();

        std::stringstream longitudeStream;
        std::stringstream latitudeStream;

        longitudeStream << longitude;
        latitudeStream << latitude;

        try
        {
            // Load the feature data and add to world

            CORE::RefPtr<DB::ReaderWriter::Options> options = new DB::ReaderWriter::Options();
            options->setMapValue("LATITUDE", latitudeStream.str());
            options->setMapValue("LONGITUDE", longitudeStream.str());
            options->setMapValue("OracleUrl", orclUrl);
            options->setMapValue("RELOAD_CACHE", UTIL::ToString(reloadCache));
            progressFunctPtr = NULL;
            progressFunctPtr = &setProgress;

            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            _subscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);

            CORE::RefPtr<CORE::IObject> ifobject = DB::readFeature(url, options.get(), progressFunctPtr);
            if (!ifobject.valid())
            {
                _unsubscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);
                _object = NULL;
                status = IAddContentStatusMessage::FAILURE;

                std::string errstring = std::string("Unable to load dgn file ") + url;
                throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);

            }
            ifobject->getInterface<CORE::IBase>(true)->setName(layerName);

            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            _object = ifobject;

            return;
        }
        catch (UTIL::Exception& e)
        {
            e.LogException();

            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }
        catch (...)
        {
            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }

        // notify gui for status
        _notifyStatusToObservers(status, 0);
    }

    void
        AddContentUIHandler::initializeAttributes()
    {
        VizUI::UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "AddContentUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("Offset", "Offset",
            CORE::DoubleAttribute::SetFuncType(this, &AddContentUIHandler::setOffset),
            CORE::DoubleAttribute::GetFuncType(this, &AddContentUIHandler::getOffset),
            "Offset name",
            groupName));

        _addAttribute(new CORE::BooleanAttribute(
            "ClampToGround", "ClampToGround",
            CORE::BooleanAttribute::SetFuncType(this, &AddContentUIHandler::setClamping),
            CORE::BooleanAttribute::GetFuncType(this, &AddContentUIHandler::getClamping),
            "AddContentUIHandler clamp to groud",
            "AddContentUIHandler properties"));
    }

    void
        AddContentUIHandler::setOffset(double offset)
    {
        _offset = offset;
    }

    double
        AddContentUIHandler::getOffset() const
    {
        return _offset;
    }

    void
        AddContentUIHandler::setClamping(bool clamping)
    {
        _clamping = clamping;
    }

    bool
        AddContentUIHandler::getClamping() const
    {
        return _clamping;;
    }

    void
        AddContentUIHandler::_notifyStatusToObservers(IAddContentStatusMessage::AddContentStatus status, unsigned int progress)
    {
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAddContentStatusMessage::AddContentStatusMessageType);
        msg->setSender(this);
        IAddContentStatusMessage* addContentStatusMsg = msg->getInterface<IAddContentStatusMessage>();
        if (addContentStatusMsg)
        {
            addContentStatusMsg->setStatus(status);
            addContentStatusMsg->setProgress(progress);
        }
        notifyObservers(*IAddContentStatusMessage::AddContentStatusMessageType, *msg);
    }


    void AddContentUIHandler::_notifyComputeMinMaxToObservers(IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODStatus status, unsigned int minLOD, unsigned int maxLOD, unsigned int progress)
    {
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*SMCUI::IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessageType);
        msg->setSender(this);
        IAddContentComputeMinMaxLODMessage* addContentMsg = msg->getInterface<SMCUI::IAddContentComputeMinMaxLODMessage>();
        if (addContentMsg)
        {
            addContentMsg->setStatus(status);
            addContentMsg->setProgress(progress);
            addContentMsg->setMinLOD(minLOD);
            addContentMsg->setMaxLOD(maxLOD);
        }
        notifyObservers(*IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessageType, *msg, CORE::IObservable::ASYNCHRONOUS);
    }


    osg::Vec4
        AddContentUIHandler::defaultExtentsQuery(const std::string& fileName)
    {
        osg::Vec4 extents;

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

        CORE::RefPtr<CORE::ITerrain> terrain = iworld->getTerrain();
        if (terrain.valid())
        {
            extents = terrain->extentsQuery(fileName);
        }
        return extents;
    }

    void
        AddContentUIHandler::setImageDraggerVisibility(bool value)
    {
        //get selected object
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());
        const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();
        CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();
        CORE::RefPtr<CORE::IObject> selectedObject = iter->second->getInterface<CORE::IObject>();

        if (selectedObject->getInterface<ELEMENTS::IOverlayImage>())
        {
            CORE::RefPtr<ELEMENTS::IOverlayImage> imageOverlay = selectedObject->getInterface<ELEMENTS::IOverlayImage>();
            imageOverlay->setDraggerVisibility(value);
        }
    }

    void AddContentUIHandler::setImageRotateVisibility(bool value)
    {
        //get selected object
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());
        const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();
        CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();
        CORE::RefPtr<CORE::IObject> selectedObject = iter->second->getInterface<CORE::IObject>();

        if (selectedObject->getInterface<ELEMENTS::IOverlayImage>())
        {
            CORE::RefPtr<ELEMENTS::IOverlayImage> imageOverlay = selectedObject->getInterface<ELEMENTS::IOverlayImage>();
            imageOverlay->setRotateSliderVisibility(value);
        }
    }

    void AddContentUIHandler::getGdalSubsets(const std::string &url, std::vector<std::string> &list)
    {
        UTIL::GDALUtils::getGdalSubsetList(url, list);
    }

    void
        AddContentUIHandler::addImageOverlay(const std::string &url, osg::Vec4d &extents, std::string rotation, int subdataset)
    {
        // Create a new raster data object
        if (url.empty())
        {
            std::string errstring = "No filename specified";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        // Check whether the filenames are valid or not
        if (!osgDB::fileExists(url))
        {
            std::string errstring = "Unable to load " + url + ". Check whether the file exists.";
            throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
            /*CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager.get());
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_mtAddImageOverlay, this));*/
            _mtAddImageOverlay(url, extents, rotation, subdataset);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw e;
        }
    }

    void
        AddContentUIHandler::_mtAddImageOverlay(const std::string &url, osg::Vec4d &extents, std::string rotation, int subdataset)
    {
        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> imageLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(imageLoadTask.get());
        imageLoadTask->isProgressable(false);
        imageLoadTask->isCancelable(false);

        try
        {
            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayImageType);
            // Get the base interface
            CORE::RefPtr<ELEMENTS::IOverlayImage> ioverlayImg = iobject->getInterface<ELEMENTS::IOverlayImage>(true);

            ioverlayImg->setSubdatset(subdataset);
            ioverlayImg->setImage(url);
            ioverlayImg->setExtents(extents);

            ioverlayImg->setRotation(rotation);
            ioverlayImg->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName(url));
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            imageLoadTask->setObjectNames(objBase->getName());

            //start the task
            imageLoadTask->start();

            // Get the world interface and add the raster object
            iworld->addObject(iobject.get());

            //finish task
            imageLoadTask->finish();
        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            imageLoadTask->terminate();
            if (iobject.valid())
            {
                // Remove the object from the world
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
                iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            }

            e.LogException();
            throw AddRasterException(e.What(), __FILE__, __LINE__);
        }
    }

    void
        AddContentUIHandler::addOSMWebData(std::string url)
    {

        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> rasterLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(rasterLoadTask.get());
        rasterLoadTask->isProgressable(false);
        rasterLoadTask->isCancelable(false);

        try
        {
            url = url + ".xyz";

            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);
            irobject->setURL(url);

            irobject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName("OSMData"));
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            rasterLoadTask->setObjectNames(objBase->getName());

            //start the task
            rasterLoadTask->start();

            // Get the world interface and add the raster object
            iworld->addObject(iobject.get());

            //finish task
            rasterLoadTask->finish();
        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            rasterLoadTask->terminate();
            if (iobject.valid())
            {
                // Remove the object from the world
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
                iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            }

            e.LogException();
            throw e;
        }
    }

    void
        AddContentUIHandler::removeOSMWebData()
    {

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

        CORE::RefPtr<CORE::IObject> iobject = iworld->getObjectByName("OSMData");
        if (iobject)
        {
            iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
        }


    }

    void
        AddContentUIHandler::addYahooWebData(std::string datasetOption)
    {

        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> rasterLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(rasterLoadTask.get());
        rasterLoadTask->isProgressable(false);
        rasterLoadTask->isCancelable(false);

        try
        {
            std::string url = datasetOption + ".yahoo";

            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);
            irobject->setURL(url);

            irobject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName("YahooData"));
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            rasterLoadTask->setObjectNames(objBase->getName());

            //start the task
            rasterLoadTask->start();

            // Get the world interface and add the raster object
            iworld->addObject(iobject.get());

            //finish task
            rasterLoadTask->finish();
        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            rasterLoadTask->terminate();
            if (iobject.valid())
            {
                // Remove the object from the world
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
                iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            }

            e.LogException();
            throw e;
        }
    }

    void
        AddContentUIHandler::removeYahooWebData()
    {

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

        CORE::RefPtr<CORE::IObject> iobject = iworld->getObjectByName("YahooData");
        if (iobject)
        {
            iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
        }


    }

    void
        AddContentUIHandler::addWorldWindWebData(std::string url)
    {

        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> rasterLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(rasterLoadTask.get());
        rasterLoadTask->isProgressable(false);
        rasterLoadTask->isCancelable(false);

        try
        {
            url += ".wms";

            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);
            irobject->setURL(url);

            irobject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName("WorldWindData"));
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            rasterLoadTask->setObjectNames(objBase->getName());

            //start the task
            rasterLoadTask->start();

            // Get the world interface and add the raster object
            iworld->addObject(iobject.get());

            //finish task
            rasterLoadTask->finish();
        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            rasterLoadTask->terminate();
            if (iobject.valid())
            {
                // Remove the object from the world
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
                iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            }

            e.LogException();
            throw e;
        }
    }

    void
        AddContentUIHandler::removeWorldWindWebData()
    {

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

        CORE::RefPtr<CORE::IObject> iobject = iworld->getObjectByName("WorldWindData");
        if (iobject)
        {
            iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
        }


    }

    void
        AddContentUIHandler::addBingWebData(std::string url)
    {

        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> rasterLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(rasterLoadTask.get());
        rasterLoadTask->isProgressable(false);
        rasterLoadTask->isCancelable(false);

        try
        {
            url = url + ".bing";

            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);
            irobject->setURL(url);

            irobject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName("BingData"));
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            rasterLoadTask->setObjectNames(objBase->getName());

            //start the task
            rasterLoadTask->start();

            // Get the world interface and add the raster object
            iworld->addObject(iobject.get());

            //finish task
            rasterLoadTask->finish();
        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            rasterLoadTask->terminate();
            if (iobject.valid())
            {
                // Remove the object from the world
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
                iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
            }

            e.LogException();
            throw e;
        }
    }

    void
        AddContentUIHandler::removeBingWebData()
    {

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

        CORE::RefPtr<CORE::IObject> iobject = iworld->getObjectByName("BingData");
        if (iobject)
        {
            iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
        }
    }


    bool
        AddContentUIHandler::addGoogleData(const GoogleDataType& type)
    {
        std::string layerName;
        std::string url;
        if (type == AddContentUIHandler::GOOGLE_STREETS)
        {
            if (!osgDB::fileExists("google_streets.html"))
            {
                LOG_ERROR("google_streets.html File not Present , cannot Continue\n");
                return false;
            }
            layerName = "Google_Streets";
            url = "google_streets.html.googleStreetsMap";

            //Cross Checking Whether the Google is already Added or not 
            if (isGoogleStreetDataAdded)
                return true;

        }
        else if (type == AddContentUIHandler::GOOGLE_HYBRID)
        {
            if (!osgDB::fileExists("google_hybrid.html"))
            {
                LOG_ERROR("google_hybrid.html File not Present , cannot Continue\n");
                return false;
            }
            layerName = "Google_Hybrid";
            url = "google_hybrid.html.googleHybridMap";

            //Cross Checking Whether the Google is already Added or not 
            if (isGoogleHybridDataAdded)
                return true;
        }
        else
        {
            LOG_ERROR("Not a Valid Options\n");
            return false;
        }

        CORE::RefPtr<CORE::IObject> iobject = NULL;

        //Creating a task of LoadType and setting its properties
        CORE::RefPtr<APP::ITask> rasterLoadTask = APP::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*APP::AppRegistryPlugin::LoadTaskType);
        CORE::RefPtr<APP::ITaskMaintainer> taskMaintainer = APP::TaskMaintainer::instance();
        taskMaintainer->addTask(rasterLoadTask.get());
        rasterLoadTask->isProgressable(false);
        rasterLoadTask->isCancelable(false);

        try
        {
            // Load the rasterdata and add to world and terrain
            iobject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
            // Get the base interface
            CORE::RefPtr<TERRAIN::IRasterObject> irobject = iobject->getInterface<TERRAIN::IRasterObject>(true);
            irobject->setURL(url);

            irobject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName(layerName));
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

            // Set Object Names to task
            CORE::RefPtr<CORE::IBase> objBase = iobject->getInterface<CORE::IBase>(true);
            rasterLoadTask->setObjectNames(objBase->getName());

            //start the task
            rasterLoadTask->start();

            // Get the world interface and add the raster object
            iworld->addObject(iobject.get());

            //finish task
            rasterLoadTask->finish();

            if (type == AddContentUIHandler::GOOGLE_STREETS)
            {
                isGoogleStreetDataAdded = true;
            }
            else if (type == AddContentUIHandler::GOOGLE_HYBRID)
            {
                isGoogleHybridDataAdded = true;
            }
        }
        catch (const UTIL::Exception& e)
        {
            //Terminate task on exception
            rasterLoadTask->terminate();
            if (iobject.valid())
            {
                // Remove the object from the world
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());
                iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));

                if (type == AddContentUIHandler::GOOGLE_STREETS)
                {
                    isGoogleStreetDataAdded = false;
                }
                else if (type == AddContentUIHandler::GOOGLE_HYBRID)
                {
                    isGoogleHybridDataAdded = false;
                }
            }

            e.LogException();
            throw e;
        }
        return true;
    }

    void
        AddContentUIHandler::removeGoogleData(const GoogleDataType& type)
    {
        std::string layerName;
        if (type == AddContentUIHandler::GOOGLE_STREETS)
        {
            isGoogleStreetDataAdded = false;
            layerName = "Google_Streets";
        }
        else if (type == AddContentUIHandler::GOOGLE_HYBRID)
        {
            layerName = "Google_Hybrid";
            isGoogleHybridDataAdded = false;
        }
        else
        {
            return;
        }

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getManager());

        CORE::RefPtr<CORE::IObject> iobject = iworld->getObjectByName(layerName);
        if (iobject)
        {
            iworld->removeObjectByID(&(iobject->getInterface<CORE::IBase>(true)->getUniqueID()));
        }
    }

    void
        AddContentUIHandler::reorderRasterBands(CORE::IObject* object, const std::vector<int>& bandOrder)
    {
        TERRAIN::IRasterObject* rObject = object->getInterface<TERRAIN::IRasterObject>();

        if (!rObject)
        {
            return;
        }

        osg::Vec3d vBandOrder;
        vBandOrder[0] = bandOrder[0];
        vBandOrder[1] = bandOrder[1];
        vBandOrder[2] = bandOrder[2];

        rObject->setBandOrder(vBandOrder);
        rObject->reorderRasterBands();

    }

    void
        AddContentUIHandler::changeRasterEnhancementType(CORE::IObject* object, const TERRAIN::IRasterObject::EnhancementType enhanceType)
    {
        TERRAIN::IRasterObject* rObject = object->getInterface<TERRAIN::IRasterObject>();

        if (!rObject)
        {
            return;
        }
        rObject->setEnhancementType(enhanceType);

    }

    void AddContentUIHandler::updateModelObject(CORE::IObject* object, std::string templateName)
    {
        try
        {
            CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
            CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
            osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
            tp->invoke(boost::bind(&AddContentUIHandler::_updateModelObject, this, object, templateName));
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            throw e;
        }

    }

    void AddContentUIHandler::_updateModelObject(CORE::IObject* object, std::string jsonFile)
    {

        TERRAIN::IModelObject* modelObject = object->getInterface<TERRAIN::IModelObject>();
        CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
        std::string objectStyleFile = modelObject->getObjectStyleFile();
        osgDB::copyFile(jsonFile, objectStyleFile);
        std::string fileLocation = osgDB::convertFileNameToUnixStyle(objectStyleFile);
        std::string objectName = osgDB::getSimpleFileName(objectStyleFile);
        modelObject->setStyleSheet(objectName);
        
        // copy file from current location to project location
        DB::StyleSheetParser::applyStyleSheet(object, objectStyleFile);

        //modelObject->update();

    }

    void AddContentUIHandler::updateMarkingsObject(CORE::IObject *object, std::string jsonFile)
    {
        CORE::RefPtr<SYMBOLOGY::IStyle> styleFile = DB::StyleSheetParser::readStyle(jsonFile);
        ELEMENTS::FeatureObject* featureNodeHolder = dynamic_cast<ELEMENTS::FeatureObject*>(object);
        if (featureNodeHolder != nullptr)
        {
            featureNodeHolder->setVizPlaceNodeStyle(styleFile);
            featureNodeHolder->setBillboard(true);
        }
    }
} // namespace SMCUI
