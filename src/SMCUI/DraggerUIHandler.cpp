#include <SMCUI/DraggerUIHandler.h>
#include <SMCUI/ILandmarkUIHandler.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Elements/IModelHolder.h>

#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/AttributeTypes.h>
#include <Util/CoordinateConversionUtils.h>

#include <VizUI/ISelectionUIHandler.h>

#include <osg/CullFace>

//////////////////////////////////////////////////////////////////////////////////
//!
//! \file DraggerUIHandler.cpp
//!
//! \brief Contains DraggerUIHandler class implementation
//!
//////////////////////////////////////////////////////////////////////////////////
namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, DraggerUIHandler);
    DEFINE_IREFERENCED(DraggerUIHandler, VizUI::UIHandler);

    DraggerUIHandler::DraggerUIHandler()
    {
        _addInterface(SMCUI::IDraggerUIHandler::getInterfaceName());
        setName(getClassname());

        //! initialize dragger container
        _draggerContainer = new SMCUI::DraggerContainer(this);

        //! setup default geometry
        _draggerContainer->setupDefaultGeometry();
        _draggerContainer->setIntersectionMask(~(CORE::TerrainNodeMask | ELEMENTS::ModelObjectNodeMask));
        _draggerContainer->setDraggerSize(150.0f);

        osg::StateSet * draggerStateset = _draggerContainer->getOrCreateStateSet();

        draggerStateset->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF);
        draggerStateset->setRenderBinDetails(2000, "RenderBin");
        draggerStateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::PROTECTED);// Light state Should PROTECTED in any analysis tool

        //! we want the dragger to handle it's own events automatically
        _draggerContainer->setHandleEvents(true);
    }

    void DraggerUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void DraggerUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void DraggerUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        //! Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                //! Register for world loaded message
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IUIHandlerManager>(getManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
                _subscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);

            }   
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the application has been loaded
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {

            setFocus(false);
        }
        else
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }


    void DraggerUIHandler::setModel(CORE::RefPtr<ELEMENTS::IModelHolder> object)
    {
        CORE::RefPtr<CORE::IWorld> world = 
            APP::AccessElementUtils::getWorldFromManager<APP::IUIHandlerManager>(getManager());

        if(!world.valid())
            return;

        if(object.valid())
        {
            //! Check if already added to world
            //! addChild() should be checking for this already but it has been removed from osg
            if(!world->getOSGGroup()->containsNode(_draggerContainer.get()))
            {
                // Add dragger object to the world
                world->getOSGGroup()->addChild(_draggerContainer.get());
            }
        }
        else
        {
            // Remove dragger object from the world
            world->getOSGGroup()->removeChild(_draggerContainer.get());
        }

         _draggerContainer->setAssociatedModel(object);

    }

    void DraggerUIHandler::sendDraggerUpdatedMessage()
    {
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IDraggerUIHandler::DraggerUpdatedMessageType);
        notifyObservers(*IDraggerUIHandler::DraggerUpdatedMessageType, *msg);
    }

    double DraggerUIHandler::getElevationAt(osg::Vec2d position)
    {
        double elevation = 0.0;

        CORE::RefPtr<CORE::IWorld> world = 
            APP::AccessElementUtils::getWorldFromManager<APP::IUIHandlerManager>(getManager());
        if(world)
        {
            CORE::ITerrain* terrain = world->getTerrain();
            if(terrain)
            {
                terrain->getElevationAt(position, elevation);
            }
        }
        return elevation;
    }

    osg::Camera* DraggerUIHandler::getCamera()
    {
        CORE::RefPtr<APP::IApplication> application = getManager()->getInterface<APP::IManager>()->getApplication();

        if(!application.valid())
        {
            return NULL;
        }

        osg::ref_ptr<osgViewer::CompositeViewer> cviewer = application->getCompositeViewer();
        if(!cviewer.valid())
        {
            return NULL;
        }

        // Get View
        osg::ref_ptr<osgViewer::View> view = cviewer->getView(0);
        if(!view.valid())
        {
            return NULL;
        }

        return view->getCamera();
    }

    void DraggerUIHandler::setFocus(bool value)
    {
        if(value == getFocus())
        {
            return;
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());
        if(selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(!value);
        }
        if(!value)
        {
            setModel(NULL);

            _draggerContainer->setDraggerMode(IDraggerUIHandler::DRAGGER_MODE_NONE);
        }

        VizUI::UIHandler::setFocus(value);
    }

    void DraggerUIHandler::setDragStage(IDraggerUIHandler::DragStage dsArg)
    {
        _dragStage = dsArg;

        if(_dragStage == IDraggerUIHandler::START)
        {
            CORE::RefPtr<SMCUI::ILandmarkUIHandler> luh =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILandmarkUIHandler>(getManager());
            if(luh.valid())
            {
                luh->setProcessMouseEvents(false);
            }
        }
        else if(_dragStage == IDraggerUIHandler::FINISH)
        {
            CORE::RefPtr<SMCUI::ILandmarkUIHandler> luh =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILandmarkUIHandler>(getManager());
            if(luh.valid())
            {
                luh->setProcessMouseEvents(true);
            }
        }
    }

    IDraggerUIHandler::DragStage 
        DraggerUIHandler::getDragStage() const
    {
        return _dragStage;
    }

    void DraggerUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "DraggerUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("DraggerSize", "DraggerSize",
            CORE::DoubleAttribute::SetFuncType(this, &DraggerUIHandler::setDraggerSize),
            CORE::DoubleAttribute::GetFuncType(this, &DraggerUIHandler::getDraggerSize),
            "Dragger Size in Screen",
            groupName));
        _addAttribute(new CORE::BooleanAttribute("FixedInScreen", "FixedInScreen",
            CORE::BooleanAttribute::SetFuncType(this, &DraggerUIHandler::setFixedInScreen),
            CORE::BooleanAttribute::GetFuncType(this, &DraggerUIHandler::getFixedInScreen),
            "Is Dragger Fixed In Screen",
            groupName));
    }

    void DraggerUIHandler::setDraggerSize(const double size)
    {
        _size = size;
        _draggerContainer->setDraggerSize(size);
    }

    double DraggerUIHandler::getDraggerSize() const
    {
        return _size;
    }

    void DraggerUIHandler::setFixedInScreen(const bool fixedInScreen)
    {
        _fixedInScreen = fixedInScreen;
        _draggerContainer->setFixedInScreen(_fixedInScreen);
    }

    bool DraggerUIHandler::getFixedInScreen() const
    {
        return _fixedInScreen;
    }

} // namespace VizUI

