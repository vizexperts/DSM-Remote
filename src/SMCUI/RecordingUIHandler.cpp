/****************************************************************************
 *
 * File             : RecordingUIHandler.cpp
 * Description      : RecordingGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, 
 *****************************************************************************/

#include <Core/CoreRegistry.h>

#include <SMCUI/RecordingUIHandler.h>

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>

#include <AVR/AVRPlugin.h>

#include <Util/Exception.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, RecordingUIHandler);
    DEFINE_IREFERENCED(RecordingUIHandler, VizUI::UIHandler);

    RecordingUIHandler::RecordingUIHandler()
    {
        _addInterface(IRecordingUIHandler::getInterfaceName());
    }

    RecordingUIHandler::~RecordingUIHandler(){}


    void
    RecordingUIHandler::startRecording(std::string filename, int width, int height)
    {
        try
        {
            CORE::RefPtr<CORE::IReferenced> refObject =  CORE::CoreRegistry::instance()->getRefFactory()->createReference(*AVR::AVRRegistryPlugin::CameraRecordingType);
            _cameraRecording = refObject->getInterface<AVR::ICameraRecording>();

            if(_cameraRecording.valid())
            {
                _cameraRecording->setFilename(filename);
                _cameraRecording->setWidth(width);
                _cameraRecording->setHeight(height);
                _cameraRecording->initialize();
            }

            if(!_cameraRecording->startRecording())
            {
                return;
            }

        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }


    }

    void
    RecordingUIHandler::stopRecording()
    {
        if(_cameraRecording.valid())
        {
            _cameraRecording->stopRecording();
        }
    }

} // namespace SMCUI

