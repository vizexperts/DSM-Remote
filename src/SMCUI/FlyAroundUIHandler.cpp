#include <SMCUI/FlyAroundUIHandler.h>
#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <Core/ITemporary.h>
#include <Core/InterfaceUtils.h>
#include <Core/ITickMessage.h>
#include <Util/CoordinateConversionUtils.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/ElementsUtils.h>
#include <osg/PolygonMode>
#include <osg/PositionAttitudeTransform>
#include <osg/LineWidth>
#include <Core/CommonUtilityFunctions.h>
namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, FlyAroundUIHandler);

    DEFINE_IREFERENCED(FlyAroundUIHandler, VizUI::UIHandler);

    FlyAroundUIHandler::FlyAroundUIHandler() : _point(NULL), _arcPat(NULL), _pause(false), _cameraUIHandler(NULL), _omega(5), _stopPreview(false)
    {
        _addInterface(IFlyAroundUIHandler::getInterfaceName());
        setName(getClassname());
    }

    FlyAroundUIHandler::~FlyAroundUIHandler(){}

    void FlyAroundUIHandler::start(FlyAround flyAround, CORE::RefPtr<VizUI::ICameraUIHandler> cameraUIHandler)
    {
        if (cameraUIHandler.valid() && _point.valid())
        {
            CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
            _subscribe(wm, *CORE::IWorldMaintainer::TickMessageType);

            if (_arcPat != NULL)
            {
                CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
                group->removeChild(_arcPat);
                _arcPat = NULL;
            }

            if (!_pause)
            {
                _angularDistance = flyAround.startAngle;
                _flyAround = flyAround;
                _range = sqrt( ( pow(_flyAround.distance, 2 ) ) + ( pow( ( _flyAround.height - _flyAround.lookAtHeight ), 2) ) );
                _pitch = osg::RadiansToDegrees(atan( ( ( _flyAround.height - _flyAround.lookAtHeight ) / _flyAround.distance) ) );

                int rounds = (_flyAround.arcAngle + _flyAround.startAngle) / 360;
                int total = (_flyAround.arcAngle + _flyAround.startAngle);
                total = total % 360;
                _totalAngle = rounds * 360 + total;
                _cameraUIHandler = cameraUIHandler;
            }

            else
            {
                _pause = false;
            }
        }

    }

    void FlyAroundUIHandler::closePopUp()
    {
        if (_arcPat != NULL)
        {
            CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
            group->removeChild(_arcPat);
            _arcPat = NULL;
        }
        
        if (_point.valid())
        {
            _longLatPos = osg::Vec3d(_point->getX(), _point->getY(), _point->getZ());
            if (_cameraUIHandler.valid())
            {
                float range = 100000;
                _cameraUIHandler->setViewPoint(_longLatPos, range, 0.0, -89.0, 0.63);
            }
        }
    }

    void FlyAroundUIHandler::stop()
    {
        if (_arcPat != NULL )
        {
            CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
            group->removeChild(_arcPat);
            _arcPat = NULL;
        }

        if (_point.valid())
        {
            _longLatPos = osg::Vec3d(_point->getX(), _point->getY(), _point->getZ());

            if (_cameraUIHandler.valid()  && !_stopPreview)
            {
                CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
                _unsubscribe(wm, *CORE::IWorldMaintainer::TickMessageType);
                _pause = false;
                _omega = 5;

                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IFlyAroundUIHandler::ShowPlayButtonMessageType);
                notifyObservers(*IFlyAroundUIHandler::ShowPlayButtonMessageType, *msg);
            }

            else if (_cameraUIHandler.valid()  && _stopPreview)
            {
                CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
                _unsubscribe(wm, *CORE::IWorldMaintainer::TickMessageType);
                _pause = false;
                _omega = 5;
                _stopPreview = false;
            }
        }
    }

    void FlyAroundUIHandler::visualise(FlyAround flyAround)
    {
        if (_arcPat != NULL)
        {
            CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
            group->removeChild(_arcPat);
            _arcPat = NULL;
        }
        CORE::RefPtr<osg::Geode> geode = new osg::Geode();
        _arcPat = new osg::PositionAttitudeTransform();

        osg::Vec3 pointECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(_point->getY(), _point->getX(), _point->getZ()));
     
        const float radius = flyAround.distance;
        int arcAngle = flyAround.arcAngle;
        int x = flyAround.startAngle + 90.0;
        x = x % 360;
       // std::cout << x << std::endl;
        float startAngle = osg::DegreesToRadians((float)x);
        const float angleDelta = 2.0f*osg::PI / (float)360;

        osg::Vec3Array* vertexArray = new osg::Vec3Array(arcAngle);
        
        for (unsigned int i = 0; i < arcAngle; ++i, startAngle += angleDelta)
        {
            float c = cosf(startAngle);
            float s = sinf(startAngle);
            (*vertexArray)[i].set((-flyAround.reverse)*(c*radius), s*radius, flyAround.height);
           
        }

        osg::Vec4Array* colors = new osg::Vec4Array;
        colors->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 0.5f));

        osg::Geometry* geometry = new osg::Geometry();
        geometry->setVertexArray(vertexArray);
        if (arcAngle == 360)
        {
            geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, 0, vertexArray->size()));
        }

        else
        {
            geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, vertexArray->size()));
        }
        geometry->setColorArray(colors);
        geometry->setColorBinding(osg::Geometry::BIND_OVERALL);
        geode->addDrawable(geometry);
        geode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(8.0f), osg::StateAttribute::ON);


        CORE::RefPtr<osg::Geode> lineGeode = new osg::Geode();
        CORE::RefPtr<osg::Geode> distanceGeode = new osg::Geode();

        osg::Geometry* heightLine = new osg::Geometry();
        osg::Geometry* distanceLine = new osg::Geometry();

        //setting vertex for distance and height line geometry
        osg::Vec3Array* vertices = new osg::Vec3Array(3);
        double altitudeD = _point->getZ() - ELEMENTS::GetAltitudeAtLongLat(_point->getX(), _point->getY());
        
        (*vertices)[0] = (*vertexArray)[0];
        (*vertices)[1] = osg::Vec3d(_point->getY(), _point->getX(), altitudeD);
        (*vertices)[1].z() = (*vertices)[0].z();
        (*vertices)[2] = osg::Vec3d(_point->getY(), _point->getX(), altitudeD);
        

        heightLine->setVertexArray(vertices);
        heightLine->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 1, 2));
        lineGeode->addDrawable(heightLine);
        lineGeode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(8.0f), osg::StateAttribute::ON);

        osg::ref_ptr<osg::Vec4Array> heightLineColorArr(new osg::Vec4Array());
        heightLineColorArr->push_back(osg::Vec4(0.0, 0.0, 1.0, 0.0));
        heightLine->setColorArray(heightLineColorArr.get());
        heightLine->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);

        distanceLine->setVertexArray(vertices);
        distanceLine->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 0, 2));
        distanceGeode->addDrawable(distanceLine);
        distanceGeode->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(8.0f), osg::StateAttribute::ON);

        osg::ref_ptr<osg::Vec4Array> distanceLineColorArr(new osg::Vec4Array());
        distanceLineColorArr->push_back(osg::Vec4(1.0, 1.0, 0.0, 0.0));
        distanceLine->setColorArray(distanceLineColorArr.get());
        distanceLine->setColorBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);

        _arcPat->addChild(geode.get());
        _arcPat->addChild(lineGeode.get());
        _arcPat->addChild(distanceGeode.get());

        osg::Vec3d localNormal;
        osg::Matrix placementRotation;
        osg::Quat circleOrientation;

        localNormal = pointECEF;
        localNormal.normalize();

        _longLatPos = osg::Vec3d(_point->getX(), _point->getY(), _point->getZ());
        osg::Vec3d northDirection = CORE::CommonUtilityFunctions::getNorthDirVec(_longLatPos);
        northDirection.normalize();

        placementRotation.makeRotate(osg::Vec3d(0, 0, 1), localNormal);
        osg::Vec3d axisXPath = osg::Vec3(0.0, -1.0, 0.0)*placementRotation;
        placementRotation.postMult(osg::Matrixd::rotate(axisXPath, northDirection));
        circleOrientation = placementRotation.getRotate();

        _arcPat->setAttitude(circleOrientation);
        _arcPat->setPosition(pointECEF);

        geode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
        geode->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);


        CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
        group->addChild(_arcPat);
        _arcPat->setNodeMask(CORE::TempObjNodeMask);
    }
    // Handles tick message
    void FlyAroundUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            float deltaTime = message.getInterface<CORE::ITickMessage>()->getDeltaRealTime();
            _angularDistance = _angularDistance + _omega * deltaTime;
            
            if (!_flyAround.loop)
            {
                if (_cameraUIHandler.valid() && (_angularDistance <= _totalAngle) && !_pause)
                {
                    _longLatPos = osg::Vec3d(_point->getX(), _point->getY(), _flyAround.lookAtHeight);
                    _cameraUIHandler->setViewPoint(_longLatPos, _range, (_flyAround.reverse)*(_angularDistance), -_pitch, 0);
                }

                else if (_pause)
                {
                    //wait
                }

                else
                {
                    stop();
                }
            }

            else
            {
                if (_cameraUIHandler.valid()  && !_pause)
                {
                    if (_angularDistance >= _totalAngle)
                    {
                        _angularDistance = _flyAround.startAngle;
                    }

                    _longLatPos = osg::Vec3d(_point->getX(), _point->getY(), _flyAround.lookAtHeight);
                    _cameraUIHandler->setViewPoint(_longLatPos, _range, (_flyAround.reverse)*(_angularDistance), -_pitch, 0);
                    
                }

                else if (_pause)
                {
                    //wait
                }

            }
            
        }
    }

    void FlyAroundUIHandler::setPointCoordinates(CORE::IPoint* point)
    {
        _point = point;
    }

    void FlyAroundUIHandler::pause(Mode preview)
    {
        _pause = true;
        if (preview != Pause)
        {
            if (preview == Stop_Preview)
            {
                _stopPreview = true;
            }

            stop();
            visualise(_flyAround);
        }

        
    }

    void FlyAroundUIHandler::increaseSpeed()
    {
        _omega = _omega * 2;
        if (_omega > 10000)
        {
            _omega = 10000;
        }
    }

    void FlyAroundUIHandler::decreaseSpeed()
    {
        _omega = _omega / 2;
    }

} // namespace SMCUI

