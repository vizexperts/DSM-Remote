#include <SMCUI/CreateFeatureUIHandler.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <Elements/IModelHolder.h>

#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/AttributeTypes.h>
#include <Core/GroupAttribute.h>
#include <Core/WorldMaintainer.h>
#include <SMCElements/ILayerComponent.h>
#include <SMCUI/ILineUIHandler.h>
#include <SMCUI/IAreaUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Core/IPenStyle.h>
#include <Core/IBrushStyle.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, CreateFeatureUIHandler);
    DEFINE_IREFERENCED(CreateFeatureUIHandler, VizUI::UIHandler);

    CreateFeatureUIHandler::CreateFeatureUIHandler()
    {
        _addInterface(SMCUI::ICreateFeatureUIHandler::getInterfaceName());

        _layerType = new CORE::NamedGroupAttribute("Feature");
    }

    void CreateFeatureUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizUI::UIHandler::onAddedToManager();
    }

    void CreateFeatureUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizUI::UIHandler::onRemovedFromManager();
    }

    void CreateFeatureUIHandler::initializeAttributes()
    {
        VizUI::UIHandler::initializeAttributes();

        _addAttribute(new CORE::GroupAttribute("Feature", "Feature",
            CORE::GroupAttribute::SetFuncType(this, &CreateFeatureUIHandler::addCustomLayerType),
            CORE::GroupAttribute::GetFuncType(this, &CreateFeatureUIHandler::getCustomLayerType),
            "CustomLayer attributes",
            "Customlayer attributes"));
    }

    void CreateFeatureUIHandler::addCustomLayerType(const CORE::NamedGroupAttribute& groupattr)
    {
        const CORE::NamedStringAttribute* attType = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupattr.getAttribute("Type"));
        const CORE::NamedStringAttribute* attGeometry = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupattr.getAttribute("Geometry"));
        const CORE::NamedDoubleAttribute* attLineWidth = 
            dynamic_cast<const CORE::NamedDoubleAttribute*>(groupattr.getAttribute("LineWidth"));
        const CORE::NamedStringAttribute* attLineColor = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupattr.getAttribute("LineColor"));

        const CORE::NamedStringAttribute* attBrushColor = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupattr.getAttribute("FillColor"));

        if(!attType || !attGeometry)
        {
            return;
        }

        std::string type = attType->getValue();
        std::string geometry = attGeometry->getValue();
        double lineWidth = 2;
        osg::Vec4 lineColor = osg::Vec4(1, 1, 1, 1);
        osg::Vec4 brushColor = osg::Vec4(1, 1, 1, 1);

        if(attLineWidth)
        {
            lineWidth = attLineWidth->getValue();
        }

        if(attLineColor)
        {
            std::string lineColorStr = attLineColor->getValue();
            readColorFromString(lineColorStr, lineColor);
        }

        if(attBrushColor)
        {
            std::string brushColorStr = attBrushColor->getValue();
            readColorFromString(brushColorStr, brushColor);
        }

        CustomLayer layer;
        layer.layerGeometry = geometry;
        layer.penWidth = lineWidth;
        layer.penColor = lineColor;
        layer.brushColor = brushColor;

        _customLayerMap[type] = layer;
    }

    void CreateFeatureUIHandler::readColorFromString(std::string colorString, osg::Vec4& color)
    {
        std::vector<std::string> tokens;
        UTIL::StringTokenizer<UTIL::IsSlash>::tokenize(tokens, colorString, UTIL::IsSlash());
        if(tokens.size() == 4)
        {
            color.r() = UTIL::ToDouble(tokens[0]);
            color.g() = UTIL::ToDouble(tokens[1]);
            color.b() = UTIL::ToDouble(tokens[2]);
            color.a() = UTIL::ToDouble(tokens[3]);
        }
    }


    CORE::RefPtr<CORE::NamedGroupAttribute> CreateFeatureUIHandler::getCustomLayerType() const
    {
        return _layerType;
    }

    CORE::RefPtr<CORE::IFeatureLayer> CreateFeatureUIHandler::getFeatureLayer(std::string type)
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer;

        CustomLayerMap::const_iterator iter = _customLayerMap.find(type);
        if(iter != _customLayerMap.end())
        {
            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                CORE::WorldMaintainer::instance();

            if(!worldMaintainer.valid())
            {
                return NULL;
            }

            CORE::RefPtr<CORE::IComponent> component = 
                worldMaintainer->getComponentByName("LayerComponent");

            if(!component.valid())
            {
                LOG_ERROR("Layer Component is not found");
                return NULL;
            }

            CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
                component->getInterface<SMCElements::ILayerComponent>();

            if(!layerComponent.valid())
            {
                return NULL;
            }

            layer = layerComponent->getFeatureLayer(type);

            if(!layer.valid())
            {
                std::vector<std::string> attrNames, attrTypes;
                std::vector<int> attrWidths, attrPrecesions;

                if(iter->second.layerGeometry == "Line")
                {
                    CORE::RefPtr<SMCUI::ILineUIHandler> lineUIHandler = 
                        APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getManager());
                    if(lineUIHandler.valid())
                    {
                        lineUIHandler->getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecesions);
                    }
                }
                else if(iter->second.layerGeometry == "Area")
                {
                    CORE::RefPtr<SMCUI::IAreaUIHandler> areaUIHandler = 
                        APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getManager());
                    if(areaUIHandler.valid())
                    {
                        areaUIHandler->getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecesions);
                    }

                }

                layer = layerComponent->getOrCreateFeatureLayer(type, iter->second.layerGeometry ,attrNames
                    , attrTypes, attrWidths, attrPrecesions);

                if(layer.valid())
                {
                    CORE::RefPtr<CORE::IPenStyle> penStyle = layer->getInterface<CORE::IPenStyle>();
                    if(penStyle.valid())
                    {
                        penStyle->setPenColor(iter->second.penColor);
                        penStyle->setPenWidth(iter->second.penWidth);
                    }

                    CORE::RefPtr<CORE::IBrushStyle> brushStyle = layer->getInterface<CORE::IBrushStyle>();
                    if(brushStyle.valid())
                    {
                        brushStyle->setBrushFillColor(iter->second.brushColor);
                    }
                }
            }            
        }

        return layer.release();
    }


}// namespace VizUI