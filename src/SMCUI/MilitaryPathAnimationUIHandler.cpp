#include <SMCUI/MilitaryPathAnimationUIHandler.h>

#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>
#include <Core/ILine.h>
#include <Core/ITemporary.h>
#include <Core/IEditable.h>
#include <Core/ITerrain.h>
#include <Core/ICompositeObject.h>

#include <App/AccessElementUtils.h>

#include <Elements/IMilitaryUnit.h>
#include <Elements/IMilitarySymbol.h>
#include <Elements/ILineAlgebra.h>
#include <Elements/ITimeBasedCheckpointPath.h>
#include <Elements/IPathController.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIcon.h>
#include <Elements/IDrawTechnique.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IClamper.h>
#include <Elements/IRepresentation.h>
#include <Elements/IIntersectionComponent.h>
#include <Elements/IOSGEarthDrawTechnique.h>

#include <VizUI/IModelPickUIHandler.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>

#include <Util/IntersectionUtils.h>

#include <SMCElements/ILayerComponent.h>
#include <Terrain/OEFeatureObjectAdapter.h>
#include <Core/Line.h>
#include <GIS/LineGeometry.h>
#include <Core/ITemporary.h>

#include <Core/IVisibility.h>
#include <Core/IPenStyle.h>

#include <VizUI/ICameraUIHandler.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, MilitaryPathAnimationUIHandler);

    DEFINE_IREFERENCED(MilitaryPathAnimationUIHandler, VizUI::UIHandler);

    MilitaryPathAnimationUIHandler::MilitaryPathAnimationUIHandler()
        : _mode(MODE_NONE)
    {
        _addInterface(IMilitaryPathAnimationUIHandler::getInterfaceName());
        setName(getClassname());
    }

    MilitaryPathAnimationUIHandler::~MilitaryPathAnimationUIHandler(){}

    void MilitaryPathAnimationUIHandler::onAddedToManager()
    {
        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

        if(component.valid())
        {
            _selectionComponent = component->getInterface<CORE::ISelectionComponent>();
        }

        VizUI::UIHandler::onAddedToManager();
    }

    void MilitaryPathAnimationUIHandler::setFocus(bool value)
    {
        // Focus area handler and activate gui
        try
        {
            if(value)
            {
                const CORE::ISelectionComponent::SelectionMap& map = 
                    _selectionComponent->getCurrentSelection();

                CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                    map.begin();

                //XXX - using the first element in the selection
                if(iter != map.end())
                {
                    if(iter->second->getInterface<ELEMENTS::IMilitaryUnit>() ||
                        iter->second->getInterface<ELEMENTS::IMilitarySymbol>())
                    {
                        _milSymbol = iter->second->getInterface<ELEMENTS::IMilitarySymbol>();
                        _unit = iter->second->getInterface<CORE::IPoint>();

                        if(_unit.valid())
                        {
                            ELEMENTS::IPathController* pathController = _unit->
                                getInterface<ELEMENTS::IPathController>();

                            if(pathController)
                            {
                                _path = pathController->getPath();
                            }
                        }
                    }
                }
            }
            else
            {
                // Reset value
                _reset();
            }

            VizUI::UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    void MilitaryPathAnimationUIHandler::setMode(IMilitaryPathAnimationUIHandler::AnimationMode mode)
    {
        _mode = mode;

        if(_mode == SMCUI::IMilitaryPathAnimationUIHandler::MODE_ASSOCIATE_PATH)
        {
            _selectionComponent->setIntermediateSelection(true);
            _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
        }
        else if(_unit.valid() && _mode == SMCUI::IMilitaryPathAnimationUIHandler::MODE_REMOVE_PATH)
        {
            ELEMENTS::IPathController* pathController = _unit->getInterface<ELEMENTS::IPathController>();
            if(pathController)
            {
                _path = pathController->getPath();
                if(_path.valid())
                {
                    CORE::ILine* line = const_cast<CORE::ILine*>(_path->getLine());
                    if(line)
                    {
                        //XXX:: problem if one line is attached to several paths
                        CORE::RefPtr<CORE::IEditable> editable = line->getInterface<CORE::IEditable>();
                        if (editable)
                            editable->setEditable(true);
                    }
                }
                pathController->removePath();
                _path = NULL;
            }
        }
        else
        {
            _selectionComponent->setIntermediateSelection(false);
            _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
        }
    }

    CORE::IObject*
        MilitaryPathAnimationUIHandler::createLineObjectfromFeature(const osgEarth::Features::Feature *feature)
    {
        if (!feature)
            return NULL;



        //Getting the line Feature Object
        CORE::IObject *object =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::LineFeatureObjectType);;
        CORE::ILine* line = NULL;

        line = object->getInterface<CORE::ILine>(true);

        const osgEarth::Features::Geometry *geometry = feature->getGeometry();
        if (!geometry)
            return NULL;

        const osgEarth::Features::MultiGeometry *multiGeom = dynamic_cast<const osgEarth::Features::MultiGeometry *>(geometry); // When tracks are selected
        const osgEarth::Features::LineString *lineString = dynamic_cast<const osgEarth::Features::LineString*>(geometry); // When routes are selected
        const osgEarth::Features::PointSet *pointSet = dynamic_cast<const osgEarth::Features::PointSet*>(geometry);// // When checkpoints are selected

        if (NULL != multiGeom)
        {

            const osgEarth::Features::GeometryCollection& vecComp = multiGeom->getComponents();

            osgEarth::Features::GeometryCollection::const_iterator itrEarth = vecComp.begin();

            if (itrEarth == vecComp.end())
                return NULL;

            while (itrEarth != vecComp.end())
            {
                osgEarth::Features::Geometry::Type gemoType = (*itrEarth)->getType();
                if (osgEarth::Features::Geometry::Type::TYPE_LINESTRING == gemoType)
                {
                    int count = (*itrEarth)->getTotalPointCount();
                    // Makeing line in reverse order because 
                    // GPX line getomatry give point in bottom-up fashion 
                    // and gpx file has timestamp in increaseing order from top to bottom.
                    for (int i = count - 1; i >= 0; --i)
                        //for (int i = 0; i < count; ++i)
                    {
                        osg::Vec3d pos = (*itrEarth)->at(i);

                        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
                        double elevAtPoint1 = 0.0;
                        if (world)
                        {
                            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

                            terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevAtPoint1);
                            pos.set(pos.x(), pos.y(), elevAtPoint1);
                        }
                        line->addPoint(pos);
                    }
                }
                ++itrEarth;
            }
        }
        else if (NULL != lineString)
        {
            int count = lineString->getTotalPointCount();
            // Makeing line in reverse order because 
            // GPX line getomatry give point in bottom-up fashion 
            // and gpx file has timestamp in increaseing order from top to bottom.
            for (int i = count - 1; i >= 0; --i)
            {
                osg::Vec3d pos = lineString->at(i);
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
                double elevAtPoint1 = 0.0;
                if (world)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

                    terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevAtPoint1);
                    pos.set(pos.x(), pos.y(), elevAtPoint1);
                }
                line->addPoint(pos);
            }
        }
        else if (NULL != pointSet)
        {
            int count  = pointSet->getTotalPointCount();
            for (int i = count - 1; i >= 0;--i)
            {
                osg::Vec3d pos = lineString->at(i);
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
                double elevAtPoint1 = 0.0;
                if (world)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

                    terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevAtPoint1);
                    pos.set(pos.x(), pos.y(), elevAtPoint1);
                }
                line->addPoint(pos);
            }
        }
        else
            return NULL;


        
        
        CORE::IPenStyle* penStyle = object->getInterface<CORE::IPenStyle>();
        if (penStyle)
        {
            if (penStyle)
            {
                osg::Vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
                penStyle->setPenColor(white);
            }
        }
    
        CORE::ITemporary* itemp = object->getInterface<CORE::ITemporary>();
        if (itemp)
            itemp->setTemporary(true);

        return object;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void MilitaryPathAnimationUIHandler::update(const CORE::IMessageType& messageType, 
        const CORE::IMessage& message)
    {
        // Check for message type
        // Check if a new object is selected
        if(messageType == *CORE::ISelectionComponent::ObjectClickedMessageType)
        {
            if(!_unit.valid())
            {
                return;
            }

            const CORE::ISelectionComponent::SelectionMap& map = 
                _selectionComponent->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                CORE::ILine* line = iter->second->getInterface<CORE::ILine>();
                
                _gpsLine = NULL;
                if (line)
                {
                    const CORE::ILine* curLine = NULL;
                    if(_path.valid())
                    {
                        curLine = _path->getLine();
                    }

                    if(line != curLine)
                    {
                        _createPath();
                        _path->setLine(line);

                        //! set line as non editable
                        CORE::RefPtr<CORE::IEditable> editable = line->getInterface<CORE::IEditable>();
                        if(editable)
                        {
                            editable->setEditable(false);
                        }

                        CORE::RefPtr<CORE::IPenStyle> penstyle = line->getInterface<CORE::IPenStyle>();
                        if (penstyle)
                        {
                            osg::Vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
                            penstyle->setPenColor(white);
                        }

                    }

                    // move unit to first point of path
                    unsigned int numPoints = line->getPointNumber();
                    if(numPoints > 0)
                    {
                        osg::Vec3d position = line->getValueAt(0);

                        CORE::IPoint* point = _unit->getInterface<CORE::IPoint>();
                        point->setValue(position);

                        //! Get camera uihandler
                        CORE::RefPtr<VizUI::ICameraUIHandler> camUIHandler =
                            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getManager());
                    }

                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage
                        (*IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType);
                    notifyObservers(*IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType, *msg);
                }
                else
                {
                    CORE::RefPtr< TERRAIN::OEFeatureObjectAdapter> featureAdapter = dynamic_cast < TERRAIN::OEFeatureObjectAdapter*>(iter->second.get());
                    if (!featureAdapter.valid())
                        return;

                    CORE::RefPtr<TERRAIN::IModelObject> modelObject =  featureAdapter->getModelObject();

                    /*CORE::IVisibility* iVisible = modelObject->getInterface<CORE::IVisibility>();
                    if (iVisible)
                    {
                        iVisible->setVisibility(false);
                    }*/
                    _attachFileURL = modelObject->getUrl();

                    CORE::RefPtr< const osgEarth::Features::Feature> feature = featureAdapter->getOEFeature();
                    CORE::RefPtr<CORE::ILine> line;
                    CORE::RefPtr<CORE::IObject> lineObject;
                    if (feature.valid())
                    {
                        lineObject = createLineObjectfromFeature(feature.get());
                        if (lineObject)
                            line = lineObject->getInterface<CORE::ILine>();
                        else
                            return;
                    }
                    else
                        return;

                    getWorldInstance()->addObject(lineObject.get());

                    const CORE::ILine* curLine = NULL;
                    if (_path.valid())
                    {
                        curLine = _path->getLine();
                    }

                    if (line != curLine)
                    {
                        _createPath();
                        _path->setLine(line);
                        _gpsLine = line;
                        //! set line as non editable
                        CORE::RefPtr<CORE::IEditable> editable = line->getInterface<CORE::IEditable>();
                        if (editable)
                        {
                            editable->setEditable(false);
                        }
                    }
                    // move unit to first point of path
                    unsigned int numPoints = line->getPointNumber();
                    if (numPoints > 0)
                    {
                        osg::Vec3d position = line->getValueAt(0);

                        CORE::IPoint* point = _unit->getInterface<CORE::IPoint>();
                        point->setValue(position);
                    }

                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage
                        (*IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType);
                    notifyObservers(*IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType, *msg);
                }
            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseDraggedMessageType)
        {
            // Query ModelPickUIHandler interface
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());                
            if(tph.valid())
            {

                CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                    CORE::WorldMaintainer::instance();
                if(!worldMaintainer.valid())
                {
                    return;
                }
                CORE::RefPtr<CORE::IComponent> component = 
                    worldMaintainer->getComponentByName("IntersectionComponent");

                if(!component.valid())
                {
                    return;
                }

                CORE::RefPtr<ELEMENTS::IIntersectionComponent> intersectionComponent = 
                    component->getInterface<ELEMENTS::IIntersectionComponent>();
                if(!intersectionComponent.valid())
                {
                    return;
                }

                osg::ref_ptr<osgViewer::View> view = intersectionComponent->getView();

                CORE::RefPtr<CORE::IComponent> lComponent = 
                    worldMaintainer->getComponentByName("LayerComponent");
                if(!lComponent.valid())
                {
                    return;
                }
                CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
                    lComponent->getInterface<SMCElements::ILayerComponent>();
                if(!layerComponent.valid())
                {
                    return;
                }
                CORE::RefPtr<CORE::IFeatureLayer> lineLayer = layerComponent->getFeatureLayer("Line");
                if(!lineLayer.valid())  // no paths to associate
                {
                    return;
                    // maybe unregister to messages also.
                }

                CORE::RefPtr<CORE::ICompositeObject> co = lineLayer->getInterface<CORE::ICompositeObject>();
                if(!co.valid())
                {
                    return;
                }
                const CORE::ObjectMap& objMap = co->getObjectMap();
                CORE::ObjectMap::const_iterator iter = objMap.begin();

                CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                float mouseX, mouseY;
                mmsg->getMouse()->getPosition(mouseX, mouseY);
                osgUtil::LineSegmentIntersector::Intersections intersections;


                if(false)
                {
                    for(; iter != objMap.end(); iter++)
                    {
                        CORE::RefPtr<CORE::ILine> line = iter->second->getInterface<CORE::ILine>();
                        if(line.valid())
                        {
                            if(UTIL::IntersectionUtils::computeIntersections(view->asView(), 
                                iter->second->getOSGNode(), mouseX, mouseY,intersections))
                            {
                                line->getInterface<CORE::ISelectable>()->setSelected(true);
                                const CORE::ILine* curLine = NULL;
                                if(_path.valid())
                                {
                                    curLine = _path->getLine();
                                }

                                if(line != curLine)
                                {
                                    _createPath();
                                    curLine->getInterface<CORE::ISelectable>()->setSelected(false);
                                    _path->setLine(line);
                                }

                                // move unit to first point of path
                                unsigned int numPoints = line->getPointNumber();
                                if(numPoints > 0)
                                {
                                    osg::Vec3d position = line->getValueAt(0);

                                    CORE::IPoint* point = _unit->getInterface<CORE::IPoint>();
                                    point->setValue(position);
                                }
                            }
                        }
                    }

                }
                else        /////////////////
                {
                    const CORE::IntersectionList& ilist = tph->getCurrentIntersections();

                    if(!ilist.empty())
                    {
                        // Get the IMouseMessage interface
                        CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
                        CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
                        CORE::IntersectionList::const_iterator iter = ilist.begin();
                        for(; iter != ilist.end(); iter++)
                        {
                            CORE::RefPtr<CORE::IObject> object = (*iter)->object;
                            if(object.valid())
                            {
                                CORE::RefPtr<CORE::ILine> line = object->getInterface<CORE::ILine>();
                                if(line.valid())
                                {
                                    const CORE::ILine* curLine = NULL;
                                    if(_path.valid())
                                    {
                                        curLine = _path->getLine();
                                    }

                                    if(line != curLine)
                                    {
                                        _createPath();
                                        _path->setLine(line);
                                    }

                                    // move unit to first point of path
                                    unsigned int numPoints = line->getPointNumber();
                                    if(numPoints > 0)
                                    {
                                        osg::Vec3d position = line->getValueAt(0);

                                        CORE::IPoint* point = _unit->getInterface<CORE::IPoint>();
                                        point->setValue(position);
                                    }

                                }
                            }
                        }
                    }           ////////////
                }
            }
        }
        else 
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }

    CORE::RefPtr<ELEMENTS::IMilitarySymbol> MilitaryPathAnimationUIHandler::getUnit() const{
        return _milSymbol;
    }

    void MilitaryPathAnimationUIHandler::_createPath()
    {
        if(!_unit.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IObject> obj 
            = CORE::CoreRegistry::instance()->getObjectFactory()->
            createObject(*ELEMENTS::ElementsRegistryPlugin::TimeBasedCheckpointPathObjectType);
        if(obj.valid())
        {
            _path = obj->getInterface<ELEMENTS::IPath>();
        }

        ELEMENTS::IPathController* pathController = _unit->getInterface<ELEMENTS::IPathController>();

        if(pathController)
        {
            pathController->setPath(_path.get());
        }
    }

    void MilitaryPathAnimationUIHandler::_reset()
    {
        setMode(MODE_NONE);
        _unit = NULL;
        _path = NULL;

        if(_point.valid())
        {
            getWorldInstance()->removeObjectByID(&(_point->getInterface<CORE::IBase>()->getUniqueID()));
            _point = NULL;
        }
    }

    unsigned int MilitaryPathAnimationUIHandler::getNumCheckpoints()
    {
        unsigned int checkpoints = 0;
        if(_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if(cpPath)
            {
                const ELEMENTS::CheckpointList& cpMap = cpPath->getCheckpointList();
                checkpoints = cpMap.size();
            }
        }

        return checkpoints;
    }

    double MilitaryPathAnimationUIHandler::getPathLength()
    {
        double length = 0;

        if(_path.valid())
        {
            const CORE::ILine* line = _path->getLine();
            if(line)
            {
                length = line->getLength();
            }
        }

        return length;

    }

    ELEMENTS::ICheckpoint* MilitaryPathAnimationUIHandler::getCheckpoint(unsigned int cpID)
    {
        CORE::RefPtr<ELEMENTS::ICheckpoint> cp = NULL;

        if(_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if(cpPath)
            {
                const ELEMENTS::CheckpointList& cpMap = cpPath->getCheckpointList();

                ELEMENTS::CheckpointList::const_iterator iter = cpMap.begin();
                unsigned int i = 0;
                for(; i < cpID && iter != cpMap.end(); ++i, ++iter) ;

                if(i == cpID && iter != cpMap.end())
                {
                    cp = *(iter);
                }
            }
        }

        return cp;
    }

    void MilitaryPathAnimationUIHandler::removeCheckPoint(ELEMENTS::ICheckpoint* cp)
    {
        if(_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if(cpPath)
            {
                cpPath->removeCheckpoint(cp);
            }
        }
    }

    bool MilitaryPathAnimationUIHandler::addCheckpoint(ELEMENTS::ICheckpoint* cp)
    {
        if(_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if(cpPath)
            {
                return cpPath->addCheckpoint(cp);
            }        
        }
        return false;
    }

    bool MilitaryPathAnimationUIHandler::addCheckpoint(const boost::posix_time::ptime& arrivalTime, 
        const boost::posix_time::ptime& departureTime, double distance)
    {

        if(_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if(cpPath)
            {
                return cpPath->addCheckpoint(arrivalTime, departureTime, distance);
            }
        }

        return false;
    }
    bool MilitaryPathAnimationUIHandler::addCheckpoint(const boost::posix_time::ptime& arrivalTime,
        const boost::posix_time::ptime& departureTime, double distance, std::string animationType)
    {
        if (_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if (cpPath)
            {
                return cpPath->addCheckpoint(arrivalTime, departureTime, distance, animationType);
            }
        }

        return false;
    }

    void MilitaryPathAnimationUIHandler::_clampPoint(osg::Vec3d& position)
    {
        

        double longitude = position.x();
        double latitude = position.y();

        double elevation = 0.0;

        CORE::RefPtr<CORE::IWorldMaintainer> maintainer = CORE::WorldMaintainer::instance();

        if(!maintainer.valid())
            return ;

        const CORE::WorldMap& wm = maintainer->getWorldMap();

        if(!wm.size())
            return ;

        CORE::RefPtr<CORE::IWorld> world = wm.begin()->second;

        if(!world.valid())
            return ;

        CORE::ITerrain* terrain = world->getTerrain();

        if(!terrain)
            return ;

        if(terrain->getElevationAt(osg::Vec2d(longitude, latitude), elevation) )
        {
            position.set(longitude, latitude, elevation);
        }

        return;
    }

    void MilitaryPathAnimationUIHandler::highlightPointAtDistance(double distance)
    {
        if(_path.valid())
        {
            const CORE::ILine* line = _path->getLine();
            if(!line)
            {
                return;
            }

            osg::Vec3d position;
            if(!line->getInterface<ELEMENTS::ILineAlgebra>()->getPointAtDistance(distance, position))
            {
                return;
            }

            //_clampPoint(position);

            // create temporary point to show
            _showPointAtDistance(position);
        }
    }

    void MilitaryPathAnimationUIHandler::_showPointAtDistance(osg::Vec3d position)
    {
        if(!_point.valid())
        {
            CORE::RefPtr<CORE::IObject> pointObject = CORE::CoreRegistry::instance()->
                getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

            CORE::RefPtr<ELEMENTS::IIcon> icon;
            //if(_unit.valid())
            //{
            //    CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder =
            //        _unit->getInterface<ELEMENTS::IIconHolder>();
            //    if(iconHolder.valid())
            //    {
            //        icon = iconHolder->getIcon();
            //    }
            //}

            if(!icon.valid())
            {
                //XXX - hard coded icon 
                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");
                if(component.valid())
                {
                    CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader =
                        component->getInterface<ELEMENTS::IIconLoader>();
                    if(iconLoader.valid())
                    {
                        icon = iconLoader->getIconByName("circle");
                    }
                }
            }

            CORE::RefPtr<ELEMENTS::IIconHolder> pointIconHolder = 
                pointObject->getInterface<ELEMENTS::IIconHolder>();
            if(pointIconHolder.valid())
            {
                pointIconHolder->setIcon(icon);
            }

            //set the current temporary state
            CORE::RefPtr<CORE::ITemporary> temp = pointObject->getInterface<CORE::ITemporary>();
            if(temp.valid())
            {
                temp->setTemporary(true);
            }

            // Add the point to the world
            getWorldInstance()->addObject(pointObject.get());
            _point = pointObject->getInterface<CORE::IPoint>();

            CORE::RefPtr<ELEMENTS::IClamper> clamp = pointObject->getInterface<ELEMENTS::IClamper>();
            if(clamp.valid())
            {
                clamp->setClampOnPlacement(false);
            }

            CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = pointObject->getInterface<ELEMENTS::IDrawTechnique>();
            if(draw.valid())
            {
                draw->setHeightOffset(0.0);
            }

            CORE::RefPtr<ELEMENTS::IRepresentation> represent = _point->getInterface<ELEMENTS::IRepresentation>();
            if(represent.valid())
            {
                represent->setRepresentationMode(ELEMENTS::IRepresentation::ICON);
            }

            _point->getInterface<CORE::IBase>()->setName("");

        }

        if(_point.valid())
        {
            // Add the point to the line
            _point->setValue(position);
        }
    }

    bool MilitaryPathAnimationUIHandler::getTrailVisibility() const
    {
        if(_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if(cpPath)
            {
                return cpPath->getTrailVisibility();
            }
        }
        return false;
    }

    void MilitaryPathAnimationUIHandler::setTrailVisibility(bool value)
    {
        if(_path.valid())
        {
            ELEMENTS::ITimeBasedCheckpointPath* cpPath = _path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
            if(cpPath)
            {
                cpPath->setTrailVisibility(value);
            }
        }
    }

    void MilitaryPathAnimationUIHandler::setCheckpointVisibility(bool value)
    {
        if(_path.valid())
        {
            _path->setCheckpointVisibility(value);
        }
    }

    bool MilitaryPathAnimationUIHandler::getCheckpointVisibility() const
    {
        if(_path.valid())
        {
            return _path->getCheckpointVisibility();
        }

        return false;
    }
    CORE::RefPtr<CORE::ILine> MilitaryPathAnimationUIHandler::getLineFromGpx() const
    {
        return _gpsLine;
    }
    const std::string& MilitaryPathAnimationUIHandler::getGpxFileURL() const
    {
        return _attachFileURL;
    }

} // namespace SMCUI

