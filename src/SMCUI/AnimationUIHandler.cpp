#include <SMCUI/AnimationUIHandler.h>
#include <Core/CoreRegistry.h>

#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <Core/InterfaceUtils.h>
#include <Core/ILine.h>
#include <Elements/ICheckpointPath.h>
#include <Elements/IPathController.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, AnimationUIHandler);
    DEFINE_IREFERENCED(AnimationUIHandler, VizUI::UIHandler);

    AnimationUIHandler::AnimationUIHandler()
    {
        _addInterface(IAnimationUIHandler::getInterfaceName());
        setName(getClassname());

        _reset();
    }

    AnimationUIHandler::~AnimationUIHandler() {}

    void AnimationUIHandler::onAddedToManager()
    {
        CORE::RefPtr<CORE::IComponent> component = CORE::WorldMaintainer::instance()->getComponentByName("AnimationComponent");

        if(component.valid())
        {
            _animationComponent = component->getInterface<CORE::IAnimationComponent>();
        }

        VizUI::UIHandler::onAddedToManager();
    }

    void AnimationUIHandler::onRemovedFromManager()
    {

    }

    bool AnimationUIHandler::isAnimationRunning() const
    {
        CORE::IAnimationComponent::AnimationState state = _animationComponent->getAnimationState();
        if(state == CORE::IAnimationComponent::STATE_PLAYING)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    void AnimationUIHandler::setFocus(bool focus)
    {
        //Reset Value
        _reset();

        try
        {
            VizUI::UIHandler::setFocus(focus);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    bool AnimationUIHandler::getFocus() const
    {
        return false;
    }

    void AnimationUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        VizUI::UIHandler::update(messageType, message);
    }

    void AnimationUIHandler::_reset()
    {

    }

    void AnimationUIHandler::setStartEndDateTime(const boost::posix_time::ptime& startTime, const boost::posix_time::ptime& endTime)
    {
        _animationComponent->setStartEndDateTime(startTime, endTime);
    }

    void AnimationUIHandler::setCurrentDateTime(const boost::posix_time::ptime& currentTime)
    {
        _animationComponent->setCurrentDateTime(currentTime);
    }

    void AnimationUIHandler::play()
    {
        _animationComponent->play();
    }

    void AnimationUIHandler::pause()
    {
        _animationComponent->pause();
    }

    void AnimationUIHandler::stop()
    {
        _animationComponent->stop();
    }

    const boost::posix_time::ptime&
        AnimationUIHandler::getStartDateTime()
    {
        return _animationComponent->getStartDateTime();
    }

    const boost::posix_time::ptime&
        AnimationUIHandler::getEndDateTime()
    {
        return _animationComponent->getEndDateTime();
    }

    const boost::posix_time::ptime& 
        AnimationUIHandler::getCurrentDateTime()
    {
        return _animationComponent->getCurrentDateTime();
    }

    double AnimationUIHandler::getTimeScale() const
    {
        return _animationComponent->getTimeScale();
    }

    void AnimationUIHandler::setTimeScale(double newTimeScale)
    {
        _animationComponent->setTimeScale(newTimeScale);
    }
}   // namespace SMCUI

