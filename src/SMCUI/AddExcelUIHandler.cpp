/****************************************************************************
 *
 * File             : AddGPSGUI.h
 * Description      : AddGPSGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *
 ****************************************************************************/

#include <SMCUI/AddCSVUIHandler.h>
#include <SMCUI/AddExcelUIHandler.h>

#include <Util/FileUtils.h>

#include <App/AccessElementUtils.h>

#include <osgDB/FileUtils>

#include <stdio.h>      /* printf */
#include <stdlib.h>     /* system, NULL, EXIT_FAILURE */

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, AddExcelUIHandler);
    DEFINE_IREFERENCED(SMCUI::AddExcelUIHandler, VizUI::UIHandler);

    AddExcelUIHandler::AddExcelUIHandler()
    {
        _addInterface(IAddExcelUIHandler::getInterfaceName());
    }

    void
        AddExcelUIHandler::readFile(const std::string &layerName, const std::string &url)
    {
        try
        {
            UTIL::TemporaryFolder *temporaryFolder = UTIL::TemporaryFolder::instance();
            if(!temporaryFolder)
            {
                throw UTIL::ReturnValueNullException("Temporary Folder is NULL", __FILE__, __LINE__);
            }

#if 0
            std::string commandToExecute = "F:\\SVN4\\Products\\DSM\\vbscripts\\Excel_to_CSV_All_Worksheets.vbs C:\\Users\\abhinav\\Desktop\\excel.xlsx C:\\Users\\abhinav\\AppData\\Roaming\\terrain_DSM\\Temp\\";
#else

            /*std::string scriptPath   = "\"" + osgDB::getRealPath("../../vbscripts/Excel_to_CSV_All_Worksheets.vbs") + "\"";*/
            std::string scriptPath     = "Excel_to_CSV_All_Worksheets.vbs";
            std::string excelFilePath  = "\"" + url  + "\"";
            std::string tempFolderPath = "\"" + temporaryFolder->getPath() + "\\\"";

            std::string commandToExecute = scriptPath + " " + excelFilePath + " " + tempFolderPath;
#endif

            int status = system(commandToExecute.c_str());
            if(status == 0)
            {
                //Getting the CSV UIHandler
                CORE::RefPtr<SMCUI::IAddCSVUIHandler> csvUHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddCSVUIHandler>(getManager());

                if(!csvUHandler.valid())
                {
                    throw UTIL::ReturnValueNullException("AddCSVUIHandler is NULL", __FILE__, __LINE__);

                }

//XXX We don't have way right now to take the output about
//number of the sheets from the VBScript
#if 0
                for(int currSheet=0; currSheet<numSheets; ++currSheet)
                {
                    std::stringstream sheetName("");
                    sheetName <<  temporaryFolder->getPath() << "\\sheet"  << currSheet << ".csv";

                    csvUHandler->readFile(sheetName.str());
                }
#else
                std::stringstream sheetName("");
                std::vector<std::string> hyperlink;
                sheetName <<  temporaryFolder->getPath() << "\\sheet"  << 1 << ".csv";
                csvUHandler->readFile(layerName, sheetName.str(), hyperlink,0,0,0);
#endif
            }
        }
        catch(UTIL::Exception &e)
        {
            e.LogException();
        }
    }
}
