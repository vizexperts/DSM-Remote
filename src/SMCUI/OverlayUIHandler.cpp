#include <SMCUI/OverlayUIHandler.h>
#include <Core/CoreRegistry.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IOverlay.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <Core/ICompositeObject.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, OverlayUIHandler);
    DEFINE_IREFERENCED(OverlayUIHandler, UIHandler);

    OverlayUIHandler::OverlayUIHandler()
    {
        _addInterface(SMCUI::IOverlayUIHandler::getInterfaceName());
    }

    ELEMENTS::IOverlay* 
    OverlayUIHandler::createOverlay(const std::string& overlayName, 
                                    const std::string& authorName, 
                                    const std::string& operationName,
                                    const std::string& operationTaskReference,
                                    const std::string& operationType,
                                    unsigned int operationNumber,
                                    const boost::posix_time::ptime& creationTime)
    {
        if(findOverlay(overlayName) != NULL)
        {
            return NULL;
        }

        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
                CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IObject> overlayObject = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayType);

        if(overlayObject == NULL)
        {
            return NULL;
        }

        CORE::RefPtr<ELEMENTS::IOverlay> overlay = overlayObject->getInterface<ELEMENTS::IOverlay>();

        overlay->setAuthorName(authorName);
        overlay->setOperationName(operationName);
        overlay->setOperationTaskReference(operationTaskReference);
        overlay->setOperationType(operationType);
        overlay->setOperationNumber(operationNumber);
        overlay->setCreationTime(creationTime);

        overlay->getInterface<CORE::IBase>()->setName(overlayName);

        getWorldInstance()->addObject(overlayObject.get());

        return overlay.get();
    }

    ELEMENTS::IOverlay*
    OverlayUIHandler::findOverlay(const std::string& overlayName)
    {
        ELEMENTS::IOverlay* result = NULL;

        const CORE::ObjectMap& objectMap = getWorldInstance()->getObjectMap();
        CORE::ObjectMap::const_iterator iter = objectMap.begin();
        while(iter != objectMap.end())
        {
            ELEMENTS::IOverlay* overlay = iter->second->getInterface<ELEMENTS::IOverlay>();
            if(overlay != NULL)
            {
                if(overlay->getInterface<CORE::IBase>()->getName() == overlayName)
                {
                    result = overlay;
                    break;
                }
            }
            ++iter;
        }
        return result;
    }

    void 
    OverlayUIHandler::setSelectedOverlayAsCurrentOverlay()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
        APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            _overlay = iter->second->getInterface<ELEMENTS::IOverlay>();
        }
    }

    bool
    OverlayUIHandler::createLayer(const std::string& layerName,
                                  const std::string& layerType,
                                  std::vector<std::string>& attrNames,
                                  std::vector<std::string>& attrTypes,
                                  std::vector<int>& attrWidths,
                                  std::vector<int>& attrPrecisions)
    {
        if(!_overlay.valid())
        {
            return false;
        }

        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
                CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IObject> featureLayer = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);

        CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
                featureLayer->getInterface<CORE::IMetadataTableHolder>();

        if(!tableHolder.valid())
        {
            LOG_ERROR("IMetadataTableHolder interface not found in FeatureLayer");
            return false;
        }

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = 
            tableHolder->getMetadataTable();

        if(!metadataTable.valid())
        {
            LOG_ERROR("Invalid IMetadataTable instance");
            return false;
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return false;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return false;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return false;
        }

        // create a new table definition
        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataCreator->createMetadataTableDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Unable to create a IMetadataTableDefn instance");
            return false;
        }

        for(int i = 0; i < (int)attrNames.size(); i++)
        {

        // create fields to store name, latitude, longitude and height
            CORE::RefPtr<CORE::IMetadataFieldDefn> field = 
                metadataCreator->createMetadataFieldDefn();

            field->setName(attrNames.at(i));
            std::string fieldType = attrTypes.at(i);
            if(fieldType == "Text")
            {
                field->setType(CORE::IMetadataFieldDefn::STRING);
            }
            else if(fieldType == "Decimal")
            {
                field->setType(CORE::IMetadataFieldDefn::DOUBLE);
            }
            else if(fieldType == "Integer")
            {
                field->setType(CORE::IMetadataFieldDefn::INTEGER);
            }
            
            field->setLength(attrWidths.at(i));
            field->setPrecision(attrPrecisions.at(i));

            // add the field to the table definition
            tableDefn->addFieldDefn(field.get());
        }

        CORE::IFeatureLayer::FeatureLayerType featureLayerType = 
            CORE::IFeatureLayer::UNKNOWN;
        if(layerType == "Point")
        {
            featureLayerType = CORE::IFeatureLayer::POINT;
        }
        else if(layerType == "Line")
        {
            featureLayerType = CORE::IFeatureLayer::LINE;
        }
        else if(layerType == "Polygon")
        {
            featureLayerType = CORE::IFeatureLayer::POLYGON;
        }
        else if(layerType == "Military Layer")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT;
        }
        else if(layerType == "Model")
        {
            featureLayerType = CORE::IFeatureLayer::MODEL;
        }
        else if(layerType == "Multi Point")
        {
            featureLayerType = CORE::IFeatureLayer::MULTIPOINT;
        }
        else if(layerType == "Multi Line")
        {
            featureLayerType = CORE::IFeatureLayer::MULTILINE;
        }
        else if(layerType == "Multi Polygon")
        {
            featureLayerType = CORE::IFeatureLayer::MULTIPOLYGON;
        }
        else if(layerType == "MilitaryLayerPoint")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_POINT;
        }
        else if(layerType == "MilitaryLayerLine")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_LINE;
        }
        else if(layerType == "MilitaryLayerPolygon")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_POLYGON;
        }
        else if(layerType == "Route")
        {
            featureLayerType = CORE::IFeatureLayer::ROUTE;
        }
        else if(layerType == "Trajectory")
        {
            featureLayerType = CORE::IFeatureLayer::TRAJECTORY;
        }
        else if(layerType == "FormationLayer")
        {
            featureLayerType = CORE::IFeatureLayer::FORMATION;
        }
        else
        {
            featureLayerType = CORE::IFeatureLayer::UNKNOWN;
        }

        metadataTable->setFeatureLayerType(featureLayerType);

        CORE::RefPtr<CORE::IBase> base = featureLayer->getInterface<CORE::IBase>();
        if(base.valid())
        {
            base->setName(layerName);
        }

        // set the field definition to the MetadataTable
        metadataTable->setMetadataTableDefn(tableDefn.get(),featureLayer->getInterface<CORE::IBase>()->getUniqueID().toString());

        // add the layer to the world
        _overlay->getInterface<CORE::ICompositeObject>()->addObject(featureLayer.get());

        return true;
    }

    ELEMENTS::IOverlay*
    OverlayUIHandler::getCurrentOverlay() const
    {
        return _overlay.get();
    }

    void
    OverlayUIHandler::setCurrentOverlay(ELEMENTS::IOverlay* overlay)
    {
        _overlay = overlay;
    }
}
