#include <SMCUI/ColorMapUIHandler.h>

#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>

#include <Elements/ColorMap.h>

#include <Core/InterfaceUtils.h>
#include <Util/FileUtils.h>
#include <Loader/IColorMapReaderWriter.h>

#include <osgDB/DynamicLibrary>
#include <osgDB/FileUtils>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI,ColorMapUIHandler);

    DEFINE_IREFERENCED(ColorMapUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    ColorMapUIHandler::ColorMapUIHandler()
    {
        _addInterface(IColorMapUIHandler::getInterfaceName());
        setName(getClassname());
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    ColorMapUIHandler::~ColorMapUIHandler()
    {
        std::string libraryname = UTIL::getPlatformLibraryName(STR(LOADER));
        osg::ref_ptr<osgDB::DynamicLibrary> dl = osgDB::DynamicLibrary::loadLibrary(libraryname);

        typedef void (*DestroyColorMapLoaderPtr)(LOADER::IColorMapReaderWriter*);

        DestroyColorMapLoaderPtr destroyLoader     = (DestroyColorMapLoaderPtr)(dl->getProcAddress("DestroyColorMapLoader"));
        destroyLoader(_loader);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ColorMapUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void ColorMapUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void ColorMapUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                // Query the component here
                CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());
                if(wmain)
                {
                    CORE::IComponent* comp = wmain->getComponentByName("ColorMapComponent");
                    if(comp)
                    {
                        _colorMapComponent = comp->getInterface<ELEMENTS::IColorMapComponent>();
                    }
                }

                std::string libraryname = UTIL::getPlatformLibraryName(STR(LOADER));
                osg::ref_ptr<osgDB::DynamicLibrary> dl = osgDB::DynamicLibrary::loadLibrary(libraryname);

                typedef LOADER::IColorMapReaderWriter* (*CreateColorMapLoaderPtr)(ELEMENTS::IColorMapComponent*);

                CreateColorMapLoaderPtr createLoader     = (CreateColorMapLoaderPtr)(dl->getProcAddress("CreateColorMapLoader"));
                _loader = createLoader(_colorMapComponent.get());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    CORE::RefPtr<ELEMENTS::IColorMap>
        ColorMapUIHandler::getColorMap(const std::string& name)
    {
        CORE::RefPtr<ELEMENTS::IColorMap> colorMap = _colorMapComponent->loadColorMap(name);
        if(!colorMap)
        {
            std::string fileName = _colorMapComponent->getColorMapFile(name);
            if(osgDB::fileExists(fileName))
            {
                colorMap = _loader->read(fileName);
                colorMap->setName(name);

                if(colorMap.valid())
                {
                    _colorMapComponent->setColorMap(colorMap.get(), name);
                }
            }
        }

        return colorMap.get();
    }

    //! write color map
    bool ColorMapUIHandler::exportColorMap(ELEMENTS::IColorMap* colorMap, const std::string& name)
    {
        std::string fileName = _colorMapComponent->getColorMapFile(name);
        _loader->write(colorMap, fileName);

        // check if file has been written, otherwise return error
        if(osgDB::fileExists(fileName))
        {
            _colorMapComponent->setColorMap(colorMap, name);
            return true;
        }

        return false;
    }

} // namespace SMCUI

