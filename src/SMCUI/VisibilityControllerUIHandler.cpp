/*****************************************************************************
*
* File             : VisibilityControllerUIHandler.cpp
* Description      : VisibilityControllerUIHandler class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

//Header files
#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>
#include <Core/IPropertyFactory.h>
#include <Core/IProperty.h>

#include <SMCUI/VisibilityControllerUIHandler.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>

#include <Elements/ElementsPlugin.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, VisibilityControllerUIHandler);
    DEFINE_IREFERENCED(VisibilityControllerUIHandler, VizUI::UIHandler);

    VisibilityControllerUIHandler::VisibilityControllerUIHandler()
        : _currentSelectedObject(NULL)
        ,_handleButtonRelease(false)
    {
        _addInterface(SMCUI::IVisibilityControllerUIHandler::getInterfaceName());
    }

    VisibilityControllerUIHandler::~VisibilityControllerUIHandler() {}

    void VisibilityControllerUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
        VizUI::UIHandler::onAddedToManager();
    }

    void VisibilityControllerUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
        VizUI::UIHandler::onRemovedFromManager();
    }

    void VisibilityControllerUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            if(wm.valid())
            {
                _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            reset();
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            if(!_handleButtonRelease)
            {
                _handleButtonRelease = true;
                return;
            }
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();
            if(mmsg->getMouse()->getButtonMask() & VizUI::IMouse::RIGHT_BUTTON)
            {
                if(_currentSelectedObject.valid())
                {
                    _currentSelectedObject->getInterface<CORE::ISelectable>()->setSelected(false);
                }

                CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());
                if(selectionUIHandler.valid())
                {
                    selectionUIHandler->clearCurrentSelection();
                }
            }
        }
        else
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }

    IVisibilityControllerUIHandler::PointAddedStatus VisibilityControllerUIHandler::addControlPoint(const boost::posix_time::ptime &time, bool visibilityState)
    {
        if(!_currentSelectedObject.valid())
        {
            return PointAddedStatus::FAIL;
        }

        // check weather visibility property has been added
        if(!_visibilityController.valid())
        {
            // create property
            CORE::RefPtr<CORE::IProperty> vcProperty = CORE::CoreRegistry::instance()->
                getPropertyFactory()->createProperty(*ELEMENTS::ElementsRegistryPlugin::VisibilityControllerPropertyType);

            if(!vcProperty.valid())
            {
                return PointAddedStatus::FAIL;
            }

            _currentSelectedObject->addProperty(vcProperty.get());

            _visibilityController = vcProperty->getInterface<ELEMENTS::IVisibilityController>();
        }

        //checking whether an control point with this time is already exists or not
        ELEMENTS::IVisibilityController::ControlPointList cpList = _visibilityController->getControlPointList();
        for (auto i = cpList.begin(); i != cpList.end(); i++)
        {
            if (i->time == time) 
            {
                return PointAddedStatus::DUPLICATE;
            }
        }

        _visibilityController->addControlPoint(time, visibilityState);
        return PointAddedStatus::SUCCESS;
    }

    void VisibilityControllerUIHandler::removeControlPoint(unsigned int index)
    {
        if(!_visibilityController.valid())
        {
            return;
        }

        _visibilityController->removeControlPoint(index);
    }

    unsigned int VisibilityControllerUIHandler::getNumControlPoints() const
    {
        if(_visibilityController.valid())
        {
            return _visibilityController->getControlPointList().size();
        }

        return 0;
    }

    const ELEMENTS::IVisibilityController::ControlPointList& VisibilityControllerUIHandler::getControlPointList() const
    {
        if(_visibilityController.valid())
        {
            return _visibilityController->getControlPointList();
        }

//        return ELEMENTS::IVisibilityController::ControlPointList();
    }

    bool VisibilityControllerUIHandler::setSelectedObjectAsCurrent()
    {

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if(!selectionUIHandler.valid())
        {
            return false;
        }

        const CORE::ISelectionComponent::SelectionMap& selMap = selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = selMap.begin();

        if(iter != selMap.end())
        {
            _currentSelectedObject = iter->second->getInterface<CORE::IObject>();
        }

        CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());

        if(tph.valid())
        {
            _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
        }

        if(!_currentSelectedObject.valid())
        {
            return false;
        }

        _handleButtonRelease = false;

        CORE::RefPtr<CORE::IProperty> vcProperty = _currentSelectedObject->
            getProperty(ELEMENTS::ElementsRegistryPlugin::VisibilityControllerPropertyType);

        if(vcProperty.valid())
        {
            _visibilityController = vcProperty->getInterface<ELEMENTS::IVisibilityController>();
            return true;
        }

        return false;
    }

    void VisibilityControllerUIHandler::reset()
    {
        _currentSelectedObject = NULL;

        _visibilityController = NULL;

        CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getManager());

        if(tph.valid())
        {
            _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
        }
    }



}
