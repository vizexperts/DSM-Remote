#include <SMCUI/SQLQueryUIHandler.h>
#include <Elements/FeatureLayerUtils.h>
#include <Terrain/IOGRLayerHolder.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IFeatureLayer.h>
#include <App/AccessElementUtils.h>
#include <Core/IComponent.h>
#include <Core/IWorldMaintainer.h>

namespace SMCUI 
{

    DEFINE_META_BASE(SMCUI, SQLQueryUIHandler);
    DEFINE_IREFERENCED(SQLQueryUIHandler, UIHandler);

    SQLQueryUIHandler::SQLQueryUIHandler()
    {
        _addInterface(SMCUI::ISQLQueryUIHandler::getInterfaceName());

        setName(getClassname());
    }

    void 
    SQLQueryUIHandler::onAddedToManager()
    {
    }

    void 
    SQLQueryUIHandler::setFocus(bool value)
    {
        if(!value)
        {
            _sqlResult = NULL;
        }

        UIHandler::setFocus(value);
    }

    void 
    SQLQueryUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        UIHandler::update(messageType, message);
    }

    void
    SQLQueryUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();
    }

    void
    SQLQueryUIHandler::clearSQLQueryResult()
    {
        _sqlResult = NULL;
    }

    CORE::ISQLQueryResult*
    SQLQueryUIHandler::getSQLQueryResult() const
    {
        return _sqlResult.get();
    }

    void
    SQLQueryUIHandler::saveSQLQueryResult()
    {
        CORE::RefPtr<CORE::IWorld> world = getWorldInstance();
        if(world.valid())
        {
            if(_sqlResult.valid())
            {
                if(!_sqlResult->isEmpty())
                {
                    CORE::RefPtr<TERRAIN::IOGRLayerHolder> ogrLayerHolder = 
                        _sqlResult->getInterface<TERRAIN::IOGRLayerHolder>();

                    if(ogrLayerHolder.valid())
                    {
                        OGRLayer* layer = ogrLayerHolder->getOGRLayer();
                        
                        CORE::RefPtr<CORE::IObject> resultLayer = 
                            ELEMENTS::FeatureLayerUtils::createFeatureLayer(layer);

                        if(resultLayer.valid())
                        {
                            world->addObject(resultLayer.get());
                        }
                    }
                }
            }
        }
    }

    CORE::ISQLQueryResult* 
    SQLQueryUIHandler::executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& whereClause)
    {
        _sqlResult = NULL;

        if(!_sqlQuery.valid())
        {
            if(!_loadSQLQuery())
            {
                return NULL;
            }
        }

        if(layer)
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTable> metadataTable = holder->getMetadataTable();
                if(metadataTable.valid())
                {
                    std::string tableName = metadataTable->getName();

                    std::string sqlQuery = "SELECT * FROM '" + tableName + "'";

                    if(!whereClause.empty())
                    {
                        sqlQuery += " WHERE " + whereClause;
                    }

                    _sqlResult = _sqlQuery->executeSQL(sqlQuery, true);
                }
            }
        }

        return _sqlResult.get();
    }

    CORE::ISQLQueryResult* 
    SQLQueryUIHandler::executeSQLQuery2(CORE::IFeatureLayer* layer, const std::string& query1,const std::string& query2)
    {
        _sqlResult = NULL;
        if(!_sqlQuery.valid())
        {
            if(!_loadSQLQuery())
            {
                return NULL;
            }
        }
         if(layer)
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTable> metadataTable = holder->getMetadataTable();
                if(metadataTable.valid())
                {
                    std::string tableName = metadataTable->getName();                    

                    std::string sqlQuery = query1 + " FROM '" + tableName + "' " + query2;                    

                    _sqlResult = _sqlQuery->executeSQL(sqlQuery, true);
                }
            }
        }

        return _sqlResult.get();


    }

    CORE::ISQLQueryResult*
    SQLQueryUIHandler::executeSQLQuery2(CORE::IFeatureLayer* layer, const std::string& query)
    {
        _sqlResult = NULL;
        if(!_sqlQuery.valid())
        {
            if(!_loadSQLQuery())
            {
                return NULL;
            }
        }
         if(layer)
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTable> metadataTable = holder->getMetadataTable();
                if(metadataTable.valid())
                {
                    std::string tableName = metadataTable->getName();                    

                    std::string sqlQuery = query;

                    _sqlResult = _sqlQuery->executeSQL(sqlQuery, true);
                }
            }
        }

        return _sqlResult.get();


    }

    CORE::ISQLQueryResult* 
    SQLQueryUIHandler::executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& fieldNames,
                                        const std::string& whereClause)
    {
        _sqlResult = NULL;

        if(!_sqlQuery.valid())
        {
            if(!_loadSQLQuery())
            {
                return NULL;
            }
        }

        if(layer)
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTable> metadataTable = holder->getMetadataTable();
                if(metadataTable.valid())
                {
                    std::string tableName = metadataTable->getName();

                    std::string fields = fieldNames;
                    if(fields.empty())
                    {
                        fields = "*";
                    }

                    std::string sqlQuery = "SELECT " + fields + " FROM '" + tableName + "'";

                    if(!whereClause.empty())
                    {
                        sqlQuery += " WHERE " + whereClause;
                    }

                    _sqlResult = _sqlQuery->executeSQL(sqlQuery, true);
                }
            }
        }

        return _sqlResult.get();
    }

    CORE::ISQLQueryResult* 
    SQLQueryUIHandler::executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& fieldNames,
                                       const std::string& whereClause, const osg::Vec4d& extents)
    {
        _sqlResult = NULL;

        if(!_sqlQuery.valid())
        {
            if(!_loadSQLQuery())
            {
                return NULL;
            }
        }

        if(layer)
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTable> metadataTable = holder->getMetadataTable();
                if(metadataTable.valid())
                {
                    std::string tableName = metadataTable->getName();

                    std::string fields = fieldNames;
                    if(fields.empty())
                    {
                        fields = "*";
                    }

                    std::string sqlQuery = "SELECT " + fields + " FROM '" + tableName + "'";

                    if(!whereClause.empty())
                    {
                        sqlQuery += " WHERE " + whereClause;
                    }

                    _sqlResult = _sqlQuery->executeSQL(sqlQuery, extents, true);
                }
            }
        }

        return _sqlResult.get();
    }

    bool
    SQLQueryUIHandler::_loadSQLQuery()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
            APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());

        if(wmain.valid())
        {
            CORE::RefPtr<CORE::IComponent> component = 
                wmain->getComponentByName("DataSourceComponent");

            if(component.valid())
            {
                _sqlQuery = component->getInterface<CORE::ISQLQuery>();
            }
        }

        return _sqlQuery.valid();
    }

} // namespace SMCUI
