#include <SMCUI/FloodingUIHandler.h>

#include <Terrain/IRasterObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>
#include <GISCompute/GISComputePlugin.h>
#include <Core/ITerrain.h>
#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/IFilterVisitor.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, FloodingUIHandler);

    DEFINE_IREFERENCED(FloodingUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    FloodingUIHandler::FloodingUIHandler()
    {
        _addInterface(IFloodingUIHandler::getInterfaceName());
        setName(getClassname());
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _extents = new osg::Vec2Array;
        _rise=0.0;
        _filledVolume=0.0;
        _reset();
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    FloodingUIHandler::~FloodingUIHandler(){}

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void FloodingUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void FloodingUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void FloodingUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool FloodingUIHandler::getFocus() const
    {
        return false;
    }
    double FloodingUIHandler::getFilledVolume()
    {
        return _filledVolume;
    }
    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void FloodingUIHandler::execute()
    {
        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if(vfactory)
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if(world)
            {
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::FloodingVisitorType);

                if(!_visitor)
                {
                    LOG_ERROR("Failed to create filter class");
                    return;
                }

                GISCOMPUTE::IFloodingVisitor* iVisitor  = 
                    _visitor->getInterface<GISCOMPUTE::IFloodingVisitor>();


               //! set the breach point
                iVisitor->setBreachPoint(_breachPoint);
                //Set the Azimuth angle of the illumination source
                iVisitor->setWaterLevelRise(_rise);

                if(_extents->size() >1)
                {
                    //set extent on visitor
                    iVisitor->setExtents(_extents.get());
                }
                // set output name
                iVisitor->setOutputName(_name);

                CORE::IBase* applyNode = NULL;
                if(!_elevObj)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    applyNode = terrain->getInterface<CORE::IBase>();
                }
                else
                {
                    applyNode = _elevObj->getInterface<CORE::IBase>();
                }

                //subscribe for filter status
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                applyNode->accept(*_visitor);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void FloodingUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if(filterMessage->getStatus()== GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::IFloodingVisitor> visitor = 
                        base->getInterface<GISCOMPUTE::IFloodingVisitor>();
                
                    if(visitor.valid())
                    {
                         
                        CORE::RefPtr<TERRAIN::IRasterObject> object = visitor->getOutput();
                        //check for AFV CFV AFT
                        _filledVolume=visitor->getFilledVolume();
                        if(object)
                        {
                            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                            if(world)
                            {
                                world->addObject(object->getInterface<CORE::IObject>(true));
                                /*CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                                terrain->addRasterObject(object);*/
                            }
                        }
                        else
                        {
                            throw UTIL::Exception("Failed to create raster object", __FILE__, __LINE__);
                        }
                    }
                }
            }
            catch(...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void FloodingUIHandler::_reset()
    {
        _extents->clear();
        _elevObj = NULL;
        _visitor = NULL;
        _rise=0.0;
        _name = "";
    }

    void FloodingUIHandler::setExtents(osg::Vec4d extents)
    {
        _extents->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _extents->push_back(osg::Vec2d(first.x(), first.y()));
        _extents->push_back(osg::Vec2d(second.x(), second.y()));
    }
    void FloodingUIHandler::setBreachPoint(const osg::Vec3d& point)
    {
        _breachPoint=point;
    }
    void FloodingUIHandler::setWaterLevelRise(double rise)
    {
        _rise=rise;

    }
    osg::Vec3d FloodingUIHandler::getBreachPoint() const 
    {
        return _breachPoint;
    }
    double FloodingUIHandler::getWaterLevelRise() const 
    {
        return _rise;

    }

    osg::Vec4d FloodingUIHandler::getExtents() const
    {
        osg::Vec4d extents(0.0, 0.0, 0.0, 0.0);
        osg::Vec2d first = _extents->at(0);
        osg::Vec2d second = _extents->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    void FloodingUIHandler::setElevationObject(TERRAIN::IElevationObject* obj)
    {
        _elevObj = obj;
    }

    void FloodingUIHandler::setOutputName(std::string& name)
    {
        _name = name;
    }

    void FloodingUIHandler::stopFilter()
    {
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }

} // namespace SMCUI

