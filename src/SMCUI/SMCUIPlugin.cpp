/*****************************************************************************
*
* File             : SMCUIPlugin.cpp
* Description      : SMCUIPlugin class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCUI/SMCUIPlugin.h>
#include <SMCUI/export.h>

#include <Util/FileUtils.h>

#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>

#include <App/IUIHandlerType.h>
#include <App/ManagerType.h>
#include <App/ManagerUtils.h>
#include <App/UndoTransactionType.h>
#include <App/ApplicationRegistry.h>

#include <Core/IBaseVisitor.h>
#include <Core/MessageType.h>
#include <Core/CoreRegistry.h>
#include <Core/MessageUtils.h>
#include <Core/CoreMessage.h>
#include <Core/NamedAttributeMessage.h>

#include <SMCUI/AddContentStatusMessage.h>
#include <SMCUI/TerrainExportStatusMessage.h>
#include <SMCUI/AddContentComputeMinMaxLODMessage.h>
#include <SMCUI/AddContentUIHandler.h>
#include <SMCUI/LineUIHandler.h>
#include <SMCUI/PointUIHandler2.h>
#include <SMCUI/AreaUIHandler.h>
#include <SMCUI/AddGPSUIHandler.h>
#include <SMCUI/OverlayUIHandler.h>
#include <SMCUI/MilitarySymbolPointUIHandler.h>
#include <SMCUI/LandmarkUIHandler.h>
#include <SMCUI/DraggerUIHandler.h>
#include <SMCUI/AnimationUIHandler.h>
#include <SMCUI/MilitaryPathAnimationUIHandler.h>
#include <SMCUI/LookUpProjectUIHandler.h>
#include <SMCUI/AddVectorUIHandler.h>
#include <SMCUI/TourUIHandler.h>
#include <SMCUI/DistanceCalculationUIHandler.h>
#include <SMCUI/SurfaceAreaCalcUIHandler.h>
#include <SMCUI/ElevationProfileUIHandler.h>
#include <SMCUI/AddCSVUIHandler.h>
#include <SMCUI/AddExcelUIHandler.h>
#include <SMCUI/SlopeAspectUIHandler.h>
#include <SMCUI/ColorElevationUIHandler.h>
#include <SMCUI/ColorMapUIHandler.h>
#include <SMCUI/AlmanacUIHandler.h>
#include <SMCUI/CreateLayerUIHandler.h>
#include <SMCUI/FeatureExportUIHandler.h>
#include <SMCUI/EventUIHandler.h>
#include <SMCUI/CreateFeatureUIHandler.h> 
#include <SMCUI/CrestClearanceUIHandler.h>
#include <SMCUI/RadioLOSUIHandler.h>
#include <SMCUI/VisibilityControllerUIHandler.h>
#include <SMCUI/ExportUIHandler.h>
#include <SMCUI/RecordingUIHandler.h>
#include <SMCUI/SlopeCalculationUIHandler.h>
#include <SMCUI/SQLQueryUIHandler.h>
#include <SMCUI/GridDisplayUIHandler.h>
#include <SMCUI/DatabaseLayerLoaderUIHandler.h>
#include <SMCUI/MinMaxElevUIHandler.h>
#include <SMCUI/HeightCalculationUIHandler.h>
#include <SMCUI/ElevationDiffUIHandler.h>
#include <SMCUI/PerimeterCalcUIHandler.h>
#include <SMCUI/SteepestPathUIHandler.h>
#include <SMCUI/ContoursGenerationUIHandler.h>
#include <SMCUI/HillshadeCalculationUIHandler.h>
#include <SMCUI/TerrainExportUIHandler.h>
#include <SMCUI/FloodingUIHandler.h>
#include <SMCUI/WatershedGenerationUIHandler.h>
#include <SMCUI/CutAndFillUIHandler.h>
#include <SMCUI/FlyAroundUIHandler.h>
#include <SMCUI/VectorLayerQueryUIHandler.h>
#include <SMCUI/TacticalSymbolsUIHandler.h>
#include <SMCUI/ExportToGeoserverUIHandler.h>

//----------------------------------------------------------------------------
// Register all interfaces
//----------------------------------------------------------------------------

DEFINE_META_INTERFACE(SMCUI, IAddContentUIHandler);
DEFINE_META_INTERFACE(SMCUI, IPointUIHandler2);
DEFINE_META_INTERFACE(SMCUI, ILineUIHandler);
#ifdef WIN32
DEFINE_META_INTERFACE(SMCUI, IAddGPSUIHandler);
#endif //WIN32
DEFINE_META_INTERFACE(SMCUI, IOverlayUIHandler);
DEFINE_META_INTERFACE(SMCUI, IAreaUIHandler);
DEFINE_META_INTERFACE(SMCUI, IDistanceCalculationUIHandler);
DEFINE_META_INTERFACE(SMCUI, ISurfaceAreaCalcUIHandler);
DEFINE_META_INTERFACE(SMCUI, IElevationProfileUIHandler);

DEFINE_META_INTERFACE(SMCUI, IAddContentStatusMessage);
DEFINE_META_INTERFACE(SMCUI, ITerrainExportStatusMessage);
DEFINE_META_INTERFACE(SMCUI, IMilitarySymbolPointUIHandler);
DEFINE_META_INTERFACE(SMCUI, ILandmarkUIHandler);
DEFINE_META_INTERFACE(SMCUI, IDraggerUIHandler);
DEFINE_META_INTERFACE(SMCUI, IAnimationUIHandler);
DEFINE_META_INTERFACE(SMCUI, IMilitaryPathAnimationUIHandler);
DEFINE_META_INTERFACE(SMCUI, ILookUpProjectUIHandler);
DEFINE_META_INTERFACE(SMCUI, IAddVectorUIHandler);
DEFINE_META_INTERFACE(SMCUI, ITourUIHandler);
DEFINE_META_INTERFACE(SMCUI, IAddContentComputeMinMaxLODMessage);
DEFINE_META_INTERFACE(SMCUI, IAddCSVUIHandler);
DEFINE_META_INTERFACE(SMCUI, IAddExcelUIHandler);
DEFINE_META_INTERFACE(SMCUI, ISlopeAspectUIHandler);
DEFINE_META_INTERFACE(SMCUI, IColorElevationUIHandler);
DEFINE_META_INTERFACE(SMCUI, IColorMapUIHandler);
DEFINE_META_INTERFACE(SMCUI, IAlmanacUIHandler);
DEFINE_META_INTERFACE(SMCUI, ICreateLayerUIHandler);
DEFINE_META_INTERFACE(SMCUI, IFeatureExportUIHandler);
DEFINE_META_INTERFACE(SMCUI, IEventUIHandler);
DEFINE_META_INTERFACE(SMCUI, ICreateFeatureUIHandler);
DEFINE_META_INTERFACE(SMCUI, ICrestClearanceUIHandler);
DEFINE_META_INTERFACE(SMCUI, IRadioLOSUIHandler);
DEFINE_META_INTERFACE(SMCUI, IVisibilityControllerUIHandler);
DEFINE_META_INTERFACE(SMCUI, IExportUIHandler);
DEFINE_META_INTERFACE(SMCUI, IRecordingUIHandler);
DEFINE_META_INTERFACE(SMCUI, ISlopeCalculationUIHandler);
DEFINE_META_INTERFACE(SMCUI, ISQLQueryUIHandler);
DEFINE_META_INTERFACE(SMCUI, IGridDisplayUIHandler);
DEFINE_META_INTERFACE(SMCUI, IDatabaseLayerLoaderUIHandler);
DEFINE_META_INTERFACE(SMCUI, IMinMaxElevUIHandler);
DEFINE_META_INTERFACE(SMCUI, IHeightCalculationUIHandler);
DEFINE_META_INTERFACE(SMCUI, IElevationDiffUIHandler);
DEFINE_META_INTERFACE(SMCUI, IPerimeterCalcUIHandler);
DEFINE_META_INTERFACE(SMCUI, ISteepestPathUIHandler);
DEFINE_META_INTERFACE(SMCUI, IContoursGenerationUIHandler);
DEFINE_META_INTERFACE(SMCUI, IHillshadeCalculationUIHandler);
DEFINE_META_INTERFACE(SMCUI, ITerrainExportUIHandler);
DEFINE_META_INTERFACE(SMCUI, IFloodingUIHandler);
DEFINE_META_INTERFACE(SMCUI, IWatershedGenerationUIHandler);
DEFINE_META_INTERFACE(SMCUI, ICutAndFillUIHandler);
DEFINE_META_INTERFACE(SMCUI, IFlyAroundUIHandler);
DEFINE_META_INTERFACE(SMCUI, IVectorLayerQueryUIHandler);
DEFINE_META_INTERFACE(SMCUI, ITacticalSymbolsUIHandler);
DEFINE_META_INTERFACE(SMCUI, IExportToGeoserverUIHandler);


REGISTER_APPREGISTRY_PLUGIN(SMCUI_DLL_EXPORT, SMCUI, SMCUIRegistryPlugin);
REGISTER_PLUGIN(SMCUI_DLL_EXPORT, SMCUI, SMCUICoreRegistryPlugin);

namespace SMCUI
{
    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::LineUIHandlerType(new VizUI::UIHandlerType("LineUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 1));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AddContentUIHandlerType(new VizUI::UIHandlerType("AddContentUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 2));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::OverlayUIHandlerType(new VizUI::UIHandlerType("OverlayUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 3));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::MilitarySymbolPointUIHandlerType(new VizUI::UIHandlerType("MilitarySymbolPointUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 4));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AreaUIHandlerType(new VizUI::UIHandlerType("AreaUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 5));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::LandmarkUIHandlerType(new VizUI::UIHandlerType("LandmarkUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 6));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::DraggerUIHandlerType(new VizUI::UIHandlerType("DraggerUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 7));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AnimationUIHandlerType(new VizUI::UIHandlerType("AnimationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 8));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::MilitaryPathAnimationUIHandlerType(new VizUI::UIHandlerType("MilitaryPathAnimationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 9));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AddGPSUIHandlerType(new VizUI::UIHandlerType("AddGPSUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 48));

    const std::string SMCUIRegistryPlugin::LibraryName = UTIL::getPlatformLibraryName("SMCUI");
    const std::string SMCUIRegistryPlugin::PluginName = "SMCUI";

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::PointUIHandler2Type(new VizUI::UIHandlerType("PointUIHandler2", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 50));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::LookUpProjectUIHandlerType(new VizUI::UIHandlerType("LookUpProjectUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 51));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AddVectorUIHandlerType(new VizUI::UIHandlerType("AddVectorUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 52));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::TourUIHandlerType(new VizUI::UIHandlerType("TourUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 53));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::DistanceCalculationUIHandlerType(new VizUI::UIHandlerType("DistanceCalculationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 54));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::SurfaceAreaCalcUIHandlerType(new VizUI::UIHandlerType("SurfaceAreaCalcUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 55));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::ElevationProfileUIHandlerType(new VizUI::UIHandlerType("ElevationProfileUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 56));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AddCSVUIHandlerType(new VizUI::UIHandlerType("AddCSVUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 57));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AddExcelUIHandlerType(new VizUI::UIHandlerType("AddExcelUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 58));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::SlopeAspectUIHandlerType(new VizUI::UIHandlerType("SlopeAspectUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 59));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::ColorElevationUIHandlerType(new VizUI::UIHandlerType("ColorElevationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 59));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::ColorMapUIHandlerType(new VizUI::UIHandlerType("ColorMapUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 60));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::AlmanacUIHandlerType(new VizUI::UIHandlerType("AlmanacUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 62));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::CreateLayerUIHandlerType(new VizUI::UIHandlerType("CreateLayerUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 63));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::FeatureExportUIHandlerType(new VizUI::UIHandlerType("FeatureExportUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 64));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::EventUIHandlerType(new VizUI::UIHandlerType("EventUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 65));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::CrestClearanceUIHandlerType(new VizUI::UIHandlerType("CrestClearanceUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 66));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::RadioLOSUIHandlerType(new VizUI::UIHandlerType("RadioLOSUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 67));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::CreateFeatureUIHandlerType(new VizUI::UIHandlerType("CreateFeatureUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 68));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::VisibilityControllerUIHandlerType(new VizUI::UIHandlerType("VisibilityControllerUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 69));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::ExportUIHandlerType(new VizUI::UIHandlerType("ExportUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 70));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::RecordingUIHandlerType(new VizUI::UIHandlerType("RecordingUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 71));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::SlopeCalculationUIHandlerType(new VizUI::UIHandlerType("SlopeCalculationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 73));


    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::SQLQueryUIHandlerType(new VizUI::UIHandlerType("SQLQueryUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 75));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::GridDisplayUIHandlerType(new VizUI::UIHandlerType("GridDisplayUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 77));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::DatabaseLayerLoaderUIHandlerType(new VizUI::UIHandlerType("DatabaseLayerLoaderUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 78));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::MinMaxElevUIHandlerType(new VizUI::UIHandlerType("MinMaxElevUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 89));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::HeightCalculationUIHandlerType(new VizUI::UIHandlerType("HeightCalculationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 92));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::ElevationDiffUIHandlerType(new VizUI::UIHandlerType("ElevationDiffUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 93));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::PerimeterCalcUIHandlerType(new VizUI::UIHandlerType("PerimeterCalcUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 94));
    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::ContoursGenerationUIHandlerType(new VizUI::UIHandlerType("ContoursGenerationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 95));
    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::HillshadeCalculationUIHandlerType(new VizUI::UIHandlerType("HillshadeCalculationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 96));
    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::FloodingUIHandlerType(new VizUI::UIHandlerType("FloodingUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 97));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::WatershedGenerationUIHandlerType(new VizUI::UIHandlerType("WatershedGenerationUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 98));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::CutAndFillUIHandlerType(new VizUI::UIHandlerType("CutAndFillUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 99));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::SteepestPathUIHandlerType(new VizUI::UIHandlerType("SteepestPathUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 100));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::FlyAroundUIHandlerType(new VizUI::UIHandlerType("FlyAroundUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 101));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::VectorLayerQueryUIHandlerType(new VizUI::UIHandlerType("VectorLayerQueryUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 102));
    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::TerrainExportUIHandlerType(new VizUI::UIHandlerType("TerrainExportUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 103));


    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::TacticalSymbolsUIHandlerType(new VizUI::UIHandlerType("TacticalSymbolsUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 104));

    const CORE::RefPtr<APP::IUIHandlerType>
        SMCUIRegistryPlugin::ExportToGeoserverUIHandlerType(new VizUI::UIHandlerType("ExportToGeoserverUIHandler", SMCUIRegistryPlugin::SMCUIRegistryPluginStartId + 105));

    SMCUIRegistryPlugin::~SMCUIRegistryPlugin(){}

    void SMCUIRegistryPlugin::load(const APP::ApplicationRegistry& ar)
    {
        registerUIHandlers(*(ar.getUIHandlerFactory()));
        registerManagers(*(ar.getManagerFactory()));
        registerTransactions(*(ar.getUndoTransactionFactory()));
    }

    void SMCUIRegistryPlugin::unload(const APP::ApplicationRegistry& ar)
    {
        deregisterUIHandlers(*(ar.getUIHandlerFactory()));
        deregisterManagers(*(ar.getManagerFactory()));
        deregisterTransactions(*(ar.getUndoTransactionFactory()));
    }

    void SMCUIRegistryPlugin::registerUIHandlers(APP::IUIHandlerFactory& uf)
    {
        uf.registerUIHandler(*SMCUIRegistryPlugin::LineUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::LineUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::PointUIHandler2Type, new CORE::Proxy<APP::IUIHandler, SMCUI::PointUIHandler2>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::AddContentUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AddContentUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::OverlayUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::OverlayUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::MilitarySymbolPointUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::MilitarySymbolPointUIHandler>());
#ifdef WIN32
        uf.registerUIHandler(*SMCUIRegistryPlugin::AddGPSUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AddGPSUIHandler>());
#endif //WIN32
        uf.registerUIHandler(*SMCUIRegistryPlugin::AreaUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AreaUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::LandmarkUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::LandmarkUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::DraggerUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::DraggerUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::AnimationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AnimationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::MilitaryPathAnimationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::MilitaryPathAnimationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::LookUpProjectUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::LookUpProjectUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::AddVectorUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AddVectorUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::TourUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::TourUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::DistanceCalculationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::DistanceCalculationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::SurfaceAreaCalcUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::SurfaceAreaCalcUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::ElevationProfileUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::ElevationProfileUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::AddCSVUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AddCSVUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::AddExcelUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AddExcelUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::SlopeAspectUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::SlopeAspectUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::ColorElevationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::ColorElevationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::ColorMapUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::ColorMapUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::AlmanacUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::AlmanacUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::CreateLayerUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::CreateLayerUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::FeatureExportUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::FeatureExportUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::EventUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::EventUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::CrestClearanceUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::CrestClearanceUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::RadioLOSUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::RadioLOSUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::CreateFeatureUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::CreateFeatureUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::VisibilityControllerUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::VisibilityControllerUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::ExportUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::ExportUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::VisibilityControllerUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::VisibilityControllerUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::RecordingUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::RecordingUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::SlopeCalculationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::SlopeCalculationUIHandler>());
        //  uf.registerUIHandler(*SMCUIRegistryPlugin::SteepPathCalcUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::SteepPathCalcUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::SQLQueryUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::SQLQueryUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::GridDisplayUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::GridDisplayUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::DatabaseLayerLoaderUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::DatabaseLayerLoaderUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::MinMaxElevUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::MinMaxElevUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::HeightCalculationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::HeightCalculationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::ElevationDiffUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::ElevationDiffUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::PerimeterCalcUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::PerimeterCalcUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::SteepestPathUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::SteepestPathUIHandler>());

        uf.registerUIHandler(*SMCUIRegistryPlugin::ContoursGenerationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::ContoursGenerationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::HillshadeCalculationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::HillshadeCalculationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::TerrainExportUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::TerrainExportUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::FloodingUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::FloodingUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::WatershedGenerationUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::WatershedGenerationUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::CutAndFillUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::CutAndFillUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::FlyAroundUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::FlyAroundUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::VectorLayerQueryUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::VectorLayerQueryUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::TacticalSymbolsUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::TacticalSymbolsUIHandler>());
        uf.registerUIHandler(*SMCUIRegistryPlugin::ExportToGeoserverUIHandlerType, new CORE::Proxy<APP::IUIHandler, SMCUI::ExportToGeoserverUIHandler>());
    }


    void SMCUIRegistryPlugin::deregisterUIHandlers(APP::IUIHandlerFactory& uf)
    {
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::LineUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::PointUIHandler2Type);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AddContentUIHandlerType);
#ifdef WIN32
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AddGPSUIHandlerType);
#endif //WIn32
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::MilitarySymbolPointUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::OverlayUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AreaUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::LandmarkUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::DraggerUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AnimationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::MilitaryPathAnimationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::LookUpProjectUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AddVectorUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::TourUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::DistanceCalculationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::SurfaceAreaCalcUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::ElevationProfileUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AddCSVUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AddExcelUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::SlopeAspectUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::ColorElevationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::ColorMapUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::AlmanacUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::CreateLayerUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::FeatureExportUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::EventUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::CreateFeatureUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::CrestClearanceUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::RadioLOSUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::VisibilityControllerUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::ExportUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::RecordingUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::SlopeCalculationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::SQLQueryUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::GridDisplayUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::DatabaseLayerLoaderUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::MinMaxElevUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::HeightCalculationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::ElevationDiffUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::PerimeterCalcUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::SteepestPathUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::ContoursGenerationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::HillshadeCalculationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::TerrainExportUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::FloodingUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::WatershedGenerationUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::CutAndFillUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::FlyAroundUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::VectorLayerQueryUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::TacticalSymbolsUIHandlerType);
        uf.deregisterUIHandler(*SMCUIRegistryPlugin::ExportToGeoserverUIHandlerType);
    }

    void SMCUIRegistryPlugin::registerManagers(APP::IManagerFactory& mf)
    {
    }

    void SMCUIRegistryPlugin::deregisterManagers(APP::IManagerFactory& mf)
    {
    }

    void SMCUIRegistryPlugin::registerTransactions(APP::IUndoTransactionFactory& tf)
    {
    }

    void SMCUIRegistryPlugin::deregisterTransactions(APP::IUndoTransactionFactory& tf)
    {
    }

    const std::string& SMCUIRegistryPlugin::getLibraryName() const
    {
        return SMCUIRegistryPlugin::LibraryName;
    }

    const std::string& SMCUIRegistryPlugin::getPluginName() const
    {
        return SMCUIRegistryPlugin::PluginName;
    }

    //-------------------------------------------------------------------------
    // SMCUI module core registry plugin
    //-------------------------------------------------------------------------


    const std::string SMCUICoreRegistryPlugin::LibraryName = UTIL::getPlatformLibraryName("SMCUI");
    const std::string SMCUICoreRegistryPlugin::PluginName = "SMCUI";

    const CORE::RefPtr<CORE::IMessageType>
        ILineUIHandler::LineUpdatedMessageType(new CORE::MessageType("LineUpdated", SMCUICoreRegistryPlugin::StartID + 1));

    const CORE::RefPtr<CORE::IMessageType>
        ILineUIHandler::LineCreatedMessageType(new CORE::MessageType("LineCreated", SMCUICoreRegistryPlugin::StartID + 2));

    const CORE::RefPtr<CORE::IMessageType>
        IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType(new CORE::MessageType("MilitarySymbolPointCreated", SMCUICoreRegistryPlugin::StartID + 3));

    const CORE::RefPtr<CORE::IMessageType>
        IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType(new CORE::MessageType("MilitarySymbolPointUpdated", SMCUICoreRegistryPlugin::StartID + 4));

    const CORE::RefPtr<CORE::IMessageType>
        IDraggerUIHandler::DraggerUpdatedMessageType(new CORE::MessageType("DraggerUpdatedMessage", SMCUICoreRegistryPlugin::StartID + 5));

    const CORE::RefPtr<CORE::IMessageType>
        IAreaUIHandler::AreaCreatedMessageType(new CORE::MessageType("AreaCreatedMessageType", SMCUICoreRegistryPlugin::StartID + 6));

    const CORE::RefPtr<CORE::IMessageType>
        ILandmarkUIHandler::LandmarkCreatedMessageType(new CORE::MessageType("LandmarkCreatedType", SMCUICoreRegistryPlugin::StartID + 7));

    const CORE::RefPtr<CORE::IMessageType>
        ILandmarkUIHandler::LandmarkUpdatedMessageType(new CORE::MessageType("LandmarkUpdatedType", SMCUICoreRegistryPlugin::StartID + 8));

    const CORE::RefPtr<CORE::IMessageType>
        IAddContentStatusMessage::AddContentStatusMessageType(new CORE::MessageType("AddContentStatusMessageType", SMCUICoreRegistryPlugin::StartID + 9));

    const CORE::RefPtr<CORE::IMessageType>
        IPointUIHandler2::PointUpdatedMessageType(new CORE::MessageType("PointUpdated", SMCUICoreRegistryPlugin::StartID + 10));

    const CORE::RefPtr<CORE::IMessageType>
        IPointUIHandler2::PointCreatedMessageType(new CORE::MessageType("PointCreated", SMCUICoreRegistryPlugin::StartID + 11));

    const CORE::RefPtr<CORE::IMessageType>
        IAreaUIHandler::AreaUpdatedMessageType(new CORE::MessageType("AreaUpdatedMessageType", SMCUICoreRegistryPlugin::StartID + 12));

    const CORE::RefPtr<CORE::IMessageType>
        ILandmarkUIHandler::LandmarkSelectedMessageType(new CORE::MessageType("LandmarkSelectedType", SMCUICoreRegistryPlugin::StartID + 13));

    const CORE::RefPtr<CORE::IMessageType>
        IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType(new CORE::MessageType("MilitarySymbolPointDeleted", SMCUICoreRegistryPlugin::StartID + 14));

    const CORE::RefPtr<CORE::IMessageType>
        IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType(new CORE::MessageType("MilitarySymbolPointSelected", SMCUICoreRegistryPlugin::StartID + 15));

    const CORE::RefPtr<CORE::IMessageType>
        IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType(new CORE::MessageType("MilitaryPathAttached", SMCUICoreRegistryPlugin::StartID + 16));

    const CORE::RefPtr<CORE::IMessageType>
        IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessageType(new CORE::MessageType("AddContentComputeMinMaxLODMessageType", SMCUICoreRegistryPlugin::StartID + 17));

    const CORE::RefPtr<CORE::IMessageType>
        IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType(new CORE::MessageType("DistanceCalculationCompleted", SMCUICoreRegistryPlugin::StartID + 18));

    const CORE::RefPtr<CORE::IMessageType>
        ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType(new CORE::MessageType("AreaCalculationCompletedMessage", SMCUICoreRegistryPlugin::StartID + 19));

    const CORE::RefPtr<CORE::IMessageType>
        IElevationProfileUIHandler::ProfileCalculatedMessageType(new CORE::MessageType("ProfileCalculatedMessage", SMCUICoreRegistryPlugin::StartID + 20));

    const CORE::RefPtr<CORE::IMessageType>
        IEventUIHandler::EventUpdatedMessageType(new CORE::MessageType("EventUpdated", SMCUICoreRegistryPlugin::StartID + 21));

    const CORE::RefPtr<CORE::IMessageType>
        IEventUIHandler::EventCreatedMessageType(new CORE::MessageType("EventCreated", SMCUICoreRegistryPlugin::StartID + 22));

    const CORE::RefPtr<CORE::IMessageType>
        ICrestClearanceUIHandler::CrestClearanceCompletedMessageType(new CORE::MessageType("CrestClearanceAnalysisCompleted", SMCUICoreRegistryPlugin::StartID + 23));

    const CORE::RefPtr<CORE::IMessageType>
        ISlopeCalculationUIHandler::SlopeCalculationCompletedMessageType(new CORE::MessageType("SlopeCalculationCompleted", SMCUICoreRegistryPlugin::StartID + 26));

    const CORE::RefPtr<CORE::IMessageType>
        IHeightCalculationUIHandler::DraggerUpdated(new CORE::MessageType("DraggerUpdated", SMCUICoreRegistryPlugin::StartID + 30));

    const CORE::RefPtr<CORE::IMessageType>
        IElevationDiffUIHandler::ElevationDiffCalculationCompletedMessageType(new CORE::MessageType("EleDiffCalculationCompleted", SMCUICoreRegistryPlugin::StartID + 31));

    const CORE::RefPtr<CORE::IMessageType>
        IFlyAroundUIHandler::ShowPlayButtonMessageType(new CORE::MessageType("ShowPlayButtonMessage", SMCUICoreRegistryPlugin::StartID + 32));

    const CORE::RefPtr<CORE::IMessageType>
        ITerrainExportUIHandler::ProgressValueUpdatedMessageType(new CORE::MessageType("ProgressValueUpdatedMessage", SMCUICoreRegistryPlugin::StartID + 33));
    const CORE::RefPtr<CORE::IMessageType>
        ITerrainExportStatusMessage::TerrainExportStatusMessageType(new CORE::MessageType("TerrainExportStatusMessageType", SMCUICoreRegistryPlugin::StartID + 34));

    SMCUICoreRegistryPlugin::~SMCUICoreRegistryPlugin(){}

    void
        SMCUICoreRegistryPlugin::load(const CORE::CoreRegistry& cr)
    {
        registerMessages(*(cr.getMessageFactory()));
    }

    void
        SMCUICoreRegistryPlugin::unload(const CORE::CoreRegistry& cr)
    {
        deregisterMessages(*(cr.getMessageFactory()));
    }
    void
        SMCUICoreRegistryPlugin::registerMessages(CORE::IMessageFactory& mf)
    {
        mf.registerMessage(*ILineUIHandler::LineUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ILineUIHandler::LineCreatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IPointUIHandler2::PointUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IPointUIHandler2::PointCreatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IDraggerUIHandler::DraggerUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IAddContentStatusMessage::AddContentStatusMessageType, new CORE::MessageProxy<SMCUI::AddContentStatusMessage>());
        mf.registerMessage(*ITerrainExportStatusMessage::TerrainExportStatusMessageType, new CORE::MessageProxy<SMCUI::TerrainExportStatusMessage>());
        mf.registerMessage(*IAreaUIHandler::AreaCreatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IAreaUIHandler::AreaUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ILandmarkUIHandler::LandmarkCreatedMessageType, new CORE::MessageProxy<CORE::NamedAttributeMessage>());
        mf.registerMessage(*ILandmarkUIHandler::LandmarkUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ILandmarkUIHandler::LandmarkSelectedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessageType, new CORE::MessageProxy<SMCUI::AddContentComputeMinMaxLODMessage>());
        mf.registerMessage(*IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IElevationProfileUIHandler::ProfileCalculatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IEventUIHandler::EventUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IEventUIHandler::EventCreatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ICrestClearanceUIHandler::CrestClearanceCompletedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ISlopeCalculationUIHandler::SlopeCalculationCompletedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IHeightCalculationUIHandler::DraggerUpdated, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IFlyAroundUIHandler::ShowPlayButtonMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
    }

    void
        SMCUICoreRegistryPlugin::deregisterMessages(CORE::IMessageFactory& mf)
    {
        mf.deregisterMessage(*ILineUIHandler::LineUpdatedMessageType);
        mf.deregisterMessage(*ILineUIHandler::LineCreatedMessageType);
        mf.deregisterMessage(*IPointUIHandler2::PointUpdatedMessageType);
        mf.deregisterMessage(*IPointUIHandler2::PointCreatedMessageType);
        mf.deregisterMessage(*IDraggerUIHandler::DraggerUpdatedMessageType);
        mf.deregisterMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType);
        mf.deregisterMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType);
        mf.deregisterMessage(*IAddContentStatusMessage::AddContentStatusMessageType);
        mf.deregisterMessage(*IAreaUIHandler::AreaCreatedMessageType);
        mf.deregisterMessage(*IAreaUIHandler::AreaUpdatedMessageType);
        mf.deregisterMessage(*ILandmarkUIHandler::LandmarkCreatedMessageType);
        mf.deregisterMessage(*ILandmarkUIHandler::LandmarkUpdatedMessageType);
        mf.deregisterMessage(*ILandmarkUIHandler::LandmarkSelectedMessageType);
        mf.deregisterMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType);
        mf.deregisterMessage(*IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType);
        mf.deregisterMessage(*IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType);
        mf.deregisterMessage(*IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessageType);
        mf.deregisterMessage(*IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType);
        mf.deregisterMessage(*ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType);
        mf.deregisterMessage(*IElevationProfileUIHandler::ProfileCalculatedMessageType);
        mf.deregisterMessage(*ICrestClearanceUIHandler::CrestClearanceCompletedMessageType);
        mf.deregisterMessage(*ISlopeCalculationUIHandler::SlopeCalculationCompletedMessageType);
        mf.deregisterMessage(*IHeightCalculationUIHandler::DraggerUpdated);
        mf.deregisterMessage(*IFlyAroundUIHandler::ShowPlayButtonMessageType);
        mf.deregisterMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
        mf.deregisterMessage(*ITerrainExportStatusMessage::TerrainExportStatusMessageType);
    }

    const std::string&
        SMCUICoreRegistryPlugin::getLibraryName() const
    {
        return SMCUICoreRegistryPlugin::LibraryName;
    }

    const std::string&
        SMCUICoreRegistryPlugin::getPluginName() const
    {
        return SMCUICoreRegistryPlugin::PluginName;
    }
}