/****************************************************************************
 *
 * File             : AddGPSGUI.h
 * Description      : AddGPSGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *
 ****************************************************************************/

#include <Core/RefPtr.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataField.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataCreator.h>
#include <Core/IPoint.h>
#include <Core/IFeatureLayer.h>
#include <Core/ICompositeObject.h>
#include <Core/ObjectFactory.h>
#include <Core/CoreRegistry.h>

#include <Elements/ElementsUtils.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IRepresentation.h>
#include <Util/FileUtils.h>

#include <Util/ValidatorUtils.h>
#include <SMCUI/AddCSVUIHandler.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <ogrsf_frmts.h>

#include <boost/algorithm/string.hpp>
#include <DB/ReadFile.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, AddCSVUIHandler);
    DEFINE_IREFERENCED(SMCUI::AddCSVUIHandler, VizUI::UIHandler);

    AddCSVUIHandler::AddCSVUIHandler() : _headerReadingDone(false)
    {
        _addInterface(IAddCSVUIHandler::getInterfaceName());
    }

    void AddCSVUIHandler::getAttributesList( std::vector<std::string>& vStrings,std::string& url)
    {
        if (!osgDB::fileExists(url))
        {
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to locate tour file",
                __FILE__, __LINE__);
        }

        //Trying to open the current tour
        std::ifstream infile(url.c_str());
        if (!infile.good())
        {
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to open Tour file",
                __FILE__, __LINE__);
        }

        // Reading the first line of the file 
        // Assuming first line to be header of the file 
        // with which we will create MetaData
        _headerReadingDone = false;
        if (!_readHeader(infile, _headerTags))
        {
            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "File corrupted.",
                __FILE__, __LINE__);
        }

        _headerReadingDone = true;
        vStrings = _headerTags;
        
    }

    bool 
        AddCSVUIHandler::readFile(const std::string &layerName, const std::string &url, std::vector<std::string>& hyperlinkList, int latIndex, int longIndex,int unitIndex)
    {

        try
        {
           
            //Trying to open the current tour
            std::ifstream infile(url.c_str());
            if(!infile.good())
            {
                UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "Unable to open Tour file",
                    __FILE__, __LINE__);
            }

            const char *pszDriverName = "ESRI Shapefile";
            OGRSFDriver *poDriver = NULL;

            OGRRegisterAll();

            poDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(pszDriverName);
            if (poDriver == NULL)
            {
                return false;
            }

            OGRDataSource *poDS = NULL;

            CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();

            if (!wmain)
            {
                LOG_ERROR("World Maintainer Not found");
                return false;
            }

            std::string projectFolder = osgDB::getFilePath(wmain->getProjectFile());

            std::string csvToShpFolder = projectFolder + "/CsvToShp";
            std::string path = osgDB::convertFileNameToNativeStyle(csvToShpFolder);
            if (!osgDB::fileExists(path))
            {
                osgDB::makeDirectory(path);
            }

            poDS = poDriver->CreateDataSource(csvToShpFolder.c_str());
            if (poDS == NULL)
            {
                LOG_ERROR("Data source could not be created");
                return false;
            }
            OGRSpatialReference ogrReference;
            ogrReference.importFromEPSG(4326);

            OGRLayer *pgLayer = NULL;
            char** options = new char*[2];
            char* option = CPLStrdup("LAUNDER=FALSE");
            options[0] = option;
            options[1] = NULL;

            std::srand(std::time(0));
            int random = std::rand();
            std::string fileName = osgDB::getNameLessExtension(osgDB::getSimpleFileName(layerName));
            fileName = fileName + std::to_string(random);

            pgLayer = poDS->CreateLayer(fileName.c_str(), &ogrReference, wkbPoint25D, options);
            CPLFree(option);
            delete[] options;

            if (pgLayer == NULL)
            {
                LOG_ERROR("Layer could not be created");
                return false;
            }

            std::string currLine("");
            std::getline(infile, currLine);

            for (int i = 0; i < _headerTags.size(); i++)
            {
                std::string value = _headerTags[i];

                std::vector<std::string>::iterator it;
                it = std::find(hyperlinkList.begin(), hyperlinkList.end(), value);

                if (it != hyperlinkList.end())
                {
                    value = "HYP_" + _headerTags[i];
                    if (value.size() > 10)
                    {
                        std::string trimmedString = value.substr(0, 10);
                        value = trimmedString;
                    }
                    std::replace(_headerTags.begin(), _headerTags.end(), _headerTags[i], value);
                    OGRFieldDefn oField(value.c_str(), OFTString);
                    oField.SetWidth(32);
                    pgLayer->CreateField(&oField);
                }

                else
                { 
                    if (value.size() > 10)
                    {
                        std::string trimmedString = value.substr(0, 10);
                        value = trimmedString;
                    }
                    std::replace(_headerTags.begin(), _headerTags.end(), _headerTags[i], value);
                    OGRFieldDefn oField(value.c_str(), OFTString);
                    oField.SetWidth(32);
                    pgLayer->CreateField(&oField);
                }
            }

            while (infile.good())
            {
                OGRFeature *ogrFeature;
                ogrFeature = OGRFeature::CreateFeature(pgLayer->GetLayerDefn());
               
                std::vector<std::string> values;
                _readHeader(infile, values);

                //in the case of empty line
                if (values.size() < 2)
                {
                    continue;
                }

                if ( ( (values.size() == _headerTags.size() - 1) || (values.size() == _headerTags.size() - 2) ) && _currLine.back() == ',')
                {
                    values.push_back("NULL");
                }

                if ( ( (values.size() == _headerTags.size() - 1) || (values.size() == _headerTags.size() - 2) ) && _currLine.front() == ',')
                {
                    values.insert(values.begin(), "NULL");
                }

                if (values.size() == _headerTags.size())
                {
                    for (int i = 0; i < _headerTags.size(); i++)
                    {
                        ogrFeature->SetField(_headerTags[i].c_str(), values[i].c_str());
                    }
                    //reseting the value
                    double latitude = 0;
                    double longitude = 0;

                    if (unitIndex == 1)
                    {

                        if (values[latIndex].find('\'') != std::string::npos || values[latIndex].find('\"') != std::string::npos)
                        {
                            LOG_ERROR("Invalid latitude");
                            return false;
                        }

                        if (values[longIndex].find('\'') != std::string::npos || values[longIndex].find('\"') != std::string::npos)
                        {
                            LOG_ERROR("Invalid longitude");
                            return false;
                        }

                        latitude = std::atof(values[latIndex].c_str());
                        longitude = std::atof(values[longIndex].c_str());


                    }

                    else if (unitIndex == 2)
                    {

                        if (values[latIndex].find('\'') == std::string::npos || values[latIndex].find('\"') != std::string::npos)
                        {
                            LOG_ERROR("Invalid latitude");
                            return false;
                        }

                        if (values[longIndex].find('\'') == std::string::npos || values[longIndex].find('\"') != std::string::npos)
                        {
                            LOG_ERROR("Invalid longitude");
                            return false;
                        }

                        std::string latString = values[latIndex].c_str();

                        std::vector<std::string> tokensLat;
                        UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokensLat, latString, UTIL::IsDelimeter(' '));

                        tokensLat[0] = boost::replace_all_copy(tokensLat[0], "\"", "");
                        float latDeg = std::atof(tokensLat[0].c_str());
                        float latMin = std::atof(tokensLat[1].c_str());

                        bool dirSouth = false;
                        

                        latitude = latDeg + latMin / 60;

                        if (latString.find('S') != std::string::npos || latString.find('s') != std::string::npos)
                        {
                            latitude = latitude * -1;
                            dirSouth = true;
                        }

                        std::string latitudeString = std::to_string((int)latDeg) + " " + std::to_string((int)latMin) + "\'";

                        if (dirSouth)
                        {
                            latitudeString = latitudeString + " S";
                        }

                        else
                        {
                            latitudeString = latitudeString + " N";
                        }

                        ogrFeature->SetField(_headerTags[latIndex].c_str(), latitudeString.c_str());


                        std::string longString = values[longIndex].c_str();

                        std::vector<std::string> tokensLong;
                        UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokensLong, longString, UTIL::IsDelimeter(' '));
                        tokensLong[0] = boost::replace_all_copy(tokensLong[0], "\"", "");
                        float longDeg = std::atof(tokensLong[0].c_str());
                        float longMin = std::atof(tokensLong[1].c_str());

                        bool dirWest = false;

                        longitude = longDeg + longMin / 60;

                        if (longString.find('W') != std::string::npos || longString.find('w') != std::string::npos)
                        {
                            longitude = longitude * -1;
                            dirWest = true;
                        }

                        std::string longitudeString = std::to_string((int)longDeg) + " " + std::to_string((int)longMin) + "\'";

                        if (dirWest)
                        {
                            longitudeString = longitudeString + " W";
                        }

                        else
                        {
                            longitudeString = longitudeString + " E";
                        }

                        ogrFeature->SetField(_headerTags[longIndex].c_str(), longitudeString.c_str());
                    }

                    else if (unitIndex == 3)
                    {
                        if (values[latIndex].find('\'') == std::string::npos || values[latIndex].find('\"') == std::string::npos)
                        {
                            LOG_ERROR("Invalid latitude");
                            return false;
                        }

                        if (values[longIndex].find('\'') == std::string::npos || values[longIndex].find('\"') == std::string::npos)
                        {
                            LOG_ERROR("Invalid longitude");
                            return false;
                        }

                        std::string latString = values[latIndex].c_str();

                        std::vector<std::string> tokensLat;
                        UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokensLat, latString, UTIL::IsDelimeter(' '));

                        tokensLat[0] = boost::replace_all_copy(tokensLat[0], "\"", "");
                        float latDeg = std::atof(tokensLat[0].c_str());
                        float latMin = std::atof(tokensLat[1].c_str());
                        float latSec = std::atof(tokensLat[2].c_str());

                        latitude = latDeg + latMin / 60 + latSec / 3600;

                        bool dirSouth = false;

                        if (latString.find('S') != std::string::npos || latString.find('s') != std::string::npos)
                        {
                            latitude = latitude * -1;
                            dirSouth = true;
                        }

                        std::string latitudeString = std::to_string((int)latDeg) + " " + std::to_string((int)latMin) + "\' " + std::to_string(latSec) + "\"";

                        if (dirSouth)
                        {
                            latitudeString = latitudeString + " S";
                        }

                        else
                        {
                            latitudeString = latitudeString + " N";
                        }

                        ogrFeature->SetField(_headerTags[latIndex].c_str(), latitudeString.c_str());




                        std::string longString = values[longIndex].c_str();

                        std::vector<std::string> tokensLong;
                        UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokensLong, longString, UTIL::IsDelimeter(' '));
                        tokensLong[0] = boost::replace_all_copy(tokensLong[0], "\"", "");
                        float longDeg = std::atof(tokensLong[0].c_str());
                        float longMin = std::atof(tokensLong[1].c_str());
                        float longSec = std::atof(tokensLong[2].c_str());

                        longitude = longDeg + longMin / 60 + longSec / 3600;

                        bool dirWest = false;

                        if (longString.find('W') != std::string::npos || longString.find('w') != std::string::npos)
                        {
                            longitude = longitude * -1;
                            dirWest = true;
                        }

                        std::string longitudeString = std::to_string((int)longDeg) + " " + std::to_string((int)longMin) + "\' " + std::to_string(longSec) + "\"";

                        if (dirWest)
                        {
                            longitudeString = longitudeString + " W";
                        }

                        else
                        {
                            longitudeString = longitudeString + " E";
                        }

                        ogrFeature->SetField(_headerTags[longIndex].c_str(), longitudeString.c_str());


                    }
                    
                   
                    //adding only valid points
                    if (!UTIL::ValidatorUtils::IsValidLatitude(latitude))
                        continue;

                    if (!UTIL::ValidatorUtils::IsValidLongitude(longitude))
                        continue;

                    OGRPoint ogrPoint(longitude, latitude, 0);

                    ogrFeature->SetGeometry(&ogrPoint);
                    pgLayer->CreateFeature(ogrFeature);
                    OGRFeature::DestroyFeature(ogrFeature);
                }
            }
           
            infile.close();
            if (poDS)
            {
                OGRDataSource::DestroyDataSource(poDS);
                poDS = NULL;
            }


            //! Add the created shape file to world as a model layer.
            CORE::RefPtr<DB::ReaderWriter::Options> readFileOptions = new DB::ReaderWriter::Options;
            readFileOptions->setMapValue("Symbology", "1");
            std::string filepath = csvToShpFolder + "/" + fileName + ".shp";
            CORE::RefPtr<CORE::IObject> modelPtr = DB::readFeature(filepath, readFileOptions.get());
            modelPtr->getInterface<CORE::IBase>()->setName(layerName);
            if (modelPtr.valid())
            {
                CORE::RefPtr <CORE::IWorld> world = ELEMENTS::GetFirstWorldFromMaintainer();
                if (world.valid())
                {
                    world->addObject(modelPtr);
                }

            }
        }
        catch(UTIL::Exception &e)
        {
            e.LogException();

            throw e;
        }

        return true;
    }

    bool
    AddCSVUIHandler::_readHeader(std::ifstream &inFile, std::vector<std::string> &vStrings)
    {
        try
        {
            //Readind the tour Name
            if(!inFile.good())
                return false;

            //reading the line
            _currLine = "";
            std::getline(inFile, _currLine);

            while (true)
            {
                size_t index = _currLine.find(",,");
                if (index != std::string::npos)
                {
                    _currLine = boost::replace_all_copy(_currLine, ",,", ",NULL,");
                }
                else
                {
                    break;
                }

            }

            if (!_headerReadingDone)
            {
                _currLine = boost::replace_all_copy(_currLine, ".", "");
                _headerReadingDone = true;
            }
        
            UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(vStrings, _currLine, UTIL::IsDelimeter(','));
            return true;
        }
        catch(...)
        {
        }

        return false;
    }

    CORE::IObject *
    AddCSVUIHandler::_createFeatureLayer()
    {
        CORE::RefPtr<CORE::IObject> featureLayer = NULL;
        {
            CORE::RefPtr<CORE::IObjectFactory> objectFactory =  
                    CORE::CoreRegistry::instance()->getObjectFactory();
            if(!objectFactory.valid())
            {
                throw UTIL::ReturnValueNullException("Object factory is NULL", __FILE__, __LINE__);
            }

            featureLayer = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);
        }

        return featureLayer.release();
    }

    CORE::IObject *
    AddCSVUIHandler::_createOverlayObject()
    {
        CORE::RefPtr<CORE::IObject> overlayObject=NULL;
        {
            CORE::IObjectFactory *objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
            if(!objectFactory)
            {
                throw UTIL::ReturnValueNullException("Object factory is NULL", __FILE__, __LINE__);
            }
        
            overlayObject = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);
        }

        return overlayObject.release();
    }
}
