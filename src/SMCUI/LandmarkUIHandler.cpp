/*****************************************************************************
*
* File             : LandmarkUIHandler.cpp
* Description      : LandmarkUIHandler class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

//Header files
#include <SMCUI/LandmarkUIHandler.h>
#include <Core/IPoint.h>
#include <Core/Point.h>
#include <VizUI/IModelPickUIHandler.h>
#include <App/AccessElementUtils.h>
#include <VizUI/IMouseMessage.h>
#include <Core/IMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <Core/IWorld.h>
#include <Util/CoordinateConversionUtils.h>
#include <Core/CoreRegistry.h>
#include <Elements/ElementsPlugin.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <App/IApplication.h>
#include <Core/ITerrain.h>
#include <Core/AttributeTypes.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataRecord.h>
#include <Core/IFeatureObject.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/ICompositeObject.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIcon.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITemporary.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <SMCUI/SMCUIPlugin.h>
#include <App/IUndoTransaction.h>
#include <Transaction/TransactionPlugin.h>
#include <Transaction/IMovePointTransaction.h>
#include <Transaction/ICompositeAddObjectTransaction.h>

#include <Core/IObjectMessage.h>
#include <SMCElements/ILayerComponent.h>
#include <Elements/IModelFeatureObject.h>
#include <Elements/OverlayObject.h>
#include <Core/INamedAttributeMessage.h>
#include <SMCUI/IDraggerUIHandler.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>


namespace SMCUI 
{

    DEFINE_META_BASE(SMCUI, LandmarkUIHandler);
    DEFINE_IREFERENCED(LandmarkUIHandler, UIHandler);

    LandmarkUIHandler::LandmarkUIHandler()
        :_offset(0.0)
        ,_mode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_NONE)
        ,_temporary(false)
    {
        _reset();
        _addInterface(SMCUI::ILandmarkUIHandler::getInterfaceName());
    }

    LandmarkUIHandler::~LandmarkUIHandler(){}


    CORE::IFeatureLayer*
        LandmarkUIHandler::getOrCreateLandmarkLayer(const std::string &layerName)
    {
        static const std::string modelLayerType = "Model";
        _modelName = layerName;

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();
        if(!worldMaintainer.valid())
        {
            return NULL;
        }
        CORE::RefPtr<CORE::IComponent> component =
            worldMaintainer->getComponentByName("LayerComponent");
        if(!component.valid())
        {
            return NULL;
        }
        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
            component->getInterface<SMCElements::ILayerComponent>();
        CORE::RefPtr<CORE::IFeatureLayer> layer = 
            layerComponent->getFeatureLayer(layerName);

        if(!layer.valid())
        {
            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecisions;

            getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecisions);

            layer = layerComponent->getOrCreateFeatureLayer(layerName, modelLayerType, attrNames, attrTypes, attrWidths, attrPrecisions);

        }
        return layer.get();
    }

    void LandmarkUIHandler::getDefaultLayerAttributes(std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions) const
    {
        attrNames.push_back("NAME");
        attrTypes.push_back("Text");
        attrWidths.push_back(128);
        attrPrecisions.push_back(1);

        attrNames.push_back("Type");
        attrTypes.push_back("Text");
        attrWidths.push_back(128);
        attrPrecisions.push_back(1);

        attrNames.push_back("Representation");
        attrTypes.push_back("Integer");
        attrWidths.push_back(1);
        attrPrecisions.push_back(1);

        //attrNames.push_back("RotationX");
        //attrTypes.push_back("Decimal");
        //attrWidths.push_back(1);
        //attrPrecisions.push_back(2);

        //attrNames.push_back("RotationY");
        //attrTypes.push_back("Decimal");
        //attrWidths.push_back(1);
        //attrPrecisions.push_back(2);

        //attrNames.push_back("RotationZ");
        //attrTypes.push_back("Decimal");
        //attrWidths.push_back(1);
        //attrPrecisions.push_back(2);

        attrNames.push_back("OrientationX");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecisions.push_back(2);

        attrNames.push_back("OrientationY");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecisions.push_back(2);

        attrNames.push_back("OrientationZ");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecisions.push_back(2);

        attrNames.push_back("OrientationW");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecisions.push_back(2);

        attrNames.push_back("ScaleX");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecisions.push_back(2);

        attrNames.push_back("ScaleY");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecisions.push_back(2);

        attrNames.push_back("ScaleZ");
        attrTypes.push_back("Decimal");
        attrWidths.push_back(1);
        attrPrecisions.push_back(2);

        attrNames.push_back("ModelFilename");
        attrTypes.push_back("Text");
        attrWidths.push_back(128);
        attrPrecisions.push_back(1);

        //attrNames.push_back("Category");
        //attrTypes.push_back("Text");
        //attrWidths.push_back(128);
        //attrPrecisions.push_back(1);
    }

    void LandmarkUIHandler::setLandmarkName(std::string modelName)
    {
        _modelName = modelName;
    }

    void LandmarkUIHandler::setLandmarkFilename(std::string fileName)
    {
        _fileName= fileName;
    }

    std::string LandmarkUIHandler::getLandmarkFilename() const    
    {
        return _fileName;
    }

    std::string LandmarkUIHandler::getLandmarkName() const    
    {
        return _modelName;
    }

    ELEMENTS::IModelFeatureObject* LandmarkUIHandler::getModelFeature() const
    {
        return _modelFeatureObject.get();
    }

    void LandmarkUIHandler::setProcessMouseEvents(bool flag)
    {
        _processMouseEvents = flag;
    }

    bool LandmarkUIHandler::getProcessMouseEvents() const
    {
        return _processMouseEvents;
    }

    void LandmarkUIHandler::setFocus(bool value)
    {
        if(!value)
        {
            _reset();
        }

        VizUI::UIHandler::setFocus(value);        
    }

    //! Function called when added to UI Manager
    void LandmarkUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void LandmarkUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            if(wm.valid())
            {
                _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
            }
        }
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_NONE);
        }
        if(messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            // Query TerrainUIHandler interface
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());                
            if(tph.valid())
            {
                // Get the IMouseMessage interface
                CORE::RefPtr<VizUI::IMouseMessage> mmsg = message.getInterface<VizUI::IMouseMessage>();
                if((mmsg->getMouse()->getButtonMask() & VizUI::IMouse::LEFT_BUTTON) > 0)
                {
                    //_leftButtonPressed = true;
                }
            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            // Get the IMouseMessage interface
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

            if(!_handleButtonRelease)
            {
                _handleButtonRelease = true;
                if(mmsg->getMouse()->getButtonMask() & VizUI::IMouse::RIGHT_BUTTON)
                {
                    return;
                }
            }

            // Query TerrainUIHandler interface
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                const CORE::IntersectionList& ilist = tph->getCurrentIntersections();
                if(!ilist.empty())
                {
                    const osgUtil::LineSegmentIntersector::Intersection& osgIntersection = ilist.front()->osgIntersection;
                    _handleButtonReleasedIntersection(mmsg->getMouse()->getButtonMask(), osgIntersection);
                }
            }
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }


    void LandmarkUIHandler::_handleButtonReleasedIntersection
        (int button, const osgUtil::LineSegmentIntersector::Intersection& osgIntersection)
    {
        if(button & VizUI::IMouse::LEFT_BUTTON)
        {
            if (_mode == SMCUI::ILandmarkUIHandler::LANDMARK_MODE_CREATE)
            {
                createLandmark();
                if(_modelFeatureObject.valid())
                {
                    osg::Vec3 currentLatLongAlt = 
                        UTIL::CoordinateConversionUtils::ECEFToGeodetic(osgIntersection.getWorldIntersectPoint());
                    osg::Vec3 p = osg::Vec3d(currentLatLongAlt.y(), currentLatLongAlt.x(), currentLatLongAlt.z());
                    _modelFeatureObject->getInterface<CORE::IPoint>()->setValue(p);

                    //addLandmarkToCurrentSelectedLayer();


                    // XXX - Notify all current observers about the intersections found message
                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILandmarkUIHandler::LandmarkCreatedMessageType);
                    notifyObservers(*ILandmarkUIHandler::LandmarkCreatedMessageType, *msg);

                }

            }
         
        }
        else if(button & VizUI::IMouse::RIGHT_BUTTON)
        {
            if(_mode == SMCUI::ILandmarkUIHandler::LANDMARK_MODE_EDIT)
            {
                CORE::RefPtr<SMCUI::IDraggerUIHandler> draggerUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IDraggerUIHandler>(getManager());
                draggerUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                if(_modelFeatureObject.valid())
                {
                    _modelFeatureObject->getInterface<CORE::ISelectable>()->setSelected(false);
                    _reset();
                }
                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

                if(component.valid())
                {
                    CORE::RefPtr<CORE::ISelectionComponent> selectionComponent =
                        component->getInterface<CORE::ISelectionComponent>();
                    selectionComponent->clearCurrentSelection();
                }

                setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_NONE);

            }
        }
    }

    CORE::RefPtr<CORE::IObject> LandmarkUIHandler::createLandmark(bool addToWorld)
    {

        CORE::RefPtr<CORE::IObject> modelObject =CORE::CoreRegistry::instance()->
            getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::ModelFeatureObjectType);

        if(!modelObject.valid())
        {
            return NULL;
        }

        modelObject->getOSGNode()->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
        // XXX - hard coded default icon
        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
                component->getInterface<ELEMENTS::IIconLoader>();
            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = 
                modelObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconLoader.valid() && iconHolder.valid())
            {
                iconHolder->setIcon(iconLoader->getIconByName("circle"));
            }
        }

        _modelFeatureObject = modelObject->getInterface<ELEMENTS::IModelFeatureObject>();
        if(_modelFeatureObject)
        {
            CORE::RefPtr<CORE::IBase> modelBase = _modelFeatureObject->getInterface<CORE::IBase>();
            if(modelBase)
            {
                modelBase->setName(_modelName);                
            }

            //_fileName = "data/LandmarkSymbols/Misc/GovernmentBuilding/GovernmentBuilding.ive";

            _modelFeatureObject->setModelFilename(_fileName);
        }

        CORE::RefPtr<ELEMENTS::IModelHolder> model =
            _modelFeatureObject->getInterface<ELEMENTS::IModelHolder>();
        //        model
        if(model)
        {
            model->setModelScale(osg::Vec3(1, 1, 1));
        }

        CORE::RefPtr<ELEMENTS::IRepresentation> represent =
            _modelFeatureObject->getInterface<ELEMENTS::IRepresentation>();
        if(represent)
        {
            if(_fileName.empty())
            {
                represent->setRepresentationMode(ELEMENTS::IRepresentation::AUTO);
            }
            else
            {
                represent->setRepresentationMode(ELEMENTS::IRepresentation::MODEL);
            }
        }
        //! set temporary state
        CORE::RefPtr<CORE::ITemporary> temp = modelObject->getInterface<CORE::ITemporary>();

        if(temp.valid())
        {
            temp->setTemporary(true);
        }

        if ( addToWorld )
        {
            // Add the model to the world
            getWorldInstance()->addObject(modelObject.get());
        }

        return modelObject;
    }

    void LandmarkUIHandler::_reset()
    {
        _modelFeatureObject = NULL;
        _processMouseEvents = false;
    }

    void LandmarkUIHandler::setMode(ILandmarkUIHandler::LandmarkMode mode)
    {
        _mode = mode;
        _handleButtonRelease = false;

        if(_mode == LANDMARK_MODE_EDIT)
        {
            setSelectedLandmarkAsCurrent();
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                //                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                //                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);  
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);  

                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ILandmarkUIHandler::LandmarkSelectedMessageType);
                notifyObservers(*ILandmarkUIHandler::LandmarkSelectedMessageType, *msg);
            }

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(true);
            }
        }
        else if(_mode == LANDMARK_MODE_CREATE)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
                //                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(false);
            }
        }
        else if(_mode == LANDMARK_MODE_NONE)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
                //                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDoubleClickedMessageType);
                //                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseDraggedMessageType);
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);

            }

            CORE::RefPtr<SMCUI::IDraggerUIHandler> draggerUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IDraggerUIHandler>(getManager());
            if(draggerUIHandler.valid())
            {
                draggerUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            }

            _modelFeatureObject = NULL;
            _modelName = "";
            _fileName = "";

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(true);
            }
        }
    }

    ILandmarkUIHandler::LandmarkMode LandmarkUIHandler::getMode()
    {
        return _mode;
    }

    void LandmarkUIHandler::setSelectedLandmarkAsCurrent()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            _modelFeatureObject = iter->second->getInterface<ELEMENTS::IModelFeatureObject>();
        }
    }

    void LandmarkUIHandler::setMetadataRecord(CORE::IMetadataRecord* record)
    {
        if(_modelFeatureObject.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _modelFeatureObject->getInterface<CORE::IMetadataRecordHolder>();
            if(holder.valid())
            {
                return holder->setMetadataRecord(record);
            }
        }
    }

    CORE::RefPtr<CORE::IMetadataRecord>
        LandmarkUIHandler::getMetadataRecord() const
    {
        if(_modelFeatureObject.valid())
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _modelFeatureObject->getInterface<CORE::IMetadataRecordHolder>();
            if(holder.valid())
            {
                return holder->getMetadataRecord();
            }
        }
        return NULL;
    }

    void LandmarkUIHandler::_update()
    {
        if(_modelFeatureObject.valid())
        {
            CORE::RefPtr<CORE::IFeatureObject> feature = 
                _modelFeatureObject->getInterface<CORE::IFeatureObject>();
            if(feature.valid())
            {
                return feature->update();
            }
        }
    }

    void LandmarkUIHandler::addToLayer(CORE::IFeatureLayer* featureLayer)
    {
        _addToLayer(featureLayer);
    }

    void LandmarkUIHandler::_addToLayer(CORE::IFeatureLayer* layer)
    {
        if(!layer)
            return;

        if(layer->getFeatureLayerType()!= CORE::IFeatureLayer::MODEL)
            return;

        if(!_modelFeatureObject.valid())
            return;

        try
        {
            CORE::RefPtr<CORE::ICompositeObject> composite = 
                layer->getInterface<CORE::ICompositeObject>();

            CORE::RefPtr<CORE::IBase> pointObject = 
                _modelFeatureObject->getInterface<CORE::IBase>();
            getWorldInstance()->removeObjectByID(&pointObject->getUniqueID());

            CORE::RefPtr<CORE::ITemporary> temp = 
                pointObject->getInterface<CORE::ITemporary>();

            if(temp.valid())
            {
                temp->setTemporary(false);
            }

            // create a transaction to add the model to the layer
            APP::IUndoTransactionFactory* transactionFactory = 
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction = transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::CompositeAddObjectTransactionType);

            CORE::RefPtr<TRANSACTION::ICompositeAddObjectTransaction> compositeAddObjectTransaction = 
                transaction->getInterface<TRANSACTION::ICompositeAddObjectTransaction>();

            compositeAddObjectTransaction->setCompositeObject(composite.get());
            compositeAddObjectTransaction->setObject(_modelFeatureObject->getInterface<CORE::IObject>());

            // execute the transaction 
            addAndExecuteUndoTransaction(transaction.get());
        }
        catch(UTIL::Exception &e)
        {
            e.LogException();
        }
    }

    void LandmarkUIHandler::addLandmarkToCurrentSelectedLayer()
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateLandmarkLayer(_modelName);
        _addToLayer(layer);
    }

    CORE::IFeatureLayer* LandmarkUIHandler::_getSelectedLayer()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        if(selectionUIHandler.valid())
        {
            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                CORE::IFeatureLayer* layer = iter->second->getInterface<CORE::IFeatureLayer>();
                return layer;
            }
        }

        return NULL;
    }

    CORE::RefPtr<CORE::IMetadataTableDefn>
        LandmarkUIHandler::getCurrentSelectedLayerDefn()
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreateLandmarkLayer(_modelName);
        if(layer.valid())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                return holder->getMetadataTable()->getMetadataTableDefn();
            }
        }

        return NULL;
    }

    void LandmarkUIHandler::removeCreatedLandmark()
    {
        if(_modelFeatureObject.valid())
        {
            getWorldInstance()->removeObjectByID(&(_modelFeatureObject->getInterface<CORE::IBase>()->getUniqueID()));
            _modelFeatureObject = NULL;
        }
    }

    void LandmarkUIHandler::setTemporaryState(bool value)
    {
        _temporary = value;
    }

    bool LandmarkUIHandler::getTemporaryState() const
    {
        return _temporary;
    }

} // namespace SMCUI

