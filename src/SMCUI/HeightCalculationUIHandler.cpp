/*****************************************************************************
 *
 * File             : HeightCalculationUIHandler.cpp
 * Description      : HeightCalculationUIHandler class definition
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, CAIR, DRDO
 *****************************************************************************/

#include <SMCUI/HeightCalculationUIHandler.h>
#include <App/IApplication.h>
#include <Core/CoreRegistry.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <App/AccessElementUtils.h>
#include <Core/ITerrain.h>
#include <Util/BaseExceptions.h>
#include <Elements/ObjectDragger.h>
#include <Elements/ElementsPlugin.h>
#include <Core/ITemporary.h>
#include <Core/AttributeTypes.h>
#include <Elements/ElementsUtils.h>
#include <osgUtil/CullVisitor>
#include <osg/Node>

#include <iostream>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI,HeightCalculationUIHandler);
    DEFINE_IREFERENCED(HeightCalculationUIHandler, VizUI::UIHandler);

    HeightCalculationUIHandler::HeightCalculationUIHandler()
        : _selection(new osg::MatrixTransform)
        , _pointDragger(new osgManipulator::Translate2DDragger)
        , _scale(1000)
        ,_geoPos(0,0,0)
    {
        _addInterface(SMCUI::IHeightCalculationUIHandler::getInterfaceName());
        setName(getClassname());

         _pointDragger->setupDefaultGeometry();
         _selection->setNodeMask(0);
         _pointDragger->addTransformUpdating(_selection.get());
         // we want the dragger to handle it's own events automatically
        _pointDragger->setHandleEvents(true);
        _pointDragger->setNodeMask(0);
        _pointUpdateCallback = new PointUpdateCallback(this);
        _pointDragger->addDraggerCallback(_pointUpdateCallback.get());
        //_updateCallback = new UpdateCallback;
        //_updateCallback->dirty = false;
        //_updateCallback->_draggerSize = 150.0f;
        //_pointDragger->setCullCallback(_updateCallback.get());

        _heightDragger = new heightManipulator;
        _heightDragger->setDragger(_pointDragger.get());
    }

    HeightCalculationUIHandler::~HeightCalculationUIHandler()
    {}

    void 
    HeightCalculationUIHandler::setPosition(const osg::Vec3d& position) 
    {
        // XXX - Remove this constant and add as an attribute
        osg::Vec3d posECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(position.y(), position.x(), position.z()));
        osg::Vec3d localNormal(posECEF);
        _pointDragger->setMatrix(osg::Matrix::rotate(osg::Vec3d(0, 0, 1), localNormal) * osg::Matrix::scale(_scale, _scale, _scale) *
                                    osg::Matrix::translate(posECEF));
        _geoPos = osg::Vec3d(position.x(), position.y(), position.z());
    }

    const osg::Vec3d&
    HeightCalculationUIHandler::getPosition() const 
    {
        return _geoPos;
    }

    void HeightCalculationUIHandler::sendDraggerPositionUpdatedMessage()
    {
        // Send position update message
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IHeightCalculationUIHandler::DraggerUpdated);
        notifyObservers(*IHeightCalculationUIHandler::DraggerUpdated, *msg);
    }

    void HeightCalculationUIHandler::updateDraggerPosition()
    {
        osg::NodePath nodePathToRoot;
        osgManipulator::computeNodePathToRoot(*_pointDragger, nodePathToRoot);
        osg::Matrix localToWorld = osg::computeLocalToWorld(nodePathToRoot);
        osg::Vec3d translate = localToWorld.getTrans();
        osg::Vec3d result = UTIL::CoordinateConversionUtils::ECEFToGeodetic(translate);
         _geoPos = osg::Vec3d(result.y(), result.x(), result.z());

        // Prevent dragger from going below the terrain
        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IUIHandlerManager>(getManager());
        CORE::ITerrain* terrain = world->getTerrain();
        double elevation;
        if(terrain && terrain->getElevationAt(osg::Vec2d(_geoPos.x(), _geoPos.y()), elevation))
        {
            _geoPos.z() = (_geoPos.z() < elevation) ? elevation : _geoPos.z();
        }
        setPosition(_geoPos);
    }

    void 
    HeightCalculationUIHandler::setFocus(bool value)
    {
        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IUIHandlerManager>(getManager());

        if(value)
        {
            //world->getOSGGroup()->addChild(_pointDragger.get());
            world->getOSGGroup()->addChild(_heightDragger.get());
        }
        else
        {
            //world->getOSGGroup()->removeChild(_pointDragger.get());
            world->getOSGGroup()->removeChild(_heightDragger.get());
        }

        _pointDragger->setMatrix(osg::Matrix::translate(osg::Vec3d(0, 0, 0)));
        _pointDragger->setNodeMask(value);
    }

    void 
    HeightCalculationUIHandler::setDragStage(IHeightCalculationUIHandler::DragStage dsArg)
    {
        _dragStage = dsArg;
    }

    IHeightCalculationUIHandler::DragStage 
    HeightCalculationUIHandler::getDragStage() const
    {
        return _dragStage;
    }

    void 
    HeightCalculationUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "HeightCalculationUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("Scale", "Scale",
                                CORE::DoubleAttribute::SetFuncType(this, &HeightCalculationUIHandler::setScale),
                                CORE::DoubleAttribute::GetFuncType(this, &HeightCalculationUIHandler::getScale),
                                "Dragger scale",
                                groupName));
    }

    void 
    HeightCalculationUIHandler::setScale(double scale)
    {
        _scale = scale;
    }

    double
    HeightCalculationUIHandler::getScale() const
    {
        return _scale;
    }

} // namespace SMCUI

