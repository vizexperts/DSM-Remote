#include <SMCUI/VectorLayerQueryUIHandler.h>

#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>
#include <Core/ICompositeObject.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <Database/DatabaseMaintainer.h>
#include <Elements/ElementsUtils.h>
#include <Elements/ElementsPlugin.h>
#include <DB/ReaderWriter.h>
#include <DB/ReadFile.h>
#include<Util/ShpFileUtils.h>
#include <Util/ThreadPool.h>
#include <OpenThreads/Mutex>
namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, VectorLayerQueryUIHandler);
    DEFINE_IREFERENCED(VectorLayerQueryUIHandler, VizUI::UIHandler);


    VectorLayerQueryUIHandler::VectorLayerQueryUIHandler()
        : _pathOfShapeFile("")
        , _ogrDataSource(NULL)
        , _resultLayer(NULL)
        , _shapeFileStoragePath("/VectorQueryData/ShapeFileStorage/")
        , _vectorQueryOutputFolder(NULL)
        , _extents(osg::Vec4d(0, 0, 0, 0))
        , _vectorQueryOutputName("")
        , _queryOutputName("")
    {

        _addInterface(SMCUI::IVectorLayerQueryUIHandler::getInterfaceName());
    }

    VectorLayerQueryUIHandler::~VectorLayerQueryUIHandler()
    {
    }

    void VectorLayerQueryUIHandler::setShapeFilePathName(const std::string& pathOfShapeFileName)
    {
        _pathOfShapeFile = pathOfShapeFileName;
    }
    void VectorLayerQueryUIHandler::setVectorQueryOutputName(const std::string& outputName)
    {
        _vectorQueryOutputName = outputName;
    }
    void VectorLayerQueryUIHandler::setLayerName(const std::string& layerName)
    {
        _queryOutputName = layerName;
    }
    std::string VectorLayerQueryUIHandler::getOGRLayerName()
    {
        if (getOGRDataSource())
        {
            _layerName = getOGRDataSource()->GetLayer(0)->GetName();
        }
        return _layerName;
    }
    std::string VectorLayerQueryUIHandler::getShapeFilePathName() const
    {
        return _pathOfShapeFile;
    }
    OGRDataSource*
        VectorLayerQueryUIHandler::getOGRDataSource()
    {
        if (_ogrDataSource == NULL)
        {
            _ogrDataSource = OGRSFDriverRegistrar::Open(getShapeFilePathName().c_str());
        }
        return _ogrDataSource;
    }
    void VectorLayerQueryUIHandler::setStyleSheet(const std::string& styleSheet)
    {
        _styleSheetName = styleSheet;
    }
    void VectorLayerQueryUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {

    }

    std::vector<std::string> VectorLayerQueryUIHandler::getFeatureAtribute(std::string url)
    {

        std::vector<std::string> attributeDefinations;
        //parsing file through ogr 
        _ogrDataSource = OGRSFDriverRegistrar::Open(url.c_str());

        //accessing only first layer 
        int firstLayer = 0;
        if (_ogrDataSource && _ogrDataSource->GetLayerCount() > 0)
        {
            //get OGR layer 
            _resultLayer = _ogrDataSource->GetLayer(firstLayer);
            OGRFeatureDefn* ogrFeatureDefn;

            //get OGR Feature defination if layer if valid 
            if (_resultLayer)
            {
                ogrFeatureDefn = _resultLayer->GetLayerDefn();
                if (ogrFeatureDefn)
                {
                    for (int i = 0; i < ogrFeatureDefn->GetFieldCount(); i++)
                    {
                        OGRFieldDefn* ogrFieldDefn = ogrFeatureDefn->GetFieldDefn(i);

                        //Can also return type - ignoring for now 
                        //OGRFieldType ogrFieldType = ogrFieldDefn->GetType();

                        attributeDefinations.push_back(ogrFieldDefn->GetNameRef());

                    }

                }
            }
        }
        _resultLayer = NULL;

        OGRDataSource::DestroyDataSource(_ogrDataSource);
        _ogrDataSource = NULL;

        return attributeDefinations;
    }

    OGRLayer*
        VectorLayerQueryUIHandler::executeQueryStatement(bool runSpatialQuery)
    {
        if (getOGRDataSource())
        {
            if (!_sqlQuery.empty())
            {
                if (runSpatialQuery)
                {
                    OGRPolygon* ogrPolygon = (OGRPolygon*)OGRGeometryFactory::createGeometry(wkbPolygon);
                    OGRLinearRing* ring = (OGRLinearRing*)OGRGeometryFactory::createGeometry(wkbLinearRing);
                    ogrPolygon->addRingDirectly(ring);
                    ring->addPoint(_extents.x(), _extents.y(), 0);
                    ring->addPoint(_extents.x(), _extents.w(), 0);
                    ring->addPoint(_extents.z(), _extents.w(), 0);
                    ring->addPoint(_extents.z(), _extents.y(), 0);

                    ring->closeRings();

                    _resultLayer = _ogrDataSource->ExecuteSQL(_sqlQuery.c_str(), ogrPolygon, "");
                }
                else
                {
                    _resultLayer = _ogrDataSource->ExecuteSQL(_sqlQuery.c_str(), NULL, "");
                }
            }
        }
        return _resultLayer;
    }
    bool VectorLayerQueryUIHandler::execute(bool runSpatialQuery)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();
        if (appdataPath.empty())
        {
            return false;
        }
        appdataPath = appdataPath + _shapeFileStoragePath;
        std::string path = osgDB::convertFileNameToNativeStyle(appdataPath);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }
        /*QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");*/
        CORE::RefPtr<CORE::UniqueID> uniqueID = CORE::UniqueID::CreateUniqueID();
        std::string ShpFileName = getOGRLayerName() + uniqueID->toString() + ".shp";
        std::string destShapeFilePath = path + ShpFileName;
        OGRLayer* resultLayer = executeQueryStatement(runSpatialQuery);
        if (resultLayer)
        {
            bool isSHPFileGenerated = UTIL::ShpFileUtils::writeOGRtoShpFile(destShapeFilePath, resultLayer);
            if (isSHPFileGenerated)
            {
                std::string fileLocation = osgDB::convertFileNameToNativeStyle(destShapeFilePath);
                CORE::RefPtr<DB::ReaderWriter::Options>  options = new DB::ReaderWriter::Options;
                if (!_styleSheetName.empty())
                options->setMapValue("StyleSheet", _styleSheetName);
                _mtAddVectorData(_queryOutputName, fileLocation, options);
                return true;

            }

        }
        return false;
    }
    void
        VectorLayerQueryUIHandler::_mtAddVectorData(const std::string &layerName, const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options)
    {
        try
        {
            options->setMapValue("Symbology", "1");
            CORE::RefPtr<CORE::IObject> object = DB::readFeature(url, options.get());
            if (!object.valid())
            {
                std::string errstring = std::string("Error loading file. Check whether the file is valid or not.");
                throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);

            }

            object->getInterface<CORE::IBase>(true)->setName(layerName);

            CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
            _subscribe(wm, *CORE::IWorldMaintainer::TickMessageType);

            if (object.valid())
            {

                CORE::RefPtr<CORE::IWorld> world = ELEMENTS::GetFirstWorldFromMaintainer();
                if (world.valid())
                {
                    _queryResultOutputFolder();
                    if (_vectorQueryOutputFolder.valid())
                    {
                        _vectorQueryOutputFolder->getInterface<CORE::ICompositeObject>()->addObject(object);
                    }
                }
                OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            }
            return;
        }
        catch (UTIL::Exception& e)
        {
            e.LogException();

        }
        catch (...)
        {

        }

    }
    void VectorLayerQueryUIHandler::_queryResultOutputFolder()
    {
        //! check whether search Bookmark already added in element list or not 
        if (!_vectorQueryOutputFolder.valid())
        {
            CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
            _vectorQueryOutputFolder = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FolderType);
            _vectorQueryOutputFolder->getInterface<CORE::IBase>()->setName(_vectorQueryOutputName);
            CORE::WorldMaintainer* worldMaintainer = CORE::WorldMaintainer::instance();
            typedef std::map<const CORE::UniqueID*, CORE::RefPtr<CORE::IWorld> > WorldMap;
            WorldMap worldmap = worldMaintainer->getWorldMap();
            WorldMap::iterator iter = worldmap.begin();
            iter->second.get()->addObject(_vectorQueryOutputFolder.get());
        }
    }
    void
        VectorLayerQueryUIHandler::setSQLQuery(const std::string& query)
    {
        _sqlQuery = query;
    }

    void
        VectorLayerQueryUIHandler::reset()
    {
        if (_ogrDataSource)
        {
            if (_resultLayer)
            {
                _ogrDataSource->ReleaseResultSet(_resultLayer);
                _resultLayer = NULL;
            }
            OGRDataSource::DestroyDataSource(_ogrDataSource);
            _ogrDataSource = NULL;
        }
        _sqlQuery = "";
        _styleSheetName = "";
        _layerName = "";
        _pathOfShapeFile="";
        _queryOutputName = "";
        _extents=osg::Vec4d(0, 0, 0, 0);
       
    }
    void
        VectorLayerQueryUIHandler::resetFolderObject()
    {
        _vectorQueryOutputFolder = NULL;
        _vectorQueryOutputName = "";
    }

    OGRLayer*
        VectorLayerQueryUIHandler::getOGRLayer()
    {
        return _resultLayer;
    }

    void
        VectorLayerQueryUIHandler::setExtents(const osg::Vec4d& extents)
    {
        _extents = extents;
    }

}