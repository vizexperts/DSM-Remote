/*****************************************************************************
*
* File             : PointUIHandler2.cpp
* Description      : PointUIHandler2 class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

//Header files
#include <SMCUI/PointUIHandler2.h>

#include <Core/IPoint.h>
#include <Core/IMessage.h>
#include <Core/IWorld.h>
#include <Core/CoreRegistry.h>
#include <Core/InterfaceUtils.h>
#include <Core/ITerrain.h>
#include <Core/AttributeTypes.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataRecord.h>
#include <Core/IFeatureObject.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/ICompositeObject.h>
#include <Core/ITemporary.h>
#include <Core/WorldMaintainer.h>
#include <Core/Point.h>
#include <Core/IBase.h>
#include <Core/IWorldMaintainer.h>
#include <Core/IText.h>

#include <App/IApplication.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransaction.h>
#include <App/AccessElementUtils.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/IModelPickUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>
#include <VizUI/VizUIPlugin.h>
#include <VizUI/IMouse.h>

#include <Util/CoordinateConversionUtils.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIcon.h>
#include <Elements/IDrawTechnique.h>
#include <Elements/IRepresentation.h>
#include <Elements/IClamper.h>

#include <Transaction/TransactionPlugin.h>
#include <Transaction/IMovePointTransaction.h>
#include <Transaction/IDeleteObjectTransaction.h>
#include <Transaction/ICompositeAddObjectTransaction.h>

#include <SMCElements/LayerComponent.h>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <Util/StyleFileUtil.h>
#include <Elements/FeatureObject.h>
#include <serialization/StyleSheetParser.h>

namespace SMCUI 
{

    DEFINE_META_BASE(SMCUI, PointUIHandler2);
    DEFINE_IREFERENCED(PointUIHandler2, UIHandler);

    PointUIHandler2::PointUIHandler2()
        :_offset(0.0f)
        ,_mode(SMCUI::IPointUIHandler2::POINT_MODE_NONE)
        ,_temporary(true)
        ,_handleButtonRelease(false)
        ,_clampingFromConfig(false)
        ,_offsetFromConfig(false)
    {
        _addInterface(SMCUI::IPointUIHandler2::getInterfaceName());
    }

    PointUIHandler2::~PointUIHandler2(){}

    void PointUIHandler2::setPoint(CORE::IPoint* Point)
    {
        _point = Point;
    }

    CORE::IPoint* PointUIHandler2::getPoint() const    
    {
        return _point.get();
    }

    void PointUIHandler2::setPointEnable(const bool val){
        _pointEnable = val;
    }

    bool PointUIHandler2::getPointEnable() const{
        return _pointEnable;
    }

    void PointUIHandler2::setFocus(bool value)
    {
        if(!value)
        {
            reset();
        }
        VizUI::UIHandler::setFocus(value);

    }

    //! Function called when added to UI Manager
    void PointUIHandler2::onAddedToManager()
    {
        reset();
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void PointUIHandler2::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
            if(wm.valid())
            {
                _subscribe(wm.get(), *CORE::IWorld::WorldLoadedMessageType);
            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            _performHardReset();
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMouseReleasedMessageType)
        {
            // Get the IMouseMessage interface
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

            if(!_handleButtonRelease)
            {
                _handleButtonRelease = true;

                // create point should work only for left mouse click
                if(mmsg->getMouse()->getButtonMask() & (VizUI::IMouse::RIGHT_BUTTON))
                {
                    return;
                }
            }

            CORE::RefPtr<VizUI::IModelPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                osg::Vec3d pos;
                if (tph->getMousePickedPosition(pos, true, false))
                {
                    if((_mode == POINT_MODE_EDIT_POINT) && (mmsg->getMouse()->getButtonMask() & VizUI::IMouse::LEFT_BUTTON))
                    {
                        if(_point.valid())
                        {
                            _point->setValue(pos);

                            // XXX - Notify all current observers about the intersections found message
                            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IPointUIHandler2::PointUpdatedMessageType);

                            notifyObservers(*IPointUIHandler2::PointUpdatedMessageType, *msg);
                        }
                    }

                    _handleButtonReleasedIntersection(mmsg->getMouse()->getButtonMask(), pos);
                }
            }
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }

    void PointUIHandler2::_performHardReset()
    {
        reset();
    }

    void PointUIHandler2::_createUndoMoveTransaction(const osg::Vec3d& longLatAlt)
    {
        APP::IUndoTransactionFactory* transactionFactory = 
            APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

        CORE::RefPtr<APP::IUndoTransaction> transaction = 
            transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::MovePointTransactionType);

        CORE::RefPtr<TRANSACTION::IMovePointTransaction> movePointTransaction = 
            transaction->getInterface<TRANSACTION::IMovePointTransaction>();

        movePointTransaction->setPoint(_point.get());
        movePointTransaction->setPosition(longLatAlt);

        // execute the transaction 
        addAndExecuteUndoTransaction(transaction.get());
    }

    void PointUIHandler2::_handleButtonReleasedIntersection(int button, const osg::Vec3d& longLatAlt)
    {
        if((button & VizUI::IMouse::LEFT_BUTTON))
        {
            if (_mode == SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT)
            {
                create(true);
                if(_point.valid())
                {
                    // Add the point to the line
                    _point->setValue(longLatAlt);

                    // XXX - Notify all current observers about the intersections found message
                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IPointUIHandler2::PointCreatedMessageType);
                    notifyObservers(*IPointUIHandler2::PointCreatedMessageType, *msg);
                }
            } 
            else if(_mode == POINT_MODE_EDIT_POINT)
            {
                if(_point.valid())
                {
                    _createUndoMoveTransaction(longLatAlt);
                    //_point->setValue(pos);

                    // XXX - Notify all current observers about the intersections found message
                    CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                    CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IPointUIHandler2::PointUpdatedMessageType);

                    notifyObservers(*IPointUIHandler2::PointUpdatedMessageType, *msg);
                }
            }
        }
        else if(button & VizUI::IMouse::RIGHT_BUTTON)
        {
            if(_mode ==  SMCUI::IPointUIHandler2::POINT_MODE_EDIT_POINT ||
                _mode == SMCUI::IPointUIHandler2::POINT_MODE_EDIT_ATTRIBUTES)
            {
                if(_point.valid())
                {
                    if(_point->getInterface<CORE::ISelectable>()->getSelected())
                    {
                        _point->getInterface<CORE::ISelectable>()->setSelected(false);
                    }

                    reset();
                }

                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

                if(component.valid())
                {
                    CORE::RefPtr<CORE::ISelectionComponent> selectionComponent = component->getInterface<CORE::ISelectionComponent>();
                    selectionComponent->clearCurrentSelection();
                }
            }
        }
    }

    void PointUIHandler2::create(bool addToWorld)
    {
        CORE::RefPtr<CORE::IObject> pointObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject
            (*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

        //XXX - hard coded icon 
        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader =
                component->getInterface<ELEMENTS::IIconLoader>();

            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder =
                pointObject->getInterface<ELEMENTS::IIconHolder>();

            if (getPointEnable()){
                if (iconHolder.valid() && iconLoader.valid()){
                    iconHolder->setIcon(iconLoader->getIconByName("circle"));
                }
            }
            else{
                if (iconHolder.valid() && iconLoader.valid()){
                    iconHolder->setIcon(iconLoader->getIconByName("circle-filled"));
                }
                CORE::RefPtr<CORE::IText> text = pointObject->getInterface<CORE::IText>();
                if (text){
                    text->setTextMode(CORE::IText::TextMode::CENTER);
                    text->setTextOffset(osg::Vec2s(-2.0f, 4.0f));
                }
            }
        }

        //set the representation to icon mode
        CORE::RefPtr<ELEMENTS::IRepresentation> representation = 
            pointObject->getInterface<ELEMENTS::IRepresentation>();

        if(representation.valid())
        {
            //if (getPointEnable())
            representation->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
            /*else
                representation->setRepresentationMode(ELEMENTS::IRepresentation::TEXT);*/
        }


        //set the current temporary state
        CORE::RefPtr<CORE::ITemporary> temp = pointObject->getInterface<CORE::ITemporary>();
        if(temp.valid())
        {
            temp->setTemporary(_temporary);
        }

        CORE::RefPtr<ELEMENTS::IClamper> clamp = pointObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            //checking whether the clamping attribute is present in config or not
            if (!_clampingFromConfig)
            {
                //if it is not setting from config , then it will set from setting component
                _setDefaultClamping();
            }
            clamp->setClampOnPlacement(_clamping);
        }

        // Add the point to the world
        if (addToWorld)
            getWorldInstance()->addObject(pointObject.get());

        _point = pointObject->getInterface<CORE::IPoint>();
        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = _point->getInterface<ELEMENTS::IDrawTechnique>();
        if(draw.valid())
        {
            //checking whether the offset attribute is present in config or not
            if (!_offsetFromConfig)
            {
                //if it is not setting from config , then it will set from setting component
                _setDefaultOffset();
            }
            draw->setHeightOffset(_offset);
        }


    }

    void PointUIHandler2::reset()
    {
        setMode(POINT_MODE_NONE);

        _point = NULL;
        _temporary = false;
    }

    // setting default clamp state using setting component
    void PointUIHandler2::_setDefaultClamping()
    {
        
            CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
            APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());

            if(wmain.valid())
            {
                CORE::IComponent* component = wmain->getComponentByName("SettingComponent");
                    if(component)
                    {
                        _settingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
                        _clamping = _settingComponent->getDefaultClampingState();
                    }
            }   
    }

    // setting default clamp state using config
    void PointUIHandler2::setClampingStateFromConfig(bool clamp)
    {
    
        _clamping = clamp;
        _clampingFromConfig = true;
    }

    bool PointUIHandler2::getClampingStateFromConfig() const
    {
        return _clamping;
    }

    // setting default offset value using setting component
    void PointUIHandler2::_setDefaultOffset()
    {
        
            CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
            APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());

            if(wmain.valid())
            {
                CORE::IComponent* component = wmain->getComponentByName("SettingComponent");
                    if(component)
                    {
                        _settingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
                        _offset = _settingComponent->getHeightOffset();
                    }
            }   
    }

    // setting default offset value using config
    void PointUIHandler2::setOffsetFromConfig(double offset)
    {
        _offset = offset;
        _offsetFromConfig = true;
    }

    double PointUIHandler2::getOffsetFromConfig() const
    {
        return _offset;
    }

    void PointUIHandler2::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "PointUIHandler2 attributes";

        _addAttribute(new CORE::DoubleAttribute("Offset", "Offset",
            CORE::DoubleAttribute::SetFuncType(this, &PointUIHandler2::setOffsetFromConfig),
            CORE::DoubleAttribute::GetFuncType(this, &PointUIHandler2::getOffsetFromConfig),
            "Offset name",
            groupName));

          _addAttribute(new CORE::BooleanAttribute("Clamp", "Clamp",
            CORE::BooleanAttribute::SetFuncType(this, &PointUIHandler2::setClampingStateFromConfig),
            CORE::BooleanAttribute::GetFuncType(this, &PointUIHandler2::getClampingStateFromConfig),
            "Clamp",
            groupName));
    }

    //Function to set the current mode
    void PointUIHandler2::setMode(IPointUIHandler2::PointMode mode)
    {
        if(_mode == mode)
        {
            return;
        }

        _mode = mode;
        _handleButtonRelease = false;

        //if mode is create mode then disable the selection else enable selection on terrain
        if(_mode == POINT_MODE_CREATE_POINT )
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(false);
            }
        }
        else if(_mode == POINT_MODE_EDIT_ATTRIBUTES)
        {
            setSelectedPointAsCurrentPoint();
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);  
            }

        }
        else if(_mode == POINT_MODE_EDIT_POINT)
        {
            setSelectedPointAsCurrentPoint();
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = APP::AccessElementUtils::getUIHandlerUsingManager
                <VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }
        }
        else if(_mode == POINT_MODE_NONE)
        {
            CORE::RefPtr<VizUI::IModelPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IModelPickUIHandler>(getManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
            }

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(true);
            }
        }
    }

    IPointUIHandler2::PointMode PointUIHandler2::getMode()
    {
        return _mode;
    }

    void PointUIHandler2::setSelectedPointAsCurrentPoint()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            _point = iter->second->getInterface<CORE::IPoint>();
        }
    }


    void PointUIHandler2::setMetadataRecord(CORE::IMetadataRecord* record)
    {
        if(_point.valid())
        {

            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                _point->getInterface<CORE::IMetadataRecordHolder>();
            if(holder.valid())
            {
                holder->setMetadataRecord(record);
                return;
            }
        }
    }

    CORE::RefPtr<CORE::IMetadataRecord> 
        PointUIHandler2::getMetadataRecord() const
    {
        if(_point.valid())
        {
            try
            {
                CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                    _point->getInterface<CORE::IMetadataRecordHolder>(true);
                if(holder.valid())
                {
                    return holder->getMetadataRecord();
                }
            }
            catch(UTIL::Exception &e)
            {
                e.LogException();
            }
        }

        return NULL;
    }

    void 
        PointUIHandler2::update()
    {
        if(_point.valid())
        {
            CORE::RefPtr<CORE::IFeatureObject> feature = 
                _point->getInterface<CORE::IFeatureObject>();
            if(feature.valid())
            {
                return feature->update();
            }
        }
    }

    void PointUIHandler2::addToLayer(CORE::IFeatureLayer *featureLayer)
    {
        _addToLayer(featureLayer);
    }

    void PointUIHandler2::_addToLayer(CORE::IFeatureLayer *featureLayer)
    {
        if(!featureLayer)
            return;

        if(featureLayer->getFeatureLayerType()!=CORE::IFeatureLayer::POINT)
            return;

        if(!_point.valid())
            return;

        try
        {
            CORE::RefPtr<CORE::ICompositeObject> composite = 
                featureLayer->getInterface<CORE::ICompositeObject>(true);

            CORE::RefPtr<CORE::IBase> pointObject = _point->getInterface<CORE::IBase>(true);
            getWorldInstance()->removeObjectByID(&pointObject->getUniqueID());

            CORE::RefPtr<CORE::ITemporary> temp = _point->getInterface<CORE::ITemporary>();
            if(temp.valid())
            {
                temp->setTemporary(false);
            }

            // create a transaction to add the point to the layer
            APP::IUndoTransactionFactory* transactionFactory = 
                APP::ApplicationRegistry::instance()->getUndoTransactionFactory();

            CORE::RefPtr<APP::IUndoTransaction> transaction = 
                transactionFactory->createUndoTransaction(*TRANSACTION::TransactionRegistryPlugin::
                CompositeAddObjectTransactionType);

            CORE::RefPtr<TRANSACTION::ICompositeAddObjectTransaction> compositeAddObjectTransaction = 
                transaction->getInterface<TRANSACTION::ICompositeAddObjectTransaction>();

            compositeAddObjectTransaction->setCompositeObject(composite.get());
            compositeAddObjectTransaction->setObject(_point->getInterface<CORE::IObject>());

            CORE::RefPtr<ELEMENTS::IDrawTechnique> lineDrawTech = _point->getInterface<ELEMENTS::IDrawTechnique>();
            jsonFile = UTIL::StyleFileUtil::getObjectFolder() + "/" + _point->getInterface<CORE::IBase>()->getUniqueID().toString() + ".json";
            if (!osgDB::fileExists(jsonFile))
            {
                osgDB::copyFile(UTIL::StyleFileUtil::getDefaultPointStyle(), jsonFile);
            }
            CORE::RefPtr<SYMBOLOGY::IStyle> styleFile = DB::StyleSheetParser::readStyle(jsonFile);
            ELEMENTS::FeatureObject* featureNodeHolder = dynamic_cast<ELEMENTS::FeatureObject*>(_point->getInterface<CORE::IObject>());
            if (featureNodeHolder != nullptr)
            {
                featureNodeHolder->setVizPlaceNodeStyle(styleFile);
                featureNodeHolder->setBillboard(true);
            }
            // execute the transaction 
            addAndExecuteUndoTransaction(transaction.get());
        }
        catch(UTIL::Exception &e)
        {
            e.LogException();
        }

    }

    void PointUIHandler2::addPointToCurrentSelectedLayer()
    {

        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreatePointLayer();
        _addToLayer(layer);
    }

    void PointUIHandler2::getDefaultLayerAttributes(std::vector<std::string> &attrNames, 
        std::vector<std::string> &attrTypes, std::vector<int> &attrWidths, std::vector<int> &attrPrecisions) const
    {
        if (getPointEnable()){
            attrNames.push_back("NAME");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(1);

#if 0  //reader writter handling this
            attrNames.push_back("_VIZ_ClamponPlacement");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(0);

            attrNames.push_back("_VIZ_ClamponTileLoading");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(0);

            attrNames.push_back("_VIZ_GUID");
            attrTypes.push_back("Integer");
            attrWidths.push_back(1);
            attrPrecisions.push_back(0);
#endif

            attrNames.push_back("_VIZ_ICON");
            attrTypes.push_back("Text");
            attrWidths.push_back(500);
            attrPrecisions.push_back(0);

            attrNames.push_back("VIZ_IMAGES");
            attrTypes.push_back("Text");
            attrWidths.push_back(1000);
            attrPrecisions.push_back(0);

            attrNames.push_back("VIZ_AUDIO");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(0);

            attrNames.push_back("VIZ_VIDEO");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(0);
            
            attrNames.push_back("VIZ_TEXT");
            attrTypes.push_back("Text");
            attrWidths.push_back(1000);
            attrPrecisions.push_back(0);
        }
        else{
            attrNames.push_back("NAME");
            attrTypes.push_back("Text");
            attrWidths.push_back(128);
            attrPrecisions.push_back(1);

            attrNames.push_back("_VIZ_ICON");
            attrTypes.push_back("Text");
            attrWidths.push_back(500);
            attrPrecisions.push_back(0);
        }
    }
    CORE::RefPtr<CORE::IFeatureLayer> 
        PointUIHandler2::getOrCreatePointLayer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();
        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return NULL;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("LayerComponent");
        if(!component.valid())
        {
            LOG_ERROR("Layer Component is not found");
            return NULL;
        }

        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
            component->getInterface<SMCElements::ILayerComponent>();

        CORE::RefPtr<CORE::IFeatureLayer> layer;
        if (getPointEnable()) {
            layer = layerComponent->getFeatureLayer("Point");
        }
        else{
            layer = layerComponent->getFeatureLayer("Layer");
        }
        if(!layer.valid())
        {

            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecisions;

            getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecisions);

            if (getPointEnable()){
                layer = layerComponent->getOrCreateFeatureLayer("Point", "Point", attrNames, attrTypes, attrWidths, attrPrecisions);
            }
            else{
                layer = layerComponent->getOrCreateFeatureLayer("Label", "Point", attrNames, attrTypes, attrWidths, attrPrecisions);
            }
        }

        return layer;
    }

    CORE::RefPtr<CORE::IMetadataTableDefn>
        PointUIHandler2::getCurrentSelectedLayerDefn()
    {

        CORE::RefPtr<CORE::IFeatureLayer> layer = getOrCreatePointLayer();

        if(layer.valid())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                layer->getInterface<CORE::IMetadataTableHolder>();

            if(holder.valid())
            {
                return holder->getMetadataTable()->getMetadataTableDefn();
            }

        }
        //  }
        return NULL;
    }

    void PointUIHandler2::removeCreatedPoint()
    {
        if(_point.valid())
        {
            getWorldInstance()->removeObjectByID(&(_point->getInterface<CORE::IBase>()->getUniqueID()));
            reset();
        }
    }

    void PointUIHandler2::setTemporaryState(bool state)
    {
        _temporary = state;
    }

    bool PointUIHandler2::getTemporaryState() const
    {
        return _temporary;
    }

    void
        PointUIHandler2::setIcon(const std::string& iconFile)
    {
        CORE::RefPtr<CORE::IMetadataRecord> metaRecord =  getMetadataRecord();

        if(metaRecord)
        {
            CORE::RefPtr<CORE::IMetadataField> iconField = metaRecord->getFieldByName("_VIZ_ICON");
            if(iconField.valid())
            {
                std::string icon = iconField->toString();
                _setValueToAttribute("_VIZ_ICON", iconFile);
            }
        }
    }

    void
        PointUIHandler2::getAssociatedImages(std::vector<std::string> &associatedImages)
    {
        associatedImages.clear();

        std::string delimeter = ";";
        CORE::RefPtr<CORE::IMetadataRecord> metaRecord =  getMetadataRecord();

        if(metaRecord)
        {
            CORE::RefPtr<CORE::IMetadataField> imageField = metaRecord->getFieldByName("VIZ_IMAGES");
            if(imageField.valid())
            {

                std::string images = imageField->toString();

                std::size_t pos = images.find(delimeter.c_str());
                while(pos != std::string::npos)
                {

                    std::string imageName = images.substr(0, pos);
                    associatedImages.push_back(imageName);
                    images = images.substr(pos + delimeter.length());
                    pos = images.find(delimeter.c_str());
                }
            }
        }
    }

   void
       PointUIHandler2::getAssociatedFilePaths(std::vector<std::string> &associatedfiles, std::string fieldName)
    {
        associatedfiles.clear();

        std::string delimeter = ";";
        CORE::RefPtr<CORE::IMetadataRecord> metaRecord =  getMetadataRecord();

        if(metaRecord)
        {
            CORE::RefPtr<CORE::IMetadataField> imageField = metaRecord->getFieldByName(fieldName);
            if(imageField.valid())
            {

                std::string images = imageField->toString();

                std::size_t pos = images.find(delimeter.c_str());
                while(pos != std::string::npos)
                {

                    std::string imageName = images.substr(0, pos);
                    associatedfiles.push_back(imageName);
                    images = images.substr(pos + delimeter.length());
                    pos = images.find(delimeter.c_str());
                }
            }
        }
    }

    void
        PointUIHandler2::addAssociatedImage(const std::string& imagePath/*, const std::string& attributeToSet*/)
    {
        std::vector<std::string> imageVector;
        getAssociatedImages(imageVector);

        std::string images;

        std::string delimeter = ";";
        int size = imageVector.size();
        int index = 0;
        while(index < size)
        {
            std::string file = imageVector[index++];
            std::string fileName = osgDB::getSimpleFileName(file);
            images += file + delimeter;
        }

        if(osgDB::fileExists(imagePath))
        {
            std::string fileName = osgDB::getSimpleFileName(imagePath);
            images += fileName + delimeter;
        }

        _setValueToAttribute("VIZ_IMAGES", images);
    }

    std::string
        PointUIHandler2::getAssociatedVideoOrAudio(const std::string& attributeToSet)
    {
        std::string data;
        CORE::RefPtr<CORE::IMetadataRecord> metaRecord =  getMetadataRecord();
        if(metaRecord)
        {
            CORE::RefPtr<CORE::IMetadataField> audioVideoField = metaRecord->getFieldByName(attributeToSet.c_str());
            if(audioVideoField.valid())
            {
                data = audioVideoField->toString();
            }
        }
        return data;
    }

    void 
        PointUIHandler2::setAssociatedVideoOrAudio(const std::string& file, const std::string& attributeToSet)
    {
        std::string fileName = osgDB::getSimpleFileName(file);
        _setValueToAttribute(attributeToSet, fileName);
    }

    void
        PointUIHandler2::_setValueToAttribute(const std::string& attribute, const std::string& textValue)
    {
        if(_point.valid())
        {
            try
            {
                CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder
                    = _point->getInterface<CORE::IMetadataRecordHolder>(true);

                CORE::RefPtr<CORE::IMetadataRecord> record = recordHolder->getMetadataRecord();

                if(record.valid())
                {
                    CORE::RefPtr<CORE::IMetadataField> metaField = record->getFieldByName(attribute);
                    if(metaField.valid())
                    {
                        metaField->fromString(textValue);
                    }
                    else
                    {
                        LOG_ERROR("Unable to get the save field " + attribute);
                    }
                }
                else
                {
                    LOG_ERROR("Metadatarecord is NULL");
                }
            }
            catch(UTIL::Exception &e)
            {
                e.LogException();
            }
        }
    }
    void
        PointUIHandler2::setAssociatedText(const std::string& textValue)
    {
        if (_point.valid())
        {
            try
            {
                CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder
                    = _point->getInterface<CORE::IMetadataRecordHolder>(true);

                CORE::RefPtr<CORE::IMetadataRecord> record = recordHolder->getMetadataRecord();

                if (record.valid())
                {
                    CORE::RefPtr<CORE::IMetadataField> metaField = record->getFieldByName("VIZ_TEXT");
                    if (metaField.valid())
                    {
                        metaField->fromString(textValue);
                    }
                    else
                    {
                        LOG_ERROR("Unable to get the save field VIZ_TEXT");
                    }
                }
                else
                {
                    LOG_ERROR("Metadatarecord is NULL");
                }
            }
            catch (UTIL::Exception &e)
            {
                e.LogException();
            }
        }
    }

    const std::string PointUIHandler2::getAssociatedText()
    {
        if (_point.valid())
        {
            try
            {
                CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder
                    = _point->getInterface<CORE::IMetadataRecordHolder>(true);

                CORE::RefPtr<CORE::IMetadataRecord> record = recordHolder->getMetadataRecord();

                if (record.valid())
                {
                    CORE::RefPtr<CORE::IMetadataField> metaField = record->getFieldByName("VIZ_TEXT");
                    if (metaField.valid())
                    {
                        return metaField->toString();
                    }
                    else
                    {
                        LOG_ERROR("Unable to get the save field VIZ_TEXT");
                    }
                }
                else
                {
                    LOG_ERROR("Metadatarecord is NULL");
                }
            }
            catch (UTIL::Exception &e)
            {
                e.LogException();
            }
        }
        return "";
    }
} // namespace SMCUI

