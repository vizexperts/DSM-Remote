/*****************************************************************************
 *
 * File             : CrestClearanceUIHandler.cpp
 * Description      : CrestClearanceUIHandler class definition
 *
 *****************************************************************************
 * Copyright 2010, VizExperts India Private Limited (unpublished)
 *****************************************************************************/

#include <SMCUI/CrestClearanceUIHandler.h>
#include <App/IApplication.h>
#include <Core/CoreRegistry.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <App/AccessElementUtils.h>
#include <Core/ITerrain.h>
#include <Util/BaseExceptions.h>
#include <iostream>
#include <Util/CoordinateConversionUtils.h>

#include <Core/IMetadataTableHolder.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Elements/IDrawTechnique.h>
#include <Core/IPenStyle.h>
#include <Core/IPoint.h>
#include <Core/Point.h>
#include <Core/IFeatureObject.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IClamper.h>
#include <Elements/OSGEarthLineDrawTechnique.h>
#include <Core/IPenStyle.h>

#include <SMCQt/GunRangeGUI.h>
#include <SMCQt/CrestClearanceGUI.h>

namespace SMCUI 
{
    DEFINE_META_BASE(SMCUI, CrestClearanceUIHandler);
    DEFINE_IREFERENCED(CrestClearanceUIHandler, UIHandler);

    // RFE1 :  
    void CrestLineUpdateCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(mutex);

        if(trajectoryList.size() < 1)
            return;

        while(!trajectoryList.empty())
        {
            for(unsigned int i = 0; i < trajectoryList.size();i++)
            {

                CORE::RefPtr<CORE::IObject> lineObject =
                    CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::LineFeatureObjectType);

                CORE::RefPtr<ELEMENTS::IClamper> clampProp = lineObject->getInterface<ELEMENTS::IClamper>();
                clampProp->setClampOnTileLoading(false);

                CORE::RefPtr<CORE::IPenStyle> penProp = lineObject->getInterface<CORE::IPenStyle>();
                penProp->setPenState(CORE::IPenStyle::ON);

                // contains the output trjectroy
                CORE::RefPtr<CORE::ILine> trajectory;

                trajectory = lineObject->getInterface<CORE::ILine>();
                lineObject->getInterface<ELEMENTS::IDrawTechnique>()->setHeightOffset(0.0);

                osg::ref_ptr<osg::Vec3Array> varray = trajectoryList.at(i).trajectory;

                if(varray.valid())
                {
                    for(osg::Vec3Array::const_iterator citer = varray->begin();
                        citer != varray->end();
                        ++citer)
                    {
                        // Get the point
                        osg::Vec3d point = *citer;
                        CORE::RefPtr<CORE::IPoint> ipoint = new CORE::Point();
                        ipoint->setValue(point.x(), point.y(), point.z());
                        trajectory->addPoint(ipoint.get());
                    }
                    output->addObject(lineObject.get());

                    CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
                                    output->getInterface<CORE::IMetadataTableHolder>();

                    tableDefn = tableHolder->getMetadataTableDefn();

                    createMetadataRecord(trajectory.get(), i);
                }
                else
                {
                    LOG_ERROR("Crest Clearance, Error in CrestLineUpdateCallback\n");
                }
            }
            trajectoryList.clear();
        }
    }

    void
    CrestLineUpdateCallback::createMetadataRecord(CORE::ILine* line, unsigned int index)
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        //create metadata record for the min max point
        CORE::RefPtr<CORE::IMetadataRecord> record = 
            metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        CORE::RefPtr<CORE::IMetadataField> velocityfield = 
            record->getFieldByName("Speed");

        velocityfield->fromDouble(trajectoryList.at(index).velocity);

        CORE::RefPtr<CORE::IMetadataField> elevationfield = 
            record->getFieldByName("Launching Angle");

        elevationfield->fromDouble(trajectoryList.at(index).elevation);

        CORE::RefPtr<CORE::IMetadataField> latitudefield = 
            record->getFieldByName("Launching Latitude");

        latitudefield->fromDouble(trajectoryList.at(index).launchingPoint.x());

        CORE::RefPtr<CORE::IMetadataField> longitudefield = 
            record->getFieldByName("Launching Longitude");

        longitudefield->fromDouble(trajectoryList.at(index).launchingPoint.y());

        CORE::RefPtr<CORE::IMetadataField> heightfield = 
            record->getFieldByName("Launching Height");

        heightfield->fromDouble(trajectoryList.at(index).launchingPoint.z());

        if(line != NULL)
        {
            CORE::RefPtr<CORE::IMetadataRecordHolder> holder = 
                line->getInterface<CORE::IMetadataRecordHolder>();

            if(holder.valid())
            {
                CORE::RefPtr<CORE::IMetadataRecord> oldrecord = holder->getMetadataRecord();
                if(!oldrecord.valid())
                {
                    holder->setMetadataRecord(record.get());
                }
            }
        }
    }



    CrestClearanceUIHandler::CrestClearanceUIHandler()
        : _visitor(NULL) , _gunAdd(false) , _crestAdd(false)
    {
        // TBD initiliaze the member variables in initialization list
        _addInterface(SMCUI::ICrestClearanceUIHandler::getInterfaceName());

        setName(getClassname());
    }

    CrestClearanceUIHandler::~CrestClearanceUIHandler()
    {}

    void 
    CrestClearanceUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void 
    CrestClearanceUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void 
    CrestClearanceUIHandler::setComputationType(ComputationType type)
    {
        if(_visitor.valid())
        {
            switch(type)
            {
            case ICrestClearanceUIHandler::ANGLE_AND_VELOCITY:
                    _visitor->setComputationType(GISCOMPUTE::ICrestClearanceVisitor::ANGLE_AND_VELOCITY);
                    break;
            case ICrestClearanceUIHandler::TRAJECTORY_CALCULATION:
                    _visitor->setComputationType(GISCOMPUTE::ICrestClearanceVisitor::TRAJECTORY_CALCULATION);
                    break;
            default:
                    _visitor->setComputationType(GISCOMPUTE::ICrestClearanceVisitor::INVALID);
                    break;
            }
        }
    }
                
    ICrestClearanceUIHandler::ComputationType 
    CrestClearanceUIHandler::getComputationType()
    {
        ICrestClearanceUIHandler::ComputationType type = ICrestClearanceUIHandler::INVALID;
        if(_visitor.valid())
        {
            switch(_visitor->getComputationType())
            {
                case GISCOMPUTE::ICrestClearanceVisitor::ANGLE_AND_VELOCITY:
                    type = ICrestClearanceUIHandler::ANGLE_AND_VELOCITY;
                    break;
                case GISCOMPUTE::ICrestClearanceVisitor::TRAJECTORY_CALCULATION:
                    type = ICrestClearanceUIHandler::TRAJECTORY_CALCULATION;
                    break;
            }            
        }
        return type;
    }

    void 
    CrestClearanceUIHandler::setLaunchingPosition(osg::Vec3d pos)
    {
        _weaponConfig.launchingPosition = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(pos.y(), pos.x(), pos.z()));
    }

    osg::Vec3d 
    CrestClearanceUIHandler::getLaunchingPosition()
    {
        return  _weaponConfig.launchingPosition;
    }

    void 
    CrestClearanceUIHandler::setTargetPosition(osg::Vec3d pos)
    {
        _weaponConfig.targetPosition = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(pos.y(), pos.x(), pos.z()));
    }

    osg::Vec3d 
    CrestClearanceUIHandler::getTargetPosition()
    {
        return  _weaponConfig.targetPosition;
    }

    void 
    CrestClearanceUIHandler::setDirection(osg::Vec3d direc)
    {
        _weaponConfig.direction = direc;
    }

    osg::Vec3d 
    CrestClearanceUIHandler::getDirection()
    {
        return  _weaponConfig.direction;
    }

    void 
    CrestClearanceUIHandler::setLaunchingAngle(double angle)
    {
        _weaponConfig.launchingAngle = osg::DegreesToRadians(angle);
    }

    double 
    CrestClearanceUIHandler::getLaunchingAngle()
    {
        return  _weaponConfig.launchingAngle;
    }

    void 
    CrestClearanceUIHandler::setLaunchingSpeed(double speed)
    {
        _weaponConfig.launchingSpeed = speed;
    }

    double 
    CrestClearanceUIHandler::getLaunchingSpeed()
    {
        return  _weaponConfig.launchingSpeed;
    }

    void 
    CrestClearanceUIHandler::setAngleStepSize(double angleStepSize)
    {
        _weaponConfig.angleStepSize = osg::DegreesToRadians(angleStepSize);
    }

    double 
    CrestClearanceUIHandler::getAngleStepSize()
    {
        return  _weaponConfig.angleStepSize;
    }

    void 
    CrestClearanceUIHandler::setMinSpeed(double minSpeed)
    {
        _weaponConfig.minSpeed = minSpeed;
    }

    double 
    CrestClearanceUIHandler::getMinSpeed()
    {
        return  _weaponConfig.minSpeed;
    }

    void 
    CrestClearanceUIHandler::setMaxSpeed(double maxspeed)
    {
        _weaponConfig.maxSpeed = maxspeed;
    }

    double 
    CrestClearanceUIHandler::getMaxSpeed()
    {
        return  _weaponConfig.maxSpeed;
    }

    void 
    CrestClearanceUIHandler::setMinAngle(double minAngle)
    {
        _weaponConfig.minAngle = osg::DegreesToRadians(minAngle);
    }

    double 
    CrestClearanceUIHandler::getMinAngle()
    {
        return  _weaponConfig.minAngle;
    }

    void 
    CrestClearanceUIHandler::setMaxAngle(double maxAngle)
    {
        _weaponConfig.maxAngle = osg::DegreesToRadians(maxAngle);
    }

    double CrestClearanceUIHandler::getMaxAngle()
    {
        return  _weaponConfig.maxAngle;
    }

    void 
    CrestClearanceUIHandler::setWeaponName(std::string weaponName)
    {
        _weaponName = weaponName;
    }

    std::string 
    CrestClearanceUIHandler::getWeaponName()
    {
        return  _weaponName;
    }

    void CrestClearanceUIHandler::toAddGun(bool value)
    {
        _gunAdd = true;        
    }

    
    void CrestClearanceUIHandler::toAddCrest(bool value)
    {
        _crestAdd = true;        
    }
    
    void 
    CrestClearanceUIHandler::setFocus(bool value)
    {
        UIHandler::setFocus(value);
        if(_visitor.valid())
        {
            if(value)
            {
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
        }
    }

    void 
    CrestClearanceUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::CrestClearanceVisitorType)
                                ->getInterface<GISCOMPUTE::ICrestClearanceVisitor>();

                // RFE1 :  
                // To create the name of the output
                 _count = 0;
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                        message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
                        
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_IDLE || filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                // XXX - Notify all current observers about the intersections found message
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType);
                notifyObservers(*SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType, *msg);
                // notify GUI
                //notifyObservers(messageType, message);

            } else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING){
                try
                {
                        // RFE1 :  
                        CORE::RefPtr<CORE::IBase> base = message.getSender();
                        CORE::RefPtr<GISCOMPUTE::ICrestClearanceVisitor> visitor = 
                                base->getInterface<GISCOMPUTE::ICrestClearanceVisitor>();
                    
                        if(visitor.valid())
                        {    
                            bool invalid;    
                            GISCOMPUTE::ICrestClearanceVisitor::TrajectoryData * tdata = visitor->getTrajectoryData(invalid);

                            if(tdata)
                            {
                                if( (getComputationType()==GISCOMPUTE::ICrestClearanceVisitor::ANGLE_AND_VELOCITY) && invalid){
/*                                    OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_invalidLineCallback->mutex);
                                    _invalidLineCallback->trajectoryList.push_back(*tdata);*/    

                                    // added to handle invalid path.
                                    filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);

                                }else{
                                    OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_lineCallback->mutex);
                                    _lineCallback->trajectoryList.push_back(*tdata);    
                                }    

                                /*_lineCallback->output = _output;

                                if(getComputationType() == GISCOMPUTE::ICrestClearanceVisitor::ANGLE_AND_VELOCITY)
                                {
                                    _invalidLineCallback->output = _invalidOutput;
                                }*/
                            }
                            else
                            {
                                throw UTIL::Exception("Failed to get trajectory data", __FILE__, __LINE__);
                            }
                        }
                        
                }
                catch(...)
                {
                    filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
                }
            }
             // notify GUI
            notifyObservers(messageType, message);
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }

    void
    CrestClearanceUIHandler::execute()
    {
        try
        {
            GISCOMPUTE::IFilterVisitor* fv = _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            if(fv == NULL)
                return;

            if (_weaponConfig.minRange < 0 && _weaponConfig.maxRange < 0)
            {
                _weaponConfig.minRange = 0;
                _weaponConfig.maxRange = (_weaponConfig.launchingPosition - _weaponConfig.targetPosition).length();
            }

            _visitor->setWeaponConfig(_weaponConfig);

            if(fv->getFilterStatus() != GISCOMPUTE::FILTER_PENDING)
            {
                // Get the terrain handle and pass the visitor to terrain
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getManager());
                CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                CORE::RefPtr<CORE::IBase> terrainobj = terrain->getInterface<CORE::IBase>();

                // RFE1 :  
                _setUpLayerObject();
                terrainobj->accept(*_visitor->getInterface<CORE::IBaseVisitor>());
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    //    XXX - Udit :  
    void CrestClearanceUIHandler::_setUpLayerObject(){
         CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
        if(world)
        {
            // count to set the feature name
           _count++;

           {
               CORE::RefPtr<CORE::IObject> layerObject = 
                   CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);

               _output = layerObject->getInterface<CORE::ICompositeObject>();

               CORE::IPenStyle* pen = _output->getInterface<CORE::IPenStyle>();
               if(pen)
               {
                   pen->setPenColor(osg::Vec4(0, 1, 0, 1));
               }

               std::ostringstream stream;
               if(getComputationType() == ANGLE_AND_VELOCITY)
               {
                   if(_weaponName.empty())
                   {
                        stream << "Crest Clearance Hit " << _count;
                   }
                   
                   else
                   {
                        stream << _weaponName;
                   }
               }
               else
               {
                   stream << "Crest Clearance Output " << _count;
               }

               _output->getInterface<CORE::IBase>()->setName(stream.str());

               _lineCallback = new CrestLineUpdateCallback();

               _createLayer(_output);

               if(layerObject != NULL)
               {
                      layerObject->getOSGNode()->setUpdateCallback(_lineCallback.get());
               }
                _lineCallback->output = _output;
                   
               // add the layer to the world
               
               world->addObject(layerObject.get());
           }

           //=====================================================

           /*if(getComputationType() == ANGLE_AND_VELOCITY)
           {
               CORE::RefPtr<CORE::IObject> invalidLayerObject = 
                   CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);

               _invalidOutput = invalidLayerObject->getInterface<CORE::ICompositeObject>();

               CORE::IPenStyle* pen = _invalidOutput->getInterface<CORE::IPenStyle>();
               if(pen)
               {
                   pen->setPenColor(osg::Vec4(1, 0, 1, 1));
               }

               std::ostringstream stream;
               stream << "Crest Clearance Miss " << _count;

               _invalidOutput->getInterface<CORE::IBase>()->setName(stream.str());

               _invalidLineCallback = new CrestLineUpdateCallback();

               _createLayer(_invalidOutput);

               if(invalidLayerObject != NULL)
               {
                   invalidLayerObject->getOSGNode()->setUpdateCallback(_invalidLineCallback.get());
               }
                _invalidLineCallback->output = _invalidOutput;

               world->addObject(invalidLayerObject.get());
           }    */
        }        
    }

    //    RFE1 :  
    void CrestClearanceUIHandler::_createLayer(CORE::ICompositeObject* output){
        CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
                output->getInterface<CORE::IMetadataTableHolder>();

        if(!tableHolder.valid())
        {
            LOG_ERROR("IMetadataTableHolder interface not found in FeatureLayer");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = 
            tableHolder->getMetadataTable();

        if(!metadataTable.valid())
        {
            LOG_ERROR("Invalid IMetadataTable instance");
            return;
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        // create a new table definition
        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataCreator->createMetadataTableDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Unable to create a IMetadataTableDefn instance");
            return;
        }

        CORE::RefPtr<CORE::IMetadataFieldDefn> latitudeLPField = 
            metadataCreator->createMetadataFieldDefn();
        CORE::RefPtr<CORE::IMetadataFieldDefn> longitudeLPField = 
            metadataCreator->createMetadataFieldDefn();
        CORE::RefPtr<CORE::IMetadataFieldDefn> heightLPField= 
            metadataCreator->createMetadataFieldDefn();
        CORE::RefPtr<CORE::IMetadataFieldDefn> angle = 
            metadataCreator->createMetadataFieldDefn();
        CORE::RefPtr<CORE::IMetadataFieldDefn> velocity = 
            metadataCreator->createMetadataFieldDefn();

        latitudeLPField->setName("Launching Latitude");
        latitudeLPField->setType(CORE::IMetadataFieldDefn::DOUBLE);
        latitudeLPField->setPrecision(8);

        longitudeLPField->setName("Launching Longitude");
        longitudeLPField->setType(CORE::IMetadataFieldDefn::DOUBLE);
        longitudeLPField->setPrecision(8);

        heightLPField->setName("Launching Height");
        heightLPField->setType(CORE::IMetadataFieldDefn::DOUBLE);
        heightLPField->setPrecision(8);

        angle->setName("Launching Angle");
        angle->setType(CORE::IMetadataFieldDefn::DOUBLE);
        angle->setPrecision(8);

        velocity->setName("Speed");
        velocity->setType(CORE::IMetadataFieldDefn::DOUBLE);
        velocity->setPrecision(8);

        // add the fields to the table definition
        tableDefn->addFieldDefn(latitudeLPField.get());
        tableDefn->addFieldDefn(longitudeLPField.get());
        tableDefn->addFieldDefn(heightLPField.get());
        tableDefn->addFieldDefn(angle.get());
        tableDefn->addFieldDefn(velocity.get());

        CORE::RefPtr<CORE::IFeatureLayer> layer = 
            output->getInterface<CORE::IFeatureLayer>();

        if(layer.valid())
        {
            layer->setFeatureLayerType(CORE::IFeatureLayer::LINE);
        }

        // set the field definition to the MetadataTable
        metadataTable->setMetadataTableDefn(tableDefn.get(),output->getInterface<CORE::IBase>()->getUniqueID().toString());
    }

    void
    CrestClearanceUIHandler::stopFilter()
    {
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
            _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }
    void
        CrestClearanceUIHandler::setMinRange(const double& minRange)
    {
        _weaponConfig.minRange = minRange;
    }
    double
        CrestClearanceUIHandler::getMinRange() const
    {
        return _weaponConfig.minRange;
    }

    void
        CrestClearanceUIHandler::setMaxRange(const double& maxRange)
    {
        _weaponConfig.maxRange = maxRange;
    }
    double
        CrestClearanceUIHandler::getMaxRange() const
    {
        return _weaponConfig.maxRange;
    }
    
    
    double CrestClearanceUIHandler::getMinVelocity() const
    {
        if (_visitor.valid())
            return _visitor->getMinVelocity();
        else
            return 0.0;
    }

    
    double CrestClearanceUIHandler::getMaxVelocity() const
    {
        if (_visitor.valid())
            return _visitor->getMaxVelocity();
        else
            return 0.0;
    }
    double CrestClearanceUIHandler::getDisplacement() const
    {
        if (_visitor.valid())
            return _visitor->getDisplacement();
        else
            return 0.0;
    }
    
} // namespace SMCUI

