/*****************************************************************************
 *
 * File             : AddContentComputeMinMaxLODMessage.h
 * Description      : AddContentComputeMinMaxLODMessage class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCUI/AddContentComputeMinMaxLODMessage.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, AddContentComputeMinMaxLODMessage)
    DEFINE_IREFERENCED(AddContentComputeMinMaxLODMessage, CORE::CoreMessage)

    AddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessage()
        : _status(IDLE),
          _progress(0),
          _minLOD(0),
          _maxLOD(99)
    {
        _addInterface(IAddContentComputeMinMaxLODMessage::getInterfaceName());
    }

    void 
    AddContentComputeMinMaxLODMessage::setStatus(const AddContentComputeMinMaxLODStatus status)
    {
        _status = status;
    }

    AddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODStatus
    AddContentComputeMinMaxLODMessage::getStatus() const
    {
        return _status;
    }

    void
    AddContentComputeMinMaxLODMessage::setProgress(unsigned int progress)
    {
        _progress = progress;
    }

    unsigned int
    AddContentComputeMinMaxLODMessage::getProgress()
    {
        return _progress;
    }

    void
    AddContentComputeMinMaxLODMessage::setMinLOD(unsigned int minLOD)
    {
        _minLOD= minLOD;
    }

    unsigned int
    AddContentComputeMinMaxLODMessage::getMinLOD()
    {
        return _minLOD;
    }

    void
    AddContentComputeMinMaxLODMessage::setMaxLOD(unsigned int maxLOD)
    {
        _maxLOD= maxLOD;
    }

    unsigned int
    AddContentComputeMinMaxLODMessage::getMaxLOD()
    {
        return _maxLOD;
    }

    AddContentComputeMinMaxLODMessage::~AddContentComputeMinMaxLODMessage(){}
}
