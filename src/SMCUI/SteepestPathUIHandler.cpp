#include <SMCUI/SteepestPathUIHandler.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <Core/ITerrain.h>
#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/IFilterVisitor.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <Core/Point.h>
#include <Core/IPenStyle.h>
#include <Core/AttributeTypes.h>
#include <Elements/IDrawTechnique.h>
#include <Core/ITickMessage.h>

namespace SMCUI 
{

    void LineUpdateCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(mutex);
        // Read the array list and push into the line
        std::queue<osg::ref_ptr<osg::Vec3dArray> > VArrayList;
        while(!arrayList.empty())
        {
            osg::ref_ptr<osg::Vec3dArray> varray = arrayList.front();
            for(osg::Vec3dArray::const_iterator citer = varray->begin();
                citer != varray->end();
                ++citer)
            {
                // Get the point
                osg::Vec3d point = *citer;
                CORE::RefPtr<CORE::IPoint> ipoint = new CORE::Point();
                //ipoint->setValue(point.x(), point.y(), point.z() + _offset);
                ipoint->setValue(point.x(), point.y(), point.z());
                line->addPoint(ipoint.get());
            }
            arrayList.pop();
        }
    }
    
    DEFINE_META_BASE(SMCUI, SteepestPathUIHandler);

    DEFINE_IREFERENCED(SteepestPathUIHandler, VizUI::UIHandler);

    SteepestPathUIHandler::SteepestPathUIHandler()
        : _line(NULL)
        , _lineCallback(new LineUpdateCallback)
        , _steepestPathVisitor(NULL)
        , _bFirstUpdate(true)
        , _updateInterval(2)
        , _maxLevel(1)
        , _cellSizeFactor(1)
        , _name("")
        , _offset(0.0)
        , _bOnTickFirstUpdate(false)
    {
        _addInterface(ISteepestPathUIHandler::getInterfaceName());
        setName(getClassname());
    }
    
    SteepestPathUIHandler::~SteepestPathUIHandler(){}

    void 
    SteepestPathUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void 
    SteepestPathUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void
    SteepestPathUIHandler::setFocus(bool value)
    {
        // If gaining focus then register to TerrainPickUIHandler otherwise unregister
        // Get TerrainPickUIHandler
        try
        {
            // Query TerrainUIHandler interface
            if(value)
            {

                CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _steepestPathVisitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::SteepPathCalcVisitorType)
                                ->getInterface<GISCOMPUTE::ISteepPathCalcVisitor>();
                if(!_steepestPathVisitor.valid())
                {
                    throw UTIL::Exception("Failed to create SteepestPathVisitor", __FILE__, __LINE__);
                }
                _subscribe(_steepestPathVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                // Subscribe to tick message
                CORE::RefPtr<CORE::IWorldMaintainer> wm = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IUIHandlerManager>(getManager());
                _subscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);

                if((STEEPCOMPUTATION_IDLE == getPlayState()) || (STEEPCOMPUTATION_EXIT == getPlayState()))
                {
                    _computeSteepPathCalElevation();
                }
            }
            else
            {
                if(_steepestPathVisitor.valid())
                {
                    _unsubscribe(_steepestPathVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                }

                CORE::RefPtr<CORE::IWorldMaintainer> wm = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IUIHandlerManager>(getManager());
                if(wm.valid())
                {
                    _unsubscribe(wm.get(), *CORE::IWorldMaintainer::TickMessageType);
                }

                /*if(_line.valid())
                {
                    CORE::RefPtr<CORE::IBase> lineObject = _line->getInterface<CORE::IBase>();
                    if(lineObject.valid())
                    {
                        iworld->removeObjectByID(&(lineObject->getUniqueID()));
                    }
                }*/
                if(_line.valid())
                {
                    _line->getInterface<CORE::IObject>()->getOSGNode()->removeUpdateCallback(_lineCallback.get());
                }
                _reset();
            }
        }
        catch(...)
        {
            // XXX - Throw a fatal error
        }


        UIHandler::setFocus(value);
    }

    void
    SteepestPathUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();

        // Add ContextPropertyName attribute
        std::string groupName = "SteepestPathUIHandler attributes";
        _addAttribute(new CORE::DoubleAttribute("MaxLevel", "MaxLevel",
                                CORE::DoubleAttribute::SetFuncType(this, &SteepestPathUIHandler::setMaxLevel),
                                CORE::DoubleAttribute::GetFuncType(this, &SteepestPathUIHandler::getMaxLevel),
                                "Max Level",
                                groupName));
        _addAttribute(new CORE::DoubleAttribute("CellSizeFactor", "CellSizeFactor",
                        CORE::DoubleAttribute::SetFuncType(this, &SteepestPathUIHandler::setCellSizeFactor),
                        CORE::DoubleAttribute::GetFuncType(this, &SteepestPathUIHandler::getCellSizeFactor),
                        "Cell Size Factor",
                        groupName));
        _addAttribute(new CORE::DoubleAttribute("UpdateInterval", "UpdateInterval",
                CORE::DoubleAttribute::SetFuncType(this, &SteepestPathUIHandler::setUpdateInterval),
                CORE::DoubleAttribute::GetFuncType(this, &SteepestPathUIHandler::getUpdateInterval),
                "Update Interval",
                groupName));

        _addAttribute(new CORE::DoubleAttribute("LineOffset", "LineOffset",
                CORE::DoubleAttribute::SetFuncType(this, &SteepestPathUIHandler::setLineOffset),
                CORE::DoubleAttribute::GetFuncType(this, &SteepestPathUIHandler::getLineOffset),
                "Line Offset",
                groupName));
    }

    void 
    SteepestPathUIHandler::setMaxLevel(double maxLevel)
    {
        _maxLevel = maxLevel;
    }

    double 
    SteepestPathUIHandler::getMaxLevel()
    {
        return _maxLevel;
    }

    void 
    SteepestPathUIHandler::setCellSizeFactor(double cellSizeFactor)
    {
        _cellSizeFactor = cellSizeFactor;
    }

    double 
    SteepestPathUIHandler::getCellSizeFactor()
    {
        return _cellSizeFactor;
    }

    void 
    SteepestPathUIHandler::setUpdateInterval(double updateInterval)
    {
        _updateInterval = updateInterval;
    }

    void
    SteepestPathUIHandler::setLineOffset(double offset)
    {
        _offset = offset;
    }

    double
    SteepestPathUIHandler::getLineOffset()
    {
        return _offset;
    }

    double 
    SteepestPathUIHandler::getUpdateInterval()
    {
        return _updateInterval;
    }

    void 
    SteepestPathUIHandler::_computeSteepPathCalElevation()
    {
        CORE::IWorld* world = getWorldInstance();
        if(NULL == world)
        {
            throw UTIL::Exception("invalid world instance", __FILE__, __LINE__);
        }
        if(!_steepestPathVisitor.valid())
        {
            throw UTIL::Exception("invalid visitor", __FILE__, __LINE__);
        }

        CORE::RefPtr<CORE::IBaseVisitor> baseVisitor
        = _steepestPathVisitor->getInterface<CORE::IBaseVisitor>();
        CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

        if(!terrain.valid())
        {
            throw UTIL::Exception("invalid terrain instance", __FILE__, __LINE__);
        }


        _steepestPathVisitor->setMaxLevel(_maxLevel);
        _steepestPathVisitor->setCellSizeFactor(_cellSizeFactor);
        _steepestPathVisitor->setUpdateInterval(_updateInterval);

        CORE::IBase* terrainBase = terrain->getInterface<CORE::IBase>();
        terrainBase->accept(*baseVisitor.get());
    }

    bool 
    SteepestPathUIHandler::setStartingPoint(const osg::Vec2d & point)
    {
        bool retval = false;
        if(_steepestPathVisitor.valid())
        {
            retval = _steepestPathVisitor->setStartingPoint(point);
        }
        else
        {
            throw UTIL::Exception("invalid visitor", __FILE__, __LINE__);
        }
        return retval;
    }

    void 
    SteepestPathUIHandler::setOutputName(std::string& name)
    {
        _name = name;
    }

    bool 
    SteepestPathUIHandler::setExtent(const osg::Vec2dArray& extents)
    {
        bool retval = false;
        if(_steepestPathVisitor.valid() && (extents.size() > 1))
        {
            double xmin = 0.0, ymin = 0.0, xmax = 0.0, ymax = 0.0;
            if( extents.at(0).x() < extents.at(1).x())
            {
                xmin = extents.at(0).x();
                xmax = extents.at(1).x();
            }
            else
            {
                xmin = extents.at(1).x();
                xmax = extents.at(0).x();
            }
            if( extents.at(0).y() < extents.at(1).y())
            {
                ymin = extents.at(0).y();
                ymax = extents.at(1).y();
            }
            else
            {
                ymin = extents.at(1).y();
                ymax = extents.at(0).y();
            }
            osg::ref_ptr<osg::Vec2dArray> areaExtents = new osg::Vec2dArray;
            areaExtents->push_back(osg::Vec2d(xmin, ymin));
            areaExtents->push_back(osg::Vec2d(xmax, ymax));;
            retval = _steepestPathVisitor->setExtent(*areaExtents.get());
        }
        else
        {
            throw UTIL::Exception("invalid visitor", __FILE__, __LINE__);
        }
        return retval;
    }

    osg::Vec2d& 
    SteepestPathUIHandler::getCurrentPoint()
    {
        if(_steepestPathVisitor.valid())
        {
            return _steepestPathVisitor->getCurrentPoint();
        }
        else
        {
            throw UTIL::Exception(" SteepPath Visitor is Invalid", __FILE__, __LINE__);
        }
    }

    osg::Vec2d& 
    SteepestPathUIHandler::getStartingPoint()
    {
        if(_steepestPathVisitor.valid())
        {
            return _steepestPathVisitor->getStartingPoint();
        }
        else
        {
            throw UTIL::Exception(" SteepPath Visitor is Invalid", __FILE__, __LINE__);
        }
    }

    bool
    SteepestPathUIHandler::play(bool multiThreaded)
    {
        bool retval = false;
        if(_steepestPathVisitor.valid())
        {
            // create line instance if state is not paused
            if(STEEPCOMPUTATION_PAUSED != getPlayState())
            {
                static int i = 1;
                std::stringstream ss;
                ss << "SteepestPathOutput" << i;
                i++;

                if(_line.valid())
                {
                    _line->getInterface<CORE::IObject>()->getOSGNode()->removeUpdateCallback(_lineCallback.get());
                }

                CORE::RefPtr<CORE::IObject> lineObject =
                CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::LineFeatureObjectType);
                lineObject->getInterface<CORE::IBase>()->setName(ss.str());
                //world->addObject(lineObject.get());
                {
                    OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                    _bFirstUpdate = true;
                    _bOnTickFirstUpdate = false;
                }
                _line = lineObject->getInterface<CORE::ILine>();
                {
                    OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_lineCallback->mutex);
                    _lineCallback->line = _line.get();
                    _lineCallback->_offset = _offset;
                }

                CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = _line->getInterface<ELEMENTS::IDrawTechnique>();

                if(draw.valid())
                {
                    draw->setHeightOffset(_offset);
                }

                CORE::RefPtr<CORE::IPenStyle> lineStyle = _line->getInterface<CORE::IPenStyle>();
                if(lineStyle.valid())
                {
                    lineStyle->setPenWidth(5);
                    osg::Vec4 yellowish(0.789, 0.965, 0.082, 1.0);
                    lineStyle->setPenColor(yellowish);
                }
                lineObject->getOSGNode()->setUpdateCallback(_lineCallback.get());
                lineObject->getInterface<CORE::IBase>()->setName(_name);
            }
            _steepestPathVisitor->setMultiThreaded(multiThreaded);
            retval = _steepestPathVisitor->play();
        }
        else
        {
            throw UTIL::Exception("invalid visitor", __FILE__, __LINE__);
        }
        return retval;
    }
 
    bool
    SteepestPathUIHandler::stop()
    {
        bool retval = true;
        if(_steepestPathVisitor.valid())
        {
            retval = _steepestPathVisitor->stop();
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            _bFirstUpdate = true;
            _bOnTickFirstUpdate = false;
        }
        return retval;
    }

    SteepestPathUIHandler::PlayState
    SteepestPathUIHandler::getPlayState()
    {
        if(_steepestPathVisitor.valid())
        {
            return static_cast<PlayState>(_steepestPathVisitor->getPlayState());
        }
        else
        {
            return STEEPCOMPUTATION_IDLE;
        }
    }

    void 
    SteepestPathUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            if(_lineCallback.valid() && _bOnTickFirstUpdate)
            {
                CORE::IWorld* world = getWorldInstance();
                if(NULL == world)
                {
                    throw UTIL::Exception("invalid world instance", __FILE__, __LINE__);
                }
                world->addObject(_line->getInterface<CORE::IObject>());
                _bOnTickFirstUpdate = false;
            }
        }
        if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType && _steepestPathVisitor.valid())
        {
            switch(message.getInterface<GISCOMPUTE::IFilterStatusMessage>()->getStatus())
            {
                case GISCOMPUTE::FILTER_SUCCESS:
                case GISCOMPUTE::FILTER_PENDING:
                case GISCOMPUTE::FILTER_PAUSED:
                    {
                        
                        osg::ref_ptr<osg::Vec3dArray> steepPointsArr = _steepestPathVisitor->getSteepestPath();
                        if((steepPointsArr->size() > 0) && ((_line->getPointNumber() + steepPointsArr->size()) > 1))
                        {
                            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                            if(_bFirstUpdate)
                            {
                                _bFirstUpdate = false;
                                _bOnTickFirstUpdate = true;
                            }
                            _addSteepestPathToScene(*steepPointsArr.get());
                        }
                    }
                    break;
                case GISCOMPUTE::FILTER_FAILURE:
                    {
                        //_unsubscribe(_steepestPathVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                        _bFirstUpdate = true;
                        _bOnTickFirstUpdate = false;
                    }
                    break;
                case GISCOMPUTE::FILTER_STOPPED:
                    {
                        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                        _bFirstUpdate = true;
                        _bOnTickFirstUpdate = false;
                    }
                    break;
            }
            // Test play state
            if(STEEPCOMPUTATION_EXIT == getPlayState())
            {
                // delete the visitor, player is reset.
                // unsuscribe for any notification further
                if(_steepestPathVisitor.valid())
                {
                    _unsubscribe(_steepestPathVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                    _steepestPathVisitor = NULL;
                }
                OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                _bFirstUpdate = true;
                _bOnTickFirstUpdate = false;
            }
        }
        else 
        {
            UIHandler::update(messageType, message);
        }
    }


    void
    SteepestPathUIHandler::_addSteepestPathToScene(const osg::Vec3dArray& steepPointsArr)
    {
        // Create a copy of the array and then put in line update callback
        osg::ref_ptr<osg::Vec3dArray> varray = new osg::Vec3dArray(steepPointsArr, osg::CopyOp::DEEP_COPY_ALL);
        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_lineCallback->mutex);
        _arrayofPoints = varray;
        _lineCallback->arrayList.push(varray.get());
    }

    void
    SteepestPathUIHandler::_reset()
    {
        _line = NULL;
        {
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_lineCallback->mutex);
            _lineCallback->line = NULL;
            while(!(_lineCallback->arrayList.empty()))
            {
                _lineCallback->arrayList.pop();
            }
            _lineCallback->_offset = 0.0;
        }
        {
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            _bFirstUpdate = true;
            _bOnTickFirstUpdate = false;
        }
        _steepestPathVisitor = NULL;
        _name = "";
    }

     //! get Output Line
     CORE::ILine* 
     SteepestPathUIHandler::getOutput() const
     {
         return _lineCallback->line.get();
     }

     // get Output  Points
     osg::Vec3dArray*
     SteepestPathUIHandler::getOutputPoints() const
     {
        return _arrayofPoints.get();
     }
} // namespace SMCUI

