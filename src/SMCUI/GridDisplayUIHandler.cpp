#include <SMCUI/GridDisplayUIHandler.h>
#include <SMCUI/SMCUIPlugin.h>

#include <Elements/ElementsUtils.h>
#include <Core/WorldMaintainer.h>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/ReadFile>
#include <Util/ThreadPool.h>
#include <Util/FileUtils.h>


#include <App/ApplicationRegistry.h>
#include <App/AppPlugin.h>
#include <App/TaskMaintainer.h>
#include <App/ITask.h>

#include <osgDB/FileNameUtils>

#include <DB/ReadFile.h>
#include <App/AccessElementUtils.h>

#include <Core/IVisibility.h>
#include <Core/CoreRegistry.h>
#include <Core/CoreRegistry.h>
#include <Core/ICompositeObject.h>
#include <Elements/ElementsPlugin.h>
#include <Core/ITemporary.h>
#include<Terrain/IModelObject.h>
#include<Symbology/IStyleSheetProperty.h> 
#include<Symbology/IStyle.h> 
#include<Symbology/SymbologyPlugin.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, GridDisplayUIHandler);
    DEFINE_IREFERENCED(GridDisplayUIHandler, VizUI::UIHandler);

    GridDisplayUIHandler::GridDisplayUIHandler()
        : _gridDisplay(false)
        , _indianGridDisplay(false)
        , _indianGridShapeFilePath("/IndianGRGrid/grid.shp")
        , _indianGridStyleTemplate("/IndianGRGrid/IndianGridStyle.json")
    {
        _addInterface(SMCUI::IGridDisplayUIHandler::getInterfaceName());
    }

    GridDisplayUIHandler::~GridDisplayUIHandler()
    {}

    void GridDisplayUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizUI::UIHandler::onAddedToManager();
    }

    void GridDisplayUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
        
        VizUI::UIHandler::onRemovedFromManager();
    }

    void GridDisplayUIHandler::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
            _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            if(_gridNode.valid())
            {
                CORE::WorldMaintainer::instance()->getOSGNode()->asGroup()->removeChild(_gridNode.get());
                _gridNode = NULL;
            }

            _gridDisplay = false;
            if (_indianGridObject.valid())
            {
                _indianGridObject = NULL;
            }
            _indianGridDisplay = false;
        }
        else 
        {
            VizUI::UIHandler::update(messageType, message);
        }
    }

    void GridDisplayUIHandler::setGridDisplay(bool value)
    {
        if(_gridDisplay == value)
            return;

        _gridDisplay = value;

        if(value)
        {
            if(!_gridNode.valid())
            {
                osgEarth::MapNode* mapNode = ELEMENTS::GetFirstWorldFromMaintainer()->getTerrain()->getMapNode();

                _gridNode = new osgEarth::Util::GeodeticGraticule(mapNode);
                osgEarth::Util::GeodeticGraticuleOptions o = _gridNode->getOptions();
                o.lineStyle()->getOrCreate<osgEarth::Util::LineSymbol>()->stroke()->color().set(1, 0, 0, 1);
                _gridNode->setOptions(o);
            }

            if(_gridNode.valid())
            {
                CORE::WorldMaintainer::instance()->getOSGNode()->asGroup()->addChild(_gridNode.get());
            }
        }
        else
        {
            if(_gridNode.valid())
            {
                CORE::WorldMaintainer::instance()->getOSGNode()->asGroup()->removeChild(_gridNode.get());
            }
        }

    }
    void GridDisplayUIHandler::setIndianGridDisplay(bool value)
    {

        if (_indianGridDisplay == value)
            return;

        _indianGridDisplay = value;
        //! Save in AppData
        std::string dataDir = UTIL::getDataDirPath();
        std::string filePath = dataDir + _indianGridShapeFilePath;
        std::string styleSheetPath = dataDir + _indianGridStyleTemplate;
        std::string layerName="IndianGrid";
        if (value)
        {
            if (!_indianGridObject.valid())
            {
                try
                {
                    if (filePath.empty() || styleSheetPath.empty())
                    {
                        return;
                    }
                    std::string fileLocation = osgDB::convertFileNameToNativeStyle(filePath);
                    CORE::RefPtr<DB::ReaderWriter::Options>  options = new DB::ReaderWriter::Options;
                    options->setMapValue("StyleSheet", styleSheetPath);
                    // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
                    CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
                    CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
                    osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();
                    tp->invoke(boost::bind(&GridDisplayUIHandler::_mtAddVectorData, this, layerName, fileLocation, options));
                }
                catch (const UTIL::Exception& e)
                {
                    e.LogException();
                    throw e;
                }
            }
            else
            {
                CORE::RefPtr<CORE::IVisibility> visibility = _indianGridObject->getInterface<CORE::IVisibility>();
                if (visibility.valid())
                {
                    visibility->setVisibility(true);

                }

            }
        }
        else
        {
            if (_indianGridObject.valid())
            {
                CORE::RefPtr<CORE::IVisibility> visibility = _indianGridObject->getInterface<CORE::IVisibility>();
                if (visibility.valid())
                {
                    visibility->setVisibility(false);

                }
            }
        }

    }
    void
        GridDisplayUIHandler::_mtAddVectorData(const std::string &layerName, const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options)
    {
        try
        {
            options->setMapValue("Symbology", "1");
            CORE::RefPtr<CORE::IObject> object = DB::readFeature(url, options.get());
            if (!object.valid())
            {
                std::string errstring = std::string("Error loading file. Check whether the file is valid or not.");
                throw UTIL::FileNotFoundException(errstring, __FILE__, __LINE__);

            }

            


            object->getInterface<CORE::IBase>(true)->setName(layerName);
            _indianGridObject = object;
            CORE::RefPtr<CORE::ITemporary> temp = _indianGridObject->getInterface<CORE::ITemporary>();
            if (temp.valid())
            {
                temp->setTemporary(true);
            }
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            CORE::WorldMaintainer* worldMaintainer = CORE::WorldMaintainer::instance();
            typedef std::map<const CORE::UniqueID*, CORE::RefPtr<CORE::IWorld> > WorldMap;
            WorldMap worldmap = worldMaintainer->getWorldMap();
            WorldMap::iterator iter = worldmap.begin();
            iter->second.get()->addObject(_indianGridObject.get());
            return;
        }
        catch (UTIL::Exception& e)
        {
             e.LogException();

         }
        catch (...)
        {
  
        }

    }
    bool GridDisplayUIHandler::getIndianGridDisplay() const
    {
        return _indianGridDisplay;
    }
    bool GridDisplayUIHandler::getGridDisplay() const
    {
        return _gridDisplay;
    }

}    // namespace SMCUI
