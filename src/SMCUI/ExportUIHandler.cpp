#include <SMCUI/ExportUIHandler.h>
#include <DB/WriteFile.h>
#include <VizUI/ISelectionUIHandler.h>

#include <App/AccessElementUtils.h>
#include <App/TaskMaintainer.h>
#include <App/ITask.h>
#include <App/ApplicationRegistry.h>
#include <App/AppPlugin.h>
#include <App/AccessElementUtils.h>
#include <App/AccessElementUtils.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>
#include <Terrain/IVirtualDataset.h>
#include <Terrain/TerrainPlugin.h>

#include <Elements/FeatureLayer.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/OGRMetadataTable.h>

#include <Core/IGeometry.h>
#include <Core/IRasterData.h>
#include <Core/IElevationData.h>
#include <Core/IObjectType.h>
#include <Core/IObject.h>
#include <Core/ICompositeObject.h>
#include <Core/WorldMaintainer.h>
#include <Core/IReferenced.h>
#include <Core/IMetadataCreator.h>
#include <Core/IObjectFactory.h>
#include <Core/CoreRegistry.h>

#include <osgDB/WriteFile>
#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <Util/FileUtils.h>
#include <ogrsf_frmts.h>
#include <gdal.h>
#include <ogr_api.h>
#include <ogr_srs_api.h>
#include <sstream>

namespace SMCUI
{
    DEFINE_IREFERENCED(ExportUIHandler, UIHandler)
    DEFINE_META_BASE(SMCUI, ExportUIHandler)

    ExportUIHandler::ExportUIHandler() : _status(IDLE)
    {
        _addInterface(IExportUIHandler::getInterfaceName());
        setName(getClassname());
    }

    bool
    ExportUIHandler::exportRaster(const std::string& filename)
    {
        vizCore::RefPtr<TERRAIN::IRasterObject> item =
                    _getCurrentSelectedItem<TERRAIN::IRasterObject>();
        if(item.valid())
        {
            vizCore::RefPtr<vizCore::IRasterData> data = item->getRasterData();
            if(data.valid())
            {
                // if a tms file is exported - append extension ".xml" at the end.
                std::string extension = osgDB::getLowerCaseFileExtension(filename);
                if(extension == "xml")
                {
                    std::string modifiedUrl = filename + ".tms";
                    item->setURL(modifiedUrl);
                    return vizDB::writeRasterFile(data.get(), modifiedUrl);
                }
                else
                {
                    item->setURL(filename);
                    return vizDB::writeRasterFile(data.get(), filename);
                }
                
            }
        }
        return false;
    }

    template <class T>
    T*
    ExportUIHandler::_getCurrentSelectedItem()
    {
        vizCore::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
        vizApp::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const vizCore::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getCurrentSelection();

        vizCore::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if(iter != map.end())
        {
            return iter->second->getInterface<T>();
        }
        return NULL;
    }

    bool 
        ExportUIHandler::exportRasterAsTMS(const std::string& directoryPath, const std::string& minLevel, const std::string& maxLevel, const osg::Vec4& selectedArea)
    {
        if (!osgDB::fileExists(directoryPath))
        {
	        std::string errstring = directoryPath + " Check whether the folderpath exists.";
            throw vizUtil::FileNotFoundException(errstring, __FILE__, __LINE__);
        }

        try
        {
            // Get the world maintainer, get threadpool and execute the multi-threaded add raster data method
	        _status = PROGRESS;
            vizCore::RefPtr<vizCore::IWorldMaintainer> wm = vizCore::WorldMaintainer::instance();
            osg::ref_ptr<vizUtil::ThreadPool> tp = wm->getComputeThreadPool();
			
            tp->invoke(boost::bind(&ExportUIHandler::_mtExportRasterAsTMS, this, directoryPath, minLevel, maxLevel,selectedArea));
            return true;
        }
        catch(const vizUtil::Exception& e)
        {
            e.LogException();
	        _status = ERRORINCREATION;
            throw TMSCreationException(e.What(), __FILE__, __LINE__);
        }
        return false;
    }

    bool 
    ExportUIHandler::exportProject(const std::string& projectFileName)
    {
        try 
        {
            //Get World object 
            vizCore::RefPtr<vizCore::IWorld> iworld = 
                vizApp::AccessElementUtils::getWorldFromManager<vizApp::IUIHandlerManager>(getManager());

            vizCore::RefPtr<vizCore::IObject> iObject = iworld->getInterface<vizCore::IObject>(); 
            if (!iObject.valid())
            {
                LOG_ERROR("Unable to create world");
                return false;
            }

            if (!vizDB::writeObjectFile(iObject, projectFileName))
            {
                LOG_ERROR("writeObjectFile failed");
                return false;
            }

            return true;
        }
        catch(const vizUtil::Exception& e)
        {
             e.LogException();
            throw ProjectExportException(e.What(), __FILE__, __LINE__);
        }
        return false;
    }

    bool ExportUIHandler::_copyDir(
        boost::filesystem::path const & source,
        boost::filesystem::path const & destination
        )
    {
        namespace fs = boost::filesystem;
        try
        {
            // Check whether the function call is valid
            if (
                !fs::exists(source) ||
                !fs::is_directory(source)
                )
            {
                std::stringstream strstrm;
                strstrm << "Source directory " << source.string()
                    << " does not exist or is not a directory." << '\n'
                    ;
                LOG_ERROR(strstrm.str());
                return false;
            }
            if (fs::exists(destination))
            {
                std::stringstream strstrm;
                strstrm << "Destination directory " << destination.string()
                    << " already exists." << '\n'
                    ;
                LOG_ERROR(strstrm.str());
                //return false;
            }
            // Create the destination directory
            if (!fs::create_directories(destination))
            {
                std::stringstream strstrm;
                strstrm << "Unable to create destination directory"
                    << destination.string() << '\n'
                    ;
                LOG_ERROR(strstrm.str());
                //return false;
            }
        }
        catch (fs::filesystem_error const & e)
        {
            std::stringstream strstrm;
            strstrm << e.what() << '\n';
            LOG_ERROR(strstrm.str());
            return false;
        }
        // Iterate through the source directory
        for (
            fs::directory_iterator file(source);
            file != fs::directory_iterator(); ++file
            )
        {
            try
            {
                fs::path current(file->path());
                if (fs::is_directory(current))
                {
                    // Found directory: Recursion
                    if (
                        !_copyDir(
                        current,
                        destination / current.filename()
                        )
                        )
                    {
                        return false;
                    }
                }
                else
                {
                    // Found file: Copy
                    fs::copy_file(
                        current,
                        destination / current.filename()
                        );
                }
            }
            catch (fs::filesystem_error const & e)
            {
                return false;
            }
        }
        return true;
    }

    void 
        ExportUIHandler::_copyFoldertoRemote(const std::string& srcdirectoryPath, const std::string& dstdirectoryPath)
    {
        if(!osgDB::fileExists(dstdirectoryPath))
        { 
            boost::filesystem::create_directories(osgDB::getFilePath(dstdirectoryPath));
        }
        bool retval = _copyDir(osgDB::convertFileNameToNativeStyle(osgDB::getFilePath(srcdirectoryPath)), osgDB::convertFileNameToNativeStyle(osgDB::getFilePath(dstdirectoryPath)));
    }

    void
        ExportUIHandler::_mtExportRasterAsTMS(const std::string& directoryPath, const std::string& minLevel, const std::string& maxLevel, const osg::Vec4& selectedArea)
    {
        //Creating a task of LoadType and setting its properties
        vizCore::RefPtr<vizApp::ITask> tmsCreationtask = vizApp::ApplicationRegistry::instance()->getTaskFactory()
            ->createTask(*vizApp::AppRegistryPlugin::TMSGenerationTaskType);
        vizCore::RefPtr<vizApp::ITaskMaintainer> taskMaintainer = vizApp::TaskMaintainer::instance();
        taskMaintainer->addTask(tmsCreationtask.get());
        tmsCreationtask->isProgressable(false);
        tmsCreationtask->isCancelable(false);
        try
        {

            vizCore::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            vizApp::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

            const vizCore::ISelectionComponent::SelectionMap& map =
                selectionUIHandler->getCurrentSelection();

            vizCore::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();

	        //map.size of selected object will always be equal to one
            if(map.size() == 1)
            {
                vizCore::RefPtr<TERRAIN::IRasterObject> rItem =
                        iter->second->getInterface<TERRAIN::IRasterObject>();

                if(rItem.valid())
                {
                    vizCore::RefPtr<vizCore::IRasterData> data = rItem->getRasterData();
                    if(data.valid())
                    {
                        CORE::RefPtr<CORE::IGISData> datGIS = data->getInterface<CORE::IGISData>();
                        std::string ext = osgDB::getFileExtension(rItem->getURL());
                        
                        // setting url and gis url for web generated raster data.
                        if (datGIS.valid() && (ext == "xyz" || ext == "wms" || ext == "bing"))
                        {
                            datGIS->setURL(rItem->getURL());
                            datGIS->setExtents(selectedArea);
                        }

                        std::string modifiedUrl = directoryPath + "/tms.tms";
                        vizCore::RefPtr<vizDB::ReaderWriter::Options> options = new DB::ReaderWriter::Options;

                        options->setMapValue("minLevel", minLevel);
                        options->setMapValue("maxLevel", maxLevel);

                        // Set Object Names to task
                        vizCore::RefPtr<vizCore::IBase> objBase = rItem->getInterface<vizCore::IBase>(true);
                        tmsCreationtask->setObjectNames(objBase->getName());
                        //start the task
                        tmsCreationtask->start();

                        if (modifiedUrl.find("//") != 0)
                        {
                            vizDB::writeRasterFile(data.get(), modifiedUrl, options);
                        }
                        else
                        {
                            std::string fileName = osgDB::getStrippedName(modifiedUrl);

                            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
                            std::string appdataPath = temporaryInstance->getAppFolderPath();

                            std::string temporaryFolder = appdataPath + "/TempTMS";
                            boost::filesystem::create_directories(temporaryFolder);
                            std::string localPath = temporaryFolder + "/" + osgDB::getStrippedName(modifiedUrl) + "." + osgDB::getFileExtension(modifiedUrl);

                            vizDB::writeRasterFile(data.get(), localPath, options);

                            // Making TMS in remote folder fails.
                            // Solution is to make TMS in local folder and copy it to remote folder.
                            _copyFoldertoRemote(localPath, modifiedUrl);

                            boost::filesystem::remove_all(temporaryFolder);

                        }
                        tmsCreationtask->finish();
				        _status = COMPLETED;
                    }
                }
                else
                {
                    vizCore::RefPtr<TERRAIN::IElevationObject> eItem =
                            iter->second->getInterface<TERRAIN::IElevationObject>();

                    if(eItem.valid())
                    {
                        vizCore::RefPtr<vizCore::IElevationData> data = eItem->getElevationData();
                        if(data.valid())
                        {
					        std::string modifiedUrl = directoryPath + "/tms.tms";

					        vizCore::RefPtr<vizDB::ReaderWriter::Options> options = new DB::ReaderWriter::Options;

					        options->setMapValue("minLevel", minLevel);
					        options->setMapValue("maxLevel", maxLevel);

                            // Set Object Names to task
                            vizCore::RefPtr<vizCore::IBase> objBase = eItem->getInterface<vizCore::IBase>(true);
                            tmsCreationtask->setObjectNames(objBase->getName());
                            //start the task
                            tmsCreationtask->start();
                            vizDB::writeElevationFile(data.get(), modifiedUrl,options);
                            tmsCreationtask->finish();
					        _status = COMPLETED;
                        }
                    }
                }
            }

        }
        catch(const vizUtil::Exception& e)
        {   
            //Terminate task on exception
            tmsCreationtask->terminate();

            e.LogException();
	        _status = ERRORINCREATION;
            throw TMSCreationException(e.What(), __FILE__, __LINE__);
        }
        catch(...)
        {
            //Terminate task on exception
            tmsCreationtask->terminate();
	        _status = ERRORINCREATION;
        }
    }

	// returns current status of tmsCreation
    void ExportUIHandler::getStatus(TMSCREATIONSTATUS& status)
    {
        status = _status;
    }
}
