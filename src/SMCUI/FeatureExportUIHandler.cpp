#include <Core/IRasterData.h>
#include <Core/IElevationData.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/ICompositeObject.h>
#include <Core/IMetadataField.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IReferenced.h>
#include <Core/IFeatureLayer.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IObjectType.h>
#include <Core/IObjectFactory.h>
#include <Core/IObject.h>
#include <Core/CoreRegistry.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/OGRMetadataRecord.h>
#include <Elements/FeatureLayer.h>
#include <Elements/FeatureObjectUtils.h>
#include <Elements/OGRMetadataTable.h>
#include <Elements/IModelFeatureObject.h>


#include <GIS/IFeatureLayer.h>

#include <DB/WriteFile.h>

#include <SMCUI/FeatureExportUIHandler.h>

#include <osgEarthUtil/WFS>

//#include <VIZUI/IOverlayUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <App/AccessElementUtils.h>

#include <Util/FileUtils.h>

#include <ogrsf_frmts.h>
#include <gdal.h>
#include <ogr_api.h>
#include <ogr_srs_api.h>

#include <sstream>
#include <fstream>
#include <iostream>

#include <osgDB/WriteFile>
#include <DB/ReaderWriter.h>
#include <curl/curl.h>
#include <VizUI/ISelectionUIHandler.h>
#include <DGN/IDGNFolder.h>
#include<Terrain/IModelObject.h>
#include <osgEarthUtil/WMS>
#include <Core/AttributeTypes.h>
#include <boost/filesystem.hpp>
#include <ZipUtil/ZipAndUnzipUtil.h>
#include <Elements/ISettingComponent.h>

namespace SMCUI
{
    DEFINE_IREFERENCED(FeatureExportUIHandler, VizUI::UIHandler);
    DEFINE_META_BASE(SMCUI, FeatureExportUIHandler);

    FeatureExportUIHandler::FeatureExportUIHandler()
        :_georbISServerPort(19093)
    {
        _addInterface(IFeatureExportUIHandler::getInterfaceName());

        setName(getClassname());
    }

    void FeatureExportUIHandler::initializeAttributes()
    {
        UIHandler::initializeAttributes();
        // Add ContextPropertyName attribute
        std::string groupName = "FeatureExportUIHandler attributes";
        _addAttribute(new CORE::IntAttribute("GeorbISServerPort", "GeorbISServerPort",
            CORE::IntAttribute::SetFuncType(this, &FeatureExportUIHandler::setGeorbISServerPort),
            CORE::IntAttribute::GetFuncType(this, &FeatureExportUIHandler::getGeorbISServerPort),
            "GeorbISServer Port",
            groupName));
    }
    void FeatureExportUIHandler::setGeorbISServerPort(int port)
    {
        _georbISServerPort = port;
    }

    void FeatureExportUIHandler::setUploadingServerPort(const std::string& port){
        _uploadingServerPort = port;
    }

    std::string FeatureExportUIHandler::getUploadingServerPort() const{
        return _uploadingServerPort;
    }

    int FeatureExportUIHandler::getGeorbISServerPort() const
    {
        return _georbISServerPort;
    }

    std::string FeatureExportUIHandler::georbISServerFolderDir(std::string& serverIp)
    {
        std::string response;
        std::string folderRequest = "/georbis-internal/data/directory";
        std::string port = std::to_string(_georbISServerPort);
        CURL *curl; CURLcode res;
        curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();

        if (curl) {
            std::string url = serverIp + ":" + port + folderRequest;

            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_string);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
            res = curl_easy_perform(curl);
            curl_easy_cleanup(curl);
            if (res == CURLE_OK) {
                //done

            }
        }
        curl_global_cleanup();
        return response;

    }
    bool FeatureExportUIHandler::georbISServerCapability(std::string& serverUrl)
    {
        //create the capability url
        std::string capabilities = serverUrl + "?service=wms&request=GetCapabilities";
        //query the capabilty url
        osg::ref_ptr<osgEarth::Util::WMSCapabilities> wmsCapabilities = osgEarth::Util::WMSCapabilitiesReader::read(capabilities, NULL);

        if (wmsCapabilities.valid())
        {
            return true;
        }

        capabilities = serverUrl + "?service=WFS&version=1.0.0&request=GetCapabilities";
        //query the capabilty url
        osg::ref_ptr<osgEarth::Util::WFSCapabilities> wfsCapabilities = osgEarth::Util::WFSCapabilitiesReader::read(capabilities, NULL);
        if (wfsCapabilities.valid())
        {
            return true;
        }

        return false;
    }

    void FeatureExportUIHandler::setConvertModel(bool value){
        convertModel = value;
    }

    void FeatureExportUIHandler::zipModel(std::string gltfPath, std::string modelPath, std::string zipName){
        // copy contents to the zip file
        /** If convert model is true, zip gltf file and ive file with ext file in Model name zip**/

        if (convertModel == true){
            // gltf Contents
            osgDB::DirectoryContents gltfContents = osgDB::getDirectoryContents(gltfPath);

            // fbx Contents
            osgDB::DirectoryContents modelContents = osgDB::getDirectoryContents(modelPath);


            // add gltf contents to the zip
            int size = gltfContents.size();

            std::vector<std::string> folderContent;

            int index = 0;
            while (index < size)
            {
                std::string fileName = gltfContents[index++];
                if ((fileName == "..") || (fileName == "."))
                    continue;

                std::string fileToUpload = gltfPath + "/" + fileName;

                folderContent.push_back(fileToUpload);
            }

            // add fbx contents as original folder to the zip file
            size = modelContents.size();

            index = 0;
            while (index < size)
            {
                std::string fileName = modelContents[index++];
                if ((fileName == "..") || (fileName == "."))
                    continue;

                std::string fileToUpload = modelPath + "/" + fileName;

                folderContent.push_back(fileToUpload);
            }

            // create zip file
            std::string zipFile = osgDB::getNameLessAllExtensions(zipName) + ".zip";
            std::string zipFileName = gltfPath + "/" + zipFile;
            // copy contents to the zip file

            ZipUtil::ZipAndUnzipUtil::addFilesToZip(folderContent, zipFileName);
        }
        else{
            std::vector<std::string> folderContent;
            folderContent.push_back(modelPath);
            folderContent.push_back(gltfPath + "/ExtensionFile.ext");

            std::string zipFile = osgDB::getNameLessAllExtensions(zipName) + ".zip";
            std::string zipFileName = gltfPath + "/" + zipFile;

            ZipUtil::ZipAndUnzipUtil::addFilesToZip(folderContent, zipFileName);
        }
    }

    bool FeatureExportUIHandler::exportGeoLayer(std::string& serverIp)
    {
        bool isSuccess = false;
        std::string port = std::to_string(_georbISServerPort);
        std::string serviceType = "";
        //std::string folderDirPath = georbISServerFolderDir(serverIp); // will use in Future
        std::string response;
        std::string dgnCacheRepoPath = "";
        std::string DGNFileNameWithoutExtn = "";
        std::string zipPathForModel = "";
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        // get the model directory path
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getManager());

        ELEMENTS::ISettingComponent* _settingComponent;

        if (wmain.valid())
        {
            CORE::IComponent* component = wmain->getComponentByName("SettingComponent");
            if (component)
            {
                _settingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
            }
        }
        const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();
        CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();
        if (iter != map.end()) {
            CORE::RefPtr<CORE::IObject> object = iter->second->getInterface<CORE::IObject>();
            std::string name = "";
            if (object->getInterface<TERRAIN::IRasterObject>())
            {
                name = object->getInterface<TERRAIN::IRasterObject>()->getURL();
                serviceType = "raster";
            }
            else if (object->getInterface<TERRAIN::IElevationObject>())
            {
                name = object->getInterface<TERRAIN::IElevationObject>()->getURL();
                serviceType = "elevation";
            }
            else if (object->getInterface<TERRAIN::IModelObject>())
            {
                name = object->getInterface<TERRAIN::IModelObject>()->getUrl();
                serviceType = "vector";
            }
            else if (object->getInterface<DGN::IDGNFolder>())
            {
                name = object->getInterface<DGN::IDGNFolder>()->getURL();
                serviceType = "dgn";
                std::string simpleFileName = osgDB::getSimpleFileName(name);
                DGNFileNameWithoutExtn = osgDB::getNameLessAllExtensions(simpleFileName);
                dgnCacheRepoPath = UTIL::getDGNCacheRepo() + "/" + DGNFileNameWithoutExtn;
            }
            else if (object->getInterface<ELEMENTS::IModelFeatureObject>()){
                // service to export MOdels Feature Object
                name = object->getInterface<ELEMENTS::IModelFeatureObject>()->getModelFilename();

                //std::string command;
                std::string cName = osgDB::convertFileNameToNativeStyle(name);
                std::string fName = osgDB::getSimpleFileName(cName);
                std::string fNameWithoutExt = osgDB::getNameLessAllExtensions(fName);
                char command[100];
                std::string outputDir = UTIL::TemporaryFolder::instance()->getAppFolderPath();

                if (convertModel == true){
                    boost::filesystem::path tempDir(outputDir + "\\Temp_Models");

                    if (boost::filesystem::create_directory(tempDir)){
                        boost::filesystem::path fbxDir(outputDir + "\\Temp_Models\\fbxModels");
                        int res;
                        std::stringstream convCommand;
                        if (boost::filesystem::create_directory(fbxDir)){
                            std::string fbxPath = boost::filesystem::path(outputDir + "\\Temp_Models\\fbxModels").string() + "\\" + fNameWithoutExt + ".fbx";


                            std::string inputPath;
                            if(_settingComponent)
                                inputPath = _settingComponent->getDataDirectoryPath() + "\\" + name;
                            else
                                inputPath = outputDir + "\\data\\LandmarkSymbols\\" + name;
#ifdef _DEBUG
                            convCommand << "osgconvd.exe --compressed " << inputPath << " " << fbxPath;
#else
                            convCommand << "osgconv.exe --compressed " << inputPath << " " << fbxPath;
#endif

                            // executing osgconv command
                            res = system(convCommand.str().c_str());
                        }
                        // if the osgconv command successfully executed
                        if (res != -1){
                            boost::filesystem::path gltfDir(outputDir + "\\Temp_Models\\gltf");
                            //if (boost::filesystem::create_directory(gltfDir)){
                            std::string inputFbxPath = outputDir + "\\Temp_Models\\fbxModels" + "\\" + fNameWithoutExt + ".fbx";

                            // fbx directory path
                            std::string fbxpath = fbxDir.string();

                            if (boost::filesystem::exists(boost::filesystem::path(inputFbxPath))){
                                std::string gltfPath = gltfDir.string();

                                std::stringstream resCommand;
                                std::string dataDir = UTIL::getDataDirPath();
                                resCommand << dataDir + "/tools/fbx2gltf/gltf.exe" << " -f " << inputFbxPath << " -o " << gltfPath << " -c";
                                int resGLTF = system(resCommand.str().c_str());

                                if (resGLTF != -1){
                                    rename((gltfPath + "\\" + fNameWithoutExt + ".gltf").c_str(), (gltfPath + "\\" + fNameWithoutExt + ".json").c_str());

                                    // get the contents of the directory
                                    std::string inputPath;
                                    if (_settingComponent)
                                        inputPath = _settingComponent->getDataDirectoryPath() + "\\" + name;
                                    else
                                        inputPath = outputDir + "\\data\\LandmarkSymbols\\" + name;
                                    // get the folder of the model 
                                    std::string modelPath = osgDB::getFilePath(inputPath);

                                    std::string extension = osgDB::getFileExtension(inputPath);

                                    std::string extensionFilePath = gltfPath + "/ExtensionFile.ext";

                                    std::ofstream fileStream;
                                    // create a file with name ExtensionFile.ext
                                    fileStream.open(extensionFilePath);

                                    // write json to the file 
                                    std::string toWrite = "{\"extension\" : \"" + extension + "\"}";
                                    fileStream.write(toWrite.c_str(), toWrite.size());

                                    fileStream.close();

                                    zipModel(gltfPath, modelPath, fNameWithoutExt);
                                    zipPathForModel = gltfPath;
                                }
                            }
                            //}
                        }
                    }
                }
                else{
                    boost::filesystem::path tempDir(outputDir + "\\Temp_Models");
                    if (boost::filesystem::create_directory(tempDir)){
                        std::string inputPath;
                        if (_settingComponent)
                            inputPath = _settingComponent->getDataDirectoryPath() + "\\" + name;
                        else
                            inputPath = outputDir + "\\data\\LandmarkSymbols\\" + name;
                        // save the model's extension in extension.ext file
                        std::string extension = osgDB::getFileExtension(inputPath);

                        std::string extensionFilePath = tempDir.string() + "/ExtensionFile.ext";

                        std::ofstream fileStream;
                        // create a file with name ExtensionFile.ext
                        fileStream.open(extensionFilePath);

                        // write json to the file 
                        std::string toWrite = "{\"extension\" : \"" + extension + "\"}";
                        fileStream.write(toWrite.c_str(), toWrite.size());

                        fileStream.close();

                        zipModel(tempDir.string(), inputPath, fNameWithoutExt);
                        zipPathForModel = tempDir.string();
                    }
                }

                //boost::filesystem::path tempDir(outputDir + "\\Temp_Models");
                //if (boost::filesystem::create_directory(tempDir)){
                //    std::string inputPath = outputDir + "\\data\\LandmarkSymbols\\" + name;

                //    // save the model's extension in extension.ext file
                //    std::string extension = osgDB::getFileExtension(inputPath);

                //    std::string extensionFilePath = tempDir.string() + "/ExtensionFile.ext";

                //    std::ofstream fileStream;
                //    // create a file with name ExtensionFile.ext
                //    fileStream.open(extensionFilePath);

                //    // write json to the file 
                //    std::string toWrite = "{\"extension\" : \"" + extension + "\"}";
                //    fileStream.write(toWrite.c_str(), toWrite.size());

                //    fileStream.close();

                //    zipModel(tempDir.string(), inputPath, fNameWithoutExt);
                //    zipPathForModel = tempDir.string();
                //}



                serviceType = "model";
            }
            else
            {
                return isSuccess;
            }
            name = osgDB::convertFileNameToNativeStyle(name);
            std::string simpleFileName = osgDB::getSimpleFileName(name);
            std::string simpleFileNameWithoutExtn = osgDB::getNameLessAllExtensions(simpleFileName);
            std::string filePathWithoutFileName = osgDB::getFilePath(name);
            filePathWithoutFileName = osgDB::convertFileNameToNativeStyle(filePathWithoutFileName);
            CURL *curl;
            curl = curl_easy_init();
            struct curl_httppost *post = NULL;
            struct curl_httppost *last = NULL;
            CURLcode res;
            if (curl)
            {
                if (serviceType == "vector")
                {
                    curl_formadd(&post, &last,
                        CURLFORM_COPYNAME, (simpleFileNameWithoutExtn + ".shx").c_str(),
                        CURLFORM_FILE, (filePathWithoutFileName + "/" + (simpleFileNameWithoutExtn + ".shx")).c_str(),
                        CURLFORM_END);
                    curl_formadd(&post, &last,
                        CURLFORM_COPYNAME, (simpleFileNameWithoutExtn + ".dbf").c_str(),
                        CURLFORM_FILE, (filePathWithoutFileName + "/" + (simpleFileNameWithoutExtn + ".dbf")).c_str(),
                        CURLFORM_END);
                    curl_formadd(&post, &last,
                        CURLFORM_COPYNAME, (simpleFileNameWithoutExtn + ".prj").c_str(),
                        CURLFORM_FILE, (filePathWithoutFileName + "/" + (simpleFileNameWithoutExtn + ".prj")).c_str(),
                        CURLFORM_END);
                    curl_formadd(&post, &last,
                        CURLFORM_COPYNAME, (simpleFileNameWithoutExtn + ".shp").c_str(),
                        CURLFORM_FILE, (filePathWithoutFileName + "/" + (simpleFileNameWithoutExtn + ".shp")).c_str(),
                        CURLFORM_END);
                }

                if (serviceType == "dgn")
                {
                    curl_formadd(&post, &last,
                        CURLFORM_COPYNAME, "dgnName",
                        CURLFORM_COPYCONTENTS, DGNFileNameWithoutExtn.c_str(),
                        CURLFORM_END);

                    std::vector<std::string> styleTemplates;
                    std::vector<std::string> vstrExts;
                    vstrExts.push_back("dbf");
                    vstrExts.push_back("shx");
                    vstrExts.push_back("shp");
                    vstrExts.push_back("prj");
                    vstrExts.push_back("json");
                    UTIL::getFilesFromFolder(dgnCacheRepoPath, styleTemplates, vstrExts, true);

                    for (int i = 0; i < styleTemplates.size(); i++)
                    {
                        std::string templateFile = UTIL::getFileName(styleTemplates[i]);
                        curl_formadd(&post, &last,
                            CURLFORM_COPYNAME, templateFile.c_str(),
                            CURLFORM_FILE, styleTemplates[i].c_str(),
                            CURLFORM_END);
                    }

                }
                else if (serviceType == "model"){
                    curl_formadd(&post, &last,
                        CURLFORM_COPYNAME, "ModelFolder",
                        CURLFORM_FILE, (zipPathForModel + "/" + (simpleFileNameWithoutExtn + ".zip")).c_str(),
                        CURLFORM_END);
                }
                else
                {
                    curl_formadd(&post, &last,
                        CURLFORM_COPYNAME, simpleFileName.c_str(),
                        CURLFORM_FILE, name.c_str(),
                        CURLFORM_END);
                }

                curl_formadd(&post, &last,
                    CURLFORM_COPYNAME, "type",
                    CURLFORM_COPYCONTENTS, serviceType.c_str(),
                    CURLFORM_END);
                curl_formadd(&post, &last,
                    CURLFORM_COPYNAME, "subPath",
                    CURLFORM_COPYCONTENTS, "georbis",
                    CURLFORM_END);
                struct curl_slist *headers = curl_slist_append(NULL, "Content-Type:multipart/form-data;boundary=--4ebf00fbcf09");
                curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
                std::string url = serverIp + ":" + _uploadingServerPort + "/georbis/internal/dsm/upload";

                if (serviceType == "model"){
                    curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_BASIC);
                    curl_easy_setopt(curl, CURLOPT_USERNAME, "admin");
                    curl_easy_setopt(curl, CURLOPT_PASSWORD, "admin");

                    url = serverIp + ":" + _uploadingServerPort + "/georbis/VMCC/Model3D/upload";
                }

                curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
                curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

                res = curl_easy_perform(curl);
                curl_easy_cleanup(curl);
                if (res == CURLE_OK)
                {
                    isSuccess = true;
                }

                curl_formfree(post);
                /* free slist */
                curl_slist_free_all(headers);
            }
            curl_global_cleanup();
        }
        boost::filesystem::remove_all(boost::filesystem::path(UTIL::TemporaryFolder::instance()->getAppFolderPath() + "\\Temp_Models"));
        return isSuccess;

    }
    bool FeatureExportUIHandler::exportFeature(const std::string& filename)
    {
        CORE::RefPtr<CORE::IFeatureObject> item =
            _getCurrentSelectedItem<CORE::IFeatureObject>();
        if (item.valid())
        {
            return DB::writeFeatureFile(item.get(), filename);
        }

        CORE::RefPtr<GIS::IFeatureLayer> featuteLayer = _getCurrentSelectedItem<GIS::IFeatureLayer>();
        if (featuteLayer.valid())
        {
            DB::ReaderWriter::Options* options = new DB::ReaderWriter::Options();
            options->setMapValue("fileName", filename);
            options->setMapValue("Editable", UTIL::ToString(false));
            LOG_INFO("Unfinished feature operation: Export feature to test.pggis");
            return DB::writeObjectFile(featuteLayer->getInterface<CORE::IObject>(), "test.pggis", options);
        }
        return false;
    }

    bool FeatureExportUIHandler::exportPointLayerToCSV(const std::string& filename)
    {
        CORE::RefPtr<CORE::IFeatureLayer> layer = _getCurrentSelectedItem<CORE::IFeatureLayer>();
        if (!layer.valid())
        {
            return false;
        }

        if (layer->getFeatureLayerType() != CORE::IFeatureLayer::POINT)
        {
            return false;
        }

        CORE::RefPtr<CORE::ICompositeObject> composite = layer->getInterface<CORE::ICompositeObject>();
        if (!composite.valid())
        {
            return false;
        }

        CORE::RefPtr<CORE::IMetadataTableHolder> holder =
            layer->getInterface<CORE::IMetadataTableHolder>();
        if (!holder.valid())
        {
            return false;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn =
            holder->getMetadataTable()->getMetadataTableDefn();

        if (!tableDefn.valid())
        {
            return false;
        }

        std::vector<std::string> attrNames;

        int tableColumnCount = tableDefn->getFieldCount();
        for (int i = 0; i < tableColumnCount; i++)
        {
            CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = tableDefn->getFieldDefnByIndex(i);
            if (fieldDefn.valid())
            {
                std::string name = fieldDefn->getName();
                if (name.find("_VIZ_") == std::string::npos)
                {
                    attrNames.push_back(fieldDefn->getName());
                }
            }
        }


        std::ofstream outputfile(filename.c_str());

        outputfile << "Latitude ,Longitude ,Altitude ";

        for (unsigned int i = 0; i < attrNames.size(); i++)
        {
            outputfile << "," << attrNames[i].c_str();
        }

        outputfile << std::endl;


        const CORE::ICompositeObject::ObjectMap& objectMap = composite->getObjectMap();

        CORE::ICompositeObject::ObjectMap::const_iterator iter = objectMap.begin();
        for (; iter != objectMap.end(); iter++)
        {
            CORE::RefPtr<CORE::IPoint> point = iter->second->getInterface<CORE::IPoint>();
            if (point.valid())
            {
                outputfile << point->getY() << "," << point->getX() << "," << point->getZ();

                for (unsigned int i = 0; i < attrNames.size(); i++)
                {
                    CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder =
                        point->getInterface<CORE::IMetadataRecordHolder>();

                    if (recordHolder.valid())
                    {
                        CORE::RefPtr<CORE::IMetadataRecord> record = recordHolder->getMetadataRecord();

                        CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName(attrNames[i]);

                        if (field.valid())
                        {
                            CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = field->getMetadataFieldDef();

                            if (fieldDefn.valid())
                            {
                                CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                                switch (type)
                                {
                                case CORE::IMetadataFieldDefn::STRING:
                                {
                                    outputfile << "," << field->toString();
                                }
                                break;
                                case CORE::IMetadataFieldDefn::INTEGER:
                                {
                                    outputfile << "," << field->toInt();
                                }
                                break;
                                case CORE::IMetadataFieldDefn::DOUBLE:
                                {
                                    outputfile << "," << field->toDouble();
                                }
                                }
                            }
                        }
                    }
                }

                outputfile << std::endl;
            }
        }

        outputfile.close();

        return true;
    }

    bool FeatureExportUIHandler::exportRaster(const std::string& filename)
    {
        CORE::RefPtr<TERRAIN::IRasterObject> item =
            _getCurrentSelectedItem<TERRAIN::IRasterObject>();
        if (item.valid())
        {
            CORE::RefPtr<CORE::IRasterData> data = item->getRasterData();
            if (data.valid())
            {
                return DB::writeRasterFile(data.get(), filename);
            }
        }
        return false;
    }

    bool FeatureExportUIHandler::exportFeatureToTin(const std::string& filename)
    {
        CORE::RefPtr<CORE::IFeatureObject> item =
            _getCurrentSelectedItem<CORE::IFeatureObject>();
        if (item.valid())
        {
            osg::ref_ptr<osg::Node> featureNode = item->getInterface<CORE::IObject>()->getOSGNode();
            if (featureNode.valid())
            {
                return osgDB::writeNodeFile(*(featureNode.get()), filename);
            }
        }
        return false;
    }

    bool FeatureExportUIHandler::exportFeatureToRaster(const std::string& filename, const osg::Vec4& color, const osg::Vec4& nodataColor, double resolution)
    {
        CORE::RefPtr<CORE::IFeatureObject> item =
            _getCurrentSelectedItem<CORE::IFeatureObject>();
        if (item.valid())
        {
            UTIL::TemporaryFolder* tempFolder = UTIL::TemporaryFolder::instance();
            std::string tempFeatureFile = "FeatureToRaster.shp";
            tempFolder->deleteFile(tempFeatureFile);
            std::string fullTempFilename = tempFolder->getPath() + "/" + tempFeatureFile;

            exportFeature(fullTempFilename);

            _createOutputFile(fullTempFilename, filename, color, nodataColor, resolution);

            OGRDataSourceH hSrcDS;

            hSrcDS = OGROpen(fullTempFilename.c_str(), FALSE, NULL);
            if (hSrcDS == NULL)
            {
                return false;
            }

            OGRLayerH hLayer = OGR_DS_GetLayer(hSrcDS, 0);
            if (hLayer == NULL)
            {
                return false;
            }

            OGRFeatureDefnH featureDefn = OGR_L_GetLayerDefn(hLayer);
            if (featureDefn == NULL)
            {
                return false;
            }
            std::string layerName = OGR_FD_GetName(featureDefn);
            OGR_DS_Destroy(hSrcDS);

            std::stringstream command;

            command << "gdal_rasterize -b 1 -b 2 -b 3 -b 4 ";
            command << "-burn " << (int)(color.r() * 255) << " ";
            command << "-burn " << (int)(color.g() * 255) << " ";
            command << "-burn " << (int)(color.b() * 255) << " ";
            command << "-burn " << (int)(color.a() * 255) << " ";
            command << "-l " << layerName << " ";
            command << "\"" << fullTempFilename << "\" \"" << filename << "\"";
            system(command.str().c_str());
        }
        return false;
    }

    bool FeatureExportUIHandler::exportElevation(const std::string& filename)
    {
        CORE::RefPtr<TERRAIN::IElevationObject> item =
            _getCurrentSelectedItem<TERRAIN::IElevationObject>();
        if (item.valid())
        {
            CORE::RefPtr<CORE::IElevationData> data = item->getElevationData();
            if (data.valid())
            {
                return DB::writeElevationFile(data.get(), filename);
            }
        }
        return false;
    }

    bool FeatureExportUIHandler::exportFeatureToGeoVrml(const std::string& filename)
    {
        /*CORE::RefPtr<ELEMENTS::IGeoVrmlObject> item =
        _getCurrentSelectedItem<ELEMENTS::IGeoVrmlObject>();*/
        CORE::RefPtr<CORE::IObject> item =
            _getCurrentSelectedItem<CORE::IObject>();
        if (item.valid())
        {
            //osg::ref_ptr<osg::Node> node = item->getInterface<CORE::IObject>()->getOSGNode();
            osg::ref_ptr<osg::Node> node = item->getOSGNode();
            if (node.valid())
            {
                return osgDB::writeNodeFile(*(node.get()), filename);
            }
        }
        return false;
    }

    template <class T>
    T*
        FeatureExportUIHandler::_getCurrentSelectedItem()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getManager());

        const CORE::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if (iter != map.end())
        {
            return iter->second->getInterface<T>();
        }
        return NULL;
    }

    bool FeatureExportUIHandler::exportLineToCSV(const std::string& filename)
    {
        CORE::RefPtr<CORE::ILine> line = _getCurrentSelectedItem<CORE::ILine>();
        if (line.valid())
        {
            unsigned int numPoints = line->getPointNumber();

            std::string name = line->getInterface<CORE::IBase>()->getName();

            std::ofstream outputfile(filename.c_str());

            outputfile << "Name ,Latitude ,Longitude ,Altitude";
            outputfile << std::endl;

            for (unsigned int i = 0; i < numPoints; i++)
            {
                osg::Vec3d point = line->getValueAt(i);

                std::string pointName = name + UTIL::ToString<int>(i + 1);

                outputfile << pointName.c_str() << "," << point.y() << "," << point.x() << "," << point.z();
                outputfile << std::endl;
            }

            outputfile.close();

            return true;
        }

        return false;
    }

    void FeatureExportUIHandler::_createOutputFile(const std::string& fullTempFilename,
        const std::string& filename,
        const osg::Vec4& color,
        const osg::Vec4& nodataColor,
        double resolution)
    {
        GDALDatasetH hDstDS = NULL;
        int bandCount = 4;
        OGREnvelope sLayerEnvelop;
        const char* driverName = "GTiff";
        bool bFirstLayer = true;
        OGREnvelope sEnvelop;
        OGRSpatialReferenceH hSRS = NULL;
        double nXSize = 0.0, nYSize = 0.0;

        OGRDataSourceH hSrcDS;

        hSrcDS = OGROpen(fullTempFilename.c_str(), FALSE, NULL);
        if (hSrcDS == NULL)
        {
            return;
        }

        OGRLayerH hLayer = OGR_DS_GetLayer(hSrcDS, 0);
        if (hLayer == NULL)
        {
        }

        GDALDriverH hDriver = GDALGetDriverByName(driverName);
        if (hDriver == NULL)
        {
            return;
        }

        if (OGR_L_GetExtent(hLayer, &sLayerEnvelop, FALSE) != OGRERR_NONE)
        {
            return;
        }

        if (bFirstLayer)
        {
            sEnvelop.MinX = sLayerEnvelop.MinX;
            sEnvelop.MinY = sLayerEnvelop.MinY;
            sEnvelop.MaxX = sLayerEnvelop.MaxX;
            sEnvelop.MaxY = sLayerEnvelop.MaxY;

            hSRS = OGR_L_GetSpatialRef(hLayer);

            bFirstLayer = FALSE;
        }
        else
        {
            sEnvelop.MinX = MIN(sEnvelop.MinX, sLayerEnvelop.MinX);
            sEnvelop.MinY = MIN(sEnvelop.MinY, sLayerEnvelop.MinY);
            sEnvelop.MaxX = MAX(sEnvelop.MaxX, sLayerEnvelop.MaxX);
            sEnvelop.MaxY = MAX(sEnvelop.MaxY, sLayerEnvelop.MaxY);
        }

        double adfProjection[6];
        sEnvelop.MinX = floor(sEnvelop.MinX / resolution) * resolution;
        sEnvelop.MaxX = ceil(sEnvelop.MaxX / resolution) * resolution;
        sEnvelop.MinY = floor(sEnvelop.MinY / resolution) * resolution;
        sEnvelop.MaxY = ceil(sEnvelop.MaxY / resolution) * resolution;

        adfProjection[0] = sEnvelop.MinX;
        adfProjection[1] = resolution;
        adfProjection[2] = 0;
        adfProjection[3] = sEnvelop.MaxY;
        adfProjection[4] = 0;
        adfProjection[5] = -resolution;
        nXSize = (int)(0.5 + (sEnvelop.MaxX - sEnvelop.MinX) / resolution);
        nYSize = (int)(0.5 + (sEnvelop.MaxY - sEnvelop.MinY) / resolution);

        hDstDS = GDALCreate(hDriver, filename.c_str(), nXSize, nYSize,
            bandCount, GDT_Byte, NULL);
        if (hDstDS == NULL)
        {
            return;
        }

        GDALSetGeoTransform(hDstDS, adfProjection);

        char* pszWKT = NULL;
        if (hSRS)
        {
            OSRExportToWkt(hSRS, &pszWKT);
        }
        if (pszWKT)
        {
            GDALSetProjection(hDstDS, pszWKT);
        }
        CPLFree(pszWKT);

        for (int iBand = 0; iBand < bandCount; iBand++)
        {
            GDALRasterBandH hBand = GDALGetRasterBand(hDstDS, iBand + 1);
            GDALFillRaster(hBand, (unsigned int)(nodataColor[iBand] * 255), 0);
        }

        OGR_DS_Destroy(hSrcDS);
        GDALClose(hDstDS);

    }

    size_t FeatureExportUIHandler::write_to_string(void *ptr, size_t size, size_t count, void *stream) {
        ((std::string*)stream)->append((char*)ptr, 0, size*count);
        return size*count;
    }

}
