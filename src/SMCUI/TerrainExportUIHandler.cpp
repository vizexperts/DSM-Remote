#include <SMCUI/TerrainExportUIHandler.h>

#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>
#include <Core/IElevationData.h>
#include <Core/IFeatureObject.h>
#include <Core/IFeatureLayer.h>
#include <Core/ITerrain.h>
#include <Core/IGeoExtent.h>
#include <Core/InterfaceUtils.h>
#include <Core/WorldMaintainer.h>
#include <CORE/ICompositeObject.h>

#include <Elements/ElementsPlugin.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IModelObject.h>
#include <Terrain/IOGRLayerHolder.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IFilterStatusMessage.h>

#include <VizUI/VizUIPlugin.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>

#include <Util/CoordinateConversionUtils.h>
#include <Util/DistanceBearingCalculationUtils.h>

#include <Util/Log.h>
#include <Util/GdalUtils.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <DB/WriteFile.h>

#include <osg/Vec3d>

#include <osgEarth/GeoMath>

#include <gdal.h>
#include <gdal_priv.h>

#include <libjson.h>
#include <map>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/algorithm/string.hpp>

#include <osgearth/SpatialReference>
#include <GIS/SpatialReference.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <DGN/IDGNFolder.h>
#include <Util/Exception.h>
#include <boost/filesystem.hpp>
//Controls whether the export process will run in the main thread itself or 
// in seperate multiple threads.
// only to be used for the purpose of debugging 
#define EXPORT_VECTOR 1
#define EXPORT_VECTOR_AS_JSON 1
#define EXPORT_VECTOR_AS_MASK 1
#define EXPORT_RASTER 1
#define EXPORT_ELEVATION 1



namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, TerrainExportUIHandler);
    DEFINE_IREFERENCED(TerrainExportUIHandler, VizUI::UIHandler);
#define EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(operation, key)\
{\
    if (!operation)\
    {\
        _statusMap[key].second = LayerStatus::FAILED;\
        LOG_ERROR("Failed operation on: " + key);\
        return false;\
    }\
}

#define EXECUTE_OPERATION(operation, key)\
{\
    if (!operation)\
    {\
        LOG_ERROR("Failed operation : " + key);\
        return false;\
    }\
}


    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    TerrainExportUIHandler::TerrainExportUIHandler()
    {
        _addInterface(ITerrainExportUIHandler::getInterfaceName());
        setName(getClassname());
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _reset();
    }
    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    TerrainExportUIHandler::~TerrainExportUIHandler(){}
    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void TerrainExportUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }
    void TerrainExportUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void TerrainExportUIHandler::setSideLength(int sideLength)
    {
        _sideLength = sideLength;
    }
    int TerrainExportUIHandler::getSideLength()
    {
        return _sideLength;
    }
    void TerrainExportUIHandler::setLongSpan(double longspan)
    {
        _longspan = longspan;
    }
    double TerrainExportUIHandler::getLongSpan()
    {
        return _longspan;
    }
    void TerrainExportUIHandler::setLatSpan(double latspan)
    {
        _latspan = latspan;
    }
    double TerrainExportUIHandler::getLatSpan()
    {
        return _latspan;
    }
    void TerrainExportUIHandler::setTileSize(int tilesize)
    {
        _tilesize = tilesize;
    }
    int TerrainExportUIHandler::getTileSize()
    {
        return _tilesize;
    }
    void TerrainExportUIHandler::setOutDir(std::string outDir)
    {
        _outDir = outDir;
    }
    std::string TerrainExportUIHandler::getOutDir()
    {
        return _outDir;
    }

    void TerrainExportUIHandler::setTempDir(std::string tempDir)
    {
        _tempDir = tempDir;
    }
    std::string TerrainExportUIHandler::getTempDir()
    {
        return _tempDir;
    }
    void TerrainExportUIHandler::setClassificationMap(std::map < std::string, std::string >& map)
    {
        _classificationMap = map;
    }
    std::map < std::string, std::string >& TerrainExportUIHandler::getClassificationMap()
    {
        return _classificationMap;
    }
    std::map < std::string, TerrainExportUIHandler::Fileinfo >& TerrainExportUIHandler::getStatusMap()
    {
        return _statusMap;
    }
    std::string TerrainExportUIHandler::getLastCompletedLayer()
    {
        return _lastCompletedLayer;
    }
    /** Set the center point */
    void TerrainExportUIHandler::setCenter(osg::Vec3d center)
    {
        _center = center;
    }
    osg::Vec3d TerrainExportUIHandler::getCenter() const
    {
        return _center;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void TerrainExportUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }
    void TerrainExportUIHandler::cancelExport(bool cancel)
    {
        //This variable is used in the inline function hasUserCancelled(bool), defined in the header.
        _cancelExport = cancel;
    }
    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool TerrainExportUIHandler::getFocus() const
    {
        return false;
    }
    osg::Vec4d TerrainExportUIHandler::_getOtherDiagonal(osg::Vec4d extents)
    {
        double ulx = extents.x();
        double uly = extents.w();
        double lrx = extents.z();
        double lry = extents.y();

        return osg::Vec4d(ulx, uly, lrx, lry);
    }

    double TerrainExportUIHandler::_getVerticalRange(std::string infile)
    {
        if (!fs::exists(infile) || !fs::is_regular_file(infile))
        {
            return 0.0f;
        }

        osg::Vec2d minmax = UTIL::GDALUtils::getMinMaxHeight(infile);
        return minmax.y() - minmax.x();
    }

    double TerrainExportUIHandler::_getSpanWestEast(osg::Vec4d ext)
    {
        double upperLeftX = ext.x();
        double upperLeftY = ext.y();
        double lowerRightX = ext.z();
        double lowerRightY = ext.w();
        const osgEarth::SpatialReference* sref = osgEarth::SpatialReference::get(UTIL::GDALUtils::UTM_ZONE43_SREF);
        double dis1 = osgEarth::GeoMath::distance(osg::Vec3d(upperLeftX, upperLeftY, 0.0), osg::Vec3d(lowerRightX, upperLeftY, 0.0), sref);
        double dis2 = osgEarth::GeoMath::distance(osg::Vec3d(upperLeftX, lowerRightY, 0.0), osg::Vec3d(lowerRightX, lowerRightY, 0.0), sref);

        //std::cout << "Diff in horizontral spans" << abs(dis1 - dis2) << std::endl;

        return (dis1 + dis2) / 2;
    }
    double TerrainExportUIHandler::_getSpanNorthSouth(osg::Vec4d ext)
    {
        double upperLeftX = ext.x();
        double upperLeftY = ext.y();
        double lowerRightX = ext.z();
        double lowerRightY = ext.w();

        const osgEarth::SpatialReference* sref = osgEarth::SpatialReference::get(UTIL::GDALUtils::UTM_ZONE43_SREF);
        double dis1 = osgEarth::GeoMath::distance(osg::Vec3d(upperLeftX, upperLeftY, 0.0), osg::Vec3d(upperLeftX, lowerRightY, 0.0), sref);
        double dis2 = osgEarth::GeoMath::distance(osg::Vec3d(lowerRightX, upperLeftY, 0.0), osg::Vec3d(lowerRightX, lowerRightY, 0.0), sref);
        //std::cout << "Diff in vertical spans" << abs(dis1 - dis2) << std::endl;
        return (dis1 + dis2) / 2;
    }
    osg::Vec2i TerrainExportUIHandler::_getOutResolution(osg::Vec2i inReso)
    {
        int noOfTiles = (std::max(inReso.x(), inReso.y()) / _tilesize) + 1;
        int noOfOverlappingPixels = noOfTiles - 1;
        int side = _tilesize * noOfTiles - noOfOverlappingPixels;

        return osg::Vec2i(side, side);
    }

    bool TerrainExportUIHandler::_writeInfoFile(std::string jsonfile, std::map <std::string, std::string> infomap)
    {
        LOG_INFO("...ENTRY... " + fs::path(jsonfile).string());
        if (!deleteFile(jsonfile)) return false;
        fs::create_directories(fs::path(jsonfile).parent_path());

        JSONNode mainNode(JSON_NODE);
        std::map<std::string, std::string>::const_iterator it(infomap.begin());
        for (; it != infomap.end(); ++it)
        {
            mainNode.push_back(JSONNode(it->first, it->second));
        }

        std::string value = mainNode.write_formatted();

        std::ofstream out(jsonfile.c_str());
        out << value;
        out.close();
        LOG_INFO("...EXIT... " + fs::path(jsonfile).string());
        return true;
    }
    bool TerrainExportUIHandler::_offsetFile(std::string infile)
    {
        if (!fileExists(infile)) return false;

        double min, max;

        cv::Mat image = cv::imread(infile, CV_LOAD_IMAGE_UNCHANGED);
        if (image.empty())
        {
            LOG_ERROR("Cannot load image " + infile);
            return false;
        }

        cv::minMaxIdx(image, &min, &max);
        int rows = image.rows;
        int cols = image.cols;
        int channels = image.channels();
        int type = image.type();

        cv::Mat baseimage(rows, cols, type, cv::Scalar(SHRT_MAX - min));
        cv::Mat newimage = image + baseimage;
        fs::path newimagepath = fs::path(infile).parent_path() / fs::path("offset" + fs::path(infile).filename().string());
        fs::create_directories(fs::path(infile).parent_path());
        if (!cv::imwrite(newimagepath.string(), newimage))
        {
            LOG_ERROR("Could not write image file" + newimagepath.string());
            return false;
        }
        return true;
    }
    bool TerrainExportUIHandler::_offsetAllFiles(std::string dir)
    {
        if (!dirExists(dir)) return false;
        //get all the filenames in the given directory
        std::vector<std::string> fileNames;
        std::vector<std::string> exts;
        exts.push_back("png");

        if (!UTIL::getFilesFromFolder(dir, fileNames, exts))
        {
            LOG_ERROR("Could not get files from folder " + dir);
            return false;
        }

        std::vector<std::string>::iterator fileNamesIterator = fileNames.begin();

        for (; fileNamesIterator != fileNames.end(); ++fileNamesIterator)
        {
            if (!_offsetFile(*fileNamesIterator))
            {
                LOG_ERROR("Can not offset file : " + *fileNamesIterator);
                return false;
            }
            if (!deleteFile(*fileNamesIterator)) return false;
        }

        return true;
    }

    void TerrainExportUIHandler::populateVectorAttributes(std::string vectorFile, std::vector < std::string>& abbtibuteList)
    {
        UTIL::GDALUtils::getVectorAttributes(vectorFile, abbtibuteList);
    }

    void TerrainExportUIHandler::generateRasterFromVector(std::string inputFile, std::vector<std::string>& attributes, std::string outDir, osg::Vec4d extents, osg::Vec2d resolution, int* statusUpdate)
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
        osg::ref_ptr<UTIL::ThreadPool> tp = worldMaintainer->getComputeThreadPool();
        tp->invoke(boost::bind(&TerrainExportUIHandler::generateRasterAttributeBased, this, inputFile, attributes, outDir, extents, resolution, statusUpdate));

    }

    void TerrainExportUIHandler::generateRasterAttributeBased(std::string inputFile, std::vector<std::string>& attributes, std::string outDir, osg::Vec4d extents, osg::Vec2d resolution, int* statusUpdate)
    {

        //entertain only one thread at a time 
        if (_rasterExportInProgress)
            return;
        else
            _rasterExportInProgress = true; 

        *statusUpdate = 0;

        //create rasterizeCatche directery
        std::string appData = UTIL::getAppDataPath();
        std::string rasterizeCatcheDir = appData + "/rasterizeCatche/";

        //delete rasterizeCatche directory if it already exist
        boost::filesystem::path folder(rasterizeCatcheDir);
        if (boost::filesystem::exists(folder))
        {
           boost::filesystem::remove_all(folder) > 0;
        }

        osgDB::makeDirectory(rasterizeCatcheDir);

        inputFile = osgDB::convertFileNameToNativeStyle(inputFile);

        std::string strippedFileName = osgDB::getStrippedName(inputFile);

        std::string cacheShp = rasterizeCatcheDir + strippedFileName + ".shp";

        OGRDataSource *ogrDS = OGRSFDriverRegistrar::Open(inputFile.c_str());

        //accessing only first layer 
        if (ogrDS && ogrDS->GetLayerCount() > 0)
        {
            OGRLayer *ogrLayer = ogrDS->GetLayer(0);

            OGRFeature *ogrFeature = NULL;
            OGRSpatialReference* sourceSRS = ogrLayer->GetSpatialRef();
            OGRwkbGeometryType sourceGeom = ogrLayer->GetGeomType();

            //create new shp file 
            OGRSFDriver* ogrDriver =
                OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName("ESRI Shapefile");
            OGRDataSource *outOgrDS;
            outOgrDS = ogrDriver->CreateDataSource(cacheShp.c_str());

            std::string layerName = "layer1";
            //create new layer
            OGRLayer *outOgrLayer = outOgrDS->CreateLayer(layerName.c_str(), sourceSRS, sourceGeom, NULL);

            for (int i = 0; i < attributes.size(); i++)
            {
                OGRFieldDefn oField(attributes[i].c_str(), OFTString);
                oField.SetWidth(32);
                outOgrLayer->CreateField(&oField);
            }
            while ((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                OGRFeature *outOgrFeature;
                outOgrFeature = OGRFeature::CreateFeature(outOgrLayer->GetLayerDefn());

                OGRGeometry* ogrGeometry = ogrFeature->GetGeometryRef();
                outOgrFeature->SetGeometryDirectly(ogrGeometry);

                for (int i = 0; i < attributes.size(); i++)
                {
                    int value = ogrFeature->GetFieldAsInteger(attributes[i].c_str());
                    value = value * 51;
                    outOgrFeature->SetField(attributes[i].c_str(), value);

                }

                //update the feature to the database
                outOgrLayer->CreateFeature(outOgrFeature);

                OGRFeature::DestroyFeature(outOgrFeature);
            }
            GDALClose(outOgrDS);
        }
        GDALClose(ogrDS);
        *statusUpdate = 10; 
        _updateRasterProgress();

        //rasterize files to cache
        for (int i = 0; i < attributes.size(); i++)
        {
            //command stream for gdal_grid util 
            std::stringstream commandStream;
            commandStream << "gdal_grid -ot Byte -zfield ";
            commandStream << attributes[i] << " -l " << strippedFileName;
            commandStream << " -txe " << extents.w() << " " << extents.x();
            commandStream << " -tye " << extents.y() << " " << extents.z();
            commandStream << " -outsize " << resolution.x() << " " << resolution.y();
            commandStream << " -of GTiff " << cacheShp << " ";
            commandStream << rasterizeCatcheDir << strippedFileName << i << ".tif";

            UTIL::GDALUtils::execSysCmd(commandStream);
            
            *statusUpdate = 10 + (i + 1) * 20;
            _updateRasterProgress();
        }

        //gdalbuildvrt doesn't support positive NS resolution 
        //generate north-up data first
        for (int i = 0; i < attributes.size(); i++)
        {
            std::stringstream commandStream;
            commandStream << "gdalwarp " << rasterizeCatcheDir << strippedFileName << i << ".tif ";
            commandStream << rasterizeCatcheDir << strippedFileName << i + attributes.size() << ".tif";
            UTIL::GDALUtils::execSysCmd(commandStream);


        }
        *statusUpdate = 80;
        _updateRasterProgress();
        //generate virtual dataset 
        std::stringstream commandStream;
        commandStream << "gdalbuildvrt -separate " << rasterizeCatcheDir << strippedFileName << ".vrt ";
        for (int i = 0; i < attributes.size(); i++)
        {
            commandStream << rasterizeCatcheDir << strippedFileName << i + attributes.size() << ".tif ";
        }
        UTIL::GDALUtils::execSysCmd(commandStream);

        *statusUpdate = 90;
        _updateRasterProgress();

        //delete file if one already exist with same name 
        std::string outFilePath = "\"" + outDir + "/" + strippedFileName + ".tiff\"";
        if (osgDB::fileExists(outFilePath))
            UTIL::deleteFile(outFilePath);
        _updateRasterProgress();

        //generate raster from virtual data set 
        std::stringstream commandStream2;
        commandStream2 << "gdal_translate " << rasterizeCatcheDir << strippedFileName << ".vrt ";
        commandStream2 << outFilePath;
        UTIL::GDALUtils::execSysCmd(commandStream2);

        *statusUpdate = 100;
        _updateRasterProgress();

        //dilete rasterizeCatche directory 
        if (boost::filesystem::exists(folder))
        {
            boost::filesystem::remove_all(folder) > 0;
        }

        _rasterExportInProgress = false; 
    }
    

    bool TerrainExportUIHandler::_cropRasterWGS84(std::string srcfile, std::string& outFile, std::string tempDir)
    {
        //crop a bigger area than required
        double bias = .005;
        osg::Vec4d extentsWithBias = _extents;
        extentsWithBias.x() -= bias; extentsWithBias.y() -= bias;
        extentsWithBias.z() += bias; extentsWithBias.z() += bias;

        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::cropRaster(srcfile, outFile, tempDir, _getOtherDiagonal(extentsWithBias)), srcfile);
        std::string projFile = (fs::path(tempDir) / fs::path("reprojectedCropped.tiff")).string();
        if (!deleteFile(projFile))return false;
        if (UTIL::GDALUtils::reProjectRaster(outFile, projFile))
        {
            if (!removeFileElseUpdateMap(outFile, srcfile)) return false;
        }
        else
        {
            // Skip Export
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            _statusMap[srcfile].second = LayerStatus::FAILED;
            LOG_ERROR("Incorrect Data , " + srcfile);
            return false;
        }
        std::string croppedUTM = (fs::path(tempDir) / fs::path("croppedUTM.tiff")).string();
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::cropRaster(projFile, croppedUTM, tempDir, _extentsUTM), srcfile);
        if (!removeFileElseUpdateMap(projFile, srcfile)) return false;
        outFile = croppedUTM;
        return true;
    }
    bool TerrainExportUIHandler::_exportRasterData(std::string srcfile, std::string outDir, std::string tempDir, int* progressPtr, int res)
    {
        if (!filesExistsElseUpdateMap(srcfile)) return false;
        if (!removeDirElseUpdateMap(outDir, srcfile)) return false;

        // input values
        osg::Vec4d ext = _extentsUTM;

        //crop the Raster
        if (hasUserCancelled(srcfile)) return true;
        std::string croppedfile = (fs::path(outDir) / "Cropped.tiff").string();
        if (UTIL::GDALUtils::isProjected(srcfile))
        {
            EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::cropRaster(srcfile, croppedfile, tempDir, ext), srcfile);
        }
        else
        {
            _cropRasterWGS84(srcfile, croppedfile, _tempDir);
        }
        //scale to unreal's maximumm supported resolution
        if (hasUserCancelled(srcfile)) return true;
        fs::path scaledfile = fs::path(outDir) / "Scaled.tiff";
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::scale(croppedfile, scaledfile.string(), "bilinear", osg::Vec2i(res, res)), srcfile);
        if (!removeFileElseUpdateMap(croppedfile, srcfile)) return false;

        if (hasUserCancelled(srcfile)) return true;
        fs::path pngFile = fs::path(outDir) / (fs::path(srcfile).stem().string() + ".png");
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::convertToPNG(scaledfile.string(), pngFile.string(), "byte"), srcfile);
        if (!removeFileElseUpdateMap(scaledfile.string(), srcfile)) return false;

        if (hasUserCancelled(srcfile)) return true;
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_clearTempFiles(outDir, std::vector<std::string>({ "xml" }), true), srcfile);

        _updateProgress(progressPtr, srcfile);

        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
        notifyObservers(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType, *msg);

        return true;

    }
    bool TerrainExportUIHandler::_exportElevationData(std::string srcfile, std::string outDir, std::string tempDir, int* progressPtr)
    {
        if (!filesExistsElseUpdateMap(srcfile)) return false;
        if (!removeDirElseUpdateMap(outDir, srcfile)) return false;
        // input values
        osg::Vec4d ext = _extentsUTM;

        //crop the Raster
        if (hasUserCancelled(srcfile)) return true;
        std::string croppedfile = (fs::path(outDir) / fs::path("Cropped.tiff")).string();
        if (UTIL::GDALUtils::isProjected(srcfile))
        {
            EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::cropRaster(srcfile, croppedfile, tempDir, ext), srcfile);
        }
        else
        {
            _cropRasterWGS84(srcfile, croppedfile, tempDir);
        }

        //rescale - bilinear
        osg::Vec2i croppedFileReso = UTIL::GDALUtils::getRasterSize(croppedfile);
        osg::Vec2i outReso = _getOutResolution(croppedFileReso);
        fs::path scaledfile = fs::path(outDir) / fs::path("ScaledCropped.tiff");
        if (hasUserCancelled(srcfile)) return true;
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::scale(croppedfile, scaledfile.string(), "bilinear", outReso), srcfile);
        if (!removeFileElseUpdateMap(croppedfile, srcfile)) return false;

        osg::Vec2d minmax = UTIL::GDALUtils::getMinMaxHeight(scaledfile.string());
        int unreal_range = 512; // @ 100% unreal's range is 512 m
        
        double minC = 0.0f;
        if (minmax.x() > 0)
            minC = minmax.x();
        int unrealfactor = std::ceil((minmax.y() - minC) / unreal_range);
        double maxC = minC + unrealfactor * unreal_range;

        // accomplishing offsetting as well in the same step. 
        // that is the 0 value in the input file will be mapped to half of the range of the outpt file,
        // as unreal will treat 0 as it's min height with is -255 at 100% 

        osg::Vec4d scaleParams(minC, maxC, 32767, 65535);
        fs::path colorScalefile = fs::path(tempDir) / fs::path("ColorScale.png");
        if (hasUserCancelled(srcfile)) return true;
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::scaleColorRange(scaledfile.string(), colorScalefile.string(), scaleParams, "uint16", "png"), srcfile);

        GDALDataType type;
        fs::path smoothFile = fs::path(outDir) / fs::path("smoothColorScale.png");
        if (UTIL::GDALUtils::getDataType(scaledfile.string(), type))
        {
            if (type == GDT_Float32)
            {
                if (hasUserCancelled(srcfile)) return true;
                EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_gaussianSmooth(colorScalefile.string(), smoothFile.string(), 31), srcfile);
            }
            else
            {
                smoothFile = colorScalefile;
            }
        }
        else
        {
            smoothFile = colorScalefile;
        }
        if (!removeFileElseUpdateMap(scaledfile.string(), srcfile)) return false;
        //if (!removeFileElseUpdateMap(colorScalefile.string(), srcfile)) return false;

        //split into tiles
        if (hasUserCancelled(srcfile)) return true;
        fs::path tilesPath = fs::path(outDir) / fs::path("tiles");
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::splitIntoTiles(smoothFile.string(), _tilesize, "uint16", tilesPath.string(), "terrain"), srcfile);
        if (!removeFileElseUpdateMap(smoothFile.string(), srcfile)) return false;

        std::map <std::string, std::string> infomap;
        double horizontalScallingFactor = _latspan / outReso.x();
        infomap["Horizontal (x-y) Scale %"] = std::to_string(horizontalScallingFactor * 100);

        //A landscape at 100% on the Z axis has a total range of 512meters. 25700.0 to -25500.
        // so at 12800% 512 meters will be exactly mapped to 65536, which is the range of UInt16.
        infomap["Vertical (z) Scale %"] = std::to_string(unrealfactor * 2 * 100);
        osg::Vec2i srcRasterSize = UTIL::GDALUtils::getRasterSize(srcfile);
        infomap["Min Height"] = std::to_string(minmax.x());
        infomap["Max Height"] = std::to_string(minmax.y());
        infomap["Vertical Range"] = std::to_string(minmax.y() - minmax.x());

        if (hasUserCancelled(srcfile)) return true;
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_clearTempFiles(outDir, std::vector<std::string>({ "xml" }), true), srcfile);


        if (hasUserCancelled(srcfile)) return true;
        fs::path infofile = fs::path(outDir) / fs::path("info.json");
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_writeInfoFile(infofile.string(), infomap), srcfile);

        _updateProgress(progressPtr, srcfile);

        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
        notifyObservers(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType, *msg);

        return true;
    }

    void TerrainExportUIHandler::_updateProgress(int* progressPtr, std::string srcfile)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
        (*progressPtr)++;
        _statusMap[srcfile].second = LayerStatus::COMPLETED;
        _lastCompletedLayer = fs::path(srcfile).filename().string();
    }

    void TerrainExportUIHandler::_updateRasterProgress()
    {
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
        notifyObservers(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType, *msg);
    }
    bool TerrainExportUIHandler::_clearTempFiles(std::string dir, std::vector<std::string>& ext, bool isReursive)
    {
        LOG_INFO("...ENTRY... " + fs::path(dir).stem().string());
        std::vector<std::string> files;
        if (!UTIL::getFilesFromFolder(dir, files, ext, isReursive)) return false;

        for (std::string file : files)
        {
            if (!deleteFile(file))return false;
            LOG_ERROR("File Deleted " + file);
        }
        LOG_INFO("...EXIT... " + fs::path(dir).stem().string());
        return true;
    }

    bool TerrainExportUIHandler::_transform(std::vector<OGRPoint*>& points, osg::Vec4d extents)
    {
        //unreal uses top left as origin .
        osg::Vec3d origin(extents.x(), extents.y(), 0.0f);
        osg::Vec3d BB(extents.z(), extents.w(), 0.0f);
        for (int i = 0; i < points.size(); i++)
        {
            osg::Vec3d P(points[i]->getX(), points[i]->getY(), 0.0f);

            osg::Vec3d Ptranslate = (P - origin);

            double deltaX = std::abs(extents.z() - extents.x());
            double deltaY = std::abs(extents.w() - extents.y());

            osg::Vec3d PScaled(Ptranslate.x() / deltaX, Ptranslate.y() / deltaY, 0.0f);
            //change Y axis positive downwards 
            // because axes in unreal is differently aligned than in geodetic co ordinate system
            points[i]->setX(PScaled.x());
            points[i]->setY((-1)*PScaled.y());

            points[i]->setZ(0.0f);
        }
        return true;
    }
    bool TerrainExportUIHandler::_isWithin(osg::Vec4d ext1, osg::Vec4d ext2)
    {
        LOG_INFO("...ENTRY...");
        osg::Vec2d firstX(ext1.x(), ext1.z());
        osg::Vec2d secondX(ext2.x(), ext2.z());
        osg::Vec2d firstY(ext1.y(), ext1.w());
        osg::Vec2d secondY(ext2.y(), ext2.w());
        if (_isWithinInterval(firstX, secondX) && _isWithinInterval(firstY, secondY)){
            LOG_INFO("...EXIT...");
            return true;
        }
        else return false;
    }
    bool TerrainExportUIHandler::_isWithinInterval(osg::Vec2d first, osg::Vec2d second)
    {
        LOG_INFO("...EXIT...");
        if (second.x() <= first.x() && first.y() <= second.y())
        {
            LOG_INFO("...ENTRY...");
            return true;
        }
        else return false;
    }
    bool TerrainExportUIHandler::_isOverlapping(osg::Vec4d ext1, osg::Vec4d ext2)
    {
        LOG_INFO("...ENTRY...");
        osg::Vec2d ux(ext1.x(), ext1.z());
        osg::Vec2d vx(ext2.x(), ext2.z());
        osg::Vec2d uy(ext1.y(), ext1.w());
        osg::Vec2d vy(ext2.y(), ext2.w());
        if (_isOverlappingInterval(ux, vx) && _isOverlappingInterval(uy, vy))
        {
            LOG_INFO("...EXIT...");
            return true;
        }
        else return false;
    }
    bool TerrainExportUIHandler::_isOverlappingInterval(osg::Vec2d u, osg::Vec2d v)
    {
        LOG_INFO("...ENTRY...");
        osg::Vec2d _near;
        osg::Vec2d _far;
        if (u.x() < v.x())
        {
            _near = u;
            _far = v;
        }
        else
        {
            _near = v;
            _far = u;
        }
        if (_near.y() > _far.x())
        {
            LOG_INFO("...EXIT...");
            return true;
        }
        else return false;
    }

    bool TerrainExportUIHandler::_exportVectorData(std::string srcfile, int* progressPtr)
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
        std::string masksOutDir = (fs::path(_outDir) / fs::path("Vectors") / fs::path("TextureMasks") / fs::path(srcfile).stem()).string();
        std::string masksTempDir = (fs::path(_tempDir) / fs::path("Vectors") / fs::path("TextureMasks") / fs::path(srcfile).stem()).string();
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_exportVectorDataAsMask(srcfile, masksOutDir, masksTempDir, progressPtr), srcfile);

        std::string jsonOutDir = (fs::path(_outDir) / fs::path("Vectors") / fs::path("Jsons") / fs::path(srcfile).stem()).string();
        std::string jsonTempDir = (fs::path(_tempDir) / fs::path("Vectors") / fs::path("Jsons") / fs::path(srcfile).stem()).string();
        EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_exportVectorDataAsJson(srcfile, jsonOutDir, jsonTempDir, progressPtr), srcfile);

        _updateProgress(progressPtr, srcfile);

        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
        notifyObservers(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType, *msg);

        return true;
    }

    bool TerrainExportUIHandler::_exportVectorDataAsJson(std::string srcfile, std::string outDir, std::string tempDir, int* progressPtr)
    {
        LOG_INFO("...ENTRY... " + fs::path(srcfile).stem().string());
        if (!filesExistsElseUpdateMap(srcfile)) return false;
        if (!removeDirElseUpdateMap(outDir, srcfile)) return false;

        fs::create_directories(outDir);

        std::string orig_srcfile = srcfile;
        if (!UTIL::GDALUtils::isProjectedVector(srcfile))
        {
            std::string outfile = (fs::path(tempDir) / fs::path(srcfile).filename()).string();
            if (!deleteFile(outfile))return false;
            if (UTIL::GDALUtils::reProjectVector(srcfile, outfile))
            {
                srcfile = outfile;
            }
            else
            {
                // Skip Export
                OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                _statusMap[orig_srcfile].second = LayerStatus::FAILED;
                LOG_ERROR("Incorrect Data , " + orig_srcfile);
                return false;
            }
        }
        OGRDataSource* shpDS = UTIL::GDALUtils::openSHPDataSource(srcfile);
        OGRLayer* shplayer = shpDS->GetLayer(0);

        OGRDataSource* clipDS = UTIL::GDALUtils::createClipingDS(_extentsUTM, shplayer->GetSpatialRef());
        // Create an inmemory polygon layer from _extents;
        OGRDataSource* resultDS = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName("Memory")->CreateDataSource("result");
        OGRLayer* resultLayer = NULL;
        if (resultDS)
        {
            resultLayer = resultDS->CreateLayer("result", shplayer->GetSpatialRef(), shplayer->GetGeomType(), NULL);
        }

        if (shplayer->Clip(clipDS->GetLayer(0), resultLayer) != OGRERR_NONE)
        {
            OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
            _statusMap[orig_srcfile].second = LayerStatus::FAILED;
            return false;
        }
        LOG_INFO("Clipped Layer for extents Created ");
        // iterate over the features of the result layer , convert them into normalised form add into GeoJsons layer 
        std::string outjsonfile = ((fs::path(outDir) / fs::path(srcfile).stem())).string() + ".json";

        OGRSFDriver* jsonDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName("geojson");
        OGRDataSource* jsonDS = NULL;
        OGRLayer* jsonLayer = NULL;
        if (jsonDriver)
            jsonDS = jsonDriver->CreateDataSource(outjsonfile.c_str());
        if (jsonDS)
        {
            std::string layername = fs::path(srcfile).stem().string();
            OGRSpatialReference* sref = shplayer->GetSpatialRef();
            OGRwkbGeometryType type = shplayer->GetGeomType();
            jsonLayer = jsonDS->CreateLayer(layername.c_str(), sref, wkbFlatten(type));
        }

        OGRFeatureDefn* ogrFeatureDefn;
        OGRFieldDefn* ogrFieldDefn;
        int featurecount = resultLayer->GetFeatureCount();
        LOG_INFO("Writing features to geojson" + outjsonfile);
        if (featurecount > 0)
        {
            ogrFeatureDefn = resultLayer->GetLayerDefn();
            for (int j = 0; j < ogrFeatureDefn->GetFieldCount(); j++)
            {
                ogrFieldDefn = ogrFeatureDefn->GetFieldDefn(j);
                jsonLayer->CreateField(ogrFieldDefn);
            }
        }
        resultLayer->ResetReading();
        for (int i = 0; i < featurecount; i++)
        {
            std::vector<OGRPoint*> points;
            OGRFeature* clippedFeature = resultLayer->GetNextFeature();
            OGRGeometry* geom = clippedFeature->StealGeometry();

            if (wkbFlatten(geom->getGeometryType()) == wkbPoint)
            {
                points.clear();
                OGRPoint* point = (OGRPoint*)geom;
                points.push_back(point);
                _transform(points, _extentsUTM);
                OGRPoint* tpoint = (OGRPoint*)OGRGeometryFactory::createGeometry(wkbPoint);
                tpoint->setX(points[0]->getX());
                tpoint->setY(points[0]->getY());
                tpoint->setZ(points[0]->getZ());

                OGRFeature* feature = OGRFeature::CreateFeature(jsonLayer->GetLayerDefn());
                feature->SetFrom(clippedFeature);
                feature->SetGeometryDirectly(tpoint);
                jsonLayer->CreateFeature(feature);
                OGRFeature::DestroyFeature(feature);

            }
            else if (wkbFlatten(geom->getGeometryType()) == wkbLineString)
            {
                points.clear();
                OGRLineString* linestring = ((OGRLineString*)geom);
                int numPoints = linestring->getNumPoints();
                for (int i = 0; i < numPoints; i++)
                {
                    OGRPoint* point = (OGRPoint*)OGRGeometryFactory::createGeometry(wkbPoint);
                    linestring->getPoint(i, point);
                    points.push_back(point);
                }
                OGRGeometryFactory::destroyGeometry(linestring);
                _transform(points, _extentsUTM);
                OGRLineString* line = (OGRLineString*)OGRGeometryFactory::createGeometry(wkbLineString);
                for (int i = 0; i < points.size(); i++)
                {
                    line->addPoint(points[i]);
                }

                OGRFeature* feature = OGRFeature::CreateFeature(jsonLayer->GetLayerDefn());
                feature->SetFrom(clippedFeature);
                feature->SetGeometryDirectly(line);
                jsonLayer->CreateFeature(feature);
                OGRFeature::DestroyFeature(feature);
            }
            else if (wkbFlatten(geom->getGeometryType()) == wkbMultiLineString)
            {
                OGRMultiLineString* multilinestring = (OGRMultiLineString*)geom;
                int no_lines = multilinestring->getNumGeometries();
                for (int i = 0; i < no_lines; i++)
                {
                    OGRLineString* linestring = (OGRLineString*)multilinestring->getGeometryRef(i);
                    int numPoints = linestring->getNumPoints();
                    points.clear();
                    for (int i = 0; i < numPoints; i++)
                    {
                        OGRPoint* point = (OGRPoint*)OGRGeometryFactory::createGeometry(wkbPoint);
                        linestring->getPoint(i, point);
                        points.push_back(point);
                    }
                    OGRGeometryFactory::destroyGeometry(linestring);
                    _transform(points, _extentsUTM);
                    OGRLineString* line = (OGRLineString*)OGRGeometryFactory::createGeometry(wkbLineString);
                    for (int i = 0; i < points.size(); i++)
                    {
                        line->addPoint(points[i]);
                    }
                    OGRFeature* feature = OGRFeature::CreateFeature(jsonLayer->GetLayerDefn());
                    feature->SetFrom(clippedFeature);
                    feature->SetGeometryDirectly(line);
                    jsonLayer->CreateFeature(feature);
                    OGRFeature::DestroyFeature(feature);
                }
            }
            else if (wkbFlatten(geom->getGeometryType()) == wkbPolygon)
            {
                OGRPolygon* polygon = (OGRPolygon*)geom;
                OGRLinearRing* ring = polygon->getExteriorRing();
                int numPoints = ring->getNumPoints();

                points.clear();
                for (int i = 0; numPoints > 1 && i < numPoints; i++)
                {
                    OGRPoint* point = (OGRPoint*)OGRGeometryFactory::createGeometry(wkbPoint);
                    ring->getPoint(i, point);
                    points.push_back(point);
                }

                OGRGeometryFactory::destroyGeometry(polygon);
                _transform(points, _extentsUTM);
                polygon = (OGRPolygon*)OGRGeometryFactory::createGeometry(wkbPolygon);
                OGRLinearRing* linearRing = (OGRLinearRing*)OGRGeometryFactory::createGeometry(wkbLinearRing);
                for (int i = 0; i < points.size(); i++)
                    linearRing->addPoint(points[i]);

                linearRing->closeRings();

                polygon->addRingDirectly(linearRing);

                OGRFeature* feature = OGRFeature::CreateFeature(jsonLayer->GetLayerDefn());
                feature->SetFrom(clippedFeature);
                feature->SetGeometryDirectly(polygon);
                jsonLayer->CreateFeature(feature);
                OGRFeature::DestroyFeature(feature);

            }
            else if (wkbFlatten(geom->getGeometryType()) == wkbMultiPolygon)
            {
                OGRMultiPolygon* multipolygon = (OGRMultiPolygon*)geom;

                int no_polygon = multipolygon->getNumGeometries();
                for (int i = 0; i < no_polygon; i++)
                {
                    OGRPolygon* polygon = (OGRPolygon*)multipolygon->getGeometryRef(i);
                    OGRLinearRing* ring = polygon->getExteriorRing();
                    int numPoints = ring->getNumPoints();
                    points.clear();
                    for (int i = 0; numPoints > 1 && i < numPoints; i++)
                    {
                        OGRPoint* point = (OGRPoint*)OGRGeometryFactory::createGeometry(wkbPoint);
                        ring->getPoint(i, point);
                        points.push_back(point);
                    }
                    OGRGeometryFactory::destroyGeometry(polygon);
                    _transform(points, _extentsUTM);
                    polygon = (OGRPolygon*)OGRGeometryFactory::createGeometry(wkbPolygon);
                    OGRLinearRing* linearRing = (OGRLinearRing*)OGRGeometryFactory::createGeometry(wkbLinearRing);
                    for (int i = 0; i < points.size(); i++)
                        linearRing->addPoint(points[i]);

                    linearRing->closeRings();

                    polygon->addRingDirectly(linearRing);

                    OGRFeature* feature = OGRFeature::CreateFeature(jsonLayer->GetLayerDefn());
                    feature->SetFrom(clippedFeature);
                    feature->SetGeometryDirectly(polygon);
                    jsonLayer->CreateFeature(feature);
                    OGRFeature::DestroyFeature(feature);
                }
            }

        }
        OGRDataSource::DestroyDataSource(resultDS);
        OGRDataSource::DestroyDataSource(jsonDS);
        OGRDataSource::DestroyDataSource(clipDS);
        OGRDataSource::DestroyDataSource(shpDS);
        LOG_INFO("Features Written to geojson " + outjsonfile);
        if (hasUserCancelled(orig_srcfile)) return true;
        //EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_clearTempFiles(outDir, std::vector<std::string>({ "xml" }), true), orig_srcfile);

        /*_updateProgress(progressPtr, orig_srcfile);
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
        notifyObservers(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType, *msg);*/
        LOG_INFO("...EXIT... " + fs::path(srcfile).stem().string());
        return true;
    }
    bool TerrainExportUIHandler::_exportVectorDataAsMask(std::string infile, std::string outDir, std::string tempDir, int* progress)
    {
        LOG_INFO("...ENTRY... " + fs::path(infile).stem().string());
        try
        {
            if (!filesExistsElseUpdateMap(infile)) return false;
            if (!removeDirElseUpdateMap(outDir, infile)) return false;

            double bufferWidth = 0.0001;
            std::string layername = "buffer_" + fs::path(infile).stem().string();
            fs::path result_shpfile = fs::path(outDir) / (layername + ".shp");

            if (hasUserCancelled(infile)) return true;
            EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::createBuffer(infile, result_shpfile.string()), infile);

            if (hasUserCancelled(infile)) return true;
            fs::path rasterfile = fs::path(outDir) / fs::path(fs::path(infile).stem().string() + ".tiff");


            EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::rasterize(result_shpfile.string(), rasterfile.string(), _extentsUTM, osg::Vec2i(_tilesize, _tilesize)), infile);


            std::map <std::string, std::string> infomap;
            double horizontalScallingFactor = std::sqrt((_latspan * _longspan) / (_tilesize * _tilesize));
            infomap["Horizontal (x-y) Scale %"] = std::to_string(horizontalScallingFactor * 100);
            fs::path infofile = fs::path(outDir) / fs::path("info.json");

            if (hasUserCancelled(infile)) return true;

            EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_writeInfoFile(infofile.string(), infomap), infile);

            fs::path pngFile = fs::path(outDir) / (fs::path(infile).stem().string() + ".png");
            if (hasUserCancelled(infile)) return true;

            EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(UTIL::GDALUtils::convertToPNG(rasterfile.string(), pngFile.string(), "byte"), infile);

            if (!removeFileElseUpdateMap(rasterfile.string(), infile)) return false;

            if (hasUserCancelled(infile)) return true;

            EXECUTE_OPERATION_ELSE_UPDATE_STATUSMAP(_clearTempFiles(outDir, std::vector<std::string>({ "xml", "shp", "shx", "dbf", "prj", "sqlite" }), false), infile);

            /*_updateProgress(progress, infile);

            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
            notifyObservers(*ITerrainExportUIHandler::ProgressValueUpdatedMessageType, *msg);*/
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
        catch (std::exception& e)
        {
            LOG_ERROR(e.what());

        }
        catch (...)
        {
            LOG_ERROR("SOME unknown exception thrown");
        }
        LOG_INFO("...EXIT... " + fs::path(infile).stem().string());
        return true;
    }

    void TerrainExportUIHandler::_dumpExtentsShape()
    {
        fs::path extentsPath = fs::path(_outDir) / fs::path("extents.shp");
        fs::path extentsJsonPath = fs::path(_outDir) / fs::path("extents.json");
        OGRRegisterAll();
        //Create Polygon From Extents
        //anti clock wise
        LOG_DEBUG_VAR("Extents : %f %f %f %f", _extents.x(), _extents.y(), _extents.z(), _extents.w());
        //LOG_DEBUG_VAR("UTM Extents : %f %f %f %f", _extentsUTM.x(), _extentsUTM.y(), _extentsUTM.z(), _extentsUTM.w());

        osg::Vec4d extents = _extents;

        OGRLinearRing ring;
        ring.addPoint(extents.x(), extents.y());
        ring.addPoint(extents.x(), extents.w());
        ring.addPoint(extents.z(), extents.w());
        ring.addPoint(extents.z(), extents.y());
        ring.addPoint(extents.x(), extents.y());

        OGRGeometry* polygon = OGRGeometryFactory::createGeometry(wkbPolygon);
        ((OGRPolygon*)polygon)->addRingDirectly(&ring);

        // Create an inmemory polygon layer from _extents;
        //OGRSFDriver* ogrDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName("ESRI Shapefile");
        OGRSpatialReference sref;
        sref.importFromProj4(UTIL::GDALUtils::WGS84_SREF.c_str());
        //const osgEarth::SpatialReference* sref = osgEarth::SpatialReference::get(UTIL::GDALUtils::WGS84_SREF);
        OGRDataSource* tempDS = UTIL::GDALUtils::createSHPDataSource(extentsPath.string());

        OGRLayer* tempLayer = NULL;
        OGRFeature* feature = NULL;
        if (tempDS)
        {
            tempLayer = tempDS->CreateLayer("extents", &sref, wkbPolygon, NULL);
            if (tempLayer)
            {
                feature = OGRFeature::CreateFeature(tempLayer->GetLayerDefn());
                feature->SetGeometryDirectly(polygon);
                tempLayer->CreateFeature(feature);
                //OGRFeature::DestroyFeature(feature);
            }
        }
        OGRDataSource::DestroyDataSource(tempDS);

        //! Write extents info to json file
        std::map <std::string, std::string> extentsMap;
        extentsMap["Extents Bottom Left Longitude"] = std::to_string(_extents.x());
        extentsMap["Extents Bottom Left Latitude"] = std::to_string(_extents.y());
        extentsMap["Extents Top Right Longitude"] = std::to_string(_extents.z());
        extentsMap["Extents Top Right Latitude"] = std::to_string(_extents.w());
        extentsMap["Center Point UTM X"] = std::to_string(_center.x());
        extentsMap["Center Point UTM Y"] = std::to_string(_center.y());
        extentsMap["SideLength"] = std::to_string(_sideLength);

        _writeInfoFile(extentsJsonPath.string(), extentsMap);
    }

    void TerrainExportUIHandler::execute(int* progressPtr)
    {
        std::string value = UTIL::getEnvironmentVariable("TERRAIN_EXPORT_MT");
        bool IS_MULTI_THREADED;
        try {
            IS_MULTI_THREADED = UTIL::strToBool(value);
        }
        catch (...)
        {
            IS_MULTI_THREADED = false;
        }
        

        LOG_INFO("...ENTRY...");
        //write the vector classification file
        fs::path jsonPath = fs::path(_outDir) / fs::path("Vectors") / fs::path("Classisification.json");
        if (!_writeInfoFile(jsonPath.string(), _classificationMap))
        {
            LOG_ERROR("Error while writing info file : " + jsonPath.string());
        }

        //OGRDataSource* clipDS = UTIL::GDALUtils::createClipingDS(_extentsUTM, );
        _dumpExtentsShape();

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
        osg::ref_ptr<UTIL::ThreadPool> tp = worldMaintainer->getComputeThreadPool();
        if (tp != NULL)
        {
            for (auto&it = _statusMap.begin(); it != _statusMap.end(); ++it)
            {
                std::string filename = it->first;
                CORE::RefPtr<CORE::IObject> iobject = (it->second).first;
                if (iobject->getInterface<TERRAIN::IRasterObject>())
                {
                    std::string name = iobject->getInterface<CORE::IBase>()->getName();
                    name = osgDB::getNameLessExtension(name);

                    int res = 8192;
                    if (_classificationMap.find(name) != _classificationMap.end())
                    {
                        std::string resolution = _classificationMap[name];
                        res = std::stoi(resolution);
                    }
                    std::string rasterFile = filename;
                    std::string outDir = (fs::path(_outDir) / fs::path("Rasters") / fs::path(rasterFile).filename().stem()).string();
                    std::string rasterTempDir = (fs::path(_tempDir) / fs::path("Rasters") / fs::path(rasterFile).filename().stem()).string();
                    try
                    {
#if EXPORT_RASTER
                        if (IS_MULTI_THREADED)
                        tp->invoke(boost::bind(&TerrainExportUIHandler::_exportRasterData, this, rasterFile, outDir, rasterTempDir, progressPtr, res));
                        else
                        _exportRasterData(rasterFile, outDir, rasterTempDir, progressPtr, res);
#endif
                    }
                    catch (const UTIL::Exception& e)
                    {
                        e.LogException();
                        throw e;
                    }
                }
                else if (iobject->getInterface<TERRAIN::IElevationObject>())
                {
                    std::string elevationFile = filename;
                    std::string outDir = (fs::path(_outDir) / fs::path("Elevations") / fs::path(elevationFile).filename().stem()).string();
                    std::string elevationTempDir = (fs::path(_tempDir) / fs::path("Elevations") / fs::path(elevationFile).filename().stem()).string();
                    try
                    {
#if EXPORT_ELEVATION
                        if (IS_MULTI_THREADED)
                            tp->invoke(boost::bind(&TerrainExportUIHandler::_exportElevationData, this, elevationFile, outDir, elevationTempDir, progressPtr));
                        else
                            _exportElevationData(elevationFile, outDir, elevationTempDir, progressPtr);
#endif
                    }
                    catch (const UTIL::Exception& e)
                    {
                        e.LogException();
                        throw e;
                    }
                }
                else if (iobject->getInterface<TERRAIN::IModelObject>()
                    || iobject->getInterface<CORE::IFeatureLayer>()
                    || iobject->getInterface<DGN::IDGNFolder>())
                {
                    std::string vectorFile = filename;

                    /*std::string masksOutDir = (fs::path(_outDir) / fs::path("Vectors") / fs::path("TextureMasks") / fs::path(vectorFile).stem()).string();
                     std::string masksTempDir = (fs::path(_tempDir) / fs::path("Vectors") / fs::path("TextureMasks") / fs::path(vectorFile).stem()).string();

                     std::string jsonOutDir = (fs::path(_outDir) / fs::path("Vectors") / fs::path("Jsons") / fs::path(vectorFile).stem()).string();
                     std::string jsonTempDir = (fs::path(_tempDir) / fs::path("Vectors") / fs::path("Jsons") / fs::path(vectorFile).stem()).string();*/
                    try
                    {
#if EXPORT_VECTOR
                        if (IS_MULTI_THREADED)
                            tp->invoke(boost::bind(&TerrainExportUIHandler::_exportVectorData, this, vectorFile, progressPtr));
                        else
                            _exportVectorData(vectorFile, progressPtr);
#endif

                        //#if EXPORT_VECTOR_AS_JSON
                        //    #if IS_MULTI_THREADED
                        //                        tp->invoke(boost::bind(&TerrainExportUIHandler::_exportVectorDataAsJson, this, vectorFile, jsonOutDir, jsonTempDir, progressPtr));
                        //    #else
                        //                        _exportVectorDataAsJson(vectorFile, jsonOutDir, jsonTempDir, progressPtr);
                        //    #endif
                        //#endif
                        //
                        //#if EXPORT_VECTOR_AS_MASK
                        //    #if IS_MULTI_THREADED
                        //                        tp->invoke(boost::bind(&TerrainExportUIHandler::_exportVectorDataAsMask, this, vectorFile, masksOutDir, masksTempDir, progressPtr));
                        //    #else
                        //                        _exportVectorDataAsMask(vectorFile, masksOutDir, masksTempDir, progressPtr);
                        //    #endif
                        //#endif

                    }
                    catch (const UTIL::Exception& e)
                    {
                        e.LogException();
                        throw e;
                    }
                }
            }
            _statusMap.clear();
        }
        else
        {
            LOG_ERROR("Thread Pool is NULL");
        }
    }
    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void TerrainExportUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {

        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }
    void TerrainExportUIHandler::_reset()
    {
        _extents.x() = 0.0;
        _extents.y() = 0.0;
        _extents.z() = 0.0;
        _extents.w() = 0.0;
    }
    void TerrainExportUIHandler::setExtents(osg::Vec4d extents)
    {
        _extents = extents;
    }
    osg::Vec4d TerrainExportUIHandler::getExtents() const
    {
        return _extents;
    }

    void TerrainExportUIHandler::setExtentsUTM(osg::Vec4d extentsUTM)
    {
        _extentsUTM = extentsUTM;
    }
    osg::Vec4d TerrainExportUIHandler::getExtentsUTM() const
    {
        return _extentsUTM;
    }
    int TerrainExportUIHandler::verifyAndCountLayers()
    {
        LOG_INFO("...ENTRY...");
        int count = 0;

        //Get the objectMap form World
        vizCore::RefPtr<vizCore::IWorldMaintainer> worldMaintainer = vizCore::WorldMaintainer::instance();
        const vizCore::WorldMap& worldMap = worldMaintainer->getWorldMap();
        CORE::RefPtr<CORE::IWorld> iworld = NULL;
        if (worldMap.size() > 0)
        {
            iworld = worldMap.begin()->second;
        }
        const CORE::ObjectMap& omap = iworld->getObjectMap();

        //So the next time when user hits export , it gets filled newly for the new area selected
        _statusMap.clear();

        //iterate over the object map and fill the status map 
        // after verifying which layers have a fullfiling dataset 
        // also does reprojection of projected raster files in case of PROCS
        LOG_INFO("searching in Objectmap");
        for (CORE::ObjectMap::const_iterator citer = omap.begin();
            citer != omap.end();
            ++citer)
        {
            CORE::RefPtr<CORE::IObject> object = citer->second;
            std::string name = object->getInterface<CORE::IBase>()->getName();
            name = osgDB::getNameLessExtension(name);
            LOG_INFO("Name " + name);
            if (_classificationMap.find(name) == _classificationMap.end())
                continue;

            CORE::RefPtr<CORE::IFeatureLayer> featureLayer = object->getInterface<CORE::IFeatureLayer>();
            if (featureLayer)
            {
                LOG_INFO("feature Layer");
                CORE::RefPtr<TERRAIN::IOGRLayerHolder> layerholder = object->getInterface<TERRAIN::IOGRLayerHolder>();
                if (layerholder)
                {
                    OGRLayer* layer = layerholder->getOGRLayer();
                    if (layer)
                    {
                        std::string layername = object->getInterface<CORE::IBase>()->getName();
                        OGREnvelope envelope;
                        if (layer->GetExtent(&envelope) == OGRERR_NONE)
                        {
                            osg::Vec4d vectorExtents(envelope.MinX, envelope.MinY, envelope.MaxX, envelope.MaxY);
                            if (_isOverlapping(vectorExtents, _extents))
                            {
                                //create Shape Files from layers.
                                std::string outfile = (fs::path(_tempDir) / "Markings" / (layername + ".shp")).string();
                                if (_writeMarkings(featureLayer, outfile))
                                {
                                    _statusMap[outfile] = std::make_pair(object, LayerStatus::INPROGRESS);
                                    //count = count+2; // 1 for texture Masks and 1 for Json
                                    count = count + 1;
                                }
                            }
                        }
                        else
                        {
                            LOG_ERROR("Could not load extents of OGR layer " + layername);
                        }
                    }
                    else
                    {
                        LOG_ERROR("Could not get OGRLayer .");
                    }
                }
                else
                {
                    LOG_ERROR("Layer Holder Object no found.");
                }
            }
            if (object->getInterface<TERRAIN::IModelObject>())
            {
                LOG_INFO("Model Object");
                CORE::RefPtr<TERRAIN::IModelObject> modelObject = object->getInterface <TERRAIN::IModelObject>();
                CORE::RefPtr<CORE::IGeoExtent> extentsObj = modelObject->getInterface<CORE::IGeoExtent>();
                osg::Vec4d vectorExtents;
                if (extentsObj->getExtents(vectorExtents))
                {
                    std::string vectorFile = modelObject->getUrl();
                    fs::path filepath(vectorFile);
                    std::string ext = filepath.extension().string();
                    if (boost::iequals(ext, ".shp"))
                    {
                        if (_isOverlapping(vectorExtents, _extents))
                        {
                            _statusMap[filepath.string()] = std::make_pair(object, LayerStatus::INPROGRESS);
                            //count = count + 2; // 1 for texture Masks and 1 for Json
                            count++;
                        }
                    }
                }
            }
            if (object->getInterface<DGN::IDGNFolder>())
            {
                LOG_INFO("DGNFolder");
                CORE::RefPtr<DGN::IDGNFolder> dgn = object->getInterface <DGN::IDGNFolder>();
                CORE::RefPtr<CORE::ICompositeObject> comp = dgn->getInterface<CORE::ICompositeObject>();
                const CORE::ICompositeObject::ObjectMap& objmap = comp->getObjectMap();
                CORE::ICompositeObject::ObjectMap::const_iterator it = objmap.begin();
                for (; it != objmap.end(); it++)
                {
                    CORE::RefPtr<CORE::IObject> iobj = it->second;
                    CORE::RefPtr<TERRAIN::IModelObject> modelObject = iobj->getInterface<TERRAIN::IModelObject>();
                    if (modelObject)
                    {
                        LOG_INFO("modelObject");
                        CORE::RefPtr<CORE::IGeoExtent> extentsObj = modelObject->getInterface<CORE::IGeoExtent>();
                        osg::Vec4d vectorExtents;
                        if (extentsObj->getExtents(vectorExtents))
                        {
                            std::string vectorFile = modelObject->getUrl();
                            fs::path filepath(vectorFile);
                            std::string ext = filepath.extension().string();
                            if (boost::iequals(ext, ".shp"))
                            {
                                if (_isOverlapping(vectorExtents, _extents))
                                {
                                    _statusMap[filepath.string()] = std::make_pair(object, LayerStatus::INPROGRESS);
                                    //count = count + 2; // 1 for texture Masks and 1 for Json
                                    count++;
                                }
                            }
                        }
                    }
                }
            }
            if (object->getInterface<TERRAIN::IElevationObject>())
            {
                LOG_INFO("ElevationObject");
                CORE::RefPtr<TERRAIN::IElevationObject> elevationObject = object->getInterface <TERRAIN::IElevationObject>();
                CORE::RefPtr<CORE::IGeoExtent> geoExtentObj = elevationObject->getInterface<CORE::IGeoExtent>();
                osg::Vec4d elevationObjExt;
                if (geoExtentObj->getExtents(elevationObjExt))
                {
                    std::string elevationFile = elevationObject->getURL();
                    fs::path filepath(elevationFile);
                    LOG_INFO("Checking is raster supported");
                    if (UTIL::GDALUtils::isSupportedRaster(elevationFile))
                    {
                        LOG_INFO("Raster supported");
                        if (_isOverlapping(elevationObjExt, _extents))
                        {
                            if (_isWithin(_extents, elevationObjExt))
                            {
                                if (_willBeAtleast1Pixel(elevationFile, _getOtherDiagonal(_extents)))
                                {
                                    //operate on the original file
                                    _statusMap[elevationFile] = std::make_pair(object, LayerStatus::INPROGRESS);
                                    count++;
                                }
                            }
                            else
                            {
                                // used as a indicator to notify that a certain layer 
                                //is inexportable because of data in sufficieny
                                // message is relayed to the user.
                                count = INT_MIN;
                                LOG_INFO("Data InSufficient, file : " + elevationFile);
                                break;
                            }
                        }
                        else
                        {
                            LOG_INFO("No OverLap with the selected Area , Skipping File : " + elevationFile);
                        }
                    }
                    else
                    {
                        LOG_ERROR("File Format is not supported by GDAL, File : " + elevationFile);
                    }
                }

            }
            if (object->getInterface<TERRAIN::IRasterObject>())
            {
                LOG_INFO("RasterObject");
                CORE::RefPtr<TERRAIN::IRasterObject> rasterObject = object->getInterface <TERRAIN::IRasterObject>();
                CORE::RefPtr<CORE::IGeoExtent> geoExtObj = rasterObject->getInterface<CORE::IGeoExtent>();
                osg::Vec4d rasterObjExt;
                if (geoExtObj->getExtents(rasterObjExt))
                {
                    std::string rasterFile = rasterObject->getURL();
                    fs::path filepath(rasterFile);
                    LOG_INFO("Checking is raster supported");
                    if (UTIL::GDALUtils::isSupportedRaster(rasterFile))
                    {
                        LOG_INFO("Raster supported");
                        if (_isOverlapping(rasterObjExt, _extents))
                        {
                            if (_isWithin(_extents, rasterObjExt))
                            {
                                if (_willBeAtleast1Pixel(rasterFile, _getOtherDiagonal(_extents)))
                                {
                                    _statusMap[rasterFile] = std::make_pair(object, LayerStatus::INPROGRESS);
                                    count++;
                                }
                            }
                            else
                            {
                                // used as a indicator to notify that a certain layer 
                                //is inexportable because of data in sufficieny
                                // message is relayed to the user.
                                count = INT_MIN;
                                LOG_INFO("Data InSufficient, file : " + rasterFile);
                                break;
                            }
                        }
                        else
                        {
                            LOG_INFO("No OverLap with the selected Area , Skipping File : " + rasterFile);
                        }
                    }
                    else
                    {
                        LOG_ERROR("File Format is not supported by GDAL, File : " + rasterFile);
                    }
                }
            }
        }
        LOG_INFO("...EXIT...");
        return count;
    }

    bool TerrainExportUIHandler::_writeMarkings(CORE::RefPtr<CORE::IFeatureLayer> layer, std::string filename)
    {
        LOG_INFO("...ENTRY...");
        CORE::RefPtr<CORE::IFeatureObject> featureObj = layer->getInterface<CORE::IFeatureObject>();
        fs::create_directories(fs::path(filename).parent_path());
        if (featureObj.valid())
        {
            if (DB::writeFeatureFile(featureObj.get(), filename))
            {
                LOG_INFO("...EXIT...");
                return true;
            }
        }
        return false;
    }

    /* -------------------------------------------------------------------- */
    /*      Compute the source window from the projected source window      */
    /*      if the projected coordinates were provided.  Note that the      */
    /*      projected coordinates are in ulx, uly, lrx, lry format,         */
    /*      while the anSrcWin is xoff, yoff, xsize, ysize with the         */
    /*      xoff,yoff being the ulx, uly in pixel/line.                     */
    /* -------------------------------------------------------------------- */
    bool TerrainExportUIHandler::_willBeAtleast1Pixel(std::string infile, osg::Vec4d extents)
    {
        LOG_INFO("...ENTRY...");
        if (!fileExists(infile)) return false;
        GDALAllRegister();
        GDALDataset* dataset;
        dataset = static_cast<GDALDataset*>(GDALOpen(infile.c_str(), GA_ReadOnly));
        if (!dataset)
        {
            LOG_ERROR("Unable to open Dataset, " + infile);
            //GDALDestroyDriverManager();
            return false;
        }

        char* projection;
        projection = (char *)GDALGetProjectionRef(dataset);
        OGRSpatialReference srs;
        srs.importFromWkt(&projection);
        const char* authorityCode = srs.GetAuthorityCode(NULL);// to get authority code for the root element
        bool returnValue = false;
        if (authorityCode)
        {
            int epsgcode = std::atoi(authorityCode);
            if (epsgcode == 32643)
            {
                //units meters
                osg::Vec2d pixelSize = UTIL::GDALUtils::getPixelSize(infile);
                if (((_latspan / std::abs(pixelSize.x())) > 1) && ((_longspan / std::abs(pixelSize.y())) > 1))
                {
                    LOG_INFO("...EXIT...");
                    GDALClose(dataset);
                    return true;
                }
                else
                {
                    GDALClose(dataset);

                    return false;
                }
            }
        }
        returnValue =  _willBeAtleast1PixelWGS84(dataset, extents);
        GDALClose(dataset);
        //Close resources
        return returnValue;
    }

    bool TerrainExportUIHandler::_willBeAtleast1PixelWGS84(GDALDataset* dataset, osg::Vec4d extents)
    {
        LOG_INFO("...ENTRY...");
        bool returnValue = false;
        if (!dataset)
            return false;

        double ulx = extents.x(), uly = extents.y(), lrx = extents.z(), lry = extents.w();
        int srcwin[4] = { 0 };
        if (ulx != 0.0 || uly != 0.0
            || lrx != 0.0 || lry != 0.0)
        {
            double	geoTransform[6];
            GDALGetGeoTransform(dataset, geoTransform);
            if (geoTransform[2] != 0.0 || geoTransform[4] != 0.0)
            {
                LOG_ERROR("The -projwin option was used, but the geotransform is\n"
                    "rotated.  This configuration is not supported.\n");
                returnValue = false;
            }
            else
            {
                srcwin[0] = (int)
                    floor((ulx - geoTransform[0]) / geoTransform[1] + 0.001);
                srcwin[1] = (int)
                    floor((uly - geoTransform[3]) / geoTransform[5] + 0.001);

                srcwin[2] = (int)((lrx - ulx) / geoTransform[1] + 0.5);
                srcwin[3] = (int)((lry - uly) / geoTransform[5] + 0.5);
                //Verify source window dimensions.
                if (srcwin[2] <= 0 || srcwin[3] <= 0)
                {
                    char ** fileList = dataset->GetFileList();
                    if (fileList && *fileList)
                        LOG_ERROR( std::string(fileList[0]) + " -srcwin  has negative width and/or height.\n");
                    returnValue = false;
                }
                else
                {
                    LOG_INFO("...EXIT...");
                    //EveryThing seems ok 
                    returnValue = true;
                }
            }
        }
        else
        {
            //incorrect Extents
            returnValue = false;
        }
        
        LOG_INFO("...EXIT...");
        return returnValue;
    }

    bool TerrainExportUIHandler::_gaussianSmooth(std::string infile, std::string destfile, int max_kernel_length)
    {
        if (!fileExists(infile)) return false;
        if (!deleteFile(destfile)) return false;

        cv::Mat src = cv::imread(infile, CV_LOAD_IMAGE_UNCHANGED);
        double min, max;
        int rows = src.rows;
        int cols = src.cols;
        int channels = src.channels();
        int type = src.type();

        cv::minMaxIdx(src, &min, &max);
        cv::Mat dst(rows, cols, type, cv::Scalar(std::numeric_limits<short>::max()));
        for (int i = 1; i < max_kernel_length; i = i + 2)
        {
            cv::GaussianBlur(src, dst, cv::Size(i, i), 0, 0);
        }

        cv::minMaxIdx(src, &min, &max);
        LOG_INFO("Min: " + std::to_string(min) + " Max: " + std::to_string(max));
        cv::imwrite(destfile, dst);
        return true;
    }

} // namespace SMCUI