/*****************************************************************************
 *
 * File             : AddContentStatusMessage.h
 * Description      : AddContentStatusMessage class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCUI/AddContentStatusMessage.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, AddContentStatusMessage);
    DEFINE_IREFERENCED(AddContentStatusMessage, CORE::CoreMessage);

    AddContentStatusMessage::AddContentStatusMessage()
        : _status(IDLE),
          _progress(0)
    {
        _addInterface(IAddContentStatusMessage::getInterfaceName());
    }

    void 
    AddContentStatusMessage::setStatus(const AddContentStatus status)
    {
        _status = status;
    }

    AddContentStatusMessage::AddContentStatus
    AddContentStatusMessage::getStatus() const
    {
        return _status;
    }

    void
    AddContentStatusMessage::setProgress(unsigned int progress)
    {
        _progress = progress;
    }

    unsigned int
    AddContentStatusMessage::getProgress()
    {
        return _progress;
    }
    void AddContentStatusMessage::setSkippedFileList(std::vector<std::string>& skippedFiles)
    {
        _skippedFiles = skippedFiles;
    }
    const std::vector<std::string>& AddContentStatusMessage::getSkippedFileList() const
    {
        return _skippedFiles;
    }
    std::vector<std::string>& AddContentStatusMessage::getSkippedFileList()
    {
        return _skippedFiles;
    }

    AddContentStatusMessage::~AddContentStatusMessage(){}
}
