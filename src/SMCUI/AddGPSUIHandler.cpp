/****************************************************************************
 *
 * File             : AddGPSGUI.h
 * Description      : AddGPSGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/
#ifdef WIN32
#include <SMCUI/AddGPSUIHandler.h>
#include <osgDB/FileUtils>
#include <Core/CoreRegistry.h>
#include <GPS/GPSPlugin.h>
#include <App/AccessElementUtils.h>
#include <Core/AttributeTypes.h>
#include <GPS/GPSPlugin.h>

#include <DB/ReaderWriter.h>
#include <DB/ReadFile.h>

#include <SMCUI/IAddContentStatusMessage.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, AddGPSUIHandler);
    DEFINE_IREFERENCED(SMCUI::AddGPSUIHandler, VizUI::UIHandler);

    AddGPSUIHandler::AddGPSUIHandler()
        : _type(IAddGPSUIHandler::None)
        , _source(NULL)
    {
        _addInterface(IAddGPSUIHandler::getInterfaceName());
    }

    AddGPSUIHandler::~AddGPSUIHandler()
    {}

    void 
    AddGPSUIHandler::loadSource(const std::string& fileName)
    {
        IAddContentStatusMessage::AddContentStatus status = IAddContentStatusMessage::PENDING;
        try
        {

        // Get world instance and add the source to world
        CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager<APP::IUIHandlerManager>(getManager());

        //no overloaded readFeature available - passing empty options and progressFunctor
        CORE::RefPtr<DB::ReaderWriter::Options> options; 
        
        void(*progressFunctPtr)(int);
        progressFunctPtr = NULL; 

        CORE::RefPtr<CORE::IObject> modelObject = DB::readFeature(fileName, options.get(), progressFunctPtr);
        world->addObject(modelObject.get());

        
        if(world && _source.valid())
        {
            world->addObject(_source->getInterface<CORE::IObject>(true));            
        }
        _source = NULL;
        return;
        }
        catch (UTIL::Exception& e)
        {
            e.LogException();

            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }
        catch (...)
        {
            // set status failure
            status = IAddContentStatusMessage::FAILURE;
        }

        // notify gui for status
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*IAddContentStatusMessage::AddContentStatusMessageType);
        msg->setSender(this);
        IAddContentStatusMessage* addContentStatusMsg = msg->getInterface<IAddContentStatusMessage>();
        if (addContentStatusMsg)
        {
            addContentStatusMsg->setStatus(status);
        }
        notifyObservers(*IAddContentStatusMessage::AddContentStatusMessageType, *msg);
    }

    void
    AddGPSUIHandler::setCurrentType(Type sourceType)
    {
        _type = sourceType;
        // Check whether the url exists or not       
        try
        {
            // Create the object of the desired type
            CORE::RefPtr<CORE::IObject> gpsobj = NULL;
            switch(_type)
            {
                //case IAddGPSUIHandler::NMEATCP:
                //    {
                //        // Create an object of type NMEATCPObject
                //        gpsobj = CORE::CoreRegistry::instance()->
                //                    getObjectFactory()->createObject(*GPS::GPSPlugin::NMEATCPObjectType);
                //    }
                //    break;

                case IAddGPSUIHandler::Log:
                    {
                        // Check the type of log file source and create object accordingly
                        gpsobj = CORE::CoreRegistry::instance()->
                                    getObjectFactory()->createObject(*GPS::GPSPlugin::NMEALogObjectType);
                    }
                    break;

                default:
                    break;
            }

            // Assign to source attribute
            _source = gpsobj->getInterface<GPS::IGPSSource>(true);
        }
        catch(UTIL::Exception& e)
        {
            e.LogException();
        }
    }
    
    CORE::Attribute* 
    AddGPSUIHandler::getSourceAttribute(const std::string& attrname) const
    {
        CORE::Attribute* attr = NULL;
        try
        {
            if(_source.valid())
            {
                attr = _source->getInterface<CORE::IAttributeHolder>(true)->getAttribute(attrname);
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }

        return attr;
    }
}
#endif //WIn32
