#include <SMCUI/HillshadeCalculationUIHandler.h>

#include <Terrain/IRasterObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <Core/ITerrain.h>

#include <VizUI/VizUIPlugin.h>
#include <Elements/ElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <Core/InterfaceUtils.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>
#include <Util/CoordinateConversionUtils.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCUI
{
    DEFINE_META_BASE(SMCUI, HillshadeCalculationUIHandler);

    DEFINE_IREFERENCED(HillshadeCalculationUIHandler, VizUI::UIHandler);

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////

    HillshadeCalculationUIHandler::HillshadeCalculationUIHandler()
    {
        _addInterface(IHillshadeCalculationUIHandler::getInterfaceName());
        setName(getClassname());
        // XXX need to set the area extent( optional), 
        //for that we need to request Area UI Handler
        _extents = new osg::Vec2Array;
        _zenithAngle = 0.0;
        _azimuthAngle = 0.0;
        _transparencyValue = 0.0;
        _reset();
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    HillshadeCalculationUIHandler::~HillshadeCalculationUIHandler(){}

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void HillshadeCalculationUIHandler::onAddedToManager()
    {
        _subscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    void HillshadeCalculationUIHandler::onRemovedFromManager()
    {
        _unsubscribe(getManager(), *APP::IApplication::ApplicationConfigurationLoadedType);
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void HillshadeCalculationUIHandler::setFocus(bool value)
    {
        // Reset value
        _reset();

        // Focus area handler and activate gui
        try
        {
            UIHandler::setFocus(value);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    bool HillshadeCalculationUIHandler::getFocus() const
    {
        return false;
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void HillshadeCalculationUIHandler::execute()
    {
        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        if (vfactory)
        {
            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
            if (world)
            {
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::HillshadeFilterVisitorType);

                if (!_visitor)
                {
                    LOG_ERROR("Failed to create filter class");
                    return;
                }

                GISCOMPUTE::IHillshadeFilterVisitor* iVisitor =
                    _visitor->getInterface<GISCOMPUTE::IHillshadeFilterVisitor>();


                //Set the zenith angle of the illumination source
                iVisitor->setZenithAngle(_zenithAngle);
                //Set the Azimuth angle of the illumination source
                iVisitor->setAzimuthAngle(_azimuthAngle);
                // Set the transparency coefficient
                iVisitor->setTransparency(_transparencyValue);
                //set color 
                iVisitor->setColor(_color);

                if (_extents->size() > 1)
                {
                    //set extent on visitor
                    iVisitor->setExtents(_extents.get());
                }
                // set output name
                iVisitor->setOutputName(_name);

                CORE::IBase* applyNode = NULL;
                if (!_elevObj)
                {
                    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                    applyNode = terrain->getInterface<CORE::IBase>();
                }
                else
                {
                    applyNode = _elevObj->getInterface<CORE::IBase>();
                }

                //subscribe for filter status
                _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                applyNode->accept(*_visitor);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // !brief 
    //
    ///////////////////////////////////////////////////////////////////
    void HillshadeCalculationUIHandler::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check for message type
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
        }
        else if (messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage =
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            try
            {
                if (filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
                {
                    CORE::RefPtr<CORE::IBase> base = message.getSender();
                    CORE::RefPtr<GISCOMPUTE::IHillshadeFilterVisitor> visitor =
                        base->getInterface<GISCOMPUTE::IHillshadeFilterVisitor>();

                    if (visitor.valid())
                    {
                        CORE::RefPtr<TERRAIN::IRasterObject> object = visitor->getOutput();

                        if (object)
                        {
                            CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(getManager());
                            if (world)
                            {
                                world->addObject(object->getInterface<CORE::IObject>(true));
                                /*CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                                terrain->addRasterObject(object);*/
                            }
                        }
                        else
                        {
                            throw UTIL::Exception("Failed to create raster object", __FILE__, __LINE__);
                        }
                    }
                }
            }
            catch (...)
            {
                filterMessage->setStatus(GISCOMPUTE::FILTER_FAILURE);
            }

            // notify GUI
            notifyObservers(messageType, message);
        }
        else
        {
            UIHandler::update(messageType, message);
        }
    }

    void HillshadeCalculationUIHandler::_reset()
    {
        _extents->clear();
        _elevObj = NULL;
        _visitor = NULL;
        _zenithAngle = 0.0;
        _azimuthAngle = 0.0;
        _transparencyValue = 0.0;
        _name = "";
    }

    void HillshadeCalculationUIHandler::setExtents(osg::Vec4d extents)
    {
        _extents->clear();
        osg::Vec2d first = osg::Vec2d(extents.x(), extents.y());
        osg::Vec2d second = osg::Vec2d(extents.z(), extents.w());
        _extents->push_back(osg::Vec2d(first.x(), first.y()));
        _extents->push_back(osg::Vec2d(second.x(), second.y()));
    }
    void HillshadeCalculationUIHandler::setZenithAngle(double angle)
    {
        _zenithAngle = angle;
    }
    void HillshadeCalculationUIHandler::setAzimuthAngle(double angle)
    {
        _azimuthAngle = angle;

    }
    void HillshadeCalculationUIHandler::setTransparency(double transparency)
    {
        _transparencyValue = transparency;

    }
    void HillshadeCalculationUIHandler::setUserDefineColor(osg::Vec4d ocolor)
    {
        _color = ocolor;

    }
    double HillshadeCalculationUIHandler::getZenithAngle() const
    {
        return _zenithAngle;
    }
    double HillshadeCalculationUIHandler::getAzimuthAngle() const
    {
        return _azimuthAngle;

    }
    double HillshadeCalculationUIHandler::getTransparency() const
    {
        return _transparencyValue;

    }
    osg::Vec4d HillshadeCalculationUIHandler::getUserDefineColor() const
    {
        return _color;

    }

    osg::Vec4d HillshadeCalculationUIHandler::getExtents() const
    {
        osg::Vec4d extents(0.0, 0.0, 0.0, 0.0);
        osg::Vec2d first = _extents->at(0);
        osg::Vec2d second = _extents->at(1);
        return osg::Vec4d(first.x(), first.y(), second.x(), second.y());
    }

    void HillshadeCalculationUIHandler::setElevationObject(TERRAIN::IElevationObject* obj)
    {
        _elevObj = obj;
    }

    void HillshadeCalculationUIHandler::setOutputName(std::string& name)
    {
        _name = name;
    }

    void HillshadeCalculationUIHandler::stopFilter()
    {
        if (_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor =
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }

} // namespace SMCUI

