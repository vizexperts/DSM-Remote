#include <SMCUI/CreateLayerUIHandler.h>

#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>

#include <Elements/ElementsPlugin.h>

#include <boost/algorithm/string.hpp>

namespace SMCUI
{
    DEFINE_IREFERENCED(CreateLayerUIHandler, VizUI::UIHandler);

    CreateLayerUIHandler::CreateLayerUIHandler()
    {
        _addInterface(SMCUI::ICreateLayerUIHandler::getInterfaceName());
    }

    const CORE::UniqueID*
        CreateLayerUIHandler::getLayerUniqueID() const
    {
        return NULL;
    }

    bool CreateLayerUIHandler::checkLayerNameAvailability(const std::string& name)
    {
        CORE::RefPtr<CORE::IWorld> world = this->getWorldInstance();

        if(!world.valid())
        {
            return false;
        }

        // need to layer feature object by iterating over the world instance
        const CORE::ObjectMap& objects = world->getObjectMap();
        CORE::ObjectMap::const_iterator iter = objects.begin();
        while(iter != objects.end())
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> metaDataTableHolder
                = iter->second->getInterface<CORE::IMetadataTableHolder>();
            if(metaDataTableHolder.valid())
            {
                if(boost::iequals(name, iter->second->getInterface<CORE::IBase>()->getName()))
                {
                    return false;
                }
            }
            ++iter;
        }
        return true;
    }

    bool CreateLayerUIHandler::createLayer(const std::string& layerName,
        const std::string& layerType,
        std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes,
        std::vector<int>& attrWidths,
        std::vector<int>& attrPrecisions)
    {
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
            CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IObject> featureLayer = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);

        CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
            featureLayer->getInterface<CORE::IMetadataTableHolder>();

        if(!tableHolder.valid())
        {
            LOG_ERROR("IMetadataTableHolder interface not found in FeatureLayer");
            return false;
        }

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = 
            tableHolder->getMetadataTable();

        if(!metadataTable.valid())
        {
            LOG_ERROR("Invalid IMetadataTable instance");
            return false;
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return false;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return false;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return false;
        }

        // create a new table definition
        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataCreator->createMetadataTableDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Unable to create a IMetadataTableDefn instance");
            return false;
        }

        for(int i = 0; i < (int)attrNames.size(); i++)
        {

            // create fields to store name, latitude, longitude and height
            CORE::RefPtr<CORE::IMetadataFieldDefn> field = 
                metadataCreator->createMetadataFieldDefn();

            field->setName(attrNames.at(i));
            std::string fieldType = attrTypes.at(i);
            if(fieldType == "Text")
            {
                field->setType(CORE::IMetadataFieldDefn::STRING);
            }
            else if(fieldType == "Decimal")
            {
                field->setType(CORE::IMetadataFieldDefn::DOUBLE);
            }
            else if(fieldType == "Integer")
            {
                field->setType(CORE::IMetadataFieldDefn::INTEGER);
            }

            field->setLength(attrWidths.at(i));
            field->setPrecision(attrPrecisions.at(i));

            // add the field to the table definition
            tableDefn->addFieldDefn(field.get());
        }

        CORE::IFeatureLayer::FeatureLayerType featureLayerType = 
            CORE::IFeatureLayer::UNKNOWN;
        if(layerType == "Point")
        {
            featureLayerType = CORE::IFeatureLayer::POINT;
        }
        else if(layerType == "Line")
        {
            featureLayerType = CORE::IFeatureLayer::LINE;
        }
        else if(layerType == "Polygon")
        {
            featureLayerType = CORE::IFeatureLayer::POLYGON;
        }
        else if(layerType == "Military Layer")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT;
        }
        else if(layerType == "Model")
        {
            featureLayerType = CORE::IFeatureLayer::MODEL;
        }
        else if(layerType == "Multipoint" || layerType == "Multi Point")
        {
            featureLayerType = CORE::IFeatureLayer::MULTIPOINT;
        }
        else if(layerType == "Multiline" || layerType == "Multi Line")
        {
            featureLayerType = CORE::IFeatureLayer::MULTILINE;
        }
        else if(layerType == "Multipolygon" || layerType == "Multi Polygon")
        {
            featureLayerType = CORE::IFeatureLayer::MULTIPOLYGON;
        }
        else if(layerType == "MilitaryLayerPoint")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_POINT;
        }
        else if(layerType == "MilitaryLayerLine")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_LINE;
        }
        else if(layerType == "MilitaryLayerPolygon")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_POLYGON;
        }
        else
        {
            featureLayerType = CORE::IFeatureLayer::UNKNOWN;
        }

        metadataTable->setFeatureLayerType(featureLayerType);

        CORE::RefPtr<CORE::IBase> base = featureLayer->getInterface<CORE::IBase>();
        if(base.valid())
        {
            base->setName(layerName);
        }

        // set the field definition to the MetadataTable
        metadataTable->setMetadataTableDefn(tableDefn.get(),featureLayer->getInterface<CORE::IBase>()->getUniqueID().toString());

        // add the layer to the world
        getWorldInstance()->addObject(featureLayer.get());

        return true;
    }
}
