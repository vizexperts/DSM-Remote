/****************************************************************************
*
* File             : CreateImageGUI.h
* Description      : CreateImageGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/CreateImageGUI.h>
#include <SMCQt/PropertyObject.h>

#include <App/AccessElementUtils.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransactionFactory.h>
#include <App/IUndoTransaction.h>
#include <App/IUIHandler.h>
#include <ELEMENTS/IIconLoader.h>
#include <ELEMENTS/IIconHolder.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/WorldMaintainer.h>
#include <Core/InterfaceUtils.h>
#include <Core/IDeletable.h>
#include <Core/IText.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITemporary.h>
#include <Core/IFeatureObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IPoint.h>
#include <Core/IPointState.h>
#include <CORE/IText.h>
#include <Core/CompositeObject.h>
//#include <Core/ITerrain.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IClamper.h>


#include <SMCElements/ILayerComponent.h>

#include <QFileDialog>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <iostream>

#include <Util/Defs.h>
#include <APP/IManager.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateImageGUI);
    DEFINE_IREFERENCED(CreateImageGUI, SMCQt::DeclarativeFileGUI);

    CreateImageGUI::CreateImageGUI()
        :_cleanup(true),
        _selectedFeatureLayer(NULL),
        _addToDefault(true),
        _objNumber(1)
    {

    }

    CreateImageGUI::~CreateImageGUI()
    {

    }

    void CreateImageGUI::_loadAndSubscribeSlots()
    {

        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild("smpMenu");
        if(smpMenu)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), 
                this, SLOT(connectSMPMenu(QString)), Qt::UniqueConnection);
        }

        QObject* contextualMenu = _findChild("contextualMenu");
        if(contextualMenu)
        {
            QObject::connect(contextualMenu, SIGNAL(loaded(QString)), 
                this, SLOT(connectContextualMenu(QString)), Qt::UniqueConnection);
        }

        //This is done to play the Audio or Video
        QObject::connect(this, SIGNAL(play(QString)), this, SLOT(playAudioOrVideo(QString)), Qt::UniqueConnection);
    }

    void CreateImageGUI::connectContextualMenu(QString type)
    {
        if(type == "ImagePoint")
        {
            // setActive
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_ATTRIBUTES);

            // populate contextual menu
            _populateContextualMenu();

            showPointAttributes();

            QObject* imageContextual = _findChild("imageContextual");
            if(imageContextual)
            {
                QObject::connect(imageContextual, SIGNAL(rename(QString)), this, 
                    SLOT(rename(QString)), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(setTextActive(bool)), this,
                    SLOT(setTextActive(bool)), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(handleLatLongAltChanged(QString)), this,
                    SLOT(handleLatLongAltChanged(QString)), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(deletePoint()), this, 
                    SLOT(deletePoint()), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(clampingChanged(bool)), this,
                    SLOT(clampingChanged(bool)), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(attachFile(QString)), this,
                    SLOT(attachFile(QString)), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(changeColor(QColor)), this, 
                    SLOT(colorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(changeTextSize(double)), this, 
                    SLOT(changeTextSize(double)), Qt::UniqueConnection);

                QObject::connect(imageContextual, SIGNAL(editEnabled(bool)), this,
                    SLOT(editEnabled(bool)), Qt::UniqueConnection);

                _subscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointUpdatedMessageType);

                CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
                if(selectionUIHandler.valid())
                {
                    selectionUIHandler->setSelectionState(false);
                }

            }

        }
        else if(type == "")
        {
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
            _unsubscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointUpdatedMessageType);

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setSelectionState(true);
            }
        }
    }

    void CreateImageGUI::attachFile(QString type)
    {
        if(!_pointUIHandler.valid())
            return;

        std::string filters;

        if(!type.compare("Image"))
        {
            filters = "Images (*.jpeg *.jpg *.png *.bmp)";
        }

        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();

        QString filepath = QFileDialog::getOpenFileName(parent, type.toStdString().c_str(), 
            "c:/", filters.c_str());

        std::string fileChosen = filepath.toStdString();;

        if(osgDB::fileExists(fileChosen))
        {
            QObject* imageContextual = _findChild("imageContextual");
            if(imageContextual)
            {
                if(!type.compare("Image"))
                {
                    //Copy the images to the project Directory
                    CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                                                    getGUIManager()->getInterface<APP::IManager>()->getWorldMaintainer();
                    if(worldMaintainer.valid())
                    {
                        std::string projectLocation = _getProjectLocation();
                        projectLocation  += "/" + osgDB::getSimpleFileName(fileChosen);
                        projectLocation = osgDB::convertFileNameToNativeStyle(projectLocation);
                        osgDB::copyFile(fileChosen, projectLocation);
                    }

                    _pointUIHandler->addAssociatedImage(fileChosen);

                    _imageList.prepend(QString::fromStdString("file:///" + fileChosen));

                    _setContextProperty("imageList",QVariant::fromValue(_imageList));
                }
            }
        }
    }

    void CreateImageGUI::editEnabled(bool value)
    {
        if(value)
        {
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_POINT);
        }
        else
        {
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_ATTRIBUTES);
        }
    }

    void CreateImageGUI::colorChanged(QColor color)
    {
        if(!_pointUIHandler.valid())
            return;

        if(color.isValid())
        {
            osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if(point.valid())
            {
                CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                if(text.valid())
                {
                    osg::Vec4  currentColor = text->getTextColor();
                    if(penColor != currentColor)
                    {
                        text->setTextColor(penColor);
                    }
                }
            }
        }
    }

    void CreateImageGUI::changeTextSize(double textSize)
    {
        if(!_pointUIHandler.valid())
            return;

        if(textSize <= 0)
            return;

        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        if(point.valid())
        {
            CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
            if(text.valid())
            {
                text->setTextSize(textSize);
            }
        }
    }


    std::string CreateImageGUI::_getProjectLocation()
    {
        std::string projectLocation;
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
        getGUIManager()->getInterface<APP::IManager>()->getWorldMaintainer();
        
        if(worldMaintainer.valid())
        {
            projectLocation = osgDB::getFilePath(worldMaintainer->getProjectFile());
        }

        return projectLocation;
    }

    void CreateImageGUI::showPointAttributes()
    {

        CORE::RefPtr<CORE::IMetadataRecord> tableRecord = 
            _pointUIHandler->getMetadataRecord();

        if(!tableRecord.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
        if(!tableDefn.valid())
            return;

        int tableColumnCount = tableDefn->getFieldCount();

        _attributeList.clear();

        bool isDescriptionPresent = false;
        QString description;

        for(int i = 0 ; i < tableColumnCount; i++)
        {
            CORE::RefPtr<CORE::IMetadataField> field = tableRecord->getFieldByIndex(i);
            if(field)
            {
                CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = field->getMetadataFieldDef();
                if(fieldDefn.valid())
                {
                    CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                    QString fieldName = QString::fromStdString(fieldDefn->getName());
                    fieldName = fieldName.trimmed();

                    if((fieldName == "Description"))
                    {
                        description = QString::fromStdString(field->toString());
                        if(description.contains("<html>"))
                        {
                            isDescriptionPresent = true;
                            continue;
                        }
                    }
                    else if(fieldName.contains("_VIZ_", Qt::CaseInsensitive))
                    {
                        continue;
                    }
                    else if(fieldName.compare("name", Qt::CaseInsensitive) == 0)
                    {
                        continue;
                    }
                    else if(fieldName.isEmpty())
                    {
                        continue;
                    }

                    QString fieldType;
                    QString fieldValue;
                    int fieldPrecesion = 1;

                    switch (type)
                    {
                        case CORE::IMetadataFieldDefn::STRING:
                        {
                            fieldType = "String";
                            fieldValue = QString::fromStdString(field->toString());
                            fieldPrecesion = fieldDefn->getLength();
                        }
                        break;
                        case CORE::IMetadataFieldDefn::INTEGER:
                        {
                            fieldType = "Integer";
                            fieldValue = QString::number(field->toInt());
                        }
                        break;
                        case CORE::IMetadataFieldDefn::DOUBLE:
                        {
                            fieldType = "Decimal";
                            fieldValue = QString::number(field->toDouble());
                            fieldPrecesion = fieldDefn->getPrecision();
                        }
                        break;
                    }

                    _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecesion));
                }
            }
        }

        if(_attributeList.empty())
        {
            QObject* imageContextual = _findChild("imageContextual");
            if(imageContextual)
            {
                imageContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
            }

            return;
        }

        QObject* showAttributes= _findChild("imageAttributes");
        if(showAttributes != NULL)
        {
            _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
            if(isDescriptionPresent)
            {
                showAttributes->setProperty("description", QVariant::fromValue(description));
            }

            QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this, 
                SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

            QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);

        }
    }

    void CreateImageGUI::handleAttributesCancelButtonPressed()
    {
        showPointAttributes();
    }

    void CreateImageGUI::handleAttributesOkButtonPressed()
    {
        CORE::RefPtr<CORE::IMetadataRecord> tableRecord = 
            _pointUIHandler->getMetadataRecord();

        if(!tableRecord.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
        if(!tableDefn.valid())
            return;

        foreach(QObject* qobj, _attributeList)
        {
            PropertyObject* propObj = qobject_cast<PropertyObject*>(qobj);
            if(propObj)
            {
                CORE::RefPtr<CORE::IMetadataField> field = 
                    tableRecord->getFieldByName(propObj->name().toStdString());
                if(field.valid())
                {
                    CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = 
                        field->getMetadataFieldDef();

                    CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                    switch (type)
                    {
                    case CORE::IMetadataFieldDefn::INTEGER:
                        {
                            field->fromInt(propObj->value().toInt());
                        }
                        break;
                    case CORE::IMetadataFieldDefn::DOUBLE:
                        {
                            field->fromDouble(propObj->value().toDouble());
                        }
                        break;
                    case CORE::IMetadataFieldDefn::STRING:
                        {
                            field->fromString(propObj->value().toStdString());
                        }
                        break;
                    }
                }
            }
        }
    }

    void CreateImageGUI::_populateContextualMenu()
    {
        QObject* imageContextual = _findChild("imageContextual");
        if(imageContextual != NULL)
        {
            //Get the selected point
            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if(!point.valid())
            {
                return;
            }

            // Populate data for contextual tab

            //! Name of the point
            QString name = QString::fromStdString(point->getInterface<CORE::IBase>()->getName());

            //! text active state
            bool textActive = point->getInterface<CORE::IText>()->getTextActive();

            //! text Size
            double currentTextSize = 0.0;
            currentTextSize = point->getInterface<CORE::IText>()->getTextSize();

            //! text color
            osg::Vec4 textColor = point->getInterface<CORE::IText>()->getTextColor();
            QColor qtextColor(int(textColor.r()*255),int(textColor.g()*255),int(textColor.b()*255),int(textColor.a()*255));

            //iconPath
            ELEMENTS::IIconHolder* iconHolder = point->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolder)
            {
                ELEMENTS::IIcon* icon = iconHolder->getIcon();
                if(icon)
                {
                    std::string iconPath = icon->getIconFilename();
                    iconPath = "file:/" + osgDB::getRealPath(iconPath);
                    imageContextual->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(iconPath)));
                }
            }

            bool clampingEnabled = true;
            CORE::RefPtr<ELEMENTS::IClamper> clamper = point->getInterface<ELEMENTS::IClamper>();
            if(clamper.valid())
            {
                clampingEnabled = clamper->clampOnPlacement();
            }

            //! Lat-Long-Alt of point
            osg::Vec3d position = point->getValue();                    
            double altitudeD = position.z() - _getAltitudeAtLongLat(position.x(), position.y());
            if(altitudeD < 0)
            {
                altitudeD = 0.0;
            }
            QString latitude = QString::number(position.y(), 'f', 4);;
            QString longitude = QString::number(position.x(), 'f', 4);
            QString altitude = QString::number(altitudeD, 'f', 4);
            //! keeping first 4 decimal places

            // populating attached files
            // checking weather the fields are available 
            CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder = 
                point->getInterface<CORE::IMetadataRecordHolder>(true);
            CORE::RefPtr<CORE::IMetadataRecord> record = recordHolder->getMetadataRecord();

            if(record.valid())
            {
                CORE::RefPtr<CORE::IMetadataField> metaField = record->getFieldByName("_VIZ_AUDIO");
                if(!metaField.valid())
                {
                    imageContextual->setProperty("hyperlinkTabVisible", QVariant::fromValue(false));
                }
            }

            std::vector<std::string> imageVector;
            _pointUIHandler->getAssociatedImages(imageVector);

            std::string projectLocation = _getProjectLocation();

            _imageList.clear();
            for(unsigned int i = 0; i < imageVector.size(); i++)
            {
                _imageList.prepend(QString::fromStdString("file:///" + projectLocation + "/" + imageVector[i]));                
            }

            imageContextual->setProperty("name", QVariant::fromValue(name));
            imageContextual->setProperty("textActive", QVariant::fromValue(textActive));
            imageContextual->setProperty("latitude", QVariant::fromValue(latitude));
            imageContextual->setProperty("longitude", QVariant::fromValue(longitude));
            imageContextual->setProperty("altitude", QVariant::fromValue(altitude));
            imageContextual->setProperty("clampingEnabled", QVariant::fromValue(clampingEnabled));
            imageContextual->setProperty("textSize", QVariant::fromValue(currentTextSize));
            imageContextual->setProperty("textColor", QVariant::fromValue(qtextColor));
           // imageContextual->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(iconPath)));

            _setContextProperty("imageList", QVariant::fromValue(_imageList));
        }
    }

    void CreateImageGUI::deletePoint()
    {
        if(!_pointUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        if(!point.valid())
        {
            return;
        }

        CORE::IDeletable* deletable = point->getInterface<CORE::IDeletable>();
        if(!deletable)
        {
            return;
        }

        CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
        if(deletionUIHandler.valid())
        {
            deletionUIHandler->deleteObject(deletable);
        }
    }

    void CreateImageGUI::clampingChanged(bool value)
    {
        if(!_pointUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        if(point.valid())
        {
            CORE::RefPtr<ELEMENTS::IClamper> clamper = point->getInterface<ELEMENTS::IClamper>();
            if(clamper.valid())
            {
                clamper->setClampOnPlacement(value);
            }
        }
    }

    void CreateImageGUI::handleLatLongAltChanged(QString attribute)
    {
        if(!_pointUIHandler.valid())
        {
            return;
        }

        //! get current point
        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        if(point.valid())
        {
            QObject* imageContextual = _findChild("imageContextual");
            if(imageContextual != NULL)
            {
                double longitudeD = point->getX();
                double latitudeD = point->getY();
                double altitudeD = point->getZ() - _getAltitudeAtLongLat(longitudeD, latitudeD);

                if(!attribute.compare("latitude", Qt::CaseInsensitive))
                {
                    QString latitude = imageContextual->property("latitude").toString();
                    latitudeD = latitude.toDouble();
                }else if(!attribute.compare("longitude", Qt::CaseInsensitive))
                {
                    QString longitude = imageContextual->property("longitude").toString();
                    longitudeD = longitude.toDouble();
                }else if(!attribute.compare("altitude", Qt::CaseInsensitive))
                {
                    QString altitude = imageContextual->property("altitude").toString();
                    altitudeD = altitude.toDouble();
                }

                osg::Vec3 position(longitudeD, latitudeD, altitudeD + _getAltitudeAtLongLat(longitudeD, latitudeD));

                point->setValue(position);
            }
        }
    }

    void CreateImageGUI::connectSMPMenu(QString type)
    {
        if(type == "markingFeaturesMenu")
        {
            // Clear the current selection
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if(selectionUIHandler.valid())
            {
                selectionUIHandler->clearCurrentSelection();
            }

            QObject* markingFeaturesMenu = _findChild("markingFeaturesMenu");
            if(markingFeaturesMenu != NULL)
            {
                QObject::connect(markingFeaturesMenu, SIGNAL(setImageEnable(bool)),
                    this, SLOT(setImageEnable(bool)), Qt::UniqueConnection);
            }
        }
        else
        {

        }
    }

    void CreateImageGUI::_resetForDefaultHandling()
    {
        _addToDefault = true;
        _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);

        setActive(false);
    }

    void CreateImageGUI::addPointsToSelectedLayer(bool value)
    {
        if(value)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                _selectedFeatureLayer = iter->second->getInterface<CORE::IFeatureLayer>();
                _addToDefault = false;
            }

            setActive(true);
        }
        else
        {
            _resetForDefaultHandling();
        }
    }


    void CreateImageGUI::setImageEnable(bool value)
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if(getActive() != value)
        {
            if(selectionUIHandler.valid())
            {
                selectionUIHandler->clearCurrentSelection();
            }
        }
        setActive(value);
    }


    void CreateImageGUI::rename(QString newName)
    {
        if(!_pointUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        if(point.valid())
        {
            point->getInterface<CORE::IBase>()->setName(newName.toStdString());
            //            point->getInterface<CORE::IText>()->setText(newName.toStdString());
        }
    }


    void CreateImageGUI::setTextActive(bool state)
    {
        if(!_pointUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        if(point.valid())
        {
            point->getInterface<CORE::IText>()->setTextActive(state);
        }
    }

    void CreateImageGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _loadPointUIHandler2();
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType==*CORE::IWorld::WorldLoadedMessageType)
        {
            _performHardReset();
        }
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            _populateAttributes();

            if(_addToDefault)
            {
                _pointUIHandler->addPointToCurrentSelectedLayer();
            }
            else
            {
                _pointUIHandler->addToLayer(_selectedFeatureLayer);
            }
            _createMetadataRecord();
        }
        else if(messageType == *SMCUI::IPointUIHandler2::PointUpdatedMessageType)
        {
            _populateContextualMenu();
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

    void CreateImageGUI::_performHardReset()
    {
        _pointNumber = 1;
        _cleanup     = true;

        setActive(false);

        if(_pointUIHandler.valid())
            _pointUIHandler->reset();
    }

    void CreateImageGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();
    }

    void CreateImageGUI::setActive(bool value)
    {
        // Check if _pointuihandler is valid
        if(!_pointUIHandler.valid())
        {   
            // try to load
            _loadPointUIHandler2();
            // Check again..
            if(!_pointUIHandler.valid())
            {
                return;
            }
        }

        if(getActive() == value)
        {
            return;
        }

        if(value)
        {
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
            _pointUIHandler->setTemporaryState(true);
            _pointUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _subscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
        }
        else
        {
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
            _pointUIHandler->setTemporaryState(false);
            _pointUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            _unsubscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
        }
        QtGUI::setActive(value);
    }

    void CreateImageGUI::onAddedToGUIManager()
    {   
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void CreateImageGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void CreateImageGUI::_loadPointUIHandler2()
    {
        _pointUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
    }

    void CreateImageGUI::_populateAttributes()
    {
        std::string name("image");

        //std::string layerName="";
        //unsigned int numObjects=0;

        /*if(_addToDefault)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
				getGUIManager()->getInterface<APP::IManager>()->getWorldMaintainer();
            if(worldMaintainer.valid())
            {
                CORE::RefPtr<CORE::IComponent> component = 
                    worldMaintainer->getComponentByName("LayerComponent");
                if(component.valid())
                {
                    CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
                        component->getInterface<SMCElements::ILayerComponent>();
                    if(layerComponent)
                    {
                        CORE::RefPtr<CORE::IFeatureLayer> layer = layerComponent->getFeatureLayer("Point");
                        if(layer.valid())
                        {
                            CORE::IBase *base = layer->getInterface<CORE::IBase>();
                            if(base)
                            {
                                layerName = base->getName();
                            }

                            CORE::ICompositeObject *compositeObject = layer->getInterface<CORE::ICompositeObject>();
                            if(compositeObject)
                            {
                                numObjects = compositeObject->getObjectMap().size();
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if(_selectedFeatureLayer.valid())
            {
                CORE::IBase *base = _selectedFeatureLayer->getInterface<CORE::IBase>();
                if(base)
                {
                    layerName = base->getName();
                }

                CORE::ICompositeObject *compositeObject = _selectedFeatureLayer->getInterface<CORE::ICompositeObject>();
                if(compositeObject)
                {
                    numObjects = compositeObject->getObjectMap().size();
                }
            }
        }*/

        /*if(!layerName.empty())
        {
            name = layerName;
        }*/


        name += UTIL::ToString<unsigned int>(_objNumber);

        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();   
        if(!point.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");
        CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = component->getInterface<ELEMENTS::IIconLoader>();
        ELEMENTS::IIcon* icon = iconLoader->getIconByName("image");
        point->getInterface<ELEMENTS::IIconHolder>()->setIcon(icon);

        point->getInterface<CORE::IBase>()->setName(name);
        CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
        if(text.valid())
        {
            text->setText(name);
            text->setTextActive(true);
        }

        CORE::RefPtr<CORE::IPointState> pointState = dynamic_cast<CORE::IPointState*>(point.get());
        //CORE::RefPtr<CORE::IPointState> pointState = point->getInterface<CORE::IPointState>();
        if(pointState.valid())
        {
            pointState->setPointState(CORE::IPointState::IMAGE_POINT);
        }

        //updating the index
        ++ _pointNumber;

        ++_objNumber;
    }

    void CreateImageGUI::_createMetadataRecord()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            getGUIManager()->getInterface<APP::IManager>()->getWorldMaintainer();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = NULL;
        if(_addToDefault)
        {
            tableDefn = 
                _pointUIHandler->getCurrentSelectedLayerDefn();
        }
        else
        {
            try
            {
                if(_selectedFeatureLayer.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                        _selectedFeatureLayer->getInterface<CORE::IMetadataTableHolder>(true);

                    if(holder.valid())
                    {
                        tableDefn = holder->getMetadataTable()->getMetadataTableDefn();
                    }
                }
            }
            catch(UTIL::Exception &e)
            {
                e.LogException();
            }
        }

        if(!tableDefn.valid())
        {
            LOG_ERROR("Invalid IMetadataTableDefn instance");
            return;
        }

        //create metadata record for the min max point
        CORE::RefPtr<CORE::IMetadataRecord> record = 
            metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        //Set the value of record attribute equal to textfield value...
        CORE::RefPtr<CORE::IMetadataField> field =
            record->getFieldByName("NAME");
        if(field)
        {
            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if(point != NULL)
            {
                field->fromString(point->getInterface<CORE::IBase>()->getName());
            }
        }
        _pointUIHandler->setMetadataRecord(record.get());
    }
} // namespace SMCQt
