/****************************************************************************
*
* File             : CreateImageGUI.h
* Description      : CreateImageGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/LayerPropertiesGUI.h>
#include <Core/WorldMaintainer.h>
#include <APP/IManager.h>
#include <App/ApplicationRegistry.h>
#include <App/AccessElementUtils.h>

#include <VizUI/ISelectionUIHandler.h>
#include <Core/IPenStyle.h>
#include <Core/IBrushStyle.h>
#include <Terrain/IVectorObject.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, LayerPropertiesGUI);
    DEFINE_IREFERENCED(LayerPropertiesGUI, SMCQt::DeclarativeFileGUI);

    LayerPropertiesGUI::LayerPropertiesGUI()
        :_selectedFeatureLayer(NULL)
    {

    }

    LayerPropertiesGUI::~LayerPropertiesGUI()
    {

    }

    void LayerPropertiesGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();
    }

    void LayerPropertiesGUI::onAddedToGUIManager()
    {   
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void LayerPropertiesGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }


    void LayerPropertiesGUI::_loadAndSubscribeSlots()
    {

        

        QObject* smpMenu = _findChild("smpMenu");
        if(smpMenu)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), 
                this, SLOT(connectSMPMenu(QString)), Qt::UniqueConnection);
        }

		DeclarativeFileGUI::_loadAndSubscribeSlots();



    }

    void LayerPropertiesGUI::connectSMPMenu(QString type)
    {
        if(type == "vectorColorChanger")
        {
		  CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
		  QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());

		  QObject* VectorColorChanger = item->findChild<QObject*>("vectorColorChangerObject");

			if(VectorColorChanger)
			{
					QObject::connect(VectorColorChanger, SIGNAL(changeColor(QColor)), this, 
						SLOT(colorChanged(QColor)), Qt::UniqueConnection);
			}

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                _selectedFeatureLayer = iter->second->getInterface<CORE::IFeatureLayer>();
            }

			if(_selectedFeatureLayer)
			{

				CORE::RefPtr<ELEMENTS::IVectorObject> vObject = 
					_selectedFeatureLayer->getInterface<ELEMENTS::IVectorObject>();
				if(vObject)
				{
					CORE::RefPtr<CORE::IPenStyle> pen = 
						_selectedFeatureLayer->getInterface<CORE::IPenStyle>();

					osg::Vec4 penColor = pen->getPenColor();

					QColor color(penColor[0]*255.0f, penColor[1]*255.0f, penColor[2]*255.0f, penColor[3]*255.0f);

					VectorColorChanger->setProperty( "vectorColor", QVariant::fromValue(color));
					VectorColorChanger->setProperty( "isValidVector", QVariant::fromValue(true));
				}
				else
				{
					VectorColorChanger->setProperty( "isValidVector", QVariant::fromValue(false));
					showError("Editable Vector Data", "Select geometries and change color from the contextual dialog");
				}
			}
        }
        else if(type == "vectorFillColorChanger")
        {
		  CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
		  QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());

		  QObject* VectorFillColorChanger = item->findChild<QObject*>("vectorFillColorChangerObject");

			if(VectorFillColorChanger)
			{
					QObject::connect(VectorFillColorChanger, SIGNAL(changeFillColor(QColor)), this, 
						SLOT(fillColorChanged(QColor)), Qt::UniqueConnection);
			}

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                _selectedFeatureLayer = iter->second->getInterface<CORE::IFeatureLayer>();
            }

			if(_selectedFeatureLayer)
			{

				CORE::RefPtr<ELEMENTS::IVectorObject> vObject = 
					_selectedFeatureLayer->getInterface<ELEMENTS::IVectorObject>();
				if(vObject)
				{
					CORE::RefPtr<CORE::IPenStyle> pen = 
						_selectedFeatureLayer->getInterface<CORE::IPenStyle>();

					osg::Vec4 penColor = pen->getPenColor();

					QColor color(penColor[0]*255.0f, penColor[1]*255.0f, penColor[2]*255.0f, penColor[3]*255.0f);

					VectorFillColorChanger->setProperty( "vectorColor", QVariant::fromValue(color));
					VectorFillColorChanger->setProperty( "isValidVector", QVariant::fromValue(true));
				}
				else
				{
					VectorFillColorChanger->setProperty( "isValidVector", QVariant::fromValue(false));
					showError("Editable Vector Data", "Select geometries and change color from the contextual dialog");
				}
			}
        }
        else
        {

        }
    }

    void LayerPropertiesGUI::colorChanged(QColor color)
    {

        if(color.isValid())
        {


			if(_selectedFeatureLayer)
			{
				CORE::RefPtr<CORE::IPenStyle> pen = 
					_selectedFeatureLayer->getInterface<CORE::IPenStyle>();

				if(pen.valid())
				{
					osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
						(float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);
					pen->setPenColor(penColor);
				}
				CORE::RefPtr<ELEMENTS::IVectorObject> vObject = 
					_selectedFeatureLayer->getInterface<ELEMENTS::IVectorObject>();
				if(vObject)
				{
					vObject->updateModelOptions();
				}
			}
        }
    }

    void LayerPropertiesGUI::fillColorChanged(QColor color)
    {

        if(color.isValid())
        {


			if(_selectedFeatureLayer)
			{
				CORE::RefPtr<CORE::IBrushStyle> brush = 
					_selectedFeatureLayer->getInterface<CORE::IBrushStyle>();

				if(brush.valid())
				{
					osg::Vec4 fillColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
						(float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);
					brush->setBrushState((CORE::IBrushStyle::BrushState)(CORE::IBrushStyle::ON | CORE::IBrushStyle::OVERRIDE));
					brush->setBrushFillColor(fillColor);
				}
				CORE::RefPtr<ELEMENTS::IVectorObject> vObject = 
					_selectedFeatureLayer->getInterface<ELEMENTS::IVectorObject>();
				if(vObject)
				{
					vObject->updateModelOptions();
				}
			}
        }
    }

    void LayerPropertiesGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType==*CORE::IWorld::WorldLoadedMessageType)
        {

        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

} // namespace SMCQt
