/****************************************************************************
*
* File             : TimeVaryingRasterGUI.h
* Description      : TimeVaryingRasterGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/TimeVaryingRasterGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>
#include <Core/WorldMaintainer.h>
#include <Terrain/ITimeVaryingRasterComponent.h>
#include <VizQt/QtUtils.h>
#include <Core/IObjectFactory.h>
#include <Core/IWorld.h>
#include <Core/CoreRegistry.h>
#include <iostream>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, TimeVaryingRasterGUI);

    TimeVaryingRasterGUI::TimeVaryingRasterGUI() : _guiObject(NULL)
    {
        setName("TimeVaryingRasterGUI");
    }

    TimeVaryingRasterGUI::~TimeVaryingRasterGUI()
    {
        _guiObject = NULL;
        setActive(false);
    }

    void
    TimeVaryingRasterGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild("popupLoader");
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, SLOT(popupLoaded(QString)),
                Qt::UniqueConnection);
        }
    }

    void 
    TimeVaryingRasterGUI::popupLoaded(QString type)
    {
        _guiObject = NULL;

        if(type == "addTimedRaster")
        {
            _guiObject = _findChild("addTimedRaster");
            if(_guiObject != NULL)
            {
                QObject::connect(_guiObject, SIGNAL(addRasterButtonClicked()), this,
                                 SLOT(addRasterFile()), Qt::UniqueConnection);
            }
        }
    }

    void 
    TimeVaryingRasterGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());

                const CORE::ComponentMap& cmap = wmain->getComponentMap();
                _selectionComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);

            

                _timeVaryingRasterUIHandler =  APP::AccessElementUtils::
                    getUIHandlerUsingManager<SMCUI::ITimeVaryingRasterUIHandler>(getGUIManager());

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void 
    TimeVaryingRasterGUI::addRasterFile()
    {
        if(!_selectionComponent.valid() || _guiObject==NULL)
            return;

         const CORE::ISelectionComponent::SelectionMap& selectionMap = 
                _selectionComponent->getCurrentSelection();

        if(selectionMap.empty())
        {
            return;
        }

        CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
        CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

        if(!selectedObject)
            return;

        ELEMENTS::IRasterObject* raster = selectedObject->getInterface<ELEMENTS::IRasterObject>();
        if(raster)
        {
            //Getting the time from the object
            QVariant time = _guiObject->property("rasterTime");
            QDateTime dataTime = time.toDateTime();

            //co  nverting QT dataTime to boost Date Time
            boost::posix_time::ptime boostDateTime = VizQt::TimeUtils::QtToBoostDateTime(dataTime);

            _timeVaryingRasterUIHandler->addRasterObject(raster, boostDateTime);
            return;
        }
    }

    void 
    TimeVaryingRasterGUI::initializeAttributes()
    {
        VizQt::QtGUI::initializeAttributes();
    }

    void 
    TimeVaryingRasterGUI::setActive(bool value)
    {
        VizQt::QtGUI::setActive(value);
    }
} // namespace SMCQt

