/****************************************************************************
*
* File             : WeatherControllerGUI.h
* Description      : WeatherControllerGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/WeatherControllerGUI.h>
#include <App/AccessElementUtils.h>

#include <SMCUI/IAnimationUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <SMCQt/TimelineMark.h>
#include <SMCQt/VizComboBoxElement.h>
#include <SMCQt/IContextualTabGUI.h>
#include <VizQt/QtUtils.h>

#include <Elements/IMilitarySymbol.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, WeatherControllerGUI);
    DEFINE_IREFERENCED(WeatherControllerGUI, SMCQt::DeclarativeFileGUI);

    WeatherControllerGUI::WeatherControllerGUI()
    {

    }

    WeatherControllerGUI::~WeatherControllerGUI()
    {

    }

    void WeatherControllerGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

         QObject* popupLoader = _findChild("popupLoader");
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }
    }

    void WeatherControllerGUI::popupLoaded(QString type)
    {
        QObject* addWeatherEvent = _findChild("addEvents");
        if(addWeatherEvent)
        {            
            if(!_wcUIHandler.valid())
            {
                return;
            }

           // bool visibilityAdded = _vcUIHandler->setSelectedObjectAsCurrent();

           
            _populateControlPoints();

            /*QObject* eventContextual = _findChild("eventContextual");
            if(eventContextual)*/
            //{
            //    CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            //        APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            //    if(!selectionUIHandler.valid())
            //    {
            //        return;
            //    }

            //    // Selection Map
            //    const CORE::ISelectionComponent::SelectionMap& map = 
            //        selectionUIHandler->getOriginalSelectionMap();

            //    CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            //        map.begin();

            //    //XXX - using the first element in the selection
            //    if(iter == map.end())
            //    {
            //        CORE::RefPtr<SMCQt::IContextualTabGUI> contextualGUI = 
            //            APP::AccessElementUtils::getGUIUsingManager<SMCQt::IContextualTabGUI>(getGUIManager());
            //        if(contextualGUI.valid())
            //        {
            //            contextualGUI->closeContextualTab();
            //        }
            //        return;
            //    }

                //Get the selected object
               /* CORE::RefPtr<CORE::IObject> object = iter->second->getInterface<CORE::IObject>();
                if(!object.valid())
                {
                    return;
                }*/

               /* CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = object->getInterface<ELEMENTS::IMilitarySymbol>();
                if(unit.valid())
                {
                    eventContextual->setProperty("movementTabVisible", QVariant::fromValue(true));
                }
                else
                {
                    eventContextual->setProperty("movementTabVisible", QVariant::fromValue(false));
                }*/

                QObject::connect(addWeatherEvent, SIGNAL(selectControlPoint(QString)), 
                    this, SLOT(selectControlPoint(QString)), Qt::UniqueConnection);

                QObject::connect(addWeatherEvent, SIGNAL(addControlPoint(QDateTime, bool, bool)), 
                    this, SLOT(addControlPoint(QDateTime, bool, bool)), Qt::UniqueConnection);

                QObject::connect(addWeatherEvent, SIGNAL(editControlPoint(QString, QDateTime, bool, bool)), 
                    this, SLOT(editControlPoint(QString, QDateTime, bool, bool)), Qt::UniqueConnection);

                //QObject::connect(eventContextual, SIGNAL(tabChanged(QString)), this,
                //    SLOT(tabSelected(QString)), Qt::UniqueConnection);

                QObject::connect(addWeatherEvent, SIGNAL(deleteControlPoint(QString)), 
                    this, SLOT(deleteControlPoint(QString)));

                CORE::RefPtr<SMCUI::IAnimationUIHandler> animationUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());
                if(animationUIHandler.valid())
                {
                    const boost::posix_time::ptime& currentTime = 
                        animationUIHandler->getCurrentDateTime();

                    QDateTime qCurTime;
                    VizQt::TimeUtils::BoostToQtDateTime(currentTime, qCurTime);
                    addWeatherEvent->setProperty("currentTime", QVariant::fromValue(qCurTime));
                }
                        
        }
      
    }

    //void WeatherControllerGUI::tabSelected(QString name)
    //{
    //    if(name == "Visibility")
    //    {
    //        _populateControlPoints();
    //    }
    //    else 
    //    {
    //        _setContextProperty("visibilityListModel", QVariant::fromValue(NULL));
    //        //_setContextProperty("timelineModel", QVariant::fromValue(NULL));
    //    }
    //}

    void WeatherControllerGUI::selectControlPoint(QString cpID)
    {
        unsigned int id = cpID.toInt();
        if(id == 0) // this means Add checkpoint mode is selected
        {
            return;
        }

        const ENV::IWeatherControllerComponent::ControlPointList& controlPointList = _wcUIHandler->getControlPointList();
        ENV::IWeatherControllerComponent::ControlPointList::const_iterator iter = controlPointList.begin();

        unsigned index = 0;
        for(; index < (id-1) && iter != controlPointList.end(); ++index, ++iter);

        if(index == (id-1) && iter != controlPointList.end())
        {
            const boost::posix_time::ptime& boostCPTime = iter->time;
            bool rainState = iter->rainState;
            bool snowState = iter->snowState;

            QDateTime qCPTime;
            VizQt::TimeUtils::BoostToQtDateTime(boostCPTime, qCPTime);

            QObject* weatherEvent = _findChild("weatherEvent");
            if(weatherEvent != NULL)
            {
                weatherEvent->setProperty("weatherEventTime", QVariant::fromValue(qCPTime));
                weatherEvent->setProperty("rainState", QVariant::fromValue(rainState));
                weatherEvent->setProperty("snowState", QVariant::fromValue(snowState));
            }
        }        
    }

    void WeatherControllerGUI::addControlPoint(QDateTime cpTime, bool rainState, bool snowState)
    {
        if(!_wcUIHandler.valid())
        {
            return;
        }

        boost::posix_time::ptime boostCpTime = VizQt::TimeUtils::QtToBoostDateTime(cpTime);

        _wcUIHandler->addControlPoint(boostCpTime, rainState, snowState);

        _populateControlPoints();
    }

    void WeatherControllerGUI::editControlPoint(QString cpID, QDateTime cpTime, bool rainState, bool snowState)
    {

        if(!_wcUIHandler.valid())
        {
            return;
        }

        int id = cpID.toInt();
        if(id == 0)
        {
            return;
        }

        _wcUIHandler->removeControlPoint(id - 1);

        boost::posix_time::ptime boostCpTime = VizQt::TimeUtils::QtToBoostDateTime(cpTime);

        _wcUIHandler->addControlPoint(boostCpTime, rainState, snowState);

        _populateControlPoints();
    }

    void WeatherControllerGUI::deleteControlPoint(QString cpID)
    {
        if(!_wcUIHandler.valid())
        {
            return;
        }

        int id = cpID.toInt();
        if(id == 0)
        {
            return;
        }

        _wcUIHandler->removeControlPoint(id - 1);

        _populateControlPoints();

    }

    void WeatherControllerGUI::_populateControlPoints()
    {
        // get animation uihandler
        CORE::RefPtr<SMCUI::IAnimationUIHandler> animationUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());
        if(!animationUIHandler.valid())
        {
            emit showError("Timeline", "Animation handler is not valid.");
            return;
        }

        //get start and end time
        const boost::posix_time::ptime& startDateTime = animationUIHandler->getStartDateTime();
        const boost::posix_time::ptime& endDateTime = animationUIHandler->getEndDateTime();

        // get Mission duration
        boost::posix_time::time_duration duration = (endDateTime - startDateTime);
        int totalSeconds = duration.total_seconds();

        int numControlPoints = _wcUIHandler->getNumControlPoints();
        double elapsedSeconds = 0;
        bool visibilityState = true;
        QColor visibleColor("blue");
        QColor notVisibleColor("grey");
        QColor profileColor(visibleColor);
        //bool moving = false;

        QList<QObject*> timelineMarkerList;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("New", "0"));

        if(numControlPoints == 0)
        {
            _setContextProperty("weatherListModel", QVariant::fromValue(NULL));
            _setContextProperty("timelineModel", QVariant::fromValue(NULL));
            QObject* timelineSlider = _findChild("timelineSlider");
            if(timelineSlider != NULL)
            {
                timelineSlider->setProperty("profileType", QVariant::fromValue(0));
            }
            return;
        }
        else
        {
            const ENV::IWeatherControllerComponent::ControlPointList& controlPointList = _wcUIHandler->getControlPointList();
            ENV::IWeatherControllerComponent::ControlPointList::const_iterator iter = controlPointList.begin();

            for(int index = 0; iter != controlPointList.end(); iter++, index++)
            {
                const boost::posix_time::ptime& cpTime = iter->time;

                double cpTimeInSeconds = (cpTime - startDateTime).total_seconds();

                // previous mark
                double posX = elapsedSeconds/totalSeconds;
                double deltaX = (cpTimeInSeconds - elapsedSeconds)/totalSeconds;
                timelineMarkerList.append(new TimelineVisibilityMark(posX, profileColor, deltaX, visibilityState));

                visibilityState = iter->rainState || iter->snowState;
                if(visibilityState)
                {
                    profileColor = visibleColor;
                }
                else
                {
                    profileColor = notVisibleColor;
                }

                elapsedSeconds = cpTimeInSeconds;

                QDateTime qCpDateTime;
                VizQt::TimeUtils::BoostToQtDateTime(cpTime, qCpDateTime);

                comboBoxList.append(new VizComboBoxElement(qCpDateTime.toString("hh:mm:ss dd MMM yyyy")
                    , QString::number(index+1)) );

            }

            double posX = elapsedSeconds/totalSeconds;
            double deltaX = (1 - posX);

            timelineMarkerList.append(new TimelineVisibilityMark(posX, profileColor, deltaX, visibilityState));

            _setContextProperty("weatherListModel", QVariant::fromValue(NULL));
            _setContextProperty("timelineModel", QVariant::fromValue(timelineMarkerList));
            _setContextProperty("weatherListModel", QVariant::fromValue(comboBoxList));

            QObject* timelineSlider = _findChild("timelineSlider");
            if(timelineSlider != NULL)
            {
                timelineSlider->setProperty("profileType", QVariant::fromValue(1));
            }
        }        
    }

    void WeatherControllerGUI::onAddedToGUIManager()
    {
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void WeatherControllerGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void WeatherControllerGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _wcUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IWeatherControllerUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }

}// namespace SMCQt
