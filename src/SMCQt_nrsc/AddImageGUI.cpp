/****************************************************************************
 *
 * File             : AddImageGUI.h
 * Description      : AddImageGUI class definition
 *
 *****************************************************************************
  * Copyright (c) 2010-2011, VizExperts india Pvt. Ltd.                                       
 *****************************************************************************/

#include <SMCQt/AddImageGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <VizQt/NameValidator.h>

#include <SMCUI/IAddContentStatusMessage.h>
#include <SMCUI/AddContentUIHandler.h>

#include <osgDB/FileNameUtils>
#include <osg/Vec4>

#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>

#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/ApplicationRegistry.h>

#include <Elements/IMilitarySymbolTypeLoader.h>

#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>

#include <Util/Log.h>

#include <VizQt/GUI.h>

#include <sstream>

#ifdef USE_QT4
	#include <QtGui/QFileDialog>
#else
	#include <QtWidgets/QFileDialog>
#endif

#include <QtCore/QFile>
#include <QtUiTools/QtUiTools>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, AddImageGUI)
    DEFINE_IREFERENCED(AddImageGUI, CORE::Base)
    
    AddImageGUI::AddImageGUI() : _minMarking(false)
                                ,_maxMarking(false)
                                ,_subdataset(-1)
    {
    }
    
    AddImageGUI::~AddImageGUI()
    {
    }

    void 
    AddImageGUI::popupLoaded(QString type)
    {
        if(type == "addImageOverlayPopUp")
        {
            QObject* addImagePopup = _findChild("addImageOverlayPopUp");
            if(addImagePopup != NULL)
            {
                QObject::connect(addImagePopup, SIGNAL(browseButtonClicked()), this, SLOT(_handleBrowseButtonClicked()), Qt::UniqueConnection);
                QObject::connect(addImagePopup, SIGNAL(minMarkClicked()), this, SLOT(_handleMinMarkClicked()), Qt::UniqueConnection);
                QObject::connect(addImagePopup, SIGNAL(maxMarkClicked()), this, SLOT(_handleMaxMarkClicked()), Qt::UniqueConnection);
                QObject::connect(addImagePopup, SIGNAL(addLayer(QString, QString, QString, QString, QString, QString)), 
                        this, SLOT(_addImageLayer(QString, QString, QString, QString,QString, QString)), Qt::UniqueConnection);
                QObject::connect(addImagePopup, SIGNAL(selectSubdataset()), this, SLOT(_subdatasetSelected()), Qt::UniqueConnection);
            }
        }
    }

    void 
    AddImageGUI::_loadAndSubscribeSlots()
    {
        QObject* popupLoader = _findChild("popupLoader");

        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }

    void
    AddImageGUI::setGUIManager(APP::IGUIManager* manager)
    {
        VizQt::IQtGUIManager* qtmanager = manager->getInterface<VizQt::IQtGUIManager>();

        if(qtmanager!=NULL)
        {
            _manager = qtmanager;
        }
    }

    APP::IGUIManager*
    AddImageGUI::getGUIManager() const
    {
		return _manager->getInterface<APP::IGUIManager>();
    }

    void 
    AddImageGUI::onAddedToGUIManager()
    {
		// Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }

        _loadAndSubscribeSlots();
    }

    void 
    AddImageGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

		DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void
    AddImageGUI::_loadAddContentUIHandler()
    {
        _addContentUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentGUIHandler>(getGUIManager());

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
    }

    void AddImageGUI::_handleBrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        // For HDF and CEOS files the extents and coordinate system is not present.
        // So giving user the control where to load the files.
        std::string formatFilter = "Image (*.jpg *.jpeg *.bmp *.png *.pic *.dds *.rgb *.bit *.tif *.hdf *.he5 *.h5 *.001)";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
                                                    label.c_str(),
                                                    _lastPath.c_str(),
                                                    formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {
            QObject* addImagePopup = _findChild("addImageOverlayPopUp");
            if(addImagePopup != NULL)
            {
                addImagePopup->setProperty("selectedFileName",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);

                //For hdf files display the subsets from which user can select to load
                std::string ext = osgDB::getLowerCaseFileExtension(filename.toStdString());
                if(ext == "hdf" || ext == "h5" || ext == "he5" || ext == "001"/*ceos ext*/)
                {
                    std::vector<std::string> subsetList;
                    _addContentUIHandler->getGdalSubsets(filename.toStdString(), subsetList);

                    QList<QObject*> comboBoxList;
                    int count = 0;
                    for( std::vector<std::string>::const_iterator iter = subsetList.begin(); iter != subsetList.end(); ++iter, count++ )
                    {
                        std::string element = *iter;
                        if(element != "")
                        {
                            comboBoxList.append(new VizComboBoxElement(element.c_str(), UTIL::ToString(count).c_str()));
                        }
                    }
                    
                    _setContextProperty("subdatasetListModel", QVariant::fromValue(comboBoxList));
                }

            }
            catch(UTIL::Exception &e)
            {
                std::string errorMessage = e.What();
                emit showError("Error in Reading File", QString::fromStdString(errorMessage), true);
            }
            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }

    void
    AddImageGUI::_subdatasetSelected()
    {
        QObject* addLayerPopup = _findChild("addImageOverlayPopUp");
        if(addLayerPopup)
        {
            _subdataset = addLayerPopup->property("subdatasetSelectedUid").toInt() +1;
        }
    }

    void 
    AddImageGUI::_addImageLayer(QString filepath, QString xmin, QString ymin, QString xmax, QString ymax, QString rotation)
    {
        std::string filename = filepath.toStdString();
        if(filename.empty())
        {
            std::string title = "Add Image File Error";
            std::string text = "No filename specified";

            showError(title.c_str(), text.c_str());
            return;
        }

        if(xmin.isEmpty() || ymin.isEmpty() || xmax.isEmpty() || ymax.isEmpty())
        {
            showError("Enter All Fields", "Please enter a Valid Latitude/Longitude");
            return;
        }

        _extents.x() = xmin.toDouble();
        _extents.y() = ymin.toDouble();
        _extents.z() = xmax.toDouble();
        _extents.w() = ymax.toDouble();

        if(_extents.x() > 180.0 || _extents.x() < -180.0 || _extents.z() > 180.0 || _extents.z() < -180.0)
        {
            showError("Enter Valid Longitude", "Please enter Longitude value between -180.0 and 180.0");
            return;
        }
        if(_extents.w() > 90.0 || _extents.w() < -90.0 || _extents.y() > 90.0 || _extents.y() < -90.0)
        {
            showError("Enter Valid Latitude", "Please enter Latitude value between -90.0 and 90.0");
            return;
        }

        try
        {
            _addContentUIHandler->addImageOverlay(filename, _extents, rotation.toStdString(), _subdataset);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
            
            // Show error for vector data
            std::string title = "Add Image File Error";
            std::string text = e.What();
            showError(title.c_str(), text.c_str());
            return;
        }

        _setContextProperty("subdatasetListModel", QVariant::fromValue(NULL));

        QObject::disconnect(this, SIGNAL(addLayer(QString, QString, QString, QString, QString, QString)), 
                        this, SLOT(_addImageLayer(QString, QString, QString, QString,QString, QString)));
    }

    void
    AddImageGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            { 
                _loadAddContentUIHandler();
            }
            catch(...)
            {}
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            // Query TerrainUIHandler interface
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());
            if(tph.valid())
            {
                // get terrain position
                osg::Vec3d pos;
                if(tph->getMousePickedPosition(pos, true))
                {
                    std::stringstream  xvalue, yvalue;
                    xvalue << pos.x();
                    yvalue << pos.y();

                    if(_minMarking)
                    {
                        QObject* addLayerPopup = _findChild("addImageOverlayPopUp");
                        addLayerPopup->setProperty("xMin",QVariant::fromValue(QString::fromStdString(xvalue.str())));
                        addLayerPopup->setProperty("yMin",QVariant::fromValue(QString::fromStdString(yvalue.str())));
                        _minMarking = false;
                    }

                    if(_maxMarking)
                    {
                        QObject* addLayerPopup = _findChild("addImageOverlayPopUp");
                        addLayerPopup->setProperty("xMax",QVariant::fromValue(QString::fromStdString(xvalue.str())));
                        addLayerPopup->setProperty("yMax",QVariant::fromValue(QString::fromStdString(yvalue.str())));
                        _maxMarking = false;
                    }
                }
            }
        }
    }

    void 
    AddImageGUI::_handleMinMarkClicked()
    {
        CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());

        _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
        _minMarking = true;
    }

    void 
    AddImageGUI::_handleMaxMarkClicked()
    {
        CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());

        _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
        _maxMarking = true;
    }

}
