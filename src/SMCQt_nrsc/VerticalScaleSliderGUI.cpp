/****************************************************************************
*
* File             : VerticalScaleSliderGUI.h
* Description      : VerticalScaleSliderGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/VerticalScaleSliderGUI.h>
#include <SMCUI/ElevationEditingUIHandler.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, VerticalScaleSliderGUI);
    DEFINE_IREFERENCED(VerticalScaleSliderGUI, SMCQt::DeclarativeFileGUI);

    VerticalScaleSliderGUI::VerticalScaleSliderGUI()
    {

    }

    VerticalScaleSliderGUI::~VerticalScaleSliderGUI()
    {

    }

    void 
    VerticalScaleSliderGUI::_loadAndSubscribeSlots()
    {
        //DeclarativeFileGUI::_loadAndSubscribeSlots();
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());

        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if(smpLeftMenu != NULL)
        {
            QObject::connect(smpLeftMenu, SIGNAL(connectVerticalScaleMenu()), this,
                SLOT(connectVerticalScaleMenu()), Qt::UniqueConnection);
        }
    }

    void 
    VerticalScaleSliderGUI::connectVerticalScaleMenu()
    {
        QObject* verticalScaleSliderObject = _findChild("verticalScaleSliderObject");
        if(verticalScaleSliderObject != NULL)
        {
            QObject::connect(verticalScaleSliderObject, SIGNAL(changeVerticalScaleValue(double)), 
              this, SLOT(changeVerticalScalevalue(double)), Qt::UniqueConnection);

            // set vertical scale value 
            // this should be done to maintain the previous state value
            double currentVerticalScaleValue = _getCurrentVerticalScale();
            verticalScaleSliderObject->setProperty( "sliderValue", currentVerticalScaleValue );
        }
  }

    // slot to accept the change in the opacity slider
    void 
    VerticalScaleSliderGUI::changeVerticalScalevalue(const double &value)
    {
        _changeVerticalScale(value);
    }

    void 
    VerticalScaleSliderGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();
    }


    void 
    VerticalScaleSliderGUI::onAddedToGUIManager()
    {   
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void 
    VerticalScaleSliderGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }
 
   
    void 
    VerticalScaleSliderGUI::_changeVerticalScale(const double& value)
    {
        if (_elevationEditingUIHandler.valid())
        {
            _elevationEditingUIHandler->setVerticalScaleValue(float(value));
        }
    }

    void 
    VerticalScaleSliderGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _elevationEditingUIHandler =  APP::AccessElementUtils::
                    getUIHandlerUsingManager<SMCUI::IElevationEditingUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    
    double 
    VerticalScaleSliderGUI::_getCurrentVerticalScale()
    {   
        if (_elevationEditingUIHandler.valid())
        {
            return _elevationEditingUIHandler->getVerticalScaleValue();
        }

        //By default the scaling factort is 1
        return 1.0;
    }

} // namespace SMCQt