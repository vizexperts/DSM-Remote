/*****************************************************************************
*
* File             : JoystickGUI.h
* Description      : JoystickGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/JoystickGUI.h>
#include <SMCQt/IconObject.h>

#include <VizOSGQTQuick/OSGViewport.h>
#include <VizQt/WindowManager.h>

#include <App/IApplication.h>
#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>

#include <Terrain/IVectorObject.h>

#include <Core/IFeatureLayer.h>
#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>

#include <VizQT/QtGUImanager.h>

#include <Util/FileUtils.h>
#include <Util/CoordinateConversionUtils.h>

#include <osgViewer/View>
#include <Touch/MultiTouchManager.h>



#include <vizOSGQtQuick/OSGViewport.h>
#include <vizOSGQtQuick/OSGNode.h>
#include <Elements/CameraComponent.h>

#define MAXJOYSTICKMINIPULATIONRANGE 500000

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, JoystickGUI);
    DEFINE_IREFERENCED(JoystickGUI, DeclarativeFileGUI);

    JoystickGUI::JoystickGUI(): _joystickEventHandler(NULL),
                                _jsManager(NULL)
                                
    {
    }

    JoystickGUI::~JoystickGUI()
    {
    }


    void JoystickGUI::onAddedToGUIManager()
    {
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {
        }
    }

    void JoystickGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
        
        if(item != NULL)
        {
            QObject* popupLoader = item->findChild<QObject*>("popupLoader");
            if(popupLoader != NULL)
            {
                QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, SLOT(connectToQML(QString)), Qt::UniqueConnection);
            }
        }
    }

    void JoystickGUI::connectToQML(QString menuName)
    {
        std::string menuName1 = menuName.toStdString();
        if(menuName == "joystickObject")
        {
            QObject* QMLJoystickObject = _findChild("joystickObject");
            JOYSTICKCONTROLLER::IJoystickManager* jsManager = _getJoystickManager();
            if(!QMLJoystickObject)
            {
                return;
            }
            if(_jsManager->isJoystickReadyToStart())
            {
                QMLJoystickObject->setProperty("joystickStatus", false);
            }
            else
            {
                QMLJoystickObject->setProperty("joystickStatus", true);
            }


            QObject::connect(QMLJoystickObject, SIGNAL(startJoystickButtonClicked()), this, SLOT(startJoystickController()));
            QObject::connect(QMLJoystickObject, SIGNAL(stopJoystickButtonClicked()), this, SLOT(stopJoystickController()));
            QObject::connect(QMLJoystickObject, SIGNAL(checkForJoystick()), this, SLOT(isJoystickPresent()));
            
            
        }
    }

    void JoystickGUI::startJoystickController()
    {
        _disableTouch();

        if(_checkIfJoystickEnabledInOtherWorld())
        {
            emit showError("JOYSTICK", "Cannot Enable Joystick Controller More than One World", true);

            QObject* QMLJoystickObject = _findChild("joystickObject");
            if(QMLJoystickObject)
            {
                QMLJoystickObject->setProperty("JoystickStatus", false);
            }

            return;
        }

		//Commented this inorder to have the same changes as made by somnath for joystick

       // double range = checkHeight();


        
		//if(range > MAXJOYSTICKMINIPULATIONRANGE)
		//{
  //          emit showError("Joystick Manipulator Error", "You cannot Start Flythrough from this height\n Please zoom in further and try again",true);
  //          QObject* QMLJoystickObject = _findChild("joystickObject");
  //          if(QMLJoystickObject)
  //          {
  //              QMLJoystickObject->setProperty("JoystickStatus", false);
  //          }

		//	
		//	return ;
		//}

        JOYSTICKCONTROLLER::IJoystickManager* jsManager = _getJoystickManager();

        QItem *myNodeObject = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getRootComponent();
        if(myNodeObject)
        {
            osgQtQuick::OSGViewport *viewport = dynamic_cast<osgQtQuick::OSGViewport*>(myNodeObject);
            if(viewport)
            {
                if(jsManager)
                {
                    APP::IApplication *application =  
                        getGUIManager()->getInterface<APP::IManager>()->getApplication();
                    application->getView()->addEventHandler(getGUIManager()->getInterface<VizQt::IQtGUIManager>()->_getJoystickEventHandler());

                    jsManager->setGraphicsWidget(application->getView());

                    if( jsManager->isJoystickReadyToStart())
                    {
                        jsManager->setJoystickEventHandler(getGUIManager()->getInterface<VizQt::IQtGUIManager>()->_getJoystickEventHandler());
                        jsManager->startJoystickAll();
                       
                        QObject* viewport = _findChild("modeType");
                        if(viewport)
                        {
                            viewport->setProperty("mode",QVariant::fromValue(QString::QString("Joystick")));
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
        }
    }

    /*JOYSTICKCONTROLLER::JoystickEventHandler* JoystickGUI::_getJoystickEventHandler()
    {
        if(!_joystickEventHandler.valid())
        {
            _joystickEventHandler = new JOYSTICKCONTROLLER::JoystickEventHandler;
        }

        return _joystickEventHandler.get();
    }*/

    void JoystickGUI::stopJoystickController()
    {
        JOYSTICKCONTROLLER::IJoystickManager* jsManager = _getJoystickManager();

        if(!jsManager)
            return;

        if(jsManager->isJoystickReadyToStart() == false)
        {
            QItem *myNodeObject = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getRootComponent();
            if(myNodeObject)
            {
                APP::IApplication *application =  
                        getGUIManager()->getInterface<APP::IManager>()->getApplication();
                if(application)
                {
                    application->getView()->removeEventHandler(getGUIManager()->getInterface<VizQt::IQtGUIManager>()->_getJoystickEventHandler());
                    QObject* viewport = _findChild("modeType");
                    if(viewport)
                    {
                        viewport->setProperty("mode",QVariant::fromValue(QString::QString("Earth Manipulator")));
                    }
                }
            }

            jsManager->stopJoystickAll();
        }
    }

    void JoystickGUI::isJoystickPresent()
    {
        JOYSTICKCONTROLLER::IJoystickManager* jsManager = _getJoystickManager();

        if(!jsManager)
            return;

        if(!_jsManager->isJoystickPresent())
        {
            emit showError("JOYSTICK", "Unable to detech the JOYSTICK", true);
        }
        else
        {
            emit showError("JOYSTICK ", "Detected the JOYSTICK", true);
        }
    }
   
    void JoystickGUI::onRemovedFromGUIManager()
    {
        // Unsubscribing from world messages
        // Subscribe for world added message to the world maintainer
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
            APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
        _unsubscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        _unsubscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);

        DeclarativeFileGUI::onRemovedFromGUIManager();

    }

    JOYSTICKCONTROLLER::IJoystickManager*
    JoystickGUI::_getJoystickManager()
    {
        if(!_jsManager.valid())
        {
             //Getting the Joystick Manager
            APP::IApplication *application =  
                    getGUIManager()->getInterface<APP::IManager>()->getApplication();
            if(application)
            {
                APP::IManager* manager = application->getManagerByName(JOYSTICKCONTROLLER::JoystickManager::ClassNameStr);
                if(manager)
                {
                    _jsManager = manager->getInterface<JOYSTICKCONTROLLER::IJoystickManager>();
                }
            }
        }

        return _jsManager.get();
    }

    void JoystickGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();
    }

    void JoystickGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
                _subscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);
            }
            catch(...)
            {

            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            
        }
        else if(messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    bool JoystickGUI::_checkIfJoystickEnabledInOtherWorld()
    {
        VizQt::IWindowManager* windowManager = VizQt::WindowManager::instance();
        if(!windowManager)
            return true;

        std::vector<APP::IApplication*> applications;
        windowManager->getApplications(applications);

        int index = 0; 
        int size = applications.size();

        APP::IApplication *presentApp =  
                    getGUIManager()->getInterface<APP::IManager>()->getApplication();
        std::string presentAppID = presentApp->getInterface<CORE::IBase>()->getUniqueID().toString();

        while (index < size)
        {
            APP::IApplication* application = applications[index++];
            std::string appID = application->getInterface<CORE::IBase>()->getUniqueID().toString();

            if(presentAppID == appID)
                continue;

            APP::IManager *manager = application->getManagerByName(JOYSTICKCONTROLLER::JoystickManager::ClassNameStr);
            if(manager)
            {
                bool joystickEnabled = !(manager->getInterface<JOYSTICKCONTROLLER::IJoystickManager>()->isJoystickReadyToStart());
                if(joystickEnabled)
                    return true;
            }
        }

        return false;
    }
    void
    JoystickGUI::_disableTouch()
    {
        if(!_multiTouchManager.valid())
	    {
             APP::IApplication *app =  
                        getGUIManager()->getInterface<APP::IManager>()->getApplication();
		    APP::IManager *multiManager = app->getManagerByClassname(TOUCH::MTManager::ClassNameStr);
		    if(multiManager)
		    {
			    _multiTouchManager = multiManager->getInterface<TOUCH::IMTManager>();
			    if(_multiTouchManager.valid())
			    {
                    CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
				    _multiTouchManager->setQuickView(qtapp->getViewerWindow());
                    
			    }
		    }
	    }
        if(_multiTouchManager->isTouchInteractionRunning())
	    {
		    _multiTouchManager->stopTouchInteraction();
            CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
            
            QObject *viewport = dynamic_cast< QObject*> (item);
            if(viewport)
            {
                viewport->setProperty("multi_touch_interaction_active",false);
            }             

	    } 
        _configureEventHandling(true);
      
    
    }

    void 
	JoystickGUI::_configureEventHandling(bool toHandle)
	{
		//Getting the application set
		std::vector<APP::IApplication*> applicationSet;

		VizQt::WindowManager::instance()->getApplications(applicationSet);

		for(unsigned int currApp=0; currApp<applicationSet.size(); ++currApp)
		{
			CORE::RefPtr<APP::IApplication> application = applicationSet[currApp];

			//Getting the GUI Manager
            CORE::RefPtr<APP::IManager> mgr				= application->getManagerByClassname(VizQt::QtGUIManager::ClassNameStr);
            CORE::RefPtr<VizQt::QtGUIManager>  qtMgr			= dynamic_cast<VizQt::QtGUIManager*>(mgr.get());

			if(qtMgr.valid())
			{
				QQuickItem *quickItem = qtMgr->getRootComponent();
				if(quickItem)
				{
					if(dynamic_cast<osgQtQuick::OSGViewport*>(quickItem))
					{
						dynamic_cast<osgQtQuick::OSGViewport*>(quickItem)->setEventHandling(toHandle);
					}
				}
			}
		}
	}
    double JoystickGUI::checkHeight()
    {
		//Getting the worldMaintainer
		CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
		CORE::RefPtr<ELEMENTS::ICameraComponent> cameraComponent;

		if(worldMaintainer)
		{
			//Getting the cameraComponent
			CORE::IComponent *component = worldMaintainer->getComponentByName("CameraComponent");
			if(component)
			{
				cameraComponent = component->getInterface<ELEMENTS::ICameraComponent>();
			}
		}

		double range, heading, pitch;
		if(cameraComponent.valid())
		{
			osg::Vec3d  lonlatPos;		

			cameraComponent->getViewPoint(lonlatPos, range, heading, pitch);
		}
        return range;

    }
    
    

} // namespace indiGUI


