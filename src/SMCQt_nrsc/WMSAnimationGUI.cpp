/****************************************************************************
 *
 * File             : WMSAnimationGUI.cpp
 * Description      : WMSAnimationGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, CAIR, DRDO
 *****************************************************************************/

#include <SMCQt/WMSAnimationGUI.h>

#include <APP/IApplication.h>
#include <APP/AccessElementUtils.h>

#include <CORE/WorldMaintainer.h>
#include <CORE/IAnimationComponent.h>
#include <CORE/IMessage.h>
#include <CORE/ITickMessage.h>
#include <CORE/ICompositeObject.h>
#include <CORE/Referenced.h>
#include <CORE/IMetadataRecordHolder.h>
#include <ELEMENTS/IMilitarySymbol.h>
#include <ELEMENTS/IOverlay.h>

#include <UTIL/StringUtils.h>
#include <UTIL/CoordinateConversionUtils.h>
#include <UTIL/VolumeGridUtils.h>
#include <UTIL/WMSUtils.h>

#include <VizUI/IMouseMessage.h>

#ifndef USE_Qt4
	#include <QtWidgets/QLineEdit>
	#include <QtWidgets/QPushButton>
	#include <QtWidgets/QTreeWidgetItem>
	#include <QtWidgets/QMessageBox>
#endif

#include <QtGUI/QIntValidator>
#include <QtCore/QResource>
#include <QtGui/QMouseEvent>


#include <VizQt/QtUtils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, WMSAnimationGUI);

	static CORE::IAnimationComponent * 
	GetAnimationComponent(APP::IGUIManager *guiManager)
    {
		CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(guiManager);
        if(!wmain)
        {
            return NULL;
        }

        CORE::IComponent* comp = wmain->getComponentByName("WMSComponent");
        if(!comp)
        {
            return NULL;
        }

        CORE::IAnimationComponent *wmsComponent = comp->getInterface<CORE::IAnimationComponent>();
        if (!wmsComponent)
        {
            return NULL;
        }

        return wmsComponent;
    }

    const std::string WMSAnimationGUI::SeekBarProgress  = "SeekBarProgress"; //Push Button play animation
    const std::string WMSAnimationGUI::LineEditProgress = "LineEditProgress"; //Push Button play animation
    const std::string WMSAnimationGUI::PBPlayPause      = "PBPlayPause"; //Push Button play animation
    const std::string WMSAnimationGUI::PBStop           = "PBStop"; //Push Button play animation

    WMSAnimationGUI::WMSAnimationGUI()
	{
    }

    WMSAnimationGUI::~WMSAnimationGUI()
    {
        setActive(false);
    }

    void 
    WMSAnimationGUI::_loadAndSubscribeSlots()
    {
		VizQt::LayoutFileGUI::_loadAndSubscribeSlots();

        /*if(!getComplete())
        {
                return;
        }*/

        // Keep the dialog box on top but not block application input
        /*_parent->setWindowFlags(_parent->windowFlags() | Qt::WindowStaysOnTopHint);*/

		_playPausePB    = _parent->findChild<QPushButton *>(_getWidgetName(PBPlayPause).c_str());
        _stopButton     = _parent->findChild<QPushButton *>(_getWidgetName(PBStop).c_str());
        _seekBar        = _parent->findChild<QSlider     *>(_getWidgetName(SeekBarProgress).c_str());
        _seekBarLE      = _parent->findChild<QLineEdit   *>(_getWidgetName(LineEditProgress).c_str());

        //Setting the icons of the buttons
        if ( !_playPausePB || !_stopButton || !_seekBar || !_seekBarLE)
        {
            setComplete(false);
            return;
        }

        _playPausePB->setCheckable(true);

        //Setting the icons ( Hardcoded )
        _stopButton->setIcon (QIcon("../../data/gui/resource/WMSAnimationPlayer/ButtonStop.png"));

        //Connects
        QObject::connect(_playPausePB,  SIGNAL(clicked(bool)),      this, SLOT(_handlePlayAnimationButtonClicked(bool)));
        QObject::connect(_stopButton,   SIGNAL(clicked()),      this, SLOT(_handlePBStopClicked()));
        QObject::connect(_seekBar,      SIGNAL(sliderMoved(int)),   this, SLOT(_handleSliderTimeChanged(int)));
        QObject::connect(this, SIGNAL(_sliderConfigurationUpdatedSignal()), this, SLOT(_changeSliderConfiguration()), Qt::QueuedConnection);

		_customSlider = new CustomSlider;
		_customSlider->setSlider(_seekBar);
		_customSlider->setGUIManager(getGUIManager()->getInterface<APP::IManager>());
    }

    void
    WMSAnimationGUI::_handleSliderTimeChanged(int elapsedTime)
    {
		CORE::RefPtr<CORE::IAnimationComponent> animationComponent =  GetAnimationComponent(getGUIManager());
        if ( !animationComponent.valid() )
        {
            return;
        }

        const boost::posix_time::ptime& startTime = animationComponent->getStartDateTime();
        boost::posix_time::ptime currentTime = startTime + boost::posix_time::seconds(elapsedTime);

        animationComponent->setCurrentDateTime(currentTime);

        _currTimeStream.clear();
        _currTimeStream.str("");

        _currTimeStream << currentTime;

        std::string time(_currTimeStream.str());

        _currTime = QString::fromStdString(time);
        _setProgressTimeIndicator();
    }

    void
    WMSAnimationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
		if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the WMSUIHandler and set it
            try
            {
				_WMSUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IWMSUIHandler>(getGUIManager());
                if ( !_WMSUIHandler.valid() )
                {
                    setComplete(false);
                }
            }

            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if (messageType == *CORE::IAnimationComponent::TimeChangeMessageType)
        {
             // update current data time
            CORE::ITimeChangeMessage* tcm = message.getInterface<CORE::ITimeChangeMessage>();

            // update slider value
            double elapsedTime = tcm->getElapsedTime();
            _getChild<QSlider>(SeekBarProgress)->setValue(elapsedTime);

            _currTimeStream.clear();
            _currTimeStream.str("");

            _currentTimeBoost = _startTimeBoost + boost::posix_time::seconds(elapsedTime);
            _currTimeStream << _currentTimeBoost;

            std::string currTimeStr(_currTimeStream.str());
            _currTime = QString::fromStdString(currTimeStr);

            _setProgressTimeIndicator();
        }
        else if (messageType == *CORE::IAnimationComponent::MinMaxTimeChangeMessageType)
        {
            emit _sliderConfigurationUpdatedSignal();
        }
    }

    void
    WMSAnimationGUI::_changeSliderConfiguration()
    {
		CORE::IAnimationComponent *component = GetAnimationComponent(getGUIManager());
        if ( !component )
        {
            return;
        }

        _startTimeBoost = component->getStartDateTime();
        _endTimeBoost   = component->getEndDateTime();

        int totalSecs = (_endTimeBoost - _startTimeBoost).total_seconds();

        QSlider* slider = _getChild<QSlider>(SeekBarProgress);
        if ( slider )
		{
            slider->setMaximum(totalSecs);
			slider->setMinimum(0);
		}

        _initializeEndTime();
    }

    void
    WMSAnimationGUI::_setInitailGUIState()
    {
        CORE::IAnimationComponent *component = GetAnimationComponent(getGUIManager());
        if ( !component )
        {
            return;
        }

        //If component already running just keeping the same state of the GUI
        if ( component->getAnimationState() != CORE::IAnimationComponent::STATE_STOPPED )
        {
            return;
        }

        _seekBarLE->clear();

        //If the state is the stop state. Setting the configuration of the slider
        _startTimeBoost = component->getStartDateTime();
        _endTimeBoost   = component->getEndDateTime();

#if 0    //Testing Code
        std::stringstream ss;
        ss << _endTimeBoost;

        std::string maxTimeStr(ss.str());

        ss.str("");
        ss << _startTimeBoost;
        std::string minTimeStr(ss.str());
#endif

        int totalSecs = (_endTimeBoost - _startTimeBoost).total_seconds();

        // set slider range
        QSlider* slider = _getChild<QSlider>(SeekBarProgress);
        if ( slider )
        {
            slider->setMinimum(0);
            slider->setMaximum(totalSecs);
            slider->setValue(0);
        }

        //setting the default icons of the buttons
        _playPausePB->setIcon(QIcon("../../data/gui/resource/WMSAnimationPlayer/ButtonPlay.png"));
        _stopButton->setIcon (QIcon("../../data/gui/resource/WMSAnimationPlayer/ButtonStop.png"));

		_playPausePB->setChecked(false);

        _initializeEndTime();
    }

    // initilalization of Qwidget, Units and ComputationType attributes 
    void 
    WMSAnimationGUI::initializeAttributes()
    {
        LayoutFileGUI::initializeAttributes();

        _addWidgetName(SeekBarProgress);
        _addWidgetName(LineEditProgress);
        _addWidgetName(PBPlayPause);
        _addWidgetName(PBStop);
    }

	void
	WMSAnimationGUI::_handleSeekBarClicked()
	{
	}

    void
    WMSAnimationGUI::setActive(bool value)
    {
        if (!getComplete())
        {
            return;
        }

        try
        {
            CORE::IAnimationComponent *component = GetAnimationComponent(getGUIManager());
            if(value)
            {
                if(component)
                {
                    _subscribe(component, *CORE::IAnimationComponent::TimeChangeMessageType);
                }

                _setInitailGUIState();
				_seekBar->installEventFilter(_customSlider);
            }
            else
            {
                if(component)
                {
                    _unsubscribe(component, *CORE::IAnimationComponent::TimeChangeMessageType);
                }
				_seekBar->removeEventFilter(_customSlider);
            }

            LayoutFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void 
    WMSAnimationGUI::_handlePlayAnimationButtonClicked(bool flag)
    {
		if (_getChild<QPushButton>(PBPlayPause)->isChecked())
        {
            CORE::RefPtr<CORE::IAnimationComponent> animationComponent =  GetAnimationComponent(getGUIManager());
            if (!animationComponent.valid() )
            {
                return;
            }

            if ( animationComponent->getAnimationState() == CORE::IAnimationComponent::STATE_STOPPED)
            {
                if(_WMSUIHandler->getLayers().size()==0)
                {
                    QMessageBox::critical(_parent, "Error", "No  Layer is Added");
                    _playPausePB->setChecked(false);
                    return;
                }

                _WMSUIHandler->startWMSComponent();
                _changeSliderConfiguration();
            }
            else
            {
                animationComponent->play();
            }

            //setting the icon of the pause button
            _playPausePB->setIcon(QIcon("../../data/gui/resource/WMSAnimationPlayer/ButtonPause.png"));
            _subscribe(animationComponent, *CORE::IAnimationComponent::MinMaxTimeChangeMessageType);
        }
        else
        {
            _handlePBPauseClicked();

            //setting the icon of the play button
            _playPausePB->setIcon(QIcon("../../data/gui/resource/WMSAnimationPlayer/ButtonPlay.png"));
        }
    }

    void
    WMSAnimationGUI::_handlePBPauseClicked()
    {
        CORE::RefPtr<CORE::IAnimationComponent> animationComponent =  GetAnimationComponent(getGUIManager());
        if ( !animationComponent.valid() )
        {
            return;
        }

        animationComponent->pause();
    }

    void
    WMSAnimationGUI::_handlePBStopClicked()
    {
        CORE::RefPtr<CORE::IAnimationComponent> animationComponent =  GetAnimationComponent(getGUIManager());
        if ( !animationComponent.valid() )
        {
            return;
        }

        animationComponent->stop();

        //Customize the other icons
        _playPausePB->setChecked(false);
        _playPausePB->setIcon(QIcon("../../data/gui/resource/WMSAnimationPlayer/ButtonPlay.png"));
        _unsubscribe(animationComponent, *CORE::IAnimationComponent::MinMaxTimeChangeMessageType);

		QSlider* slider = _getChild<QSlider>(SeekBarProgress);
        if ( slider )
        {
            slider->setMinimum(0);
            slider->setMaximum(0);
            slider->setValue(0);
        }
    }

    void
    WMSAnimationGUI::_initializeEndTime()
    {
        std::stringstream endDateStream;
        endDateStream << _endTimeBoost;

        _endTime = QString::fromStdString(endDateStream.str());
    }

    void
    WMSAnimationGUI::_setProgressTimeIndicator()
    {
        _timeFormatString = _currTime + "/" + _endTime;
        _seekBarLE->setText(_timeFormatString);
    }

	void
	CustomSlider::setSlider(QSlider *slider)
	{
		_slider = slider;
	}

	bool 
	CustomSlider::eventFilter(QObject *obj, QEvent *event)
	{
		if (!_slider)
		{
			// pass the event on to the parent class
			return QObject::eventFilter(obj, event);
		}

		if (obj == _slider) 
		{
			if (event->type() == QEvent::MouseButtonRelease) 
			{
				if(!_guiManager)
					return false;

				CORE::IAnimationComponent *animationComponent = GetAnimationComponent(_guiManager->getInterface<APP::IGUIManager>());
				if(!animationComponent)
					return false;

				QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
				int updatedInterval		= QStyle::sliderValueFromPosition(_slider->minimum(), _slider->maximum(), mouseEvent->x(), _slider->width());

				boost::posix_time::ptime startTime = animationComponent->getStartDateTime();
				startTime += boost::posix_time::seconds(updatedInterval);
					
				animationComponent->setCurrentDateTime(startTime);
				_slider->setValue(updatedInterval);

				return true;
			} else {
				return false;
			}
		} 
		else 
		{
			// pass the event on to the parent class
			return QObject::eventFilter(obj, event);
		 }
	}



} // namespace indiGUI
