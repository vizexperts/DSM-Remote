/*****************************************************************************
*
* File             : ContextualTabGUI.cpp
* Description      : ContextualTabGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <SMCQt/BuildingExtrusionGUI.h>

#include <VizUI/ISelectionUIHandler.h>

#include <Core/ISelectionComponent.h>
#include <Core/IWorldMaintainer.h>
#include <Core/IMessage.h>
#include <Core/INamedAttributeMessage.h>
#include <Terrain/IVectorObject.h>
#include <Core/IFeatureObject.h>
#include <Core/IFeatureLayer.h>

#include <App/AccessElementUtils.h>
#include <App/IManager.h>


namespace SMCQt
{

 DEFINE_META_BASE(SMCQt, BuildingExtrusionGUI);
 DEFINE_IREFERENCED(BuildingExtrusionGUI, SMCQt::DeclarativeFileGUI);

 BuildingExtrusionGUI::BuildingExtrusionGUI():_buildingExtrusionUIHandler(NULL)
    {
    }

    BuildingExtrusionGUI::~BuildingExtrusionGUI()
    {
    }   

    void
    BuildingExtrusionGUI::_loadAndSubscribeSlots()
    {
       DeclarativeFileGUI::_loadAndSubscribeSlots();
       CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
       _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

       QObject* smpMenu = _findChild("smpMenu");
       if(smpMenu)
       {
           QObject::connect(smpMenu, SIGNAL(loaded(QString)), 
               this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
       }
        
    }
    void
    BuildingExtrusionGUI::connectSmpMenu(QString)
    {
        QObject* polygonLayer = _findChild("polygonLayerNonEditableTab");
        if(polygonLayer)
        {
            QObject::connect(polygonLayer, SIGNAL(extrudeBuilding(bool)), this, SLOT(extrudeBuilding(bool)), Qt::UniqueConnection);
        }
    }

    void 
    BuildingExtrusionGUI::extrudeBuilding()
    {
        if(!_buildingExtrusionUIHandler)
            return;

       _buildingExtrusionUIHandler->extrudeBuilding(_url, *_vectorObject);
    }

    void 
    BuildingExtrusionGUI::intrudeBuilding()
    {
       /* if(!_buildingExtrusionUIHandler)
            return;

        _buildingExtrusionUIHandler->intrudeBuilding(_url);  
        return;*/
    }

    void BuildingExtrusionGUI::onAddedToGUIManager()
    {   
        try
        {            
            APP::IManager* manager = getGUIManager()->getInterface<APP::IManager>();
            CORE::IWorldMaintainer* maintainer =
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IManager>(manager);           
            const CORE::ComponentMap cmap = maintainer->getComponentMap();
            CORE::RefPtr<CORE::ISelectionComponent> comp =
                CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
            _subscribe(comp.get(), *CORE::ISelectionComponent::SelectionMessageType);
           
        }
        catch(...)
        {
        }
        _loadAndSubscribeSlots();
        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void BuildingExtrusionGUI::onRemovedFromGUIManager()
    {
        CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
        _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void BuildingExtrusionGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
             _buildingExtrusionUIHandler =  APP::AccessElementUtils::
                    getUIHandlerUsingManager<SMCUI::IBuildingExtrusionUIHandler>(getGUIManager());
        }
        else if(messageType == *CORE::ISelectionComponent::SelectionMessageType)
        {
            CORE::INamedAttributeMessage* namedMsg= message.getInterface<CORE::INamedAttributeMessage>();
            std::string name = namedMsg->getNamedAttribute()->toString();
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if(selectionUIHandler.valid())
            {
                _selectObject(name);                    
            }           
        }
    }

    
    void BuildingExtrusionGUI::_selectObject(const std::string& name)
    {       
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if(!selectionUIHandler.valid())
        {
            return;
        }
      
        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();  

        
        if(iter == map.end())
        {
            return;
        }
       
        if(name == CORE::IFeatureLayer::getInterfaceName())
        {
            CORE::RefPtr<CORE::IFeatureLayer> layer = 
                iter->second->getInterface<CORE::IFeatureLayer>();           
            CORE::RefPtr<ELEMENTS::IVectorObject> vectorObject = layer->getInterface<ELEMENTS::IVectorObject>();          
            if(vectorObject)
            {                      
                _url = vectorObject->getURL(); 
                _vectorObject = vectorObject;
            }
        }
    }
}