/******************************************************s**********************
 *
 * File             : CompassGUI.cpp
 * Description      : CompassGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2011-2012, CAIR, DRDO
 *****************************************************************************/

#include <SMCQt/CompassGUI.h>

#include <Core/WorldMaintainer.h>

#include <APP/IApplication.h>
#include <APP/AccessElementUtils.h>
#include <APP/WorldManager.h>

#include <Util/CoordinateConversionUtils.h>

#include <QtCore/QVariant>
#include <QtCore/QString>

const static std::string SubWindowConfig = "../../config/tkpconfigSubWindow.xml";

namespace SMCQt
{
    
	DEFINE_META_BASE(SMCQt, CompassGUI);
    Q_DECLARE_METATYPE(QList<SMCQt::VizComboBoxElement*>)

    CompassGUI::CompassGUI()
    {
    }

    CompassGUI::~CompassGUI()
    {
	}

	void 
	CompassGUI::onAddedToGUIManager()
	{
		try
        {
			CORE::RefPtr<APP::IApplication> app = _manager->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {
        }

		DeclarativeFileGUI::onAddedToGUIManager();
	}

    void
    CompassGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
		
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
        
        if(item != NULL)
        {
            _QMLObject = item->findChild<QObject*>("compassImageObjectName");
        }
    }

    void
    CompassGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the UIHandlers 
            try
            {
				//subscribing to the worldMaintainer Tick Message
				CORE::IWorldMaintainer *worldMaintainer = getGUIManager()->getInterface<APP::IManager>()->getWorldMaintainer();
				if(worldMaintainer)
				{
					_subscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);
				}

				_view = _manager->getInterface<APP::IManager>()->getApplication()->getView();
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
		else if(messageType == *CORE::IWorldMaintainer::TickMessageType)
		{
			if(_view.valid() && _QMLObject)
			{
				// Get the main camera from view
				osg::ref_ptr<osg::Camera> camera = _view->getCamera();

				// Get the eye, center and up vector of the camerea
				osg::Vec3d eye,center,up;
				camera->getViewMatrixAsLookAt(eye,center,up,10000);

				osg::Vec3d lv,sv,uv;
				lv = center -eye;
				sv = lv^up;
				uv = sv^lv;

				double angleToDecideUpOrLook = acos( (lv*eye)/(lv.length()*eye.length()));

				// if angle between look vector and the camera position vector
				// is more than 160 it means the direction vector near about matches with
				// radial direction of the earth so for computing angle with north direction
				// consider up vector other wise the look vector
				if(osg::RadiansToDegrees(angleToDecideUpOrLook) > 175.0 )
				{
					// change the center as up vector position
					center = eye + (uv*10000);
				}

				// Get the ECEF to ENU matrix to convert ECEF co-ordinate to ENU
				osg::ref_ptr<UTIL::ECEFToENUConverter> enuToEcef = new UTIL::ECEFToENUConverter;
				enuToEcef->setOrigin(eye, false);
				osg::Matrix mat1 = enuToEcef->getMatrix();

           
				// As the local co-ordinate system is reference to eye
				// get the local point for center and i.e the local front vector
				osg::Vec3 fLocalVector =  center*mat1;

	           
				// Now project this on xy plane to find angle with y axis which is north
				fLocalVector.set(fLocalVector.x(), fLocalVector.y(), 0.0);

				// Now find the angle between front local vector with north(0,1,0 )
				double angle = acos((osg::Vec3(0,1,0)*fLocalVector)/(fLocalVector.length()));

				if(fLocalVector.x() < 0)
					angle = -angle;

				double degreeAngle = osg::RadiansToDegrees(angle);
				degreeAngle *= -1;

				_QMLObject->setProperty("rotation", QVariant::fromValue(degreeAngle));

			}
		}
        else
        {
			DeclarativeFileGUI::update(messageType, message);
        }
    }


} // namespace indiGUI
