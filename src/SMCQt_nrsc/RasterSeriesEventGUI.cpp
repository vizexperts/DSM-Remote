/****************************************************************************
*
* File             : WeatherControllerGUI.h
* Description      : WeatherControllerGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/RasterSeriesEventGUI.h>
#include <App/AccessElementUtils.h>

#include <SMCUI/IAnimationUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <SMCQt/TimelineMark.h>
#include <SMCQt/VizComboBoxElement.h>
#include <SMCQt/IContextualTabGUI.h>
#include <VizQt/QtUtils.h>

#include <Elements/IMilitarySymbol.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, RasterSeriesEventGUI);
    DEFINE_IREFERENCED(RasterSeriesEventGUI, SMCQt::DeclarativeFileGUI);

    RasterSeriesEventGUI::RasterSeriesEventGUI()
    {

    }

    RasterSeriesEventGUI::~RasterSeriesEventGUI()
    {

    }

    void RasterSeriesEventGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

         QObject* popupLoader = _findChild("popupLoader");
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }
    }

    void RasterSeriesEventGUI::popupLoaded(QString type)
    {
        QObject* rasterSeriesEventGUI = _findChild("addEvents");
        if(rasterSeriesEventGUI)
        {            
            if(!_rsEventUIHandler.valid())
            {
                return;
            }

           // bool visibilityAdded = _vcUIHandler->setSelectedObjectAsCurrent();

            QObject::connect(rasterSeriesEventGUI, SIGNAL(selectRaster(QString)), 
                this, SLOT(selectRaster(QString)), Qt::UniqueConnection);

            QObject::connect(rasterSeriesEventGUI, SIGNAL(deleteRaster(QString)), 
                this, SLOT(deleteRaster(QString)), Qt::UniqueConnection);

            QObject::connect(rasterSeriesEventGUI, SIGNAL(editRasterControlPoint(QString, QDateTime)), 
                this, SLOT(editRasterControlPoint(QString, QDateTime)), Qt::UniqueConnection);
           
            _populateRasterList();
        
            CORE::RefPtr<SMCUI::IAnimationUIHandler> animationUIHandler = 
                        APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());
            if(animationUIHandler.valid())
            {
                const boost::posix_time::ptime& currentTime = 
                    animationUIHandler->getCurrentDateTime();

                QDateTime qCurTime;
                VizQt::TimeUtils::BoostToQtDateTime(currentTime, qCurTime);
                rasterSeriesEventGUI->setProperty("currentTime", QVariant::fromValue(qCurTime));
            }
        }
    }
    void RasterSeriesEventGUI::_populateRasterList()
    {
        TERRAIN::ITimeVaryingRasterComponent::TimedRasterList timeRasterList = _rsEventUIHandler->getRasterSeries();
        QList<QObject*> comboBoxList;
        for(int currItem = 0 ; currItem < timeRasterList.size() ; ++currItem)
        {
            TERRAIN::ITimeVaryingRasterComponent::TimedRaster timeRaster = timeRasterList[currItem];
            CORE::RefPtr<ELEMENTS::IRasterObject> rasterObject = timeRaster.rasterObject;
            CORE::RefPtr<CORE::IBase> base = rasterObject->getInterface<CORE::IBase>();
            std::string name = base->getName();           
            comboBoxList.append(new VizComboBoxElement(QString::fromStdString(name)
                    , QString::number(currItem)) );
        }
        _setContextProperty("rasterSeriesList", QVariant::fromValue(comboBoxList));

    }
    void RasterSeriesEventGUI::selectRaster(QString uid)
    {
        QObject* rasterSeriesEventGUI = _findChild("addEvents");
        if(rasterSeriesEventGUI)
        {
            unsigned int id = uid.toInt();
            TERRAIN::ITimeVaryingRasterComponent::TimedRasterList timeRasterList = _rsEventUIHandler->getRasterSeries();
            if(id < timeRasterList.size())
            {
                TERRAIN::ITimeVaryingRasterComponent::TimedRaster timeRaster = timeRasterList[id];        
                boost::posix_time::ptime time = timeRaster.time;
                QDateTime qCurTime;
                VizQt::TimeUtils::BoostToQtDateTime(time, qCurTime);
                rasterSeriesEventGUI->setProperty("currentTime", QVariant::fromValue(qCurTime));
            }
        }    
    }

    void RasterSeriesEventGUI::editRasterControlPoint(QString uid, QDateTime qTime)
    {
        unsigned int id = uid.toInt();
        TERRAIN::ITimeVaryingRasterComponent::TimedRasterList timeRasterList = _rsEventUIHandler->getRasterSeries();
        if(id < timeRasterList.size())
        {
            TERRAIN::ITimeVaryingRasterComponent::TimedRaster timeRaster = timeRasterList[id];
            CORE::RefPtr<ELEMENTS::IRasterObject> rasterObject = timeRaster.rasterObject;
            if(rasterObject)
            {
                timeRasterList.erase(timeRasterList.begin() + id);
                _rsEventUIHandler->setRasterSeries(timeRasterList);
                boost::posix_time::ptime time =  VizQt::TimeUtils::QtToBoostDateTime(qTime);
                TERRAIN::ITimeVaryingRasterComponent::TimedRaster timeRaster;
                timeRaster.rasterObject = rasterObject;
                timeRaster.time = time;
                _rsEventUIHandler->addRasterObject(timeRaster);
                _populateRasterList();
            }
        }


    }
    void RasterSeriesEventGUI::deleteRaster(QString uid)
    {        
        unsigned int id = uid.toInt();
        TERRAIN::ITimeVaryingRasterComponent::TimedRasterList timeRasterList = _rsEventUIHandler->getRasterSeries();
        timeRasterList.erase(timeRasterList.begin() + id);
        _rsEventUIHandler->setRasterSeries(timeRasterList);
        _populateRasterList();        
    }

    void RasterSeriesEventGUI::onAddedToGUIManager()
    {
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void RasterSeriesEventGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void RasterSeriesEventGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _rsEventUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IRasterSeriesEventUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }
}