/****************************************************************************
*
* File             : CollaborationGUI.h
* Description      : CollaborationGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/CollaborationGUI.h>
#include <App/AccessElementUtils.h>
#include <App/IApplication.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <VizUI/ICameraUIHandler.h>
#include <Core/WorldMaintainer.h>
#include <HLA/NetworkCollaborationManager.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CollaborationGUI);
    DEFINE_IREFERENCED(CollaborationGUI, SMCQt::DeclarativeFileGUI);

    CollaborationGUI::CollaborationGUI() : _isWallRunning(false)
                                            , _isControllerRunning(false)
                                            , _isCollobarationStopped(true)
    {

    }

    CollaborationGUI::~CollaborationGUI()
    {

    }

    void CollaborationGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if(smpLeftMenu != NULL)
        {
            QObject::connect(smpLeftMenu, SIGNAL(connectCollaborationMenu()), this,
                SLOT(connectCollaborationMenu()), Qt::UniqueConnection);
        }
    }

    void CollaborationGUI::connectCollaborationMenu()
    {
        QObject* smpCollaborationMenu = _findChild("smpCollaborationMenu");
        if(smpCollaborationMenu != NULL)
        {
            // connect to signals
            QObject::connect(smpCollaborationMenu, SIGNAL(startWall(bool)), 
                this, SLOT(startWall(bool)), Qt::UniqueConnection);

            QObject::connect(smpCollaborationMenu, SIGNAL(startController(bool)), 
                this, SLOT(startController(bool)), Qt::UniqueConnection);

            QObject::connect(smpCollaborationMenu, SIGNAL(stopCollaboration(bool)), 
                this, SLOT(stopCollaboration(bool)), Qt::UniqueConnection);

            smpCollaborationMenu->setProperty("wallEnabled", QVariant::fromValue(_isWallRunning));
            smpCollaborationMenu->setProperty("controllerEnabled", QVariant::fromValue(_isControllerRunning));
            smpCollaborationMenu->setProperty("stopEnabled", QVariant::fromValue(_isCollobarationStopped));
        }
    }

    void CollaborationGUI::startWall(bool value)
    {   
        if (value)
        {
            _isWallRunning = value;
            // Remote Signal specific code 
            if (_remoteSignalComponent.get())
            {
                _remoteSignalComponent->setIsClient(false);
                _remoteSignalComponent->initialize();
                _isCollobarationStopped = false;
            }
        }
    }
    
    void CollaborationGUI::startController(bool value)
    {
        if (value)
        {
            _isControllerRunning = value;
            // Remote Signal specific code 
            if (_remoteSignalComponent.get())
            {
                _remoteSignalComponent->setIsClient(true);
                _remoteSignalComponent->initialize();
                _isCollobarationStopped = false;
            }
        }
    }
    
    void CollaborationGUI::stopCollaboration(bool value)
    {
        if (value)
        {
            _isCollobarationStopped = true;
            if (_isControllerRunning)
            {
                /// Remote Signal Specific code
                _remoteSignalComponent->stopTCPClient();
            }
            else if (_isWallRunning)
            {
                /// Remote Signal Specific code
                _remoteSignalComponent->stopTCPServer();
            }
            else 
            {
                emit showError("Failure",  "The application is not in collaboration mdoe!!" );
            }
        }
    }
    void CollaborationGUI::onAddedToGUIManager()
    {   
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }
    }

    void CollaborationGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}
    }

    void CollaborationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            
            // Query the LineUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();

                _subscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);


                APP::IManager* mgr = getGUIManager()->getInterface<APP::IManager>();
                mgr = mgr->getApplication()->getManagerByName(HLA::NetworkCollaborationManager::ClassNameStr);
                if(mgr)
                {
                    _netwrkCollaborationMgr = mgr->getInterface<HLA::INetworkCollaborationManager>();
                    if(!_netwrkCollaborationMgr.valid())
                    {
                        
                    }
                }

                vizCore::RefPtr<vizCore::IComponent> icomponent= wmain->getComponentByName("RemoteSignalComponent");
                _remoteSignalComponent = icomponent->getInterface<vizRemoteSignal::IRemoteSignalComponent>();
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ProjectResignRequestRejectedMessageType)
        {
            emit showError("Error Destroying Project", "Joined members denied the request.");
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ProjectDestroyedMessageType)
        {
            LOG_DEBUG("Project destroyed successfully!");

            if (_isWallRunning)
            {
                _isWallRunning = false;
            }

            if (_isWallRunning)
            {
                _isControllerRunning = false;
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void CollaborationGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();
    }

} // namespace indiGUI
