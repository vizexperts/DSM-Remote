/******************************************************s**********************
 *
 * File             : CreateWindowGUI.cpp
 * Description      : CreateWindowGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2011-2012, CAIR, DRDO
 *****************************************************************************/

#include <SMCQt/CreateWindowGUI.h>

#include <Core/WorldMaintainer.h>

#include <APP/IApplication.h>
#include <APP/AccessElementUtils.h>
#include <APP/WorldManager.h>

#include <VizQt/QtUtils.h>
#include <VizQt/WindowManager.h>
#include <VizQt/CameraSyncManager.h>

//! C++ related Header's
#include <iostream>

#include <QtCore/QVariant>
#include <QtCore/QString>

const static std::string SubWindowConfig = "../../config/tkpconfigSubWindow.xml";

namespace SMCQt
{
    
	DEFINE_META_BASE(SMCQt, CreateWindowGUI);
    Q_DECLARE_METATYPE(QList<SMCQt::VizComboBoxElement*>)

        CreateWindowGUI::CreateWindowGUI() : _createWindowClient(NULL) 
                                            , _createWindowService(NULL)
    {
        
    }

    CreateWindowGUI::~CreateWindowGUI()
    {
        if (_createWindowService)
        {
            delete _createWindowService;
            _createWindowService = NULL;
        }

        if (_createWindowClient)
        {
            delete _createWindowClient;
            _createWindowClient = NULL;
        }
	}

    void
    CreateWindowGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
		
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
        
        if(item != NULL)
        {
            QObject* popupLoader = item->findChild<QObject*>("popupLoader");
            if(popupLoader != NULL)
            {
                QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, SLOT(connectToQML(QString)), Qt::UniqueConnection);
            }
        }
    }

    void CreateWindowGUI::connectToQML(QString menuName)
    {
		std::string menuName1 = menuName.toStdString();
        if(menuName == "createWorldWindows")
        {
			_QMLObject = _findChild("createWorldWindows");
			if(!_QMLObject)
			{
				return;
			}
                   
			QObject::connect(_QMLObject, SIGNAL(buttonClicked(QString)), this, SLOT(_handleButtonClicked()));
			
			_setGUIState();
			//setting GUI according to current applicatio state		
        }
    }

	void	
	CreateWindowGUI::_setGUIState()
	{
		if(!_QMLObject)
			return;

		QObject *defaultRadioButton = _QMLObject->findChild<QObject*>("defaultRadioButton");
		if(defaultRadioButton)
		{
			defaultRadioButton->setProperty("checked", QVariant::fromValue(true));
		}
	}

	void 
	CreateWindowGUI::_handleButtonClicked()
	{
		QObject *nameEdit = _QMLObject->findChild<QObject*>("nameEdit");
		if(!nameEdit)
		{
			emit showError("Error", "Internal Error Occured");
			return;
		}

		QVariant variant = nameEdit->property("text");
		std::string name = variant.toString().toStdString();

		if(!name.size())
		{
			emit showError("Parameter Missing", "Name field is Empty");
			return;
		}

        // checking if the name has already been taken
        VizQt::IWindowManager *windowManager = VizQt::WindowManager::instance();

		std::vector<APP::IApplication*> vApplications;
		windowManager->getApplications(vApplications);
        for(unsigned int currApp=0; currApp<vApplications.size(); ++currApp)
		{
			//Getting the world name from the application
			CORE::RefPtr<APP::IApplication> application = vApplications[currApp];

			

			// we are getting the worldName from the maintainer
			CORE::RefPtr<APP::IManager> mgr = application->getManagerByClassname(STR(APP::WorldManager));
			if(!mgr.valid())
				continue;

			CORE::RefPtr<APP::WorldManager> wmgr = dynamic_cast<APP::WorldManager*>(mgr.get());
			if(!wmgr.valid())
				continue;

			CORE::RefPtr<CORE::IWorldMaintainer> wmain = wmgr->getWorldMaintainer();
			if(!wmain.valid())
				continue;
				
			if(wmain->getWorldMap().begin() != wmain->getWorldMap().end())
			{
				const CORE::RefPtr<CORE::IWorld> world = wmain->getWorldMap().begin()->second;
                std::string worldName = world->getInterface<CORE::IBase>()->getName();
                if(worldName==name)
                {
                    emit showError("World Already Exist", "World with same name already exist");
			        return; 
                }
				
			}
		}
       


		//Getting the create Mode
		QObject *radioButton = _QMLObject->findChild<QObject*>("defaultRadioButton");
		if(!radioButton)
		{
			emit showError("Error", "Internal Error Occured");
			return;
		}

		variant = radioButton->property("checked");
		bool isDefault   = variant.toBool();

		if(isDefault)
		{
            _createDefaultWindowSlot(QString(SubWindowConfig.c_str()), QString(name.c_str()));
			//VizQt::WindowManager::instance()->createNewWindow(SubWindowConfig, name);
		}
		else
		{
            _createCopyWindowSlot(QString(SubWindowConfig.c_str()), QString(name.c_str()));
			/*CORE::IWorld *world = getGUIManager()->getInterface<APP::IManager>()->getWorldMaintainer()->getWorldMap().begin()->second;
			VizQt::WindowManager::instance()->createNewWindow(SubWindowConfig, world, name);*/
		}

		CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());

		QObject* popupLoader = item->findChild<QObject*>("popupLoader");
		if(popupLoader)
		{
			QMetaObject::invokeMethod(popupLoader, "unload");
		}
	}
    
    void 
    CreateWindowGUI::_createCopyWindowSlot(QString subWindowConfig, QString windowName)
    {
        if (_remoteSignalComponent->isClient())
        {
            emit _createCopyWindowSignal(subWindowConfig, windowName);
        }

        CORE::IWorld *world = getGUIManager()->getInterface<APP::IManager>()->getWorldMaintainer()->getWorldMap().begin()->second;
        VizQt::WindowManager::instance()->createNewWindow(subWindowConfig.toStdString(), world, windowName.toStdString());
    }

    void 
    CreateWindowGUI::_createDefaultWindowSlot(QString subWindowConfig, QString windowName)
    {
        if (_remoteSignalComponent->isClient())
        {
            emit _createDefaultWindowSignal(subWindowConfig, windowName);
        }

        VizQt::WindowManager::instance()->createNewWindow(subWindowConfig.toStdString(), windowName.toStdString());
    }

    void 
	CreateWindowGUI::disconnectFromQML(QString menuName)
    {
    }


    void
    CreateWindowGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the UIHandlers 
            try
            {
                CORE::RefPtr<CORE::IComponent> icomponent = CORE::WorldMaintainer::instance()->getComponentByName("RemoteSignalComponent");

                if(icomponent.valid())
                {
                    _remoteSignalComponent = icomponent->getInterface<vizRemoteSignal::IRemoteSignalComponent>();
                    //_remoteSignalComponent->initialize();
                }

                vizRemoteSignal::ServicesManager *manager = _remoteSignalComponent->getServicesManager();

                if (manager)
                {
                    if (_remoteSignalComponent->isClient())
                    {
                        _createWindowClient = new REMOTESIGNAL::CreateWindowClient(manager);

                        connect(this, SIGNAL(_createDefaultWindowSignal(QString, QString)),
                            _createWindowClient, SLOT(_createDefaultWindowSlot(QString, QString)));

                        connect(this, SIGNAL(_createCopyWindowSignal(QString, QString)),
                            _createWindowClient, SLOT(_createCopyWindowSlot(QString, QString)));
                    }
                    else 
                    {
                        _createWindowService = new REMOTESIGNAL::CreateWindowService(manager);

                        connect(_createWindowService, SIGNAL(_createDefaultWindowSlot(QString, QString)),
                            this, SLOT(_createDefaultWindowSlot(QString, QString)));

                        connect(_createWindowService, SIGNAL(_createCopyWindowSlot(QString, QString)),
                            this, SLOT(_createCopyWindowSlot(QString, QString)));
                        
                    }
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
			DeclarativeFileGUI::update(messageType, message);
        }
    }

    void 
    CreateWindowGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();
    }

    void 
    CreateWindowGUI::setActive(bool value)
    { 
    }

} // namespace indiGUI
