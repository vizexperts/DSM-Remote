/******************************************************s**********************
 *
 * File             : CameraSyncGUI.cpp
 * Description      : CameraSyncGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2011-2012, CAIR, DRDO
 *****************************************************************************/

#include <SMCQt/CameraSyncGUI.h>

#include <Core/WorldMaintainer.h>

#include <APP/IApplication.h>
#include <APP/AccessElementUtils.h>
#include <APP/WorldManager.h>

#include <VizQt/QtUtils.h>
#include <VizQt/WindowManager.h>
#include <VizQt/CameraSyncManager.h>

//! C++ related Header's
#include <iostream>

#include <QtCore/QVariant>
#include <QtCore/QString>


namespace SMCQt
{
    const std::string CameraSyncGUI::ViewComboBox = "viewsList";
    const std::string CameraSyncGUI::AttachDeattachButton = "latitude";

	DEFINE_META_BASE(SMCQt, CameraSyncGUI);
    Q_DECLARE_METATYPE(QList<SMCQt::VizComboBoxElement*>)

    CameraSyncGUI::CameraSyncGUI()
    {
    }

    CameraSyncGUI::~CameraSyncGUI()
    {
	}

    void
    CameraSyncGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
		
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
        
        if(item != NULL)
        {
            QObject* popupLoader = item->findChild<QObject*>("popupLoader");
            if(popupLoader != NULL)
            {
                QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, SLOT(connectToQML(QString)), Qt::UniqueConnection);
            }
        }
    }

    void CameraSyncGUI::connectToQML(QString menuName)
    {
		std::string menuName1 = menuName.toStdString();
        if(menuName == "cameraSync")
        {
			_QMLCameraSyncObject = _findChild("cameraSync");
			if(!_QMLCameraSyncObject)
			{
				return;
			}
                   
			QObject::connect(_QMLCameraSyncObject, SIGNAL(attchbuttonClicked()), this, SLOT(_handleAttachButtonClicked()));
			QObject::connect(_QMLCameraSyncObject, SIGNAL(detachbuttonClicked()), this, SLOT(_handleDettachButtonClicked()));
			
			_setGUIState();
			//setting GUI according to current applicatio state		
        }
    }

	void	
	CameraSyncGUI::_populateComboBox()
	{
        VizQt::IWindowManager *windowManager = VizQt::WindowManager::instance();

		std::vector<APP::IApplication*> vApplications;
		windowManager->getApplications(vApplications);

		//getting attached applications
		std::vector<APP::IApplication*> vAttachedApplications;
		VizQt::CameraSyncManager::instance()->getAttachedApplications(getGUIManager()->getInterface<APP::IManager>()->getApplication(),
			vAttachedApplications);

		_viewList.clear();

		QObject *attchObject    = _QMLCameraSyncObject->findChild<QObject*>("attachButton");
		QObject *comboBoxObject = _QMLCameraSyncObject->findChild<QObject*>("viewComboBox");
		if(vApplications.size()==vAttachedApplications.size() || vApplications.size()<=1)
		{
			_viewList.append(new VizComboBoxElement("All Views Attached", "0"));

			comboBoxObject->setProperty("enabled", QVariant::fromValue(false));
			attchObject->setProperty("enabled", QVariant::fromValue(false));
		}
		else
		{
			_viewList.append(new VizComboBoxElement("Select View", "0"));

			comboBoxObject->setProperty("enabled", QVariant::fromValue(true));
			attchObject->setProperty("enabled", QVariant::fromValue(true));
		}
			  

		for(unsigned int currApp=0; currApp<vApplications.size(); ++currApp)
		{
			//Getting the world name from the application
			CORE::RefPtr<APP::IApplication> application = vApplications[currApp];

			//Not taking the same application 
			if(application.get()==getGUIManager()->getInterface<APP::IManager>()->getApplication())
				continue;

			//finding if the application is in attached application set or not
			if(std::find(vAttachedApplications.begin(), vAttachedApplications.end(), application.get())!=vAttachedApplications.end())
				continue;

			//Else we are getting the worldName from the maintainer
			CORE::RefPtr<APP::IManager> mgr = application->getManagerByClassname(STR(APP::WorldManager));
			if(!mgr.valid())
				continue;

			CORE::RefPtr<APP::WorldManager> wmgr = dynamic_cast<APP::WorldManager*>(mgr.get());
			if(!wmgr.valid())
				continue;

			CORE::RefPtr<CORE::IWorldMaintainer> wmain = wmgr->getWorldMaintainer();
			if(!wmain.valid())
				continue;
				
			if(wmain->getWorldMap().begin() != wmain->getWorldMap().end())
			{
				const CORE::RefPtr<CORE::IWorld> world = wmain->getWorldMap().begin()->second;

				_viewList.append(new VizComboBoxElement(QString::fromStdString(world->getInterface<CORE::IBase>()->getName()),
				QString::number(currApp+1)));
			}
		}

        _setContextProperty(ViewComboBox.c_str(), QVariant::fromValue(_viewList));
	}

	void	
	CameraSyncGUI::_setGUIState()
	{
		QObject *attachButton	= _QMLCameraSyncObject->findChild<QObject*>("attachButton");
		QObject *detachButton	= _QMLCameraSyncObject->findChild<QObject*>("deattachButton");

		QObject *comboBoxObject = _QMLCameraSyncObject->findChild<QObject*>("viewComboBox");

		if(!attachButton || !detachButton || !comboBoxObject)
			return;


		bool isApplicationAttached = 
			VizQt::CameraSyncManager::instance()->isAttached(getGUIManager()->getInterface<APP::IManager>()->getApplication());

		if(isApplicationAttached)
		{
			detachButton->setProperty("visible", QVariant::fromValue(true));
			detachButton->setProperty("enabled", QVariant::fromValue(true));
		}
		else
		{
			detachButton->setProperty("visible", QVariant::fromValue(false));
			detachButton->setProperty("enabled", QVariant::fromValue(false));
		}

		_populateComboBox();
	}

	void
	CameraSyncGUI::_handleDettachButtonClicked()
	{
		VizQt::ICameraSyncManager *manager = VizQt::CameraSyncManager::instance();
		if(manager)
		{
			manager->detachCamera(getGUIManager()->getInterface<APP::IManager>()->getApplication());
		}

		_setGUIState();
	}

	void 
	CameraSyncGUI::_handleAttachButtonClicked()
	{
		QObject *comboBox = _QMLCameraSyncObject->findChild<QObject*>("viewComboBox");
		if(!comboBox)
			return;

		QVariant syncGUID = comboBox->property("selectedUID");
		int index = syncGUID.toInt();
		if(index==0)
		{
			emit showError("Selection Error", "Please select valid view");
			return;
		}

		std::vector<APP::IApplication*> vApplications;
        VizQt::WindowManager::instance()->getApplications(vApplications);
		if(vApplications.size()<index)
		{
			emit showError("Selection Error", "Please open and close the window Again. This might solve the problem");
			return;
		}

		VizQt::ICameraSyncManager *manager = VizQt::CameraSyncManager::instance();
		if(manager)
		{
			try
			{
				manager->syncCamera(vApplications[index-1], getGUIManager()->getInterface<APP::IManager>()->getApplication());
			}
			catch(UTIL::Exception &e)
			{
				emit showError("Error in Attaching Views", QString::fromStdString(e.What()));
			}
		}

		_setGUIState();
	}

    void 
	CameraSyncGUI::disconnectFromQML(QString menuName)
    {
    }


    void
    CameraSyncGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the UIHandlers 
            try
            {
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
			DeclarativeFileGUI::update(messageType, message);
        }
    }

    void 
    CameraSyncGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();
    }

    void 
    CameraSyncGUI::setActive(bool value)
    { 
    }

} // namespace indiGUI
