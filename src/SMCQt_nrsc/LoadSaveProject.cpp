
#include <App/IUIHandlerManager.h>
#include <VizUI/UIHandlerManager.h>
#include <SMCUI/IAddContentGUIHandler.h>
#include <Util/StringUtils.h>
#include <SMCUI/AddContentUIHandler.h>
#include <VizUI/IMouseMessage.h>
#include <Core/InterfaceUtils.h>
#include <Core/AttributeTypes.h>
#include <App/AccessElementUtils.h>
#include <Util/BaseExceptions.h>
#include <SMCQt/LoadSaveProject.h>

#include <QtUiTools/QUiLoader>
#ifdef USE_QT4
#include <QtGui/QFileDialog>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QMessageBox>
#else
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QMessageBox>
#endif
#include <VizQt/IQtGUI.h>
#include <osgDB/FileNameUtils>

#include <App/AccessElementUtils.h>
#include <Database/IDatabaseMaintainer.h>
#include <DATABASE/IDBRecord.h>
#include <DATABASE/IDBField.h>
#include <DATABASE/IDBTable.h>
#include <DATABASE/ISQLQueryResult.h>
#include <DATABASE/ISQLQuery.h>

#include <Core/IMessage.h>
#include <Core/IMessageType.h>
#include <Core/IObject.h>
#include <Core/GroupAttribute.h>
#include <Core/NamedAttribute.h>
#include <Core/CoreRegistry.h>
#include <Core/IObjectMessage.h>
#include <App/ApplicationRegistry.h>

#include <osgDB/ReadFile>
#include <osgDB/DatabasePager>
#include <osgDB/FileCache>
#include <osgDB/Archive>
#include <osgDB/Registry>

#include <osg/PositionAttitudeTransform>
#include <Elements/IModelFeatureObject.h>
#include <Elements/ElementsPlugin.h>
#include <ELEMENTS/IModelHolder.h>
#include <ELEMENTS/IModel.h>
#include <Util/CoordinateConversionUtils.h>
#include <ELEMENTS/IRepresentation.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, LoadSaveProject);
    DEFINE_IREFERENCED(LoadSaveProject, VizQt::QtGUI);

    LoadSaveProject::LoadSaveProject()
        : _manager(NULL)
        , _active(false)
        , _dbUIHandler(NULL)
    {
    }

    LoadSaveProject::~LoadSaveProject()
    {}

    void
    LoadSaveProject::setActive(bool active)
    {
        _active = active;
    }

    bool
    LoadSaveProject::getActive() const
    {
        //TBD
        return _active;
    }

    void
    LoadSaveProject::onAddedToGUIManager()
    {
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}
    }

    void 
    LoadSaveProject::_createProjectSchema(const std::string& schemaName, const std::string& tableName)
    {
        if(!_dbUIHandler.valid())
            return;

        CORE::RefPtr<DATABASE::ISQLQuery> query = _dbUIHandler->createSQLQuery();

        if(_checkForExistingSchema(schemaName))
            return;

        if(query != NULL)
        {
            std::string toSchemaLowerForm =  QString(schemaName.c_str()).toLower().toStdString();
            std::string toTableLowerForm =  QString(tableName.c_str()).toLower().toStdString();
            _tableNameSaveLoad = toTableLowerForm;

            std::string queryStatement = "CREATE SCHEMA %s CREATE TABLE %s ( projectname varchar(1000) NOT NULL UNIQUE, path varchar(1000)  NOT NULL CHECK (path <> '') )";
            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str());

            if(query->exec(buffer))
            {
                 //Successfully query
            }
            else
            {
                //Failed to execute the command
            }
        }
    }
    
    bool
    LoadSaveProject::_checkForExistingSchema(const std::string& schemaName)
    {
        if(!_dbUIHandler.valid())
            return false;

        CORE::RefPtr<DATABASE::ISQLQuery> query = _dbUIHandler->createSQLQuery();

        int count = 0;
        if(query != NULL)
        {
            std::string toLowerForm =  QString(schemaName.c_str()).toLower().toStdString();

            std::string queryStatement = "SELECT count(schema_name) FROM information_schema.schemata WHERE schema_name = '%s'";
            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toLowerForm.c_str());

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                if(result.valid())
                {
                    count = result->getInt();
                    if(count >= 1)
                        return true;
                }
            }
        }
        return false;
    }

    //Populate the database with the project path
    void
    LoadSaveProject::_addProjectNameAndPath(const std::string& projectName, const std::string& path,
                                          const std::string& schemaName, const std::string& tableName)
    {
        if(!_dbUIHandler.valid())
            return;

        CORE::RefPtr<DATABASE::ISQLQuery> query = _dbUIHandler->createSQLQuery();

        if(query != NULL)
        {
            std::string queryStatement = "INSERT INTO %s.%s (projectname, path) VALUES ('%s', '%s')";

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), schemaName.c_str(), tableName.c_str(), projectName.c_str(), path.c_str());

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
            }
        }
    }

    void
    LoadSaveProject::_getPresentProjects(const std::string& schemaName, const std::string& tableName)
    {
         if(!_dbUIHandler.valid())
            return;

        CORE::RefPtr<DATABASE::ISQLQuery> query = _dbUIHandler->createSQLQuery();

        int count = 0;
        _projectNameAndPath.clear();

        if(query != NULL)
        {
            std::string queryStatement = "SELECT projectname, path FROM %s.%s";

            std::string toSchemaLowerForm =  QString(schemaName.c_str()).toLower().toStdString();
            std::string toTableLowerForm =  QString(tableName.c_str()).toLower().toStdString();

            char buffer[1000] = {'\0'};
            sprintf(buffer, queryStatement.c_str(), toSchemaLowerForm.c_str(), toTableLowerForm.c_str());

            if(query->exec(buffer))
            {
                CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();
                if(result.valid())
                {
                    CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
                    count = resultTable->getTotalRecords();
                    for(int i = 0; i < count; i++)
                    {
                        CORE::RefPtr<DATABASE::IDBRecord> record = resultTable->getNextRecord();
                        if(record != NULL)
                        {
                            DATABASE::IDBField* field1 =  record->getFieldByIndex(0);
                            std::string projectName = field1->toString();
                            DATABASE::IDBField* field2 =  record->getFieldByIndex(1);
                            std::string projectPath = field2->toString();

                            _projectNameAndPath[projectName] = projectPath;
                        }
                    }
                }
            }
        }
    }

    //The function to find the Search for the layers
    void LoadSaveProject::smpSearch(QString layerName)
    {
        //NO need to use this function
        return;

        std::stringstream ss;

        std::string layer = ((layerName.isEmpty()) ? "" : layerName.toStdString());

        //Testing for postGreSQL
        _tableNameSaveLoad = QString("SMCProject").toLower().toStdString();
        _schemaNameSaveLoad =  QString("TestProject2").toLower().toStdString();

        _createProjectSchema(std::string("TestProject2"), std::string("SMCProject"));
        _getPresentProjects(std::string("TestProject2"), std::string("SMCProject"));

        if(_projectNameAndPath.size())
        {
            QStringList list;
            std::map<std::string, std::string>::iterator iter =  _projectNameAndPath.begin();
            while(iter != _projectNameAndPath.end())
            {
                QString name = QString::fromStdString( std::string(iter->first) );
                list.push_back(name);
                iter++;
            }

            CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            //qtapp->getRootContext()->setContextProperty("layerList", QVariant::fromValue(_selectionLayerList));
            //return;
        }

        //Enable only for populating the project to the Database
        /*{
            osgDB::setCurrentWorkingDirectory(std::string("C:/Program Files (x86)/Apache Software Foundation/Apache2.2/htdocs/projects"));
            osgDB::DirectoryContents directoryContent = osgDB::getDirectoryContents(std::string("C:/Program Files (x86)/Apache Software Foundation/Apache2.2/htdocs/projects"));
            
            int count = directoryContent.size();
            int index = 0;
            while(index < count)
            {
                std::string name = directoryContent[index++];
                if( (!name.compare(".")) || (!name.compare("..")) )
                    continue;

                osgDB::FileType type = osgDB::fileType(name);
                if(type == osgDB::DIRECTORY)
                {
                    std::string projectName, path;
                    projectName = name;
                    path = osgDB::getRealPath(name);
                    _addProjectNameAndPath(projectName, path, _schemaNameSaveLoad,_tableNameSaveLoad);
                }
                else
                    continue;
            }
        }*/
/*
        //Testing for CURL downloading the data from the WebServer 
        if ( osgDB::containsServerAddress( layer ) )
        {
            int x = 10;
        }

        //osgDB::ReaderWriter* readerWrite = new DBPLUGINS::ReaderWriterCURL;
        layer += ".curl";
        osg::ref_ptr<osg::Node> node = osgDB::readNodeFile(layer);


        CORE::RefPtr<CORE::IWorld> iworld =     
                APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());

        CORE::RefPtr<CORE::IObject> modelObject =
        CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::ModelFeatureObjectType);

        if(!modelObject.valid())
        {
            return;
        }
        ELEMENTS::IModel* model = modelObject->getInterface<ELEMENTS::IModel>();
        if(model)
        {
            model->setOSGNode(node);
        }
        else
        {
            osg::ref_ptr<osg::Group> group  = modelObject->getOSGNode()->asGroup();
            if(group.valid())
            {
                group->removeChildren(0, 1);
                group->addChild(node.get());
            }
        }

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> modelFeatureObject = modelObject->getInterface<ELEMENTS::IModelFeatureObject>();

        if(modelFeatureObject.valid())
        {
            CORE::IPoint* point = modelFeatureObject->getInterface<CORE::IPoint>();
            if(point)
            {
                point->setValue(osg::Vec3d(73.0, 23.0, 10000));
            }
        }

        CORE::RefPtr<ELEMENTS::IModelHolder> scale = modelFeatureObject->getInterface<ELEMENTS::IModelHolder>();

        ELEMENTS::IRepresentation* representation = modelFeatureObject->getInterface<ELEMENTS::IRepresentation>();
        if(representation)
            representation->setRepresentationMode(ELEMENTS::IRepresentation::MODEL);
        // model
        if(scale)
        {
            scale->setModelScale(osg::Vec3(9000, 9000, 5000));
            scale->setModelMaximumViewDistance(1000000.0);
        }

       // _cameraUIHandler->setViewPoint(osg::Vec3d(73.0, 23.0, 10000), 10000, 0, -89, 2);
        iworld->addObject(modelObject.get());
*/
        return;

        if( (layerName.toStdString().find('*') != std::string::npos ) ||
            ( layerName.toStdString().find('?') != std::string::npos ) )
        {
            ss<<layer;//<<"$";
        }
        else
        {
            ss<<"*"<<layer<<"*";
        }

        std::string pattern = ss.str();

        QRegExp rx(pattern.c_str(), Qt::CaseInsensitive, QRegExp::Wildcard);
        
        //This will contain the final filtered List 
        QStringList list;
        for(int i = 0 ; i <  _layerList.size(); i++)
        {
            if(rx.indexIn(_layerList.at(i), 0) != -1 )
                list.append(_layerList.at(i));
        }
        
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if(!layerName.size())
        {
            qtapp->getRootContext()->setContextProperty("layerList", QVariant::fromValue(_selectionLayerList));
            return;
        }

        if(list.size())
        {
            qtapp->getRootContext()->setContextProperty("layerList", QVariant::fromValue(_selectionLayerList));
        }
        else
        {
            qtapp->getRootContext()->setContextProperty("layerList", QVariant::fromValue(NULL));
        }
    }

    QStringList
    LoadSaveProject::_getProjectQListFromProjectMap(std::map<std::string, std::string> projectNameAndPath)
    {
        QStringList list;
        if(projectNameAndPath.size())
        {
            
            std::map<std::string, std::string>::iterator iter =  _projectNameAndPath.begin();
            while(iter != _projectNameAndPath.end())
            {
                QString name = QString::fromStdString( std::string(iter->first) );
                list.push_back(name);
                iter++;
            }
        }
        return list;
    }

    void
    LoadSaveProject::loadFiles()
    {
		QStringList list;

        if(!makeConnectionWithDatabase())
		{
			list.push_back("Test");
			CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
			qtapp->getRootContext()->setContextProperty("smFiles", QVariant::fromValue(list));

			return;
		}

        _getPresentProjects(std::string("TestProject2"), std::string("SMCProject"));
 
		list.clear();
        list = _getProjectQListFromProjectMap(_projectNameAndPath);

		if(list.empty())
		{
			list.push_back("Test");
		}
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        qtapp->getRootContext()->setContextProperty("smFiles", QVariant::fromValue(list));

        _getPresentProjects(std::string("TestProject"), std::string("SMCProject"));

        list.clear();
        list = _getProjectQListFromProjectMap(_projectNameAndPath);
        qtapp->getRootContext()->setContextProperty("planFiles", QVariant::fromValue(list));

    }

    void LoadSaveProject::_loadAndSubscribeSlots()
    {
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
        if(item != NULL)
        {
            QObject* gui = item->findChild<QObject*>("desktop");
            if(gui!= NULL)
            {
                QObject::connect(gui, SIGNAL(loadFiles()), this, SLOT(loadFiles()));
            }
        }
    }

    void
    LoadSaveProject::onRemovedFromGUIManager()
    {
        // XXX TBD
        closeDatabaseConnection();
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

    }

    void 
    LoadSaveProject::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
            }
            catch(...)
            {
                // XXX - Ignoring the message here?
            }
        }
        else
        {
            VizQt::GUI::update(messageType, message);
        }
    }

    void 
    LoadSaveProject::initializeAttributes()
    {
        VizQt::GUI::initializeAttributes();

        std::string groupName = "LoadSaveProject Properties";

         // Add ContextPropertyName attribute
        _addAttribute(new CORE::StringAttribute("ContextProperty", "ContextProperty",
                                CORE::StringAttribute::SetFuncType(this, &LoadSaveProject::setContextPropertyName),
                                CORE::StringAttribute::GetFuncType(this, &LoadSaveProject::getContextPropertyName),
                                "Context property name",
                                groupName));

        groupName = "Credential";
        _addAttribute(new CORE::GroupAttribute("Credential", "Credential",
                        CORE::GroupAttribute::SetFuncType(this, &LoadSaveProject::addCredential),
                        CORE::GroupAttribute::GetFuncType(this, &LoadSaveProject::setCredential),
                        std::string("Credential"), groupName));
    }

    void
    LoadSaveProject::addCredential(const CORE::NamedGroupAttribute &credential)
    {
        const CORE::NamedStringAttribute* attrDatabase = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(std::string("Database")));

        const CORE::NamedStringAttribute*  attrUserName = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(std::string("Username")));

        const CORE::NamedStringAttribute* attrPassword  = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(std::string("Password")));

        const CORE::NamedStringAttribute* attrHostname = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(std::string("Hostname")));

        const CORE::NamedStringAttribute* attrPort = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(std::string("Portnumber")));

        if( (!attrDatabase) || (!attrUserName) || (!attrPassword) 
            || (!attrHostname) || (!attrPort) )
            return;

        std::string database = attrDatabase->getValue();
        std::string hostname = attrHostname->getValue();
        std::string port = attrPort->getValue();
        std::string username = attrUserName->getValue();
        std::string password = attrPassword->getValue();

        //Filiing the credential in the dataabase options 
        _dbOptions.clear();
        _dbOptions.insert(std::pair<std::string, std::string>("Name", ""));
        _dbOptions.insert(std::pair<std::string, std::string>("Database", database));
        _dbOptions.insert(std::pair<std::string, std::string>("Host", hostname));
        _dbOptions.insert(std::pair<std::string, std::string>("Port", port));
        _dbOptions.insert(std::pair<std::string, std::string>("Username", username));
        _dbOptions.insert(std::pair<std::string, std::string>("Password", password));

    }

    CORE::RefPtr<CORE::NamedGroupAttribute> 
    LoadSaveProject::setCredential() 
    {
        return NULL;
    }

    bool
    LoadSaveProject::makeConnectionWithDatabase()
    {

         //Getting the dataUIhandler for geting the vector and raster data from the postgresql
        _dbUIHandler =
            vizApp::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getGUIManager());

        CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
        CORE::RefPtr<APP::IManager> mgr = app->getManagerByClassname("VizUI::UIHandlerManager");
        CORE::RefPtr<APP::IUIHandlerManager> uiHandlerMgr = mgr->getInterface<vizApp::IUIHandlerManager>();
        
        if(_dbUIHandler.valid())
        {
            _dbUIHandler->setManager(uiHandlerMgr);
        }
        
        bool databaseConnectionStatus = _checkConnectionWithDatabase();
        return databaseConnectionStatus;
    }

    bool
    LoadSaveProject::_checkConnectionWithDatabase()
    {
        if(_dbOptions.size() < 5)
            return false;

        //Right now pasing the dummy value
        if(_dbUIHandler->testConnection(_dbOptions))
        {
            //QMessageBox::warning(this, QString::QString("Test Connection"), QString::QString("Connection is Established"));
            return true;
        }
        else
        {
            //QMessageBox::warning(this, QString::QString("Connecting error"), QString::QString("Unable to connect"));
            return false;
        }

        return false;
    }


    VizDataUI::IDatabaseUIHandler*
    LoadSaveProject::_getDatabaseUIHandler()
    {
       VizDataUI::IDatabaseUIHandler* dbUIHandler = NULL;
       if(!_dbUIHandler)
       {
           CORE::RefPtr<APP::IUIHandlerFactory> uf = APP::ApplicationRegistry::instance()->getUIHandlerFactory();
           const APP::IUIHandlerType *uhType = uf->getUIHandlerType("DatabaseUIHandler");
           CORE::RefPtr<vizApp::IUIHandler> _uih = uf->createUIHandler(*uhType);
           _dbUIHandler = _uih->getInterface<VizDataUI::IDatabaseUIHandler>();
       }
       return dbUIHandler;
    }

    void 
    LoadSaveProject::closeDatabaseConnection()
    {
        if(_dbUIHandler)
        {
            _dbUIHandler->disconnect();
        }
    }
} // namespace indiGUI
