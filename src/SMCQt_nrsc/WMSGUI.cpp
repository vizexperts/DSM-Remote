/****************************************************************************
 *
 * File             : WMSGUI.cpp
 * Description      : WMSGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, CAIR, DRDO
 *****************************************************************************/

#include <SMCQt/WMSGUI.h>

#include <APP/IApplication.h>
#include <APP/AccessElementUtils.h>

#include <CORE/WorldMaintainer.h>

#include <UTIL/StringUtils.h>
#include <UTIL/CoordinateConversionUtils.h>
#include <UTIL/VolumeGridUtils.h>
#include <UTIL/WMSUtils.h>

#include <VizUI/IMouseMessage.h>

#include <CORE/ICompositeObject.h>
#include <CORE/Referenced.h>
#include <CORE/IMetadataRecordHolder.h>

#ifndef USE_Qt4
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTreeWidgetItem>
#include <QtWidgets/QMessageBox>
#endif

#include <QtGUI/QIntValidator>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, WMSGUI);
 
    const std::string WMSGUI::LEServerUrl   = "LEServerUrl";    //line Edit
    const std::string WMSGUI::PBConnect     = "PBConnect";      //Push Button connect
    const std::string WMSGUI::PBAddLayers   = "PBAddLayers";    //Push Button for starting timebase animation
    const std::string WMSGUI::TRLayer       = "TRLayer";        //Layer Information Tree

    WMSGUI::WMSGUI()
    {
    }

    WMSGUI::~WMSGUI()
    {
    }

    void 
    WMSGUI::_loadAndSubscribeSlots()
    {
        LayoutFileGUI::_loadAndSubscribeSlots();

        if(!getComplete())
        {
                return;
        }

        // Keep the dialog box on top but not block application input
        //_parent->setWindowFlags(_parent->windowFlags() | Qt::WindowStaysOnTopHint);

		_serverUrlLE    = _parent->findChild<QLineEdit *>(_getWidgetName(LEServerUrl).c_str());
        _connectPB      = _parent->findChild<QPushButton *>(_getWidgetName(PBConnect).c_str());
        _addLayersPB    = _parent->findChild<QPushButton *>(_getWidgetName(PBAddLayers).c_str());
        _layerTR        = _parent->findChild<QTreeWidget *>(_getWidgetName(TRLayer).c_str());

        if ( !_serverUrlLE || !_connectPB || !_addLayersPB || !_layerTR )
        {
            setComplete(false);
        }

        //Connects
        QObject::connect(_connectPB,   SIGNAL(clicked(bool)), 
                            this, SLOT(_handleConnectButtonClicked(bool)));

        QObject::connect(_addLayersPB, SIGNAL(clicked()), 
                            this, SLOT(_handleAddLayersButtonClicked()));

        QObject::connect(_layerTR,     SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), 
                            this, SLOT(_handleTreeItemDoubleClicked(QTreeWidgetItem*, int)));
    }

    void
    WMSGUI::onRemovedFromGUIManager()
    {
        LayoutFileGUI::onRemovedFromGUIManager();
    }

    void
    WMSGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _WMSUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IWMSUIHandler>(getGUIManager());

                if ( !_WMSUIHandler.valid() )
                {
                    setComplete(false);
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
    }


    // initilalization of Qwidget, Units and ComputationType attributes 
    void 
    WMSGUI::initializeAttributes()
    {
        LayoutFileGUI::initializeAttributes();

        _addWidgetName(LEServerUrl);
        _addWidgetName(PBConnect);
        _addWidgetName(PBAddLayers);
        _addWidgetName(TRLayer);
    }
    
    
    void
    WMSGUI::setActive(bool value)
    {
        if (!getComplete())
        {
            return;
        }

        try
        {
            if(value)
            {
                _reset();
            }
            else
            {
                _cleanUP();
            }

            LayoutFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void
    WMSGUI::_cleanUP()
    {
    }

    void
    WMSGUI::_reset()
    {
        _layerTR->clear();
        _serverUrlLE->clear();

        _addLayersPB->setEnabled(false);
    }

    void 
    WMSGUI::_handleConnectButtonClicked(bool flag)
    {
        std::string str = _serverUrlLE->text().toStdString();

        if(!str.empty())
        {
            _WMSUIHandler->setServerUrl(str);
            if( _WMSUIHandler->initializeLayers() )
            {
                _addLayersPB->setEnabled(true); 
                // Enable AddLayers Button only if we have Found some layers nad a successful connection
            }
            
            if(_WMSUIHandler->getLayers().size())
                _showAllLayers( _WMSUIHandler->getLayers() );
            else
            {
                QMessageBox::critical(_parent, "Invalid URL", "No map Layer exists");
                return;
            }
        }
        else
        {
            QMessageBox::critical(_parent, "Invalid URL", "Please enter Valid URL");
            return;
        }
    }

    void 
    WMSGUI::_handleAddLayersButtonClicked()
    {
        QTreeWidgetItemIterator treeIterator(_layerTR, QTreeWidgetItemIterator::Checked);
        if ( !(*treeIterator) )
        {
            QMessageBox::critical(_parent, "No Layer Checked", "Please select layer from the list first");
            return;
        }

        while (*treeIterator)
        {
            _WMSUIHandler->addSelectedLayer( ((*treeIterator)->text(0)).toStdString());
            ++treeIterator;
        }

        QMessageBox::information(_parent, "Confirmation Message", "Layer(s) Added");
    }

    void 
    WMSGUI::_handleTreeItemDoubleClicked(QTreeWidgetItem* item, int column)
    {
        std::cout<<"\nLayer Name: "<<item->text(0).toStdString();
    }

    void
    WMSGUI::_showAllLayers(const UTIL::Layer::LayerList &list)
    {
        //Clearing the previously stored Tree
        _layerTR->clear();

        _layerTR->setColumnCount(5);

        QTreeWidgetItem *topLevelItem = new QTreeWidgetItem();
        _layerTR->addTopLevelItem(topLevelItem);

        topLevelItem->setText(0, "WMSLayers");

        _getTreeWidgetItem(list, topLevelItem);
    }

    // Iterates LayerList and display all layers as QTTreeWidget items
    void 
    WMSGUI::_getTreeWidgetItem(const UTIL::Layer::LayerList &list, QTreeWidgetItem *parentWidget)
    {
        unsigned int numOfLayers = list.size();
        for(unsigned int currLayer=0; currLayer!=numOfLayers; ++currLayer) 
        {
            //Creating the Item
            QTreeWidgetItem * item = new QTreeWidgetItem();

            //configuring the Item
            item->setText(0, QString::fromStdString(list[currLayer]->getName()));
            item->setText(1, QString::fromStdString(list[currLayer]->getTitle()));

            if (list[currLayer]->getLayers().size() != 0)
            {
                _getTreeWidgetItem(list[currLayer]->getLayers(), item);
            }
            else
            {
                /* Setting all layer selection checkboxes to unChecked state */
                item->setCheckState(0,Qt::Unchecked);
                item->setText(2, QString::fromStdString(to_simple_string(list[currLayer]->getMaxTime())));
                item->setText(3, QString::fromStdString(to_simple_string(list[currLayer]->getMinTime())));
                item->setText(4, QString::fromStdString(to_simple_string(list[currLayer]->getTimeDefault())));
            }

            parentWidget->addChild(item);
        }
    }
} // namespace indiGUI
