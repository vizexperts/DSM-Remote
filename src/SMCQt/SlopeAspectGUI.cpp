/****************************************************************************
*
* File             : SlopeAspectGUI.h
* Description      : SlopeAspectGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <SMCQt/SlopeAspectGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/ISlopeAspectFilterVisitor.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Elements/IColorMapComponent.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCQt
{
    const std::string SlopeAspectGUI::SlopeAnalysisObjectName = "SlopeAnalysis";

    DEFINE_META_BASE(SMCQt, SlopeAspectGUI);
    DEFINE_IREFERENCED(SlopeAspectGUI,DeclarativeFileGUI);

    SlopeAspectGUI::SlopeAspectGUI()
    {}

    SlopeAspectGUI::~SlopeAspectGUI()
    {}

	void SlopeAspectGUI::_updateProgressBarVisibility(bool value)
	{
		QObject *slopeAnalysis = _findChild("SlopeAnalysis");
		if (slopeAnalysis)
		{
			slopeAnalysis->setProperty("progressbarvisible", QVariant::fromValue(value));
		}
	}


    void SlopeAspectGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(_handleProgressValueChanged(int)),Qt::QueuedConnection);
    }

    void SlopeAspectGUI::_populateAnalysisListMenu()
    {
        QList<QObject*> analysisList;
        analysisList.append(new VizComboBoxElement("Slope Aspect","0"));
        analysisList.append(new VizComboBoxElement("Slope Percent","1"));
        analysisList.append(new VizComboBoxElement("Slope Degree","2"));
        _setContextProperty("analysisListModel", QVariant::fromValue(analysisList));
    }

    void SlopeAspectGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::ISlopeAspectUIHandler>(getGUIManager());
                _colorUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IColorMapUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    if(_slopeAspectObject)
                    {
                        _slopeAspectObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        _slopeAspectObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        _slopeAspectObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        _slopeAspectObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
             GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if(progress < 100)
                {
                   emit _setProgressValue(progress);
                }
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                    emit _setProgressValue(100);
            }
            else
            {
                 if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
					_updateProgressBarVisibility(false);
                    emit showError("Filter Status", "Failed to apply filter");
                    _handlePBStopClicked();
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void SlopeAspectGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                _slopeAspectObject = _findChild(SlopeAnalysisObjectName);

                if(_slopeAspectObject)
                {
                    // start and stop button handlers
                    connect(_slopeAspectObject, SIGNAL(startAnalysis()),this,SLOT(_handlePBStartClicked()),Qt::UniqueConnection);
                    connect(_slopeAspectObject, SIGNAL(stopAnalysis()),this, SLOT(_handlePBStopClicked()),Qt::UniqueConnection);
                    connect(_slopeAspectObject, SIGNAL(markPosition(bool)),this, 
                        SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);

                    _populateAnalysisListMenu();
                    _populateColorMap();

                    // enable start button
                    _slopeAspectObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    _slopeAspectObject->setProperty("isStopButtonEnabled",false);

                    //disable progress value
                    _slopeAspectObject->setProperty("progressValue",0);
                }
            }
            else
            {
                _reset();
                _handlePBStopClicked();
            }

            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }

            CORE::RefPtr<APP::IGUI> gui = getGUIManager()->getGUIByClassname("SMCQt::ColorPaletteGUI");
            if (gui.valid())
            {
                VizQt::IQtGUI* qtGUI = gui->getInterface<VizQt::IQtGUI>();
                QObject::connect(dynamic_cast<VizQt::QtGUI*>(qtGUI), SIGNAL(newColorMapAdded(const QString&)),
                    this, SLOT(_hadleNewColorMapAdded(const QString&)));
            }
            else
            {
                QObject::disconnect(this, SLOT(_hadleNewColorMapAdded(const QString&)));
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void SlopeAspectGUI::_handleProgressValueChanged(int value)
    {
        if(_slopeAspectObject)
        {
            _slopeAspectObject->setProperty("progressValue",QVariant::fromValue(value));
        } 
    }

    //! 
    void SlopeAspectGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        //get name of colorMap
        if(_slopeAspectObject)
        {
            std::string name = _slopeAspectObject->property("outputName").toString().toStdString();

            if(name == "")
            {
				_updateProgressBarVisibility(false);
                showError( "Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            _slopeAspectObject->setProperty("outputName",QString::fromStdString(name));
            _uiHandler->setOutputName(name);
            //select color Map
            int uid = _slopeAspectObject->property("colorMapSelectedUid").toString().toInt();
            std::string mapName = _slopeAspectObject->property("colorMapName").toString().toStdString();
            if (_colorUIHandler)
            {
                CORE::RefPtr<ELEMENTS::IColorMap> colorMap = _colorUIHandler->getColorMap(mapName);
                if (uid == 0)
                {
					_updateProgressBarVisibility(false);
                    showError("Insufficient / Incorrect data", "Color map is not selected, select a  valid color map");
                    return;
                }
                if (uid > 0)
                {
                    //_uiHandler->setColorMapType(static_cast<vizElements::IColorMap::COLORMAP_TYPE>(uid));
                    _uiHandler->setColorMap(colorMap.get());
                }
                else
                {
					_updateProgressBarVisibility(false);
                    showError("Insufficient / Incorrect data", "The selected color map is not valid");
                    return;
                }
            }
           if (uid == 0)
            {
				_updateProgressBarVisibility(false);
                showError("Insufficient / Incorrect data", "Color map is not selected, select a  valid color map");
                return;
            }
            if (uid > 0)
            {
                _uiHandler->setColorMapType(static_cast<vizElements::IColorMap::COLORMAP_TYPE>(uid));
            }
            else
            {
				_updateProgressBarVisibility(false);
                showError("Insufficient / Incorrect data", "The selected color map is not valid");
                return;
            }
            bool advanceOptionClicked = _slopeAspectObject->property("isAdvanceOptionClicked").toBool();
            if (advanceOptionClicked)
            {
                //Map Step Size
                int stepSizeVal = _slopeAspectObject->property("stepSizeVal").toInt();

                if (stepSizeVal <= 0)
                {
					_updateProgressBarVisibility(false);
                    showError("Insufficient / Incorrect data", "Map Size is not correct");
                    return;
                }
                _uiHandler->setMapSize(stepSizeVal);
            }


            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = _slopeAspectObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = _slopeAspectObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = _slopeAspectObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() =  _slopeAspectObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
					_updateProgressBarVisibility(false);
                    showError( "Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
						_updateProgressBarVisibility(false);
                        showError( "Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                }
            }

            //RFE2
#ifdef SLOPE_ASPECT_ENABLE_ELEVATION_LAYER
            // set elevation object
            {
                TERRAIN::IElevationObject* elevObj = _getCurrentElevationObject();

                if(!elevObj)
                {
					_updateProgressBarVisibility(false);
                    showError( "Elevation Object", "Select an elevation object");
                    return;
                }

                // check for validity of extents
                CORE::IGeoExtent* gExtent = elevObj->getInterface<CORE::IGeoExtent>();

                if(gExtent)
                {
                    osg::Vec4d elevExtents;
                    if(gExtent->getExtents(elevExtents))
                    {
                        if(extents.x() < elevExtents.x() || extents.y() < elevExtents.y() ||
                            extents.z() > elevExtents.z() || extents.w() > elevExtents.w())
                        {
							_updateProgressBarVisibility(false);
                            showError( "Area Extents", "Specified area extent is "
                                "outside of selected elevation object");

                            return;
                        }
                    }
                }

                _uiHandler->setElevationObject(elevObj);
            }
#endif

            // set compute type
            {
                std::string uid = _slopeAspectObject->property("analysisMapSelectedUid").toString().toStdString();
                std::string text = _slopeAspectObject->property("analysisMapName").toString().toStdString();

                GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType type = 
                    GISCOMPUTE::ISlopeAspectFilterVisitor::COMPUTE_SLOPE_DEGREE;

                if(text == "Slope Degree")
                {
                    type = GISCOMPUTE::ISlopeAspectFilterVisitor::COMPUTE_SLOPE_DEGREE;
                }
                else if(text == "Slope Percent")
                {
                    type = GISCOMPUTE::ISlopeAspectFilterVisitor::COMPUTE_SLOPE_PERCENT;
                }
                else if(text == "Slope Aspect")
                {
                    type = GISCOMPUTE::ISlopeAspectFilterVisitor::COMPUTE_ASPECT;
                }
                _uiHandler->setComputeType(type);
            }

            _slopeAspectObject->setProperty("progressValue",0); 
            _handlePBAreaClicked(false);
            // start filter

            _uiHandler->execute();
        }
    }

    void SlopeAspectGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();
        if(_slopeAspectObject)
        {
            //disable stop button
            _slopeAspectObject->setProperty("isStopButtonEnabled",false);
            _slopeAspectObject->setProperty("isStartButtonEnabled",true);
            _slopeAspectObject->setProperty("progressValue",QVariant::fromValue(0));
        } 
    }

    void SlopeAspectGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            
            if(_slopeAspectObject)
            {
                _slopeAspectObject->setProperty( "isMarkSelected" , false );
            }
        }
    }

    void SlopeAspectGUI::_reset()
    {
        if(_slopeAspectObject)
        {
            _slopeAspectObject->setProperty("isMarkSelected",false);
            _slopeAspectObject->setProperty("bottomLeftLongitude","longitude");
            _slopeAspectObject->setProperty("bottomLeftLatitude","latitude");
            _slopeAspectObject->setProperty("topRightLongitude","longitude");
            _slopeAspectObject->setProperty("topRightLatitude","latitude");
            _slopeAspectObject->setProperty("outputName","outputName");
            _slopeAspectObject->setProperty("stepSizeVal", 100);
            _slopeAspectObject->setProperty("progressValue",0);
            _slopeAspectObject->setProperty("isAdvanceOptionClicked", false);
            _handlePBAreaClicked(false);
        }
    }
    void SlopeAspectGUI::_hadleNewColorMapAdded(const QString& text)
    {
        _populateColorMap();
    }
    void SlopeAspectGUI::_populateColorMap()
    {
        APP::IGUIManager* guiMgr = getGUIManager();
        CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(guiMgr);
        if(!wmain)
        {
            return;
        }
        CORE::IComponent* comp = wmain->getComponentByName("ColorMapComponent");
        if(!comp)
        {
            return;
        }
        ELEMENTS::IColorMapComponent* colorMapComponent = comp->getInterface<ELEMENTS::IColorMapComponent>();
        const ELEMENTS::IColorMapComponent::ColorMapList& list = colorMapComponent->getColorMapList();
        QList<QObject*> colorMapList;
        colorMapList.clear();
        //tomporary Commment
        colorMapList.append(new VizComboBoxElement("Create New Color Map","0"));
        //colorMapList.append(new VizComboBoxElement("Red-Green color map","1"));
        int uid = 1;
        ELEMENTS::IColorMapComponent::ColorMapList::const_iterator iter = list.begin();
        for(; iter != list.end(); ++iter)
        {
            colorMapList.append(new VizComboBoxElement(QString::fromStdString(iter->first),QString::number(uid)));
            uid++;
        }
        _setContextProperty("colorMapListModel", QVariant::fromValue(colorMapList));
    }

}//namespace SMCQt
