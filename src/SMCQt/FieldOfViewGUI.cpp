/****************************************************************************
*
* File             : FieldOfViewGUI.cpp
* Description      : FieldOfViewGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/FieldOfViewGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>

#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <Core/ILine.h>
#include <Core/IWorld.h>
#include <Core/IFeatureLayer.h>
#include <Core/IEditable.h>
#include <Core/IDeletable.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/ICompositeObject.h>
#include <Core/ITemporary.h>

#include <SMCElements/ILayerComponent.h>

#include <VizUI/IDeletionUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/ICameraUIHandler.h>

#include <SMCUI/IGridDisplayUIHandler.h>

#include <serialization/AsciiStreamOperator.h>
#include <serialization/IProjectLoaderComponent.h>

#include <Elements/ElementsUtils.h>
#include <Elements/FeatureObjectUtils.h>

#include <ogrsf_frmts.h>
#include <ogr_api.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <Core/ITerrain.h>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, FieldOfViewGUI);
    DEFINE_IREFERENCED(FieldOfViewGUI,DeclarativeFileGUI);


    FieldOfViewGUI::FieldOfViewGUI()
    {
    }

    FieldOfViewGUI::~FieldOfViewGUI()
    {
    }

    void FieldOfViewGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if(smpLeftMenu != NULL)
        {
            QObject::connect(smpLeftMenu, SIGNAL(connectFovMenu()), this,
                SLOT(connectFov()), Qt::UniqueConnection);
        }
    }

    void FieldOfViewGUI::connectFov()
    {
        _fovObject = _findChild("ViewSettings");
        if(_fovObject != NULL)
        {
            double fov;
            double verticalScaleValue=1;
            CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
            if(wmain)
            {
                CORE::IComponent* component = wmain->getComponentByName("SettingComponent");
                if(component)
                {
                    _settingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
                    if(_settingComponent.valid())
                    {
                        fov = _settingComponent->getFov();
                        _fovObject->setProperty("fovValue",QVariant::fromValue(fov));
                        _fovObject->setProperty("scaleValue", QVariant::fromValue(verticalScaleValue));
                        handleVerticalScale(verticalScaleValue);
                    }
                }

                if(_cameraComponent.valid())
                {
                    bool useAOR = _cameraComponent->getUseAOR();
                    _fovObject->setProperty("useAOR", QVariant::fromValue(useAOR));

                    bool useOrtho = _cameraComponent->isOrtho();
                    _fovObject->setProperty("useOrtho", QVariant::fromValue(useOrtho));
                }

                CORE::RefPtr<VizUI::ISelectionUIHandler> selUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
                if(selUIHandler.valid())
                {
                    bool selectionState = selUIHandler->getSelectionState();
                    _fovObject->setProperty("selection", QVariant::fromValue(selectionState));
                }
                //Grid and IndianGrid Check Set
                CORE::RefPtr<SMCUI::IGridDisplayUIHandler> gridUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IGridDisplayUIHandler>(getGUIManager());

                if (gridUIHandler.valid())
                {
                    bool gridDiplayState = gridUIHandler->getGridDisplay();
                    _fovObject->setProperty("dispayGrid", QVariant::fromValue(gridDiplayState));

                    bool indianGridDiplayState = gridUIHandler->getIndianGridDisplay();
                    _fovObject->setProperty("dispayIndianGrid", QVariant::fromValue(indianGridDiplayState));
                }

                // connect to signals
                QObject::connect(_fovObject, SIGNAL(changeFov(double)), 
                    this, SLOT(handleChangeFov(double)), Qt::UniqueConnection);
                QObject::connect(_fovObject, SIGNAL(verticalScale(double)),
                    this, SLOT(handleVerticalScale(double)), Qt::UniqueConnection);

                QObject::connect(_fovObject, SIGNAL(changeAorSetting(bool)), this, SLOT(handleChangeAor(bool)), 
                    Qt::UniqueConnection);

                QObject::connect(_fovObject, SIGNAL(changeToOrtho(bool)), this, SLOT(handleChangeOrtho(bool)), 
                    Qt::UniqueConnection);

                QObject::connect(_fovObject, SIGNAL(changeSelectionState(bool)), this, SLOT(handleChangeSelectionState(bool)), 
                    Qt::UniqueConnection);

                QObject::connect(_fovObject, SIGNAL(markAOR(bool)), this, SLOT(handleMarkAor(bool)), Qt::UniqueConnection);

                QObject::connect(_fovObject, SIGNAL(displayGrid(bool)), this, SLOT(handleDisplayGrid(bool)), Qt::UniqueConnection);
                QObject::connect(_fovObject, SIGNAL(displayIndianGrid(bool)), this, SLOT(handleDisplayIndianGrid(bool)), Qt::UniqueConnection);

                QObject::connect(_fovObject, SIGNAL(save()), this,
                    SLOT(saveFovSetting()), Qt::UniqueConnection);
            }
        }
    }

    void FieldOfViewGUI::handleVerticalScale(double value)
    {
        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
        CORE::ITerrain* terrain = world->getTerrain();
        terrain->setVerticalScaleValue(value);
    }
    void FieldOfViewGUI::handleDisplayGrid(bool value)
    {
        CORE::RefPtr<SMCUI::IGridDisplayUIHandler> gridUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IGridDisplayUIHandler>(getGUIManager());

        if(!gridUIHandler.valid())
            return;

        gridUIHandler->setGridDisplay(value);
    }
    void FieldOfViewGUI::handleDisplayIndianGrid(bool value)
    {
        CORE::RefPtr<SMCUI::IGridDisplayUIHandler> gridUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IGridDisplayUIHandler>(getGUIManager());

        if (!gridUIHandler.valid())
            return;

        gridUIHandler->setIndianGridDisplay(value);
    }
    void FieldOfViewGUI::handleChangeSelectionState(bool value)
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if(selUIHandler.valid())
        {
            selUIHandler->setSelectionState(value);
        }
    }


    void FieldOfViewGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        static int tickCount = 0;

        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {

                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());

                //! subscribe to world loaded message
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                //! subscribe to project loaded message type
                CORE::RefPtr<CORE::IComponent> component = ELEMENTS::GetComponentFromMaintainer("ProjectLoaderComponent");
                if(component.valid())
                {
                    _subscribe(component.get(), *DB::IProjectLoaderComponent::ProjectLoadedMessageType);
                }

                CORE::IComponent* cameraComponent = ELEMENTS::GetComponentFromMaintainer("CameraComponent");
                if(cameraComponent)
                {
                    _cameraComponent = cameraComponent->getInterface<VIEW::ICameraComponent>();
                }

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
            _subscribe(wmain.get(), *CORE::WorldMaintainer::TickMessageType);

            tickCount = 0;

        }
        else if(messageType == *DB::IProjectLoaderComponent::ProjectLoadedMessageType)
        {
            _area = NULL;
            _aorLayer = NULL;
            handleChangeAor(false);                

            _initializeAORFeature();
        }
        else if(messageType == *SMCUI::IAreaUIHandler::AreaCreatedMessageType)
        {
            if(_areaHandler.valid())
            {
                _area = _areaHandler->getCurrentArea();
                if(_area.valid())
                {
                    _setAorToCameraComponent();

                    _configureCreatedArea();

                    _areaHandler->reset();
                }

                // disable mark button
                _fovObject = _findChild("ViewSettings");
                if(_fovObject)
                {
                    _fovObject->setProperty("markButtonChecked", QVariant::fromValue(false));
                }

                // Make marked polygon non editable and unfilled

            }
        }
        else if(messageType ==  *CORE::IWorldMaintainer::TickMessageType)
        {
            try
            {
                if(tickCount == 3)
                {
                    CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                        APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                    CORE::RefPtr<VizUI::ICameraUIHandler> camerauihandler = 
                        APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager()); 
                    if(wmain.get())
                    {
                        _unsubscribe(wmain.get(), *CORE::WorldMaintainer::TickMessageType);

                        CORE::IComponent* component = wmain->getComponentByName("SettingComponent");
                        if(component)
                        {
                            _settingComponent = component->getInterface<ELEMENTS::ISettingComponent>();

                            if(!camerauihandler.valid())
                            {
                                throw;
                            }
                            else
                            {
                                double fov = _settingComponent->getFov();

                                //first time when settings is not saved
                                if(fov < 10)
                                    fov = 30;

                                camerauihandler->setFieldOfView(fov);
                            }
                        }
                    }
                }
            }
            catch(...)
            {
                LOG_ERROR("Unable to update FOV");
            }

            tickCount++;
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void FieldOfViewGUI::_configureCreatedArea()
    {
        _getAORLayer();
        if(!_aorLayer.valid())
            return;

        _aorLayer->getInterface<CORE::ICompositeObject>()->removeObjectByName("AOR");

        if(_area.valid())
        {
            CORE::IObject* object = _area->getInterface<CORE::IObject>();

            CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = ELEMENTS::getMetaDataCreator();

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = _aorLayer->getInterface<CORE::IMetadataTableHolder>(true)->getMetadataTable()->getMetadataTableDefn();
            CORE::RefPtr<CORE::IMetadataRecord> record = metadataCreator->createMetadataRecord();
            record->setTableDefn(tableDefn.get());

            _area->getInterface<CORE::IMetadataRecordHolder>()->setMetadataRecord(record.get());

            ELEMENTS::GetFirstWorldFromMaintainer()->removeObjectByID(&(_area->getInterface<CORE::IBase>()->getUniqueID()));

            _area->getInterface<CORE::IEditable>()->setEditable(false);
            _area->getInterface<CORE::IBase>()->setName("AOR");

            _area->getInterface<CORE::ITemporary>()->setTemporary(false);

            _aorLayer->getInterface<CORE::ICompositeObject>()->addObject(object);
        }
    }

    void FieldOfViewGUI::_initializeAORFeature()
    {
        //! Try to get AOR layer and assign it to Camera component
        _getAORLayer(false);

        if(_aorLayer.valid())
        {
            //! get first object
            CORE::RefPtr<CORE::ICompositeObject> composite = _aorLayer->getInterface<CORE::ICompositeObject>();            
            CORE::RefPtr<CORE::IObject> object = composite->getObjectByName("AOR");
            if(!object.valid() && composite->getObjectCount() > 0)
            {
                const CORE::ObjectMap& objMap = composite->getObjectMap();
                CORE::ObjectMap::const_iterator iter = objMap.begin();
                if(iter != objMap.end())
                {
                    object = iter->second;
                }
            }

            if(object.valid())
            {
                _area = object->getInterface<CORE::IPolygon>();
            }

            if(_area.valid())
            {
                _setAorToCameraComponent();

                jumpToObject(_area->getInterface<CORE::IObject>());

                handleChangeAor(true);                
            }
        }
    }

    void FieldOfViewGUI::_setAorToCameraComponent()
    {

        //! pass created AOR to CameraComponent
        if(_cameraComponent.valid())
        {
            _cameraComponent->setAOR(_area);
        }
    }

    void FieldOfViewGUI::_getAORLayer(bool create)
    {
        //! layer is already present
        if(_aorLayer.valid())
        {
            return;
        }

        //! get world maintainer
        CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
        if(!wm.valid())
        {
            return;
        }

        //! get layer component
        CORE::RefPtr<CORE::IComponent> component = wm->getComponentByName("LayerComponent");
        if(!component.valid())
        {
            return;
        }

        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = component->getInterface<SMCElements::ILayerComponent>();
        if(!layerComponent.valid())
        {
            return;
        }

        std::string layerType = "_VIZ_AOR";
        std::string layerGeometry = "Area";

        //! Check if AOR layer is already added
        _aorLayer = layerComponent->getFeatureLayer(layerType);

        if(create)
        {
            if(!_aorLayer.valid())
            {
                //! get layer attributes
                std::vector<std::string> attrNames, attrTypes;
                std::vector<int> attrWidths, attrPrecesions;
                if(_areaHandler.valid())
                {
                    _areaHandler->getDefaultLayerAttributes(attrNames, attrTypes, attrWidths, attrPrecesions);
                }

                //! Create layer
                //! this takes care of adding it to world
                _aorLayer = layerComponent->getOrCreateFeatureLayer(layerType, layerGeometry, attrNames, attrTypes, attrWidths, attrPrecesions);

                //! set layer as non editable
                _aorLayer->getInterface<CORE::IEditable>()->setEditable(false);
            }        
        }
    }

    void FieldOfViewGUI::handleChangeFov(double fovValue)
    {
        if(_fovObject != NULL)
        {
            CORE::RefPtr<VizUI::ICameraUIHandler> camerauihandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager()); 
            if(camerauihandler)
            {
                if(fovValue < 10)
                {
                }
                else
                {
                    camerauihandler->setFieldOfView(fovValue);
                }
            }
        }
    }

    void FieldOfViewGUI::handleChangeAor(bool value)
    {
        if(_cameraComponent.valid())
        {
            _cameraComponent->setUseAOR(value);

            if(value && _area.valid())
            {
                jumpToObject(_area->getInterface<CORE::IObject>());
            }
        }
    }

    void FieldOfViewGUI::handleChangeOrtho(bool value)
    {
        if(_cameraComponent.valid())
        {
            _cameraComponent->setOrtho(value);
        }
    }

    void FieldOfViewGUI::handleMarkAor(bool value)
    {
        if(value)
        {
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_AREA);
            if(_area.valid())
            {
                //! remove previously created AOR
                CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
                if(deletionUIHandler.valid())
                {
                    deletionUIHandler->deleteObject(_area->getInterface<CORE::IDeletable>());
                }
            }
            _area = _areaHandler->getCurrentArea();

            //! subscribe to area created message
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaCreatedMessageType);

        }
        else
        {
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            if(_area.valid())
            {
                _removeObject(_area.get());
                _area = NULL;
            }

            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaCreatedMessageType);
        }
    }

    void FieldOfViewGUI::saveFovSetting()
    {
        if(_fovObject != NULL)
        {
            _writeSettings();
        }
    }

    void FieldOfViewGUI::_writeSettings()
    {
        DB::BaseWrapper *wrapper   = DB::BaseWrapperManager::instance()->findWrapper("SettingComponent");
        if(wrapper)
        {
            CORE::IComponent *settingComponent = ELEMENTS::GetComponentFromMaintainer("SettingComponent");
            CORE::IBase* base = NULL;
            if(settingComponent)
            {
                base = settingComponent->getInterface<CORE::IBase>();

                std::stringstream inputstream;
                CORE::RefPtr<DB::OutputIterator> oi = new AsciiOutputIterator(&inputstream);

                DB::OutputStream os( NULL );
                os.start( oi.get(), DB::OutputStream::WRITE_ATTRIBUTE);
                wrapper->write(os, *base);

                std::ofstream filePtr;
                std::string settingConfigFile = 
                    settingComponent->getInterface<ELEMENTS::ISettingComponent>()->getSettingFilePath();

                filePtr.open(settingConfigFile.c_str(), std::ios::out);
                std::string dataToWrite = inputstream.str();
                filePtr << dataToWrite.c_str();
                filePtr.close();
            }
        }
    }
} // namespace SMCQt
