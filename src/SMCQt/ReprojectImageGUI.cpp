/****************************************************************************
*
* File             : ReprojectImageGUI.h
* Description      : ReprojectImageGUI class definition
*
*****************************************************************************
* Copyright (c) 2010-2011, VizExperts india Pvt. Ltd.                                       
*****************************************************************************/

#include <SMCQt/ReprojectImageGUI.h>
#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>
#include <Core/IVisitorFactory.h>
#include <SMCUI/AddContentUIHandler.h>
#include <osgDB/FileNameUtils>
#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/IUIHandlerManager.h>
#include <App/ApplicationRegistry.h>
#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>
#include <Util/Log.h>
#include <Util/StringUtils.h>
#include <VizQt/GUI.h>
#include <QFileDialog>
#include <QtCore/QFile>
#include <QtUiTools/QtUiTools>
#include <iostream>
#include <gdal.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ReprojectImageGUI)
        DEFINE_IREFERENCED(ReprojectImageGUI, CORE::Base)

        ReprojectImageGUI::ReprojectImageGUI()
        :_stereoWizard(NULL)
    {
    }

    ReprojectImageGUI::~ReprojectImageGUI()
    {
    }

    // to load the connect and disconnect slot from the qml
    void ReprojectImageGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
        QObject::connect(this,SIGNAL(_updateGUIStatus()),this,SLOT(_handleGUIStatus()),Qt::QueuedConnection);


    }

    //called when the PanSharpen qml is loaded.
    void ReprojectImageGUI::connectPopupLoader(QString type)
    {
        if(type == "ReprojectImage")
        {
            setActive( true );
            _reprojectObject = _findChild("ReprojectImage");
            if(_reprojectObject != NULL)
            {
                setActive( true );
                QObject::connect(_reprojectObject, SIGNAL(browseInputImage()),
                    this, SLOT(_handleInputBrowseButtonClicked()), Qt::UniqueConnection);

                QObject::connect(_reprojectObject, SIGNAL(browseOutputImage()), 
                    this, SLOT(_handleoutputBrowseButtonClicked()), Qt::UniqueConnection);

                QObject::connect(_reprojectObject, SIGNAL(viewImage(QString)),
                    this, SLOT(_viewImage(QString)),Qt::QueuedConnection);

                QObject::connect(_reprojectObject, SIGNAL(okButtonClicked()), 
                    this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);

            }
        }
    }


    // to browse the pan image
    void 
        ReprojectImageGUI::_handleInputBrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            _reprojectObject = _findChild("ReprojectImage");
            if(_reprojectObject != NULL)
            {
                _reprojectObject->setProperty("inputImagePath",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }

    // to browse and save output image
    void ReprojectImageGUI::_handleoutputBrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getSaveFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            _reprojectObject = _findChild("ReprojectImage");
            if(_reprojectObject != NULL)
            {
                _reprojectObject->setProperty("outputImagePath",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in writing File", "Unknown Error", true);
            }
        }

    }

    void ReprojectImageGUI::closeStereoWizard(Stereo::MainWindowGUI* stereoWiz)
    {        
        if (stereoWiz)
        {
            delete stereoWiz;
            stereoWiz = NULL;
        }
    }


    // to view the specified image    
    void ReprojectImageGUI::_viewImage(QString imagePath)
    { 
        std::string filename = imagePath.toStdString();
        if(filename != "")
        {                
            Stereo::MainWindowGUI* stereoWizard = new Stereo::MainWindowGUI(filename,Stereo::Image::NONE);
            _stereoWizard.push_back(stereoWizard);
            QObject::connect(stereoWizard,SIGNAL(applicationClosed(Stereo::MainWindowGUI*)),
                this,SLOT(closeStereoWizard(Stereo::MainWindowGUI*)), Qt::QueuedConnection);
            stereoWizard->showMaximized();
            stereoWizard->setWindowTitle(filename.c_str());
        }
    }

    void ReprojectImageGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler, APP::IGUIManager>(getGUIManager());

                if(!_addContentUIHandler.valid())
                {
                    setActive(false);
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }

    //function called when Fuse button is clicked

    void ReprojectImageGUI::_handleOkButtonClicked()
    {
        _reprojectObject = _findChild("ReprojectImage");
        if(_reprojectObject)
        {
            std::string outputName = _reprojectObject->property("outputImagePath").toString().toStdString();
            if(outputName == "")
            {
                showError("outputName", "Please enter a output image Name");
                return;
            }
            std::string inputName = _reprojectObject->property("inputImagePath").toString().toStdString();
            if(inputName == "")
            {
                showError("inputName", "Please enter  input image");
                return;
            }
            std::string projAttrType = _reprojectObject->property("projAttrType").toString().toStdString();


            std::stringstream command;
            command.str("");
            if( projAttrType == "EPSG")
            {
                QString leEPSGCodeTemp = _reprojectObject->property("leEPSGCode").toString();
                int leEPSGCode = leEPSGCodeTemp.toInt();
                if(leEPSGCode <= 0)
                {
                    showError("EPSG Code", "Please enter a valid integer greater than 0 for EPSG Code");
                    return;
                }               
                command << "gdalwarp";
                command << " -t_srs " << "EPSG:" << leEPSGCode;
                command << " \"" << inputName << "\" \"" << outputName << "\"" ;
            }
            else
            {
                command << "gdalwarp";
                command << " -t_srs " << projAttrType;
                command << " \"" << inputName << "\" \"" << outputName<< "\"" ;        
            }
            _reprojectObject->setProperty("calculating",true);
            _reprojectObject->setProperty("startButtonEnabled",false);
            std::cout << command.str().c_str()<< std::endl;
            if(system(command.str().c_str()))
            {
                emit _updateGUIStatus();
                emit showError("Filter Status", "Failed to apply filter");
                return;
            }

            emit _updateGUIStatus();
            emit showError("Filter Status", "Successfully applied filter", false);

        }



    }


    void ReprojectImageGUI::setActive(bool value)
    {
        try
        {
            _handleGUIStatus();

            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }



    // updates GUI according to the filter status
    void ReprojectImageGUI::_handleGUIStatus()
    {
        _reprojectObject = _findChild("ReprojectImage");
        if(_reprojectObject)
        {
            _reprojectObject->setProperty("calculating",false);
            _reprojectObject->setProperty("startButtonEnabled",true);
            _reprojectObject->setProperty("leEPSGCode","4326");            
        }

    }  



}// namespace SMCQt
