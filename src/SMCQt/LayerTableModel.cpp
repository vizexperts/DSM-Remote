#include <SMCQt/LayerTableModel.h>
#include <Core/ICompositeObject.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataField.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IFeatureObject.h>
#include <Core/RefPtr.h>
#include <QDateTimeEdit>
#include <iostream>

namespace SMCQt
{
    
    LayerTableModel::LayerTableModel(QObject* parent, CORE::IFeatureLayer* layer, OGRLayer* ogrlayer)
        :QAbstractTableModel(parent)
    {
        // assign layer
        _layer = layer;
        _ogrlayer = ogrlayer;

        // creation of layer name
        if(_ogrlayer)
        {
            _roleHash = QAbstractTableModel::roleNames();
            OGRFeatureDefn* featureDefn = _ogrlayer->GetLayerDefn();
            int fieldCount = featureDefn->GetFieldCount();
            for(int i=0; i<fieldCount; ++i)
            {
                OGRFieldDefn* fieldDefn = featureDefn->GetFieldDefn(i);
                const char* fieldNameChar = fieldDefn->GetNameRef();
                std::string fieleNameStr = std::string(fieldNameChar);
                _roleNamesVector.push_back(fieleNameStr);
                _roleMap[fieleNameStr] = i;
                if(fieldDefn)
                {
                    _roleHash.insert(_roleMap.find(fieleNameStr)->second, QByteArray(fieldNameChar));
                }
            }
        }
        else
        {
            _roleHash = QAbstractTableModel::roleNames();
            CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = _layer->getInterface<CORE::IMetadataTableHolder>();
            if(tableHolder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTable> table = tableHolder->getMetadataTable();
                if(table.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = table->getMetadataTableDefn();
                    if(tableDefn.valid())
                    {
                        unsigned fieldCount = tableDefn->getFieldCount();
                        for(unsigned i=1; i<=fieldCount; ++i)
                        {
                            CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = tableDefn->getFieldDefnByIndex(i);
                            std::string fieldDefnStr = fieldDefn->getName();
                            _roleNamesVector.push_back(fieldDefnStr);
                            _roleMap[fieldDefnStr] = i;
                            if(fieldDefn.valid())
                            {
                                _roleHash.insert(_roleMap.find(fieldDefnStr)->second, QByteArray(fieldDefnStr.c_str()));
                            }
                        }
                    }
                }
            }
        }
    }

    QHash<int,QByteArray> LayerTableModel::roleNames() const
    {
        return _roleHash;
    }

    int 
    LayerTableModel::rowCount(const QModelIndex &parent) const
    {
        if(parent.isValid())
        {
            return 0;
        }
        if(_ogrlayer)
        {
             return _ogrlayer->GetFeatureCount();
        }
        else
        {
            CORE::RefPtr<CORE::ICompositeObject> composite = 
                _layer->getInterface<CORE::ICompositeObject>();

            if(composite.valid())
            {
                return composite->getObjectCount();
            }
        }
        return 0;
    }

    int
    LayerTableModel::columnCount(const QModelIndex &parent) const
    {
        if(_ogrlayer)
        {
            return _ogrlayer->GetLayerDefn()->GetFieldCount();
        }
        else
        {
            CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
                _layer->getInterface<CORE::IMetadataTableHolder>();

            if(tableHolder.valid())
            {
                CORE::RefPtr<CORE::IMetadataTable> table = tableHolder->getMetadataTable();
                if(table.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = table->getMetadataTableDefn();
                    if(tableDefn.valid())
                    {
                        int fieldCount = tableDefn->getFieldCount();
                        if(fieldCount == 0)
                        {
                            return 1;
                        }
                        else
                        {
                            return fieldCount;
                        }
                    }
                }
            }
        }
        
        return 0;
    }

    QVariant
    LayerTableModel::data(const QModelIndex &index, int role) const
    {
        int row = index.row();
        int column = index.column();
        if(_ogrlayer)
        {
            _ogrlayer->ResetReading();

            OGRFeature* feature = NULL;
            for (int i = 0; i < row; i++)
            {
                feature = _ogrlayer->GetNextFeature();
                if (feature == NULL)
                {
                    return QVariant();
                }
                OGRFeature::DestroyFeature(feature);
            }
            feature = _ogrlayer->GetNextFeature();

            if(feature == NULL)
            {
                return row;
            }

            QVariant result;

            // checking the role in already generated roles
            for(unsigned i=0; i<_roleNamesVector.size(); ++i)
            {
                if(role == _roleMap.find(_roleNamesVector.at(i))->second)
                {
                    OGRFieldDefn* defn = feature->GetFieldDefnRef(role);
                    OGRFieldType type = defn->GetType();
                    switch(type)
                    {
                        case OFTInteger:
                            result.setValue<int>(feature->GetFieldAsInteger(role));
                            break;
                        case OFTReal:
                            result.setValue<double>(feature->GetFieldAsDouble(role));
                            break;
                        case OFTString:
                            result.setValue<QString>(feature->GetFieldAsString(role));
                            break;
                        case OFTDateTime:
                            {
                                int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0, tzflag = 0;
                                if(feature->GetFieldAsDateTime(role, &year, &month, &day, &hour, &min, &sec, &tzflag))
                                {
                                    QDate qDate(year, month, day);
                                    QTime qTime(hour, min, sec);

                                    QDateTime qDateTime(qDate, qTime);
                                    result.setValue<QDateTime>(qDateTime);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            OGRFeature::DestroyFeature(feature);
            return result;
        }
        else
        {
            const CORE::IFeatureLayer::FIDMap& fidMap = _layer->getFIDMap();
            CORE::IFeatureLayer::FIDMap::const_iterator iter = fidMap.begin();
            for(int i = 0; i < row; i++)
            {
                if(iter == fidMap.end())
                {
                    return QVariant();
                }
                ++iter;
            }
            if(iter == fidMap.end())
            {
                return row;
            }
            long fid = iter->first;
            CORE::RefPtr<CORE::IFeatureObject> featureObject = _layer->getFeatureByFID(fid);
            if(featureObject.valid())
            {
                CORE::RefPtr<CORE::IMetadataRecordHolder> holder = featureObject->getInterface<CORE::IMetadataRecordHolder>();
                if(holder.valid())
                {
                    CORE::RefPtr<CORE::IMetadataRecord> record = holder->getMetadataRecord();
                    if(record.valid())
                    {
                        CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByIndex(role);
                        
                        // checking the role in already generated roles
                        for(unsigned i=0; i<_roleNamesVector.size(); ++i)
                        {
                            if(role == _roleMap.find(_roleNamesVector.at(i))->second)
                            {
                                if(field.valid())
                                {
                                    CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = field->getMetadataFieldDef();
                                    if(fieldDefn.valid())
                                    {
                                        CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                                        QVariant result;
                                        switch (type)
                                        {
                                            case CORE::IMetadataFieldDefn::STRING:
                                            {
                                                result.setValue<QString>(field->toString().c_str());
                                            }
                                            break;
                                            case CORE::IMetadataFieldDefn::INTEGER:
                                            {
                                                result.setValue<int>(field->toInt());
                                            }
                                            break;
                                            case CORE::IMetadataFieldDefn::DOUBLE:
                                            {
                                                result.setValue<double>(field->toDouble());
                                            }
                                            break;
                                        }
                                        return result;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return QVariant();
    }
    
    Qt::ItemFlags
    LayerTableModel::flags(const QModelIndex &index) const
    {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }

    LayerTableModel::~LayerTableModel()
    {
    }
}
