/****************************************************************************
*
* File             : TTSGUI.h
* Description      : TTSGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#ifdef WIN32

#include <SMCQt/TTSGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <Core/WorldMaintainer.h>

#include <QFileDialog>

#include <osgDB/FileNameUtils>

#include <VizQt/QtUtils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, TTSGUI);

    TTSGUI::TTSGUI()
        :_ttsStatus(STOPPED)
    {
        setName("TTSGUI");
    }

    TTSGUI::~TTSGUI()
    {
        setActive(false);
        _handleStopButtonClicked();
    }

    void TTSGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if( popupLoader != NULL )
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, 
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void TTSGUI::connectPopupLoader(QString type)
    {
        if(type == "textToSpeechMenuObject")
        {
            //XXX AG only do setActive after connecting and getting all the QObjects
            setActive( true );
            QObject* _textToSpeechMenuObject = _findChild("textToSpeechMenuObject");
            if(_textToSpeechMenuObject)
            {
                // connectivity of signals and slots
                QObject::connect( _textToSpeechMenuObject, SIGNAL(qmlHandleBrowseButtonClicked()), 
                    this, SLOT(_handleBrowseButtonClicked()));
                QObject::connect( _textToSpeechMenuObject, SIGNAL(qmlHandlePlayButtonClicked()), this, 
                    SLOT(_handlePlayButtonClicked()));
                QObject::connect( _textToSpeechMenuObject, SIGNAL(qmlHandleStopButtonClicked()), this, 
                    SLOT(_handleStopButtonClicked()));
                QObject::connect( _textToSpeechMenuObject, SIGNAL(qmlHandleCloseButtonClicked()), this, 
                    SLOT(_handleCloseButtonClicked()));
            }
        }
    }

    void TTSGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                if(wmain)
                {
                    CORE::IComponent* component = wmain->getComponentByName("TextToSpeechComponent");
                    if(component)
                    {
                        _ttsComponent = component->getInterface<AUDIO::ITextToSpeechComponent>();
                    }
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void TTSGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        if(value)
        {
        }
        else
        {
            _reset();
        }
        DeclarativeFileGUI::setActive(value);
    }

    void TTSGUI::_handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        std::string directory = "/home";
        std::string formatFilter = "Text File (*.txt)";
        std::string caption = "Log Files";
        QString filename = QFileDialog::getOpenFileName(parent, caption.c_str(), directory.c_str(), formatFilter.c_str(), 0);
        if(!filename.isEmpty())
        {
            QObject* textToSpeechMenuObject = _findChild("textToSpeechMenuObject");
            if( textToSpeechMenuObject )
            {
                textToSpeechMenuObject->setProperty( "ttsFileLE", filename );
                try
                {
                    directory = osgDB::getFilePath(filename.toStdString());
                    getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
                }
                catch(...){}
            }
        }
    }

    void TTSGUI::_handlePlayButtonClicked()
    {
        QObject* _textToSpeechMenuObject = _findChild("textToSpeechMenuObject");
        if(_textToSpeechMenuObject)
        {

            if(_ttsStatus == STOPPED)
            {
                if(_textToSpeechMenuObject)
                {
                    QString file = _textToSpeechMenuObject->property("ttsFileLE").toString();

                    if(file.isEmpty())
                    {
                        showError("File Empty", "Please select a file to play");
                        return;
                    }
                    else
                    {
                        _textToSpeechMenuObject->setProperty("browsePBEnabled",false);
                    }
                    if(_ttsComponent.valid())
                    {
                        _ttsComponent->setTextFile(file.toStdString());

                        if(!_ttsComponent->play())
                        {
                            showError("Error Reading File", "Error in reading file");
                            return;
                        }

                        _textToSpeechMenuObject->setProperty("playPBtext","Pause");
                        _ttsStatus = PLAYING;
                        _textToSpeechMenuObject->setProperty("stopPEnabled",true);

                    }
                }
            }
            else if(_ttsStatus == PAUSED)
            {
                _ttsComponent->resume();

                if(_textToSpeechMenuObject)
                {
                    _textToSpeechMenuObject->setProperty("playPBtext","Pause");
                }

                _ttsStatus = PLAYING;
            }
            else if(_ttsStatus == PLAYING)
            {
                _ttsComponent->pause();

                if(_textToSpeechMenuObject)
                {
                    _textToSpeechMenuObject->setProperty("playPBtext","Resume");
                }

                _ttsStatus = PAUSED;
            }
        }
    }

    void TTSGUI::_handleStopButtonClicked()
    {
        QObject* _textToSpeechMenuObject = _findChild("textToSpeechMenuObject");
        if(_textToSpeechMenuObject)
        {
            _textToSpeechMenuObject->setProperty("browsePBEnabled",true);
            if(_ttsStatus != STOPPED)
            {
                _ttsComponent->stop();

                if(_textToSpeechMenuObject)
                {
                    _textToSpeechMenuObject->setProperty("stopPEnabled",false);
                    _textToSpeechMenuObject->setProperty("playPBtext","Play");
                }

                _ttsStatus = STOPPED;
            }
        }
    }

    void TTSGUI::_handleCloseButtonClicked()
    {
        _handleStopButtonClicked();

        setActive(false);
    }

    void TTSGUI::_reset()
    {
        QObject* _textToSpeechMenuObject = _findChild("textToSpeechMenuObject");

        if(_textToSpeechMenuObject)
        {
            _textToSpeechMenuObject->setProperty("ttsFileLE","");
            _textToSpeechMenuObject->setProperty("playPBtext","Play");
            _textToSpeechMenuObject->setProperty("stopPEnabled",false);
        }

    }
} 

#endif //WIn32
