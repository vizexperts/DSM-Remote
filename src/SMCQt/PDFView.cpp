#include <SMCQt/pdfview.h>
#include <poppler-qt5.h>

#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtGui/QPainter>
#include <QtCore/QFile>
#include <QtCore/QDebug>

#include <iostream>
namespace SMCQt{

    PDFView::PDFView(QQuickItem *parent) :
        QQuickPaintedItem(parent),
        _source(""),
        _currentPage(0),
        _zoom(1.0),
        posX(0),
        posY(0),
        phX(QApplication::desktop()->physicalDpiX()),
        phY(QApplication::desktop()->physicalDpiY()),
        _numPages(0)
    {
        connect(this, SIGNAL(currentPageChanged()), this, SLOT(loadPage()), Qt::UniqueConnection);
        connect(this, SIGNAL(zoomChanged()), this, SLOT(loadPage()), Qt::UniqueConnection);

        setRenderTarget(QQuickPaintedItem::FramebufferObject);

        setFlag(ItemHasContents);
        std::cout << "Dump";
    }

    PDFView::~PDFView()
    {
    }

    QString PDFView::source() const
    {
        return _source;
    }

    int PDFView::currentPage() const
    {
        return _currentPage;
    }

    qreal PDFView::zoom() const
    {
        return _zoom;
    }

    int PDFView::numPages() const
    {
        return _numPages;
    }

    QSize PDFView::pageSize() const
    {
        return _pageSize;
    }

    void PDFView::setSource(const QString &source)
    {
        if ((!source.isEmpty()) && (source != _source))
        {
            _source = source;
            _currentPage = 0;
            loadPDF();
            emit sourceChanged();
        }
        else {
            qDebug() << "Enter a valid url.";
            return;
        }
    }

    void PDFView::setCurrentPage(int currPage)
    {
        if ((currPage > (numPages() - 1)) || (currPage < 0))
        {
            return;
        }

        if (currPage != _currentPage)
        {
            _currentPage = currPage;
            emit currentPageChanged();
        }
    }

    void PDFView::setZoom(qreal zoom)
    {
        if (_zoom != zoom){
            _zoom = zoom;
            emit zoomChanged();
        }
    }

    void PDFView::loadPDF()
    {
        if (!QFile::exists(_source)){
            //qDebug() << "Not Found File:" << _source;
            return;
        }

        docPDF = Poppler::Document::load(_source, "", "");
        if (docPDF)
        {
            if (docPDF->isLocked() || docPDF->isEncrypted())
            {
                //qDebug() << " Encrypted PDF not supported ";
                emit encryptedPDFError();
                return;
            }

            _numPages = docPDF->numPages();
            loadPage();
        }

        else
        {
            //qDebug() << " Corrupted PDF , Please give path of a valid PDF ";
            emit corruptedPDFError();
            return;
        }

    }

    void PDFView::updatePosition()
    {
        if (!_pageSize.isValid()){
            qDebug() << "Parent Size Not Valid.";
            return;
        }
        int parentWidth = width();
        int parentHeight = height();

        if (_pageSize.width() < parentWidth){
            posX = (parentWidth - _pageSize.width()) / 2;
        }
        else {
            posX = 0;
        }

        if (_pageSize.height() < parentHeight){
            posY = (parentHeight - _pageSize.height()) / 2;
        }
        else {
            posY = 0;
        }
    }

    void PDFView::saveToFile(const QUrl &path)
    {
        qDebug() << "Save To File!";
        page.save(path.toString(), "JPG");
    }

    void PDFView::loadPage()
    {
        int resX = _zoom*phX;
        int resY = _zoom*phY;

        Poppler::Page* p = docPDF->page(_currentPage);
        if (p)
        {
            page = p->renderToImage(resX, resY, 1, 1);
            if (page.isNull())
            {
                //qDebug() << "Unable to render to image.";
            }
        }

        _pageSize = page.size();

        update();
    }

    void PDFView::paint(QPainter * painter)
    {
        painter->setClipRect(0, 0, width(), height());

        updatePosition();

        int wid = width();
        int hig = height();

        if (!page.isNull())
        {
            QImage pageToDraw = page.scaled(width(), height(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

            if (!pageToDraw.isNull())
            {
                painter->drawImage(QPoint(posX, posY), pageToDraw, childrenRect().toRect());
            }
        }
    }

}