#include <SMCQt/TimelineMark.h>

namespace SMCQt
{
    TimelineMovementMark::TimelineMovementMark(QObject* parent)
    {

    }

    TimelineMovementMark::TimelineMovementMark(double posX, QColor profileColor, double deltaX, double distance, QObject *parent)
        :QObject(parent)
        , m_posX(posX)
        , m_profileColor(profileColor)
        , m_deltaX(deltaX)
        ,m_distance(distance)
    {

    }

    double TimelineMovementMark::posX() const
    {
        return m_posX;
    }

    void TimelineMovementMark::setPosX(double posX)
    {
        if(posX != m_posX)
        {
            m_posX = posX;
            emit posXChanged();
        }
    }

    QColor TimelineMovementMark::profileColor() const
    {
        return m_profileColor;
    }

    void TimelineMovementMark::setProfileColor(QColor profileColor)
    {
        if(profileColor != m_profileColor)
        {
            m_profileColor = profileColor;
            emit profileColorChanged();
        }
    }

    double TimelineMovementMark::deltaX() const
    {
        return m_deltaX;
    }

    void TimelineMovementMark::setDeltaX(double deltaX)
    {
        if(deltaX != m_deltaX)
        {
            m_deltaX = deltaX;
            emit deltaXChanged();
        }
    }
    double TimelineMovementMark::distance() const
    {
        return m_distance;
    }

    void TimelineMovementMark::setDistance(double distance)
    {
        if(distance != m_distance)
        {
            m_distance = distance;
            emit distanceChanged();
        }
    }

    TimelineVisibilityMark::TimelineVisibilityMark(QObject* parent)
    {

    }

    TimelineVisibilityMark::TimelineVisibilityMark(double posX, QColor profileColor, double deltaX, bool visibility, QObject *parent)
        :QObject(parent)
        , m_posX(posX)
        , m_profileColor(profileColor)
        , m_deltaX(deltaX)
        ,m_visibility(visibility)
    {

    }

    double TimelineVisibilityMark::posX() const
    {
        return m_posX;
    }

    void TimelineVisibilityMark::setPosX(double posX)
    {
        if(posX != m_posX)
        {
            m_posX = posX;
            emit posXChanged();
        }
    }

    QColor TimelineVisibilityMark::profileColor() const
    {
        return m_profileColor;
    }

    void TimelineVisibilityMark::setProfileColor(QColor profileColor)
    {
        if(profileColor != m_profileColor)
        {
            m_profileColor = profileColor;
            emit profileColorChanged();
        }
    }

    double TimelineVisibilityMark::deltaX() const
    {
        return m_deltaX;
    }

    void TimelineVisibilityMark::setDeltaX(double deltaX)
    {
        if(deltaX != m_deltaX)
        {
            m_deltaX = deltaX;
            emit deltaXChanged();
        }
    }

    bool TimelineVisibilityMark::visibility() const
    {
        return m_visibility;
    }

    void TimelineVisibilityMark::setVisibility(bool visibility)
    {
        if(visibility != m_visibility)
        {
            m_visibility = visibility;
            emit visibilityChanged();
        }
    }
}
