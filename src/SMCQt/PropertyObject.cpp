#include <SMCQt/PropertyObject.h>

namespace SMCQt{

    PropertyObject::PropertyObject(QObject* parent) : QObject(parent)
    {

    }

    PropertyObject::PropertyObject(const QString &name, const QString &type, const QString &value, int precesion , QObject *parent)
        : QObject(parent), m_name(name), m_type(type), m_value(value), m_precesion(precesion)
    {
    }

    QString PropertyObject::name() const
    {
        return m_name;
    }

    void PropertyObject::setName(const QString &name)
    {
        if(name != m_name)
        {
            m_name = name;
            emit nameChanged();
        }
    }

    void PropertyObject::setPrecesion(int precesion)
    {
        if(precesion != m_precesion)
        {
            m_precesion = precesion;
            emit precesionChanged();
        }
    }

    int PropertyObject::precesion() const
    {
        return m_precesion;
    }

    QString PropertyObject::type() const
    {
        return m_type;
    }
    void PropertyObject::setType(const QString &type)
    {
        if(type != m_type)
        {
            m_type = type;
            emit typeChanged();
        }
    }

    QString PropertyObject::value() const
    {
        return m_value;
    }

    void PropertyObject::setValue(const QString& value)
    {
        if(value != m_value)
        {
            m_value = value;
            emit valueChanged();
        }
    }
}
