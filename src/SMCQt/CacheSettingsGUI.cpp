#include <SMCQt/CacheSettingsGUI.h>
#include <App/AccessElementUtils.h>
#include <DGN/IDGNFolder.h>
#include <Util/FileUtils.h>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CacheSettingsGUI);
    DEFINE_IREFERENCED(CacheSettingsGUI, DeclarativeFileGUI);

    CacheSettingsGUI::CacheSettingsGUI()
    {
    }

    CacheSettingsGUI::~CacheSettingsGUI()
    {
    }

    void CacheSettingsGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if (smpLeftMenu != NULL)
        {
            QObject::connect(smpLeftMenu, SIGNAL(connectCacheSettingsMenu()), this,
                SLOT(connectCacheSettingsMenu()), Qt::UniqueConnection);
        }
    }

    void CacheSettingsGUI::connectCacheSettingsMenu()
    {
        _cacheSettingsObject = _findChild("cacheSettings");
        if (_cacheSettingsObject != NULL)
        {
            // connect to signals
            QObject::connect(_cacheSettingsObject, SIGNAL(clearDGNCache()),
                this, SLOT(handleClearDGNCache()), Qt::UniqueConnection);
        }
    }
    
    void CacheSettingsGUI::handleClearDGNCache()
    {
        //! Ask for user confirmation
        QString msg = "Are you sure you want to clear cache ?";
        emit criticalQuestion("Clear Cache", msg, false, "clearCache()", "");
    }

    void CacheSettingsGUI::clearCache()
    {
        std::set<std::string> loadedDGNs;
        if (_cacheSettingsObject != NULL)
        {
            //create a set of DGNFolder objects that are loaded in world 

            CORE::IWorld *world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
            if (world)
            {
                const CORE::ObjectMap& objects = world->getObjectMap();
                CORE::ObjectMap::const_iterator iter = objects.begin();

                for (; iter != objects.end(); ++iter)
                {
                    DGN::IDGNFolder* dgnFolderObject = iter->second->getInterface<DGN::IDGNFolder>();
                    if (dgnFolderObject != NULL)
                    {
                        loadedDGNs.insert(dgnFolderObject->getInterface<CORE::IBase>()->getName());
                    }
                }
            }

            std::string dgnCacheRepo = UTIL::getDGNCacheRepo();
            fs::path root(dgnCacheRepo);
            fs::directory_iterator it(root);
            fs::directory_iterator endit;
            
            //iterate over all directories in the repo
            
            for (; it != endit; ++it)
            {
                if (fs::is_directory(*it))
                {
                    //Remove the directories that are not loaded in the world
            
                    std::string dgnName = it->path().stem().string();
                    if (loadedDGNs.find(dgnName) == loadedDGNs.end())
                    {
                        try
                        {
                            fs::remove_all(it->path());
                        }
                        catch (...)
                        {
                            LOG_ERROR("Can't Delete , File or Directory is in USE!");
                        }
                    }
                }
            }
            _cacheSettingsObject->setProperty("dgnCacheCBchecked", QVariant::fromValue(false));
        }
    }

} // namespace SMCQt
