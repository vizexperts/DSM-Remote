/****************************************************************************
*
* File             : UndoRedoGUI.h
* Description      : UndoRedoGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/UndoRedoGUI.h>

#include <App/AccessElementUtils.h>

#include <VizUI/ICameraUIHandler.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, UndoRedoGUI);
    DEFINE_IREFERENCED(UndoRedoGUI, DeclarativeFileGUI);

    UndoRedoGUI::UndoRedoGUI()
    {
    }

    UndoRedoGUI::~UndoRedoGUI()
    {
    }

    void UndoRedoGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpFileMenu = _findChild("smpFileMenu");
        if(smpFileMenu != NULL)
        {
            QObject::connect(smpFileMenu, SIGNAL(undo()), this, SLOT(undo()), Qt::UniqueConnection);
            QObject::connect(smpFileMenu, SIGNAL(redo()), this, SLOT(redo()), Qt::UniqueConnection);
        }
    }

    void UndoRedoGUI::undo()
    {
        _uihandlerManager->undoTransaction();
    }

    void UndoRedoGUI::redo()
    {
        _uihandlerManager->redoTransaction();
    }


    void UndoRedoGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                CORE::RefPtr<VizUI::ICameraUIHandler> cui = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager());
                if(cui.valid())
                {
                    _uihandlerManager = 
                        cui->getInterface<APP::IUIHandler>()->getManager();
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void UndoRedoGUI::initializeAttributes()
    {
        if(_uihandlerManager.valid())
        {
            _undoStackSize = _uihandlerManager->getUndoStackLimit();
        }

        DeclarativeFileGUI::initializeAttributes();
    }

} // namespace SMCQt

