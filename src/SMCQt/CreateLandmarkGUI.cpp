/****************************************************************************
*
* File             : CreateLandmarkGUI.h
* Description      : CreateLandmarkGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/CreateLandmarkGUI.h>
#include <SMCQt/PropertyObject.h>
#include <Elements/IClippable.h>

#include <App/Application.h>
#include <App/IUIHandler.h>
#include <App/AccessElementUtils.h>

#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IDeletable.h>
#include <Core/IBase.h>

#include <VizUI/IDeletionUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <Elements/IModelFeatureObject.h>
#include <Elements/IRepresentation.h>
#include <Elements/IModelHolder.h>
#include <Elements/IModel.h>
#include <Elements/ElementsUtils.h>

#include <SMCElements/ILayerComponent.h>

#include <Util/FileUtils.h>
#include <Util/DownloadModelUtil.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>

#include <QFileInfo>

#include <QFileDialog>

#include <SMCQt/VizComboBoxElement.h>

#include <serialization/AsciiStreamOperator.h>
#include <serialization/IProjectLoaderComponent.h>

#include <GIS/ITable.h>
#include <GIS/IFieldDefinition.h>
#include <GIS/IStringFieldDefinition.h>
#include <GIS/IDoubleFieldDefinition.h>
#include <GIS/FieldDefinitionType.h>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <string>
#include <Curl/EasyCurl.h>
#include <ZipUtil/ZipAndUnzipUtil.h>

namespace SMCQt
{

    DEFINE_META_BASE(SMCQt, CreateLandmarkGUI);
    DEFINE_IREFERENCED(CreateLandmarkGUI, DeclarativeFileGUI);

    std::string CreateLandmarkGUI::LANDMARK_APPDATA_DIR = "/data/LandmarkSymbols/";
    const std::string CreateLandmarkGUI::LandmarkContextualObjectName = "landmarkContextual";
    const std::string CreateLandmarkGUI::MouseButtonClickedPropertyName = "mouseButtonClicked";
    const std::string CreateLandmarkGUI::XPlaneMinExtent = "xPlaneMinExtent";
    const std::string CreateLandmarkGUI::XPlaneMaxExtent = "xPlaneMaxExtent";
    const std::string CreateLandmarkGUI::XPlaneMinExtentOpposite = "xPlaneMinExtentOpposite";
    const std::string CreateLandmarkGUI::XPlaneMaxExtentOpposite = "xPlaneMaxExtentOpposite";
    const std::string CreateLandmarkGUI::YPlaneMinExtent = "yPlaneMinExtent";
    const std::string CreateLandmarkGUI::YPlaneMaxExtent = "yPlaneMaxExtent";
    const std::string CreateLandmarkGUI::YPlaneMinExtentOpposite = "yPlaneMinExtentOpposite";
    const std::string CreateLandmarkGUI::YPlaneMaxExtentOpposite = "yPlaneMaxExtentOpposite";
    const std::string CreateLandmarkGUI::ZPlaneMinExtent = "zPlaneMinExtent";
    const std::string CreateLandmarkGUI::ZPlaneMaxExtent = "zPlaneMaxExtent";
    const std::string CreateLandmarkGUI::ZPlaneMinExtentOpposite = "zPlaneMinExtentOpposite";
    const std::string CreateLandmarkGUI::ZPlaneMaxExtentOpposite = "zPlaneMaxExtentOpposite";

    const std::string CreateLandmarkGUI::XPlaneSpinBoxMin = "xPlaneSpinBoxMinValue";
    const std::string CreateLandmarkGUI::XPlaneSpinBoxMax = "xPlaneSpinBoxMaxValue";
    const std::string CreateLandmarkGUI::XPlaneSpinBoxMinOpposite = "xPlaneSpinBoxMinValueOpposite";
    const std::string CreateLandmarkGUI::XPlaneSpinBoxMaxOpposite = "xPlaneSpinBoxMaxValueOpposite";
    const std::string CreateLandmarkGUI::YPlaneSpinBoxMin = "yPlaneSpinBoxMinValue";
    const std::string CreateLandmarkGUI::YPlaneSpinBoxMax = "yPlaneSpinBoxMaxValue";
    const std::string CreateLandmarkGUI::YPlaneSpinBoxMinOpposite = "yPlaneSpinBoxMinValueOpposite";
    const std::string CreateLandmarkGUI::YPlaneSpinBoxMaxOpposite = "yPlaneSpinBoxMaxValueOpposite";
    const std::string CreateLandmarkGUI::ZPlaneSpinBoxMin = "zPlaneSpinBoxMinValue";
    const std::string CreateLandmarkGUI::ZPlaneSpinBoxMax = "zPlaneSpinBoxMaxValue";
    const std::string CreateLandmarkGUI::ZPlaneSpinBoxMinOpposite = "zPlaneSpinBoxMinValueOpposite";
    const std::string CreateLandmarkGUI::ZPlaneSpinBoxMaxOpposite = "zPlaneSpinBoxMaxValueOpposite";

    const std::string CreateLandmarkGUI::XPlaneSpinBoxDistance = "xPlaneSpinBoxDistance";
    const std::string CreateLandmarkGUI::YPlaneSpinBoxDistance = "yPlaneSpinBoxDistance";
    const std::string CreateLandmarkGUI::ZPlaneSpinBoxDistance = "zPlaneSpinBoxDistance";
    const std::string CreateLandmarkGUI::XPlaneSpinBoxDistanceOpposite = "xPlaneSpinBoxDistanceOpposite";
    const std::string CreateLandmarkGUI::YPlaneSpinBoxDistanceOpposite = "yPlaneSpinBoxDistanceOpposite";
    const std::string CreateLandmarkGUI::ZPlaneSpinBoxDistanceOpposite = "zPlaneSpinBoxDistanceOpposite";

    const std::string CreateLandmarkGUI::XPlaneDistance = "xPlaneDistance";
    const std::string CreateLandmarkGUI::YPlaneDistance = "yPlaneDistance";
    const std::string CreateLandmarkGUI::ZPlaneDistance = "zPlaneDistance";
    const std::string CreateLandmarkGUI::XPlaneDistanceOpposite = "xPlaneDistanceOpposite";
    const std::string CreateLandmarkGUI::YPlaneDistanceOpposite = "yPlaneDistanceOpposite";
    const std::string CreateLandmarkGUI::ZPlaneDistanceOpposite = "zPlaneDistanceOpposite";

    const std::string CreateLandmarkGUI::OpacitySliderValue = "opacityValue";
    const std::string CreateLandmarkGUI::OpacitySpinBoxValue = "opacitySpinBoxValue";

    const std::string CreateLandmarkGUI::SERVER_URL = "/georbis/VMCC/Model3D/list";
    const std::string CreateLandmarkGUI::GeorbisServerlistDir = "/GeorbISServerList";

    const std::string CreateLandmarkGUI::MODEL_DOWNLOAD_URL = "/georbis/images/Model3D/";

    CreateLandmarkGUI::CreateLandmarkGUI()
        : _addToDefault(true)
        , _selectedFeatureLayer(NULL)
        , _reloadLandmarkList(false)
        , _modelSelectionPaneOpenedFromContextualMenu(false)
        , _crossSection(false)
        , _crossSectionPersistant(false)
        , _xClip1(false)
        , _xClip2(false)
        , _yClip1(false)
        , _yClip2(false)
        , _zClip1(false)
        , _zClip2(false)
        , _xPlane1(0.0)
        , _xPlane2(0.0)
        , _yPlane1(0.0)
        , _yPlane2(0.0)
        , _zPlane1(0.0)
        , _zPlane2(0.0)
        , _bb(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)
        , _opacity(0.3)
        , _removeBoundary(false)
    {
        _qmlDirectoryTreeModel = NULL;
    }

    CreateLandmarkGUI::~CreateLandmarkGUI()
    {
    }

    void CreateLandmarkGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if (smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSMPMenu(QString)),
                Qt::UniqueConnection);
        }

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if (popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, SLOT(popupLoaded(QString)),
                Qt::UniqueConnection);
        }
    }

    void CreateLandmarkGUI::_parseResponse(std::string serverName, std::string response){

        QString list = QString::fromLocal8Bit(response.c_str());

        // correct JSON received from server
        list.remove('\\');
        list.replace("\"{", "{");
        list.replace("}\"", "}");
        std::cout << list.toStdString();

        QJsonDocument jdoc = QJsonDocument::fromJson(list.toUtf8());

        QFileInfo fileInfo(QString::fromStdString(serverName));
        QMLDirectoryTreeModelItem *rootItem = new QMLDirectoryTreeModelItem(fileInfo);
        rootItem->setCheckable(false);

        std::map<std::string, std::string> nameUIDMap;

        if (jdoc.isNull())
        {
            LOG_ERROR("Invalid JSON...");
            return;
        }

        if (!jdoc.isObject())
        {
            LOG_ERROR("Document is not a object,..");
            return;
        }

        QJsonObject responseObj = jdoc.object();

        bool status = responseObj["status"].toBool();

        if (!status){
            LOG_ERROR("Server Error", "Could not the list of models in server");
            return;
        }

        //! get result
        QJsonValueRef dataRef = responseObj["data"];
        QJsonValueRef applicationList((dataRef.toObject())["applications"]);
        if (applicationList.isArray())
        {
            QJsonArray results = applicationList.toArray();
            foreach(const QJsonValue& value, results)
            {
                QJsonObject item = value.toObject();
                std::string uID = item["id"].toString().toStdString();
                QString name = item["name"].toString();
                name.remove(".json");
                /*if (!name.contains("FBX")){
                    continue;
                    }*/
                nameUIDMap[name.toStdString()] = uID;

                QFileInfo tempFileInfo(name);
                QMLDirectoryTreeModelItem* nameItem = new QMLDirectoryTreeModelItem(tempFileInfo);
                nameItem->setCheckable(false);
                rootItem->addItem(nameItem);
            }

            _uidNameMap[serverName] = nameUIDMap;
            _serverModelsItem->addItem(rootItem);
        }
    }

    void CreateLandmarkGUI::connectContextualMenu()
    {
        _mode = MODE_CONTEXTUALMENU;
        setActive(true);

        _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_EDIT);

        _model = _landmarkUIHandler->getModelFeature();

        _modelSelectionPaneOpenedFromContextualMenu = false;

        QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
        if (landmarkContextual != NULL)
        {

            QVariant mouseButtonClicked = landmarkContextual->property(MouseButtonClickedPropertyName.c_str());
            _mouseClickRight = (mouseButtonClicked.toInt() == Qt::RightButton);

            if (_mouseClickRight)
                _populateContextualMenu();

            QObject::connect(landmarkContextual, SIGNAL(rename(QString)),
                this, SLOT(changeName(QString)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(handleLatLongAltScaleChanged(QString)),
                this, SLOT(handleLatLongAltScaleChanged(QString)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(snapToGround()), this,
                SLOT(snapToGround()), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(reorient()), this,
                SLOT(reorient()), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(deleteLandmark()), this,
                SLOT(deleteLandmark()), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(openModelSelectionPane(bool)), this,
                SLOT(openModelSelectionPane(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setEnableDragger(bool)), this,
                SLOT(setEnableDragger(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(crossSection()),
                this, SLOT(toggleCrossSection()), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(closeClippingFromGUI()),
                this, SLOT(closeClippingFromGUI()), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(toggleXPlane(bool)),
                this, SLOT(toggleXPlane1(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(toggleXPlaneOpposite(bool)),
                this, SLOT(toggleXPlane2(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setFrontBackPlaneDistance(double)),
                this, SLOT(setXPlane(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setFrontBackPlaneDistanceOpposite(double)),
                this, SLOT(setXPlaneOpposite(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(toggleYPlane(bool)),
                this, SLOT(toggleYPlane1(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(toggleYPlaneOpposite(bool)),
                this, SLOT(toggleYPlane2(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setLeftRightPlaneDistance(double)),
                this, SLOT(setYPlane(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setLeftRightPlaneDistanceOpposite(double)),
                this, SLOT(setYPlaneOpposite(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(toggleZPlane(bool)),
                this, SLOT(toggleZPlane1(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(toggleZPlaneOpposite(bool)),
                this, SLOT(toggleZPlane2(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setTopBottomPlaneDistance(double)),
                this, SLOT(setZPlane(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setTopBottomPlaneDistanceOpposite(double)),
                this, SLOT(setZPlaneOpposite(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setOpacity(double)),
                this, SLOT(setOpacity(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(setOpacitySpinBox(double)),
                this, SLOT(setOpacity(double)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(toggleQuadsboundary(bool)),
                this, SLOT(toggleQuadsBoundary(bool)), Qt::UniqueConnection);

            QObject::connect(landmarkContextual, SIGNAL(togglePersistanceCheck(bool)),
                this, SLOT(togglePersistanceCheck(bool)), Qt::UniqueConnection);

            showLandmarkAttributes();

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if (selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(false);
            }
        }
    }

    void CreateLandmarkGUI::handleClippingGUI()
    {
        if (_crossSectionPersistant)
        {
            if (_isModelValid())
            {

                CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
                if (modelClip)
                {
                    QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
                    if (landmarkContextual != NULL)
                    {
                        if (modelClip->getClippingEnabled())
                        {
                            landmarkContextual->setProperty("xPlaneEnabled", _xClip1);
                            landmarkContextual->setProperty("yPlaneEnabled", _yClip1);
                            landmarkContextual->setProperty("zPlaneEnabled", _zClip1);
                            landmarkContextual->setProperty("xPlaneOppositeEnabled", _xClip2);
                            landmarkContextual->setProperty("yPlaneOppositeEnabled", _yClip2);
                            landmarkContextual->setProperty("zPlaneOppositeEnabled", _zClip2);
                            landmarkContextual->setProperty("clippingPersistantEnabled", true);
                            landmarkContextual->setProperty("persistentEnabledOnOtherModel", false);
                        }
                        else
                        {
                            landmarkContextual->setProperty("persistentEnabledOnOtherModel", true);
                        }
                    }

                }
            }

        }

    }

    void CreateLandmarkGUI::disconnectContextualMenu()
    {
        QFileInfo emptyFileInfo;
        _currentSelectedFile = emptyFileInfo;

        _modelSelectionPaneOpenedFromContextualMenu = false;

        setActive(false);
        _mode = MODE_NONE;
        _setContextProperty("landmarkListModel", QVariant::fromValue(NULL));
        _setContextProperty("landmarkCategoryList", QVariant::fromValue(NULL));
        _draggerUIHandler2->getInterface<APP::IUIHandler>()->setFocus(false);
        _unsubscribe(_draggerUIHandler2.get(), *SMCUI::IDraggerUIHandler::DraggerUpdatedMessageType);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(true);
            selectionUIHandler->clearCurrentSelection();
        }

        _model = NULL;
    }

    void CreateLandmarkGUI::showLandmarkAttributes()
    {
        if (!_mouseClickRight)
        {
            QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
            if (landmarkContextual != NULL)
            {
                CORE::RefPtr<VizUI::ISelectionUIHandler> _selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
                const CORE::ISelectionComponent::SelectionMap& selectionMap = _selectionUIHandler->getCurrentSelection();

                if (selectionMap.empty())
                {
                    return;
                }

                CORE::ISelectable* selectable = selectionMap.begin()->second.get();
                if (selectable)
                {
                    _selectedFeature = selectable->getInterface<GIS::IFeature>();
                }
            }
        }

        if (_selectedFeature.valid())
        {
            GIS::ITable* table = _selectedFeature->getTable();
            const GIS::ITable::IFieldDefinitionList& definitionList = table->getFieldDefinitionList();

            int tableColumnCount = definitionList.size();

            _attributeList.clear();

            bool isDescriptionPresent = false;
            QString description;

            for (int i = 0; i < tableColumnCount; i++)
            {
                const GIS::Field* field = _selectedFeature->getField(i);
                if (field)
                {
                    CORE::RefPtr<GIS::IFieldDefinition> fieldDefn = definitionList[i].get();
                    if (fieldDefn.valid())
                    {
                        const GIS::FieldDefinitionType& type = fieldDefn->getFieldDefinitionType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName = fieldName.trimmed();

                        if (fieldName.contains("Type", Qt::CaseInsensitive) || fieldName.contains("Orientation", Qt::CaseInsensitive) ||
                            fieldName.contains("Scale", Qt::CaseInsensitive) || fieldName.contains("Representation", Qt::CaseInsensitive) ||
                            fieldName.contains("ModelFileName", Qt::CaseInsensitive) || fieldName.contains("NAME", Qt::CaseInsensitive))
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecision = 1;

                        if (type == GIS::StringFieldDefinitionType)
                        {
                            fieldType = "String";
                            fieldValue = QString::fromStdString(field->getAsString());
                            fieldValue = fieldValue.simplified();
                            fieldPrecision = fieldDefn->getInterface<GIS::IStringFieldDefinition>()->getLength();
                        }
                        else if (type == GIS::IntegerFieldDefinitionType)
                        {
                            fieldType = "Integer";
                            fieldValue = QString::number(field->getAsInt32());
                        }
                        else if (type == GIS::DoubleFieldDefinitionType)
                        {
                            fieldType = "Double";
                            fieldValue = QString::number(field->getAsReal64());
                            fieldPrecision = fieldDefn->getInterface<GIS::IDoubleFieldDefinition>()->getPrecision();
                        }

                        if (!fieldValue.isEmpty())
                        {
                            _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecision));
                        }
                    }
                }
            }

            if (_attributeList.empty() && !_mouseClickRight)
            {
                QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
                if (landmarkContextual)
                {
                    landmarkContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("landmarkAttributes");
            if (showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                if (isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this,
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);

            }
        }
        else
        {

            CORE::RefPtr<CORE::IMetadataRecord> tableRecord =
                _landmarkUIHandler->getMetadataRecord();

            if (!tableRecord.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
            if (!tableDefn.valid())
                return;

            int tableColumnCount = tableDefn->getFieldCount();

            _attributeList.clear();




            if (!_mouseClickRight)
            {
                CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
                if (!model.valid())
                {
                    return;
                }

                QString name = QString::fromStdString(model->getInterface<CORE::IBase>()->getName());

                osg::Vec3 position = model->getInterface<CORE::IPoint>()->getValue();
                double altitudeD = ceil(position.z() - ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y()));
                if (altitudeD < 0)
                {
                    altitudeD = 0.0;
                }
                QString latitude = QString::number(position.y(), 'f', 6);
                QString longitude = QString::number(position.x(), 'f', 6);
                QString altitude = QString::number(altitudeD, 'f', 6);

                osg::Vec3 modelScale = model->getInterface<ELEMENTS::IModelHolder>()->getModelScale();
                QString scale = QVariant(modelScale.x()).toString();

                _attributeList.append(new PropertyObject("name", "String", name, name.length()));
                _attributeList.append(new PropertyObject("latitude", "String", latitude, 1));
                _attributeList.append(new PropertyObject("longitude", "String", longitude, 1));
                _attributeList.append(new PropertyObject("altitude", "String", altitude, 1));
                _attributeList.append(new PropertyObject("scale", "String", scale, 1));
            }

            bool isDescriptionPresent = false;
            QString description;
            for (int i = 0; i < tableColumnCount; i++)
            {
                CORE::RefPtr<CORE::IMetadataField> field = tableRecord->getFieldByIndex(i);
                if (field)
                {
                    CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = field->getMetadataFieldDef();
                    if (fieldDefn.valid())
                    {
                        CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName = fieldName.trimmed();

                        if (fieldName.contains("Type", Qt::CaseInsensitive) || fieldName.contains("Orientation", Qt::CaseInsensitive) ||
                            fieldName.contains("Scale", Qt::CaseInsensitive) || fieldName.contains("Representation", Qt::CaseInsensitive) ||
                            fieldName.contains("ModelFileName", Qt::CaseInsensitive) || fieldName.contains("NAME", Qt::CaseInsensitive))
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecesion = 1;

                        switch (type)
                        {
                        case CORE::IMetadataFieldDefn::STRING:
                        {
                            fieldType = "String";
                            fieldValue = QString::fromStdString(field->toString());
                            fieldPrecesion = fieldDefn->getLength();
                        }
                        break;
                        case CORE::IMetadataFieldDefn::INTEGER:
                        {
                            fieldType = "Integer";
                            fieldValue = QString::number(field->toInt());
                        }
                        break;
                        case CORE::IMetadataFieldDefn::DOUBLE:
                        {
                            fieldType = "Double";
                            fieldValue = QString::number(field->toDouble());
                            fieldPrecesion = fieldDefn->getPrecision();
                        }
                        break;
                        }

                        _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecesion));
                    }
                }
            }

            if (_attributeList.empty())
            {
                QObject* landmakrContextual = _findChild(LandmarkContextualObjectName);
                if (landmakrContextual)
                {
                    landmakrContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("landmarkAttributes");
            if (showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                if (isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this,
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);

            }
        }
    }

    void CreateLandmarkGUI::_populateContextualMenu()
    {
        QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
        if (landmarkContextual != NULL)
        {
            // get the selected model
            CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
            if (!model.valid())
            {
                return;
            }

            QString name = QString::fromStdString(model->getInterface<CORE::IBase>()->getName());

            osg::Vec3 position = model->getInterface<CORE::IPoint>()->getValue();
            double altitudeD = ceil(position.z() - ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y()));
            if (altitudeD < 0)
            {
                altitudeD = 0.0;
            }
            QString latitude = QString::number(position.y(), 'f', 6);
            QString longitude = QString::number(position.x(), 'f', 6);
            QString altitude = QString::number(altitudeD, 'f', 6);

            osg::Vec3 modelScale = model->getInterface<ELEMENTS::IModelHolder>()->getModelScale();
            QString scale = QVariant(modelScale.x()).toString();

            landmarkContextual->setProperty("name", QVariant::fromValue(name));
            landmarkContextual->setProperty("latitude", QVariant::fromValue(latitude));
            landmarkContextual->setProperty("longitude", QVariant::fromValue(longitude));
            landmarkContextual->setProperty("altitude", QVariant::fromValue(altitude));
            landmarkContextual->setProperty("scale", QVariant::fromValue(scale));
            landmarkContextual->setProperty("valueChanged", QVariant::fromValue(false));

        }
    }

    void CreateLandmarkGUI::setEnableDragger(bool value)
    {
        if (!value)
        {
            _draggerUIHandler2->getInterface<APP::IUIHandler>()->setFocus(false);
            _unsubscribe(_draggerUIHandler2.get(), *SMCUI::IDraggerUIHandler::DraggerUpdatedMessageType);
        }
        else
        {
            _draggerUIHandler2->getInterface<APP::IUIHandler>()->setFocus(true);
            _subscribe(_draggerUIHandler2.get(), *SMCUI::IDraggerUIHandler::DraggerUpdatedMessageType);
            _updateDragger();
        }
    }

    // Connections for Landmarks Menu
    void CreateLandmarkGUI::connectSMPMenu(QString type)
    {
        // Placing new landmarks
        if (type == "smpLandmarksMenu")
        {
            initializeList();
            QObject* smpLandmarksMenu = _findChild("smpLandmarksMenu");
            if (smpLandmarksMenu != NULL)
            {
                QObject::connect(smpLandmarksMenu, SIGNAL(filter(QString)), this,
                    SLOT(search(QString)), Qt::UniqueConnection);

                QObject* landmarkDirTreeView = _findChild("landmarkDirTreeView");
                if (landmarkDirTreeView != NULL)
                {
                    if (_modelSelectionPaneOpenedFromContextualMenu)
                    {
                        QObject::connect(landmarkDirTreeView, SIGNAL(clicked(int)), this,
                            SLOT(setModel(int)), Qt::UniqueConnection);
                    }
                    else
                    {
                        QObject::connect(landmarkDirTreeView, SIGNAL(clicked(int)), this,
                            SLOT(modelSelected(int)), Qt::UniqueConnection);
                    }
                }

                QObject::connect(smpLandmarksMenu, SIGNAL(menuDestroyed()), this,
                    SLOT(menuDestroyed()), Qt::UniqueConnection);
            }

        }
        else
        {
            if (_modelSelectionPaneOpenedFromContextualMenu)
            {
                //! Find contextual menu and unselect the button
                QObject* setModelButton = _findChild("setModelButton");
                if (setModelButton)
                {
                    setModelButton->setProperty("checked", QVariant::fromValue(false));
                }

                _modelSelectionPaneOpenedFromContextualMenu = false;
            }

            setActive(false);
            _setContextProperty("landmarkListModel", QVariant::fromValue(NULL));
            _setContextProperty("landmarkCategoryList", QVariant::fromValue(NULL));
        }
    }

    //! Called when connected Menu gas been destroyed
    void CreateLandmarkGUI::menuDestroyed()
    {
        QFileInfo emptyFileInfo;
        _currentSelectedFile = emptyFileInfo;
        _setContextProperty("landmarkListModel", QVariant::fromValue(NULL));
        _setContextProperty("landmarkCategoryList", QVariant::fromValue(NULL));
        setActive(false);
    }

    void CreateLandmarkGUI::search(QString qstr)
    {
        _qmlDirectoryTreeModel->customizeTree(qstr.trimmed(), _qmlDirectoryTreeModelRootItem, NULL);
    }

    void CreateLandmarkGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _landmarkUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILandmarkUIHandler>(getGUIManager());
                _draggerUIHandler2 =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IDraggerUIHandler>(getGUIManager());

                CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
                CORE::RefPtr<CORE::IComponent> component = worldMaintainer->getComponentByName("SettingComponent");
                if (component.valid())
                {
                    _globalSettingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
                }

                ELEMENTS::ISettingComponent::SettingsAttributes setting = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

                if (setting.keyValuePair.find("Server") != setting.keyValuePair.end())
                    _webServerAddress = setting.keyValuePair["Server"];

                if (setting.keyValuePair.find("Username") != setting.keyValuePair.end())
                    _webserverUsername = setting.keyValuePair["Username"];

                if (setting.keyValuePair.find("Password") != setting.keyValuePair.end())
                    _webserverPassword = setting.keyValuePair["Password"];

                if (setting.keyValuePair.find("Resource") != setting.keyValuePair.end())
                    _resourceLocationOnServer = setting.keyValuePair["Resource"];

                CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
                _subscribe(component, *ELEMENTS::ISettingComponent::SettingChangedMessageType);


            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if (messageType == *ELEMENTS::ISettingComponent::SettingChangedMessageType)
        {
            _reloadLandmarkList = true;
        }
        else if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            if (!_landmarkUIHandler.valid())
            {
                _landmarkUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILandmarkUIHandler>(getGUIManager());
            }
            if (!_draggerUIHandler2.valid())
            {
                _draggerUIHandler2 =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IDraggerUIHandler>(getGUIManager());
            }
            _subscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkSelectedMessageType);

        }
        else if (messageType == *SMCUI::ILandmarkUIHandler::LandmarkCreatedMessageType)
        {
            if (_addToDefault)
            {
                setRepresentationMode(2);
            }

            _createMetadataRecord();
            CORE::RefPtr<ELEMENTS::IModelFeatureObject> modelFeatureObject = _landmarkUIHandler->getModelFeature();

            if (_addToDefault)
            {
                _landmarkUIHandler->addLandmarkToCurrentSelectedLayer();
            }
            else
            {
                _landmarkUIHandler->addToLayer(_selectedFeatureLayer);
            }

            emit landmarkCreated();
            _landmarkUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);


        }
        else if (messageType == *SMCUI::IDraggerUIHandler::DraggerUpdatedMessageType)
        {
            _populateContextualMenu();
        }
        else if (messageType == *SMCUI::ILandmarkUIHandler::LandmarkSelectedMessageType)
        {
            // if contextual menu is open then dont show the dragger
            if (_mode == MODE_CONTEXTUALMENU)
            {
                return;
            }

            _mode = MODE_EDIT_LANDMARK;
        }
    }

    void CreateLandmarkGUI::_resetForDefaultHandling()
    {
        _addToDefault = true;
        _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_NONE);
        _landmarkUIHandler->setLandmarkFilename("");

        setActive(false);
    }

    void CreateLandmarkGUI::_updateDragger()
    {
        if (_draggerUIHandler2.valid())
        {
            if (_draggerUIHandler2->getInterface<APP::IUIHandler>()->getFocus())
            {
                CORE::RefPtr<ELEMENTS::IModelFeatureObject> object = _landmarkUIHandler->getModelFeature();

                if (object.valid())
                {
                    CORE::RefPtr<ELEMENTS::IModelHolder> modelObject = object->getInterface<ELEMENTS::IModelHolder>();
                    if (modelObject.valid())
                        _draggerUIHandler2->setModel(modelObject);
                }
            }
        }
    }

    void CreateLandmarkGUI::setModel(int index)
    {
        if (index < 0)
            return;

        if (index >= _qmlDirectoryTreeModel->rowCount())
            return;


        if (!_landmarkUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
        if (!model.valid())
        {
            return;
        }

        QMLDirectoryTreeModelItem* item = dynamic_cast<QMLDirectoryTreeModelItem*>(_qmlDirectoryTreeModel->getItem(index));

        if (item != NULL)
        {
            _currentSelectedFile = item->getFileInfo();
            if (!_currentSelectedFile.isFile() && !_currentSelectedFile.isDir()){
                if (_uidNameMap.find(_currentSelectedFile.fileName().toStdString()) == _uidNameMap.end()){
                    // get parent item
                    QMLDirectoryTreeModelItem* itemParent = dynamic_cast<QMLDirectoryTreeModelItem*>(item->parent());

                    if (itemParent == NULL){
                        return;
                    }
                    else{

                        // get parent name
                        _selectedServer = itemParent->getFileInfo().fileName().toStdString();

                        // get serverURL
                        QString serverURL = QString::fromStdString(UTIL::DownloadModelUtil::getServerURLFromName(_selectedServer));
                        serverURL.remove("/maps");

                        std::string modelURL = serverURL.toStdString();

                        // get currently selected model file name
                        _selectedName = item->getFileInfo().fileName().toStdString();

                        _selectedUID = (_uidNameMap[_selectedServer])[_selectedName];

                        _selectedModelRelativeLocation = modelURL + "/" + _selectedUID + "/" + _selectedName;
                        //setActive(true);
                        model->setModelFilename(_selectedModelRelativeLocation);
                        model->getInterface<ELEMENTS::IRepresentation>()->setRepresentationMode(ELEMENTS::IRepresentation::MODEL);
                    }
                }
            }
            else{
                std::string modelAppDataDirPath = _globalSettingComponent->getDataDirectoryPath();
                QDir modelAppDataDir(QString::fromStdString(modelAppDataDirPath));

                std::string relativePath = modelAppDataDir.relativeFilePath(_currentSelectedFile.absoluteFilePath()).toStdString();

                model->setModelFilename(relativePath);
                model->getInterface<ELEMENTS::IRepresentation>()->setRepresentationMode(ELEMENTS::IRepresentation::MODEL);
            }
        }
    }

    void CreateLandmarkGUI::initializeList()
    {
        std::string modelAppDataDirPath = _globalSettingComponent->getDataDirectoryPath();
        //std::string modelAppDataDirPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + LANDMARK_APPDATA_DIR;

        QDir modelAppDataDir(QString::fromStdString(modelAppDataDirPath));

        if (_qmlDirectoryTreeModel == NULL)
        {
            _qmlDirectoryTreeModel = new QMLDirectoryTreeModel;
        }

        QFileInfo fileInfo(modelAppDataDir.absolutePath());
        _qmlDirectoryTreeModelRootItem = new QMLDirectoryTreeModelItem(fileInfo);
        _qmlDirectoryTreeModelRootItem->setCheckable(false);

        _qmlDirectoryTreeModel->clear();
        _qmlDirectoryTreeModelRootItem->clear();
        _qmlCategoriesList.clear();

        _qmlDirectoryTreeModel->addItem(_qmlDirectoryTreeModelRootItem);

        _populateDir(modelAppDataDir, _qmlDirectoryTreeModelRootItem, true);

        if (!_globalSettingComponent->isLocalProject()){
            // get the list of servers available
            _georbISServerList = UTIL::DownloadModelUtil::readAllServers();
            _serverModelsItem = new QMLDirectoryTreeModelItem(QFileInfo(QString("Server Models")));
            _serverModelsItem->setCheckable(false);
            // get the list of models in each server
            for (int i = 0; i < _georbISServerList.size(); i++){
                std::string serverName = _georbISServerList.at(i);
                std::string response = UTIL::DownloadModelUtil::readModelsFromServer(serverName);
                if (response == "Error"){
                    continue;
                }

                // parse model list json
                _parseResponse(serverName, response);
            }
            _qmlDirectoryTreeModel->addItem(_serverModelsItem);
        }
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {
            qtapp->getRootContext()->setContextProperty("landmarkListModel", _qmlDirectoryTreeModel);
            _setContextProperty("landmarkCategoryList", QVariant::fromValue(_qmlCategoriesList));
        }
    }

    bool CreateLandmarkGUI::_populateDir(QDir dir, QMLDirectoryTreeModelItem* parentItem, bool fillCategories)
    {
        if (dir.exists())
        {
            dir.setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs);
            dir.setSorting(QDir::DirsFirst);

            QStringList filters;
            filters << "*.ive" << "*.obj" << "*.flt" << "*.3ds" << "*.txt" << "*.dae" << "*.osg" << "*.fbx" << "*.ply" << "*.stl";
            dir.setNameFilters(filters);


            QFileInfoList list = dir.entryInfoList();

            for (int i = 0; i < list.size(); ++i)
            {
                QFileInfo fileInfo = list.at(i);
                if (fileInfo.fileName() == "ServerModels"){
                    continue;
                }
                QMLDirectoryTreeModelItem* item = new QMLDirectoryTreeModelItem(fileInfo);

                std::string path = fileInfo.absoluteFilePath().toStdString();

                //not to show folder/files with .txt in them

                if (path.substr(path.find_last_of(".") + 1) == "txt")
                    return false;


                item->setCheckable(false);

                if (parentItem != NULL)
                    parentItem->addItem(item);
                else
                    _qmlDirectoryTreeModel->addItem(item);

                if (fileInfo.isDir())
                {
                    if (fillCategories)
                        _qmlCategoriesList.append(new VizComboBoxElement(fileInfo.baseName(), QString::number(i)));

                    if (!_populateDir(QDir(fileInfo.absoluteFilePath()), item))
                        _qmlDirectoryTreeModel->removeItem(item);
                }
            }
        }
        return true;
    }

    //! Called when a Model is selected from Landmarks Menu
    void CreateLandmarkGUI::modelSelected(int index)
    {
        if (index < 0)
            return;

        if (index >= _qmlDirectoryTreeModel->rowCount())
            return;


        QMLDirectoryTreeModelItem* item = dynamic_cast<QMLDirectoryTreeModelItem*>(_qmlDirectoryTreeModel->getItem(index));

        if (item != NULL)
        {
            _currentSelectedFile = item->getFileInfo();
            // check if the selected item is not server name
            if (_uidNameMap.find(_currentSelectedFile.fileName().toStdString()) == _uidNameMap.end()){
                if (!_currentSelectedFile.isFile() && !_currentSelectedFile.isDir())
                {
                    // get parent item
                    QMLDirectoryTreeModelItem* itemParent = dynamic_cast<QMLDirectoryTreeModelItem*>(item->parent());

                    if (itemParent == NULL){
                        setActive(false);
                    }
                    else{

                        // get parent name
                        _selectedServer = itemParent->getFileInfo().fileName().toStdString();

                        // get serverURL
                        QString serverURL = QString::fromStdString(UTIL::DownloadModelUtil::getServerURLFromName(_selectedServer));
                        serverURL.remove("/maps");

                        std::string modelURL = serverURL.toStdString();

                        // get currently selected model file name
                        _selectedName = item->getFileInfo().fileName().toStdString();

                        _selectedUID = (_uidNameMap[_selectedServer])[_selectedName];

                        _mode = MODE_CREATE_LANDMARK;
                        _selectedModelRelativeLocation = modelURL + "/" + _selectedUID + "/" + _selectedName;
                        setActive(true);
                    }
                }
                else
                {
                    _mode = MODE_CREATE_LANDMARK;
                    setActive(true);
                }
            }
        }
    }

    //! Called when something is deselected
    void CreateLandmarkGUI::deselectModel(QString model)
    {
        //_currentSelectedFile

        _mode = MODE_NONE;
        _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_EDIT);
        setActive(false);
    }

    void CreateLandmarkGUI::changeName(QString newName)
    {
        if (!_landmarkUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
        if (model.valid())
        {
            model->getInterface<CORE::IBase>()->setName(newName.toStdString());
        }
    }

    bool CreateLandmarkGUI::_isModelValid()
    {
        if (!_landmarkUIHandler.valid())
        {
            return false;
        }

        if (!_model.valid())
        {
            return false;
        }

        return true;

    }

    void CreateLandmarkGUI::closeClippingFromGUI()
    {
        //if cross section is persistant 
        if (_crossSectionPersistant)
            return;

        if (_crossSection)
        {
            toggleCrossSection();
        }
    }

    void CreateLandmarkGUI::toggleCrossSection()
    {


        if (!_crossSection)
        {
            _crossSection = true;
        }

        else
        {
            _crossSection = false;


            QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
            if (landmarkContextual != NULL)
            {
                bool presistantEnabledCheck = landmarkContextual->property("persistentEnabledOnOtherModel").toBool();

                if (presistantEnabledCheck)
                    return;
                else if (_crossSectionPersistant)
                {
                    return;
                }
            }

            _xClip1 = false;
            _yClip1 = false;
            _zClip1 = false;
            _xClip2 = false;
            _yClip2 = false;
            _zClip2 = false;
            _xPlane1 = 0.0;
            _xPlane2 = 0.0;
            _yPlane1 = 0.0;
            _yPlane2 = 0.0;
            _zPlane1 = 0.0;
            _zPlane2 = 0.0;
            _opacity = 0.3;
            _removeBoundary = false;
            _bb.init();

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                //Removing all the quads
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_X1, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_X1);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_X2, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_X2);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Y1, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Y1);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Y2, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Y2);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Z1, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Z1);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Z2, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Z2);
            }

            toggleClipping();

            if (landmarkContextual != NULL)
            {
                landmarkContextual->setProperty("xPlaneEnabled", false);
                landmarkContextual->setProperty("yPlaneEnabled", false);
                landmarkContextual->setProperty("zPlaneEnabled", false);
                landmarkContextual->setProperty("xPlaneOppositeEnabled", false);
                landmarkContextual->setProperty("yPlaneOppositeEnabled", false);
                landmarkContextual->setProperty("zPlaneOppositeEnabled", false);
            }
        }
    }

    //Refreshes the current state of planes 
    void CreateLandmarkGUI::toggleClipping()
    {
        if (!_isModelValid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
        if (modelClip)
        {
            //enables the plane that are set for clipping
            modelClip->setClipMask(ELEMENTS::IClippable::ADD_NOTHING, true);
        }
    }

    void CreateLandmarkGUI::_setBoundingBox()
    {
        if (!_isModelValid())
        {
            return;
        }

        //get the bounding box from iclippable
        CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
        if (modelClip)
        {
            _bb = modelClip->getBounds();
            if (!_bb.valid())
            {
                showError("Bounding Box is invalid", "3d model is not valid");
                return;
            }
        }
    }

    void CreateLandmarkGUI::_setPlaneExtents()
    {

        //set the min max in x , y and z plane's minimum and maximum value
        QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
        if (landmarkContextual != NULL)
        {
            //Populating the model's min/max extents in respective sliders of landmarkContextual popup 
            //c_str() to convert std::string into char*

            //Setting min max value of slider for - and + direction
            landmarkContextual->setProperty(XPlaneMinExtent.c_str(), getPrecision(_bb._min.x()));
            landmarkContextual->setProperty(XPlaneMaxExtent.c_str(), getPrecision(_bb._max.x()));
            landmarkContextual->setProperty(XPlaneMinExtentOpposite.c_str(), getPrecision(_bb._min.x()));
            landmarkContextual->setProperty(XPlaneMaxExtentOpposite.c_str(), getPrecision(_bb._max.x()));

            //Setting min max value of spin box for - and + direction
            landmarkContextual->setProperty(XPlaneSpinBoxMin.c_str(), getPrecision(_bb._min.x()));
            landmarkContextual->setProperty(XPlaneSpinBoxMax.c_str(), getPrecision(_bb._max.x()));
            landmarkContextual->setProperty(XPlaneSpinBoxMinOpposite.c_str(), getPrecision(_bb._min.x()));
            landmarkContextual->setProperty(XPlaneSpinBoxMaxOpposite.c_str(), getPrecision(_bb._max.x()));

            landmarkContextual->setProperty(YPlaneMinExtent.c_str(), getPrecision(_bb._min.y()));
            landmarkContextual->setProperty(YPlaneMaxExtent.c_str(), getPrecision(_bb._max.y()));
            landmarkContextual->setProperty(YPlaneMinExtentOpposite.c_str(), getPrecision(_bb._min.y()));
            landmarkContextual->setProperty(YPlaneMaxExtentOpposite.c_str(), getPrecision(_bb._max.y()));

            landmarkContextual->setProperty(YPlaneSpinBoxMin.c_str(), getPrecision(_bb._min.y()));
            landmarkContextual->setProperty(YPlaneSpinBoxMax.c_str(), getPrecision(_bb._max.y()));
            landmarkContextual->setProperty(YPlaneSpinBoxMinOpposite.c_str(), getPrecision(_bb._min.y()));
            landmarkContextual->setProperty(YPlaneSpinBoxMaxOpposite.c_str(), getPrecision(_bb._max.y()));

            landmarkContextual->setProperty(ZPlaneMinExtent.c_str(), getPrecision(_bb._min.z()));
            landmarkContextual->setProperty(ZPlaneMaxExtent.c_str(), getPrecision(_bb._max.z()));
            landmarkContextual->setProperty(ZPlaneMinExtentOpposite.c_str(), getPrecision(_bb._min.z()));
            landmarkContextual->setProperty(ZPlaneMaxExtentOpposite.c_str(), getPrecision(_bb._max.z()));

            landmarkContextual->setProperty(ZPlaneSpinBoxMin.c_str(), getPrecision(_bb._min.z()));
            landmarkContextual->setProperty(ZPlaneSpinBoxMax.c_str(), getPrecision(_bb._max.z()));
            landmarkContextual->setProperty(ZPlaneSpinBoxMinOpposite.c_str(), getPrecision(_bb._min.z()));
            landmarkContextual->setProperty(ZPlaneSpinBoxMaxOpposite.c_str(), getPrecision(_bb._max.z()));

            //Both Spinbox and slider should be updating simultaneously if any one of them changes
            landmarkContextual->setProperty(XPlaneDistance.c_str(), _xPlane1);
            landmarkContextual->setProperty(XPlaneSpinBoxDistance.c_str(), _xPlane1);

            landmarkContextual->setProperty(XPlaneDistanceOpposite.c_str(), _xPlane2);
            landmarkContextual->setProperty(XPlaneSpinBoxDistanceOpposite.c_str(), _xPlane2);

            landmarkContextual->setProperty(YPlaneDistance.c_str(), _yPlane1);
            landmarkContextual->setProperty(YPlaneSpinBoxDistance.c_str(), _yPlane1);

            landmarkContextual->setProperty(YPlaneDistanceOpposite.c_str(), _yPlane2);
            landmarkContextual->setProperty(YPlaneSpinBoxDistanceOpposite.c_str(), _yPlane2);

            landmarkContextual->setProperty(ZPlaneDistance.c_str(), _zPlane1);
            landmarkContextual->setProperty(ZPlaneSpinBoxDistance.c_str(), _zPlane1);

            landmarkContextual->setProperty(ZPlaneDistanceOpposite.c_str(), _zPlane2);
            landmarkContextual->setProperty(ZPlaneSpinBoxDistanceOpposite.c_str(), _zPlane2);

            landmarkContextual->setProperty(OpacitySliderValue.c_str(), _opacity * 100);
            landmarkContextual->setProperty(OpacitySpinBoxValue.c_str(), _opacity * 100);

        }

    }

    //function that set opacity value
    void CreateLandmarkGUI::setOpacity(double opacity)
    {
        _opacity = opacity;
        if (!_isModelValid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
        if (modelClip)
        {
            //Setting the quad's opacity for all 3 planes
            modelClip->setPlaneQuadOpacity(_opacity);
            // to reflect the opacity changes at the same time
            modelClip->setClipMask(ELEMENTS::IClippable::ADD_NOTHING, true);
            // to reflect the remove boundary changes at the same time
            toggleQuadsBoundary(_removeBoundary);
        }


    }

    // function to enable/disable boundaries of quads in all three planes
    void CreateLandmarkGUI::toggleQuadsBoundary(bool remove)
    {
        _removeBoundary = remove;
        if (!_isModelValid())
        {
            return;
        }

        if (remove)
        {
            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                //removes all the boundary quads for all axis in + and - direction
                modelClip->removeQuadsBoundary(remove);

            }
        }

        else
        {
            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                modelClip->setClipMask(ELEMENTS::IClippable::ADD_NOTHING, true);
            }

            toggleClipping();
        }
    }

    void CreateLandmarkGUI::togglePersistanceCheck(bool check)
    {
        _crossSectionPersistant = check;

    }

    //enabling/disabling of plane1
    void CreateLandmarkGUI::toggleXPlane1(bool xClip1)
    {
        if (xClip1)
        {

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                _setBoundingBox();

                if (!_xClip1)
                {
                    //setting 1st clipPlane at maximum bound of model
                    _xPlane1 = getPrecision(_bb._max.x());
                    _xClip1 = true;
                }

                _setPlaneExtents();

                //setting the plane distance for - and + direction along x axis             
                modelClip->setPlaneDistance(ELEMENTS::IClippable::PLANE_X1, _xPlane1);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_X1, true);
                toggleQuadsBoundary(_removeBoundary);

            }
            _xClip1 = true;
        }

        else
        {
            _xClip1 = false;
            _xPlane1 = 0.0;

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_X1, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_X1);
            }
            toggleClipping();

        }

    }

    //enabling/disabling of plane2
    void CreateLandmarkGUI::toggleXPlane2(bool xClip2)
    {
        if (xClip2)
        {

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                _setBoundingBox();

                if (!_xClip2)
                {
                    //setting 2nd clipPlane at minimum bound of model
                    _xPlane2 = getPrecision(_bb._min.x());
                    _xClip2 = true;
                }

                _setPlaneExtents();

                //setting the plane distance for - and + direction along x axis           
                modelClip->setPlaneDistance(ELEMENTS::IClippable::PLANE_X2, _xPlane2);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_X2, true);
                toggleQuadsBoundary(_removeBoundary);

            }
            _xClip2 = true;
        }

        else
        {
            _xClip2 = false;
            _xPlane2 = 0.0;

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_X2, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_X2);
            }
            toggleClipping();
        }

    }

    //to get the value upto 2 decimals
    double CreateLandmarkGUI::getPrecision(double value)
    {
        long double value1 = value * 100;
        int valueInt = value1;
        return (double)valueInt / 100;

    }

    // sets the xplane 1 distance value
    void CreateLandmarkGUI::setXPlane(double xplane)
    {
        if (_xClip1)
        {
            _xPlane1 = getPrecision(xplane);
            toggleXPlane1(true);
        }


    }

    // sets the xplane 2 distance value
    void CreateLandmarkGUI::setXPlaneOpposite(double xplaneOpposite)
    {
        // if xOpposite is set that means oppsosite plane distance is to be changed
        if (_xClip2)
        {
            _xPlane2 = getPrecision(xplaneOpposite);
            toggleXPlane2(true);
        }


    }

    void CreateLandmarkGUI::toggleYPlane1(bool yClip1)
    {
        if (yClip1)
        {

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                _setBoundingBox();

                if (!_yClip1)
                {
                    //setting 1st clipPlane at maximum bound of model
                    _yPlane1 = getPrecision(_bb._max.y());
                    _yClip1 = true;
                }

                _setPlaneExtents();

                //setting the plane distance for - and + direction along x axis             
                modelClip->setPlaneDistance(ELEMENTS::IClippable::PLANE_Y1, _yPlane1);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Y1, true);
                toggleQuadsBoundary(_removeBoundary);

            }
            _yClip1 = true;
        }

        else
        {
            _yClip1 = false;
            _yPlane1 = 0.0;

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Y1, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Y1);
            }
            toggleClipping();

        }
    }

    void CreateLandmarkGUI::toggleYPlane2(bool yClip2)
    {
        if (yClip2)
        {

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                _setBoundingBox();

                if (!_yClip2)
                {
                    //setting 2nd clipPlane at minimum bound of model
                    _yPlane2 = getPrecision(_bb._min.y());
                    _yClip2 = true;
                }

                _setPlaneExtents();

                //setting the plane distance for - and + direction along x axis           
                modelClip->setPlaneDistance(ELEMENTS::IClippable::PLANE_Y2, _yPlane2);
                // _setActiveClippingPlanes();
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Y2, true);
                toggleQuadsBoundary(_removeBoundary);

            }
            _yClip2 = true;
        }

        else
        {
            _yClip2 = false;
            _yPlane2 = 0.0;

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Y2, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Y2);
            }
            toggleClipping();

        }

    }

    void CreateLandmarkGUI::setYPlane(double yplane)
    {
        if (_yClip1)
        {
            _yPlane1 = getPrecision(yplane);
            toggleYPlane1(true);
        }


    }

    void CreateLandmarkGUI::setYPlaneOpposite(double yplaneOpposite)
    {
        // if xOpposite is set that means oppsosite plane distance is to be changed
        if (_yClip2)
        {
            _yPlane2 = getPrecision(yplaneOpposite);
            toggleYPlane2(true);
        }


    }

    void CreateLandmarkGUI::toggleZPlane1(bool zClip1)
    {
        if (zClip1)
        {

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                _setBoundingBox();

                if (!_zClip1)
                {
                    //setting 1st clipPlane at maximum bound of model
                    _zPlane1 = getPrecision(_bb._max.z());
                    _zClip1 = true;
                }

                _setPlaneExtents();

                //setting the plane distance for - and + direction along x axis             
                modelClip->setPlaneDistance(ELEMENTS::IClippable::PLANE_Z1, _zPlane1);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Z1, true);
                toggleQuadsBoundary(_removeBoundary);

            }
            _zClip1 = true;
        }

        else
        {
            _zClip1 = false;
            _zPlane1 = 0.0;

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Z1, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Z1);
            }
            toggleClipping();

        }

    }

    void CreateLandmarkGUI::toggleZPlane2(bool zClip2)
    {
        if (zClip2)
        {

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                _setBoundingBox();

                if (!_zClip2)
                {
                    //setting 2nd clipPlane at minimum bound of model
                    _zPlane2 = getPrecision(_bb._min.z());
                    _zClip2 = true;
                }

                _setPlaneExtents();

                //setting the plane distance for - and + direction along x axis           
                modelClip->setPlaneDistance(ELEMENTS::IClippable::PLANE_Z2, _zPlane2);
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Z2, true);
                toggleQuadsBoundary(_removeBoundary);
            }
            _zClip2 = true;
        }

        else
        {
            _zClip2 = false;
            _zPlane2 = 0.0;

            if (!_isModelValid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IClippable> modelClip = _model->getInterface<ELEMENTS::IClippable>();
            if (modelClip)
            {
                modelClip->setClipMask(ELEMENTS::IClippable::PLANE_Z2, false);
                modelClip->removePlaneQuad(ELEMENTS::IClippable::PLANE_Z2);
            }
            toggleClipping();
        }

    }

    void CreateLandmarkGUI::setZPlane(double zplane)
    {
        if (_zClip1)
        {
            _zPlane1 = getPrecision(zplane);
            toggleZPlane1(true);
        }


    }

    void CreateLandmarkGUI::setZPlaneOpposite(double zplaneOpposite)
    {
        // if xOpposite is set that means oppsosite plane distance is to be changed
        if (_zClip2)
        {
            _zPlane2 = getPrecision(zplaneOpposite);
            toggleZPlane2(true);
        }


    }

    void CreateLandmarkGUI::handleLatLongAltScaleChanged(QString attribute)
    {
        if (!_landmarkUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
        if (!model.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPoint> point = model->getInterface<CORE::IPoint>();
        CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = model->getInterface<ELEMENTS::IModelHolder>();

        QObject* landmarkContextual = _findChild(LandmarkContextualObjectName);
        if (landmarkContextual != NULL)
        {
            double longitudeD = point->getX();
            double latitudeD = point->getY();
            double altitudeD = point->getZ() - ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD);
            double scaleD = modelHolder->getModelScale().x();

            if (!attribute.compare("latitude", Qt::CaseInsensitive))
            {
                QString latitude = landmarkContextual->property("latitude").toString();
                latitudeD = latitude.toDouble();
            }
            else if (!attribute.compare("longitude", Qt::CaseInsensitive))
            {
                QString longitude = landmarkContextual->property("longitude").toString();
                longitudeD = longitude.toDouble();
            }
            else if (!attribute.compare("altitude", Qt::CaseInsensitive))
            {
                QString altitude = landmarkContextual->property("altitude").toString();
                altitudeD = altitude.toDouble();
            }
            else if (!attribute.compare("scale", Qt::CaseInsensitive))
            {
                QString scale = landmarkContextual->property("scale").toString();
                scaleD = scale.toDouble();
            }

            osg::Vec3 position(longitudeD, latitudeD, altitudeD + ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD));
            osg::Vec3 modelScale(scaleD, scaleD, scaleD);

            osg::Quat orientation = model->getInterface<ELEMENTS::IModelHolder>()->getModelOrientation();

            model->getInterface<CORE::IPoint>()->setValue(position);

            model->getInterface<ELEMENTS::IModelHolder>()->setModelScale(modelScale);
            model->getInterface<ELEMENTS::IModelHolder>()->setModelOrientation(orientation);

            _updateDragger();
        }
    }

    void CreateLandmarkGUI::snapToGround()
    {
        if (!_landmarkUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
        if (!model.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPoint> point = model->getInterface<CORE::IPoint>();
        if (point.valid())
        {
            osg::Vec3 position = point->getValue();

            position.z() = ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y());

            osg::Quat orientation = model->getInterface<ELEMENTS::IModelHolder>()->getModelOrientation();

            point->setValue(position);

            model->getInterface<ELEMENTS::IModelHolder>()->setModelOrientation(orientation);

            _updateDragger();
        }
    }

    void CreateLandmarkGUI::reorient()
    {
        if (!_landmarkUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
        if (!model.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = model->getInterface<ELEMENTS::IModelHolder>();
        if (modelHolder.valid())
        {
            modelHolder->setModelXRotate(0);
            modelHolder->setModelYRotate(0);
            modelHolder->setModelZRotate(0);

            _updateDragger();
        }
    }


    void CreateLandmarkGUI::deleteLandmark()
    {
        if (!_landmarkUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = _landmarkUIHandler->getModelFeature();
        if (!model.valid())
        {
            return;
        }

        CORE::IDeletable* deletable = model->getInterface<CORE::IDeletable>();
        if (!deletable)
        {
            return;
        }

        CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
        if (deletionUIHandler.valid())
        {
            deletionUIHandler->deleteObject(deletable);
        }
    }

    void CreateLandmarkGUI::openModelSelectionPane(bool value)
    {
        QObject* smpMenu = _findChild(SMP_RightMenu);

        if (smpMenu == NULL)
            return;

        if (value)
        {
            smpMenu->setProperty("menuLoaderSource", QVariant::fromValue(QString("")));
            smpMenu->setProperty("minimized", QVariant::fromValue(false));
            smpMenu->setProperty("menuLoaderSource", QVariant::fromValue(QString("Lanmarks.qml")));
        }
        else
        {
            smpMenu->setProperty("menuLoaderSource", QVariant::fromValue(QString("")));
            smpMenu->setProperty("minimized", QVariant::fromValue(true));
        }

        _modelSelectionPaneOpenedFromContextualMenu = value;
    }

    void CreateLandmarkGUI::setActive(bool value)
    {
        if (!_landmarkUIHandler.valid())
        {
            return;
        }

        if (value)
        {
            if (_mode == MODE_EDIT_LANDMARK)
            {
                _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_EDIT);
                _landmarkUIHandler->setSelectedLandmarkAsCurrent();
                _landmarkUIHandler->setProcessMouseEvents(true);
                _draggerUIHandler2->getInterface<APP::IUIHandler>()->setFocus(true);
                _landmarkUIHandler->setTemporaryState(false);
                _updateDragger();

                _subscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkUpdatedMessageType);
            }
            else if (_mode == MODE_CONTEXTUALMENU)
            {
                _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_EDIT);
                _landmarkUIHandler->setSelectedLandmarkAsCurrent();
                _landmarkUIHandler->setProcessMouseEvents(true);
                _draggerUIHandler2->getInterface<APP::IUIHandler>()->setFocus(false);
                _landmarkUIHandler->setTemporaryState(false);
                _updateDragger();

                _subscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkUpdatedMessageType);
            }
            else if (_mode == MODE_CREATE_LANDMARK)
            {
                _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_CREATE);
                _landmarkUIHandler->setProcessMouseEvents(true);

                if (_addToDefault && _currentSelectedFile.isFile())
                {
                    _landmarkUIHandler->setLandmarkName(_currentSelectedFile.baseName().toStdString());

                    std::string modelAppDataDirPath = _globalSettingComponent->getDataDirectoryPath();

                    QDir modelAppDataDir(QString::fromStdString(modelAppDataDirPath));

                    std::string relativePath = modelAppDataDir.relativeFilePath(_currentSelectedFile.absoluteFilePath()).toStdString();

                    _landmarkUIHandler->setLandmarkFilename(relativePath);
                    _landmarkUIHandler->setTemporaryState(true);
                }
                else
                {
                    _landmarkUIHandler->setLandmarkName(_selectedName);
                    _landmarkUIHandler->setLandmarkFilename(_selectedModelRelativeLocation);
                    _landmarkUIHandler->setTemporaryState(true);
                }

                _subscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkCreatedMessageType);
                _subscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkUpdatedMessageType);

            }
        }
        else
        {
            if (_mode == MODE_EDIT_LANDMARK)
            {
                _mode = MODE_NONE;
                _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_NONE);
                _draggerUIHandler2->getInterface<APP::IUIHandler>()->setFocus(false);
                _landmarkUIHandler->setTemporaryState(false);
                _landmarkUIHandler->setProcessMouseEvents(false);

                _unsubscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkCreatedMessageType);
                _unsubscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkUpdatedMessageType);
            }
            else if (_mode == MODE_CREATE_LANDMARK)
            {

                _mode = MODE_NONE;
                _landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_NONE);
                _landmarkUIHandler->setTemporaryState(false);
                _landmarkUIHandler->setProcessMouseEvents(false);
                _landmarkUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _landmarkUIHandler->setLandmarkFilename("");
                _draggerUIHandler2->getInterface<APP::IUIHandler>()->setFocus(false);

                _unsubscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkCreatedMessageType);
                _unsubscribe(_landmarkUIHandler.get(), *SMCUI::ILandmarkUIHandler::LandmarkUpdatedMessageType);
            }
        }

        DeclarativeFileGUI::setActive(value);
    }
    void CreateLandmarkGUI::setRepresentationMode(int mode)
    {
        CORE::RefPtr<ELEMENTS::IModelFeatureObject> modelObject = _landmarkUIHandler->getModelFeature();
        if (modelObject.valid())
        {

            CORE::RefPtr<ELEMENTS::IRepresentation> represent =
                modelObject->getInterface<ELEMENTS::IRepresentation>();

            represent->setRepresentationMode(ELEMENTS::IRepresentation::RepresentationMode(mode));
        }

        _landmarkUIHandler->_update();

    }
    void CreateLandmarkGUI::setMode(int mode)
    {
        _mode = (LandmarkMode)(mode);
    }

    void CreateLandmarkGUI::_createMetadataRecord()
    {
        CORE::RefPtr<ELEMENTS::IModelFeatureObject> modelFeatureObject = _landmarkUIHandler->getModelFeature();
        if (!modelFeatureObject.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer =
            CORE::WorldMaintainer::instance();

        if (!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component =
            worldMaintainer->getComponentByName("DataSourceComponent");

        if (!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator =
            component->getInterface<CORE::IMetadataCreator>();

        if (!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = NULL;
        if (_addToDefault)
        {
            tableDefn = _landmarkUIHandler->getCurrentSelectedLayerDefn();
        }
        else
        {
            if (_selectedFeatureLayer.valid())
            {
                CORE::RefPtr<CORE::IMetadataTableHolder> metadataTableHolder = _selectedFeatureLayer->getInterface<CORE::IMetadataTableHolder>(true);

                if (metadataTableHolder.valid())
                {
                    tableDefn = metadataTableHolder->getMetadataTable()->getMetadataTableDefn();
                }
            }
        }


        if (!tableDefn.valid())
        {
            LOG_ERROR("Invalid IMetadataTableDefn instance");
            return;
        }

        CORE::RefPtr<CORE::IMetadataRecord> record = metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        // set name
        modelFeatureObject->getInterface<CORE::IBase>()->setName(_currentSelectedFile.baseName().toStdString());

        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("NAME");
            if (field.valid())
            {
                field->fromString(_currentSelectedFile.baseName().toStdString());
            }
        }
        //{
        //    CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("Type");
        //    if(field.valid())
        //    {
        //        field->fromString(_currentModelType);
        //    }
        //}
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("Representation");
            if (field.valid())
            {
                field->fromInt(ELEMENTS::IRepresentation::MODEL);
            }
        }

        osg::Quat orientation(0, 0, 0, 1);
        osg::Vec3 scale(1, 1, 1);
        std::string modelFileName = "";

        CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = modelFeatureObject->getInterface<ELEMENTS::IModelHolder>();

        if (modelHolder.valid())
        {
            orientation = modelHolder->getModelOrientation();
            scale = modelHolder->getModelScale();
            CORE::RefPtr<ELEMENTS::IModel> model = modelHolder->getModel();
            if (model.valid())
            {
                modelFileName = model->getModelFilename();
            }
        }

        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("OrientationX");
            if (field.valid())
            {
                field->fromDouble(orientation.x());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("OrientationY");
            if (field.valid())
            {
                field->fromDouble(orientation.y());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("OrientationZ");
            if (field.valid())
            {
                field->fromDouble(orientation.z());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("OrientationW");
            if (field.valid())
            {
                field->fromDouble(orientation.w());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("ScaleX");
            if (field.valid())
            {
                field->fromDouble(scale.x());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("ScaleY");
            if (field.valid())
            {
                field->fromDouble(scale.y());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("ScaleZ");
            if (field.valid())
            {
                field->fromDouble(scale.z());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("ModelFileName");
            if (field.valid())
            {
                field->fromString(modelFileName);
            }
        }

        _landmarkUIHandler->setMetadataRecord(record.get());
    }

    void CreateLandmarkGUI::popupLoaded(QString type)
    {
        if (type == "addLandmarkTypePopup")
        {
            QObject* smpNewLandmark = _findChild("addLandmarkTypePopup");
            if (smpNewLandmark != NULL)
            {
                QObject::connect(smpNewLandmark, SIGNAL(addLandmarkType(QString)), this,
                    SLOT(addNewLandmark(QString)), Qt::UniqueConnection);

                QObject::connect(smpNewLandmark, SIGNAL(browseButtonClicked()), this,
                    SLOT(browseButtonClicked()), Qt::UniqueConnection);

                QObject::connect(smpNewLandmark, SIGNAL(showGUIError(QString, QString)), this,
                    SLOT(showGUIError(QString, QString)), Qt::UniqueConnection);
            }
        }

        else if (type == "addLandmarkDirectoryPopup")
        {
            QObject* LandmarkMenu = _findChild("addLandmarkDirectoryPopup");
            if (LandmarkMenu != NULL)
            {
                QObject::connect(LandmarkMenu, SIGNAL(browseButtonClicked()), this,
                    SLOT(browseLandmarkButtonClicked()), Qt::UniqueConnection);

                QObject::connect(LandmarkMenu, SIGNAL(onClickedOk(QString)), this,
                    SLOT(onClickedOk(QString)), Qt::UniqueConnection);
            }
        }
    }

    void CreateLandmarkGUI::browseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        if (_lastPath.empty())
            _lastPath = directory.toStdString();


        QString filepath = QFileDialog::getOpenFileName(parent, tr("Open Model File"),
            _lastPath.c_str(), tr("ModelFile (*.ive *.obj *.flt *.3ds *.dae *.osg *.fbx *.ply *.stl)"), 0);

        QFileInfo fileInfo(filepath);

        std::string file = fileInfo.baseName().toStdString();

        _lastPath = filepath.toStdString();

        QObject* addLandmarkTypePopup = _findChild("addLandmarkTypePopup");
        if (addLandmarkTypePopup)
        {
            addLandmarkTypePopup->setProperty("modelFilePath", filepath);
            addLandmarkTypePopup->setProperty("landmarkName", file.c_str());
        }
    }

    void CreateLandmarkGUI::browseLandmarkButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption = "Landmark Folder";


        if (_lastPath.empty())
        {
            _lastPath = directory.toStdString();
        }

        QString dirPath = QFileDialog::getExistingDirectory(parent, caption, directory);
        _lastPath = dirPath.toStdString();

        QObject* LandmarkMenu = _findChild("addLandmarkDirectoryPopup");
        if (LandmarkMenu)
        {
            LandmarkMenu->setProperty("userDirectoryPath", dirPath);
        }

    }

    void CreateLandmarkGUI::addModelsToSelectedLayer(bool value)
    {
        if (value)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& map =
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter =
                map.begin();

            //XXX - using the first element in the selection
            if (iter != map.end())
            {
                _selectedFeatureLayer = iter->second->getInterface<CORE::IFeatureLayer>();
                _addToDefault = false;
                _mode = MODE_CREATE_LANDMARK;
            }

            setActive(true);
        }
        else
        {
            _resetForDefaultHandling();
        }
    }

    void CreateLandmarkGUI::addNewLandmark(QString modelName)
    {
        if (modelName.isEmpty())
        {
            return;
        }

        try{

            std::string appFolderPath = _globalSettingComponent->getDataDirectoryPath();

            QObject* addLandmarkTypePopup = _findChild("addLandmarkTypePopup");
            //QObject* LandmarkMenu = _findChild("addLandmarkDirectoryPopup");

            if (addLandmarkTypePopup)
            {
                QString category = addLandmarkTypePopup->property("modelCategory").toString();
                if (!category.isEmpty())
                {
                    appFolderPath += "/" + category.toStdString() + "/";
                }
            }

            appFolderPath += osgDB::getSimpleFileName(modelName.toStdString());
            QFile::copy(modelName, QString::fromStdString(appFolderPath));
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
            return;
        }

        QObject* addLandmarkTypePopup = _findChild("addLandmarkTypePopup");
        QMetaObject::invokeMethod(addLandmarkTypePopup, "closePopup");

        _setContextProperty("landmarkListModel", QVariant::fromValue(NULL));

        initializeList();
    }
    void CreateLandmarkGUI::doReloadFeatureQml()
    {
        QObject* smpMenu = _findChild("smpMenu");
        QVariant returnedValue;
        QVariant input = "Features.qml";
        QMetaObject::invokeMethod(smpMenu, "setSource", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, input));
        QMetaObject::invokeMethod(this, "doReloadLandmarksQml", Qt::QueuedConnection);
    }

    void CreateLandmarkGUI::doReloadLandmarksQml()
    {
        QObject* smpMenu = _findChild("smpMenu");
        QVariant returnedValue;
        QVariant input = "Landmarks.qml";
        QMetaObject::invokeMethod(smpMenu, "setSource", Q_RETURN_ARG(QVariant, returnedValue), Q_ARG(QVariant, input));

    }
    void CreateLandmarkGUI::onClickedOk(QString directory)
    {
        std::string path = directory.toStdString();
        if (!path.empty())
        {
            if (_globalSettingComponent.valid())
            {
                _globalSettingComponent->setDataDirectoryPath(path);
                _writeSettings();
                QMetaObject::invokeMethod(this, "doReloadFeatureQml", Qt::QueuedConnection);
                //emit showError(QString::fromStdString("Directory Saved"),QString::fromStdString("Landmark Directory has been set as default directory"));
            }
        }
        else
        {
            emit showError(QString::fromStdString("Insufficient data"), QString::fromStdString("Landmark name or model file path is not specified."));
            return;
        }
    }

    void CreateLandmarkGUI::_writeSettings()
    {
        DB::BaseWrapper *wrapper = DB::BaseWrapperManager::instance()->findWrapper("SettingComponent");
        if (wrapper)
        {
            CORE::IComponent *settingComponent = ELEMENTS::GetComponentFromMaintainer("SettingComponent");
            CORE::IBase* base = NULL;
            if (settingComponent)
            {
                base = settingComponent->getInterface<CORE::IBase>();

                std::stringstream inputstream;
                CORE::RefPtr<DB::OutputIterator> oi = new AsciiOutputIterator(&inputstream);

                DB::OutputStream os(NULL);
                os.start(oi.get(), DB::OutputStream::WRITE_ATTRIBUTE);
                wrapper->write(os, *base);

                std::ofstream filePtr;
                std::string settingConfigFile =
                    settingComponent->getInterface<ELEMENTS::ISettingComponent>()->getSettingFilePath();

                filePtr.open(settingConfigFile.c_str(), std::ios::out);
                std::string dataToWrite = inputstream.str();
                filePtr << dataToWrite.c_str();
                filePtr.close();
            }
        }
    }

}   // namespace SMCQt
