#include <SMCQt/QMLTreeModel.h>

#include <SMCQt/ElementListTreeModel.h>

namespace SMCQt
{
    QMLTreeModelItem::QMLTreeModelItem() 
        : _level(0)
        , _isOpened(false)
        , _isItemCheckable(true)
        , _isItemDraggable(false)
        ,_checkState(Qt::Checked)
        ,_parent(NULL)
    {
    }

    QMLTreeModelItem::~QMLTreeModelItem()
    {
        clear();
    }

    void QMLTreeModelItem::adjustChildrenLevels()
    {
        foreach(QMLTreeModelItem *item, _children)
        {
            item->_level = _level+1;
            item->adjustChildrenLevels();
        }
    }

    QMLTreeModelItem* QMLTreeModelItem::parent()
    {
        return _parent;
    }

    void QMLTreeModelItem::addItem(QMLTreeModelItem* item)
    {
        _children <<item;
        adjustChildrenLevels();
        item->_parent = this;
    }

    void QMLTreeModelItem::removeItem(QMLTreeModelItem* item)
    {
        if(_children.contains(item))
        {
            _children.removeAt(_children.indexOf(item));
            delete item;
        }
    }

    void QMLTreeModelItem::clear()
    {
        while(!_children.empty())
        {
            QMLTreeModelItem* item = _children.takeFirst();
            delete item;
        }

        _children.clear();
        _isOpened = false;

    }

    int QMLTreeModelItem::getLevel()
    {
        return _level;
    }

    bool QMLTreeModelItem::isOpened()
    {
        return _isOpened;
    }

    void QMLTreeModelItem::setOpened(bool value)
    {
        _isOpened = value;
    }

    bool QMLTreeModelItem::isCheckable()
    {
        return _isItemCheckable;
    }

    void QMLTreeModelItem::setCheckable(bool value)
    {
        _isItemCheckable = value;
    }

    Qt::CheckState QMLTreeModelItem::getCheckState()
    {
        return _checkState;
    }

    void QMLTreeModelItem::setCheckState(Qt::CheckState state)
    {
        _checkState = state;
    }

    bool QMLTreeModelItem::draggable() const
    {
        return _isItemDraggable;
    }

    void QMLTreeModelItem::setDragEnabled(bool draggable)
    {
        _isItemDraggable = draggable;
    }

    const QList<QMLTreeModelItem*> & QMLTreeModelItem::getChildren() const
    {
        return _children;
    }

    void QMLTreeModelItem::moveChild(int from, int to)
    {
        if((from < 0) || (to < 0))
            return;

        if((from >= _children.size()) || (to >= _children.size()))
            return;

        _children.move(from, to);
    }

    int QMLTreeModelItem::getChildCount() const
    {
        return _children.size();
    }

    bool QMLTreeModelItem::hasChildren()
    {
        return !_children.empty();
    }

    QString SimpleTreeModelItem::getName() const
    {
        return _name;
    }

    void SimpleTreeModelItem::setName(QString name)
    {
        _name = name;
    }

    QString WmsTreeModelItem::getName() const
    {
        return _name;
    }

    void WmsTreeModelItem::setName(QString name)
    {
        _name = name;
    }

    QString WmsTreeModelItem::getTitle() const
    {
        return _title;
    }

    void WmsTreeModelItem::setTitle(QString title)
    {
        _title = title;
    }

    QString WmsTreeModelItem::getAbstraction() const
    {
        return _abstraction;
    }

    void WmsTreeModelItem::setAbstraction(QString abstraction)
    {
        _abstraction = abstraction;
    } 

    QMLTreeModel::QMLTreeModel(QObject *parent) : QAbstractListModel(parent)
    {
#ifdef USE_QT4
        setRoleNames(roleNames());
#endif
    }

    QHash<int,QByteArray> QMLTreeModel::roleNames() const
    {
        QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
        roles.insert(LevelRole, QByteArray("level"));
        roles.insert(IsOpenedRole, QByteArray("isOpened"));
        roles.insert(HasChildrenRole, QByteArray("hasChildren"));
        roles.insert(CountRole, QByteArray("count"));
        roles.insert(IsCheckableRole, QByteArray("isItemCheckable"));
        roles.insert(CheckedStateRole, QByteArray("itemCheckedState"));
        roles.insert(IsDraggableRole, QByteArray("isItemDraggable"));

        return roles;
    }

    void QMLTreeModel::addItem(QMLTreeModelItem *item)
    {
        _items <<item;
        adjustChildrenLevels();
    }

    void QMLTreeModel::removeItem(QMLTreeModelItem* item)
    {
        if(!item)
        {
            return;
        }

        int index = indexOf(item);

        if(index != -1)
        {
            //! Remove all children too if item has children
            if(item->hasChildren() && item->isOpened())
            {
                int i = index+1;
                //! Count the number of direct and indirect descendants to remove
                for (; i < _items.size() && (_items[i]->getLevel() > _items[index]->getLevel()); ++i){}
                --i;
                if(i <= index)
                {
                    return;
                }

                beginRemoveRows(QModelIndex(), index, i);

                LOG_DEBUG_VAR("Removing rows : %d to %d",index, i );

                while( i >= index)
                {
                    _items.removeAt(i--);
                }

                endRemoveRows();
            }
            else
            {
                beginRemoveRows(QModelIndex(), index, index);

                LOG_DEBUG_VAR("Removing rows : %d to %d",index, index );

                _items.removeAt(index);

                endRemoveRows();
            }
        }

        QMLTreeModelItem* parent = item->parent();
        if(parent != NULL)
        {
            parent->removeItem(item);

            if(!parent->hasChildren())
                emitDataChanged(index);
        }

    }

    void QMLTreeModel::clear()
    {
        if(_items.isEmpty())
            return;

        while(!_items.empty())
        {
            QMLTreeModelItem* item = _items.first();
            removeItem(item);
        }

        qDeleteAll(_items.begin(), _items.end());
        _items.clear();
    }

    void QMLTreeModel::adjustChildrenLevels()
    {
        foreach(QMLTreeModelItem* item, _items)
        {
            item->adjustChildrenLevels();
        }
    }
    QVariant QMLTreeModel::data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        if (index.row() > (_items.size()-1) )
            return QVariant();

        QMLTreeModelItem *item = _items.at(index.row());
        switch (role)
        {
        case Qt::DisplayRole:
        case LevelRole:
            return QVariant::fromValue(item->getLevel());
        case IsOpenedRole:
            return QVariant::fromValue(item->isOpened());
        case HasChildrenRole:
            return QVariant::fromValue(item->hasChildren());
        case CountRole:
            return QVariant::fromValue(rowCount());
        case IsCheckableRole:
            return QVariant::fromValue(item->isCheckable());
        case CheckedStateRole:
            return QVariant::fromValue(int(item->getCheckState()));
        case IsDraggableRole:
            return QVariant::fromValue(item->draggable());
        default:
            return QVariant();
        }
    }

    int QMLTreeModel::rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent)
        return _items.size();
    }

    void QMLTreeModel::toggleItem(int numIndex)
    {
        if (numIndex > (_items.size()-1))
            return;

        // return for negative index. 
        if (0 > numIndex)
            return;

        if(!_items[numIndex]->hasChildren())
            return;

        if (_items[numIndex]->isOpened())
            closeItem(numIndex);
        else
            openItem(numIndex);
    }

    void QMLTreeModel::move(int numIndex, int newParentIndex)
    {
        if((numIndex < 0) || (newParentIndex < 0))
            return;

        if((numIndex >= _items.size()) || (newParentIndex >= _items.size()))
            return;

        QMLTreeModelItem* itemToMove = _items.at(numIndex);

        if(!itemToMove)
            return;

        QMLTreeModelItem* itemParent = itemToMove->parent();
        QMLTreeModelItem* newItemParent = _items.at(newParentIndex);

        if(itemParent && newItemParent)
        {
            int parentIndex = indexOf(itemParent);

            if(parentIndex != newParentIndex)
            {
                itemParent->removeItem(itemToMove);
                newItemParent->addItem(itemToMove);
            }

            closeItem(0);
            openItem(0);
        }
    }

    void QMLTreeModel::toggleCheckedState(int numIndex)
    {
        if(numIndex > (_items.size() - 1))
            return;

        if(!_items[numIndex]->isCheckable())
            return;

        QModelIndex modelIndex = index(numIndex);

        if(_items[numIndex]->getCheckState() == Qt::Checked)
        {
            _items[numIndex]->setCheckState(Qt::Unchecked);
        }
        else
        {
            _items[numIndex]->setCheckState(Qt::Checked);
        }

        emit dataChanged(modelIndex, modelIndex);
    }

    void QMLTreeModel::openItem(int numIndex)
    {
        if(numIndex < 0)
        {
            return;
        }

        if (numIndex > (_items.size()-1))
            return;

        if(!_items[numIndex]->hasChildren())
            return;

        if (_items[numIndex]->isOpened())
            return;

        QModelIndex modelIndex = index(numIndex);


        //Set flag isOpened
        _items[numIndex]->setOpened(true);
        //Notify qml about data changed

        emit dataChanged(modelIndex, modelIndex);


        int i = numIndex+1;
        //Notify qml that you are about to change the row data
        beginInsertRows(QModelIndex(), i, i+_items[numIndex]->getChildCount()-1);
        //add all descendants element in model after it element
        foreach(QMLTreeModelItem *item, _items[numIndex]->getChildren())
        {
            _items.insert(i++, item);
        }
        //Notify qml that all rows are added
        endInsertRows();

    }

    int QMLTreeModel::getLevel(int index)
    {
        if(index == -1)
            return 0;
        if(index > (_items.size() - 1))
            return -1;
        return _items[index]->getLevel();
    }

    void QMLTreeModel::closeItem(int numIndex)
    {
        if(numIndex < 0)
            return;

        if(numIndex > (_items.size()-1))
            return;

        if(!_items[numIndex]->hasChildren())
            return;

        if (!_items[numIndex]->isOpened())
            return;

        QModelIndex modelIndex = index(numIndex);
        //reset flag isOpened
        _items[numIndex]->setOpened(false);
        //Notify qml about data change
        emit dataChanged(modelIndex, modelIndex);
        int i = numIndex+1;
        //Count the number of direct ad indirect descendants to remove
        for (; i < _items.size() && (_items[i]->getLevel() > _items[numIndex]->getLevel()); ++i){}
        --i;
        if(i <= numIndex)
        {
            return;
        }
        beginRemoveRows(QModelIndex(), numIndex+1, i);
        while (i > numIndex)
        {
            _items[i]->setOpened(false);
            _items.removeAt(i--);
        }
        endRemoveRows();
    }

    QMLTreeModelItem* QMLTreeModel::getItem(int numIndex)
    {
        if(numIndex > _items.size() -1)
        {
            return NULL;
        }
        return _items[numIndex];
    }

    int QMLTreeModel::indexOf(QMLTreeModelItem* item) const
    {
        if(item)
            return _items.indexOf(item);

        return -1;
    }

    void QMLTreeModel::emitDataChanged(int numIndex)
    {
        if(numIndex >= 0)
        {
            QModelIndex modelIndex = index(numIndex);

            emit dataChanged(modelIndex, modelIndex);
        }
    }

    QHash<int,QByteArray> SimpleTreeModel::roleNames() const
    {
        QHash<int, QByteArray> roles = QMLTreeModel::roleNames();
        roles.insert(NameRole, QByteArray("name"));

        return roles;
    }

    QVariant SimpleTreeModel::data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        if (index.row() > (_items.size()-1) )
            return QVariant();

        SimpleTreeModelItem *item = dynamic_cast<SimpleTreeModelItem*>(_items.at(index.row()));
        switch (role)
        {
        case Qt::DisplayRole:
        case NameRole:
            return QVariant::fromValue(item->getName());
        default:
            return QMLTreeModel::data(index, role);
        }
    }

    QHash<int,QByteArray> WmsTreeModel::roleNames() const
    {
        QHash<int, QByteArray> roles = QMLTreeModel::roleNames();
        roles.insert(NameRole, QByteArray("name"));
        roles.insert(TitleRole, QByteArray("title"));
        roles.insert(AbstractionRole, QByteArray("abstraction"));

        return roles;
    }

    QVariant WmsTreeModel::data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        if (index.row() > (_items.size()-1) )
            return QVariant();

        WmsTreeModelItem *item = dynamic_cast<WmsTreeModelItem*>(_items.at(index.row()));

        switch (role)
        {
        case Qt::DisplayRole:
        case NameRole:
            return QVariant::fromValue(item->getName());
        case TitleRole:
            return QVariant::fromValue(item->getTitle());
        case AbstractionRole:
            return QVariant::fromValue(item->getAbstraction());
        default:
            return QMLTreeModel::data(index, role);
        }
    }

}   //namespace SMCQt

