#include <SMCQt/MilitaryPathAnimationGUI.h>
#include <SMCQt/TimelineMark.h>
#include <SMCQt/VizComboBoxElement.h>

#include <VizQt/QtUtils.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Elements/IMilitarySymbol.h>

#include <SMCUI/SMCUIPlugin.h>
#include <SMCUI/IMilitaryPathAnimationUIHandler.h>
#include <SMCUI/IMilitarySymbolPointUIHandler.h>
#include <Util/CoordinateConversionUtils.h>
#include <Elements/ElementsUtils.h>
#include <Core/CommonUtilityFunctions.h>
#include <Util/FileUtils.h>

#include <VizUI/ISelectionUIHandler.h>
#include <osg/LineWidth>
#include <osg/LineStipple>
#include <osgText/Text>

#include <Util/StringUtils.h>

#include <Core/IAnimationComponent.h>

#include <Elements/Model.h>
#include <Elements/IAnimatedModel.h>

#include <QFileDialog>

#include <Core/IAnimationComponent.h>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <Core/ITerrain.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include "boost/date_time/gregorian/gregorian.hpp"
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, MilitaryPathAnimationGUI);
    DEFINE_IREFERENCED(MilitaryPathAnimationGUI, SMCQt::DeclarativeFileGUI);

    const std::string MilitaryPathAnimationGUI::EventContextualMenuObjectName = "eventContextual";

    MilitaryPathAnimationGUI::MilitaryPathAnimationGUI()
        : _mode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_NONE)
        , _selectedGPXNodeType(NULL)
        , _selectedGPXNodeSegment(NULL)
        , _bAttachedCheckpointsFromFile(false)
        , _stages(-2)
    {}

    MilitaryPathAnimationGUI::~MilitaryPathAnimationGUI()
    {}

    void MilitaryPathAnimationGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        CORE::RefPtr<SMCUI::IMilitarySymbolPointUIHandler> milSymbolUIHandler
            = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IMilitarySymbolPointUIHandler>(getGUIManager());
        _subscribe(milSymbolUIHandler.get(), *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType);
    }

    void MilitaryPathAnimationGUI::connectContextualMenu()
    {
        setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_CHECKPOINT);
        setActive(true);
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (eventContextual)
        {
            QObject::connect(eventContextual, SIGNAL(attachPath(bool)),
                this, SLOT(attachPath(bool)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(removePath()),
                this, SLOT(removePath()), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(addCheckpoint(double, QDateTime, QDateTime, QString)),
                this, SLOT(addCheckpoint(double, QDateTime, QDateTime, QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(showGhostAt(double)), this,
                SLOT(showGhostAt(double)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(selectCheckpoint(QString)),
                this, SLOT(selectCheckpoint(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(editCheckpoint(QString, double, QDateTime, QDateTime, QString)),
                this, SLOT(editCheckpoint(QString, double, QDateTime, QDateTime, QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(deleteCheckpoint(QString)),
                this, SLOT(deleteCheckpoint(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(tabChanged(QString)), this,
                SLOT(tabSelected(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(setEnableAxis(bool)), this,
                SLOT(setEnableAxis(bool)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(selectTimestampingMethod(QString)), this,
                SLOT(selectTimestampingMethod(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(selectType(QString)), this,
                SLOT(selectGPXType(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(selectSegment(QString)), this,
                SLOT(selectGPXTrackSegment(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(selectSegment(QString)), this,
                SLOT(selectGPXRoutes(QString)), Qt::UniqueConnection);


            QObject::connect(eventContextual, SIGNAL(showTrailChanged(bool)), this, SLOT(setTrailVisibility(bool)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(selectAnimationType(QString)), this, SLOT(selectAnimationType(QString)), Qt::UniqueConnection);

            double pathLength = _uiHandler->getPathLength();
            eventContextual->setProperty("pathLength", QVariant::fromValue(pathLength));

            bool pathAttached = false;
            if (pathLength != 0)
            {
                pathAttached = true;
            }

            eventContextual->setProperty("pathAttached", QVariant::fromValue(pathAttached));

            bool trailVisibility = _uiHandler->getTrailVisibility();
            eventContextual->setProperty("trailVisible", QVariant::fromValue(trailVisibility));

            // set current time
            const boost::posix_time::ptime& currentDateTime = _animationUIHandler->getCurrentDateTime();
            QDateTime qCurrentDateTime;
            VizQt::TimeUtils::BoostToQtDateTime(currentDateTime, qCurrentDateTime);

            eventContextual->setProperty("currentTime", QVariant::fromValue(qCurrentDateTime));
            eventContextual->setProperty("visibilityEventTime", QVariant::fromValue(qCurrentDateTime));
            eventContextual->setProperty("departureTime", QVariant::fromValue(qCurrentDateTime));
            eventContextual->setProperty("arrivalTime", QVariant::fromValue(qCurrentDateTime));

            _populateAnimatedList();

        }

        if (eventContextual != NULL)
        {
           eventContextual->setProperty("stages", QVariant::fromValue(_stages));
           eventContextual->setProperty("checkpointComboBoxList", "--Select--");
        }

    }

    void MilitaryPathAnimationGUI::updateAxis(osg::Vec3d point){

        patNode = new osg::PositionAttitudeTransform;

        osg::ref_ptr<osg::Node> node = osgDB::readNodeFile(UTIL::getDataDirPath() + "/models/axes.osgt");

        node->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF);

        CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = _uiHandler->getUnit()->getInterface<ELEMENTS::IModelHolder>();

        patNode->addChild(node->asGroup());
        patNode->setAttitude(modelHolder->getModelOrientation());

    }

    void MilitaryPathAnimationGUI::setEnableAxis(bool value){
        CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
        if (value){
            CORE::RefPtr<ELEMENTS::IMilitarySymbol> object = _uiHandler->getUnit();

            if (object.valid()){
                CORE::RefPtr<ELEMENTS::IModelHolder> milObject = object->getInterface<ELEMENTS::IModelHolder>();
                CORE::RefPtr<CORE::IPoint> point = milObject->getInterface<CORE::IPoint>();
                // add geometry Nodes to pat node
                updateAxis(point->getValue());

                osg::Vec3d position = point->getValue();
                patNode->setPosition(UTIL::CoordinateConversionUtils::GeodeticToECEF(UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(position)));
                group->addChild(patNode);
            }

            QObject* eventObject = _findChild(EventContextualMenuObjectName);
            CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = _uiHandler->getUnit()->getInterface<ELEMENTS::IModelHolder>();

            std::string facingDir = getDirection(modelHolder->getModelFacingDirection());
            if (facingDir == ""){
                facingDir = "- Y";
            }
            std::string normalDir = getDirection(modelHolder->getModelNormalDirection());
            if (normalDir == ""){
                normalDir = "  Z";
            }

            if (eventObject){
                eventObject->setProperty("facingDirection", QVariant::fromValue(QString::fromStdString(facingDir)));
                eventObject->setProperty("normalDirection", QVariant::fromValue(QString::fromStdString(normalDir)));
            }
        }
        else{
            if (patNode){
                group->removeChild(patNode);
            }
        }
    }

    std::string MilitaryPathAnimationGUI::getDirection(osg::Vec3d value){
        std::string returnDir;
        if (value == osg::Vec3d(0.0, 1.0, 0.0)){
            returnDir = "  Y";
        }
        else if (value == osg::Vec3d(1.0, 0.0, 0.0)){
            returnDir = "  X";
        }
        else if (value == osg::Vec3d(0.0, 0.0, 1.0)){
            returnDir = "  Z";
        }
        else if (value == osg::Vec3d(0.0, -1.0, 0.0)){
            returnDir = "- Y";
        }
        else if (value == osg::Vec3d(-1.0, 0.0, 0.0)){
            returnDir = "- X";
        }
        else if (value == osg::Vec3d(0.0, 0.0, -1.0)){
            returnDir = "- Z";
        }
        return returnDir;
    }

    void MilitaryPathAnimationGUI::disconnectContextualMenu()
    {
        if (_uiHandler.valid())
        {
            _uiHandler->setCheckpointVisibility(false);
        }
        if (patNode){
            CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());

            group->removeChild(patNode);
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }

        setActive(false);
        QObject* timelineSlider = _findChild("timelineSlider");
        if (timelineSlider != NULL)
        {
            timelineSlider->setProperty("profileType", QVariant::fromValue(0));
        }
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (eventContextual != NULL)
        {
            _stages = eventContextual->property("stages").toUInt();
        }

    }

    void MilitaryPathAnimationGUI::setAxis(QString facing, QString normal){
        if (facing == normal){
            emit showError("Axis Wrongly selected", "Please select both direction different");
        }
        else if (facing == ""){
            emit showError("Facing Axis", "Facing Axis not Selected");
        }
        else if (normal == ""){
            emit showError("Normal Axis", "Normal Axis not Selected");
        }
        else{
            selectedNormalAxis = normal.toStdString();
            selectedFacingAxis = facing.toStdString();
            osg::Vec3d facingD(0.0, 1.0, 0.0);
            osg::Vec3d normalD(0.0, 0.0, 1.0);
            // set facing direction as par user selection
            if (selectedFacingAxis == "  X"){
                osg::Vec3d f(1.0, 0.0, 0.0);
                facingD = f;
            }
            else if (selectedFacingAxis == "  Y"){
                osg::Vec3d f(0.0, 1.0, 0.0);
                facingD = f;
            }
            else if (selectedFacingAxis == "  Z"){
                osg::Vec3d f(0.0, 0.0, 1.0);
                facingD = f;
            }
            else if (selectedFacingAxis == "- X"){
                osg::Vec3d f(-1.0, 0.0, 0.0);
                facingD = f;
            }
            else if (selectedFacingAxis == "- Y"){
                osg::Vec3d f(0.0, -1.0, 0.0);
                facingD = f;
            }
            else if (selectedFacingAxis == "- Z"){
                osg::Vec3d f(0.0, 0.0, -1.0);
                facingD = f;
            }
            // set normal direction as par user selection
            if (selectedNormalAxis == "  X"){
                osg::Vec3d f(1.0, 0.0, 0.0);
                normalD = f;
            }
            else if (selectedNormalAxis == "  Y"){
                osg::Vec3d f(0.0, 1.0, 0.0);
                normalD = f;
            }
            else if (selectedNormalAxis == "  Z"){
                osg::Vec3d f(0.0, 0.0, 1.0);
                normalD = f;
            }
            else if (selectedNormalAxis == "- X"){
                osg::Vec3d f(-1.0, 0.0, 0.0);
                normalD = f;
            }
            else if (selectedNormalAxis == "- Y"){
                osg::Vec3d f(0.0, -1.0, 0.0);
                normalD = f;
            }
            else if (selectedNormalAxis == "- Z"){
                osg::Vec3d f(0.0, 0.0, -1.0);
                normalD = f;
            }

            CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = _uiHandler->getUnit()->getInterface<ELEMENTS::IModelHolder>();

            modelHolder->setModelFacingDirection(facingD);
            modelHolder->setModelNormalDirection(normalD);
        }
    }

    void MilitaryPathAnimationGUI::tabSelected(QString name)
    {
        if (name == "Movement")
        {
            int stages = 0;
            QObject* eventContextual = _findChild(EventContextualMenuObjectName);
            if (eventContextual != NULL)
            {
                stages = eventContextual->property("stages").toUInt();
                if (stages != -2)
                {
                    eventContextual->setProperty("checkpointComboBoxList", "--Select--");
                }
            }
            
            if (0 != _uiHandler->getNumCheckpoints())
                _populateCheckpointModel();

            if (_uiHandler.valid())
            {
                _uiHandler->setCheckpointVisibility(true);
            }
        }
        else
        {
            //_setContextProperty("checkpointListModel", QVariant::fromValue(NULL));
            //_setContextProperty("timelineModel" , QVariant::fromValue(NULL));

            if (_uiHandler.valid())
            {
                _uiHandler->setCheckpointVisibility(false);
            }
        }
    }

    void MilitaryPathAnimationGUI::selectCheckpoint(QString cpID)
    {
        int id = cpID.toInt();
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        int diffvalue = 1;
        if (id == 0) // this means Add checkpoint mode is selected
        {
            int stages = 0;
            if (eventContextual != NULL)
            {
                stages = eventContextual->property("stages").toUInt();
            }
            if (stages == 3 && _bAttachedCheckpointsFromFile)
            {
                _manualCheckpoints();
            }
            else if (false == _bAttachedCheckpointsFromFile && (stages == -1 || stages == 0))
            {
                _parseAndAttachTimestmapNode();
            }
            return;
        }
        else if (id == 1 && !_bAttachedCheckpointsFromFile)
        {
            diffvalue = 2;
            eventContextual->setProperty("checkpointMode", "Add Checkpoint");
        }

        CORE::RefPtr<ELEMENTS::ICheckpoint> cp = _uiHandler->getCheckpoint(id - diffvalue);
        if (cp.valid())
        {
            const boost::posix_time::ptime& arrivalDateTime = cp->getArrivalTime();
            const boost::posix_time::ptime& departureTime = cp->getDepartureTime();

            QDateTime qArriavalDateTime;
            QDateTime qDepartureDateTime;
            VizQt::TimeUtils::BoostToQtDateTime(arrivalDateTime, qArriavalDateTime);
            VizQt::TimeUtils::BoostToQtDateTime(departureTime, qDepartureDateTime);

            double positionOnPath = cp->getDistanceOnPath();

            if (eventContextual != NULL)
            {
                eventContextual->setProperty("arrivalTime", QVariant::fromValue(qArriavalDateTime));
                eventContextual->setProperty("departureTime", QVariant::fromValue(qDepartureDateTime));
                QMetaObject::invokeMethod(eventContextual, "setPositionOnPath",
                    Q_ARG(QVariant, QVariant(positionOnPath)));
            }
        }
    }

    void MilitaryPathAnimationGUI::editCheckpoint(QString cpID, double position,
        QDateTime qArrivalDateTime, QDateTime qDepartureDateTime, QString animationType)
    {
        int id = cpID.toInt();
        if (id == 0) // this means Add checkpoint mode is selected
        {
            return;
        }
        CORE::RefPtr<ELEMENTS::ICheckpoint> cp = _uiHandler->getCheckpoint(id - 1);
        if (cp.valid())
        {
            boost::posix_time::ptime arrivalTime(VizQt::TimeUtils::QtToBoostDateTime(qArrivalDateTime));
            boost::posix_time::ptime departureTime(VizQt::TimeUtils::QtToBoostDateTime(qDepartureDateTime));

            _uiHandler->removeCheckPoint(cp.get());
            bool checkpointAdded = _uiHandler->addCheckpoint(arrivalTime, departureTime, position, animationType.toStdString());

            if (!checkpointAdded)
            {

                emit showError("Edit Checkpoint", "Invalid arrival or departure time.", false);

                // reinsert the previous checkpoint
                _uiHandler->addCheckpoint(cp.get());
            }
            else
            {
                _checkMissionStartEndTimeWith(qArrivalDateTime, qDepartureDateTime);
                _populateCheckpointModel();
            }
        }
    }

    void MilitaryPathAnimationGUI::deleteCheckpoint(QString cpID)
    {
        int id = cpID.toInt();
        if (id == 0) // this means Add checkpoint mode is selected
        {
            return;
        }
        CORE::RefPtr<ELEMENTS::ICheckpoint> cp = _uiHandler->getCheckpoint(id - 1);
        if (cp.valid())
        {
            _uiHandler->removeCheckPoint(cp.get());
        }

        _populateCheckpointModel();

    }

    void MilitaryPathAnimationGUI::_populateCheckpointModel()
    {
        if (!_animationUIHandler.valid())
        {
            emit showError("Timeline", "Animation handler is not valid.");
            return;
        }

        //get start and end time
        const boost::posix_time::ptime& startDateTime = _animationUIHandler->getStartDateTime();
        const boost::posix_time::ptime& endDateTime = _animationUIHandler->getEndDateTime();

        // get Mission duration
        boost::posix_time::time_duration duration = (endDateTime - startDateTime);
        int totalSeconds = duration.total_seconds();

        int numCheckpoints = _uiHandler->getNumCheckpoints();
        double elapsedSeconds = 0;
        double distance = 0;
        QColor movingColor("green");
        QColor stoppedColor("red");
        QColor profileColor = stoppedColor;
        //        bool moving = false;

        QList<QObject*> timelineMarkerList;
        QList<QObject*> checkpointList;


        if (_bAttachedCheckpointsFromFile)
        {
            checkpointList.append(new VizComboBoxElement(CHECKPOINT_MANUAL, "0"));
        }
        else
        {
            checkpointList.append(new VizComboBoxElement(CHECKPOINT_ATTACHFILE, "0"));
            checkpointList.append(new VizComboBoxElement("New", "1"));
            QObject* eventContextual = _findChild(EventContextualMenuObjectName);
            if (eventContextual)
                eventContextual->setProperty("stages", QVariant::fromValue(-1));
        }

        for (int i = 0; i < numCheckpoints; i++)
        {
            CORE::RefPtr<ELEMENTS::ICheckpoint> cp = _uiHandler->getCheckpoint(i);
            if (cp.valid())
            {
                const boost::posix_time::ptime& arrivalTime = cp->getArrivalTime();
                const boost::posix_time::ptime& departureTime = cp->getDepartureTime();

                double arrivalTimeInSeconds = (arrivalTime - startDateTime).total_seconds();
                double departureTimeInSeconds = (departureTime - startDateTime).total_seconds();

                if (departureTimeInSeconds < arrivalTimeInSeconds)
                {
                    emit showError("Timeline", "InvalidCheckpoint.");
                    continue;
                }
                else
                {
                    // Previous mark
                    double posX = elapsedSeconds / totalSeconds;
                    //moving = false;
                    double deltaX = (arrivalTimeInSeconds - elapsedSeconds) / totalSeconds;
                    timelineMarkerList.append(new TimelineMovementMark(posX, profileColor, deltaX, distance));

                    distance = cp->getDistanceOnPath();
                    // mark for arrival time
                    // unit is not moving right now as departure time is greater than arrival time
                    profileColor = stoppedColor;
                    elapsedSeconds = arrivalTimeInSeconds;
                    posX = (elapsedSeconds / totalSeconds);
                    deltaX = (departureTimeInSeconds - elapsedSeconds) / totalSeconds;

                    QDateTime qArrivalDate;
                    VizQt::TimeUtils::BoostToQtDateTime(arrivalTime, qArrivalDate);
                    QString strin = qArrivalDate.toString("hh:mm:ss dd MMM yyyy");
                    std::string stdstring = strin.toStdString();
                    if (_bAttachedCheckpointsFromFile)
                        checkpointList.append(new VizComboBoxElement(qArrivalDate.toString("hh:mm:ss dd MMM yyyy"), QString::number(i + 1)));
                    else
                        checkpointList.append(new VizComboBoxElement(qArrivalDate.toString("hh:mm:ss dd MMM yyyy"), QString::number(i + 2)));

                    if (deltaX > 0)
                    {
                        timelineMarkerList.append(new TimelineMovementMark(posX, profileColor, deltaX, distance));
                    }

                    // departure time will be added in next iteration
                    // unit is moving after departure
                    profileColor = movingColor;
                    elapsedSeconds = departureTimeInSeconds;
                }
            }
        }

        double posX = elapsedSeconds / totalSeconds;
        double deltaX = (1 - posX);
        profileColor = stoppedColor;
        if (deltaX > 0.0)
        {
            timelineMarkerList.append(new TimelineMovementMark(posX, profileColor, deltaX, distance));
        }


        _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));
        _setContextProperty("timelineModel", QVariant::fromValue(timelineMarkerList));
        _setContextProperty("checkpointListModel", QVariant::fromValue(checkpointList));

        QObject* timelineSlider = _findChild("timelineSlider");
        if (timelineSlider != NULL)
        {
            timelineSlider->setProperty("profileType", QVariant::fromValue(2));
        }
    }

    void MilitaryPathAnimationGUI::addCheckpoint(double position, QDateTime qArrivalDateTime,
        QDateTime qDepartureDateTime, QString animationType)
    {
        boost::posix_time::ptime arrivalTime(VizQt::TimeUtils::QtToBoostDateTime(qArrivalDateTime));
        boost::posix_time::ptime departureTime(VizQt::TimeUtils::QtToBoostDateTime(qDepartureDateTime));

        bool checkpointAdded = _uiHandler->addCheckpoint(arrivalTime, departureTime, position,animationType.toStdString());

        if (!checkpointAdded)
        {
            emit showError("Add Checkpoint", "Invalid arrival or departure time.", false);
        }
        else
        {
            _checkMissionStartEndTimeWith(qArrivalDateTime, qDepartureDateTime);

            _populateCheckpointModel();
        }
    }

    // check if the new checkpoint added has bounds out of mission start/end time
    void MilitaryPathAnimationGUI::_checkMissionStartEndTimeWith(QDateTime qArrivalDateTime,
        QDateTime qDepartureDateTime)
    {
        // get the animation uihandler
        if (!_animationUIHandler.valid())
        {
            return;
        }

        // start/end time
        boost::posix_time::ptime startTime = _animationUIHandler->getStartDateTime();
        boost::posix_time::ptime endTime = _animationUIHandler->getEndDateTime();

        // arrival/departure time
        boost::posix_time::ptime arrivalTime(VizQt::TimeUtils::QtToBoostDateTime(qArrivalDateTime));
        boost::posix_time::ptime departureTime(VizQt::TimeUtils::QtToBoostDateTime(qDepartureDateTime));

        // flag to check out of bounds
        bool timeChanged = false;

        if (arrivalTime < startTime)
        {
            startTime = arrivalTime;
            timeChanged = true;
        }

        if (departureTime > endTime)
        {
            endTime = departureTime;
            timeChanged = true;
        }

        // if flag is true set new time
        if (timeChanged)
        {
            _animationUIHandler->setStartEndDateTime(startTime, endTime);

            QObject* timeline = _findChild("timeline");
            if (timeline)
            {
                QDateTime qStartDateTime;
                QDateTime qEndDateTime;

                VizQt::TimeUtils::BoostToQtDateTime(startTime, qStartDateTime);
                VizQt::TimeUtils::BoostToQtDateTime(endTime, qEndDateTime);

                // set start/end date/time
                timeline->setProperty("startDate", QVariant::fromValue(qStartDateTime));
                timeline->setProperty("endDate", QVariant::fromValue(qEndDateTime));
            }

        }
    }

    void MilitaryPathAnimationGUI::showGhostAt(double position)
    {
        _uiHandler->highlightPointAtDistance(position);
    }

    void MilitaryPathAnimationGUI::attachPath(bool active)
    {
        if (active)
        {
            setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_ASSOCIATE_PATH);
            setActive(active);


            /*QList<QObject*> checkpointListModel;
            checkpointListModel.append(new VizComboBoxElement(CHECKPOINT_ATTACHFILE, QString::number(0)));
            checkpointListModel.append(new VizComboBoxElement(CHECKPOINT_MANUAL, QString::number(1)));
            _setContextProperty("checkpointListModel", QVariant::fromValue(checkpointListModel));*/

            _subscribe(_uiHandler.get(), *SMCUI::IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType);
        }
        else
        {
            setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_NONE);
            setActive(active);
            _unsubscribe(_uiHandler.get(), *SMCUI::IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType);
        }
    }

    void MilitaryPathAnimationGUI::removePath()
    {
        emit question("Checkpoint Path", "Remove Attached Path ?", false,
            "handleRemovePathOkClicked()", "handleRemovePathCancelClicked()");
    }

    void MilitaryPathAnimationGUI::handleRemovePathOkClicked()
    {

        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (eventContextual != NULL)
        {
            eventContextual->setProperty("pathAttached", QVariant::fromValue(false));
            eventContextual->setProperty("stages", QVariant::fromValue(-2));
            eventContextual->setProperty("checkpointComboBoxList", "--Select--");
        }

        CORE::ILine* iLine = _uiHandler->getLineFromGpx();
        if (iLine)
        {
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            if (world.valid())
            {
                try
                {
                    world->removeObjectByID(&(iLine->getInterface<CORE::IBase>(true)->getUniqueID()));
                }
                catch (...){}
            }
        }

        setActive(false);
        setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_REMOVE_PATH);
        setActive(true);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }
    }

    // XXX - Move these slots to a proper place
    void MilitaryPathAnimationGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        // Check whether UI Handler is valid or not
        //if(value == getActive())
        //{
        //    return;
        //}

        // Focus UI Handler and activate GUI
        try
        {
            _uiHandler->getInterface<APP::IUIHandler>()->setFocus(value);

            if (value)
            {
                _uiHandler->setMode(_mode);
            }
            else
            {
                _uiHandler->setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_NONE);
                _reset();
            }

            VizQt::QtGUI::setActive(value);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void MilitaryPathAnimationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the Military path animation UIHandler and set it
            try
            {
                _animationUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());

                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IMilitaryPathAnimationUIHandler>(getGUIManager());

            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if (messageType == *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointSelectedMessageType)
        {
            CORE::RefPtr<SMCUI::IMilitarySymbolPointUIHandler> milSymbolUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager
                <SMCUI::IMilitarySymbolPointUIHandler>(getGUIManager());

            if (!milSymbolUIHandler.valid())
            {
                //! unable to get MilitarySymbolPointUIHandler
                return;
            }

            //! get the selected symbol
            CORE::RefPtr<ELEMENTS::IMilitarySymbol> milSymbol = milSymbolUIHandler->getMilitarySymbol();
            if (!milSymbol.valid())
            {
                //! Unable to find the selected symbol
                return;
            }

            // activate military path animation uihandler
            setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_CHECKPOINT);
            setActive(true);
            
            if (_stages == -1 || _stages == 3)
                _populateCheckpointModel();
        }
        else if (messageType == *SMCUI::IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType)
        {
            // Deactivate UIHandler

            QObject* eventContextual = _findChild(EventContextualMenuObjectName);
            if (eventContextual != NULL)
            {
                eventContextual->setProperty("pathAttached", QVariant::fromValue(true));

                double pathLength = _uiHandler->getPathLength();
                eventContextual->setProperty("pathLength", QVariant::fromValue(pathLength));
            }

            // activate military path animation uihandler
            setMode(SMCUI::IMilitaryPathAnimationUIHandler::MODE_CHECKPOINT);
            setActive(true);

            _populateCheckpointModel();
            _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));

            _populateAnimatedList();

            QList<QObject*> checkpointListModel;
            checkpointListModel.append(new VizComboBoxElement(CHECKPOINT_ATTACHFILE, QString::number(0)));
            checkpointListModel.append(new VizComboBoxElement(CHECKPOINT_MANUAL, QString::number(1)));
            _setContextProperty("checkpointListModel", QVariant::fromValue(checkpointListModel));

            if (NULL != eventContextual)
            {
                eventContextual->setProperty("checkpointComboBoxList", "--Select--");
                eventContextual->setProperty("stages", QVariant::fromValue(-2));
            }

            _unsubscribe(_uiHandler.get(), *SMCUI::IMilitaryPathAnimationUIHandler::MilitaryPathAttachedMessageType);
        }
        if (messageType == *CORE::IAnimationComponent::AnimationStopMessageType)
        {
            _uiHandler->getPathLength();
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void MilitaryPathAnimationGUI::setMode(int mode)
    {
        _mode = static_cast<SMCUI::IMilitaryPathAnimationUIHandler::AnimationMode>(mode);
    }

    void MilitaryPathAnimationGUI::_reset()
    {
        _mode = SMCUI::IMilitaryPathAnimationUIHandler::MODE_NONE;
    }

    void MilitaryPathAnimationGUI::setTrailVisibility(bool value)
    {
        if (_uiHandler.valid())
        {
            _uiHandler->setTrailVisibility(value);
        }
    }
    void MilitaryPathAnimationGUI::selectAnimationType(QString animationType)
    {
        if ("None" == animationType)
        {
            CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = _uiHandler->getUnit()->getInterface<ELEMENTS::IModelHolder>();
            if (modelHolder)
            {
                CORE::RefPtr<ELEMENTS::IModel> imodel = modelHolder->getModel();
                if (imodel.valid())
                {
                    CORE::RefPtr<ELEMENTS::IAnimatedModel> ianimModel = imodel->getInterface < ELEMENTS::IAnimatedModel >();
                    if (ianimModel.valid())
                    {
                        ianimModel->stop();
                    }
                }
            }
            return;
        }
        else
        {
            CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = _uiHandler->getUnit()->getInterface<ELEMENTS::IModelHolder>();
            if (modelHolder)
            {
                CORE::RefPtr<ELEMENTS::IModel> imodel = modelHolder->getModel();
                if (imodel.valid())
                {
                    CORE::RefPtr<ELEMENTS::IAnimatedModel> ianimModel = imodel->getInterface < ELEMENTS::IAnimatedModel >();
                    if (ianimModel.valid())
                    {
                        ianimModel->stop();
                        ianimModel->play(animationType.toStdString().c_str());
                    }
                }
            }
        }
    }
    void MilitaryPathAnimationGUI::_populateAnimatedList()
    {
        QList<QObject*> animatedList;
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);

      
        animatedList.append(new VizComboBoxElement("None", QString::number(0)));
        CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = _uiHandler->getUnit()->getInterface<ELEMENTS::IModelHolder>();
        std::vector<std::string> animationList;
        if (modelHolder)
        {
            CORE::RefPtr<ELEMENTS::IModel> imodel = modelHolder->getModel();
            if (imodel.valid())
            {
                CORE::RefPtr<ELEMENTS::IAnimatedModel> ianimModel = imodel->getInterface < ELEMENTS::IAnimatedModel >();
                if (ianimModel.valid())
                {
                    ianimModel->getAnimationList(animationList);
                }
            }
        }

        for (int i = 0; i < animationList.size(); ++i)
        {
            animatedList.append(new VizComboBoxElement(animationList[i].c_str(), QString::number(i + 1)));
        }

        if (eventContextual)
        {
            if (animationList.empty())
                eventContextual->setProperty("animatedPath", QVariant::fromValue(false));
            else
                eventContextual->setProperty("animatedPath", QVariant::fromValue(true));
        }
        _setContextProperty("animationTypeListModel", QVariant::fromValue(animatedList));
    }

    void MilitaryPathAnimationGUI::_trimMillisecondPart(std::string &trimString) const
    {
        int index = trimString.find_last_of(".");
        if (index != trimString.npos)
        {
            int i;
            for (i = index; i < trimString.size(); ++i)
            {
                if (trimString[i] == 'Z')
                    break;
            }
            if (i < trimString.size())
            {
                trimString.erase(index, i - index);
            }
        }
    }
    void MilitaryPathAnimationGUI::_parseGPXNodeInfoTrack(xercesc::DOMNode* node, ListfTrackPoints &trackpoints)
    {
        double eleve = -1.0;
        std::string time;
        boost::posix_time::ptime bsttime;
        double lat = 0.0, lon = 0.0;
        GPXTrackPoints trackpoint;
        xercesc::DOMNodeList* children = node->getChildNodes();
        int ninnerchildcount = children->getLength();
        for (int j = 0; j < ninnerchildcount; ++j)
        {

            xercesc::DOMNode* innerchild = children->item(j);
            if (innerchild && innerchild->getNodeName())
            {
                std::string name = xercesc::XMLString::transcode(innerchild->getNodeName());
                if (name == "ele")
                {
                    const XMLCh* pele = innerchild->getTextContent();
                    if (pele)
                        eleve = atof(xercesc::XMLString::transcode(pele));
                }
                else if (name == "time")
                {
                    const XMLCh* ptime = innerchild->getTextContent();
                    if (ptime)
                    {
                        time = xercesc::XMLString::transcode(ptime);
                        // As our application's granularity is to seconds only ingoring milli second part.

                        _trimMillisecondPart(time);

                        QDateTime datetimeqt1 = QDateTime::fromString(QString::fromStdString(time), "yyyy-MM-dd'T'HH:mm:ss'Z'");
                        bsttime = VizQt::TimeUtils::QtToBoostDateTime(datetimeqt1);
                    }
                }
            }
        }
        xercesc::DOMNamedNodeMap *attibMap = node->getAttributes();
        int count = attibMap->getLength();
        for (int i = 0; i < count; ++i)
        {
            xercesc::DOMNode* attrib = attibMap->item(i);
            const XMLCh* ch = attrib->getNodeName();
            if (ch)
            {
                std::string  tag = xercesc::XMLString::transcode(ch);
                if ("lat" == tag)
                    lat = atof(xercesc::XMLString::transcode(attrib->getTextContent()));
                else if ("lon" == tag)
                    lon = atof(xercesc::XMLString::transcode(attrib->getTextContent()));
            }
        }
        //TrackPoints* trakPtNode = new TrackPoints(eleve, time, lon, lat);
        trackpoint.setElevation(0.0);
        trackpoint.setTime(time);
        trackpoint.setBoostTime(bsttime);
        trackpoint.setLongitude(lon);
        trackpoint.setLatitude(lat);
        trackpoints.push_front(trackpoint);

    }
    bool compare(GPXTrackPoints& itr1, GPXTrackPoints& itr2)
    {
        try
        {
            return (itr1.getBoostTime() < itr2.getBoostTime()) ? true : false;
        }
        catch (std::bad_cast ex)
        {
            return false;
        }
    }
    void MilitaryPathAnimationGUI::_parseGPXRoute(xercesc::DOMNode* elementRoot, VecofRoutes &vecofRoutes)
    {
        if (NULL == elementRoot)
            return;
        xercesc::DOMNodeList* children = elementRoot->getChildNodes();
        int ncount = children->getLength();
        int count = 1;
        for (int i = 0; i < ncount; ++i)
        {
            xercesc::DOMNode* child = children->item(i);
            std::string name = xercesc::XMLString::transcode(child->getNodeName());
            if (name == "rtept")
            {
                _parseGPXNodeInfoTrack(child, vecofRoutes);
            }
        }
    }
    void MilitaryPathAnimationGUI::_parseGPXTrack(xercesc::DOMNode* elementRoot, MapofTrkSegment& segmentStruct)
    {
        xercesc::DOMNodeList* children = elementRoot->getChildNodes();
        int ncount = children->getLength();
        int count = 1;

        for (int i = 0; i < ncount; ++i)
        {
            xercesc::DOMNode* child = children->item(i);
            std::string name = xercesc::XMLString::transcode(child->getNodeName());
            if (name == "trkseg")
            {
                GPXTrackSeg& vecTrkPt = segmentStruct[_currentFile + " Track Segment - " + std::to_string(count++)];


                xercesc::DOMNodeList* innerchildren = child->getChildNodes();
                int ninnerchildcount = innerchildren->getLength();
                for (int j = 0; j < ninnerchildcount; ++j)
                {
                    xercesc::DOMNode* innerchild = innerchildren->item(j);
                    if (innerchild && innerchild->getNodeName())
                    {
                        std::string name2 = xercesc::XMLString::transcode(innerchild->getNodeName());
                        if (name2 == "trkpt")
                        {
							_parseGPXNodeInfoTrack(innerchild, vecTrkPt.getTrakPointList());
                        }
                    }
                }


                //vecTrkPt.getTrakPtInfoVector().sort(compare);
                //std::sort(vecTrkPt.getTrakPtInfoVector().begin(), vecTrkPt.getTrakPtInfoVector().end(),compare);
            }
        }

    }
    void MilitaryPathAnimationGUI::_parseXmlInfo(const std::string& fileName)
    {
        std::string gpxfile = fileName;
        gpxfile = osgDB::convertFileNameToUnixStyle(gpxfile);
        xercesc::XercesDOMParser* parser = new xercesc::XercesDOMParser();
        parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
        parser->setDoNamespaces(false);
        parser->setDoSchema(false);
        parser->setLoadExternalDTD(false);

        parser->parse(gpxfile.c_str());
        xercesc::DOMDocument* xmlDoc = parser->getDocument();

        xercesc::DOMElement* elementRoot = xmlDoc->getDocumentElement();
        if (!elementRoot)
        {
            return;
        }
        xercesc::DOMNodeList* children = elementRoot->getChildNodes();
        const  XMLSize_t nodeCount = children->getLength();
        int count = 1;
        bool bWaypoint = false;
        for (XMLSize_t x = 0; x < nodeCount; ++x)
        {
            xercesc::DOMNode* currentNode = children->item(x);
            std::string name = xercesc::XMLString::transcode(currentNode->getNodeName());
            if ("trk" == name)
            {
                GPXTrack &mapofsegments = _gpxData.getGPXTrackMap()[_currentFile + " Tracks"];
				_parseGPXTrack(currentNode, mapofsegments.getTrackSegmentMap());
            }
            else if ("wpt" == name)
            {
                ListfTrackPoints& waypoinTackPoint = _gpxData.getGPXWaypointsMap()[_currentFile + " Waypoints"];
                _parseGPXNodeInfoTrack(currentNode, waypoinTackPoint);
            }
            else if ("rte" == name)
            {
                MapofRoutes & route = _gpxData.getGPXRoutesMap();
                _parseGPXRoute(currentNode, route[_currentFile + " Routes"]);
            }
        }
        QList<QObject*> _checkpointListModel;



        MapofTracks::iterator itr = _gpxData.getGPXTrackMap().begin();
        int i = 1;
        _checkpointListModel.append(new VizComboBoxElement(CHECKPOINT_MANUAL, "0"));

        while (itr != _gpxData.getGPXTrackMap().end())
        {
            QString strselectTimestamp;
            _checkpointListModel.append(new VizComboBoxElement(itr->first.c_str(), QString::number(i++)));
            ++itr;
        }
        MapofWaypoint::iterator itrMWay = _gpxData.getGPXWaypointsMap().begin();
        while (itrMWay != _gpxData.getGPXWaypointsMap().end())
        {
            QString strselectTimestamp;
            _checkpointListModel.append(new VizComboBoxElement(itrMWay->first.c_str(), QString::number(i++)));
            ++itrMWay;
        }

        MapofRoutes::iterator itrRoute = _gpxData.getGPXRoutesMap().begin();
        while (itrRoute != _gpxData.getGPXRoutesMap().end())
        {
            QString strselectTimestamp;
            _checkpointListModel.append(new VizComboBoxElement(itrRoute->first.c_str(), QString::number(i++)));
            ++itrRoute;
        }
        _setContextProperty("checkpointListModel", QVariant::fromValue(_checkpointListModel));
        CORE::IWorldMaintainer* worldMaintainer = APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
    }
    void MilitaryPathAnimationGUI::_parseAndAttachTimestmapNode()
    {
        _bAttachedCheckpointsFromFile = true;
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (eventContextual != NULL)
        {
            eventContextual->setProperty("timeStampfile", QVariant::fromValue(_bAttachedCheckpointsFromFile));
        }
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "C:/";
        QString caption = "Browse for Timestamp file";
        QString filters = "Gpx file (*.gpx)";

        std::string l_strSelectedFileURL = "C:/";

        if (_uiHandler.valid())
            l_strSelectedFileURL = _uiHandler->getGpxFileURL();

        QString fileName = QFileDialog::getOpenFileName(parent, caption,
            l_strSelectedFileURL.c_str(), filters);
        //files = fileName;
        //_lastPath = osgDB::getFilePath(fileName.toStdString());


        if (fileName.isEmpty())
        {
            if (eventContextual != NULL)
            {
                eventContextual->setProperty("selectTimestampNode", QVariant::fromValue(false));
            }
            if (eventContextual != NULL)
            {
                //eventContextual->setProperty("stages", QVariant::fromValue(-2));
            }
            _bAttachedCheckpointsFromFile = false;
            return;
        }

        if (eventContextual != NULL)
        {
            eventContextual->setProperty("selectTimestampNode", QVariant::fromValue(true));
        }


        QList<QObject*> checkpointListModel;

        /*       std::vector < MilitaryPathAnimationGUI::TrackSeg> trackSegment;
        if (!trackSegment.empty())
        trackSegment.clear();*/

        _gpxData.getGPXTrackMap().clear();
        _gpxData.getGPXRoutesMap().clear();
        _gpxData.getGPXWaypointsMap().clear();

        if (eventContextual != NULL)
        {
            eventContextual->setProperty("checkpointComboBoxList", "--Select--");
        }
        _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));

        CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(_manager);
        CORE::RefPtr<CORE::IWorldMaintainer> wm = iworld->getWorldMaintainer();
        osg::ref_ptr<UTIL::ThreadPool> tp = wm->getComputeThreadPool();

        _currentFile = "";// osgDB::getStrippedName(fileName.toStdString());

        _parseXmlInfo(fileName.toStdString());

        if (eventContextual != NULL)
        {
            eventContextual->setProperty("stages", QVariant::fromValue(1));
        }


        /**/
    }
    void MilitaryPathAnimationGUI::selectTimestampingMethod(QString selectedOption)
    {
        if (selectedOption == CHECKPOINT_ATTACHFILE)
        {
            _parseAndAttachTimestmapNode();
        }
        else if (selectedOption == CHECKPOINT_MANUAL)
        {
            _manualCheckpoints();
        }
    }


    void MilitaryPathAnimationGUI::_manualCheckpoints()
    {
        // if checkpoints selection mode changes from attached files to manual checkpoint 
        // Checkpoints will be removed.
        //if (_bAttachedCheckpointsFromFile != false)
        {
            int countCheckPoints = 0;
            while (countCheckPoints = _uiHandler->getNumCheckpoints())
            {
                CORE::RefPtr<ELEMENTS::ICheckpoint> cp = _uiHandler->getCheckpoint(0);
                if (cp.valid())
                {
                    _uiHandler->removeCheckPoint(cp.get());
                }
            }
        }
        _bAttachedCheckpointsFromFile = false;
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (eventContextual != NULL)
        {
            eventContextual->setProperty("pathAttached", QVariant::fromValue(true));
            eventContextual->setProperty("stages", QVariant::fromValue(-1));
            eventContextual->setProperty("checkpointMode", "Add Checkpoint");
            eventContextual->setProperty("checkpointComboBoxList", "--Select--");
            eventContextual->setProperty("timeStampfile", QVariant::fromValue(_bAttachedCheckpointsFromFile));
        }
        _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));

        _populateCheckpointModel();

    }

    void MilitaryPathAnimationGUI::selectGPXType(QString selectedOption)
    {
        if (selectedOption == CHECKPOINT_MANUAL)
        {
            _manualCheckpoints();
            return;
        }
        if (selectedOption.indexOf("Tracks", 0) != -1)
        {
            _selectTrackSegment(selectedOption);
        }
        else if (selectedOption.indexOf("Waypoints", 0) != -1)
        {
            _selectWaypointSegment(selectedOption);
        }
        else if (selectedOption.indexOf("Routes", 0) != -1)
        {
            _selectRoutePoints(selectedOption);
        }
    }

    void MilitaryPathAnimationGUI::selectGPXRoutes(QString selectedOption)
    {
        if (selectedOption.isEmpty())
            return;

        if (selectedOption == CHECKPOINT_MANUAL)
        {
            _manualCheckpoints();
            return;
        }
        if (selectedOption.indexOf("Routes") == -1)
        {
            return;
        }
        _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));


        MapofRoutes & routes = _gpxData.getGPXRoutesMap();
        MapofRoutes::iterator iterRoutes = routes.find(selectedOption.toStdString());
        QList<QObject*> checkpointListModel;
        int count = 0;
        boost::posix_time::ptime startTime, endTime;

        osg::Vec3d firstCordinate;
        osg::Vec3d presentCordinate;
        double distanceFromNode = 0;
        osg::Vec3d  previouscordinate;

        checkpointListModel.append(new VizComboBoxElement(CHECKPOINT_MANUAL, QString::number(0)));

        ListfTrackPoints::iterator itr = _selectedGPXNodeSegment->begin();
        if (itr != _selectedGPXNodeSegment->end())
        {
            endTime = startTime = itr->getBoostTime();
            if (firstCordinate.valid())
            {
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(_manager);
                osg::Vec3d pos(itr->getLatitude(), itr->getLongitude(), 0.0);
                double elevAtPoint1 = 0.0;
                //if (world)
                //{
                //    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

                //    //terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevAtPoint1);
                //    //pos.set(pos.x(), pos.y(), elevAtPoint1);
                //}
                //firstCordinate = osg::Vec3d(itr->getLongitude(), itr->getLatitude(), itr->getElevation());
                firstCordinate = pos;
                firstCordinate = UTIL::CoordinateConversionUtils::GeodeticToECEF(firstCordinate);
            }
            previouscordinate = firstCordinate;
            presentCordinate = firstCordinate;
            ++itr;
            bool checkpointAdded = false;

            if (_uiHandler.valid())
                checkpointAdded = _uiHandler->addCheckpoint(startTime, startTime, distanceFromNode);
            if (false == checkpointAdded)
            {
                emit showError("Add Checkpoint", "Invalid arrival or departure time.", false);
                return;
            }
        }
        unsigned long time = GetTickCount();
        for (; itr != _selectedGPXNodeSegment->end(); ++itr)
        {
            std::string datetime = itr->getTime();

            // Time in gpx file is or ISO Extended ISO 6801 format.
            // Parsing that format.

            endTime = itr->getBoostTime();

            //presentCordinate = osg::Vec3d(itr->getLongitude(), itr->getLatitude(), itr->getElevation());
            //CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(_manager);
            //osg::Vec3d pos(itr->getLatitude(), itr->getLongitude(), 0.0);
            osg::Vec3d pos(itr->getLatitude(), itr->getLongitude(), 0.0);
            /*double elevAtPoint1 = 0.0;
            if (world)
            {
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

            terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevAtPoint1);
            pos.set(pos.x(), pos.y(), elevAtPoint1);
            }*/
            //firstCordinate = osg::Vec3d(itr->getLongitude(), itr->getLatitude(), itr->getElevation());
            presentCordinate = UTIL::CoordinateConversionUtils::GeodeticToECEF(pos);

            distanceFromNode += (presentCordinate - previouscordinate).length();
            previouscordinate = presentCordinate;

            bool checkpointAdded = false;

            if (_uiHandler.valid())
                checkpointAdded = _uiHandler->addCheckpoint(endTime, endTime, distanceFromNode);
            if (false == checkpointAdded)
            {
                emit showError("Add Checkpoint", "Invalid arrival or departure time. Add manual checkpoits", false);
                _manualCheckpoints();
                return;
            }
        }

        _startTime = startTime;
        _endTime = endTime;

        adjustStartandEndTime();

        _populateCheckpointModel();

        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (eventContextual)
            eventContextual->setProperty("stages", QVariant::fromValue(3));
    }
    void MilitaryPathAnimationGUI::selectGPXTrackSegment(QString selectedOption)
    {
        if (selectedOption.isEmpty())
            return;

        if (selectedOption == CHECKPOINT_MANUAL)
        {
            _manualCheckpoints();
            return;
        }
        if (selectedOption.indexOf("Track Segment") == -1)
        {
            return;
        }

        //Reseting combo-box to empty.
        _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));


        //Reseting default fo combobox value.
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (eventContextual)
        {
            eventContextual->setProperty("checkpointComboBoxList", "--Select--");
        }

        MapofTrkSegment::iterator itr2 = _selectedGPXNodeType->find(selectedOption.toStdString());
        int counter = 1;
        QList<QObject*> _checkpointListModel;
        _checkpointListModel.append(new VizComboBoxElement("New", QString::number(0)));

        if (itr2 != _selectedGPXNodeType->end())
        {
            boost::posix_time::ptime startTime, endTime;

            osg::Vec3d firstCordinate;
            osg::Vec3d presentCordinate;

            // getting refrence from track segment for processing.
			ListfTrackPoints& trackSegment = itr2->second.getTrakPointList();

			_selectedGPXNodeSegment = &itr2->second.getTrakPointList();

            double distanceFromNode = 0;

            osg::Vec3d  previouscordinate;

            ListfTrackPoints::iterator itr = trackSegment.begin();

            bool decreasingOrder = false;
            if (itr != trackSegment.end())
            {
                ListfTrackPoints::iterator secondIteratoritr = ++itr;
                if (secondIteratoritr != trackSegment.end())
                {
                    --itr;
                    if (secondIteratoritr->getBoostTime() < itr->getBoostTime())
                    {
                        decreasingOrder = true;
                    }
                }
            }

            itr = trackSegment.begin();
            if (itr->getTime().empty())
            {
                emit showError("Parse time line error", "No timsstamps in gpx file. Selecting manual checkpoints.");
                _manualCheckpoints();
                return;
            }

            if (decreasingOrder)
                trackSegment.sort(compare);

            itr = trackSegment.begin();
            if (itr != trackSegment.end())
            {
                endTime = startTime = itr->getBoostTime();
                if (firstCordinate.valid())
                {
                    CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(_manager);
                    osg::Vec3d pos(itr->getLatitude(), itr->getLongitude(), 0.0);
                    double elevAtPoint1 = 0.0;
                    //if (world)
                    //{
                    //    CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

                    //    //terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevAtPoint1);
                    //    //pos.set(pos.x(), pos.y(), elevAtPoint1);
                    //}
                    //firstCordinate = osg::Vec3d(itr->getLongitude(), itr->getLatitude(), itr->getElevation());
                    firstCordinate = pos;
                    firstCordinate = UTIL::CoordinateConversionUtils::GeodeticToECEF(firstCordinate);
                }
                previouscordinate = firstCordinate;
                presentCordinate = firstCordinate;
                ++itr;
                bool checkpointAdded = false;

                if (_uiHandler.valid())
                    checkpointAdded = _uiHandler->addCheckpoint(startTime, startTime, distanceFromNode);
                if (false == checkpointAdded)
                {
                    emit showError("Add Checkpoint", "Invalid arrival or departure time.", false);
                    return;
                }
            }
            unsigned long time = GetTickCount();
            for (; itr != trackSegment.end(); ++itr)
            {
                std::string datetime = itr->getTime();

                // Time in gpx file is or ISO Extended ISO 6801 format.
                // Parsing that format.

                endTime = itr->getBoostTime();

                //presentCordinate = osg::Vec3d(itr->getLongitude(), itr->getLatitude(), itr->getElevation());
                //CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(_manager);
                //osg::Vec3d pos(itr->getLatitude(), itr->getLongitude(), 0.0);
                osg::Vec3d pos(itr->getLatitude(), itr->getLongitude(), 0.0);
                /*double elevAtPoint1 = 0.0;
                if (world)
                {
                CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();

                terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevAtPoint1);
                pos.set(pos.x(), pos.y(), elevAtPoint1);
                }*/
                //firstCordinate = osg::Vec3d(itr->getLongitude(), itr->getLatitude(), itr->getElevation());
                presentCordinate = UTIL::CoordinateConversionUtils::GeodeticToECEF(pos);

                distanceFromNode += (presentCordinate - previouscordinate).length();
                previouscordinate = presentCordinate;

                bool checkpointAdded = false;

                if (_uiHandler.valid())
                    checkpointAdded = _uiHandler->addCheckpoint(endTime, endTime, distanceFromNode);
                if (false == checkpointAdded)
                {
                    emit showError("Add Checkpoint", "Invalid arrival or departure time.", false);
                    return;
                }
            }

            _startTime = startTime;
            _endTime = endTime;

            adjustStartandEndTime();

            _populateCheckpointModel();
        }

        if (eventContextual)
        {
            eventContextual->setProperty("stages", QVariant::fromValue(3));
            eventContextual->setProperty("checkpointComboBoxList", "--Select--");

        }
    }

    void MilitaryPathAnimationGUI::_selectTrackSegment(QString selectedOption)
    {

        _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));
        std::string selectedString = selectedOption.toStdString();
        if (selectedOption == CHECKPOINT_MANUAL)
        {
            _manualCheckpoints();
            return;
        }



        MapofTracks::iterator itr = _gpxData.getGPXTrackMap().find(selectedOption.toStdString());

        QList<QObject*> checkpointListModel;
        checkpointListModel.append(new VizComboBoxElement(CHECKPOINT_MANUAL, "0"));

        if (itr != _gpxData.getGPXTrackMap().end())
        {
            _selectedGpxType = selectedOption;
			MapofTrkSegment::iterator itr2 = itr->second.getTrackSegmentMap().begin();
            int i = 1;
			_selectedGPXNodeType = &itr->second.getTrackSegmentMap();
            while (itr2 != _selectedGPXNodeType->end())
            {
                QString strselectTimestamp;
                checkpointListModel.append(new VizComboBoxElement(itr2->first.c_str(), QString::number(i++)));
                ++itr2;
            }
            _setContextProperty("checkpointListModel", QVariant::fromValue(checkpointListModel));
        }
        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (NULL != eventContextual)
        {
            eventContextual->setProperty("checkpointComboBoxList", "--Select--");
            eventContextual->setProperty("stages", QVariant::fromValue(2));
        }


    }
    void MilitaryPathAnimationGUI::_selectWaypointSegment(QString selectedOption)
    {
        if (selectedOption.isEmpty())
        {
            return;
        }
        if (selectedOption == CHECKPOINT_MANUAL)
        {
            _manualCheckpoints();
            return;
        }
        boost::posix_time::ptime startTime, endTime;
        bool firstVal = false;

        osg::Vec3d firstCordinate;
        osg::Vec3d presentCordinate;
        const MapofWaypoint & watpointMap = _gpxData.getGPXWaypointsMap();
        MapofWaypoint::const_iterator itrMap = watpointMap.find(selectedOption.toStdString());
        if (itrMap != watpointMap.end())
        {

            ListfTrackPoints::const_iterator itr = itrMap->second.begin();

            unsigned long distanceFromNode = 0;
            osg::Vec3d  previouscordinate;

            for (; itr != itrMap->second.end(); ++itr)
            {
                const GPXTrackPoints& TrackPoints = *itr;
                const std::string datetime = TrackPoints.getTime();
                if (!datetime.empty())
                {
                    // Time in gpx file is or ISO Extended ISO 6801 format.
                    // Parsing that format.

                    osg::Vec3d  previouscordinate;
                    if (false == firstVal)
                    {
                        firstVal = true;
                        startTime = TrackPoints.getBoostTime();
                        if (firstCordinate.valid())
                        {
                            firstCordinate = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(TrackPoints.getLatitude(), TrackPoints.getLongitude(), TrackPoints.getElevation()));
                        }
                        previouscordinate = firstCordinate;
                    }
                    else
                    {
                        previouscordinate = presentCordinate;
                    }
                    endTime = TrackPoints.getBoostTime();
                    presentCordinate = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(TrackPoints.getLatitude(), TrackPoints.getLongitude(), TrackPoints.getElevation()));
                    int len = (presentCordinate - previouscordinate).length();
                    distanceFromNode += len;

                    bool checkpointAdded = false;
                    if (_uiHandler.valid())
                        checkpointAdded = _uiHandler->addCheckpoint(endTime, endTime, distanceFromNode);

                    if (false == checkpointAdded)
                    {
                        emit showError("Add Checkpoint", "Invalid arrival or departure time.", false);
                        return;
                    }
                }
                else
                {
                    emit showError("Add Checkpoint", "No time checkpoints in waypoints", false);
                    _manualCheckpoints();
                    return;
                }
            }

            _startTime = startTime;
            _endTime = endTime;

            adjustStartandEndTime();

            _populateCheckpointModel();
        }
    }
    void MilitaryPathAnimationGUI::_selectRoutePoints(QString selectedOption)
    {
        if (selectedOption.isEmpty())
            return;
        if (selectedOption == CHECKPOINT_MANUAL)
        {
            _manualCheckpoints();
            return;
        }
        _setContextProperty("checkpointListModel", QVariant::fromValue(NULL));


        MapofRoutes & routes = _gpxData.getGPXRoutesMap();
        MapofRoutes::iterator iterRoutes = routes.find(selectedOption.toStdString());
        QList<QObject*> checkpointList;
        int count = 1;
        checkpointList.append(new VizComboBoxElement(CHECKPOINT_MANUAL, "0"));
        if (iterRoutes != routes.end())
        {
            checkpointList.append(new VizComboBoxElement(iterRoutes->first.c_str(), std::to_string(count++).c_str()));
            _selectedGPXNodeSegment = &iterRoutes->second;
        }
        _setContextProperty("checkpointListModel", QVariant::fromValue(checkpointList));

        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if (NULL != eventContextual)
        {
            eventContextual->setProperty("checkpointComboBoxList", "--Select--");
            eventContextual->setProperty("stages", QVariant::fromValue(2));
        }
    }
    void MilitaryPathAnimationGUI::adjustStartandEndTime()
    {
        QDateTime qStartDateTime;
        QDateTime qEndDateTime;

        VizQt::TimeUtils::BoostToQtDateTime(_startTime, qStartDateTime);
        VizQt::TimeUtils::BoostToQtDateTime(_endTime, qEndDateTime);
        std::stringstream str;
        str << " The GPS Playback time is from ";
        str <<"\""<<qStartDateTime.toString("hh:mm:ss dd MMM yyyy").toStdString().c_str()<<"\"";
        str << " to ";
        str << "\"" << qEndDateTime.toString("hh:mm:ss dd MMM yyyy").toStdString().c_str() << "\"";
        str << ". Do you want the Mission start time and end time changed to these limits ?";
        emit question("Mission time change.", str.str().c_str(), false,
            "okAdjustmentofTime()", "cancelAdjustmentofTime()");
    }
    void MilitaryPathAnimationGUI::okAdjustmentofTime()
    {
        QObject* timeline = _findChild("timeline");
        if (timeline)
        {
            QDateTime qStartDateTime;
            QDateTime qEndDateTime;

            VizQt::TimeUtils::BoostToQtDateTime(_startTime, qStartDateTime);
            VizQt::TimeUtils::BoostToQtDateTime(_endTime, qEndDateTime);

            // set start/end date/time
            timeline->setProperty("startDate", QVariant::fromValue(qStartDateTime));
            timeline->setProperty("endDate", QVariant::fromValue(qEndDateTime));

            if (_endTime > _startTime)
                _animationUIHandler->setStartEndDateTime(_startTime, _endTime);
            else
                _animationUIHandler->setStartEndDateTime(_endTime, _startTime);

            _populateCheckpointModel();


            bool animationRunning = _animationUIHandler->isAnimationRunning();
            timeline->setProperty("animationRunning", QVariant::fromValue(animationRunning));

            // set Slider range
            QObject* timelineSlider = timeline->findChild<QObject*>("timelineSlider");
            if (timelineSlider != NULL)
            {
                timelineSlider->setProperty("minimumValue", QVariant::fromValue(0));
                timelineSlider->setProperty("maximumValue", QVariant::fromValue((_endTime - _startTime).total_seconds()));
                timelineSlider->setProperty("value", QVariant::fromValue(0));
            }
        }
    }
    void MilitaryPathAnimationGUI::cancelAdjustmentofTime()
    {
        const boost::posix_time::ptime& startDateTime = _animationUIHandler->getStartDateTime();
        const boost::posix_time::ptime& endDateTime = _animationUIHandler->getEndDateTime();


        if (_startTime > _endTime)
            _animationUIHandler->setStartEndDateTime(_startTime, _endTime);
        else
            _animationUIHandler->setStartEndDateTime(_endTime, _startTime);

        _populateCheckpointModel();

        _animationUIHandler->setStartEndDateTime(startDateTime, endDateTime);
    }
} // namespace indiGUI
