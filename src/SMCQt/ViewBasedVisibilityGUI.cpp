#include <SMCQt/ViewBasedVisibilityGUI.h>

#include <VizUI/ISelectionUIHandler.h>

#include <App/AccessElementUtils.h>

#include <View/ICameraComponent.h>
#include <View/ViewPlugin.h>
#include <View/IViewBasedVisibility.h>

#include <Core/IProperty.h>
#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ViewBasedVisibilityGUI);

    ViewBasedVisibilityGUI::ViewBasedVisibilityGUI()
    {

    }

    ViewBasedVisibilityGUI::~ViewBasedVisibilityGUI()
    {}

    void ViewBasedVisibilityGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }
    }

    void ViewBasedVisibilityGUI::popupLoaded(QString type)
    {
        if(type == "viewBasedVisibilityPopup")
        {
            QObject* viewBasedVisibilityPopup = _findChild("viewBasedVisibilityPopup");
            if(viewBasedVisibilityPopup)
            {
                //! get current object

                //! Check weather current object has ViewBasedVisibilityProperty
                //! Get the current selection
                CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

                const CORE::ISelectionComponent::SelectionMap& slMap = selectionUIHandler->getCurrentSelection();
                
                if(slMap.empty())
                    return;

                _currentSelection = slMap.begin()->second;

                if(!_currentSelection.valid())
                    return;

                CORE::RefPtr<CORE::IProperty> vbvProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(VIEW::ViewCoreRegistryPlugin::ViewBasedVisibilityPropertyType.get());
                if(vbvProp.valid())
                {
                    
                    VIEW::IViewBasedVisibility* vbvProperty = vbvProp->getInterface<VIEW::IViewBasedVisibility>();
                    if(vbvProperty == NULL)
                        return;

                    QMetaObject::invokeMethod(viewBasedVisibilityPopup, "setPropEnabled", 
                        Q_ARG(QVariant, QVariant(true)));

                    //! max LOD --> min Altitude
                    QMetaObject::invokeMethod(viewBasedVisibilityPopup, "setMaxLod",            
                        Q_ARG(QVariant, QVariant(vbvProperty->getMinAltitude())));

                    //! min LOD --> max Altitude
                    QMetaObject::invokeMethod(viewBasedVisibilityPopup, "setMinLod", 
                        Q_ARG(QVariant, QVariant(vbvProperty->getMaxAltitude())));

                }

                QObject::connect(viewBasedVisibilityPopup, SIGNAL(togleProperty(bool)), this, SLOT(_toggleViewBasedVisProp(bool)));
                QObject::connect(viewBasedVisibilityPopup, SIGNAL(setLOD(QString,int)), this, SLOT(_setLOD(QString,int)));
                QObject::connect(viewBasedVisibilityPopup, SIGNAL(closed()), this, SLOT(_popupClosed()));
                //QObject::connect(elementList, SIGNAL(goToLod(int, bool)), this, SLOT(_goToLod(int, bool)));

                if(!_cameraComponent.valid())
                {
                    CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
                    CORE::IComponent* component = wm->getComponentByName("CameraComponent");
                    if(component)
                    {
                        _cameraComponent = component->getInterface<VIEW::ICameraComponent>();
                    }
                }

                if(_cameraComponent.valid())
                {
                    int currLod = (int)_cameraComponent->getRange();
                    if(currLod > 15000000)
                    {
                        currLod = 15000000;
                        _cameraComponent->setRange(currLod);
                    }
                    viewBasedVisibilityPopup->setProperty("currentLOD", currLod);
                }

                CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();  
                _subscribe(wm, *CORE::IWorldMaintainer::TickMessageType);
            }
        }
    }


    void ViewBasedVisibilityGUI::_popupClosed()
    {
        CORE::IWorldMaintainer* wm = CORE::WorldMaintainer::instance();
        _unsubscribe(wm, *CORE::IWorldMaintainer::TickMessageType);
    }

    void ViewBasedVisibilityGUI::_toggleViewBasedVisProp(bool value)
    {
        if(!_currentSelection.valid())
        {
            LOG_ERROR("Selection cleared!!!");
        }

        if(value)
        {
            //! create property
            CORE::RefPtr<CORE::IProperty> vbcProp = CORE::CoreRegistry::instance()->getPropertyFactory()->createProperty(*VIEW::ViewCoreRegistryPlugin::ViewBasedVisibilityPropertyType);

            _currentSelection->getInterface<CORE::IObject>()->addProperty(vbcProp.get());
        }
        else
        {
            //! remove property
            _currentSelection->getInterface<CORE::IObject>()->removeProperty(VIEW::ViewCoreRegistryPlugin::ViewBasedVisibilityPropertyType.get());
        }
    }

    void ViewBasedVisibilityGUI::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
        if(messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            _setVisualizationSpinners(); //Update spinner values with currnt lod
        }

        VizQt::GUI::update(messageType, message);
    }

    void ViewBasedVisibilityGUI::_setVisualizationSpinners()
    {
        QObject* viewBasedVisibilityPopup = _findChild("viewBasedVisibilityPopup");
        if(_cameraComponent.valid())
        {
            int lod  = (int)_cameraComponent->getRange();
            if(lod > 15000000){
                _cameraComponent->setRange((double)15000000);
                lod = 15000000;
            }
            if(viewBasedVisibilityPopup)
            {
                viewBasedVisibilityPopup->setProperty("currentLOD", lod);
            }
        }
    }

    void ViewBasedVisibilityGUI::_setLOD(QString type, int lod)
    {
        if(!_currentSelection.valid())
            return;

        //! Get property
        CORE::RefPtr<CORE::IProperty> vbcProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(VIEW::ViewCoreRegistryPlugin::ViewBasedVisibilityPropertyType.get());
        if(!vbcProp.valid())
            return;

        CORE::RefPtr<VIEW::IViewBasedVisibility> viewBasedVisibilityProperty = vbcProp->getInterface<VIEW::IViewBasedVisibility>();
        if(!viewBasedVisibilityProperty.valid())
            return;


        if(type.toStdString() == "max")                     //! max LOD --> min Altitude
        {
            viewBasedVisibilityProperty->setMinAltitude(lod);
        }
        else if(type.toStdString() == "min")                //! min LOD --> max Altitude
        {
            viewBasedVisibilityProperty->setMaxAltitude(lod);
        }
    }

    void ViewBasedVisibilityGUI::_goToLod(int lod, bool zoomin)
    {
        if(lod > 10)//minimum distance in osgearth is 10
        {
            if(_cameraComponent.valid())
            {
                _cameraComponent->setRange((double)lod);
            }
        }
    }




}