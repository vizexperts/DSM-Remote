/****************************************************************************
*
* File             : CPTourGUI.h
* Description      : CPTourGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/CPTourGUI.h>

#include <App/AccessElementUtils.h>
#include <Elements/ElementsUtils.h>
#include <Core/WorldMaintainer.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CPTourGUI);
    DEFINE_IREFERENCED(CPTourGUI, DeclarativeFileGUI);

    CPTourGUI::CPTourGUI()
        :_cpTourComponent(NULL)
        ,_animationUIHandler(NULL)
    {}

    CPTourGUI::~CPTourGUI()
    {}

    void CPTourGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* bottomLoader = _findChild("bottomLoader");
        if(bottomLoader != NULL)
        {
            QObject::connect(bottomLoader, SIGNAL(cptourMenuLoaded()), this,
                SLOT(connectTourMenu()), Qt::UniqueConnection);
        }
    }

    void CPTourGUI::connectTourMenu()
    {
        QObject* CheckpointTourMenu = _findChild("CheckpointTourMenu");
        if(CheckpointTourMenu)
        {
            //! Set tour name and time value
            unsigned int totalTime = _cpTourComponent->getTotalTime();
            CheckpointTourMenu->setProperty("totalTime", QVariant::fromValue(totalTime));

            QString tourName = "Tour Name";
            CheckpointTourMenu->setProperty("tourName", QVariant::fromValue(tourName));
            //! connect tour menu
            QObject::connect(CheckpointTourMenu, SIGNAL(playTour()), this, SLOT(playTour()), Qt::UniqueConnection);
            QObject::connect(CheckpointTourMenu, SIGNAL(pauseTour()), this, SLOT(pauseTour()), Qt::UniqueConnection);
            QObject::connect(CheckpointTourMenu, SIGNAL(stop()), this, SLOT(stopTour()), Qt::UniqueConnection);

            QObject::connect(CheckpointTourMenu, SIGNAL(increaseSpeedFactor()), 
                this, SLOT(increaseSpeedFactor()), Qt::UniqueConnection);

            QObject::connect(CheckpointTourMenu, SIGNAL(decreaseSpeedFactor()), 
                this, SLOT(decreaseSpeedFactor()), Qt::UniqueConnection);

            QObject::connect(CheckpointTourMenu, SIGNAL(closed()), 
                this, SLOT(closed()), Qt::UniqueConnection);

            //! subscribe to tick message
            CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

            _subscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);

        }
    }

    void CPTourGUI::playTour()
    {
        if(_animationUIHandler.valid())
        {
            _animationUIHandler->play();
        }
    }

    void CPTourGUI::pauseTour()
    {
        if(_animationUIHandler.valid())
        {
            _animationUIHandler->pause();
        }
    }

    void CPTourGUI::stopTour()
    {
        if(_animationUIHandler.valid())
        {
            _animationUIHandler->stop();
        }
    }

    void CPTourGUI::decreaseSpeedFactor()
    {
        if(_animationUIHandler.valid())
        {
            double timeScale = _animationUIHandler->getTimeScale();
            timeScale/=2.0;

            _animationUIHandler->setTimeScale(timeScale);

            QObject* CheckpointTourMenu = _findChild("CheckpointTourMenu");
            if(CheckpointTourMenu)
            {
                CheckpointTourMenu->setProperty("speedFactor", QVariant::fromValue(timeScale));
            }
        }
    }

    void CPTourGUI::increaseSpeedFactor()
    {
        if(_animationUIHandler.valid())
        {
            double timeScale = _animationUIHandler->getTimeScale();
            timeScale*=2.0;

            _animationUIHandler->setTimeScale(timeScale);

            QObject* CheckpointTourMenu = _findChild("CheckpointTourMenu");
            if(CheckpointTourMenu)
            {
                CheckpointTourMenu->setProperty("speedFactor", QVariant::fromValue(timeScale));
            }
        }
    }

    void CPTourGUI::closed()
    {
        //! unsubscribe to tick message
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
            APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

        _unsubscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);

        stopTour();

        //! reset Checkpoint Tour Component
        _cpTourComponent->reset();
    }

    void CPTourGUI::onAddedToGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void CPTourGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void CPTourGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _animationUIHandler = APP::AccessElementUtils::
                    getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());

                CORE::IComponent* component = 
                    ELEMENTS::GetComponentFromMaintainer("CheckpointPathTourComponent");

                if(component)
                {
                    _cpTourComponent = component->getInterface<SMCElements::ICheckpointPathTourComponent>();

                    _subscribe(_cpTourComponent.get(), *SMCElements::ICheckpointPathTourComponent::CPTourStartedMessageType);
                }

            }
            catch(const UTIL::Exception e)
            {
                e.LogException();
            }
        }
        else if(messageType == *SMCElements::ICheckpointPathTourComponent::CPTourStartedMessageType)
        {
            //! load CPTour GUI
            std::cout << "Tour Started" << std::endl;
            QObject* bottomLoader = _findChild("bottomLoader");
            if(bottomLoader)
            {
                QMetaObject::invokeMethod(bottomLoader, "load",
                    Q_ARG(QVariant, QVariant(QString("CheckpointTourPlayer.qml"))));
            }
        }
        else if(messageType == *CORE::WorldMaintainer::TickMessageType)
        {
            //! update time in GUI
            unsigned int elapsedTime = _cpTourComponent->getElapsedTime();
            QObject* CheckpointTourMenu = _findChild("CheckpointTourMenu");
            if(CheckpointTourMenu)
            {
                CheckpointTourMenu->setProperty("elapsedTime", QVariant::fromValue(elapsedTime));
            }
        }
    }
}
