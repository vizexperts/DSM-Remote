/*****************************************************************************
*
* File             : ViewshedAnalysisGUI.cpp
* Description      : ViewshedAnalysisGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <Core/CoreRegistry.h>
#include <Core/IDeletable.h>
#include <Core/ICompositeObject.h>
#include <Core/AttributeTypes.h>
#include <Core/InterfaceUtils.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <GIS/IPoint.h>
#include <GIS/IFeature.h>
#include <GIS/IGeometry.h>
#include <Core/ITerrain.h>
#include <Core/WorldMaintainer.h>

#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>

#include <VizUI/UIHandlerManager.h>

#include <VizQt/NameValidator.h>

#include <Terrain/IRasterObject.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <GISCompute/IViewShedVisitor.h>

#include <Util/StringUtils.h>
#include <iomanip>
#include <Util/FileUtils.h>

#include <SMCQt/ViewshedAnalysisGUI.h>
#include <SMCQt/ViewshedObserverObject.h>
#include <App/IApplication.h>

#include <qcolor.h>

#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/ReadFile>

#include <QFileDialog>

#include <DB/ReadFile.h>

#include <iostream>
#include <SMCQt/VizComboBoxElement.h>

#include <Database/IDatabase.h>
#include <Database/DatabaseUtils.h>
#include <DB/ReaderWriter.h>
#include <DB/ReaderWriterManager.h>
#include <Database/ISQLQueryResult.h>
#include <GIS/ISpatialiteDatabaseHolder.h>
#include <GIS/ISpatialDatabase.h>
#include <Database/ISQLQuery.h>

#include <Database/IDBTable.h>
#include <Database/IDBRecord.h>
#include <Database/IDBField.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ViewshedAnalysisGUI);

    const std::string ViewshedAnalysisGUI::ViewshedAnalysisObjectName = "ViewshedAnalysisObject";

    ViewshedAnalysisGUI::ViewshedAnalysisGUI()
        : _pointHandler(NULL),_nolookat(true),_fixedfov(true),noOfObjects(0)
    {
        _saveTifFilePath = UTIL::TemporaryFolder::instance()->getPath();
    }

    ViewshedAnalysisGUI::~ViewshedAnalysisGUI()
    {
        setActive(false);
    }

    CORE::RefPtr<DATABASE::ISQLQueryResult> executeStatement1(CORE::RefPtr<DATABASE::IDatabase> db, std::string queryStatement)
    {
        if(!db.valid())
            return NULL;

        CORE::RefPtr<DATABASE::ISQLQuery>  query = db->createSQLQuery();
        if(!query.valid())
            return NULL;

        if(!query->exec(queryStatement))
            return NULL;

        CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();

        return result;
    }



    void ViewshedAnalysisGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this,SIGNAL(_updateSuccessStatus()),this,SLOT(_handleSuccessStatus()),Qt::QueuedConnection);
    }

    // whenever on click on Viewshed button on right  bar
    void ViewshedAnalysisGUI::connectAnalysisLoader()
    {
        _tab = false;

        // Default list set to <None>
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("<None>", UTIL::ToString(0).c_str()));    

        _setContextProperty("layerColumnNameListModel", QVariant::fromValue(comboBoxList));
        _setContextProperty("layerColumnHeightListModel", QVariant::fromValue(comboBoxList));
        _setContextProperty("layerColumnRangeListModel", QVariant::fromValue(comboBoxList));

        _setContextProperty("layerColumnlookatLatListModel", QVariant::fromValue(comboBoxList));
        _setContextProperty("layerColumnlookatLongListModel", QVariant::fromValue(comboBoxList));
        _setContextProperty("layerColumnlookatEleListModel", QVariant::fromValue(comboBoxList));

        _setContextProperty("layerColumnverticalFOVListModel", QVariant::fromValue(comboBoxList));
        _setContextProperty("layerColumnhorizontalFOVListModel", QVariant::fromValue(comboBoxList));

        // set default save directory for tif
        QObject* addLayerPopup = NULL;
        QString fileName = _saveTifFilePath.c_str();

        addLayerPopup = _findChild(ViewshedAnalysisObjectName);

        if(addLayerPopup)
            addLayerPopup->setProperty("saveTifFilePath",QVariant::fromValue(fileName));


        //XXX NP use correct variable names, underscore is reserved for class variables
        QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);

        // connect the signal of various buttons in crest clearance qml
        if(viewshedAnalysisObject)
        {
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlHandleAddButtonClicked()), 
                this, SLOT(_handleAddButtonClicked()));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlHandleRemoveButtonClicked(int)), 
                this, SLOT(_handleRemoveButtonClicked(int)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlHandleStartButtonClicked()), 
                this, SLOT(_handleStartButtonClicked()));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlHandleStopButtonClicked()), this,
                SLOT(_handleStopButtonClicked()));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlHandleMarkButtonClicked(bool)), 
                this,SLOT(_handlePBmarkPositionClicked(bool)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlHandlePointButtonClicked(bool)), 
                this,SLOT(_handlePBPointPositionClicked(bool)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlBrowseButtonClicked()), 
                this,SLOT(_handleBrowseButtonClicked()));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlBuidingBrowseButtonClicked()), 
                this,SLOT(_handleBrowseBuildingShpButtonClicked()));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlSaveTifPathButton()), 
                this,SLOT(_handleSaveTifPathButton()));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlnameChanged(QString)), 
                this,SLOT(_handleNameChanged(QString)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlheightChanged(QString)), 
                this,SLOT(_handleHeightChanged(QString)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlrangeChanged(QString)), 
                this,SLOT(_handleRangeChanged(QString)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmltabChanged(bool)), 
                this,SLOT(_handletabChanged(bool)));

            QObject::connect( viewshedAnalysisObject, SIGNAL(qmllookatLatChanged(QString)), 
                this,SLOT(_handleLookatLatChanged(QString)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmllookatLongChanged(QString)), 
                this,SLOT(_handleLookatLongChanged(QString)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmllookatEleChanged(QString)), 
                this,SLOT(_handleLookatEleChanged(QString)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlverticalFOVChanged(QString)), 
                this,SLOT(_handleVerticalFOVChanged(QString)));
            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlhorizontalFOVChanged(QString)), 
                this,SLOT(_handleHorizontalFOVChanged(QString)));

            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlSensorChanged()), 
                this,SLOT(_handleSensorChanged()));

            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlHandleMarklookatLatLongButtonClicked(bool)), 
                this,SLOT(_handleMarklookatLatLongButtonClicked(bool)));

            QObject::connect( viewshedAnalysisObject, SIGNAL(qmlfixedfov(bool)), 
                this,SLOT(_handlefixedfov(bool)));

            QObject::connect(viewshedAnalysisObject, SIGNAL(closed()), this, SLOT(_viewShedClosed()));


        }

        //setting and appending label 
        comboBoxList.pop_back(); 
        comboBoxList.append(new VizComboBoxElement("Custom", UTIL::ToString(0).c_str()));
        _setContextProperty("sensorListModel", QVariant::fromValue(comboBoxList));

        QString qstr = "valid"; 
        viewshedAnalysisObject->setProperty("projectValid",qstr);

        //reading the sqlite database
        CORE::RefPtr<DATABASE::IDatabase> _database = DATABASE::ValidateDatabaseFile("Sensors.sqlite"); 

        if(!_database.valid())
        {
            qstr = "       Database not found ";
            viewshedAnalysisObject->setProperty("projectValid",qstr);
            return;
        }

        std::string metaQueryStatement = "select SensorType,Range from Sensor_Config";
        CORE::RefPtr<DATABASE::ISQLQueryResult> metaResult = executeStatement1(_database, metaQueryStatement);

        if(!metaResult.valid())
        {
            qstr = "      Database Invalid";
            viewshedAnalysisObject->setProperty("projectValid",qstr);
            return;
        }

        CORE::RefPtr<DATABASE::IDBTable> tableName = metaResult->getResultTable();

        if(!tableName.valid())
            return;

        int count = 0;
        CORE::RefPtr<DATABASE::IDBRecord> record = tableName->getNextRecord();

        while(record.valid())
        {
            DATABASE::IDBField* sensorType = record->getFieldByIndex(0);
            std::string sensor = sensorType->toString();
            comboBoxList.append(new VizComboBoxElement(sensor.c_str(), UTIL::ToString(count).c_str()));
            record = tableName->getNextRecord();
        }

        _setContextProperty("sensorListModel", QVariant::fromValue(comboBoxList));
    }

    void ViewshedAnalysisGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {

            CORE::RefPtr<CORE::IComponent> component = 
                CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

            if(component.valid())
            {
                _pointHandlerObj = component->getInterface<CORE::ISelectionComponent>();
            }

            // Query the LineUIHandler and set it
            try
            {
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the point is selected
        else if(messageType == *CORE::ISelectionComponent::ObjectClickedMessageType)
        {

            _pointHandlerObj;

            const CORE::ISelectionComponent::SelectionMap& map = 
                _pointHandlerObj->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();
            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                QString longitude;
                QString latitude;

                CORE::IPoint* pickedPoint = iter->second->getInterface<CORE::IPoint>();

                if(pickedPoint){
                    longitude = QString::number(pickedPoint->getX(), 'g', 6);
                    latitude  = QString::number(pickedPoint->getY(), 'g', 6);
                } else {
                    GIS::IFeature* myPointFeature = iter->second->getInterface<GIS::IFeature>();
                    if(myPointFeature){
                        GIS::IPoint *gisPickedPoint =  myPointFeature->getGeometry()->getInterface<GIS::IPoint>();
                        if(gisPickedPoint){
                            longitude = QString::number(gisPickedPoint->getValue()[0], 'g', 6);
                            latitude  = QString::number(gisPickedPoint->getValue()[1], 'g', 6);
                        }
                    }
                }

                if( longitude.length() && latitude.length() ) {
                    QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
                    if(viewshedAnalysisObject)
                    {
                        viewshedAnalysisObject->setProperty("leLong",longitude);
                        viewshedAnalysisObject->setProperty("leLat", latitude );
                    }
                }

            }

            /**/
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            if (_nolookat)
            {
                CORE::IPoint* point = _pointHandler->getPoint();
                _setViewerPoint(point);
                QString longitude = QString::number(point->getX(), 'g', 6);
                QString latitude  = QString::number(point->getY(), 'g', 6);
                QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
                if(viewshedAnalysisObject && _nolookat)
                {
                    viewshedAnalysisObject->setProperty("leLong",longitude);
                    viewshedAnalysisObject->setProperty("leLat", latitude );
                }
            }

            else
            {
                CORE::IPoint* point = _pointHandler->getPoint();
                _setLookatPoint(point);
                QString longitude = QString::number(point->getX(), 'g', 6);
                QString latitude  = QString::number(point->getY(), 'g', 6);

                QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
                if(viewshedAnalysisObject) 
                {
                    viewshedAnalysisObject->setProperty("leLookAtLong",longitude);
                    viewshedAnalysisObject->setProperty("leLookAtLat", latitude );

                }
            }


        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            // add object only if filter status is successful
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                // Show result on terrain
                CORE::RefPtr<CORE::IBase> visitor = message.getSender();
                CORE::RefPtr<TERRAIN::IRasterObject> robj = 
                    visitor->getInterface<GISCOMPUTE::IViewshedVisitor>()->getOutput();
                CORE::RefPtr<CORE::IWorld> world = 
                    APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
                world->addObject(robj->getInterface<CORE::IObject>());
                emit _updateSuccessStatus();
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
            {
                emit showError("Filter Status", "Failed to Apply Filter");
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void ViewshedAnalysisGUI::_viewShedClosed()
    {
    }

    void ViewshedAnalysisGUI::_handleSuccessStatus()
    {
        QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
        if(viewshedAnalysisObject)
        {
            noOfObjects--;
            if(noOfObjects == 0)
            {
                viewshedAnalysisObject->setProperty("calculating",false);
                viewshedAnalysisObject->setProperty("startButtonEnabled",true);
                viewshedAnalysisObject->setProperty("stopButtonEnabled",false);
            }
        }
    }
    void ViewshedAnalysisGUI::_handleViewshedAnalysisButtonPressed(const std::string& playEvent)
    {
        bool res = false;
        if(playEvent == "Start")
        {
            QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
            if(viewshedAnalysisObject)
            {
                viewshedAnalysisObject->setProperty("startButtonEnabled",false);
                viewshedAnalysisObject->setProperty("stopButtonEnabled",true);
                viewshedAnalysisObject->setProperty("calculating",true);
            }

            res = play();
        }
        else if(playEvent == "Stop")
        {
            QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
            if(viewshedAnalysisObject)
            {
                viewshedAnalysisObject->setProperty("startButtonEnabled",true);
                viewshedAnalysisObject->setProperty("stopButtonEnabled",false);
                viewshedAnalysisObject->setProperty("calculating",false);

            }

            res = stop();
        }

        else if(playEvent == "Exit")
        {
            stop();

            // Clear all objects
            std::map<std::string, CORE::RefPtr<CORE::IObject> >::iterator iter = _objectList.begin();
            for(; iter != _objectList.end(); ++iter)
            {
                _removeObject(iter->second.get());
            }

            _objectList.clear();
            _visitorList.clear();

            if(_viewerPoint.valid())
            {
                _removeObject(_viewerPoint.get());
                _viewerPoint = NULL;
            }

            if(_lookatPoint.valid())
            {
                _removeObject(_lookatPoint.get());
                _lookatPoint = NULL;
            }

            _reset();
        }
    }
    void ViewshedAnalysisGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                _reset();

                connectAnalysisLoader();

                QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
                if(viewshedAnalysisObject)
                {
                    viewshedAnalysisObject->setProperty("startButtonEnabled",true);
                    viewshedAnalysisObject->setProperty("stopButtonEnabled",false);
                }
            }
            else
            {
                _originalBuidingLayerPath.clear();
                _handlePBmarkPositionClicked(false);
                _handleMarklookatLatLongButtonClicked(false);

                _setContextProperty("layerColumnNameListModel", QVariant::fromValue(NULL));
                _setContextProperty("layerColumnHeightListModel", QVariant::fromValue(NULL));
                _setContextProperty("layerColumnRangeListModel", QVariant::fromValue(NULL));

                _setContextProperty("layerColumnlookatLatListModel", QVariant::fromValue(NULL));
                _setContextProperty("layerColumnlookatLongListModel", QVariant::fromValue(NULL));
                _setContextProperty("layerColumnlookatEleListModel", QVariant::fromValue(NULL));

                _setContextProperty("layerColumnverticalFOVListModel", QVariant::fromValue(NULL));
                _setContextProperty("layerColumnhorizontalFOVListModel", QVariant::fromValue(NULL));

                std::string exit("Exit");
                _handleViewshedAnalysisButtonPressed(exit);

            }

            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ViewshedAnalysisGUI::_handlePBPointPositionClicked(bool pressed)
    {
        if(_pointHandlerObj.valid())
        {            
            if(pressed)
            {
                _pointHandlerObj->setIntermediateSelection(true);
                _subscribe(_pointHandlerObj.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
            }
            else
            {
                _pointHandlerObj->setIntermediateSelection(false);
                _unsubscribe(_pointHandlerObj.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "Fail to initiate mark observer position");
        }
    }

    void ViewshedAnalysisGUI::_handleSaveTifPathButton()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption;
        QString filters;
        caption = "Browse for layer file";
        std::string label;
        std::string _lastPath;
        if(_lastPath.empty())
            _lastPath = directory.toStdString();

        QString files;

        QString fileName = QFileDialog::getExistingDirectory(parent, caption, 
            _lastPath.c_str());

        QObject* addLayerPopup = NULL;

        addLayerPopup = _findChild(ViewshedAnalysisObjectName);

        if(addLayerPopup)
        {
            addLayerPopup->setProperty("saveTifFilePath",QVariant::fromValue(fileName));
            _saveTifFilePath = fileName.toStdString();

            if(_saveTifFilePath.length() < 2)
                return;
        }

    }


    void ViewshedAnalysisGUI::_handleBrowseBuildingShpButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption;
        QString filters;
        caption = "Browse for layer file";
        std::string label;
        std::string _lastPath;
        if(_lastPath.empty())
            _lastPath = directory.toStdString();

        filters = "Vector File (*.shp *.gml *.pg *.kml)";

        QString files;

        QString fileName = QFileDialog::getOpenFileName(parent, caption, 
            _lastPath.c_str(), filters);

        QObject* addLayerPopup = NULL;

        addLayerPopup = _findChild(ViewshedAnalysisObjectName);

        if(addLayerPopup)
        {
            addLayerPopup->setProperty("selectedBuidingFileName",QVariant::fromValue(fileName));
            _originalBuidingLayerPath = fileName.toStdString();

            if(_originalBuidingLayerPath.length() < 2)
                return;

        }
    }

    void ViewshedAnalysisGUI::_handleBrowseButtonClicked()
    {
        _disableMarking();
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption;
        QString filters;
        caption = "Browse for layer file";
        std::string label;
        std::string _lastPath;
        if(_lastPath.empty())
            _lastPath = directory.toStdString();

        filters = "Vector File (*.shp *.gml *.pg *.kml)";

        QString files;

        QString fileName = QFileDialog::getOpenFileName(parent, caption, 
            _lastPath.c_str(), filters);

        QObject* addLayerPopup = NULL;

        addLayerPopup = _findChild(ViewshedAnalysisObjectName);

        if(addLayerPopup)
        {
            addLayerPopup->setProperty("selectedFileName",QVariant::fromValue(fileName));
            _originalLayerPath = fileName.toStdString();

            if(_originalLayerPath.length() < 2)
                return;

            QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);

            QColor rectColor = viewshedAnalysisObject->property("rectColorFile").value<QColor>();

            _viewShedPoints = OGRSFDriverRegistrar::Open(_originalLayerPath.c_str(), TRUE);

            int numPoints = 0;

            for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
            { 
                OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
                OGRFeature* ogrFeature = NULL;
                QList<QObject*> comboBoxList;

                if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
                {
                    int fieldCount = ogrFeature->GetFieldCount();

                    for(int i = 0; i < fieldCount; i++ ){
                        OGRFieldDefn *fieldDef = ogrFeature->GetFieldDefnRef(i);
                        if( fieldDef->GetType() ==  OFTInteger || fieldDef->GetType() ==  OFTReal || fieldDef->GetType() == OFTString){
                            comboBoxList.append(new VizComboBoxElement(fieldDef->GetNameRef(), UTIL::ToString(i).c_str()));    
                        }
                    }                    
                }

                _setContextProperty("layerColumnNameListModel", QVariant::fromValue(comboBoxList));
                _setContextProperty("layerColumnHeightListModel", QVariant::fromValue(comboBoxList));
                _setContextProperty("layerColumnRangeListModel", QVariant::fromValue(comboBoxList));

                _setContextProperty("layerColumnlookatLatListModel", QVariant::fromValue(comboBoxList));
                _setContextProperty("layerColumnlookatLongListModel", QVariant::fromValue(comboBoxList));
                _setContextProperty("layerColumnlookatEleListModel", QVariant::fromValue(comboBoxList));

                _setContextProperty("layerColumnverticalFOVListModel", QVariant::fromValue(comboBoxList));
                _setContextProperty("layerColumnhorizontalFOVListModel", QVariant::fromValue(comboBoxList));
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handlePBmarkPositionClicked(bool pressed)
    {
        if(_pointHandler.valid())
        {            
            if(pressed)
            {
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "Fail to initiate mark observer position");
        }
    }

    void ViewshedAnalysisGUI::_handleMarklookatLatLongButtonClicked(bool pressed)
    {
        _nolookat = !pressed;

        if(_pointHandler.valid())
        {            
            if(pressed)
            {
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "Fail to initiate mark observer position");
        }

       // _handlePBmarkPositionClicked(pressed);
    }



    bool ViewshedAnalysisGUI::play()
    {
        bool ret = false;
        _visitorList.clear();

        for(int i = 0; i < _twObserverList.size(); i++)
        {
            QString name = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->name();
            double lat = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->lat();
            double lon = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->longi();
            double height = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->height();
            double range = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->range();
            QColor color = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->color();

            double lookatlongi = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->lookatlongi();
            double lookatlat = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->lookatlat();
            double lookatele = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->lookatele();
            double horizontalFOV = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->horizontalFOV();
            double verticalFOV = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->verticalFOV();

            osg::Vec4d ocolor = osg::Vec4d(color.red() / 255.0, color.green() / 255.0, color.blue() / 255.0, color.alpha() / 255.0);

            osg::Vec3 pos(lon, lat, height);

            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
            CORE::IBase* applyNode = terrain->getInterface<CORE::IBase>();
            CORE::RefPtr<CORE::IVisitorFactory> vfactory = 
                CORE::CoreRegistry::instance()->getVisitorFactory();
            CORE::RefPtr<CORE::IBaseVisitor> visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::ViewshedVisitorType);
            GISCOMPUTE::IViewshedVisitor* viewshedVisitor = visitor->getInterface<GISCOMPUTE::IViewshedVisitor>();
            viewshedVisitor->setColor(ocolor);
            viewshedVisitor->setName(name.toStdString());
            viewshedVisitor->setFilePath(_saveTifFilePath);
            viewshedVisitor->setPosition(pos);
            viewshedVisitor->setRange(range);

            viewshedVisitor->setBuildingShpPath(_originalBuidingLayerPath.c_str());

            viewshedVisitor->setViewerLookAt(osg::Vec3(lookatlongi,lookatlat,lookatele));
            viewshedVisitor->setHoriAngle(horizontalFOV);
            viewshedVisitor->setVertAngle(verticalFOV);

            _visitorList[name.toStdString()] = visitor;
            _subscribe(visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

            applyNode->accept(*visitor);
            ret = true;
        }
        return ret;
    }

    bool ViewshedAnalysisGUI::stop()
    {
        bool ret = false;
        // Go through the list of filters and stop them
        for(VisitorList::iterator iter = _visitorList.begin();
            iter != _visitorList.end();
            ++iter)
        {
            CORE::RefPtr<CORE::IBaseVisitor> visitor = iter->second;
            try
            {
                visitor->getInterface<GISCOMPUTE::IFilterVisitor>(true)->stopFilter();
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
            ret = true;
        }

        return ret;
    }

    void ViewshedAnalysisGUI::_handleStartButtonClicked()
    {
        // get Label play Event and handle
        _disableMarking();
        noOfObjects = _twObserverList.size();
        if(!_twObserverList.isEmpty())
        {
            QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
            if (viewshedAnalysisObject)
            {
                std::string pbtext = viewshedAnalysisObject->property("startButtonLabel").toString().toStdString();  
                _handleViewshedAnalysisButtonPressed("Start");
            }
        }
        else {
            emit showError("Observer Error","Please enter at least one observer parameter");
        }
    }

    void ViewshedAnalysisGUI::_handleStopButtonClicked()
    {
        // get Label Stop Event and handle
        _disableMarking();
        QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
        if(viewshedAnalysisObject)
        {
            std::string pbtext = viewshedAnalysisObject->property("stopButtonLabel").toString().toStdString();  
            _handleViewshedAnalysisButtonPressed("Stop");
        }
    }

    void ViewshedAnalysisGUI::_disableMarking()
    {
        _handlePBmarkPositionClicked(false);
        _handleMarklookatLatLongButtonClicked(false);

        QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
        if(viewshedAnalysisObject)
        {
            viewshedAnalysisObject->setProperty("markviewerButton",false);
            viewshedAnalysisObject->setProperty("markLookatButton",false);
            viewshedAnalysisObject->setProperty("checkPickPosition",false);

        }
    }

    void ViewshedAnalysisGUI::_handletabChanged(bool value)
    {
        _tab = value;
        if (_tab)
        {
            _disableMarking();
        }
    }

    //! Called when choose add button pressed
    void ViewshedAnalysisGUI::_handleAddButtonClicked()
    {

        if(!_tab){

            _disableMarking();
            QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);

            if (viewshedAnalysisObject)
            {

                QString leName = viewshedAnalysisObject->property("leName").toString();
                if(leName =="")
                {
                    showError("Observer Name", "Please enter a observer name");
                    return;
                }

                std::string name = leName.toStdString();

                QColor rectColor = viewshedAnalysisObject->property("rectColor").value<QColor>();

                QString leLatTemp = viewshedAnalysisObject->property("leLat").toString();
                if(leLatTemp == "")
                {
                    showError("Observer Latitude", "Please specify latitude between -90 to 90");
                    return;
                }
                double leLat = leLatTemp.toDouble();

                QString leLongTemp = viewshedAnalysisObject->property("leLong").toString();
                if(leLongTemp == "")
                {
                    showError("Observer Longitude", "Please specify longitude between -180 to 180");
                    return;
                }
                double leLong = leLongTemp.toDouble();

                QString leHeightTemp = viewshedAnalysisObject->property("leHeight").toString();
                if(leHeightTemp == "")
                {
                    showError("Observer Height", "Please enter a valid height greater than Zero ");
                    return;
                }
                double leHeight = leHeightTemp.toDouble();

                QString leHFOVTemp = viewshedAnalysisObject->property("leHfov").toString();
                if(leHFOVTemp == "")
                {
                    showError("Observer Horizontal FOV", "Please enter a valid Horizontal FOV value ");
                    return;
                }
                double leHFOV = leHFOVTemp.toDouble();

                QString leVFOVTemp = viewshedAnalysisObject->property("leVfov").toString();
                if(leVFOVTemp == "")
                {
                    showError("Observer Vertical FOV", "Please enter a valid Vertical FOV value ");
                    return;
                }
                double leVFOV = leVFOVTemp.toDouble();


                QString leLatiTemp = viewshedAnalysisObject->property("leLookAtLat").toString();
                if(leLatiTemp == "")
                {
                    showError("Observer LookAt Lat", "Please enter a valid lookat latitude ");
                    return;
                }
                float leLati = leLatiTemp.toFloat();

                QString leLongiTemp = viewshedAnalysisObject->property("leLookAtLong").toString();
                if(leLongiTemp == "")
                {
                    showError("Observer LookAt Long", "Please enter a valid lookat longitude ");
                    return;
                }
                float leLongi = leLongiTemp.toFloat();

                QString leEleTemp = viewshedAnalysisObject->property("leLookAtEle").toString();
                if(leEleTemp == "")
                {
                    showError("Observer Elevation Angle", "Please enter a valid Elevation Angle ");
                    return;
                }
                double leEle = leEleTemp.toDouble();


                QString leRangeTemp = viewshedAnalysisObject->property("leRange").toString();
                if(leRangeTemp == "")
                {
                    showError("Observer Range", "Please enter a valid integer greater than 10 for Range");
                    return;
                }
                double leRange = leRangeTemp.toDouble();


                if(leRange < 10)
                {
                    showError("Observer Range", "Please enter a valid integer greater than 10 for Range");
                    return;
                }

                // check if the attribute name already exist
                if(! _twObserverList.isEmpty()) 
                {
                    int noOfRows = _twObserverList.size();
                    for(int i= 0; i < noOfRows; i++)
                    {
                        if((static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->name().toStdString() == leName.toStdString())
                        {
                            showError("Observer Name", "Observer name already exist");
                            return;
                        }
                    }
                }

                osg::Vec3f lookatPos(leLati,leLongi,leEle);
                _addAttributeToTable(leName, rectColor, leLong, leLat, leHeight, leRange, lookatPos, leHFOV, leVFOV);
            }

        } else {

            QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
            if (viewshedAnalysisObject)
            {
                //checking whether the vector data file is being added or not
                if(_originalLayerPath.length() < 2)
                {
                    showError("Add File", "Please add a vector file for Viewshed Analysis");
                    viewshedAnalysisObject->setProperty("isFilePathValid",false);
                    return;
                }
                
                viewshedAnalysisObject->setProperty("isFilePathValid",true);
                QColor rectColor = viewshedAnalysisObject->property("rectColorFile").value<QColor>();

                for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
                { 
                    OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
                    ogrLayer->ResetReading();
                    OGRFeature* ogrFeature = NULL;
                    while((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
                    {
                        int fieldCount = ogrFeature->GetFieldCount();

                        OGRGeometry* ogrGeometry = ogrFeature->GetGeometryRef();
                        OGRwkbGeometryType geometryType = ogrGeometry->getGeometryType();

                        if(geometryType == wkbPoint)
                        {
                            OGRPoint* ogrPoint = dynamic_cast<OGRPoint*>(ogrGeometry);
                            
                            _nameIndex = ogrFeature->GetFieldIndex("Name");
                            QString leName =ogrFeature->GetFieldAsString(_nameIndex);
                
                            QString Name = viewshedAnalysisObject->property("nameValue").toString();
                            std::string name = Name.toStdString();

                            //checking whether name attribute has been set from vector file or not
                            if(name =="<None>")
                            {
                                showError("Observer Name", "Please select a field for observer name from added vector file");
                                return;
                            }

                           
                            _heightIndex = ogrFeature->GetFieldIndex("Height");
                            double leHeight = ogrFeature->GetFieldAsDouble(_heightIndex);

                            //checking whether height attribute has been set from vector file or not
                            QString Height = viewshedAnalysisObject->property("heightValue").toString();
                            std::string height = Height.toStdString();
                            if(height == "<None>")
                            {
                                showError("Observer Height", "Please select a field for height from added vector file");
                                return;
                            }
                            
                            _rangeIndex = ogrFeature->GetFieldIndex("Range");
                            double leRange = ogrFeature->GetFieldAsDouble(_rangeIndex);

                            //checking whether range attribute has been set from vector file or not
                            QString Range = viewshedAnalysisObject->property("rangeValue").toString();
                            std::string range = Range.toStdString();
                            if(range == "<None>")
                            {
                                showError("Observer Range", "Please select a field for range from added vector file");
                                return;
                            }

                            _lookatLatIndex = ogrFeature->GetFieldIndex("LookAtLat");
                            _lookatLongIndex = ogrFeature->GetFieldIndex("LookAtLong");
                            _lookatEleIndex = ogrFeature->GetFieldIndex("ElevAngle");
                            osg::Vec3f lookatPos(ogrFeature->GetFieldAsDouble(_lookatLatIndex),ogrFeature->GetFieldAsDouble(_lookatLongIndex),ogrFeature->GetFieldAsDouble(_lookatEleIndex));

                            _horizontalFOVIndex = ogrFeature->GetFieldIndex("HFOV");
                            _verticalFOVIndex = ogrFeature->GetFieldIndex("VFOV");
                            double lehorizontalFOV = ogrFeature->GetFieldAsDouble(_horizontalFOVIndex);
                            double leverticalFOV = ogrFeature->GetFieldAsDouble(_verticalFOVIndex);
                            osg::Vec3f notFixedFOV(0,0,0);

                            //checking hfov , vfov , lookatpos is valid only when _fixedfov is true
                            if ( _fixedfov )
                            {
                                QString leVFOVTemp = viewshedAnalysisObject->property("verticalFOVValue").toString();
                                std::string vfov = leVFOVTemp.toStdString();

                                //checking whether Vfov attribute has been set from vector file or not
                                if(vfov == "<None>")
                                {
                                    showError("Observer Vertical FOV", "Please select vfov field from added vector File ");
                                    return;
                                }

                                QString leHFOVTemp = viewshedAnalysisObject->property("horizontalFOVValue").toString();
                                std::string hfov = leHFOVTemp.toStdString();

                                //checking whether Hfov attribute has been set from vector file or not
                                if(hfov == "<None>")
                                {
                                    showError("Observer Horizontal FOV", "Please select hfov field from added vector File ");
                                    return;
                                }
                                

                                QString leLatiTemp = viewshedAnalysisObject->property("lookatLatValue").toString();
                                std::string lookatLat = leLatiTemp.toStdString();

                                //checking whether lookat latitude attribute has been set from vector file or not
                                if(lookatLat == "<None>")
                                {
                                    showError("Observer Look at lat", "Please select a field for lookat latitude from added vector file ");
                                    return;
                                }
                                
                                float leLati = leLatiTemp.toFloat();

                                QString leLongiTemp = viewshedAnalysisObject->property("lookatLongValue").toString();
                                std::string lookatLong = leLongiTemp.toStdString();

                                //checking whether lookat longitude attribute has been set from vector file or not
                                if(leLongiTemp == "<None>")
                                {
                                    showError("Observer Look at long", "Please select a field for lookat longitude from added vector file");
                                    return;
                                }
                            
                                float leLongi = leLongiTemp.toFloat();

                                QString leEleTemp = viewshedAnalysisObject->property("lookatEleValue").toString();
                                std::string lookatEle = leEleTemp.toStdString();

                                //checking whether elevation angle attribute has been set from vector file or not
                                if(lookatEle == "<None>")
                                {
                                    showError("Observer Elevation Angle", "Please select a field for lookat Elevation from added vector file ");
                                    return;
                                }
                               
                            }


                            else
                            {
                                    lookatPos = notFixedFOV;
                                    lehorizontalFOV = 360;
                                    leverticalFOV = 180;
                                
                            }
                            
                            _addAttributeToTable(leName, rectColor, ogrPoint->getX(), ogrPoint->getY(), leHeight, leRange, lookatPos, lehorizontalFOV, leverticalFOV);

                            }
                            OGRFeature::DestroyFeature(ogrFeature);
                        }
                    }
             }
            
        }
    }
    

    void ViewshedAnalysisGUI::_addAttributeToTable(const QString& leName, const QColor& rectColor,
        double leLong, double leLat, double leHeight, double leRange,
        osg::Vec3f lookatPos, double leHorizontalFOV, double leVerticalFOV)
    {

        QObject *object = new ViewshedObserverObject(leName, rectColor, leLong, leLat, leHeight, leRange, lookatPos.y(),lookatPos.x(), lookatPos.z(), leHorizontalFOV, leVerticalFOV);
        _twObserverList.append(object);

        _setContextProperty("observerModel", QVariant::fromValue(_twObserverList));


        // Add the current point to object list
        _objectList[leName.toStdString()] = _viewerPoint;
    }

    void ViewshedAnalysisGUI::_handleNameChanged(QString leName)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _nameIndex = ogrFeature->GetFieldIndex("Name");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }

    }

    void ViewshedAnalysisGUI::_handleHeightChanged(QString leHeight)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _heightIndex = ogrFeature->GetFieldIndex("Height");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handleRangeChanged(QString leRange)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _rangeIndex = ogrFeature->GetFieldIndex("Range");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handleLookatLatChanged(QString leLookatLat)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _lookatLatIndex = ogrFeature->GetFieldIndex("LookAtLat");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handleLookatLongChanged(QString leLookatLong)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _lookatLongIndex = ogrFeature->GetFieldIndex("LookAtLong");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handleLookatEleChanged(QString leLookatEle)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _lookatEleIndex = ogrFeature->GetFieldIndex("ElevAngle");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handleHorizontalFOVChanged(QString leHorizontalFOV)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _horizontalFOVIndex = ogrFeature->GetFieldIndex("HFOV");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handleVerticalFOVChanged(QString leVerticalFOV)
    {
        for(int i=0; i < _viewShedPoints->GetLayerCount(); i++)
        { 
            OGRLayer* ogrLayer = _viewShedPoints->GetLayer(i);
            ogrLayer->ResetReading();
            OGRFeature* ogrFeature = NULL;
            if((ogrFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                _verticalFOVIndex = ogrFeature->GetFieldIndex("VFOV");
                OGRFeature::DestroyFeature(ogrFeature);
            }
        }
    }

    void ViewshedAnalysisGUI::_handleSensorChanged()
    {
        QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);

        QString sensor = viewshedAnalysisObject->property("leSensorType").toString();

        std::string sensorType = sensor.toStdString();

        int _Range;
        double _Hfov;
        double _Vfov;
              
              CORE::RefPtr<DATABASE::IDatabase> _database = DATABASE::ValidateDatabaseFile("Sensors.sqlite");
              if(!_database.valid())
                      return;

              else
              {
                  std::string metaQueryStatement = "select Range from Sensor_Config where SensorType ='" + sensorType + "'";
                  CORE::RefPtr<DATABASE::ISQLQueryResult> metaResult = executeStatement1(_database, metaQueryStatement);

                  if(!metaResult.valid())
                  return;

                  CORE::RefPtr<DATABASE::IDBTable> tableName = metaResult->getResultTable();
                  if(!tableName.valid())
                  return;

                  CORE::RefPtr<DATABASE::IDBRecord> record = tableName->getNextRecord();
                  DATABASE::IDBField* field = record->getFieldByIndex(0);
                  _Range = field->toInt();

                  std::string metaQueryStatement1 = "select HFOV from Sensor_Config where SensorType ='" + sensorType + "'";
                  CORE::RefPtr<DATABASE::ISQLQueryResult> metaResult1 = executeStatement1(_database, metaQueryStatement1);
                  tableName = metaResult1->getResultTable();
                  if(!tableName.valid())
                  return;

                  record = tableName->getNextRecord();
                  field = record->getFieldByIndex(0);
                  _Hfov = field->toFloat();

                  std::string metaQueryStatement2 = "select VFOV from Sensor_Config where SensorType ='" + sensorType + "'";
                  CORE::RefPtr<DATABASE::ISQLQueryResult> metaResult2 = executeStatement1(_database, metaQueryStatement2);
                  tableName = metaResult2->getResultTable();
                  if(!tableName.valid())
                  return;

                  record = tableName->getNextRecord();
                  field = record->getFieldByIndex(0);
                  _Vfov = field->toFloat();
              }


        viewshedAnalysisObject->setProperty("leName",QVariant::fromValue(QString::QString(sensorType.c_str())));
        viewshedAnalysisObject->setProperty("leRange",UTIL::ToString(_Range).c_str());

        if ( _fixedfov == true)
        {
            viewshedAnalysisObject->setProperty("leHfov",UTIL::ToString(_Hfov).c_str());
            viewshedAnalysisObject->setProperty("leVfov",UTIL::ToString(_Vfov).c_str());
        }

        else
        {
            viewshedAnalysisObject->setProperty("leHfov",360);
            viewshedAnalysisObject->setProperty("leVfov",180);
            viewshedAnalysisObject->setProperty("leLookAtLat",0);
            viewshedAnalysisObject->setProperty("leLookAtLong",0);
            viewshedAnalysisObject->setProperty("leLookAtEle",0);
        }

    }

    void ViewshedAnalysisGUI::_handlefixedfov(bool checked) 
    {
        QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);
        if ( viewshedAnalysisObject )
        {
            _fixedfov = checked;
            _handleSensorChanged();
        }
    }

    //! Called when choose remove button pressed
    void 
        ViewshedAnalysisGUI::_handleRemoveButtonClicked(int currentIndex)
    {
        _disableMarking();
        //XXX AG Check whether condition should be >= or not in second condition
        if(!_twObserverList.isEmpty())
        {

            if(currentIndex < 0 || currentIndex > _twObserverList.size())
            {
                showError( "Remove Attribute", "Please select a Attribute to remove");
                return;
            }
            else
            {
                // delete corresponding point from _objectList
                std::string temp = (static_cast<ViewshedObserverObject *>(_twObserverList.at(currentIndex)))->name().toStdString();
                _objectList.erase(temp);

                _twObserverList.removeAt(currentIndex);
                _setContextProperty("observerModel", QVariant::fromValue(_twObserverList));
            }
        }
        else
        {
            showError("Remove Attribute", " No Attributes in the list to remove");
        }
    }

    void ViewshedAnalysisGUI::_setViewerPoint(CORE::IPoint* point)
    {
        _removeObject(_viewerPoint.get());
        _viewerPoint = NULL;

        _viewerPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
        _viewerPoint->getInterface<CORE::IBase>()->setName("Viewer Point");
    }

    void ViewshedAnalysisGUI::_setLookatPoint(CORE::IPoint* point)
    {
        _removeObject(_lookatPoint.get());
        _lookatPoint = NULL;
        _lookatPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
        _lookatPoint->getInterface<CORE::IBase>()->setName("Look at Point");
    }

    void ViewshedAnalysisGUI::_reset()
    {
        QObject* viewshedAnalysisObject = _findChild(ViewshedAnalysisObjectName);

        if(viewshedAnalysisObject)
        {
            viewshedAnalysisObject->setProperty("leLat","");
            viewshedAnalysisObject->setProperty("leLong","");
            viewshedAnalysisObject->setProperty("leHeight","");
            viewshedAnalysisObject->setProperty("leRange","");
            viewshedAnalysisObject->setProperty("leName","");
            viewshedAnalysisObject->setProperty("rectColor","green");
            viewshedAnalysisObject->setProperty("startButtonEnabled",true);
            viewshedAnalysisObject->setProperty("stopButtonEnabled",false);
            viewshedAnalysisObject->setProperty("progressValue",0);

            _twObserverList.clear();
            _setContextProperty("observerModel", QVariant::fromValue(_twObserverList));
        }
    }

} // namespace SMCQt
