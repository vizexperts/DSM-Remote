/****************************************************************************
*
* File             : MeasureHeightGUI.h
* Description      : MeasureHeightGUI class definition
*
*****************************************************************************
* Copyright (c) 2010-2011, CAIR, DRDO
*****************************************************************************/

#include <SMCQt/MeasureHeightGUI.h>
#include <App/IApplication.h>
#include <Core/Referenced.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/IPoint.h>
#include <sstream>
#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITerrain.h>
#include <Util/StringUtils.h>
#include <Util/UnitsConversionUtils.h>
#include <Core/ITransformable.h>
#include <osg/PositionAttitudeTransform>
#include <Util/CoordinateConversionUtils.h>
#include <VizUI/IPointUIHandler.h>
#include <osgText/Text>
#include <osg/Geode>
#include <Core/ITemporary.h>
#include <Elements/ElementsUtils.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouse.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt,MeasureHeightGUI);
    DEFINE_IREFERENCED(MeasureHeightGUI,DeclarativeFileGUI);

    const std::string MeasureHeightGUI::HeightAnalysisObjectName = "HeightAnalysis";

    MeasureHeightGUI::MeasureHeightGUI()
        : _markingEnabled(false)
        , _resetFlag(false)
        , _isPointCreated(false)
        , _skiponce(false)
        , _size (25)
        , _fontColor(1.0, 1.0, 1.0, 1.0)
    {

    }

    // MeasureHeightGUI Destructor
    MeasureHeightGUI::~MeasureHeightGUI()
    {
        setActive(false);
    }

    void MeasureHeightGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {

                _tph = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());
                _heightCalculationUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IHeightCalculationUIHandler>(getGUIManager());

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            // get terrain position
            osg::Vec3d pos;
            if(_tph->getMousePickedPosition(pos,true))
            {  
                _heightCalculationUIHandler->setPosition(pos);                        
                _displayPositionValues();

                _resetFlag = false;
                _isPointCreated = true;

            }
        }
        else if(messageType == *SMCUI::IHeightCalculationUIHandler::DraggerUpdated)
        {
            // Update position of the point
            osg::Vec3d pos = _heightCalculationUIHandler->getPosition();

            // This is done to prevent pointUIHandler from terrain picking when dragger is active
            switch(_heightCalculationUIHandler->getDragStage())
            {
            case SMCUI::IHeightCalculationUIHandler::START:
                break;

            case SMCUI::IHeightCalculationUIHandler::FINISH:
                _skiponce = true;
                break;

            default:
                break;
            }

            // Update display values
            _displayPositionValues();

            _resetFlag = false;
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }



    void MeasureHeightGUI::setActive(bool value)
    {
        if(getActive() == value)
            return;

        // Focus area handler and activate gui
        try
        {            
            if(value)
            {
                _heightAnalysisObject = _findChild(HeightAnalysisObjectName);
                if(_heightAnalysisObject)
                {
                    QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(_handleCalculationTypeUpdatedTimer())); 
                    QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(_displayPositionValues())); 
                    _displayPositionValues(); // called _displayPositionValues function to prevent  black screen
                    _heightCalculationUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                    _subscribe(_heightCalculationUIHandler, *SMCUI::IHeightCalculationUIHandler::DraggerUpdated);
                    connect(_heightAnalysisObject, SIGNAL(markPosition(bool)),this,SLOT(_handleMarkButtonToggled(bool)),Qt::UniqueConnection);
                    connect(_heightAnalysisObject, SIGNAL(updatePosition()),this,SLOT(_updatePosition()),Qt::UniqueConnection);
                    connect(_heightAnalysisObject, SIGNAL(fontSizeChanged(QString)),this,SLOT(_fontSizeChanged(QString)),Qt::UniqueConnection);
                    connect(_heightAnalysisObject, SIGNAL(fontColorChanged(QColor)),this,SLOT(_fontColorChanged(QColor)),Qt::UniqueConnection);

                }
            }
            else
            {
                if(_markingEnabled)
                {
                    _handleMarkButtonToggled(false);
                }
                CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
                group->removeChild(_pat);
                _pat = NULL;
                _heightCalculationUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            }

            // remove the created point
            if(_isPointCreated)
            {
                _isPointCreated = false;
            }

            DeclarativeFileGUI::setActive(value);

        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    // Handling MarkButton attributes
    void MeasureHeightGUI::_handleMarkButtonToggled(bool value)
    {

        if(_heightCalculationUIHandler.valid())
        {
            if(value)
            {
                _markingEnabled = true;
                _subscribe(_tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
                _subscribe(_tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
                _timer.start(10);
                //_heightCalculationUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                //_subscribe(_heightCalculationUIHandler, *SMCUI::IHeightCalculationUIHandler::DraggerUpdated);

            }
            else
            {
                _unsubscribe(_tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
                _unsubscribe(_tph.get(), *VizUI::IMouseMessage::HandledMouseReleasedMessageType);
                _timer.stop();

                //_heightCalculationUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                //   _subscribe(_heightCalculationUIHandler.get(), *SMCUI::IHeightCalculationUIHandler::DraggerUpdated);

                _markingEnabled = false;
            }

        }
    }

    void MeasureHeightGUI::_updatePosition()
    {
        if(!_heightAnalysisObject)
            return;

        osg::Vec3d pos;

        //        _timer.stop();

        bool okx = true;
        pos.x() = _heightAnalysisObject->property("longitude").toString().toFloat();

        bool oky = true;
        pos.y() = _heightAnalysisObject->property("latitude").toString().toDouble(&oky);

        bool okz = true;
        pos.z() = _heightAnalysisObject->property("heightvalue").toString().toDouble(&okz);
        _heightCalculationUIHandler->setPosition(pos);

        _displayPositionValues();

        _heightAnalysisObject->setProperty("isUpdateSelected",false);  
        //     _timer.start(10);

    }

    void MeasureHeightGUI::_handleCalculationTypeUpdatedTimer()
    {    
        _heightAnalysisObject = _findChild(HeightAnalysisObjectName);

        if(_heightAnalysisObject == NULL)
            return;

        osg::Vec3d pos;


        bool okx = true;
        pos.x() = _heightAnalysisObject->property("longitude").toString().toFloat(&okx);

        bool oky = true;
        pos.y() = _heightAnalysisObject->property("latitude").toString().toDouble(&oky);


        pos.z() = _heightCalculationUIHandler->getPosition().z();


        QString y = _heightAnalysisObject->property("calType").toString();
        std::string type = y.toStdString();
        QString z = _heightAnalysisObject->property("unitType").toString();
        std::string unit = z.toStdString();


        if(type == "Relative to Terrain")
        {
            CORE::WorldMaintainer *WorldMaintainer=NULL;
            CORE::RefPtr<CORE::IWorldMaintainer> maintainer = WorldMaintainer->instance();
            const CORE::WorldMap& wm = maintainer->getWorldMap();

            // clamp the point to ground
            CORE::IWorld* world = wm.begin()->second;;
            if(world)
            {
                CORE::ITerrain* terrain = world->getTerrain();

                double elevation;
                if(terrain && terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevation))
                {
                    pos.z() -= elevation;
                }
            }
        }


        std::string from = UTIL::UnitsConversionUtils::M;
        std::string to = UTIL::UnitsConversionUtils::M;
        double result = 0.0;

        if(unit == "m")
        {    
            to = UTIL::UnitsConversionUtils::M;
        }
        else if (unit == "km")
        {
            to = UTIL::UnitsConversionUtils::KM;
        }

        UTIL::UnitsConversionUtils::convert(from, to, pos.z(), result);
        pos.z() = result;

        std::string x = UTIL::ToString(pos.z()).c_str();
        double m = UTIL::ToDouble(x);


        if ( m < 0 )
        {
            _heightAnalysisObject->setProperty("heightvalue",0);
        }

        else
        {
            _heightAnalysisObject->setProperty("heightvalue",m);

        }

    }

    void MeasureHeightGUI::_fontSizeChanged(QString x)
    {
        float y = x.toFloat();
        _size = y;

    }

    //get the fontColor
    void MeasureHeightGUI::_fontColorChanged(QColor color)
    {
        QColor cl = color;
        osg::Vec4 penColor((float)cl.red() / 255.0f, (float)cl.green() / 255.0f,
            (float)cl.blue() / 255.0f, (float)cl.alpha() / 255.0f);

        _fontColor = penColor;


    }

    void MeasureHeightGUI::_displayPositionValues()
    {
        osg::Vec3d pos = _heightCalculationUIHandler->getPosition();
        osg::Vec3d posECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(pos.y(), pos.x(), pos.z()));
        osg::Vec3d localNormal(posECEF);

        if(_heightAnalysisObject == NULL)
            return;

        _heightAnalysisObject->setProperty("longitude",UTIL::ToString(pos.x()).c_str());
        _heightAnalysisObject->setProperty("latitude",UTIL::ToString(pos.y()).c_str());
        // _heightAnalysisObject->setProperty("heightvalue",UTIL::ToString(pos.z()).c_str());

        QString y = _heightAnalysisObject->property("calType").toString();
        std::string type = y.toStdString();


        if(type == "Relative to Terrain")
        {
            CORE::WorldMaintainer *WorldMaintainer=NULL;
            CORE::RefPtr<CORE::IWorldMaintainer> maintainer = WorldMaintainer->instance();
            const CORE::WorldMap& wm = maintainer->getWorldMap();

            // clamp the point to ground
            CORE::IWorld* world = wm.begin()->second;;
            if(world)
            {
                CORE::ITerrain* terrain = world->getTerrain();

                double elevation;
                if(terrain && terrain->getElevationAt(osg::Vec2d(pos.x(), pos.y()), elevation))
                {
                    pos.z() -= elevation;
                }
            }
        }

        QString z = _heightAnalysisObject->property("unitType").toString();
        std::string unit = z.toStdString();

        std::string from = UTIL::UnitsConversionUtils::M;
        std::string to = UTIL::UnitsConversionUtils::M;

        double result = 0.0;

        if(unit == "m")
        {    
            to = UTIL::UnitsConversionUtils::M;
        }
        else if (unit == "km")
        {
            to = UTIL::UnitsConversionUtils::KM;
        }


        UTIL::UnitsConversionUtils::convert(from, to, pos.z(), result);
        pos.z() = result;

        std::string lati = UTIL::ToString(pos.x()).c_str();
        std::string longi = UTIL::ToString(pos.y()).c_str();
        _height = UTIL::ToString(pos.z());

        osg::ref_ptr<osg::Geode> geode;

        if ( _pat != NULL )
        {
            CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
            group->removeChild(_pat);
            _pat = NULL;
        }


        _pat = new osg::PositionAttitudeTransform;
        geode = new osg::Geode;

        _pat->addChild(geode);



        osg::ref_ptr<osgText::Text> osgText;

        osgText = new osgText::Text;

        osgText->setText(longi + ",  " + lati + ",  " + _height + " " + unit );

        osg::ref_ptr<osgText::Font> font = osgText::readFontFile("verdana.ttf");
        osgText->setFont(font.get());

        osgText->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
        osgText->setColor(_fontColor);
        osgText->setCharacterSize(_size);
        osgText->setAxisAlignment(osgText::Text::SCREEN);
        osgText->setAutoRotateToScreen(true);


        geode->addDrawable(osgText);


        _pat->setPosition(posECEF);
        _pat->setScale ( osg::Vec3( 100.0f, 100.0f, 100.0f ) );

        geode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
        geode->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE|osg::StateAttribute::OFF);

        CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
        group->addChild ( _pat );
        _pat->setNodeMask(CORE::TempObjNodeMask);

    }



}
