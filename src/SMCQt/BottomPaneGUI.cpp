/*****************************************************************************
*
* File             : BottomPaneGUI.h
* Description      : BottomPaneGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/BottomPaneGUI.h>

#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>
#include <Core/WorldMaintainer.h>

#include <serialization/IProjectLoaderComponent.h>

#include <VizUI/UIHandlerManager.h>

#include <Util/StringUtils.h>
#include <Util/CoordinateConversionUtils.h>
#include <Util/GdalUtils.h>

#include <Core/InterfaceUtils.h>
#include <Elements/ElementsUtils.h>

#include <cmath>
#include <iostream>

#include <mgrs.h>

#include <proj_api.h>


#include <QJSONDocument>
#include <QJSONObject>
#include <QtCore/QJsonArray>
#include <Util/IndianGRUtil.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, BottomPaneGUI);
    DEFINE_IREFERENCED(BottomPaneGUI, CORE::Base);

    BottomPaneGUI::BottomPaneGUI()
        : _cameraUIHandler(NULL)
        ,_cameraComponent(NULL)
    {
        initializeZoneSRS();
        fillGridMat();
        fillMapSheetMat(); 
    }

    void BottomPaneGUI::fillMapSheetMat(){
        
        std::vector<int> columnLength = { 7, 7, 7, 6, 4, 5, 5, 9, 9, 9, 6, 5, 9, 9, 9, 8, 8, 5, 5, 4 };

        //initialize rows 
        _mapSheetMat.resize(9); 

        //initialize columns 
        for (int i = 0; i < 9; i++){
            _mapSheetMat[i].resize(20); 
        }

        int value = 0; 
        for (int i = 0; i < 20; i++){

            int col = columnLength[i];
            int c = 0; 
            for (; c < col; c++){
                _mapSheetMat[c][i] = ++value;
            }

            for (c; c < 9; c++){
                _mapSheetMat[c][i] = 0; 
            }
        }

    }

    void BottomPaneGUI::fillGridMat()
    {
        int startChar = 65;

        gridMat.resize(25);

        for (int i = 0; i < 25; i++){
            gridMat[i].resize(25);
        }

        for (int i = 0; i < 5; i++){

            for (int j = 0; j < 5; j++){

                char num = startChar++;
                std::string numString(&num);
                numString.resize(1);

                if (startChar == 73)
                    startChar++;

                int inChar = 65;

                for (int k = 0; k < 5; k++){

                    for (int l = 0; l < 5; l++){

                        char inNum = inChar++;
                        std::string inNumString(&inNum);
                        inNumString.resize(1);

                        gridMat[i * 5 + k][j * 5 + l] = numString + inNumString;

                        if (inChar == 73)
                            inChar++;
                    }
                }
            }
        }
    }

    void BottomPaneGUI::initializeZoneSRS(){

        if (!UTIL::IndianGRUtil::checkInitailized)
        UTIL::IndianGRUtil::initializeZoneSRS();

        //Now this method is handle by IndianGRUtil

        /*targetSRS_0.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=39.5 +lat_0=39.5 +lon_0=68 +k_0=0.99846154 +x_0=2153865.73916853 +y_0=2368292.194628102 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_1A.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=68 +k_0=0.99878641 +x_0=2743195.592233322 +y_0=914398.5307444407 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_1B.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=90 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_2A.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=26 +lat_0=26 +lon_0=74 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_2B.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=26 +lat_0=26 +lon_0=90 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_3A.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=19 +lat_0=19 +lon_0=80 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_3B.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=19 +lat_0=19 +lon_0=100 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_4A.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=12 +lat_0=12 +lon_0=80 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");
        targetSRS_4B.oGRSpatialReference.importFromProj4("+proj=lcc +lat_1=12 +lat_0=12 +lon_0=104 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377301.243 +b=6356100.228 +to_meter=1.0 +towgs84=295,736,257 +no_defs");

        QFile settingFile(QString::fromStdString("../../config/GRBounds.json"));

        if (!settingFile.open(QIODevice::ReadOnly))
        {
            return; 
        }

        QByteArray settingData = settingFile.readAll();
        QString setting(settingData);
        QJsonDocument jsonResponse = QJsonDocument::fromJson(setting.toUtf8());
        QJsonObject jsonObject = jsonResponse.object();


        QJsonObject  boundsObject = jsonObject["0"].toObject();
        fillJsonBounds(targetSRS_0, boundsObject); 

        boundsObject = jsonObject["IA"].toObject();
        fillJsonBounds(targetSRS_1A, boundsObject);

        boundsObject = jsonObject["IB"].toObject();
        fillJsonBounds(targetSRS_1B, boundsObject);

        boundsObject = jsonObject["IIA"].toObject();
        fillJsonBounds(targetSRS_2A, boundsObject);

        boundsObject = jsonObject["IIB"].toObject();
        fillJsonBounds(targetSRS_2B, boundsObject);

        boundsObject = jsonObject["IIIA"].toObject();
        fillJsonBounds(targetSRS_3A, boundsObject);

        boundsObject = jsonObject["IIIB"].toObject();
        fillJsonBounds(targetSRS_3B, boundsObject);

        boundsObject = jsonObject["IVA"].toObject();
        fillJsonBounds(targetSRS_4A, boundsObject);

        boundsObject = jsonObject["IVB"].toObject();
        fillJsonBounds(targetSRS_4B, boundsObject);

        _sourceSRS.importFromEPSG(4326);*/
    }

    //Now this method is handle by IndianGRUtil

    /*void BottomPaneGUI::fillJsonBounds(SpatialSRS& spatialSRS, QJsonObject&  boundsObject){
        spatialSRS.south = boundsObject["south"].toDouble();
        spatialSRS.north = boundsObject["north"].toDouble();
        spatialSRS.west = boundsObject["west"].toDouble();
        spatialSRS.east = boundsObject["east"].toDouble();
    }*/

    //Now this method is handle by IndianGRUtil
    /*bool BottomPaneGUI::isWithinBounds(osg::Vec2d pos, SpatialSRS spatialSRS){
        if (pos.x() > spatialSRS.south && pos.x() < spatialSRS.north && pos.y() > spatialSRS.west && pos.y() < spatialSRS.east)
            return true;
        else
            return false; 
    }*/

    BottomPaneGUI::~BottomPaneGUI()
    {
    }

    QString BottomPaneGUI::positionText()
    { 
        if(!_cameraUIHandler.valid())
        {
            // flag warning to logger
            return QString("Invalid");
        }

        osg::Vec3d pos = _cameraUIHandler->getCurrentMousePosition();
        return QString::number(pos.x())+" "+ QString::number(pos.y())+" "+ QString::number(pos.z());
    }
    QString BottomPaneGUI::eyePositionText()
    {
        if(!_cameraComponent.valid())
        {
            // flag warning to logger
            return QString("Invalid");
        }

        double pos = _cameraComponent->getRange();

        return QString::number(pos);

    }

    QString BottomPaneGUI::gridText1()
    {

        if(!_mapSheetComponent.valid())
            return QString("Invalid");

        osg::Vec3d pos = _cameraUIHandler->getCurrentMousePosition();

        QString gr = QString::fromStdString(_mapSheetComponent->getMapSheetNumber(osg::Vec2d(pos.x(), pos.y())));

        QString ret = QString("GR: %1").arg(gr);

        return ret;
    }

    QString BottomPaneGUI::GRText()
    {
        if(!_cameraUIHandler.valid())
        {
            // flag warning to logger
            return QString("NA");
        }

        osg::Vec3d pos = _cameraUIHandler->getCurrentMousePosition();
        osg::Vec2d abspos = osg::Vec2(std::abs(pos.x()), std::abs(pos.y()));
        std::string grString=UTIL::IndianGRUtil::convertLatLongToGR(abspos);
        if (grString != "")
        {
            return QString::fromStdString(grString);
        }


        //Now this method is handle by IndianGRUtil

        /*if (abspos.x() > 90.0 || abspos.x() < -90.0 || abspos.y() > 180.0 || abspos.y() < -180.0) return "";

        OGRSpatialReference targetSRS = targetSRS_0.oGRSpatialReference;

        std::string zone = "";
        if (isWithinBounds(abspos, targetSRS_0))
        {
            targetSRS = targetSRS_0.oGRSpatialReference;
            zone = "0";
        }
        else if (isWithinBounds(abspos, targetSRS_1A))
        {
            zone = "IA";
            targetSRS = targetSRS_1A.oGRSpatialReference;
        }
        else if (isWithinBounds(abspos, targetSRS_1B))
        {
            zone = "IB";
            targetSRS = targetSRS_1B.oGRSpatialReference;
        }
        else if (isWithinBounds(abspos, targetSRS_2A))
        {
            zone = "IIA";
            targetSRS = targetSRS_2A.oGRSpatialReference;
        }
        else if (isWithinBounds(abspos, targetSRS_2B))
        {
            zone = "IIB";
            targetSRS = targetSRS_2B.oGRSpatialReference;
        }
        else if (isWithinBounds(abspos, targetSRS_3A))
        {
            zone = "IIIA";
            targetSRS = targetSRS_3A.oGRSpatialReference;
        }
        else if (isWithinBounds(abspos, targetSRS_3B))
        {
            zone = "IIIB";
            targetSRS = targetSRS_3B.oGRSpatialReference;
        }
        else if (isWithinBounds(abspos, targetSRS_4A))
        {
            zone = "IVA";
            targetSRS = targetSRS_4A.oGRSpatialReference;
        }
        else if (isWithinBounds(abspos, targetSRS_4B))
        {
            zone = "IVB";
            targetSRS = targetSRS_4B.oGRSpatialReference;
        }

        if (zone != ""){
            OGRCoordinateTransformation* coordinateTransform =
                OGRCreateCoordinateTransformation(&_sourceSRS, &targetSRS);

            coordinateTransform->Transform(1, &abspos.y(), &abspos.x());

            int col = (std::abs)((int)(abspos.y() / 100000) % 25);
            int row = (std::abs)((int)(abspos.x() / 100000) % 25);



            std::string grSTring = gridMat[24 - row][col];

            zone =  zone +" "+ grSTring;

            int easting = ((int)abspos.y()) % 100000;
            int northing = ((int)abspos.x()) % 100000;

            QString ret = QString("%1 %2 %3").arg(zone.c_str()).arg(FixLengthOfInteger(easting).c_str()).arg(FixLengthOfInteger(northing).c_str());

            return ret;
        }*/
        else{
            return QString("NA");
        }
    }

    std::string BottomPaneGUI::FixLengthOfInteger(int input){

        std::stringstream stream; 
        std::string fillingString; 

        if (input < 10){
            fillingString =  "0000";
        }
        else if (input < 100){
            fillingString = "000";
        }
        else if (input < 1000){
            fillingString = "00";
        }
        else if (input < 10000){
            fillingString = "0";
        }

        stream << fillingString << input; 

        return stream.str(); 
    }

    QString BottomPaneGUI::gridText()
    {
        if (!_cameraUIHandler.valid())
        {
            // flag warning to logger
            return QString("Invalid");
        }

        osg::Vec3d pos = _cameraUIHandler->getCurrentMousePosition();
        osg::Vec3d abspos = osg::Vec3(std::abs(pos.x()), std::abs(pos.y()), pos.z());

        if (abspos.x() > 90.0 || abspos.x() < -90.0 || abspos.y() > 180.0 || abspos.y() < -180.0) return "";
        unsigned int epsg = 0;

        const char* dst_arg = NULL;
        const char* zone = "";
        if (abspos.x() > 35.5800 && abspos.x() < 37.0800 && abspos.y() > 71.1000 && abspos.y() < 77.8700)
        {
            epsg = 24370;
            zone = "0";
            dst_arg = "+proj=lcc +lat_1=39.5 +lat_0=39.5 +lon_0=68 +k_0=0.99846154 +x_0=2153865.73916853 +y_0=2368292.194628102 +a=6377299.36559538 +b=6356098.359005157 +to_meter=0.9143985307444408 +no_defs";
        }
        else if (abspos.x() > 28.0000 && abspos.x() < 35.5500 && abspos.y() > 70.3300 && abspos.y() < 97.4500)
        {
            epsg = 24378;
            zone = "I";
            dst_arg = "+proj=lcc +lat_1=32.5 +lat_0=32.5 +lon_0=68 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs";
        }
        else if (abspos.x() > 21.0000 && abspos.x() < 28.0000 && abspos.y() > 68.1000 && abspos.y() < 82.0000)
        {
            epsg = 24379;
            zone = "IIa";
            dst_arg = "+proj=lcc +lat_1=26 +lat_0=26 +lon_0=74 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs";
        }
        else if (abspos.x() > 21.0000 && abspos.x() < 28.000 && abspos.y() > 82.0000 && abspos.y() < 97.4500)
        {
            //XXX need to change the boundaries
            epsg = 24380;
            zone = "IIb";
            dst_arg = "+proj=lcc +lat_1=26 +lat_0=26 +lon_0=90 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs";
        }
        else if (abspos.x() > 15.0000 && abspos.x() < 21.0000 && abspos.y() > 70.1000 && abspos.y() < 87.0000)
        {
            epsg = 24381;
            zone = "III";
            dst_arg = "+proj=lcc +lat_1=19 +lat_0=19 +lon_0=80 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs";
        }
        else if (abspos.x() > 8.0000 && abspos.x() < 15.0000 && abspos.y() > 73.9000 && abspos.y() < 80.4000)
        {
            epsg = 24383;
            zone = "IV";
            dst_arg = "+proj=lcc +lat_1=12 +lat_0=12 +lon_0=80 +k_0=0.99878641 +x_0=2743195.5 +y_0=914398.5 +a=6377299.151 +b=6356098.145120132 +towgs84=295,736,257,0,0,0,0 +units=m +no_defs";
        }
        double northing = osg::DegreesToRadians(abspos.x());
        double easting = osg::DegreesToRadians(abspos.y());
        if (epsg != 0)
        {
            projPJ pj_dest, pj_latlong;
            pj_latlong = pj_init_plus("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
            pj_dest = pj_init_plus(dst_arg);
            pj_transform(pj_latlong, pj_dest, 1, 1, &easting, &northing, NULL);

        }

        QString ret = QString("Zone: %1 E: %2 N: %3").arg(zone).arg((int)easting).arg((int)northing);

        return ret;
    }
    QString BottomPaneGUI::mgrsText()
    {
        if(!_cameraUIHandler.valid())
        {
            // flag warning to logger
            return QString("Invalid");
        }

        osg::Vec3d pos = _cameraUIHandler->getCurrentMousePosition();
        osg::Vec3d abspos = osg::Vec3(std::abs(pos.x()), std::abs(pos.y()), pos.z());

        if (abspos.x() > 90.0 || abspos.x() < -90.0 || abspos.y() > 180.0 || abspos.y() < -180.0) return "";



        char mgrs[512];

        Convert_Geodetic_To_MGRS (osg::DegreesToRadians(abspos.x()), osg::DegreesToRadians(abspos.y()), 4, mgrs);

        QString ret = QString("MGRS: %1").arg(mgrs);

        return ret;
    }


    int getFloatRow(float value)
    {

        if (value < 0.25)
            return 0;
        else if (value < 0.5)
            return 1;
        else if (value < 0.75)
            return 2;
        else if (value < 1)
            return 3;
        else
            return 0;
    }
    QString BottomPaneGUI::mapSheetText(){

        if (!_cameraUIHandler.valid())
        {
            // flag warning to logger
            return QString("NA");
        }

        osg::Vec3d pos = _cameraUIHandler->getCurrentMousePosition();
        float lat = std::abs(pos.x());
        float lon = std::abs(pos.y());

        int matRow = 10 - (lat / 4);
        int matCol = (lon - 44) / 4;

        if (matRow > 8 || matRow < 0 || matCol >19 || matCol <0)
            return QString("NA");

        int mapNumber = _mapSheetMat[matRow][matCol]; 

        if (mapNumber == 0)
            return QString("NA"); 

        

        int charCol = ((int)lon - 44) % 4; 
        int charRow = 3 - ((int)lat % 4);

        char mapChar = 65 + (charCol * 4) + charRow;


        float latFloat = (lat - (int)lat);
        float lonFloat = (lon - (int)lon);

        int floatCol = getFloatRow(lonFloat);
        int floatRow = 3- getFloatRow(latFloat);

        int floatNum = 1 + (floatCol * 4) + floatRow;

        QString ret = QString("%1%2%3").arg((int)mapNumber).arg(mapChar).arg(floatNum);
        return ret;

    }


    void BottomPaneGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                // Query the cameraUIHandler and projectedCoordinatesHandler
                _cameraUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler, APP::IGUIManager>(getGUIManager()); 
                CORE::RefPtr<CORE::IComponent> component = ELEMENTS::GetComponentFromMaintainer("CameraComponent");
                if(component)
                {
                    _cameraComponent = component->getInterface<VIEW::ICameraComponent>();
                }

                component = ELEMENTS::GetComponentFromMaintainer("MapSheetInfoComponent");
                if(component.valid())
                {
                    _mapSheetComponent = component->getInterface<ELEMENTS::IMapSheetInfoComponent>();
                }
            }
            catch(UTIL::Exception& e)
            {
                e.LogException();
                //setComplete(false);
            }
        }
        else
        {
            VizQt::GUI::update(messageType, message);
        }
    }
} // namespace indiGUI
