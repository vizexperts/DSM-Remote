/****************************************************************************
*
* File             : CreateLayerGUI.h
* Description      : CreateLayerGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/AttributeObject.h>
#include <SMCQt/CreateLayerGUI.h>
#include <SMCQt/EventGUI.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <SMCUI/IPointUIHandler2.h>
#include <SMCUI/ILineUIHandler.h>
#include <SMCUI/IAreaUIHandler.h>
#include <SMCUI/ILandmarkUIHandler.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateLayerGUI);
    DEFINE_IREFERENCED(CreateLayerGUI, DeclarativeFileGUI);

    CreateLayerGUI::CreateLayerGUI()
        : _cbLayerTemplateName("")
    {}

    CreateLayerGUI::~CreateLayerGUI()
    {}

    void CreateLayerGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if( popupLoader != NULL )
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void CreateLayerGUI::connectPopupLoader(QString type)
    {
        if(type == "CreateLayerMenuObject")
        {
            setActive( true );
            //XXX AG Dont use underscore for the local variables
            QObject* _createLayerMenuObject = _findChild("CreateLayerMenuObject");
            if(_createLayerMenuObject)
            {
                // connectivity of signals and slots
                QObject::connect( _createLayerMenuObject, SIGNAL(qmlHandleAddButtonClicked()), this,
                    SLOT(_handleAddButtonClicked()));
                QObject::connect( _createLayerMenuObject, SIGNAL(qmlHandleUpdateButtonClicked(int)), this,
                    SLOT(_handleUpdateButtonClicked(int)));
                QObject::connect( _createLayerMenuObject, SIGNAL(qmlHandleRemoveButtonClicked(int)), this,
                    SLOT(_handleRemoveButtonClicked(int)));
                QObject::connect( _createLayerMenuObject, SIGNAL(qmlHandleOkButtonClicked()), this,
                    SLOT(_handleOkButtonClicked()));
                QObject::connect( _createLayerMenuObject, SIGNAL(qmlHandleCancelButtonClicked()), this,
                    SLOT(_handleCancelButtonClicked()));
            }
        }
    }

    void CreateLayerGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {   
                _createLayerUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ICreateLayerUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void CreateLayerGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        // Check whether UI Handler is valid or not

        // Focus UI Handler and activate GUI
        try
        {
            _createLayerUIHandler->getInterface<APP::IUIHandler>()->setFocus(value);
            DeclarativeFileGUI::setActive(value);
            // clear the GUI
            _clear();
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    //! Called when choose add button pressed
    void CreateLayerGUI::_handleAddButtonClicked()
    {
        //XXX AG Dont use underscore for local variablse
        QObject* _createLayerMenuObject = _findChild("CreateLayerMenuObject");
        QString leAttrName = _createLayerMenuObject->property("leAttrName").toString();
        if(leAttrName =="")
        {
            showError("Attribute Name", "Please enter a attribute name");
            return;
        }

        QString cbAttrType = _createLayerMenuObject->property("cbAttrType").toString();

        QString leAttrWidthTemp = _createLayerMenuObject->property("leAttrWidth").toString();
        int leAttrWidth = leAttrWidthTemp.toInt();
        if(leAttrWidth <= 0)
        {
            showError("Attribute Width", "Please enter a valid integer greater than 0 for Width");
            return;
        }

        QString leAttrPrecisionTemp = _createLayerMenuObject->property("leAttrPrecision").toString();
        int leAttrPrecision = leAttrPrecisionTemp.toInt();
        if(leAttrPrecision <= 0)
        {
            showError("Attribute Precision", "Please enter a valid integer greater than 0 for Precision");
            return;
        }

        // check if the attribute name already exist
        if(! _twAttrList.isEmpty()) 
        {
            int noOfRows = _twAttrList.size();
            for(int i= 0; i < noOfRows; i++)
            {
                if((static_cast<AttributeObject *>(_twAttrList.at(i)))->name().toStdString() == leAttrName.toStdString())
                {
                    showError("Attribute Name", "Attribute name already exist");
                    return;
                }
            }
        }
        _addAttributeToTable(leAttrName, cbAttrType, leAttrWidth, leAttrPrecision);
    }

    //! Called when choose update button pressed
    void CreateLayerGUI::_handleUpdateButtonClicked(int currentIndex)
    {
        //XXX AG Rather than copy/paste the code, use can create another function
        //that checks the validity of all the things required for the attribute adding
        //Same function can be called from add and update functoin

        QObject* _createLayerMenuObject = _findChild("CreateLayerMenuObject");
        QString leAttrName = _createLayerMenuObject->property("leAttrName").toString();
        if(leAttrName =="")
        {
            showError("Attribute Name", "Please enter a attribute name");
            return;
        }

        QString cbAttrType = _createLayerMenuObject->property("cbAttrType").toString();

        QString leAttrWidthTemp = _createLayerMenuObject->property("leAttrWidth").toString();
        int leAttrWidth = leAttrWidthTemp.toInt();
        if(leAttrWidth <= 0)
        {
            showError("Attribute Width", "Please enter a valid integer greater than 0 for Width");
            return;
        }

        QString leAttrPrecisionTemp = _createLayerMenuObject->property("leAttrPrecision").toString();
        int leAttrPrecision = leAttrPrecisionTemp.toInt();
        if(leAttrPrecision <= 0)
        {
            showError("Attribute Precision", "Please enter a valid integer greater than 0 for Precision");
            return;
        }

        if(currentIndex < 0 || currentIndex > _twAttrList.size())
        {
            showError( "Update Row", "Please select a Row to Update");
        }
        else
        {
            QObject *object = new AttributeObject(leAttrName, cbAttrType, leAttrWidth, leAttrPrecision);
            _twAttrList.replace(currentIndex, object);

            _setContextProperty("layerModel", QVariant::fromValue(_twAttrList));

        }
    }

    //! Called when choose remove button pressed
    void CreateLayerGUI::_handleRemoveButtonClicked(int currentIndex)
    {
        if(currentIndex < 0 || currentIndex >= _twAttrList.size())
        {
            showError( "Remove Attribute", "Please select a Attribute to remove");
        }
        else
        {
            _twAttrList.removeAt(currentIndex);
            _setContextProperty("layerModel", QVariant::fromValue(_twAttrList));
        }
    }

    //! Called when choose ok button pressed
    void CreateLayerGUI::_handleOkButtonClicked()
    {
        //XXX AG Indentation improper
        //XXX AG Object Name improper
        QObject* _createLayerMenuObject = _findChild("CreateLayerMenuObject");
        QString leLayerName = _createLayerMenuObject->property("leLayerName").toString();
        if(leLayerName =="")
        {
            showError("Layer Name", "Please enter a Layer name");
            _createLayerMenuObject->setProperty("popupClosed",false);
            return;
        }
        if(_createLayerUIHandler->checkLayerNameAvailability(leLayerName.toStdString()))
        {        
            QString cbLayerTemplate = _createLayerMenuObject->property("cbLayerTemplate").toString();
            if(cbLayerTemplate == "")
            {
                showError("Layer Template Type ", "Please Select a Valid Template");
                return;
            } 

            std::vector<std::string> attrNames, attrTypes;
            std::vector<int> attrWidths, attrPrecisions;

            for(int i = 0; i < _twAttrList.size(); i++)
            {
                //XXX AG Why doing static_cast again and again. Use the variable to store the result from first computation.
                std::string attrName = (static_cast<AttributeObject *>(_twAttrList.at(i)))->name().toStdString();
                std::string attrType = (static_cast<AttributeObject *>(_twAttrList.at(i)))->type().toStdString();
                int attrWidth = (static_cast<AttributeObject *>(_twAttrList.at(i)))->width();
                int attrPrecision = (static_cast<AttributeObject *>(_twAttrList.at(i)))->precision();
                attrNames.push_back(attrName);
                attrTypes.push_back(attrType);
                attrWidths.push_back(attrWidth);
                attrPrecisions.push_back(attrPrecision);
            }

            std::vector<std::string> defAttrNames, defAttrTypes;
            std::vector<int> defAttrWidths, defAttrPrecisions;

            // adding default attributes to the layers
            //XXX Rather than hardcoding these names use const std::String with proper commenting
            if(cbLayerTemplate.toStdString() == "Point")
            {
                CORE::RefPtr<SMCUI::IPointUIHandler2> pointUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
                pointUIHandler->getDefaultLayerAttributes(defAttrNames, defAttrTypes, defAttrWidths, defAttrPrecisions);
            }
            else if(cbLayerTemplate.toStdString() == "Line")
            {
                CORE::RefPtr<SMCUI::ILineUIHandler> lineUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getGUIManager());
                lineUIHandler->getDefaultLayerAttributes(defAttrNames, defAttrTypes, defAttrWidths, defAttrPrecisions);
            }
            else if(cbLayerTemplate.toStdString()== "Polygon")
            {
                CORE::RefPtr<SMCUI::IAreaUIHandler> areaUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
                areaUIHandler->getDefaultLayerAttributes(defAttrNames, defAttrTypes, defAttrWidths, defAttrPrecisions);
            }
            else if(cbLayerTemplate.toStdString() == "Model")
            {
                CORE::RefPtr<SMCUI::ILandmarkUIHandler> modelUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILandmarkUIHandler>(getGUIManager());
                modelUIHandler->getDefaultLayerAttributes(defAttrNames, defAttrTypes, defAttrWidths, defAttrPrecisions);
            }

            //XXX What if both default nameas and user names are same. should throw the error that attribute with this name 
            // already present
            for(int i = 0; i < (int)defAttrNames.size(); i++)
            {
                attrNames.push_back(defAttrNames[i]);
                attrTypes.push_back(defAttrTypes[i]);
                attrPrecisions.push_back(defAttrPrecisions[i]);
                attrWidths.push_back(defAttrWidths[i]);
            }

            _createLayerUIHandler->createLayer(leLayerName.toStdString(), cbLayerTemplate.toStdString(),
                attrNames, attrTypes, attrWidths, attrPrecisions);
        }
        else
        {
            showError( "Layer Name", std::string("Layer named " + leLayerName.toStdString() + " is already exist").c_str());
            _createLayerMenuObject->setProperty("popupClosed",false);
            return;
        }
        _createLayerMenuObject->setProperty("popupClosed",true);
        _clear();
    }

    //! Called when choose cancel button pressed
    void CreateLayerGUI::_handleCancelButtonClicked()
    {
        setActive(false);
    }

    void CreateLayerGUI::_clearAttributeParams()
    {  
        //XXX Use Static std string
        QObject* _createLayerMenuObject = _findChild("CreateLayerMenuObject");
        _createLayerMenuObject->setProperty("leAttrName","");
        _createLayerMenuObject->setProperty("cbAttrType","Integer");
        _createLayerMenuObject->setProperty("leAttrWidth","1");
        _createLayerMenuObject->setProperty("leAttrPrecision","1");
    }

    void CreateLayerGUI::_addAttributeToTable(const QString& attrName, const QString& attrType, int attrWidth, int attrPrecision)
    {
        QObject *object = new AttributeObject(attrName, attrType, attrWidth, attrPrecision);
        _twAttrList.append(object);

        _setContextProperty("layerModel", QVariant::fromValue(_twAttrList));
    }

    void CreateLayerGUI::_clearAttributeTable()
    {
        _twAttrList.clear();
        _setContextProperty("layerModel", QVariant::fromValue(_twAttrList));
    }

    void CreateLayerGUI::_clear()
    {
        QObject* _createLayerMenuObject = _findChild("CreateLayerMenuObject");
        _createLayerMenuObject->setProperty("leLayerName","");
        _createLayerMenuObject->setProperty("cbAttrType","Line");
        _clearAttributeTable();
        _clearAttributeParams();
    }

} // namespace GUI
