/****************************************************************************
*
* File             : FlyAroundGUI.h
* Description      : FlyAroundGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/FlyAroundGUI.h>
#include <App/IApplication.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPoint.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <VizUI/ISelectionUIHandler.h>

#include <Util/FileUtils.h>
#include <QJSONDocument>
#include <QJSONObject>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, FlyAroundGUI);
    DEFINE_IREFERENCED(FlyAroundGUI, DeclarativeFileGUI);

    const std::string FlyAroundGUI::FlyAroundAnalysisPopup = "flyAroundPopup";

    FlyAroundGUI::FlyAroundGUI() : _start(false), _lastFlyaroundName(""), _preview(false)
    {}

    FlyAroundGUI::~FlyAroundGUI()
    {}

    void FlyAroundGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IFlyAroundUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }

        else if (messageType == *SMCUI::IFlyAroundUIHandler::ShowPlayButtonMessageType)
        {
            QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
            if (FlyAroundAnalysisObject)
            {
                _start = false;
                FlyAroundAnalysisObject->setProperty("playVisible", true);
                FlyAroundAnalysisObject->setProperty("pauseVisible", false);
            }
        }

        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void FlyAroundGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
                if (FlyAroundAnalysisObject)
                {
                    //populate the list
                    _readAllFlyAround();
                  
                    int count = 2;
                    QList<QObject*> comboBoxList;

                    comboBoxList.append(new VizComboBoxElement("Select Fly around", "0") );
                    comboBoxList.append(new VizComboBoxElement("Create New Fly around", "1"));

                    for (int i = 0; i < _flyaroundList.size(); i++)
                    {
                        std::string flyAroundName = _flyaroundList[i];
                        comboBoxList.append(new VizComboBoxElement(flyAroundName.c_str(), UTIL::ToString(count).c_str()));
                    }

                    _setContextProperty("flyAroundList", QVariant::fromValue(comboBoxList));

                    //registering slots 
                    connect(FlyAroundAnalysisObject, SIGNAL(stopFlyAround()), this, SLOT(_handlePBStopClicked()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(startExistingFlyAround(QString)), this, SLOT(_startExistingFlyaroundComboBoxClicked(QString)), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(saveFlyAround()), this, SLOT(_handleSaveButtonClicked()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(previewFlyAround()), this, SLOT(_handlePreviewClicked()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(pauseFlyAround()), this, SLOT(_handlePauseClicked()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(editFlyAround(QString, bool)), this, SLOT(_editExistingFlyaround(QString,bool)), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(toggleOpenFlyaroundParameterGrid(QString)), this, SLOT(_showParameterGrid(QString)), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(visualise(QString)), this, SLOT(_visualise(QString)), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(increaseSpeed()), this, SLOT(_increaseSpeed()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(decreaseSpeed()), this, SLOT(_decreaseSpeed()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(stopFlyAroundPreview()), this, SLOT(_handleStopFlyAroundPreview()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(apply()), this, SLOT(_applyChanges()), Qt::UniqueConnection);
                    connect(FlyAroundAnalysisObject, SIGNAL(deleteSelectedFlyaround(QString)), this, SLOT(_delete(QString)), Qt::UniqueConnection);


                    _subscribe(_uiHandler.get(), *SMCUI::IFlyAroundUIHandler::ShowPlayButtonMessageType);


                }
            }
            else
            {
                _handlePBStopClicked();
                _reset();

                _start = false;
                _preview = false;
                _flyaroundList.clear();
                _lastFlyaroundName = "";
                _unsubscribe(_uiHandler.get(), *SMCUI::IFlyAroundUIHandler::ShowPlayButtonMessageType);

                if (_cameraUIHandler.valid())
                {
                    _cameraUIHandler = NULL;
                }

                if (!_uiHandler.valid())
                {
                    return;
                }

                _uiHandler->closePopUp();

                
            }
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void FlyAroundGUI::_handleStopFlyAroundPreview()
    {
        if (!_uiHandler.valid())
        {
            return;
        }

        if (_start)
        {
            _start = false;
            _preview = false;
            _uiHandler->pause(SMCUI::IFlyAroundUIHandler::Mode::Stop_Preview);
        }
    }


    // internal function that returns current slelected point
    CORE::RefPtr<CORE::IPoint> FlyAroundGUI::_getSelectedPointFeature()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        const CORE::ISelectionComponent::SelectionMap& slMap = selectionUIHandler->getCurrentSelection();

        if (slMap.empty())
            return NULL;

        _currentSelection = slMap.begin()->second;

        CORE::RefPtr<CORE::IObject> currObject = _currentSelection->getInterface<CORE::IObject>();
        CORE::RefPtr<CORE::IPoint> point = currObject->getInterface<CORE::IPoint>();
        return point;
    }
    
    // internal function to write in JSON
    void FlyAroundGUI::_writeFlyAround(QString name, QString flyAroundValues)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::stringstream insertStatement;

        std::string flyAroundFolder = appdataPath + "/FlyAround";
       
        std::string path = osgDB::convertFileNameToNativeStyle(flyAroundFolder);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }
        std::string fileName = "/" + name.toStdString() + ".flyaround";
        QFile settingFile(QString::fromStdString(flyAroundFolder + fileName));

        if (!settingFile.open(QIODevice::WriteOnly))
        {
            emit showError("Save setting", "Could not open flyaround file for save");
            return;
        }

        QJsonObject settingObject;
        settingObject[flyAroundValues] = "";
        QJsonDocument settingDoc(settingObject);
        settingFile.write(settingDoc.toJson());
    }

    //internal function to read from JSON and populate the comboboz list
    void FlyAroundGUI::_readAllFlyAround()
    {
        _flyaroundList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string flyAroundFolder = appdataPath + "/FlyAround";

        std::string path = osgDB::convertFileNameToNativeStyle(flyAroundFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _flyaroundList.push_back(fileNameWithoutExtension);
        }

    }

    // template function to create new flyAround from existing ones
    void FlyAroundGUI::_editExistingFlyaround(QString selectedValue, bool checked)
    {
        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (_start && !_preview)
        {
            emit  showError("Cannot Edit", "Please stop the flyaround and try again");
            if (FlyAroundAnalysisObject)
            {
                FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid", QVariant::fromValue(false));
                FlyAroundAnalysisObject->setProperty("editFlyAroundButton", QVariant::fromValue(false));
            }
            return;
        }

        if (_preview)
        {
            _handleStopFlyAroundPreview();
            if (FlyAroundAnalysisObject)
            {
                FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid", QVariant::fromValue(false));
                FlyAroundAnalysisObject->setProperty("editFlyAroundButton", QVariant::fromValue(false));
            }
            return;
        }

        if (selectedValue.toStdString() == "Select Fly around" || selectedValue.toStdString() == "Create New Fly around")
        {
            emit showError("Select flyaround", "Select a valid flyaround to edit");
            if (FlyAroundAnalysisObject)
            {
                FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid", QVariant::fromValue(false));
                FlyAroundAnalysisObject->setProperty("editFlyAroundButton", QVariant::fromValue(false));
            }
            return;
        }

        if (FlyAroundAnalysisObject && checked)
        {
            SMCUI::IFlyAroundUIHandler::FlyAround flyAround = _getFlyaround(selectedValue.toStdString());

            if (flyAround.distance < 0)
            {
                return;
            }

            FlyAroundAnalysisObject->setProperty("flyaroundNameString", QString::fromStdString(flyAround.name));
            FlyAroundAnalysisObject->setProperty("distanceString", QString::number(flyAround.distance));
            FlyAroundAnalysisObject->setProperty("heightString", QString::number(flyAround.height));
            FlyAroundAnalysisObject->setProperty("lookAtHeightString", QString::number(flyAround.lookAtHeight));
            FlyAroundAnalysisObject->setProperty("arcAngleString", QString::number(flyAround.arcAngle));
            FlyAroundAnalysisObject->setProperty("startAngleString", QString::number(flyAround.startAngle));
            FlyAroundAnalysisObject->setProperty("loop", QVariant::fromValue(flyAround.loop));
            if (flyAround.reverse == -1)
            {
                FlyAroundAnalysisObject->setProperty("reverseChecked", true);
            }

            else 
            {
                FlyAroundAnalysisObject->setProperty("reverseChecked", false);
            }
           
         }

        FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid", QVariant::fromValue(checked));
    }

    void FlyAroundGUI::_applyChanges()
    {
        if (_preview)
        {
            emit  showError("Cannot Save", "Please stop the flyaround and try again");
            return;
        }

        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (FlyAroundAnalysisObject)
        {
            QString newName = FlyAroundAnalysisObject->property("flyaroundNameString").toString();

            // If the name of new flyAround is matching with earlier saved ones then asking question
            if ( isAlreadyPresent(newName.toStdString()) )
            {
                emit question("Fly around with this name exists", "Do you want to overwrite the existing one?", false,
                    "_okSlotForOverWriting()", "_cancelSlotForOverWriting()");
            }

            else
            {
                _okSlotForOverWriting();
            }
           
        }
    }

    void FlyAroundGUI::_okSlotForOverWriting()
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string flyAroundFolder = appdataPath + "/FlyAround";

        std::string path = osgDB::convertFileNameToNativeStyle(flyAroundFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (FlyAroundAnalysisObject)
        {
            QString lpName = FlyAroundAnalysisObject->property("value").toString();
            if (lpName == "")
            {
                return;
            }

            std::string filename = lpName.toStdString() + ".flyaround";
            std::string filePath = flyAroundFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, flyAroundFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
                //std::cout << "Existing Delete Shp File from" << flyAroundFolder << std::endl;
            }

            _handleSaveButtonClicked();
        }
    }

    void FlyAroundGUI::_cancelSlotForOverWriting()
    {
        return;
    }

    void FlyAroundGUI::_delete(QString value)
    {
        if (_start && !_preview)
        {
           
            emit  showError("Cannot delete", "Please stop the flyaround and try again");
            return;
        }

        if (_preview)
        {
            _handleStopFlyAroundPreview();
            QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
            if (FlyAroundAnalysisObject)
            {
                FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid", QVariant::fromValue(false));
                FlyAroundAnalysisObject->setProperty("editFlyAroundButton", QVariant::fromValue(false));
            }

        }

        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (value.toStdString() == "Select Fly around" || value.toStdString() == "Create New Fly around")
        {
            _uiHandler->stop();
            emit  showError("Not a valid input flyaround", "Cannot delete flyaround with this name");
            return;
        }

        emit question("Delete fly around", "Do you really want to delete?", false,
            "_okSlotForDeleting()", "_cancelSlotForDeleting()");

    }

    void FlyAroundGUI::_okSlotForDeleting()
    {
        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string flyAroundFolder = appdataPath + "/FlyAround";

        std::string path = osgDB::convertFileNameToNativeStyle(flyAroundFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        if (FlyAroundAnalysisObject)
        {
            QString lpName = FlyAroundAnalysisObject->property("value").toString();
            if (lpName == "")
            {
                return;
            }

            std::string filename = lpName.toStdString() + ".flyaround";
            std::string filePath = flyAroundFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, flyAroundFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
              //  std::cout << "Deleted flyAround File from " << flyAroundFolder << std::endl;
            }

            _readAllFlyAround();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select Fly around", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New Fly around", "1"));

            for (int i = 0; i < _flyaroundList.size(); i++)
            {
                std::string flyAroundName = _flyaroundList[i];
                comboBoxList.append(new VizComboBoxElement(flyAroundName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("flyAroundList", QVariant::fromValue(comboBoxList));

            std::string defaultName = "Select Fly around";
            FlyAroundAnalysisObject->setProperty("value", QString::fromStdString(defaultName));
            QString flyAroundName = FlyAroundAnalysisObject->property("value").toString();
            _visualise(flyAroundName);
            _showParameterGrid(flyAroundName);
        }

    }

    void FlyAroundGUI::_cancelSlotForDeleting()
    {
        return;
    }

    // to just see the trajectory and not running the preview/start
    void FlyAroundGUI::_visualise(QString selectedValue)
    {
        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (_start)
        {
            emit  showError("Cannot Select", "Please stop the flyAround and try again");
            FlyAroundAnalysisObject->setProperty("value", _lastFlyaroundName);
            return;
        }

        if (selectedValue.toStdString() == "Select Fly around" || selectedValue.toStdString() == "Create New Fly around")
        {
            _uiHandler->stop();
            return;
        }

        else
        {
            SMCUI::IFlyAroundUIHandler::FlyAround flyAround = _getFlyaround(selectedValue.toStdString());

            if (flyAround.distance < 0)
            {
                return;
            }

            if (!_uiHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _getSelectedPointFeature();
            if (point.valid())
            {
                _uiHandler->setPointCoordinates(point);
                _uiHandler->visualise(flyAround);

            }
        }

    }

    // to decide whether to show parameters grid or not
    void FlyAroundGUI::_showParameterGrid(QString selectedValue)
    {
        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);

        if (selectedValue.toStdString() == "Select Fly around" )
        {
            if (FlyAroundAnalysisObject)
            {
                FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid", QVariant::fromValue(false));
            }
        }

        else if (selectedValue.toStdString() == "Create New Fly around")
        {
            if (FlyAroundAnalysisObject)
            {
                FlyAroundAnalysisObject->setProperty("flyaroundNameString", "");
                FlyAroundAnalysisObject->setProperty("distanceString", "");
                FlyAroundAnalysisObject->setProperty("heightString","" );
                FlyAroundAnalysisObject->setProperty("lookAtHeightString", "");
                FlyAroundAnalysisObject->setProperty("arcAngleString","" );
                FlyAroundAnalysisObject->setProperty("startAngleString","");
                FlyAroundAnalysisObject->setProperty("loop", false);
                FlyAroundAnalysisObject->setProperty("reverseChecked", false);
                FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid",false);

            }
        }
    }

    // checks all flyaround files with the name passed and returns true if its present
    bool FlyAroundGUI::isAlreadyPresent(std::string name)
    {
         UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string flyAroundFolder = appdataPath + "/FlyAround";

        std::string path = osgDB::convertFileNameToNativeStyle(flyAroundFolder);
        if (!osgDB::fileExists(path))
        {
           // emit showError("Read setting", "FlyAround folder not found", true);
            //making flyAround to be invalid that is handled in parent function
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == name)
            {
                return true;
            }
        }

        return false;
    }

    // extracting flyAround parameters with given flyAroundname
    SMCUI::IFlyAroundUIHandler::FlyAround FlyAroundGUI::_getFlyaround(std::string flyAroundName)
    {
        SMCUI::IFlyAroundUIHandler::FlyAround flyAround;
        std::stringstream s;

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string flyAroundFolder = appdataPath + "/FlyAround";

        std::string path = osgDB::convertFileNameToNativeStyle(flyAroundFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting", "Flyaround folder not found", true);
            //making flyAround to be invalid that is handled in parent function
            flyAround.distance = -1;
            return flyAround;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == flyAroundName)
            {
                found = true;
            }
        }

        if (!found)
        {
            emit showError("Read setting", "Fly around with this name not found", true);
            //making flyAround to be invalid that is handled in parent function
            flyAround.distance = -1;
            return flyAround;
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + "/FlyAround/" + flyAroundName + ".flyaround"));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                emit showError("Read setting", "Can not open file for reading", true);
                //making flyAround to be invalid that is handled in parent function
                flyAround.distance = -1;
                return flyAround;
            }

            QByteArray settingData = settingFile.readAll();
            QString setting(settingData);
            //Getting whole json as std string
            std::string str = setting.toStdString();

            int i = 0;
            // my concerned string is like "1000 2000 360 120 1"
            while (str[i] != '"')
            {
                i++;
            }

            i++;

            while (str[i] != '"')
            {
                s << str[i];
                i++;
            }

            std::string word = "";
            float a[8];
            int m = 0;

            //while there are words to extract
            while (!s.fail())
            {
                //extract offsets 
                s >> word;
                //converts strings to ints
                std::stringstream convert;
                //insert the digits into our converter
                convert << word;
                //will hold the number inside the string
                int theNumber = 0;
                //convert the string number into a int number and place it inside of theNumber variable
                convert >> theNumber;
                a[m] = theNumber;
                m++;
            }

            flyAround.distance = a[0];
            flyAround.height = a[1];
            flyAround.lookAtHeight = a[2];
            flyAround.arcAngle = a[3];
            flyAround.startAngle = a[4];
            flyAround.loop = a[5];
            flyAround.reverse = a[6];
            flyAround.name = flyAroundName;

            return flyAround;
        }

    }

    // function for saving the flyAround
    void FlyAroundGUI::_handleSaveButtonClicked()
    {
        if (_preview)
        {
            emit  showError("Cannot Save", "Please stop the flyaround and try again");
            return;
        }

        SMCUI::IFlyAroundUIHandler::FlyAround flyAround;
        QString lpName;
       
        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (FlyAroundAnalysisObject)
        {
            lpName = FlyAroundAnalysisObject->property("flyaroundNameString").toString();
            if (lpName == "")
            {
                emit showError("Name", "Flyaround name not specified", true);
                return;
            }

            if (lpName.toStdString() == "Select Fly around" || lpName.toStdString() == "Create New Fly around")
            {
                emit  showError("Please change Fly around name", "Fly around with this name is not allowed");
                return;   
            }

            flyAround.name = lpName.toStdString();
          
            QString lpDistance = FlyAroundAnalysisObject->property("distanceString").toString();
            if (lpDistance == "")
            {
                emit showError("Distance", "Flyaround distance not specified", true);
                return;
            }

            flyAround.distance = lpDistance.toDouble();

            QString lpHeight = FlyAroundAnalysisObject->property("heightString").toString();
            if (lpHeight == "")
            {
                emit showError("Viewing Height", "Flyaround height not specified", true);
                return;
            }

            flyAround.height = lpHeight.toDouble();

            QString lpLookHeight = FlyAroundAnalysisObject->property("lookAtHeightString").toString();
            if (lpLookHeight == "")
            {
                emit showError("Look at Height", "Flyaround height not specified", true);
                return;
            }

            flyAround.lookAtHeight = lpLookHeight.toDouble();

            QString lpArcAngle = FlyAroundAnalysisObject->property("arcAngleString").toString();
            if (lpArcAngle == "")
            {
                emit showError("ArcAngle", "Flyaround arcangle not specified", true);
                return;
            }

            flyAround.arcAngle = lpArcAngle.toDouble();

            QString lpStartAngle = FlyAroundAnalysisObject->property("startAngleString").toString();
            if (lpStartAngle == "")
            {
                emit showError("StartAngle", "Flyaround startangle not specified", true);
                return;
            }

            flyAround.startAngle = lpStartAngle.toDouble();

            bool loop = FlyAroundAnalysisObject->property("loop").toBool();

            flyAround.loop = loop;

            bool reverseChecked = FlyAroundAnalysisObject->property("reverseChecked").toBool();
            int reverse = 1;

            if (reverseChecked)
            {
                reverse = -1;
            }

            flyAround.reverse = reverse;


        }

        if (!_uiHandler.valid())
        {
            return;
        }

        bool editChecked = FlyAroundAnalysisObject->property("editFlyAroundButton").toBool();

        bool present = isAlreadyPresent(flyAround.name);

        if (present && !editChecked)
        {
            emit showError("Please Change the name", "Flyaround with this name is already present", true);
            return;
        }

        std::stringstream insertStatement;
        insertStatement << flyAround.distance << " " << flyAround.height << " " << flyAround.lookAtHeight << " " << flyAround.arcAngle << " " << flyAround.startAngle << " " << flyAround.loop << " " << flyAround.reverse;

        QString values = QString::fromStdString (insertStatement.str() );
        
        _writeFlyAround(lpName, values);
        _readAllFlyAround();

        //populate the list again
        int count = 2;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select Fly around", "0"));
        comboBoxList.append(new VizComboBoxElement("Create New Fly around", "1"));

        for (int i = 0; i < _flyaroundList.size(); i++)
        {
            std::string flyAroundName = _flyaroundList[i];
            comboBoxList.append(new VizComboBoxElement(flyAroundName.c_str(), UTIL::ToString(count).c_str()));
        }


        _setContextProperty("flyAroundList", QVariant::fromValue(comboBoxList));

        if (_flyaroundList.size() > 0)
        {
            FlyAroundAnalysisObject->setProperty("value", lpName);
            QString flyAroundName = FlyAroundAnalysisObject->property("value").toString();
            _visualise(flyAroundName);
        }


    }
 
    // start flyAround with saved values
    void FlyAroundGUI::_startExistingFlyaroundComboBoxClicked(QString value)
    {
        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);

        if (!_uiHandler.valid())
        {
            return;
        }

        if (value.toStdString() == "")
        {
            emit showError("Name", "Flyaround name not specified", true);
            return;
        }

        std::string savedValue = value.toStdString();

        SMCUI::IFlyAroundUIHandler::FlyAround flyAround = _getFlyaround(savedValue);

        if (flyAround.distance < 0)
        {
            emit showError("Flyaround", "Saved Values Not found", true);
        }

        CORE::RefPtr<CORE::IPoint> point = _getSelectedPointFeature();
        if (point.valid())
        {
            _uiHandler->setPointCoordinates(point);
            _cameraUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager());
            _start = true;
            _uiHandler->start(flyAround, _cameraUIHandler);

        }

        _lastFlyaroundName = value;
    }

    // stop flyAround
    void FlyAroundGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }

        if (_start)
        {
            _start = false;
            _uiHandler->pause(SMCUI::IFlyAroundUIHandler::Mode::Stop);
        }

        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (FlyAroundAnalysisObject)
        {
            FlyAroundAnalysisObject->setProperty("playVisible", true);
            FlyAroundAnalysisObject->setProperty("pauseVisible", false);
        }


    }

    // pause flyAround
    void FlyAroundGUI::_handlePauseClicked()
    {
        if (!_uiHandler.valid())
        {
            return;
        }

        if (_start)
        {
            _start = false;
            _uiHandler->pause(SMCUI::IFlyAroundUIHandler::Mode::Pause);
        }

    }

    // preview flyAround
    void FlyAroundGUI::_handlePreviewClicked()
    {
        
        if (!_uiHandler.valid())
        {
            return;
        }

        SMCUI::IFlyAroundUIHandler::FlyAround flyAround;

        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (FlyAroundAnalysisObject)
        {

            QString lpDistance = FlyAroundAnalysisObject->property("distanceString").toString();
            if (lpDistance == "")
            {
                emit showError("Distance", "Flyaround distance not specified", true);
                return;
            }

            flyAround.distance = lpDistance.toDouble();

            QString lpHeight = FlyAroundAnalysisObject->property("heightString").toString();
            if (lpHeight == "")
            {
                emit showError(" Viewing Height", "Flyaround height not specified", true);
                return;
            }

            flyAround.height = lpHeight.toDouble();

            QString lpLookHeight = FlyAroundAnalysisObject->property("lookAtHeightString").toString();
            if (lpLookHeight == "")
            {
                emit showError("Look at Height", "Flyaround height not specified", true);
                return;
            }

            flyAround.lookAtHeight = lpLookHeight.toDouble();

            QString lpArcAngle = FlyAroundAnalysisObject->property("arcAngleString").toString();
            if (lpArcAngle == "")
            {
                emit showError("ArcAngle", "Flyaround arcangle not specified", true);
                return;
            }

            flyAround.arcAngle = lpArcAngle.toDouble();

            QString lpStartAngle = FlyAroundAnalysisObject->property("startAngleString").toString();
            if (lpStartAngle == "")
            {
                emit showError("StartAngle", "Flyaround startangle not specified", true);
                return;
            }

            flyAround.startAngle = lpStartAngle.toDouble();
            flyAround.loop = FlyAroundAnalysisObject->property("loop").toBool();

            bool reverseChecked = FlyAroundAnalysisObject->property("reverseChecked").toBool();
            int reverse = 1;

            if (reverseChecked)
            {
                reverse = -1;
            }

            flyAround.reverse = reverse;
        }

        CORE::RefPtr<CORE::IPoint> point = _getSelectedPointFeature();
        if (point.valid())
        {
            _uiHandler->setPointCoordinates(point);
            _cameraUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager());
            _start = true;
            _preview = true;
            _uiHandler->start(flyAround, _cameraUIHandler);
        }

    }

    // increase speed
    void FlyAroundGUI::_increaseSpeed()
    {
        if (!_uiHandler.valid())
        {
            return;
        }

        if (_start)
        {
            _uiHandler->increaseSpeed();
        }
       
    }

    // decrease speed
    void FlyAroundGUI::_decreaseSpeed()
    {
        if (!_uiHandler.valid())
        {
            return;
        }

        if (_start)
        {
            _uiHandler->decreaseSpeed();
        }
    }

    // resets variables
    void FlyAroundGUI::_reset()
    {
        QObject* FlyAroundAnalysisObject = _findChild(FlyAroundAnalysisPopup);
        if (FlyAroundAnalysisObject)
        {
            FlyAroundAnalysisObject->setProperty("flyaroundNameString", "");
            FlyAroundAnalysisObject->setProperty("distanceString", "");
            FlyAroundAnalysisObject->setProperty("heightString", "");
            FlyAroundAnalysisObject->setProperty("lookAtHeightString", "");
            FlyAroundAnalysisObject->setProperty("arcAngleString", "");
            FlyAroundAnalysisObject->setProperty("playVisible", true);
            FlyAroundAnalysisObject->setProperty("pauseVisible", false);
            FlyAroundAnalysisObject->setProperty("openFlyaroundParameterGrid",false);
            FlyAroundAnalysisObject->setProperty("editFlyAroundButton", false);
            FlyAroundAnalysisObject->setProperty("loop", false);
            FlyAroundAnalysisObject->setProperty("reverseChecked",false);
        }
    }

}//namespace SMCQt
