/*****************************************************************************
 *
 * File             : RasterUploadDialogGUI.cpp
 * Description      : RasterUploadDialogGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, VizExperts India Pvt. Ltd.
 *****************************************************************************/
#include <SMCQt/PGCreateDataTypeDialogGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <QtGui/QIntValidator>

#include <QFileDialog>
#include <QMessageBox>

#include <QtCore/QFile>

#include <QtUiTools/QtUiTools>

#include <Util/Log.h>

#include <App/UIHandlerFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/AccessElementUtils.h>

#include <SMCUI/SMCUIPlugin.h>
#include <SMCUI/IAddContentStatusMessage.h>
#include <SMCUI/AddContentUIHandler.h>
#include <vector>

#include <Database/IRasterDatabase.h>

#include <VizQt/IQtGUIManager.h>
#include <Core/ISelectionComponent.h>
#include <VizUI/ISelectionUIHandler.h>



namespace SMCQt
{
    PGCreateDataTypeDialogGUI::PGCreateDataTypeDialogGUI() : _dbSelect(false)
    {
    }

    PGCreateDataTypeDialogGUI::~PGCreateDataTypeDialogGUI()
    {    
    }

    void 
    PGCreateDataTypeDialogGUI::popupLoaded(QString type)
    {
        QObject* createDataType = _findChild("createDataType");
        if(createDataType != NULL)
        {
            QObject::connect( createDataType, SIGNAL(createButtonClicked()), this ,SLOT(_handleCreateButtonClicked()), Qt::UniqueConnection);
            createDataType->setProperty("rasterCheckBox",QVariant::fromValue(true)); 
            std::string tempDatabaseName;
            ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::LAYERDATABASECONN);
            if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Database"))
            tempDatabaseName = settingAttrib.keyValuePair["Database"];
            databaseName = tempDatabaseName.c_str();  
            createDataType->setProperty("createdDatabase",QVariant::fromValue(databaseName)); 
            _dbSelect = true;
            _displaySchema();
        }
    }
       


    void 
    PGCreateDataTypeDialogGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
                CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("SettingComponent");
                _globalSettingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void 
    PGCreateDataTypeDialogGUI::_loadAndSubscribeSlots()
    {
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }


   void PGCreateDataTypeDialogGUI::_displaySchema()
   {
        dataTypeList.clear();                  
        if (databaseName!="")
        {
            if(makeConnectionWithDatabase())
            {
                if(!_dbUIHandler)
                {
                    LOG_ERROR("Invalid DBUIHandler");
                    return;
                }

                std::vector<std::string> schemaList = _dbUIHandler->getSchemaList();    
                for(unsigned int i = 0; i < schemaList.size(); i++)
                {
                    const char *dataType;
                    dataType = strchr(schemaList[i].c_str(),'_');
                    if (dataType != NULL)
                    {
                        dataType = dataType + 1;
                        dataType = strchr(dataType,'_');
                        if(dataType != NULL)
                        {
                            dataType = dataType + 1;
                            if(strlen(dataType) != 0)
                            {
                                std::string data = dataType;
                                dataTypeList.push_back(dataType);
                            }
                        }
                    }
                }
                _setContextProperty("dataTypeListed", QVariant::fromValue(dataTypeList));
                _dbSelect = true;
            }
        }

    }
 
    void 
    PGCreateDataTypeDialogGUI::_handleCreateButtonClicked()
    {
        if(_dbSelect)
        {

            std::string dataType = "";
            QObject* createDataType = _findChild("createDataType");
            if(createDataType)
            {             
                QString tempDataType= createDataType->property("newDataType").toString();
                _rasterRadioButton= createDataType->property("rasterCheckBox").toBool();
                _vectorRadioButton= createDataType->property("vectorCheckBox").toBool();
                _elevationRadioButton= createDataType->property("elevationCheckBox").toBool();
                dataType = tempDataType.toStdString();
            }
            
            if(dataType != "")
            {            
                std::string schema = "tkp_";
                if(_rasterRadioButton)
                {
                    schema += "raster_" + dataType;
                }
                else if(_vectorRadioButton)
                {
                    schema += "vector_" + dataType;
                }
                else if(_elevationRadioButton)
                {
                    schema += "elevation_" + dataType;
                }
                if(_dbUIHandler->createSchema(schema))
                {          
                    dataTypeList.push_back(dataType.c_str());
                    _setContextProperty("dataTypeListed", QVariant::fromValue(dataTypeList));        
                    createDataType->setProperty("newDataType", "");                    
                    showError("Success", "Data Type has been created" , false);
                }
                else
                {
                    showError("Failed", "Failed to create Data Type");
                }

            }
            else
            {                
                showError("Invalid DataType", "Enter Valid DataType");
            }
        }
        else
        {          
            showError("Invalid Database", "Please select valid Database");
        }

    }



    bool
    PGCreateDataTypeDialogGUI::makeConnectionWithDatabase()
    {
         //Getting the dataUIhandler for geting the vector and raster data from the postgresql
        _dbUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getGUIManager());

        return _checkConnectionWithDatabase();
    }

    bool
    PGCreateDataTypeDialogGUI::_checkConnectionWithDatabase()
    {
        _dbOptions.clear();
        _dbOptions = _getDatabaseCredential();
        if(_dbOptions.size() < 5)
            return false;

        _dbUIHandler->initialize(_dbOptions);
        std::string status;
        if(_dbUIHandler->testConnection(_dbOptions))
        {
            status = "Connected";
            _dbOptions.insert(std::pair<std::string, std::string>("Status", status));
            _dbUIHandler->initialize(_dbOptions);
            return true;
        }
        else
        {
            status = "Not Connected";
            _dbOptions.insert(std::pair<std::string, std::string>("Status", status));
            _dbUIHandler->initialize(_dbOptions);
            return false;
        }

        return false;
    }

           
    VizDataUI::IDatabaseUIHandler*
    PGCreateDataTypeDialogGUI::_getDatabaseUIHandler()
    {
        VizDataUI::IDatabaseUIHandler* dbUIHandler = NULL;
        if(!_dbUIHandler)
        {
            CORE::RefPtr<APP::IUIHandlerFactory> uf = APP::ApplicationRegistry::instance()->getUIHandlerFactory();
            const APP::IUIHandlerType *uhType = uf->getUIHandlerType("DatabaseUIHandler");
            CORE::RefPtr<APP::IUIHandler> _uih = uf->createUIHandler(*uhType);
            _dbUIHandler = _uih->getInterface<VizDataUI::IDatabaseUIHandler>();
        }
        return dbUIHandler;
    }

    std::map<std::string, std::string>
    PGCreateDataTypeDialogGUI::_getDatabaseCredential()
    {
        // Then Get the Database Information(credentials) of connected Database
        ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::LAYERDATABASECONN);

        //Filiing the credential in the dataabase options 
        std::map<std::string, std::string> dbOptions;

        std::map<std::string, std::string>::iterator iter = settingAttrib.keyValuePair.begin();
        while(iter != settingAttrib.keyValuePair.end())
        {
            dbOptions.insert(std::pair<std::string, std::string>(iter->first, iter->second));
            iter++;
        }

        return dbOptions;
    }
    

    bool
    PGCreateDataTypeDialogGUI::_presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap, const std::string& keyword)
    {
        if(settingMap.find(keyword) != settingMap.end())
        {
            return true;
        }
        return false;
    }

 
}
