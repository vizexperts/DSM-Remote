/****************************************************************************
*
* File             : WatershedGenerationGUI.h
* Description      : WatershedGenerationGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/
//#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/WatershedGenerationGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, WatershedGenerationGUI);
    DEFINE_IREFERENCED(WatershedGenerationGUI,DeclarativeFileGUI);

    const std::string WatershedGenerationGUI::WatershedAnalysisPopup = "WatershedAnalysis";

    WatershedGenerationGUI::WatershedGenerationGUI()
        : _state(NO_MARK)
    {
        
    }

    WatershedGenerationGUI::~WatershedGenerationGUI()
    {}

    void WatershedGenerationGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(_handleProgressValueChanged(int)),Qt::QueuedConnection);
    }

    void WatershedGenerationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IWatershedGenerationUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                switch(_state)
                {
                case AREA_MARK: 
                    {
                        // Get the polygon
                        CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                        // Pass the polygon points to the surface area calc handler
                        CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                        if(line.valid() && line->getPointNumber() > 1)
                        {
                            // Get 0th and 2nd point since line is drawn clockwise
                            osg::Vec3d first    = line->getPoint(0)->getValue();
                            osg::Vec3d second   = line->getPoint(2)->getValue();
                            QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
                            if(WatershedAnalysisObject)
                            {
                                WatershedAnalysisObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                                WatershedAnalysisObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                                WatershedAnalysisObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                                WatershedAnalysisObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                            }
                        }
                        break;
                    }
                case OUTLETREGION_MARK: 
                    {
                        // Get the polygon
                        CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                        // Pass the polygon points to the surface area calc handler
                        CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                        if(line.valid() && line->getPointNumber() > 1)
                        {
                            // Get 0th and 2nd point since line is drawn clockwise
                            osg::Vec3d first    = line->getPoint(0)->getValue();
                            osg::Vec3d second   = line->getPoint(2)->getValue();
                            QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
                            if(WatershedAnalysisObject)
                            {
                                WatershedAnalysisObject->setProperty("outletBottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                                WatershedAnalysisObject->setProperty("outletBottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                                WatershedAnalysisObject->setProperty("outletTopRightLongitude",UTIL::ToString(second.x()).c_str());
                                WatershedAnalysisObject->setProperty("outletTopRightLatitude",UTIL::ToString(second.y()).c_str());
                            }
                        }
                        break;
                    }
                default:
                    break;
                }

            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
             GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if(progress < 100)
                {
                   emit _setProgressValue(progress);
                }
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                    emit _setProgressValue(100);
            }
            else
            {
                 if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    emit showError("Filter Status", "Failed to apply filter");
                    //Again Set Calculate 
                    _handlePBStopClicked();
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void WatershedGenerationGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
                if(WatershedAnalysisObject)
                {
                    // start and stop button handlers
                    connect(WatershedAnalysisObject, SIGNAL(startAnalysis()),this,SLOT(_handlePBStartClicked()),Qt::UniqueConnection);
                    connect(WatershedAnalysisObject, SIGNAL(stopAnalysis()),this, SLOT(_handlePBStopClicked()),Qt::UniqueConnection);
                    connect(WatershedAnalysisObject, SIGNAL(markPosition(bool)),this,SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);
                    connect(WatershedAnalysisObject, SIGNAL(markOutletPosition(bool)),this,SLOT(_handlePBOutletAreaClicked(bool)),Qt::UniqueConnection);

                    // enable start button
                    WatershedAnalysisObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    WatershedAnalysisObject->setProperty("isStopButtonEnabled",false);

                    //disable progress value
                    WatershedAnalysisObject->setProperty("progressValue",0);
                }
            }
            else
            {
                _reset();
                _handlePBStopClicked();
            }
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void WatershedGenerationGUI::_handleProgressValueChanged(int value)
    {
        QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
        if(WatershedAnalysisObject)
        {
            WatershedAnalysisObject->setProperty("progressValue",QVariant::fromValue(value));
        } 
    }

    //! 
    void WatershedGenerationGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
        if(WatershedAnalysisObject)
        {
            std::string StreamOutputName = WatershedAnalysisObject->property("outputNameStream").toString().toStdString();

            if(StreamOutputName == "")
            {
                showError( "Insufficient / Incorrect data", "Stream Output name is not specified");
                return;
            }
            WatershedAnalysisObject->setProperty("outputNameStream",QString::fromStdString(StreamOutputName));
            _uiHandler->setStreamOutputName(StreamOutputName);

            std::string WatershedOutputName = WatershedAnalysisObject->property("outputNameWater").toString().toStdString();

            if(WatershedOutputName == "")
            {
                showError( "Insufficient / Incorrect data", "Stream Output name is not specified");
                return;
            }
            WatershedAnalysisObject->setProperty("outputNameWater",QString::fromStdString(WatershedOutputName));
            _uiHandler->setWatershedOutputName(WatershedOutputName);

            //get offset
            QString TempThresholdVal=WatershedAnalysisObject->property("thresholdVal").toString();
            if(TempThresholdVal == "")
            {
                showError( "Insufficient / Incorrect data", "Threshold Value is not specified");
                return;
            }
            double Threshold = TempThresholdVal.toDouble();;
            if(Threshold < 0)
            {
                showError( "Insufficient / Incorrect data", "Please enter a Threshold Value  ");
                return;
            }
            _uiHandler->setThreshold (Threshold);


            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = WatershedAnalysisObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = WatershedAnalysisObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = WatershedAnalysisObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() =  WatershedAnalysisObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError( "Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
                        showError( "Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                }
            }
            osg::Vec4d outletExtents;
            //set outlet Aread
            {
                bool okx = true;
                outletExtents.x() = WatershedAnalysisObject->property("outletBottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                outletExtents.y() = WatershedAnalysisObject->property("outletBottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                outletExtents.z() = WatershedAnalysisObject->property("outletTopRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                outletExtents.w() =  WatershedAnalysisObject->property("outletTopRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError( "Insufficient / Incorrect data", "Outlet Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(outletExtents.x() >= outletExtents.z() || outletExtents.y() >= outletExtents.w())
                    {
                        showError( "Insufficient / Incorrect data", "Outlet Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setOutletExtents(outletExtents);
                }
            }

            WatershedAnalysisObject->setProperty("progressValue",0); 
            _handlePBAreaClicked(false);
            _handlePBOutletAreaClicked(false);
            // start filter

            _uiHandler->execute();
        }
    }

    void WatershedGenerationGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();
        QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
        if(WatershedAnalysisObject)
        {
            //disable stop button
            WatershedAnalysisObject->setProperty("isStopButtonEnabled",false);
            WatershedAnalysisObject->setProperty("isStartButtonEnabled",true);
            WatershedAnalysisObject->setProperty("progressValue",QVariant::fromValue(0));
        } 
    }

    void WatershedGenerationGUI::_handlePBRunInBackgroundClicked()
    {}

    void WatershedGenerationGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _handlePBOutletAreaClicked(false);
            _state = AREA_MARK;
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
             _state = NO_MARK;
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            
            QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
            if(WatershedAnalysisObject)
            {
                WatershedAnalysisObject->setProperty( "isMarkSelected" , false );
            }
        }
    }

    void WatershedGenerationGUI::_handlePBOutletAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _handlePBAreaClicked(false);
            _state = OUTLETREGION_MARK;
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
             _state = NO_MARK;
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            
            QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
            if(WatershedAnalysisObject)
            {
                WatershedAnalysisObject->setProperty( "isOutletMarkSelected" , false );
            }
        }
    }
    void WatershedGenerationGUI::_reset()
    {
        QObject* WatershedAnalysisObject = _findChild(WatershedAnalysisPopup);
        if(WatershedAnalysisObject)
        {
            WatershedAnalysisObject->setProperty("isMarkSelected",false);
            WatershedAnalysisObject->setProperty("isOutletMarkSelected",false);
            WatershedAnalysisObject->setProperty("bottomLeftLongitude","longitude");
            WatershedAnalysisObject->setProperty("bottomLeftLatitude","latitude");
            WatershedAnalysisObject->setProperty("topRightLongitude","longitude");
            WatershedAnalysisObject->setProperty("topRightLatitude","latitude");

            WatershedAnalysisObject->setProperty("outletBottomLeftLongitude","longitude");
            WatershedAnalysisObject->setProperty("outletBottomLeftLatitude","latitude");
            WatershedAnalysisObject->setProperty("outletTopRightLongitude","longitude");
            WatershedAnalysisObject->setProperty("outletTopRightLatitude","latitude");

            WatershedAnalysisObject->setProperty("outputNameStream","");
            WatershedAnalysisObject->setProperty("outputNameWater","");

            WatershedAnalysisObject->setProperty("thresholdVal",0);
            WatershedAnalysisObject->setProperty("progressValue",0);
            _handlePBAreaClicked(false);
            _handlePBOutletAreaClicked(false);
            _state=NO_MARK;
        }
    }

}//namespace SMCQt
