
#include <SMCQt/VizProfileObject.h>

namespace SMCQt{

    VizProfileObject::VizProfileObject() 
    {

    }

    VizProfileObject::VizProfileObject(const QString& name, const QString& color, const bool active):
        _name(name), _color(color), _active(active)
    {

    }

    VizProfileObject::VizProfileObject(const VizProfileObject& obj)
    {
        _name = obj.name(); 
        _color = obj.color();
        _active = obj.active();
    }

    QString VizProfileObject::name() const
    {
        return _name; 
    }

    void VizProfileObject::setName(const QString& name)
    {
        if (name != _name)
        {
            _name = name; 
            emit nameChanged(); 
        }
    }

    QString VizProfileObject::color() const
    {
        return _color; 
    }

    void VizProfileObject::setColor(const QString& color)
    {
        if (_color != color)
        {
            _color = color; 
            emit colorChanged(); 
        }
    }

    bool VizProfileObject::active() const
    {
        return _active; 
    }

    void VizProfileObject::setActive(bool active)
    {
        if (_active != active)
        {
            _active = active; 
            emit activeChanged(); 
        }
    }
}