/****************************************************************************
*
* File             : MeasureAreaGUI.h
* Description      : MeasureAreaGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <Core/IDeletable.h>
#include <Core/ICompositeObject.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <Core/Line.h>

#include <SMCQt/MeasureAreaGUI.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Util/StringUtils.h>
#include <Util/UnitsConversionUtils.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, MeasureAreaGUI);

    const std::string MeasureAreaGUI::MeasureAreaGUIPopup = "areaAnalysis";

    MeasureAreaGUI::MeasureAreaGUI()
        : _resetFlag(false)
        , _markingEnabled(false)
        , _enableDrag(false)
        ,_selectedUnit("m")
    {
    }

    MeasureAreaGUI::~MeasureAreaGUI()
    {
    }

    void MeasureAreaGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(handleAreaUpdateTimer()));

        QObject::connect(this, SIGNAL(_calculationCompleted()),
            this, SLOT(handleCalculationCompleted()), Qt::QueuedConnection);
    }

    void MeasureAreaGUI::setActive(bool value)
    {
        if(getActive() == value)
            return;

        if(value)
        {
            QObject* areaAnalysis = _findChild(MeasureAreaGUIPopup);
            if(areaAnalysis)
            {
                _timer.start(10);

                QObject::connect(areaAnalysis, SIGNAL(changeUnit(QString)),
                    this, SLOT(changeUnit(QString)), Qt::UniqueConnection);

                QObject::connect(areaAnalysis, SIGNAL(mark(bool)),
                    this, SLOT(markArea(bool)), Qt::UniqueConnection);

                QObject::connect(areaAnalysis, SIGNAL(selectArea(bool)), 
                    this, SLOT(selectArea(bool)), Qt::UniqueConnection);

                QObject::connect(areaAnalysis, SIGNAL(start()), 
                    this, SLOT(startCalculation()), Qt::UniqueConnection);

                QObject::connect(areaAnalysis, SIGNAL(stop()), 
                    this, SLOT(stopCalculation()), Qt::UniqueConnection);

            }
        }
        else
        {
            stopCalculation();
            if(_markingEnabled)
            {
                markArea(false);
            }
            else
            {
                selectArea(false);
            }

            _selectedUnit = "m";

            _timer.stop();
        }

        DeclarativeFileGUI::setActive(value);
    }

    void MeasureAreaGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the AreaUIHandler and set it
            try
            {   
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IAreaUIHandler>(getGUIManager());                

                _surfaceAreaCalcHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::ISurfaceAreaCalcUIHandler>(getGUIManager());                

                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

                if(component.valid())
                {
                    _selectionComponent = component->getInterface<CORE::ISelectionComponent>();
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
        }
        // Check whether the area calculation is completed
        else if(messageType == *SMCUI::ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType)
        {
            _surfaceAreaCalcHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _unsubscribe(_surfaceAreaCalcHandler.get(), 
                *SMCUI::ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType);

            emit _calculationCompleted();
        }
        else if(messageType == *CORE::ISelectionComponent::ObjectClickedMessageType)
        {
            const CORE::ISelectionComponent::SelectionMap& map = 
                _selectionComponent->getCurrentSelection();

            _area = NULL;

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();
            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                CORE::IPolygon* area = iter->second->getInterface<CORE::IPolygon>();
                if(area)
                {
                    _area = area;
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void MeasureAreaGUI::handleCalculationCompleted()
    {
        QObject* areaAnalysis = _findChild(MeasureAreaGUIPopup);
        if(areaAnalysis)
        {
            areaAnalysis->setProperty("calculating", QVariant::fromValue(false));
        }
    }

    void MeasureAreaGUI::handleAreaUpdateTimer()
    {
        if(_surfaceAreaCalcHandler.valid())
        {             
            double convertedUnits;

            UTIL::UnitsConversionUtils::convert(UTIL::UnitsConversionUtils::M,
                _selectedUnit.toStdString(), _surfaceAreaCalcHandler->getArea(), convertedUnits, 2);

            QObject* areaAnalysis = _findChild(MeasureAreaGUIPopup);

            QString unit = QString::number(convertedUnits, 'f', 2);

            if(areaAnalysis)
            {
                if(!_resetFlag)
                {
                    areaAnalysis->setProperty("value", QVariant::fromValue(unit));
                }
                else 
                {
                    areaAnalysis->setProperty("value", QVariant::fromValue(0.0));
                }
            }
        }   

    }

    void MeasureAreaGUI::changeUnit(QString unit)
    {
        if(_selectedUnit != unit)
        {
            _selectedUnit = unit;
        }
    }

    void MeasureAreaGUI::selectArea(bool value)
    {
        if(!_selectionComponent.valid())
        {
            return;
        }

        _selectionComponent->clearCurrentSelection();

        if(value)
        {
            // set selection state to intermediate selection
            // subscribe to object clicked message

            _selectionComponent->setIntermediateSelection(true);
            _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
        }
        else
        {
            _selectionComponent->setIntermediateSelection(false);
            _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
            _area = NULL;
            _resetFlag = true;
        }

    }

    void MeasureAreaGUI::startCalculation()
    {
        if(_areaHandler.valid())
        {
            if(!_area.valid())
            {
                emit showError("Measure Area", "No Area Drawn", false);
                return;
            }


            CORE::RefPtr<CORE::IClosedRing> line = _area->getExteriorRing();

            if(!line.valid())
            {
                emit showError("Measure Area", "No Area Drawn", false);
                return;
            }

            if(line->getPointNumber() < 3)
            {
                emit showError("Measure Area", "No Area Drawn", false);
                return;
            }

            _areaHandler->_setProcessMouseEvents(false);

            // set the computation type
            bool projected = false;
            QObject* areaAnalysis = _findChild(MeasureAreaGUIPopup);
            if(areaAnalysis)
            {
                projected = areaAnalysis->property("projected").toBool();
            }

            if(projected)
            {
                _surfaceAreaCalcHandler->setComputationType(SMCUI::ISurfaceAreaCalcUIHandler::PROJECTED);
            }
            else 
            {
                _surfaceAreaCalcHandler->setComputationType(SMCUI::ISurfaceAreaCalcUIHandler::AERIAL);
            }

            // Check the status of the UIHandler
            // If not busy, then give the polygon to the area calculator
            //            _areaHandler->setProcessMouseEvents(false);

            // Subscribe to area calculation message
            if(_surfaceAreaCalcHandler.valid())
            {
                _subscribe(_surfaceAreaCalcHandler.get(), 
                    *SMCUI::ISurfaceAreaCalcUIHandler::AreaCalculationCompletedMessageType);

                _surfaceAreaCalcHandler->getInterface<APP::IUIHandler>()->setFocus(true);

                osg::ref_ptr<osg::Vec3dArray> arr (new osg::Vec3dArray);
                for(unsigned int i=0; i < line->getPointNumber(); i++)
                {
                    arr->push_back(osg::Vec3d(line->getPoint(i)->getValue()));
                }

                _surfaceAreaCalcHandler->setPoints(arr.get());

                if(areaAnalysis)
                {
                    areaAnalysis->setProperty("calculating", QVariant::fromValue(true));
                }

                _surfaceAreaCalcHandler->execute();
                _resetFlag = false;
            }
        }
    }

    void MeasureAreaGUI::stopCalculation()
    {
        if(_surfaceAreaCalcHandler.valid())
        {
            _surfaceAreaCalcHandler->stop();
        }
        else
        {
            LOG_ERROR("Area Computation Filter not valid");
        }
    }

    void MeasureAreaGUI::_reset()
    {
        _resetFlag = true;
        _area = NULL;
    }

    void MeasureAreaGUI::markArea(bool value)
    {
        if(value)
        {
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_AREA);
            _area = _areaHandler->getCurrentArea();
        }
        else
        {
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            if(_area.valid())
            {
                _removeObject(_area.get());
                _area = NULL;
            }
            _resetFlag = true;
        }
        _markingEnabled = value;
    }
} // namespace SMCQt
