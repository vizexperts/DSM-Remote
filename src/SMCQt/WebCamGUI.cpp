#include <SMCQt/WebCamGUI.h>

#include <VizQt/LayoutFileGUI.h>
#include <VizQt/NameValidator.h>

#include <SMCUI/IAddContentStatusMessage.h>
#include <SMCUI/AddContentUIHandler.h>

#include <QFileDialog>

#include <QFile>
#include <QtUiTools/QtUiTools>

#include <osgDB/FileNameUtils>
#include <osg/Vec4>

#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>

#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/ApplicationRegistry.h>

#include <Elements/IMilitarySymbolTypeLoader.h>

#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>

#include <Util/Log.h>

#include <sstream>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, WebCamGUI)
        DEFINE_IREFERENCED(WebCamGUI, VizQt::GUI)

        void 
        WebCamGUI::_loadAndSubscribeSlots()
    {
        VizQt::LayoutFileGUI::_loadAndSubscribeSlots();
        makeConnections();
    }

    WebCamGUI::WebCamGUI(QWidget *parent) : _isAppInstanceRunning(false)
    {
    }

    void WebCamGUI::setActive(bool active)
    {
        if(_isAppInstanceRunning!=true)
        {
            if(active)
            {
                UTIL::ThreadPool* tp = CORE::WorldMaintainer::instance()->getComputeThreadPool();
                if(tp)
                {
                    tp->invoke(boost::bind(&WebCamGUI::_startWebCam, this));
                }
            }
        }

        VizQt::LayoutFileGUI::setActive(active);
    }

    void WebCamGUI::_startWebCam()
    {
        _isAppInstanceRunning = true;
        system("MicroDVR2.0.exe");
        _isAppInstanceRunning = false;
    }

    WebCamGUI::~WebCamGUI()
    {
    }

    void WebCamGUI::initialize()
    {
    }

    void WebCamGUI::makeConnections()
    {
    }

    void WebCamGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Set the NetworkCollborationManager
            try
            { 
                initialize();
            }
            catch(...)
            {}
        }
    }

}
