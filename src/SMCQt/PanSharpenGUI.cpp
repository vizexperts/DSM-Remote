/****************************************************************************
*
* File             : PanSharpenGUI.h
* Description      : PanSharpenGUI class definition
*
*****************************************************************************
* Copyright (c) 2010-2011, VizExperts india Pvt. Ltd.                                       
*****************************************************************************/

#include <SMCQt/PanSharpenGUI.h>
#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>
#include <Core/IVisitorFactory.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <SMCUI/AddContentUIHandler.h>
#include <osgDB/FileNameUtils>
#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/IUIHandlerManager.h>
#include <App/ApplicationRegistry.h>
#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>
#include <Util/Log.h>
#include <Util/StringUtils.h>
#include <VizQt/GUI.h>
#include <QFileDialog>
#include <QtCore/QFile>
#include <QtUiTools/QtUiTools>
#include <iostream>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, PanSharpenGUI);
    DEFINE_IREFERENCED(PanSharpenGUI, CORE::Base);

    const std::string PanSharpenGUI::PanSharpenGUIObjectName = "panSharpening";

    PanSharpenGUI::PanSharpenGUI()
        :_stereoWizard(NULL)
    {
    }

    PanSharpenGUI::~PanSharpenGUI()
    {
        setActive(false);
    }

    // to load the connect and disconnect slot from the qml
    void PanSharpenGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this,SIGNAL(_updateGUIStatus()),this,SLOT(_handleGUIStatus()),Qt::QueuedConnection);

    }

    void PanSharpenGUI::_loadAddContentUIHandler()
    {
        _addContentUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
    }

    // to browse the pan image
    void 
        PanSharpenGUI::_handleBrowsePanImage()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            QObject* panSharpenPopup = _findChild(PanSharpenGUIObjectName);
            if(panSharpenPopup != NULL)
            {
                panSharpenPopup->setProperty("panImagePath",QVariant::fromValue(filename));
                panSharpenPopup->setProperty("panButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }

    // to browse the MS image
    void 
        PanSharpenGUI::_handleBrowseMSImage()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {
            QObject* panSharpenPopup = _findChild(PanSharpenGUIObjectName);
            if(panSharpenPopup != NULL)
            {
                panSharpenPopup->setProperty("msImagePath",QVariant::fromValue(filename));
                panSharpenPopup->setProperty("msButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }


    // to browse the band1 image
    void 
        PanSharpenGUI::_handleBrowseBand1Image()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            QObject* panSharpenPopup = _findChild(PanSharpenGUIObjectName);
            if(panSharpenPopup != NULL)
            {
                panSharpenPopup->setProperty("band1ImagePath",QVariant::fromValue(filename));
                panSharpenPopup->setProperty("band1ImageButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }

    // to browse the band2 image
    void 
        PanSharpenGUI::_handleBrowseBand2Image()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            QObject* panSharpenPopup = _findChild(PanSharpenGUIObjectName);
            if(panSharpenPopup != NULL)
            {
                panSharpenPopup->setProperty("band2ImagePath",QVariant::fromValue(filename));
                panSharpenPopup->setProperty("band2ImageButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }

    // to browse the band3 image
    void 
        PanSharpenGUI::_handleBrowseBand3Image()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            QObject* panSharpenPopup = _findChild(PanSharpenGUIObjectName);
            if(panSharpenPopup != NULL)
            {
                panSharpenPopup->setProperty("band3ImagePath",QVariant::fromValue(filename));
                panSharpenPopup->setProperty("band3ImageButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }

    // to browse and save output image
    void 
        PanSharpenGUI::_handleBrowseOutputImage()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getSaveFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            QObject* panSharpenPopup = _findChild(PanSharpenGUIObjectName);
            if(panSharpenPopup != NULL)
            {
                panSharpenPopup->setProperty("outputNamePath",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in writing File", "Unknown Error", true);
            }
        }

    }

    void 
        PanSharpenGUI::closeStereoWizard(Stereo::MainWindowGUI* stereoWiz)
    {        
        if (stereoWiz)
        {
            delete stereoWiz;
            stereoWiz = NULL;
        }
    }


    // to view the specified image    
    void 
        PanSharpenGUI::_viewImage(QString imagePath)
    { 
        std::string filename = imagePath.toStdString();
        if(filename != "")
        {                
            Stereo::MainWindowGUI* stereoWizard = new Stereo::MainWindowGUI(filename,Stereo::Image::NONE);
            _stereoWizard.push_back(stereoWizard);
            QObject::connect(stereoWizard,SIGNAL(applicationClosed(Stereo::MainWindowGUI*)),
                this,SLOT(closeStereoWizard(Stereo::MainWindowGUI*)), Qt::QueuedConnection);
            stereoWizard->showMaximized();
            stereoWizard->setWindowTitle(filename.c_str());
        }
    }


    // to view the multi-spectral image   
    void 
        PanSharpenGUI::_showMSImage()
    { 
        if(_panVisitor.valid())    
        {   std::string filename = _panVisitor->getMSpath();
        if(filename != "")
        {                
            Stereo::MainWindowGUI* stereoWizard = new Stereo::MainWindowGUI(filename,Stereo::Image::NONE);
            _stereoWizard.push_back(stereoWizard);
            QObject::connect(stereoWizard,SIGNAL(applicationClosed(Stereo::MainWindowGUI*)),
                this,SLOT(closeStereoWizard(Stereo::MainWindowGUI*)), Qt::QueuedConnection);
            stereoWizard->showMaximized();
            stereoWizard->setWindowTitle("multispectral Image");
        }
        }
    }

    void
        PanSharpenGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            { 
                _loadAddContentUIHandler();
            }
            catch(...)
            {}
        }
        // to check the filter status
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                QObject* panSharpenObject = _findChild(PanSharpenGUIObjectName);
                if(panSharpenObject)
                {
                    std::string msFlag = panSharpenObject->property("msImagePath").toString().toStdString();
                    if(msFlag == "")
                    {
                        panSharpenObject->setProperty("msImageEnabled",true);
                    }
                    panSharpenObject->setProperty("outputButtonEnabled",true);
                }
                emit _updateGUIStatus();
                emit showError("Filter Status", "Successfully applied filter", false);
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
            {
                emit _updateGUIStatus();
                emit showError("Filter Status", "Failed to apply filter");
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }

    //function called when Fuse button is clicked
    void 
        PanSharpenGUI::_fuseImages(QString panImagePath, QString msImagePath,QString band1ImagePath,QString band2ImagePath,QString band3ImagePath )
    {            
        std::string panPath = panImagePath.toStdString();
        std::string msPath = msImagePath.toStdString();
        std::string band1Path = band1ImagePath.toStdString();
        std::string band2Path = band2ImagePath.toStdString();
        std::string band3Path = band3ImagePath.toStdString();
        QString outputName;
        std::string advancedoption;

        if(panPath.empty() || ( msPath.empty()&&(band1Path.empty()||band2Path.empty()||band3Path.empty())))
        {
            std::string title = "Add Image File Error";
            std::string text = "filename missing";

            showError(title.c_str(), text.c_str());
            return;
        }



        QObject* panSharpenObject = _findChild(PanSharpenGUIObjectName);
        if(panSharpenObject)
        {

            outputName = panSharpenObject->property("outputNamePath").toString();
            advancedoption = panSharpenObject->property("showAdvancedOptions").toString().toStdString();


            if(outputName == "")
            {
                showError("outputName", "Please enter a outputName");
                return;
            }

            // Pass the parameters to the visitor
            panSharpenObject->setProperty("startButtonEnabled",false);
            panSharpenObject->setProperty("stopButtonEnabled",true);
            panSharpenObject->setProperty("calculating",true);
            panSharpenObject->setProperty("msImageEnabled",false);
            panSharpenObject->setProperty("outputButtonEnabled",false);
            if(advancedoption == "true")
            {
                panSharpenObject->setProperty("msImagePath","");
                msPath.clear();

            }
            else if (advancedoption == "false")
            {
                panSharpenObject->setProperty("band1ImagePath","");
                panSharpenObject->setProperty("band2ImagePath","");
                panSharpenObject->setProperty("band3ImagePath","");
                band1Path.clear();
                band2Path.clear();
                band3Path.clear();

            }   
        }

        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
        CORE::IBase* applyNode = terrain->getInterface<CORE::IBase>();

        if(!_panVisitor.valid())
        {
            CORE::RefPtr<CORE::IVisitorFactory> vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
            _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::PanSharpenVisitorType);
            _panVisitor = _visitor->getInterface<GISCOMPUTE::IPanSharpenVisitor>();
        }

        _panVisitor->setPanImage(panPath);
        std::string stdOutputName(outputName.toStdString());
        _panVisitor->setOutputName(stdOutputName);
        _panVisitor->setMSImage(msPath);
        _panVisitor->setBand1Image(band1Path);
        _panVisitor->setBand2Image(band2Path);
        _panVisitor->setBand3Image(band3Path);

        _subscribe(_panVisitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

        applyNode->accept(*(_panVisitor->getInterface<CORE::IBaseVisitor>()));
    }


    //function called when cancel button is clicked
    void
        PanSharpenGUI::_handleStopButtonClicked()
    {        

        _handleGUIStatus();
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }
    }


    void PanSharpenGUI::setActive(bool value)
    {
        try
        {
            _reset();
            if(value)
            {
                QObject* panSharpenObj = _findChild(PanSharpenGUIObjectName);
                if(panSharpenObj != NULL)
                {
                    QObject::connect(panSharpenObj, SIGNAL(browsePanImage()),
                        this, SLOT(_handleBrowsePanImage()), Qt::UniqueConnection);

                    QObject::connect(panSharpenObj, SIGNAL(browseMSImage()),
                        this, SLOT(_handleBrowseMSImage()), Qt::UniqueConnection);

                    QObject::connect(panSharpenObj, SIGNAL(browseBand1Image()),
                        this, SLOT(_handleBrowseBand1Image()), Qt::UniqueConnection);

                    QObject::connect(panSharpenObj, SIGNAL(browseBand2Image()),
                        this, SLOT(_handleBrowseBand2Image()), Qt::UniqueConnection);

                    QObject::connect(panSharpenObj, SIGNAL(browseBand3Image()),
                        this, SLOT(_handleBrowseBand3Image()), Qt::UniqueConnection);

                    QObject::connect(panSharpenObj, SIGNAL(browseOutputImage()), 
                        this, SLOT(_handleBrowseOutputImage()), Qt::UniqueConnection);

                    QObject::connect(panSharpenObj, SIGNAL(showMSImage()),
                        this, SLOT(_showMSImage()), Qt::UniqueConnection);

                    QObject::connect(panSharpenObj, SIGNAL(viewImage(QString)),
                        this, SLOT(_viewImage(QString)),Qt::QueuedConnection);

                    QObject::connect(panSharpenObj, SIGNAL(stopButtonClicked()), 
                        this, SLOT(_handleStopButtonClicked()), Qt::UniqueConnection);
                    QObject::connect(panSharpenObj, SIGNAL(fuseImages(QString, QString, QString, QString, QString)), 
                        this, SLOT(_fuseImages(QString, QString, QString, QString, QString)), Qt::UniqueConnection);
                }
            }
            else
            {
                _handleStopButtonClicked();
            }

            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }



    // updates GUI according to the filter status
    void
        PanSharpenGUI::_handleGUIStatus()
    {
        QObject* panSharpenObject = _findChild(PanSharpenGUIObjectName);
        if(panSharpenObject)
        {
            panSharpenObject->setProperty("calculating",false);
            panSharpenObject->setProperty("startButtonEnabled",true);
            panSharpenObject->setProperty("stopButtonEnabled",false);            
        }

    }  

    void
        PanSharpenGUI::_reset()
    {
        QObject* panSharpenObject = _findChild(PanSharpenGUIObjectName);
        if(panSharpenObject)
        {
            panSharpenObject->setProperty("calculating",false);
            panSharpenObject->setProperty("startButtonEnabled",true);
            panSharpenObject->setProperty("stopButtonEnabled",false);
            panSharpenObject->setProperty("panButtonEnabled",false);
            panSharpenObject->setProperty("msButtonEnabled",false);
            panSharpenObject->setProperty("band1ImageButtonEnabled",false);
            panSharpenObject->setProperty("band2ImageButtonEnabled",false);
            panSharpenObject->setProperty("band3ImageButtonEnabled",false);
            panSharpenObject->setProperty("msImageEnabled",false);
            panSharpenObject->setProperty("outputButtonEnabled",false);
            panSharpenObject->setProperty("panImagePath","");
            panSharpenObject->setProperty("msImagePath","");
            panSharpenObject->setProperty("band1ImagePath","");
            panSharpenObject->setProperty("band2ImagePath","");
            panSharpenObject->setProperty("band3ImagePath","");
            panSharpenObject->setProperty("outputNamePath","");

        }

    } 


}// namespace SMCQt
