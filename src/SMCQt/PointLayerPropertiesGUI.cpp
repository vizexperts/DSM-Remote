/****************************************************************************
*
* File             : PointLayerPropertiesGUI.h
* Description      : PointLayerPropertiesGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/PointLayerPropertiesGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/IconObject.h>

#include <App/IApplication.h>

#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IPoint.h>
#include <Core/IProperty.h>
#include <Core/GroupAttribute.h>
#include <Core/NamedAttribute.h>

#include <App/AccessElementUtils.h>

#include <QtCore/QStringList>

#include <sstream>
#include <QFileDialog>
#include <QMessageBox>
#include <QStringList>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <Util/FileUtils.h>
#include <VizUI/ISelectionUIHandler.h>

#include <GIS/IMarkerStyle.h>
#include <GIS/IHistogramStyle.h>
#include <GIS/IFieldDefinition.h>
#include <GIS/GISPlugin.h>
#include <GIS/ILayerMarkerConfig.h>
#include <GIS/IFeatureLayer.h>
#include <GIS/IDrawPropertyHolder.h>
#include <Core/CoreRegistry.h>
#include <SMCQt/VizComboBoxElement.h>


namespace SMCQt
{

    DEFINE_META_BASE(SMCQt, PointLayerPropertiesGUI);
    DEFINE_IREFERENCED(PointLayerPropertiesGUI, DeclarativeFileGUI);

    PointLayerPropertiesGUI::PointLayerPropertiesGUI()
    {
        _parentObjectName = "";
    }

    PointLayerPropertiesGUI::~PointLayerPropertiesGUI()
    {
        _showIconMenu(false);
    }

    void PointLayerPropertiesGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopup(QString)), Qt::UniqueConnection);
        }
    }

    void PointLayerPropertiesGUI::connectContextualMenu()
    {

        _parentObjectName = "pointContextual";

        _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_ATTRIBUTES);
        QObject* pointContextual = _findChild(_parentObjectName);
        if(pointContextual)
        {
            QObject* PointLayerProperties = _findChild("PointLayerProperties"); 
            if(PointLayerProperties)
            {
                QObject::connect(PointLayerProperties, SIGNAL(onOkButtonClicked()), this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);
                QObject::connect(PointLayerProperties, SIGNAL(onCancelButtonClicked()), this, SLOT(_handleCancelButtonClicked()), Qt::UniqueConnection);
                QObject::connect(PointLayerProperties, SIGNAL(onApplyButtonClicked()), this, SLOT(_handleApplyButtonClicked()), Qt::UniqueConnection);
                QObject::connect(PointLayerProperties, SIGNAL(onBrowseButtonClicked()), this, SLOT(_handleBrowseButtonClicked()), Qt::UniqueConnection);
            }

            QObject::connect(pointContextual, SIGNAL(showIconMenu(bool)), this, SLOT(_showIconMenu(bool)), Qt::UniqueConnection);
        }
    }

    void PointLayerPropertiesGUI::disconnectContextualMenu()
    {
        _parentObjectName = "";
    }

    void PointLayerPropertiesGUI::connectPopup(QString type)
    {
        if(type == "pointLayerStylePopup")
        {
            _parentObjectName = type.toStdString();
            QObject* parentObject = _findChild(_parentObjectName);

            //! Get the current selection
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& slMap = selectionUIHandler->getCurrentSelection();
            
            if(slMap.empty())
                return;

            _currentSelection = slMap.begin()->second;

            //! Get selected icon
            CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
            if(markerProp)
            {
                GIS::IMarkerStyle* markerConfig = markerProp->getInterface<GIS::IMarkerStyle>();
                if(markerConfig)
                {
                    const ELEMENTS::IIcon* icon =  markerConfig->getIcon();
                    _originalIcon = "circle";
                    if(icon)
                    {
                        _originalIcon = icon->getName();
                        std::string iconPath = icon->getIconFilename();
                        iconPath = "file://" + osgDB::getRealPath(iconPath);
                        parentObject->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(iconPath)));
                    }
                }

                GIS::IMarkerStyle* markerConfig1 = markerProp->getInterface<GIS::IMarkerStyle>();
                 if(markerConfig1)
                {
                    osg::Vec4f Color = markerConfig1->getFontColor();
                    _color = QColor(int(Color.r()*255),int(Color.g()*255),int(Color.b()*255),int(Color.a()*255));
                    parentObject->setProperty("featureColor", QVariant::fromValue(_color));
                }

                GIS::IMarkerStyle* markerConfig2 = markerProp->getInterface<GIS::IMarkerStyle>();
                if(markerConfig2)
                {
                    float i = 0;
                    i = markerConfig2->getFontSize();
                    _size = (int)i;
                    QMetaObject::invokeMethod(parentObject, "setFontSize",Q_ARG(QVariant, QVariant::fromValue(_size)));
                    //parentObject->setProperty("fontsize", QVariant::fromValue(_size));
                }

                GIS::IMarkerStyle* markerConfig3 = markerProp->getInterface<GIS::IMarkerStyle>();
                if(markerConfig3)
                {
                    std::string i = markerConfig3->getFont();
                    _fontfile = i;
                    //QMetaObject::invokeMethod(PointLayerStylePopup, "setFontSize",Q_ARG(QVariant, QVariant::fromValue(_size)));
                    parentObject->setProperty("fontstyle", QVariant::fromValue(QString::fromStdString(_fontfile)));
                }

                GIS::ILayerMarkerConfig* layerMarkerConfig = markerProp->getInterface<GIS::ILayerMarkerConfig>();
                if(layerMarkerConfig)
                {
                    if(layerMarkerConfig->isEnabledTextFieldDefinition())
                    {
                        _originalColumnName = layerMarkerConfig->getTextFieldDefinition();
                    }

                    if(_originalColumnName.empty())
                    {
                        _originalColumnName = "<None>";
                    }

                    parentObject->setProperty("selectedColumnName", QVariant::fromValue(QString::fromStdString(_originalColumnName)));
                }

                GIS::IHistogramStyle* histogramStyle = markerProp->getInterface<GIS::IHistogramStyle>();
                if(histogramStyle)
                {
                    _originalHistWidth = histogramStyle->getHistWidth();
                    QMetaObject::invokeMethod(parentObject, "setHistogramWidth", Q_ARG(QVariant, QVariant::fromValue(_originalHistWidth)));
                }
            }

            //! fill annotation columns
            CORE::RefPtr<GIS::IFeatureLayer> featureTable  = _currentSelection->getInterface<GIS::IFeatureLayer>();

            QList<QObject*> comboBoxList;

            int count = 0;
            comboBoxList.append(new VizComboBoxElement("<None>", UTIL::ToString(count++).c_str()));
            if(featureTable.valid())
            {
                const GIS::ITable::IFieldDefinitionList& fieldDefinitionList = featureTable->getFieldDefinitionList();
                GIS::ITable::IFieldDefinitionList::const_iterator citer = fieldDefinitionList.begin();
                while(citer != fieldDefinitionList.end())
                {
                    std::string columnName = (*citer)->getName();

                    comboBoxList.append(new VizComboBoxElement(columnName.c_str(), UTIL::ToString(count++).c_str()));
                    citer++;
                }

                _setContextProperty("layerColumnNameListModel", QVariant::fromValue(comboBoxList));
            }

            if(parentObject)
            {
                QObject* PointLayerProperties = _findChild("PointLayerProperties");
                if(PointLayerProperties)
                {
                    QObject::connect(PointLayerProperties, SIGNAL(onOkButtonClicked()), this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);
                    QObject::connect(PointLayerProperties, SIGNAL(onCancelButtonClicked()), this, SLOT(_handleCancelButtonClicked()), Qt::UniqueConnection);
                    QObject::connect(PointLayerProperties, SIGNAL(onApplyButtonClicked()), this, SLOT(_handleApplyButtonClicked()), Qt::UniqueConnection);
                    QObject::connect(PointLayerProperties, SIGNAL(onBrowseButtonClicked()), this, SLOT(_handleBrowseButtonClicked()), Qt::UniqueConnection);
                    //QObject::connect(PointLayerProperties, SIGNAL(closed()), this, SLOT(popupClosed()), Qt::UniqueConnection);
                }

                QObject::connect(parentObject, SIGNAL(showIconMenu(bool)), this, SLOT(_showIconMenu(bool)), Qt::UniqueConnection);
            }

            //! Try to get the draw property type
            CORE::RefPtr<const CORE::IPropertyType> drawPropertyType = _currentSelection->getInterface<GIS::IDrawPropertyHolder>()->getDrawPropertyType();
            if(drawPropertyType.valid())
            {
                _originalDrawTechnique = QString::fromStdString(drawPropertyType->getName());
                const DB::ReaderWriter::Options* options = _currentSelection->getInterface<GIS::IDrawPropertyHolder>()->getDrawPropertyOption();
                std::string modelName;
                if(options)
                {
                    options->getMapValue("Model", modelName);
                }
                _originaModelType = QString::fromStdString(modelName);
            }

            _resetStyleOnClose = false;

            QMetaObject::invokeMethod(parentObject, "setDrawPropertyType"
                , Q_ARG(QVariant, QVariant::fromValue(_originalDrawTechnique))
                , Q_ARG(QVariant, QVariant::fromValue(_originaModelType)));

            QObject::connect(parentObject, SIGNAL(annotationChanged(QString)), this, SLOT(_annotationChanged(QString)), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(changeDrawTechniqueType(QString, QString)), this, SLOT(changeDrawTechniqueType(QString, QString)), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(changeHistWidth(double)), this, SLOT(_changeHistWidth(double)), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(okButtonPressed()), this, SLOT(_handlePopupOkButtonPressed()), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(closed()), this, SLOT(popupClosed()), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(fontSizeChanged(QString)), this, SLOT(_fontSizeChanged(QString)), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(fontColorChanged(QColor)), this, SLOT(_fontColorChanged(QColor)), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(fontStyleChanged(int)), this, SLOT(_fontStyleChanged(int)), Qt::UniqueConnection);
            QObject::connect(parentObject, SIGNAL(changeColorTransparency(int)), this, SLOT(_changeColorTransparency(int)), Qt::UniqueConnection);
        
            if(_currentSelection.valid())
            {
                CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
              
                if(markerProp)
                {
                    GIS::IHistogramStyle* histogramStyle = markerProp->getInterface<GIS::IHistogramStyle>();
                    if(histogramStyle)
                    {
                        osg::ref_ptr<osg::Vec4dArray> histColorArray = histogramStyle->getHistColorArray();
                        if(histColorArray->size())
                        parentObject->setProperty("colorTransparency", QVariant::fromValue((*histColorArray)[0].a()));
                    }
                }
            }
        }
        else if(type == "histogramLegendPopup")
        {
            //! Get the current selection
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& slMap = selectionUIHandler->getCurrentSelection();

            if (slMap.empty())
                return;

            _currentSelection = slMap.begin()->second;

            QObject* histogramLegendPopup = _findChild("histogramLegendPopup");

            if(histogramLegendPopup)
            {
                QObject::connect(histogramLegendPopup, SIGNAL(changeHistColor(QColor, int)), this, SLOT(_changeHistColor(QColor, int)), Qt::UniqueConnection);

                if(_currentSelection.valid())
                {
                    QString layerName = QString::fromStdString(_currentSelection->getInterface<CORE::IBase>()->getName());
                    histogramLegendPopup->setProperty("title", QVariant::fromValue(layerName + " Legend"));
                }
            }

            showLayerLegend();
        }
    }

    //get the font size
    void PointLayerPropertiesGUI::_fontSizeChanged(QString x)
    {
        float y = x.toFloat();
        
        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(!markerProp)
            return;
       
        GIS::IMarkerStyle* markerConfig = markerProp->getInterface<GIS::IMarkerStyle>();
        if(markerConfig)
        {
            markerConfig->setFontSize(y);
        }
   
    }

    //get the fontColor
    void PointLayerPropertiesGUI::_fontColorChanged(QColor color)
    {
        _color = color;
        
        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(!markerProp)
            return;
       
        GIS::IMarkerStyle* markerConfig = markerProp->getInterface<GIS::IMarkerStyle>();
        if(markerConfig)
        {
             osg::Vec4 penColor((float)_color.red() / 255.0f, (float)_color.green() / 255.0f,
                (float)_color.blue() / 255.0f, (float)_color.alpha() / 255.0f);
            markerConfig->setFontColor(penColor);
        }
   
    }

    //get the fontStyle
    void PointLayerPropertiesGUI::_fontStyleChanged(int x)
    {
        std::string s;

        if ( x == 1 )
        {
            s = "Arial.ttf";
        }

        if ( x == 2 )
        {
            s = "Impact.ttf";
        }

        if ( x == 3 )
        {
            s = "Verdana.ttf";
        }

        if ( x == 4 )
        {
            s = "Calibri.ttf";
        }

        if ( x == 5 )
        {
            s = "Times.ttf";
        }

        if ( x == 6 )
        {
            s = "Vani.ttf";
        }

        if ( x == 7 )
        {
            s = "Cour.ttf";
        }


        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(!markerProp)
            return;
       
        GIS::IMarkerStyle* markerConfig = markerProp->getInterface<GIS::IMarkerStyle>();
        if(markerConfig)
        {
            markerConfig->setFont(s);
        }
    }
  
    void PointLayerPropertiesGUI::_handlePopupOkButtonPressed()
    {
        _resetStyleOnClose = false;
    }

    void PointLayerPropertiesGUI::_annotationChanged(QString value)
    {
        if(!_currentSelection.valid())
            return;

        _resetStyleOnClose = true;

        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(markerProp)
        {
            GIS::ILayerMarkerConfig* layerMarkerConfig = markerProp->getInterface<GIS::ILayerMarkerConfig>();
            if(layerMarkerConfig)
            {
                if(value == "<None>")
                {
                    layerMarkerConfig->setTextFieldDefinition("");
                }
                else
                {
                    layerMarkerConfig->enableTextFieldDefinition(true);
                    layerMarkerConfig->setTextFieldDefinition(value.toStdString());
                }
            }
        }
    }
    
    void PointLayerPropertiesGUI::_changeColorTransparency(int alphaValue){
    
          if(!_currentSelection.valid())
            return;

          CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
          
          if(markerProp)
            {
                GIS::IHistogramStyle* histogramStyle = markerProp->getInterface<GIS::IHistogramStyle>();
                if(histogramStyle)
                {
                    osg::ref_ptr<osg::Vec4dArray> histColorArray = histogramStyle->getHistColorArray();
                    
                    for(int index =0; index< histColorArray->size(); index++)
                    {
                        (*histColorArray)[index].a() = (float)alphaValue/100;
                    }

                    histogramStyle->setHistColorArray(histColorArray);
                }
            }

    }

    void PointLayerPropertiesGUI::_changeHistColor(QColor color, int index)
    {
        if(!_currentSelection.valid())
            return;

        _resetStyleOnClose = true;

        LOG_DEBUG_VAR("Change Histogram Color at %d to (%d,%d,%d, %d)", index, color.red(), color.green(), color.blue(), color.alpha());

        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(markerProp)
        {
            GIS::IHistogramStyle* histogramStyle = markerProp->getInterface<GIS::IHistogramStyle>();
            if(histogramStyle)
            {
                osg::ref_ptr<osg::Vec4dArray> histColorArray = histogramStyle->getHistColorArray();

                LOG_DEBUG_VAR("Color array size : %d", histColorArray->size());

                osg::Vec4 histColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

                if(index >= histColorArray->size())
                {
                    histColorArray->push_back(histColor);
                }
                else
                {
                    (*histColorArray)[index] = histColor;
                }

                QList<QObject*> qHistColorArray;

                histogramStyle->setHistColorArray(histColorArray);
            }
        }
    }

    void PointLayerPropertiesGUI::showLayerLegend()
    {
        if(!_currentSelection.valid())
            return;

        //! Check if histogram property is there
        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(markerProp)
        {
            GIS::IHistogramStyle* histogramStyle = markerProp->getInterface<GIS::IHistogramStyle>();
            if(histogramStyle)
            {
                //! Get draw property options
                const DB::ReaderWriter::Options* options = _currentSelection->getInterface<GIS::IDrawPropertyHolder>()->getDrawPropertyOption();
                std::string histogramNames;
                std::string histogramValues; 
                options->getMapValue(GIS::IHistogramStyle::HISTOGRAM_NAMES, histogramNames);
                options->getMapValue(GIS::IHistogramStyle::HISTOGRAM_VALUE, histogramValues);
                QString qhistogramNames = QString::fromStdString(histogramNames);
                QStringList qhistogramNameList = qhistogramNames.split(':', QString::SkipEmptyParts);

                osg::ref_ptr< osg::Vec4dArray > histColorArray = histogramStyle->getHistColorArray();

                while(histColorArray->size() < qhistogramNameList.size())
                {
                    histColorArray->push_back(osg::Vec4d(0, 0, 1, 0.8));
                }
                

                _originalHistColorArray.clear();
                
                std::stringstream histogramValueStream(histogramValues); 
                for(int i = 0; i < histColorArray->size(); i++)
                {
                    QColor originalHistColor = 
                        QColor(int(histColorArray->at(i).r()*255),int(histColorArray->at(i).g()*255),int(histColorArray->at(i).b()*255),int(histColorArray->at(i).a()*255));
                        histogramValueStream>>histogramValues; 
                        if(histogramValues.size() && histogramValues[0]!='0'){
                            _originalHistColorArray.push_back( new QLegendColorListObject(qhistogramNameList[i] + QString::fromStdString(" (")+QString::fromStdString(histogramValues)+ QString::fromStdString(")") , originalHistColor, i) );
                        }
                }

                _setContextProperty("legendListModel", QVariant::fromValue(_originalHistColorArray));
            }
        }
    }

    void PointLayerPropertiesGUI::_changeHistWidth(double width)
    {
        if(!_currentSelection.valid())
            return;

        _resetStyleOnClose = true;

        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(markerProp)
        {
            GIS::IHistogramStyle* histogramStyle = markerProp->getInterface<GIS::IHistogramStyle>();
            if(histogramStyle)
            {
                histogramStyle->setHistWidth(width);
            }
        }
    }

    void PointLayerPropertiesGUI::changeDrawTechniqueType(QString drawTechnique, QString modelType)
    {
        if(!_currentSelection.valid())
            return;

        if(drawTechnique.compare("HistogramDrawProperty") == 0)
        {
            //! check weather the layer has 'value' column
            CORE::RefPtr<GIS::IFeatureLayer> featureTable  = _currentSelection->getInterface<GIS::IFeatureLayer>();
            GIS::IFieldDefinition* fieldDef = featureTable->getFieldDefinition("value1");
            if(fieldDef == NULL)
            {
                emit showError("Point Layer Style", "No integer field named 'value1' in shape file.");
                return;
            }
        }

        if(drawTechnique.compare("WeaponRangeProperty") == 0)
        {
            //! check weather the layer has 'WeaponType' , 'MinRange' , 'MaxRange' column
            CORE::RefPtr<GIS::IFeatureLayer> featureTable  = _currentSelection->getInterface<GIS::IFeatureLayer>();
            GIS::IFieldDefinition* fieldDef1 = featureTable->getFieldDefinition("WeaponType");
            GIS::IFieldDefinition* fieldDef2 = featureTable->getFieldDefinition("MinRange");
            GIS::IFieldDefinition* fieldDef3 = featureTable->getFieldDefinition("MaxRange");
            if(fieldDef1 == NULL)
            {
                emit showError("Point Layer Style", "No Text field named 'WeaponType' in shape file.");
                return;
            }

            if(fieldDef2 == NULL)
            {
                emit showError("Point Layer Style", "No integer field named 'MinRange' in shape file.");
                return;
            }

            if(fieldDef3 == NULL)
            {
                emit showError("Point Layer Style", "No integer field named 'MaxRange' in shape file.");
                return;
            }
        }

        //! get propert type
        const CORE::IPropertyType* propertyType = CORE::CoreRegistry::instance()->getPropertyFactory()->getPropertyType(drawTechnique.toStdString());

        _resetStyleOnClose = true;

        _setBusy();

        //! add to draw property holder
        if(propertyType)
        {
            _currentSelection->getInterface<GIS::IDrawPropertyHolder>()->setDrawPropertyOption("Model", modelType.toStdString());
            _currentSelection->getInterface<GIS::IDrawPropertyHolder>()->setDrawPropertyType(propertyType);
        }
    }

    void PointLayerPropertiesGUI::_setBusy()
    {
        if(!_currentSelection.valid())
            return;

        QObject* pointLayerStylePopup = _findChild("pointLayerStylePopup");
        if(pointLayerStylePopup)
        {
            pointLayerStylePopup->setProperty("busyBarVisible", QVariant::fromValue(true));
        }

        //! subscribe to IDrawPropertyHolder::DrawOptionsUpdatedMessageType message
        _subscribe(_currentSelection.get(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
    }

    void PointLayerPropertiesGUI::popupClosed()
    {
        _parentObjectName = "";

        if(!_resetStyleOnClose)
            return;
            
        if(!_currentSelection.valid())
            return;

        CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
        if(!markerProp)
            return;

        //changeDrawTechniqueType(_originalDrawTechnique, _originaModelType);

        CORE::RefPtr<CORE::IComponent> component = CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");
        CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = component->getInterface<ELEMENTS::IIconLoader>();
        ELEMENTS::IIcon* icon = iconLoader->getIconByName(_originalIcon);
        if(icon)
        {
            GIS::IMarkerStyle* markerConfig = markerProp->getInterface<GIS::IMarkerStyle>();
            if(markerConfig)
            {
                markerConfig->setIcon(icon);
            }
        }

        GIS::ILayerMarkerConfig* layerMarkerConfig = markerProp->getInterface<GIS::ILayerMarkerConfig>();
        if(layerMarkerConfig)
        {
            if(_originalColumnName == "<None>")
            {
                layerMarkerConfig->setTextFieldDefinition("");
            }
            else
            {
                layerMarkerConfig->enableTextFieldDefinition(true);
                layerMarkerConfig->setTextFieldDefinition(_originalColumnName);
            }
        }

        GIS::IHistogramStyle* histogramStyle = markerProp->getInterface<GIS::IHistogramStyle>();
        if(histogramStyle)
        {
            histogramStyle->setHistWidth(_originalHistWidth);

        }

        _originalIcon = "";
        _originalColumnName = "";
        _originalDrawTechnique = "";
        _originaModelType = "";
    }

    void PointLayerPropertiesGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {
                _pointUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        if (messageType == *GIS::IDrawPropertyHolder::DrawCompletedMessageType)
        {
            //! stop busy bar
            QObject* pointLayerStylePopup = _findChild("pointLayerStylePopup");
            if(pointLayerStylePopup)
            {
                pointLayerStylePopup->setProperty("busyBarVisible", QVariant::fromValue(false));
            }

            //! unsubscribe
            if(_currentSelection.valid())
            {
                _unsubscribe(_currentSelection.get(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
            }
            else
            {
                _unsubscribe(message.getSender(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void PointLayerPropertiesGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();

        std::string groupName = "DefinedIcon";
        _addAttribute(new CORE::GroupAttribute("DefinedIcon", "DefinedIcon",
            CORE::GroupAttribute::SetFuncType(this, &PointLayerPropertiesGUI::addIcon),
            CORE::GroupAttribute::GetFuncType(this, &PointLayerPropertiesGUI::getIcon),
            std::string("DefinedIcon"), groupName));

        std::string appData = UTIL::TemporaryFolder::instance()->getAppFolderPath();
        std::string pointTemp = appData+"/data/userIcons/";
        QDir myDir(QString::fromStdString(pointTemp));
        myDir.setFilter(QDir::Files);
        QStringList fileList = myDir.entryList();

        for(int i=0;i<fileList.size();i++)
        {
            std::string temp = (fileList.at(i)).toLocal8Bit().constData();
            std::string temp1 = pointTemp + temp;
            _uploadNewImage(temp1);
        }
    }

    void PointLayerPropertiesGUI::_showIconMenu(bool value)
    {
        // Focus area handler and activate gui
        try
        {
            if(value)
            {
                _fillSymbolTable();
            }
            else
            {

            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void PointLayerPropertiesGUI::_resetQMLIconProperty()
    {
        QObject* qobject = _findChild(_parentObjectName);

        QObject* PointLayerProperties = _findChild("PointLayerProperties");

        std::string iconPath = PointLayerProperties->property("selectedIcon").toString().toStdString();

        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
            component->getInterface<ELEMENTS::IIconLoader>();

        iconPath = iconLoader->getIconByName(iconPath)->getIconFilename();
        iconPath = "file:/" + osgDB::getRealPath(iconPath);
        if(qobject)
            qobject->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(iconPath)));

        //if(_pointUIHandler)
        //{
        //    CORE::IPoint* point = _pointUIHandler->getPoint();
        //    if(point)
        //    {
        //        CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = point->getInterface<ELEMENTS::IIconHolder>();
        //        if(iconHolder.valid())
        //        {
        //            CORE::RefPtr<ELEMENTS::IIcon> icon = iconHolder->getIcon();
        //            if(icon.valid())
        //            {
        //                std::string iconPath = icon->getIconFilename();
        //                iconPath =  "file:/" + osgDB::getRealPath(iconPath);
        //                pointContextual->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(iconPath)));
        //            }
        //        }
        //    }
        //}
    }

    void PointLayerPropertiesGUI::_handleOkButtonClicked()
    {   
        QObject* PointLayerProperties = _findChild("PointLayerProperties");
        _handleApplyButtonClicked();
        PointLayerProperties->setProperty("buttonRowVisible" ,QVariant::fromValue(false));
        _setContextProperty("symbolModel", QVariant::fromValue(NULL));

        QObject* parentObject = _findChild(_parentObjectName);
        if(parentObject)
        {
            parentObject->setProperty("styleColumnVisible", QVariant::fromValue(true));
        }
    }

    void PointLayerPropertiesGUI::_handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        std::string appData = UTIL::TemporaryFolder::instance()->getAppFolderPath();
        QString iconFile = QFileDialog::getOpenFileName(parent, tr("Choose Icon"), QString::fromStdString(_lastBrowsedpath), tr("Image Files (*.png *.jpg *.bmp)"));
        if(!iconFile.isEmpty())
        {
            QImage image(iconFile);
            QSize size = image.size();
            if(size.height()<=128 && size.width()<=128)
            {
                std::string temp = iconFile.toLocal8Bit().constData();
                std::string pointTemp = appData+"/data/userIcons/";
                if(!QDir(QString::fromStdString(pointTemp)).exists())
                {
                    QDir().mkdir(QString::fromStdString(pointTemp));
                }
                std::string temp1 = pointTemp+osgDB::getSimpleFileName(temp);

                _lastBrowsedpath = osgDB::getFilePath(temp);

                QFile::copy(QString::fromStdString(temp),QString::fromStdString(temp1));
                _uploadNewImage(temp1);
                _fillSymbolTable();
            }
            else
            {
                showError("Icon Size Error", "Please Select a File upto 128 x 128", false);
            }
        }

    }

    void PointLayerPropertiesGUI::_uploadNewImage(const std::string& icon)
    {
        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = component->getInterface<ELEMENTS::IIconLoader>();

            iconLoader->loadIcon(osgDB::getStrippedName(icon), icon);
        }
    }

    void PointLayerPropertiesGUI::_handleCancelButtonClicked()
    {
        QObject* qobject = _findChild(_parentObjectName);

        if(qobject)
        {
            qobject->setProperty("styleColumnVisible" ,QVariant::fromValue(true));
        }

        QObject* PointLayerProperties = _findChild("PointLayerProperties");
        PointLayerProperties->setProperty("buttonRowVisible" ,QVariant::fromValue(false));
        _setContextProperty("symbolModel", QVariant::fromValue(NULL));
    }


    void PointLayerPropertiesGUI::_handleApplyButtonClicked()
    {
        QObject* PointLayerProperties = _findChild("PointLayerProperties");

        if(_pointUIHandler && (_parentObjectName.compare("pointContextual") == 0) )
        {
            CORE::IPoint* point = _pointUIHandler->getPoint();
            if(point)
            {
                CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = point->getInterface<ELEMENTS::IIconHolder>();
                if(iconHolder.valid())
                {

                    CORE::RefPtr<CORE::IComponent> component = 
                        CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

                    CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
                        component->getInterface<ELEMENTS::IIconLoader>();

                    std::string iconPath = PointLayerProperties->property("selectedIcon").toString().toStdString();
                    ELEMENTS::IIcon* icon = iconLoader->getIconByName(iconPath);
                    if(icon)
                    {
                        iconHolder->setIcon(icon);
                    }
                }
            }
        }

        if((_parentObjectName.compare("pointLayerStylePopup") == 0))
        {
            CORE::RefPtr<CORE::IComponent> component = CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = component->getInterface<ELEMENTS::IIconLoader>();

            _resetStyleOnClose = true;

            std::string iconPath = PointLayerProperties->property("selectedIcon").toString().toStdString();
            ELEMENTS::IIcon* icon = iconLoader->getIconByName(iconPath);
            if(icon)
            {
                CORE::IProperty* markerProp = _currentSelection->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerMarkerConfigPropertyType.get());
                if(markerProp)
                {
                    GIS::IMarkerStyle* markerConfig = markerProp->getInterface<GIS::IMarkerStyle>();
                    if(markerConfig)
                    {
                        markerConfig->setIcon(icon);
                    }
                }
            }
        }

        _resetQMLIconProperty();
    }

    void PointLayerPropertiesGUI::_fillSymbolTable()
    {
        //QStringList list;
        QList<QObject*> list;
        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");
        CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
            component->getInterface<ELEMENTS::IIconLoader>();
        ELEMENTS::IIconLoader::IconMap::const_iterator iter = iconLoader->getIconMap().begin();

        while(iter != iconLoader->getIconMap().end())
        {
            std::string iconFile = osgDB::getRealPath(iter->second->getIconFilename());
            list.append(new IconObject(QString::fromStdString(iter->first), QString::fromStdString(iconFile)));
            ++iter;
        }

        _setContextProperty("symbolModel", QVariant::fromValue(list));

        QObject* PointLayerProperties = _findChild("PointLayerProperties");
        PointLayerProperties->setProperty("buttonRowVisible" ,QVariant::fromValue(true));

    }

    void PointLayerPropertiesGUI::addIcon(const CORE::NamedGroupAttribute &credential)
    {
        const CORE::NamedStringAttribute* attrIcon = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute("IconPath"));
        std::string iconPath = attrIcon->getValue();
        if(osgDB::fileExists(iconPath))
            _uploadNewImage(iconPath);
    }

    CORE::RefPtr<CORE::NamedGroupAttribute>
        PointLayerPropertiesGUI::getIcon()
    {
        return NULL;
    }

} // namespace SMCQt

