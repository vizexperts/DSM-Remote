#include <SMCQt/CreateCursorGUI.h>

#include <App/AccessElementUtils.h>
#include <Core/WorldMaintainer.h>
#include <Core/AttributeTypes.h>
#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>


//////////////////////////////////////////////////////////////////////////////
//!
//! \file CreateCursorGUI.cpp
//!
//! \brief Custom cursor implementation 
//!
//////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateCursorGUI);
    DEFINE_IREFERENCED(CreateCursorGUI, CORE::Base);

    CreateCursorGUI::CreateCursorGUI()
        : _cursorPackPath("../../data/gui/IconsSets/DSM_Cursor/")
        , _cursorImpl(CURSORIMPL_DEFAULT)
    {
    }

    CreateCursorGUI::~CreateCursorGUI()
    {
    }

    void CreateCursorGUI::onAddedToGUIManager()
    {
        DeclarativeFileGUI::onAddedToGUIManager();

        //! Connect to cursor change message from QML
        CORE::RefPtr<VizQt::IQtGUIManager> qtmanager = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QObject* item = qobject_cast<QObject*>(qtmanager->getRootComponent());
        if(item)
        {
            QObject::connect(item, SIGNAL(changeCursor(int)), this, SLOT(changeCursor(int)));
        }

        //! Cursor object
        _cursorObject = _findChild("cursorObject");
    }

    //! Create function handlers for Attributes from XML
    void CreateCursorGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();

        _addAttribute(new CORE::StringAttribute("CursoPackPath", "CursoPackPath",
                        CORE::StringAttribute::SetFuncType(this, &CreateCursorGUI::setCursorPackPath),
                        CORE::StringAttribute::GetFuncType(this, &CreateCursorGUI::getCursorPackPath),
                        "CursoPackPath",
                        "CursoPackPath attributes"));

        _addAttribute(new CORE::StringAttribute("CursoPackPathWrtQML", "CursoPackPathWrtQML",
                        CORE::StringAttribute::SetFuncType(this, &CreateCursorGUI::setCursorPackPathWrtQML),
                        CORE::StringAttribute::GetFuncType(this, &CreateCursorGUI::getCursorPackPathWrtQML),
                        "CursoPackPathWrtQML",
                        "CursoPackPathWrtQML attributes"));

        _addAttribute(new CORE::IntAttribute("CursorImplementation", "CursorImplementation",
                        CORE::IntAttribute::SetFuncType(this, &CreateCursorGUI::setCursorImplementation),
                        CORE::IntAttribute::GetFuncType(this, &CreateCursorGUI::getCursorImplementation),
                        "CursorImplementation",
                        "CursorImplementation attributes"));
    }

    void CreateCursorGUI::setCursorPackPath(const std::string & path)
    {
        _cursorPackPath = path;
    }

    std::string CreateCursorGUI::getCursorPackPath()
    {
        return _cursorPackPath;
    }

    void CreateCursorGUI::setCursorPackPathWrtQML(const std::string & path)
    {
        _cursorPackPathWrtQML = path;
    }

    std::string CreateCursorGUI::getCursorPackPathWrtQML()
    {
        return _cursorPackPathWrtQML;
    }

    void CreateCursorGUI::setCursorImplementation(int impl)
    {
        _cursorImpl = CursorImplementation(impl);
    }

    int CreateCursorGUI::getCursorImplementation()
    {
        return int(_cursorImpl);
    }

    void CreateCursorGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                _layoutView = qtapp->getLayoutView();

                //! update cursor position on every frame
                if(_layoutView)
                {   
                    QObject::connect(_layoutView, SIGNAL(beforeRendering()), this, 
                        SLOT(updateMouseCursorPosition()), Qt::DirectConnection);
                }

                if(getCursorImplementation() == CURSORIMPL_QML)
                {
                    //! Set QML Cursor visible
                    if(_cursorObject)
                    {
                        _cursorObject->setProperty("visible", QVariant::fromValue(true));
                    }
                }

                //! set cursor for VizQuickView
                changeCursor(int(Qt::ArrowCursor));
            }
            catch(UTIL::Exception& e)
            {
                e.LogException();
                //setComplete(false);
            }
        }
        else
        {
            VizQt::GUI::update(messageType, message);
        }
    }

    void CreateCursorGUI::updateMouseCursorPosition()
    {
        if(!_cursorObject)
        {
            return;
        }

        if(!_layoutView)
        {
            return;
        }

        //! Get cursor position in window coordinates
        QPoint position = _layoutView->mapFromGlobal(QCursor::pos());

        _cursorObject->setProperty("x", QVariant::fromValue(position.x()));
        _cursorObject->setProperty("y", QVariant::fromValue(position.y()));
    }

    void CreateCursorGUI::changeCursor(int shape)
    {
        if(_layoutView)
        {
            //! Set default windows cursor
            if(getCursorImplementation() == CURSORIMPL_DEFAULT)
            {
                _layoutView->setCursor(QCursor(Qt::CursorShape(shape)));
            }
            else
            {
                if(getCursorImplementation() == CURSORIMPL_QML)
                {
                    //! Hide windows cursor
                    if(_layoutView->cursor().shape() != Qt::BlankCursor)
                    {
                        _layoutView->setCursor(QCursor(Qt::BlankCursor));
                    }
                }

                //! Get cursor file path
                std::string path = getCursorPath(Qt::CursorShape(shape));

                if(path == "")
                {
                    //! Set Arrow cursor if path not found
                    path = getCursorPath(Qt::ArrowCursor);
                    _setCursor(path, 0, 0);
                }
                else
                {
                    //! Set hotspot at top-left for ArrowCursor
                    if(Qt::CursorShape(shape) == Qt::ArrowCursor)
                    {
                        _setCursor(path, 0, 0);
                    }
                    else
                    {
                        _setCursor(path);
                    }
                }
            }
        }        
    }

    void CreateCursorGUI::_setCursor(const std::string& path, int hotspotX, int hotspotY)
    {
        if(getCursorImplementation() == CURSORIMPL_QT)
        {
            _layoutView->setCursor(QCursor(QPixmap(QString::fromStdString(path)), hotspotX, hotspotY));
        }
        else if(getCursorImplementation() == CURSORIMPL_QML)
        {
            if(_cursorObject)
            {
                _cursorObject->setProperty("cursorSource", QVariant::fromValue(QString::fromStdString(path)));

                //! Shift Hotspot to Image center if hotspot not specified
                if(hotspotX == -1)
                    hotspotX = -16;
                
                if(hotspotY == -1)
                    hotspotY = -16;

                _cursorObject->setProperty("imageHorizontalOffset", hotspotX);
                _cursorObject->setProperty("imageVerticalOffset", hotspotY);
            }
        }
    }

    std::string CreateCursorGUI::getCursorPath(Qt::CursorShape cursorShape)
    {
        std::string path = "";
        std::string folder = "";

        if(getCursorImplementation() == CURSORIMPL_QML)
        {
            folder = getCursorPackPathWrtQML();
        }
        else
        {
            folder = getCursorPackPath();
        }

        switch(cursorShape)
        {
        case Qt::ArrowCursor : 
            path = folder + "Arrow.png";
            break;
        case Qt::UpArrowCursor : 
            path = folder + "UpArrow.png";
            break;
        case Qt::CrossCursor : 
            path = folder + "Cross.png";
            break;
        case Qt::WaitCursor : 
            path = folder + "Wait.png";
            break;
        case Qt::IBeamCursor : 
            path = folder + "IBeam.png";
            break;
        case Qt::SizeVerCursor : 
            path = folder + "SizeVer.png";
            break;
        case Qt::SizeHorCursor : 
            path = folder + "SizeHor.png";
            break;
        case Qt::SizeBDiagCursor : 
            path = folder + "SizeBDiag.png";
            break;
        case Qt::SizeFDiagCursor : 
            path = folder + "SizeFDiag.png";
            break;
        case Qt::SizeAllCursor : 
            path = folder + "SizeAll.png";
            break;
        case Qt::SplitVCursor : 
            path = folder + "SplitV.png";
            break;
        case Qt::PointingHandCursor : 
            path = folder + "PointingHand.png";
            break;
        case Qt::ForbiddenCursor : 
            path = folder + "Forbidden.png";
            break;
        case Qt::WhatsThisCursor : 
            path = folder + "WhatsThis.png";
            break;
        case Qt::BusyCursor:
            path = folder + "Busy";
            break;
        case Qt::OpenHandCursor:
            path = folder + "OpenHand";
            break;
        case Qt::ClosedHandCursor:
            path = folder + "ClosedHand";
            break;
        case Qt::DragCopyCursor:
            path = folder + "DragCopy";
            break;
        case Qt::DragMoveCursor:
            path = folder + "DragMove";
            break;
        case Qt::DragLinkCursor:
            path = folder + "DragLink";
            break;
        }

        return path;

    }
}   // SMCQt

#endif //WIN32
