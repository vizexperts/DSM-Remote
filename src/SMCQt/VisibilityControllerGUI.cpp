/****************************************************************************
*
* File             : VisibilityControllerGUI.h
* Description      : VisibilityControllerGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/VisibilityControllerGUI.h>
#include <App/AccessElementUtils.h>

#include <SMCUI/IAnimationUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <SMCQt/TimelineMark.h>
#include <SMCQt/VizComboBoxElement.h>
#include <SMCQt/IContextualTabGUI.h>
#include <VizQt/QtUtils.h>

#include <Elements/IMilitarySymbol.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, VisibilityControllerGUI);
    DEFINE_IREFERENCED(VisibilityControllerGUI, SMCQt::DeclarativeFileGUI);

    const std::string VisibilityControllerGUI::EventContextualMenuObjectName = "eventContextual";

    VisibilityControllerGUI::VisibilityControllerGUI()
    {

    }

    VisibilityControllerGUI::~VisibilityControllerGUI()
    {

    }

    void VisibilityControllerGUI::connectContextualMenu()
    {
        if(!_vcUIHandler.valid())
        {
            return;
        }


        _vcUIHandler->setSelectedObjectAsCurrent();

        _populateControlPoints();

        QObject* eventContextual = _findChild(EventContextualMenuObjectName);
        if(eventContextual)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            if(!selectionUIHandler.valid())
            {
                return;
            }

            // Selection Map
            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getOriginalSelectionMap();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter == map.end())
            {
                CORE::RefPtr<SMCQt::IContextualTabGUI> contextualGUI = 
                    APP::AccessElementUtils::getGUIUsingManager<SMCQt::IContextualTabGUI>(getGUIManager());
                if(contextualGUI.valid())
                {
                    contextualGUI->closeContextualTab();
                }
                return;
            }

            //Get the selected object
            CORE::RefPtr<CORE::IObject> object = iter->second->getInterface<CORE::IObject>();
            if(!object.valid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = object->getInterface<ELEMENTS::IMilitarySymbol>();
            if(unit.valid())
            {
                eventContextual->setProperty("movementTabVisible", QVariant::fromValue(true));
                eventContextual->setProperty("propertiesTabVisible", QVariant::fromValue(true));
            }
            else
            {
                eventContextual->setProperty("movementTabVisible", QVariant::fromValue(false));
                eventContextual->setProperty("propertiesTabVisible", QVariant::fromValue(false));
            }

            QObject::connect(eventContextual, SIGNAL(selectControlPoint(QString)), 
                this, SLOT(selectControlPoint(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(addControlPoint(QDateTime, bool)), 
                this, SLOT(addControlPoint(QDateTime, bool)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(editControlPoint(QString, QDateTime, bool)), 
                this, SLOT(editControlPoint(QString, QDateTime, bool)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(tabChanged(QString)), this,
                SLOT(tabSelected(QString)), Qt::UniqueConnection);

            QObject::connect(eventContextual, SIGNAL(deleteControlPoint(QString)), 
                this, SLOT(deleteControlPoint(QString)));

            CORE::RefPtr<SMCUI::IAnimationUIHandler> animationUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());
            if(animationUIHandler.valid())
            {
                const boost::posix_time::ptime& currentTime = 
                    animationUIHandler->getCurrentDateTime();

                QDateTime qCurTime;
                VizQt::TimeUtils::BoostToQtDateTime(currentTime, qCurTime);
                eventContextual->setProperty("currentTime", QVariant::fromValue(qCurTime));
            }
        }            
    }

    void VisibilityControllerGUI::disconnectContextualMenu()
    {
        if(!_vcUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if(selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }

        _vcUIHandler->reset();
    }

    void VisibilityControllerGUI::tabSelected(QString name)
    {
        if(name == "Visibility")
        {
            _populateControlPoints();
        }
        else 
        {
            _setContextProperty("visibilityListModel", QVariant::fromValue(NULL));
            //_setContextProperty("timelineModel", QVariant::fromValue(NULL));
        }
    }

    void VisibilityControllerGUI::selectControlPoint(QString cpID)
    {
        unsigned int id = cpID.toInt();
        if(id == 0) // this means Add checkpoint mode is selected
        {
            return;
        }

        const ELEMENTS::IVisibilityController::ControlPointList& controlPointList = _vcUIHandler->getControlPointList();
        ELEMENTS::IVisibilityController::ControlPointList::const_iterator iter = controlPointList.begin();

        unsigned index = 0;
        for(; index < (id-1) && iter != controlPointList.end(); ++index, ++iter);

        if(index == (id-1) && iter != controlPointList.end())
        {
            const boost::posix_time::ptime& boostCPTime = iter->time;
            bool visibilityState = iter->visibilityState;

            QDateTime qCPTime;
            VizQt::TimeUtils::BoostToQtDateTime(boostCPTime, qCPTime);

            QObject* eventContextual = _findChild(EventContextualMenuObjectName);
            if(eventContextual != NULL)
            {
                eventContextual->setProperty("visibilityEventTime", QVariant::fromValue(qCPTime));
                eventContextual->setProperty("visibilityState", QVariant::fromValue(visibilityState));
            }
        }        
    }

    void VisibilityControllerGUI::addControlPoint(QDateTime cpTime, bool cpVisibilityState)
    {
        if(!_vcUIHandler.valid())
        {
            return;
        }

        boost::posix_time::ptime boostCpTime = VizQt::TimeUtils::QtToBoostDateTime(cpTime);

        SMCUI::IVisibilityControllerUIHandler::PointAddedStatus
            status = _vcUIHandler->addControlPoint(boostCpTime, cpVisibilityState);
        //if this point is already exist
        if (status == SMCUI::IVisibilityControllerUIHandler::DUPLICATE) {
            emit showError("Add Control Point", "Control Point for this date and time is already exist!\nPlease edit existing point.", false);
        }

        _populateControlPoints();
    }

    void VisibilityControllerGUI::editControlPoint(QString cpID, QDateTime cpTime, bool cpVisibilityState)
    {

        if(!_vcUIHandler.valid())
        {
            return;
        }

        int id = cpID.toInt();
        if(id == 0)
        {
            return;
        }

        _vcUIHandler->removeControlPoint(id - 1);

        boost::posix_time::ptime boostCpTime = VizQt::TimeUtils::QtToBoostDateTime(cpTime);

        _vcUIHandler->addControlPoint(boostCpTime, cpVisibilityState);

        _populateControlPoints();
    }

    void VisibilityControllerGUI::deleteControlPoint(QString cpID)
    {
        if(!_vcUIHandler.valid())
        {
            return;
        }

        int id = cpID.toInt();
        if(id == 0)
        {
            return;
        }

        _vcUIHandler->removeControlPoint(id - 1);

        _populateControlPoints();

    }

    void VisibilityControllerGUI::_populateControlPoints()
    {
        // get animation uihandler
        CORE::RefPtr<SMCUI::IAnimationUIHandler> animationUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());
        if(!animationUIHandler.valid())
        {
            emit showError("Timeline", "Animation handler is not valid.");
            return;
        }

        //get start and end time
        const boost::posix_time::ptime& startDateTime = animationUIHandler->getStartDateTime();
        const boost::posix_time::ptime& endDateTime = animationUIHandler->getEndDateTime();

        // get Mission duration
        boost::posix_time::time_duration duration = (endDateTime - startDateTime);
        int totalSeconds = duration.total_seconds();

        int numControlPoints = _vcUIHandler->getNumControlPoints();
        double elapsedSeconds = 0;
        bool visibilityState = true;
        QColor visibleColor("blue");
        QColor notVisibleColor("grey");
        QColor profileColor(visibleColor);
        //bool moving = false;

        QList<QObject*> timelineMarkerList;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("New", "0"));

        if(numControlPoints == 0)
        {
            _setContextProperty("visibilityListModel", QVariant::fromValue(comboBoxList));
            _setContextProperty("timelineModel", QVariant::fromValue(NULL));
            QObject* timelineSlider = _findChild("timelineSlider");
            if(timelineSlider != NULL)
            {
                timelineSlider->setProperty("profileType", QVariant::fromValue(0));
            }
            return;
        }
        else
        {
            const ELEMENTS::IVisibilityController::ControlPointList& controlPointList = _vcUIHandler->getControlPointList();
            ELEMENTS::IVisibilityController::ControlPointList::const_iterator iter = controlPointList.begin();

            for(int index = 0; iter != controlPointList.end(); iter++, index++)
            {
                const boost::posix_time::ptime& cpTime = iter->time;

                double cpTimeInSeconds = (cpTime - startDateTime).total_seconds();

                // previous mark
                double posX = elapsedSeconds/totalSeconds;
                double deltaX = (cpTimeInSeconds - elapsedSeconds)/totalSeconds;
                timelineMarkerList.append(new TimelineVisibilityMark(posX, profileColor, deltaX, visibilityState));

                visibilityState = iter->visibilityState;
                if(visibilityState)
                {
                    profileColor = visibleColor;
                }
                else
                {
                    profileColor = notVisibleColor;
                }

                elapsedSeconds = cpTimeInSeconds;

                QDateTime qCpDateTime;
                VizQt::TimeUtils::BoostToQtDateTime(cpTime, qCpDateTime);

                comboBoxList.append(new VizComboBoxElement(qCpDateTime.toString("hh:mm:ss dd MMM yyyy")
                    , QString::number(index+1)) );

            }

            double posX = elapsedSeconds/totalSeconds;
            double deltaX = (1 - posX);

            timelineMarkerList.append(new TimelineVisibilityMark(posX, profileColor, deltaX, visibilityState));

            _setContextProperty("visibilityListModel", QVariant::fromValue(NULL));
            _setContextProperty("timelineModel", QVariant::fromValue(timelineMarkerList));
            _setContextProperty("visibilityListModel", QVariant::fromValue(comboBoxList));

            QObject* timelineSlider = _findChild("timelineSlider");
            if(timelineSlider != NULL)
            {
                timelineSlider->setProperty("profileType", QVariant::fromValue(1));
            }
        }        
    }

    void VisibilityControllerGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _vcUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IVisibilityControllerUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }

}// namespace SMCQt
