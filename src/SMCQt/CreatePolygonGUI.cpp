/****************************************************************************
*
* File             : CreatePolygonGUI.h
* Description      : CreatePolygonGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/CreatePolygonGUI.h>
#include <SMCQt/PropertyObject.h>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IPolygon.h>
#include <Core/IFeatureLayer.h>
#include <Core/AttributeTypes.h>
#include <Core/IEditable.h>
#include <Core/IPenStyle.h>
#include <Core/IBrushStyle.h>
#include <Core/IMetadataRecord.h>
#include <Core/IDeletable.h>
#include <Core/ILine.h>
#include <Core/ITerrain.h>

#include <Elements/IClamper.h>
#include <Elements/ElementsUtils.h>


#include <VizUI/IDeletionUIHandler.h>

#include <GIS/ILine.h>
#include <GIS/ITable.h>
#include <GIS/IFieldDefinition.h>
#include <GIS/IStringFieldDefinition.h>
#include <GIS/IDoubleFieldDefinition.h>
#include <GIS/FieldDefinitionType.h>
#include <QFileDialog>
#include <Util/StyleFileUtil.h>
#include <Elements/VizPlaceNode.h>
#include <Elements/FeatureObject.h> 
#include <serialization/StyleSheetParser.h>


namespace SMCQt
{

    DEFINE_META_BASE(SMCQt, CreatePolygonGUI);
    DEFINE_IREFERENCED(CreatePolygonGUI, DeclarativeFileGUI);

    const std::string CreatePolygonGUI::PolygonContextualMenuObjectName = "polygonContextual";

    CreatePolygonGUI::CreatePolygonGUI()
        :_cleanup(true)
        ,_clamping(true)
        ,_selectedFeatureLayer(NULL)
        ,_addToDefault(true)
        ,_nextAreaCount(1)
    {
    }

    CreatePolygonGUI::~CreatePolygonGUI()
    {
    }

    void CreatePolygonGUI::_loadAndSubscribeSlots()
    {

        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }
    }

    void CreatePolygonGUI::setPolygonEnable(bool value)
    {
        if(getActive() != value)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->clearCurrentSelection();
            }
        }

        setActive(value);
    }

    void CreatePolygonGUI::connectContextualMenu()
    {

        QObject* polygonContextual = _findChild(PolygonContextualMenuObjectName);
        if(!polygonContextual)
            return;


        QVariant temp = polygonContextual->property("mouseButtonClicked");
        _mouseClickRight = (temp.toInt() == Qt::RightButton);


        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }

        if(_mouseClickRight)
        {

            _populateContextualMenu();

            //! connect to property change handlers
            QObject::connect(polygonContextual, SIGNAL(rename(QString)), this,
                SLOT(rename(QString)), Qt::UniqueConnection);

            QObject::connect(polygonContextual, SIGNAL(changePenColor(QColor)), 
                this, SLOT(penColorChanged(QColor)), Qt::UniqueConnection);

            QObject::connect(polygonContextual, SIGNAL(changeFillColor(QColor)),
                this, SLOT(fillColorChanged(QColor)), Qt::UniqueConnection);

            QObject::connect(polygonContextual, SIGNAL(changeWidth(double)), 
                this, SLOT(lineWidthChanged(double)), Qt::UniqueConnection);

            QObject::connect(polygonContextual, SIGNAL(deleteArea()), this, SLOT(deleteArea()), Qt::UniqueConnection);

            QObject::connect(polygonContextual, SIGNAL(toggleFillColor(bool)),
                this, SLOT(toggleFillColor(bool)), Qt::UniqueConnection);
        }
        if(!_mouseClickRight)
        {
            showAttributes();
        }
    }

    void CreatePolygonGUI::disconnectContextualMenu()
    {
        connectEditVerticesMenu(false);

        if(_areaUIHandler.valid())
        {
            _areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if(selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }
    }

    void CreatePolygonGUI::_populateContextualMenu()
    {
        QObject* polygonContextual = _findChild(PolygonContextualMenuObjectName);
        if(polygonContextual != NULL)
        {
            const CORE::ISelectionComponent::SelectionMap& selectionMap = _selectionUIHandler->getCurrentSelection();

            if(selectionMap.empty())
            {
                return;
            }

            CORE::ISelectable* selectable = selectionMap.begin()->second.get();
            if(selectable)
            {
                _selectedFeature = selectable->getInterface<GIS::IFeature>();
            }

            if(_selectedFeature.valid())
            {
                //Populate data for contextual tab

                //! Name of the line
                QString name = QString::fromStdString(_selectedFeature->getInterface<CORE::IBase>()->getName());

                //! line width
                double lineWidth = 1;

                //! line color
                osg::Vec4 lineColor = osg::Vec4d(0, 0, 0, 1);


                //! fill color
                osg::Vec4 fillColor = osg::Vec4d(0, 0, 1, 0.5);
                bool fillColorEnabled = false;


                QColor qLineColor(int(lineColor.r()*255),int(lineColor.g()*255),int(lineColor.b()*255),int(lineColor.a()*255));
                QColor qFillColor(int(fillColor.r()*255),int(fillColor.g()*255),int(fillColor.b()*255),int(fillColor.a()*255));

                polygonContextual->setProperty("name", QVariant::fromValue(name));
                polygonContextual->setProperty("lineWidth", QVariant::fromValue(lineWidth));
                polygonContextual->setProperty("penColor", QVariant::fromValue(qLineColor));                    
                polygonContextual->setProperty("fillColor", QVariant::fromValue(qFillColor));
                polygonContextual->setProperty("fillColorEnabled", QVariant::fromValue(fillColorEnabled));

                connectEditVerticesMenu(true);
                showAttributes();

            }
            else
            {
                //Get the selected line
                CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
                if(!area.valid())
                {
                    return;
                }

                //Populate data for contextual tab

                //! Name of the line
                QString name = QString::fromStdString(area->getInterface<CORE::IBase>()->getName());

                //! line width
                double lineWidth = area->getInterface<CORE::IPenStyle>()->getPenWidth();

                //! line color
                osg::Vec4 lineColor;
                CORE::RefPtr<CORE::IPenStyle> penStyle = area->getInterface<CORE::IPenStyle>();
                if(penStyle.valid())
                {
                    lineColor = penStyle->getPenColor();
                }


                //! fill color
                osg::Vec4 fillColor;
                bool fillColorEnabled = true;
                CORE::RefPtr<CORE::IBrushStyle> brushStyle = area->getInterface<CORE::IBrushStyle>();
                if(brushStyle.valid())
                {
                    fillColor = brushStyle->getBrushFillColor();
                    if(brushStyle->getBrushState() == CORE::IBrushStyle::OFF)
                    {
                        fillColorEnabled = false;
                    }
                }

                QColor qLineColor(int(lineColor.r()*255),int(lineColor.g()*255),int(lineColor.b()*255),int(lineColor.a()*255));
                QColor qFillColor(int(fillColor.r()*255),int(fillColor.g()*255),int(fillColor.b()*255),int(fillColor.a()*255));

                polygonContextual->setProperty("name", QVariant::fromValue(name));
                polygonContextual->setProperty("lineWidth", QVariant::fromValue(lineWidth));
                polygonContextual->setProperty("penColor", QVariant::fromValue(qLineColor));                    
                polygonContextual->setProperty("fillColor", QVariant::fromValue(qFillColor));
                polygonContextual->setProperty("fillColorEnabled", QVariant::fromValue(fillColorEnabled));

                connectEditVerticesMenu(true);
                showAttributes();
            }
        }

    }

    void CreatePolygonGUI::_performHardReset()
    {
        _nextAreaCount = 1;
        _clamping = true;
        _cleanup = true;

        setActive(false);
        if(_areaUIHandler.valid())
            _areaUIHandler->reset();
    }

    void CreatePolygonGUI::rename(QString newName )
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_areaUIHandler.valid())
            {
                return;
            }

            if ( newName == "" )
            {
                showError("Edit Polygon Name", "Polygon name cannot be blank.", true);

                QObject* polygonContextual = _findChild(PolygonContextualMenuObjectName);
                if( polygonContextual != NULL)
                {
                    CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
                    std::string oldName = area->getInterface<CORE::IBase>()->getName();
                    polygonContextual->setProperty("name", QVariant::fromValue( QString::fromStdString(oldName) ));
                }
                return;
            }

            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                area->getInterface<CORE::IBase>()->setName(newName.toStdString());
            }
        }
    }

    void CreatePolygonGUI::penColorChanged(QColor color)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_areaUIHandler.valid())
            {
                return;
            }

            if(!color.isValid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                CORE::RefPtr<CORE::IPenStyle> penStyle = area->getInterface<CORE::IPenStyle>();
                if(penStyle.valid())
                {
                    osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                        (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);
                    penStyle->setPenColor(penColor);
                }
            }
        }
    }

    void CreatePolygonGUI::fillColorChanged(QColor color)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_areaUIHandler.valid())
            {
                return;
            }

            if(!color.isValid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                CORE::RefPtr<CORE::IBrushStyle> brushStyle = area->getInterface<CORE::IBrushStyle>();
                if(brushStyle.valid())
                {
                    osg::Vec4 fillColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                        (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);
                    brushStyle->setBrushFillColor(fillColor);
                }
            }
        }
    }
    void CreatePolygonGUI::toggleFillColor(bool value)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_areaUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                CORE::RefPtr<CORE::IBrushStyle> brushStyle = area->getInterface<CORE::IBrushStyle>();
                if(brushStyle.valid())
                {
                    if(value)
                    {
                        brushStyle->setBrushState(CORE::IBrushStyle::BrushState(CORE::IBrushStyle::PROTECTED 
                            | CORE::IBrushStyle::ON));
                    }
                    else
                    {
                        brushStyle->setBrushState(CORE::IBrushStyle::OFF);
                    }
                }
            }
        }
    }

    void CreatePolygonGUI::lineWidthChanged(double lineWidth)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_areaUIHandler.valid())
            {
                return;
            }

            if(!(lineWidth > 0))
            {
                return;
            }

            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                CORE::RefPtr<CORE::IPenStyle> penStyle = area->getInterface<CORE::IPenStyle>();
                if(penStyle.valid())
                {
                    penStyle->setPenWidth(lineWidth);
                }
            }
        }
    }

    void CreatePolygonGUI::deleteArea()
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_areaUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(!area.valid())
            {
                return;
            }

            CORE::IDeletable* deletable = area->getInterface<CORE::IDeletable>();
            if(!deletable)
            {
                return;
            }

            CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
            if(deletionUIHandler.valid())
            {
                deletionUIHandler->deleteObject(deletable);
            }
        }

    }

    void CreatePolygonGUI::setEditMode(bool mode)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_areaUIHandler.valid())
            {
                return;
            }

            if ( mode )
            {
                _areaUIHandler->setMode( SMCUI::IAreaUIHandler::AREA_MODE_EDIT );
            }
            else
            {
                QObject* editPolygonVertices = _findChild("editPolygonVertices");
                if( editPolygonVertices != NULL)
                {
                    editPolygonVertices->setProperty("enableDelete", QVariant::fromValue(false));
                    editPolygonVertices->setProperty("editable", QVariant::fromValue( false ));
                }
                _areaUIHandler->setMode( SMCUI::IAreaUIHandler::AREA_MODE_NO_OPERATION );
            }
        }
    }

    void CreatePolygonGUI::connectEditVerticesMenu(bool value)
    {
        if(!_areaUIHandler.valid())
        {
            return;
        }
        CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
        if(!area.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::ILine> line = area->getExteriorRing();
        if(!line.valid())
        {
            return;
        }

        if(value)
        {
            QObject* editPolygonVertices = _findChild("editPolygonVertices");
            if( editPolygonVertices != NULL )
            {
                QString latitude = "";
                QString longitude = "";

                editPolygonVertices->setProperty("latitude", QVariant::fromValue( latitude ));
                editPolygonVertices->setProperty("longitude", QVariant::fromValue( longitude ));
                editPolygonVertices->setProperty("editable", QVariant::fromValue( false ));

                QObject::connect(editPolygonVertices, SIGNAL(deleteVertex()),
                    this, SLOT(deleteVertex()), Qt::UniqueConnection);

                QObject::connect(editPolygonVertices, SIGNAL(handleLatChanged()), 
                    this, SLOT(handleLatChanged()), Qt::UniqueConnection);

                QObject::connect(editPolygonVertices, SIGNAL(handleLongChanged()),
                    this, SLOT(handleLongChanged()), Qt::UniqueConnection);

                QObject::connect(editPolygonVertices, SIGNAL(setEditMode(bool)),
                    this, SLOT(setEditMode(bool)), Qt::UniqueConnection);

                _subscribe(_areaUIHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            }
        }
        else
        {
            _unsubscribe(_areaUIHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }

    }

    void CreatePolygonGUI::deleteVertex()
    {
        std::vector<int> selectedPointList = _areaUIHandler->getSelectedVertexList();


        if ( selectedPointList.size()+3 > _areaUIHandler->getNumPoints() )
        {
            // a line should have atleast two points.
            showError("Delete Points", "A Polygon should have atleast three points. Please unselect some points.", true);
            return;
        }

        // sort , reverse, and update the global selectedPointList 

        std::sort( selectedPointList.begin(), selectedPointList.end() );
        std::reverse( selectedPointList.begin(), selectedPointList.end() );
        _areaUIHandler->setSelectedVertexList( selectedPointList );

        bool flag = false;

        while ( selectedPointList.size() != 0 )
        {
            flag = _areaUIHandler->_deletePointAtIndex(selectedPointList[0]);

            if ( !flag )
                break;

            selectedPointList = _areaUIHandler->getSelectedVertexList();
        }
    }

    void CreatePolygonGUI::handleLatChanged()
    {
        if(!_areaUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
        if(!area.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::ILine> line = area->getExteriorRing();
        if(!line.valid())
        {
            return;
        }

        std::vector<int> selectedPointList = _areaUIHandler->getSelectedVertexList();
        int currentSelectedPoint = selectedPointList[0];

        if( currentSelectedPoint > line->getPointNumber()-1)
        {
            return;
        }

        QObject* editPolygonVertices = _findChild("editPolygonVertices");
        if(editPolygonVertices != NULL)
        {
            QString latitude = editPolygonVertices->property("latitude").toString();
            osg::Vec3 position = line->getValueAt( currentSelectedPoint );

            if ( latitude == "" )
            {
                showError("Edit Latitude", "Latitude field cannont be empty.", true);
                editPolygonVertices->setProperty( "latitude", position.y() );
                return;
            }

            double latitudeD = latitude.toDouble();
            double longitudeD = position.x();
            double altitudeD = ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD);

            position = osg::Vec3d(longitudeD, latitudeD, altitudeD);
            _areaUIHandler->_movePointAtIndex( currentSelectedPoint, position);
        }
    }

    void CreatePolygonGUI::handleLongChanged()
    {
        if(!_areaUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
        if(!area.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::ILine> line = area->getExteriorRing();
        if(!line.valid())
        {
            return;
        }

        std::vector<int> selectedPointList = _areaUIHandler->getSelectedVertexList();
        int currentSelectedPoint = selectedPointList[0];

        if( currentSelectedPoint > line->getPointNumber()-1)
        {
            return;
        }

        QObject* editPolygonVertices = _findChild("editPolygonVertices");
        if(editPolygonVertices != NULL)
        {
            QString longitude = editPolygonVertices->property("longitude").toString();
            osg::Vec3 position = line->getValueAt( currentSelectedPoint );

            if ( longitude == "" )
            {
                showError("Edit Longitude", "Longitude field cannont be empty.", true);
                editPolygonVertices->setProperty( "longitude", position.x() );
                return;
            }

            double latitudeD = position.y();
            double longitudeD = longitude.toDouble();
            double altitudeD = ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD);

            position = osg::Vec3d(longitudeD, latitudeD, altitudeD);
            _areaUIHandler->_movePointAtIndex( currentSelectedPoint, position);
        }
    }


    void CreatePolygonGUI::onVertexSelected( )
    {
        if(!_areaUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
        if(!area.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::ILine> line = area->getExteriorRing();
        if(!line.valid())
        {
            return;
        }

        std::vector<int> selectedPointList = _areaUIHandler->getSelectedVertexList();

        if ( selectedPointList.size() == 0 )
        {
            // disable the delete button
            QObject* editPolygonVertices = _findChild("editPolygonVertices");
            if(editPolygonVertices != NULL)
            {
                QString latitude = "";
                QString longitude = "";

                editPolygonVertices->setProperty("latitude", QVariant::fromValue(latitude));
                editPolygonVertices->setProperty("longitude", QVariant::fromValue(longitude));
                editPolygonVertices->setProperty("editable", QVariant::fromValue(false));
                editPolygonVertices->setProperty("enableDelete", QVariant::fromValue(false));
            }
        }
        else if ( selectedPointList.size() == 1 )
        {
            // update the ui
            int currentSelectedPoint = selectedPointList[0];

            if( currentSelectedPoint > line->getPointNumber()-1)
            {
                return;
            }

            osg::Vec3 position = line->getValueAt( currentSelectedPoint );

            QObject* editPolygonVertices = _findChild("editPolygonVertices");
            if( editPolygonVertices != NULL)
            {
                QString latitude =QString::number( position.y() );
                QString longitude = QString::number( position.x() );

                editPolygonVertices->setProperty("latitude", QVariant::fromValue( latitude ));
                editPolygonVertices->setProperty("longitude", QVariant::fromValue( longitude ));
                editPolygonVertices->setProperty("editable", QVariant::fromValue( true ));
                editPolygonVertices->setProperty("enableDelete", QVariant::fromValue( true ));
            }
        }
        else
        {
            // disable the ui

            QObject* editPolygonVertices = _findChild("editPolygonVertices");
            if( editPolygonVertices != NULL)
            {
                QString latitude = "";
                QString longitude = "";

                editPolygonVertices->setProperty("latitude", QVariant::fromValue( latitude ));
                editPolygonVertices->setProperty("longitude", QVariant::fromValue( longitude ));
                editPolygonVertices->setProperty("editable", QVariant::fromValue( false ));
            }
        }
    }


    void CreatePolygonGUI::connectSmpMenu(QString type)
    {
        if(type == "markingFeaturesMenu")
        {
            QObject* markingFeaturesMenu = _findChild("markingFeaturesMenu");
            if(markingFeaturesMenu != NULL)
            {
                QObject::connect(markingFeaturesMenu, SIGNAL(setPolygonEnable(bool)),
                    this, SLOT(setPolygonEnable(bool)), Qt::UniqueConnection);                    
            }
        }
    }

    void CreatePolygonGUI::_resetForDefaultHandling()
    {
        _addToDefault = true;

        setActive(false);

        _areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
    }

    void CreatePolygonGUI::addPolygonToSelectedLayer(bool value)
    {
        if(value)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                _selectedFeatureLayer = iter->second->getInterface<CORE::IFeatureLayer>();
                _addToDefault = false;
            }

            setActive(true);
        }
        else
        {
            _resetForDefaultHandling();
        }
    }

    void CreatePolygonGUI::selectOption(QString option)
    {
        if(option == "Polygon")
        {
            setActive(true);
        }
        else
        {
            setActive(false);
        }
    }

    void CreatePolygonGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _loadAreaUIHandler();

                //subscribing to world messages
                CORE::IWorldMaintainer *wmain = CORE::WorldMaintainer::instance();
                _subscribe(wmain, *CORE::IWorld::WorldLoadedMessageType);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType==*CORE::IWorld::WorldLoadedMessageType)
        {
            _performHardReset();
        }
        else if(messageType == *SMCUI::IAreaUIHandler::AreaCreatedMessageType)
        {
            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                if(_areaUIHandler->getNumPoints() > 2)
                {
                    _populateAttributes();
                    _createMetadataRecord();

                    ELEMENTS::IClamper* clamper = area->getInterface<ELEMENTS::IClamper>();
                    if(clamper)
                    {
                        clamper->setClampOnTileLoading(_clamping);
                    }

                    if(_addToDefault)
                    {
                        _areaUIHandler->addAreaToCurrentSelectedLayer();
                    }
                    else
                    {
                        _areaUIHandler->addToLayer(_selectedFeatureLayer);
                    }

                    _areaUIHandler->reset();

                    QObject* markingFeaturesMenu = _findChild("markingFeaturesMenu");
                    if ( markingFeaturesMenu )
                    {
                        QMetaObject::invokeMethod( markingFeaturesMenu, "setState",
                            Q_ARG( QVariant, QVariant( "Area" ) ),
                            Q_ARG( QVariant, QVariant( false ) ) );

                        markingFeaturesMenu->setProperty( "selectedOption", "" );
                    }

                    QObject* polygonLayerTab = _findChild("polygonLayerTab");
                    if ( polygonLayerTab )
                    {
                        QMetaObject::invokeMethod( polygonLayerTab, "switchSelection");
                    }
                }                
                else
                {
                    emit showError("Create Area", "Cannot create area with less than 3 points.");
                    _areaUIHandler->removeCreatedArea();
                }
            }
        }
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            onVertexSelected();
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

    void CreatePolygonGUI::setClamping(bool clamping)
    {
        _clamping = clamping;
    }

    bool CreatePolygonGUI::getClamping() const
    {
        return _clamping;;
    }

    void CreatePolygonGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        if(!_areaUIHandler.valid())
        {   
            return;
        }

        if(getActive() == value)
        {
            return;
        }

        if(value)
        {
            _areaUIHandler->setTemporaryState(true);
            _areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_AREA);
            _areaUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _subscribe(_areaUIHandler.get(), *SMCUI::IAreaUIHandler::AreaCreatedMessageType);
        }
        else
        {
            _selectedFeature = NULL;

            if(_areaUIHandler->getMode() == SMCUI::IAreaUIHandler::AREA_MODE_CREATE_AREA)
                _areaUIHandler->removeCreatedArea();

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            selectionUIHandler->clearCurrentSelection();

            _areaUIHandler->setTemporaryState(false);
            _areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _areaUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            _unsubscribe(_areaUIHandler.get(), *SMCUI::IAreaUIHandler::AreaCreatedMessageType);
        }
        QtGUI::setActive(value);
    }

    void CreatePolygonGUI::_loadAreaUIHandler()
    {
        _selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        _areaUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
    }

    void CreatePolygonGUI::_populateAttributes()
    {
        std::string name = "Area " + UTIL::ToString<int>(_nextAreaCount);
        ++_nextAreaCount;

        CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
        if(!area.valid())
        {
            return;
        }

        area->getInterface<CORE::IBase>()->setName(name);
    }

    void CreatePolygonGUI::_createMetadataRecord()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = NULL;
        if(_addToDefault)
        {
            tableDefn = _areaUIHandler->getCurrentSelectedLayerDefn();
        }
        else
        {
            try
            {
                if(_selectedFeatureLayer.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                        _selectedFeatureLayer->getInterface<CORE::IMetadataTableHolder>(true);

                    if(holder.valid())
                    {
                        tableDefn = holder->getMetadataTable()->getMetadataTableDefn();
                    }
                }
            }
            catch(UTIL::Exception &e)
            {
                e.LogException();
            }
        }

        if(!tableDefn.valid())
        {
            LOG_ERROR("Invalid IMetadataTableDefn instance");
            return;
        }

        //create metadata record for the min max point
        CORE::RefPtr<CORE::IMetadataRecord> record = 
            metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        CORE::RefPtr<CORE::IMetadataField> field = 
            record->getFieldByName("NAME");
        if(field)
        {
            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area != NULL)
            {
                field->fromString(area->getInterface<CORE::IBase>()->getName());
            }
        }

        _areaUIHandler->setMetadataRecord(record.get());
    }

    void   CreatePolygonGUI::showAttributes()
    {



        if(!_mouseClickRight)
        {
            const CORE::ISelectionComponent::SelectionMap& selectionMap = _selectionUIHandler->getCurrentSelection();

            if(selectionMap.empty())
            {
                return;
            }

            CORE::ISelectable* selectable = selectionMap.begin()->second.get();
            if(selectable)
            {
                _selectedFeature = selectable->getInterface<GIS::IFeature>();
            }

            if(_selectedFeature.valid())
            {
                //Populate data for contextual tab

                //! Name of the line
                QString name = QString::fromStdString(_selectedFeature->getInterface<CORE::IBase>()->getName());
                _attributeList.append(new PropertyObject("Name ", "String", name, name.length()));
            }
        }




        if(_selectedFeature.valid())
        {
            GIS::ITable* table = _selectedFeature->getTable();
            const GIS::ITable::IFieldDefinitionList& definitionList = table->getFieldDefinitionList();

            int tableColumnCount = definitionList.size();

            _attributeList.clear();

            bool isDescriptionPresent = false;
            QString description;




            for(int i = 0 ; i < tableColumnCount; i++)
            {
                const GIS::Field* field = _selectedFeature->getField(i);
                if(field)
                {
                    CORE::RefPtr<GIS::IFieldDefinition> fieldDefn = definitionList[i].get();
                    if(fieldDefn.valid())
                    {
                        const GIS::FieldDefinitionType& type = fieldDefn->getFieldDefinitionType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName = fieldName.trimmed();

                        if((fieldName == "Description"))
                        {
                            description = QString::fromStdString(field->getAsString());
                            if(description.contains("<html>"))
                            {
                                isDescriptionPresent = true;
                                continue;
                            }
                        }
                        else if(fieldName.contains("_VIZ_", Qt::CaseInsensitive))
                        {
                            continue;
                        }
                        else if(fieldName.compare("name", Qt::CaseInsensitive) == 0)
                        {
                            continue;
                        }
                        else if(fieldName.isEmpty())
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecision = 1;

                        if (type == GIS::StringFieldDefinitionType)
                        {
                            fieldType = "String";
                            fieldValue = QString::fromStdString(field->getAsString());
                            fieldPrecision = fieldDefn->getInterface<GIS::IStringFieldDefinition>()->getLength();
                        }
                        else if(type == GIS::IntegerFieldDefinitionType)
                        {
                            fieldType = "Integer";
                            fieldValue = QString::number(field->getAsInt32());
                        }
                        else if(type == GIS::DoubleFieldDefinitionType)
                        {
                            fieldType = "Double";
                            fieldValue = QString::number(field->getAsReal64());
                            fieldPrecision = fieldDefn->getInterface<GIS::IDoubleFieldDefinition>()->getPrecision();
                        }

                        _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecision));
                    }
                }
            }

            if(_attributeList.empty())
            {
                QObject* polygonContextual = _findChild(PolygonContextualMenuObjectName);
                if(polygonContextual)
                {
                    polygonContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("polygonAttributes");
            if(showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                if(isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this, 
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);
            }
        }
        else
        {
            CORE::RefPtr<CORE::IMetadataRecord> tableRecord = 
                _areaUIHandler->getMetadataRecord();

            if(!tableRecord.valid())
            {
                return;
            }

            _attributeList.clear();

            bool isDescriptionPresent = false;
            QString description;

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
            if(!tableDefn.valid())
                return;

            if(!_mouseClickRight)
            {
                CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
                if(!area.valid())
                {
                    return;
                }

                QString name = QString::fromStdString(area->getInterface<CORE::IBase>()->getName());
                _attributeList.append(new PropertyObject("Name ", "String", name, name.length()));
                std::string uniqueId = area->getInterface<CORE::IBase>()->getUniqueID().toString() + ".json";
                std::string objectFolderLocation = UTIL::StyleFileUtil::getObjectFolder();
                jsonFile = objectFolderLocation + "/" + uniqueId;
                std::string jsonContent;
                if (osgDB::fileExists(jsonFile))
                {
                    std::ifstream inFile(jsonFile.c_str());
                    if (!inFile.is_open())
                        return;
                    jsonContent.assign((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());
                }
                QObject *polygonContextual = _findChild("polygonContextual");
                if (polygonContextual)
                {
                    polygonContextual->setProperty("jsonString", QString::fromStdString(jsonContent));
                    std::string jsonSchemaFile = "../../data/Styles/StyleSchema.json";
                    QFileInfo schemafileInfo(jsonSchemaFile.c_str());
                    QString strFilePath = schemafileInfo.absoluteFilePath();
                    std::string schemaPath = strFilePath.toStdString();
                    std::ifstream schemaFile(schemaPath.c_str());
                    if (!schemaFile.is_open())
                        return;

                    std::string schemaData((std::istreambuf_iterator<char>(schemaFile)), std::istreambuf_iterator<char>());
                    polygonContextual->setProperty("jsonSchemaContent", QString::fromStdString(schemaData));
                }
            }

            int tableColumnCount = tableDefn->getFieldCount();

            for(int i = 0 ; i < tableColumnCount; i++)
            {
                CORE::RefPtr<CORE::IMetadataField> field = tableRecord->getFieldByIndex(i);
                if(field)
                {
                    CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = field->getMetadataFieldDef();
                    if(fieldDefn.valid())
                    {
                        CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName = fieldName.trimmed();

                        if((fieldName == "Description"))
                        {
                            description = QString::fromStdString(field->toString());
                            if(description.contains("<html>"))
                            {
                                isDescriptionPresent = true;
                                continue;
                            }
                        }
                        else if(fieldName.contains("_VIZ_", Qt::CaseInsensitive))
                        {
                            continue;
                        }
                        else if(fieldName.compare("name", Qt::CaseInsensitive) == 0)
                        {
                            continue;
                        }
                        else if(fieldName.isEmpty())
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecesion;

                        switch (type)
                        {
                        case CORE::IMetadataFieldDefn::STRING:
                            {
                                fieldType = "String";
                                fieldValue = QString::fromStdString(field->toString());
                                fieldPrecesion = fieldDefn->getLength();
                            }
                            break;
                        case CORE::IMetadataFieldDefn::INTEGER:
                            {
                                fieldType = "Integer";
                                fieldValue = QString::number(field->toInt());
                            }
                            break;
                        case CORE::IMetadataFieldDefn::DOUBLE:
                            {
                                fieldType = "Double";
                                fieldValue = QString::number(field->toDouble());
                                fieldPrecesion = fieldDefn->getPrecision();
                            }
                            break;
                        }

                        _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecesion));
                    }
                }
            }

            if(_attributeList.empty())
            {
                QObject* polygonContextual = _findChild(PolygonContextualMenuObjectName);
                if(polygonContextual)
                {
                    polygonContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("polygonAttributes");
            if(showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                if(isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this, 
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);
            }
        }
    }

    void CreatePolygonGUI::handleAttributesCancelButtonPressed()
    {
        showAttributes();

        if(_selectedFeature.valid())
        {
            _selectionUIHandler->clearCurrentSelection();
        }
    }

    void CreatePolygonGUI::handleAttributesOkButtonPressed()
    {
        if(_selectedFeature.valid())
        {
            _selectionUIHandler->clearCurrentSelection();
        }
        else
        {
            CORE::RefPtr<CORE::IMetadataRecord> tableRecord = 
                _areaUIHandler->getMetadataRecord();

            if(!tableRecord.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
            if(!tableDefn.valid())
                return;

            int tableColumnCount = tableDefn->getFieldCount();

            foreach(QObject* qobj, _attributeList)
            {
                PropertyObject* propObj = qobject_cast<PropertyObject*>(qobj);
                if(propObj)
                {
                    CORE::RefPtr<CORE::IMetadataField> field = 
                        tableRecord->getFieldByName(propObj->name().toStdString());
                    if(field.valid())
                    {
                        CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = 
                            field->getMetadataFieldDef();

                        CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                        switch (type)
                        {
                        case CORE::IMetadataFieldDefn::INTEGER:
                            {
                                field->fromInt(propObj->value().toInt());
                            }
                            break;
                        case CORE::IMetadataFieldDefn::DOUBLE:
                            {
                                field->fromDouble(propObj->value().toDouble());
                            }
                            break;
                        case CORE::IMetadataFieldDefn::STRING:
                            {
                                field->fromString(propObj->value().toStdString());
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    void CreatePolygonGUI::handleStyleApplyButton()
    {
        std::string currentSelectedJsonFile = jsonFile;
        std::ofstream outfile(currentSelectedJsonFile.c_str());

        QObject* templatelistPopup = _findChild("polygonContextual");
        if (templatelistPopup != NULL)
        {
            std::string outString = templatelistPopup->property("jsonOutput").toString().toStdString();
            outfile << outString;
        }
        outfile.close();
        CORE::RefPtr<CORE::IPolygon> polygon = _areaUIHandler->getCurrentArea();
        CORE::RefPtr<ELEMENTS::IDrawTechnique> polygonDrawTech = polygon->getInterface<ELEMENTS::IDrawTechnique>();
        CORE::RefPtr<SYMBOLOGY::IStyle> styleFile = DB::StyleSheetParser::readStyle(jsonFile);
        ELEMENTS::FeatureObject* featureNodeHolder = dynamic_cast<ELEMENTS::FeatureObject*>(polygon->getInterface<CORE::IObject>());
        if (featureNodeHolder != nullptr)
        {
            featureNodeHolder->setVizPlaceNodeStyle(styleFile);
            featureNodeHolder->setBillboard(true);
        }
    }

    void CreatePolygonGUI::handleFontButton()
    {
        QString directory = "c:/";
        QString caption = "Font";

        // populating the font list again as per new font directory
        QString filters = "Font (*.ttf *.TTF)";
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filepath = QFileDialog::getOpenFileName(parent, caption, directory, filters);

        std::string fileChosen = osgDB::getSimpleFileName(filepath.toStdString());
        if (fileChosen != "")
        {
            QObject *polygonContextual = _findChild("polygonContextual");
            if (polygonContextual)
            {
                polygonContextual->setProperty("fontString", QString::fromStdString(fileChosen));
            }
        }
    }
} // namespace SMCQt

