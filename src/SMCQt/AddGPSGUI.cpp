/****************************************************************************
*
* File             : AddGPSGUI.h
* Description      : AddGPSGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#ifdef WIN32
#include <SMCQt/AddGPSGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>

#include <osgDB/FileNameUtils>

#ifdef USE_QT4
#include <QtGui/QFileDialog>
#include <QtGui/QLineEdit>
#include <QtGui/QGroupBox>
#include <QtGui/QDateEdit>
#include <QtGui/QTabWidget>
#else
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QTabWidget>
#endif

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, AddGPSGUI);
    const std::string AddGPSGUI::AddGps = "AddGps";
    AddGPSGUI::AddGPSGUI()
        : _agpsUIHandler(NULL)
    {
    }

    AddGPSGUI::~AddGPSGUI()
    {}

    void AddGPSGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void AddGPSGUI::connectPopupLoader(QString type)
    {
        if(type == "AddGps")
        {
            setActive(true);
            _gpsObject = _findChild(AddGps);
            if(_gpsObject)
            {
                // start and browse button handlers
                setActive(true);
                connect(_gpsObject, SIGNAL(browseButtonClicked()),this, SLOT(_handleBrowseButtonClicked()),Qt::UniqueConnection);
                connect(_gpsObject, SIGNAL(okButtonClicked()),this, SLOT(_handleOkButtonClicked()),Qt::UniqueConnection);
            }
        }
    }

    void AddGPSGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _agpsUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddGPSUIHandler, APP::IGUIManager>(getGUIManager());

                if(!_agpsUIHandler.valid())
                {
                    setActive(false);
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void AddGPSGUI::setActive(bool value)
    {
        try
        {
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void AddGPSGUI::_handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        std::string directory = "/home";
        std::string formatFilter = "GPS File (*.gpi *.gpx *.loc *.mps)";
        std::string caption = "GPS data Files";
        QString filename = QFileDialog::getOpenFileName(parent, caption.c_str(), directory.c_str(), formatFilter.c_str(), 0);
        if(!filename.isEmpty())
        {
            _gpsObject = _findChild(AddGps);
            if(_gpsObject)
            {
                _gpsObject->setProperty("sourceFile",QVariant::fromValue(filename));
                try
                {
                    directory = osgDB::getFilePath(filename.toStdString());
                    getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
                }
                catch(...){}
            }
        }
    }

    void AddGPSGUI::_handleCancelButtonClicked()
    {
        setActive(false);
    }

    void AddGPSGUI::_handleOkButtonClicked()
    {
        _gpsObject = _findChild(AddGps);
        if(_gpsObject)
        {
            try
            {
               // _configureLogfile();
                QString filename = _gpsObject->property("sourceFile").toString();
                _agpsUIHandler->loadSource(filename.toStdString());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
                emit showError("Add GPS Data Error", e.What().c_str());
                return;
            }

            QMetaObject::invokeMethod(_gpsObject, "closePopup");
        }
        setActive(false);
    }

    void AddGPSGUI::_configureNMEATCPSource()
    {
        // Set type as NMEATCP Source
        _agpsUIHandler->setCurrentType(SMCUI::IAddGPSUIHandler::NMEATCP);

        // Query IP and port attributes and set values
        // XXX - make attributes configurable
        CORE::Attribute* addressAttr = _agpsUIHandler->getSourceAttribute("IPAddress");
        if(addressAttr)
        {
            _gpsObject = _findChild(AddGps);
            if(_gpsObject)
            { 
                QString addressStr = _gpsObject->property("address").toString().trimmed();
                if(addressStr.isEmpty())
                {
                    throw UTIL::Exception("Address needs to be specified", __FILE__, __LINE__);
                }
                addressAttr->fromString(addressStr.toStdString());
            }
        }

        CORE::Attribute* portAttr = _agpsUIHandler->getSourceAttribute("Port");
        if(portAttr)
        {
            _gpsObject = _findChild(AddGps);
            if(_gpsObject)
            { 
                QString portStr = _gpsObject->property("port").toString().trimmed();
                if(portStr.isEmpty())
                {
                    throw UTIL::Exception("Port needs to be specified", __FILE__, __LINE__);
                }
                portAttr->fromString(portStr.toStdString());
            }
        }
    }

    void AddGPSGUI::_configureLogfile()
    {
        // Set type as NMEATCP Source
        _agpsUIHandler->setCurrentType(SMCUI::IAddGPSUIHandler::Log);

        // Query file attribute
        CORE::Attribute* fileAttr = _agpsUIHandler->getSourceAttribute("Filename");
        if(fileAttr)
        {   
            _gpsObject = _findChild(AddGps);
            if(_gpsObject)
            {
                QString filename = _gpsObject->property("sourceFile").toString();
                if(filename.isEmpty())
                {
                    throw UTIL::Exception(" Source data file path is not specified, please browse any valid data file. ", __FILE__, __LINE__);
                }
                else
                {
                    fileAttr->fromString(filename.toStdString());
                }
            }
        }

        // Query format attribute
        CORE::Attribute* formatAttr = _agpsUIHandler->getSourceAttribute("Format");
        if(formatAttr)
        {
            formatAttr->fromString("nmea");
        }
    }

    void AddGPSGUI::_reset()
    {
        _gpsObject = _findChild(AddGps);
        if(_gpsObject)
        {
            _gpsObject->setProperty("isVisible","false");
        }
    }
} // namespace indiGUI

#endif //WIN32
