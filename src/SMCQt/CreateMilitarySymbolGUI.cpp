/****************************************************************************
*
* File             : CreateMilitarySymbolGUI.h
* Description      : CreateMilitarySymbolGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/


#include <SMCQt/CreateMilitarySymbolGUI.h>
#include <SMCQt/IconObject.h>

#include <Core/IWorld.h>
#include <Core/WorldMaintainer.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Elements/ElementsUtils.h>
#include <Elements/IProjectSettingsComponent.h>
#include <Elements/MilitarySymbolType.h>

#include <Util/Log.h>

#include <QObject>

namespace SMCQt
{


    CreateMilitarySymbolGUI::CreateMilitarySymbolGUI() 
    {}

    CreateMilitarySymbolGUI::~CreateMilitarySymbolGUI()
    {
        //        delete _resourcesList;
    }

    void CreateMilitarySymbolGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();


        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader != NULL)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }
    }

    void CreateMilitarySymbolGUI::popupLoaded(QString type)
    {
        if(type == "smpResourcesMenu")
        {
            _populateGUI();
        }
    }

    void CreateMilitarySymbolGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                // Query the component here
                CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                if(wmain)
                {
                    CORE::IComponent* comp = wmain->getComponentByName("SymbolTypeLoader");
                    if(comp)
                    {
                        _symbolLoaderComp = comp->getInterface<ELEMENTS::ISymbolTypeLoader>();
                    }

                    _uihandler = APP::AccessElementUtils::
                        getUIHandlerUsingManager<SMCUI::IMilitarySymbolPointUIHandler>(getGUIManager());
                }

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

    void CreateMilitarySymbolGUI::_populateGUI()
    {
        if(!_symbolLoaderComp.valid())
        {
            return;
        }

        // frist get the name of symbology used
        QString symbology = QString::fromStdString(_symbolLoaderComp->getSymbologyType());

        _uniqueAcrossAffiliation = _symbolLoaderComp->isUniformAcrossAffiliation();


        // Get the symbol map
        const ELEMENTS::ISymbolTypeLoader::SymbolMap& symbolMap = _symbolLoaderComp->getSymbolMap();

        _populateList("Friend");

        QObject* smpResourcesMenu = _findChild("smpResourcesMenu");
        if(smpResourcesMenu != NULL)
        {
            smpResourcesMenu->setProperty("symbology", QVariant::fromValue(symbology));
            smpResourcesMenu->setProperty("valueSet", QVariant::fromValue(true));

            CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
            CORE::RefPtr<ELEMENTS::IProjectSettingsComponent> projectSettingComponent;
            CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("ProjectSettingsComponent");
            if (component.valid())
            {
                projectSettingComponent = component->getInterface<ELEMENTS::IProjectSettingsComponent>();
                smpResourcesMenu->setProperty("symbolSize", QString::fromStdString(std::to_string((int)projectSettingComponent->getMilSymbolSize())));
            }

            QObject::connect(smpResourcesMenu, SIGNAL(showAffiliation(QString)), 
                this, SLOT(showAffiliation(QString)), Qt::UniqueConnection);

            QObject::connect(smpResourcesMenu, SIGNAL(showCategory(QString, QString)),
                this, SLOT(showCategory(QString, QString)), Qt::UniqueConnection);

            QObject::connect(smpResourcesMenu, SIGNAL(setSymbolSize(QString)),
                this, SLOT(setSymbolSize(QString)), Qt::UniqueConnection);

            
        }
    }

    void CreateMilitarySymbolGUI::showAffiliation(QString affiliation)
    {

        _setContextProperty("categoriesModel", QVariant::fromValue(NULL));
        _setContextProperty("symbolsModel", QVariant::fromValue(NULL));
        _populateList(affiliation.toStdString());
    }

    void CreateMilitarySymbolGUI::showCategory(QString affiliation, QString category)
    {
        _setContextProperty("symbolsModel", QVariant::fromValue(NULL));
        _populateList(affiliation.toStdString(), category.toStdString());
    }

    void CreateMilitarySymbolGUI::setSymbolSize(QString symbolSize)
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        CORE::RefPtr<ELEMENTS::IProjectSettingsComponent> _projectSettingComponent;
        CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("ProjectSettingsComponent");
        if (component.valid())
        {
            _projectSettingComponent = component->getInterface<ELEMENTS::IProjectSettingsComponent>();
        }

        bool ok = true;

        double size = symbolSize.toDouble(&ok);
        LOG_INFO("symbol Size form Resouces.qml " + std::to_string(size));

        if (ok)
            _projectSettingComponent->setMilSymbolSize(size);
        else
            _projectSettingComponent->setMilSymbolSize(32.0f);
    }


    void CreateMilitarySymbolGUI::_populateList(const std::string& affiliationStr, const std::string& categoryStr)
    {
        ELEMENTS::IMilitarySymbolType::Affiliation affiliation = ELEMENTS::IMilitarySymbolType::COMMON;
        std::string affiliationName;
        if(!_uniqueAcrossAffiliation)
        {
            affiliation = ELEMENTS::getAffiliationFromString(affiliationStr);
            affiliationName = affiliationStr;
        }
        else
        {
            affiliationName = "Common";
        }

        // get featureLayerTypeCategoryMap
        const ELEMENTS::ISymbolTypeLoader::SymbolFeatureTypeCategoryMap& symbolFeatureTypeCategoryMap =
            _symbolLoaderComp->getSymbolFeatureTypeCategoryMap();

        // only populating elements with point geometry right now
        ELEMENTS::ISymbolTypeLoader::SymbolFeatureTypeCategoryMap::const_iterator featureMapIter =
            symbolFeatureTypeCategoryMap.find(CORE::IFeatureLayer::POINT);

        if(featureMapIter == symbolFeatureTypeCategoryMap.end())
        {
            return;
        }

        // get affiliation map
        const ELEMENTS::ISymbolTypeLoader::SymbolAffiliationTypeCategoryMap& symbolAffiliationTypeCategoryMap =
            featureMapIter->second;

        //find affiliation
        ELEMENTS::ISymbolTypeLoader::SymbolAffiliationTypeCategoryMap::const_iterator affiliationTypeIter =
            symbolAffiliationTypeCategoryMap.find(affiliation);

        if(affiliationTypeIter == symbolAffiliationTypeCategoryMap.end())
        {
            return;
        }

        // get category map
        const ELEMENTS::ISymbolTypeLoader::SymbolCategoryMap& symbolCategoryMap =
            affiliationTypeIter->second;

        const ELEMENTS::ISymbolTypeLoader::SymbolMap& symbolMap = _symbolLoaderComp->getSymbolMap();

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();

        if(categoryStr == "")    // populate categories if category passed was empty
        {
            ELEMENTS::ISymbolTypeLoader::SymbolCategoryMap::const_iterator categoryIter =
                symbolCategoryMap.begin();

            // list of category names
            QStringList qCategoryList;
            QList<QObject*> qSymbolList;

            bool first = true; // to populate the symbols from first category

            // iterate through categories
            for(; categoryIter != symbolCategoryMap.end(); categoryIter++)
            {
                ELEMENTS::ISymbolTypeLoader::SymbolCategory symbolCategory = categoryIter->second;
                ELEMENTS::ISymbolTypeLoader::SymbolSubCategoryMap subCategoryMap = symbolCategory.symbolSubCategories;
                ELEMENTS::ISymbolTypeLoader::SymbolSubCategoryMap::const_iterator subCategoryIter =
                    subCategoryMap.begin();

                std::string categoryName;

                // add category as entry is subcategory doesnt exist
                if(!symbolCategory.subCategoryExists)
                {
                    categoryName = categoryIter->first;
                    qCategoryList.append(QString::fromStdString(categoryName));
                }

                // iterate through sub categories
                for(; subCategoryIter != subCategoryMap.end(); subCategoryIter++)
                {
                    // add sub categories to list if sub categories exist in parent category
                    if(symbolCategory.subCategoryExists)
                    {
                        categoryName = subCategoryIter->first;
                        qCategoryList.append(QString::fromStdString(categoryName));
                    }
                    if(first)
                    {
                        // populate symbols 
                        const ELEMENTS::ISymbolTypeLoader::SymbolTypes& symbolTypeVector = subCategoryIter->second;
                        ELEMENTS::ISymbolTypeLoader::SymbolTypes::const_iterator symbolIter =symbolTypeVector.begin();

                        while(symbolIter != symbolTypeVector.end())
                        {
                            std::string symbolName = *(symbolIter);
                            std::string fullName = affiliationName+"/"+categoryName+"/"+symbolName;

                            CORE::RefPtr<ELEMENTS::IMilitarySymbolType> milSymbolType = 
                                _symbolLoaderComp->getMilitarySymbolTypeByFullName(fullName);
                            if(milSymbolType.valid())
                            {
                                std::string strFileName = _uihandler->getQMLIconPath(milSymbolType);

                                QString name = QString::fromStdString(symbolName);
                                QString iconPath = QString::fromStdString(strFileName);

                                qSymbolList.append(new IconObject(name, iconPath));
                            }
                            symbolIter++;
                        }
                        _setContextProperty("symbolsModel", QVariant::fromValue(qSymbolList));

                        first = false;
                    }
                }
            }
            _setContextProperty("categoriesModel", QVariant::fromValue(qCategoryList));

            // initialize the selected value variables with first elements making sure there are no garbage values
            QObject* smpResourcesMenu = _findChild("smpResourcesMenu");
            if(smpResourcesMenu != NULL)
            {
                smpResourcesMenu->setProperty("selectedAffiliation", 
                    QVariant::fromValue(QString::fromStdString(affiliationStr)));

                smpResourcesMenu->setProperty("selectedCategory", QVariant::fromValue(qCategoryList.first()));
                IconObject* iiconObj = qobject_cast<IconObject*>(qSymbolList.first());
                smpResourcesMenu->setProperty("selectedSymbol", QVariant::fromValue(iiconObj->name()));
            }
        }
        else // categoryStr != "" 
        {
            // find the category from map
            ELEMENTS::ISymbolTypeLoader::SymbolCategoryMap::const_iterator categoryIter =
                symbolCategoryMap.find(categoryStr);
            ELEMENTS::ISymbolTypeLoader::SymbolSubCategoryMap::const_iterator subCategoryIter;

            // if not found in map search through subctegories
            if(categoryIter == symbolCategoryMap.end())
            {
                categoryIter = symbolCategoryMap.begin();
                while(categoryIter != symbolCategoryMap.end())
                {
                    if(categoryIter->second.subCategoryExists)
                    {
                        subCategoryIter = categoryIter->second.symbolSubCategories.find(categoryStr);
                        break;
                    }
                    categoryIter++;
                }
            }
            else 
            {
                subCategoryIter = categoryIter->second.symbolSubCategories.begin();
            }

            if(categoryIter != symbolCategoryMap.end())
            {
                if(subCategoryIter != categoryIter->second.symbolSubCategories.end())
                {
                    QList<QObject*> symbolsModel;

                    // populate symbols 
                    const ELEMENTS::ISymbolTypeLoader::SymbolTypes& symbolTypeVector = subCategoryIter->second;
                    ELEMENTS::ISymbolTypeLoader::SymbolTypes::const_iterator symbolIter =symbolTypeVector.begin();

                    while(symbolIter != symbolTypeVector.end())
                    {
                        std::string symbolName = *(symbolIter);
                        std::string fullName = affiliationName+"/"+categoryStr+"/"+symbolName;

                        CORE::RefPtr<ELEMENTS::IMilitarySymbolType> milSymbolType = 
                            _symbolLoaderComp->getMilitarySymbolTypeByFullName(fullName);
                        if(milSymbolType.valid())
                        {
                            std::string strFileName = _uihandler->getQMLIconPath(milSymbolType);

                            QString name = QString::fromStdString(symbolName);
                            QString iconPath = QString::fromStdString(strFileName);

                            symbolsModel.append(new IconObject(name, iconPath));
                        }
                        symbolIter++;
                    }
                    _setContextProperty("symbolsModel", QVariant::fromValue(symbolsModel));

                    // initialize the selected value variables with first elements making sure there are no garbage values
                    QObject* smpResourcesMenu = _findChild("smpResourcesMenu");
                    if(smpResourcesMenu != NULL)
                    {
                        smpResourcesMenu->setProperty("selectedAffiliation", 
                            QVariant::fromValue(QString::fromStdString(affiliationStr)));
                        smpResourcesMenu->setProperty("selectedCategory", 
                            QVariant::fromValue(QString::fromStdString(categoryStr)));

                        IconObject* iiconObj = qobject_cast<IconObject*>(symbolsModel.first());
                        smpResourcesMenu->setProperty("selectedSymbol", QVariant::fromValue(iiconObj->name()));
                    }
                }
            }
        }
    }
} // namespace SMCQt
