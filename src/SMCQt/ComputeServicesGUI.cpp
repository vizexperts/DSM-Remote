#include <SMCQt/ComputeServicesGUI.h>

#include <App/Application.h>
#include <App/IUIHandler.h>
#include <App/AccessElementUtils.h>

#include <SMCQt/QMLTreeModel.h>

#include <osgDB/FileNameUtils>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ComputeServicesGUI);
    DEFINE_IREFERENCED(ComputeServicesGUI, DeclarativeFileGUI);

    ComputeServicesGUI::ComputeServicesGUI()
    {
        
    }

    ComputeServicesGUI::~ComputeServicesGUI()
    {

    }

    void ComputeServicesGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _computeServicesUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<ComputeServices::IComputeServicesUIHandler>(getGUIManager());
            }
            catch (...)
            { }
        }

        DeclarativeFileGUI::update(messageType, message);
    }

    void ComputeServicesGUI::popupLoaded()
    {
        if (!_computeServicesUIHandler.valid())
            return;

        std::string responseStr;
        ComputeServices::IComputeServicesUIHandler::Connection connection;

        connection.ip = "http://192.168.0.73";
        connection.port = 19090;
        connection.connectionMode = ComputeServices::IComputeServicesUIHandler::GEORBIS_SERVER;

        _computeServicesUIHandler->setConnection(connection);
        _computeServicesUIHandler->listServices(responseStr);

        //LOG_DEBUG(responseStr);

        QJsonDocument doc = QJsonDocument::fromJson(QString::fromStdString(responseStr).toUtf8());

        QFile saveFile("E:\\tree\\vs2013\\DSM_Data\\response.json");
        if (saveFile.open(QIODevice::WriteOnly))
        {
            saveFile.write(doc.toJson());
            saveFile.close();
        }

        if (doc.isNull())
        {
            LOG_ERROR("Invalid JSON...");
            return;
        }

        if (!doc.isObject())
        {
            LOG_ERROR("Document is not a object,..");
            return;
        }

        QJsonObject responseObj = doc.object();

        //! get success code
        bool success = responseObj["success"].toBool();
        if (!success)
        {
            LOG_ERROR("Failure respopnse code.");
            return;
        }

        QMultiMap<QString, QString> map;

        //! get result
        QJsonValueRef resultValue(responseObj["result"]);
        if (resultValue.isArray())
        {
            QJsonArray results = resultValue.toArray();
            foreach(const QJsonValue& value, results)
            {
                QString serviceFullName = value.toString();

                QString groupName = serviceFullName.section('_', 0, 0);
                QString  serviceName = serviceFullName.section('_', 1);

                map.insertMulti(groupName, serviceName);
            }
        }
        else
        {
            QJsonObject results = resultValue.toObject();
            foreach (const QString& value, results.keys())
            {
                QString groupName = value.section('_', 0, 0);
                QString  serviceName = value.section('_', 1);

                map.insertMulti(groupName, serviceName);
            }
        }

        //! create a tree model
        SimpleTreeModel* _treeModel = new SimpleTreeModel;
        _treeModel->clear();
       
        foreach(QString key, map.uniqueKeys())
        {
            QStringList values = map.values(key);

            SimpleTreeModelItem* simpleKeyItem = new SimpleTreeModelItem(key);
            simpleKeyItem->setCheckable(false);

            foreach (QString value , values)
            {
                SimpleTreeModelItem* simpleValueItem = new SimpleTreeModelItem(value);
                simpleValueItem->setCheckable(false);
                simpleKeyItem->addItem(simpleValueItem);
            }

            _treeModel->addItem(simpleKeyItem);
        }

        //_setContextProperty("analysisListModel", QVariant::fromValue(serviceList));
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {
            qtapp->getRootContext()->setContextProperty("analysisListModel", _treeModel);
        }

    }

}