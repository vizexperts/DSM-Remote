/****************************************************************************
*
* File             : LoginGUI.cpp
* Description      : LoginGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/LoginGUI.h>
#include <App/IApplication.h>
#include <Core/GroupAttribute.h>
#include <Core/AttributeTypes.h>
#include <Core/NamedAttribute.h>
#include <vector>

#include <VizQt/IQtApplication.h>
#include <Os/osLogon.h>

#include <QJSONDocument>
#include <QJSONObject> 

#include <Database/DatabaseUtils.h>
#include <Database/IDBRecord.h>
#include <Database/IDBTable.h>
#include <Database/IDBField.h>

#include <lm_attr.h>
#include <lmclient.h>
#include <Elements/SettingComponent.h>
#include <Core/WorldMaintainer.h>
#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>
#include <serialization/AsciiStreamOperator.h>

struct lm_handle;
typedef lm_handle LM_HANDLE;

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, LoginGUI);
    DEFINE_IREFERENCED(LoginGUI, DeclarativeFileGUI);

    const std::string LoginGUI::USERNAME                = "Username";
    const std::string LoginGUI::PASSWORD                = "Password";
    const std::string LoginGUI::TYPE                    = "Type";

    const std::string LoginGUI::PermissionsTableName    = "VizPermissions";
    const std::string LoginGUI::PermissionsTable_Username    = "username";
    const std::string LoginGUI::PermissionsTable_Analysis    = "analysis";
    const std::string LoginGUI::PermissionsTable_Planning    = "planning";
    const std::string LoginGUI::PermissionsTable_Add_Data    = "add_data";
    const std::string LoginGUI::PermissionsTable_AdminUser    = "adminUser";

    const std::string LoginGUI::UacSettingsMenuName = "uacSettingsMenu";
    std::map<std::string, std::string> readSettings(DB::InputStream& is);
    QPermissionsModel::QPermissionsModel(QObject *parent) : QAbstractListModel(parent)
    {
#ifdef USE_QT4
        setRoleNames(roleNames());
#endif
    }

    QPermissionsObject* QPermissionsModel::getItem(int numIndex)
    {
        if(!_isValidIndex(numIndex))
            return NULL;

        return _items[numIndex];
    }

    QHash<int,QByteArray> QPermissionsModel::roleNames() const
    {
        QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
        roles.insert(UsernameRole, QByteArray("username"));
        roles.insert(AnalysisRole, QByteArray("analysis"));
        roles.insert(PlanningRole, QByteArray("planning"));
        roles.insert(AddDataRole, QByteArray("add_data"));
        roles.insert(AdminUserRole, QByteArray("adminUser"));
        roles.insert(DirtyRole, QByteArray("dirty"));

        return roles;
    }
    
    void QPermissionsModel::analysisRoleChanged(int index, bool value)
    {
        if(!_isValidIndex(index))
            return;

        _items.at(index)->setAnalysis(value);

        markDirty(index);
    }

    void QPermissionsModel::planningRoleChanged(int index, bool value)
    {
        if(!_isValidIndex(index))
            return;

        _items.at(index)->setPlanning(value);
        markDirty(index);
    }

    void QPermissionsModel::addDataRoleChanged(int index, bool value)
    {
        if(!_isValidIndex(index))
            return;

        _items.at(index)->setAdd_data(value);
        markDirty(index);
    }

    void QPermissionsModel::adminUserRoleChanged(int index, bool value)
    {
        if(!_isValidIndex(index))
            return;

        _items.at(index)->setAdminUser(value);
        markDirty(index);
    }

    bool QPermissionsModel::addItem(QPermissionsObject *item)
    {
        if(item == NULL)
            return false;

        beginInsertRows(QModelIndex(), _items.size(), _items.size());
        _items <<item;
        endInsertRows();

        return true;
    }

    bool QPermissionsModel::removeItem(int index)
    {
        if(!_isValidIndex(index))
            return false;

        beginRemoveRows(QModelIndex(), index, index);
        _items.removeAt(index);
        endRemoveRows();

        return true;
    }

    void QPermissionsModel::clear()
    {
        //qDeleteAll(_items.begin(), _items.end());//No need to call qDeleteAll will clear in clear funtion
        _items.clear();
    }

    int QPermissionsModel::rowCount(const QModelIndex& Parent) const
    {
        Q_UNUSED(Parent);
        return _items.size();
    }

    const QList<QPermissionsObject*>& QPermissionsModel::getChildren() const
    {
        return _items;
    }

    QVariant QPermissionsModel::data(const QModelIndex& index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        if(!_isValidIndex(index.row()))
            return QVariant();

        QPermissionsObject *item = _items.at(index.row());
        switch (role)
        {
        case Qt::DisplayRole:
        case UsernameRole:
            return QVariant::fromValue(item->username());
        case AnalysisRole:
            return QVariant::fromValue(item->analysis());
        case PlanningRole:
            return QVariant::fromValue(item->planning());
        case AddDataRole:
            return QVariant::fromValue(item->add_data());
        case AdminUserRole:
            return QVariant::fromValue(item->adminUser());
        default:
            return QVariant();
        }
    }

    LoginGUI::LoginGUI()
    {
        _credentialsMap.clear();
        _permissionsListModel = NULL;
        _isVuPresent = false;
        _modulesAvailableInLicense.clear();
    }

    LoginGUI::~LoginGUI()
    {}

    void LoginGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();

        std::string groupName = "LoginGUI Attributes";
        _addAttribute(new CORE::GroupAttribute("Credential", "Credential",
            CORE::GroupAttribute::SetFuncType(this, &LoginGUI::addCredential),
            CORE::GroupAttribute::GetFuncType(this, &LoginGUI::getCredential),
            std::string("Credential"), groupName));

        _addAttribute(new CORE::StringAttribute("AuthDatabase", "AuthDatabase",
            CORE::StringAttribute::SetFuncType(this, &LoginGUI::setAuthDatabase),
            CORE::StringAttribute::GetFuncType(this, &LoginGUI::getAuthDatabase),
            std::string("AuthDatabase"), groupName));
    }

    void LoginGUI::setAuthDatabase(const std::string& authDB)
    {
        _authenticationDB = authDB;
    }

    std::string LoginGUI::getAuthDatabase() const
    {
        return _authenticationDB;
    }

    void LoginGUI::addCredential(const CORE::NamedGroupAttribute &credential)
    {

        const CORE::NamedStringAttribute* attrUserName = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(USERNAME));

        const CORE::NamedStringAttribute* attrPassword = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(PASSWORD));

        const CORE::NamedStringAttribute* attrType = 
            dynamic_cast<const CORE::NamedStringAttribute*>(credential.getAttribute(TYPE));
        if(!attrUserName || !attrPassword || !attrType)
        {
            return;
        }

        std::string username    = attrUserName->getValue();
        std::string password    = attrPassword->getValue();
        std::string type        = attrType->getValue();

        CredentialsMap::const_iterator iter = _credentialsMap.find(username);
        if(iter == _credentialsMap.end())   
        {
            User user;
            user.password = password;

            if(type == "USERTYPE_INSTRUCTOR")
            {
                user.type = USERTYPE_INSTRUCTOR;
            }
            else if(type == "USERTYPE_MODELCREATOR")
            {
                user.type = USERTYPE_MODELCREATOR;
            }
            else if(type == "USERTYPE_SYNDICATE")
            {
                user.type = USERTYPE_SYNDICATE;
            }
            else 
            {
                user.type = USERTYPE_NONE;
            }

            _credentialsMap[username] = user;
        }
    }

    CORE::RefPtr<CORE::NamedGroupAttribute> LoginGUI::getCredential() const
    {
        return _credentialType;
    }

    void LoginGUI::connectUacPopup()
    {
        QObject* uacSettingsMenu = _findChild(UacSettingsMenuName);
        if(!uacSettingsMenu)
            return;

        //! populate all users list
        _populateUserList();

        //! Connect to signals and slots..
        QObject::connect(uacSettingsMenu, SIGNAL(okButtonClicked()), this, SLOT(okButtonClicked()));
        QObject::connect(uacSettingsMenu, SIGNAL(cancelButtonClicked()), this, SLOT(cancelButtonClicked()));
        QObject::connect(uacSettingsMenu, SIGNAL(addToActiveUsers(int)), this, SLOT(addToActiveUsers(int)));
        QObject::connect(uacSettingsMenu, SIGNAL(removeFromActiveUsers(int)), this, SLOT(removeFromActiveUsers(int)));
        //QObject::connect(uacSettingsMenu, SIGNAL(markRowDirty(int)), this, SLOT(markRowDirty(int)));

        QObject::connect(uacSettingsMenu, SIGNAL(analysisRoleChanged(int, bool)), _permissionsListModel, SLOT(analysisRoleChanged(int, bool)), Qt::UniqueConnection);
        QObject::connect(uacSettingsMenu, SIGNAL(planningRoleChanged(int, bool)), _permissionsListModel, SLOT(planningRoleChanged(int, bool)), Qt::UniqueConnection);
        QObject::connect(uacSettingsMenu, SIGNAL(addDataRoleChanged(int, bool)), _permissionsListModel, SLOT(addDataRoleChanged(int, bool)), Qt::UniqueConnection);
        QObject::connect(uacSettingsMenu, SIGNAL(adminUserRoleChanged(int, bool)), _permissionsListModel, SLOT(adminUserRoleChanged(int, bool)), Qt::UniqueConnection);
    }

    void LoginGUI::disconnectUacPopup()
    {
        _setContextProperty("nonActiveUsersList", QVariant::fromValue(NULL));
        _setContextProperty("permissionsListModel", QVariant::fromValue(NULL));
    }

    void LoginGUI::okButtonClicked()
    {
        if(_permissionsListModel == NULL)
            return;


        //! Get the database
        CORE::RefPtr<DATABASE::IDatabase> database = DATABASE::GetDatabaseFromMaintainer(_authenticationDB);

        if(!database.valid())
        {
            emit showError("UAC Settings", "Authentication server is not reachable.");
            return;
        }

        //! Apply the changes
        foreach(QPermissionsObject* item, _permissionsListModel->getChildren() )
        {
            if(item->dirty())
            {
                item->setDirty(false);

                //! Update in the database
                std::stringstream queryStatement;
                queryStatement << "UPDATE " << PermissionsTableName << " SET " 
                    << PermissionsTable_Analysis << " = " << (item->analysis() ? "'1'" : "'0'") << ", "
                    << PermissionsTable_Planning << " = " << (item->planning() ? "'1'" : "'0'") << ", "
                    << PermissionsTable_Add_Data << " = " << (item->add_data() ? "'1'" : "'0'") << ", "
                    << PermissionsTable_AdminUser << " = " <<(item->adminUser() ? "'1'" : "'0'")
                    << " WHERE " << PermissionsTable_Username << " = '" << item->username().toStdString() << "'";


                DATABASE::ExecuteSQLQuery(queryStatement.str(), database);
            }
        }


        //! Show a message that the changes in permissions would be applicable from the next logon...
        emit showError("UAC", "Your changes have been saved. Changes would only be applicable from next logon.", false);

    }

    void LoginGUI::cancelButtonClicked()
    {
        //! repopulate the values from the database,.,.,

        _populateUserList();
    }

    void LoginGUI::addToActiveUsers(int index)
    {   
        //! Get the user from non active users list
        if(index < 0 || index >= _nonActiveUserList.size())
            return;

        QString username = _nonActiveUserList[index];

        //! Get the database
        CORE::RefPtr<DATABASE::IDatabase> database = DATABASE::GetDatabaseFromMaintainer(_authenticationDB);

        if(!database.valid())
        {
            emit showError("UAC Settings", "Authentication server is not reachable.");
            return;
        }

        //! Add entry to the database
        std::stringstream queryStatement;
        queryStatement << "INSERT INTO " << PermissionsTableName << " (" 
            << PermissionsTable_Username <<", "
            << PermissionsTable_Analysis << ", "
            << PermissionsTable_Planning << ", "
            << PermissionsTable_Add_Data << ", "
            << PermissionsTable_AdminUser << " ) "
            <<" VALUES ( " 
            <<"'" << username.toStdString() << "', "                    //! Username
            << "'1', "                                                  //! Analysis
            << "'1', "                                                  //! Planning
            << "'1', "                                                  //! Add Data
            << "'0' )";                                                 //! AdminUser

        DATABASE::ExecuteSQLQuery(queryStatement.str(), database);

        //! move the value from non active users list to active users list
        _nonActiveUserList.removeAt(index);

        QPermissionsObject* permObj = new QPermissionsObject(username);
        permObj->setAnalysis(true);
        permObj->setPlanning(true);
        permObj->setAdd_data(true);
        _permissionsListModel->addItem(permObj);

        _setContextProperty("nonActiveUsersList", QVariant::fromValue(_nonActiveUserList));
    }

    void LoginGUI::removeFromActiveUsers(int index)
    {
        //! Get the user from active users list
        if(index < 0 || index >= _permissionsListModel->rowCount())
            return;

        QPermissionsObject* permObj = _permissionsListModel->getItem(index);
        QString username = permObj->username();

        //! Get the database
        CORE::RefPtr<DATABASE::IDatabase> database = DATABASE::GetDatabaseFromMaintainer(_authenticationDB);

        if(!database.valid())
        {
            emit showError("UAC Settings", "Authentication server is not reachable.");
            return;
        }

        //! Remove entry from the database
        std::stringstream queryStatement;
        queryStatement << "DELETE FROM " << PermissionsTableName << " WHERE username = '" << username.toStdString() <<"'";
        DATABASE::ExecuteSQLQuery(queryStatement.str(), database);

        //! move the value from non active users list to active users list
        _permissionsListModel->removeItem(index);

        _nonActiveUserList.push_back(username);

        _setContextProperty("nonActiveUsersList", QVariant::fromValue(_nonActiveUserList));
        //_setContextProperty("permissionsListModel", QVariant::fromValue(_permissionsListModel));
    }

    void LoginGUI::_populateUserList()
    {
        //!
        if(_permissionsListModel == NULL)
        {
            _permissionsListModel = new QPermissionsModel();
        }

        std::string serverName;
        std::vector<std::string> usernameList;

        //! Get the list of all users available from the os
        OS::Logon::GetUserList(usernameList);
        _nonActiveUserList.clear();
        for(std::vector<std::string>::const_iterator userNameItr = usernameList.begin(); userNameItr != usernameList.end(); ++userNameItr)
        {
            QString username = QString::fromStdString(*userNameItr);

            _nonActiveUserList.push_back(QString::fromStdString(*userNameItr));
        }

        //! Get the list of all registered users from the database

        //! Connect to DB
        CORE::RefPtr<DATABASE::IDatabase> database = DATABASE::GetDatabaseFromMaintainer(_authenticationDB);

        if(!database.valid())
        {
            emit showError("UAC Settings", "Authentication server is not reachable.");
            return;
        }

        //! Create a permissions table if it doesn't exist.
        _createTable(PermissionsTableName, database);

        //! Select all the usernames that are given some access..
        std::stringstream queryStatement;
        queryStatement << "SELECT * FROM " + PermissionsTableName +" " ;

        CORE::RefPtr<DATABASE::ISQLQueryResult> result = DATABASE::ExecuteSQLQuery(queryStatement.str(), database);

        //! Delete all the _items
        _permissionsListModel->clear();

        if(result.valid())
        {
            //! get the result table
            CORE::RefPtr<DATABASE::IDBTable> resultTable = result->getResultTable();
            if(resultTable.valid())
            {
                CORE::RefPtr<DATABASE::IDBRecord> record = NULL;

                while((record = resultTable->getNextRecord()) != NULL)
                {
                    DATABASE::IDBField* usernameField = record->getFieldByName(PermissionsTable_Username);

                    //! Column type in sql server is 'bit'
                    //! However using odbc driver this is being returned as string somehow
                    DATABASE::IDBField* analysisField = record->getFieldByName(PermissionsTable_Analysis);
                    DATABASE::IDBField* planningField = record->getFieldByName(PermissionsTable_Planning);
                    DATABASE::IDBField* add_dataField = record->getFieldByName(PermissionsTable_Add_Data);
                    DATABASE::IDBField* adminUserField = record->getFieldByName(PermissionsTable_AdminUser);


                    QString qusername = QString::fromStdString(usernameField->toString());
                    QPermissionsObject* permObj = new QPermissionsObject(qusername);
                    permObj->setAnalysis(UTIL::ToType<bool>(analysisField->toString()));
                    permObj->setPlanning(UTIL::ToType<bool>(planningField->toString()));
                    permObj->setAdd_data(UTIL::ToType<bool>(add_dataField->toString()));
                    permObj->setAdminUser(UTIL::ToType<bool>(adminUserField->toString()));
                    _permissionsListModel->addItem(permObj);

                    //! remove from the non active users list..
                    _nonActiveUserList.removeOne(qusername);
                }
            }
        }


        _setContextProperty("nonActiveUsersList", QVariant::fromValue(_nonActiveUserList));
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QQmlContext* rootContext  = qtapp->getRootContext();
        if(rootContext)
        {
            rootContext->setContextProperty("permissionsListModel", _permissionsListModel);
        }
    }

    void LoginGUI::_createTable(const std::string& tableName, CORE::RefPtr<DATABASE::IDatabase> database)
    {
        //! Execute this code at most once...
        static bool created = false;

        if(!created)
        {

            //! Make sure you migrate your old database tables to new if you want to add a new permission type.
            std::stringstream queryStatement;
            queryStatement << "CREATE TABLE " << tableName 
                << " ( username varchar(100) PRIMARY KEY, "     //! Username
                << " analysis bit DEFAULT 0, "                  //! Analysis permission
                << " planning bit DEFAULT 0, "                  //! Planning permission
                << " adminUser bit DEFAULT 0, "                  //! Admin role permission
                << " add_data bit DEFAULT 0) ";                 //! Add data permission

            DATABASE::ExecuteSQLQuery(queryStatement.str(), database);
        }

        created = true;
    }

    void LoginGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // checking the licenses first up because there is no point in validating credentials if license is not available
            _modulesAvailableInLicense.clear();
            _isVuPresent = false;
            _checkLicenses();

            if (!_isVuPresent)
            {
                CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
                if (app.valid())
                {
                    app->stop();
                }
            }

            else
            {
                QObject* vizStyleObject = _findChild("vizStyle");
                if (vizStyleObject)
                {
                    UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
                    std::string appdataPath = temporaryInstance->getAppFolderPath();

                    QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");
                    if (settingFile.open(QIODevice::ReadOnly))
                    {
                        QByteArray settingData = settingFile.readAll();

                        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

                        QJsonObject settingObject = settingDoc.object();
                        QString uiScale = settingObject["UIScale"].toString();

                        if (!uiScale.isEmpty())
                        {
                            bool oky = true;
                            double uiScaleVal = uiScale.toDouble(&oky);
                            if (oky)
                            {
                                vizStyleObject->setProperty("uiScale", QVariant::fromValue(uiScaleVal));
                            }


                        }
                    }
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }


    bool LoginGUI::_isPresentInLicenseFile(std::string moduleName)
    {
        VENDORCODE code;
        
        //! Handle
        LM_HANDLE* _lmHandle;

        if (lc_new_job(NULL, lc_new_job_arg2, &code, &_lmHandle))
        {
            LOG_ERROR("Error while loading library");
        }

        // "../../Licenses"
        std::string licensePath = LICENSE_CHECK_PATH ;
        lc_set_attr(_lmHandle, LM_A_LICENSE_DEFAULT, (LM_A_VAL_TYPE)licensePath.c_str());

        if (lc_checkout(_lmHandle, (LM_CHAR_PTR)moduleName.c_str(), "1.0", 1, LM_CO_NOWAIT, &code, LM_DUP_NONE))
        {
            LOG_ERROR("License not found or invalid license for - " + moduleName);
            return false;
        }

        return true;
    }

    void LoginGUI::_checkLicenses()
    {
        if (_isPresentInLicenseFile("GEORBIS_DESK_VU"))
        {
            _modulesAvailableInLicense.push_back("georbIS_Vu");
            _isVuPresent = true;
        }

        if (_isPresentInLicenseFile("GEORBIS_DESK_PRO"))
        {
            _modulesAvailableInLicense.push_back("georbIS_Pro");
        }

        if (_isPresentInLicenseFile("GEORBIS_DESK_STRATEGEM"))
        {
            _modulesAvailableInLicense.push_back("georbIS_Strategem");
        }

        if (_isPresentInLicenseFile("GEORBIS_DESK_INTEL"))
        {
            _modulesAvailableInLicense.push_back("georbIS_Intel");
        }

        if (_isPresentInLicenseFile("GEORBIS_DESK_FOOTPRINT"))
        {
            _modulesAvailableInLicense.push_back("georbIS_Footprint");
        }

        if (_isPresentInLicenseFile("GEORBIS_DESK_ANALYST"))
        {
            _modulesAvailableInLicense.push_back("georbIS_Analyst");
        }

        if (_isPresentInLicenseFile("GEORBIS_DESK_PROCESSOR"))
        {
            _modulesAvailableInLicense.push_back("georbIS_Processor");
        }
    }

    void LoginGUI::checkPassword(QString userName, QString password)
    {
        // Check for userName and password

        bool result = false;

        OS::Logon logon(userName.toStdString().c_str() , password.toStdString().c_str() );

        bool accessLevel_analysis = true;
        bool accessLevel_planning = true;
        bool accessLevel_add_data = true;
        bool accessLevel_adminUser = true;

#ifdef _DEBUG
        if(userName.isEmpty() && password.isEmpty())
        {
            result = true;
        }
#endif

        if (logon.TryToLogon() && _isVuPresent)
        {
            //! Connect to DB
            CORE::RefPtr<DATABASE::IDatabase> database = DATABASE::GetDatabaseFromMaintainer(_authenticationDB);

            result = true;

            if(database.valid())
            {
                //! Create database table is it doesn't exist
                _createTable(PermissionsTableName, database);

                //! Get the authentication info from database
                std::stringstream queryStatement;
                queryStatement << "SELECT * FROM " << PermissionsTableName 
                    << " WHERE " << PermissionsTable_Username << " = '" << userName.toStdString() <<"' ";

                CORE::RefPtr<DATABASE::ISQLQueryResult> result = DATABASE::ExecuteSQLQuery(queryStatement.str(), database);
                if(result.valid())
                {
                    CORE::RefPtr<DATABASE::IDBTable> table = result->getResultTable();
                    if(table.valid())
                    {
                        //! Take only the first record
                        CORE::RefPtr<DATABASE::IDBRecord> record = table->getNextRecord();
                        if(record.valid())
                        {
                            DATABASE::IDBField* usernameField = record->getFieldByName(PermissionsTable_Username);
                            DATABASE::IDBField* analysisField = record->getFieldByName(PermissionsTable_Analysis);
                            DATABASE::IDBField* planningField = record->getFieldByName(PermissionsTable_Planning);
                            DATABASE::IDBField* add_dataField = record->getFieldByName(PermissionsTable_Add_Data);
                            DATABASE::IDBField* adminUserField = record->getFieldByName(PermissionsTable_AdminUser);

                            accessLevel_analysis = UTIL::ToType<bool>(analysisField->toString());
                            accessLevel_planning = UTIL::ToType<bool>(planningField->toString());
                            accessLevel_add_data = UTIL::ToType<bool>(add_dataField->toString());
                            accessLevel_adminUser= UTIL::ToType<bool>(adminUserField->toString());
                        }
                    }
                }
                else
                {
                    //! Create the user entry in database with default values,.,.
                    std::stringstream queryStatement1;
                    queryStatement1 << "INSERT INTO " << PermissionsTableName << " (" 
                        << PermissionsTable_Username <<", "
                        << PermissionsTable_Analysis << ", "
                        << PermissionsTable_Planning << ", "
                        << PermissionsTable_Add_Data << ", "
                        << PermissionsTable_AdminUser << " ) "
                        <<" VALUES ( " 
                        <<"'" << userName.toStdString() << "', "                    //! Username
                        << "'1', "                                                  //! Analysis
                        << "'1', "                                                  //! Planning
                        << "'1', "                                                  //! Add Data
                        << "'0' )";                                                 //! AdminUser

                    DATABASE::ExecuteSQLQuery(queryStatement1.str(), database);
                }
            }
        }
        
        if(result)
        {
            _currentUserName = userName.toStdString();
            QObject* startMenu =_findChild("startMenu");
            if(startMenu != NULL)
            {
                if(userName == "")                      //! A little hack in case blank username password is enabled. Only for testing...
                    userName = "VizExperts";
                startMenu->setProperty("name", userName);
            }

            QObject* loginScreen = _findChild("loginScreen");
            if (loginScreen != NULL)
            {
                //! Set the user authentication level,.,.,
                CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
                if (item)
                {
                    item->setProperty("user_type", QVariant::fromValue(1));

                    QObject* licenseManager = _findChild("licenseManager");
                    if (licenseManager)
                    {
                        //enabling the modes whose licenses are available in license file
                        for (int i = 0; i < _modulesAvailableInLicense.size(); i++)
                        {
                            std::string mode = _modulesAvailableInLicense[i];
                            licenseManager->setProperty(mode.c_str(), true);
                        }
                    }
                }
            }

            //! logging in
            if(loginScreen != NULL)
            {
                QMetaObject::invokeMethod(loginScreen, "login", Q_ARG(QVariant, QVariant(true)));
            }
        }
        else
        {
            QObject* loginScreen = _findChild("loginScreen");
            if(loginScreen != NULL)
            {
                QMetaObject::invokeMethod(loginScreen, "login", Q_ARG(QVariant, QVariant(false)));
                std::string status = "Invalid Username or Password";

                //checking whether login failed was due to license or invalid credentials
                if (!_isVuPresent)
                {
                    status = "Invalid License";
                    _modulesAvailableInLicense.clear();
                }

                //updating onto the login screen
                loginScreen->setProperty("status", QString::fromStdString(status));
            }
        }
    }

    void LoginGUI::checkLocalProject(bool value)
    {
        if (value)
        {
            QString ip = "localhost";
            saveIPAddress(ip);
        }
        
    }

    void LoginGUI::saveIPAddress(QString ip)
    {
        std::string ipAddress = ip.toStdString();
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();

        CORE::RefPtr<CORE::IComponent> settingComponent = worldMaintainer->getComponentByName("SettingComponent");

        if (settingComponent.valid())
        {
            CORE::RefPtr<ELEMENTS::ISettingComponent> _globalSettingComponent = settingComponent->getInterface<ELEMENTS::ISettingComponent>();
            std::string settingConfigFile = ELEMENTS::ISettingComponent::SETTINGCONFIGFILE;
                
                DB::BaseWrapper *wrapper = DB::BaseWrapperManager::instance()->findWrapper("SettingComponent");
                if (wrapper)
                {
                    CORE::IBase* base = NULL;
                    if (settingComponent)
                    {
                        base = settingComponent->getInterface<CORE::IBase>();
                        if (!osgDB::fileExists(settingConfigFile))
                        {
                            std::stringstream inputstream;
                            CORE::RefPtr<DB::OutputIterator> oi = new AsciiOutputIterator(&inputstream);

                            DB::OutputStream os(NULL);
                            os.start(oi.get(), DB::OutputStream::WRITE_ATTRIBUTE);
                            wrapper->write(os, *base);

                            std::ofstream filePtr;
                            filePtr.open(settingConfigFile.c_str(), std::ios::out);
                            std::string dataToWrite = inputstream.str();
                            filePtr << dataToWrite.c_str();
                            filePtr.close();
                        }
                        //This is for reading
                        std::stringstream outputStream;

                        outputStream << std::ifstream(settingConfigFile.c_str(), std::ios::in).rdbuf();
                        std::string stringWritten = outputStream.str();

                        DB::InputIterator* ii = new AsciiInputIterator(&outputStream);

                        DB::InputStream is(NULL);
                        is.start(ii, true);
                        std::string settings;

                        is >> settings >> DB::VIZ_BEGIN_BRACKET;
                        std::string groupName;
                        bool foundGroup = false;
                        std::string temp;
                        is >> temp;
                        is >> groupName;

                        std::string projectGroup = ELEMENTS::ISettingComponent::PROJECTGROUPNAME;
                        std::string projectType = ELEMENTS::ISettingComponent::PROJECTTYPE;
                        std::string webServerGroup = ELEMENTS::ISettingComponent::WEBSERVERGROUPNAME;
                        std::string layerDatabseGroup = ELEMENTS::ISettingComponent::LAYERDATABASEGROUPNAME;
                        std::string localDataGroup = ELEMENTS::ISettingComponent::LOCALDATAGROUPNAME;

                        do
                        {
                            if (groupName == projectType)
                            {
                                std::map<std::string, std::string> attributeMap = readSettings(is);
                                std::map<std::string, std::string>::iterator itr;
                                for (itr = attributeMap.begin(); itr != attributeMap.end(); ++itr)
                                {
                                    if (itr->first == "isLocal")
                                    {
                                        if (ipAddress == "localhost")
                                        itr->second = "true";
                                        else
                                        itr->second = "false";

                                    }
                                }
                                _globalSettingComponent->setProjectType(attributeMap);
                                foundGroup = true;
                            }
                            else if (groupName == projectGroup)
                            {
                                std::map<std::string, std::string> attributeMap = readSettings(is);
                                std::map<std::string, std::string>::iterator itr;
                                for (itr = attributeMap.begin(); itr != attributeMap.end(); ++itr)
                                {
                                    if (itr->first == "Host")
                                    {
                                        itr->second = ipAddress;
                                    }
                                }
                                _globalSettingComponent->setProjectSetting(attributeMap);
                                foundGroup = true;
                            }
                            else if (groupName == webServerGroup)
                            {
                                std::map<std::string, std::string> attributeMap = readSettings(is);
                                std::map<std::string, std::string>::iterator itr;
                                for (itr = attributeMap.begin(); itr != attributeMap.end(); ++itr)
                                {
                                    if (itr->first == "Server")
                                    {
                                        itr->second = ipAddress;
                                    }
                                }
                                _globalSettingComponent->setWebServerSetting(attributeMap);
                                foundGroup = true;
                            }
                            else if (groupName == layerDatabseGroup)
                            {
                                std::map<std::string, std::string> attributeMap = readSettings(is);
                                std::map<std::string, std::string>::iterator itr;
                                for (itr = attributeMap.begin(); itr != attributeMap.end(); ++itr)
                                {
                                    if (itr->first == "Host")
                                    {
                                        itr->second = ipAddress;
                                    }
                                }
                                _globalSettingComponent->setLayerDatabaseSetting(attributeMap);
                                foundGroup = true;
                            }
                            else
                            {
                                foundGroup = false;
                                std::stringstream inputstream;
                                CORE::RefPtr<DB::OutputIterator> oi = new AsciiOutputIterator(&inputstream);

                                DB::OutputStream os(NULL);
                                os.start(oi.get(), DB::OutputStream::WRITE_ATTRIBUTE);
                                wrapper->write(os, *base);

                                std::ofstream filePtr;
                                filePtr.open(settingConfigFile.c_str(), std::ios::out);
                                std::string dataToWrite = inputstream.str();
                                filePtr << dataToWrite.c_str();
                                filePtr.close();
                            }
                            is >> groupName;
                        } while (foundGroup);
                        emit showError("Project Setting", "Please Restart The Application");

                        is.advanceToCurrentEndBracket();

                    }
                }
               
        }

    }
    std::map<std::string, std::string> readSettings(DB::InputStream& is)
    {
        int attributeNumber;
        is >> attributeNumber >> DB::VIZ_BEGIN_BRACKET;

        std::map<std::string, std::string> attributeMap;

        int counter = 0;
        while (counter < attributeNumber)
        {
            std::string parameter;
            std::string parameterValue;

            is >> parameter;
            is.readWrappedString(parameterValue);

            attributeMap[parameter] = parameterValue;

            counter++;
        }
        is >> DB::VIZ_END_BRACKET;

        return attributeMap;
    }

}   // namespace SMCQt