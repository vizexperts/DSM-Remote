/****************************************************************************
*
* File             : AddServerLayerGUI.h
* Description      : AddServerLayerGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/AddServerLayerGUI.h>

#include <QComboBox>
#include <QLineEdit>
#include <QTreeWidget>
#include <QDateTimeEdit>
#include <QValidator>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <Core/WorldMaintainer.h>

#include <VizQt/QtUtils.h>

#include <Core/IObjectFactory.h>
#include <Core/IWorld.h>
#include <Core/CoreRegistry.h>

#include <Terrain/TerrainPlugin.h>
#include <Terrain/IWMSRasterLayer.h>

#include <osgEarthUtil/WMS>
#include <SMCQt/QMLTreeModel.h>

#include <Util/FileUtils.h>
#include <SMCQt/VizComboBoxElement.h>
#include <QJSONDocument>
#include <QJSONObject>
#include <osgEarthUtil/WFS>
#include <Terrain/IModelObject.h>
#include <osgEarthDrivers/feature_wfs/WFSFeatureOptions>
#include <DB/ReaderWriter.h>
#include <DB/ReadFile.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, AddServerLayerGUI);
    DEFINE_IREFERENCED(AddServerLayerGUI,DeclarativeFileGUI);

    //XXX --> UI for WMS and WFS are different because of Poup Up property 
    //that we cannot set variable at the time of popup load 
    //to decide whether the current service is for WMS or WFS

    const std::string AddServerLayerGUI::AddWMSLayerPopup = "addWMSLayerPopUp";
    const std::string AddServerLayerGUI::AddWFSLayerPopup = "addWFSLayerPopUp";
    const std::string AddServerLayerGUI::GeorbisServerlistDir = "/GeorbISServerList";

    AddServerLayerGUI::AddServerLayerGUI()
        : _childItem(NULL)
        , _wmsTreeModel(NULL)
        , _serverAddress("")
        , _layerName("")
        , _noFurtherUpdate(false)
    {
    }

    AddServerLayerGUI::~AddServerLayerGUI()
    {
    }

    void AddServerLayerGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader != NULL)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopUpLoader(QString)), Qt::UniqueConnection);
        }
    }

    void AddServerLayerGUI::connectPopUpLoader(QString type)
    {
        _readAllServers();

        //populate the server list
        int count = 0;
        QList<QObject*> comboBoxList;



        for (int i = 0; i < _georbISServerList.size(); i++)
        {
            std::string serverName = _georbISServerList[i];
            comboBoxList.append(new VizComboBoxElement(serverName.c_str(), UTIL::ToString(count).c_str()));
        }

        comboBoxList.append(new VizComboBoxElement("Custom", UTIL::ToString(count).c_str()));

        _setContextProperty("georbISServerList", QVariant::fromValue(comboBoxList));

        QObject* wmsObject = _findChild(AddWMSLayerPopup);
        if (wmsObject != NULL)
        {

            QObject::connect(wmsObject, SIGNAL(addButtonClicked()), this, SLOT(_handleAddButtonClicked()), Qt::UniqueConnection);
            QObject::connect(wmsObject, SIGNAL(connectButtonClicked()), this, SLOT(_handleConnectButtonClicked()), Qt::UniqueConnection);
            QObject::connect(wmsObject, SIGNAL(enteredByUser(QString)), this, SLOT(_layerNameAddedByUser(QString)), Qt::UniqueConnection);

        }
        else{
            QObject* wfsObject = _findChild(AddWFSLayerPopup);
            if (wfsObject){

                QObject::connect(wfsObject, SIGNAL(addButtonClicked()), this, SLOT(_handleAddButtonClicked()), Qt::UniqueConnection);
                QObject::connect(wfsObject, SIGNAL(wfsConnectButtonClicked()), this, SLOT(_handleConnectButtonClicked()), Qt::UniqueConnection);
                QObject::connect(wfsObject, SIGNAL(enteredByUser(QString)), this, SLOT(_layerNameAddedByUser(QString)), Qt::UniqueConnection);

            }
        }

        QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(_updateLayerName()));
        _timer.start(10);

        _wmsTreeModel = new WmsTreeModel();
        _childItem = new WmsTreeModelItem();

    }

    //internal function to read from georbISServers and populate the combobox list
    void AddServerLayerGUI::_readAllServers()
    {
        _georbISServerList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string georbISServerFolder = appdataPath + GeorbisServerlistDir;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _georbISServerList.push_back(fileNameWithoutExtension);
        }
    }

    void AddServerLayerGUI::_updateLayerName()
    {
        if (!_noFurtherUpdate)
        {
            std::string layerNames;
            _layerName = "";

            _getLayerNames(layerNames, _childItem);

            QObject* wmsObject = _findChild(AddWMSLayerPopup);
            if (wmsObject)
            {
                wmsObject->setProperty("layerName", QString::fromStdString(_layerName));
            }

            QObject* wfsObject = _findChild(AddWFSLayerPopup);
            if (wfsObject)
            {
                wfsObject->setProperty("layerName", QString::fromStdString(_layerName));
            }
        }
    }

    void AddServerLayerGUI::_layerNameAddedByUser(QString layerName)
    {
        QObject* wmsObject = _findChild(AddWMSLayerPopup);
        if (wmsObject)
        {
            _layerName = layerName.toStdString();
            wmsObject->setProperty("layerName", QString::fromStdString(_layerName));
            _noFurtherUpdate = true;
        }
        QObject* wfsObject = _findChild(AddWFSLayerPopup);
        if (wfsObject)
        {
            _layerName = layerName.toStdString();
            wfsObject->setProperty("layerName", QString::fromStdString(_layerName));
            _noFurtherUpdate = true;
        }
    }

    std::string AddServerLayerGUI::_getWMSPath(std::string serverName)
    {

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string databaseFolder = appdataPath + GeorbisServerlistDir;

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting", "ServerList folder not found", true);
            //making database to be invalid that is handled in parent function
           
            return "";
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);
        std::string serverExtension;
        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == serverName)
            {
                found = true;
                serverExtension = osgDB::getLowerCaseFileExtension(currFileName);
            }
        }

        if (!found)
        {
            return "";
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + GeorbisServerlistDir + "//" + serverName + "." + serverExtension));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                return "";
            }

            QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
            if (jdoc.isNull())
            {
               
                return "";
            }

            QJsonObject obj = jdoc.object();
            return obj["Host"].toString().toStdString();
        }
    }

    void AddServerLayerGUI::_handleAddButtonClicked()
    {
        std::string layerName;
        QObject* wmsObject = _findChild(AddWMSLayerPopup);
        QObject* wfsObject = _findChild(AddWFSLayerPopup);
       
        std::string layerNames;

        _getLayerNames(layerNames, _childItem);

        if (wmsObject)
        {
            layerName = wmsObject->property("layerName").toString().toStdString();
        }

        if(layerNames.empty())
        {
            showError("Select Layer", "Please select atleast 1 layer to show");
            return;
        }
        else if (wmsObject){
            CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
            CORE::RefPtr<CORE::IObject> object = objectFactory->createObject(*TERRAIN::TerrainRegistryPlugin::WMSRasterLayerType);

            CORE::RefPtr<TERRAIN::IWMSRasterLayer> wmsLayer = object->getInterface<TERRAIN::IWMSRasterLayer>();
            wmsLayer->setURL(_serverAddress);
            wmsLayer->setWMSLayers(layerNames);

            CORE::RefPtr<CORE::IBase> base = object->getInterface<CORE::IBase>();

            base->setName(layerName);

            CORE::RefPtr<CORE::IWorld> world =
                APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());

            if (world.valid())
            {
                world->addObject(object.get());
            }
        }
        else if (wfsObject){
            QString layString = QString::fromStdString(layerNames);
            QStringList resLayers = layString.split(",");

            int numLayers = resLayers.size();

            int index = 0;
            while (index < numLayers){

                std::string currLayerName = resLayers.at(index).toStdString();

                CORE::RefPtr<DB::ReaderWriter::Options>  options = new DB::ReaderWriter::Options;
                options->setMapValue("Layer", currLayerName);
                options->setMapValue("LayerName", currLayerName);
                options->setMapValue("WFSFormat", "application/json");
                options->setMapValue("Symbology", "1");
                std::string wfsURL = _serverAddress+".wfs";
                CORE::RefPtr<CORE::IObject> modelObject = DB::readFeature(wfsURL, options.get());
/*
                CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
                CORE::RefPtr<CORE::IObject> object = objectFactory->createObject(*TERRAIN::TerrainRegistryPlugin::ModelObjectType);

                CORE::RefPtr<TERRAIN::IModelObject> wfsLayer = object->getInterface<TERRAIN::IModelObject>();
                wfsLayer->setUrl(_serverAddress);

                osgEarth::Drivers::WFSFeatureOptions _optionsWFS;
                _optionsWFS.url() = _serverAddress;
                _optionsWFS.typeName() = currLayerName;
                _optionsWFS.outputFormat() = "json";

                wfsLayer->setWFSOptions(_optionsWFS);

                CORE::RefPtr<CORE::IBase> base = object->getInterface<CORE::IBase>();

                base->setName(currLayerName);*/

                CORE::RefPtr<CORE::IWorld> world =
                    APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());

                if (world.valid())
                {
                    world->addObject(modelObject.get());
                }
                index++;
            }
        }
        /*CORE::RefPtr<CORE::IWorld> world = 
            APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());

        if(world.valid())
        {
            world->addObject(object.get());
        }*/

        if(wmsObject)
        {
            QMetaObject::invokeMethod(wmsObject, "closePopup");
        }
        if (wfsObject){
            QMetaObject::invokeMethod(wfsObject, "closePopup");
        }
        reset();
    }


    void AddServerLayerGUI::_handleConnectButtonClicked()
    {
        QObject* wmsObject = _findChild(AddWMSLayerPopup);
        QObject* wfsObject = _findChild(AddWFSLayerPopup);
        if(wmsObject)
        {
            std::string serverAddress;
            if (wmsObject->property("value").toString().toStdString() == "Custom")
            {
                serverAddress = wmsObject->property("serverAddress").toString().toStdString();
                if(serverAddress.empty())
                {
                    showError("Server Address", "Please give a non-empty Server Address.");
                    return;
                }
            }

            else
            {
                std::string serverName = wmsObject->property("value").toString().toStdString();
                serverAddress = _getWMSPath(serverName);
            }

            std::string server = serverAddress;

            //create the capability url
            std::string capabilities = server + "?service=wms&version=1.1.1&request=GetCapabilities";

            //query the capabilty url
            osg::ref_ptr<osgEarth::Util::WMSCapabilities> wmsCapabilities = osgEarth::Util::WMSCapabilitiesReader::read(capabilities, NULL);

            if(!wmsCapabilities.valid())
            {
                showError("Error", "Error in connecting to the Server. \nPlease check if the server address is correct and server is online.");
                return;
            }

            const osgEarth::Util::WMSLayer::LayerList& layerList =  wmsCapabilities->getLayers();
            _childItem->setName("Layers");
            _childItem->setCheckable(false);
            _populate(layerList,_childItem);
            _wmsTreeModel->addItem(_childItem);

            CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            if(qtapp->getRootContext())
            {
                qtapp->getRootContext()->setContextProperty("wmsTreeModel",_wmsTreeModel);
            }

            _serverAddress = server;
        }
        else{
            if (wfsObject)
            {
                std::string serverAddress;
                if (wfsObject->property("value").toString().toStdString() == "Custom")
                {
                    serverAddress = wfsObject->property("serverAddress").toString().toStdString();
                    if (serverAddress.empty())
                    {
                        showError("Server Address", "Please give a non-empty Server Address.");
                        return;
                    }
                }

                else
                {
                    std::string serverName = wfsObject->property("value").toString().toStdString();
                    serverAddress = _getWMSPath(serverName);
                }

                std::string server = serverAddress;

                //create the capability url
                std::string capabilities = server + "?service=WFS&version=1.0.0&request=GetCapabilities";

                //query the capabilty url
                osg::ref_ptr<osgEarth::Util::WFSCapabilities> wfsCapabilities = osgEarth::Util::WFSCapabilitiesReader::read(capabilities, NULL);

                if (!wfsCapabilities.valid())
                {
                    showError("Error", "Error in connecting to the Server. \nPlease check if the server address is correct and server is online.");
                    return;
                }


                const std::vector<osg::ref_ptr<osgEarth::Util::WFSFeatureType>> featureTypes = wfsCapabilities->getFeatureTypes();
                _childItem->setName("Layers");
                _childItem->setCheckable(false);
                _populate(featureTypes, _childItem);
                _wmsTreeModel->addItem(_childItem);

                CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                if (qtapp->getRootContext())
                {
                    qtapp->getRootContext()->setContextProperty("wfsTreeModel", _wmsTreeModel);
                }

                _serverAddress = server;
            }
        }

    }

    void AddServerLayerGUI::_removeChildItem(WmsTreeModelItem* item)
    {
        if(item)
        {
            foreach(QMLTreeModelItem* childItem, item->getChildren())
            {
                WmsTreeModelItem* child = dynamic_cast<WmsTreeModelItem*>(childItem);
                if(child)
                {
                    _wmsTreeModel->removeItem(child);
                }
            }
        }
        _setContextProperty("wmsTreeModel", NULL);
        _setContextProperty("wfsTreeModel", NULL);
    }


    void AddServerLayerGUI::_getLayerNames(std::string& layers, WmsTreeModelItem* item)
    {
        if(item->getCheckState() == Qt::Checked)
        {
            QString name = item->getName();
            if(!name.isEmpty())
            {
                if(layers.empty())
                {
                    if (name.toStdString() != "Layers")
                    {
                        layers = name.toStdString();
                        _layerName = layers;
                    }
                }
                else
                {
                    layers += "," + name.toStdString();
                }
            }
        }
        foreach(QMLTreeModelItem* childItem, item->getChildren())
        {
            WmsTreeModelItem* child = dynamic_cast<WmsTreeModelItem*>(childItem);
            _getLayerNames(layers,child);
        }

    }

    void AddServerLayerGUI::_populate(const osgEarth::Util::WMSLayer::LayerList& layerList, WmsTreeModelItem* _childItem)
    {

        osgEarth::Util::WMSLayer::LayerList::const_iterator iter = layerList.begin();

        while(iter != layerList.end())
        {
            osg::ref_ptr<osgEarth::Util::WMSLayer> wmsLayer = (*iter).get();
            WmsTreeModelItem* item = new WmsTreeModelItem();
            item->setName(wmsLayer->getName().c_str());
            item->setTitle( wmsLayer->getTitle().c_str());
            item->setAbstraction( wmsLayer->getAbstract().c_str());
            item->setCheckState(Qt::Unchecked);

            _childItem->addItem(item);

            _populate(wmsLayer->getLayers(),item);
            iter++;
        }
    }

    void AddServerLayerGUI::_populate(const std::vector<osg::ref_ptr<osgEarth::Util::WFSFeatureType>>& featureList, WmsTreeModelItem* _childItem)
    {

        std::vector<osg::ref_ptr<osgEarth::Util::WFSFeatureType>>::const_iterator iter = featureList.begin();

        while (iter != featureList.end())
        {
            osg::ref_ptr<osgEarth::Util::WFSFeatureType> wfsLayer = (*iter).get();
            WmsTreeModelItem* item = new WmsTreeModelItem();
            item->setName(wfsLayer->getName().c_str());
            item->setTitle(wfsLayer->getTitle().c_str());
            item->setAbstraction(wfsLayer->getAbstract().c_str());
            item->setCheckState(Qt::Unchecked);

            _childItem->addItem(item);

            //_populate(wfsLayer,item);
            iter++;
        }
    }

    void AddServerLayerGUI::reset()
    {
        QObject* wmsObject = _findChild(AddWMSLayerPopup);
        if(wmsObject)
        {
            wmsObject->setProperty("browseButtonEnabled",QVariant::fromValue(true));
            wmsObject->setProperty("connectButtonEnabled", QVariant::fromValue(true));
            wmsObject->setProperty("addButtonEnabled",QVariant::fromValue(true));
            wmsObject->setProperty("serverAddress","");
            wmsObject->setProperty("layerName","");
            _removeChildItem(_childItem);
            _timer.stop();
            _layerName = "";
            _noFurtherUpdate = false;
        }
        else{
            QObject* wfsObject = _findChild(AddWFSLayerPopup);
            if (wfsObject)
            {
                wfsObject->setProperty("browseButtonEnabled", QVariant::fromValue(true));
                wfsObject->setProperty("connectButtonEnabled", QVariant::fromValue(true));
                wfsObject->setProperty("addButtonEnabled", QVariant::fromValue(true));
                wfsObject->setProperty("serverAddress", "");
                wfsObject->setProperty("layerName", "");
                _removeChildItem(_childItem);
                _timer.stop();
                _layerName = "";
                _noFurtherUpdate = false;
            }
        }
    }

} // namespace SMCQt

