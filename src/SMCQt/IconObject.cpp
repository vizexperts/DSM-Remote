#include <SMCQt/IconObject.h>

namespace SMCQt
{
    IconObject::IconObject(QObject* parent) : QObject(parent)
    {

    }

    IconObject::IconObject(const QString &name, const QString &path, QObject *parent)
        : QObject(parent), m_name(name), m_path(path)
    {

    }

    IconObject::IconObject(const QString &name, const QString &path, bool audioPresent ,bool micEnable,QObject *parent)
        : QObject(parent), m_name(name), m_path(path), m_audioPresent(audioPresent), m_micEnable(micEnable)
    {
       
    }

    QString IconObject::name() const
    {
        return m_name;
    }

    void IconObject::setName(const QString &name)
    {
        if(name != m_name)
        {
            m_name = name;
            emit nameChanged();
        }
    }

    QString IconObject::path() const
    {
        return m_path;
    }

    void IconObject::setPath(const QString& path)
    {
        if(path != m_path)
        {
            m_path = path;
            emit pathChanged();
        }
    }

    // returns the m_audioPresent value
    bool IconObject::audioPresent() const
    {
        return m_audioPresent;
    }

    // sets the m_audioPresent value
    void IconObject::setAudioPresent(bool present)
    {
        if (present != m_audioPresent)
        {
            m_audioPresent = present;
            emit audioPresentChanged();
        }
    }

    // returns the m_micEnable value 
    bool IconObject::micEnable() const
    {
        return m_micEnable;
    }

    // sets the m_micEnable value 
    void IconObject::setMicEnable(bool micEnable)
    {
        if (micEnable != m_micEnable)
        {
            m_micEnable = micEnable;
            emit micEnableChanged();
        }
    }
}
