/****************************************************************************
*
* File             : FloodingGUI.h
* Description      : FloodingGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/
//#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/FloodingGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>
#include<iostream>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, FloodingGUI);
    DEFINE_IREFERENCED(FloodingGUI,DeclarativeFileGUI);

    const std::string FloodingGUI::FloodingAnalysisPopup = "FloodingAnalysis";

    FloodingGUI::FloodingGUI()
    {}

    FloodingGUI::~FloodingGUI()
    {}

    void FloodingGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(_handleProgressValueChanged(int)),Qt::QueuedConnection);
    }

    void FloodingGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IFloodingUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
                 _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
                    if(FloodingAnalysisObject)
                    {
                        FloodingAnalysisObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        FloodingAnalysisObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        FloodingAnalysisObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        FloodingAnalysisObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
             GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if(progress < 100)
                {
                   emit _setProgressValue(progress);
                }
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                    //check base on Type
                    double VolumeOutput=_uiHandler->getFilledVolume()*0.000000001;
                    QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
                    if(FloodingAnalysisObject)
                    {
                        FloodingAnalysisObject->setProperty("outputFilledVol", QString::number(VolumeOutput));
                    }
                    emit _setProgressValue(100);
            }
            else
            {
                 if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    emit showError("Filter Status", "Failed to apply filter");
                    _handlePBStopClicked();
                }
            }
        }
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            if(!_pointHandler.valid())
            {
                LOG_ERROR("Point UI handler not valid");
            }

            CORE::IPoint* point = _pointHandler->getPoint();

            if(point == NULL)
            {
                LOG_ERROR("Point is not valid");
            }

            CORE::IObject *object = point->getInterface<CORE::IObject>();

            if(object == NULL)
            {
                LOG_ERROR("Point Object is not valid");
            }

            QString pointX, pointY, pointZ;
            pointX = QString::number(point->getX(),'f',6);
            pointY = QString::number(point->getY(),'f',6);
            pointZ = QString::number(point->getZ(),'f',6);
                            // delete previous point 
            _removeObject(_viewerPoint.get());
            _viewerPoint = NULL;
                        // get pointer of new point
            _viewerPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
            _viewerPoint->getInterface<CORE::IBase>()->setName("Breachpoint");
            //set the property of viewPoint position
            QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
            if(FloodingAnalysisObject)
            {
                FloodingAnalysisObject->setProperty("breachPointLongitude",pointX);
                FloodingAnalysisObject->setProperty("breachPointLatitude",pointY);
                FloodingAnalysisObject->setProperty("breachPointAltitude",pointZ);
            }



        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void FloodingGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
                if(FloodingAnalysisObject)
                {
                    // start and stop button handlers
                    connect(FloodingAnalysisObject, SIGNAL(startAnalysis()),this,SLOT(_handlePBStartClicked()),Qt::UniqueConnection);
                    connect(FloodingAnalysisObject, SIGNAL(stopAnalysis()),this, SLOT(_handlePBStopClicked()),Qt::UniqueConnection);
                    connect(FloodingAnalysisObject, SIGNAL(markPosition(bool)),this,SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);
                    connect(FloodingAnalysisObject, SIGNAL(markBreachPointPosition(bool)),this,SLOT(_handlePBMarkBreachAtClicked(bool)),Qt::UniqueConnection);

                    // enable start button
                    FloodingAnalysisObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    FloodingAnalysisObject->setProperty("isStopButtonEnabled",false);

                    //disable progress value
                    FloodingAnalysisObject->setProperty("progressValue",0);
                }
            }
            else
            {
                _reset();
                _handlePBStopClicked();
            }
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }
    void FloodingGUI::_handlePBMarkBreachAtClicked(bool pressed)
    {
        if(pressed)
        {
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
            if(FloodingAnalysisObject)
            {
                FloodingAnalysisObject->setProperty( "isBreachPointSelected" , false );
            }
            _setPointHandlerEnabled(pressed);
        }
    }
    void FloodingGUI::_setPointHandlerEnabled(bool enable)
    {
        if(_pointHandler.valid())
        {
            if(enable)
            {
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);                
            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark trans/recv position");
        }
    }
    void FloodingGUI::_handleProgressValueChanged(int value)
    {
        QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
        if(FloodingAnalysisObject)
        {
            FloodingAnalysisObject->setProperty("progressValue",QVariant::fromValue(value));
        } 
    }

    //! 
    void FloodingGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
        if(FloodingAnalysisObject)
        {
            std::string name = FloodingAnalysisObject->property("outputName").toString().toStdString();

            if(name == "")
            {
                showError( "Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            FloodingAnalysisObject->setProperty("outputName",QString::fromStdString(name));
            _uiHandler->setOutputName(name);
 
            //get water level
            QString QStringWaterLevelVal=FloodingAnalysisObject->property("waterLevelVal").toString();
            if(QStringWaterLevelVal == "")
            {
                showError( "Insufficient / Incorrect data", "Water Level Value is not specified");
                return;
            }
            double WaterLevel = QStringWaterLevelVal.toDouble();;
            _uiHandler->setWaterLevelRise(WaterLevel);
           if( FloodingAnalysisObject->property("breachPointLatitude").toString().toStdString().length()==0||
                FloodingAnalysisObject->property("breachPointLongitude").toString().toStdString().length()==0||
                FloodingAnalysisObject->property("breachPointAltitude").toString().toStdString().length()==0)
            {
                showError("Insufficient data","Breach point parameters are not specified");
                return;
            }

            double x1 = 0.0, y1 = 0.0, z1= 0.0;
            y1 = FloodingAnalysisObject->property("breachPointLatitude").toDouble();
            x1= FloodingAnalysisObject->property("breachPointLongitude").toDouble();
            z1 = FloodingAnalysisObject->property("breachPointAltitude").toDouble();
            osg::Vec3d vpPos(osg::Vec3d (x1,y1,z1));
            _uiHandler->setBreachPoint(vpPos);

            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = FloodingAnalysisObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = FloodingAnalysisObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = FloodingAnalysisObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() =  FloodingAnalysisObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError( "Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
                        showError( "Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                }
            }

            FloodingAnalysisObject->setProperty("progressValue",0); 
            _handlePBAreaClicked(false);
            _handleProgressValueChanged(false);
            _handlePBMarkBreachAtClicked(false);
            // start filter

            _uiHandler->execute();

        }
    }

    void FloodingGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();
        QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
        if(FloodingAnalysisObject)
        {
            //disable stop button
            FloodingAnalysisObject->setProperty("isStopButtonEnabled",false);
            FloodingAnalysisObject->setProperty("isStartButtonEnabled",true);
            FloodingAnalysisObject->setProperty("progressValue",QVariant::fromValue(0));
        } 
    }

    void FloodingGUI::_handlePBRunInBackgroundClicked()
    {}

    void FloodingGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            
            QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
            if(FloodingAnalysisObject)
            {
                FloodingAnalysisObject->setProperty( "isMarkSelected" , false );
            }
        }
    }

    void FloodingGUI::_reset()
    {
        QObject* FloodingAnalysisObject = _findChild(FloodingAnalysisPopup);
        if(FloodingAnalysisObject)
        {
            _removeObject(_viewerPoint.get());
            _viewerPoint = NULL;
            FloodingAnalysisObject->setProperty("isBreachPointSelected",false);
            FloodingAnalysisObject->setProperty("isMarkSelected",false);
            FloodingAnalysisObject->setProperty("bottomLeftLongitude","longitude");
            FloodingAnalysisObject->setProperty("bottomLeftLatitude","latitude");
            FloodingAnalysisObject->setProperty("topRightLongitude","longitude");
            FloodingAnalysisObject->setProperty("topRightLatitude","latitude");
            FloodingAnalysisObject->setProperty("outputName","outputName");
            FloodingAnalysisObject->setProperty("progressValue",0);
            FloodingAnalysisObject->setProperty("breachPointLatitude","latitude");
            FloodingAnalysisObject->setProperty("breachPointLongitude","longitude");
            FloodingAnalysisObject->setProperty("breachPointAltitude","altitude");
            FloodingAnalysisObject->setProperty("waterLevelVal","Water Level");
            _handlePBMarkBreachAtClicked(false);
            _handlePBAreaClicked(false);
        }
    }

}//namespace SMCQt
