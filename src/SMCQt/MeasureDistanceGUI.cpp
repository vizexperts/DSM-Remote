/****************************************************************************
*
* File             : MeasureDistanceGUI.h
* Description      : MeasureDistanceGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <Core/RefPtr.h>
#include <Core/ILine.h>
#include <Core/AttributeTypes.h>
#include <Core/IDeletable.h>
#include <Core/ICompositeObject.h>
#include <Core/WorldMaintainer.h>
#include <Core/IPoint.h>


#include <SMCQt/MeasureDistanceGUI.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Util/StringUtils.h>
#include <Util/UnitsConversionUtils.h>

#include <Elements/IDrawTechnique.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt,MeasureDistanceGUI);

    const std::string MeasureDistanceGUI::DistanceAnalysisObjectName = "distanceAnalysis";

    MeasureDistanceGUI::MeasureDistanceGUI()
        : _line(NULL)
        , _resetFlag(false)
        , _markingEnabled(false)
        , _selectedUnit("m")
    {
    }

    MeasureDistanceGUI::~MeasureDistanceGUI()
    {
    }

    void MeasureDistanceGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(handleDistanceUpdateTimer()));

        QObject::connect(this, SIGNAL(_calculationCompleted()),
            this, SLOT(handleCalculationCompleted()), Qt::QueuedConnection);
    }

    void MeasureDistanceGUI::setActive(bool value)
    {
        if(getActive() == value)
            return;

        if(value)
        {
            QObject* distanceAnalysis = _findChild(DistanceAnalysisObjectName);
            if(distanceAnalysis)
            {

                _timer.start(10);

                QObject::connect(distanceAnalysis, SIGNAL(changeUnit(QString)),
                    this, SLOT(changeUnit(QString)), Qt::UniqueConnection);

                QObject::connect(distanceAnalysis, SIGNAL(mark(bool)),
                    this, SLOT(markLine(bool)), Qt::UniqueConnection);

                QObject::connect(distanceAnalysis, SIGNAL(selectLine(bool)), 
                    this, SLOT(selectLine(bool)), Qt::UniqueConnection);

                QObject::connect(distanceAnalysis, SIGNAL(start()), 
                    this, SLOT(startCalculation()), Qt::UniqueConnection);

                QObject::connect(distanceAnalysis, SIGNAL(stop()), 
                    this, SLOT(stopCalculation()), Qt::UniqueConnection);

                //QObject::connect(distanceAnalysis, SIGNAL(closed()), 
                //    this, SLOT(handleClosed()), Qt::UniqueConnection);
            }
        }
        else
        {
            stopCalculation();
            if(_markingEnabled)
            {
                markLine(false);
            }
            else
            {
                selectLine(false);
            }

            _selectedUnit = "m";

            _timer.stop();

        }

        DeclarativeFileGUI::setActive(value);
    }

    void MeasureDistanceGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {
                _lineHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getGUIManager());

                _distanceCalculationHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IDistanceCalculationUIHandler>(getGUIManager());

                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

                if(component.valid())
                {
                    _selectionComponent = component->getInterface<CORE::ISelectionComponent>();
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the line has been updated
        else if(messageType == *SMCUI::ILineUIHandler::LineUpdatedMessageType)
        {
        }
        // Check whether the distance calculation is completed
        else if(messageType == *SMCUI::IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType)
        {
            _distanceCalculationHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _unsubscribe(_distanceCalculationHandler.get(), 
                *SMCUI::IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType);

            emit _calculationCompleted();

        }
        else if(messageType == *CORE::ISelectionComponent::ObjectClickedMessageType)
        {
            const CORE::ISelectionComponent::SelectionMap& map = 
                _selectionComponent->getCurrentSelection();

            _line = NULL;

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();
            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                CORE::ILine* line = iter->second->getInterface<CORE::ILine>();
                if(line)
                {
                    _line = line;
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void MeasureDistanceGUI::handleCalculationCompleted()
    {
        QObject* distanceAnalysis = _findChild(DistanceAnalysisObjectName);
        if(distanceAnalysis)
        {
            distanceAnalysis->setProperty("calculating", QVariant::fromValue(false));
        }
    }

    void MeasureDistanceGUI::handleDistanceUpdateTimer()
    {
        if(_distanceCalculationHandler.valid())
        {             
            double convertedUnits;

            UTIL::UnitsConversionUtils::convert(UTIL::UnitsConversionUtils::M,
                _selectedUnit.toStdString(), _distanceCalculationHandler->getDistance(), convertedUnits, 1);

            QString unit = QString::number( convertedUnits, 'f', 4 );

            QObject* distanceAnalysis = _findChild(DistanceAnalysisObjectName);
            if(distanceAnalysis)
            {
                if(!_resetFlag)
                {
                    distanceAnalysis->setProperty("value", QVariant::fromValue(unit));
                }
                else 
                {
                    distanceAnalysis->setProperty("value", QVariant::fromValue(0.0));
                }
            }
        }   

    }

    void MeasureDistanceGUI::markLine(bool value)
    {
        if(value)
        {
            if(_lineHandler.valid())
            {
                _subscribe(_lineHandler.get(), *SMCUI::ILineUIHandler::LineUpdatedMessageType);
                bool temporaryState = _lineHandler->getTemporaryState();
                _lineHandler->setTemporaryState(true);
                _lineHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE);
                _lineHandler->setColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
                _lineHandler->setWidth(3.0);
                _line = _lineHandler->getCurrentLine();
                _lineHandler->setTemporaryState(temporaryState);
                _lineHandler->_setProcessMouseEvents(true);
            }
            //CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = _line->getInterface<ELEMENTS::IDrawTechnique>();
            //if(draw.valid())
            //{
            //    draw->setHeightOffset(1.0);
            //    draw->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);
            //}
        }
        else
        {
            _unsubscribe(_lineHandler.get(), *SMCUI::ILineUIHandler::LineUpdatedMessageType);
            _lineHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);


            //remove the previously added point
            if(_line.valid())
            {
                _removeObject(_line.get());
                _line = NULL;
            }

            _resetFlag = true;

        }
        _markingEnabled = value;
    }

    void MeasureDistanceGUI::changeUnit(QString unit)
    {
        if(_selectedUnit != unit)
        {
            _selectedUnit = unit;
        }
    }

    void MeasureDistanceGUI::selectLine(bool value)
    {
        if(!_selectionComponent.valid())
        {
            return;
        }

        _selectionComponent->clearCurrentSelection();

        if(value)
        {
            // set selection state to intermediate selection
            // subscribe to object clicked message

            _selectionComponent->setIntermediateSelection(true);
            _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
        }
        else
        {
            _selectionComponent->setIntermediateSelection(false);
            _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ObjectClickedMessageType);
            _line = NULL;
            _resetFlag = true;
        }

    }

    void MeasureDistanceGUI::startCalculation()
    {
        if(_lineHandler.valid())
        {
            if(!_line.valid())
            {
                emit showError("Measure Distance", "No Line Drawn", false);
                return;
            }


            CORE::RefPtr<CORE::ILine> line = _line;

            if(line->getPointNumber() < 2)
            {
                emit showError("Measure Distance", "No Line Drawn", false);
                return;
            }

            _lineHandler->_setProcessMouseEvents(false);

            // set the computation type
            bool projected = false;
            QObject* distanceAnalysis = _findChild(DistanceAnalysisObjectName);
            if(distanceAnalysis)
            {
                projected = distanceAnalysis->property("projected").toBool();
            }

            if(projected)
            {
                _distanceCalculationHandler->setComputationType(SMCUI::IDistanceCalculationUIHandler::PROJECTED);
            }
            else
            {
                _distanceCalculationHandler->setComputationType(SMCUI::IDistanceCalculationUIHandler::AERIAL);
            }

            // Subscribe to distance calculation message
            if(_distanceCalculationHandler.valid())
            {
                _subscribe(_distanceCalculationHandler.get(), 
                    *SMCUI::IDistanceCalculationUIHandler::DistanceCalculationCompletedMessageType);

                _distanceCalculationHandler->getInterface<APP::IUIHandler>()->setFocus(true);

                // Pass the polygon points to the distance calculation handler
                osg::ref_ptr<osg::Vec3dArray> arr (new osg::Vec3dArray);
                for(unsigned int i=0; i < line->getPointNumber(); i++)
                {
                    arr->push_back(osg::Vec3d(line->getPoint(i)->getValue()));
                }

                _distanceCalculationHandler->setPoints(arr.get());

                if(distanceAnalysis)
                {
                    distanceAnalysis->setProperty("calculating", QVariant::fromValue(true));
                }

                _distanceCalculationHandler->execute();       
                _resetFlag = false;
            }
        }
    }

    void MeasureDistanceGUI::stopCalculation()
    {
        if(_distanceCalculationHandler.valid())
        {
            _distanceCalculationHandler->stop();
        }
        else
        {
            LOG_ERROR("Distance calculation filter not valid");
        }
    }

    void MeasureDistanceGUI::_reset()
    {
        _resetFlag = true;
    }

} // namespace SMCQt

