#include <App/AccessElementUtils.h>
#include <SMCUI/IAddContentUIHandler.h>

#include <SMCQt/RoadNetworkGUI.h>
#include <SMCQt/VizComboBoxElement.h>
#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <Util/FileUtils.h>
#include <QJSONDocument>
#include <QJSONObject>

#include <Core/IMessage.h>
#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITemporary.h>
#include <Core/IFeatureLayer.h>

#include <VizUI/ISelectionUIHandler.h>
#include <GIS/IFeatureLayer.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouseMessage.h>
#include <VizUI/IMouse.h>

#include <Elements/ElementsUtils.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IRepresentation.h>
#include <Elements/IClamper.h>
#include <Elements/IDrawTechnique.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, RoadNetworkGUI);
    DEFINE_IREFERENCED(RoadNetworkGUI, DeclarativeFileGUI);

    RoadNetworkGUI::RoadNetworkGUI()
        : _guiState(NONE), _databaseName("Select Database Instance")
    {
        _startPoint = NULL;
        _endPoint = NULL;
        _featureLayer = NULL;
        _visitor = NULL;
    }

    RoadNetworkGUI::~RoadNetworkGUI()
    {}

    void RoadNetworkGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader != NULL)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, 
                SLOT(connectPopup(QString)), Qt::UniqueConnection);
        }
    }

    void RoadNetworkGUI::connectPopup(QString type)
    {
        if(type == "roadNetworkPopup")
        {
            _guiState = NONE;

            QObject* roadNetworkPopup = _findChild("roadNetworkPopup");
            if(roadNetworkPopup)
            {
                _populateDatabaseList();
                _populateStyleList();

                QObject::connect(roadNetworkPopup, SIGNAL(closed()), this, SLOT(popupClosed()), Qt::UniqueConnection);
                QObject::connect(roadNetworkPopup, SIGNAL(markStart(bool)), this, SLOT(markStart(bool)), Qt::UniqueConnection);
                QObject::connect(roadNetworkPopup, SIGNAL(markEnd(bool)), this, SLOT(markEnd(bool)), Qt::UniqueConnection);
                QObject::connect(roadNetworkPopup, SIGNAL(selectDatabase(QString)), this, SLOT(setDatabase(QString)), Qt::UniqueConnection);
                QObject::connect(roadNetworkPopup, SIGNAL(setStyle(QString)), this, SLOT(setStyle(QString)), Qt::UniqueConnection);
                QObject::connect(roadNetworkPopup, SIGNAL(compute()), this, SLOT(compute()), Qt::UniqueConnection);
            }

        }
     
    }

    void RoadNetworkGUI::setDatabase(QString value)
    {
        _databaseName = value.toStdString();
    }

    void RoadNetworkGUI::setStyle(QString value)
    {
        if (value.toStdString() != "Select Style")
        {
            _styleJsonPath = _styleFolderPath + "/" + value.toStdString() + ".json";
        }
    }

    void RoadNetworkGUI::_populateDatabaseList()
    {
        _readAllDatabaseInstanceName();

        int count = 1;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select Database Instance", "0"));
       

        for (int i = 0; i < _databaseInstanceList.size(); i++)
        {
            std::string databaseName = _databaseInstanceList[i];
            comboBoxList.append(new VizComboBoxElement(databaseName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("databaseList", QVariant::fromValue(comboBoxList));
    }

    void RoadNetworkGUI::_readAllDatabaseInstanceName()
    {
        _databaseInstanceList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string databaseFolder = appdataPath + "/Database";

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            if (osgDB::getLowerCaseFileExtension(currFileName) == "pgres")
            {
                std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

                _databaseInstanceList.push_back(fileNameWithoutExtension);
            }
        }
    }

    void RoadNetworkGUI::_populateStyleList()
    {
        _readAllStyle();

        int count = 1;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select Style", "0"));

        if (_styleList.size() == 0)
        {
            _setContextProperty("styleList", QVariant::fromValue(comboBoxList));
            return;
        }

        for (int i = 0; i < _styleList.size(); i++)
        {
            std::string databaseName = _styleList[i];
            comboBoxList.append(new VizComboBoxElement(databaseName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("styleList", QVariant::fromValue(comboBoxList));
    }

    void RoadNetworkGUI::_readAllStyle()
    {
        _styleList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        _styleFolderPath = appdataPath + "/Styles";

        std::string path = osgDB::convertFileNameToNativeStyle(_styleFolderPath);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            if (osgDB::getLowerCaseFileExtension(currFileName) == "json")
            {
                std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

                _styleList.push_back(fileNameWithoutExtension);
            }
        }
    }

    void RoadNetworkGUI::_getDatabaseParameters(std::string pgresFileName)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string databaseFolder = appdataPath + "/Database";

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            //making database to be invalid that is handled in parent function
            return ;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);
        std::string databaseExtension;
        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == pgresFileName)
            {
                found = true;
                databaseExtension = osgDB::getLowerCaseFileExtension(currFileName);
            }
        }

        if (!found)
        {
            //making database to be invalid that is handled in parent function
            return;
        }

        else
        {
            if (databaseExtension == "pgres")
            {
                QFile settingFile(QString::fromStdString(appdataPath + "/Database/" + pgresFileName + "." + databaseExtension));


                if (!settingFile.open(QIODevice::ReadOnly))
                {
                    //making database to be invalid that is handled in parent function
                    return;
                }

                QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
                if (jdoc.isNull())
                {
                    return;
                }

                QJsonObject obj = jdoc.object();

                _dbOptions.clear();
                _dbOptions.insert(std::pair<std::string, std::string>("Database", obj["Driver"].toString().toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Host", obj["Host"].toString().toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Password", obj["Password"].toString().toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Port", obj["Port"].toString().toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Username", obj["Username"].toString().toStdString()));
            }
        }
    }

    void RoadNetworkGUI::compute()
    {
        QObject* roadNetworkPopup = _findChild("roadNetworkPopup");
        if (roadNetworkPopup)
        {
            QString output = roadNetworkPopup->property("outputLayer").toString();
            _outputLayerName = output.toStdString();

            if (_outputLayerName == "")
            {
                emit showError("Route Calculation", "Please provide layer name for saving.");
                return;
            }

            std::srand(std::time(0));
            int random = std::rand();

            _uniqueName = _outputLayerName + std::to_string(random);
        }

        if (_startPoint == NULL || _endPoint == NULL)
        {
            emit showError("Route Calculation", "Start/End point not selected");
            return;
        }

        if (_databaseName == "Select Database Instance")
        {
            emit showError("Route Calculation", "Please select a database from list.");
            return;
        }

        //! get current selected object
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();

        if (iter == map.end())
            return;

        CORE::RefPtr<CORE::IObject> obj = iter->second->getInterface<CORE::IObject>();
        if (!obj.valid())
            return;

        _featureLayer = obj->getInterface<TERRAIN::IModelObject>();
        selectionUIHandler->setMouseClickSelectionState(false);

        if(!_featureLayer.valid())
        {
            emit showError("Route Calculation", "Shape file is not valid.");
            return;
        }

        CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
        _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::ShortestPathCalculationVisitorType)
            ->getInterface<GISCOMPUTE::IShortestPathCalculationVisitor>();

        if(!_visitor.valid())
        {
            emit showError("Route Calculation", "Route Calculation filter not found.");
            return;
        }

        if (roadNetworkPopup)
        {
            QString output = roadNetworkPopup->property("styleName").toString();
            std::string styleName = output.toStdString();

            if (styleName == "Select Style")
            {
                _styleJsonPath = _styleFolderPath + "/" + "Line.json";
            }
        }

        try
        {
            GISCOMPUTE::IShortestPathCalculationVisitor::Config config;
            config.startPoint = _startPoint->getInterface<CORE::IObject>();
            config.endPoint = _endPoint->getInterface<CORE::IObject>();
            config.outputLayerName = _uniqueName;

            _subscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

            _visitor->setConfig(config);
            _getDatabaseParameters(_databaseName);
            _visitor->setDatabaseParameters(_dbOptions);

            GISCOMPUTE::IFilterVisitor* fv = _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            if(fv == NULL)
                return;

            if(fv->getFilterStatus() != GISCOMPUTE::FILTER_PENDING)
            {
                _featureLayer->getInterface<CORE::IBase>()->accept(*_visitor->getInterface<CORE::IBaseVisitor>());
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void RoadNetworkGUI::popupClosed()
    {
        _featureLayer = NULL;

        if(_guiState == MARK_START)
            markStart(false);
        else if(_guiState == MARK_END)
            markEnd(false);

        if(_startPoint != NULL)
        {
            _removeObject(_startPoint.get());
            _startPoint = NULL;
        }

        if(_endPoint != NULL)
        {
            _removeObject(_endPoint.get());
            _endPoint = NULL;
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        _databaseInstanceList.clear();
        _styleList.clear();
        _databaseName = "Select Database Instance";
        _dbOptions.clear();

        if (_visitor.valid())
        {
            _unsubscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            _visitor = NULL;
        }

       
        selectionUIHandler->setMouseClickSelectionState(true);
    }

    void RoadNetworkGUI::markStart(bool value)
    {
        if(value)
        {
            _guiState = MARK_START;

            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
            }
        }
        else
        {
            _guiState = NONE;

            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
            }
        }
    }

    void RoadNetworkGUI::markEnd(bool value)
    {
        if(value)
        {
            _guiState = MARK_END;

            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());
            if(tph.valid())
            {
                _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
            }
        }
        else
        {
            _guiState = NONE;

            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());
            if(tph.valid())
            {
                _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
            }
        }
    }

    void RoadNetworkGUI::_handleMousePressed(int buttonMask, osg::Vec3d longLatAlt)
    {
        if(_guiState != MARK_START && _guiState != MARK_END)
            return;

        if(!(buttonMask & VizUI::IMouse::LEFT_BUTTON))
            return;

        CORE::RefPtr<CORE::IPoint> point = NULL;
        std::string propertyName;

        switch(_guiState)
        {
        case MARK_START:
            {
                if(_startPoint == NULL)
                {
                    _startPoint = _createPoint();
                    _startPoint->getInterface<CORE::IBase>()->setName("Start");
                }

                point = _startPoint;
                propertyName = "startText";

                break;
            }
        case MARK_END:
            {
                if(_endPoint == NULL)
                {
                    _endPoint = _createPoint();
                    _endPoint->getInterface<CORE::IBase>()->setName("End");
                }

                point = _endPoint;
                propertyName = "endText";

                break;
            }
        }

        if(point != NULL)
        {
            point->setValue(longLatAlt);
        }

        QObject* roadNetworkPopup = _findChild("roadNetworkPopup");
        if(roadNetworkPopup)
        {
            QString text = "( " + QString::number(longLatAlt.x()) + " , " + QString::number(longLatAlt.y()) + ")"; 
            roadNetworkPopup->setProperty(propertyName.c_str() , QVariant::fromValue(text));
        }
    }

    CORE::RefPtr<CORE::IPoint> RoadNetworkGUI::_createPoint()
    {
        CORE::RefPtr<CORE::IObject> pointObject = 
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

        CORE::RefPtr<CORE::IPoint> point = pointObject->getInterface<CORE::IPoint>();

        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
                component->getInterface<ELEMENTS::IIconLoader>();

            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = 
                pointObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolder.valid() && iconLoader.valid())
            {
                iconHolder->setIcon(iconLoader->getIconByName("circle"));
            }
        }

        //set the representation to icon mode
        CORE::RefPtr<ELEMENTS::IRepresentation> representation = 
            pointObject->getInterface<ELEMENTS::IRepresentation>();

        if(representation.valid())
        {
            representation->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
        }


        //set the current temporary state
        CORE::RefPtr<CORE::ITemporary> temp = pointObject->getInterface<CORE::ITemporary>();
        if(temp.valid())
        {
            temp->setTemporary(true);
        }

        CORE::RefPtr<ELEMENTS::IClamper> clamp = pointObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            clamp->setClampOnPlacement(true);
        }

        CORE::RefPtr<ELEMENTS::IDrawTechnique> draw = pointObject->getInterface<ELEMENTS::IDrawTechnique>();
        if(draw.valid())
        {
            draw->setHeightOffset(10);
        }

        ELEMENTS::GetFirstWorldFromMaintainer()->addObject(pointObject.get());
        return point;
    }

    void RoadNetworkGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
       if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            // Send completed on FILTER_IDLE or FILTER_SUCCESS
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                        message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_IDLE || filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                //! Success
                std::cout << "Success!!" << std::endl;

                CORE::RefPtr<CORE::IBase> visitorBase = message.getSender();
                CORE::RefPtr<GISCOMPUTE::IShortestPathCalculationVisitor> visitor = visitorBase->getInterface<GISCOMPUTE::IShortestPathCalculationVisitor>();
                if(visitor.valid())
                {
                    CORE::RefPtr<CORE::IObject> modelObject = visitor->getOutputFolderObject();
                    modelObject->getInterface<CORE::IBase>()->setName(_outputLayerName);
             
                    if(modelObject.valid())
                    {
                       
                        //! Add to world
                        CORE::RefPtr<CORE::IWorld> world = ELEMENTS::GetFirstWorldFromMaintainer();
                        if(world.valid())
                        {
                            
                            SMCUI::IAddContentUIHandler* _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());
                           
                            CORE::RefPtr<CORE::IObject> shortestPathResultObject = visitor->getShortestPathObject();
                            if (shortestPathResultObject.valid())
                            {
                                _addContentUIHandler->updateModelObject(shortestPathResultObject, _styleJsonPath);
                                world->addObject(modelObject->getInterface<CORE::IObject>());

                                if (_visitor.valid())
                                {
                                    _unsubscribe(_visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                                    _visitor = NULL;
                                }
                                
                            }
    
                        }
                    }
                }
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
            {
                std::cout << "Failure!!" << std::endl;
            }
        }
        else if(messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {
            CORE::RefPtr<CORE::IMessage> pmsg = const_cast<CORE::IMessage*>(&message);
            CORE::RefPtr<VizUI::IMouseMessage> mmsg = pmsg->getInterface<VizUI::IMouseMessage>();

            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());

            osg::Vec3d longLatAlt;
            if(tph->getMousePickedPosition(longLatAlt, true))
            {
                _handleMousePressed(mmsg->getMouse()->getButtonMask(), longLatAlt);
            }
        }
        else 
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }
}