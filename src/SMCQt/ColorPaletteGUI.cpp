
#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Util/StringUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorldMaintainer.h>

#include <Elements/IColorMapComponent.h>
#include <Elements/IColorMap.h>

#include <VizQt/NameValidator.h>

#include <QtCore/QStringList>

#include <sstream>
#include <cmath>


namespace SMCQt
{
    ColorObject::ColorObject(QObject* parent) : QObject(parent)
    {
    }

    ColorObject::ColorObject(double minVal, double maxVal, QColor cellColor, QObject *parent)
        : QObject(parent), min_Value(minVal), max_Value(maxVal), cell_Color(cellColor)
    {

    }
    void ColorObject::setMinValue(double minVal)
    {
        if (minVal != min_Value)
        {
            min_Value = minVal;
            emit minValueChanged();
        }
    }

    double ColorObject::minVal() const
    {
        return min_Value;
    }

    void ColorObject::setMaxValue(double maxVal)
    {
        if (maxVal != max_Value)
        {
            max_Value = maxVal;
            emit maxValueChanged();
        }
    }

    double ColorObject::maxVal() const
    {
        return max_Value;
    }

    void ColorObject::setcellColor(QColor cellColor)
    {
        if (cellColor != cell_Color)
        {
            cell_Color = cellColor;
            emit cellColorChanged();
        }
    }

    QColor ColorObject::cellColor() const
    {
        return cell_Color;
    }


    DEFINE_META_BASE(SMCQt, ColorPaletteGUI);
    DEFINE_IREFERENCED(ColorPaletteGUI, DeclarativeFileGUI);
    const std::string ColorPaletteGUI::ColorMap = "ColorMap";

    ColorPaletteGUI::ColorPaletteGUI()
        : _defaultColor(osg::Vec4(1, 1, 1, 1)), _isClamp(false)
    {}

    ColorPaletteGUI::~ColorPaletteGUI()
    {}

    void ColorPaletteGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                // Query the component here
                CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                if (wmain)
                {
                    CORE::IComponent* comp = wmain->getComponentByName("ColorMapComponent");
                    if (comp)
                    {
                        _colorMapComponent = comp->getInterface<ELEMENTS::IColorMapComponent>();
                    }
                }
                // get ui handler
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IColorMapUIHandler>(getGUIManager());
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void ColorPaletteGUI::setActive(bool value)
    {
        if (value == getActive())
        {
            return;
        }
        // Focus UI Handler and activate GUI
        try
        {
            _resetColorTable();
            if (value)
            {
                _populateColorMap();

                QObject* colorMapObject = _findChild(ColorMap);
                if (colorMapObject)
                {
                    connect(colorMapObject, SIGNAL(generate()), this, SLOT(_handleGenerateButtonClicked()), Qt::UniqueConnection);
                    connect(colorMapObject, SIGNAL(save()), this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);
                    connect(colorMapObject, SIGNAL(cancel()), this, SLOT(_handleCancelButtonClicked()), Qt::UniqueConnection);
                    connect(colorMapObject, SIGNAL(rowInsert()), this, SLOT(_handleInsertRowButtonClicked()), Qt::UniqueConnection);
                    connect(colorMapObject, SIGNAL(rowDelete()), this, SLOT(_handleDeleteRowButtonClicked()), Qt::UniqueConnection);
                    connect(colorMapObject, SIGNAL(defaultColorButton(QColor)), this, SLOT(_handleDefaultColorButtonClicked(QColor)), Qt::UniqueConnection);
                    connect(colorMapObject, SIGNAL(isClamp(bool)), this, SLOT(_handleClampChecked(bool)), Qt::UniqueConnection);
                    connect(colorMapObject, SIGNAL(selectTemplateList()), this,
                        SLOT(_handleCBTemplatesIndexChanged()), Qt::UniqueConnection);
                    //connect(colorMapObject, SIGNAL(deleteColorMap()), this, SLOT(deleteColorMap()), Qt::UniqueConnection);

                }

                _fillColorTable(_colorMap.get());
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ColorPaletteGUI::_handleDefaultColorButtonClicked(QColor color)
    {
        if (!_uiHandler.valid())
            return;

        _defaultColor = osg::Vec4((float)color.red() / 255.0f, (float)color.green() / 255.0f, (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);
    }

    void ColorPaletteGUI::_handleCBTemplatesIndexChanged()
    {
        QObject* colorMapObject = _findChild(ColorMap);
        if (colorMapObject)
        {
            std::string uid = colorMapObject->property("templateSelectedUid").toString().toStdString();
            std::string templateName = colorMapObject->property("templateName").toString().toStdString();
            if (!_colorMapComponent || !_uiHandler)
            {
                return;
            }
            if (uid == "0")
            {
                QColor defaultColor(255, 255, 255, 255);
                colorMapObject->setProperty("isClampChecked", false);
                colorMapObject->setProperty("defaultColor", QVariant::fromValue(defaultColor));
                _resetColorTable();
            }
            else
            {
                _colorMap = _uiHandler->getColorMap(templateName);
                if (_colorMap)
                {
                    colorMapObject->setProperty("colorMapName", "");
                    colorMapObject->setProperty("mapSize", "");
                    colorMapObject->setProperty("minmValue", "");
                    colorMapObject->setProperty("maxmValue", "");
                    colorMapObject->setProperty("alphaValue", "");
                    _fillColorTable(_colorMap.get());
                }
            }
        }
    }

    void ColorPaletteGUI::_handleOkButtonClicked()
    {
        if (!_colorMap)
        {
            showError("ColorMap", "ColorMap not generated");
            return;
        }

        QObject* colorMapObject = _findChild(ColorMap);
        if (colorMapObject)
        {
            std::string name = colorMapObject->property("colorMapName").toString().toStdString();
            std::string uid = colorMapObject->property("templateSelectedUid").toString().toStdString();
            if (name == "")
            {
                if (uid!="0")
                {
                 name = colorMapObject->property("templateName").toString().toStdString();
                }
                else
                {
                    showError("Color Map", "Specify color map name");
                    return;
                }
            }
            if (uid == "0")
            {
                int colorMapsize = _templateColorMapList.size();
                for (int j = 0; j < colorMapsize; ++j)
                {
                    //XXX not need to use 
                    std::string templateName = qobject_cast<VizComboBoxElement*>(_templateColorMapList.at(j))->name().toStdString();
                    if (templateName == name)
                    {
                        showError("Color Map", "Color map name already exist give other name");
                        return;
                    }
                }
            }
            _colorMap->setClamped(_isClamp); //Removed according PJ no need
            _colorMap->setDefaultColor(_defaultColor);
            ELEMENTS::IColorMap::RangeColorMap& map = _colorMap->getMap();
            map.clear();
            foreach(QObject* qobj, _colorList)
            {
                ColorObject* colorObj = qobject_cast<ColorObject*>(qobj);
                if (colorObj)
                {
                    double min = colorObj->minVal();
                    double max = colorObj->maxVal();
                    QColor color1 = colorObj->cellColor();
                    osg::Vec4 fColor = osg::Vec4((float)color1.red() / 255.0f,
                        (float)color1.green() / 255.0f,
                        (float)color1.blue() / 255.0f,
                        (float)color1.alpha() / 255.0f);
                    map[osg::Vec2(min, max)] = fColor;
                }
            }
            if (!_uiHandler->exportColorMap(_colorMap.get(), name))
            {
                showError("Export Color Map", "Failed to create color map file. "
                    "Check your file permission.");
                _resetColorTable();
                return;
            }
            //_resetColorTable(); //No need to Clean 
            colorMapObject->setProperty("isCancelDisabled", true);
            colorMapObject->setProperty("isSaveDisabled", false);
            emit newColorMapAdded(name.c_str());
        }
    }

    void ColorPaletteGUI::_handleCancelButtonClicked()
    {
        _resetColorTable();
    }

    void ColorPaletteGUI::_handleInsertRowButtonClicked()
    {
        if (_colorList.empty())
        {
            QObject* colorMapObject = _findChild(ColorMap);
            if (colorMapObject)
            {
                QColor cellColor = QColor(0, 0, 0, 1);
                _colorList.append(new ColorObject(_minSize, _minSize+(_maxSize-_minSize)/_mapSize, cellColor));
                _setContextProperty("colorListModel", QVariant::fromValue(_colorList));
            }

        }
        else
        {
            QObject* qobj = _colorList.last();
            ColorObject *colorMapObject = dynamic_cast<ColorObject*>(qobj);
            double minSize = colorMapObject->maxVal();
            double maxSize = minSize + (minSize - colorMapObject->minVal());
            QColor cellColor = QColor(0, 0, 0, 1);
            _colorList.append(new ColorObject(minSize, maxSize, cellColor));
            _setContextProperty("colorListModel", QVariant::fromValue(_colorList));
        }
    }

    void ColorPaletteGUI::_handleDeleteRowButtonClicked()
    {
        QObject* colorMapObject = _findChild(ColorMap);
        if (colorMapObject)
        {
            int rowindex = colorMapObject->property("rowIndex").toInt();
            _colorList.removeAt(rowindex);
        }
        _setContextProperty("colorListModel", QVariant::fromValue(_colorList));
    }

    void ColorPaletteGUI::_handleGenerateButtonClicked()
    {
        QObject* colorMapObject = _findChild(ColorMap);
        if (colorMapObject)
        {
            bool ok = true;
             _mapSize = colorMapObject->property("mapSize").toUInt(&ok);
            if (!ok || _mapSize == 0)
            {
                showError("Color Map", "Specify color map size");
                return;
            }
             _minSize = colorMapObject->property("minmValue").toFloat(&ok);
            if (!ok)
            {
                showError("Color Map", "Specify minimum value");
                return;
            }

             _maxSize = colorMapObject->property("maxmValue").toFloat(&ok);
             if (!ok || _maxSize <= _minSize)
            {
                showError("Color Map", "Maximum value must be greater than minimum value");
                return;
            }

             _alphaValue = colorMapObject->property("alphaValue").toInt(&ok);

            if (!ok || _alphaValue == 0)
            {
                showError("Transparency", "Specify transparency value between (0 to 255)");
                return;
            }

            _colorMap = _colorMapComponent->createColorMap();
           _colorMap->setClamped(_isClamp);//Removed according PJ no need
            _colorMap->setDefaultColor(_defaultColor);
            ELEMENTS::IColorMap::RangeColorMap& map = _colorMap->getMap();
            float colorstep = 2.0f / (float)_mapSize;
            float step = (_maxSize - _minSize) / (float)_mapSize;
            unsigned int j;
            for (j = 0; j < _mapSize / 2; ++j)
            {
                float colorval = colorstep * j;
                map[osg::Vec2(_minSize + j * step, _minSize + (j + 1) * step)] =
                    osg::Vec4(colorval, 1, 0, (float)_alphaValue / 255.0);
            }
            for (; j < _mapSize; ++j)
            {
                float colorval = colorstep * (j - _mapSize / 2);
                map[osg::Vec2(_minSize + j * step, _minSize + (j + 1) * step)] =
                    osg::Vec4(1, 1 - colorval, 0, (float)_alphaValue / 255.0);
            }
            _fillColorTable(_colorMap.get());
        }
    }

    void ColorPaletteGUI::_populateColorMap()
    {
        if (!_colorMapComponent)
        {
            return;
        }

        const ELEMENTS::IColorMapComponent::ColorMapList& list = _colorMapComponent->getColorMapList();
        _templateColorMapList.clear();
        _templateColorMapList.append(new VizComboBoxElement("Select Color Map", "0"));
        int uid = 0;
        ELEMENTS::IColorMapComponent::ColorMapList::const_iterator iter = list.begin();
        for (; iter != list.end(); ++iter)
        {
            uid++;
            std::string tempName = iter->first;
            _templateColorMapList.append(new VizComboBoxElement(QString::fromStdString(iter->first), QString::number(uid)));
        }
        _setContextProperty("templateColorMapListModel", QVariant::fromValue(_templateColorMapList));
        _resetButton();
    }

    void ColorPaletteGUI::_resetColorTable()
    {
#ifdef WIN32
        _setContextProperty("colorListModel", NULL);
#else //WIN32
        _setContextProperty("colorListModel", "");
#endif //WIN32
        QObject*  colorMapObject = _findChild(ColorMap);
        if (colorMapObject)
        {
            colorMapObject->setProperty("isDeleteRowDisabled", true);
            colorMapObject->setProperty("isSaveDisabled", true);
        }
        _colorMap = NULL;
        _colorList.clear();
    }

    void ColorPaletteGUI::_resetButton()
    {
        QObject* colorMapObject = _findChild(ColorMap);
        if (colorMapObject)
        {
            colorMapObject->setProperty("isGenerateButtonDisabled", false);
            colorMapObject->setProperty("isDeleteRowDisabled", false);
            colorMapObject->setProperty("isSaveDisabled", false);
            colorMapObject->setProperty("isCancelDisabled", false);
        }
    }

    void ColorPaletteGUI::_fillColorTable(ELEMENTS::IColorMap* colorMap)
    {
        if (!_colorMap)
            return;

        QObject* colorMapObject = _findChild(ColorMap);
        if (colorMapObject)
        {
            osg::Vec4 defaultColor = colorMap->getDefaultColor();
            int cx = (int)(255 * defaultColor.x());
            int cy = (int)(255 * defaultColor.y());
            int cz = (int)(255 * defaultColor.z());
            int cw = (int)(255 * defaultColor.w());
            QColor color(cx, cy, cz, cw);
            const ELEMENTS::IColorMap::RangeColorMap& map = colorMap->getMap();
            ELEMENTS::IColorMap::RangeColorMap::const_iterator iter = map.begin();
            _mapSize = map.size();
            osg::Vec2 range = iter->first;
            _minSize = range.x();
            _maxSize = _minSize + _mapSize*(range.y() - range.x());
            //qDeleteAll(_colorList.begin(), _colorList.end()); //No need to call qDeleteAll will clear in clear funtion
            _colorList.clear();

            for (; iter != map.end(); ++iter)
            {
                osg::Vec2 range = iter->first;
                osg::Vec4 color = iter->second;
                std::string strMin = UTIL::ToString(range.x());
                std::string strMax = UTIL::ToString(range.y());
                double min = UTIL::ToDouble(strMin);
                double max = UTIL::ToDouble(strMax);
                int ccx = (int)(255 * color.x());
                int ccy = (int)(255 * color.y());
                int ccz = (int)(255 * color.z());
                int ccw = (int)(255 * color.w());
                QColor qcolor(ccx, ccy, ccz, ccw);
                _colorList.append(new ColorObject(min, max, qcolor));
            }
            QColor temp(255 * defaultColor.x(), 255 * defaultColor.y(), 255 * defaultColor.z(), 255 * defaultColor.w());
            bool clampChecked = colorMap->isClamped();
            _setContextProperty("colorListModel", QVariant::fromValue(_colorList));
            colorMapObject->setProperty("isClampChecked", clampChecked);
            colorMapObject->setProperty("defaultColor", QVariant::fromValue(temp));
            _resetButton();
        }
    }

    void ColorPaletteGUI::_handleClampChecked(bool checked)
    {
        _isClamp = checked;
    }

    void ColorPaletteGUI::deleteColorMap()
    {
        QObject* colorMapObject = _findChild(ColorMap);
        {
            std::string uid = colorMapObject->property("templateSelectedUid").toString().toStdString();
            if (uid != "0")
            {
                _templateColorMapList.removeAt(std::stoi(uid));
                _setContextProperty("templateColorMapListModel", QVariant::fromValue(_templateColorMapList));
            }
        }
    }
}
