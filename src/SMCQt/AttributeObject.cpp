#include <SMCQt/AttributeObject.h>

namespace SMCQt{

    AttributeObject::AttributeObject(QObject* parent) : QObject(parent)
    {

    }

    AttributeObject::AttributeObject(const QString &name, const QString &type, int width, int precision , QObject *parent)
        : QObject(parent), m_name(name), m_type(type), m_width(width), m_precision(precision)
    {
    }

    QString AttributeObject::name() const
    {
        return m_name;
    }

    void AttributeObject::setName(const QString &name)
    {
        if(name != m_name)
        {
            m_name = name;
            emit nameChanged();
        }
    }

    void AttributeObject::setPrecision(int precision)
    {
        if(precision != m_precision)
        {
            m_precision = precision;
            emit precisionChanged();
        }
    }

    int AttributeObject::precision() const
    {
        return m_precision;
    }

    QString AttributeObject::type() const
    {
        return m_type;
    }
    void AttributeObject::setType(const QString &type)
    {
        if(type != m_type)
        {
            m_type = type;
            emit typeChanged();
        }
    }

    int AttributeObject::width() const
    {
        return m_width;
    }

    void AttributeObject::setWidth(int width)
    {
        if(width != m_width)
        {
            m_width = width;
            emit widthChanged();
        }
    }
}
