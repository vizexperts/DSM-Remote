#include <SMCQt/ApplicationSettingsGUI.h>
#include <Util/FileUtils.h>
#include<Util/ShpFileUtils.h>
#include <SMCQt/VizComboBoxElement.h>
#include <serialization/SerializationUtils.h>
#include <QJSONDocument>
#include <QJSONObject>
#include <QJsonArray> 




namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ApplicationSettingsGUI);
    std::string ApplicationSettingsGUI::_applicationSelectedJsonFile;
   
    ApplicationSettingsGUI::ApplicationSettingsGUI()
    {
    }

    void ApplicationSettingsGUI::connectSlots()
    {

        QObject* object = _findChild("appSettings");

        if (object){
            connect(object, SIGNAL(jsonParsing()), this, SLOT(JsonParsing()), Qt::UniqueConnection);
        }
    }

    void ApplicationSettingsGUI::createComboBoxItem(QString name)
    {


        std::string applicationSettingsSchemaFile = "../../config/ApplicationSettingsSchema.json";
        QFileInfo applicationSchemafileInfo(applicationSettingsSchemaFile.c_str());
        QString applicationSchemaPath = applicationSchemafileInfo.absoluteFilePath();

        QFile settingFile(applicationSchemaPath);

        if (!settingFile.open(QIODevice::ReadOnly))
        {
            // settingsJson not found 
            return;
        }

        QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
        if (jdoc.isNull())
        {
            return;
        }


        QJsonObject root = jdoc.object();
        
        QJsonValue value = root.value(name); 

        if (value.isObject())
        {
            QJsonObject valueObjet = value.toObject();

            QJsonValue value2 = valueObjet.value("values");

            QJsonArray arrwe = value2.toArray();
            
            QList<QObject*> styleSheetBoxList;
            int i = 0; 
            for (QJsonArray::Iterator it = arrwe.begin(); it != arrwe.end(); it++)
            {
                std::string  value = it->toString().toStdString();
                styleSheetBoxList.append(new VizComboBoxElement(value.c_str(), UTIL::ToString(i++).c_str()));

            }
            _setContextProperty(name.toStdString(), QVariant::fromValue(styleSheetBoxList));
        }
    }

    void ApplicationSettingsGUI::saveJsonFile()
    {
        std::ofstream outfile(_applicationSelectedJsonFile.c_str());

        QObject* object = _findChild("appSettings");
        if (object != NULL)
        {
            std::string outString = object->property("jsonOutput").toString().toStdString();
            outfile << outString;
        }

        outfile.close();
    }


    void ApplicationSettingsGUI::JsonParsing()

    {

        // Reading Applications Settings JsonFile
        std::string applicationWideSettingsFile = "../../config/ApplicationWideSettings.json";
        QFileInfo applicationfileInfo(applicationWideSettingsFile.c_str());
        QString applicationPath = applicationfileInfo.absoluteFilePath();
        std::string absolutePath = applicationPath.toStdString();
        _applicationSelectedJsonFile = absolutePath;
        std::string fileName = osgDB::getSimpleFileName(absolutePath);

        std::ifstream inFile(absolutePath.c_str());
        if (!inFile.is_open())
        	return;

        std::string jsonData((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());


        QObject* object = _findChild("appSettings");
        if (object != NULL)
        {
            object->setProperty("jsonContent", QString::fromStdString(jsonData));
        }

        //Reading Application Settings json Schema file
        std::string ApplicationSchemaFile = "../../config/ApplicationSettingsSchema.json";
        QFileInfo ApplicationschemafileInfo(ApplicationSchemaFile.c_str());
        QString strFilePath = ApplicationschemafileInfo.absoluteFilePath();
        std::string schemaPath = strFilePath.toStdString();

       std::ifstream schemaFile(schemaPath.c_str());
        if (!schemaFile.is_open())
            return;
        std::string schemaData((std::istreambuf_iterator<char>(schemaFile)), std::istreambuf_iterator<char>());

        QObject* SchemaObject = _findChild("appSettings");
        if (SchemaObject != NULL)
        {
            SchemaObject->setProperty("jsonSchemaContent", QString::fromStdString(schemaData));

        }

        schemaFile.close(); 
    }
}
