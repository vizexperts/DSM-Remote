/*****************************************************************************
 *
 * File             : SteepestPathGUI.cpp
 * Description      : SteepestPathGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, CAIR, DRDO
 *****************************************************************************/

#include <SMCQt/SteepestPathGUI.h>
#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>
#include <Core/AttributeTypes.h>
#include <Core/InterfaceUtils.h>
#include <Util/StringUtils.h>
#include <GISCompute/IFilterStatusMessage.h>

#include <App/IApplication.h>

#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>

namespace SMCQt
{
    const std::string SteepestPathGUI::SteepestPathAnalysisPopup = "steepestPathAnalysis";

    DEFINE_META_BASE(SMCQt,SteepestPathGUI);
    DEFINE_IREFERENCED(SteepestPathGUI, DeclarativeFileGUI);

    SteepestPathGUI::SteepestPathGUI()
        : _steepPathGUIHandler(NULL), _pointHandler(NULL), _areaHandler(NULL)
        , _startPoint(NULL), _vExtent(NULL), _iPolygon(NULL), _bPolygonRemovable(false)
    {
    }

    SteepestPathGUI::~SteepestPathGUI()
    {
        setActive(false);
    }


    void
    SteepestPathGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }

    void 
    SteepestPathGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {
                _pointHandler 
                    = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
                _areaHandler 
                    = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
                _steepPathGUIHandler 
                    = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ISteepestPathUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }

        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            CORE::IPoint* point = _pointHandler->getPoint();
            if(_steepPathGUIHandler.valid() && point)
            {
                _setStartPoint(point);
                QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                QString pointX, pointY, pointZ;

                pointX = QString::number(point->getX());
                pointY = QString::number(point->getY());
                pointZ = QString::number(point->getZ());

                if(SteepestPathAnalysisObject)
                {
                    SteepestPathAnalysisObject->setProperty("startPointLongitude",pointX);
                    SteepestPathAnalysisObject->setProperty("startPointLatitude",pointY);
                    SteepestPathAnalysisObject->setProperty("startPointAltitude",pointZ);
                }
            }
            else
            {
                emit showError("Filter Error", "Either Filter not Initiated or Start Point is invalid");
            }

        }
        
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                    if(SteepestPathAnalysisObject)
                    {
                        SteepestPathAnalysisObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        SteepestPathAnalysisObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        SteepestPathAnalysisObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        SteepestPathAnalysisObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }

                    if(!_vExtent.valid())
                    {
                        _vExtent = new osg::Vec2dArray;
                    }
                    else
                    {
                        _vExtent->clear();
                    }
                    _vExtent->push_back(osg::Vec2(first.x(), first.y()));
                    _vExtent->push_back(osg::Vec2(second.x(), second.y()));

                    if(_iPolygon.valid() && _bPolygonRemovable)
                    {
                        APP::IGUIManager* guiMgr = getGUIManager();
                        CORE::IWorld* world = APP::AccessElementUtils::getWorldFromManager(guiMgr);
                        if(!world)
                        {
                            return;
                        }
                        world->removeObjectByID(&(_iPolygon->getInterface<IBase>()->getUniqueID()));
                        _bPolygonRemovable = false;
                    }
                    
                    _iPolygon = polygon;
                }
            }
        }

        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                        message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
            {
                emit showError("Filter Status", "Failed to apply filter");

            }
        }

        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    
    void 
    SteepestPathGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }

        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                if(SteepestPathAnalysisObject)
                {
                    // start and stop button handlers
                    connect(SteepestPathAnalysisObject, SIGNAL(startAnalysis()),this,SLOT(_handleStartButtonClicked()),Qt::UniqueConnection);
                    connect(SteepestPathAnalysisObject, SIGNAL(stopAnalysis()),this, SLOT(_handleStopButtonClicked()),Qt::UniqueConnection);
                    connect(SteepestPathAnalysisObject, SIGNAL(markPosition(bool)),this,SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);
                    connect(SteepestPathAnalysisObject, SIGNAL(markBreachPointPosition(bool)),this,SLOT(_handlePBStartPointClicked(bool)),Qt::UniqueConnection);

                    // enable start button
                    SteepestPathAnalysisObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    SteepestPathAnalysisObject->setProperty("isStopButtonEnabled",false);
                }
            }
            else
            {
                _reset();
                _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

            }
            
            if(value)
            {
                _steepPathGUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _subscribe(_steepPathGUIHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _steepPathGUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_steepPathGUIHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }

            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
                
    }

    bool
    SteepestPathGUI::play()
    {
        bool ret = false;
        if(_steepPathGUIHandler.valid())
        {
            ret = _steepPathGUIHandler->play();
        }
        else
        {
            emit showError("SteepPath Error", "Could not play");
        }

        QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
        if(SteepestPathAnalysisObject)
        {
            SteepestPathAnalysisObject->setProperty( "progressBar", false );
        }
        return ret;
    }

    bool
    SteepestPathGUI::stop()
    {
        bool ret = false;

        if(_steepPathGUIHandler.valid())
        {
            ret = _steepPathGUIHandler->stop();
        }
        else
        {
            emit showError("SteepPath Error", "Could not stop");
        }
        return ret;
    }

    void 
    SteepestPathGUI::_handlePBAreaClicked(bool pressed)
    {
        if(_areaHandler.valid())
        {
            if(pressed)
            {
                _areaHandler->setTemporaryState(true);
                _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
                _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);
            }
            else
            {
                // to take ownership
                _bPolygonRemovable = true;
                _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
                _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
                QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                if(SteepestPathAnalysisObject)
                {
                    SteepestPathAnalysisObject->setProperty( "isMarkAreaSelected" , false );
                }
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark area fail");
        }
        
    }

    void 
    SteepestPathGUI::_handlePBStartPointClicked(bool pressed)
    {
        if(_pointHandler.valid())
        {            
            if(pressed)
            {
                _pointHandler->setTemporaryState(true);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);

            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
                QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                if(SteepestPathAnalysisObject)
                {
                   SteepestPathAnalysisObject->setProperty("isStartPointSelected",false);
                }
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark start point fail");
        }
    }
    
    void 
    SteepestPathGUI::_handleStartButtonClicked()
    {
        if ((_steepPathGUIHandler->getPlayState() < SMCUI::ISteepestPathUIHandler::STEEPCOMPUTATION_PLAYING)
            || (_steepPathGUIHandler->getPlayState() > SMCUI::ISteepestPathUIHandler::STEEPCOMPUTATION_PAUSED))
        {
            double LatVal = 0.0;
            double LongVal = 0.0;

            // set output name
            {

                QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                if (SteepestPathAnalysisObject)
                {
                    std::string name = SteepestPathAnalysisObject->property("outputName").toString().toStdString();

                    if (name == "")
                    {
                        showError("Enter Output Name", "Output layer name not specified");
                        _handleStopButtonClicked();
                        return;
                    }
                    _steepPathGUIHandler->setOutputName(name);
                }

            }

            // set start point
            {
                QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                if (SteepestPathAnalysisObject)
                {

                    if (SteepestPathAnalysisObject->property("startPointLatitude").toString().toStdString().length() == 0 ||
                        SteepestPathAnalysisObject->property("startPointLongitude").toString().toStdString().length() == 0 ||
                        SteepestPathAnalysisObject->property("startPointAltitude").toString().toStdString().length() == 0)
                    {
                        showError("Insufficient data", "Start point parameters are not specified");
                        return;
                    }

                }

                if (!_startPoint.valid())
                {
                    showError("Mark Start Point", "Please mark the starting point");
                    _handleStopButtonClicked();
                    return;

                }
                CORE::IPoint* point = _startPoint->getInterface<CORE::IPoint>();
                if (NULL == point)
                {
                    _handleStopButtonClicked();
                    return;
                }
                LatVal = point->getX();
                LongVal = point->getY();
                osg::Vec2d vPoint(point->getX(), point->getY());
                if (!_steepPathGUIHandler->setStartingPoint(vPoint))
                {
                    return;
                }
            }

            // set area extent
            {
                QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
                if (SteepestPathAnalysisObject)
                {
                    osg::Vec4d extents;
                   
                    bool okx = true;
                    extents.x() = SteepestPathAnalysisObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                    bool oky = true;
                    extents.y() = SteepestPathAnalysisObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                    bool okz = true;
                    extents.z() = SteepestPathAnalysisObject->property("topRightLongitude").toString().toDouble(&okz);

                    bool okw = true;
                    extents.w() = SteepestPathAnalysisObject->property("topRightLatitude").toString().toDouble(&okw);

                    bool completeAreaSpecified = okx & oky & okz & okw;

                    if (!completeAreaSpecified)// && partialAreaSpecified)
                    {
                        showError("Insufficient / Incorrect data", "Area extent field is missing");
                        return;
                    }

                    if (completeAreaSpecified)
                    {
                        if (extents.x() >= extents.z() || extents.y() >= extents.w())
                        {
                            showError("Insufficient / Incorrect data", "Area extents are not valid. "
                                "Bottom left coordinates must be less than upper right.");

                            if (SteepestPathAnalysisObject)
                            {
                                SteepestPathAnalysisObject->setProperty("isMarkAreaSelected", false);
                                SteepestPathAnalysisObject->setProperty("isStartPointSelected", false);
                                SteepestPathAnalysisObject->setProperty("isStartButtonEnabled", true);
                                SteepestPathAnalysisObject->setProperty("isStopButtonEnabled", false);
                                _handlePBStartPointClicked(false);
                                _handlePBAreaClicked(false);
                            }

                            return;
                        }
                        _steepPathGUIHandler->setExtent(*_vExtent.get());
                    }

                    if (SteepestPathAnalysisObject)
                    {
                        SteepestPathAnalysisObject->setProperty("isMarkAreaSelected", false);
                        SteepestPathAnalysisObject->setProperty("isStartPointSelected", false);
                        SteepestPathAnalysisObject->setProperty("isStartButtonEnabled", false);
                        SteepestPathAnalysisObject->setProperty("isStopButtonEnabled", true);
                        _handlePBStartPointClicked(false);
                        _handlePBAreaClicked(false);
                    }
                }
            }

            QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
            if (SteepestPathAnalysisObject)
            {
                SteepestPathAnalysisObject->setProperty("progressBar", true);
            }
            play();

            if (SteepestPathAnalysisObject)
            {
                //disable stop button
                SteepestPathAnalysisObject->setProperty("isStopButtonEnabled", false);
                SteepestPathAnalysisObject->setProperty("isStartButtonEnabled", true);
            }
        }
    }

    void 
    SteepestPathGUI::_handleStopButtonClicked()
    {
        if(!_steepPathGUIHandler.valid())
        {
            return;
        }

        QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
        if(SteepestPathAnalysisObject)
        {
            //disable stop button
            SteepestPathAnalysisObject->setProperty("isStopButtonEnabled",false);
            SteepestPathAnalysisObject->setProperty("isStartButtonEnabled",true);
        } 

        if (_steepPathGUIHandler->getPlayState() == SMCUI::ISteepestPathUIHandler::STEEPCOMPUTATION_IDLE )
        {
            return;
        }

        stop();
        
          
    }

    void 
    SteepestPathGUI::_removeObject(CORE::IObject* obj)
    {
        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        if(world.valid() && obj)
        {
            try
            {
                world->removeObjectByID(&(obj->getInterface<CORE::IBase>(true)->getUniqueID()));
            }
            catch(...){}
        }
    }

 
    void
    SteepestPathGUI::_setStartPoint(CORE::IPoint* point)
    {
        _removeObject(_startPoint.get());
        _startPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

    void
    SteepestPathGUI::_reset()
    {
        QObject* SteepestPathAnalysisObject = _findChild(SteepestPathAnalysisPopup);
        if(SteepestPathAnalysisObject)
        {
            SteepestPathAnalysisObject->setProperty("isStartPointSelected",false);
            SteepestPathAnalysisObject->setProperty("isMarkAreaSelected",false);
            SteepestPathAnalysisObject->setProperty("bottomLeftLongitude","longitude");
            SteepestPathAnalysisObject->setProperty("bottomLeftLatitude","latitude");
            SteepestPathAnalysisObject->setProperty("topRightLongitude","longitude");
            SteepestPathAnalysisObject->setProperty("topRightLatitude","latitude");
            SteepestPathAnalysisObject->setProperty("outputName","outputName");
            SteepestPathAnalysisObject->setProperty("startPointLatitude","latitude");
            SteepestPathAnalysisObject->setProperty("startPointLongitude","longitude");
            SteepestPathAnalysisObject->setProperty("startPointAltitude","altitude");
            SteepestPathAnalysisObject->setProperty("isStartButtonEnabled","true");
            SteepestPathAnalysisObject->setProperty("isStopButtonEnabled","false");
        }
      
        _iPolygon = NULL;
        _bPolygonRemovable = false;
        if(_startPoint.valid())
        {
            _removeObject(_startPoint.get());
        }

    }

} // namespace SMCQT
