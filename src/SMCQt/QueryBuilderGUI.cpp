/****************************************************************************
*
* File             : QueryBuilderGUI.h
* Description      : QueryBuilderGUI class definition
*
*****************************************************************************
* Copyright (c) 2013-2014, CAIR, DRDO
*****************************************************************************/

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Core/IFeatureObject.h>
#include <Core/IGeoExtent.h>
#include <VizUI/ICameraUIHandler.h>
#include <Core/ISQLQueryResult.h>
#include <Terrain/IOGRLayerHolder.h>
#include <Core/CoreRegistry.h>
#include <Elements/ElementsPlugin.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/ICompositeObject.h>
#include <Core/Line.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Elements/FeatureLayerUtils.h>
#include <Core/WorldMaintainer.h>
#include <Elements/FeatureObjectUtils.h>
#include <Core/IPolygon.h>
#include <Core/ITemporary.h>
#include <Core/IBaseUtils.h>
#include <SMCQt/QueryBuilderGUI.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, QueryBuilderGUI);
    DEFINE_IREFERENCED(QueryBuilderGUI, SMCQt::DeclarativeFileGUI);

    QueryBuilderGUI::QueryBuilderGUI()
    {
    }

    QueryBuilderGUI::~QueryBuilderGUI()
    {
        setActive(false);
    }

    void QueryBuilderGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void QueryBuilderGUI::connectPopupLoader(QString type)
    {
        if(type == "QueryBuilder")
        {
            setActive(true);
            QObject* queryBuilderObject = _findChild("QueryBuilder");
            if(queryBuilderObject)
            {
                // connectivity of all the buttons
                connect(queryBuilderObject, SIGNAL(addToSelectedFields(int)), this, SLOT(_handleAddToSelectButtonClicked(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(addToWhere(int)), this, SLOT(_handleAddToWhereButtonClicked(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(markPosition(bool)), this, SLOT(_handlePBAreaClicked(bool)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(operatorButtonClicked(QString)), this, SLOT(_handleOperatorButtonClicked(QString)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(queryButtonClicked()), this, SLOT(_handleQueryButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(clearButtonClicked()), this, SLOT(_handleClearButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(cancelButtonClicked()), this, SLOT(_handleCancelButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(okButtonClicked()), this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(selectedFieldDeletion(int)), this, SLOT(_handleDeleteFromSelectedFields(int )), Qt::UniqueConnection);
            }
        }
        if(type == "QueryBuilderTable")
        {
            QObject* queryBuilderTableObject = _findChild("QueryBuilderTable");
            if(queryBuilderTableObject)
            {
                connect(queryBuilderTableObject, SIGNAL(tableOkButtonClicked()), this, SLOT(_handleTableOkButtonClicked()), Qt::UniqueConnection);
                // connect signal and slots for QueryBuilderTable
            }
        }
    }

    void QueryBuilderGUI::_handleTableOkButtonClicked()
    {
        _selectedFieldDefinitions.clear();
        _setContextProperty("selectedFieldsList", QVariant::fromValue(_selectedFieldDefinitions));
    }

    void QueryBuilderGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                // set the remaining handlers for further use
                _sqlQueryUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ISQLQueryUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());


            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    QObject* queryBuilderObject = _findChild("QueryBuilder");
                    if(queryBuilderObject)
                    {
                        queryBuilderObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        queryBuilderObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        queryBuilderObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        queryBuilderObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
    }

    void QueryBuilderGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

            QObject* queryBuilderObject = _findChild("QueryBuilder");
            if(queryBuilderObject)
            {
                queryBuilderObject->setProperty( "isMarkSelected" , false );
            }
        }
    }

    void QueryBuilderGUI::_handleOperatorButtonClicked(QString buttonName)
    {
        QObject* queryBuilderObject = _findChild("QueryBuilder");
        if(queryBuilderObject)
        {
            std::string whereClause = queryBuilderObject->property("sqlWhereText").toString().toStdString();
            whereClause.append(buttonName.toStdString());
            queryBuilderObject->setProperty("sqlWhereText", whereClause.c_str());
        }
    }

    void QueryBuilderGUI::setActive(bool value)
    {
        // Focus area handler and activate GUI
        try
        {
            if(value)
            {
                _getSelectedLayer();
                _parentLayer = _currentLayer;

                _fillTableDefinitionFields();

            }
            else
            {
                _selectedFieldDefinitions.clear();
                _currentLayer = NULL;
                _parentLayer = NULL;
                QObject* queryBuilderObject = _findChild("QueryBuilder");
                if(queryBuilderObject)
                {
                    queryBuilderObject->setProperty("isMarkSelected", false);
                    queryBuilderObject->setProperty("sqlWhereText", "");
                    queryBuilderObject->setProperty("bottomLeftLongitude","");
                    queryBuilderObject->setProperty("bottomLeftLatitude","");
                    queryBuilderObject->setProperty("topRightLongitude","");
                    queryBuilderObject->setProperty("topRightLatitude","");
                }
            }
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void QueryBuilderGUI::_fillTableDefinitionFields()
    {
        if(_currentLayer.valid())
        {
            QObject* queryBuilderObject = _findChild("QueryBuilder");
            if(queryBuilderObject)
            {
                _allFieldDefinitions.clear();
                CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = _currentLayer->getInterface<CORE::IMetadataTableHolder>();
                if(tableHolder.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTable> table = tableHolder->getMetadataTable();
                    if(table.valid())
                    {
                        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = table->getMetadataTableDefn();
                        if(tableDefn.valid())
                        {
                            for(int i=0; i < tableDefn->getFieldCount(); ++i)
                            {
                                CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = tableDefn->getFieldDefnByIndex(i);
                                if(fieldDefn.valid())
                                {
                                    _allFieldDefinitions.append(QString::fromStdString(fieldDefn->getName()));
                                }
                            }
                            _setContextProperty("allFieldsList", QVariant::fromValue(_allFieldDefinitions));
                        }

                    }
                }
            }
        }
    }

    void QueryBuilderGUI::_handleAddToSelectButtonClicked(int index)
    {
        QObject* queryBuilderObject = _findChild("QueryBuilder");
        if(queryBuilderObject)
        {
            if(index != -1)
            {
                QString fieldName = _allFieldDefinitions.at(index);
                _selectedFieldDefinitions.append(fieldName);
            }
            _setContextProperty("selectedFieldsList", QVariant::fromValue(_selectedFieldDefinitions));
        }
    }

    void QueryBuilderGUI::_handleAddToWhereButtonClicked(int index)
    {
        QObject* queryBuilderObject = _findChild("QueryBuilder");
        if(queryBuilderObject)
        {
            if(index != -1)
            {
                QString fieldName = _allFieldDefinitions.at(index);
                std::string whereClause = queryBuilderObject->property("sqlWhereText").toString().toStdString();
                whereClause.append(fieldName.toStdString());
                whereClause.append(" ");
                queryBuilderObject->setProperty("sqlWhereText", whereClause.c_str());
            }
        }
    }

    void QueryBuilderGUI::_handleDeleteFromSelectedFields(int index)
    {
        QObject* queryBuilderObject = _findChild("QueryBuilder");
        if(queryBuilderObject)
        {
            if(index != -1)
            {
                _selectedFieldDefinitions.removeAt(index);
                _setContextProperty("selectedFieldsList", QVariant::fromValue(_selectedFieldDefinitions));
            }
        }
    }

    void QueryBuilderGUI::_handleQueryButtonClicked()
    {
        // Fill the table header fields and create table view

        // getting the area extents
        osg::Vec4d extents;
        QObject* queryBuilderObject = _findChild("QueryBuilder");
        if(queryBuilderObject)
        {
            bool okX = true;
            extents.x() = queryBuilderObject->property("bottomLeftLongitude").toString().toDouble(&okX);

            bool okY = true;
            extents.y() = queryBuilderObject->property("bottomLeftLatitude").toString().toDouble(&okY);

            bool okZ = true;
            extents.z() = queryBuilderObject->property("topRightLongitude").toString().toDouble(&okZ);

            bool okW = true;
            extents.w() = queryBuilderObject->property("topRightLatitude").toString().toDouble(&okW);

            bool completedAreaSpecified = okX & okY & okZ & okW;
            bool partialAreaSpecified = okX | okY | okZ | okW;

            if(!completedAreaSpecified && partialAreaSpecified)
            {
                showError( "Area Extents Error", "Please Enter Valid Extents Or Remain All Empty");
                return;
            }

            if(completedAreaSpecified)
            {
                if(extents.x() >= extents.z() || extents.y() >= extents.w())
                {
                    showError("Area Extents Error", "Bottom left coordinates must be less than upper right.");
                    return;
                }
            }
        }


        QMetaObject::invokeMethod(queryBuilderObject, "displayTable");

        QObject* queryBuilderTableObject = _findChild("QueryBuilderTable");

        // getting the name of fields to be displayed

        std::string selectFields;
        if(_selectedFieldDefinitions.size() > 0)
        {
            for(int i=0; i< _selectedFieldDefinitions.size(); ++i)
            {
                // creation of table view
                QString fieldDefinitionName = _selectedFieldDefinitions.at(i);
                if(queryBuilderTableObject)
                {
                    std::string id = fieldDefinitionName.toStdString();
                    id.append("Id");

                    std::string text("modelData.");
                    text.append(fieldDefinitionName.toStdString());

                    QMetaObject::invokeMethod(queryBuilderTableObject, "addTableViewColumn", Q_ARG(QVariant, QVariant(fieldDefinitionName))
                        , Q_ARG(QVariant, QVariant(fieldDefinitionName))
                        , Q_ARG(QVariant, QVariant(QString(id.c_str())))
                        , Q_ARG(QVariant, QVariant(QString(text.c_str()))));
                    //queryBuilderTableObject->setProperty("text", QVariant(QString(text.c_str())));
                }

                selectFields.append(_selectedFieldDefinitions.at(i).toStdString());
                if(i != _selectedFieldDefinitions.size()-1)
                {
                    selectFields.append(",");
                }
            }
        }
        else
        {
            for(int i=0; i< _allFieldDefinitions.size(); ++i)
            {
                // creation of table view
                QString fieldDefinitionName = _allFieldDefinitions.at(i);
                if(queryBuilderTableObject)
                {
                    std::string id = fieldDefinitionName.toStdString();
                    id.append("Id");

                    std::string text("modelData.");
                    text.append(fieldDefinitionName.toStdString());

                    QMetaObject::invokeMethod(queryBuilderTableObject, "addTableViewColumn", Q_ARG(QVariant, QVariant(fieldDefinitionName))
                        , Q_ARG(QVariant, QVariant(fieldDefinitionName))
                        , Q_ARG(QVariant, QVariant(QString(id.c_str())))
                        , Q_ARG(QVariant, QVariant(QString(text.c_str()))));
                }
            }
            selectFields.append("*");
        }

        // getting the where clause
        std::string whereClause;
        if(queryBuilderObject)
        {
            whereClause = queryBuilderObject->property("sqlWhereText").toString().toStdString();
        }

        CORE::WorldMaintainer* worldMaintainer = CORE::WorldMaintainer::instance();
        CORE::IWorld* world = NULL;
        if(worldMaintainer)
        {
            typedef std::map<const CORE::UniqueID*, CORE::RefPtr<CORE::IWorld> > WorldMap;
            WorldMap worldmap = worldMaintainer->getWorldMap();
            WorldMap::iterator iter = worldmap.begin();
            if(iter != worldmap.end())
            {
                world = iter->second.get();
            }
        }

        // remove previous result layer
        if(_currentLayer && _currentLayer != _parentLayer)
        {
            world->removeObjectByID(&(_currentLayer->getInterface<CORE::IBase>()->getUniqueID()));
        }

        _currentLayer = NULL;
        _currentOGRLayer = NULL;

        CORE::RefPtr<CORE::ISQLQueryResult> sqlQueryResult = NULL;
        OGRLayer* ogrSQLLayer = NULL;
        // Spatial query is not working so in all case if part is executed
        //if(!completedAreaSpecified)
        if(true)
        {
            sqlQueryResult = _sqlQueryUIHandler->executeSQLQuery(_parentLayer.get(), selectFields, whereClause );
            CORE::RefPtr<TERRAIN::IOGRLayerHolder> holder = sqlQueryResult->getInterface<TERRAIN::IOGRLayerHolder>();
            ogrSQLLayer = holder->getOGRLayer();
            //CORE::RefPtr<CORE::IObject> sqlLayerObject = _createFeatureLayer(ogrSQLLayer);
            //_currentLayer = sqlLayerObject->getInterface<CORE::IFeatureLayer>();
        }
        else
        {
            // first run spatial query and then sql query
            sqlQueryResult = _sqlQueryUIHandler->executeSQLQuery(_parentLayer.get(), "", whereClause, extents);
            if(selectFields != "")
            {
                if(sqlQueryResult.valid() && !sqlQueryResult->isEmpty())
                {
                    CORE::RefPtr<TERRAIN::IOGRLayerHolder> holder = sqlQueryResult->getInterface<TERRAIN::IOGRLayerHolder>();
                    ogrSQLLayer = holder->getOGRLayer();
                    CORE::RefPtr<CORE::IObject> sqlLayerObject = _createFeatureLayer(ogrSQLLayer);
                    sqlQueryResult->execute();
                    if(sqlLayerObject.valid())
                    {
                        sqlQueryResult = _sqlQueryUIHandler->executeSQLQuery(sqlLayerObject->getInterface<CORE::IFeatureLayer>(), selectFields, "");
                    }
                }
            }
        }

        _currentOGRLayer = ogrSQLLayer;

        // creation of new table model to be shown
        _tableModel = new LayerTableModel(NULL, _currentLayer.get(), _currentOGRLayer);

        // connectivity of model to view
        CORE::RefPtr<VizQt::IQtGUIManager> qtmgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if(qtmgr)
        {
            QQmlContext* rootContext = qtmgr->getRootContext();
            if(rootContext)
            {
                rootContext->setContextProperty("queryTableModel", _tableModel);
            }
        }

    }

    void QueryBuilderGUI::_handleOkButtonClicked()
    {
        _allFieldDefinitions.clear();
        setActive(false);
        _setContextProperty("selectedFieldsList", QVariant::fromValue(_selectedFieldDefinitions));
        _setContextProperty("allFieldList", QVariant::fromValue(_allFieldDefinitions));
    }

    void QueryBuilderGUI::_handleClearButtonClicked()
    {
        QObject* queryBuilderObject = _findChild("QueryBuilder");
        if(queryBuilderObject)
        {
            _selectedFieldDefinitions.clear();
            _setContextProperty("selectedFieldsList", QVariant::fromValue(_selectedFieldDefinitions));
            queryBuilderObject->setProperty("sqlWhereText", "");
            queryBuilderObject->setProperty("isMarkSelected", false);
            queryBuilderObject->setProperty("bottomLeftLongitude", "");
            queryBuilderObject->setProperty("bottomLeftLatitude", "");
            queryBuilderObject->setProperty("topRightLongitude", "");
            queryBuilderObject->setProperty("topRightLatitude", "");

        }
    }

    void QueryBuilderGUI::_handleCancelButtonClicked()
    {
        // nothing to do 
    }

    CORE::RefPtr<CORE::IObject>
        QueryBuilderGUI::_createFeatureLayer(OGRLayer *ogrLayer)
    {
        if(ogrLayer)
        {
            ogrLayer->ResetReading();
            std::vector<OGRFeature*> featureList;
            OGRFeature* readFeature = NULL;
            while((readFeature = ogrLayer->GetNextFeature()) != NULL)
            {
                featureList.push_back(readFeature); //Destroying below After Use
            }

            CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
            CORE::RefPtr<CORE::IComponent> component = worldMaintainer->getComponentByName("DataSourceComponent");
            CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = component->getInterface<CORE::IMetadataCreator>();
            CORE::RefPtr<CORE::IObject> featureLayerObject = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);
            CORE::RefPtr<CORE::ICompositeObject> featureLayerComposite = featureLayerObject->getInterface<CORE::ICompositeObject>();
            CORE::RefPtr<CORE::IBase> featureLayerBase = featureLayerObject->getInterface<CORE::IBase>();
            CORE::RefPtr<CORE::IMetadataTableHolder> metadataTableHolder = featureLayerObject->getInterface<CORE::IMetadataTableHolder>();
            CORE::RefPtr<CORE::IMetadataTable> metadataTable = metadataTableHolder->getMetadataTable();
            OGRFeatureDefn* ogrFeatureDef = ogrLayer->GetLayerDefn();

            // set the name of the layer
            featureLayerBase->setName(ogrFeatureDef->GetName());

            // check the layer type
            CORE::IFeatureLayer::FeatureLayerType featureLayerType = _parentLayer->getFeatureLayerType();
            metadataTable->setFeatureLayerType(featureLayerType);
            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = metadataCreator->createMetadataTableDefn();

            // iterate over table definition and create a new IMeatadataTableDefn
            for(int fieldIndex = 0; fieldIndex < ogrFeatureDef->GetFieldCount(); fieldIndex++)
            {
                CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = metadataCreator->createMetadataFieldDefn();
                if(!fieldDefn.valid())
                {
                    LOG_ERROR("Unable to create a IMetadataFieldDefn instance");
                    continue;
                }

                OGRFieldDefn* ogrFieldDefn = ogrFeatureDef->GetFieldDefn(fieldIndex);

                CORE::IMetadataFieldDefn::FieldType type;

                OGRFieldType ogrtype = ogrFieldDefn->GetType();

                switch (ogrtype)
                {
                case OFTInteger:
                    type = CORE::IMetadataFieldDefn::INTEGER;
                    break;
                case OFTReal:
                    type = CORE::IMetadataFieldDefn::DOUBLE;
                    break;
                case OFTString:
                    type = CORE::IMetadataFieldDefn::STRING;
                    break;
                    //                    case OFTDateTime:
                    //                        type = CORE::IMetadataFieldDefn::DATETIME;
                    //                        break;
                default:
                    type = CORE::IMetadataFieldDefn::STRING;
                    break;
                }

                std::string fieldDefnName = ogrFieldDefn->GetNameRef();

                //set the parameters for the fieldDefn
                fieldDefn->setType(type);
                fieldDefn->setName(fieldDefnName);
                fieldDefn->setLength(ogrFieldDefn->GetWidth());
                fieldDefn->setPrecision(ogrFieldDefn->GetPrecision());

                //add field definition to the table definition
                tableDefn->addFieldDefn(fieldDefn.get());
            }

            metadataTable->setMetadataTableDefn(tableDefn.get(),featureLayerObject->getInterface<CORE::IBase>()->getUniqueID().toString());

            OGRLayer* metadataOGRLayer = metadataTable->getInterface<TERRAIN::IOGRLayerHolder>()->getOGRLayer();

            if(!metadataOGRLayer)
            {
                LOG_ERROR("Invalid OGRLayer in metadataTable");
                return NULL;
            }

            metadataOGRLayer->StartTransaction();

            OGRFeature *ogrFeature = NULL;

            //set the reader to the start of the layer
            for(unsigned int i = 0; i < featureList.size(); i++)
            {
                ogrFeature = featureList[i];
                CORE::RefPtr<CORE::IObject> featureObject = ELEMENTS::FeatureObjectUtils::createFeatureObject(ogrFeature);

                // create a metadata record
                CORE::RefPtr<CORE::IMetadataRecord> metadataRecord = metadataCreator->createMetadataRecord();

                metadataRecord->setTableDefn(tableDefn.get());

                //iterate over all fields
                for(int iField = 0; iField < ogrFeatureDef->GetFieldCount(); iField++)
                {
                    CORE::RefPtr<CORE::IMetadataField> field = metadataRecord->getFieldByIndex(iField);

                    CORE::IMetadataFieldDefn::FieldType type = field->getMetadataFieldDef()->getType();

                    switch (type)
                    {
                    case CORE::IMetadataFieldDefn::INTEGER:
                        {
                            field->fromInt(ogrFeature->GetFieldAsInteger(iField));
                            break;
                        }
                    case CORE::IMetadataFieldDefn::DOUBLE:
                        {
                            field->fromDouble(ogrFeature->GetFieldAsDouble(iField));
                            break;
                        }
                    case CORE::IMetadataFieldDefn::STRING:
                        {
                            field->fromString(ogrFeature->GetFieldAsString(iField));
                            break;
                        }
                        /*                        case CORE::IMetadataFieldDefn::DATETIME:
                        {
                        int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0, tzflag = 0;
                        if(ogrFeature->GetFieldAsDateTime(iField, &year, &month, &day, &hour, &min, &sec, &tzflag))
                        {
                        field->fromDateTime(year, month, day, hour, min, sec, tzflag);
                        }
                        break;
                        }
                        */                        default:
                        {
                            field->fromString(ogrFeature->GetFieldAsString(iField));
                            break;
                        }
                    }
                }

                if(featureObject.valid())
                {
                    CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder = featureObject->getInterface<CORE::IMetadataRecordHolder>();

                    recordHolder->setMetadataRecord(metadataRecord.get());
                    featureLayerComposite->addObject(featureObject.get());
                }

                //destroy the feature handle
                OGRFeature::DestroyFeature(ogrFeature);
            }

            metadataOGRLayer->CommitTransaction();
            //featureLayerObject->getInterface<indiCore::ITemporary>()->setTemporary(true);
            return featureLayerObject;
        }
        return NULL;
    }


    void QueryBuilderGUI::_getSelectedLayer()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();
        CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();
        if(iter != map.end())
        {
            _currentLayer = iter->second->getInterface<CORE::IFeatureLayer>();
        }
    }


}