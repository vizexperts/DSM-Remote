
#include <SMCQt/FilesListModel.h>
#include <QtAlgorithms>

namespace SMCQt
{

    bool filesGroupLessThan(const FilesGroup& g1, const FilesGroup& g2)
    {
        QDateTime d1 = QDateTime::fromString(g1.getName(), "MMMM yyyy");
        QDateTime d2 = QDateTime::fromString(g2.getName(), "MMMM yyyy");

        // reverse sorting so that most current month is at top
        return d1 > d2;
    }

    FilesGroup::FilesGroup(QString name)
        : _name(name)
    {

    }

    void FilesGroup::addFile(QString fileName)
    {
        _children.append(fileName);
    }

    QString FilesGroup::getName() const
    {
        return _name;
    }

    void FilesGroup::setName(QString name)
    {
        if(_name != name)
        {
            _name = name;
        }
    }

    QStringList FilesGroup::children() const
    {
        return _children;
    }

    FilesListModel::FilesListModel(QObject* parent) : QAbstractListModel(parent)
    {
 #ifdef USE_QT4
        setRoleNames(roleNames());
#endif
        clear();
    }

    int FilesListModel::rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent)

        return _groups.size();
    }

    QHash<int, QByteArray> FilesListModel::roleNames() const
    {
        QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
        roles.insert(GroupNameRole, QByteArray("groupName"));
        roles.insert(ChildrenRole, QByteArray("groupContents"));
        roles.insert(CountRole, QByteArray("count"));

        return roles;
    }

    void FilesListModel::clear()
    {
        _groups.clear();
    }

    bool operator ==(const FilesGroup& lhs, const FilesGroup& rhs)
    {
        if(lhs.getName() == rhs.getName())
        {
            return true;
        }

        return false;
    }

    void FilesListModel::addFile(QString fileName, QString groupName)
    {
        FilesGroup group(groupName);

        int index = _groups.indexOf(group);
        if(index == -1)
        {
            group.addFile(fileName);
            _groups.push_front(group);
        }
        else
        {
            _groups[index].addFile(fileName);
        }
    }

     

    void FilesListModel::sort()
    {
        qSort(_groups.begin(), _groups.end(), filesGroupLessThan);
    }


    QVariant FilesListModel::data(const QModelIndex &index, int role) const
    {
        if(!index.isValid())
            return QVariant();

        if(index.row() >(_groups.size() - 1))
            return QVariant();

        const FilesGroup& group = _groups.at(index.row());

        switch(role)
        {
        case Qt::DisplayRole:   
        case GroupNameRole: 
            return QVariant::fromValue(group.getName());
        case ChildrenRole:
            return QVariant::fromValue(group.children());
        case CountRole:
            return QVariant::fromValue(group.children().size());
        default:
            return QVariant();
        }
    }
}   //namespace SMCQt