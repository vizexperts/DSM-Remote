#include <SMCQt/VizComboBoxElement.h>

namespace SMCQt{

    VizComboBoxElement::VizComboBoxElement(const QString& name, const QString& uID, QObject *parent)
                            :QObject(parent), m_name(name), m_uID(uID)
    {
    }

    QString
    VizComboBoxElement::name() const
    {
        return m_name;
    }

    void
    VizComboBoxElement::setName(const QString &name)
    {
        if(m_name != name)
        {
            m_name = name;
            emit nameChanged();
        }
    }

    void
    VizComboBoxElement::setUID(const QString &uID)
    {
        if(m_uID != uID)
        {
            m_uID = uID;
            emit isUIDChanged();
        }

    }

    QString
    VizComboBoxElement::uID() const
    {
        return m_uID;
    }
}
