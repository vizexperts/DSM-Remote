/****************************************************************************
*
* File             : ElementClassificationGUI.cpp
* Description      : ElementClassificationGUI class definition
*
*****************************************************************************
* Copyright (c) 2010-2011, CAIR, DRDO
*****************************************************************************/

#include <SMCQt/ElementClassificationGUI.h>
#include <App/AccessElementUtils.h>
#include <Core/IComponent.h>
#include <Elements/FeatureLayerUtils.h>
#include <Core/IVisibility.h>
#include <Core/IVisibilityChangedMessage.h>
#include <Core/IMessageFactory.h>
#include <Core/CoreRegistry.h>

#include <VizUI/UIHandlerManager.h>
#include <VizUI/ICompassUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <App/IApplication.h>
#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>


#include <Terrain/IModelObject.h>
#include <DGN/IDGNFolder.h>
#include <iostream>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ElementClassificationGUI);

    ElementClassificationGUI::ElementClassificationGUI()
    {
    }

    ElementClassificationGUI::~ElementClassificationGUI()
    {
        //setActive(false);
    }

    void ElementClassificationGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if (popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }

    }

    void ElementClassificationGUI::connectPopupLoader(QString type)
    {
        if (type == "elementClassification")
        {
            //setActive(true);
            QObject* elementClassificationObject = _findChild("elementClassification");
            if (elementClassificationObject)
            {
                // start and browse button handlers
                connect(elementClassificationObject, SIGNAL(okButtonClicked()), this, SLOT(_handleCloseButtonClicked()), Qt::UniqueConnection);
            }
        }
    }

    void ElementClassificationGUI::onRemovedFromGUIManager()
    {
        // Unsubscribing from world messages
        CORE::RefPtr<CORE::IWorldMaintainer> wmain =
            APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
        _unsubscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        _unsubscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }


    void ElementClassificationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                APP::IGUIManager* guiMgr = getGUIManager();
                CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                    APP::AccessElementUtils::getWorldMaintainerFromManager(guiMgr);

                _treeModel = new ElementListTreeModel;
                _dgnLayers = new SMCQt::ElementListTreeModelItem;

                connect(_treeModel, SIGNAL(itemChangedSendNotify(CORE::RefPtr<const CORE::IObject>)),
                    SLOT(_itemChangedSendNotify(CORE::RefPtr<const CORE::IObject>)));


                if (wmain.valid())
                {
                    CORE::RefPtr<CORE::IComponent> comp = wmain->getComponentByName("DGNInfoComponent");
                    if (comp.valid())
                    {
                        _dgnInfoComponent = comp->getInterface<ELEMENTS::IDGNInfoComponent>();
                    }
                }
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if (messageType == *CORE::IVisibilityChangedMessage::VisibilityChangedMessageType)
        {
            CORE::RefPtr<CORE::IVisibilityChangedMessage> msg = message.getInterface<CORE::IVisibilityChangedMessage>();
            CORE::RefPtr<const CORE::IObject> object = msg->getObject();

            std::string layerName = object->getInterface<CORE::IBase>()->getName();

            ElementListTreeModelItem* item = NULL;
            item = _findElementInTree(_dgnLayers, object);
            int index = _treeModel->indexOf(item);
            if (item)
            {
                if (item->changingCheckStatus())
                {
                    return;
                }
                _treeModel->toggleManualCheckedState(index);
            }

        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void ElementClassificationGUI::setActive(bool value)
    {
        if (value == getActive())
        {
            return;
        }
        if (value)
        {
            _fillFeatureMap();
            _setup = true;
            _fillClassification();
            _setup = false;

            CORE::RefPtr<APP::IGUI> gui = getGUIManager()->getGUIByClassname("SMCQt::ElementListGUI");
            if (gui.valid())
            {
                _subscribe(gui.get(), *CORE::IVisibilityChangedMessage::VisibilityChangedMessageType);
            }
        }
        else
        {
            _setContextProperty("elementClassificationTreeModel", QVariant::fromValue(NULL));

            // clear treeModel
            _treeModel->clear();
            _dgnLayers->clear();
            _featureMap.clear();

            CORE::RefPtr<APP::IGUI> gui = getGUIManager()->getGUIByClassname("SMCQt::ElementListGUI");
            if (gui.valid())
            {
                _unsubscribe(gui.get(), *CORE::IVisibilityChangedMessage::VisibilityChangedMessageType);
            }
        }
        DeclarativeFileGUI::setActive(value);
    }

    void ElementClassificationGUI::setPrefer3D(bool value)
    {
        //! Get current selected layer.
        CORE::RefPtr<VizUI::ISelectionUIHandler> selUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (!selUIHandler.valid())
            return;

        const CORE::ISelectionComponent::SelectionMap& map =
            selUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        if (iter == map.end())
        {
            return;
        }

        CORE::RefPtr<CORE::IObject> selectedObject = iter->second->getInterface<CORE::IObject>();
        if (!selectedObject.valid())
            return;

        CORE::RefPtr<DGN::IDGNFolder> dgnFolder = selectedObject->getInterface<DGN::IDGNFolder>();
        if (!dgnFolder.valid())
        {
            emit showError("Convert to 3D", "Currently selected object cannot be converted to 3D.");
            return;
        }

        std::string message = std::string("Converting to ") + (value ? "3D" : "2D");

        QObject::connect(this, SIGNAL(executed()), this, SLOT(_toggle3DFinished()), Qt::QueuedConnection);

        /*if (_isToggling2d3d)
            return;

        _isToggling2d3d = true;*/

        //_loadBusyBar(boost::bind(&DGN::IDGNFolder::setPrefer3D, dgnFolder.get(), value), message);

		dgnFolder->setPrefer3D(value);

    }

    void ElementClassificationGUI::_toggle3DFinished()
    {
        _isToggling2d3d = false;

    }

    void ElementClassificationGUI::_handleCloseButtonClicked()
    {

        setActive(false);
    }

    void ElementClassificationGUI::_itemChangedSendNotify(CORE::RefPtr<const CORE::IObject> object)
    {
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*CORE::IVisibilityChangedMessage::VisibilityChangedMessageType);
        msg->getInterface<CORE::IVisibilityChangedMessage>()->setObject(object);
        notifyObservers(*CORE::IVisibilityChangedMessage::VisibilityChangedMessageType, *msg, CORE::IObservable::SYNCHRONOUS);

    }

    ElementListTreeModelItem* ElementClassificationGUI::_findElementInTree(ElementListTreeModelItem* item, const CORE::IObject* object)
    {
        if (!object || !item)
        {
            return NULL;
        }

        const CORE::IObject* curObject = item->getObject();
        if (curObject == object)
        {
            return item;
        }

        foreach(QMLTreeModelItem* childItem, item->getChildren())
        {
            ElementListTreeModelItem* child = dynamic_cast<ElementListTreeModelItem*>(childItem);

            ElementListTreeModelItem* element = _findElementInTree(child, object);
            if (element)
            {
                return element;
            }
        }
        return NULL;
    }

    ElementListTreeModelItem* ElementClassificationGUI::_isAChild(ElementListTreeModelItem* parent, std::string name)
    {
        if (parent)
        {
            foreach(QMLTreeModelItem* childItem, parent->getChildren())
            {
                ElementListTreeModelItem* child = dynamic_cast<ElementListTreeModelItem*>(childItem);
                if (child->getName().toStdString() == name)
                {
                    return child;
                }
            }
        }
        return NULL;
    }

    ElementListTreeModelItem* ElementClassificationGUI::_prepareNewItem(QString name)
    {
        ElementListTreeModelItem* newItem = new SMCQt::ElementListTreeModelItem;
        newItem->setName(name);
        newItem->setCheckable(true);
        newItem->setCheckState(Qt::Unchecked);
        return newItem;
    }

    ElementListTreeModelItem* ElementClassificationGUI::_getOrCreateChild(ElementListTreeModelItem* parent, std::string name)
    {
        if (parent)
        {
            ElementListTreeModelItem* child = _isAChild(parent, name);
            if (!child)
            {
                child = _prepareNewItem(QString(name.c_str()));
                parent->addItem(child);
            }
            return child;
        }
        return NULL;
    }

    void ElementClassificationGUI::_processLayer(std::string layerName, FeatureMapAndFlag& featureLayer, ElementListTreeModelItem* parentItem)
    {
        if (!featureLayer.isProcessed)
        {
            featureLayer.isProcessed = true;

            /////add layer to parent
            ElementListTreeModelItem* layerConstraint = new SMCQt::ElementListTreeModelItem;
            parentItem->addItem(layerConstraint);

            QVariant var(QString::fromStdString(layerName));
            layerConstraint->setSubName(QString::fromStdString(layerName));
            std::string properLayerName;

            if (_dgnInfoComponent->getProperLayerName(layerName, properLayerName))
            {
                layerConstraint->setName(QString(properLayerName.c_str()));
            }
            else
            {
                layerConstraint->setName(layerName.c_str());
            }
            ////layer added

            int totalLayerCount = (featureLayer.featureLayerVector).size();
            int visibleCount = 0;

            for (int i = 0; i < totalLayerCount; i++)
            {
                CORE::RefPtr<CORE::IVisibility> visibility =
                    featureLayer.featureLayerVector[i]->getInterface<CORE::IVisibility>();
                ElementListTreeModelItemDgnSpecific *indivisualLayerItem = new SMCQt::ElementListTreeModelItemDgnSpecific;
                indivisualLayerItem->setObject(featureLayer.featureLayerVector[i]->getInterface<CORE::IObject>());
                CORE::RefPtr<const CORE::IObject> parentObject = featureLayer.featureLayerVector[i]->getInterface<CORE::IObject>()->getParent();
                std::string parentAndItemName = featureLayer.featureLayerVector[i]->getInterface<CORE::IBase>()->getName() + "(" + parentObject->getInterface<CORE::IBase>()->getName() + ")";
                indivisualLayerItem->setName(QString::fromStdString(parentAndItemName));

                layerConstraint->addItem(indivisualLayerItem);

                if (visibility.valid())
                {

                    if (visibility->getVisibility())
                    {
                        visibleCount++;
                    }
                }
            }

            if (visibleCount == 0)
            {
                _setCheckState(layerConstraint, Qt::Unchecked);
            }
            else if (visibleCount == totalLayerCount)
            {
                _setCheckState(layerConstraint, Qt::Checked);
            }
            else
            {
                _setCheckState(layerConstraint, Qt::PartiallyChecked);
            }
        }
    }

    void ElementClassificationGUI::_firstPass()
    {
        _dgnLayers->setName("DGN Layers");
        _dgnLayers->setCheckable(true);
        _dgnLayers->setCheckState(Qt::Unchecked);
        _dgnLayers->setDragEnabled(false);

        const ELEMENTS::IDGNInfoComponent::MajorClassifications& majorClassificationMap = _dgnInfoComponent->getMajorClassificationMap();
        ELEMENTS::IDGNInfoComponent::MajorClassifications::const_iterator iterMajor = majorClassificationMap.begin();

        for (; iterMajor != majorClassificationMap.end(); ++iterMajor)
        {
            ElementListTreeModelItem* majorConstraint = _getOrCreateChild(_dgnLayers, iterMajor->first);

            bool layerProcessed = false;
            const ELEMENTS::IDGNInfoComponent::MinorClassifications& minorClassificationMap = iterMajor->second;
            ELEMENTS::IDGNInfoComponent::MinorClassifications::const_iterator iterMinor = minorClassificationMap.begin();

            for (; iterMinor != minorClassificationMap.end(); ++iterMinor)
            {
                const std::vector<std::string>& layerNames = iterMinor->second;
                std::vector<std::string>::const_iterator iterLayer = layerNames.begin();

                ElementListTreeModelItem* minorConstraint = _getOrCreateChild(majorConstraint, iterMinor->first);
                
                for (; iterLayer != layerNames.end(); ++iterLayer)
                {
                    FeatureMap::iterator iter = _featureMap.find(*iterLayer);
                
                    if (iter != _featureMap.end())
                    {
                        _processLayer(iter->first, iter->second, minorConstraint);
                    }
                }
            
                if (minorConstraint->getChildCount() == 0)
                    majorConstraint->removeItem(minorConstraint);
            }
            
            if (majorConstraint->getChildCount() == 0)
                _dgnLayers->removeItem(majorConstraint);
        }
    }

    void ElementClassificationGUI::_secondPass()
    {
        const std::string UNKNOWN = "Unknown";
        ElementListTreeModelItem* majorUnknownItem = _getOrCreateChild(_dgnLayers, UNKNOWN);
        ElementListTreeModelItem* minorUnknownItem = _getOrCreateChild(majorUnknownItem, UNKNOWN);

        FeatureMap::iterator it = _featureMap.begin();
        for (; it != _featureMap.end(); ++it)
        {
            if (!it->second.isProcessed)
            {
                _processLayer(it->first, it->second, minorUnknownItem);
            }
        }
    }

    void ElementClassificationGUI::_fillClassification()
    {
        if (!_featureMap.empty())
        {
            _firstPass();
            _secondPass();

            _treeModel->addItem(_dgnLayers);
            getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getRootContext()->setContextProperty("elementClassificationTreeModel", _treeModel);
        }
    }

    void ElementClassificationGUI::_fillFeatureMap()
    {
        std::vector<CORE::RefPtr<TERRAIN::IModelObject> > layers;

        CORE::IWorld *world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
        if (world)
        {
            const CORE::ObjectMap& objects = world->getObjectMap();
            CORE::ObjectMap::const_iterator iter = objects.begin();

            while (iter != objects.end())
            {
                CORE::RefPtr<DGN::IDGNFolder> dgFolderObject = iter->second->getInterface<DGN::IDGNFolder>();
                if (dgFolderObject == NULL)
                {
                    ++iter;
                    continue;
                }
                CORE::RefPtr<CORE::ICompositeObject> compositeObject = dgFolderObject->getInterface<CORE::ICompositeObject>();

                const CORE::ICompositeObject::ObjectMap& objectMap = compositeObject->getObjectMap();
                CORE::ICompositeObject::ObjectMap::const_iterator iter2 = objectMap.begin();
                while (iter2 != objectMap.end())
                {
                    CORE::RefPtr<TERRAIN::IModelObject> layer
                        = iter2->second->getInterface<TERRAIN::IModelObject>();
                    if (layer)
                    {
                        layers.push_back(layer);
                    }
                    ++iter2;
                }
                ++iter;
            }
        }
        for (unsigned int i = 0; i < layers.size(); i++)
        {
            CORE::RefPtr<TERRAIN::IModelObject> layer = layers[i];
            if (layer.valid())
            {
                CORE::RefPtr<CORE::IBase> base = layer->getInterface<CORE::IBase>();
                std::string nameOfLayer = UTIL::Trim(base->getName());
                _featureMap[nameOfLayer].isProcessed = false;
                _featureMap[nameOfLayer].featureLayerVector.push_back(layer.get());
            }
        }

    }

    void ElementClassificationGUI::_setCheckState(SMCQt::ElementListTreeModelItem* item, Qt::CheckState checkState)
    {
        item->setCheckState(checkState);
        //item->setCheckState(0, checkState);

        SMCQt::ElementListTreeModelItem* parentItem = dynamic_cast<SMCQt::ElementListTreeModelItem*> (item->parent());
        //QTreeWidgetItem* parentItem = dynamic_cast<QTreeWidgetItem*>(item->parent());
        if (parentItem)
        {
            _updateCheckStatus(parentItem);
        }

        //int childCount = item->getChildCount();
        //int childCount = item->childCount();
        QList<QMLTreeModelItem*> childrenList = item->getChildren();
        if (checkState != Qt::PartiallyChecked)
        {
            for (int i = 0; i < childrenList.size(); i++)
            {
                if (checkState != childrenList[i]->getCheckState())
                {
                    childrenList.at(0)->setCheckState(checkState);
                    //item->child(i)->setCheckState(0, checkState);
                }
            }
        }
    }

    void ElementClassificationGUI::_updateCheckStatus(SMCQt::ElementListTreeModelItem* item)
    {

        bool foundChecked = false, foundUnchecked = false, foundPartial = false;
        QList<QMLTreeModelItem*> childrenList = item->getChildren();
        for (int i = 0; i < childrenList.size(); i++)
        {
            Qt::CheckState childState = childrenList.at(i)->getCheckState();
            if (childState == Qt::Checked)
            {
                foundChecked = true;
            }
            else if (childState == Qt::Unchecked)
            {
                foundUnchecked = true;
            }
            else if (childState == Qt::PartiallyChecked)
            {
                foundPartial = true;
            }
        }


        if (foundChecked && !foundUnchecked && !foundPartial)
        {
            item->setCheckState(Qt::Checked);
        }
        else if (!foundChecked && foundUnchecked && !foundPartial)
        {
            item->setCheckState(Qt::Unchecked);
        }
        else
        {
            item->setCheckState(Qt::PartiallyChecked);
        }

        ElementListTreeModelItem* parentItem = dynamic_cast<ElementListTreeModelItem*>(item->parent());
        if (parentItem)
        {
            _updateCheckStatus(parentItem);
        }

    }

} // namespace SMCQt






