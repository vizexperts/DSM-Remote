/*****************************************************************************
*
* File             : ContextualTabGUI.cpp
* Description      : ContextualTabGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/ContextualTabGUI.h>
#include <SMCQt/PropertyObject.h>

#include <QApplication>

#include <QtCore/QThread>

#include <VizUI/UIHandlerManager.h>
#include <VizUI/IMouseMessage.h>

#include <Core/IWorldMaintainer.h>
#include <Core/IMessage.h>
#include <Core/InterfaceUtils.h>
#include <Core/IPoint.h>
#include <Core/ILine.h>
#include <Core/IPolygon.h>
#include <Core/IFeatureLayer.h>
#include <Core/IIndexable.h>
#include <Core/IMultiPoint.h>
#include <Core/IMultiLine.h>
#include <Core/IMultiPolygon.h>
#include <Core/ITerrain.h>
#include <Core/IWorldMaintainer.h>
#include <Core/WorldMaintainer.h>
#include <Core/IRasterData.h>
#include <Core/ITemporary.h>
#include <Core/IGeoExtent.h>

#include <App/AccessElementUtils.h>

#include <Elements/IMilitaryUnit.h>
#include <Elements/IModelFeatureObject.h>
#include <Elements/IMilitarySymbol.h>
#include <Elements/IOverlayImage.h>
#include <Elements/FenceDrawTechnique.h>
#include <Elements/IEventFeature.h>
#include <Elements/IColorMapRaster.h>

#include <SMCUI/ILineUIHandler.h>
#include <SMCUI/IAreaUIHandler.h>
#include <SMCUI/ILandmarkUIHandler.h>
#include <SMCUI/IMilitarySymbolPointUIHandler.h>
#include <SMCUI/IPointUIHandler2.h>
#include <SMCUI/ITacticalSymbolsUIHandler.h>

#include <Util/IntersectionUtils.h>
#include <Util/CoordinateConversionUtils.h>
#include <Util/GraphicsAlgoUtils.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IVectorObject.h>
#include <Terrain/IElevationObject.h>
#include <Terrain/IModelObject.h>
#include <Terrain/IOEFeatureObject.h>

#include <GIS/IFeatureLayer.h>
#include <GIS/IFeature.h>
#include <GIS/IGeometry.h>
#include <GIS/GeometryType.h>
#include <GIS/IPoint.h>
#include <GIS/ILine.h>
#include <GIS/IPolygon.h>
#include <GIS/ConversionUtil.h>
#include <GIS/IDrawPropertyHolder.h>

#include <DGN/IDGNFolder.h>

#include <TacticalSymbols/ITacticalSymbol.h>

#include <osgDB/FileNameUtils>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ContextualTabGUI);
    DEFINE_IREFERENCED(ContextualTabGUI, DeclarativeFileGUI)

    const std::string ContextualTabGUI::pointHyperlinkActivePropertyName = "pointHyperlinkActive";
    const std::string ContextualTabGUI::pointAttributeActivePropertyName = "pointAttributeActive";
    const std::string ContextualTabGUI::TOOLBOXOBJECTNAME = "toolBox";

    const std::string ContextualTabGUI::AddFeatureToolboxButton = "addFeatureToolboxButton";
    const std::string ContextualTabGUI::RasterReorderToolboxButton = "rasterReorderToolboxButton";
    const std::string ContextualTabGUI::ExportLayerToolboxButton = "exportLayerToolboxButton";
    const std::string ContextualTabGUI::ExportToWebserverToolboxButton = "exportWebserverToolboxButton";
    const std::string ContextualTabGUI::QuerySelectedDataToolboxButton = "queryselectedToolboxButton";
    const std::string ContextualTabGUI::NewRasterWindowWebserverToolboxButton = "tkNewRasterToolboxButton";
    const std::string ContextualTabGUI::OpacitySliderToolboxButton = "opacitySliderToolboxButton";
    const std::string ContextualTabGUI::OverviewCreationToolboxButton = "tkOverviewCreationToolboxButton";
    const std::string ContextualTabGUI::GeneralInfoToolboxButton = "generalInfoToolboxButton";
    const std::string ContextualTabGUI::CreateTMSToolboxButton = "createTmsToolboxButton";
    const std::string ContextualTabGUI::PostGISToolboxButton = "tkPostgisToolboxButton";
    const std::string ContextualTabGUI::RouteCalculationToolboxButton = "routeCalculationToolboxButton";
    const std::string ContextualTabGUI::ExportWebLayerToolboxButton = "exportWebLayerToolboxButton";
    const std::string ContextualTabGUI::PreviewTrackToolboxButton = "previewTrackToolboxButton";
    const std::string ContextualTabGUI::VectorStyleToolboxButton = "vectorStyleToolboxButton";
    const std::string ContextualTabGUI::VectorAttributeToolboxButton = "vectorAttributeTableToolboxButton";
    const std::string ContextualTabGUI::HistogramLegendsToolboxButton = "legendsToolboxButton";
    const std::string ContextualTabGUI::ConnectedFeaturesToolboxButton = "connectedFeaturesToolboxButton";
    const std::string ContextualTabGUI::ConvertTo3DToolboxButton = "convertTo3DToolboxButton";
    const std::string ContextualTabGUI::FlyAroundToolboxButton = "flyaroundToolboxButton";
    const std::string ContextualTabGUI::ApplyDicthToTerrainToolboxButton = "applyDitchToTerrainToolboxButton";
    const std::string ContextualTabGUI::colorLegendToolboxButton = "colorLegendToolboxButton";

    ContextualTabGUI::ContextualTabGUI()
        : _name("")
        , _tabname("")
        , _showGUI(false)
        , _isOpenContextualFromButton(false)
    {
        _addInterface(SMCQt::IContextualTabGUI::getInterfaceName());
    }

    ContextualTabGUI::~ContextualTabGUI()
    {
    }

    void ContextualTabGUI::activateByObjectTypeName(std::string name)
    {
        // TBD
        return;
    }


    void ContextualTabGUI::onAddedToGUIManager()
    {
        QObject* toolBoxObject = _findChild("toolBox");

        QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(updatePosition()), Qt::UniqueConnection);
        //        _timer.start(10);
        QObject::connect(toolBoxObject, SIGNAL(openContextualMenu()), this, SLOT(openContextualMenuFromButton()), Qt::UniqueConnection);

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void ContextualTabGUI::updatePosition()
    {
        if (!_showGUI)
            return;

        if (_name != "")
        {
            QObject* contextualMenu = _findChild("contextualMenu");
            if (contextualMenu)
            {
                // dont update the position if user has dragged the menu
                if (contextualMenu->property("moved").toBool())
                {
                    return;
                }
            }

            _selectTab();
        }
    }

    void ContextualTabGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        //Checking whether the thread is current or not
        if (QThread::currentThread() != QApplication::instance()->thread())
        {
            return;
        }

        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                // Query the UIHandlerManager
                APP::IManager* manager = getGUIManager()->getInterface<APP::IManager>();
                CORE::IWorldMaintainer* maintainer =
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IManager>(manager);

                _subscribe(maintainer, *CORE::IWorld::WorldLoadedMessageType);

                _selectionUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
                if (_selectionUIHandler.valid())
                {
                    _subscribe(_selectionUIHandler.get(), *CORE::ISelectionComponent::MultipleSelectionMessageType);
                }

                CORE::RefPtr<CORE::IComponent> component =
                    CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

                if (component.valid())
                {
                    _selectionComponent = component->getInterface<CORE::ISelectionComponent>();

                    _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::SelectionMessageType);
                    _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ClearSelectionMessageType);
                }

            }
            catch (...)
            {
                // XXX - Ignoring the message here?
            }
        }
        if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            if (_selectionUIHandler.valid())
            {
                _unsubscribe(_selectionUIHandler.get(), *CORE::ISelectionComponent::ObjectLongPressedMessageType);
            }

            if (_showGUI)
            {
                QObject* contextualMenu = _findChild("contextualMenu");
                if (contextualMenu != NULL)
                {
                    QMetaObject::invokeMethod(contextualMenu, "unload");
                }
                _showGUI = false;
            }

            _deactivateUIHandler();

            _tabname = "";
            _name = "";
            _timer.stop();

        }
        if (messageType == *CORE::ISelectionComponent::SelectionMessageType)
        {
            if (_selectionUIHandler.valid())
            {
                _selectObject();

                if (_selectionComponent.valid() && _selectionComponent->getSelectionType() == CORE::ISelectionComponent::SELECTION_TYPE::TERRAIN_SELECTION)
                {
                    if (_selectionUIHandler->getLastButtonMask() & VizUI::IMouse::RIGHT_BUTTON)
                    {
                        _mouseClickLeft = false;
                        _openContextualMenuForSelection();

                    }
                    else if (_selectionUIHandler->getLastButtonMask() & VizUI::IMouse::LEFT_BUTTON)
                    {
                        _mouseClickLeft = true;
                        _openContextualMenuForSelection();
                    }
                }

                // Subscribe to ObjectLongPressedMessageType
                _subscribe(_selectionUIHandler.get(), *CORE::ISelectionComponent::ObjectLongPressedMessageType);
            }
        }
        else if (messageType == *CORE::ISelectionComponent::MultipleSelectionMessageType)
        {
            // handle multiple objects in selectionUIHandler
            if (_selectionUIHandler.valid())
            {
                if (_selectionUIHandler->getLastButtonMask() & VizUI::IMouse::RIGHT_BUTTON)
                {
                    _mouseClickLeft = false;
                }
                else if (_selectionUIHandler->getLastButtonMask() & VizUI::IMouse::LEFT_BUTTON)
                {
                    _mouseClickLeft = true;
                }

                _handleMultipleSelectionMessage();
            }
        }
        else if (messageType == *CORE::ISelectionComponent::ObjectLongPressedMessageType)
        {
            _openContextualMenuForSelection();
        }
        else if (messageType == *CORE::ISelectionComponent::ClearSelectionMessageType)
        {
            if (_selectionUIHandler.valid())
            {
                _unsubscribe(_selectionUIHandler.get(), *CORE::ISelectionComponent::ObjectLongPressedMessageType);
            }

            //Reset ToolBox
            QObject* toolBoxObject = _findChild(TOOLBOXOBJECTNAME);
            if (toolBoxObject != NULL)
            {
                QMetaObject::invokeMethod(toolBoxObject, "resetProperty");

            }

            if (_showGUI)
            {

                QObject* contextualMenu = _findChild("contextualMenu");
                if (contextualMenu != NULL)
                {
                    QMetaObject::invokeMethod(contextualMenu, "unload");
                }

                _showGUI = false;
            }

            _deactivateUIHandler();

            _tabname = "";
            _name = "";
            _timer.stop();
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void ContextualTabGUI::closeContextualTab()
    {
        // close the contextual menu
        QObject* contextualMenu = _findChild("contextualMenu");
        if (contextualMenu)
        {
            QMetaObject::invokeMethod(contextualMenu, "unload");
        }

        _showGUI = false;

    }

    void ContextualTabGUI::_handleMultipleSelectionMessage()
    {
        // first populate the object list from handler
        if (!_selectionUIHandler.valid())
        {
            return;
        }

        std::set<CORE::IObject*> objectList = _selectionUIHandler->getMultipleSelectionList();
        if (objectList.empty())
        {
            return;
        }

        QStringList objectNameList;
        objectNameList.clear();
        std::set<CORE::IObject*>::const_iterator iter = objectList.begin();
        int numObjects = 0;
        for (; iter != objectList.end(); iter++)
        {
            //restricting maximum selection to 5 objects
            if (numObjects == 5)
            {
                break;
            }

            CORE::IObject* object = (*iter);
            if (!object)
            {
                continue;
            }
            numObjects++;
            std::string name = object->getInterface<CORE::IBase>()->getName();
            objectNameList.append(QString::fromStdString(name));
        }

        // set list
        _setContextProperty("multipleSelectionModel", QVariant::fromValue(objectNameList));

        QObject* multipleSelectionMenu = _findChild("multipleSelectionMenu");
        if (multipleSelectionMenu != NULL)
        {
            QObject::connect(multipleSelectionMenu, SIGNAL(highlightItem(int)), this,
                SLOT(highlightItemInMultipleSelection(int)), Qt::UniqueConnection);
            QObject::connect(multipleSelectionMenu, SIGNAL(selectItem(int)), this,
                SLOT(selectItemInMultipleSelection(int)), Qt::UniqueConnection);
            QObject::connect(multipleSelectionMenu, SIGNAL(openMenu(int)), this,
                SLOT(openContextualMenuForItemInMultipleSelection(int)), Qt::UniqueConnection);
            QObject::connect(multipleSelectionMenu, SIGNAL(closeMenu()), this,
                SLOT(closeMultipleSelectionItemMenu()), Qt::UniqueConnection);

            osg::Vec2 p = _selectionUIHandler->getMouseCoordinates();

            int posX = p.x();
            int posY = p.y();

            // set position of popup
            QMetaObject::invokeMethod(multipleSelectionMenu, "loadAt",
                Q_ARG(QVariant, QVariant(int(posX))),
                Q_ARG(QVariant, QVariant(int(posY))));
        }

    }

    void ContextualTabGUI::closeMultipleSelectionItemMenu(){
        _selectionUIHandler->clearCurrentSelection();
    }

    void ContextualTabGUI::highlightItemInMultipleSelection(int index)
    {
        _selectionUIHandler->highlightSelectionInMultipleSelection(index);
    }

    void ContextualTabGUI::selectItemInMultipleSelection(int index)
    {
        _selectionUIHandler->selectObjectInMultipleSelection(index);
    }

    void ContextualTabGUI::openContextualMenuForItemInMultipleSelection(int index)
    {

        _selectionUIHandler->selectObjectInMultipleSelection(index);
        _openContextualMenuForSelection();
    }

    void ContextualTabGUI::openContextualMenuFromButton(){
        _isOpenContextualFromButton = true;
        _mouseClickLeft = false;
        _selectObject();
        _openContextualMenuForSelection();
    }

    void ContextualTabGUI::_openContextualMenuForSelection()
    {
        // Saving the mouse position to use for non-Point objects
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            // Get Application
            CORE::RefPtr<APP::IApplication> application =
                getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            if (!application.valid())
            {
                return;
            }

            // Get composite viewer
            osg::ref_ptr<osgViewer::CompositeViewer> cviewer = application->getCompositeViewer();
            if (!cviewer.valid())
            {
                return;
            }

            // Get View
            osg::ref_ptr<osgViewer::View> view = cviewer->getView(0);
            if (!view.valid())
            {
                return;
            }

            osg::Vec2 mousePos;
            osg::Vec3 latLongAlt;
            //if we are opening context using button then we will use extent's center of geometry
            if (_isOpenContextualFromButton) {
                // Selection Map
                const CORE::ISelectionComponent::SelectionMap& map =
                    selectionUIHandler->getCurrentSelection();

                CORE::ISelectionComponent::SelectionMap::const_iterator iter =
                    map.begin();
                if (iter == map.end())
                {
                    return;
                }

                CORE::RefPtr<CORE::IGeoExtent> objExtents;
                objExtents = iter->second->getInterface<CORE::IGeoExtent>();
                if (!objExtents.valid())
                {
                    return;
                }

                osg::Vec3d tempPos;
                objExtents->getCenter(tempPos);
                _position = tempPos;

            } else {
                mousePos = selectionUIHandler->getMouseCoordinates();
                osg::Vec3 intersectionPoint;
                UTIL::IntersectionUtils::getPickPosition(view.get(), intersectionPoint, mousePos, CORE::TerrainNodeMask);
                latLongAlt = UTIL::CoordinateConversionUtils::ECEFToGeodetic(intersectionPoint);
                _position = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(latLongAlt);
            }

        }

        QObject* quickLaunchBar = _findChild(SMP_QuickLaunchGUI);
        if (!quickLaunchBar)
            return;

        QVariant pointHyperlink = quickLaunchBar->property(pointHyperlinkActivePropertyName.c_str());
        bool pointHyperlinkValue = pointHyperlink.toBool();

        QVariant pointAttribute = quickLaunchBar->property(pointAttributeActivePropertyName.c_str());
        bool pointAttributeValue = pointAttribute.toBool();


        if (_mouseClickLeft){
            if (!pointHyperlinkValue & !pointAttributeValue)
            {
                return;
            }
        }

        //If we are opening contexual Menu from button then make it false
        if (_isOpenContextualFromButton){
            _isOpenContextualFromButton = false;
        }

        //clear the attribute list 
        _attributeList.clear();

        _selectTab();

        _timer.start(10);

    }


    void ContextualTabGUI::_deactivateUIHandler()
    {
        if (_name == ELEMENTS::IMilitarySymbol::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::IMilitarySymbolPointUIHandler> militarySymbolPointUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IMilitarySymbolPointUIHandler>(getGUIManager());
            if (militarySymbolPointUIHandler.valid())
            {
                militarySymbolPointUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE);
            }
        }
        else if (_name == CORE::ILine::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::ILineUIHandler> lineUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getGUIManager());
            if (lineUIHandler.valid())
            {
                lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);
            }
        }
        else if (_name == CORE::IPolygon::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::IAreaUIHandler> areaUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            if (areaUIHandler.valid())
            {
                areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            }
        }
        else if (_name == ELEMENTS::IModelFeatureObject::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::ILandmarkUIHandler> landmarkUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILandmarkUIHandler>(getGUIManager());
            if (landmarkUIHandler.valid())
            {
                landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_NONE);
            }
        }
        else if (_name == CORE::IPoint::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::IPointUIHandler2> pointUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
            if (pointUIHandler.valid())
            {
                pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
            }
        }
    }

    std::string ContextualTabGUI::_identifyObject(CORE::RefPtr<CORE::IObject> object)
    {
        std::string name;

        if (!object.valid())
            return name;

        if (object->getInterface<GIS::IFeatureLayer>())
        {
            name = GIS::IFeatureLayer::getInterfaceName();
        }
        else if (object->getInterface<CORE::IFeatureLayer>())
        {
            name = CORE::IFeatureLayer::getInterfaceName();
        }
        else if (object->getInterface<TERRAIN::IRasterObject>())
        {
            name = TERRAIN::IRasterObject::getInterfaceName();
        }
        else if (object->getInterface<TERRAIN::IElevationObject>())
        {
            name = TERRAIN::IElevationObject::getInterfaceName();
        }
        else if (object->getInterface<ELEMENTS::IMilitarySymbol>())
        {
            name = ELEMENTS::IMilitarySymbol::getInterfaceName();
        }
        else if (object->getInterface<CORE::ILine>())
        {
            name = CORE::ILine::getInterfaceName();
        }
        else if (object->getInterface<CORE::IPolygon>())
        {
            name = CORE::IPolygon::getInterfaceName();
        }
        else if (object->getInterface<ELEMENTS::IModelFeatureObject>())
        {
            name = ELEMENTS::IModelFeatureObject::getInterfaceName();
        }
        else if (object->getInterface<ELEMENTS::IEventFeature>())
        {
            name = ELEMENTS::IEventFeature::getInterfaceName();
        }
        else if (object->getInterface<TERRAIN::IModelObject>())
        {
            name = TERRAIN::IModelObject::getInterfaceName();
        }
        else if (object->getInterface<CORE::IPoint>())
        {
            name = CORE::IPoint::getInterfaceName();
        }
        else if (object->getInterface<DGN::IDGNFolder>())
        {
            name = DGN::IDGNFolder::getInterfaceName();
        }
        else if (object->getInterface<TACTICALSYMBOLS::ITacticalSymbol>())
        {
            name = TACTICALSYMBOLS::ITacticalSymbol::getInterfaceName();
        }

        return name;
    }

    // This function if called on right click. Used to open contextual menu.
    void ContextualTabGUI::_selectTab()
    {
        // Get Application
        CORE::RefPtr<APP::IApplication> application =
            getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
        if (!application.valid())
        {
            return;
        }

        // Get composite viewer
        osg::ref_ptr<osgViewer::CompositeViewer> cviewer = application->getCompositeViewer();
        if (!cviewer.valid())
        {
            return;
        }

        // Get View
        osg::ref_ptr<osgViewer::View> view = cviewer->getView(0);
        if (!view.valid())
        {
            return;
        }


        // Selection UIHandler
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (!selectionUIHandler.valid())
        {
            return;
        }

        // Selection Map
        const CORE::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getOriginalSelectionMap();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if (iter == map.end())
        {
            return;
        }

        CORE::RefPtr<CORE::ITemporary> temp = iter->second->getInterface<CORE::ITemporary>();
        if (temp.valid())
        {
            if (temp->getTemporary())
            {
                return;
            }
        }

        // ! previous position os _screenPos
        osg::Vec3 prevScreenPos = _screenPos;

        // ! previous _tabName
        QString prevTabname = _tabname;

        if (_position == osg::Vec3(0.0, 0.0, 0.0))
        {
            _screenPos = osg::Vec3(950, 450, 0);
        } 
        else
        {
            UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, _position);
        }

        // Set Appropriate _tabName and _screenPos to load contexual menu
        // opens _tabName+"Contextual.qml" at _screenPos
        if (_name == ELEMENTS::IMilitarySymbol::getInterfaceName())
        {
            CORE::RefPtr<CORE::IPoint> point =
                iter->second->getInterface<CORE::IPoint>();

            if (point.valid())
            {
                {
                    osg::Vec3 position = point->getValue();
                    UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, position);
                    _tabname = "MilitarySymbolPoint";
                }
            }
            else
            {
                CORE::RefPtr<CORE::ILine> line =
                    iter->second->getInterface<CORE::ILine>();

                if (line.valid())
                {
                    _tabname = "MilitarySymbolLine";
                }
                else
                {
                    CORE::RefPtr<CORE::IPolygon> polygon =
                        iter->second->getInterface<CORE::IPolygon>();

                    if (polygon.valid())
                    {
                        _tabname = "MilitarySymbolPolygon";
                    }
                }
            }
        }
        else if (_name == CORE::IPoint::getInterfaceName())
        {

            CORE::RefPtr<CORE::IPoint> point =
                iter->second->getInterface<CORE::IPoint>();
            if (point.valid())
            {
                _tabname = "Point";
                osg::Vec3 position = point->getValue();
                UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, position);
                QObject* quickLaunchBar = _findChild(SMP_QuickLaunchGUI);
                if (!quickLaunchBar)
                    return;

                QVariant pointHyperlink = quickLaunchBar->property(pointHyperlinkActivePropertyName.c_str());
                bool pointHyperlinkValue = pointHyperlink.toBool();

                if (pointHyperlinkValue && _mouseClickLeft)
                    _tabname = "Hyperlink";
            }

        }
        else if (_name == CORE::ILine::getInterfaceName())
        {

            CORE::RefPtr<ELEMENTS::FenceDrawTechnique> fenceDrawTechnique =
                dynamic_cast<ELEMENTS::FenceDrawTechnique*>(iter->second.get());

            if (fenceDrawTechnique.valid())
            {
                _tabname = "Fence";
            }
            else
            {
                _tabname = "Line";
            }

        }
        else if (_name == CORE::IPolygon::getInterfaceName())
        {
            _tabname = "Polygon";

            osg::Vec3d point = UTIL::CoordinateConversionUtils::GeodeticToECEF(
                UTIL::CoordinateConversionUtils::longLatHeightToLatLongHeight(_position));

            osg::Vec3d pointOnLine;
            CORE::RefPtr<CORE::IPolygon> polygon = iter->second->getInterface<CORE::IPolygon>();
            if (polygon.valid())
            {
                CORE::RefPtr<CORE::ILine> line = polygon->getExteriorRing();
                if (!line.valid())
                {
                    return;
                }

                int numPoints = line->getPointNumber();
                if (numPoints == 0)
                {
                    CORE::RefPtr<SMCUI::IAreaUIHandler> areaUIHandler =
                        APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
                    if (areaUIHandler.valid())
                    {
                        areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
                    }
                    return;
                }

                osg::ref_ptr<osg::Vec3dArray> points = new osg::Vec3dArray();
                for (unsigned int i = 0; i < line->getPointNumber(); i++)
                {
                    points->push_back(UTIL::CoordinateConversionUtils::GeodeticToECEF(
                        UTIL::CoordinateConversionUtils::longLatHeightToLatLongHeight(line->getValueAt(i))));
                }

                UTIL::GraphicsAlgoUtils::getClosestPointToLine(point, pointOnLine, points.get());

                _position = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(
                    UTIL::CoordinateConversionUtils::ECEFToGeodetic(pointOnLine));
                UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, _position);
            }
            CORE::RefPtr<CORE::ILine> line = iter->second->getInterface<CORE::ILine>();
            if (line.valid())
            {
                osg::ref_ptr<osg::Vec3dArray> points = new osg::Vec3dArray();
                for (unsigned int i = 0; i < line->getPointNumber(); i++)
                {
                    points->push_back(UTIL::CoordinateConversionUtils::GeodeticToECEF(
                        UTIL::CoordinateConversionUtils::longLatHeightToLatLongHeight(line->getValueAt(i))));
                }
                // Find closest point on line
                UTIL::GraphicsAlgoUtils::getClosestPointToLine(point, pointOnLine, points.get());

                _position = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(
                    UTIL::CoordinateConversionUtils::ECEFToGeodetic(pointOnLine));
                UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, _position);
            }
        }
        else if (_name == ELEMENTS::IModelFeatureObject::getInterfaceName())
        {
            CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = iter->second->getInterface<ELEMENTS::IModelFeatureObject>();
            if (model)
            {
                _tabname = "Landmarks";
                CORE::RefPtr<CORE::IPoint> point = model->getInterface<CORE::IPoint>();
                if (point.valid())
                {
                    osg::Vec3 position = point->getValue();
                    UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, position);
                }
            }
        }
        else if (iter->second->getInterface<GIS::IFeature>() != NULL)
        {
            CORE::RefPtr<GIS::IFeature> feature = iter->second->getInterface<GIS::IFeature>();

            CORE::RefPtr<GIS::IGeometry> geometry = feature->getGeometry();
            const GIS::GeometryType& geometryType = geometry->getGeometryType();

            if (geometryType == GIS::PointType)
            {
                _tabname = "Point";

                UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, _position);

            }
            else if (geometryType == GIS::LineType)
            {
                _tabname = "Line";

                GIS::ILine* line = geometry->getInterface<GIS::ILine>();

                osg::Vec3d point, pointOnLine;
                osg::ref_ptr<osg::Vec3dArray> points = new osg::Vec3dArray();
                for (unsigned int i = 0; i < line->getPointCount(); i++)
                {
                    const osg::Vec4d& value = line->getPoint(i);
                    osg::Vec3d ecefPosition;
                    GIS::ConversionUtil::GetECEF(osg::Vec3d(value.x(), value.y(), value.z()), ecefPosition, GIS::WGS_84);
                    points->push_back(ecefPosition);
                }
                // Find closest point on line
                UTIL::GraphicsAlgoUtils::getClosestPointToLine(point, pointOnLine, points.get());

                _position = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(
                    UTIL::CoordinateConversionUtils::ECEFToGeodetic(pointOnLine));
                UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, _position);

            }
            else if (geometryType == GIS::PolygonType)
            {
                _tabname = "Polygon";
            }
            else if (geometryType == GIS::MultiLineType)
            {
                _tabname = "Line";
            }
        }
        else if (iter->second->getInterface<TERRAIN::IOEFeatureObject>() != NULL)
        {
            //feature list only open on left click 
            if (!_mouseClickLeft)
                return; 

            CORE::RefPtr<TERRAIN::IOEFeatureObject> feature = iter->second->getInterface<TERRAIN::IOEFeatureObject>();

            osg::Vec3 position = feature->getFeatureLocation();

            osg::Vec3 latLongAlt = UTIL::CoordinateConversionUtils::ECEFToGeodetic(position);
            _position = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(latLongAlt);

            UTIL::IntersectionUtils::getScreenCoordinatesForLongLatAlt(view.get(), _screenPos, _position);

            _tabname = "Feature";
            _name = "Feature";
        }
        else if (_name == ELEMENTS::IEventFeature::getInterfaceName())
        {
            _tabname = "EventEdit";
        }
        else if (_name == TACTICALSYMBOLS::ITacticalSymbol::getInterfaceName())
        {
            _tabname = "TacticalSymbol";
        }
        else
        {
            _tabname = "";
        }


        // ! return if neither of _tabName not _screenPos has changed
       if ((prevTabname == _tabname) && ((prevScreenPos == _screenPos)))
            return;

        QObject* contextualMenu = _findChild("contextualMenu");
        if (contextualMenu != NULL && _tabname!="TacticalSymbol")
        {
            _showGUI = QMetaObject::invokeMethod(contextualMenu, "load",
                Q_ARG(QVariant, QVariant(_tabname)),
                Q_ARG(QVariant, QVariant(bool(_mouseClickLeft))),
                Q_ARG(QVariant, QVariant(int(_screenPos.x()))),
                Q_ARG(QVariant, QVariant(int(_screenPos.y()))));
        }
		else if(!_mouseClickLeft){
			_showGUI = QMetaObject::invokeMethod(contextualMenu, "load",
				Q_ARG(QVariant, QVariant(_tabname)),
				Q_ARG(QVariant, QVariant(false)),
				Q_ARG(QVariant, QVariant(int(_screenPos.x()))),
				Q_ARG(QVariant, QVariant(int(_screenPos.y()))));
		}
        updatePosition();

    }

    void ContextualTabGUI::_enableToolboxButton(const std::string& buttonName, bool enabled)
    {
        QObject* object = _findChild(buttonName);
        if (object)
        {
            object->setProperty("enabled", QVariant::fromValue(enabled));
        }
    }

    void ContextualTabGUI::_setlayerTypeInToolbox(const std::string& layerType)
    {
        QObject* object = _findChild(TOOLBOXOBJECTNAME);
        if (object)
        {
            object->setProperty("layerType", QVariant::fromValue(QString::fromStdString(layerType)));
        }
    }

    // This function is called on left click. It opens the contextual tab.
    void ContextualTabGUI::_selectObject()
    {
        QObject* toolBoxObject = _findChild(TOOLBOXOBJECTNAME);
        QMetaObject::invokeMethod(toolBoxObject, "resetProperty");

        // Selection UIHandler
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (!selectionUIHandler.valid())
        {
            return;
        }

        // Selection Map
        const CORE::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if (iter == map.end())
        {
            return;
        }

        //! Get the selected object
        CORE::RefPtr<CORE::IObject> selectedObject = iter->second->getInterface<CORE::IObject>();
        if (!selectedObject.valid())
            return;

        _name = _identifyObject(selectedObject);

        // Activate objects usiing UIHandlers 
        if (_name == GIS::IFeatureLayer::getInterfaceName())
        {
            //_enableToolboxButton(ExportLayerToolboxButton, true);
            //_enableToolboxButton(ExportToWebserverToolboxButton, true);
           // _enableToolboxButton(RouteCalculationToolboxButton, true);
           // _enableToolboxButton(VectorAttributeToolboxButton, true);
            
            //_enableToolboxButton(ExportWebLayerToolboxButton, true);
            bool is_line_geometry = false;
            if (iter->second->getInterface<GIS::IFeatureLayer>() != NULL)
            {
                CORE::RefPtr<GIS::IFeatureLayer> featureLayer = iter->second->getInterface<GIS::IFeatureLayer>();

                const GIS::GeometryType& geometryType = featureLayer->getGeometryType();
                std::string layerType;
                if (geometryType == GIS::PointType || geometryType == GIS::MultiPointType)
                {
                    layerType = "Point";
                    //DrawTechnique HistogramDrawProperty
                    CORE::RefPtr<const CORE::IPropertyType> drawPropertyType = featureLayer->getInterface<GIS::IDrawPropertyHolder>()->getDrawPropertyType();
                    if (drawPropertyType.valid())
                    {
                        std::string drawProp = drawPropertyType->getName();
                        if (drawProp.compare("HistogramDrawProperty") == 0)
                        {
                            _enableToolboxButton(HistogramLegendsToolboxButton, true);
                        }
                    }

                }
                else if (geometryType == GIS::LineType || geometryType == GIS::MultiLineType)
                {
                    layerType = "Line";
                    is_line_geometry = true;
                }
                else if (geometryType == GIS::PolygonType || geometryType == GIS::MultiPolygonType)
                {
                    layerType = "Polygon";
                }

                //toolBoxObject->setProperty("layerType", QVariant::fromValue(layerType));
                if (!is_line_geometry) //Temporary Line Style Commented and When Generic 3d Connected Feature Will complete uncomment
                {
                    _setlayerTypeInToolbox(layerType);
                    _enableToolboxButton(VectorStyleToolboxButton, true);
                }

            }
        }
        else if (_name == CORE::IFeatureLayer::getInterfaceName())
        {
            CORE::RefPtr<CORE::IFeatureLayer> layer =
                iter->second->getInterface<CORE::IFeatureLayer>();

            if (layer.valid())
            {
                switch (layer->getFeatureLayerType())
                {
                case CORE::IFeatureLayer::POINT:
                    _enableToolboxButton(AddFeatureToolboxButton, true);
                    _setlayerTypeInToolbox("Point");
                    _enableToolboxButton(ExportLayerToolboxButton, true);
                    _enableToolboxButton(QuerySelectedDataToolboxButton, true);
                    break;
                case CORE::IFeatureLayer::LINE:
                    _enableToolboxButton(AddFeatureToolboxButton, true);
                    _setlayerTypeInToolbox("Line");
                    _enableToolboxButton(ExportLayerToolboxButton, true);
                    break;
                case CORE::IFeatureLayer::POLYGON:
                    _enableToolboxButton(AddFeatureToolboxButton, true);
                    _setlayerTypeInToolbox("Polygon");
                    _enableToolboxButton(ExportLayerToolboxButton, true);
                    _enableToolboxButton(QuerySelectedDataToolboxButton, true);
                    break;
                case CORE::IFeatureLayer::MILITARY_UNIT_POINT:
                    _setlayerTypeInToolbox("MilitaryUnit");
                    break;
                case CORE::IFeatureLayer::MODEL:
                    _enableToolboxButton(AddFeatureToolboxButton, true);
                    _setlayerTypeInToolbox("ModelFeatureObject");
                    _enableToolboxButton(ExportLayerToolboxButton, true);
                    break;
                }
            }
        }
        else if (_name == TERRAIN::IRasterObject::getInterfaceName())
        {
            CORE::RefPtr<TERRAIN::IRasterObject> rasterObject = iter->second->getInterface<TERRAIN::IRasterObject>();
            CORE::RefPtr<ELEMENTS::IColorMapRaster> colorMapRaster = rasterObject->getInterface<ELEMENTS::IColorMapRaster>();
            if (colorMapRaster)
            {
                _enableToolboxButton(colorLegendToolboxButton, true);
            }
            std::string extension = osgDB::getLowerCaseFileExtension(rasterObject->getURL());
            if (extension == "tif" || extension == "tiff" || extension == "ecw" || extension == "img")
            {
                _enableToolboxButton(ExportToWebserverToolboxButton, true);
            }

            // extension = "" means its a directory containing multiple raster layers
            if (extension == "tif" || extension == "tiff" || extension=="ecw" || extension == "")
            {
	            _enableToolboxButton(CreateTMSToolboxButton, true);
            }

            // enabling button for web based data. 
            if (extension == "bing" || extension == "xyz" || extension == "wms")
            {
                _enableToolboxButton(CreateTMSToolboxButton, true);
            }
            _enableToolboxButton(RasterReorderToolboxButton, true);
            _enableToolboxButton(NewRasterWindowWebserverToolboxButton, true);
            _enableToolboxButton(OpacitySliderToolboxButton, true);
            _enableToolboxButton(OverviewCreationToolboxButton, true);
            _enableToolboxButton(GeneralInfoToolboxButton, true);
			
        }

        else if (_name == TERRAIN::IElevationObject::getInterfaceName())
        {
            CORE::RefPtr<CORE::IObject> object = iter->second->getInterface<CORE::IObject>();
            CORE::RefPtr<TERRAIN::IElevationObject> elevObject = iter->second->getInterface<TERRAIN::IElevationObject>();
            std::string extension = osgDB::getLowerCaseFileExtension(elevObject->getURL());
            if (extension == "tif" || extension == "tiff" || extension == "ecw" || extension == "img")
            {
                _enableToolboxButton(ExportToWebserverToolboxButton, true);
            }

            // extension = "" means its a directory containing multiple elevation layers
            if (extension == "tif" || extension == "tiff" || extension == "ecw" || extension == "")
            {
	            _enableToolboxButton(CreateTMSToolboxButton, true);
            }
          
            _enableToolboxButton(PostGISToolboxButton, true);
        }

        else if (_name == ELEMENTS::IMilitarySymbol::getInterfaceName())
        {
            CORE::RefPtr<CORE::IPoint> point =
                iter->second->getInterface<CORE::IPoint>();

            if (point.valid())
            {
                CORE::RefPtr<SMCUI::IMilitarySymbolPointUIHandler> militarySymbolPointUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IMilitarySymbolPointUIHandler>(getGUIManager());
                if (militarySymbolPointUIHandler.valid())
                {
                    militarySymbolPointUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_ATTRIBUTE);
                }
            }

        }
        else if (_name == CORE::IPoint::getInterfaceName())
        {
            CORE::RefPtr<CORE::IPoint> point =
                iter->second->getInterface<CORE::IPoint>();
            if (point.valid())
            {
                _enableToolboxButton(FlyAroundToolboxButton, true);

                CORE::RefPtr<SMCUI::IPointUIHandler2> pointUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
                if (pointUIHandler.valid())
                {
                    pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_ATTRIBUTES);
                }
            }
        }
        else if (_name == CORE::ILine::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::ILineUIHandler> lineUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getGUIManager());
            if (lineUIHandler.valid())
            {
                lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NO_OPERATION);
            }
        }
        else if (_name == CORE::IPolygon::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::IAreaUIHandler> areaUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            if (areaUIHandler.valid())
            {
                areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NO_OPERATION);
            }
        }
        else if (_name == TACTICALSYMBOLS::ITacticalSymbol::getInterfaceName())
        {
            CORE::RefPtr<SMCUI::ITacticalSymbolsUIHandler> tacticalSymbolUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ITacticalSymbolsUIHandler>(getGUIManager());
            if (tacticalSymbolUIHandler.valid())
            {
                tacticalSymbolUIHandler->setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_NO_OPERATION);
            }

        }
        else if (_name == ELEMENTS::IModelFeatureObject::getInterfaceName())
        {
            CORE::RefPtr<ELEMENTS::IModelFeatureObject> model = iter->second->getInterface<ELEMENTS::IModelFeatureObject>();
            if (model)
            {
                _enableToolboxButton(FlyAroundToolboxButton, true);
                _enableToolboxButton(ExportToWebserverToolboxButton, true);
                //CORE::RefPtr<SMCUI::ILandmarkUIHandler> landmarkUIHandler = 
                //    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILandmarkUIHandler>(getGUIManager());
                //if(landmarkUIHandler.valid())
                //{
                //    landmarkUIHandler->setMode(SMCUI::ILandmarkUIHandler::LANDMARK_MODE_EDIT);
                //}
            }
        }

        else if (_name == ELEMENTS::IEventFeature::getInterfaceName())
        {
            //toolBoxObject->setProperty("eventLayerButtonEnable", QVariant::fromValue(true));
        }
        else if (_name == TERRAIN::IModelObject::getInterfaceName())
        {
            _setlayerTypeInToolbox("Model");
            _enableToolboxButton(VectorStyleToolboxButton, true);
            _enableToolboxButton(VectorAttributeToolboxButton, true);
            _enableToolboxButton(ExportToWebserverToolboxButton, true);
           
            CORE::RefPtr<TERRAIN::IModelObject> modelObject =
                iter->second->getInterface<TERRAIN::IModelObject>();
            if (modelObject.valid())
            {
                if (modelObject->getGeometryType() == UTIL::ShpFileUtils::LINE)
                {
                    _enableToolboxButton(RouteCalculationToolboxButton, true);
                    _enableToolboxButton(ConnectedFeaturesToolboxButton, true);
                    _enableToolboxButton(ApplyDicthToTerrainToolboxButton, true);
                    
                }
            }

        }
        else if (_name == DGN::IDGNFolder::getInterfaceName())
        {
            CORE::RefPtr<DGN::IDGNFolder> dgnFolder =
                iter->second->getInterface<DGN::IDGNFolder>();
            if (dgnFolder.valid())
            {
                _enableToolboxButton(ExportToWebserverToolboxButton, true);
                QObject* convertTo3DToolboxButton = _findChild(ConvertTo3DToolboxButton);
                if (convertTo3DToolboxButton)
                {
                    convertTo3DToolboxButton->setProperty("enabled", true);
                    convertTo3DToolboxButton->setProperty("checked", dgnFolder->getPrefer3D());
                }
            }
        }
        else
        {
            QObject* smpMenu = _findChild(SMP_RightMenu);
            if (smpMenu)
            {
                smpMenu->setProperty("minimized", QVariant::fromValue(true));
            }
        }
    }
} // namespace SMCQt