/****************************************************************************
*
* File             : HillshadeCalculationGUI.h
* Description      : HillshadeCalculationGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/
//#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/HillshadeCalculationGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>
#include<iostream>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, HillshadeCalculationGUI);
    DEFINE_IREFERENCED(HillshadeCalculationGUI, DeclarativeFileGUI);

    const std::string HillshadeCalculationGUI::HillshadeAnalysisPopup = "HillshadeAnalysis";

    HillshadeCalculationGUI::HillshadeCalculationGUI()
    {}

    HillshadeCalculationGUI::~HillshadeCalculationGUI()
    {}

    void HillshadeCalculationGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_setProgressValue(int)), this, SLOT(_handleProgressValueChanged(int)), Qt::QueuedConnection);
    }

    void HillshadeCalculationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IHillshadeCalculationUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if (messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if (_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if (line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first = line->getPoint(0)->getValue();
                    osg::Vec3d second = line->getPoint(2)->getValue();
                    QObject* HillshadeAnalysisObject = _findChild(HillshadeAnalysisPopup);
                    if (HillshadeAnalysisObject)
                    {
                        HillshadeAnalysisObject->setProperty("bottomLeftLongitude", UTIL::ToString(first.x()).c_str());
                        HillshadeAnalysisObject->setProperty("bottomLeftLatitude", UTIL::ToString(first.y()).c_str());
                        HillshadeAnalysisObject->setProperty("topRightLongitude", UTIL::ToString(second.x()).c_str());
                        HillshadeAnalysisObject->setProperty("topRightLatitude", UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
        else if (messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage =
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if (filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if (progress < 100)
                {
                    emit _setProgressValue(progress);
                }
            }
            else if (filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                emit _setProgressValue(100);
            }
            else
            {
                if (filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    emit showError("Filter Status", "Failed to apply filter");
                    _handlePBStopClicked();
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void HillshadeCalculationGUI::setActive(bool value)
    {
        if (value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if (value)
            {
                QObject* HillshadeAnalysisObject = _findChild(HillshadeAnalysisPopup);
                if (HillshadeAnalysisObject)
                {
                    // start and stop button handlers
                    connect(HillshadeAnalysisObject, SIGNAL(startAnalysis()), this, SLOT(_handlePBStartClicked()), Qt::UniqueConnection);
                    connect(HillshadeAnalysisObject, SIGNAL(stopAnalysis()), this, SLOT(_handlePBStopClicked()), Qt::UniqueConnection);
                    connect(HillshadeAnalysisObject, SIGNAL(markPosition(bool)), this, SLOT(_handlePBAreaClicked(bool)), Qt::UniqueConnection);

                    // enable start button
                    HillshadeAnalysisObject->setProperty("isStartButtonEnabled", true);

                    //disable stop button
                    HillshadeAnalysisObject->setProperty("isStopButtonEnabled", false);

                    //disable progress value
                    HillshadeAnalysisObject->setProperty("progressValue", 0);
                }
            }
            else
            {
                _reset();
                _handlePBStopClicked();
            }
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if (value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void HillshadeCalculationGUI::_handleProgressValueChanged(int value)
    {
        QObject* HillshadeAnalysisObject = _findChild(HillshadeAnalysisPopup);
        if (HillshadeAnalysisObject)
        {
            HillshadeAnalysisObject->setProperty("progressValue", QVariant::fromValue(value));
        }
    }

    //! 
    void HillshadeCalculationGUI::_handlePBStartClicked()
    {
        if (!_uiHandler.valid())
        {
            return;
        }
        QObject* HillshadeAnalysisObject = _findChild(HillshadeAnalysisPopup);
        if (HillshadeAnalysisObject)
        {
            std::string name = HillshadeAnalysisObject->property("outputName").toString().toStdString();

            if (name == "")
            {
                showError("Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            HillshadeAnalysisObject->setProperty("outputName", QString::fromStdString(name));
            _uiHandler->setOutputName(name);

            //get Elevation
            QString QStringZenithAngle = HillshadeAnalysisObject->property("zenithVal").toString();
            if (QStringZenithAngle == "")
            {
                showError("Insufficient / Incorrect data", "Zenith Angle Value is not specified");
                return;
            }
            double ZenithAngle = QStringZenithAngle.toDouble();

            if (ZenithAngle < 0 || ZenithAngle > 90)
            {
                showError("Insufficient / Incorrect data", "Please enter a Zenith Angle Between 0 - 90 ");
                return;
            }
            _uiHandler->setZenithAngle(ZenithAngle);

            //get offset
            QString QStringAzimuthAngle = HillshadeAnalysisObject->property("azimuthVal").toString();
            if (QStringAzimuthAngle == "")
            {
                showError("Insufficient / Incorrect data", "azimuth Value is not specified");
                return;
            }
            double AzimuthAngle = QStringAzimuthAngle.toDouble();;
            if (AzimuthAngle < 0 || AzimuthAngle > 360)
            {
                showError("Insufficient / Incorrect data", "Please enter a azimuth Value Value between 0-360 ");
                return;
            }
            _uiHandler->setAzimuthAngle(AzimuthAngle);

            //get Transparency
            QString QStringTransparency = HillshadeAnalysisObject->property("alphaChannelVal").toString();
            if (QStringTransparency == "")
            {
                showError("Insufficient / Incorrect data", "Transparency Value is not specified");
                return;
            }
            double Transparency = QStringTransparency.toDouble();;
            if (Transparency < 0 || Transparency > 255)
            {
                showError("Insufficient / Incorrect data", "Please enter a Transparency Value between 0-255  ");
                return;
            }

            _uiHandler->setTransparency(Transparency / 255.0);
            //colo set
            QColor rectColor = HillshadeAnalysisObject->property("rectColor").value<QColor>();
            osg::Vec4d ocolor = osg::Vec4d(rectColor.red() / 255.0, rectColor.green() / 255.0, rectColor.blue() / 255.0, rectColor.alpha() / 255.0);
            _uiHandler->setUserDefineColor(ocolor);

            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = HillshadeAnalysisObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = HillshadeAnalysisObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = HillshadeAnalysisObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() = HillshadeAnalysisObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if (!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError("Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if (completeAreaSpecified)
                {
                    if (extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
                        showError("Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                }
            }

            HillshadeAnalysisObject->setProperty("progressValue", 0);
            _handlePBAreaClicked(false);
            // start filter

            _uiHandler->execute();
        }
    }

    void HillshadeCalculationGUI::_handlePBStopClicked()
    {
        if (!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();
        QObject* HillshadeAnalysisObject = _findChild(HillshadeAnalysisPopup);
        if (HillshadeAnalysisObject)
        {
            //disable stop button
            HillshadeAnalysisObject->setProperty("isStopButtonEnabled", false);
            HillshadeAnalysisObject->setProperty("isStartButtonEnabled", true);
            HillshadeAnalysisObject->setProperty("progressValue", QVariant::fromValue(0));
        }
    }

    void HillshadeCalculationGUI::_handlePBRunInBackgroundClicked()
    {}

    void HillshadeCalculationGUI::_handlePBAreaClicked(bool pressed)
    {
        if (!_areaHandler.valid())
        {
            return;
        }

        if (pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

            QObject* HillshadeAnalysisObject = _findChild(HillshadeAnalysisPopup);
            if (HillshadeAnalysisObject)
            {
                HillshadeAnalysisObject->setProperty("isMarkSelected", false);
            }
        }
    }

    void HillshadeCalculationGUI::_reset()
    {
        QObject* HillshadeAnalysisObject = _findChild(HillshadeAnalysisPopup);
        if (HillshadeAnalysisObject)
        {
            HillshadeAnalysisObject->setProperty("isMarkSelected", false);
            HillshadeAnalysisObject->setProperty("bottomLeftLongitude", "longitude");
            HillshadeAnalysisObject->setProperty("bottomLeftLatitude", "latitude");
            HillshadeAnalysisObject->setProperty("topRightLongitude", "longitude");
            HillshadeAnalysisObject->setProperty("topRightLatitude", "latitude");
            HillshadeAnalysisObject->setProperty("outputName", "outputName");
            HillshadeAnalysisObject->setProperty("azimuthVal", 0);
            HillshadeAnalysisObject->setProperty("zenithVal", 0);
            HillshadeAnalysisObject->setProperty("progressValue", 0);
            _handlePBAreaClicked(false);
        }
    }

}//namespace SMCQt
