/****************************************************************************
*
* File             : CreateFeatureGUI.h
* Description      : CreateFeatureGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <SMCQt/CreateFeatureGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IDeletable.h>
#include <Core/ITerrain.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IText.h>
#include <Core/IWorld.h>
#include <Core/IPolygon.h>
#include <Core/ICompositeObject.h>
#include <Core/AttributeTypes.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/IPenStyle.h>
#include <Core/IBrushStyle.h>
#include <Core/CoreRegistry.h>
#include <Core/IProperty.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>
#include <Elements/IClamper.h>
#include <Elements/ElementsUtils.h>
#include <App/IUIHandler.h>

#include <Util/StringUtils.h>

#include <SMCElements/ILayerComponent.h>

#include <SMCUI/IFeatureExportUIHandler.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateFeatureGUI);
    DEFINE_IREFERENCED(CreateFeatureGUI, SMCQt::DeclarativeFileGUI);

    CreateFeatureGUI::CreateFeatureGUI()
    {
    }

    CreateFeatureGUI::~CreateFeatureGUI()
    {
    }

    void CreateFeatureGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }
    }

    void CreateFeatureGUI::connectSmpMenu(QString type)
    {
        if((type == "lineFeaturesMenu"))
        {
            _selectedFeatureGeometry = "Line";
            QObject* lineFeaturesMenu = _findChild("lineFeaturesMenu");
            if(lineFeaturesMenu != NULL)
            {
                QObject::connect(lineFeaturesMenu, SIGNAL(setFeatureEnable(QString)), this, 
                    SLOT(createLineFeatureType(QString)), Qt::UniqueConnection);
            }
        }
        else if(type == "areaFeaturesMenu")
        {
            _selectedFeatureGeometry = "Area";
            QObject* areaFeaturesMenu = _findChild("areaFeaturesMenu");
            if(areaFeaturesMenu != NULL)
            {
                QObject::connect(areaFeaturesMenu, SIGNAL(setFeatureEnable(QString)), this, 
                    SLOT(createAreaFeatureType(QString)), Qt::UniqueConnection);
            }
        }
    }

    void CreateFeatureGUI::createLineFeatureType(QString type)
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }

        if(!_lineUIHandler.valid())
        {
            return;
        }

        _selectedFeatureType = type.toStdString();

        if(type == "")
        {
            if(_lineUIHandler->getMode() == SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE)
                _lineUIHandler->removeCreatedLine();

            _lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);

            _lineUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            _unsubscribe(_lineUIHandler.get(), *SMCUI::ILineUIHandler::LineCreatedMessageType);
        }
        else
        {
            _lineUIHandler->setClampingState(true);

            _lineUIHandler->setTemporaryState(true);
            _lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE);
            _lineUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _subscribe(_lineUIHandler.get(), *SMCUI::ILineUIHandler::LineCreatedMessageType);
        }
    }

    void CreateFeatureGUI::createAreaFeatureType(QString type)
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }

        if(!_lineUIHandler.valid())
        {
            return;
        }

        _selectedFeatureType = type.toStdString();

        if(type == "")
        {
            if(_areaUIHandler->getMode() == SMCUI::IAreaUIHandler::AREA_MODE_CREATE_AREA)
                _areaUIHandler->removeCreatedArea();

            _areaUIHandler->setTemporaryState(false);
            _areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _areaUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            _unsubscribe(_areaUIHandler.get(), *SMCUI::IAreaUIHandler::AreaCreatedMessageType);
        }
        else
        {
            //            _lineUIHandler->setClampingState(true);

            _areaUIHandler->setTemporaryState(true);
            _areaUIHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_AREA);
            _areaUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _subscribe(_areaUIHandler.get(), *SMCUI::IAreaUIHandler::AreaCreatedMessageType);
        }
    }

    void CreateFeatureGUI::_loadUIHandlers()
    {
        _lineUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getGUIManager());

        _areaUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());        

        _createFeatureUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ICreateFeatureUIHandler>(getGUIManager());

        _pointUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
    }

    void CreateFeatureGUI::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _loadUIHandlers();

                //subscribing to world messages
                _subscribe(wmain, *CORE::IWorld::WorldLoadedMessageType);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *SMCUI::ILineUIHandler::LineCreatedMessageType)
        {
            CORE::RefPtr<CORE::ILine> line =  _lineUIHandler->getCurrentLine();
            if(line.valid())
            {
                if(line->getPointNumber() > 1)
                {
                    _populateAttributes();

                    _createMetadataRecord();

                    CORE::RefPtr<CORE::IFeatureLayer> featureLayer = 
                        _createFeatureUIHandler->getFeatureLayer(_selectedFeatureType);

                    _lineUIHandler->addToLayer(featureLayer);

                    _lineUIHandler->_reset();

                    QObject* lineFeaturesMenu = _findChild("lineFeaturesMenu");
                    if ( lineFeaturesMenu )
                    {
                        QMetaObject::invokeMethod( lineFeaturesMenu, "resetLineFeatures" );
                    }
                }
                else
                {
                    emit showError("Create Line", "Cannot create line with less than 2 points.", true);
                    _lineUIHandler->removeCreatedLine();
                }
            }
        }
        else if(messageType == *SMCUI::IAreaUIHandler::AreaCreatedMessageType)
        {
            CORE::RefPtr<CORE::IPolygon> area = _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                if(_areaUIHandler->getNumPoints() > 2)
                {
                    _populateAttributes();
                    _createMetadataRecord();

                    ELEMENTS::IClamper* clamper = area->getInterface<ELEMENTS::IClamper>();
                    if(clamper)
                    {
                        clamper->setClampOnTileLoading(true);
                    }

                    CORE::RefPtr<CORE::IFeatureLayer> featureLayer = 
                        _createFeatureUIHandler->getFeatureLayer(_selectedFeatureType);

                    _areaUIHandler->addToLayer(featureLayer);

                    _areaUIHandler->reset();

                    QObject* areaFeaturesMenu = _findChild("areaFeaturesMenu");
                    if ( areaFeaturesMenu )
                    {
                        QMetaObject::invokeMethod( areaFeaturesMenu, "resetAreaFeatures" );
                    }
                }                
                else
                {
                    emit showError("Create Area", "Cannot create area with less than 3 points.");
                    _areaUIHandler->removeCreatedArea();
                }
            }
        }

    }

    void CreateFeatureGUI::_createMetadataRecord()
    {
        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = ELEMENTS::getMetaDataCreator();

        if(!metadataCreator.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = NULL;
        {
            try
            {
                CORE::RefPtr<CORE::IFeatureLayer> featureLayer = 
                    _createFeatureUIHandler->getFeatureLayer(_selectedFeatureType);

                if(featureLayer.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                        featureLayer->getInterface<CORE::IMetadataTableHolder>(true);

                    if(holder.valid())
                    {
                        tableDefn = holder->getMetadataTable()->getMetadataTableDefn();
                    }
                }

            }
            catch(UTIL::Exception &e)
            {
                e.LogException();
            }
        }

        if(!tableDefn.valid())
        {
            return;
        }

        //create metadata record for the min max point
        CORE::RefPtr<CORE::IMetadataRecord> record = 
            metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        CORE::RefPtr<CORE::IMetadataField> field = 
            record->getFieldByName("NAME");
        if(field)
        {
            field->fromString(_selectedFeatureType);
        }

        if(_selectedFeatureGeometry == "Line")
        {
            _lineUIHandler->setMetadataRecord(record.get());
        }
        else if(_selectedFeatureGeometry == "Area")
        {
            _areaUIHandler->setMetadataRecord(record.get());
        }
        else
        {
            _pointUIHandler->setMetadataRecord(record.get());
        }

    }

    void CreateFeatureGUI::_populateAttributes()
    {
        if(_selectedFeatureGeometry == "Line")
        {
            CORE::RefPtr<CORE::ILine> line =  _lineUIHandler->getCurrentLine();
            if(line.valid())
            {
                CORE::RefPtr<CORE::IFeatureLayer> featureLayer = 
                    _createFeatureUIHandler->getFeatureLayer(_selectedFeatureType);
                if(!featureLayer.valid())
                {
                    return;
                }

                CORE::RefPtr<CORE::IPenStyle> penStyle = line->getInterface<CORE::IPenStyle>();
                if(penStyle.valid())
                {
                    penStyle->setPenColor(featureLayer->getInterface<CORE::IPenStyle>()->getPenColor());
                    penStyle->setPenWidth(featureLayer->getInterface<CORE::IPenStyle>()->getPenWidth());
                }

                line->getInterface<CORE::IBase>()->setName(_selectedFeatureType);
            }            
        }
        else if(_selectedFeatureGeometry == "Area")
        {
            CORE::RefPtr<CORE::IPolygon> area =  _areaUIHandler->getCurrentArea();
            if(area.valid())
            {
                CORE::RefPtr<CORE::IFeatureLayer> featureLayer = 
                    _createFeatureUIHandler->getFeatureLayer(_selectedFeatureType);
                if(!featureLayer.valid())
                {
                    return;
                }

                CORE::RefPtr<CORE::IPenStyle> penStyle = area->getInterface<CORE::IPenStyle>();
                if(penStyle.valid())
                {
                    penStyle->setPenColor(featureLayer->getInterface<CORE::IPenStyle>()->getPenColor());
                    penStyle->setPenWidth(featureLayer->getInterface<CORE::IPenStyle>()->getPenWidth());
                }

                CORE::RefPtr<CORE::IBrushStyle> brushStyle = area->getInterface<CORE::IBrushStyle>();
                if(brushStyle.valid())
                {
                    brushStyle->setBrushFillColor(featureLayer->getInterface<CORE::IBrushStyle>()->getBrushFillColor());
                }

                area->getInterface<CORE::IBase>()->setName(_selectedFeatureType);
            }            
        }
    }
}