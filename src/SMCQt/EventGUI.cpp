/****************************************************************************
*
* File             : ElevationProfileGUI.cpp
* Description      : ElevationProfileGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/EventGUI.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>
#include <Core/ICompositeObject.h>
#include <Core/IProperty.h>
#include <SMCElements/IDataSourceComponent.h>
#include <serialization/IProjectLoaderComponent.h>

#include <VizQt/QtUtils.h>

#include <QFileDialog>


#include <Effects/IEventPlayer.h>
#include <Effects/EffectPlugin.h>
#include <Effects/IEvent.h>
#include <Effects/IInfoEvent.h>
#include <Effects/IExplosionEvent.h>
#include <Effects/IAlarmEvent.h>
#include <Effects/IGunfireEvent.h>
#include <Effects/IEventType.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IMilitarySymbol.h>
#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>
#include <Elements/IEventFeature.h>
#include <Core/IDeletable.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, EventGUI);
    DEFINE_IREFERENCED(EventGUI, SMCQt::DeclarativeFileGUI);

    EventGUI::EventGUI()
        :_addEventGUI(NULL)
        //, _infoEventModel(new InfoEventModel)
    {}

    EventGUI::~EventGUI()
    {
        //delete _infoEventModel;
    }

    void EventGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if (popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void EventGUI::closeIconClicked()
    {
        if (_addedEvent.valid())
        {
            _addedEvent->stop();
        }

        // reset the mouse handler and remove the event.
        _eventUIHandler->removeCreatedEvent();
        _eventUIHandler->reset();
        _addedEvent = NULL;
    }
    void EventGUI::setActive(bool value)
    {
        connectPopupLoader("addEventGUI");
    }
    void EventGUI::connectPopupLoader(QString type)
    {
        if (type == "addEventGUI")
        {
            QObject* addEventGUI = _findChild("addEventGUI");
            previewCreated = false;
            editingEvent = false;
            _addedEvent = NULL;

            if (addEventGUI)
            {
                const boost::posix_time::ptime& currentDateTime =
                    _animationComponent->getCurrentDateTime();

                QDateTime qCurrentDateTime;
                VizQt::TimeUtils::BoostToQtDateTime(currentDateTime, qCurrentDateTime);

                QObject::connect(addEventGUI, SIGNAL(handleOkButtonClicked()),
                    this, SLOT(handleOkButtonClicked()), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(handleCancelButtonClicked()),
                    this, SLOT(handleCancelButtonClicked()), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(attachFile()),
                    this, SLOT(attachInfoEventImageFile()), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(attachAudioFile()),
                    this, SLOT(attachInfoEventAudioFile()), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(attachVideoFile()),
                    this, SLOT(attachInfoEventVideoFile()), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(handleMarkButtonClicked(bool)),
                    this, SLOT(handleMarkButtonClicked(bool)), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(handlePreviewButtonClicked(bool)),
                    this, SLOT(handlePreviewButtonClicked(bool)), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(handleExplosionScaleChanged(double)),
                    this, SLOT(handleExplosionScaleChanged(double)), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(handleEventTypeChanged()),
                    this, SLOT(handleEventTypeChanged()), Qt::UniqueConnection);

                QObject::connect(addEventGUI, SIGNAL(closed()), this,
                    SLOT(closeIconClicked()), Qt::UniqueConnection);

                addEventGUI->setProperty("arrivalTime", QVariant::fromValue(qCurrentDateTime));
                addEventGUI->setProperty("departureTime", QVariant::fromValue(qCurrentDateTime));
            }
        }
    }

    void EventGUI::handleExplosionScaleChanged(double explosionScale)
    {
        if (_addedEvent.valid())
        {
            CORE::RefPtr<EFFECTS::IExplosionEvent> addedExplosionEvent =
                _addedEvent->getInterface<EFFECTS::IExplosionEvent>();

            // stop the event first because we cannont change parameter during the animation
            _addedEvent->stop();
            if (addedExplosionEvent.valid())
            {
                addedExplosionEvent->setExplosionScale(explosionScale);
            }

            // start back the animation only if the preview button is selected
            QObject* addEventGUI = _findChild("addEventGUI");
            bool previewSelected = addEventGUI->property("previewButtonSelected").toBool();
            if (previewSelected)
            {
                _addedEvent->start(EFFECTS::IEvent::FORWARD);
            }
        }
    }

    void EventGUI::handleCancelButtonClicked()
    {
        QObject* addEventGUI = _findChild("addEventGUI");
        if (!editingEvent)
        {
            // stop the event.
            if (_addedEvent.valid())
            {
                _addedEvent->stop();
            }

            // reset the mouse handler and remove the event.
            _eventUIHandler->removeCreatedEvent();
            _eventUIHandler->reset();
            _addedEvent = NULL;

            // unload the qml
            if (addEventGUI)
            {
                QMetaObject::invokeMethod(addEventGUI, "closePopup");
            }
        }
        else
        {
            //QObject* editEvent = _findChild("editEvent");
            //QMetaObject::invokeMethod(editEvent, "deselectButton");
        }
    }

    void EventGUI::handlePreviewButtonClicked(bool showPreview)
    {
        QObject* addEventGUI = _findChild("addEventGUI");

        // if preview not created, create one
        if (!previewCreated)
        {
            bool result = false;

            if (addEventGUI)
            {
                std::string eventType = addEventGUI->property("eventType").toString().toStdString();
                if (eventType == "")
                {
                    showError("Add Event", "No type selected");
                    addEventGUI->setProperty("markButtonSelected", false);
                    addEventGUI->setProperty("previewButtonSelected", false);
                    return;
                }
                else if (eventType == "Info")
                {
                    result = configureInfoEvent();
                }

                else if (eventType == "Explosion")
                {
                    result = configureExplosionEvent();
                }

                else if (eventType == "Alarm")
                {
                    result = configureAlarmEvent();
                }
                else if (eventType == "Gunfire")
                {
                    result = configureGunfireEvent();
                }
            }

            // if preview creation was sucessful
            if (result)
            {
                previewCreated = true;
            }
        }

        if (previewCreated  && _addedEvent.valid())
        {
            // enable the preview
            if (showPreview)
            {
                isPreview = true;
                /*QObject* infoContainerObject = _findChild("infoEventContainer");
                    QVariant retunedVal;
                    QVariant msg = "enable";
                    QMetaObject::invokeMethod(infoContainerObject, "enable", Q_RETURN_ARG(QVariant, retunedVal), Q_ARG(QVariant, msg));*/
                addEventGUI->setProperty("markButtonSelected", false);
                _addedEvent->start(EFFECTS::IEvent::FORWARD);
            }

            // disable the preview
            else
            {
                _addedEvent->stop();
            }
        }
    }

    bool EventGUI::configureGunfireEvent()
    {
        QObject* addEventGUI = _findChild("addEventGUI");
        if (addEventGUI)
        {
            // get the start and end time
            QDateTime startDateTime, endDateTime;

            startDateTime = addEventGUI->property("arrivalTime").toDateTime();
            endDateTime = addEventGUI->property("departureTime").toDateTime();

            boost::posix_time::ptime boostStartTime = VizQt::TimeUtils::QtToBoostDateTime(startDateTime);
            boost::posix_time::ptime boostEndTime = VizQt::TimeUtils::QtToBoostDateTime(endDateTime);
            if (boostStartTime >= boostEndTime)
            {
                showError("Add Event", "End time must be greater than the Start time");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            std::string eventName = addEventGUI->property("eventName").toString().toStdString();
            if (eventName == "")
            {
                showError("Add Gunfire Event", "Enter the Name for the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            _addedEvent = _eventUIHandler->addEvent(eventName, "Gunfire", boostStartTime, boostEndTime);

            if (!(_addedEvent.valid()))
            {
                showError("Add Event", "Please mark the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            CORE::RefPtr<EFFECTS::IGunfireEvent> addedGunfireEvent =
                _addedEvent->getInterface<EFFECTS::IGunfireEvent>();

            if (!addedGunfireEvent.valid())
            {
                showError("Add Event", "Invalid Event Added. Please try again.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }
            return true;
        }
        return false;
    }

    bool EventGUI::configureAlarmEvent()
    {
        QObject* addEventGUI = _findChild("addEventGUI");
        if (addEventGUI)
        {
            // get the start and end time
            QDateTime startDateTime, endDateTime;

            startDateTime = addEventGUI->property("arrivalTime").toDateTime();
            endDateTime = addEventGUI->property("departureTime").toDateTime();

            boost::posix_time::ptime boostStartTime = VizQt::TimeUtils::QtToBoostDateTime(startDateTime);
            boost::posix_time::ptime boostEndTime = VizQt::TimeUtils::QtToBoostDateTime(endDateTime);
            if (boostStartTime >= boostEndTime)
            {
                showError("Add Event", "End time must be greater than the Start time");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            std::string eventName = addEventGUI->property("eventName").toString().toStdString();
            if (eventName == "")
            {
                showError("Add Alarm Event", "Enter the Name for the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            _addedEvent = _eventUIHandler->addEvent(eventName, "Alarm", boostStartTime, boostEndTime);

            if (!(_addedEvent.valid()))
            {
                showError("Add Event", "Please mark the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            CORE::RefPtr<EFFECTS::IAlarmEvent> addedAlarmEvent = _addedEvent->getInterface<EFFECTS::IAlarmEvent>();
            if (!addedAlarmEvent.valid())
            {
                showError("Add Event", "Invalid Event Added. Please try again.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }
            return true;
        }
        return false;
    }

    void EventGUI::handleEventTypeChanged()
    {
        if (_addedEvent.valid())
        {
            previewCreated = false;
            _addedEvent->stop();
            _eventUIHandler->removeCreatedEvent();
            _eventUIHandler->reset();

            // remove the event
            // TBD

            QObject* addEventGUI = _findChild("addEventGUI");
            addEventGUI->setProperty("markButtonSelected", false);
            addEventGUI->setProperty("previewButtonSelected", false);
        }
    }

    void EventGUI::closePreview(){
        // stop the event

        if (_addedEvent.valid())
        {
            _addedEvent->stop();
        }

        QObject* addEventGUI = _findChild("addEventGUI");

        if (addEventGUI)
            addEventGUI->setProperty("previewButtonSelected", false);

        QObject* editEventGUI = _findChild("editEvent");

        if (editEventGUI)
            editEventGUI->setProperty("previewButtonSelected", false);
    }

    void EventGUI::handleOkButtonClicked()
    {
        // not editing the event. i.e creating a new event.
        if (!editingEvent)
        {
            bool result = false;

            if (!previewCreated)
            {
                QObject* addEventGUI = _findChild("addEventGUI");
                if (addEventGUI)
                {
                    std::string eventType = addEventGUI->property("eventType").toString().toStdString();
                    if (eventType == "")
                    {
                        showError("Add Event", "No type selected");
                        addEventGUI->setProperty("markButtonSelected", false);
                        addEventGUI->setProperty("previewButtonSelected", false);
                        return;
                    }
                    else if (eventType == "Info")
                    {
                        result = configureInfoEvent();
                    }

                    else if (eventType == "Explosion")
                    {
                        result = configureExplosionEvent();
                    }

                    else if (eventType == "Alarm")
                    {
                        result = configureAlarmEvent();
                    }
                    else if (eventType == "Gunfire")
                    {
                        result = configureGunfireEvent();
                    }
                }
            }
            if (result || previewCreated)
            {
                QObject* addEventGUI = _findChild("addEventGUI");
                if (_addedEvent.valid())
                {
                    _addedEvent->stop();
                }
                if (addEventGUI)
                {
                    _eventUIHandler->reset();
                    QMetaObject::invokeMethod(addEventGUI, "closePopup");
                }
                _eventUIHandler->reset();
            }
        }
        // editing the event
        else
        {
            if (!_selectionComponent.valid())
                return;

            const CORE::ISelectionComponent::SelectionMap& selectionMap =
                _selectionComponent->getCurrentSelection();

            if (selectionMap.empty())
            {
                return;
            }

            CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
            CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

            ELEMENTS::IEventFeature *eventFeature = selectedObject->getInterface<ELEMENTS::IEventFeature>();
            CORE::RefPtr<EFFECTS::IEventPlayer> player = eventFeature->getEventPlayer();
            EFFECTS::IEventPlayer::EventInfoList *eventList =
                const_cast<EFFECTS::IEventPlayer::EventInfoList*>(&(player->getEventInfoList()));

            EFFECTS::IEventPlayer::EventInfoList::const_iterator iter = eventList->begin();
            if (iter == eventList->end())
            {
                return;
            }

            // set the changed value in the window to the event
            QObject* addEventGUI = _findChild("editEvent");

            // get and set the name
            QString eventName = addEventGUI->property("eventName").toString();
            if (eventName == "")
            {
                showError("Add Explosion Event", "Enter the Name for the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                return;
            }
            eventFeature->getInterface<CORE::IBase>()->setName(eventName.toStdString());

            // get and set the time of the event
            QDateTime startDateTime, endDateTime;

            startDateTime = addEventGUI->property("arrivalTime").toDateTime();
            endDateTime = addEventGUI->property("departureTime").toDateTime();

            boost::posix_time::ptime boostStartTime = VizQt::TimeUtils::QtToBoostDateTime(startDateTime);
            boost::posix_time::ptime boostEndTime = VizQt::TimeUtils::QtToBoostDateTime(endDateTime);
            if (boostStartTime >= boostEndTime)
            {
                showError("Add Event", "End time must be greater than the Start time");
                addEventGUI->setProperty("markButtonSelected", false);
                return;
            }
            (*eventList)[0].startTime = boostStartTime;
            (*eventList)[0].endTime = boostEndTime;

            // do type specific funtionalities
            const std::string eventType = (*eventList)[0].event->getEventType()->getEventName();
            if (eventType == "Explosion")
            {
                // change the scale for explosion event
                double explosionScale = addEventGUI->property("explosionScale").toDouble();

                CORE::RefPtr<EFFECTS::IExplosionEvent> explosionEvent =
                    (*eventList)[0].event->getInterface<EFFECTS::IExplosionEvent>();

                // stop the event first because we cannont change parameter during the animation
                (*eventList)[0].event->stop();
                if (explosionEvent.valid())
                {
                    explosionEvent->setExplosionScale(explosionScale);
                }
            }
            else if (eventType == "Alarm")
            {
                // At the moment no features in alarm event to edit
            }
            else if (eventType == "Gunfire")
            {
                // At the moment no features in Gunfire event to edit
            }
            else if (eventType == "Info")
            {
                CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent =
                    (*eventList)[0].event->getInterface<EFFECTS::IInfoEvent>();

                // get/set the description
                std::string eventDescription = addEventGUI->property("infoEventDescription").toString().toStdString();
                infoEvent->setDescription(eventDescription);

                std::string imagePath = addEventGUI->property("iconPath").toString().toStdString();
                infoEvent->setImagePath(imagePath);

                std::string audioPath = addEventGUI->property("audioPath").toString().toStdString();
                infoEvent->setImagePath(audioPath);

                std::string videoPath = addEventGUI->property("videoPath").toString().toStdString();
                infoEvent->setImagePath(videoPath);

                // set Font Propertis for the event

                std::string fontFamily = addEventGUI->property("fontFamily").toString().toStdString();
                int fontSize = addEventGUI->property("fontSize").toInt();
                std::string weight = addEventGUI->property("fontWeight").toString().toStdString();

                // font for info event
                EFFECTS::IInfoEvent::Font font;
                font.size = fontSize;
                font.style = fontFamily;
                if (weight == "Light"){
                    font.weight = 25;
                }
                else if (weight == "Normal"){
                    font.weight = 50;
                }
                else if (weight == "DemiBold"){
                    font.weight = 63;
                }
                else if (weight == "Bold"){
                    font.weight = 75;
                }
                else if (weight == "Black"){
                    font.weight = 87;
                }
                infoEvent->setFont(font);

                QColor colorStyle = qvariant_cast<QColor>(addEventGUI->property("descColor"));
                std::string colorString = colorStyle.name().toStdString();
                infoEvent->setColor(colorString);

                /*QObject* infoContainerObject = _findChild("infoEventContainer");
                QVariant retunedVal;
                QVariant msg = "disable";
                QMetaObject::invokeMethod(infoContainerObject, "disable", Q_RETURN_ARG(QVariant, retunedVal), Q_ARG(QVariant, msg));*/
            }

            // unselect the edit button in the gui and unload the edit gui
            //QObject* editEvent = _findChild("editEvent");
            //QMetaObject::invokeMethod( editEvent, "deselectButton");
            _eventUIHandler->reset();
        }
    }

    bool EventGUI::configureExplosionEvent()
    {
        QObject* addEventGUI = _findChild("addEventGUI");
        if (addEventGUI)
        {
            // get the start and end time
            QDateTime startDateTime, endDateTime;

            startDateTime = addEventGUI->property("arrivalTime").toDateTime();
            endDateTime = addEventGUI->property("departureTime").toDateTime();

            boost::posix_time::ptime boostStartTime = VizQt::TimeUtils::QtToBoostDateTime(startDateTime);
            boost::posix_time::ptime boostEndTime = VizQt::TimeUtils::QtToBoostDateTime(endDateTime);
            if (boostStartTime >= boostEndTime)
            {
                showError("Add Event", "End time must be greater than the Start time");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            // get the title of the event
            double explosionScale = addEventGUI->property("explosionScale").toDouble();

            std::string eventName = addEventGUI->property("eventName").toString().toStdString();

            if (eventName == "")
            {
                showError("Add Explosion Event", "Enter the Name for the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }


            _addedEvent = _eventUIHandler->addEvent(eventName, "Explosion", boostStartTime, boostEndTime);

            if (!(_addedEvent.valid()))
            {
                showError("Add Event", "Please mark the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            CORE::RefPtr<EFFECTS::IExplosionEvent> addedExplosionEvent =
                _addedEvent->getInterface<EFFECTS::IExplosionEvent>();
            if (!addedExplosionEvent.valid())
            {
                showError("Add Event", "Invalid Event Added. Please try again.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }
            addedExplosionEvent->setExplosionScale(explosionScale);
            return true;
        }
        return false;
    }

    void EventGUI::handleMarkButtonClicked(bool enable)
    {
        if (enable)
        {
            // make the preview to stop
            QObject* addEventGUI = _findChild("addEventGUI");
            addEventGUI->setProperty("previewButtonSelected", false);
            if (_addedEvent)
            {
                _addedEvent->stop();
            }

            _eventUIHandler->setMode(SMCUI::IEventUIHandler::EVENT_MODE_CREATE_POINT);
        }
        else
        {
            _eventUIHandler->setMode(SMCUI::IEventUIHandler::EVENT_MODE_NONE);
        }
    }

    void EventGUI::muteAudio(QString title, bool mute){
        //QList<InfoEventItem> infoLists = _infoEventModel->getInfoList();

        for (int i = 0; i < infoEventList.size(); i++){
            CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent = infoEventList[i];
            if (infoEvent->getTitle() == title.toStdString()){
                if (mute){
                    infoEvent->mute();
                    break;
                }
                else{
                    infoEvent->unMute();
                    break;
                }
            }
        }
    }

    void EventGUI::setInfoEvent(QObject* addEventGUI, CORE::RefPtr<EFFECTS::IInfoEvent> addedInfoEvent){
        // get the description
        bool descriptionPresent = addEventGUI->property("descriptionPresent").toBool();
        std::string eventDescription = addEventGUI->property("infoEventDescription").toString().toStdString();
        if (eventDescription == "")
        {
            descriptionPresent = false;
        }

        std::string imagePath = addEventGUI->property("iconPath").toString().toStdString();
        std::string audioPath = addEventGUI->property("audioPath").toString().toStdString();
        std::string videoPath = addEventGUI->property("videoPath").toString().toStdString();
        std::string fontFamily = addEventGUI->property("fontFamily").toString().toStdString();
        int fontSize = addEventGUI->property("fontSize").toInt();
        std::string weight = addEventGUI->property("fontWeight").toString().toStdString();

        // font for info event
        EFFECTS::IInfoEvent::Font font;
        font.size = fontSize;
        font.style = fontFamily;
        if (weight == "Light"){
            font.weight = 25;
        }
        else if (weight == "Normal"){
            font.weight = 50;
        }
        else if (weight == "DemiBold"){
            font.weight = 63;
        }
        else if (weight == "Bold"){
            font.weight = 75;
        }
        else if (weight == "Black"){
            font.weight = 87;
        }

        QColor colorStyle = qvariant_cast<QColor>(addEventGUI->property("descColor"));
        std::string colorString = colorStyle.name().toStdString();

        addedInfoEvent->setDescription(eventDescription);
        addedInfoEvent->setImagePath(imagePath);
        addedInfoEvent->setAudioPath(audioPath);
        addedInfoEvent->setVideoPath(videoPath);
        addedInfoEvent->setColor(colorString);
        addedInfoEvent->setFont(font);
        //addedInfoEvent->setPosition(posX, posY);

        // subscribe to events start and ended message type
        _subscribe(addedInfoEvent.get(), *EFFECTS::IInfoEvent::InfoEventStartedMessageType);
        _subscribe(addedInfoEvent.get(), *EFFECTS::IInfoEvent::InfoEventEndedMessageType);
    }

    bool EventGUI::configureInfoEvent()
    {
        QObject* addEventGUI = _findChild("addEventGUI");
        if (addEventGUI)
        {
            // get the start and end time
            QDateTime startDateTime, endDateTime;

            startDateTime = addEventGUI->property("arrivalTime").toDateTime();
            endDateTime = addEventGUI->property("departureTime").toDateTime();

            boost::posix_time::ptime boostStartTime = VizQt::TimeUtils::QtToBoostDateTime(startDateTime);
            boost::posix_time::ptime boostEndTime = VizQt::TimeUtils::QtToBoostDateTime(endDateTime);
            if (boostStartTime >= boostEndTime)
            {
                emit showError("Add Event", "End time must be greater than the Start time");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }

            // get the title of the event
            std::string eventTitle = addEventGUI->property("eventName").toString().toStdString();
            if (eventTitle == "")
            {
                emit showError("Add Info Event", "Enter the Name for the event.");
                addEventGUI->setProperty("markButtonSelected", false);
                addEventGUI->setProperty("previewButtonSelected", false);
                return false;
            }
            // register the evnet ot the event player

            _addedEvent = _eventUIHandler->addEvent(eventTitle, "Info", boostStartTime, boostEndTime);

            if (!_addedEvent.valid())
            {
                emit showError("Add Event", "No event added. Please try again");
                return false;
            }

            CORE::RefPtr<EFFECTS::IInfoEvent> addedInfoEvent = _addedEvent->getInterface<EFFECTS::IInfoEvent>();
            if (!addedInfoEvent.valid())
            {
                emit showError("Add Event", "Invalid Event Added. Please try again.");
                return false;
            }
            addedInfoEvent->setTitle(eventTitle);

            setInfoEvent(addEventGUI, addedInfoEvent);

            return true;
        }
        return false;
    }

    void EventGUI::attachInfoEventImageFile()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        static std::string directory = "c:/";

        QString filePath = QFileDialog::getOpenFileName(parent, tr("Open Image File"),
            directory.c_str(), tr("Image File (*.jpg *.jpeg *.png)"));

        FILE *file = fopen(filePath.toStdString().c_str(), "r");


        if (file == NULL)
        {
            return;
        }
        else
        {
            fclose(file);
        }

        directory = filePath.toStdString();

        QObject* addEventGUI = _findChild("addEventGUI");
        if (addEventGUI)
        {
            addEventGUI->setProperty("iconPath", QVariant::fromValue(filePath));
        }

        QObject* editEventGUI = _findChild("editEvent");
        if (editEventGUI){
            editEventGUI->setProperty("iconPath", QVariant::fromValue(filePath));
        }
    }

    // attach audio file to the info event

    void EventGUI::attachInfoEventAudioFile(){
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        static std::string directory = "c:/";

        QString filePath = QFileDialog::getOpenFileName(parent, tr("Open Audio File"),
            directory.c_str(), tr("Audio File (*.mp3 *.wav)"));

        FILE *file = fopen(filePath.toStdString().c_str(), "r");


        if (file == NULL)
        {
            return;
        }
        else
        {
            fclose(file);
        }

        directory = filePath.toStdString();

        QObject* addEventGUI = _findChild("addEventGUI");
        if (addEventGUI)
        {
            addEventGUI->setProperty("audioPath", QVariant::fromValue(filePath));
        }

        QObject* editEventGUI = _findChild("editEvent");
        if (editEventGUI){
            editEventGUI->setProperty("audioPath", QVariant::fromValue(filePath));
        }
    }

    void EventGUI::attachInfoEventVideoFile(){
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        static std::string directory = "c:/";

        QString filePath = QFileDialog::getOpenFileName(parent, tr("Open Video File"),
            directory.c_str(), tr("Video File (*.mpeg *.mpg *.avi *.rmvb *.wmv *.dat *.mp4)"));

        FILE *file = fopen(filePath.toStdString().c_str(), "r");


        if (file == NULL)
        {
            return;
        }
        else
        {
            fclose(file);
        }

        directory = filePath.toStdString();

        QObject* addEventGUI = _findChild("addEventGUI");
        if (addEventGUI)
        {
            addEventGUI->setProperty("videoPath", QVariant::fromValue(filePath));
        }

        QObject* editEventGUI = _findChild("editEvent");
        if (editEventGUI)
        {
            editEventGUI->setProperty("videoPath", QVariant::fromValue(filePath));
        }
    }

    void EventGUI::onAddedToGUIManager()
    {
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();

        QQmlContext* rootContext = qtapp->getRootContext();
        if (rootContext)
        {
            //rootContext->setContextProperty("infoEventList", _infoEventModel);
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void EventGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            _eventUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IEventUIHandler>(getGUIManager());

            CORE::IComponent* component = CORE::WorldMaintainer::instance()->
                getComponentByName("AnimationComponent");
            if (component)
            {
                _animationComponent = component->getInterface<CORE::IAnimationComponent>();
                _subscribe(_animationComponent, *CORE::IAnimationComponent::AnimationPauseMessageType);
                _subscribe(_animationComponent, *CORE::IAnimationComponent::AnimationPlayingMessageType);
            }

            CORE::IComponent* projectLoaderComponent = CORE::WorldMaintainer::instance()->
                getComponentByName("ProjectLoaderComponent");
            if (projectLoaderComponent)
            {
                _subscribe(projectLoaderComponent, *DB::IProjectLoaderComponent::ProjectLoadedMessageType);
            }


            component = CORE::WorldMaintainer::instance()->getComponentByName("SelectionComponent");

            if (component)
            {
                _selectionComponent = component->getInterface<CORE::ISelectionComponent>();
            }
        }
        else if (messageType == *DB::IProjectLoaderComponent::ProjectLoadedMessageType)
        {

            CORE::IComponent* component = CORE::WorldMaintainer::instance()->getComponentByName("LayerComponent");

            CORE::RefPtr<SMCElements::IDataSourceComponent> dataSourceComponent =
                component->getInterface<SMCElements::IDataSourceComponent>();
            if (!dataSourceComponent.valid())
            {
                return;
            }

            CORE::RefPtr<ELEMENTS::IFeatureDataSource> datasource =
                dataSourceComponent->getFeatureDataSource("Events");

            if (!datasource.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IFeatureLayer> infoEventLayer = NULL;

            CORE::RefPtr<CORE::ICompositeObject> composite = datasource->getInterface<CORE::ICompositeObject>();

            if (composite.valid())
            {
                const CORE::ObjectMap& objectMap = composite->getObjectMap();

                CORE::ObjectMap::const_iterator iter = objectMap.begin();

                while (iter != objectMap.end())
                {
                    CORE::RefPtr<CORE::IBase> base = iter->second->getInterface<CORE::IBase>();

                    if (base.valid())
                    {
                        if (base->getName() == "Info")
                        {
                            infoEventLayer = base->getInterface<CORE::IFeatureLayer>();
                            break;
                        }
                    }
                    iter++;
                }
            }

            if (infoEventLayer.valid())
            {
                CORE::RefPtr<CORE::ICompositeObject> compositeObject =
                    infoEventLayer->getInterface<CORE::ICompositeObject>();

                if (compositeObject.valid())
                {
                    const CORE::ObjectMap& objectMap = compositeObject->getObjectMap();

                    CORE::ObjectMap::const_iterator iter = objectMap.begin();

                    for (; iter != objectMap.end(); iter++)
                    {
                        CORE::RefPtr<CORE::IObject> obj = iter->second;
                        if (obj.valid())
                        {
                            CORE::IProperty *eventPlayerProperty =
                                obj->getProperty(EFFECTS::EffectRegistryPlugin::EventPlayerPropertyType.get());
                            if (eventPlayerProperty)
                            {
                                EFFECTS::IEventPlayer *eventPlayer =
                                    eventPlayerProperty->getInterface<EFFECTS::IEventPlayer>();
                                if (eventPlayer)
                                {
                                    const EFFECTS::IEventPlayer::EventInfoList &evnetInfoList
                                        = eventPlayer->getEventInfoList();

                                    for (unsigned int currIndex = 0; currIndex < evnetInfoList.size(); ++currIndex)
                                    {
                                        // subscribe to events start and ended message type
                                        _subscribe(evnetInfoList.at(currIndex).event.get(),
                                            *EFFECTS::IInfoEvent::InfoEventStartedMessageType);
                                        _subscribe(evnetInfoList.at(currIndex).event.get(),
                                            *EFFECTS::IInfoEvent::InfoEventEndedMessageType);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (messageType == *EFFECTS::IInfoEvent::InfoEventStartedMessageType)
        {
            CORE::RefPtr<CORE::IBase> senderBase = message.getSender();
            if (!senderBase.valid())
            {
                return;
            }
            CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent = senderBase->getInterface<EFFECTS::IInfoEvent>();

            //_infoEventModel->addItem(infoEvent);
            infoEventList.push_back(infoEvent);

            QObject* infoContainerObject = _findChild("infoContainer");
            QVariant retunedVal;
            QString title = QString::fromStdString(infoEvent->getTitle());
            QString audioPath = QString::fromStdString(infoEvent->getAudioPath());
            QString videoPath = QString::fromStdString(infoEvent->getVideoPath());
            QString imagePath = QString::fromStdString(infoEvent->getImagePath());
            QString description = QString::fromStdString(infoEvent->getDescription());
            //QFont fontStyle;
            EFFECTS::IInfoEvent::Font font = infoEvent->getFont();

            QFont fontStyle;
            fontStyle.setWeight(font.weight);
            fontStyle.setFamily(QString::fromStdString(font.style));
            fontStyle.setPixelSize(font.size);
            /*
                        int fontSize = fontStyle.pixelSize();
                        int fontWeight = fontStyle.weight();
                        QFont::Style style = fontStyle.style();*/
            QColor color(QString::fromStdString(infoEvent->getColor()));

            QMetaObject::invokeMethod(infoContainerObject, "addEvent", Q_ARG(QVariant, title), Q_ARG(QVariant, description), Q_ARG(QVariant, audioPath), Q_ARG(QVariant, videoPath), Q_ARG(QVariant, imagePath), Q_ARG(QVariant, infoEvent->getPosition().x()), Q_ARG(QVariant, infoEvent->getPosition().y()), Q_ARG(QVariant, fontStyle), Q_ARG(QVariant, color));
        }
        else if (messageType == *EFFECTS::IInfoEvent::InfoEventEndedMessageType)
        {
            CORE::RefPtr<CORE::IBase> senderBase = message.getSender();
            if (!senderBase.valid())
            {
                return;
            }
            CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent = senderBase->getInterface<EFFECTS::IInfoEvent>();

            // remove the event from the list
            int index = infoEventList.indexOf(infoEvent);
            if (index != -1)
            {
                infoEventList.removeAt(index);
            }

            //_infoEventModel->removeItem(infoEvent);

            QObject* infoContainerObject = _findChild("infoContainer");
            QVariant retunedVal;
            QString title = QString::fromStdString(infoEvent->getTitle());

            // unset preview mode so that position cannot be updated
            if (isPreview){
                QObject* infoItemObject = _findChild(title.toStdString());
                if (infoItemObject != NULL){
                    int x = infoItemObject->property("x").toInt();
                    int y = infoItemObject->property("y").toInt();
                    osg::Vec2f position(x, y);
                    infoEvent->setPosition(position);
                }
                isPreview = false;
            }
            QMetaObject::invokeMethod(infoContainerObject, "removeEvent", Q_ARG(QVariant, title));
        }
        else if (messageType == *CORE::IAnimationComponent::AnimationPauseMessageType){

            //QList<InfoEventItem> infoLists = _infoEventModel->getInfoList();
            for (int i = 0; i < infoEventList.size(); i++){
                CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent = infoEventList[i];

                std::string onjectName = infoEvent->getTitle();
                QObject* infoItemObject = _findChild(onjectName);
                QVariant retunedVal;

                QString msg = "play";
                QMetaObject::invokeMethod(infoItemObject, "pauseMedia");
            }

        }
        else if (messageType == *CORE::IAnimationComponent::AnimationPlayingMessageType){
            //QList<InfoEventItem> infoLists = _infoEventModel->getInfoList();
            for (int i = 0; i < infoEventList.size(); i++){
                CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent = infoEventList[i];

                std::string onjectName = infoEvent->getTitle();
                QObject* infoItemObject = _findChild(onjectName);
                QVariant retunedVal;

                QString msg = "pause";
                QMetaObject::invokeMethod(infoItemObject, "playMedia");
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void EventGUI::menuClosed()
    {
        _eventUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);
    }

    // called when we click in the element list
    // and in the editeventcontextualTab, we click to delete the event
    void EventGUI::deleteEvent()
    {
        if (!_selectionComponent.valid())
            return;

        const CORE::ISelectionComponent::SelectionMap& selectionMap =
            _selectionComponent->getCurrentSelection();

        if (selectionMap.empty())
        {
            return;
        }

        CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
        CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();
        ELEMENTS::IEventFeature *eventFeature = selectedObject->getInterface<ELEMENTS::IEventFeature>();

        CORE::RefPtr<EFFECTS::IEventPlayer> player = eventFeature->getEventPlayer();

        player->clear();

        CORE::RefPtr<CORE::IDeletable> deletionHandle = eventFeature->getInterface<CORE::IDeletable>();
        if (!deletionHandle.valid())
        {
            return;
        }

        CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
        if (deletionUIHandler.valid())
        {
            deletionUIHandler->deleteObject(deletionHandle);
        }
    }

    // called when we click in the element list
    // and in the editeventcontextualTab, we click to edit the event
    void EventGUI::editEvent(bool enableEdit)
    {
        if (enableEdit)
        {
            editingEvent = enableEdit;
            if (!_selectionComponent.valid())
                return;

            const CORE::ISelectionComponent::SelectionMap& selectionMap =
                _selectionComponent->getCurrentSelection();

            if (selectionMap.empty())
            {
                return;
            }

            CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
            CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

            ELEMENTS::IEventFeature *eventFeature = selectedObject->getInterface<ELEMENTS::IEventFeature>();
            CORE::RefPtr<EFFECTS::IEventPlayer> player = eventFeature->getEventPlayer();
            const EFFECTS::IEventPlayer::EventInfoList& eventList = player->getEventInfoList();

            EFFECTS::IEventPlayer::EventInfoList::const_iterator iter = eventList.begin();
            if (iter == eventList.end())
            {
                return;
            }

            // set the window title and event name and disable change in eventType
            QObject* editEventGUI = _findChild("editEvent");

            // event name
            std::string eventName = eventFeature->getInterface<CORE::IBase>()->getName();
            editEventGUI->setProperty("eventName", QVariant::fromValue(QString::fromStdString(eventName)));

            // window title
            std::string windowTitle = "Edit " + eventName + " Event";
            QMetaObject::invokeMethod(editEventGUI, "changeWindowTitle",
                Q_ARG(QVariant, QVariant(QString::fromStdString(windowTitle))));

            // Hide the eventType
            editEventGUI->setProperty("eventTypeVisible", false);

            // set the menu of corresponding event type
            const std::string eventType = eventList[0].event->getEventType()->getEventName();
            editEventGUI->setProperty("eventType", QVariant::fromValue(QString::fromStdString(eventType)));

            // set the time in the calender
            boost::posix_time::ptime boostStartTime = eventList[0].startTime;
            boost::posix_time::ptime boostEndTime = eventList[0].endTime;

            QDateTime startTime;
            QDateTime endTime;

            VizQt::TimeUtils::BoostToQtDateTime(boostStartTime, startTime);
            VizQt::TimeUtils::BoostToQtDateTime(boostEndTime, endTime);

            QMetaObject::invokeMethod(editEventGUI, "setTimeDate", Q_ARG(QVariant, QVariant(startTime)), Q_ARG(QVariant, QVariant(endTime)));

            if (eventType == "Info")
            {
                CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent = eventList[0].event->getInterface<EFFECTS::IInfoEvent>();

                // get and set the description
                std::string description = infoEvent->getDescription();
                if (description != "")
                {
                    editEventGUI->setProperty("infoDescriptionPresent", true);
                }
                editEventGUI->setProperty("infoEventDescription", QVariant::fromValue(QString::fromStdString(description)));

                // get and set the image path
                std::string imagePath = infoEvent->getImagePath();
                editEventGUI->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(imagePath)));

                // get and set the audio path
                std::string audioPath = infoEvent->getAudioPath();
                editEventGUI->setProperty("audioPath", QVariant::fromValue(QString::fromStdString(audioPath)));

                // get and set video path
                std::string videoPath = infoEvent->getVideoPath();
                editEventGUI->setProperty("videoPath", QVariant::fromValue(QString::fromStdString(videoPath)));

                // get and set font value
                EFFECTS::IInfoEvent::Font fontS = infoEvent->getFont();
                // set fontfamily, weight, and size
                editEventGUI->setProperty("fontFamily", QVariant::fromValue(QString::fromStdString(fontS.style)));
                editEventGUI->setProperty("fontSize", QVariant::fromValue(fontS.size));

                // get the font weight and convert it to corresponding String
                QString fontWS;
                int fontWeight = fontS.weight;
                if (fontWeight == 25){
                    fontWS = "Light";
                }
                else if (fontWeight == 50){
                    fontWS = "Normal";
                }
                else if (fontWeight == 63){
                    fontWS = "DemiBold";
                }
                else if (fontWeight == 75){
                    fontWS = "Bold";
                }
                else if (fontWeight == 87){
                    fontWS = "Black";
                }
                editEventGUI->setProperty("fontWeight", QVariant::fromValue(fontWS));

                // get and set color
                QString color(QString::fromStdString(infoEvent->getColor()));
                editEventGUI->setProperty("descColor", QVariant::fromValue(color));

                // set and get x and Y value
                //int x = infoEvent->X();
                //int y = infoEvent->Y();
                //addEventGUI->setProperty("x", QVariant::fromValue(x));
                //addEventGUI->setProperty("y", QVariant::fromValue(y));

            }

            else if (eventType == "Explosion")
            {
                CORE::RefPtr<EFFECTS::IExplosionEvent> explosionEvent
                    = eventList[0].event->getInterface<EFFECTS::IExplosionEvent>();

                double explosionScale = explosionEvent->getExplosionScale();
                editEventGUI->setProperty("explosionScale", explosionScale);
            }

            else if (eventType == "Alarm")
            {
                // no parameters to for Alarm event
            }
            else if (eventType == "Gunfire")
            {
                // no parameters to for Gunfire event
            }
        }
        // hide the edit window
        else
        {
            editingEvent = enableEdit;
        }
    }

    // called when we click in the element list
    // and in the editeventcontextualTab, we click to show the event
    void EventGUI::showEventPreview(bool showPreview)
    {
        if (!_selectionComponent.valid())
            return;

        const CORE::ISelectionComponent::SelectionMap& selectionMap =
            _selectionComponent->getCurrentSelection();

        if (selectionMap.empty())
        {
            return;
        }

        CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
        CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();
        ELEMENTS::IEventFeature *eventFeature = selectedObject->getInterface<ELEMENTS::IEventFeature>();

        CORE::RefPtr<EFFECTS::IEventPlayer> player = eventFeature->getEventPlayer();
        EFFECTS::IEventPlayer::EventInfoList *eventList = const_cast<EFFECTS::IEventPlayer::EventInfoList*>(&(player->getEventInfoList()));

        EFFECTS::IEventPlayer::EventInfoList::const_iterator iter = eventList->begin();

        if (iter == eventList->end())
        {
            return;
        }

        if (showPreview)
        {
            isPreview = true;
            QObject* editEventGUI = _findChild("editEvent");
            if (editEventGUI){
                QDateTime startDateTime, endDateTime;

                startDateTime = editEventGUI->property("arrivalTime").toDateTime();
                endDateTime = editEventGUI->property("departureTime").toDateTime();

                boost::posix_time::ptime boostStartTime = VizQt::TimeUtils::QtToBoostDateTime(startDateTime);
                boost::posix_time::ptime boostEndTime = VizQt::TimeUtils::QtToBoostDateTime(endDateTime);
                if (boostStartTime >= boostEndTime)
                {
                    emit showError("Add Event", "End time must be greater than the Start time");
                    editEventGUI->setProperty("markButtonSelected", false);
                    editEventGUI->setProperty("previewButtonSelected", false);
                    return;
                }

                // set the updated Event start and end time
                (*eventList)[0].startTime = boostStartTime;
                (*eventList)[0].endTime = boostEndTime;

                CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent = (*eventList)[0].event->getInterface<EFFECTS::IInfoEvent>();

                setInfoEvent(editEventGUI, infoEvent);

                _eventUIHandler->reset();
            }
            //_eventUIHandler->reset();
            (*eventList)[0].event->start(EFFECTS::IEvent::FORWARD);
        }
        else
        {
            (*eventList)[0].event->stop();
        }
    }

} // namespace SMCQt


