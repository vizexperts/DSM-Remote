/****************************************************************************
*
* File             : GetInformationGUI.h
* Description      : GetInformationGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/AttributeObject.h>
#include <SMCQt/GetInformationGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/AccessElementUtils.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransaction.h>
#include <App/IUIHandler.h>
#include <App/IApplication.h>

#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>
#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <Elements/ElementsPlugin.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>

#include <osgEarth/ImageLayer>
#include <osgEarthAnnotation/ImageOverlay>

#include <Terrain/IRasterObject.h>
#include <Core/IRasterData.h>
#include <Core/RasterData.h>
#include <Core/GISData.h>
#include <gdal_priv.h>


#include <SMCUI/IPointUIHandler2.h>
#include <SMCUI/ILineUIHandler.h>
#include <SMCUI/IAreaUIHandler.h>
#include <SMCUI/IAddContentUIHandler.h>


#include <SMCQt/EventGUI.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, GetInformationGUI);
    DEFINE_IREFERENCED(GetInformationGUI, SMCQt::DeclarativeFileGUI);

    GetInformationGUI::GetInformationGUI()
    {
    }

    GetInformationGUI::~GetInformationGUI()
    {
    }

    void GetInformationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());

                const CORE::ComponentMap& cmap = wmain->getComponentMap();
                _selectionComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void GetInformationGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if( popupLoader != NULL )
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void GetInformationGUI::connectPopupLoader(QString type)
    {
        if(type == "RasterInfoObject")
        {
            setActive( true );

            if(!_selectionComponent.valid())
                return;

            const CORE::ISelectionComponent::SelectionMap& selectionMap = 
                _selectionComponent->getCurrentSelection();
            if(selectionMap.empty())
                return;

            CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
            CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();
            if(!selectedObject)
                return;

            TERRAIN::IRasterObject* raster = selectedObject->getInterface<TERRAIN::IRasterObject>();
            //CORE::IRasterData* rasterdata = raster->getRasterData();
            //CORE::IGISData* gisdata = rasterdata->getInterface<CORE::IGISData>();

            if(raster)
            {

                std::string url = raster->getURL();
                if(url.empty())
                {
                    url = raster->getRuntimeURL();
                }
                std::string ext = osgDB::getFileExtension(url);
                if(ext == "")
                {
                    showError("Invalid Dataset", "This feature currently doesn't works with directory loading");
                    return;
                }
                if(ext == "xyz" || ext == "yahoo" || ext =="wms" || ext == "bing" || ext == "google")
                {
                    showError("Invalid Dataset" , "This Feature doesn't work with web data");
                    return;
                }
                if(ext == "tms")
                {
                    showError("Invalid Dataset", "This feature doesn't work with TMS data");
                    return;
                }

                // adding extents to list
                osg::Vec4d extents;
                raster->getExtents(extents);

                _addAttributeToTable("", QString::fromStdString(""));

                std::stringstream convertToString;
                convertToString << extents[0] << " , " << extents[1];
                _addAttributeToTable("Left, Bottom", QString::fromStdString(convertToString.str()));

                convertToString.str("");
                convertToString << extents[2] << " , " << extents[3];
                _addAttributeToTable("Right, Top", QString::fromStdString(convertToString.str()));

                _readAttributesFromFile(url);

                _addAttributeToTable("", QString::fromStdString(""));
            }
        }
        if(type == "VersionInfoObject")
        {
            setActive( true );

            _addAttributeToTable("", QString::fromStdString(""));

            _addAttributeToTable("Version number", QString(DSM_VERSION));

            _addAttributeToTable("Build number", QString(DSM_BUILD_NUMBER));

           
            std::string license = "Vu";
            std::string cartridges = "";
            QObject* licenseManager = _findChild("licenseManager");
            if (licenseManager)
            {
                bool pro = licenseManager->property("georbIS_Pro").toBool();
                if (pro)
                {
                    license = "Pro";
                }

                bool strategem = licenseManager->property("georbIS_Strategem").toBool();
                if (strategem)
                {
                    cartridges += "Strategem, ";
                }

                bool intel = licenseManager->property("georbIS_Intel").toBool();
                if (intel)
                {
                    cartridges += " Intel, ";
                }

                bool footprint = licenseManager->property("georbIS_Footprint").toBool();
                if (footprint)
                {
                    cartridges += " Footprint, ";
                }

                bool analyst = licenseManager->property("georbIS_Analyst").toBool();
                if (analyst)
                {
                    cartridges += " Analyst, ";
                }

                bool processor = licenseManager->property("georbIS_Processor").toBool();
                if (processor)
                {
                    cartridges += "Processor, ";
                }
                
            }


            _addAttributeToTable("License", QString::fromStdString(license));

            if (cartridges.size() == 0)
            {
                cartridges = "None";
            }

            else
            {
                cartridges.pop_back();
                cartridges.pop_back();
            }
            _addAttributeToTable("Catridges", QString::fromStdString(cartridges));
            _addAttributeToTable(" ", QString::fromStdString(""));
        }
    }

    void GetInformationGUI::_readAttributesFromFile(std::string &url)
    {
        GDALDatasetH dataset = GDALOpen(url.c_str(), GA_ReadOnly);
        if(dataset)
        {
            //! BASIC INFORMATION WHICH ALL FILE CONTAINS 

            // get the driver name corresponding to file
            GDALDriverH driver = GDALGetDatasetDriver(dataset);
            _addAttributeToTable("Driver", GDALGetDriverLongName(driver));

            // get the file name 
            char **fileList = GDALGetFileList(dataset);
            _addAttributeToTable("File Name", fileList[0]);
            CSLDestroy(fileList);

            std::stringstream convertToString;

            // get the image width and height
            convertToString << GDALGetRasterXSize(dataset);
            _addAttributeToTable("Image Width ", QString::fromStdString(convertToString.str()));

            convertToString.str("");
            convertToString << GDALGetRasterYSize(dataset);
            _addAttributeToTable("Image Height", QString::fromStdString(convertToString.str()));

            // get Origin
            /*double origin[6];
            convertToString.str("");
            if(GDALGetGeoTransform(dataset, origin) == CE_None)
            {
            convertToString << origin[0] << " , " << origin[3];
            _addAttributeToTable("Origin", QString::fromStdString(convertToString.str()));
            }*/

            // get Number of bands
            convertToString.str("");
            convertToString << GDALGetRasterCount(dataset);
            _addAttributeToTable("Number of Bands", QString::fromStdString(convertToString.str()));

            //! FILE DEPENDENT INFORMATION, SOME FILE CONTAINS AND SOME ARE NOT 

            // Basic MetaData about the image
            _getMetaData(dataset);

            // Image Structure Metadata 
            _getImageStructureMetadata(dataset);
        }
    }

    void GetInformationGUI::_getMetaData(GDALDatasetH dataset)
    {
        char **metadata;
        metadata = GDALGetMetadata( dataset, NULL );
        std::string line;
        unsigned position;
        if( CSLCount(metadata) > 0 )
        {
            for(int i = 0; metadata[i] != NULL; i++ )
            {
                line = metadata[i];
                position = line.find("=");
                _addAttributeToTable(QString::fromStdString(line.substr(0,position)), QString::fromStdString(line.substr(position+1)));
            }
        }
    }

    void GetInformationGUI::_getImageStructureMetadata(GDALDatasetH dataset)
    {
        char **imageMetadata;
        imageMetadata = GDALGetMetadata( dataset, "IMAGE_STRUCTURE" );
        std::string line;
        unsigned position;
        if( CSLCount(imageMetadata) > 0 )
        {
            for(int i = 0; imageMetadata[i] != NULL; i++ )
            {
                line = imageMetadata[i];
                position = line.find("=");
                _addAttributeToTable(QString::fromStdString(line.substr(0,position)), QString::fromStdString(line.substr(position+1)));
            }
        }
    }

    void GetInformationGUI::_addAttributeToTable(const QString& attrName, const QString& attrValue)
    {
        QObject *object = new AttributeObject(attrName, attrValue);
        _twAttrList.append(object);

        _setContextProperty("listInfoModel", QVariant::fromValue(_twAttrList));
    }

    void GetInformationGUI::_clearAttributeTable()
    {
        _twAttrList.clear();
        _setContextProperty("listInfoModel", QVariant::fromValue(_twAttrList));
    }


    void 
        GetInformationGUI::setActive(bool value)
    {
        try
        {
            DeclarativeFileGUI::setActive(value);
            // clear the GUI
            _clearAttributeTable();
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }
}

