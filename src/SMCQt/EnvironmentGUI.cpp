/****************************************************************************
*
* File             : EnvironmentGUI.h
* Description      : EnvironmentGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/EnvironmentGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>

#include <GISCompute/IFilterStatusMessage.h>

#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>

#include <Env/IWeatherComponent.h>
#include <Env/IFogEffect.h>
#include <Env/ICloudEffect.h>
#include <Env/IPrecipitationEffect.h>
#include <Env/IEffect.h>

#include <VizUI/ICameraUIHandler.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, EnvironmentGUI);
    DEFINE_IREFERENCED(EnvironmentGUI, SMCQt::DeclarativeFileGUI);

    EnvironmentGUI::EnvironmentGUI()
        : _firstTime(true)
    {}

    EnvironmentGUI::~EnvironmentGUI()
    {
    }

    void EnvironmentGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if(smpLeftMenu != NULL)
        {
            QObject::connect(smpLeftMenu, SIGNAL(connectWeatherMenu()), this,
                SLOT(connectWeatherMenu()), Qt::UniqueConnection);
        }
    }

    void EnvironmentGUI::connectWeatherMenu()
    {
        QObject* smpWeatherMenu = _findChild("smpWeatherMenu");
        if(smpWeatherMenu != NULL)
        {
            // initialize menu
            bool fog = false;
            bool dust = false;
            int fogDensity = 0;
            bool cloud = false;
            int cloudDensity = 0;
            bool rain = false;
            bool snow = false;
            bool sunMoonEffect = false;
            bool lightEffect = false;

            ENV::IWeatherComponent *comp = _getWeatherComponent();
            if(comp)
            {
                ENV::IEffect *effect0 = comp->getEffectAtIndex(0);
                if(effect0)
                {
                        ENV::IFogEffect *fogEffect = effect0->getInterface<ENV::IFogEffect>();
                        if(fogEffect)
                        {
                            if (effect0->getStatus())
                            {
                                ENV::IFogEffect::EffectType type = fogEffect->getEffectType();
                                if (type == ENV::IFogEffect::FOG)
                                {
                                    fog = true;
                                }
                                else if (type == ENV::IFogEffect::DUST)
                                {
                                    dust = true;
                                }
                            }
                            fogDensity = 100*fogEffect->getFogDensity();
                        }
                }

                ENV::IEffect* effect1 = comp->getEffectAtIndex(1);
                if(effect1)
                {
                    if(effect1->getStatus())
                    {
                        cloud = true;
                    }
                    ENV::ICloudEffect* cloudEffect = effect1->getInterface<ENV::ICloudEffect>();
                    if(cloudEffect)
                    {
                        cloudDensity = cloudEffect->getCloudDensity();
                    }
                }

                ENV::IEffect* effect2 = comp->getEffectAtIndex(2);
                if(effect2)
                {
                    if(effect2->getStatus())
                    {
                        ENV::IPrecipitationEffect* precipitationEffect = 
                            effect2->getInterface<ENV::IPrecipitationEffect>();
                        if(precipitationEffect)
                        {
                            ENV::IPrecipitationEffect::EffectType type = precipitationEffect->getEffectType();
                            if(type == ENV::IPrecipitationEffect::RAIN)
                            {
                                rain = true;
                            }
                            else if(type == ENV::IPrecipitationEffect::SNOW)
                            {
                                snow = true;
                            }
                        }
                    }
                }
                lightEffect = comp->getLightEffect();
                ENV::IEffect* effect4 = comp->getEffectAtIndex(4);
                if(effect4)
                {
                    if(effect4->getStatus())
                    {
                        sunMoonEffect = true;
                    }
                }
            }

            smpWeatherMenu->setProperty("lightEnabled", QVariant::fromValue(lightEffect));
            smpWeatherMenu->setProperty("sunMoonStartsEnabled", QVariant::fromValue(sunMoonEffect));
            smpWeatherMenu->setProperty("cloudsEnabled", QVariant::fromValue(cloud));
            smpWeatherMenu->setProperty("cloudsDensity", QVariant::fromValue(cloudDensity));
            smpWeatherMenu->setProperty("snowEnabled", QVariant::fromValue(snow));
            smpWeatherMenu->setProperty("rainEnabled", QVariant::fromValue(rain));
            smpWeatherMenu->setProperty("fogEnabled", QVariant::fromValue(fog));
            smpWeatherMenu->setProperty("dustEnabled", QVariant::fromValue(dust));
            smpWeatherMenu->setProperty("fogDensity", QVariant::fromValue(fogDensity));

            // connect to signals
            QObject::connect(smpWeatherMenu, SIGNAL(toggleSunMoonStars(bool)), 
                this, SLOT(toggleSunMoonStars(bool)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(toggleLight(bool)), 
                this, SLOT(toggleLight(bool)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(toggleCloudsEnable(bool)), 
                this, SLOT(toggleCloudsEnable(bool)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(setCloudsDensity(int)), 
                this, SLOT(setCloudsDensity(int)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(toggleSnow(bool)), 
                this, SLOT(toggleSnow(bool)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(toggleRain(bool)), 
                this, SLOT(toggleRain(bool)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(toggleFog(bool)), 
                this, SLOT(toggleFog(bool)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(toggleDust(bool)), 
                this, SLOT(toggleDust(bool)), Qt::UniqueConnection);

            QObject::connect(smpWeatherMenu, SIGNAL(setFogDensity(int)),
                this, SLOT(setFogDensity(int)), Qt::UniqueConnection);
        }
    }

    void EnvironmentGUI::_update()
    {
        try
        {
            ENV::IWeatherComponent *comp = _getWeatherComponent();

            CORE::RefPtr<VizUI::ICameraUIHandler> camerauihandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager()); 

            if(!camerauihandler.valid())
                throw;

            osg::Vec3d pos = camerauihandler->getCurrentCameraPos();

            if(comp != NULL)
            {
                comp->update(pos);
            }

            if(_firstTime)
            {
                toggleSunMoonStars(true);

                _firstTime = false;
            }
        }
        catch(...)
        {
            LOG_ERROR("Unable to update environment");
        }
    }

    void EnvironmentGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                _subscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            _update();
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    ENV::IWeatherComponent* EnvironmentGUI::_getWeatherComponent()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> maintainer = CORE::WorldMaintainer::instance();

        if(maintainer.valid())
        {
            CORE::RefPtr<CORE::IComponent> component = 
                maintainer->getComponentByName("WeatherComponent");

            if (!component.valid())
            {
                return NULL;
            }

            CORE::RefPtr<ENV::IWeatherComponent> weatherComp = component->getInterface<ENV::IWeatherComponent>();

            if(weatherComp.valid())
            {
                return weatherComp.get();
            }
        }

        return NULL;
    }

    void EnvironmentGUI::toggleFog(bool value)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();
        if(comp)
        {
            comp->toggleEffect(ENV::IWeatherComponent::FOG_EFFECT ,value);
            ENV::IEffect *effect = comp->getEffectAtIndex(0);

            if(effect != NULL)
            {
                ENV::IFogEffect *fogEffect = effect->getInterface<ENV::IFogEffect>();

                if(fogEffect != NULL)
                {
                    fogEffect->setEffectType(ENV::IFogEffect::FOG);
                }
            }
        }
    }

    void EnvironmentGUI::toggleDust(bool value)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();
        if(comp)
        {
            comp->toggleEffect(ENV::IWeatherComponent::FOG_EFFECT ,value);
            ENV::IEffect *effect = comp->getEffectAtIndex(0);

            if(effect != NULL)
            {
                ENV::IFogEffect *fogEffect = effect->getInterface<ENV::IFogEffect>();

                if(fogEffect != NULL)
                {
                    fogEffect->setEffectType(ENV::IFogEffect::DUST);
                }
            }
        }
    }

    void EnvironmentGUI::setFogDensity(int value)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();

        if(comp != NULL)
        {
            ENV::IEffect *effect = comp->getEffectAtIndex(0);

            if(effect != NULL)
            {
                ENV::IFogEffect *fogEffect = effect->getInterface<ENV::IFogEffect>();

                if(fogEffect != NULL)
                {
                    fogEffect->setFogDensity((double)value / 100);
                }
            }
        }
    }

    void EnvironmentGUI::toggleSunMoonStars(bool state)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();

        if(comp != NULL)
        {
            comp->toggleEffect(ENV::IWeatherComponent::SUNMOONLIGHT_EFFECT ,state);
            if(state == false)
            {
                comp->enableLightEffect(false);
                bool lightEffect = comp->getLightEffect();
                QObject* smpWeatherMenu = _findChild("smpWeatherMenu");
                if(smpWeatherMenu != NULL)
                {
                   smpWeatherMenu->setProperty("lightEnabled", QVariant::fromValue(lightEffect));
                }
            }
        }
    }

    void EnvironmentGUI::toggleLight(bool state)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();
        if(comp != NULL)
        {
            comp->enableLightEffect(state);
        }
    }

    void EnvironmentGUI::toggleCloudsEnable(bool enable)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();

        if(comp != NULL)
        {           
            comp->toggleEffect(ENV::IWeatherComponent::CLOUD_EFFECT ,enable);
        }
    }

    void EnvironmentGUI::toggleRain(bool state)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();

        if(comp != NULL)
        {
            comp->toggleEffect(ENV::IWeatherComponent::PRECIPITATION_EFFECT ,state);
            ENV::IEffect *effect = comp->getEffectAtIndex(2);

            if(effect != NULL)
            {
                ENV::IPrecipitationEffect *precipitationEffect = effect->getInterface<ENV::IPrecipitationEffect>();

                if(precipitationEffect != NULL)
                {

                    precipitationEffect->setEffectType(ENV::IPrecipitationEffect::RAIN);
                }
            }
        }
    }

    void EnvironmentGUI::toggleSnow(bool state)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();

        if(comp != NULL)
        {
            comp->toggleEffect(ENV::IWeatherComponent::PRECIPITATION_EFFECT ,state);
            ENV::IEffect *effect = comp->getEffectAtIndex(2);

            if(effect != NULL)
            {
                ENV::IPrecipitationEffect *precipitationEffect = effect->getInterface<ENV::IPrecipitationEffect>();

                if(precipitationEffect != NULL)
                {
                    precipitationEffect->setEffectType(ENV::IPrecipitationEffect::SNOW);
                }
            }
        }
    }

    void EnvironmentGUI::setCloudsDensity(int value)
    {
        ENV::IWeatherComponent *comp = _getWeatherComponent();

        if(comp != NULL)
        {
            ENV::IEffect *effect = comp->getEffectAtIndex(1);

            if(effect != NULL)
            {
                ENV::ICloudEffect *cloudEffect = effect->getInterface<ENV::ICloudEffect>();

                if(cloudEffect != NULL)
                {
                    cloudEffect->setCloudDensity(ENV::ICloudEffect::CloudDensity(value));
                }
            }
        }
    }

} // namespace indiGUI
