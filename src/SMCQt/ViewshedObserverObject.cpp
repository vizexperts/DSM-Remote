#include <SMCQt/ViewshedObserverObject.h>

namespace SMCQt{

    ViewshedObserverObject::ViewshedObserverObject(QObject* parent) : QObject(parent)
    {

    }

    ViewshedObserverObject::ViewshedObserverObject(const QString &name, const QColor &color, double longi, double lat, double height, double range, QObject *parent)
        : QObject(parent), m_name(name), m_color(color), m_longi(longi), m_lat(lat), m_height(height), m_range(range)
    {
        m_lookatlongi = 0;
        m_lookatlat = 0;
        m_lookatele = 0;
        m_horizontalFOV = 360;
        m_verticalFOV = 180;
    }

    ViewshedObserverObject::ViewshedObserverObject(const QString &name, const QColor &color, double longi, double lat, double height, double range, double lookatlongi, double lookatlat, double lookatele, double horizontalFOV, double verticalFOV, QObject *parent)
        : QObject(parent), m_name(name), m_color(color), m_longi(longi), m_lat(lat), m_height(height), m_range(range), 
                                         m_lookatlongi(lookatlongi), m_lookatlat(lookatlat), m_lookatele(lookatele), m_horizontalFOV(horizontalFOV), m_verticalFOV(verticalFOV)
    {
    }

    QString ViewshedObserverObject::name() const
    {
        return m_name;
    }
    void ViewshedObserverObject::setName(const QString &name)
    {
        if(name != m_name)
        {
            m_name = name;
            emit nameChanged();
        }
    }

    QColor ViewshedObserverObject::color() const
    {
        return m_color;
    }
    void ViewshedObserverObject::setColor(const QColor &color)
    {
        if(color != m_color)
        {
            m_color = color;
            emit colorChanged();
        }
    }

    double ViewshedObserverObject::longi() const
    {
        //value to be shown in UI upto 3 decimal places only
        long double longi = m_longi*1000; 
        int longiInt = longi; 

        return (double)longiInt/1000; 
    }
    void ViewshedObserverObject::setLongi(double longi)
    {
        if(longi != m_longi)
        {
            m_longi = longi;
            emit longiChanged();
        }
    }

    double ViewshedObserverObject::lat() const
    {
        //value to be shown in UI upto 3 decimal places only
        long double lati = m_lat*1000; 
        int latiInt = lati;

        return (double)latiInt/1000; 
    }
    void ViewshedObserverObject::setLat(double lat)
    {
        if(lat != m_lat)
        {
            m_lat = lat;
            emit latChanged();
        }
    }

    double ViewshedObserverObject::height() const
    {
        return m_height;
    }
    void ViewshedObserverObject::setHeight(double height)
    {
        if(height != m_height)
        {
            m_height = height;
            emit heightChanged();
        }
    }

    double ViewshedObserverObject::range() const
    {
        return m_range;
    }
    void ViewshedObserverObject::setRange(double range)
    {
        if(range != m_range)
        {
            m_range = range;
            emit rangeChanged();
        }
    }

    double ViewshedObserverObject::lookatlongi() const
    {
        long double lookatlongi = m_lookatlongi*1000; 
        int longiInt = lookatlongi; 

        return (double)longiInt/1000;
    }
    void ViewshedObserverObject::setlookatLongi(double lookatlongi)
    {
        if(lookatlongi != m_lookatlongi)
        {
            m_lookatlongi = lookatlongi;
            emit lookatlongiChanged();
        }
    }

    double ViewshedObserverObject::lookatlat() const
    {
        long double lookatlat = m_lookatlat*1000; 
        int latiInt = lookatlat;

        return (double)latiInt/1000; 

    }
    void ViewshedObserverObject::setlookatLat(double lookatlat)
    {
        if(lookatlat != m_lookatlat)
        {
            m_lookatlat = lookatlat;
            emit lookatlatChanged();
        }
    }

    double ViewshedObserverObject::lookatele() const
    {
        return m_lookatele;
    }
    void ViewshedObserverObject::setlookatEle(double lookatele)
    {
        if(lookatele != m_lookatele)
        {
            m_lookatele = lookatele;
            emit lookateleChanged();
        }
    }

    double ViewshedObserverObject::horizontalFOV() const
    {
        return m_horizontalFOV;
    }
    void ViewshedObserverObject::sethorizontalFOV(double horizontalFOV)
    {
        if(horizontalFOV != m_horizontalFOV)
        {
            m_horizontalFOV = horizontalFOV;
            emit horizontalFOVChanged();
        }
    }

    double ViewshedObserverObject::verticalFOV() const
    {
        return m_verticalFOV;
    }
    void ViewshedObserverObject::setverticalFOV(double verticalFOV)
    {
        if(verticalFOV != m_verticalFOV)
        {
            m_verticalFOV = verticalFOV;
            emit verticalFOVChanged();
        }
    }
}
