/****************************************************************************
*
* File             : ResourceManagerGUI.cpp
* Description      : ResourceManagerGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/ResourceManagerGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <SMCUI/IMilitaryPathAnimationUIHandler.h>

#include <Core/IMessage.h>
#include <Core/INamedAttributeMessage.h>
#include <Core/WorldMaintainer.h>
#include <Core/ISelectable.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataCreator.h>
#include <Core/IVisibility.h>
#include <Core/IPoint.h>
#include <Core/IDeletable.h>
#include <Core/ITerrain.h>
#include <Core/IObjectMessage.h>
#include <Core/IText.h>
#include <Core/ICompositeObject.h>
#include <Core/IProperty.h>
#include <Core/CoreRegistry.h>
#include <Core/ISelectionComponent.h>


#include <SMCElements/ILayerComponent.h>

#include <Elements/IMilitarySymbol.h>
#include <Elements/IMilitaryRange.h>
#include <Elements/IRepresentation.h>
#include <Elements/IPathController.h>
#include <Elements/IPath.h>
#include <Elements/MilitarySymbolType.h>
#include <Elements/IClamper.h>
#include <Elements/IProjectSettingsComponent.h>
#include <Elements/ElementsUtils.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIcon.h>
#include <Elements/IIconLoader.h>
#include <VizUI/IDeletionUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Elements/IMilitarySymbolType.h>
#include <Elements/IModelHolder.h>

namespace SMCQt
{
    const std::string ResourceManagerGUI::MilitarySymbolContextualMenuObjectName = "militarySymbolPointContextual";

    DEFINE_META_BASE(SMCQt, ResourceManagerGUI);
    DEFINE_IREFERENCED(ResourceManagerGUI, DeclarativeFileGUI);

    ResourceManagerGUI::ResourceManagerGUI()
    {

    }

    ResourceManagerGUI::~ResourceManagerGUI()
    {

    }

    void ResourceManagerGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader != NULL)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSMPMenu(QString)), Qt::UniqueConnection);
        }

    }
    void ResourceManagerGUI::popupLoaded(QString type)
    {
        if(type == "smpResourcesMenu")
        {
            QObject* smpResourcesMenu = _findChild("smpResourcesMenu");
            if(smpResourcesMenu != NULL)
            {
                QObject::connect(smpResourcesMenu, SIGNAL(addSymbol(QString, QString, QString)), this,
                    SLOT(addMilitaryResource(QString, QString, QString)), Qt::UniqueConnection);
            }
        }
    }

    void ResourceManagerGUI::connectContextualMenu()
    {
        _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_ATTRIBUTE);

        _populateContextualMenu();

        QObject* militarySymbolPointContextual = _findChild(MilitarySymbolContextualMenuObjectName);
        if(militarySymbolPointContextual != NULL)
        {    
            QObject::connect(militarySymbolPointContextual, SIGNAL(rename(QString)), 
                this, SLOT(rename(QString)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(handleLatLongAltChanged(QString)), 
                this, SLOT(handleLatLongAltChanged(QString)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(deleteSymbol()), this, 
                SLOT(deleteSymbol()), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(editEnabled(bool)), 
                this, SLOT(editEnabled(bool)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(clampingChanged(bool)), 
                this, SLOT(clampingChanged(bool)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(textVisibilityChanged(bool)), 
                this, SLOT(textVisibilityChanged(bool)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(sphereVisibilityChanged(bool)), 
                this, SLOT(sphereVisibilityChanged(bool)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(sphereRangeChanged(double)), 
                this, SLOT(sphereRangeChanged(double)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(sphereRangeChanged(double)), 
                this, SLOT(sphereRangeChanged(double)), Qt::UniqueConnection);

            QObject::connect(militarySymbolPointContextual, SIGNAL(changeSymbolSize(QString)),
                this, SLOT(changeSymbolSize(QString)), Qt::UniqueConnection);

            /*QObject::connect(militarySymbolPointContextual, SIGNAL(handleUserIdChanged(QString)), 
            this, SLOT(handleUserIdChanged(QString)), Qt::UniqueConnection);*/

            _subscribe(_unitUIHandler.get(), 
                *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType);

            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if(selectionUIHandler.valid())
            {
                selectionUIHandler->setMouseClickSelectionState(false);
            }
        }
    }

    void ResourceManagerGUI::disconnectContextualMenu()
    {
        _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE);

        _unsubscribe(_unitUIHandler.get(), 
            *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if(selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(true);
            selectionUIHandler->clearCurrentSelection();
        }
    }

    void ResourceManagerGUI::editEnabled(bool value)
    {
        if(value)
        {
            _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_POSITION);
        }
        else
        {
            _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_EDIT_ATTRIBUTE);
        }
    }

    void ResourceManagerGUI::_populateContextualMenu()
    {
        QObject* militarySymbolPointContextual = _findChild(MilitarySymbolContextualMenuObjectName);
        if(militarySymbolPointContextual != NULL)
        {    
            // populate data for contextual tab            

            // get the selected symbol
            CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = _unitUIHandler->getMilitarySymbol();
            if(!unit.valid())
            {
                return;
            }

            // Path for iconfile in GUI
            QString iconPath = QString::fromStdString(_unitUIHandler->getQMLIconPath(unit->getMilitarySymbolType()));

            //Name of military symbol
            QString symbolName = QString::fromStdString(unit->getInterface<CORE::IBase>()->getName());

            // Type of symbol
            QString symbolType = QString::fromStdString(unit->getMilitarySymbolType()->getMilitaryType());

            CORE::RefPtr<CORE::IPoint> point = unit->getInterface<CORE::IPoint>();
            if(!point.valid())
            {
                return;
            }

            //! Lat-Long-Alt of point
            osg::Vec3 position = point->getValue();                    
            double altitudeD = position.z() - ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y());
            if(altitudeD < 0)
            {
                altitudeD = 0.0;
            }
            QString latitude    = QString::number(position.y(), 'f', 6);
            QString longitude    = QString::number(position.x(), 'f', 6); 
            QString altitude    = QString::number(altitudeD, 'f', 6); 

            //! Clamping state
            bool clampingEnabled = true;
            CORE::RefPtr<ELEMENTS::IClamper> clamper = unit->getInterface<ELEMENTS::IClamper>();
            if(clamper.valid())
            {
                clampingEnabled = clamper->clampOnPlacement();
            }

            //! Text visibility
            bool textVisible = false;
            CORE::RefPtr<CORE::IText> text = unit->getInterface<CORE::IText>();
            if(text.valid())
            {
                textVisible = text->getTextActive();
            }

            //! Range visibility
            bool sphereVisible = false;
            double sphereRange = 0.0;
            CORE::RefPtr<ELEMENTS::IMilitaryRange> militaryRange = unit->getInterface<ELEMENTS::IMilitaryRange>();
            if ( militaryRange.valid() )
            {
                sphereRange = militaryRange->getSphereRange();
                sphereVisible = militaryRange->getSphereVisibility();
            }

            militarySymbolPointContextual->setProperty("iconPath", QVariant::fromValue(iconPath));
            militarySymbolPointContextual->setProperty("symbolName", QVariant::fromValue(symbolName));
            militarySymbolPointContextual->setProperty("symbolType", QVariant::fromValue(symbolType));
            militarySymbolPointContextual->setProperty("latitude", QVariant::fromValue(latitude));
            militarySymbolPointContextual->setProperty("longitude", QVariant::fromValue(longitude));
            militarySymbolPointContextual->setProperty("altitude", QVariant::fromValue(altitude));
            militarySymbolPointContextual->setProperty("clampingEnabled", QVariant::fromValue(clampingEnabled));
            militarySymbolPointContextual->setProperty("textVisible", QVariant::fromValue(textVisible));
            militarySymbolPointContextual->setProperty("sphereRadius", QVariant::fromValue(sphereRange));
            militarySymbolPointContextual->setProperty("sphereVisible", QVariant::fromValue(sphereVisible));

        }
    }

    void ResourceManagerGUI::clampingChanged(bool value)
    {
        if(!_unitUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = _unitUIHandler->getMilitarySymbol();
        if(unit.valid())
        {
            CORE::RefPtr<ELEMENTS::IClamper> clamper = unit->getInterface<ELEMENTS::IClamper>();
            if(clamper.valid())
            {
                clamper->setClampOnPlacement(value);
                QObject* militarySymbolPointContextual = _findChild(MilitarySymbolContextualMenuObjectName);
                if(militarySymbolPointContextual)
                {
                    //militarySymbolPointContextual->setProperty("altitude", QVariant::fromValue(0));
                }
            }
        }

        if(!value)
        {
            handleLatLongAltChanged("altitude");
            return;
        }
    }

    void ResourceManagerGUI::textVisibilityChanged(bool value)
    {
        if(!_unitUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = _unitUIHandler->getMilitarySymbol();
        if(unit.valid())
        {
            unit->getInterface<CORE::IText>()->setTextActive(value);
        }
    }

    void ResourceManagerGUI::sphereVisibilityChanged ( bool value )
    {
        if(!_unitUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IMilitaryRange> range = _unitUIHandler->getMilitaryRange();
        if(range.valid())
        {
            range->setSphereVisibility ( value );
        }
    }

    void ResourceManagerGUI::changeSymbolSize(QString symbolSize)
    {
        bool ok = true;

        double size = symbolSize.toDouble(&ok);
        
        if (!ok)
        {
            size = 32.0f;
        }   
            
        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit =  _unitUIHandler->getMilitarySymbol();
        if (unit != NULL)
        {
            CORE::RefPtr<ELEMENTS::IIconHolder> iconholder = unit->getInterface<ELEMENTS::IIconHolder>();
            if (iconholder != NULL)
            {

                CORE::RefPtr<ELEMENTS::IMilitarySymbolType> milSymType = unit->getMilitarySymbolType();
                if (milSymType != NULL)
                {
                    CORE::RefPtr<ELEMENTS::IIcon> newIcon = milSymType->getIcon(unit->getAffiliation(),
                        unit->getPlan(),
                        unit->getSymbolColor(),
                        unit->getSymbolLineWidth(),
                        size);

                    iconholder->setIcon(newIcon);
                }
                
            }
            
        }
        
    }

    void ResourceManagerGUI::sphereRangeChanged ( double  value )
    {
        if(!_unitUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IMilitaryRange> range = _unitUIHandler->getMilitaryRange();
        if(range.valid())
        {
            range->setSphereRange ( value );
        }
    }

    void ResourceManagerGUI::deleteSymbol()
    {
        if(!_unitUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = _unitUIHandler->getMilitarySymbol();
        if(!unit.valid())
        {
            return;
        }

        CORE::IDeletable* deletable = unit->getInterface<CORE::IDeletable>();
        if(!deletable)
        {
            return;
        }


        CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
        if(deletionUIHandler.valid())
        {
            deletionUIHandler->deleteObject(deletable);
        }
    }

    void ResourceManagerGUI::rename(QString newName)
    {
        if(!_unitUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = _unitUIHandler->getMilitarySymbol();
        if(!unit.valid())
        {
            return;
        }

        if ( newName == "" )
        {
            showError("Edit Name", "Military symbol name cannot be blank.", true);

            {
                QObject* militarySymbolPointContextual = _findChild(MilitarySymbolContextualMenuObjectName);
                if( militarySymbolPointContextual != NULL)
                {
                    std::string oldName = unit->getInterface<CORE::IBase>()->getName();
                    militarySymbolPointContextual->setProperty("symbolName", QVariant::fromValue( QString::fromStdString(oldName) ));
                }
            }
            return;
        }

        unit->getInterface<CORE::IBase>()->setName(newName.toStdString());
    }

    void ResourceManagerGUI::handleLatLongAltChanged(QString attribute)
    {
        if(!_unitUIHandler.valid())
        {
            return;
        }

        // get current unit
        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = _unitUIHandler->getMilitarySymbol();
        if(!unit.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IPoint> point = unit->getInterface<CORE::IPoint>();

        QObject* militarySymbolPointContextual = _findChild(MilitarySymbolContextualMenuObjectName);
        if(militarySymbolPointContextual != NULL)
        {
            double longitudeD = point->getX();
            double latitudeD = point->getY();
            double altitudeD = point->getZ() - ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD);



            if(!attribute.compare("latitude", Qt::CaseInsensitive))
            {
                QString latitude = militarySymbolPointContextual->property("latitude").toString();
                if ( latitude == "" )
                {
                    showError("Edit Latitude", "Latitude value field cannot be blank.", true);
                    militarySymbolPointContextual->setProperty("latitude", QVariant::fromValue(QString::number(latitudeD,'f',4)));
                    return;
                }
                latitudeD = latitude.toDouble();
            }
            else if(!attribute.compare("longitude", Qt::CaseInsensitive))
            {
                QString longitude = militarySymbolPointContextual->property("longitude").toString();
                if ( longitude == "" )
                {
                    showError("Edit Longitude", "Longitude value field cannot be blank.", true);
                    militarySymbolPointContextual->setProperty("longitude", QVariant::fromValue(QString::number(longitudeD,'f',4)));
                    return;
                }

                longitudeD = longitude.toDouble();
            }
            else if(!attribute.compare("altitude", Qt::CaseInsensitive))
            {
                QString altitude = militarySymbolPointContextual->property("altitude").toString();
                //   if ( altitude == "" )
                //{
                // showError("Edit Name", "Military symbol name cannot be blank.", true);
                // return;
                //}

                altitudeD = altitude.toDouble();
            }

            osg::Vec3 position(longitudeD, latitudeD, altitudeD + ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD));

            point->setValue(position);
        }
    }

    void ResourceManagerGUI::connectSMPMenu(QString type)
    {
        if((type == "smpAvailableResourceMenu"))
        {            
            QObject* smpAvailableResourceMenu = _findChild("smpAvailableResourceMenu");
            if(smpAvailableResourceMenu != NULL)
            {
                QObject::connect(smpAvailableResourceMenu, SIGNAL(selectResource(QString)), this,
                    SLOT(selectResource(QString)), Qt::UniqueConnection);

                QObject::connect(smpAvailableResourceMenu, SIGNAL(deselectResource(QString)),
                    this, SLOT(deselectResource(QString)), Qt::UniqueConnection);

                QObject::connect(smpAvailableResourceMenu, SIGNAL(deleteResource(QString)), this, 
                    SLOT(deleteResource(QString)), Qt::UniqueConnection);

                QObject::connect(smpAvailableResourceMenu, SIGNAL(filter(QString)), this, 
                    SLOT(search(QString)), Qt::UniqueConnection);

                QObject::connect(smpAvailableResourceMenu, SIGNAL(destroyed()), this, 
                    SLOT(menuDestroyed()), Qt::UniqueConnection);
            }

            _populateResourceList();

            _subscribe(_unitUIHandler.get(), *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType);
            _subscribe(_unitUIHandler.get(), *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType);

            setActive(true);
        }
    }


    void ResourceManagerGUI::addMilitaryResource(QString qAffiliation, QString category, QString name)
    {
        if(!_resourceManagerComponent.valid())
        {
            emit showError("Resource Allocation", "Resource Manager Not Found");
            return;
        }
        QString fullResourceName = qAffiliation+"/"+category+"/"+name;

        _resourceManagerComponent->addResource(fullResourceName.toStdString());

        _populateResourceList();
    }

    void ResourceManagerGUI::selectResource(QString fullName)
    {
        if(!_unitUIHandler.valid())
        {
            emit showError("Resource Deployment", "Unit Handler Not Valid.");
            return;
        }

        _currentResourceFullName = fullName;

        _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_CREATE_UNIT);
        _unitUIHandler->setMilitarySymbolType(fullName.toStdString());
        _unitUIHandler->setTemporaryState(true);

    }

    void ResourceManagerGUI::deselectResource(QString uID)
    {
        _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE);
        _unitUIHandler->setTemporaryState(false);
        _unitUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

    }



    void ResourceManagerGUI::deleteResource(QString uID)
    {
        if(!_resourceManagerComponent.valid())
        {
            emit showError("Resource Allocation", "Resource Manager Not Found");
            return;
        }
        // delete resource

        //        _populateResourceList();
    }

    void ResourceManagerGUI::search(QString str)
    {
        _populateResourceList(str);
    }

    void ResourceManagerGUI::menuDestroyed()
    {
        _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE);
        _unitUIHandler->setTemporaryState(false);
        _unitUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

        _unsubscribe(_unitUIHandler.get(), *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType);
        _unsubscribe(_unitUIHandler.get(), *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType);

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();

        _setContextProperty("resourceListModel", QVariant::fromValue(NULL));
    }

    void ResourceManagerGUI::_populateResourceList(QString str)
    {
        if(!_resourceManagerComponent.valid())
        {
            emit showError("Resource Allocation", "Resource Manager Not Valid.");
            return;
        }

        _resourceList.clear();

        const SMCElements::IResourceManagerComponent::MapResourceToInstances& resourceMap = 
            _resourceManagerComponent->getResourceMap();

        CORE::RefPtr<CORE::IComponent> component = ELEMENTS::GetComponentFromMaintainer("LayerComponent");
        if(!component.valid())
        {
            emit showError("Resource Allocation", "Layer Component Not Found.");
            return;
        }

        CORE::RefPtr<SMCElements::ILayerComponent> layerComponent = 
            component->getInterface<SMCElements::ILayerComponent>();

        SMCElements::IResourceManagerComponent::MapResourceToInstances::const_iterator citer =
            resourceMap.begin();
        for(; citer !=resourceMap.end(); citer++)
        {
            QString fullName = QString::fromStdString(citer->first);

            int allocated = citer->second;
            int deployed = 0;

            QStringList tokens = fullName.split("/");
            if(tokens.size() < 3)
            {
                continue;
            }
            QString affiliation = tokens[0];
            QString category = tokens[1];
            QString type = tokens[2];

            if(!type.contains(str, Qt::CaseInsensitive))
            {
                continue;
            }

            std::string fullSymbolName = fullName.toStdString();

            CORE::RefPtr<CORE::IFeatureLayer> layer = 
                layerComponent->getFeatureLayer(fullSymbolName);
            if(layer.valid())
            {
                deployed = layer->getInterface<CORE::ICompositeObject>()->getObjectCount();
            }

            if(_symbolTypeLoader->isUniformAcrossAffiliation())
            {
                fullSymbolName = "Common/"+category.toStdString()+"/"+type.toStdString();
            }

            CORE::RefPtr<ELEMENTS::IMilitarySymbolType> symbolType =
                _symbolTypeLoader->getMilitarySymbolTypeByFullName(fullSymbolName);
            if(!symbolType.valid())
            {
                continue;
            }

            QString iconPath = QString::fromStdString(_unitUIHandler->getQMLIconPath(symbolType));

            _resourceList.append(new ResourceObject(iconPath, type, category, affiliation, allocated, deployed));                        
        }

        _setContextProperty("resourceListModel", QVariant::fromValue(_resourceList));
    }

    void ResourceManagerGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    CORE::WorldMaintainer::instance();
                if(wmain.valid())
                {
                    _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                    //! Get ResourceManagerComponent
                    CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("ResourceManagerComponent");
                    if(component.valid())
                    {
                        _resourceManagerComponent = component->getInterface<SMCElements::IResourceManagerComponent>();
                    }

                    //! Get SymbolTypeLoader Component
                    CORE::RefPtr<CORE::IComponent> component2 = wmain->getComponentByName("SymbolTypeLoader");
                    if(component2.valid())
                    {
                        _symbolTypeLoader = component2->getInterface<ELEMENTS::ISymbolTypeLoader>();
                    }

                    _unitUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IMilitarySymbolPointUIHandler>(getGUIManager());

                    if(_unitUIHandler.valid())
                    {
                        _subscribe(_unitUIHandler.get(), *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType);
                    }
                }                
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }

        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            if(!_resourceManagerComponent.valid())
            {
                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("ResourceManagerComponent");
                if(component.valid())
                {
                    _resourceManagerComponent = component->getInterface<SMCElements::IResourceManagerComponent>();
                }
            }

            CORE::RefPtr<CORE::IWorld> iworld = 
                APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());

            // Subscribe to object removed and added messages
            _subscribe(iworld.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
            _subscribe(iworld.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);


            if(!_unitUIHandler.valid())
            {
                _unitUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IMilitarySymbolPointUIHandler>(getGUIManager());
            }
        }
        else if(messageType == *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointCreatedMessageType)
        {

            CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit =  _unitUIHandler->getMilitarySymbol();
            if(unit.valid())
            {
                QObject* smpAvailableResourceMenu = _findChild("smpAvailableResourceMenu");
                if(smpAvailableResourceMenu)
                {
                    smpAvailableResourceMenu->setProperty("selectedResource", QVariant::fromValue(QString("")));
                }

                _createMetadataRecord();
                _configureSymbol();
                _unitUIHandler->setMode(SMCUI::IMilitarySymbolPointUIHandler::MILITARY_MODE_NONE);
            }
        }
        else if(messageType == *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointUpdatedMessageType)
        {
            _populateContextualMenu();
        }
        else if(messageType == *SMCUI::IMilitarySymbolPointUIHandler::MilitarySymbolPointDeletedMessageType)
        {
            QString layerName =  QString::fromStdString(_unitUIHandler->getMilitarySymbolType());

            foreach(QObject* obj, _resourceList)
            {
                ResourceObject* object = qobject_cast<ResourceObject*>(obj);
                if(object)
                {
                    QString fullName = object->affiliation()+"/"+object->category()+"/"+object->type();
                    if(fullName == layerName)
                    {
                        object->setDeployed(object->deployed()-1);
                        break;
                    }
                }
            }
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

    void ResourceManagerGUI::_configureSymbol()
    {
        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit =  _unitUIHandler->getMilitarySymbol();
        if(unit.valid())
        {

            //            unit->setSymbolLineWidth(6.0); 
            //            unit->update();

            QStringList tokens = _currentResourceFullName.split("/");
            if(tokens.size() < 3)
            {
                return;
            }

            //XXX :- had to be done here again as OGRFeature was not formed when affiliation was applied previously.
            ELEMENTS::IMilitarySymbolType::Affiliation affiliation = ELEMENTS::getAffiliationFromString(tokens[0].toStdString());
            unit->setAffiliation(affiliation);

            QString name = tokens[2];
            unit->getInterface<CORE::IBase>()->setName(name.toStdString());

            ELEMENTS::IRepresentation::RepresentationMode representationMode = ELEMENTS::IRepresentation::ICON_AND_TEXT;

            CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                CORE::WorldMaintainer::instance();
            if(wmain.valid())
            {
                CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("ProjectSettingsComponent");
                if(component.valid())
                {
                    representationMode = 
                        component->getInterface<ELEMENTS::IProjectSettingsComponent>()->getMilitarySymbolRepresentationMode();
                }
            }


            CORE::RefPtr<ELEMENTS::IRepresentation> represent = unit->getInterface<ELEMENTS::IRepresentation>();
            if(represent)
            {
                represent->setRepresentationMode(representationMode);
            }

            foreach(QObject* obj, _resourceList)
            {
                ResourceObject* object = qobject_cast<ResourceObject*>(obj);
                if(object)
                {
                    QString fullName = object->affiliation()+"/"+object->category()+"/"+object->type();
                    if(fullName == _currentResourceFullName)
                    {
                        object->setDeployed(object->deployed()+1);
                        break;
                    }
                }
            }
        }
    }

    void ResourceManagerGUI::_createMetadataRecord()
    {
        CORE::RefPtr<CORE::IMetadataRecord> record;

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = ELEMENTS::getMetaDataCreator();

        if(!metadataCreator.valid())
            return;

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn =
            _unitUIHandler->getCurrentSelectedLayerDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Invalid IMetadataTableDefn instance");
            return;
        }

        record = metadataCreator->createMetadataRecord();

        if(!record.valid())
        {
            return;
        }

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> unit = _unitUIHandler->getMilitarySymbol();
        
        CORE::RefPtr<ELEMENTS::IModelHolder> modelHolder = unit->getInterface < ELEMENTS::IModelHolder >();
        
        osg::Vec3d facingDirection = modelHolder->getModelFacingDirection(); 

        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("FacingDirectionX");
            if (field.valid())
            {
                field->fromDouble(facingDirection.x());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("FacingDirectionY");
            if (field.valid())
            {
                field->fromDouble(facingDirection.y());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("FacingDirectionZ");
            if (field.valid())
            {
                field->fromDouble(facingDirection.z());
            }
        }

        osg::Vec3d normalDirection = modelHolder->getModelNormalDirection();

        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("NormalDirectionX");
            if (field.valid())
            {
                field->fromDouble(normalDirection.x());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("NormalDirectionY");
            if (field.valid())
            {
                field->fromDouble(normalDirection.y());
            }
        }
        {
            CORE::RefPtr<CORE::IMetadataField> field = record->getFieldByName("NormalDirectionZ");
            if (field.valid())
            {
                field->fromDouble(normalDirection.z());
            }
        }

        _unitUIHandler->setMetadataRecord(record.get());

    }

}