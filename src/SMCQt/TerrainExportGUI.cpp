/****************************************************************************
*
* File             : TerrainExportGUI.h
* Description      : TerrainExportGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/
//#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/TerrainExportGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/QMLTreeModel.h>
#include <SMCQt/VizComboBoxElement.h>
#include <SMCQt/ClassificationObject.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <Core/WorldMaintainer.h>
#include <Core/IFeatureLayer.h>

#include <VizUI/VizUIPlugin.h>
#include <VizUI/ITerrainPickUIHandler.h>

#include <Terrain/IElevationObject.h>
#include <Terrain/IModelObject.h>
#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>

#include <GISCompute/IFilterStatusMessage.h>
#include <QFileDialog>

#include <Util/FileUtils.h>
#include <Util/Log.h>
#include<iostream>
#include <osgDB/FileUtils>
#include <libjson.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <osgEarth/GeoData>
#include <osgEarth/SpatialReference>
#include <Util/GdalUtils.h>

#include <DGN/IDGNFolder.h>
#include <Core/ICompositeObject.h>
#include <Util/Log.h>
#include <Util/CalculateExtentUtils.h>

namespace fs = boost::filesystem;

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, TerrainExportGUI);
    DEFINE_IREFERENCED(TerrainExportGUI, DeclarativeFileGUI);

    const std::string TerrainExportGUI::TerrainExportPopup = "TerrainExport";
    const std::string TerrainExportGUI::SnowConvertPopup = "SnowConvert";
    const std::string TerrainExportGUI::ClassificationJson = "../../config/ClassificationConfig.json";
    const std::string TerrainExportGUI::TempDir = (fs::path(UTIL::getAppDataPath()) / ("TerrainExport_TempData")).string();
    TerrainExportGUI::TerrainExportGUI()
    {

    }
    TerrainExportGUI::~TerrainExportGUI()
    {}
    void TerrainExportGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

    }
    void TerrainExportGUI::_handleProgressValueChanged(int value, std::string message)
    {
        QString str = QString::fromStdString("STATUS : " + message + " completed...");
        LOG_INFO(str.toStdString());
        QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
        if (TerrainExportAnalysisObject)
        {
            TerrainExportAnalysisObject->setProperty("progressValue", QVariant::fromValue(value));

            TerrainExportAnalysisObject->setProperty("progressMessage", QVariant::fromValue(str).toString());
        }

        QObject* snowCoverObject = _findChild(SnowConvertPopup);

        if (snowCoverObject)
        {
            snowCoverObject->setProperty("progressValue", QVariant::fromValue(value)); 

            //if process is complete, hide progress bar 
            if (value == 100)
                snowCoverObject->setProperty("progressBarVisible", QVariant::fromValue(false)); 
            else 
                snowCoverObject->setProperty("progressBarVisible", QVariant::fromValue(true));
        }
    }
    void TerrainExportGUI::_cancelButtonClicked(bool hasUserCancelled)
    {
        _uiHandler->cancelExport(hasUserCancelled);
        QString str = QString::fromStdString("STATUS : canceled...");
        QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
        if (TerrainExportAnalysisObject)
        {
            postTerrainExport();
            TerrainExportAnalysisObject->setProperty("progressValue", QVariant::fromValue(0));
            TerrainExportAnalysisObject->setProperty("progressMessage", QVariant::fromValue(str).toString());
            //reset the state of the cancel button
            TerrainExportAnalysisObject->setProperty("isCancelButtonEnabled", "true");
            TerrainExportAnalysisObject->setProperty("isCancelButtonSelected", "false");

        }
    }
    void TerrainExportGUI::_browseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        std::string label = "Export Terrain Data";
        QString files;
        QString filepath = QFileDialog::getExistingDirectory(parent, label.c_str(), directory.toStdString().c_str());
        files = filepath;
        QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
        if (files.isEmpty())
            return;

        if (TerrainExportAnalysisObject)
        {
            TerrainExportAnalysisObject->setProperty("selectedDir", QVariant::fromValue(files).toString());

            //! Check if this directory contains "extents.json". 
            //! Prompt the user that this already contains a terrain dump.
            //! Layers exported with the same name would be overriden.
            fs::path exportFolder = files.toStdString();
            fs::path extentsFile = exportFolder / "extents.json";
            if (fs::exists(extentsFile))
            {
                _readPreviousDumpInfo(extentsFile.string());
            }
        }

        QObject* snowCoverObject = _findChild(SnowConvertPopup);

        if (snowCoverObject)
        {
            snowCoverObject->setProperty("selectedDir", QVariant::fromValue(files).toString());
        }

    }

    void TerrainExportGUI::_readPreviousDumpInfo(std::string extentsFile)
    {
        QString message = "Selected folder already contains a terrain dump.\n Layers exported with same name would be overridden.";
        emit showError("Terrain Export", message, false);

        osgDB::ifstream inFile(extentsFile.c_str());
        if (!inFile.is_open())
        {
            LOG_ERROR("Unable to open extents.json file. Not populating the extents");
            return;
        }

        //read content from file into a string stream 
        std::ostringstream buffer;
        buffer << inFile.rdbuf();

        //parse json file 
        JSONNode mainNode;
        try
        {
            mainNode = libjson::parse(buffer.str());
        }
        catch (...)
        {
            //error parsing json file 
            LOG_ERROR("Unable to open extents.json file. Not populating the extents");
            return;
        }

        osg::Vec4d extents;
        osg::Vec3d centerPointUTM;
        int sideLength;
        //int tileSize
        JSONNode::iterator iter = mainNode.end();
#define READ_EXTENTS_FROM_JSON(EXT_STRING, EXT_VALUE) \
    iter = mainNode.find(EXT_STRING);	\
    if (iter != mainNode.end())	\
            {   \
        JSONNode vectorTypes = *iter;	\
        EXT_VALUE = iter->as_float();	\
            }
        READ_EXTENTS_FROM_JSON("Extents Bottom Left Longitude", extents.x());
        READ_EXTENTS_FROM_JSON("Extents Bottom Left Latitude", extents.y());
        READ_EXTENTS_FROM_JSON("Extents Top Right Longitude", extents.z());
        READ_EXTENTS_FROM_JSON("Extents Top Right Latitude", extents.w());
        READ_EXTENTS_FROM_JSON("Center Point UTM X", centerPointUTM.x());
        READ_EXTENTS_FROM_JSON("Center Point UTM Y", centerPointUTM.y());
        READ_EXTENTS_FROM_JSON("SideLength", sideLength);

#undef READ_EXTENTS_FROM_JSON

        LOG_DEBUG_VAR("Extents Read from dump folder : %f %f %f %f", extents.x(), extents.y(), extents.z(), extents.w());

        QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
        if (TerrainExportAnalysisObject)
        {
            TerrainExportAnalysisObject->setProperty("bottomLeftLongitude", UTIL::ToString(extents.x()).c_str());
            TerrainExportAnalysisObject->setProperty("bottomLeftLatitude", UTIL::ToString(extents.y()).c_str());
            TerrainExportAnalysisObject->setProperty("topRightLongitude", UTIL::ToString(extents.z()).c_str());
            TerrainExportAnalysisObject->setProperty("topRightLatitude", UTIL::ToString(extents.w()).c_str());
            QMetaObject::invokeMethod(TerrainExportAnalysisObject, "setSideLength", Q_ARG(QVariant, QVariant(QString::number(sideLength))));
        }

        _uiHandler->setTileSize(TILE_SIZE);
        double side = _getOptimisedSideLength();

        _uiHandler->setLatSpan(side);
        _uiHandler->setLongSpan(side);
        _uiHandler->setCenter(centerPointUTM);

        _areaHandler->setFixedRectangleWidthHeight(osg::Vec2d(side, side));
        _areaHandler->_setProcessMouseEvents(false);
        _areaHandler->setTemporaryState(true);

        _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
        _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_FIXED_RECTANGLE);
        _areaHandler->setExtents(extents);
    }


    double TerrainExportGUI::_getOptimisedSideLength()
    {
        /**
          * for 10 Km area we will have 5 tiles.
          * so accordingly no of overlapping pixel per scanline would be no_tiles - 1 .
          * so the total side length would be (no_tiles * tile_size) - overlap_pixels
          **/
        double side = TILE_SIZE * 5 - 4;
        QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
        if (TerrainExportAnalysisObject)
        {
            bool widthOK = true;
            //std::string widthStr = TerrainExportAnalysisObject->property("sideLength").toString().toStdString;
            int width = TerrainExportAnalysisObject->property("sideLength").toString().toFloat(&widthOK);
            if (!widthOK) width = 10000;

            int no_overLappingPix = width / TILE_SIZE;
            int no_tiles = no_overLappingPix + 1;
            side = (TILE_SIZE * no_tiles) - no_overLappingPix; // 10081 : 10 km : optimised value for unreal.
        }
        return side;
    }
    void TerrainExportGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::ITerrainExportUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if (messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
            if (TerrainExportAnalysisObject)
            {
                //extents in longlat format
                osg::Vec4d extents = _areaHandler->getExtents();
                
                //set extents in UIhandler in UTM format
                const osgEarth::SpatialReference* utm = osgEarth::SpatialReference::get(UTIL::GDALUtils::UTM_ZONE43_SREF);
                const osgEarth::SpatialReference* wgs84 = osgEarth::SpatialReference::get("EPSG:4326");

                osg::Vec3d center = _areaHandler->getClickedPoint();
                osgEarth::GeoPoint centerPt(wgs84, center.x(), center.y());
                osgEarth::GeoPoint centerPtUTM = centerPt.transform(utm);
                if (centerPtUTM.isValid())
                {
                    _uiHandler->setCenter(osg::Vec3d(centerPtUTM.x(), centerPtUTM.y(), centerPtUTM.z()));
                }

                TerrainExportAnalysisObject->setProperty("selectedDir", _uiHandler->getOutDir().c_str());
                TerrainExportAnalysisObject->setProperty("bottomLeftLongitude", UTIL::ToString(extents.x()).c_str());
                TerrainExportAnalysisObject->setProperty("bottomLeftLatitude", UTIL::ToString(extents.y()).c_str());
                TerrainExportAnalysisObject->setProperty("topRightLongitude", UTIL::ToString(extents.z()).c_str());
                TerrainExportAnalysisObject->setProperty("topRightLatitude", UTIL::ToString(extents.w()).c_str());
            }

            QObject* snowCoverObject = _findChild(SnowConvertPopup);
            if (snowCoverObject)
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if (line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first = line->getPoint(0)->getValue();
                    osg::Vec3d second = line->getPoint(2)->getValue();

                    snowCoverObject->setProperty("bottomLeftLongitude", UTIL::ToString(first.x()).c_str());
                    snowCoverObject->setProperty("bottomLeftLatitude", UTIL::ToString(first.y()).c_str());
                    snowCoverObject->setProperty("topRightLongitude", UTIL::ToString(second.x()).c_str());
                    snowCoverObject->setProperty("topRightLatitude", UTIL::ToString(second.y()).c_str());
                }
            }
        }
        else if (messageType == *SMCUI::ITerrainExportUIHandler::ProgressValueUpdatedMessageType)
        {
            QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
            if (TerrainExportAnalysisObject)
            {
                int progress = _layersProcessed * 100 / _totalLayers;
                LOG_INFO("Layers... " + std::to_string(_layersProcessed) + " / " + std::to_string(_totalLayers));
                if (progress < 100)
                    _handleProgressValueChanged(progress, _uiHandler->getLastCompletedLayer());

                else if (progress >= 100)
                {
                    _timer.stop();
                    double elapsed = _timer.elapsed();

                    _handleProgressValueChanged(100, "All Layers Exported");
                    std::string messageStr = "Terrain Exported in " + std::to_string(elapsed) + "sec.";
                    emit showError("Terrain Export Status", messageStr.c_str(), false, "postTerrainExport()");
                }
            }

            QObject* snowCoverObject = _findChild(SnowConvertPopup);
            if (snowCoverObject)
            {
                _handleProgressValueChanged(_progressStatus, ""); 
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }
    void TerrainExportGUI::postTerrainExport()
    {
        _handleExportPressed(false);
        _handleMarkButtonPressed(false);
        //after Execution Delete Temp Dir
        if (fs::exists(TempDir) && fs::is_directory(TempDir))
        {
            try
            {
                LOG_INFO("Removing Dir : " + TempDir);
                fs::remove_all(TempDir);
            }
            catch (...)
            {
                LOG_ERROR("Can not remove Dir " + TempDir);
            }
        }
        //TODO :: If all layers arent success then _handlecancelButtonPressed(true)
    }
    // XXX - Move these slots to a proper place
    void TerrainExportGUI::setActive(bool value)
    {
        if (value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            if (value)
            {
                QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
                if (TerrainExportAnalysisObject)
                {
                    // start and stop button handlers
                    connect(TerrainExportAnalysisObject, SIGNAL(terrainExport(bool)), this, SLOT(_handleExportPressed(bool)), Qt::UniqueConnection);
                    connect(TerrainExportAnalysisObject, SIGNAL(markFixedArea(bool)), this, SLOT(_handleMarkButtonPressed(bool)), Qt::UniqueConnection);
                    connect(TerrainExportAnalysisObject, SIGNAL(browseButtonClicked()), this, SLOT(_browseButtonClicked()), Qt::UniqueConnection);
                    connect(TerrainExportAnalysisObject, SIGNAL(cancelButtonClicked(bool)), this, SLOT(_cancelButtonClicked(bool)), Qt::UniqueConnection);
                    // enable start button
                    TerrainExportAnalysisObject->setProperty("isStartButtonEnabled", true);

                    osgDB::ifstream inFile(ClassificationJson.c_str());
                    if (!inFile.is_open())
                        return;

                    //read content from file into a string stream 
                    std::ostringstream buffer;
                    buffer << inFile.rdbuf();

                    //parse json file 
                    JSONNode mainNode;
                    try
                    {
                        mainNode = libjson::parse(buffer.str());
                    }
                    catch (...)
                    {
                        //error parsing json file 
                    }

                    //read Stylesheet 
                    JSONNode::iterator iter = mainNode.find("VectorClassificationTypes");
                    vectorClassificationTypeList.clear();
                    std::string defaultVectorType = "";

                    if (iter != mainNode.end())
                    {
                        JSONNode vectorTypes = *iter;
                        defaultVectorType = vectorTypes.begin()->as_string();
                        for (JSONNode::iterator i = vectorTypes.begin(); i != vectorTypes.end(); ++i)
                        {
                            //std::cout << i->as_string().c_str() << std::endl;
                            vectorClassificationTypeList.append(new VizComboBoxElement(i->as_string().c_str(), i->as_string().c_str()));
                        }
                    }
                    _setContextProperty("vectorTypeList", QVariant::fromValue(vectorClassificationTypeList));

                    JSONNode::iterator iter2 = mainNode.find("RasterClassificationTypes");
                    rasterClassificationTypelist.clear();
                    std::string defaultRasterType = "";

                    if (iter2 != mainNode.end())
                    {
                        JSONNode rasterTypes = *iter2;
                        defaultRasterType = rasterTypes.begin()->as_string();
                        for (JSONNode::iterator i = rasterTypes.begin(); i != rasterTypes.end(); ++i)
                        {
                            //std::cout << i->as_string().c_str() << std::endl;
                            rasterClassificationTypelist.append(new VizComboBoxElement(i->as_string().c_str(), i->as_string().c_str()));
                        }
                    }
                    _setContextProperty("rasterTypeList", QVariant::fromValue(rasterClassificationTypelist));


                    CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
                    const CORE::WorldMap& worldMap = worldMaintainer->getWorldMap();
                    CORE::RefPtr<CORE::IWorld> iworld = NULL;
                    if (worldMap.size() > 0)
                    {
                        iworld = worldMap.begin()->second;
                    }
                    const CORE::ObjectMap& omap = iworld->getObjectMap();
                    vectorList.clear();
                    rasterList.clear();
                    for (CORE::ObjectMap::const_iterator citer = omap.begin();
                        citer != omap.end();
                        ++citer)
                    {
                        CORE::RefPtr<CORE::IObject> object = citer->second;

                        if (object->getInterface<TERRAIN::IRasterObject>())
                        {
                            CORE::RefPtr<TERRAIN::IRasterObject> rasterObject = object->getInterface <TERRAIN::IRasterObject>();
                            std::string rasterFile = rasterObject->getURL();
                            fs::path filepath(rasterFile);
                            std::string ext = filepath.extension().string();
                            //TODO check for gdal supported raster file.
                            if (true)
                            {
                                std::string name = fs::path(rasterObject->getURL()).stem().string();
                                rasterList.append(new ClassificationObject(name.c_str(), defaultRasterType.c_str(), true));
                            }
                        }
                        _setContextProperty("rasterTypeList", QVariant::fromValue(rasterClassificationTypelist));

                        if (object->getInterface<TERRAIN::IElevationObject>())
                        {
                            CORE::RefPtr<TERRAIN::IElevationObject> elevationObject = object->getInterface <TERRAIN::IElevationObject>();
                            std::string elevationFile = elevationObject->getURL();
                            fs::path filepath(elevationFile);
                            std::string ext = filepath.extension().string();
                            //TODO check for gdal supported elevation file.
                            if (UTIL::GDALUtils::isSupportedRaster(elevationFile))
                            {
                                std::string name = fs::path(elevationObject->getURL()).stem().string();
                                rasterList.append(new ClassificationObject(name.c_str(), defaultRasterType.c_str(), true));
                            }
                        }
                        if (object->getInterface<CORE::IFeatureLayer>())
                        {
                            std::string name = "";
                            CORE::RefPtr<CORE::IBase> base = object->getInterface<CORE::IBase>();
                            if (base)
                            {
                                name = base->getName();
                                vectorList.append(new ClassificationObject(name.c_str(), defaultVectorType.c_str(), true));
                            }

                        }
                        if (object->getInterface<TERRAIN::IModelObject>())
                        {
                            CORE::RefPtr<TERRAIN::IModelObject> modelObject = object->getInterface <TERRAIN::IModelObject>();
                            std::string vectorFile = modelObject->getUrl();
                            fs::path filepath(vectorFile);
                            std::string ext = filepath.extension().string();
                            if (boost::iequals(ext, ".shp"))
                            {
                                std::string name = fs::path(modelObject->getUrl()).stem().string();
                                vectorList.append(new ClassificationObject(name.c_str(), defaultVectorType.c_str(), true));
                            }

                        }
                        if (object->getInterface<DGN::IDGNFolder>())
                        {
                            CORE::RefPtr<DGN::IDGNFolder> dgn = object->getInterface <DGN::IDGNFolder>();
                            std::string name = fs::path(dgn->getURL()).stem().string();
                            vectorList.append(new ClassificationObject(name.c_str(), "DGN", true));
                        }

                    }
                    
                    _setContextProperty("vectorList", QVariant::fromValue(vectorList));
                    _setContextProperty("rasterList", QVariant::fromValue(rasterList));
                }


                QObject* snowCoverObject = _findChild(SnowConvertPopup);

                if (snowCoverObject)
                {
                    connect(snowCoverObject, SIGNAL(populateAttributes(QString)), this, SLOT(_populateVectorAttributes(QString)), Qt::UniqueConnection);
                    connect(snowCoverObject, SIGNAL(browseButtonClicked()), this, SLOT(_browseButtonClicked()), Qt::UniqueConnection);
                    connect(snowCoverObject, SIGNAL(generateButtonClicked()), this, SLOT(_generateRasterButtonClicked()), Qt::UniqueConnection);
                    connect(snowCoverObject, SIGNAL(markPosition(bool)), this, SLOT(_handleMarkButtonPressed(bool)), Qt::UniqueConnection);

                    CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
                    const CORE::WorldMap& worldMap = worldMaintainer->getWorldMap();
                    CORE::RefPtr<CORE::IWorld> iworld = NULL;
                    if (worldMap.size() > 0)
                    {
                        iworld = worldMap.begin()->second;
                    }

                    std::string defaultVectorType = "";

                    const CORE::ObjectMap& omap = iworld->getObjectMap();
                    vectorClassificationTypeList.clear();
                    bool firstVectorObject = true; 
                    for (CORE::ObjectMap::const_iterator citer = omap.begin();
                        citer != omap.end();
                        citer++)
                    {
                        CORE::RefPtr<CORE::IObject> object = citer->second;

                        if (object->getInterface<TERRAIN::IModelObject>())
                        {

                            CORE::RefPtr<TERRAIN::IModelObject> modelObject = object->getInterface <TERRAIN::IModelObject>();
                            std::string vectorFile = modelObject->getUrl();
                            fs::path filepath(vectorFile);
                            std::string ext = filepath.extension().string();
                            if (boost::iequals(ext, ".shp"))
                            {
                                std::string name = filepath.stem().string();
                                vectorClassificationTypeList.append(new VizComboBoxElement(name.c_str(), filepath.string().c_str()));
                            }

                            if (firstVectorObject)
                            {
                                std::vector<std::string> abbtibuteList;
                                _uiHandler->populateVectorAttributes(filepath.string(), abbtibuteList);

                                QList<QObject*> attributeQlist;
                                for (int i = 0; i < abbtibuteList.size(); i++)
                                {
                                    attributeQlist.push_back(new VizComboBoxElement(abbtibuteList[i].c_str(), abbtibuteList[i].c_str()));
                                }

                                _setContextProperty("attributeList", QVariant::fromValue(attributeQlist));
                                firstVectorObject = false; 
                            }
                        }
                    }

                    _setContextProperty("vectorTypeList", QVariant::fromValue(vectorClassificationTypeList));
                   
                }
            }
            else
            {
                _reset();
            }

            uiHandler->setFocus(value);
            DeclarativeFileGUI::setActive(value);
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void TerrainExportGUI::_populateVectorAttributes(QString file)
    {
        std::vector<std::string> abbtibuteList; 
        _uiHandler->populateVectorAttributes(file.toStdString(), abbtibuteList);

        QList<QObject*> attributeQlist; 
        for (int i = 0; i < abbtibuteList.size(); i++)
        {
            attributeQlist.push_back(new VizComboBoxElement(abbtibuteList[i].c_str(), abbtibuteList[i].c_str()));
        }

        _setContextProperty("attributeList", QVariant::fromValue(attributeQlist));
    }

    void TerrainExportGUI::_generateRasterButtonClicked()
    {
        QObject* snowCoverObject = _findChild(SnowConvertPopup);

        if (snowCoverObject)
        {
            std::string snowDataFile = snowCoverObject->property("snowDataFile").toString().toStdString();
            std::string snowStability = snowCoverObject->property("snowStability").toString().toStdString();
            std::string snowType = snowCoverObject->property("snowType").toString().toStdString();
            std::string snowDepth = snowCoverObject->property("snowDepth").toString().toStdString();
            std::string selectedDir = snowCoverObject->property("selectedDir").toString().toStdString();

            double bottomLeftLongitude = snowCoverObject->property("bottomLeftLongitude").toDouble();
            double bottomLeftLatitude = snowCoverObject->property("bottomLeftLatitude").toDouble();
            double topRightLongitude = snowCoverObject->property("topRightLongitude").toDouble();
            double topRightLatitude = snowCoverObject->property("topRightLatitude").toDouble();

            double xResolution = snowCoverObject->property("xResolution").toDouble();
            double yResolution = snowCoverObject->property("yResolution").toDouble();

            if (selectedDir.empty())
                return; 

            std::vector<std::string> attribute;
            attribute.push_back(snowStability);
            attribute.push_back(snowType);
            attribute.push_back(snowDepth);

            osg::Vec4d extents;
            extents.w() = bottomLeftLongitude;
            extents.x() = topRightLongitude;
            extents.y() = topRightLatitude;
            extents.z() = bottomLeftLatitude;

            osg::Vec2d resolution(xResolution, yResolution);

            //subscribe to progress value message 
            _subscribe(_uiHandler.get(), *SMCUI::ITerrainExportUIHandler::ProgressValueUpdatedMessageType);

            _uiHandler->generateRasterFromVector(snowDataFile, attribute, selectedDir,extents,resolution, &_progressStatus);
        }


    }

    //! 
    void TerrainExportGUI::_handleExportPressed(bool pressed)
    {
        if (pressed)
        {
            _subscribe(_uiHandler.get(), *SMCUI::ITerrainExportUIHandler::ProgressValueUpdatedMessageType);

            if (!_uiHandler.valid())
            {
                return;
            }

            //build classification map form the layers that are in memory
            std::map<std::string, std::string> classificationMap;
            int size = vectorList.size();
            for (int i = 0; i < size; i++)
            {
                ClassificationObject* obj = (ClassificationObject*)vectorList[i];
                if (obj->state())
                    classificationMap[obj->name().toStdString()] = obj->type().toStdString();
            }
            size = rasterList.size();
            for (int i = 0; i < size; i++)
            {
                ClassificationObject* obj = (ClassificationObject*)rasterList[i];
                if (obj->state())
                    classificationMap[obj->name().toStdString()] = obj->type().toStdString();
            }

            _uiHandler->setClassificationMap(classificationMap);

            QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
            if (TerrainExportAnalysisObject)
            {
                std::string outDir = TerrainExportAnalysisObject->property("selectedDir").toString().toStdString();
                if (!outDir.empty())
                {
                    _uiHandler->setOutDir(outDir);
                    _uiHandler->setTempDir((fs::path(TempDir) / fs::path(outDir).filename().stem()).string());
                }
                else
                {
                    //Default Path;
                }

                TerrainExportAnalysisObject->setProperty("progressValue", 0);
                _handleMarkButtonPressed(false);

                osg::Vec4d extents = _areaHandler->getExtents();
                osg::Vec4d extentsUTM = _areaHandler->getExtentsUTM();
                _uiHandler->setExtents(extents);
                _uiHandler->setExtentsUTM(extentsUTM);

                _layersProcessed = 0;
                _totalLayers = 0;

                // createFiles required files in Temp Dir
                _totalLayers = _uiHandler->verifyAndCountLayers();

                if (_totalLayers > 0)
                {
                    //Ideally we should be notified the cancel process is complete and
                    //on that notification we should cancel
                    // using this small hack till then reset the state of the cancel 
                    // button  before executing
                    _uiHandler->cancelExport(false);
                    _timer.start();
                    _uiHandler->execute(&_layersProcessed);
                }

                if (_totalLayers < 0)
                {
                    emit showError("Data Error", "Terrain Data(Raster) to be Exported isn't sufficient for the window size", false, "postTerrainExport()");
                    return;
                }
            }
        }
        else
        {
            _unsubscribe(_uiHandler.get(), *SMCUI::ITerrainExportUIHandler::ProgressValueUpdatedMessageType);
            QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
            if (TerrainExportAnalysisObject)
            {
                TerrainExportAnalysisObject->setProperty("isExportSelected", false);
                TerrainExportAnalysisObject->setProperty("isProgressBarVisible", false);
                TerrainExportAnalysisObject->setProperty("isProgressMessageVisible", false);

            }
        }
    }
    void TerrainExportGUI::_handleMarkButtonPressed(bool pressed)
    {
        QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
        if (TerrainExportAnalysisObject)
        {

            if (pressed)
            {
                _uiHandler->setTileSize(TILE_SIZE);
                double side = _getOptimisedSideLength();
                _uiHandler->setLatSpan(side);
                _uiHandler->setLongSpan(side);

            _areaHandler->setFixedRectangleWidthHeight(osg::Vec2d(side, side));
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);

            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_FIXED_RECTANGLE);

            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

                QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
                if (TerrainExportAnalysisObject)
                {
                    TerrainExportAnalysisObject->setProperty("isMarkSelected", false);
                }
            }
        }
        else
        {
            QObject* snowCoverObject = _findChild(SnowConvertPopup);

            if (snowCoverObject)
            {
                if (pressed)
                {
                    _areaHandler->_setProcessMouseEvents(true);
                    _areaHandler->setTemporaryState(true);
                    //            _areaHandler->setHandleMouseClicks(false);
                    _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                    _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

                    //           _areaHandler->setBorderWidth(3.0);
                    //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
                    _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
                }
                else
                {
                    _areaHandler->_setProcessMouseEvents(false);
                    _areaHandler->setTemporaryState(false);
                    _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                    _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
                    _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

                }
            }

        }


    }
    void TerrainExportGUI::_reset()
    {
        QObject* TerrainExportAnalysisObject = _findChild(TerrainExportPopup);
        if (TerrainExportAnalysisObject)
        {
            _handleMarkButtonPressed(false);
            TerrainExportAnalysisObject->setProperty("isCancelButtonSelected", false);
            TerrainExportAnalysisObject->setProperty("bottomLeftLongitude", "longitude");
            TerrainExportAnalysisObject->setProperty("bottomLeftLatitude", "latitude");
            TerrainExportAnalysisObject->setProperty("topRightLongitude", "longitude");
            TerrainExportAnalysisObject->setProperty("topRightLatitude", "latitude");
            TerrainExportAnalysisObject->setProperty("selectedDir", "c:/");

            APP::IUIHandler* areaUIHandler = _areaHandler->getInterface<APP::IUIHandler>();
            areaUIHandler->setFocus(false);
        }
        QObject* snowCoverObject = _findChild(SnowConvertPopup);

        if (snowCoverObject)
        {
            APP::IUIHandler* areaUIHandler = _areaHandler->getInterface<APP::IUIHandler>();
            areaUIHandler->setFocus(false);
        }
    }
}//namespace SMCQt
