
#include <sstream>
#include <string>
#include <SMCQt/RasterWindowGUI.h>
#include <Stereo/MainWindowGUI.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>

#include <App/AccessElementUtils.h>
#include <Util/BaseExceptions.h>

#include <VizUI/ISelectionUIHandler.h>

#include <Core/ISelectionComponent.h>

#include <Core/IWorldMaintainer.h>
#include <Core/INamedAttributeMessage.h>
#include <Core/IMessage.h>
#include <Core/ISelectionComponent.h>
#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>
#include <Core/IWorldMaintainer.h>
#include <Core/INamedAttributeMessage.h>
#include <Core/IMessage.h>
#include <Core/IObject.h>


namespace SMCQt
{  

    RasterWindowGUI::RasterWindowGUI()
        :_stereoWizard(NULL)

    {
    }

    RasterWindowGUI::~RasterWindowGUI()
    {

    }

    void RasterWindowGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *CORE::ISelectionComponent::SelectionMessageType)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if(selectionUIHandler.valid())
            {
                _selectObject();                   
                _subscribe(selectionUIHandler.get(), *CORE::ISelectionComponent::ObjectLongPressedMessageType);

            }

        }


    }

    void RasterWindowGUI::_selectObject()
    {

        // Selection UIHandler
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if(!selectionUIHandler.valid())
        {
            return;
        }

        // Selection Map
        const CORE::ISelectionComponent::SelectionMap& map = 
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
            map.begin();  

        //XXX - using the first element in the selection
        if(iter == map.end())
        {
            return;
        }

        if(iter->second->getInterface<TERRAIN::IRasterObject>())
        {
            CORE::RefPtr<CORE::IObject> object =  iter->second->getInterface<CORE::IObject>();
            TERRAIN::IRasterObject* raster = object->getInterface<TERRAIN::IRasterObject>();
            _enhancementType = static_cast<Stereo::Image::EnhancementType>(raster->getEnhancementType());
            url = raster->getRuntimeURL();            
        }
        else if(iter->second->getInterface<TERRAIN::IElevationObject>())
        {
            CORE::RefPtr<CORE::IObject> object =  iter->second->getInterface<CORE::IObject>();
            TERRAIN::IElevationObject* elevation = object->getInterface<TERRAIN::IElevationObject>();
            url = elevation->getURL();

        }
    }




    void RasterWindowGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }
    }

    void
        RasterWindowGUI::popupLoaded(QString)
    {
        QObject* createOverview = _findChild("CreateOverviewPopUp");
        if(createOverview != NULL)
        {
            QObject::connect( createOverview, SIGNAL(okButtonClicked()), this ,SLOT(okButtonClicked()), Qt::UniqueConnection);
        }

    }

    void RasterWindowGUI::okButtonClicked()
    {
        std::string str = "gdaladdo -r average ";
        str = str + url;
        int level =0;
        QObject* createOverview = _findChild("CreateOverviewPopUp");
        if(createOverview)
        {            
            level= createOverview->property("levelNumber").toInt();
            if (!level)
            {
                showError("Invalid level","Enter valid level value");
            }
        }
        int n=1;
        for (int i=0; i<level; i++)
        {
            n=n*2;           
            std::string temp;
            std::ostringstream convert;
            convert<<n;
            temp = convert.str();
            str= str + " " +temp ;

        }
        const char* c = str.c_str();
        system(c);
    }


    void RasterWindowGUI::connectSmpMenu(QString type)
    {
        if(type == "ContextualRasterTab")
        {
            QObject* contextualRasterTab = _findChild("ContextualRasterTab");
            if(contextualRasterTab)
            {
                QObject::connect(contextualRasterTab, SIGNAL(rasterWindow()), 
                    this, SLOT(rasterWindow()), Qt::UniqueConnection);             
            }
        }
    }

    void
        RasterWindowGUI::onAddedToGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            APP::IManager* manager = getGUIManager()->getInterface<APP::IManager>();
            CORE::IWorldMaintainer* maintainer =
                APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IManager>(manager);
            _subscribe(maintainer, *CORE::IWorld::WorldLoadedMessageType);
            const CORE::ComponentMap cmap = maintainer->getComponentMap();
            CORE::RefPtr<CORE::ISelectionComponent> comp =
                CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
            _subscribe(comp.get(), *CORE::ISelectionComponent::SelectionMessageType);
            _subscribe(comp.get(), *CORE::ISelectionComponent::ClearSelectionMessageType);
        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void 
        RasterWindowGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            APP::IManager* manager = getGUIManager()->getInterface<APP::IManager>();
            CORE::IWorldMaintainer* maintainer =
                APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IManager>(manager);
            _unsubscribe(maintainer, *CORE::IWorld::WorldLoadedMessageType);
            const CORE::ComponentMap cmap = maintainer->getComponentMap();
            CORE::RefPtr<CORE::ISelectionComponent> comp =
                CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
            _unsubscribe(comp.get(), *CORE::ISelectionComponent::SelectionMessageType);
            _unsubscribe(comp.get(), *CORE::ISelectionComponent::ClearSelectionMessageType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void RasterWindowGUI::closeStereoWizard(Stereo::MainWindowGUI* stereoWiz)
    {
        if (stereoWiz)
        {
            delete stereoWiz;
            stereoWiz = NULL;
        }
        QObject* toolBoxObject = _findChild("toolBox");
        if (toolBoxObject != NULL)
        {
            QMetaObject::invokeMethod(toolBoxObject, "newRasterWindowButtonUncheck");

        }

    }

    void RasterWindowGUI::rasterWindow()
    {

        Stereo::MainWindowGUI* stereoWizard = new Stereo::MainWindowGUI(url,_enhancementType);
        _stereoWizard.push_back( stereoWizard);

        QObject::connect(stereoWizard, 
            SIGNAL(applicationClosed(Stereo::MainWindowGUI*)),
            this, 
            SLOT(closeStereoWizard(Stereo::MainWindowGUI*)), Qt::QueuedConnection);

        stereoWizard->showMaximized();

    } 

}


