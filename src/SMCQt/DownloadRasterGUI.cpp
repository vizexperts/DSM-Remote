/****************************************************************************
*
* File             : DownloadRasterGUI.h
* Description      : DownloadRasterGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/DownloadRasterGUI.h>

#include <QComboBox>
#include <QLineEdit>
#include <QTreeWidget>
#include <QDateTimeEdit>
#include <QValidator>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <Core/WorldMaintainer.h>

#include <VizQt/QtUtils.h>
#include <QFileDialog>

#include <Core/IObjectFactory.h>
#include <Core/IWorld.h>
#include <Core/CoreRegistry.h>

#include <Terrain/TerrainPlugin.h>
#include <Terrain/IWMSRasterLayer.h>

#include <osgEarthUtil/WMS>
#include <SMCQt/QMLTreeModel.h>
#include <curl/curl.h>

#include <Util/FileUtils.h>
#include <SMCQt/VizComboBoxElement.h>
#include <QJSONDocument>
#include <QJSONObject>
#include <Terrain/IModelObject.h>
#include <DB/ReaderWriter.h>
#include <DB/ReadFile.h>
#include <SMCUI/IAreaUIHandler.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <boost/filesystem.hpp>
#include <Util/CoordinateConversionUtils.h>
#include <boost/algorithm/string/replace.hpp>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, DownloadRasterGUI);
    DEFINE_IREFERENCED(DownloadRasterGUI, DeclarativeFileGUI);

    const std::string DownloadRasterGUI::GetTiffFromExtentsPopup= "getTiffFromExtent";
    const std::string DownloadRasterGUI::GeorbisServerlistDir = "/GeorbISServerList";
    bool DownloadRasterGUI::fileWritten = false;

    DownloadRasterGUI::DownloadRasterGUI()
        : _childItem(NULL)
        , _getTiffTreeModel(NULL)
        , _serverAddress("")
        , _layerName("")
        , _noFurtherUpdate(false)
    {
    }

    DownloadRasterGUI::~DownloadRasterGUI()
    {
    }

    void DownloadRasterGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader != NULL)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopUpLoader(QString)), Qt::UniqueConnection);
        }
    }

    void DownloadRasterGUI::connectPopUpLoader(QString type)
    {
        _readAllServers();

        //populate the server list
        int count = 0;
        QList<QObject*> comboBoxList;

        _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());

        for (int i = 0; i < _georbISServerList.size(); i++)
        {
            std::string serverName = _georbISServerList[i];
            comboBoxList.append(new VizComboBoxElement(serverName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("georbISServerList", QVariant::fromValue(comboBoxList));

        QObject* getTiffObject = _findChild(GetTiffFromExtentsPopup);
        if (getTiffObject != NULL)
        {

            QObject::connect(getTiffObject, SIGNAL(exportButtonClicked()), this, SLOT(_handleExportButtonClicked()), Qt::UniqueConnection);
            QObject::connect(getTiffObject, SIGNAL(connectButtonClicked()), this, SLOT(_handleConnectButtonClicked()), Qt::UniqueConnection);
            QObject::connect(getTiffObject, SIGNAL(browseButtonClicked()), this, SLOT(_handleBrowseButtonClicked()), Qt::UniqueConnection);
            QObject::connect(getTiffObject, SIGNAL(clearArea()), this, SLOT(_handleClearAreaButton()), Qt::UniqueConnection);
            QObject::connect(getTiffObject, SIGNAL(markPosition(bool)), this, SLOT(_handlePBAreaClicked(bool)), Qt::UniqueConnection);

        }

        QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(_updateLayerName()));
        _timer.start(10);

        _getTiffTreeModel = new WmsTreeModel();
        _childItem = new WmsTreeModelItem();

    }

    //internal function to read from georbISServers and populate the combobox list
    void DownloadRasterGUI::_readAllServers()
    {
        _georbISServerList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string georbISServerFolder = appdataPath + GeorbisServerlistDir;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _georbISServerList.push_back(fileNameWithoutExtension);
        }
    }

    void DownloadRasterGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch (const UTIL::Exception e)
            {
                e.LogException();
            }
        }
        // check whether the polygon has been updated
        else if (messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if (_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if (line.valid() && line)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first = line->getPoint(0)->getValue();
                    osg::Vec3d second = line->getPoint(2)->getValue();
                    QObject* getTiffObject = _findChild(GetTiffFromExtentsPopup);
                    if (getTiffObject)
                    {
                        getTiffObject->setProperty("bottomLeftLongitude", UTIL::ToString(first.x()).c_str());
                        getTiffObject->setProperty("bottomLeftLatitude", UTIL::ToString(first.y()).c_str());
                        getTiffObject->setProperty("topRightLongitude", UTIL::ToString(second.x()).c_str());
                        getTiffObject->setProperty("topRightLatitude", UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }

        DeclarativeFileGUI::update(messageType, message);
    }

    void DownloadRasterGUI::_updateLayerName()
    {
        if (!_noFurtherUpdate)
        {
            std::string layerNames;
            _layerName = "";

            _getLayerNames(layerNames, _childItem);

            QObject* getTiffObject = _findChild(GetTiffFromExtentsPopup);
            if (getTiffObject)
            {
                getTiffObject->setProperty("layerName", QString::fromStdString(_layerName));
            }
        }
    }

    std::string DownloadRasterGUI::_getWMSPath(std::string serverName)
    {

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string databaseFolder = appdataPath + GeorbisServerlistDir;

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting", "ServerList folder not found", true);
            //making database to be invalid that is handled in parent function
           
            return "";
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);
        std::string serverExtension;
        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == serverName)
            {
                found = true;
                serverExtension = osgDB::getLowerCaseFileExtension(currFileName);
            }
        }

        if (!found)
        {
            return "";
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + GeorbisServerlistDir +"//"+ serverName + "." + serverExtension));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                return "";
            }

            QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
            if (jdoc.isNull())
            {
               
                return "";
            }

            QJsonObject obj = jdoc.object();
            return obj["Host"].toString().toStdString();
        }
    }

    void DownloadRasterGUI::_handlePBAreaClicked(bool pressed)
    {
        if (!_areaHandler.valid())
        {
            return;
        }

        if (pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

            QObject* queryBuilderObject = _findChild("getTiffFromExtent");
            if (queryBuilderObject)
            {
                queryBuilderObject->setProperty("isMarkSelected", false);
            }
        }
    }


    void DownloadRasterGUI::_handleClearAreaButton()
    {
        QObject* getTiffObject = _findChild(GetTiffFromExtentsPopup);
        if (getTiffObject)
        {
            getTiffObject->setProperty("isMarkSelected", false);
            _handlePBAreaClicked(false);
            getTiffObject->setProperty("bottomLeftLongitude", "");
            getTiffObject->setProperty("bottomLeftLatitude", "");
            getTiffObject->setProperty("topRightLongitude", "");
            getTiffObject->setProperty("topRightLatitude", "");
        }
    }


    void DownloadRasterGUI::_handleExportButtonClicked(){
        // code to export the Extent and download raster
        double topRLat, topRLong, bottomLLat, bottomLLong;

        std::string response;

        QObject* getTiffObject = _findChild(GetTiffFromExtentsPopup);
        if (getTiffObject)
        {
            bottomLLong = getTiffObject->property("bottomLeftLongitude").toDouble();
            bottomLLat = getTiffObject->property("bottomLeftLatitude").toDouble();
            topRLong = getTiffObject->property("topRightLongitude").toDouble();
            topRLat = getTiffObject->property("topRightLatitude").toDouble();
            response = getTiffObject->property("outputDirectory").toString().toStdString();
        }

        // get the extents of the layer

        //create the capability url
        std::string capabilities = _serverAddress + "?service=wms&request=GetCapabilities";

        //query the capabilty url
        osg::ref_ptr<osgEarth::Util::WMSCapabilities> wmsCapabilities = osgEarth::Util::WMSCapabilitiesReader::read(capabilities, NULL);
        osg::ref_ptr<osgEarth::Util::WMSLayer> wmsLayer;
        if (wmsCapabilities.valid()){
             wmsLayer= wmsCapabilities->getLayerByName(_layerName);
        }
        else{
            QMetaObject::invokeMethod(getTiffObject, "closePopup");
            showError("Server Reachability", "Server not Reachable", true);
            return;
        }

        double minX, minY, maxX, maxY;
        wmsLayer->getLatLonExtents(minX, minY, maxX, maxY);

        std::string callString;
        // check if bound are out of extents
        if (bottomLLat < minY || bottomLLong < minX || topRLat > maxY || topRLong > maxX){

            callString = _serverAddress + "?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Ftiff&TRANSPARENT=false&LAYERS=" + _layerName + "&WIDTH=512&HEIGHT=512&CRS=EPSG:4326&STYLES=&BBOX=" + std::to_string(minY) + "%2C" + std::to_string(minX) + "%2C" + std::to_string(maxY) + "%2C" + std::to_string(maxX);
        }
        else{
            callString = _serverAddress + "?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Ftiff&TRANSPARENT=false&LAYERS=" + _layerName + "&WIDTH=512&HEIGHT=512&CRS=EPSG:4326&STYLES=&BBOX=" + std::to_string(bottomLLat) + "%2C" + std::to_string(bottomLLong) + "%2C" + std::to_string(topRLat) + "%2C" + std::to_string(topRLong);
        }

        // creating curl request

        CURL *curl; CURLcode res;
        curl_global_init(CURL_GLOBAL_ALL);
        curl = curl_easy_init();

        boost::replace_all(_layerName, " ", "");
        boost::replace_all(_layerName, ":", "_");
        boost::replace_all(_layerName, ",", "_");

        response = response +"/"+_layerName + ".tiff";

        //std::ofstream file(response.c_str());
        FILE* fp;

        if (curl) {
            fp = fopen(response.c_str(), "wb");
            curl_easy_setopt(curl, CURLOPT_URL, callString.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_File);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
            res = curl_easy_perform(curl);
            curl_easy_cleanup(curl);
            if (res == CURLE_OK) {
                //done
                QMetaObject::invokeMethod(getTiffObject, "closePopup");
                if (fileWritten){
                    showError("Download Raster", "Success : Raster downloaded for given extents ", true);
                }
                else{
                    showError("Download Raster", "Failed to save file", true);
                }
            }
            else{
                showError("Download Raster", "Failed to download file," + res, true);
            }
        }
        curl_global_cleanup();
    }

    size_t DownloadRasterGUI::write_to_File(void *ptr, size_t size, size_t nmemb, FILE *stream) {
        size_t written;
        if (stream){
            written = fwrite(ptr, size, nmemb, stream);
            fileWritten = true;
        }
        else{
            fileWritten = false;
        }
        return written;
    }

    void DownloadRasterGUI::_handleBrowseButtonClicked(){
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        static std::string directory = "c:/";

        QString dirLocation = QFileDialog::getExistingDirectory(parent, tr("Open Directory"),
            directory.c_str(), QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);

        QObject* getTiffObject = _findChild("getTiffFromExtent");

        if (getTiffObject){
            getTiffObject->setProperty("outputDirectory", QVariant::fromValue(dirLocation));
        }
    }

    void DownloadRasterGUI::_handleConnectButtonClicked()
    {
        QObject* getTiffObject = _findChild(GetTiffFromExtentsPopup);
        if (getTiffObject)
        {
            std::string serverAddress;

            std::string serverName = getTiffObject->property("value").toString().toStdString();
            serverAddress = _getWMSPath(serverName);

            std::string server = serverAddress;

            //create the capability url
            std::string capabilities = server + "?service=wms&request=GetCapabilities";

            //query the capabilty url
            osg::ref_ptr<osgEarth::Util::WMSCapabilities> wmsCapabilities = osgEarth::Util::WMSCapabilitiesReader::read(capabilities, NULL);

            if (!wmsCapabilities.valid())
            {
                showError("Error", "Error in connecting to the Server. \nPlease check if the server address is correct and server is online.");
                return;
            }

            const osgEarth::Util::WMSLayer::LayerList& layerList = wmsCapabilities->getLayers();
            _childItem->setName("Layers");
            _childItem->setCheckable(false);
            _populate(layerList, _childItem);
            _getTiffTreeModel->addItem(_childItem);

            _setContextProperty("getTiffTreeModel", QVariant::fromValue(_getTiffTreeModel));
            _serverAddress = server;
        }
    }

    void DownloadRasterGUI::_removeChildItem(WmsTreeModelItem* item)
    {
        if(item)
        {
            foreach(QMLTreeModelItem* childItem, item->getChildren())
            {
                WmsTreeModelItem* child = dynamic_cast<WmsTreeModelItem*>(childItem);
                if(child)
                {
                    _getTiffTreeModel->removeItem(child);
                }
            }
        }
        _setContextProperty("getTiffTreeModel", NULL);
    }


    void DownloadRasterGUI::_getLayerNames(std::string& layers, WmsTreeModelItem* item)
    {
        if(item->getCheckState() == Qt::Checked)
        {
            QString name = item->getName();
            if(!name.isEmpty())
            {
                if(layers.empty())
                {
                    if (name.toStdString() != "Layers")
                    {
                        layers = name.toStdString();
                        _layerName = layers;
                    }
                }
                else
                {
                    layers += "," + name.toStdString();
                }
            }
        }
        foreach(QMLTreeModelItem* childItem, item->getChildren())
        {
            WmsTreeModelItem* child = dynamic_cast<WmsTreeModelItem*>(childItem);
            _getLayerNames(layers,child);
        }

    }

    void DownloadRasterGUI::_populate(const osgEarth::Util::WMSLayer::LayerList& layerList, WmsTreeModelItem* _childItem)
    {

        osgEarth::Util::WMSLayer::LayerList::const_iterator iter = layerList.begin();

        while(iter != layerList.end())
        {
            osg::ref_ptr<osgEarth::Util::WMSLayer> wmsLayer = (*iter).get();
            WmsTreeModelItem* item = new WmsTreeModelItem();
            item->setName(wmsLayer->getName().c_str());
            item->setTitle( wmsLayer->getTitle().c_str());
            item->setAbstraction( wmsLayer->getAbstract().c_str());
            item->setCheckState(Qt::Unchecked);

            _childItem->addItem(item);

            _populate(wmsLayer->getLayers(),item);
            iter++;
        }
    }

    void DownloadRasterGUI::reset()
    {
        QObject* getTiffObject = _findChild(GetTiffFromExtentsPopup);
        if(getTiffObject)
        {
            getTiffObject->setProperty("browseButtonEnabled",QVariant::fromValue(true));
            getTiffObject->setProperty("connectButtonEnabled", QVariant::fromValue(true));
            getTiffObject->setProperty("addButtonEnabled",QVariant::fromValue(true));
            getTiffObject->setProperty("serverAddress","");
            getTiffObject->setProperty("layerName","");
            getTiffObject->setProperty("isMarkSelected", false);
            getTiffObject->setProperty("bottomLeftLongitude", "longitude");
            getTiffObject->setProperty("bottomLeftLatitude", "latitude");
            getTiffObject->setProperty("topRightLongitude", "longitude");
            getTiffObject->setProperty("topRightLatitude", "latitude");
            _handlePBAreaClicked(false);
            _removeChildItem(_childItem);
            _timer.stop();
            _layerName = "";
            _noFurtherUpdate = false;
        }
    }

} // namespace SMCQt

