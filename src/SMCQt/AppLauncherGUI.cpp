#ifdef USE_QT4

/****************************************************************************
 *
 * File             : LoginGUI.cpp
 * Description      : LoginGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCQt/AppLauncherGUI.h>

#include <App/IApplication.h>

#include <Core/GroupAttribute.h>
#include <Core/NamedAttribute.h>

#include <vector>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, AppLauncherGUI);
    DEFINE_IREFERENCED(AppLauncherGUI, DeclarativeFileGUI);


    AppLauncherGUI::AppLauncherGUI()
        :_stereoWizard(NULL)
    {

    }

    AppLauncherGUI::~AppLauncherGUI()
    {}

    void AppLauncherGUI::initializeAttributes()
    {
        CORE::Base::initializeAttributes();

        //std::string groupName = "LoginGUI Attributes";
  //      _addAttribute(new CORE::GroupAttribute("Credential", "Credential",
  //                      CORE::GroupAttribute::SetFuncType(this, &LoginGUI::addCredential),
        //                CORE::GroupAttribute::GetFuncType(this, &LoginGUI::getCredential),
        //                std::string("Credential"), groupName));
    }



    void AppLauncherGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* gui = _findChild("applauncher");
        if(gui != NULL)
        {
            QObject::connect(gui, SIGNAL(launchTerrainWizard()),this, SLOT(launchTerrainWizard()), Qt::UniqueConnection);
            QObject::connect(gui, SIGNAL(launchStereoWizard()),this, SLOT(launchStereoWizard()), Qt::UniqueConnection);
            QObject::connect(gui, SIGNAL(launchStreamingWizard()),this, SLOT(launchStreamingWizard()), Qt::UniqueConnection);
            
        }
    }

    void AppLauncherGUI::launchTerrainWizard()
    {

    }

    void AppLauncherGUI::launchStereoWizard()
    {
        if(!_stereoWizard)
        {
            _stereoWizard = new Stereo::MainWindowGUI();
            QObject::connect(_stereoWizard, SIGNAL(applicationClosed()),this, SLOT(closeStereoWizard()), Qt::QueuedConnection);
            _stereoWizard->showMaximized();
        }
        else
        {
            if(!_stereoWizard->isActiveWindow())
            {
                _stereoWizard->activateWindow();                
            }
            _stereoWizard->showMaximized();
            
        }
        
        //_stereoWizard->deleteLater();

    }

    void AppLauncherGUI::launchStreamingWizard()
    {

    }

    void AppLauncherGUI::closeStereoWizard()
    {
        delete _stereoWizard;
        _stereoWizard = NULL;
    }

    void
    AppLauncherGUI::onAddedToGUIManager()
    {   
        _loadAndSubscribeSlots();

        // Subscribe for application loaded message
        try
        {
            CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
            _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        }
        catch(...)
        {
        }
    }

    void 
    AppLauncherGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

}   // namespace SMCQt

#endif