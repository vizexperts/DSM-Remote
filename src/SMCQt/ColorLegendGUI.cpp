
#include <SMCQt/ColorLegendGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Util/StringUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorldMaintainer.h>

#include <Elements/IColorMapComponent.h>
#include <Elements/IColorMap.h>
#include <Elements/IColorMapRaster.h>
#include<Core/IObject.h>
#include <VizUI/ISelectionUIHandler.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ColorLegendGUI);
    DEFINE_IREFERENCED(ColorLegendGUI, DeclarativeFileGUI);
    const std::string ColorLegendGUI::ColorLegend = "ColorLegend";

    ColorLegendObject::ColorLegendObject(QObject* parent) : QObject(parent)
    {
    }

    ColorLegendObject::ColorLegendObject(double minVal, double maxVal, QColor cellColor, QObject *parent)
        : QObject(parent), min_Value(minVal), max_Value(maxVal), cell_Color(cellColor)
    {

    }

    //These functions may be needed in the future for editing the color legend value
    void ColorLegendObject::setMinValue(double minVal)
    {
        if (minVal != min_Value)
        {
            min_Value = minVal;
            emit minValueChanged();
        }
    }

    double ColorLegendObject::minVal() const
    {
        return min_Value;
    }

    void ColorLegendObject::setMaxValue(double maxVal)
    {
        if (maxVal != max_Value)
        {
            max_Value = maxVal;
            emit maxValueChanged();
        }
    }

    double ColorLegendObject::maxVal() const
    {
        return max_Value;
    }

    void ColorLegendObject::setcellColor(QColor cellColor)
    {
        if (cellColor != cell_Color)
        {
            cell_Color = cellColor;
            emit cellColorChanged();
        }
    }

    QColor ColorLegendObject::cellColor() const
    {
        return cell_Color;
    }
    ColorLegendGUI::ColorLegendGUI(){}
    ColorLegendGUI::~ColorLegendGUI()
    {}
    void ColorLegendGUI::fillLegendColorTable(ELEMENTS::IColorMap* colorMap)
    {
        if (!colorMap)
            return;

        QObject* colorLegend = _findChild("colorLegend");
        if (colorLegend)
        {
            osg::Vec4 defaultColor = colorMap->getDefaultColor();
            int cx = (int)(255 * defaultColor.x());
            int cy = (int)(255 * defaultColor.y());
            int cz = (int)(255 * defaultColor.z());
            int cw = (int)(255 * defaultColor.w());
            QColor color(cx, cy, cz, cw);
            const ELEMENTS::IColorMap::RangeColorMap& map = colorMap->getMap();
            ELEMENTS::IColorMap::RangeColorMap::const_iterator iter = map.begin();

            //qDeleteAll(_colorList.begin(), _colorList.end()); //No need to call qDeleteAll will clear in clear funtion
            _colorList.clear();

            for (; iter != map.end(); ++iter)
            {
                osg::Vec2 range = iter->first;
                osg::Vec4 color = iter->second;
                std::string strMin = UTIL::ToString(range.x());
                std::string strMax = UTIL::ToString(range.y());
                double min = UTIL::ToDouble(strMin);
                double max = UTIL::ToDouble(strMax);
                int ccx = (int)(255 * color.x());
                int ccy = (int)(255 * color.y());
                int ccz = (int)(255 * color.z());
                int ccw = (int)(255 * color.w());
                QColor qcolor(ccx, ccy, ccz, ccw);
                _colorList.append(new ColorLegendObject(min, max, qcolor));
            }

            _setContextProperty("colorLegendListModel", QVariant::fromValue(_colorList));
        }
    }
    
    void ColorLegendGUI::handleSelectColorMap()
    {
        // Selection UIHandlersetProperty
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (!selectionUIHandler.valid())
        {
            return;
        }

        // Selection Map
        const CORE::ISelectionComponent::SelectionMap& map =
            selectionUIHandler->getCurrentSelection();

        CORE::ISelectionComponent::SelectionMap::const_iterator iter =
            map.begin();

        //XXX - using the first element in the selection
        if (iter == map.end())
        {
            return;
        }

        //! Get the selected object
        CORE::RefPtr<CORE::IObject> selectedObject = iter->second->getInterface<CORE::IObject>();
        if (!selectedObject.valid())
            return;

        CORE::RefPtr<ELEMENTS::IColorMapRaster> colorMapRaster = selectedObject->getInterface<ELEMENTS::IColorMapRaster>();
        if (colorMapRaster)
        {
            CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
            if (wmain)
            {
                CORE::IComponent* comp = wmain->getComponentByName("ColorMapComponent");
                if (comp)
                {
                    _colorMapComponent = comp->getInterface<ELEMENTS::IColorMapComponent>();
                }
            }
            // get ui handler
            _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IColorMapUIHandler>(getGUIManager());
            std::string colorMapName = colorMapRaster->getColorMapName();
            _colorMap = _uiHandler->getColorMap(colorMapRaster->getColorMapName());
            if (_colorMap)
            {
                fillLegendColorTable(_colorMap.get());
            }
        }
    }

}