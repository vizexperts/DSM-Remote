/****************************************************************************
*
* File             : LineLayerPropertiesGUI.h
* Description      : LineLayerPropertiesGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/LineLayerPropertiesGUI.h>
#include <Core/IProperty.h>
#include <Core/IWorldMaintainer.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <VizUI/ISelectionUIHandler.h>
#include <SMCUI/IAddVectorUIHandler.h> 

#include <GIS/GISPlugin.h>
#include <GIS/ILineStyle.h>
#include <GIS/IDrawPropertyHolder.h>

#include <GIS/IIBStyle.h>
#include <Core/CoreRegistry.h>
#include <Core/IPropertyFactory.h>

#include <GIS/IOSGOperationQueueHolder.h>
#include <Elements/ElementsUtils.h>
#include <Elements/ElementsPlugin.h>

#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/ReadFile>

#include <DB/ReadFile.h>

#include <GIS/IFeatureLayer.h> 

#include <View/IViewBasedVisibility.h>
#include <View/ViewPlugin.h>

#include <Terrain/IModelObject.h>

#include <QFileDialog>
#include <Util/FileUtils.h>
#include <QJSONDocument>
#include <QJSONObject>
#include <SMCQt/VizComboBoxElement.h>
#include <SMCQt/VizProfileObject.h> 
#include <QtCore/QJsonArray>
#include <VizUI/IDeletionUIHandler.h>
#include <Core/ITaggable.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/ProfileObject.h>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, LineLayerPropertiesGUI);
    DEFINE_IREFERENCED(LineLayerPropertiesGUI, DeclarativeFileGUI);

    const std::string LineLayerPropertiesGUI::profileTemplate = "/ProfileTemplate/"; 
    QList<QString> colorList = { "red", "cyan", "yellow", "orange", "green", "blue", "pink", "voilet", "darkRed", "grey" };

    LineLayerPropertiesGUI::LineLayerPropertiesGUI() 
        :_connectedFeatureTemplateStoragePath("/ConnectedFeatureTemplate/")
    {

    }

    LineLayerPropertiesGUI::~LineLayerPropertiesGUI()
    {

    }

    void LineLayerPropertiesGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopup(QString)), Qt::UniqueConnection);
        }

        qmlRegisterType<ProfileObject>("ProfileObject", 1, 0, "ProfileObject");
    }

    void LineLayerPropertiesGUI::connectPopup(QString type)
    {
        if(type == "lineLayerStylePopup")
        {
            QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
            
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& slMap = selectionUIHandler->getCurrentSelection();
            
            if(slMap.empty())
                return;

            _currentSelection = slMap.begin()->second;


            //populate the list
            //set Type Fence
            lineLayerStylePopup->setProperty("type", "Fence");
            _readAllTemplate();

            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select 3D style template", "0"));
            comboBoxList.append(new VizComboBoxElement("Create new 3D style template", "1"));

            for (int i = 0; i < _templateList.size(); i++)
            {
                std::string templateName = _templateList[i];
                comboBoxList.append(new VizComboBoxElement(templateName.c_str(), UTIL::ToString(count).c_str()));
                count++;
            }

            _setContextProperty("connected3DFeatureList", QVariant::fromValue(comboBoxList));
            QString defaultName = "Select 3D style template";
            if (_templateList.size()>0)
            {
                defaultName = _templateList[_templateList.size() - 1].c_str();
            }
            lineLayerStylePopup->setProperty("selectedTemplateName", defaultName);
            QString templateName = lineLayerStylePopup->property("selectedTemplateName").toString();
            _editExistingConnectedFeature(defaultName);


            QObject::connect(lineLayerStylePopup, SIGNAL(okButtonPressed()), this, SLOT(_handlePopupOkButtonPressed()), Qt::UniqueConnection);
            QObject::connect(lineLayerStylePopup, SIGNAL(closed()), this, SLOT(popupClosed()), Qt::UniqueConnection);
            QObject::connect(lineLayerStylePopup, SIGNAL(browseButtonClicked(QString,bool)), this, SLOT(browseButtonClicked(QString,bool)), Qt::UniqueConnection);
            //QObject::connect(lineLayerStylePopup, SIGNAL(browseUnfencedButtonClicked()), this, SLOT(browseUnfencedButtonClicked()), Qt::UniqueConnection);
            QObject::connect(this, SIGNAL(sendFenceFolderCreated(bool)), this, SLOT(recFenceFolderCreated(bool)), Qt::UniqueConnection);
            
            QObject::connect(lineLayerStylePopup, SIGNAL(editConnectedFeature(QString)), this, SLOT(_editExistingConnectedFeature(QString)), Qt::UniqueConnection);
            QObject::connect(lineLayerStylePopup, SIGNAL(deleteSelectedTemplate(QString)), this, SLOT(_deleteConnectedFeatureTemplate(QString)), Qt::UniqueConnection);
            QObject::connect(lineLayerStylePopup, SIGNAL(saveTemplate()), this, SLOT(_handleSaveButtonClicked()), Qt::UniqueConnection);
            QObject::connect(lineLayerStylePopup, SIGNAL(applyButtonClick()), this, SLOT(changeDrawTechniqueType()), Qt::UniqueConnection);
        }

        else if (type == "ditchProfile")
        {

            QObject* ditchProfilePopup = _findChild("ditchProfile");

            if (ditchProfilePopup) {

                _readAllDitchProfiles();

                int count = 2;
                QList<QObject*> comboBoxList;

                comboBoxList.append(new VizComboBoxElement("Select profile", "0"));
                comboBoxList.append(new VizComboBoxElement("Create new profile", "1"));

                for (int i = 0; i < _profileList.size(); i++)
                {
                    std::string profileName = _profileList[i];
                    comboBoxList.append(new VizComboBoxElement(profileName.c_str(), UTIL::ToString(count).c_str()));
                    count++;
                }

                _setContextProperty("profileList", QVariant::fromValue(comboBoxList));

                QObject::connect(ditchProfilePopup, SIGNAL(browseButtonClicked()), this, SLOT(_browseButtonClicked()), Qt::UniqueConnection);
                QObject::connect(ditchProfilePopup, SIGNAL(saveProfile(QVariant)), this, SLOT(_saveNewProfile(QVariant)), Qt::UniqueConnection);
                QObject::connect(ditchProfilePopup, SIGNAL(showGUIMessage(QString)), this, SLOT(_guiMessageSlot(QString)), Qt::UniqueConnection);
                //QObject::connect(ditchProfilePopup, SIGNAL(editProfile(QString)), this, SLOT(_editProfile(QString)), Qt::UniqueConnection);
         
            }

        }

        else if (type == "ditch")
        {
            QObject* ditchPopup = _findChild("ditch");
            if (ditchPopup) {

                _readAllDitchProfiles();

                int count = 1;

                QList<QObject*> comboBoxList;
                comboBoxList.append(new VizComboBoxElement("Select profile", "0"));

                for (int i = 0; i < _profileList.size(); i++)
                {
                    std::string profileName = _profileList[i];
                    comboBoxList.append(new VizComboBoxElement(profileName.c_str(), UTIL::ToString(count).c_str()));
                    count++;
                }

                _setContextProperty("profileList", QVariant::fromValue(comboBoxList));

                _profileTemplateList.clear(); 
                _setContextProperty("profileTemplateList", QVariant::fromValue(_profileTemplateList));

                QObject::connect(ditchPopup, SIGNAL(browseButtonClicked()), this, SLOT(_browseButtonClicked()), Qt::UniqueConnection);
                QObject::connect(ditchPopup, SIGNAL(applyProfileToTerrain(QVariant)), this, SLOT(_applySelectedProfileToTerrain(QVariant)), Qt::UniqueConnection);
                QObject::connect(ditchPopup, SIGNAL(loadTemplate(QString, bool)), this, SLOT(_loadTemplatesOfSelectedProfile(QString, bool)), Qt::UniqueConnection);
                QObject::connect(ditchPopup, SIGNAL(saveTemplate(QString , QVariant )), this, SLOT(_saveTemplate(QString , QVariant )), Qt::UniqueConnection);
                QObject::connect(ditchPopup, SIGNAL(addNewTemplate(QString)), this, SLOT(_addNewTemplate(QString)), Qt::UniqueConnection);
                QObject::connect(ditchPopup, SIGNAL(deleteTemplate(QString)), this, SLOT(_deleteTemplate(QString)), Qt::UniqueConnection);
                QObject::connect(ditchPopup, SIGNAL(deleteProfile(QString)), this, SLOT(_deleteProfile(QString)), Qt::UniqueConnection);
            }
        }
    }

    void LineLayerPropertiesGUI::_deleteTemplate(QString templateName)
    {
        QObject* ditchPopup = _findChild("ditch");
        if (ditchPopup)
        {
            std::string  profileName = ditchPopup->property("profileName").toString().toStdString();
            if (templateName.contains("Default"))
            {
                emit showError("Profile Template", "Default Template cannot be deleted");
                return;
            }

            std::string profileFileName = profileName + ".profile";

            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();
            std::string profileTemplateFolder = appdataPath + profileTemplate;

            QFile profileFile(QString::fromStdString(profileTemplateFolder + profileFileName));

            if (!profileFile.open(QIODevice::ReadWrite))
            {
                emit showError("Save setting", "Could not open georbIS Server file ");
                return;
            }

            QByteArray settingData = profileFile.readAll();
            QString setting(settingData);

            QJsonDocument jdoc = QJsonDocument::fromJson(setting.toUtf8());

            QJsonObject jsonObject = jdoc.object();

            QJsonObject::Iterator itr = jsonObject.find(templateName);

            if (itr != jsonObject.end())
            {
                jsonObject.erase(itr); 
            }


            for (QList<QObject*>::iterator itr = _profileTemplateList.begin(); itr != _profileTemplateList.end(); itr++)
            {

                VizProfileObject* ptr = dynamic_cast<VizProfileObject*>(*itr);
                    if (ptr->name() == templateName)
                    {
                        _profileTemplateList.erase(itr);
                        break; 
                    }
            }

            _setContextProperty("profileTemplateList", QVariant::fromValue(_profileTemplateList));


            QJsonDocument settingDoc(jsonObject);

            profileFile.seek(0);
            profileFile.write(settingDoc.toJson());
            profileFile.resize(profileFile.pos());
            profileFile.close();



        }
    }

    void LineLayerPropertiesGUI::_addNewTemplate(QString templateName)
    {
        QObject* ditchPopup = _findChild("ditch");
        if (ditchPopup)
        {
            std::string  profileName = ditchPopup->property("profileName").toString().toStdString();

            std::string profileFileName = profileName + ".profile";

            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();
            std::string profileTemplateFolder = appdataPath + profileTemplate;

            QFile profileFile(QString::fromStdString(profileTemplateFolder + profileFileName));

            if (!profileFile.open(QIODevice::ReadWrite))
            {
                emit showError("Save setting", "Could not open georbIS Server file for save");
                return;
            }

            QByteArray settingData = profileFile.readAll();
            QString setting(settingData);

            QJsonDocument jdoc = QJsonDocument::fromJson(setting.toUtf8());

            QJsonObject jsonObject = jdoc.object();

            std::string templateNameString = templateName.toStdString();

            QJsonArray pointsArray = jsonObject[templateNameString.c_str()].toArray();

            QStringList stringList = jsonObject.keys();

            int count = jsonObject.size(); 
            std::string newTemplateString = std::to_string(--count); 

            while (1)
            {
                int itr; 
                for (itr = 0; itr < stringList.size(); itr++)
                {
                    if (stringList[itr].contains(newTemplateString.c_str()))
                    {
                        count++; 
                        break; 
                    }
                }
                if (itr == stringList.size())
                {
                    break; 
                }else 
                    newTemplateString = std::to_string(count);
                
            }

            std::string templateFinalString = profileName + "_" + newTemplateString;

             jsonObject[templateFinalString.c_str()] = pointsArray;

             _profileTemplateList.push_back(new VizProfileObject(QString(templateFinalString.c_str()), colorList[count%colorList.size()], true));
             _setContextProperty("profileTemplateList", QVariant::fromValue(_profileTemplateList));

            QJsonDocument settingDoc(jsonObject);

            profileFile.seek(0);
            profileFile.write(settingDoc.toJson());
            profileFile.resize(profileFile.pos());
            profileFile.close();

        }
    }

    void LineLayerPropertiesGUI::_saveTemplate(QString templateName, QVariant pointList)
    {
        QObject* ditchPopup = _findChild("ditch");
        if (ditchPopup)
        {
            std::string  profileName = ditchPopup->property("profileName").toString().toStdString();

            std::string profileFileName = profileName + ".profile";

            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();
            std::string profileTemplateFolder = appdataPath + profileTemplate;

            QFile profileFile(QString::fromStdString(profileTemplateFolder + profileFileName));

            if (!profileFile.open(QIODevice::ReadWrite))
            {
                emit showError("Save setting", "Could not open georbIS Server file for save");
                return;
            }

            QByteArray settingData = profileFile.readAll();
            QString setting(settingData);

            QJsonDocument jdoc = QJsonDocument::fromJson(setting.toUtf8());

            QJsonObject jsonObject = jdoc.object();

            std::string templateNameString = templateName.toStdString();

            QJsonArray pointsData;
            _pointsList = pointList.toList();
            for (int i = 0; i < _pointsList.size(); i++)
            {
                QJsonObject pointData;
                pointData["x"] = _pointsList[i].toPointF().x();
                pointData["y"] = _pointsList[i].toPointF().y();

                pointsData.push_back(pointData);

            }

            jsonObject[templateNameString.c_str()] = pointsData;

            QJsonDocument settingDoc(jsonObject);

            profileFile.seek(0);
            profileFile.write(settingDoc.toJson());
            profileFile.resize(profileFile.pos());
            profileFile.close(); 
        }
    }
    void LineLayerPropertiesGUI::_loadTemplatesOfSelectedProfile(QString teplateName, bool refreshTemplateList)
    {
        QObject* ditchPopup = _findChild("ditch");
        if (ditchPopup)
        {
            std::string  profileName = ditchPopup->property("profileName").toString().toStdString();
            std::string profileFileName = profileName + ".profile";

            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();
            std::string profileTemplateFolder = appdataPath + profileTemplate;

            QFile profileFile(QString::fromStdString(profileTemplateFolder + profileFileName));

            if (!profileFile.open(QIODevice::ReadOnly))
            {
                emit showError("Save setting", "Could not open georbIS Server file");
                return;
            }

            QByteArray settingData = profileFile.readAll();
            QString setting(settingData);

            QJsonDocument jdoc = QJsonDocument::fromJson(setting.toUtf8());


            QJsonObject jsonObject = jdoc.object();

            if (refreshTemplateList)
            {
                _profileTemplateList.clear(); 
                QStringList stringList = jsonObject.keys();

                stringList.removeOne(QString("Texture"));
                for (int j = 0; j < stringList.size(); j++)
                {
                    _profileTemplateList.push_back(new VizProfileObject(stringList[j], colorList[j%colorList.size()], true));
                }
                _setContextProperty("profileTemplateList", QVariant::fromValue(_profileTemplateList));

                QString textureFile = jsonObject["Texture"].toString();
                ditchPopup->setProperty("topTextureFile", textureFile);
            }

            std::string templateNameString = teplateName.toStdString(); 

            QJsonArray pointsData = jsonObject[templateNameString.c_str()].toArray();

            //now populate the corresponding profileObject 
            QList<QVariant> pointVector;

            foreach(const QJsonValue& value, pointsData)
            {
                QJsonObject valueObject = value.toObject();
                pointVector.push_back(QPointF(valueObject["x"].toDouble(), valueObject["y"].toDouble()));
            }

            ditchPopup->setProperty("points", pointVector);
            ditchPopup->setProperty("templateName", QString::fromStdString(templateNameString));

        }

    }

    void LineLayerPropertiesGUI::_guiMessageSlot(QString message)
    {
        emit showError("Drawing", message);
    }

    void LineLayerPropertiesGUI::_readAllDitchProfiles()
    {
        _profileList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string templateFolder = appdataPath + profileTemplate;


        std::string path = osgDB::convertFileNameToNativeStyle(templateFolder);
        if (!osgDB::fileExists(path))
        {
            LOG_ERROR("Profile template folder not present");
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            if (osgDB::getFileExtension(currFileName) == "profile")
            {
                std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
                _profileList.push_back(fileNameWithoutExtension);
            }
        }
    }

    void LineLayerPropertiesGUI::_deleteProfile(QString value)
    {

        if (value.toStdString() == "Select profile" || value.toStdString() == "Create new profile")
        {
            emit  showError("Not a valid input", "Cannot delete this profile");
            return;
        }
        emit question("Delete profile ", "Do you really want to delete?", false,
            "_okSlotForDeletingProfile()", "_cancelSlotForDeleting()");

    }

    void LineLayerPropertiesGUI::_okSlotForDeletingProfile()
    {
        //QObject* ditchProfilePopup = _findChild("ditchProfile");
        QObject* ditchPopup = _findChild("ditch");

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string profileFolder = appdataPath + profileTemplate;

        std::string path = osgDB::convertFileNameToNativeStyle(profileFolder);
        if (!osgDB::fileExists(path))
        {
            LOG_ERROR("Profile template folder not present");
            return;
        }

        if (ditchPopup)
        {
            QString lpName = ditchPopup->property("profileName").toString();
            std::string extension = ".profile";

            if (lpName.isEmpty())
            {
                return;
            }

            std::string filename = lpName.toStdString() + extension;
            std::string filePath = profileFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, profileFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
                LOG_INFO("Profile successfully deleted");
            }
            else
            {
                return; 
            }

            _readAllDitchProfiles();

            //populate the list again
            int count = 1;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select profile", "0"));

            for (int i = 0; i < _profileList.size(); i++)
            {
                std::string profileName = _profileList[i];
                comboBoxList.append(new VizComboBoxElement(profileName.c_str(), UTIL::ToString(count).c_str()));
                count++;
            }

            _setContextProperty("profileList", QVariant::fromValue(comboBoxList));

            std::string defaultName = "Select profile";
             ditchPopup->setProperty("profileName", QString::fromStdString(defaultName));

             _profileTemplateList.clear();
             _setContextProperty("profileTemplateList", QVariant::fromValue(_profileTemplateList));

             QMetaObject::invokeMethod(ditchPopup, "clearGraphs"); 
        }

    }

    void LineLayerPropertiesGUI::_browseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption = "Browse for texture file";
       ;
        QString filters = "texture file (*.jpg *.jpeg *.png)";

        if (_lastPath.empty())
            _lastPath = directory.toStdString();
        QString fileName = QFileDialog::getOpenFileName(parent, caption,
            _lastPath.c_str(), filters);
        
        _lastPath = osgDB::getFilePath(fileName.toStdString());

        if (fileName.isEmpty())
            return;

        QObject*  ditchProfile = _findChild("ditchProfile");
        if (ditchProfile)
        {
            ditchProfile->setProperty("sideTextureFilePath", QVariant::fromValue(fileName));
        }

        QObject*  ditch = _findChild("ditch");
        if (ditch)
        {
            ditch->setProperty("topTextureFile", QVariant::fromValue(fileName));
        }
    }

    void LineLayerPropertiesGUI::_applySelectedProfileToTerrain(QVariant pointProfileMap)
    {
        QObject* ditchPopup = _findChild("ditch");
        if (ditchPopup)
        {
            std::string  profileName = ditchPopup->property("profileName").toString().toStdString();

            std::string profileFileName = profileName + ".profile";

            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();
            std::string profileTemplateFolder = appdataPath + profileTemplate;

            QFile profileFile(QString::fromStdString(profileTemplateFolder + profileFileName));

            if (!profileFile.open(QIODevice::ReadOnly))
            {
                emit showError("Apply Profile", "Could not open profile ");
                return;
            }


            QByteArray settingData = profileFile.readAll();
            QString setting(settingData);

            QJsonDocument jdoc = QJsonDocument::fromJson(setting.toUtf8());


            QJsonObject jsonObject = jdoc.object();

            QJsonArray pointsData = jsonObject["PointsData"].toArray();

            std::vector<osg::Vec2d> pointVector;

            foreach(const QJsonValue& value, pointsData)
            {
                QJsonObject valueObject = value.toObject(); 
                valueObject["y"].toDouble(); 
                pointVector.push_back(osg::Vec2d (valueObject["x"].toDouble(), valueObject["y"].toDouble())); 
            }

            std::string  textureFile = ditchPopup->property("topTextureFile").toString().toStdString();

            if (!osgDB::fileExists(textureFile))
            {
                emit showError("Texture File error", "Texture File not found", true);
                return;
            }

            std::map<double, std::string> LocalpointProfileMap;

            QMap<QString, QVariant> LocalpointProfileQMap = pointProfileMap.toMap(); 

            if (LocalpointProfileQMap.empty())
            {
                LocalpointProfileQMap.insert("10", "Default");
            }

            for (QMap<QString, QVariant>::iterator itr = LocalpointProfileQMap.begin(); itr != LocalpointProfileQMap.end(); itr++)
            {
                std::string localString = itr.key().toStdString(); 
                LocalpointProfileMap.insert(std::pair<double, std::string>(std::stof(localString), itr.value().toString().toStdString()));
            }

            CORE::RefPtr<SMCUI::IAddVectorUIHandler> vectorUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddVectorUIHandler>(getGUIManager());
            vectorUIHandler->setDistanceProfileObject(profileFileName, LocalpointProfileMap);
            vectorUIHandler->setProfileTexture(textureFile);
            vectorUIHandler->createProfileFromVector(pointVector);
            
            profileName.resize(10);
            std::string applyString = '"' + profileName + '"' +" applied to Terrain!";
            ditchPopup->setProperty("profileAppliedText", QString::fromStdString(applyString));
        }

    }

    void LineLayerPropertiesGUI::_saveNewProfile(QVariant points)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::stringstream insertStatement;

        std::string profileTemplateFolder = appdataPath + profileTemplate;

        std::string path = osgDB::convertFileNameToNativeStyle(profileTemplateFolder);
        if (!osgDB::fileExists(path))
        {
            LOG_INFO("Created directory");
            osgDB::makeDirectory(path);
        }

        QObject* ditchProfilePopup = _findChild("ditchProfile");
        if (ditchProfilePopup)
        {
            QString name = ditchProfilePopup->property("name").toString();
            if (name.isEmpty())
            {
                emit showError("Profile name", "Please provide a profile name for saving");
                return;
            }

            for (int i = 0; i < _profileList.size(); i++)
            {
                if (name.toStdString() == _profileList[i])
                {
                    emit showError("Save Profile", "Profile with same name alreay exist");
                    return;
                }
            }

            std::string fileName;
            fileName = "/" + name.toStdString() + ".profile";

            QString sideTexturePath = ditchProfilePopup->property("sideTextureFilePath").toString();
            QFile settingFile(QString::fromStdString(profileTemplateFolder + fileName));

            if (!settingFile.open(QIODevice::WriteOnly))
            {
                emit showError("Save setting", "Could not open georbIS Server file");
                return;
            }

            QJsonObject settingObject;
            QJsonArray pointsData;

            _pointsList = points.toList();
            for (int i = 0; i < _pointsList.size(); i++)
            {
                QJsonObject pointData;
                pointData["x"] = _pointsList[i].toPointF().x();
                pointData["y"] = _pointsList[i].toPointF().y();
                
                pointsData.push_back(pointData);

            }

            settingObject["Texture"] = sideTexturePath;
            settingObject["Default"] = pointsData;

            QJsonDocument settingDoc(settingObject);
            settingFile.write(settingDoc.toJson());

            _readAllDitchProfiles();

            std::string defaultName = "Select profile";
            ditchProfilePopup->setProperty("currentProfileName", QString::fromStdString(defaultName));
            
            std::string profileCreatedText = "Profile " + name.toStdString() + " Created!"; 
            ditchProfilePopup->setProperty("profileCreatedText", QString::fromStdString(profileCreatedText));
    }
    }


    void LineLayerPropertiesGUI::_handleApplyButtonClicked()
    {
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");

        QString selectedTemplateName = lineLayerStylePopup->property("selectedTemplateName").toString();
        if (selectedTemplateName.toStdString() == "Select 3D style template" || selectedTemplateName.toStdString() == "Create new 3D style template")
        {
            emit  showError("Invalid 3D style template", "Please Choose Valid 3D Style");
            return;
        }

    }
    void LineLayerPropertiesGUI::_editExistingConnectedFeature(QString selectedValue)
    {

        _clearConnectedFeatureStruct(); 
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        bool editChecked = lineLayerStylePopup->property("editTemplateButton").toBool();

        if (editChecked)
        {
            if (selectedValue.toStdString() == "Select 3D style template" || selectedValue.toStdString() == "Create new 3D style template")
            {
                emit showError("Invalid 3D style template", "Select a valid template to edit");
                if (lineLayerStylePopup)
                {
                    lineLayerStylePopup->setProperty("selectedTemplateName", "Select 3D style template");
                    lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", QVariant::fromValue(false));
                    lineLayerStylePopup->setProperty("editTemplateButton", QVariant::fromValue(false));
                }
                return;
            }
        }
        else
        {
            if (selectedValue.toStdString() == "Select 3D style template")
            {
                lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", QVariant::fromValue(false));
                lineLayerStylePopup->setProperty("editTemplateButton", QVariant::fromValue(false));
                return;
            }
            else if (selectedValue.toStdString() == "Create new 3D style template")
            {
                _showParameterGrid(selectedValue);
                return;
            }
        }


        bool isExist = _getConnectedFeatureTemplate(selectedValue.toStdString());
        if (!isExist)
        {
            lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", QVariant::fromValue(false));
            return;
        }

        if (lineLayerStylePopup)
        {
            templateSelected();
            if (!editChecked)
            {
                lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", QVariant::fromValue(false));
            }
            else
            {
                lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", QVariant::fromValue(true));
            }
            QString templateName = lineLayerStylePopup->property("selectedTemplateName").toString();
            lineLayerStylePopup->setProperty("templateName", templateName);

        }

    }
    void LineLayerPropertiesGUI::templateSelected()
    {
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        if (lineLayerStylePopup)
        {

            if (!_connectedFeatureDataStack->type.empty())
            {
                if (_connectedFeatureDataStack->type == "Fence")
                {
                    lineLayerStylePopup->setProperty("geometryType", 0);
                }
                else if (_connectedFeatureDataStack->type == "Bridge")
                {
                    lineLayerStylePopup->setProperty("geometryType", 1);
                }
                else if (_connectedFeatureDataStack->type == "Power Line")
                {
                    lineLayerStylePopup->setProperty("geometryType", 2);
                }
                else if (_connectedFeatureDataStack->type == "Custom")
                {
                    lineLayerStylePopup->setProperty("geometryType", 3);
                }
                else
                {
                    //none
                }
                lineLayerStylePopup->setProperty("type", QString::fromStdString(_connectedFeatureDataStack->type));
            }
            if (_connectedFeatureDataStack->numberOfPillar > 0)
            {
                lineLayerStylePopup->setProperty("numberOfPillar", _connectedFeatureDataStack->numberOfPillar);
            }
            if (!_connectedFeatureDataStack->firstPillarModelPath.empty())
            {
                lineLayerStylePopup->setProperty("pillarModelPath1", QString::fromStdString(_connectedFeatureDataStack->firstPillarModelPath));
            }
            if (!_connectedFeatureDataStack->secondPillarModelPath.empty())
            {
                lineLayerStylePopup->setProperty("pillarModelPath2", QString::fromStdString(_connectedFeatureDataStack->secondPillarModelPath));
            }
            if (!_connectedFeatureDataStack->thirdPillarModelPath.empty())
            {
                lineLayerStylePopup->setProperty("pillarModelPath3", QString::fromStdString(_connectedFeatureDataStack->thirdPillarModelPath));
            }
            lineLayerStylePopup->setProperty("interPillarDistance", _connectedFeatureDataStack->interPillarDistance);
            lineLayerStylePopup->setProperty("featureWidth", _connectedFeatureDataStack->featureWidth);

            if (!_connectedFeatureDataStack->sideTexturePath.empty())
            {
                lineLayerStylePopup->setProperty("sideTexturePath", QString::fromStdString(_connectedFeatureDataStack->sideTexturePath));
                lineLayerStylePopup->setProperty("sideOffset", _connectedFeatureDataStack->sideOffset);
            }
            if (!_connectedFeatureDataStack->topSideTexturePath.empty())
            {
                lineLayerStylePopup->setProperty("topSideTexturePath", QString::fromStdString(_connectedFeatureDataStack->topSideTexturePath));
                lineLayerStylePopup->setProperty("topSideOffset", _connectedFeatureDataStack->topSideOffset);
            }
            if (!_connectedFeatureDataStack->bottomSideTexturePath.empty())
            {
                lineLayerStylePopup->setProperty("bottomSideTexturePath", QString::fromStdString(_connectedFeatureDataStack->bottomSideTexturePath));
                lineLayerStylePopup->setProperty("bottomSideOffset", _connectedFeatureDataStack->bottomSideOffset);
            }
            if (!_connectedFeatureDataStack->inner3DModelPath.empty())
            {
                lineLayerStylePopup->setProperty("inner3DModelPath", QString::fromStdString(_connectedFeatureDataStack->inner3DModelPath));
                lineLayerStylePopup->setProperty("numberOfInnerModel", _connectedFeatureDataStack->numberOfInnerModel);
                lineLayerStylePopup->setProperty("innerModelOffset", _connectedFeatureDataStack->innerModelOffset);
            }




            if (!_connectedFeatureDataStack->featureModelPath.empty())
            {
                lineLayerStylePopup->setProperty("featureModelPath", QString::fromStdString(_connectedFeatureDataStack->featureModelPath));
                lineLayerStylePopup->setProperty("featureOffsetVal", _connectedFeatureDataStack->featureOffsetVal);
                std::string sideOffFeatureVal = "";
                switch (_connectedFeatureDataStack->sidesOfFeatureVal)
                {
                case 0:
                    sideOffFeatureVal = "Left Side";
                    break;
                case 1:
                    sideOffFeatureVal = "Right Side";
                    break;
                case 2:
                    sideOffFeatureVal = "Both Side";
                    break;
                default:
                    break;
                }
                lineLayerStylePopup->setProperty("sidesOfFeatureVal", QString::fromStdString(sideOffFeatureVal));
            }
            else
            {
                lineLayerStylePopup->setProperty("sidesOfFeatureVal", QString::fromStdString("Both Side"));
            }
        }
    }
    void LineLayerPropertiesGUI::_clearConnectedFeatureStruct()
    {
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        if (lineLayerStylePopup)
        {
            lineLayerStylePopup->setProperty("numberOfPillar", "1");
            lineLayerStylePopup->setProperty("pillarModelPath1", "");
            lineLayerStylePopup->setProperty("pillarModelPath2", "");
            lineLayerStylePopup->setProperty("pillarModelPath3", "");
            lineLayerStylePopup->setProperty("interPillarDistance", "");
            lineLayerStylePopup->setProperty("featureWidth", "");
            lineLayerStylePopup->setProperty("sideTexturePath", "");
            lineLayerStylePopup->setProperty("topSideTexturePath", "");
            lineLayerStylePopup->setProperty("bottomSideTexturePath", "");
            lineLayerStylePopup->setProperty("inner3DModelPath", "");
            lineLayerStylePopup->setProperty("sideOffset", "");
            lineLayerStylePopup->setProperty("topSideOffset", "");
            lineLayerStylePopup->setProperty("bottomSideOffset", "");
            lineLayerStylePopup->setProperty("numberOfInnerModel", "");
            lineLayerStylePopup->setProperty("innerModelOffset", "");
            lineLayerStylePopup->setProperty("featureModelPath", "");
            lineLayerStylePopup->setProperty("featureOffsetVal", "");
            lineLayerStylePopup->setProperty("geometryType", "0");
            lineLayerStylePopup->setProperty("type","Fence");
            lineLayerStylePopup->setProperty("sidesOfFeatureVal", "Left Side");
        }
        _connectedFeatureDataStack = NULL;
        _connectedFeatureDataStack = new GIS::ConnectedFeatureData();

    }
    bool LineLayerPropertiesGUI::_getConnectedFeatureTemplate(std::string templateName)
    {
        _clearConnectedFeatureStruct();
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string templateFolder = appdataPath + _connectedFeatureTemplateStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(templateFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting error", "Template folder not found in Application", true);
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == templateName)
            {
                found = true;
            }
        }

        if (!found)
        {
            emit showError("Read setting error", "Template this name not found in Application", true);
            return false;
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + _connectedFeatureTemplateStoragePath + templateName + ".feature"));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                emit showError("Read setting error", "Can not open file from Application for reading ", true);
                return false;
            }

            //reading JSON
            QByteArray settingData = settingFile.readAll();
            QString setting(settingData);
            QJsonDocument jsonResponse = QJsonDocument::fromJson(setting.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            QJsonObject templateRootObject = jsonObject["3dConnectedFeature"].toObject();
            if (!templateRootObject.isEmpty())
            {
                    if (!templateRootObject["Type"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->type = templateRootObject["Type"].toString().toStdString();
                    }
                    if (!templateRootObject["NumberOfPillar"].isNull())
                    {
                        _connectedFeatureDataStack->numberOfPillar = templateRootObject["NumberOfPillar"].toInt();
                    }
                    if (!templateRootObject["PillarModelPath1"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->firstPillarModelPath = templateRootObject["PillarModelPath1"].toString().toStdString();
                    }
                    if (!templateRootObject["PillarModelPath2"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->secondPillarModelPath = templateRootObject["PillarModelPath2"].toString().toStdString();
                    }
                    if (!templateRootObject["PillarModelPath3"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->thirdPillarModelPath = templateRootObject["PillarModelPath3"].toString().toStdString();
                    }
                    if (!templateRootObject["InterPillarDistance"].isNull())
                    {
                        _connectedFeatureDataStack->interPillarDistance = templateRootObject["InterPillarDistance"].toDouble();
                    }
                    if (!templateRootObject["FeatureWidth"].isNull())
                    {
                        _connectedFeatureDataStack->featureWidth = templateRootObject["FeatureWidth"].toDouble();
                    }
                    if (!templateRootObject["SideTexturePath"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->sideTexturePath = templateRootObject["SideTexturePath"].toString().toStdString();
                    }
                    if (!templateRootObject["TopSideTexturePath"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->topSideTexturePath = templateRootObject["TopSideTexturePath"].toString().toStdString();
                    }
                    if (!templateRootObject["BottomSideTexturePath"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->bottomSideTexturePath = templateRootObject["BottomSideTexturePath"].toString().toStdString();
                    }
                    if (!templateRootObject["Inner3DModelPath"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->inner3DModelPath = templateRootObject["Inner3DModelPath"].toString().toStdString();
                    }
                    if (!templateRootObject["SideOffset"].isNull())
                    {
                        _connectedFeatureDataStack->sideOffset = templateRootObject["SideOffset"].toDouble();
                    }
                    if (!templateRootObject["TopSideOffset"].isNull())
                    {
                        _connectedFeatureDataStack->topSideOffset = templateRootObject["TopSideOffset"].toDouble();
                    }
                    if (!templateRootObject["BottomSideOffset"].isNull())
                    {
                        _connectedFeatureDataStack->bottomSideOffset = templateRootObject["BottomSideOffset"].toDouble();
                    }
                    if (!templateRootObject["NumberOfInnerModel"].isNull())
                    {
                        _connectedFeatureDataStack->numberOfInnerModel = templateRootObject["NumberOfInnerModel"].toDouble();
                    }
                    if (!templateRootObject["InnerModelOffset"].isNull())
                    {
                        _connectedFeatureDataStack->innerModelOffset = templateRootObject["InnerModelOffset"].toDouble();
                    }
                    if (!templateRootObject["FeatureModelPath"].toString().isEmpty())
                    {
                        _connectedFeatureDataStack->featureModelPath = templateRootObject["FeatureModelPath"].toString().toStdString();
                    }
                    if (!templateRootObject["FeatureOffsetVal"].isNull())
                    {
                        _connectedFeatureDataStack->featureOffsetVal = templateRootObject["FeatureOffsetVal"].toDouble();
                    }
                    if (!templateRootObject["SidesOfFeatureVal"].isNull())
                    {
                        _connectedFeatureDataStack->sidesOfFeatureVal = templateRootObject["SidesOfFeatureVal"].toInt();
                    }

            }
        }
        return true;
    }
    //function for saving the template
    void LineLayerPropertiesGUI::_handleSaveButtonClicked()
    {
        QString lpName;
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        if (lineLayerStylePopup)
        {
            lpName = lineLayerStylePopup->property("templateName").toString();
            if (lpName == "")
            {
                emit showError("Template name invalid", "Template name not specified", true);
                return;
            }
            if ((lpName == "Select 3D style template") || (lpName == "Create new 3D style template"))
            {
                emit showError("Template name invalid", "Please enter name other than " + lpName, true);
                return;
            }

            bool editChecked = lineLayerStylePopup->property("editTemplateButton").toBool();

            bool present = _isAlreadyPresent(lpName.toStdString());

            if (present && !editChecked)
            {
                emit showError("Please Change the name", "Template with this name is already present", true);
                return;
            }
            if (editChecked)//overwrite or new add tempalte Handling
            {
                QString selectedTemplateName = lineLayerStylePopup->property("selectedTemplateName").toString();
                if (!selectedTemplateName.isEmpty())
                {
                    if (lpName == selectedTemplateName)
                    {
                        bool present = _isAlreadyPresent(selectedTemplateName.toStdString());
                        if (!present)
                        {
                            emit showError("Error in Saving", "Template Folder Doesn't Exist", true);
                            return;
                        }
                        else
                        {
                            //New Tempalte Added handling
                            if (!updateTemplate()) //fillup with Current Update Data
                            {
                                return;
                            }
                        }
                    }
                    else
                    {
                        //validation Check
                        if (!updateTemplate()) //fillup with Current Update Data
                        {
                            return;
                        }
                    }
                }
            }
            else
            {
                //read data and Store in _connectedFeature 
                if (!updateTemplate()) //fillup with Current Update Data
                {
                    return;
                }
            }



            _writeTemplate(lpName);
            _readAllTemplate();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select 3D style template", "0"));
            comboBoxList.append(new VizComboBoxElement("Create new 3D style template", "1"));

            for (int i = 0; i < _templateList.size(); i++)
            {
                std::string templateName = _templateList[i];
                comboBoxList.append(new VizComboBoxElement(templateName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("connected3DFeatureList", QVariant::fromValue(comboBoxList));
            QString defaultName = "Select 3D style template";
            if (_templateList.size()>0)
            {
                defaultName = lpName;
            }
            lineLayerStylePopup->setProperty("selectedTemplateName", defaultName);
            QString templateName = lineLayerStylePopup->property("selectedTemplateName").toString();
            _editExistingConnectedFeature(defaultName);
        }


    }
    bool LineLayerPropertiesGUI::updateTemplate()
    {
      
        bool validation=true;
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        if (!lineLayerStylePopup)
            return false;

        if (lineLayerStylePopup->property("type").toString().isEmpty())
        {
            emit showError("Invalid Type Selection", "Please Select Proper Type ", true);
            return false;
        }
        _connectedFeatureDataStack->type = lineLayerStylePopup->property("type").toString().toStdString();
        int numberOfPillar = lineLayerStylePopup->property("numberOfPillar").toString().toInt();
        if (lineLayerStylePopup->property("numberOfPillar").toString().isEmpty() || numberOfPillar <= 0 || numberOfPillar>3)
        {
            emit showError("Pillar Selection Error", "Please Select Valid Number Of Pillar ", true);
            return false;
        }
        _connectedFeatureDataStack->numberOfPillar = lineLayerStylePopup->property("numberOfPillar").toInt();
        switch (numberOfPillar)
        {
            case 1:
            {
                if (lineLayerStylePopup->property("pillarModelPath1").toString().isEmpty())
                {
                    emit showError("Pillar Model Path Error", "Please Select Valid Pillar Model Path ", true);
                    return false;
                }
                else
                {
                    _connectedFeatureDataStack->firstPillarModelPath = lineLayerStylePopup->property("pillarModelPath1").toString().toStdString();
                    _connectedFeatureDataStack->secondPillarModelPath = "";
                    _connectedFeatureDataStack->thirdPillarModelPath = "";

                }
            }
            break;
            case 2:
            {
                if (lineLayerStylePopup->property("pillarModelPath1").toString().isEmpty() || lineLayerStylePopup->property("pillarModelPath2").toString().isEmpty())
                {
                    emit showError("Pillar Model Path Error", "Please Select Valid Pillar Model Path ", true);
                    return false;
                }
                else
                {
                    _connectedFeatureDataStack->firstPillarModelPath = lineLayerStylePopup->property("pillarModelPath1").toString().toStdString();
                    _connectedFeatureDataStack->secondPillarModelPath = lineLayerStylePopup->property("pillarModelPath2").toString().toStdString();
                    _connectedFeatureDataStack->thirdPillarModelPath = "";

                }
            }
            break;
            case 3:
            {
                if (lineLayerStylePopup->property("pillarModelPath1").toString().isEmpty() || lineLayerStylePopup->property("pillarModelPath2").toString().isEmpty() || lineLayerStylePopup->property("pillarModelPath3").toString().isEmpty())
                {
                    emit showError("Pillar Model Path Error", "Please Select Valid Pillar Model Path ", true);
                    return false;
                }
                else
                {
                    _connectedFeatureDataStack->firstPillarModelPath = lineLayerStylePopup->property("pillarModelPath1").toString().toStdString();
                    _connectedFeatureDataStack->secondPillarModelPath = lineLayerStylePopup->property("pillarModelPath2").toString().toStdString();
                    _connectedFeatureDataStack->thirdPillarModelPath = lineLayerStylePopup->property("pillarModelPath3").toString().toStdString();

                }
            }
            break;
            
            default:
                break;
        }
        if (lineLayerStylePopup->property("interPillarDistance").toString().isEmpty() || lineLayerStylePopup->property("interPillarDistance").toDouble() <=0 )
        {
            emit showError("Inter Pillar Distance Error", "Please Enter Proper Distance ", true);
            return false;
        }
        else
        {
            _connectedFeatureDataStack->interPillarDistance = lineLayerStylePopup->property("interPillarDistance").toDouble();
        }
        if (lineLayerStylePopup->property("featureWidth").toString().isEmpty() || lineLayerStylePopup->property("featureWidth").toDouble()<=0)
        {
            emit showError("Feature Width Error", "Please Enter Proper Feature Width ", true);
            return false;
        }
        else
        {
            _connectedFeatureDataStack->featureWidth = lineLayerStylePopup->property("featureWidth").toDouble();
        }

        if (lineLayerStylePopup->property("sideImageTextureVisible").toBool() && !lineLayerStylePopup->property("sideTexturePath").toString().isEmpty())
        {
            if (lineLayerStylePopup->property("sideOffset").toString().isEmpty())
            {
                emit showError("Side Image Offset Error", "Please Enter Proper Side Image Offset", true);
                return false;
            }
            else
            {
                _connectedFeatureDataStack->sideTexturePath = lineLayerStylePopup->property("sideTexturePath").toString().toStdString();;
                _connectedFeatureDataStack->sideOffset = lineLayerStylePopup->property("sideOffset").toDouble();
            }
        }
        else
        {
            _connectedFeatureDataStack->sideTexturePath = "";
            _connectedFeatureDataStack->sideOffset = 0;
        }
        if (lineLayerStylePopup->property("topSideImageTextureVisible").toBool() &&  !lineLayerStylePopup->property("topSideTexturePath").toString().isEmpty())
        {
            if (lineLayerStylePopup->property("topSideOffset").toString().isEmpty())
            {
                emit showError("Top Side Image Offset Error", "Please Enter Proper Top Side Image Offset", true);
                return false;
            }
            else
            {
                _connectedFeatureDataStack->topSideTexturePath = lineLayerStylePopup->property("topSideTexturePath").toString().toStdString();;
                _connectedFeatureDataStack->topSideOffset = lineLayerStylePopup->property("topSideOffset").toDouble();
            }
        }
        else
        {
            _connectedFeatureDataStack->topSideTexturePath = "";
            _connectedFeatureDataStack->topSideOffset = 0;
        }
        if (lineLayerStylePopup->property("bottomSideImageTextureVisible").toBool() && !lineLayerStylePopup->property("bottomSideTexturePath").toString().isEmpty())
        {
            if (lineLayerStylePopup->property("bottomSideOffset").toString().isEmpty())
            {
                emit showError("Bottom Side Image Offset Error", "Please Enter Proper Bottom Side Image Offset", true);
                return false;
            }
            else
            {
                _connectedFeatureDataStack->bottomSideTexturePath = lineLayerStylePopup->property("bottomSideTexturePath").toString().toStdString();;
                _connectedFeatureDataStack->bottomSideOffset = lineLayerStylePopup->property("bottomSideOffset").toDouble();
            }
        }
        else
        {
            _connectedFeatureDataStack->bottomSideTexturePath = "";
            _connectedFeatureDataStack->bottomSideOffset = 0;
        }
        if (lineLayerStylePopup->property("inner3DModelVisible").toBool() &&  !lineLayerStylePopup->property("inner3DModelPath").toString().isEmpty())
        {
            if (lineLayerStylePopup->property("innerModelOffset").toString().isEmpty())
            {
                emit showError("Inner Model Offset Error", "Please Enter Proper Inner Model offset", true);
                return false;
            }
            else if (lineLayerStylePopup->property("numberOfInnerModel").toString().isEmpty() || lineLayerStylePopup->property("numberOfInnerModel").toString().toDouble() <=0 )
            {
                emit showError("Number of Inner Model Error", "Please Enter Proper Inner Model Number", true);
                return false;
            }
            else
            {
                _connectedFeatureDataStack->inner3DModelPath = lineLayerStylePopup->property("inner3DModelPath").toString().toStdString();;
                _connectedFeatureDataStack->innerModelOffset = lineLayerStylePopup->property("innerModelOffset").toDouble();
                _connectedFeatureDataStack->numberOfInnerModel = lineLayerStylePopup->property("numberOfInnerModel").toInt();
            }
        }
        else
        {
            _connectedFeatureDataStack->inner3DModelPath ="";
            _connectedFeatureDataStack->innerModelOffset = 0;
            _connectedFeatureDataStack->numberOfInnerModel = 0;
        }

        if (lineLayerStylePopup->property("featureModelVisible").toBool() &&  !lineLayerStylePopup->property("featureModelPath").toString().isEmpty())
        {
            if (lineLayerStylePopup->property("featureOffsetVal").toString().isEmpty())
            {
                emit showError("Feature Model Offset Error", "Please Enter Proper Feature Model offset", true);
                return false;
            }
            else if (lineLayerStylePopup->property("sidesOfFeatureVal").toString().isEmpty())
            {
                emit showError("Side Of Feature Error", "Please Enter Proper Side Of Feature Error", true);
                return false;
            }
            else
            {
                _connectedFeatureDataStack->featureModelPath = lineLayerStylePopup->property("featureModelPath").toString().toStdString();;
                _connectedFeatureDataStack->featureOffsetVal = lineLayerStylePopup->property("featureOffsetVal").toDouble();
                int uid = 0;
                if (lineLayerStylePopup->property("sidesOfFeatureVal").toString() == "Left Side")
                {
                    uid = 0;
                }
                else if (lineLayerStylePopup->property("sidesOfFeatureVal").toString() == "Right Side")
                {
                    uid = 1;
                }
                else if (lineLayerStylePopup->property("sidesOfFeatureVal").toString() == "Both Side")
                {
                    uid = 2;
                }
                _connectedFeatureDataStack->sidesOfFeatureVal = uid;


            }
        }
        else
        {
            _connectedFeatureDataStack->featureModelPath    = "";
            _connectedFeatureDataStack->featureOffsetVal    = 0;
            _connectedFeatureDataStack->sidesOfFeatureVal   = 0;
        }
        return true;

    }
    void LineLayerPropertiesGUI::_deleteConnectedFeatureTemplate(QString value)
    {
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        if (value.toStdString() == "Select 3D style template" || value.toStdString() == "Create new 3D style template")
        {
            emit  showError("Invalid template selection", "Please Choose Valid template");
            lineLayerStylePopup->setProperty("editTemplateButton", false);
            return;
        }

        emit question("Delete template", "Do you really want to delete?", false,
            "_okSlotForDeleting()", "_cancelSlotForDeleting()");

    }
    void LineLayerPropertiesGUI::_cancelSlotForDeleting()
    {
        return;
    }
    void LineLayerPropertiesGUI::_okSlotForDeleting()
    {

        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        lineLayerStylePopup->setProperty("editTemplateButton", false);
        lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", false);
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();
        std::string templateFolder = appdataPath + _connectedFeatureTemplateStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(templateFolder);
        if (!osgDB::fileExists(path))
        {
            emit  showError("Deletion Error", "Tempalte Folder doesn't Exist in Application");
            return;
        }

        if (lineLayerStylePopup)
        {
            QString lpName = lineLayerStylePopup->property("selectedTemplateName").toString();
            if (lpName == "")
            {
                return;
            }

            std::string filename = lpName.toStdString() + ".feature";
            std::string filePath = templateFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, templateFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
            }
            _readAllTemplate();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select 3D style template", "0"));
            comboBoxList.append(new VizComboBoxElement("Create new 3D style template", "1"));

            for (int i = 0; i < _templateList.size(); i++)
            {
                std::string templateName = _templateList[i];
                comboBoxList.append(new VizComboBoxElement(templateName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("connected3DFeatureList", QVariant::fromValue(comboBoxList));

            QString defaultName = "Select 3D style template";
            if (_templateList.size()>0)
            {
                defaultName = _templateList[_templateList.size() - 1].c_str();
            }
            lineLayerStylePopup->setProperty("selectedTemplateName", defaultName);
            QString templateName = lineLayerStylePopup->property("selectedTemplateName").toString();
            _showParameterGrid(templateName);
        }

    }
    // to decide whether to show parameters grid or not
    void LineLayerPropertiesGUI::_showParameterGrid(QString selectedValue)
    {
        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");

        if (selectedValue.toStdString() == "Select 3D style template")
        {
            if (lineLayerStylePopup)
            {
                lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", QVariant::fromValue(false));
            }
        }

        else if (selectedValue.toStdString() == "Create new 3D style template")
        {
            if (lineLayerStylePopup)
            {
                lineLayerStylePopup->setProperty("templateName", "");
                lineLayerStylePopup->setProperty("open3DConnectedFeatureParameterGrid", QVariant::fromValue(true));
            }
        }
    }
    //internal function to read from JSON and populate the comboboz list
    void LineLayerPropertiesGUI::_readAllTemplate()
    {
        _templateList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string templateFolder = appdataPath + _connectedFeatureTemplateStoragePath;


        std::string path = osgDB::convertFileNameToNativeStyle(templateFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _templateList.push_back(fileNameWithoutExtension);
        }

    }
    // checks all Tempalte files with the name passed and returns true if its present
    bool LineLayerPropertiesGUI::_isAlreadyPresent(std::string name)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string templateFolder = appdataPath + _connectedFeatureTemplateStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(templateFolder);
        if (!osgDB::fileExists(path))
        {
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == name)
            {
                return true;
            }
        }

        return false;
    }
    // internal function to write in JSON
    void LineLayerPropertiesGUI::_writeTemplate(QString name)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::stringstream insertStatement;

        std::string templateFolder = appdataPath + _connectedFeatureTemplateStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(templateFolder);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }
        std::string fileName = "/" + name.toStdString() + ".feature";
        //Delete File and Rewrite Again
        if (_isAlreadyPresent(name.toStdString()))
        {
            UTIL::deleteFile(templateFolder + fileName);
            //delete file
        }
        QFile settingFile(QString::fromStdString(templateFolder + fileName));

        QByteArray settingData;
        if (settingFile.open(QIODevice::ReadOnly))
        {
            settingData = settingFile.readAll();
            settingFile.close();
        }
        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

        //**** writing in JSON
        QJsonObject templateRootObject; // root 
        QJsonObject templateObject; 


        if (!_connectedFeatureDataStack->type.empty())
        {
            templateObject.insert("Type", _connectedFeatureDataStack->type.c_str());
        }
            templateObject.insert("NumberOfPillar", _connectedFeatureDataStack->numberOfPillar);
        if (!_connectedFeatureDataStack->firstPillarModelPath.empty())
        {
            templateObject.insert("PillarModelPath1", _connectedFeatureDataStack->firstPillarModelPath.c_str());
        }
        if (!_connectedFeatureDataStack->secondPillarModelPath.empty())
        {
            templateObject.insert("PillarModelPath2", _connectedFeatureDataStack->secondPillarModelPath.c_str());
        }
        if (!_connectedFeatureDataStack->thirdPillarModelPath.empty())
        {
            templateObject.insert("PillarModelPath3", _connectedFeatureDataStack->thirdPillarModelPath.c_str());
        }
        templateObject.insert("InterPillarDistance", _connectedFeatureDataStack->interPillarDistance);
        templateObject.insert("FeatureWidth", _connectedFeatureDataStack->featureWidth);
        if (!_connectedFeatureDataStack->sideTexturePath.empty())
        {
            templateObject.insert("SideTexturePath", _connectedFeatureDataStack->sideTexturePath.c_str());
            templateObject.insert("SideOffset", _connectedFeatureDataStack->sideOffset);

        }
        if (!_connectedFeatureDataStack->topSideTexturePath.empty())
        {
            templateObject.insert("TopSideTexturePath", _connectedFeatureDataStack->topSideTexturePath.c_str());
            templateObject.insert("TopSideOffset", _connectedFeatureDataStack->topSideOffset);
        }
        if (!_connectedFeatureDataStack->bottomSideTexturePath.empty())
        {
            templateObject.insert("BottomSideTexturePath", _connectedFeatureDataStack->bottomSideTexturePath.c_str());
            templateObject.insert("BottomSideOffset", _connectedFeatureDataStack->bottomSideOffset);

        }
        if (!_connectedFeatureDataStack->inner3DModelPath.empty())
        {
            templateObject.insert("Inner3DModelPath", _connectedFeatureDataStack->inner3DModelPath.c_str());
            templateObject.insert("NumberOfInnerModel", _connectedFeatureDataStack->numberOfInnerModel);
            templateObject.insert("InnerModelOffset", _connectedFeatureDataStack->innerModelOffset);
        }

        if (!_connectedFeatureDataStack->featureModelPath.empty())
        {
            templateObject.insert("FeatureModelPath", _connectedFeatureDataStack->featureModelPath.c_str());
            templateObject.insert("FeatureOffsetVal", _connectedFeatureDataStack->featureOffsetVal);
            templateObject.insert("SidesOfFeatureVal", _connectedFeatureDataStack->sidesOfFeatureVal);
        }
        templateRootObject.insert("3dConnectedFeature", templateObject);
        settingDoc.setObject(templateRootObject);
        //**********
        /* settingDoctemp.setObject(settingObject);*/
        if (!settingFile.open(QIODevice::WriteOnly))
        {
            emit showError("Save setting error", "Could not open Template file for save");
            return;
        }

        settingFile.write(settingDoc.toJson());
    }
    void LineLayerPropertiesGUI::recFenceFolderCreated(bool isAlreadyFolder){
        CORE::RefPtr<CORE::IObject> featureLayerObject = _tempCurrentSelection->getInterface<CORE::IObject>();
        CORE::RefPtr<CORE::ICompositeObject> featureLayerObjectParentComp = featureLayerObject->getParent()->getInterface<CORE::ICompositeObject>();
        if (isAlreadyFolder)
        {
            //Nothing
        }
        else
        {
            featureLayerObjectParentComp->removeObjectByID(&(featureLayerObject->getInterface<CORE::IBase>()->getUniqueID()));
            _folderObject->getInterface<CORE::ICompositeObject>()->addObject(featureLayerObject.get());
            featureLayerObjectParentComp->addObject(_folderObject.get());


        }
        _unsubscribe(_tempCurrentSelection.get(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);

        CORE::IComponent* component = ELEMENTS::GetComponentFromMaintainer("TileLoaderComponent");
        if (component)
        {
            _subscribe(component, *GIS::IOSGOperationQueueHolder::OperationQueueStatusChangedMessageType);
        }

        _tempCurrentSelection = NULL;
        _folderObject = NULL;

        _originalGateLayerPath.clear();
        _originalUnfencedLayerPath.clear();

        return;
    }

    void LineLayerPropertiesGUI::_setBusy()
    {
        if(!_currentSelection.valid())
            return;

        QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
        if(lineLayerStylePopup)
        {
            lineLayerStylePopup->setProperty("busyBarVisible", QVariant::fromValue(true));
        }

        //! subscribe to IDrawPropertyHolder::DrawCompletedMessageType message
        _subscribe(_currentSelection.get(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
    }


    void LineLayerPropertiesGUI::_makeFenceFolder()
    {
        CORE::RefPtr<CORE::IObject> featureLayerSyleObject = _tempCurrentSelection->getInterface<CORE::IObject>();

        CORE::RefPtr<CORE::IObject> parent = featureLayerSyleObject->getParent();
        CORE::RefPtr<CORE::ITaggable> tagObject = parent->getInterface<CORE::ITaggable>();
        CORE::RefPtr<CORE::ICompositeObject> compositeObject = NULL;
        compositeObject =parent->getInterface<CORE::ICompositeObject>();
        CORE::RefPtr<CORE::IObject> folderObject = NULL;
        std::string pathofCurrentLine;
        bool isAlreadyFolder = false;
        if (tagObject.valid())
        {
            isAlreadyFolder = true;
            folderObject = compositeObject->getInterface<CORE::IObject>();
            //delete Previous Node and Create New Node
            //Iterate Remove Gis Feature Layer and Add new GIS Feature Layer
            CORE::ICompositeObject::ObjectMap::const_iterator iter =
                compositeObject->getObjectMap().begin();
            //! Index of item to be deleted
            CORE::RefPtr<CORE::IDeletable> itemToBeDeleted = NULL;
            // add a item for each object in composite object
            while (iter != compositeObject->getObjectMap().end())
            {
                CORE::RefPtr<CORE::IObject> obj = iter->second.get();

                CORE::RefPtr<GIS::IFeatureLayer> gisFeatureLayerobj = obj->getInterface<GIS::IFeatureLayer>();
                if (gisFeatureLayerobj.valid())
                {
                    itemToBeDeleted = obj->getInterface<CORE::IDeletable>();
                }
                ++iter;
            }
            if (itemToBeDeleted.valid())
            {
                CORE::RefPtr<VizUI::IDeletionUIHandler> delUIHandler = APP::AccessElementUtils::
                    getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
                //CORE::RefPtr< CORE::IDeletable> deletable = obj->getInterface<CORE::IDeletable>();
                if (delUIHandler.valid())
                {
                    delUIHandler->deleteObject(itemToBeDeleted);

                }
            }
        }
        else
        {
            compositeObject = NULL;
           
            CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
            folderObject = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FolderType);
            compositeObject = folderObject->getInterface<CORE::ICompositeObject>();

            folderObject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName(pathofCurrentLine) + " Folder");
        }
        CORE::RefPtr<CORE::IObject> featureLayerObject = _tempCurrentSelection->getInterface<CORE::IObject>();


        CORE::RefPtr<TERRAIN::IModelObject> modelObject = _tempCurrentSelection->getInterface<TERRAIN::IModelObject>();
        if (modelObject.valid())
        {
            pathofCurrentLine = modelObject->getUrl();
        }
        CORE::RefPtr<GIS::IFeatureLayer> gisFeatureLayer = _tempCurrentSelection->getInterface<GIS::IFeatureLayer>();
        if (gisFeatureLayer.valid())
        {
            pathofCurrentLine = gisFeatureLayer->getDataSource();
        }

        CORE::RefPtr<DB::ReaderWriter::Options>  optionsGate = new DB::ReaderWriter::Options;
        optionsGate->setMapValue("Clamping", UTIL::ToString(true));
        optionsGate->setMapValue("Editable", UTIL::ToString(false));
        optionsGate->setMapValue("TileLoader", UTIL::ToString(false));

        CORE::RefPtr<DB::ReaderWriter::Options>  optionsUnfence = new DB::ReaderWriter::Options;
        optionsUnfence->setMapValue("Clamping", UTIL::ToString(true));
        optionsUnfence->setMapValue("Editable", UTIL::ToString(false));
        optionsUnfence->setMapValue("TileLoader", UTIL::ToString(false));

        CORE::RefPtr<DB::ReaderWriter::Options>  optionsModFence = new DB::ReaderWriter::Options;
        optionsModFence->setMapValue("Clamping", UTIL::ToString(true));
        optionsModFence->setMapValue("Editable", UTIL::ToString(false));
        optionsModFence->setMapValue("TileLoader", UTIL::ToString(false));

        if(!_originalGateLayerPath.empty()){
            _originalGateLayerPath = osgDB::convertFileNameToNativeStyle(_originalGateLayerPath);
            CORE::RefPtr<CORE::IObject> gateObject = DB::readFeature(_originalGateLayerPath, optionsGate.get());
            gateObject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName(_originalGateLayerPath));
            compositeObject->addObject(gateObject.get());
            optionsModFence->setMapValue("GateDataSource", _originalGateLayerPath);

            //Mod Fence file
            CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
            if(wmtnr == NULL)
            {
                LOG_ERROR("World maintainer not valid");
                return;
            }
            std::string projectLocation = wmtnr->getProjectFile();

            projectLocation = osgDB::getFilePath(projectLocation) + "\\FenceData";
            osgDB::makeDirectory(projectLocation);
            projectLocation = projectLocation + "\\" + osgDB::getStrippedName(_originalGateLayerPath) + "FenceMod.shp";
            optionsModFence->setMapValue("ModFenceDataSource", projectLocation);
        }

        if(!_originalUnfencedLayerPath.empty()){
            _originalUnfencedLayerPath = osgDB::convertFileNameToNativeStyle(_originalUnfencedLayerPath);
            CORE::RefPtr<CORE::IObject> unfenceModelObject = DB::readFeature(_originalUnfencedLayerPath, optionsUnfence.get());
            unfenceModelObject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName(_originalUnfencedLayerPath));
            compositeObject->addObject(unfenceModelObject.get());
        }

        optionsModFence->setMapValue("DrawTechnique", "ConnectedFeatureDrawProperty");

        CORE::RefPtr<CORE::IObject> fenceModelObject = DB::readFeature(pathofCurrentLine, optionsModFence.get());
        fenceModelObject->getInterface<CORE::IBase>(true)->setName(osgDB::getSimpleFileName(pathofCurrentLine) + " Model");

        //! XXX: Add view based visibility property to fence model layer
        CORE::RefPtr<CORE::IProperty> vbcProp = CORE::CoreRegistry::instance()->getPropertyFactory()->
            createProperty(*VIEW::ViewCoreRegistryPlugin::ViewBasedVisibilityPropertyType);

        fenceModelObject->addProperty(vbcProp.get());

        vbcProp->getInterface<VIEW::IViewBasedVisibility>()->setMaxAltitude(750);
        //Apply Style 
        applyInputStyle(fenceModelObject);
        compositeObject->addObject(fenceModelObject.get());

        _subscribe(fenceModelObject.get(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
        if (_currentSelection.valid())
        {
            _unsubscribe(_currentSelection.get(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
        }
        _folderObject = folderObject;
        emit sendFenceFolderCreated(isAlreadyFolder);
    }
    void LineLayerPropertiesGUI::applyInputStyle(CORE::RefPtr<CORE::IObject> obj)
    {
        //_resetStyleOnClose = true;

        CORE::IProperty* layerStyleProperty = obj->getInterface<CORE::IObject>()->getProperty(GIS::GISRegistryPlugin::LayerStylePropertyType.get());
        if (!layerStyleProperty)
            return;

        GIS::ILineStyle* lineStyle = layerStyleProperty->getInterface<GIS::ILineStyle>();
        if (lineStyle)
        {
            osg::Vec4 penColor((float)_originalColor.red() / 255.0f, (float)_originalColor.green() / 255.0f,
                (float)_originalColor.blue() / 255.0f, (float)_originalColor.alpha() / 255.0f);

            lineStyle->setLineColor(penColor);
            lineStyle->setLineWidth(_originalLineWidth);
            lineStyle->setLineStipple(_originalLineStipple, _originalLineStippleFactor);

        }

        GIS::IConnectedFeatureStyle* connectedFeatureStyle = layerStyleProperty->getInterface<GIS::IConnectedFeatureStyle>();
        if (connectedFeatureStyle)
        {
            //connectedFeatureStyle->setConnectedFeatureConfig(_connectedFeatureDataStack);
            connectedFeatureStyle->setFenceConfig(_connectedFeatureDataStack);
        }

        GIS::IIBStyle* ibStyle = layerStyleProperty->getInterface<GIS::IIBStyle>();
        if (ibStyle)
        {
            ibStyle->setIBWidth(_originalIBWidth);
            ibStyle->setAutoScale(_originalIBAutoScale);
            ibStyle->setIBFlip(_originalIBFlip);
            ibStyle->setOneLineIB(_originalOneLineIB);

            if (!_originalOneLineIB)
            {
                osg::Vec4 penColor1((float)_originalInnerColor.red() / 255.0f, (float)_originalInnerColor.green() / 255.0f,
                    (float)_originalInnerColor.blue() / 255.0f, (float)_originalInnerColor.alpha() / 255.0f);

                osg::Vec4 penColor2((float)_originalOuterColor.red() / 255.0f, (float)_originalOuterColor.green() / 255.0f,
                    (float)_originalOuterColor.blue() / 255.0f, (float)_originalOuterColor.alpha() / 255.0f);

                ibStyle->setInnerBounColor(penColor1);
                ibStyle->setOuterBounColor(penColor2);
            }

            else
            {
                osg::Vec4 penColor1((float)_originalSingleLineIBColor.red() / 255.0f, (float)_originalSingleLineIBColor.green() / 255.0f,
                    (float)_originalSingleLineIBColor.blue() / 255.0f, (float)_originalSingleLineIBColor.alpha() / 255.0f);

                ibStyle->setSingleIBBounColor(penColor1);
            }
        }
    }
    // This is called when apply button is pressed
    void LineLayerPropertiesGUI::changeDrawTechniqueType()
    {
        if(!_currentSelection.valid())
            return;

        _setBusy();

        //! If current selection is TERRAIN::IModelObject
        //! Then just convert it to GIS::IFeatureLayer
        CORE::RefPtr<TERRAIN::IModelObject> modelObject = _currentSelection->getInterface<TERRAIN::IModelObject>();
        if (modelObject.valid())
        {
            //! Get data source
            std::string datasource = modelObject->getUrl();

            try
            {
                //osg::ref_ptr<UTIL::ThreadPool> tp = CORE::WorldMaintainer::instance()->getComputeThreadPool();
                _tempCurrentSelection = _currentSelection;
                osg::ref_ptr<UTIL::ThreadPool> tp = APP::AccessElementUtils::getWorldFromManager(_manager)->getWorldMaintainer()->getComputeThreadPool();
                tp->invoke(boost::bind(&LineLayerPropertiesGUI::_makeFenceFolder, this));

            }

            catch (const UTIL::Exception& e)
            {
                e.LogException();
                throw e;
            }

            return;

        }
        applyInputStyle(_currentSelection->getInterface<CORE::IObject>());

    }

    void LineLayerPropertiesGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *GIS::IDrawPropertyHolder::DrawCompletedMessageType)
        {
            //! unsubscribe
            if(_currentSelection.valid())
            {
                _unsubscribe(_currentSelection.get(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
            }

            if (message.getSender())
            {
                _unsubscribe(message.getSender(), *GIS::IDrawPropertyHolder::DrawCompletedMessageType);
            }

            //! stop busy bar
            QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
            if(lineLayerStylePopup)
            {
                lineLayerStylePopup->setProperty("busyBarVisible", QVariant::fromValue(false));
                QMetaObject::invokeMethod(lineLayerStylePopup, "closePopup");
            }
            

        } 
        else if(messageType == *GIS::IOSGOperationQueueHolder::OperationQueueStatusChangedMessageType)
        {
            GIS::IOSGOperationQueueHolder::OperationQueueStatus operationQueueStatus = GIS::IOSGOperationQueueHolder::IDLE;
            CORE::IComponent* component = ELEMENTS::GetComponentFromMaintainer("TileLoaderComponent");
            if(component)
            {
                GIS::IOSGOperationQueueHolder* queueHolder = component->getInterface<GIS::IOSGOperationQueueHolder>();
                if(queueHolder)
                {
                    operationQueueStatus = queueHolder->getOperationQueueStatus();
                }  
            }

            if(operationQueueStatus == GIS::IOSGOperationQueueHolder::IDLE)
            {
                QObject* lineLayerStylePopup = _findChild("lineLayerStylePopup");
                if(lineLayerStylePopup)
                {
                    lineLayerStylePopup->setProperty("busyBarVisible", QVariant::fromValue(false));
                    if (_shouldClose)
                    {
                        QMetaObject::invokeMethod(lineLayerStylePopup, "closePopup");
                    }
                }
                _unsubscribe(component, *GIS::IOSGOperationQueueHolder::OperationQueueStatusChangedMessageType);
            }
        }

        DeclarativeFileGUI::update(messageType, message);
    }

    void LineLayerPropertiesGUI::_handlePopupOkButtonPressed()
    {
        _resetStyleOnClose = false;
        _shouldClose = true;
    }

    void LineLayerPropertiesGUI::popupClosed()
    {
        if(!_currentSelection.valid())
            return;

        _originalColor = Qt::black;
        _originalInnerColor = Qt::green;
        _originalOuterColor = Qt::red;
        _originalSingleLineIBColor = Qt::black;
        _originalLineWidth = 1;
        _currentSelection = NULL;
    }

    void LineLayerPropertiesGUI::browseButtonClicked(QString propertyValue,bool modelFile)
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption;
        QString filters;
        caption = "Browse for layer file";
        std::string label;
        std::string _lastPath;
        if(_lastPath.empty())
            _lastPath = directory.toStdString();
        if (modelFile)
        filters = "Model File (*.ive)";
        else
            filters = "Texture File (*.png)";
        
        QString files;
        
        QString fileName = QFileDialog::getOpenFileName(parent, caption, 
            _lastPath.c_str(), filters);

        QObject* addLayerPopup = NULL;

        addLayerPopup = _findChild("lineLayerStylePopup");

        if(addLayerPopup)
        {
            addLayerPopup->setProperty(propertyValue.toStdString().c_str(), QVariant::fromValue(fileName));
            //_originalGateLayerPath = fileName.toStdString();
        }
            
    }

}
