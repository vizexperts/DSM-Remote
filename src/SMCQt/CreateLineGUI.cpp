/****************************************************************************
*
* File             : CreateLineGUI.h
* Description      : CreateLineGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/CreateLineGUI.h>
#include <SMCQt/PropertyObject.h>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IDeletable.h>
#include <Core/ITerrain.h>
#include <Core/ILine.h>
#include <Core/IEditable.h>
#include <Core/IText.h>
#include <Core/IWorld.h>
#include <Core/ICompositeObject.h>
#include <Core/AttributeTypes.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <Elements/ElementsUtils.h>
#include <Elements/VizPlaceNode.h>

#include <Util/StringUtils.h>
#include <Util/StyleFileUtil.h>

#include <SMCElements/ILayerComponent.h>

#include <SMCUI/IFeatureExportUIHandler.h>

#include <iostream>

#include <QFileDialog>
#include <GIS/ILine.h>
#include <GIS/ITable.h>
#include <GIS/IFieldDefinition.h>
#include <GIS/IStringFieldDefinition.h>
#include <GIS/IDoubleFieldDefinition.h>
#include <GIS/FieldDefinitionType.h>
#include <Util/FileUtils.h>
#include <boost/algorithm/string.hpp>
#include <SMCQt/VizComboBoxElement.h>

#include <Elements/FeatureObject.h> 
#include <Elements/IAnnotationNodeQuery.h>
#include <serialization/StyleSheetParser.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateLineGUI);
    DEFINE_IREFERENCED(CreateLineGUI, VizQt::QtGUI);

    const std::string CreateLineGUI::LineContextualMenuObjectName = "lineContextual";

    CreateLineGUI::CreateLineGUI()
        : _clamping(true),
        _currLineNumber(1),
        _selectedFeatureLayer(NULL),
        _addToDefault(true),
        _fontDir("")
    {
    }

    CreateLineGUI::~CreateLineGUI()
    {
    }

    void CreateLineGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }
    }

    void CreateLineGUI::connectContextualMenu()
    {

        QObject* lineContextual = _findChild(LineContextualMenuObjectName);
        if(!lineContextual)
            return;

        QVariant temp = lineContextual->property("mouseButtonClicked");
        _mouseClickRight = (temp.toInt() == Qt::RightButton);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if(selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }

        if(_mouseClickRight)
        {

            // assuming there is a Font folder present in user's appdata/terrainDSM folder (should be already present in Appdata installer)
            if (_fontDir.empty())
            {
                UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
                std::string appdataPath = temporaryInstance->getAppFolderPath();
                _fontDir = appdataPath + "/Fonts";
            }

            // populates all the font name in _fontList
            _populateFontList();

            _populateContextualMenu();

            QObject* lineContextual = _findChild(LineContextualMenuObjectName);
            if(lineContextual != NULL)
            {
                //! connect to property change handlers
                QObject::connect(lineContextual, SIGNAL(rename(QString)), this, 
                    SLOT(rename(QString)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(setTextActive(bool)), this,
                    SLOT(setTextActive(bool)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(changeColor(QColor)), this,
                    SLOT(colorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(changeWidth(double)), this, 
                    SLOT(lineWidthChanged(double)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(deleteLine()), this,
                    SLOT(deleteLine()), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(exportLine()), this,
                    SLOT(exportLine()), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(browseButtonClicked()), this,
                    SLOT(handleBrowseButtonClicked()), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(changeTextSize(double)), this,
                    SLOT(changeTextSize(double)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(changeTextColor(QColor)), this,
                    SLOT(textColorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(outlineModeChanged(bool)), this,
                    SLOT(outlineChanged(bool)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(changeOutlineColor(QColor)), this,
                    SLOT(outlineColorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(billboardModeChanged(bool)), this,
                    SLOT(billboardChanged(bool)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(changeBillboardColor(QColor)), this,
                    SLOT(billboardColorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(fontChanged(QString)), this,
                    SLOT(fontChanged(QString)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(alignmentChanged(int)), this,
                    SLOT(alignmentChanged(int)), Qt::UniqueConnection);

                QObject::connect(lineContextual, SIGNAL(layoutChanged(int)), this,
                    SLOT(layoutChanged(int)), Qt::UniqueConnection);
            }
        }

        if(!_mouseClickRight)
        {
            showLineAttributes();
        }
    }

    void CreateLineGUI::disconnectContextualMenu()
    {
        connectEditVerticesMenu(false);
        _lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if(selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }

    }

    void CreateLineGUI::_populateContextualMenu()
    {
        QObject* lineContextual = _findChild(LineContextualMenuObjectName);
        if(lineContextual != NULL)
        {
            const CORE::ISelectionComponent::SelectionMap& selectionMap = _selectionUIHandler->getCurrentSelection();

            if(selectionMap.empty())
            {
                return;
            }

            CORE::ISelectable* selectable = selectionMap.begin()->second.get();
            if(selectable)
            {
                _selectedFeature = selectable->getInterface<GIS::IFeature>();
            }

            if(_selectedFeature.valid())
            {
                GIS::IGeometry* geometry = _selectedFeature->getGeometry();
                GIS::ILine* line = geometry->getInterface<GIS::ILine>();

                //! Name of the line
                QString name = QString::fromStdString(_selectedFeature->getInterface<CORE::IBase>()->getName());

                //! line width
                double lineWidth = 1;

                //! line color
                osg::Vec4 lineColor = osg::Vec4(0, 0, 0, 1);
                QColor qcolor(int(lineColor.r()*255),int(lineColor.g()*255),int(lineColor.b()*255),int(lineColor.a()*255));

                //! text active state
                bool textActive = false;

                lineContextual->setProperty("name", QVariant::fromValue(name));
                lineContextual->setProperty("lineWidth", QVariant::fromValue(lineWidth));
                lineContextual->setProperty("penColor", QVariant::fromValue(qcolor));                    
                lineContextual->setProperty("textActive", QVariant::fromValue(textActive));                 

                connectEditVerticesMenu(true);
                showLineAttributes();

                _lineUIHandler->setMode( SMCUI::ILineUIHandler::LINE_MODE_NO_OPERATION );
            }
            else
            {


                //Get the selected line
                CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
                if(!line.valid())
                {
                    return;
                }
               
                //Populate data for contextual tab

                //! Name of the line
                QString name = QString::fromStdString(line->getInterface<CORE::IBase>()->getName());

                //! line width
                double lineWidth = _lineUIHandler->getWidth();

                //! line color
                osg::Vec4 lineColor = _lineUIHandler->getColor();
                QColor qcolor(int(lineColor.r()*255),int(lineColor.g()*255),int(lineColor.b()*255),int(lineColor.a()*255));

                //! text active state
                bool textActive = line->getInterface<CORE::IText>()->getTextActive();

                //! font name
                std::string font = osgDB::getNameLessExtension(osgDB::getSimpleFileName(line->getInterface<CORE::IText>()->getFont()));

                //! layout id
                int layout = line->getInterface<CORE::IText>()->getLayout();

                //! alignmentt id
                int alignment = line->getInterface<CORE::IText>()->getAlignment();

                //! is outline set
                bool isOutline = line->getInterface<CORE::IText>()->isOutline();

                //! is billboard set
                bool isBillBoard = line->getInterface<CORE::IText>()->isBillboard();

                //! text Size
                double currentTextSize = 0.0;
                currentTextSize = line->getInterface<CORE::IText>()->getTextSize();

                //! text color
                osg::Vec4 textColor = line->getInterface<CORE::IText>()->getTextColor();
                QColor qtextColor(int(textColor.r() * 255), int(textColor.g() * 255), int(textColor.b() * 255), int(textColor.a() * 255));

                //! outline color
                osg::Vec4 outlineColor = line->getInterface<CORE::IText>()->getOutlineColor();
                QColor qoutlineColor(int(outlineColor.r() * 255), int(outlineColor.g() * 255), int(outlineColor.b() * 255), int(outlineColor.a() * 255));

                //! billboard color
                osg::Vec4 billBoardColor = line->getInterface<CORE::IText>()->getTextBillboardColor();
                QColor qbillboardColor(int(billBoardColor.r() * 255), int(billBoardColor.g() * 255), int(billBoardColor.b() * 255), int(billBoardColor.a() * 255));

                lineContextual->setProperty("name", QVariant::fromValue(name));
                lineContextual->setProperty("lineWidth", QVariant::fromValue(lineWidth));
                lineContextual->setProperty("penColor", QVariant::fromValue(qcolor));                    
                lineContextual->setProperty("textActive", QVariant::fromValue(textActive));                 

                lineContextual->setProperty("textColor", QVariant::fromValue(qtextColor));
                lineContextual->setProperty("textSize", QVariant::fromValue(currentTextSize));
                lineContextual->setProperty("outlineColor", QVariant::fromValue(qoutlineColor));
                lineContextual->setProperty("billboardColor", QVariant::fromValue(qbillboardColor));
                lineContextual->setProperty("outline", QVariant::fromValue(isOutline));
                lineContextual->setProperty("billboard", QVariant::fromValue(isBillBoard));
                lineContextual->setProperty("layoutId", QVariant::fromValue(layout));
                lineContextual->setProperty("alignmentId", QVariant::fromValue(alignment));
                lineContextual->setProperty("fontType", QString::fromStdString(font));

                connectEditVerticesMenu(true);
                showLineAttributes();
            }
        }
        //}
    }

    void CreateLineGUI::exportLine()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption = "Export Line";
        QString filters = "CSV File(*.csv)";

        QString fileName = QFileDialog::getSaveFileName(parent, caption, 
            directory, filters, 0);

        CORE::RefPtr<SMCUI::IFeatureExportUIHandler> featureExportuihandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IFeatureExportUIHandler>(getGUIManager());
        if(featureExportuihandler.valid())
        {
            featureExportuihandler->exportLineToCSV(fileName.toStdString());
        }

    }

    void CreateLineGUI::setTextActive(bool state)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(line.valid())
            {
                line->getInterface<CORE::IText>()->setTextActive(state);
            }
        }
    }

    void CreateLineGUI::connectSmpMenu(QString type)
    {
        if(type == "markingFeaturesMenu")
        {
            QObject* markingFeaturesMenu = _findChild("markingFeaturesMenu");
            if(markingFeaturesMenu != NULL)
            {
                QObject::connect(markingFeaturesMenu, SIGNAL(setLineEnable(bool)), this,
                    SLOT(setLineEnable(bool)), Qt::UniqueConnection);
            }
        }
    }

    void
        CreateLineGUI::_resetForDefaultHandling()
    {
        _addToDefault = true;

        setActive(false);

        _lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);
    }

    void CreateLineGUI::addLineToSelectedLayer(bool value)
    {
        if(value)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& map = 
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter = 
                map.begin();

            //XXX - using the first element in the selection
            if(iter != map.end())
            {
                _selectedFeatureLayer = iter->second->getInterface<CORE::IFeatureLayer>();
                _addToDefault = false;
            }

            setActive(true);
        }
        else
        {
            _resetForDefaultHandling();
        }
    }

    void 
        CreateLineGUI::_performHardReset()
    {
        _currLineNumber = 1;
        _clamping = true;
        setActive(false);
    }

    void CreateLineGUI::setLineEnable(bool value)
    {
        if(getActive() != value)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            if(selectionUIHandler.valid())
            {
                selectionUIHandler->clearCurrentSelection();
            }
        }

        setActive(value);
    }

    void CreateLineGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _loadLineUIHandler();

                //subscribing to world messages
                _subscribe(wmain, *CORE::IWorld::WorldLoadedMessageType);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType==*CORE::IWorld::WorldLoadedMessageType)
        {
            _performHardReset();
        }
        else if(messageType == *SMCUI::ILineUIHandler::LineCreatedMessageType)
        {
            CORE::RefPtr<CORE::ILine> line =  _lineUIHandler->getCurrentLine();
            if(line.valid())
            {
                if(line->getPointNumber() > 1)
                {
                    _populateAttributes();
                    _createMetadataRecord();

                    if(_addToDefault)
                    {
                        _lineUIHandler->addLineToDefaultLayer();
                    }
                    else
                    {
                        _lineUIHandler->addToLayer(_selectedFeatureLayer);
                    }

                    _lineUIHandler->_reset();

                    QObject* markingFeaturesMenu = _findChild("markingFeaturesMenu");
                    if ( markingFeaturesMenu )
                    {
                        QMetaObject::invokeMethod( markingFeaturesMenu, "setState",
                            Q_ARG( QVariant, QVariant( "Lines" ) ),
                            Q_ARG( QVariant, QVariant( false ) ) );

                        markingFeaturesMenu->setProperty( "selectedOption", "" );
                    }

                    QObject* lineLayerTab = _findChild("lineLayerTab");
                    if ( lineLayerTab )
                    {
                        QMetaObject::invokeMethod( lineLayerTab, "switchSelection");
                    }
                }
                else
                {
                    showError("Create Line", "Cannot create line with less than 2 points.", true);
                    _lineUIHandler->removeCreatedLine();
                }
            }
        }
        else if(messageType == *SMCUI::ILineUIHandler::LineUpdatedMessageType)
        {
            onVertexSelected();
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

    void CreateLineGUI::setClamping(bool clamping)
    {
        _clamping = clamping;
    }

    bool CreateLineGUI::getClamping() const
    {
        return _clamping;;
    }

    // XXX - Move these slots to a proper place
    void CreateLineGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        if(!_lineUIHandler.valid())
        {   
            return;
        }

        if(getActive() == value)
        {
            return;
        }

        if(value)
        {
            // line created by this GUI are clamped
            _lineUIHandler->setClampingState(_clamping);

            _lineUIHandler->setTemporaryState(true);
            _lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE);
            _lineUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _subscribe(_lineUIHandler.get(), *SMCUI::ILineUIHandler::LineCreatedMessageType);
        }
        else
        {
            _selectedFeature = NULL;
            // UIHandler is in Create_LIne_Mode --> User closed the menu before adding the line being created
            //--> Deleted the line being created
            if(_lineUIHandler->getMode() == SMCUI::ILineUIHandler::LINE_MODE_CREATE_LINE)
                _lineUIHandler->removeCreatedLine();

            //clear any selected lines if selected
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            selectionUIHandler->clearCurrentSelection();

            _lineUIHandler->setMode(SMCUI::ILineUIHandler::LINE_MODE_NONE);

            _lineUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            _unsubscribe(_lineUIHandler.get(), *SMCUI::ILineUIHandler::LineCreatedMessageType);
        }
        QtGUI::setActive(value);
    }

    void CreateLineGUI::_populateFontList()
    {
        _fontsList.clear();

        std::string path = osgDB::convertFileNameToNativeStyle(_fontDir);
        if (!osgDB::fileExists(path))
        {
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            if (boost::iequals(osgDB::getFileExtension(currFileName), "ttf"))
            {
                _fontsList.push_back(osgDB::getNameLessExtension(currFileName));
            }
        }

        int count = 1;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select font", "0"));

        for (int i = 0; i < _fontsList.size(); i++)
        {
            std::string georbISServerName = _fontsList[i];
            comboBoxList.append(new VizComboBoxElement(georbISServerName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("fontsList", QVariant::fromValue(comboBoxList));

    }

    void CreateLineGUI::handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption = "Font Folder";

        QString dirPath = QFileDialog::getExistingDirectory(parent, caption, directory);
        _fontDir = dirPath.toStdString();
        _populateFontList();
    }

    void CreateLineGUI::fontChanged(QString font)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
                return;

            if (font.isEmpty() || font == "Select font")
                return;

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if (line.valid())
            {
                CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                if (text.valid())
                {
                    font += ".ttf";
                    text->setFont(_fontDir + "\\" + font.toStdString());
                }
            }
        }
    }

    void CreateLineGUI::changeTextSize(double textSize)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
                return;

            if (textSize <= 0)
                return;

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if (line.valid())
            {
                CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setTextSize(textSize);
                }
            }
        }
    }

    void CreateLineGUI::textColorChanged(QColor color)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
                return;

            if (color.isValid())
            {
                osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

                CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
                if (line.valid())
                {
                    CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                    if (text.valid())
                    {
                        osg::Vec4  currentColor = text->getTextColor();
                        if (penColor != currentColor)
                        {
                            text->setTextColor(penColor);
                        }
                    }
                }
            }
        }
    }

    void CreateLineGUI::outlineChanged(bool value)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if (line.valid())
            {
                CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setOutline(value);
                }
            }
        }
    }

    void CreateLineGUI::outlineColorChanged(QColor color)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
                return;

            if (color.isValid())
            {
                osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

                CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
                if (line.valid())
                {
                    CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                    if (text.valid())
                    {
                        osg::Vec4  currentColor = text->getOutlineColor();
                        if (penColor != currentColor)
                        {
                            text->setOutlineColor(penColor);
                        }
                    }
                }
            }
        }
    }

    void CreateLineGUI::layoutChanged(int layoutId)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if (line.valid())
            {
                CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setLayout(static_cast<CORE::IText::Layout>(layoutId));
                }
            }
        }
    }

    void CreateLineGUI::alignmentChanged(int alignmentId)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if (line.valid())
            {
                CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setAlignment(static_cast<CORE::IText::Alignment>(alignmentId));
                }
            }
        }
    }

    void CreateLineGUI::billboardChanged(bool value)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if (line.valid())
            {
                CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setBillboard(value);
                }
            }
        }
    }


    void CreateLineGUI::billboardColorChanged(QColor color)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_lineUIHandler.valid())
                return;

            if (color.isValid())
            {
                osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

                CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
                if (line.valid())
                {
                    CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
                    if (text.valid())
                    {
                        osg::Vec4  currentColor = text->getTextBillboardColor();
                        if (penColor != currentColor)
                        {
                            text->setTextBillboardColor(penColor);
                        }
                    }
                }
            }
        }
    }

    void CreateLineGUI::colorChanged(QColor color)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(color.isValid())
            {
                osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);
                if(penColor != _lineUIHandler->getColor())
                {
                    _lineUIHandler->setColor(penColor);
                }
            }
        }
    }

    void CreateLineGUI::deleteLine()
    {
        if(_selectedFeature.valid())
        {
            CORE::IDeletable* deletable = _selectedFeature->getInterface<CORE::IDeletable>();
            if(!deletable)
            {
                return;
            }

            CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
            if(deletionUIHandler.valid())
            {
                deletionUIHandler->deleteObject(deletable);
            }
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(!line.valid())
            {
                return;
            }

            CORE::IDeletable* deletable = line->getInterface<CORE::IDeletable>();
            if(!deletable)
            {
                return;
            }

            CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler = 
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
            if(deletionUIHandler.valid())
            {
                deletionUIHandler->deleteObject(deletable);
            }
        }
    }

    void CreateLineGUI::rename(QString newName)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            if ( newName == "" )
            {
                showError("Edit Line Name", "Line name cannot be blank.", true);

                //                {
                QObject* lineContextual = _findChild(LineContextualMenuObjectName);
                if( lineContextual != NULL)
                {
                    CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
                    std::string oldName = line->getInterface<CORE::IBase>()->getName();

                    lineContextual->setProperty("name", QVariant::fromValue( QString::fromStdString(oldName) ));
                }
                //              }
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(line.valid())
            {
                line->getInterface<CORE::IBase>()->setName(newName.toStdString());
            }
        }
    }

    void CreateLineGUI::lineWidthChanged(double lineWidth)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if((lineWidth != _lineUIHandler->getWidth()) && (lineWidth != 0) )
            {
                _lineUIHandler->setWidth(lineWidth);
            }
        }
    }

    void CreateLineGUI::setEditMode( bool mode )
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            if ( mode )
            {
                _lineUIHandler->setMode( SMCUI::ILineUIHandler::LINE_MODE_EDIT );
            }
            else
            {
                QObject* editLineVertices = _findChild("editLineVertices");
                if(editLineVertices != NULL)
                {
                    editLineVertices->setProperty("enableDelete", QVariant::fromValue(false));
                    editLineVertices->setProperty("editable", QVariant::fromValue( false ));
                }

                _lineUIHandler->setMode( SMCUI::ILineUIHandler::LINE_MODE_NO_OPERATION );
            }
        }
    }

    void CreateLineGUI::connectEditVerticesMenu(bool value)
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(!line.valid())
            {
                return;
            }

            if(value)
            {

                QObject* editLineVertices = _findChild("editLineVertices");
                if(editLineVertices != NULL)
                {
                    QString latitude = "";
                    QString longitude = "";

                    editLineVertices->setProperty("latitude", QVariant::fromValue(latitude));
                    editLineVertices->setProperty("longitude", QVariant::fromValue(longitude));
                    editLineVertices->setProperty("editable", QVariant::fromValue(false));

                    QObject::connect(editLineVertices, SIGNAL(deleteVertex()),
                        this, SLOT(deleteVertex()), Qt::UniqueConnection);

                    QObject::connect(editLineVertices, SIGNAL(handleLatChanged()),
                        this, SLOT(handleLatChanged()), Qt::UniqueConnection);

                    QObject::connect(editLineVertices, SIGNAL(handleLongChanged()),
                        this, SLOT(handleLongChanged()), Qt::UniqueConnection);

                    QObject::connect(editLineVertices, SIGNAL(setEditMode( bool )),
                        this, SLOT(setEditMode( bool )), Qt::UniqueConnection);

                    _subscribe(_lineUIHandler.get(), *SMCUI::ILineUIHandler::LineUpdatedMessageType);
                }
            }
            else
            {
                _unsubscribe(_lineUIHandler.get(), *SMCUI::ILineUIHandler::LineUpdatedMessageType);
            }
        }
    }

    void CreateLineGUI::onVertexSelected( )
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(!line.valid())
            {
                return;
            }

            std::vector<int> selectedPointList = _lineUIHandler->getSelectedVertexList();

            if ( selectedPointList.size() == 0 )
            {
                // disable the delete button
                QObject* editLineVertices = _findChild("editLineVertices");
                if(editLineVertices != NULL)
                {
                    QString latitude = "";
                    QString longitude = "";

                    editLineVertices->setProperty("latitude", QVariant::fromValue(latitude));
                    editLineVertices->setProperty("longitude", QVariant::fromValue(longitude));
                    editLineVertices->setProperty("editable", QVariant::fromValue(false));
                    editLineVertices->setProperty("enableDelete", QVariant::fromValue(false));
                }
            }
            else if ( selectedPointList.size() == 1 )
            {
                // update the ui
                int currentSelectedPoint = selectedPointList[0];

                if( currentSelectedPoint > line->getPointNumber()-1)
                {
                    return;
                }

                osg::Vec3 position = line->getValueAt( currentSelectedPoint );

                QObject* editLineVertices = _findChild("editLineVertices");
                if(editLineVertices != NULL)
                {
                    QString latitude =QString::number( position.y() );
                    QString longitude = QString::number( position.x() );

                    editLineVertices->setProperty("latitude", QVariant::fromValue(latitude));
                    editLineVertices->setProperty("longitude", QVariant::fromValue(longitude));
                    editLineVertices->setProperty("editable", QVariant::fromValue(true));
                    editLineVertices->setProperty("enableDelete", QVariant::fromValue(true));
                }
            }
            else
            {
                // disable the ui

                QObject* editLineVertices = _findChild("editLineVertices");
                if(editLineVertices != NULL)
                {
                    QString latitude = "";
                    QString longitude = "";

                    editLineVertices->setProperty("latitude", QVariant::fromValue(latitude));
                    editLineVertices->setProperty("longitude", QVariant::fromValue(longitude));
                    editLineVertices->setProperty("editable", QVariant::fromValue(false));
                }
            }
        }
    }

    void CreateLineGUI::deleteVertex()
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            std::vector<int> selectedPointList = _lineUIHandler->getSelectedVertexList();
            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(!line.valid())
            {
                return;
            }

            if ( selectedPointList.size()+2 > line->getPointNumber() )
            {
                // a line should have atleast two points.
                showError("Delete Points", "A line should have atleast two points. Please unselect some points.", true);
                return;
            }

            // sort , reverse, and update the global selectedPointList 
            std::sort( selectedPointList.begin(), selectedPointList.end() );
            std::reverse( selectedPointList.begin(), selectedPointList.end() );
            _lineUIHandler->setSelectedVertexList( selectedPointList );

            bool flag = false;

            while ( selectedPointList.size() != 0 )
            {
                flag = _lineUIHandler->_deletePointAtIndex(selectedPointList[0]);

                if ( !flag )
                    break;

                selectedPointList = _lineUIHandler->getSelectedVertexList();
            }
        }
    }

    void CreateLineGUI::handleLatChanged()
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(!line.valid())
            {
                return;
            }

            std::vector<int> selectedPointList = _lineUIHandler->getSelectedVertexList();
            int currentSelectedPoint = selectedPointList[0];

            if(currentSelectedPoint > line->getPointNumber()-1)
            {
                return;
            }

            QObject* editLineVertices = _findChild("editLineVertices");
            if(editLineVertices != NULL)
            {
                osg::Vec3 position = line->getValueAt( currentSelectedPoint );
                QString latitude = editLineVertices->property("latitude").toString();

                if ( latitude == "" )
                {
                    showError("Edit Latitude", "Latitude field cannont be empty.", true);
                    editLineVertices->setProperty( "latitude", position.y() );
                    return;
                }

                double latitudeD = latitude.toDouble();
                double longitudeD = position.x();
                double altitudeD = ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD);

                position = osg::Vec3(longitudeD, latitudeD, altitudeD);
                _lineUIHandler->_movePointAtIndex( currentSelectedPoint, position);
            }
        }
    }

    void CreateLineGUI::handleLongChanged()
    {
        if(_selectedFeature.valid())
        {
        }
        else
        {
            if(!_lineUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(!line.valid())
            {
                return;
            }

            std::vector<int> selectedPointList = _lineUIHandler->getSelectedVertexList();
            int currentSelectedPoint = selectedPointList[0];

            if( currentSelectedPoint > line->getPointNumber()-1)
            {
                return;
            }

            QObject* editLineVertices = _findChild("editLineVertices");
            if(editLineVertices != NULL)
            {
                osg::Vec3 position = line->getValueAt( currentSelectedPoint);

                QString longitude = editLineVertices->property("longitude").toString();
                if ( longitude == "" )
                {
                    showError("Edit Longitude", "Longitude field cannont be empty.", true);
                    editLineVertices->setProperty( "longitude", position.x() );
                    return;
                }

                double latitudeD = position.y();
                double longitudeD = longitude.toDouble();
                double altitudeD = ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD);

                position = osg::Vec3(longitudeD, latitudeD, altitudeD);
                _lineUIHandler->_movePointAtIndex( currentSelectedPoint, position);
            }
        }
    }

    void CreateLineGUI::_loadLineUIHandler()
    {
        _lineUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILineUIHandler>(getGUIManager());
        _selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
    }

    void CreateLineGUI::_populateAttributes()
    {
        std::string name = "Line " + UTIL::ToString<int>(_currLineNumber);

        CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();   
        if(!line.valid())
        {
            return;
        }

        line->getInterface<CORE::IBase>()->setName(name);
        ++_currLineNumber;
    }

    void CreateLineGUI::_createMetadataRecord()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = NULL;
        if(_addToDefault)
        {
            tableDefn = 
                _lineUIHandler->getCurrentSelectedLayerDefn();
        }
        else
        {
            try
            {
                if(_selectedFeatureLayer.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTableHolder> holder = 
                        _selectedFeatureLayer->getInterface<CORE::IMetadataTableHolder>(true);

                    if(holder.valid())
                    {
                        tableDefn = holder->getMetadataTable()->getMetadataTableDefn();
                    }
                }
            }
            catch(UTIL::Exception &e)
            {
                e.LogException();
            }
        }

        if(!tableDefn.valid())
        {
            LOG_ERROR("Invalid IMetadataTableDefn instance");
            return;
        }

        //create metadata record for the min max point
        CORE::RefPtr<CORE::IMetadataRecord> record = 
            metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        CORE::RefPtr<CORE::IMetadataField> field = 
            record->getFieldByName("NAME");
        if(field)
        {
            CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
            if(line != NULL)
            {
                field->fromString(line->getInterface<CORE::IBase>()->getName());
            }
        }

        _lineUIHandler->setMetadataRecord(record.get());
    }

    void
        CreateLineGUI::showLineAttributes()
    {
        if(_selectedFeature.valid())
        {
            GIS::ITable* table = _selectedFeature->getTable();
            const GIS::ITable::IFieldDefinitionList& definitionList = table->getFieldDefinitionList();

            int tableColumnCount = definitionList.size();

            _attributeList.clear();

            bool isDescriptionPresent = false;
            QString description;


            if(!_mouseClickRight)
            {
                QString name = QString::fromStdString(_selectedFeature->getInterface<CORE::IBase>()->getName());
                _attributeList.append(new PropertyObject("Name ", "String", name, name.length()));
            }


            for(int i = 0 ; i < tableColumnCount; i++)
            {
                const GIS::Field* field = _selectedFeature->getField(i);
                if(field)
                {
                    CORE::RefPtr<GIS::IFieldDefinition> fieldDefn = definitionList[i].get();
                    if(fieldDefn.valid())
                    {
                        const GIS::FieldDefinitionType& type = fieldDefn->getFieldDefinitionType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName = fieldName.trimmed();

                        if((fieldName == "Description"))
                        {
                            description = QString::fromStdString(field->getAsString());
                            if(description.contains("<html>"))
                            {
                                isDescriptionPresent = true;
                                continue;
                            }
                        }
                        else if(fieldName.contains("_VIZ_", Qt::CaseInsensitive))
                        {
                            continue;
                        }
                        else if(fieldName.compare("name", Qt::CaseInsensitive) == 0)
                        {
                            continue;
                        }
                        else if(fieldName.isEmpty())
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecision = 1;

                        if (type == GIS::StringFieldDefinitionType)
                        {
                            fieldType = "String";
                            fieldValue = QString::fromStdString(field->getAsString());
                            fieldPrecision = fieldDefn->getInterface<GIS::IStringFieldDefinition>()->getLength();
                        }
                        else if(type == GIS::IntegerFieldDefinitionType)
                        {
                            fieldType = "Integer";
                            fieldValue = QString::number(field->getAsInt32());
                        }
                        else if(type == GIS::DoubleFieldDefinitionType)
                        {
                            fieldType = "Double";
                            fieldValue = QString::number(field->getAsReal64());
                            fieldPrecision = fieldDefn->getInterface<GIS::IDoubleFieldDefinition>()->getPrecision();
                        }

                        _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecision));
                    }
                }
            }

            if(_attributeList.empty())
            {
                QObject* lineContextual = _findChild(LineContextualMenuObjectName);
                if(lineContextual)
                {
                    lineContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("lineAttributes");
            if(showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                //            if(isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this, 
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);
            }
        }
        else
        {
            CORE::RefPtr<CORE::IMetadataRecord> tableRecord = 
                _lineUIHandler->getMetadataRecord();

            if(!tableRecord.valid())
            {
                return;
            }

            _attributeList.clear();

            bool isDescriptionPresent = false;
            QString description;

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
            if(!tableDefn.valid())
                return;

            int tableColumnCount = tableDefn->getFieldCount();

            if(!_mouseClickRight)
            {
                CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
                if(!line.valid())
                {
                    return;
                }

                //Populate data for contextual tab
                std::string uniqueId = line->getInterface<CORE::IBase>()->getUniqueID().toString() + ".json";
                std::string objectFolderLocation = UTIL::StyleFileUtil::getObjectFolder();
                jsonFile = objectFolderLocation + "/" + uniqueId;
                //! Name of the line
                QString name = QString::fromStdString(line->getInterface<CORE::IBase>()->getName());
                _attributeList.append(new PropertyObject("Name ", "String", name, name.length()));
                std::string jsonContent;
                if (osgDB::fileExists(jsonFile))
                {
                    std::ifstream inFile(jsonFile.c_str());
                    if (!inFile.is_open())
                        return;
                    jsonContent.assign((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());
                }
                QObject *lineContextual = _findChild("lineContextual");
                if (lineContextual)
                {
                    lineContextual->setProperty("jsonString", QString::fromStdString(jsonContent));

                    std::string jsonSchemaFile = "../../data/Styles/StyleSchema.json";
                    QFileInfo schemafileInfo(jsonSchemaFile.c_str());
                    QString strFilePath = schemafileInfo.absoluteFilePath();
                    std::string schemaPath = strFilePath.toStdString();
                    std::ifstream schemaFile(schemaPath.c_str());
                    if (!schemaFile.is_open())
                        return;

                    std::string schemaData((std::istreambuf_iterator<char>(schemaFile)), std::istreambuf_iterator<char>());
                    lineContextual->setProperty("jsonSchemaContent", QString::fromStdString(schemaData));
                }
            }

            for(int i = 0 ; i < tableColumnCount; i++)
            {
                CORE::RefPtr<CORE::IMetadataField> field = tableRecord->getFieldByIndex(i);
                if(field)
                {
                    CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = field->getMetadataFieldDef();
                    if(fieldDefn.valid())
                    {
                        CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName =  fieldName.trimmed();

                        if((fieldName == "Description"))
                        {
                            description = QString::fromStdString(field->toString());
                            if(description.contains("<html>"))
                            {
                                isDescriptionPresent = true;
                                continue;
                            }
                        }
                        else if(fieldName.contains("_VIZ_", Qt::CaseInsensitive))
                        {
                            continue;
                        }
                        else if(fieldName.compare("name", Qt::CaseInsensitive) == 0)
                        {
                            continue;
                        }
                        else if(fieldName.isEmpty())
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecesion = 0;

                        switch (type)
                        {
                        case CORE::IMetadataFieldDefn::STRING:
                            {
                                fieldType = "String";
                                fieldValue = QString::fromStdString(field->toString());
                                fieldPrecesion = fieldDefn->getLength();
                            }
                            break;
                        case CORE::IMetadataFieldDefn::INTEGER:
                            {
                                fieldType = "Integer";
                                fieldValue = QString::number(field->toInt());
                            }
                            break;
                        case CORE::IMetadataFieldDefn::DOUBLE:
                            {
                                fieldType = "Double";
                                fieldValue = QString::number(field->toDouble());
                                fieldPrecesion = fieldDefn->getPrecision();
                            }
                            break;
                        }

                        _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecesion));
                    }
                }
            }

            if(_attributeList.empty())
            {
                QObject* lineContextual = _findChild(LineContextualMenuObjectName);
                if(lineContextual)
                {
                    lineContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("lineAttributes");
            if(showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                //            if(isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this, 
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);
            }
        }
    }

    void CreateLineGUI::handleAttributesCancelButtonPressed()
    {
        showLineAttributes();

        if(_selectedFeature.valid())
        {
            _selectionUIHandler->clearCurrentSelection();
        }
    }

    void CreateLineGUI::handleAttributesOkButtonPressed()
    {
        if(_selectedFeature.valid())
        {
            _selectionUIHandler->clearCurrentSelection();
        }
        else
        {
            CORE::RefPtr<CORE::IMetadataRecord> tableRecord = 
                _lineUIHandler->getMetadataRecord();

            if(!tableRecord.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
            if(!tableDefn.valid())
                return;

            int tableColumnCount = tableDefn->getFieldCount();

            foreach(QObject* qobj, _attributeList)
            {
                PropertyObject* propObj = qobject_cast<PropertyObject*>(qobj);
                if(propObj)
                {
                    CORE::RefPtr<CORE::IMetadataField> field = 
                        tableRecord->getFieldByName(propObj->name().toStdString());
                    if(field.valid())
                    {
                        CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = 
                            field->getMetadataFieldDef();

                        CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                        switch (type)
                        {
                        case CORE::IMetadataFieldDefn::INTEGER:
                            {
                                field->fromInt(propObj->value().toInt());
                            }
                            break;
                        case CORE::IMetadataFieldDefn::DOUBLE:
                            {
                                field->fromDouble(propObj->value().toDouble());
                            }
                            break;
                        case CORE::IMetadataFieldDefn::STRING:
                            {
                                field->fromString(propObj->value().toStdString());
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    void CreateLineGUI::handleStyleApplyButton()
    {
        std::string currentSelectedJsonFile = jsonFile;
        std::ofstream outfile(currentSelectedJsonFile.c_str());

        QObject* templatelistPopup = _findChild("lineContextual");
        if (templatelistPopup != NULL)
        {
            std::string outString = templatelistPopup->property("jsonOutput").toString().toStdString();
            outfile << outString;
        }
        outfile.close();
        CORE::RefPtr<CORE::ILine> line = _lineUIHandler->getCurrentLine();
        CORE::RefPtr<SYMBOLOGY::IStyle> styleFile = DB::StyleSheetParser::readStyle(jsonFile);
        ELEMENTS::FeatureObject* featureNodeHolder = dynamic_cast<ELEMENTS::FeatureObject*>(line->getInterface<CORE::IObject>());
        if (featureNodeHolder != nullptr)
        {
            featureNodeHolder->setVizPlaceNodeStyle(styleFile);
            featureNodeHolder->setBillboard(true);

        }
    }

    void CreateLineGUI::handleFontButton()
    {
        QString directory = "c:/";
        QString caption = "Font";

        // populating the font list again as per new font directory
        QString filters = "Font (*.ttf *.TTF)";
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filepath = QFileDialog::getOpenFileName(parent, caption, directory, filters);

        std::string fileChosen = osgDB::getSimpleFileName(filepath.toStdString());
        if (fileChosen != "")
        {
            QObject *lineContextual = _findChild("lineContextual");
            if (lineContextual)
            {
                lineContextual->setProperty("fontString", QString::fromStdString(fileChosen));
            }
        }
    }

} // namespace SMCQt

