/****************************************************************************
 *
 * File             : NetworkPublishGUI.h
 * Description      : NetworkPublishGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCQt/NetworkPublishGUI.h>

#include <App/IApplication.h>

#include <VizQt/NameValidator.h>

#include <HLA/NetworkCollaborationManager.h>

#include <Core/AttributeTypes.h>

#include <QLineEdit>
#include <QPushButton>
#include <QDialog>
#include <QMessageBox>
#include <QLabel>
#include <QProgressBar>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, NetworkPublishGUI);
    DEFINE_IREFERENCED(NetworkPublishGUI, VizQt::LayoutFileGUI);

    const std::string NetworkPublishGUI::LEName="LEName";
    const std::string NetworkPublishGUI::PBPublish="PBPublish";
    const std::string NetworkPublishGUI::PBCancel="PBCancel";
    const std::string NetworkPublishGUI::PBResign="PBResign";
    const std::string NetworkPublishGUI::LBMessage="LBMessage";
    const std::string NetworkPublishGUI::PBProgress="PBProgress";
    
    NetworkPublishGUI::NetworkPublishGUI()
        : _published(false)
        , _destroying(false)
    {
    }

    NetworkPublishGUI::~NetworkPublishGUI()
    {
        setActive(false);
    }

    void 
    NetworkPublishGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {   
                APP::IManager* mgr = getGUIManager()->getInterface<APP::IManager>();
                mgr = mgr->getApplication()->getManagerByName(HLA::NetworkCollaborationManager::ClassNameStr);
                if(mgr)
                {
                    _netwrkCollaborationMgr = mgr->getInterface<HLA::INetworkCollaborationManager>();
                    if(!_netwrkCollaborationMgr.valid())
                    {
                        setComplete(false);
                    }

                    if(_syncMode.compare("Event")==0)
                    {
                        _netwrkCollaborationMgr->setSynchronizationMode(HLA::IHLAComponent::Events);
                    }
                    else
                    {
                        _netwrkCollaborationMgr->setSynchronizationMode(HLA::IHLAComponent::Objects);
                    }
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ProjectDestroyedMessageType)
        {
            QMessageBox::information(_parent, "Success", "Project Destroyed Successfully.");
            _published=false;
            QPushButton *pb = _getChild<QPushButton>(PBPublish);
            QLineEdit* le = _getChild<QLineEdit>(LEName);
            le->setEnabled(true);
            le->setText("");
            pb->setEnabled("true");
            pb->setText("Publish");
            _destroying = false;
            _getChild<QLabel>(LBMessage)->hide();
            _getChild<QProgressBar>(PBProgress)->hide();
            _getChild<QPushButton>(PBResign)->hide();            
            _unsubscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ProjectDestroyedMessageType);
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ProjectResignRequestRejectedMessageType)
        {
            emit showError("Error Destroying Project", "Joined members denied the request.");
            _getChild<QPushButton>(PBPublish)->setEnabled(true);
            _getChild<QPushButton>(PBResign)->hide();
        }
    }

    void 
    NetworkPublishGUI::setSyncMode(const std::string &mode)
    {
        _syncMode = mode;
    }

    std::string 
    NetworkPublishGUI::getSyncMode()
    {
        return _syncMode;
    }

    void 
    NetworkPublishGUI::initializeAttributes()
    {
        LayoutFileGUI::initializeAttributes();

        _addWidgetName(LEName);
        _addWidgetName(PBPublish);
        _addWidgetName(PBCancel);
        _addWidgetName(PBResign);
        _addWidgetName(LBMessage);
        _addWidgetName(PBProgress);

        _addAttribute(new CORE::StringAttribute("Mode", "Mode",
            CORE::StringAttribute::SetFuncType(this, &NetworkPublishGUI::setSyncMode),
            CORE::StringAttribute::GetFuncType(this, &NetworkPublishGUI::getSyncMode),
            "Sync Mode Setting",
            "Sync Mode Setting"));
    }

    void 
    NetworkPublishGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }

        _parent->show();
        _parent->activateWindow ();
        _parent->raise();
        _parent->setFocus(Qt::OtherFocusReason);

        // If value is set properly then set focus for area UI handler
        // Check whether UI Handler is valid or not
        if(!getComplete())
        {
            return;
        }

        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                if(!_published)
                {
                    _getChild<QLineEdit>(LEName)->setText("");
                    _getChild<QPushButton>(PBResign)->hide();
                }
                else 
                {
                    //_getChild<QPushButton>(PBResign)->show();
                    //_getChild<QPushButton>(PBResign)->setEnabled(true);
                }

                _getChild<QLabel>(LBMessage)->hide();
                _getChild<QProgressBar>(PBProgress)->hide();

                _cleanup = false;
            }
            else
            {
                _handleCancelButtonClicked();
            }
            LayoutFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void 
    NetworkPublishGUI::_handlePublishButtonClicked()
    {
        QPushButton *pb = _getChild<QPushButton>(PBPublish);
        QLineEdit* le = _getChild<QLineEdit>(LEName);
        QPushButton* pbResign = _getChild<QPushButton>(PBResign);

        if(pb->text().contains("Publish"))
        {
            if(!le->text().isEmpty())
            {
                std::string projectName = le->text().toStdString();
                std::string errMsg;
                if(_netwrkCollaborationMgr->publishProject(projectName, errMsg))
                {
                    QMessageBox::information(_parent, "Success", "Project Published Successfully.");
                    _published=true;
                    le->setEnabled(false);
                    pb->setText("Destroy");
                    //pbResign->setEnabled(true);
                    //pbResign->show();
                }
                else
                    QMessageBox::information(_parent, "Failure", "Project Failed to Publish.\n Possible Reason : " + QString::fromStdString(errMsg));                
            }
            else 
                QMessageBox::information(_parent, "Failure", "Project Name is empty.");
        }
        else if(pb->text().contains("Destroy"))
        {
            std::string errMsg;
            if(_netwrkCollaborationMgr->requestProjectDestruction(errMsg))
            {
                _destroying = true;
                pb->setEnabled(false);
                _getChild<QLabel>(LBMessage)->show();
                _getChild<QProgressBar>(PBProgress)->show();

                _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ProjectDestroyedMessageType);
                _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ProjectResignRequestRejectedMessageType);
            }
            else
                QMessageBox::information(_parent, "Failure", "Request for destruction can't be made.\n Possible Reason : " + QString::fromStdString(errMsg));            
        }
    }

    void 
    NetworkPublishGUI::_handleCancelButtonClicked()
    {
        if(!_cleanup)
        {
            if(_destroying)
            {
                _netwrkCollaborationMgr->cancelProjectDestructionRequest();
                _getChild<QLabel>(LBMessage)->hide();
                _getChild<QProgressBar>(PBProgress)->hide();
                _destroying=false;
            }          
            _cleanup = true;
            qobject_cast<QDialog *>(_parent)->close();
        }
    }

    void
    NetworkPublishGUI::_handleResignButtonClicked()
    {
        if(_destroying)
        {
            _netwrkCollaborationMgr->cancelProjectDestructionRequest();
            _getChild<QLabel>(LBMessage)->hide();
            _getChild<QProgressBar>(PBProgress)->hide();
            _destroying=false;
        }

        _getChild<QPushButton>(PBPublish)->setEnabled(false);
        _getChild<QPushButton>(PBCancel)->setEnabled(false);
        _getChild<QPushButton>(PBResign)->setEnabled(false);
        
        std::string errMsg;

        if(_netwrkCollaborationMgr->resignProject(errMsg))
        {
            QMessageBox::information(_parent, "Success", "Project Resigned Successfully.");
            _published=false;
            QPushButton *pb = _getChild<QPushButton>(PBPublish);
            QLineEdit* le = _getChild<QLineEdit>(LEName);
            le->setEnabled(true);
            le->setText("");
            pb->setEnabled("true");
            pb->setText("Publish");
            _getChild<QPushButton>(PBResign)->hide();  
        }
        else
        {
            QMessageBox::information(_parent, "Failure", "Project Failed to Resign.\n Possible Reason : " + QString::fromStdString(errMsg));
            _getChild<QPushButton>(PBPublish)->setEnabled(true);            
            _getChild<QPushButton>(PBResign)->setEnabled(true);
        }
    }

    void 
    NetworkPublishGUI::_loadAndSubscribeSlots()
    {
        LayoutFileGUI::_loadAndSubscribeSlots();
        if(!getComplete())
        {
            return;
        }

        _getChild<QLineEdit>(LEName)->setValidator(new VizQt::NameValidator());
        _connect(PBPublish, SIGNAL(clicked()), SLOT(_handlePublishButtonClicked()));
        _connect(PBCancel, SIGNAL(clicked()), SLOT(_handleCancelButtonClicked()));
        // Disabling Resign as this brings confusion in the workflow. Keeping Workflow Simple.
        // The publisher application can only destroy the project and other joined applications can resign
        //_connect(PBResign, SIGNAL(clicked()), SLOT(_handleResignButtonClicked()));
    }
}
