/****************************************************************************
*
* File             : ExtractROIGUI.cpp
* Description      : ExtractROIGUI class definition
*
*****************************************************************************
* Copyright (c) 2010-2011, VizExperts india Pvt. Ltd.                                       
*****************************************************************************/

#include <SMCQt/ExtractROIGUI.h>
#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>
#include <Core/IVisitorFactory.h>
#include <SMCUI/AddContentUIHandler.h>
#include <osgDB/FileNameUtils>
#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/IUIHandlerManager.h>
#include <App/ApplicationRegistry.h>
#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>
#include <Util/Log.h>
#include <Util/StringUtils.h>
#include <VizQt/GUI.h>
#include <QFileDialog>
#include <QtCore/QFile>
#include <QtUiTools/QtUiTools>
#include <iostream>
#include <gdal.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ExtractROIGUI)
        DEFINE_IREFERENCED(ExtractROIGUI, CORE::Base)

        ExtractROIGUI::ExtractROIGUI()
        :_stereoWizard(NULL)
    {
    }

    ExtractROIGUI::~ExtractROIGUI()
    {
    }

    // to load the connect and disconnect slot from the qml
    void ExtractROIGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }

        QObject::connect(this,SIGNAL(_updateGUIStatus()),this,SLOT(_handleGUIStatus()),Qt::QueuedConnection);
    }


    void 
        ExtractROIGUI::_handleCancelButtonClicked()
    {
        _extractROI = _findChild("imagesubset");
        if(_extractROI)
        {
            _extractROI->setProperty("calculating",false);
            _extractROI->setProperty("okButtonEnabled",true);
            _extractROI->setProperty("inputImagePath","");
            _extractROI->setProperty("outputImagePath","");
            _extractROI->setProperty("startX","");
            _extractROI->setProperty("startY","");
            _extractROI->setProperty("sizeX","");
            _extractROI->setProperty("sizeY","");

        }
    }


    void 
        ExtractROIGUI::connectPopupLoader(QString type)
    {
        if(type == "imagesubset")
        {
            setActive( true );
            _extractROI = _findChild("imagesubset");
            if(_extractROI != NULL)
            {
                setActive( true );
                QObject::connect(_extractROI, SIGNAL(browseInputImage()),
                    this, SLOT(_handleInputBrowseButtonClicked()), Qt::UniqueConnection);

                QObject::connect(_extractROI, SIGNAL(browseOutputImage()), 
                    this, SLOT(_handleoutputBrowseButtonClicked()), Qt::UniqueConnection);

                QObject::connect(_extractROI, SIGNAL(viewImage(QString)),
                    this, SLOT(_viewImage(QString)),Qt::QueuedConnection);

                QObject::connect(_extractROI, SIGNAL(calculate()), 
                    this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);

                QObject::connect(_extractROI, SIGNAL(cancelButtonClicked()), 
                    this, SLOT(_handleCancelButtonClicked()), Qt::UniqueConnection);

            }
        }
    }


    void 
        ExtractROIGUI::_handleInputBrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            _extractROI = _findChild("imagesubset");
            if(_extractROI != NULL)
            {
                _extractROI->setProperty("inputImagePath",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }

    }






    // to browse and save output image
    void 
        ExtractROIGUI::_handleoutputBrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}


        std::string formatFilter = "Image ( *.tif )";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getSaveFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);


        if(!filename.isEmpty())
        {
            _extractROI = _findChild("imagesubset");
            if(_extractROI != NULL)
            {
                _extractROI->setProperty("outputImagePath",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in writing File", "Unknown Error", true);
            }
        }

    }

    void 
        ExtractROIGUI::closeStereoWizard(Stereo::MainWindowGUI* stereoWiz)
    {        
        if (stereoWiz)
        {
            delete stereoWiz;
            stereoWiz = NULL;
        }
    }


    // to view the specified image    
    void 
        ExtractROIGUI::_viewImage(QString imagePath)
    { 
        std::string filename = imagePath.toStdString();
        if(filename != "")
        {                
            Stereo::MainWindowGUI* stereoWizard = new Stereo::MainWindowGUI(filename,Stereo::Image::NONE);
            _stereoWizard.push_back(stereoWizard);
            QObject::connect(stereoWizard,SIGNAL(applicationClosed(Stereo::MainWindowGUI*)),
                this,SLOT(closeStereoWizard(Stereo::MainWindowGUI*)), Qt::QueuedConnection);
            stereoWizard->showMaximized();
            stereoWizard->setWindowTitle(filename.c_str());
        }
    }

    void
        ExtractROIGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler, APP::IGUIManager>(getGUIManager());

                if(!_addContentUIHandler.valid())
                {
                    setActive(false);
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }



    void ExtractROIGUI::_handleOkButtonClicked()
    {
        _extractROI = _findChild("imagesubset");
        if(_extractROI)
        {
            std::string outputName = _extractROI->property("outputImagePath").toString().toStdString();
            if(outputName == "")
            {
                showError("outputName", "Please enter a output image Name");
                return;
            }
            std::string inputName = _extractROI->property("inputImagePath").toString().toStdString();
            if(inputName == "")
            {
                showError("inputName", "Please enter  input image");
                return;
            }

            QString startXtemp  = _extractROI->property("startX").toString();
            if( startXtemp == ""  )
            {
                showError("startX", "Please enter  value");
                return;
            }

            QString startYtemp  = _extractROI->property("startY").toString();
            if( startYtemp  == ""  )
            {
                showError("startY", "Please enter   value");
                return;
            }

            QString sizeXtemp   = _extractROI->property("sizeX").toString();
            if( sizeXtemp   == "" )
            {
                showError("sizeX", "Please enter value");
                return;
            }

            QString sizeYtemp   = _extractROI->property("sizeY").toString();
            if(sizeYtemp  == "" )
            {
                showError("sizeY", "Please enter  value");
                return;
            }

            int startX = startXtemp.toInt();
            int startY = startYtemp.toInt();
            int sizeX = sizeXtemp.toInt();
            int sizeY = sizeYtemp.toInt();

            if( startX < 0 || startY < 0 || sizeX < 0 || sizeY < 0 )
            {
                showError("ROI values", "Please enter a valid positive integer ");
                return;
            }

            _extractROI->setProperty("calculating",true);
            _extractROI->setProperty("okButtonEnabled",false);

            std::stringstream command;
            command.str("");

            command << "gdal_translate" << " -srcwin";
            command << " " << startX << " " << startY;
            command << " " << sizeX << " " << sizeY;
            command << " " << inputName << " " << outputName;
            std::cout << command.str().c_str()<< std::endl;
            if(system(command.str().c_str()))
            {
                emit showError("Filter Status", "Failed to apply filter");
                emit _updateGUIStatus();
                return;
            }
            emit _updateGUIStatus();
            emit showError("Filter Status", "Successfully applied filter", false);



        }



    }


    void 
        ExtractROIGUI::setActive(bool value)
    {
        try
        {
            _handleGUIStatus();
            if(value)
            {
            }
            else
            {
            }

            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }



    // updates GUI according to the filter status
    void
        ExtractROIGUI::_handleGUIStatus()
    {
        _extractROI = _findChild("imagesubset");
        if(_extractROI)
        {
            _extractROI->setProperty("calculating",false);
            _extractROI->setProperty("okButtonEnabled",true);


        }

    }  



}// namespace SMCQt
