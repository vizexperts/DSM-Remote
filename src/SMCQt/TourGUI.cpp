/****************************************************************************
*
* File             : TourGUI.h
* Description      : TourGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/TourGUI.h>
#include <SMCQt/IconObject.h>

#include <App/AccessElementUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/IAnimationComponent.h>
#include <Audio/IAudioComponent.h>
#include <Core/ITickMessage.h>
#include <Core/AttributeTypes.h>

#include <VizQt/QtUtils.h>
#include <QtMultimedia/QMediaRecorder>
#include <Util/FileUtils.h>
#include <boost/filesystem.hpp>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, TourGUI);
    DEFINE_IREFERENCED(TourGUI, DeclarativeFileGUI);

    const std::string TourGUI::NarrationFile = "Narration.wav";

    TourGUI::TourGUI() 
        : _isCurrentRecordedSaved(-1)
        ,_tourUIHandler(NULL)
        ,_animationUIHandler(NULL)
        ,_menuType(TOUR_MENU)
        ,_audioPlayer(NULL)
        ,_audioRecorder(NULL)
        ,_recordingState(IDLE)
        ,_audioToDelete("")
        ,_enableMicForTour("")
        ,_tourToPlay(1)
    {
    }

    TourGUI::~TourGUI()
    {
        delete _audioPlayer;
        delete _audioRecorder;
    }

    void TourGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* bottomLoader = _findChild("bottomLoader");
        if(bottomLoader != NULL)
        {
            QObject::connect(bottomLoader, SIGNAL(tourMenuLoaded()), this,
                SLOT(connectTourMenu()), Qt::UniqueConnection);
        }

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void TourGUI::connectPopupLoader(QString type)
    {
        if(!_tourUIHandler.valid())
            return;

        if(type == "tourSequenceGUI")
        {
            _menuType = MenuType::SEQUENCE_MENU;
            QObject* tourSequenceGUI = _findChild("tourSequenceGUI");
            if(tourSequenceGUI)
            {
                _remainingTourList.clear();
                _currentTourSequence.clear();
                //! populate tour list
                const SMCElements::ITourComponent::MapTourToFile& mapTourToFile = _tourUIHandler->getTourMap();

                SMCElements::ITourComponent::MapTourToFile::const_iterator constItr = mapTourToFile.begin();
                for(; constItr != mapTourToFile.end(); ++constItr)
                {
                    _remainingTourList.append(QString::fromStdString(constItr->first));
                }

                _setContextProperty("tourSequenceAvailableList", QVariant::fromValue(_remainingTourList));
                _setContextProperty("tourSequenceList", QVariant::fromValue(_currentTourSequence));

                connect(tourSequenceGUI, SIGNAL(addToSequenceList(int)), this, SLOT(addToSequenceList(int)));
                connect(tourSequenceGUI, SIGNAL(removeFromSequence(int)), this, SLOT(removeFromSequence(int)));
                connect(tourSequenceGUI, SIGNAL(moveInSequence(int,int)), this, SLOT(moveInSequence(int,int)));
                connect(tourSequenceGUI, SIGNAL(saveSequence(QString)), this, SLOT(saveSequence(QString)));
                connect(tourSequenceGUI, SIGNAL(openBottomLoader()), this, SLOT(openBottomLoader()));
            }
        }
    }

    void TourGUI::addToSequenceList(int index)
    {
        QObject* tourSequenceGUI = _findChild("tourSequenceGUI");
        if(tourSequenceGUI)
        {
            if(index != -1 && !_remainingTourList.empty())
            {
                QString tourName = _remainingTourList.at(index);
                _remainingTourList.removeAt(index);
                _currentTourSequence.append(tourName);
            }

            _setContextProperty("tourSequenceAvailableList", QVariant::fromValue(_remainingTourList));
            _setContextProperty("tourSequenceList", QVariant::fromValue(_currentTourSequence));

        }
    }

    void TourGUI::removeFromSequence(int index)
    {
        QObject* tourSequenceGUI = _findChild("tourSequenceGUI");
        if(tourSequenceGUI)
        {
            if(index != -1)
            {
                QString tourName = _currentTourSequence.at(index);
                _currentTourSequence.removeAt(index);
                _remainingTourList.append(tourName);
            }
            _setContextProperty("tourSequenceAvailableList", QVariant::fromValue(_remainingTourList));
            _setContextProperty("tourSequenceList", QVariant::fromValue(_currentTourSequence));
        }
    }

    void TourGUI::moveInSequence(int from, int to)
    {
        QObject* tourSequenceGUI = _findChild("tourSequenceGUI");
        if(tourSequenceGUI)
        {
            if(from != -1 && to != -1)
            {
                _currentTourSequence.move(from, to);
            }

            //_setContextProperty("tourSequenceAvailableList", QVariant::fromValue(_remainingTourList));
            _setContextProperty("tourSequenceList", QVariant::fromValue(_currentTourSequence));
        }
    }
    void TourGUI::openBottomLoader()
    {
        QObject* bottomLoader = _findChild("bottomLoader");
        if (bottomLoader)
        {
            QMetaObject::invokeMethod(bottomLoader, "load", Q_ARG(QVariant, QVariant("TourMenu.qml")));
        }
        QObject* tourMenu = _findChild("tourMenu");
        if (tourMenu)
        {
            tourMenu->setProperty("menuType", QVariant::fromValue((int)MenuType::SEQUENCE_MENU));
        }
        
    }

    void TourGUI::muteNarration(bool checked)
    {
        if (checked && _audioRecorder && (_recordingState == RECORDING || _recordingState == RECORDING_PAUSED))
        {
            _setVolume(0);
        }

        else if (!checked && _audioRecorder && (_recordingState == RECORDING || _recordingState == RECORDING_PAUSED))
        {
            _setVolume(1);
        }
    }

    void TourGUI::saveSequence(QString name)
    {
        if(name.isEmpty())
        {
            emit showError("Save Tour Sequence Error", "Sequence name can not be empty.");
            return;
        }

        if(!_tourUIHandler.valid())
        {
            emit showError("Save Tour Sequence Error", "TourUIHandler not valid. Please check config file.", true);
        }

        if(_currentTourSequence.empty())
        {
            emit showError("Save Tour Sequence Error", "Sequence can not be empty.");
        }

        std::string sequenceName = name.toStdString();
        std::vector<std::string> sequence;
        sequence.clear();
        for(int i = 0; i < _currentTourSequence.length(); ++i)
        {
            sequence.push_back(_currentTourSequence[i].toStdString());
        }

        try
        {
            _tourUIHandler->saveSequence(sequenceName, sequence);
        }
        catch(UTIL::Exception &e)
        {
            std::string errorMessage = e.What();
            emit showError("Save Sequence Error", errorMessage.c_str());
            return;
        }

        //! close popup
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QMetaObject::invokeMethod(popupLoader, "unloadAll");
        }
        _updateTourListGUI();
    }

    void TourGUI::menuTypeChanged(int menuType)
    {
        _menuType = MenuType(menuType);

        _updateTourListGUI();
    }



    void TourGUI::connectTourMenu()
    {
        if(!_tourUIHandler.valid())
        {
            return;
        }


        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            _menuType = TOUR_MENU;
            // connection for tour
            QObject::connect(tourMenu, SIGNAL(addNewTour()),
                this, SLOT(addNewTour()),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(playTour(QString)), 
                this, SLOT(playTour(QString)),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(enableMicInModel(QString)),
                this, SLOT(enableMicInQMLModel(QString)), Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(micClicked(QString)),
                this, SLOT(micClicked(QString)), Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(deleteAudio(QString)),
                this, SLOT(deleteAudio(QString)), Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(deleteTour(QString)),
                this, SLOT(confirmTourDelete(QString)),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(pauseTour()),
                this, SLOT(pauseTour()),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(muteNarration(bool)),
                this, SLOT(muteNarration(bool)), Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(decreaseSpeedFactor()),
                this, SLOT(decreaseSpeedFactor()), Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(increaseSpeedFactor()),
                this, SLOT(increaseSpeedFactor()), Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(stopTour()), 
                this, SLOT(stopTour()),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(tourSliderValueChanged(int)),
                this, SLOT(tourSliderValueChanged(int)),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(populateTourList()),
                this, SLOT(populateTourList()),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(changeRecorderType(int)),
                this, SLOT(changeRecorderType(int)),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(changeTourName(QString, QString)),
                this, SLOT(changeTourName(QString, QString)),Qt::UniqueConnection);

            QObject::connect(tourMenu, SIGNAL(changeMenuType(int)),
                this, SLOT(menuTypeChanged(int)),Qt::UniqueConnection);

            // connecting check point related signals & slots
            connectTourSpeedEditor();

            float speedFactor = 1.0;   // default speed factor when tour gui is opened
            _tourUIHandler->setSpeedFactor(speedFactor);

            tourMenu->setProperty("speedFactor", QVariant::fromValue(speedFactor));

            int recorderType = int(_tourUIHandler->getRecorderType());

            tourMenu->setProperty("recorderType", QVariant::fromValue(recorderType));

            // get tour state from handler
            SMCElements::ITourComponent::TourMode tourMode = _tourUIHandler->getTourState();
            if(tourMode == SMCElements::ITourComponent::Tour_Playing)
            {
                QString currentTourName = QString::fromStdString(_tourUIHandler->getCurrentTourName());   
                unsigned int duration = _tourUIHandler->getTourDuration();
                unsigned int elapsedTime = _tourUIHandler->getTourCurrentTime();

                tourMenu->setProperty("currentItemName", QVariant::fromValue(currentTourName));
                tourMenu->setProperty("showSlider", QVariant::fromValue(true));
                tourMenu->setProperty("elapsedTime", QVariant::fromValue(elapsedTime));
                tourMenu->setProperty("tourDuration", QVariant::fromValue(duration));

                SMCElements::ITourComponent::PlayerState playerState = _tourUIHandler->getPlayerState();
                if(playerState == SMCElements::ITourComponent::Player_Playing)
                {
                    tourMenu->setProperty("animationRunning", QVariant::fromValue(true));
                }
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                _subscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);
            }

            // connect to recorderLoaded signal from popup
            QObject* recorderLoader = _findChild("recorderLoader");
            if(recorderLoader)
            {
                QObject::connect(recorderLoader, SIGNAL(recorderLoaded()), 
                    this, SLOT(connectRecorder()), Qt::UniqueConnection);
            }
        }
    }

    void TourGUI::connectTourSpeedEditor()
    {
        if(!_tourUIHandler.valid())
        {
            return;
        }

        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            _checkPointList.clear();
#ifdef WIN32
            _setContextProperty("checkPointList", NULL);
#else //WIN32
            _setContextProperty("checkPointList", 0);
#endif //WIN32
            // connections for checkpoint manipulation
            QObject::connect(tourMenu, SIGNAL(createCheckPoint(int)),this, SLOT(createCheckPoint(int)),Qt::UniqueConnection);
            QObject::connect(tourMenu, SIGNAL(modifyCheckPoint(int, int, double)),this, SLOT(modifyCheckPoint(int, int, double)),Qt::UniqueConnection);
            QObject::connect(tourMenu, SIGNAL(deleteCheckPoint(int)),this, SLOT(deleteCheckPoint(int)),Qt::UniqueConnection);
            QObject::connect(tourMenu, SIGNAL(resetSpeedProfile()),this, SLOT(resetSpeedProfile()),Qt::UniqueConnection);
            QObject::connect(tourMenu, SIGNAL(saveSpeedProfile()),this, SLOT(saveSpeedProfile()),Qt::UniqueConnection);
            QObject::connect(tourMenu, SIGNAL(cpContextualOpened(int)),this, SLOT(setCurrentCheckpointProperties(int)),Qt::UniqueConnection);
            QObject::connect(tourMenu, SIGNAL(previewAtCp(int)),this, SLOT(previewAtCheckPoint(int)),Qt::UniqueConnection);
        }
    }

    void TourGUI::createCheckPoint(int timeStamp)
    {
        if(!_tourUIHandler.valid())
        {
            emit showError("CheckPoint error", "Error in creating checkpoint");
            return;
        }
        _tourUIHandler->createNewCheckPoint(timeStamp);
        _populateCheckPointList();
    }

    void TourGUI::setCurrentCheckpointProperties(int index)
    {
        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            const SMCElements::ITourComponent::MapTimeToSpeed &mapTimeToSpeed = _tourUIHandler->getCheckPoints();
            SMCElements::ITourComponent::MapTimeToSpeed::const_iterator constItr= mapTimeToSpeed.find(_checkPointList.at(index).toInt());
            tourMenu->setProperty("speedAtActiveCheckPoint",QVariant::fromValue(constItr->second));
            tourMenu->setProperty("activeCheckPointIndex",QVariant::fromValue(index));
        }
    }

    void TourGUI::resetSpeedProfile()
    {
        if(!_tourUIHandler.valid())
        {
            emit showError("CheckPoint error", "Error in resetting default speed profile");
            return;
        }
        _tourUIHandler->resetSpeedProfile();
        _populateCheckPointList();
    }

    void TourGUI::saveSpeedProfile()
    {
        if(!_tourUIHandler.valid())
        {
            emit showError("CheckPoint error", "Error in saving checkpoints");
            return;
        }

        //emit question("Save Confirmation", "Changes are not revertable, Do you want to save...?",false,
        //    "closeApplicationOkClicked()", "closeApplicationCancleClicked()");

        _tourUIHandler->saveSpeedProfile();
    }

    void TourGUI::modifyCheckPoint(int index, int newTime, double newSpeed)
    {
        if(!_tourUIHandler.valid())
        {
            emit showError("CheckPoint error", "Error in modifying checkpoint");
            return;
        }

        if( index < 0 )
            return;

        _tourUIHandler->editCheckPoint(_checkPointList.at(index).toInt(), newTime, newSpeed);
        _populateCheckPointList();
    }

    void TourGUI::deleteCheckPoint(int index)
    {
        if(!_tourUIHandler.valid())
        {
            emit showError("CheckPoint error", "Error in deleting checkpoint");
            return;
        }
        _tourUIHandler->deleteCheckPoint(_checkPointList.at(index).toInt());
        _populateCheckPointList();
    }

    void TourGUI::previewAtCheckPoint(int time)
    {
        if(!_tourUIHandler.valid())
        {
            emit showError("CheckPoint error", "Error in deleting checkpoint");
            return;
        }
        //_tourUIHandler->previewAtCheckPoint(_checkPointList.at(index).toInt());
        _tourUIHandler->previewAtCheckPoint(time);
    }
    void TourGUI::_populateCheckPointList()
    {
        if(!_tourUIHandler.valid())
        {
            return;
        }
        const SMCElements::ITourComponent::MapTimeToSpeed &mapTimeToSpeed = _tourUIHandler->getCheckPoints();

        SMCElements::ITourComponent::MapTimeToSpeed::const_iterator constItr=mapTimeToSpeed.begin();

        // populating the checkpoint list 
        _checkPointList.clear();

        for(;constItr!=mapTimeToSpeed.end(); ++constItr)
        {
            _checkPointList.append(QVariant::fromValue(constItr->first));
        }
        _setContextProperty("checkPointList",_checkPointList);
    }

    void TourGUI::_resetCheckPointGUI()
    {
        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            tourMenu->setProperty("speedAtActiveCheckPoint",QVariant::fromValue(1.0));
            tourMenu->setProperty("activeCheckPointIndex",QVariant::fromValue(-1));
        }
    }

    void TourGUI::markKeyframe()
    {
        if(_tourUIHandler.valid())
        {
            _tourUIHandler->recordKeyframe();
        }
    }

    void TourGUI::changeTourName( QString oldName, QString newName )
    {
        if(_menuType == TOUR_MENU)
        {
            try
            {   
                _tourUIHandler->setTourName( oldName.toStdString(), newName.toStdString());
            }
            catch(UTIL::Exception &e)
            {
                std::string message = e.What();
                emit showError("Tour Rename", message.c_str());
            }
        }
        
        else
        {
            try
            {   
                _tourUIHandler->setSequenceName( oldName.toStdString(), newName.toStdString());
            }
            catch(UTIL::Exception &e)
            {
                std::string message = e.What();
                emit showError("Sequence Rename", message.c_str());
            }
        }

        _updateTourListGUI();
    }

    void TourGUI::changeRecorderType(int type)
    {
        if(_tourUIHandler.valid())
        {
            _tourUIHandler->setRecorderType(SMCElements::ITourComponent::RecorderType(type));
        }
    }

    void TourGUI::tourSliderValueChanged(int value)
    {
        if(!_tourUIHandler.valid())
        {
            return;
        }

        _tourUIHandler->setTourCurrTime(value);
        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            float speedFactor = _tourUIHandler->getSpeedFactor();
            tourMenu->setProperty("speedFactor", QVariant::fromValue(speedFactor));
        }
    }

    void TourGUI::populateTourList()
    {
        _updateTourListGUI();
    }

    void TourGUI::addNewTour()
    {
        // configure to add new tour
        _isCurrentRecordedSaved = -1;
    }

    void TourGUI::confirmTourDelete(QString tourName)
    {
        _tourToDelete = tourName;

        QString message;
        if(_menuType == TOUR_MENU)
        {
            message = "Do you want to delete tour?";
        }
        else
        {
            message = "Do you want to delete sequence?";
        }

        emit question("Delete Tour", message, false,
            "deleteTourOkButtonPressed()", "deleteTourCancelButtonPressed()");
    }

    void TourGUI::deleteTourOkButtonPressed()
    {
        if(!_tourUIHandler.valid())
        {
            emit showError("Tour Delete Error", "Error in deleting tour");
            return;
        }
        if(_tourToDelete.isEmpty())
        {
            emit showError("Tour Delete Error", "No Tour Selected");
            return;
        }

        bool status  = false;
        try
        {
            if(_menuType == TOUR_MENU)
            {
                status = _tourUIHandler->deleteTour(_tourToDelete.toStdString());
            }
            else
            {
                _tourUIHandler->deleteSequence(_tourToDelete.toStdString());
            }
            _tourToDelete = "";
        }
        catch(...)
        {

        }

        _updateTourListGUI();
    }

    void TourGUI::_launchOSGView(std::string tourNameToPlay)
    {
        std::string projectFileName = CORE::WorldMaintainer::instance()->getProjectFile();

        //Get stereo setting
        std::string command = "GeorbIS.exe --osg --project " + projectFileName + " --tourName " + '\"' 
            + tourNameToPlay + '\"';

        //! display settings
        if(osg::DisplaySettings::instance()->getStereo())
        {
            osg::DisplaySettings* ds = osg::DisplaySettings::instance();

            osg::DisplaySettings::StereoMode stereoMode = ds->getStereoMode();
            std::string stereo = " --stereoMode " + UTIL::ToString((int)stereoMode);
            std::string eyeSeperation = " --eyeSeperation " + UTIL::ToString(ds->getEyeSeparation());
            std::string screenDistance = " --screenDistance " + UTIL::ToString(ds->getScreenDistance());
            std::string screenHeight = " --screenHeight " + UTIL::ToString(ds->getScreenHeight());
            std::string screenWidth = " --screenWidth " + UTIL::ToString(ds->getScreenWidth());

            command += stereo;
            command += eyeSeperation;
            command += screenDistance;
            command += screenHeight;
            command += screenWidth;
        }

        system(command.c_str());
    }

    void TourGUI::micClicked(QString tourname)
    {
        if (_audioRecorder != NULL)
        {
            if (_recordingState == IDLE)
            {
                // setting audio settings for audio recorder
                
                _audioRecorder->setAudioInput(_audioRecorder->defaultAudioInput());
                QAudioEncoderSettings audioSettings;
                audioSettings.setCodec("audio/pcm");
                audioSettings.setQuality(QMultimedia::HighQuality);

                _audioRecorder->setEncodingSettings(audioSettings);

                
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
                if (!wmain)
                {
                    LOG_ERROR("World Maintainer Not found");
                    return;
                }

                _unsubscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);

                // getting current project path from world maintainer
                std::string projectPath = osgDB::getFilePath(wmain->getProjectFile());

                std::string path = osgDB::convertFileNameToNativeStyle(projectPath);
                if (!osgDB::fileExists(path))
                {
                    LOG_ERROR("Project folder not present");
                    return;
                }

                QObject* tourMenu = _findChild("tourMenu");
                if (tourMenu)
                {
                    QString tourName = tourMenu->property("currentItemName").toString();
                    tourName = QString::fromStdString(path) + "/" + tourName + "/" + QString::fromStdString(NarrationFile);

                    // audio dump location will be project's folder with filename same as sequence's name
                    _audioRecorder->setOutputLocation(QUrl::fromLocalFile(tourName));
                    _recordingState = RECORDING;
                    tourMenu->setProperty("recordingRunning", QVariant::fromValue(true));
                    emit showError("Narration", "Narration recording will be started on playing the sequence");
                }
            }
        }

        else
        {
            LOG_ERROR("Could not find audio recorder object");
            return;
        }
    }

    // utility function to play narration
    void TourGUI::_playAudio(std::string tourName)
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return;
        }

        std::string projectPath = osgDB::getFilePath(wmain->getProjectFile());

        std::string path = osgDB::convertFileNameToNativeStyle(projectPath + "/" + tourName);
        if (!osgDB::fileExists(path))
        {
            LOG_ERROR("Sequence folder not present");
            return;
        }

        tourName = path + "/" + NarrationFile;

        // playing the audio file
        _audioPlayer->setMedia(QUrl::fromLocalFile(QString::fromStdString(tourName)));
        _audioPlayer->setVolume(100);

        if (_tourUIHandler.valid())
        {
            float speedFactor = _tourUIHandler->getSpeedFactor();
            _audioPlayer->setPlaybackRate(speedFactor);
        }

        _audioPlayer->play();
    }

    // deletes audio for given sequence name
    void TourGUI::deleteAudio(QString tourname)
    {
        _audioToDelete = tourname.toStdString();
        emit question("Delete", "Do you really want to delete this narration?", false,
                "_okSlotForDeleting()", "_cancelSlotForDeleting()");
    }

    void TourGUI::_setVolume(int volume) {

        CORE::RefPtr<CORE::IComponent> audioComponent =
            CORE::WorldMaintainer::instance()->getComponentByName("AudioComponent");

        if (audioComponent.valid())
        {
            CORE::RefPtr<AUDIO::IAudioComponent> audio =
                audioComponent->getInterface<AUDIO::IAudioComponent>();

            try{
                audio->setMicrophoneVolume(volume);
            }
            catch (...)
            {
                LOG_ERROR("Failed to set microphone volume");
            }
            
        }
        
    }

    void TourGUI::_okSlotForDeleting()
    {
        bool present = _isAudioPresent(_audioToDelete);
        if (present)
        {
            // output location will be project's tour folder with name same as sequence
            CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
            if (!wmain)
            {
                LOG_ERROR("World Maintainer Not found");
                return;
            }
            std::string projectPath = osgDB::getFilePath(wmain->getProjectFile());

            std::string folderPath = projectPath + "/" + _audioToDelete;
            std::string filePath = folderPath + "/" + NarrationFile;

            std::string fileExist = osgDB::findFileInDirectory(NarrationFile, folderPath);

            try{
                if (!fileExist.empty())
                {
                    _audioPlayer->setMedia(QUrl::fromLocalFile(""));
                    UTIL::deleteFile(filePath);
                    LOG_INFO("Narration has been successfully deleted");
                    enableMicInQMLModel(QString::fromStdString(_audioToDelete));
                    _audioToDelete.clear();
                }
            }

            catch (...)
            {
                emit showError("Error", "Error in deleting narration");
                return;
            }

            
            _unsubscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);

            
        }
    }

    void TourGUI::_cancelSlotForDeleting()
    {
        return;
    }

    void TourGUI::playTour(QString tourName)
    {
        if(getOpenTourInExternalViewer())
        {
            CORE::RefPtr<VizQt::IQtGUIManager> qtapp = 
                getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            if(qtapp->getLayoutWidget()->isFullScreen())
            {
                qtapp->getLayoutWidget()->showMaximized();
            }

            osg::ref_ptr<UTIL::ThreadPool> tp = CORE::WorldMaintainer::instance()->getComputeThreadPool();
            tp->invoke(boost::bind(&TourGUI::_launchOSGView, this, tourName.toStdString()));

            return;
        }

        if(!_tourUIHandler.valid())
        {
            emit showError("Tour Play Error", "Error in playing tour");
            return;
        }
        if(tourName.isEmpty())
        {
            emit showError("Tour Play Error", "No Tour Selected");
            return;
        }

        bool status = false;
        try
        {
            // resetting speed factor to 1 at the start of every tour/sequence 
            _tourUIHandler->setSpeedFactor(1);
            QObject* tourMenu = _findChild("tourMenu");
            if (tourMenu)
            {
                tourMenu->setProperty("speedFactor", QVariant::fromValue(1));
            }

            if(_menuType == TOUR_MENU)
            {
                status = _tourUIHandler->playTour(tourName.toStdString());
            }
            else
            {
                // start recording audio and if the audio was paused earlier then changing _recordingState to RECORDING
                if ((!_isAudioPresent(tourName.toStdString()) && _recordingState == RECORDING) || (_isAudioPresent(tourName.toStdString()) && _recordingState == RECORDING_PAUSED))
                {
                    if (_recordingState == RECORDING_PAUSED)
                    {
                        _recordingState = RECORDING;
                    }
                    _audioRecorder->record();
                }

                //if we are not recording then we have to play the audio on playing sequence
                else if (_isAudioPresent(tourName.toStdString()) )
                {
                    if (_recordingState == IDLE)
                    {
                        // starting the audio
                        _playAudio(tourName.toStdString());
                        _recordingState = PLAYING_AUDIO;
                    }

                    else if (_recordingState == PLAYING_PAUSED)
                    {
                        // resuming the paused audio
                        _audioPlayer->play();
                        _recordingState = PLAYING_AUDIO;
                    }
                }

                // getting tour count in _tourToPlay variable and updating the same in UI
                _getNumberOfToursInSequence(_tourToPlay, tourName.toStdString());
                if (tourMenu)
                {
                    tourMenu->setProperty("tourToPlay", QVariant::fromValue(_tourToPlay));
                }
                _tourUIHandler->playSequence(tourName.toStdString());
                status = true;
            }
        }
        catch(UTIL::Exception &e)
        {
            std::string errorMessage = e.What();
            emit showError("Play Tour Error", errorMessage.c_str());
        }
        catch(...)
        {
        }

        if(!status)
        {
            emit showError("Tour Play Error", "Error in playing tour");
            return;
        }

        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            int totalTime = _tourUIHandler->getTourDuration();
            tourMenu->setProperty("totalTime", totalTime );
            tourMenu->setProperty("animationRunning", QVariant::fromValue(true));
            tourMenu->setProperty("showSlider", QVariant::fromValue(true));
            if(_menuType == TOUR_MENU)
            {
                tourMenu->setProperty("tourName", QVariant::fromValue( "         Playing tour : " + tourName ) );
            }
            else
            {

                tourMenu->setProperty("tourName", QVariant::fromValue( "         Playing tour : " + tourName + " : " + 
                    QString::fromStdString(_tourUIHandler->getCurrentTourName())) );
            }
            tourMenu->setProperty("tourPlayerState", "MINIMIZED" );
        }

        if(_checkPointList.empty())
        {
            _populateCheckPointList();
            _resetCheckPointGUI();
        }

        CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
            APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

        _subscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);

        CORE::RefPtr<SMCElements::ITourComponent> tourComponent = 
            wmain->getComponentByName("TourComponent")->getInterface<SMCElements::ITourComponent>();
        _subscribe(tourComponent.get(), *SMCElements::ITourComponent::SequenceTourChangedMessageType);
    }

    void TourGUI::pauseTour()
    {
        if(!_tourUIHandler.valid())
        {
            return;
        }

        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            tourMenu->setProperty("animationRunning", QVariant::fromValue(false));
            tourMenu->setProperty("tourName",
                QVariant::fromValue( QString::fromStdString( "         Paused tour : " + _tourUIHandler->getCurrentTourName() ) ) );
        }

        if (_isAudioPresent(tourMenu->property("currentItemName").toString().toStdString()) && _recordingState == RECORDING && _audioRecorder)
        {
            // pausing the recording of audio if _recordingState = RECORDING
            _audioRecorder->pause();
            _recordingState = RECORDING_PAUSED;
        }

        else if (_isAudioPresent(tourMenu->property("currentItemName").toString().toStdString()) && _recordingState == PLAYING_AUDIO && _audioPlayer)
        {
            // pausing the audio playback if _recordingState = PLAYING_AUDIO
            _audioPlayer->pause();
            _recordingState = PLAYING_PAUSED;
        }

        _tourUIHandler->pauseTour();

        
    }
    void TourGUI::stopTour()
    {
        if(!_tourUIHandler.valid())
        {
            return;
        }

        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            tourMenu->setProperty("animationRunning", QVariant::fromValue(false));
            tourMenu->setProperty("showSlider", QVariant::fromValue(false));
            tourMenu->setProperty("tourPlayerState", "" );
            tourMenu->setProperty("recordingRunning", QVariant::fromValue(false));
            
            // resetting _tourToPlay to its default value i.e. 1 
            _tourToPlay = 1;
            tourMenu->setProperty("tourToPlay", QVariant::fromValue(_tourToPlay));

#ifdef WIN32
            tourMenu->setProperty("checkPointList", NULL);
#else //WIN32
            tourMenu->setProperty("checkPointList", "");
#endif //WIN32
        }

        QString tourName = tourMenu->property("currentItemName").toString();

        if (_audioRecorder && (_recordingState == RECORDING || _recordingState == RECORDING_PAUSED))
        {
            // stops audio recording if above conditions are met
            _audioRecorder->stop();
            _recordingState = IDLE;
            if (tourMenu)
            {
                enableMicInQMLModel(tourName);
            }
        }

        else if (_isAudioPresent(tourName.toStdString()) && _audioPlayer && (_recordingState == PLAYING_AUDIO || _recordingState == PLAYING_PAUSED))
        {
            // stops audio playback if above conditions are met
            _audioPlayer->stop();
            _audioPlayer->setMedia(QUrl::fromLocalFile(""));
            _recordingState = IDLE;
            if (tourMenu)
            {
                enableMicInQMLModel(tourName);
            }
        }

        _tourUIHandler->stopTour();
        _setVolume(1);

        //! calling reset all explicitly so that current sequence name/playing state are also reset
        _tourUIHandler->resetAll();

        _checkPointList.clear();

        CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
            APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

        _unsubscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);

        CORE::RefPtr<SMCElements::ITourComponent> tourComponent = 
            wmain->getComponentByName("TourComponent")->getInterface<SMCElements::ITourComponent>();
        _unsubscribe(tourComponent.get(), *SMCElements::ITourComponent::SequenceTourChangedMessageType);
    }

    void TourGUI::decreaseSpeedFactor()
    {
        if(_tourUIHandler.valid())
        {
            _tourUIHandler->decreaseSpeedFactor();

            QObject* tourMenu = _findChild("tourMenu");
            if(tourMenu)
            {
                float speedFactor = _tourUIHandler->getSpeedFactor();

                tourMenu->setProperty("speedFactor", QVariant::fromValue(speedFactor));

                if (_recordingState == PLAYING_AUDIO && _audioPlayer)
                {
                    _audioPlayer->setPlaybackRate(speedFactor);
                }
            }
        }
    }

    void TourGUI::increaseSpeedFactor()
    {
        if(_tourUIHandler.valid())
        {
            _tourUIHandler->increaseSpeedFactor();

            QObject* tourMenu = _findChild("tourMenu");
            if(tourMenu)
            {
                float speedFactor = _tourUIHandler->getSpeedFactor();

                tourMenu->setProperty("speedFactor", QVariant::fromValue(speedFactor));

                if (_recordingState == PLAYING_AUDIO && _audioPlayer)
                {
                    _audioPlayer->setPlaybackRate(speedFactor);
                }
            }
        }
    }

    void TourGUI::editCurrentTourSpeed()
    {
        //_tourUIHandler->
    }


    void TourGUI::connectRecorder()
    {
        QObject* tourRecorder = _findChild("tourRecorder");
        if(tourRecorder)
        {
            QObject::connect(tourRecorder, SIGNAL(startRecording()),
                this, SLOT(startRecording()), Qt::UniqueConnection);

            QObject::connect(tourRecorder, SIGNAL(pauseRecording()),
                this, SLOT(pauseRecording()), Qt::UniqueConnection);

            QObject::connect(tourRecorder, SIGNAL(stopRecording()),
                this, SLOT(stopRecording()), Qt::UniqueConnection);

            QObject::connect(tourRecorder, SIGNAL(saveTour()),
                this, SLOT(saveTour()), Qt::UniqueConnection);

            QObject::connect(tourRecorder, SIGNAL(closed()),
                this, SLOT(recorderClosed()), Qt::UniqueConnection);

            QObject::connect(tourRecorder, SIGNAL(markKeyframe()),
                this, SLOT(markKeyframe()), Qt::UniqueConnection);

            // set initial properties

            tourRecorder->setProperty("recording", QVariant::fromValue(false));

            int recorderType = int(_tourUIHandler->getRecorderType());

            tourRecorder->setProperty("recorderType", QVariant::fromValue(recorderType));
        }
    }

    void TourGUI::openSaver()
    {
        QObject* tourRecorder = _findChild("tourRecorder");
        if(tourRecorder)
        {
            QMetaObject::invokeMethod(tourRecorder, "showSaver", Q_ARG(QVariant, QVariant(true)));
        }
    }

    void TourGUI::recorderClosed()
    {
        if(_isCurrentRecordedSaved==0)
        {
            emit criticalQuestion("Recording", 
                " Do you want to save current recording ?",false,
                "openSaver()", "closeRecorderSilently()");
            return;
        }

        //Explicitily stopping the recorder
        _tourUIHandler->stopTourRecording();
        _isCurrentRecordedSaved = -1;

        QObject* recorderLoader = _findChild("recorderLoader");
        if(recorderLoader)
        {
            recorderLoader->setProperty("load", QVariant::fromValue(false));
        }

        QObject* bottomLoader = _findChild("bottomLoader");
        if(bottomLoader)
        {
            QMetaObject::invokeMethod(bottomLoader, "load", Q_ARG(QVariant, QVariant("TourMenu.qml")));
        }

        QObject* tourMenu = _findChild("tourMenu");
        if(tourMenu)
        {
            tourMenu->setProperty("showSlider", QVariant::fromValue(false));
        }
    }

    void
        TourGUI::closeRecorderSilently()
    {
        if(!_tourUIHandler.valid())
            return;

        _tourUIHandler->stopTourRecording();

        QObject* recorderLoader = _findChild("recorderLoader");
        if(recorderLoader)
        {
            recorderLoader->setProperty("load", QVariant::fromValue(false));
        }
    }

    void TourGUI::saveTour(bool overwrite)
    {
        QString name;
        QObject* tourRecorder = _findChild("tourRecorder");
        if(tourRecorder)
        {
            name  = tourRecorder->property("tourName").toString();
        }

        if(name == "")
        {
            emit showError("Save Tour", "Tour name cannot be empty.", false);
            return;
        }


        // save tour with given name
        bool status = false;

        try
        {
            status = _tourUIHandler->saveCurrentRecordedTour(name.toStdString(), overwrite);
        }
        catch(UTIL::Exception &e)
        {
            if(e.TypeEnum()==UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE)
            {
                if(UTIL::FileReadWriteExceptionType::getFileErrorMode() ==
                    UTIL::FileReadWriteExceptionType::FILE_OVERWRITE_ERROR )
                {
                    emit question("Tour Save Error",
                        "Tour with same name already exists. Do you want to overwrite ?",false,
                        "overwriteSave()"
                        "");
                    return;
                }
            }

            std::string errorMessage = e.What();
            emit showError("Save Tour Error", errorMessage.c_str());

            //close the saver
            QObject* recorder = _findChild("tourRecorder");
            if(recorder)
            {
                QMetaObject::invokeMethod(recorder, "showSaver", Q_ARG(QVariant, QVariant(false)));
            }

            return;
        }
        catch(...)
        {
        }

        //Kept true regardless of status to make sure 
        //GUI doesn't go bad if status is false
        _isCurrentRecordedSaved = 1;

        if(!status)
        {
            emit showError("Save Tour Error", "Error in saving tour recording");
            return;
        }
        else
        {
            //close the saver
            QObject* recorder = _findChild("tourRecorder");
            if(recorder)
            {
                QMetaObject::invokeMethod(recorder, "showSaver", Q_ARG(QVariant, QVariant(false)));
            }

            //updating the tour list
            _updateTourListGUI();
        }
    }

    void TourGUI::overwriteSave()
    {
        saveTour(true);
    }

    void TourGUI::startRecording()
    {
        // start recording
        if(!_tourUIHandler.valid())
        {
            emit showError("Start Tour", " Application Error!!Recorder Is Invalid");
            return;
        }

        //Checking the state of the recorder
        SMCElements::ITourComponent::RecorderState recordState = _tourUIHandler->getRecordState();
        if(recordState!=SMCElements::ITourComponent::Recorder_Recording)
        {
            if(!_tourUIHandler->startTourRecording())
            {
                emit showError("Start Tour", " Recording Error. Can not start");
                return;
            }
            else
            {
                _isCurrentRecordedSaved = 0;
            }
        }

        QObject* tourRecorder = _findChild("tourRecorder");
        if(tourRecorder)
        {
            tourRecorder->setProperty("recording", QVariant::fromValue(true));
        }
    }

    void TourGUI::stopRecording()
    {
        // start recording
        if(!_tourUIHandler.valid())
        {
            emit showError("Start Tour", " Application Error!! Recorder is invalid");
            return;
        }

        if(!_tourUIHandler->stopTourRecording())
        {
            emit showError("Stop Tour Recording", "No recording in progress", false);
            return;
        }
        else
        {
            QObject* tourRecorder = _findChild("tourRecorder");
            if(tourRecorder)
            {
                tourRecorder->setProperty("recording", QVariant::fromValue(false));
                QMetaObject::invokeMethod(tourRecorder, "showSaver", Q_ARG(QVariant, QVariant(true)));    
            }
        }
    }

    void TourGUI::pauseRecording()
    {
        // start recording
        if(!_tourUIHandler.valid())
        {
            emit showError("Start Tour", " Application Error!! Recorder is invalid");
            return;
        }

        if(!_tourUIHandler->pauseTourRecording())
        {
            emit showError("Pause Tour", " Recording Error. Can not Pause");
            return;
        }

        QObject* tourRecorder = _findChild("tourRecorder");
        if(tourRecorder)
        {
            tourRecorder->setProperty("recording", QVariant::fromValue(false));
        }
    }

    void TourGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {   
                _animationUIHandler = APP::AccessElementUtils::
                    getUIHandlerUsingManager<SMCUI::IAnimationUIHandler>(getGUIManager());

                _tourUIHandler =  APP::AccessElementUtils::
                    getUIHandlerUsingManager<SMCUI::ITourUIHandler>(getGUIManager());

                _audioRecorder = new QAudioRecorder;

                _audioPlayer = new QMediaPlayer;
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *CORE::WorldMaintainer::TickMessageType)
        {
            unsigned int elapsedTime = _tourUIHandler->getTourCurrentTime();

            QObject* tourMenu = _findChild("tourMenu");
            if(tourMenu)
            {
                tourMenu->setProperty("elapsedTime", QVariant::fromValue(elapsedTime));
                tourMenu->setProperty("speedFactor", QVariant::fromValue(_tourUIHandler->getSpeedFactor()));

                if (!tourMenu->property("animationRunning").toBool() && _tourToPlay == 0)
                {
                    // if animation is not running and we are subscribed to tick means we have just completed the sequence playback
                    if (_audioRecorder && _recordingState == RECORDING)
                    {
                        // stopping the recording
                        _audioRecorder->stop();
                        _recordingState = IDLE;

                        // refreshing the qml list view
                        enableMicInQMLModel(tourMenu->property("currentItemName").toString());

                    }

                    if (_audioPlayer && _recordingState == PLAYING_AUDIO)
                    {
                        // stopping the recording
                        _audioPlayer->stop();
                        _audioPlayer->setMedia(QUrl::fromLocalFile(""));
                        _recordingState = IDLE;

                        // refreshing the qml list view
                        enableMicInQMLModel(tourMenu->property("currentItemName").toString());
                    }

                    tourMenu->setProperty("showSlider", QVariant::fromValue(false));
                    tourMenu->setProperty("recordingRunning", QVariant::fromValue(false));
                    _setVolume(1);
                    // resetting _tourToPlay to its default value i.e. 1 and updating the same in UI
                    _tourToPlay = 1;
                    QObject* tourMenu = _findChild("tourMenu");
                    if (tourMenu)
                    {
                        tourMenu->setProperty("tourToPlay", QVariant::fromValue(_tourToPlay));
                    }

                }

                else if (_tourToPlay == 1)
                {
                    // tourToPlay = 1 means last tour in this sequence is about to start
                    _tourToPlay--;
                    QObject* tourMenu = _findChild("tourMenu");
                    if (tourMenu)
                    {
                        tourMenu->setProperty("tourToPlay", QVariant::fromValue(_tourToPlay));
                    }
                }
                
            }
        }
        else if(messageType == *SMCElements::ITourComponent::SequenceTourChangedMessageType)
        {
            if(_menuType == SEQUENCE_MENU)
            {
                QString tourName = QString::fromStdString(_tourUIHandler->getCurrentTourName());
                QString sequenceName = QString::fromStdString(_tourUIHandler->getCurrentSequenceName());
                int totalTime = _tourUIHandler->getTourDuration();
                _tourToPlay--;
                QObject* tourMenu = _findChild("tourMenu");
                if(tourMenu)
                {
                    tourMenu->setProperty("tourName", 
                        QVariant::fromValue( "         Playing tour : " + sequenceName + " : " + tourName) );

                     tourMenu->setProperty("totalTime", totalTime );
                     tourMenu->setProperty("tourToPlay", QVariant::fromValue(_tourToPlay));

                }


            }
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

    void TourGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();

        _addAttribute(new CORE::BooleanAttribute("OpenTourInExternalViewer", "OpenTourInExternalViewer",
            CORE::BooleanAttribute::SetFuncType(this, &TourGUI::setOpenTourInExternalViewer),
            CORE::BooleanAttribute::GetFuncType(this, &TourGUI::getOpenTourInExternalViewer),
            "OpenTourInExternalViewer",
            "OpenTourInExternalViewer Attributes"));
    }

    void TourGUI::setOpenTourInExternalViewer(bool value)
    {
        _openInExternalViewer = value;
    }

    bool TourGUI::getOpenTourInExternalViewer() const
    {
        return _openInExternalViewer;
    }

    // checks whether audio with given name is present or not
    bool TourGUI::_isAudioPresent(std::string tourName)
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return false;
        }

        std::string projectPath = osgDB::getFilePath(wmain->getProjectFile());

        std::string path = osgDB::convertFileNameToNativeStyle(projectPath+ "/" + tourName);
        if (!osgDB::fileExists(path))
        {
            LOG_ERROR("Project folder not present");
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            if (currFileName == NarrationFile)
            {
                return true;
            }
        }

        return false;
    }

    // returns number of tours in given sequence
    void TourGUI::_getNumberOfToursInSequence(int& size,const std::string& sequenceName)
    {
        size = 0;
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return;
        }

        std::string projectPath = osgDB::getFilePath(wmain->getProjectFile());

        std::string path = osgDB::convertFileNameToNativeStyle(projectPath + "/" + sequenceName);
        if (!osgDB::fileExists(path))
        {
            LOG_ERROR("Project folder not present");
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            // counting all the files having extension = tour
            if (osgDB::getFileExtension(currFileName) == "tour")
            {
                size++;
            }
        }
    }

    // refreshes the qml list view model
    void TourGUI::enableMicInQMLModel(QString tourName)
    {
        _enableMicForTour = tourName.toStdString();

        if (!_enableMicForTour.empty())
        {
            _updateTourListGUI();
            _enableMicForTour.clear();
            _recordingState = IDLE;
        }
    }
    void TourGUI::_updateTourListGUI()
    {
        if(_menuType == TOUR_MENU)
        {
            const SMCElements::ITourComponent::MapTourToFile &mapTourToFile
                = _tourUIHandler->getTourMap();

            //Populating the list
            QList<QObject*> tourListModel;

            SMCElements::ITourComponent::MapTourToFile::const_iterator consItr = mapTourToFile.begin();
            for( ; consItr != mapTourToFile.end() ; ++consItr)
            {
                tourListModel.append(new IconObject(QString::fromStdString(consItr->first), 
                    "../../Symbols/unknown.png"));
            }

            _setContextProperty("tourListModel", QVariant::fromValue(tourListModel));
        }
        else
        {
            const SMCElements::ITourComponent::MapSequenceToTourList &mapSequenceToTourList = _tourUIHandler->getSequenceList();

            //Populating the list
            QList<QObject*> tourListModel;

            SMCElements::ITourComponent::MapSequenceToTourList::const_iterator consItr = mapSequenceToTourList.begin();
            for( ; consItr != mapSequenceToTourList.end() ; ++consItr)
            {
                // enable mic button if current sequence name = _enableMicForTour
                if ( !_enableMicForTour.empty() &&  (consItr->first == _enableMicForTour))
                {
                    bool audioPresent = _isAudioPresent(consItr->first);

                    tourListModel.append(new IconObject(QString::fromStdString(consItr->first),
                        "../../Symbols/unknown.png", audioPresent, true));

                    QObject* tourMenu = _findChild("tourMenu");
                    if (tourMenu)
                    {
                        tourMenu->setProperty("narrationPresent", audioPresent);
                    }
                }

                else
                {
                    tourListModel.append(new IconObject(QString::fromStdString(consItr->first),
                        "../../Symbols/unknown.png", _isAudioPresent(consItr->first), false));
                }
            }

            // updating model on QML side
            _setContextProperty("tourListModel", QVariant::fromValue(tourListModel));
        }
    }
} // namespace SMCQt
