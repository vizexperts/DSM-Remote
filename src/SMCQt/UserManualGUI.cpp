#include <SMCQt/UserManualGUI.h>
#include <Util/FileUtils.h>
#include <Util/GdalUtils.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, UserManualGUI);
    void UserManualGUI::openUserManual()
    {
        std::string dataDir = UTIL::getDataDirPath();
        std::string fileName = dataDir+"/Help/Help.pdf";
        std::stringstream str;
        str << "cmd.exe /C " << '"' << fileName<< '"';
        UTIL::GDALUtils::execSysCmd(str);
    }
}