/****************************************************************************
*
* File             : ScreenshotGUI.h
* Description      : ScreenshotGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/ScreenShotGUI.h>

#include <Core/InterfaceUtils.h>

#include <App/IApplication.h>
#include <App/IUIHandlerManager.h>

#include <VizQt/QtUtils.h>
#include <VizQt/AdapterWidget.h>

#include <Util/FileUtils.h>

#include <Core/AttributeTypes.h>

#include <osgUtil/Optimizer>
#include <osg/Switch>
#include <osg/CoordinateSystemNode>
#include <osgText/Text>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/DriveManipulator>
#include <osgGA/KeySwitchMatrixManipulator>
#include <osgGA/StateSetManipulator>
#include <osgGA/AnimationPathManipulator>
#include <osgGA/TerrainManipulator>
#include <osgDB/FileUtils>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>

#include <AVR/WindowCaptureCallback.h>

#include <iostream>
#include <sstream>
#include <string.h>

#include <QtCore/QFile>
#include <QtCore/QFileInfo>

#include <osgDB/FileNameUtils>
#include <QFileDialog>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ScreenshotGUI);

    ScreenshotGUI::ScreenshotGUI()
    {
        // Create the temp directory
        osgDB::makeDirectory(UTIL::TemporaryFolder::instance()->getPath() + std::string("/capture_temp_dir"));
    }

    ScreenshotGUI::~ScreenshotGUI()
    {
        _performCleanup();
        setActive(false);
    }

    void ScreenshotGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if( popupLoader != NULL )
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void ScreenshotGUI::connectPopupLoader(QString type)
    {
        if(type == "screenshotMenuObject")
        {
            setActive( true );
            QObject* _screenshotMenuObject = _findChild("screenshotMenuObject");
            if(_screenshotMenuObject)
            {
                // connectivity of signals and slots
                QObject::connect( _screenshotMenuObject, SIGNAL(qmlHandleBrowseButtonClicked()), this,
                    SLOT(_handleBrowseButtonClicked()));
                QObject::connect( _screenshotMenuObject, SIGNAL(qmlHandleCaptureButtonClicked()), this,
                    SLOT(_handleCaptureButtonClicked()));
            }
        }
    }

    void ScreenshotGUI::_performCleanup()
    {
        try
        {
            osg::ref_ptr<osgViewer::View> view = getGUIManager()->getInterface<APP::IManager>(true)
                ->getApplication()->getCompositeViewer()->getView(0);
            if(view.valid())
            {
                osg::ref_ptr<osg::Camera> camera = view->getCamera();
                if(_captureCallback.valid())
                {
                    camera->setFinalDrawCallback(NULL);
                }
            }
        }
        catch(...)
        {
        }
    }

    void ScreenshotGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                // Query the UIHandlerManager
                const APP::ManagerMap& mmap = getGUIManager()->getInterface<APP::IManager>(true)
                    ->getApplication()->getManagerMap();
                CORE::RefPtr<APP::IUIHandlerManager> umgr = CORE::InterfaceUtils::getFirstOf
                    <APP::IManager, APP::IUIHandlerManager>(mmap);
                // Get the UIHandler map
                const APP::UIHandlerMap& umap = umgr->getUIHandlerMap();
                //setScreenshotUIHandler(CORE::InterfaceUtils::getFirstOf<APP::IUIHandler, indiUI::IScreenshotHandler>(umap));                
            }
            catch(...)
            {
                // XXX - Ignoring the message here?
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void ScreenshotGUI::_handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "";
        QString formatFilter = "PNG File (*.png)";
        QString caption = "Png Files";

        QString path = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);

        QObject* _screenshotMenuObject = _findChild("screenshotMenuObject");
        if(_screenshotMenuObject)
        {
            _screenshotMenuObject->setProperty("fileName",QVariant::fromValue(path));
        }
    }


    void ScreenshotGUI::_handleCaptureButtonClicked()
    {
        // Get manager->application->compositeviewer->view[0]->camera->takescreenshot
        try
        {
            // Get file name from exit box
            QObject* _screenshotMenuObject = _findChild("screenshotMenuObject");
            QString filename =  _screenshotMenuObject->property("fileName").toString();
            if(filename == "")
            {
                showError( "Insufficient / Incorrect data", "Output file name is not specified, please browse/specify a file to save as");
                return;
            }

            osg::ref_ptr<osgViewer::View> view = getGUIManager()->getInterface<APP::IManager>(true)
                ->getApplication()->getCompositeViewer()->getView(0);

            if(view.valid())
            {
                // Get the camera
                osg::ref_ptr<osg::Camera> camera = view->getCamera();

                // Create the capture callback
                osg::Viewport* view = camera->getViewport();
                if(!_captureCallback.valid())
                {
                    GLenum readBuffer = GL_BACK;
                    AVR::FramePosition position = AVR::END_FRAME;
                    AVR::Mode mode = AVR::TRIPLE_PBO;
                    AVR::WindowCaptureCallback* callback = new AVR::WindowCaptureCallback(mode, position, readBuffer,
                        (unsigned int)(view->x()),
                        (unsigned int)(view->y()));
                    _captureCallback = callback;
                }

                camera->setFinalDrawCallback(_captureCallback);

                AVR::WindowCaptureCallback* callback = static_cast<AVR::WindowCaptureCallback*>(_captureCallback.get());


                callback->setFilename(filename.toStdString());
                callback->setCapture(true);
            }

            if(_screenshotMenuObject)
                QMetaObject::invokeMethod(_screenshotMenuObject, "closePopup");
        }
        catch(const UTIL::ReturnValueNullException& e)
        {
            e.LogException();
        }
        
    }

    void ScreenshotGUI::setActive(bool value)
    {
        QObject* _screenshotMenuObject = _findChild("screenshotMenuObject");
        if(_screenshotMenuObject)
        {
            _screenshotMenuObject->setProperty("fileName","");
        }

        DeclarativeFileGUI::setActive(value);
    }
} // namespace GUI
