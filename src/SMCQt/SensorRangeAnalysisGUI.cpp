/*****************************************************************************
*
* File             : SensorRangeAnalysisGUI.cpp
* Description      : SensorRangeAnalysisGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <Core/CoreRegistry.h>
#include <Core/IDeletable.h>
#include <Core/ICompositeObject.h>
#include <Core/AttributeTypes.h>
#include <Core/InterfaceUtils.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/ITerrain.h>

#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>

#include <VizUI/UIHandlerManager.h>

#include <VizQt/NameValidator.h>

#include <Terrain/IRasterObject.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <GISCompute/IViewShedVisitor.h>
#include <Elements/ElementsUtils.h>

#include <Util/StringUtils.h>


#include <SMCQt/SensorRangeAnalysisGUI.h>
#include <SMCQt/ViewshedObserverObject.h>
#include <App/IApplication.h>

#include <qcolor.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, SensorRangeAnalysisGUI);

    const std::string SensorRangeAnalysisGUI::SensorRangeAnalysisObjectName = "SensorRangeAnalysisObject";

    SensorRangeAnalysisGUI::SensorRangeAnalysisGUI()
        : _pointHandler(NULL), noOfObjects(0)
    {
    }

    SensorRangeAnalysisGUI::~SensorRangeAnalysisGUI()
    {
        setActive(false);
    }

    void SensorRangeAnalysisGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this,SIGNAL(_updateSuccessStatus()),this,SLOT(_handleSuccessStatus()),Qt::QueuedConnection);
    }

    void SensorRangeAnalysisGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());

                CORE::RefPtr<CORE::IComponent> component = CORE::WorldMaintainer::instance()->getComponentByName("MapSheetInfoComponent");
                if(component.valid())
                {
                    _mapSheetInfoComponent = component->getInterface<ELEMENTS::IMapSheetInfoComponent>();
                    component = NULL;
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            CORE::IPoint* point = _pointHandler->getPoint();
            _setCurrentPoint(point);
            QString longitude = QString::number(point->getX(), 'g', 6);
            QString latitude  = QString::number(point->getY(), 'g', 6);
            QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);
            if(sensorRangeAnalysisObject)
            {
                sensorRangeAnalysisObject->setProperty("leLong",longitude);
                sensorRangeAnalysisObject->setProperty("leLat", latitude );
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            // add object only if filter status is successful
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                // Show result on terrain
                CORE::RefPtr<CORE::IBase> visitor = message.getSender();
                CORE::RefPtr<TERRAIN::IRasterObject> robj = 
                    visitor->getInterface<GISCOMPUTE::IViewshedVisitor>()->getOutput();
                CORE::RefPtr<CORE::IWorld> world = 
                    APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
                world->addObject(robj->getInterface<CORE::IObject>());
                emit _updateSuccessStatus();
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
            {
                emit showError("Filter Status", "Failed to Apply Filter");
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void SensorRangeAnalysisGUI::_handleSuccessStatus()
    {
        QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);
        if(sensorRangeAnalysisObject)
        {
            noOfObjects--;
            if(noOfObjects == 0)
            {
                sensorRangeAnalysisObject->setProperty("calculating",false);
                sensorRangeAnalysisObject->setProperty("startButtonEnabled",true);
                sensorRangeAnalysisObject->setProperty("stopButtonEnabled",false);
            }
        }
    }
    void SensorRangeAnalysisGUI::_handleSensorRangeAnalysisButtonPressed(const std::string& playEvent)
    {
        bool res = false;
        if(playEvent == "Start")
        {
            QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);
            if(sensorRangeAnalysisObject)
            {
                sensorRangeAnalysisObject->setProperty("startButtonEnabled",false);
                sensorRangeAnalysisObject->setProperty("stopButtonEnabled",true);
                sensorRangeAnalysisObject->setProperty("calculating",true);
            }

            res = play();
        }
        else if(playEvent == "Stop")
        {
            QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);
            if(sensorRangeAnalysisObject)
            {
                sensorRangeAnalysisObject->setProperty("startButtonEnabled",true);
                sensorRangeAnalysisObject->setProperty("stopButtonEnabled",false);
                sensorRangeAnalysisObject->setProperty("calculating",false);

            }

            res = stop();
        }

        else if(playEvent == "Exit")
        {
            stop();

            // Clear all objects
            std::map<std::string, CORE::RefPtr<CORE::IObject> >::iterator iter = _objectList.begin();
            for(; iter != _objectList.end(); ++iter)
            {
                _removeObject(iter->second.get());
            }

            _objectList.clear();
            _visitorList.clear();

            if(_currentPoint.valid())
            {
                _removeObject(_currentPoint.get());
                _currentPoint = NULL;
            }

            _reset();
        }
    }
    void SensorRangeAnalysisGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                _reset();

                QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);

                // connect the signal of various buttons in crest clearance qml
                if(sensorRangeAnalysisObject)
                {
                    sensorRangeAnalysisObject->setProperty("startButtonEnabled",true);
                    sensorRangeAnalysisObject->setProperty("stopButtonEnabled",false);

                    QObject::connect( sensorRangeAnalysisObject, SIGNAL(qmlHandleAddButtonClicked()), 
                        this, SLOT(_handleAddButtonClicked()));
                    QObject::connect( sensorRangeAnalysisObject, SIGNAL(qmlHandleRemoveButtonClicked(int)), 
                        this, SLOT(_handleRemoveButtonClicked(int)));
                    QObject::connect( sensorRangeAnalysisObject, SIGNAL(qmlHandleStartButtonClicked()), 
                        this, SLOT(_handleStartButtonClicked()));
                    QObject::connect( sensorRangeAnalysisObject, SIGNAL(qmlHandleStopButtonClicked()), this,
                        SLOT(_handleStopButtonClicked()));
                    QObject::connect( sensorRangeAnalysisObject, SIGNAL(qmlHandleMarkButtonClicked(bool)), 
                        this,SLOT(_handlePBmarkPositionClicked(bool)));
                }
            }
            else
            {
                std::string exit("Exit");
                _handleSensorRangeAnalysisButtonPressed(exit);

            }

            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void SensorRangeAnalysisGUI::_handlePBmarkPositionClicked(bool pressed)
    {
        if(_pointHandler.valid())
        {            
            if(pressed)
            {
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);

                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "Fail to initiate mark observer position");
        }
    }

    bool SensorRangeAnalysisGUI::play()
    {
        bool ret = false;
        _visitorList.clear();

        for(int i = 0; i < _twObserverList.size(); i++)
        {
            QString name = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->name();
            double lat = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->lat();
            double lon = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->longi();
            double height = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->height();
            double range = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->range();
            QColor color = (static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->color();
            osg::Vec4d ocolor = osg::Vec4d(color.red() / 255.0, color.green() / 255.0, color.blue() / 255.0, color.alpha() / 255.0);

            osg::Vec3 pos(lon, lat, height);

            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
            CORE::IBase* applyNode = terrain->getInterface<CORE::IBase>();
            CORE::RefPtr<CORE::IVisitorFactory> vfactory = 
                CORE::CoreRegistry::instance()->getVisitorFactory();
            CORE::RefPtr<CORE::IBaseVisitor> visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::ViewshedVisitorType);
            GISCOMPUTE::IViewshedVisitor* viewshedVisitor = visitor->getInterface<GISCOMPUTE::IViewshedVisitor>();
            viewshedVisitor->setColor(ocolor);
            viewshedVisitor->setName(name.toStdString());
            viewshedVisitor->setPosition(pos);
            viewshedVisitor->setRange(range);
            _visitorList[name.toStdString()] = visitor;
            _subscribe(visitor.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

            applyNode->accept(*visitor);
            ret = true;
        }
        return ret;
    }

    bool SensorRangeAnalysisGUI::stop()
    {
        bool ret = false;
        // Go through the list of filters and stop them
        for(VisitorList::iterator iter = _visitorList.begin();
            iter != _visitorList.end();
            ++iter)
        {
            CORE::RefPtr<CORE::IBaseVisitor> visitor = iter->second;
            try
            {
                visitor->getInterface<GISCOMPUTE::IFilterVisitor>(true)->stopFilter();
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
            ret = true;
        }

        return ret;
    }

    void SensorRangeAnalysisGUI::_handleStartButtonClicked()
    {
        // get Label play Event and handle
        noOfObjects = _twObserverList.size();
        if(!_twObserverList.isEmpty())
        {
            QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);
            std::string pbtext = sensorRangeAnalysisObject->property("startButtonLabel").toString().toStdString();  
            _handleSensorRangeAnalysisButtonPressed("Start");
        }
        else {
            emit showError("Observer Error","Please enter at least one observer parameter");
        }
    }

    void SensorRangeAnalysisGUI::_handleStopButtonClicked()
    {
        // get Label Stop Event and handle
        QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);
        if(sensorRangeAnalysisObject)
        {
            std::string pbtext = sensorRangeAnalysisObject->property("stopButtonLabel").toString().toStdString();  
            _handleSensorRangeAnalysisButtonPressed("Stop");
        }
    }

    //! Called when choose add button pressed
    void SensorRangeAnalysisGUI::_handleAddButtonClicked()
    {
        QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);
        QString mapsheet = sensorRangeAnalysisObject->property("mapsheet").toString();
        QString gr = sensorRangeAnalysisObject->property("gr").toString();
        QString leName = sensorRangeAnalysisObject->property("leName").toString();
        if(leName =="")
        {
            showError("Observer Name", "Please enter a observer name");
            return;
        }

        QColor rectColor = sensorRangeAnalysisObject->property("rectColor").value<QColor>();

        QString leLatTemp = sensorRangeAnalysisObject->property("leLat").toString();
        if(leLatTemp == "" && mapsheet == "")
        {
            showError("Observer Latitude", "Please specify latitude between -90 to 90 or Mapsheet and GR Number");
            return;
        }

        QString leLongTemp = sensorRangeAnalysisObject->property("leLong").toString();
        if(leLongTemp == "" && mapsheet == "")
        {
            showError("Observer Longitude", "Please specify longitude between -180 to 180 or Mapsheet and GR Number");
            return;
        }
        double leLat,leLong;
        if(mapsheet!="")//&& (leLatTemp == "" || leLongTemp == ""))
        {
            osg::Vec2d longLat = _mapSheetInfoComponent->getLatLongFromMapsheetAndGR(mapsheet.toStdString(),gr.toStdString());
            leLat = longLat.y();
            leLong = longLat.x();
        }
        else
        {
            leLat = leLatTemp.toDouble();
            leLong = leLongTemp.toDouble();
        }    

        QString leHeightTemp = sensorRangeAnalysisObject->property("leHeight").toString();
        if(leHeightTemp == "")
        {
            showError("Observer Height", "Please enter a valid height greater than Zero ");
            return;
        }
        double leHeight = leHeightTemp.toDouble();

        QString leRangeTemp = sensorRangeAnalysisObject->property("leRange").toString();
        if(leRangeTemp == "")
        {
            showError("Observer Range", "Please enter a valid integer greater than 10 for Range");
            return;
        }
        double leRange = leRangeTemp.toDouble();

        if(leRange < 10)
        {
            showError("Observer Range", "Please enter a valid integer greater than 10 for Range");
            return;
        }

        // check if the attribute name already exist
        if(! _twObserverList.isEmpty()) 
        {
            int noOfRows = _twObserverList.size();
            for(int i= 0; i < noOfRows; i++)
            {
                if((static_cast<ViewshedObserverObject *>(_twObserverList.at(i)))->name().toStdString() == leName.toStdString())
                {
                    showError("Observer Name", "Observer name already exist");
                    return;
                }
            }
        }

        _addAttributeToTable(leName, rectColor, leLong, leLat, leHeight, leRange);
    }

    void SensorRangeAnalysisGUI::_addAttributeToTable(const QString& leName, const QColor& rectColor,
        double leLong, double leLat, double leHeight, double leRange)
    {

        QObject *object = new ViewshedObserverObject(leName, rectColor, leLong, leLat, leHeight, leRange);
        _twObserverList.append(object);

        _setContextProperty("observerModel", QVariant::fromValue(_twObserverList));


        // Add the current point to object list
        _objectList[leName.toStdString()] = _currentPoint;
        _currentPoint = NULL;
    }

    //! Called when choose remove button pressed
    void 
        SensorRangeAnalysisGUI::_handleRemoveButtonClicked(int currentIndex)
    {

        //XXX AG Check whether condition should be >= or not in second condition
        if(!_twObserverList.isEmpty())
        {

            if(currentIndex < 0 || currentIndex > _twObserverList.size())
            {
                showError( "Remove Attribute", "Please select a Attribute to remove");
                return;
            }
            else
            {
                // delete corresponding point from _objectList
                std::string temp = (static_cast<ViewshedObserverObject *>(_twObserverList.at(currentIndex)))->name().toStdString();
                _objectList.erase(temp);

                _twObserverList.removeAt(currentIndex);
                _setContextProperty("observerModel", QVariant::fromValue(_twObserverList));
            }
        }
        else
        {
            showError("Remove Attribute", " No Attributes in the list to remove");
        }
    }

    void SensorRangeAnalysisGUI::_setCurrentPoint(CORE::IPoint* point)
    {
        _removeObject(_currentPoint.get());
        _currentPoint = NULL;

        _currentPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

    void SensorRangeAnalysisGUI::_reset()
    {
        QObject* sensorRangeAnalysisObject = _findChild(SensorRangeAnalysisObjectName);

        if(sensorRangeAnalysisObject)
        {
            sensorRangeAnalysisObject->setProperty("leLat","");
            sensorRangeAnalysisObject->setProperty("leLong","");
            sensorRangeAnalysisObject->setProperty("leHeight","");
            sensorRangeAnalysisObject->setProperty("leRange","");
            sensorRangeAnalysisObject->setProperty("leName","");
            sensorRangeAnalysisObject->setProperty("rectColor","yellow");
            sensorRangeAnalysisObject->setProperty("startButtonEnabled",true);
            sensorRangeAnalysisObject->setProperty("stopButtonEnabled",false);
            sensorRangeAnalysisObject->setProperty("progressValue",0);

            _twObserverList.clear();
            _setContextProperty("observerModel", QVariant::fromValue(_twObserverList));
        }
    }

} // namespace SMCQt
