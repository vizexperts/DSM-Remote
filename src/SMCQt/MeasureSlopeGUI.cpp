/****************************************************************************
*
* File             : MeasureSlopeGUI.h
* Description      : MeasureSlopeGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <Core/WorldMaintainer.h>
#include <Core/ITerrain.h>
#include <Core/IWorld.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouse.h>
#include <VizUI/ISelectionUIHandler.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <SMCQt/MeasureSlopeGUI.h>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, MeasureSlopeGUI);
    const std::string MeasureSlopeGUI::SlopeAnalysisObjectName = "slopeAnalysis";

    MeasureSlopeGUI::MeasureSlopeGUI()
    {
    }

    MeasureSlopeGUI::~MeasureSlopeGUI()
    {
    }

    void MeasureSlopeGUI::setSlopeEnable(bool value)
    {
        if(getActive() == value)
            return;

        if(value)
        {
            QObject* slopeAnalysisObject = _findChild(SlopeAnalysisObjectName);
            if(slopeAnalysisObject)
            {
               _slopeCalculationHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ISlopeCalculationUIHandler>(getGUIManager());

                connect(slopeAnalysisObject, SIGNAL(markViewPointPosition(bool)),this, 
                    SLOT(_handlePBMarkPointFirstClicked(bool)),Qt::UniqueConnection);

                connect(slopeAnalysisObject, SIGNAL(markLookAtPosition(bool)),this, 
                    SLOT(_handlePBMarkPointSecondClicked(bool)),Qt::UniqueConnection);
            }
        }
        else
        {
            QObject* slopeAnalysisObject = _findChild(SlopeAnalysisObjectName);
            if(slopeAnalysisObject)
            {
                slopeAnalysisObject->setProperty("isMarkViewPointSelected",false);
                slopeAnalysisObject->setProperty("isMarkLookAtSelected",false);
                slopeAnalysisObject->setProperty("value","");
            }
            _state = NO_MARK;
            _slopeCalculationHandler->reset();
            _slopeCalculationHandler = NULL;
            _removeObject(_point1.get());
            _removeObject(_point2.get());

            _point1 = NULL;
            _point2 = NULL;

            _setPointHandlerEnabled(false);

        }

        DeclarativeFileGUI::setActive(value);

    }

    void MeasureSlopeGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the PointUIHandler and set it
            try
            {   
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());                
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            _processPoint();
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void MeasureSlopeGUI::_processPoint()
    {
        if(!_pointHandler.valid())
        {
            LOG_ERROR("Point UI handler not valid");
        }

        CORE::IPoint* point = _pointHandler->getPoint();

        if(point == NULL)
        {
            LOG_ERROR("Point is not valid");
        }

        CORE::IObject *object = point->getInterface<CORE::IObject>();

        if(object == NULL)
        {
            LOG_ERROR("Point Object is not valid");
        }

        switch(_state)
        {
        case POINT1_MARK: 
            {
                // delete previous point 
                _removeObject(_point1.get());
                _point1 = NULL;

                // get pointer of new point
                _point1 = (point) ? point->getInterface<CORE::IObject>() : NULL;
                _point1->getInterface<CORE::IBase>()->setName("Point 1");
                break;
            }

        case POINT2_MARK: 
            {
                // delete previous point 
                _removeObject(_point2.get());
                _point2 = NULL; 

                // get pointer of new point
                _point2 = (point) ? point->getInterface<CORE::IObject>() : NULL;
                _point2->getInterface<CORE::IBase>()->setName("Point 2");
                break;
            }
        default:
            break;
        }
        _slopeCalculation();
    }
    void MeasureSlopeGUI::_slopeCalculation()
    {      
        if(_point1.valid() &&  _point2.valid())
        {
            CORE::IPoint* FromPoint = _point1->getInterface<CORE::IPoint>();
            CORE::IPoint* ToPoint   = _point2->getInterface<CORE::IPoint>();
            _slopeCalculationHandler->setPoint1(FromPoint);
            _slopeCalculationHandler->setPoint2(ToPoint);
            //calcualte Function
           _slopeCalculationHandler->execute();
            std::string slopeval=_slopeCalculationHandler->getSlopeText();
            assert(slopeval.c_str() != NULL);
            QObject* slopeAnalysisObject = _findChild(SlopeAnalysisObjectName);
            if(slopeAnalysisObject)
            {
                    slopeAnalysisObject->setProperty("value", QVariant::fromValue(QString::fromStdString(slopeval)));
                  
            }
        }

    }
    void MeasureSlopeGUI::_handlePBMarkPointFirstClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = POINT1_MARK;
            if(POINT2_MARK == lastState)
            {
                QObject* slopeAnalysisObject = _findChild(SlopeAnalysisObjectName);
                if(slopeAnalysisObject)
                {
                    slopeAnalysisObject->setProperty("isMarkLookAtSelected",false);
                }
            }
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(POINT1_MARK == lastState)
            {   
                _state = NO_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    void MeasureSlopeGUI  ::_handlePBMarkPointSecondClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = POINT2_MARK;
            if(POINT1_MARK == lastState)
            {
                QObject* slopeAnalysisObject = _findChild(SlopeAnalysisObjectName);
                if(slopeAnalysisObject)
                {
                    slopeAnalysisObject->setProperty("isMarkViewPointSelected",false);
                }
            }
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(POINT2_MARK == lastState)
            {
                _state = NO_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }

    }
    void MeasureSlopeGUI::_setPointHandlerEnabled(bool enable)
    {
        if(_pointHandler.valid())
        {
            if(enable)
            {
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);                
            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark trans/recv position");
        }
    }
}
