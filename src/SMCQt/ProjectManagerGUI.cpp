/*****************************************************************************
*
* File             : ProjectManagerGUI.cpp
* Description      : ProjectManagerGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/ProjectManagerGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/IUIHandlerManager.h> 
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <VizUI/UIHandlerManager.h>
#include <VizUI/ISelectionUIHandler.h>

#include <Elements/ElementsUtils.h>

#include <Util/StringUtils.h>
#include <Util/FileUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IMessageFactory.h>
#include <Core/CoreRegistry.h>
#include <Core/GroupAttribute.h>
#include <Core/NamedAttribute.h>
#include <Core/AttributeTypes.h>
#include <Core/ITerrain.h>
#include <Core/IComponent.h>

#include <osgDB/DynamicLibrary>
#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <osgDB/ReaderWriter>

#include <QFileDialog>
#include <QApplication>
#include <QFileInfo>

#include <VizQt/QtUtils.h>
#include <QtUiTools/QUiLoader>
#include <QDesktopWidget>

#include "DBPlugins/curl/ReaderWriterCURL.h"

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>
#include <serialization/IProjectLoadingStatusMessage.h>
#include <serialization/IProjectLoaderComponent.h>

#include <DB/WriteFile.h>
#include <DB/ReadFile.h>
#include <Curl/EasyCurl.h>

#include <Terrain/IElevationObject.h>
#include <Terrain/IRasterObject.h>
#include <Terrain/IVectorObject.h>

#include <Core/IFeatureLayer.h>
#include <Elements/ElementsPlugin.h>
#include <Core/IDeletable.h>

#include <QJSONDocument>
#include <QJSONObject> 

#define BOOST_FILESYSTEM_VERSION 3

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <SMCQt/PDFView.h>
#include <QtCore/QJsonArray>
#include <View/ICameraComponent.h>

//const std::string defaultProjectFile = "../../tests/rasterobject.xml";
const std::string defaultProjectFile = "../../tests/Default.pro";

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ProjectManagerGUI);
    DEFINE_IREFERENCED(ProjectManagerGUI, CORE::Base);

    const std::string ProjectManagerGUI::PLANNERDIRECTORY = "Plan";
    const std::string ProjectManagerGUI::TEMPLATEDIRECTORY = "Template";

    const std::string ProjectManagerGUI::PLANNERFILEEXTENSION = "plan";
    const std::string ProjectManagerGUI::TEMPLATEFILEEXTENSION = "pro";

    const std::string ProjectManagerGUI::VizStyleObjectName = "vizStyle";

    const std::string ProjectManagerGUI::PLANNERSERVERFOLDER = "Plan";
    const std::string ProjectManagerGUI::TEMPLATESERVERFOLDER = "Template";

    const std::string ProjectManagerGUI::SERVERPLANNERDIRECTORY = "Server Plans";

    const std::string ProjectManagerGUI::SERVERTEMPLATEFILE = "ServerSetting.json";

    // smtemplate menu string value
    const std::string ProjectManagerGUI::smpTemplatesMenuObjectName = "smpTemplatesMenu";

    ProjectManagerGUI::ProjectManagerGUI()
        : _projectType(SMCUI::ILookUpProjectUIHandler::PLANNER)
        , _lookUpUIHandler(NULL)
        , _globalSettingComponent(NULL)
        , _plannerListModel(new FilesListModel)
        , _activeTab(PLANNER)
        , _applicationName("")
        , _windowScaleFactor(1.5)
        , _aspectRatio(1.33)
        , _loadingProject(false)
        , _strTemlateName("")
    {
        _calcNormalWindowWidth();
    }

    ProjectManagerGUI::~ProjectManagerGUI(){}

    void ProjectManagerGUI::_loadAndSubscribeSlots()
    {

        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* desktop = _findChild("desktop");
        if (desktop != NULL)
        {
            QObject::connect(desktop, SIGNAL(logoff()), this, SLOT(logoff()), Qt::UniqueConnection);

            QObject::connect(desktop, SIGNAL(loadFiles()), this, SLOT(loadFiles()), Qt::UniqueConnection);

            QObject::connect(desktop, SIGNAL(createNewProject(QString)), this,
                SLOT(newProjectName(QString)), Qt::UniqueConnection);

            QObject::connect(desktop, SIGNAL(loadProject(QString, int)), this,
                SLOT(loadProject(QString, int)), Qt::UniqueConnection);

            QObject::connect(desktop, SIGNAL(deleteProject(QString, int)), this,
                SLOT(confirmDeleteProject(QString, int)), Qt::UniqueConnection);

            QObject::connect(desktop, SIGNAL(closeApplication()), this,
                SLOT(confirmCloseApplication()), Qt::UniqueConnection);

            QObject::connect(desktop, SIGNAL(searchProjects(QString, int)), this,
                SLOT(searchProjects(QString, int)), Qt::UniqueConnection);

            //This will set the active tab whenever user clicks on the tabs of the tabFrame
            QObject::connect(desktop, SIGNAL(activeTab(int)), this,
                SLOT(setActiveTab(int)), Qt::UniqueConnection);

            QObject::connect(this, SIGNAL(_setProgressValue(int)), this,
                SLOT(_onSetProgressValue(int)), Qt::QueuedConnection);
        }

        QObject* smpFileMenu = _findChild("smpFileMenu");
        if (smpFileMenu)
        {
            QObject::connect(smpFileMenu, SIGNAL(closeProject()), this, SLOT(closeProject()), Qt::UniqueConnection);
            QObject::connect(smpFileMenu, SIGNAL(save()), this, SLOT(projectSave()), Qt::UniqueConnection);
            QObject::connect(smpFileMenu, SIGNAL(saveAs()), this, SLOT(projectSaveAs()), Qt::UniqueConnection);
        }
        // Getting template menu object.
        QObject *smTemplateMenu = _findChild("smpTemplatesMenu");
        if (smTemplateMenu)
        {
            QObject::connect(smTemplateMenu, SIGNAL(deleteTemplate(QString)), this, SLOT(deleteTemplate(QString)));
        }
        qmlRegisterType<PDFView>("PDFView", 1, 0, "PDFView");
    }

    void ProjectManagerGUI::setActiveTab(int currTabID)
    {
        QObject* desktop = _findChild("desktop");
        if (desktop != NULL)
        {
            //Comparing the IDS
            QVariant intVariant = desktop->property("tabType_SMP");
            if (intVariant.toInt() == currTabID)
            {
                _activeTab = PLANNER;
            }
        }
    }

    void ProjectManagerGUI::closeProject()
    {
        //! If the current project is a template
        if (_projectType == SMCUI::ILookUpProjectUIHandler::TEMPLATE)
        {
            openDesktop();
        }

        //! Save the current project and exit to desktop
        else
        {
            emit question("Closing Project", "Do you want to save the current project ?", true,
                "projectSaveOnClose()", "openDesktop()");
        }
    }

    void ProjectManagerGUI::projectSaveOnClose()
    {
        QObject::connect(this, SIGNAL(executed()), this, SLOT(openDesktop()), Qt::QueuedConnection);

        _loadBusyBar(boost::bind(&ProjectManagerGUI::_executeProjectSave, this), "Saving Project");
    }

    void ProjectManagerGUI::openDesktop()
    {
        QObject* desktop = _findChild("desktop");
        if (desktop)
        {
            desktop->setProperty("minimized", QVariant::fromValue(false));
        }

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QWidget* mainWindow = qtapp->getLayoutWidget();

        if (mainWindow)
        {
            mainWindow->setWindowTitle(QString::fromStdString("GeorbIS : " + _presentProject));
        }

        CORE::IComponent* cameraComponent = ELEMENTS::GetComponentFromMaintainer("CameraComponent");
        CORE::RefPtr<VIEW::ICameraComponent> _cameraComponent = cameraComponent->getInterface<VIEW::ICameraComponent>();
        if(_cameraComponent.valid() && _cameraComponent->isOrtho())
        _cameraComponent->setOrtho(false);
        QObject::disconnect(this, SIGNAL(executed()), this, SLOT(openDesktop()));
    }

    void ProjectManagerGUI::confirmCloseApplication()
    {
        emit question("Closing Application", "Do you want to exit ?", false,
            "closeApplicationOkClicked()", "emptySlot()");

    }

    void ProjectManagerGUI::closeApplicationOkClicked()
    {
        getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getQApplication()->closeAllWindows();
    }

    void ProjectManagerGUI::_assignSignalForSaveAsAndExport()
    {
        QObject* saveAsProject = _findChild("saveAsProject");
        if (saveAsProject != NULL)
        {
            QObject::connect(saveAsProject, SIGNAL(projectSaveAs(QString)),
                this, SLOT(projectSaveAsProject(QString)), Qt::UniqueConnection);
        }
    }

    void ProjectManagerGUI::logoff()
    {
        try
        {
            CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            QWidget* mainWindow = qtapp->getLayoutWidget();

            mainWindow->showNormal();
        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ProjectManagerGUI::loadFiles()
    {
        try
        {
            QObject::connect(this, SIGNAL(executed()), this, SLOT(populateProjectList()), Qt::QueuedConnection);

            _loadBusyBar(boost::bind(&ProjectManagerGUI::_executeLoadFiles, this), "Please wait while loading project list \n User interface is not accessible");

        }
        catch (const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ProjectManagerGUI::populateProjectList()
    {
        // populate recentPlanFiles
        QStringList recentPlanFiles;
        recentPlanFiles.clear();
        for (unsigned int i = 0; i < _recentPlannerProjects.size(); i++)
        {
            recentPlanFiles.append(QString::fromStdString(_recentPlannerProjects[i]));
        }

        // populate Templates
        QList<QObject*> qTemplateProjectListModel;
        qTemplateProjectListModel.clear();
        for (unsigned int i = 0; i < _templateProjects.size(); i++)
        {
            qTemplateProjectListModel.append(new VizComboBoxElement(QString::fromStdString(_templateProjects[i]), QString::number(i)));
        }

        int index = 0;
        _plannerListModel->clear();
        while (index < _plannerProjectList.size())
        {
            QPair<QString, QString> project = _plannerProjectList[index++];

            _plannerListModel->addFile(project.first, project.second);
        }

        _plannerListModel->sort();

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {

            qtapp->getRootContext()->setContextProperty("planFiles", QVariant::fromValue(NULL));
            qtapp->getRootContext()->setContextProperty("planFiles", _plannerListModel);
            qtapp->getRootContext()->setContextProperty("recentPlanFiles", QVariant::fromValue(recentPlanFiles));
            qtapp->getRootContext()->setContextProperty("templateProjectsList", QVariant::fromValue(qTemplateProjectListModel));
        }

        QObject::disconnect(this, SIGNAL(executed()), this, SLOT(populateProjectList()));
    }

    void ProjectManagerGUI::_getProjectSettings()
    {
        ELEMENTS::ISettingComponent::SettingsAttributes setting = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if (setting.keyValuePair.find("Server") != setting.keyValuePair.end())
            _webServerAddress = setting.keyValuePair["Server"];

        if (setting.keyValuePair.find("Username") != setting.keyValuePair.end())
            _webserverUsername = setting.keyValuePair["Username"];

        if (setting.keyValuePair.find("Password") != setting.keyValuePair.end())
            _webserverPassword = setting.keyValuePair["Password"];

        if (setting.keyValuePair.find("Path") != setting.keyValuePair.end())
            _projectLocationOnServer = setting.keyValuePair["Path"];

        setting = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::PROJECTDATABASECONN);
        if (setting.keyValuePair.find("Database") != setting.keyValuePair.end())
            _databaseToConnect = setting.keyValuePair["Database"];
    }

    bool ProjectManagerGUI::_presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap, const std::string& keyword)
    {
        if (settingMap.find(keyword) != settingMap.end())
        {
            return true;
        }
        return false;
    }

    bool ProjectManagerGUI::_getDatabaseCredential(const std::string& database)
    {
        if (!_globalSettingComponent.valid() || database.empty())
            return false;

        ELEMENTS::ISettingComponent* settingComp = _globalSettingComponent->getInterface<ELEMENTS::ISettingComponent>();
        // Then Get the Database Information(credentials) of this Database
        ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = settingComp->getGlobalSetting(ELEMENTS::ISettingComponent::PROJECTDATABASECONN);

        //Filling the credential in the database options 
        //_databaseCredential.connectionName = CORE::UniqueID::CreateUniqueID()->toString();
        bool isValid = true;

        //Done this becoz this Code Region is Prone to Crash in case the Key is not found in map Eg:- settingAttrib.keyValuePair["database"] 
        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Database"))
            _databaseCredential.database = settingAttrib.keyValuePair["Database"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Host"))
            _databaseCredential.hostname = settingAttrib.keyValuePair["Host"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Port"))
            _databaseCredential.port = settingAttrib.keyValuePair["Port"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Username"))
            _databaseCredential.username = settingAttrib.keyValuePair["Username"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Password"))
            _databaseCredential.password = settingAttrib.keyValuePair["Password"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "CreatorSchema"))
            _databaseCredential.creatorSchemaName = settingAttrib.keyValuePair["CreatorSchema"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "CreatorTable"))
            _databaseCredential.creatortableName = settingAttrib.keyValuePair["CreatorTable"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "PlannerSchema"))
            _databaseCredential.plannerSchemaName = settingAttrib.keyValuePair["PlannerSchema"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "PlannerTable"))
            _databaseCredential.plannertableName = settingAttrib.keyValuePair["PlannerTable"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "TemplateSchema"))
            _databaseCredential.templateSchemaName = settingAttrib.keyValuePair["TemplateSchema"];
        else
            isValid = false;

        if (_presentInGlobalSettingMap(settingAttrib.keyValuePair, "TemplateTable"))
            _databaseCredential.templateTableName = settingAttrib.keyValuePair["TemplateTable"];
        else
            isValid = false;

        return isValid;
    }

    std::string ProjectManagerGUI::_getProjectLocationOnServer()
    {
        std::string webLink;
        webLink += "http://";
        webLink += _webServerAddress;
        if (!_projectLocationOnServer.empty())
            webLink += "/" + _projectLocationOnServer;

        return webLink;
    }

    void ProjectManagerGUI::_executeLoadFiles()
    {
        if (!_globalSettingComponent.valid())
            return;

        bool isLocal = _globalSettingComponent->isLocalProject();
        if (isLocal)
        {
            _getTheAvailableLocalProjects();
            return;
        }
        // for server

        if (!_projectSaveAndLoadComponent.valid())
            return;

        _getProjectSettings();
        //Here we will set the database Credentail taken from the XML config File 
        // And also set the Schema and Table name of Creator and Planner also read from XML config file
        if (!_getDatabaseCredential(_databaseToConnect))
        {
            emit showError("Database Connection", "Not able to get Database Credential");
            return;
        }

        std::string link = _getProjectLocationOnServer();

        if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
        {

            emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection status");
            _globalSettingComponent->setLocalProject(true);
            _getTheAvailableLocalProjects();
            return;
        }
        _plannerProjectList.clear();
        _recentPlannerProjects.clear();


        // lookup Ui handler to get data from database
        if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
        {
            emit showError("Database Connection", "Not able to make connection to Database");
            return;
        }
        //Get the Projects from the database
        std::vector<std::pair<std::string, std::string> > tempList = _lookUpUIHandler->
            getProjectListWithCreationTime(SMCUI::ILookUpProjectUIHandler::PLANNER);

        for (unsigned int index = 0; index < tempList.size(); index++)
        {
            std::pair<std::string, std::string> project = tempList[index];

            QString creationDateStr = QString::fromStdString(project.second).simplified();
            QDateTime creationDateTime = QDateTime::fromString(creationDateStr, "dd MMMM yyyy HH:mm:ss.zzz");
            QString groupName = creationDateTime.toString("MMMM yyyy");

            _plannerProjectList.append(qMakePair(QString::fromStdString(project.first), groupName));
        }

        _recentPlannerProjects = _lookUpUIHandler->getRecentProjectsList(SMCUI::ILookUpProjectUIHandler::PLANNER);

        // list of template projects

        //Get the Template list from the database
        _templateProjects.clear();

        std::vector<std::pair<std::string, std::string> > templateList = _lookUpUIHandler->getProjectListFromProjectMap(SMCUI::ILookUpProjectUIHandler::TEMPLATE);

        for (unsigned int index = 0; index < templateList.size(); index++)
        {
            std::pair<std::string, std::string> project = templateList[index];
            _templateProjects.push_back(project.first);

        }

        _templateProjects.push_back("Default"); 

        _lookUpUIHandler->closeDatabaseConnection();

        return;
    }

    void ProjectManagerGUI::newProjectName(QString projectName)
    {
        if (projectName.isEmpty())
            return;

        if (!_globalSettingComponent.valid())
            return;

        bool isLocal = _globalSettingComponent->isLocalProject();

        _newCreatorProjectName = projectName.toStdString();

        if (_projectAlreadyProject(_newCreatorProjectName, SMCUI::ILookUpProjectUIHandler::PLANNER))
        {
            QString msg = " \'" + projectName + "\' already exists. Please try a different name.";
            emit showError("New Project", msg);
            return;
        }
        else
        {
            _createNewProject(_newCreatorProjectName);
        }
    }

    void ProjectManagerGUI::_addEntryToDatabase(const std::string& projectName,
        const SMCUI::ILookUpProjectUIHandler::NewOrSaveType& type,
        const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType)
    {
        if (!_lookUpUIHandler.valid())
            return;

        std::string filePath = osgDB::getFilePath(projectName);
        std::string fileName = osgDB::getStrippedName(projectName);
        std::string ext = osgDB::getFileExtension(projectName);

        if (type == SMCUI::ILookUpProjectUIHandler::NEWPROJECT)
        {
            //Check if the Same Projcet name then take the UID update it 
            std::string uid = _lookUpUIHandler->getProjectUIDByName(fileName, projectType);
            if (uid.empty())
            {
                uid = CORE::UniqueID::CreateUniqueID()->toString();
                _lookUpUIHandler->addOrUpdateProjectName(fileName, uid, type, projectType);
            }
            else
            {
                //If the project already exits , then show error
                std::string msg = "\'" + fileName + "\' already exists. Please give a different name.";
                emit showError(" Project Name", QString::fromStdString(msg));
            }
        }
        else if (type == SMCUI::ILookUpProjectUIHandler::SAVEPROJECT)
        {
            _currentProjectUID = _lookUpUIHandler->getProjectUIDByName(fileName,
                SMCUI::ILookUpProjectUIHandler::PLANNER);

            if (_currentProjectUID.empty())
                return;

            _lookUpUIHandler->addOrUpdateProjectName(fileName, _currentProjectUID, type, projectType);
        }

        if (type == SMCUI::ILookUpProjectUIHandler::SAVEASPROJECT)
        {
            std::string uid = _lookUpUIHandler->getProjectUIDByName(fileName, projectType);
            if (uid.empty())
            {
                std::string uid = CORE::UniqueID::CreateUniqueID()->toString();
                _lookUpUIHandler->addOrUpdateProjectName(fileName, uid,
                    SMCUI::ILookUpProjectUIHandler::NEWPROJECT, projectType);
            }
            else
            {
                //If the project already exits , then show error
                std::string msg = "\'" + fileName + "\' already exists. Please give a different name.";
                emit showError(" Project Name", QString::fromStdString(msg));
            }
        }

        if (type == SMCUI::ILookUpProjectUIHandler::EXPORTASPLAN)
        {
            std::string planUID = CORE::UniqueID::CreateUniqueID()->toString();
            _lookUpUIHandler->addOrUpdateProjectName(fileName, planUID,
                SMCUI::ILookUpProjectUIHandler::NEWPROJECT, SMCUI::ILookUpProjectUIHandler::PLANNER);
        }
    }

    void ProjectManagerGUI::_uploadDefaultProjectToServerNamedAs(const std::string& newName)
    {
        if (!_projectSaveAndLoadComponent.valid())
        {
            emit showError("Project Load And Save", "Not able to Upload to webserver");
            return;
        }

        std::string serverLocationToUpload = _getProjectLocationOnServer();

        std::string defaultTemplate = _getDefaultTemplateNameFromServer();
        if (defaultTemplate == ""){
            osgDB::copyFile(defaultProjectFile, newName);
        }
        else{
            copyDefaultTemplateToProjectLocation(defaultTemplate,newName);
        }
        std::string projectFolder = osgDB::getFilePath(newName);

        std::string link;

        std::string fileName = osgDB::getSimpleFileName(osgDB::getNameLessAllExtensions(newName));

        std::string ext = osgDB::getFileExtension(newName);
        //make the link for the ZIP File
        link = serverLocationToUpload + "/" + PLANNERSERVERFOLDER;

        _projectSaveAndLoadComponent->saveProject(fileName, projectFolder, link, true);
    }

    void ProjectManagerGUI::_createNewProject(const std::string& newProjectName)
    {
        if (!_globalSettingComponent.valid())
            return;

        bool isLocalProject = _globalSettingComponent->isLocalProject();

        std::string projectFilename;
        if (isLocalProject)
        {
            std::string localProjectLocation = _globalSettingComponent->getLocalProjectLocation();

            if (_activeTab == PLANNER)
            {
                projectFilename = localProjectLocation + "/" + PLANNERDIRECTORY + "/" + newProjectName + "/" + newProjectName + "." + PLANNERFILEEXTENSION;
            }
        }
        else{
            std::string link = _getProjectLocationOnServer();

            if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
            {

                emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection status");
                return;
            }

            // There is not existing Database connection then return;
            ELEMENTS::ISettingComponent::SettingsAttributes presentDatabaseSetting = _globalSettingComponent->getGlobalSetting
                (ELEMENTS::ISettingComponent::PROJECTDATABASECONN);

            if (!_getDatabaseCredential(_databaseToConnect))
                return;

            if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
            {
                emit showError("Database Connection", "NOt able to connect to the Database");
                return;
            }

            std::string newModifiedProjectName = newProjectName;
            if (_activeTab == PLANNER)
                _addEntryToDatabase(newModifiedProjectName, SMCUI::ILookUpProjectUIHandler::NEWPROJECT, SMCUI::ILookUpProjectUIHandler::PLANNER);
            else
                _addEntryToDatabase(newModifiedProjectName, SMCUI::ILookUpProjectUIHandler::NEWPROJECT, SMCUI::ILookUpProjectUIHandler::TEMPLATE);

            _lookUpUIHandler->closeDatabaseConnection();
        }

        if (osgDB::convertToLowerCase(_getPresentProject()) == osgDB::convertToLowerCase(newProjectName))
        {
            _currentProjectUID = "";
        }

        /* UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
         std::string temporaryPath = osgDB::convertFileNameToNativeStyle(temporaryInstance->getPath());
         */
        std::string temporaryPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "/Project";
        if (isLocalProject)
        {
            projectFilename = osgDB::convertFileNameToNativeStyle(projectFilename);
            _copyDefaultProjectTo(projectFilename);
        }
        else{
            projectFilename = osgDB::convertFileNameToNativeStyle(temporaryPath) + "/" + SERVERPLANNERDIRECTORY + "/" + newProjectName + "/" + newProjectName + "." + PLANNERFILEEXTENSION;
            _uploadDefaultProjectToServerNamedAs(projectFilename);
        }

        loadFiles();
    }

    std::string ProjectManagerGUI::_getDefaultTemplateNameFromServer(){

        std::string projectName;

        std::string link = _getProjectLocationOnServer();
        //! downlaod serverSetting.json File from server
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();
        std::string fileLocation = link + "/" + SERVERTEMPLATEFILE;

        _projectSaveAndLoadComponent->downloadServerSettingFile(appdataPath + "/" + SERVERTEMPLATEFILE, fileLocation);
        //std::string link = _getProjectLocationOnServer();
        //_projectSaveAndLoadComponent->downloadServerTemplateFile();
        //! read the file 
        //! read defaultTemplate tag from the file
        std::string serverTemplateFile = appdataPath + "/" + SERVERTEMPLATEFILE;

        QFile settingFile(QString::fromStdString(serverTemplateFile));
        if (settingFile.open(QIODevice::ReadOnly))
        {
            QByteArray settingData = settingFile.readAll();

            QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

            QJsonObject settingObject = settingDoc.object();

            QString defaultTemplate = settingObject["DefaultTemplate"].toString();

            if (!defaultTemplate.isEmpty())
            {
                projectName = defaultTemplate.toStdString();
            }
        }

        return projectName;
    }

    void ProjectManagerGUI::copyDefaultTemplateToProjectLocation(std::string _templateName,std::string projectLocation){
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string templateLocation = appdataPath + "/" + "Project/" + SERVERPLANNERDIRECTORY + "/" +_templateName;

        std::string newProjectLocation = QFileInfo(QString::fromStdString(projectLocation)).dir().absolutePath().toStdString();

        // copy all the files from template directory to project directory
        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(templateLocation);
        int index = 0;
        int size = contents.size();
        while (index < size)
        {
            std::string fileName = contents[index++];
            if ((fileName == ".") || (fileName == ".."))
                continue;

            std::string ext = osgDB::getFileExtension(fileName);
            //if( ( ext != "sqlite" ) && ( ext != CREATORFILEEXTENSION ) && ( ext != PLANNERFILEEXTENSION) 
            //    && ( ext != "zip") && ( ext != TKFILEEXTENSION) && ( ext != TEMPLATEFILEEXTENSION))
            {
                std::string realPath = templateLocation + "/" + fileName;
                std::string destination = newProjectLocation + "/" + fileName;
                osgDB::copyFile(realPath, destination);
            }
        }

        // copy files from projectLocation to new Project Location
        //planner file extension is used because templates in case of server are stored in .plan format
        //! XXX need to change it to pro extension

        osgDB::copyFile(newProjectLocation +"/"+_templateName+"." + PLANNERFILEEXTENSION, projectLocation);

        // remove template named Files
        remove((newProjectLocation + "/" + _templateName + "." + PLANNERFILEEXTENSION).c_str());
        remove((newProjectLocation + "/" + _templateName + ".zip").c_str());
    }

    void ProjectManagerGUI::_copyDefaultProjectTo(std::string projectFilename)
    {
        //Move the default preojcet to save it as LocalProject
        osgDB::makeDirectoryForFile(projectFilename);

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");
        if (!settingFile.open(QIODevice::ReadOnly))
        {
            osgDB::copyFile(defaultProjectFile, projectFilename);
            return;
        }

        QByteArray settingData = settingFile.readAll();

        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

        QJsonObject settingObject = settingDoc.object();

        QString defaultTemplate = settingObject["DefaultTemplate"].toString();

        if (defaultTemplate.isEmpty())
        {
            osgDB::copyFile(defaultProjectFile, projectFilename);
            return;
        }

        //! copy template to the project directory
        std::string localProjectLocation = _globalSettingComponent->getLocalProjectLocation();
        std::string templateLocation = localProjectLocation + "/" + TEMPLATEDIRECTORY + "/" + defaultTemplate.toStdString();
        std::string newProjectLocation = QFileInfo(QString::fromStdString(projectFilename)).dir().absolutePath().toStdString();

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(templateLocation);
        int index = 0;
        int size = contents.size();
        while (index < size)
        {
            std::string fileName = contents[index++];
            if ((fileName == ".") || (fileName == ".."))
                continue;

            std::string ext = osgDB::getFileExtension(fileName);
            //if( ( ext != "sqlite" ) && ( ext != CREATORFILEEXTENSION ) && ( ext != PLANNERFILEEXTENSION) 
            //    && ( ext != "zip") && ( ext != TKFILEEXTENSION) && ( ext != TEMPLATEFILEEXTENSION))
            {
                std::string realPath = templateLocation + "/" + fileName;
                std::string destination = newProjectLocation + "/" + fileName;
                osgDB::copyFile(realPath, destination);
            }
        }

        osgDB::copyFile(newProjectLocation + "/" + defaultTemplate.toStdString() + "." + TEMPLATEFILEEXTENSION, projectFilename);

        temporaryInstance->deleteFile(newProjectLocation + "/" + defaultTemplate.toStdString() + "." + TEMPLATEFILEEXTENSION);
    }

    void ProjectManagerGUI::_getLocalProject(const std::string& projectLocation, std::vector<std::string>& creatorOrPlannerList, int limit)
    {

        QDir dir(projectLocation.c_str());
        QFileInfoList dirList = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Time);

        if (dirList.size() < limit)
        {
            limit = dirList.size();
        }

        for (int i = 0; i < limit; i++)
        {
            creatorOrPlannerList.push_back(dirList.at(i).baseName().toStdString());
        }

        //adding default template
        creatorOrPlannerList.push_back("Default");

        /*osgDB::DirectoryContents content = osgDB::getDirectoryContents(projectLocation);
        int totalContentSize = content.size();
        int index = 0;
        while(index < totalContentSize)
        {
        std::string fileName = content[index++];

        if((fileName == ".") || (fileName == ".."))
        continue;

        std::string absolutefile= osgDB::convertFileNameToNativeStyle(projectLocation) + "/" + fileName;
        osgDB::FileType type = osgDB::fileType(absolutefile);
        if(type == osgDB::DIRECTORY)
        {
        creatorOrPlannerList.push_back(fileName);
        if(creatorOrPlannerList.size() > 6)
        return;
        }
        }*/
    }

    void ProjectManagerGUI::_getTheAvailableLocalProjects()
    {
        if (!_globalSettingComponent.valid())
            return;

        std::string projectLocation = _globalSettingComponent->getLocalProjectLocation();

        _plannerProjectList.clear();
        _recentPlannerProjects.clear();
        _templateProjects.clear();


        std::string planProjectPath = osgDB::convertFileNameToNativeStyle(projectLocation + "/" + PLANNERDIRECTORY);

        std::string templateProjectPath = osgDB::convertFileNameToNativeStyle(projectLocation + "/" + TEMPLATEDIRECTORY);

        osgDB::makeDirectory(planProjectPath);

        _getLocalProject(planProjectPath, _recentPlannerProjects);
        _getLocalProject(templateProjectPath, _templateProjects, INT_MAX);
        _getLocalProjectDateWise(planProjectPath, 1);
    }

    void ProjectManagerGUI::_getLocalProjectDateWise(const std::string& projectLocation, int type)
    {
        QDir dir(projectLocation.c_str());
        QFileInfoList dirList = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Time);
        int index = 0;
        int size = dirList.size();
        while (index < size)
        {
            if (dirList.at(index).isFile())
            {
                index++;
                continue;
            }

            QFileInfo folderName = dirList.at(index++);
            std::string name = folderName.baseName().toStdString();
            std::string date = folderName.lastModified().toString().toStdString();
            QString creationDateStr = QString::fromStdString(date.c_str()).simplified();

            QDateTime creationDateTime = QDateTime::fromString(creationDateStr, "ddd MMM dd HH:mm:ss yyyy");

            QString groupName = creationDateTime.toString("MMMM yyyy");
            switch (type)
            {

            case 1:
                _plannerProjectList.append(qMakePair(QString::fromStdString(name), groupName));
                break;

            }
        }
    }

    bool ProjectManagerGUI::_localProjectAlreadyPresent(const std::string& projectName, const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType)
    {

        if (!_globalSettingComponent.valid())
            return false;

        std::string projectLocation = _globalSettingComponent->getLocalProjectLocation();

        std::string projectDir;

        if (projectType == SMCUI::ILookUpProjectUIHandler::PLANNER)
        {
            projectDir = PLANNERDIRECTORY;
        }
        else if (projectType == SMCUI::ILookUpProjectUIHandler::TEMPLATE)
        {
            projectDir = TEMPLATEDIRECTORY;
        }

        projectLocation += "/" + projectDir + "/" + projectName;

        projectLocation = osgDB::convertFileNameToNativeStyle(projectLocation);
        return osgDB::fileExists(projectLocation);
    }


    void ProjectManagerGUI::projectSave()
    {
        _loadBusyBar(boost::bind(&ProjectManagerGUI::_executeProjectSave, this), "Saving Project");
    }

    void ProjectManagerGUI::_executeProjectSave()
    {
        if (!isProjectSavable)
        {
            emit showError("Saving Project", "Saving template is not supported. Please use SaveAs Option");
            return;
        }
        SaveFile();
    }

    //This is just for assignment the Slot where the actual Save and ExportToPlan Slot to be mapped
    void ProjectManagerGUI::projectSaveAs()
    {
        if (!_dummyUID.empty())
        {
            emit showError("Project Save", "Cannot Save or Export a Dummy Project");
            return;
        }
        _assignSignalForSaveAsAndExport();
    }

    void ProjectManagerGUI::_setProjectType(const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType)
    {
        _projectType = projectType;
    }

    void ProjectManagerGUI::projectSaveAsProject(QString fileName)
    {
        _setProjectType(SMCUI::ILookUpProjectUIHandler::PLANNER);

        QObject* saveAsProject = _findChild("saveAsProject");

        _loadBusyBar(boost::bind(&ProjectManagerGUI::_executeSaveAs, this, fileName), "Saving Project");

    }

    void ProjectManagerGUI::_executeSaveAs(QString fileName)
    {
        std::string filename = fileName.toStdString();
        if (fileName != "")
        {
            if (!_dummyUID.empty())
            {
                emit showError("Saving Project", "Cannot Save A dummy project");
                return;
            }


            _newProjectName = filename;

            SMCUI::ILookUpProjectUIHandler::ProjectType projectType;

            projectType = SMCUI::ILookUpProjectUIHandler::PLANNER;


            if (_projectAlreadyProject(_newProjectName, projectType))
            {
                QObject* smpNewProject = _findChild("saveAsProject");
                smpNewProject->setProperty("projectName", "");
                QString msg = " \' " + fileName + " \' already exists. Please give a different name.";
                emit showError(" Project Name", msg);
                return;
            }
            else
            {
                SaveAsFile(_newProjectName, projectType);
            }
            _resetPopUpGUI();
        }
        else
        {
            emit showError("Project Name is empty", "Please Enter the Project Name");
        }
    }

    void ProjectManagerGUI::projectSaveAsTemplate(QString name)
    {
        if (name.isEmpty())
        {
            emit showError("Save as template", "Please specify a project name.");
            return;
        }

        //since _projectAlreadyProject checks only for templates in appdata 
        //rather than saving path information of default template, just doing a check 
        if (name.contains("Default"))
        {
            emit showError("Save as template", "Default template already exists. Please give a different name.");
            return;
        }

        SMCUI::ILookUpProjectUIHandler::ProjectType projectType = SMCUI::ILookUpProjectUIHandler::TEMPLATE;

        if (_projectAlreadyProject(name.toStdString(), projectType))
        {
            QObject* smpNewProject = _findChild("saveAsTemplate");
            smpNewProject->setProperty("templateName", "");
            QString msg = " \' " + name + " \' already exists. Please give a different name.";
            emit showError(" Template Name", msg);
            return;
        }
        else
        {
            _newProjectName = name.toStdString();
            SaveAsFile(_newProjectName, SMCUI::ILookUpProjectUIHandler::TEMPLATE);
        }

        // populate Templates
        _templateProjects.push_back(_newProjectName);
        QList<QObject*> qTemplateProjectListModel;
        qTemplateProjectListModel.clear();
        for (unsigned int i = 0; i < _templateProjects.size(); i++)
        {
            qTemplateProjectListModel.append(new VizComboBoxElement(QString::fromStdString(_templateProjects[i]), QString::number(i)));
        }

        _setContextProperty("templateProjectsList", QVariant::fromValue(qTemplateProjectListModel));

        //set the created template as default template
        setDefaultTemplate(QString::fromStdString(_newProjectName));

        //! close the popup..
        _resetPopUpGUI();

    }

    void ProjectManagerGUI::setDefaultTemplate(QString name)
    {
        //conveting name to uppercase as template list downloaded from server has uppercase
        name = name.toUpper();

        // return condition changed so we can set empty string as default template name when every template is deleted.
        //if ( _templateProjects.size()== 1)
         //   return;

        if (_globalSettingComponent->isLocalProject()){
            //! Save in AppData
            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();

            QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");
            QByteArray settingData;
            if (settingFile.open(QIODevice::ReadOnly))
            {
                settingData = settingFile.readAll();
                settingFile.close();
            }

            QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);
            QJsonObject settingObject = settingDoc.object();
            settingObject.insert("DefaultTemplate", name);
            settingDoc.setObject(settingObject);
            QJsonDocument settingDoctemp;
            settingDoctemp.setObject(settingObject);
            if (settingFile.open(QIODevice::WriteOnly))
            {
                settingFile.write(settingDoctemp.toJson());
            }
        }
        else{
            //! Save in AppData
            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();

            QFile settingFile(QString::fromStdString(appdataPath + "/" + SERVERTEMPLATEFILE));
            QByteArray settingData;
            if (settingFile.open(QIODevice::ReadOnly))
            {
                settingData = settingFile.readAll();
                settingFile.close();
            }

            QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);
            QJsonObject settingObject = settingDoc.object();
            settingObject.insert("DefaultTemplate", name);
            settingDoc.setObject(settingObject);
            QJsonDocument settingDoctemp;
            settingDoctemp.setObject(settingObject);
            if (settingFile.open(QIODevice::WriteOnly))
            {
                settingFile.write(settingDoctemp.toJson());
            }
            settingFile.close();
            // write the settings file in server in case of usign Apache server
            CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            QObject* qmlRoot = qobject_cast<QObject*>(qtapp->getRootComponent());
            if (!qmlRoot)
            {
                return;
            }

            if (!_lookUpUIHandler.valid())
                return;

            if (!_projectSaveAndLoadComponent.valid())
                return;
            std::string link = _getProjectLocationOnServer();

            if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
            {
                emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection please.");
                return;
            }

            if (!_getDatabaseCredential(_databaseToConnect))
                return;

            if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
            {
                emit showError("Database Connectivity", "Not able to Connect to Database");
                return;
            }

            // upload the settings file in server 
            std::string fileToSave = appdataPath + "/" + SERVERTEMPLATEFILE;
            std::string fullURL = link + "/" + SERVERTEMPLATEFILE;
            if (_projectSaveAndLoadComponent->saveDefaultTemplate(fileToSave, fullURL)){
                remove(fileToSave.c_str());
            }
            else{
                emit showError("Saving Template", "cannot save template, Check Server Connection");
            }
        }

        return;
    }
    void ProjectManagerGUI::deleteTemplate(QString name)
    {
        if (name.isEmpty())
        {
            emit showError("Template deletion", "Invalid template name");
            return;
        }

        if (name.contains("Default"))
        {
            emit showError("Template deletion", "Sorry! Default Template Cannot be deleted");
            return; 
        }

        _strTemlateName = name;
        QString msg = "Are you sure you want to delete \' " + _strTemlateName + " \' template ?";
        emit question("Template Deletion", msg, false, "confirmTemplateDeletion()", "cancelTemplateDeletion");
    }
    void ProjectManagerGUI::confirmTemplateDeletion()
    {
        QObject *smTemplateMenu = _findChild("smpTemplatesMenu");
        if (nullptr != smTemplateMenu)
        {
            // deleting template folder from appdata
            QString strDefaultTemplate = smTemplateMenu->property("curTemplateComboBoxValue").toString();/*getDefaultTemplate()*/;

            if (_globalSettingComponent->isLocalProject()){
                std::string localProjectLocation = _globalSettingComponent->getLocalProjectLocation();
                std::string templateLocation = localProjectLocation + "/" + TEMPLATEDIRECTORY + "/" + strDefaultTemplate.toStdString();

                UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
                temporaryInstance->deleteSubdirectory(templateLocation);
            }
            else{
                // files deletion from server
                if (!_lookUpUIHandler.valid())
                    return;

                if (!_projectSaveAndLoadComponent.valid())
                    return;

                std::string link = _getProjectLocationOnServer();

                if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
                {
                    emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection please.");
                    return;
                }

                if (!_getDatabaseCredential(_databaseToConnect))
                    return;

                if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
                {
                    emit showError("Database Connectivity", "Not able to Connect to Database");
                    return;
                }

                std::string projectToDelete = strDefaultTemplate.toStdString();

                _lookUpUIHandler->deleteProjectByName(projectToDelete, SMCUI::ILookUpProjectUIHandler::TEMPLATE);
            }
        }

        // refreshing template combo-box 
        refreshTemplateComboBox();
    }
    void ProjectManagerGUI::cancelTemplateDeletion()
    {
        return;
    }

    bool ProjectManagerGUI::_projectAlreadyProject(const std::string& projectName, const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType)
    {
        if (!_globalSettingComponent.valid())
            return false;

        bool isLocalProject = _globalSettingComponent->isLocalProject();
        if (isLocalProject)
        {
            bool present = _localProjectAlreadyPresent(projectName, projectType);
            return present;
        }

        if (!_lookUpUIHandler.valid())
            return false;

        if (!_getDatabaseCredential(_databaseToConnect))
            return false;

        if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
        {
            return false;
        }

        return _lookUpUIHandler->isProjectAlreadyPresent(projectName, projectType);
    }

    //This willl clear the contents of the Folder specified folder
    void ProjectManagerGUI::_flushTheDirectoryContents(const std::string& folderName)
    {

        osgDB::FileType type = osgDB::fileType(folderName);
        if (type != osgDB::DIRECTORY)
            return;

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(folderName);

        unsigned int index = 0;
        while (index < contents.size())
        {
            std::string filename = contents[index++];
            if ((filename == ".") || (filename == ".."))
                continue;

            //Flushing only the sqlite file
            std::string extension = osgDB::getLowerCaseFileExtension(filename);
            if (extension != "sqlite" && extension != "zip")
                continue;

            std::string absoluteFileName = folderName + "/" + filename;
            temporaryInstance->deleteFile(absoluteFileName, true);
        }
    }

    /*NOTE : Type  0-- sm file, 1-- plan file*/
    void ProjectManagerGUI::loadProject(QString fileName, int type) //Changed it to project name
    {
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QWidget* mainWindow = qtapp->getLayoutWidget();

        mainWindow->show();

        if (_loadingProject)
        {
            LOG_ERROR("Already loading a project,..");
            return;
        }

        _loadingProject = true;

        QObject* statusBar = _findChild("statusBar");
        if (statusBar)
        {
            QMetaObject::invokeMethod(statusBar, "quicKLoadBar",
                Q_ARG(QVariant, QVariant(QString("Loading ..."))));
        }

        QObject::connect(this, SIGNAL(executed()), this, SLOT(addWorldToMaintainer()), Qt::QueuedConnection);

        //First we subscribe to project loading status message and remove the world
        _subscribeToProjectLoaderComponentMessages();

        //! Turn of selection component 
        CORE::RefPtr<VizUI::ISelectionUIHandler> selUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if (selUIHandler.valid())
        {
            selUIHandler->setSelectionState(false);
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
        LOG_DEBUG("Clearing world,.. Begin");
        worldMaintainer->clearWorlds();
        LOG_DEBUG("Clearing world,..Completed");


        if (!_globalSettingComponent.valid())
            return;

        //Now reading the world on different thread
        _loadBusyBar(boost::bind(&ProjectManagerGUI::_executeLoadProject, this, fileName, type), "Loading ...", false);
    }

    void ProjectManagerGUI::addWorldToMaintainer()
    {
        CORE::IWorldMaintainer *wmain = CORE::WorldMaintainer::instance();
        if (_worldReadFromFile.valid())
        {
            CORE::RefPtr<CORE::IWorld> world = _worldReadFromFile->getInterface<CORE::IWorld>();
            wmain->addWorld(world);
        }

        QObject* desktop = _findChild("desktop");
        if (desktop)
        {
            desktop->setProperty("minimized", QVariant::fromValue(true));
        }

        QObject* loginScreen = _findChild("loginScreen");
        if (loginScreen)
        {
            loginScreen->setProperty("locked", QVariant::fromValue(false));
        }

        QObject::disconnect(this, SIGNAL(executed()), this, SLOT(addWorldToMaintainer()));

        // Setting the name of project on Title window
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QWidget* mainWindow = qtapp->getLayoutWidget();

        if (mainWindow)
        {
            mainWindow->setWindowTitle(QString::fromStdString("GeorbIS : " + _presentProject));
        }

        // Setting the name of project on Quick Launch bar
        QObject* QuickLaunchGUI = _findChild("QuickLaunchGUI");
        if (QuickLaunchGUI)
        {
            QuickLaunchGUI->setProperty("projectName", QVariant::fromValue(QString::fromStdString(_presentProject)));
        }
    }

    void ProjectManagerGUI::_performHardReset()
    {
        _currentProjectUID = "";
    }

    void ProjectManagerGUI::_calcNormalWindowWidth()
    {
        QRect rec = QApplication::desktop()->screenGeometry();
        _screenResolutionY = rec.height();
        _screenResolutionX = rec.width();

        _normalWindowWidth = _screenResolutionX / _windowScaleFactor;
        _normalWindowHeight = _normalWindowWidth / _aspectRatio;

        if (_normalWindowHeight > _screenResolutionY)
        {
            _normalWindowHeight = _screenResolutionY - 200;
            _normalWindowWidth = _normalWindowHeight*_aspectRatio;
        }

        _windowPosX = (_screenResolutionX / 2) - (_normalWindowWidth / 2);
        _windowPosY = (_screenResolutionY / 2) - (_normalWindowHeight / 2);
    }

    void ProjectManagerGUI::confirmDeleteProject(QString filename, int type)
    {
        QString msg = "Are you sure you want to delete \'" + filename + "\' ?";

        _projectToDeleteName = filename;
        // hardcoding PLANNER since we have only planner type of projects now
        _projectToDeleteType = PLANNER;

        emit criticalQuestion("Delete Project", msg, false, "deleteProject()", "cancelDelete()");
    }

    void ProjectManagerGUI::cancelDelete()
    {
        _projectToDeleteName = "";
    }

    CORE::IObject* ProjectManagerGUI::_readLocalProject(const std::string& projectName, const int& type)
    {
        if (type == PLANNER)
        {
            _setProjectType(SMCUI::ILookUpProjectUIHandler::PLANNER);
        }

        CORE::RefPtr<CORE::IObject> object = _readLocalProjectFile(projectName, type);
        return object.get();
    }

    CORE::IObject* ProjectManagerGUI::_readProjectFile(const std::string& projectName, const int& type)
    {
        if (!_globalSettingComponent.valid())
            return NULL;

        bool isLocal = _globalSettingComponent->isLocalProject();
        if (isLocal)
        {
            CORE::IObject* object = _readLocalProject(projectName, type);
            return object;
        }

        // for server
        _setProjectType(SMCUI::ILookUpProjectUIHandler::PLANNER);
        _currentProjectUID = _lookUpUIHandler->getProjectUIDByName(projectName,
            SMCUI::ILookUpProjectUIHandler::PLANNER);

        _setPresentProject(projectName);

        //Get the location of the project on the Server
        ELEMENTS::ISettingComponent::SettingsAttributes presentWebServerSetting =
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if (presentWebServerSetting.keyValuePair.find("Path") != presentWebServerSetting.keyValuePair.end())
            _projectLocationOnServer = presentWebServerSetting.keyValuePair["Path"];
        else
            return NULL;

        /*UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string temporaryPath = osgDB::convertFileNameToNativeStyle(temporaryInstance->getPath());*/

        std::string temporaryPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "/Project";
        std::string projectFilename;
        projectFilename = osgDB::convertFileNameToNativeStyle(temporaryPath) + "/" + SERVERPLANNERDIRECTORY + "/" + projectName + "/" + projectName + "." + PLANNERFILEEXTENSION;

        //This will create a direcrtory if not present , And the directory preent it will not do Anything
        bool isFolderPresent = osgDB::makeDirectoryForFile(projectFilename);
        if (isFolderPresent)
        {
            _flushTheDirectoryContents(osgDB::getFilePath(projectFilename));
        }

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return NULL;
        }

        wmtnr->setProjectFile(projectFilename);

        std::string projectLocation = osgDB::getFilePath(projectFilename);

        std::string zipProjectFile;

        zipProjectFile = _getProjectLocationOnServer() + "/" + PLANNERSERVERFOLDER + "/" + projectName + ".zip";

        _projectSaveAndLoadComponent->loadProject(zipProjectFile, projectLocation);

        CORE::RefPtr<CORE::IObject> object = NULL;
        if (osgDB::fileExists(projectFilename))
        {
            try
            {
                object = DB::readObjectFile(projectFilename, NULL);
            }
            catch (...)
            {
                object = NULL;
            }
        }
        else
        {
            object = NULL;
        }

        //Close the database connection
        _lookUpUIHandler->closeDatabaseConnection();

        return object.release();
    }

    void ProjectManagerGUI::_deleteLocalProject(const std::string& projectName, const int& type)
    {
        if (!_globalSettingComponent.valid())
            return;

        std::string localProjectname = _globalSettingComponent->getLocalProjectLocation();

        std::string projectFoldername;

        if (type == PLANNER)
        {
            projectFoldername = localProjectname + "/" + PLANNERDIRECTORY + "/" + projectName;
        }

        projectFoldername = osgDB::convertFileNameToNativeStyle(projectFoldername);

        _flushTheDirectoryContents(projectFoldername);
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        temporaryInstance->deleteSubdirectory(projectFoldername);

        _projectToDeleteName = "";

        loadFiles();
    }

    void ProjectManagerGUI::deleteProject()
    {
        if (!_globalSettingComponent.valid())
            return;

        bool isLocal = _globalSettingComponent->isLocalProject();
        if (isLocal)
        {
            _deleteLocalProject(_projectToDeleteName.toStdString(), _projectToDeleteType);
            return;
        }

        // files deletion from server
        if (!_lookUpUIHandler.valid())
            return;

        if (!_projectSaveAndLoadComponent.valid())
            return;

        std::string link = _getProjectLocationOnServer();

        if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
        {
            emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection please.");
            return;
        }

        if (!_getDatabaseCredential(_databaseToConnect))
            return;

        if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
        {
            emit showError("Database Connectivity", "Not able to Connect to Database");
            return;
        }

        std::string projectName = _projectToDeleteName.toStdString();

        //Get the location of the project on the Server
        ELEMENTS::ISettingComponent::SettingsAttributes presentWebServerSetting =
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if (presentWebServerSetting.keyValuePair.find("Path") != presentWebServerSetting.keyValuePair.end())
            _projectLocationOnServer = presentWebServerSetting.keyValuePair["Path"];
        else
            return;

        _lookUpUIHandler->deleteProjectByName(projectName, SMCUI::ILookUpProjectUIHandler::PLANNER);

        loadFiles();

        // remove project from server,...??

        _projectToDeleteName = "";

    }

    CORE::IObject* ProjectManagerGUI::_readLocalProjectFile(const std::string& fileName, const int& type)
    {
        if (!_globalSettingComponent.valid())
            return NULL;

        std::string localProjectname = _globalSettingComponent->getLocalProjectLocation();

        std::string projectName = fileName;
        _setPresentProject(projectName);

        std::string projectFilename;
        if (type == PLANNER)
        {
            projectFilename = localProjectname + "/" + PLANNERDIRECTORY + "/" + projectName + "/" + projectName + "." + PLANNERFILEEXTENSION;
        }

        projectFilename = osgDB::convertFileNameToNativeStyle(projectFilename);

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return NULL;
        }

        wmtnr->setProjectFile(projectFilename);

        std::string projectLocation = osgDB::getFilePath(projectFilename);

        CORE::RefPtr<CORE::IObject> object = NULL;
        if (osgDB::fileExists(projectFilename))
        {
            object = DB::readObjectFile(projectFilename, NULL);
            if (!object.valid())
            {
                emit showError("Project Loading", "Not a proper Project file to load");
                return NULL;
            }
        }
        else
        {
            object = DB::readObjectFile(defaultProjectFile, NULL);
            emit showError("Project Loading", "Project Not found, Loading the default project");
        }

        return object.release();
    }

    void ProjectManagerGUI::_loadLocalProject(const std::string& fileName, const int& type)
    {
        if (!_globalSettingComponent.valid())
            return;

        //setting the application Mode
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QObject* qmlRoot = qobject_cast<QObject*>(qtapp->getRootComponent());
        if (!qmlRoot)
        {
            return;
        }


        std::string localProjectname = _globalSettingComponent->getLocalProjectLocation();

        std::string projectName = fileName;

        if (fileName.empty() || fileName == "Default")
        {
            //! Load default template
            //! Read the default template if has been set by the user.
            UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
            std::string appdataPath = temporaryInstance->getAppFolderPath();

            QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");
            if (settingFile.open(QIODevice::ReadOnly))
            {
                QByteArray settingData = settingFile.readAll();

                QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

                QJsonObject settingObject = settingDoc.object();

                QString defaultTemplate = settingObject["DefaultTemplate"].toString();

                if (!defaultTemplate.isEmpty())
                {
                    projectName = defaultTemplate.toStdString();
                }
            }

        }
        _setVizStyleGlobalSetting();
        _setPresentProject(projectName);

        std::string projectFilename;

        if (type == 0)
        {
            _setProjectType(SMCUI::ILookUpProjectUIHandler::PLANNER);
            projectFilename = localProjectname + "/" + PLANNERDIRECTORY + "/" + projectName + "/" + projectName + "." + PLANNERFILEEXTENSION;

            qmlRoot->setProperty("application_mode", QVariant::fromValue(2));
        }

        else if (type == 3)
        {
            isProjectSavable = false;
            _setProjectType(SMCUI::ILookUpProjectUIHandler::TEMPLATE);
            projectFilename = localProjectname + "/" + TEMPLATEDIRECTORY + "/" + projectName + "/" + projectName + "." + TEMPLATEFILEEXTENSION;

            qmlRoot->setProperty("application_mode", QVariant::fromValue(1));

        }

        projectFilename = osgDB::convertFileNameToNativeStyle(projectFilename);

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return;
        }

        wmtnr->setProjectFile(projectFilename);

        std::string projectLocation = osgDB::getFilePath(projectFilename);

        _worldReadFromFile = NULL;

        if (osgDB::fileExists(projectFilename))
        {

            LOG_ALWAYS("Reading project file : " + projectFilename);
            _worldReadFromFile = DB::readObjectFile(projectFilename, NULL);
            if (!_worldReadFromFile.valid())
            {
                emit showError("Project Loading", "Not a proper Project file to load");
                return;
            }
        }
        else
        {
            _worldReadFromFile = DB::readObjectFile(defaultProjectFile, NULL);
            emit showError("Project Loading", "Project Not found, Loading the default project");
        }
    }
    void ProjectManagerGUI::_setVizStyleGlobalSetting()
    {
        QObject* vizStyleObject = _findChild(VizStyleObjectName);
        if (!vizStyleObject)
        {
            return;
        }
        //! File open
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");
        if (settingFile.open(QIODevice::ReadOnly))
        {
            QByteArray settingData = settingFile.readAll();

            QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

            QJsonObject settingObject = settingDoc.object();

            QString latlongUnit = settingObject["LatLongUnit"].toString();
            QString dockType = settingObject["DockType"].toString();
            QString grVisibility = settingObject["GrVisibility"].toString();
            QString grShowZone = settingObject["GrShowZone"].toString();
            QString grShowLetter = settingObject["GrShowLetter"].toString();
            QString grNumdigits = settingObject["GrNumdigits"].toString();
            QString grMapSheet = settingObject["GrShowMapsheet"].toString();

            if (!latlongUnit.isEmpty())
            {
                bool oky = true;
                int latlongunitType = latlongUnit.toInt(&oky);
                if (oky)
                {
                    vizStyleObject->setProperty("latlongUnit", QVariant::fromValue(latlongunitType));
                }

            }

            if (!grShowLetter.isEmpty())
            {
                bool oky = true;
                int grShowLetterType = grShowLetter.toInt(&oky);
                if (oky)
                {
                    vizStyleObject->setProperty("showGR", QVariant::fromValue(grShowLetterType));
                }

            }

            if (!grShowZone.isEmpty())
            {
                bool oky = true;
                int grShowZoneType = grShowZone.toInt(&oky);
                if (oky)
                {
                    vizStyleObject->setProperty("showZone", QVariant::fromValue(grShowZoneType));
                }

            }

            if (!grMapSheet.isEmpty())
            {
                bool oky = true;
                int grShowZoneType = grMapSheet.toInt(&oky);
                if (oky)
                {
                    vizStyleObject->setProperty("showMapSheet", QVariant::fromValue(grShowZoneType));
                }

            }
            if (!grNumdigits.isEmpty())
            {
                bool oky = true;
                int numOfGrDigitsType = grNumdigits.toInt(&oky);
                if (oky)
                {
                    vizStyleObject->setProperty("numOfGrDigits", QVariant::fromValue(numOfGrDigitsType));
                }

            }
            if (!dockType.isEmpty())
            {
                bool oky = true;
                int dockTypeValue = dockType.toInt(&oky);
                if (oky)
                {
                    vizStyleObject->setProperty("docked", QVariant::fromValue(dockTypeValue));
                }

            }
            if (!grVisibility.isEmpty())
            {
                bool oky = true;
                int grVisibilityVal = grVisibility.toInt(&oky);
                if (oky)
                {
                    if (grVisibilityVal == 0)
                    {
                        vizStyleObject->setProperty("indianGrVisibility", QVariant::fromValue(false));
                        vizStyleObject->setProperty("mgrsVisibility", QVariant::fromValue(true));

                    }
                    else if (grVisibilityVal == 1)
                    {
                        vizStyleObject->setProperty("indianGrVisibility", QVariant::fromValue(true));
                        vizStyleObject->setProperty("mgrsVisibility", QVariant::fromValue(false));

                    }
                    else if (grVisibilityVal == 2)
                    {
                        vizStyleObject->setProperty("indianGrVisibility", QVariant::fromValue(true));
                        vizStyleObject->setProperty("mgrsVisibility", QVariant::fromValue(true));

                    }
                    else if (grVisibilityVal == 3)
                    {
                        vizStyleObject->setProperty("indianGrVisibility", QVariant::fromValue(false));
                        vizStyleObject->setProperty("mgrsVisibility", QVariant::fromValue(false));

                    }
                }

            }
        }

    }

    QString ProjectManagerGUI::getDefaultTemplate()
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        if (_globalSettingComponent->isLocalProject()){
            QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");
            if (settingFile.open(QIODevice::ReadOnly))
            {
                QByteArray settingData = settingFile.readAll();

                QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

                QJsonObject settingObject = settingDoc.object();

                QString defaultTemplate = settingObject["DefaultTemplate"].toString();
                return defaultTemplate;
            }
        }
        else{
            std::string defaultTemplate = _getDefaultTemplateNameFromServer();

            return QString::fromStdString(defaultTemplate);
        }
        //return "Base";
		return "Default";
    }

    void ProjectManagerGUI::_executeLoadProject(QString fileName, int type)
    {
        _userType = type;

        isProjectSavable = true;

        if (!_globalSettingComponent.valid())
            return;

        bool isLocalProject = _globalSettingComponent->isLocalProject();
        if (isLocalProject)
        {
            _loadLocalProject(fileName.toStdString(), type);
            //Reset the Dummy ID
            _dummyUID = "";

            return;
        }

        // load projects from server
        //setting the application Mode
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QObject* qmlRoot = qobject_cast<QObject*>(qtapp->getRootComponent());
        if (!qmlRoot)
        {
            return;
        }

        if (!_lookUpUIHandler.valid())
            return;

        if (!_projectSaveAndLoadComponent.valid())
            return;

        std::string link = _getProjectLocationOnServer();

        if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
        {
            emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection please.");
            return;
        }

        if (!_getDatabaseCredential(_databaseToConnect))
            return;

        if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
        {
            emit showError("Database Connectivity", "Not able to Connect to Database");
            return;
        }

        // in case of loading default template

        std::string projectName = fileName.toStdString();

        if (fileName.toStdString().empty())
        {

            projectName = _getDefaultTemplateNameFromServer();
        }

        if (type == PLANNER){
            _setProjectType(SMCUI::ILookUpProjectUIHandler::PLANNER);
            _currentProjectUID = _lookUpUIHandler->getProjectUIDByName(projectName, SMCUI::ILookUpProjectUIHandler::PLANNER);
        }
        else{
            _setProjectType(SMCUI::ILookUpProjectUIHandler::TEMPLATE);
            _currentProjectUID = _lookUpUIHandler->getProjectUIDByName(projectName, SMCUI::ILookUpProjectUIHandler::TEMPLATE);
            isProjectSavable = false;
        }

        qmlRoot->setProperty("application_mode", QVariant::fromValue(2));


        _setPresentProject(projectName);

        //Get the location of the project on the Server
        ELEMENTS::ISettingComponent::SettingsAttributes presentWebServerSetting =
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if (presentWebServerSetting.keyValuePair.find("Path") != presentWebServerSetting.keyValuePair.end())
            _projectLocationOnServer = presentWebServerSetting.keyValuePair["Path"];
        else
            return;
        /*
                UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
                std::string temporaryPath = osgDB::convertFileNameToNativeStyle(temporaryInstance->getPath());*/

        std::string projectFilename;
        std::string temporaryPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "/Project";
        projectFilename = osgDB::convertFileNameToNativeStyle(temporaryPath) + "/" + SERVERPLANNERDIRECTORY + "/" + projectName + "/" + projectName + "." + PLANNERFILEEXTENSION;


        bool isFolderPresent = osgDB::makeDirectoryForFile(projectFilename);
        if (isFolderPresent)
        {
            _flushTheDirectoryContents(osgDB::getFilePath(projectFilename));
        }

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return;
        }

        wmtnr->setProjectFile(projectFilename);

        std::string projectLocation = osgDB::getFilePath(projectFilename);

        std::string zipProjectFile;
        if (type == PLANNER)
            zipProjectFile = _getProjectLocationOnServer() + "/" + PLANNERSERVERFOLDER + "/" + projectName + ".zip";
        else
            zipProjectFile = _getProjectLocationOnServer() + "/" + TEMPLATESERVERFOLDER + "/" + projectName + ".zip";

        _projectSaveAndLoadComponent->loadProject(zipProjectFile, projectLocation);

        _worldReadFromFile = NULL;
        if (osgDB::fileExists(projectFilename))
        {
            try
            {
                _worldReadFromFile = DB::readObjectFile(projectFilename, NULL);
            }
            catch (...)
            {
                _worldReadFromFile = NULL;
            }
        }
        else
        {
            _worldReadFromFile = NULL;
        }

        if (!_worldReadFromFile.valid())
        {
            _worldReadFromFile = DB::readObjectFile(defaultProjectFile);
            emit showError("Project Load", "Not able to find the Actual Project , Loading the Default Project");
        }

        //Close the database connection
        _lookUpUIHandler->closeDatabaseConnection();

        //Reset the Dummy ID
        _dummyUID = "";
    }

    void ProjectManagerGUI::searchProjects(QString filter, int type)
    {
        switch (type)
        {
        case 1:
            searchPlanProjects(filter);
            break;
        }
    }

    void ProjectManagerGUI::searchPlanProjects(QString filter)
    {

        QStringList plannerSearcheResult;
        for (int index = 0; index < _plannerProjectList.size(); index++)
        {
            QString name = _plannerProjectList[index].first;
            if (name.contains(filter, Qt::CaseInsensitive))
            {
                plannerSearcheResult.append(name);
            }
        }

        _setContextProperty("plannerSearcheResult", QVariant::fromValue(plannerSearcheResult));
    }

    std::string ProjectManagerGUI::_getPresentProject() const
    {
        return _presentProject;
    }

    void ProjectManagerGUI::_setPresentProject(const std::string& projectName)
    {
        _presentProject = projectName;
        // Setting the name of project on Quick Launch bar
        QObject* QuickLaunchGUI = _findChild("QuickLaunchGUI");
        if (QuickLaunchGUI)
        {
            if (_userType == SMCUI::ILookUpProjectUIHandler::TEMPLATE)
            {
                QuickLaunchGUI->setProperty("typeOfProject", QVariant::fromValue(QString::fromStdString("Template")));
            }
            else
            {
                QuickLaunchGUI->setProperty("typeOfProject", QVariant::fromValue(QString::fromStdString("Project")));
            }
            QuickLaunchGUI->setProperty("projectName", QVariant::fromValue(QString::fromStdString(_presentProject)));
        }
    }

    bool ProjectManagerGUI::_saveAsLocalProject(const std::string& file, const SMCUI::ILookUpProjectUIHandler::ProjectType& projectType)
    {
        if (!_globalSettingComponent.valid())
            return false;

        std::string localProjectName = _globalSettingComponent->getLocalProjectLocation();

        std::string sourceFolder = _getPresentProject();

        // clear undo stack
        _clearUndoStack();

        std::string projectFolderName;
        std::string projectFileExtension;
        //std::string fileName;
        switch (projectType)
        {

        case SMCUI::ILookUpProjectUIHandler::PLANNER:
            projectFolderName = PLANNERDIRECTORY;
            projectFileExtension = PLANNERFILEEXTENSION;
            break;
        case SMCUI::ILookUpProjectUIHandler::TEMPLATE:
            projectFolderName = TEMPLATEDIRECTORY;
            projectFileExtension = TEMPLATEFILEEXTENSION;
            break;

        }

        std::string projectFilename = localProjectName + "/" + projectFolderName + "/" + file + "/" + file + "." + projectFileExtension;

        projectFilename = osgDB::convertFileNameToNativeStyle(projectFilename);

        _setPresentProject(file);

        projectFilename = osgDB::convertFileNameToNativeStyle(projectFilename);
        osgDB::makeDirectoryForFile(projectFilename);

        std::string destinationFolder = osgDB::getFilePath(projectFilename);
        destinationFolder = osgDB::convertFileNameToNativeStyle(destinationFolder);

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
        std::string previousProjectFile = wmtnr->getProjectFile();
        sourceFolder = osgDB::getFilePath(previousProjectFile);
        sourceFolder = osgDB::convertFileNameToNativeStyle(sourceFolder);


        //Move the pdf , audio , video to other folder
        _moveOtherAssociatedFileToDestinationFolder(sourceFolder, destinationFolder);

        

        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return false;
        }

        
        wmtnr->setProjectFile(projectFilename);

        CORE::IWorld *world = wmtnr->getWorldMap().begin()->second.get();

        if (world == NULL)
        {
            LOG_ERROR("World not valid");
            return false;
        }

        CORE::RefPtr<DB::ReaderWriter::Options> options = new DB::ReaderWriter::Options;

        options->setMapValue("VisibilityMode", "OFF");

        DB::writeObjectFile(world->getInterface<CORE::IObject>(), projectFilename, options);

        if (projectType == SMCUI::ILookUpProjectUIHandler::TEMPLATE)
        {
            //! redundant code
            //! Need to fix
            _setPresentProject(osgDB::getStrippedName(previousProjectFile));
            wmtnr->setProjectFile(previousProjectFile);
        }

        //Right now not zipping the content of the Project
        /*std::string projectName = osgDB::getStrippedName(projectFilename);
        std::string locationToSave = osgDB::getFilePath(projectFilename);
        _projectSaveAndLoadComponent->saveProject(projectName, locationToSave, locationToSave, true);*/

        isProjectSavable = true;

        return true;
    }

    bool ProjectManagerGUI::SaveAsFile(const std::string& file, const SMCUI::ILookUpProjectUIHandler::ProjectType& projectType)
    {
        //Get the Database to connect to from the Global Setting configuration
        if (!_globalSettingComponent.valid())
            return false;

        std::string sourceFolder = _getPresentProject();

        bool isProjectLocal = _globalSettingComponent->isLocalProject();
        if (isProjectLocal)
        {
            bool success = _saveAsLocalProject(file, projectType);
            return success;
        }


        if (!_projectSaveAndLoadComponent.valid())
            return false;

        std::string link = _getProjectLocationOnServer();

        if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
        {
            emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection please.");
            return false;
        }

        // There is not existing Database connection then return;
        ELEMENTS::ISettingComponent::SettingsAttributes presentDatabaseSetting =
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::PROJECTDATABASECONN);

        ELEMENTS::ISettingComponent::SettingsAttributes presentWebServerSetting =
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if (presentWebServerSetting.keyValuePair.find("Path") != presentWebServerSetting.keyValuePair.end())
            _projectLocationOnServer = presentWebServerSetting.keyValuePair["Path"];
        else
            return false;

        if (!_getDatabaseCredential(_databaseToConnect))
        {
            emit showError("Database Connection", "Not able to get databaseCredential");
            return false;
        }

        if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
        {
            emit showError("Database Connection", "Not able to Connect to Database");
            return false;
        }

        // clear undo stack
        _clearUndoStack();
        /*
                UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
                std::string temporaryPath = osgDB::convertFileNameToNativeStyle(temporaryInstance->getPath());*/

        std::string temporaryPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "/Project";
        std::string projectFolderName;
        std::string fileName;

        fileName += file;
        projectFolderName = temporaryPath + "/" + SERVERPLANNERDIRECTORY + "/" + file;

        std::string projectFilename;
        projectFilename = projectFolderName + "/" + fileName + "." + PLANNERFILEEXTENSION;

        _setPresentProject(file);

        projectFilename = osgDB::convertFileNameToNativeStyle(projectFilename);

        //This is necessary 2 create any project Directory other than Temp directory
        //such that we can know the specific file to Upload to server
        bool isFolderPresent = osgDB::makeDirectoryForFile(projectFilename);
        if (isFolderPresent)
        {
            _flushTheDirectoryContents(osgDB::getFilePath(projectFilename));
        }

        std::string destinationFolder = osgDB::getFilePath(projectFilename);
        destinationFolder = osgDB::convertFileNameToNativeStyle(destinationFolder);

        sourceFolder = temporaryPath + "/" + SERVERPLANNERDIRECTORY + "/" + sourceFolder;
        sourceFolder = osgDB::convertFileNameToNativeStyle(sourceFolder);

        //Move the pdf , audio , video to other folder
        _moveOtherAssociatedFileToDestinationFolder(sourceFolder, destinationFolder);

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();

        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return false;
        }

        std::string previousProjectFile = wmtnr->getProjectFile();
        wmtnr->setProjectFile(projectFilename);


        //Hav to make sure that u hav set the present Webserver where u want to upload the project

        _addEntryToDatabase(projectFilename, SMCUI::ILookUpProjectUIHandler::SAVEASPROJECT, projectType);

        // XXX - get first world in world map
        // this should be changed when support for multiple worlds are given
        CORE::IWorld *world = wmtnr->getWorldMap().begin()->second.get();

        if (world == NULL)
        {
            LOG_ERROR("World not valid");
            return false;
        }


        /*bool visibilityFlag = _isVisibilityFlagOn();*/
        CORE::RefPtr<DB::ReaderWriter::Options> options = new DB::ReaderWriter::Options;

        /*if (_isVisibilityModeEnabled)
            options->setMapValue("VisibilityMode", "ON");
            else
            options->setMapValue("VisibilityMode", "OFF");
            */
        DB::writeObjectFile(world->getInterface<CORE::IObject>(), projectFilename, options);

        _lookUpUIHandler->closeDatabaseConnection();

        //Upload the file created in the Project Folder to the Server
        _uploadProjectFilesToServer(projectFilename, projectType);
        //wmtnr->setProjectFile(previousProjectFile);
        //_setPresentProject(osgDB::getStrippedName(previousProjectFile));

        return true;
    }

    void ProjectManagerGUI::_uploadProjectFilesToServer(const std::string& projectFilePath, const SMCUI::ILookUpProjectUIHandler::ProjectType& projectType)
    {
        std::string projectName = osgDB::getNameLessAllExtensions(osgDB::getSimpleFileName(projectFilePath));
        if (projectName != "")
        {
            if (!_projectSaveAndLoadComponent.valid())
            {
                emit showError("Project Load And Save", "Not able to Upload to webserver");
                return;
            }

            std::string serverLocationToUpload = _getProjectLocationOnServer();

            if (projectType == SMCUI::ILookUpProjectUIHandler::PLANNER)
                serverLocationToUpload += "/" + PLANNERSERVERFOLDER;
            else
            {
                serverLocationToUpload += "/" + TEMPLATESERVERFOLDER;
            }

            std::string fileName = osgDB::getSimpleFileName(osgDB::getNameLessAllExtensions(projectFilePath));
            _projectSaveAndLoadComponent->saveProject(fileName, projectFilePath, serverLocationToUpload, true);
        }
    }



    void ProjectManagerGUI::_moveOtherAssociatedFileToDestinationFolder(const std::string& sourcefolderName, const std::string& destinationfolderName)
    {

        if (!osgDB::fileExists(sourcefolderName))
            return;

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(sourcefolderName);
        int index = 0;
        int size = contents.size();
        while (index < size)
        {
            std::string fileName = contents[index++];
            if ((fileName == ".") || (fileName == ".."))
                continue;

            std::string ext = osgDB::getFileExtension(fileName);

            if ((ext != "sqlite") && (ext != PLANNERFILEEXTENSION)
                && (ext != "zip") && (ext != TEMPLATEFILEEXTENSION))
            { 
                std::string realPath = sourcefolderName + "/" + fileName;
                std::string destination = destinationfolderName + "/" + fileName;
                if (osgDB::fileType(realPath) != osgDB::DIRECTORY)
                {
                    osgDB::copyFile(realPath, destination);
                }
                else
                {
                    boost::filesystem::path src(realPath), dst(destination);
                    UTIL::copyDirectoryRecursively(src, dst);
                }
            }
        }
    }

    bool ProjectManagerGUI::_saveLocalProject()
    {
        if (!_globalSettingComponent.valid())
            return false;

        std::string projectFullPath("");
        std::string localProjectPath = _globalSettingComponent->getLocalProjectLocation();

        if (_activeTab == PLANNER)
        {
            projectFullPath = localProjectPath + "/" + PLANNERDIRECTORY + "/" + _getPresentProject() + "/" + _getPresentProject() + "." + PLANNERFILEEXTENSION;
        }

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return false;
        }

        wmtnr->setProjectFile(projectFullPath);

        // XXX - get first world in world map
        // this should be changed when support for multiple worlds are given
        if (wmtnr->getWorldMap().begin() == wmtnr->getWorldMap().end())
        {
            LOG_ERROR("No World to Save");
            return false;
        }

        CORE::IWorld *world = wmtnr->getWorldMap().begin()->second.get();
        if (world == NULL)
        {
            LOG_ERROR("World not valid");
            return false;
        }

        DB::writeObjectFile(world->getInterface<CORE::IObject>(), projectFullPath);

        //Right now not zipping the content of the Project
        /*std::string projectName = osgDB::getStrippedName(projectFilename);
        std::string locationToSave = osgDB::getFilePath(projectFilename);
        _projectSaveAndLoadComponent->saveProject(projectName, locationToSave, locationToSave, true);*/

        return true;
    }

    void ProjectManagerGUI::cleanupProjectFolder()
    {
        if (!_globalSettingComponent.valid())
            return;

        // clear undo stack
        _clearUndoStack();

        // get the project directory

        bool isProjectLocal = _globalSettingComponent->isLocalProject();
        std::string strProjectPath;

        if (isProjectLocal)
        {
            strProjectPath = _globalSettingComponent->getLocalProjectLocation();

            if (_activeTab == PLANNER)
            {
                strProjectPath = strProjectPath + "/" + PLANNERDIRECTORY + "/" + _getPresentProject() + "/";
            }
        }
        else{
            /*UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();*/
            std::string temporaryPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "/Project";
            strProjectPath = temporaryPath + "/" + SERVERPLANNERDIRECTORY + "/" + _getPresentProject() + "/";
        }

        // delete all the .sqlite  and json files
        boost::filesystem::path projectDirectory(strProjectPath);
        boost::filesystem::directory_iterator end_iter;

        if (boost::filesystem::exists(projectDirectory) && boost::filesystem::is_directory(projectDirectory))
        {
            for (boost::filesystem::directory_iterator dir_iter(projectDirectory); dir_iter != end_iter; ++dir_iter)
            {
                if (boost::filesystem::is_regular_file(*dir_iter))
                {
                    const boost::filesystem::path &currentFile = dir_iter->path();
                    if (currentFile.extension() == ".sqlite")
                    {
                        //printf ( "%s\n", currentFile.string().c_str() );
                        UTIL::deleteFile(currentFile.string().c_str());
                    }
                }
            }
        }
        else
        {
            LOG_ERROR("Project directory is not correct.");
            return;
        }
    }

    bool ProjectManagerGUI::SaveFile()
    {
        // cleanup the project folder
        cleanupProjectFolder();

        if (!_dummyUID.empty())
        {
            showError("Project Saving", "Cannot save a Dummy Project");
            return false;
        }

        //Get the Database to connect to from the Global Setting configuration
        if (!_globalSettingComponent.valid())
            return false;

        bool isProjectLocal = _globalSettingComponent->isLocalProject();
        if (isProjectLocal)
        {
            bool status = _saveLocalProject();
            return status;
        }

        if (!_projectSaveAndLoadComponent.valid())
            return false;

        std::string link = _getProjectLocationOnServer();

        if (!_projectSaveAndLoadComponent->isWebserverAlive(link))
        {
            emit showError("Apache Connectivity", "Apache Server not reachable, Check the Connection please.");
            return false;
        }

        // There is not existing Database connection then return;
        ELEMENTS::ISettingComponent::SettingsAttributes presentDatabaseSetting = _globalSettingComponent->getGlobalSetting
            (ELEMENTS::ISettingComponent::PROJECTDATABASECONN);

        ELEMENTS::ISettingComponent::SettingsAttributes presentWebServerSetting = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if (presentWebServerSetting.keyValuePair.find("Path") != presentWebServerSetting.keyValuePair.end())
            _projectLocationOnServer = presentWebServerSetting.keyValuePair["Path"];
        else
            return false;

        if (!_getDatabaseCredential(_databaseToConnect))
        {
            emit showError("Database Connection", "Not able to Connect to get Database Credentail");
            return false;
        }

        if (!_lookUpUIHandler->makeConnectionWithDatabase(_databaseCredential))
        {
            emit showError("Database Connection", "Not able to Connect to Database");
            return false;
        }

        //UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();

        std::string fileName = _getPresentProject();

        std::string projectFilename;
        std::string temporaryPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "/Project";
        projectFilename = temporaryPath + "/" + SERVERPLANNERDIRECTORY + "/" + _getPresentProject() + "/" + fileName + "." + PLANNERFILEEXTENSION;

        bool isFolderPresent = osgDB::makeDirectoryForFile(projectFilename);
        if (isFolderPresent)
        {
            _flushTheDirectoryContents(osgDB::getFilePath(projectFilename));
        }

        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();

        if (wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return false;
        }

        wmtnr->setProjectFile(projectFilename);

        //Hav to make sure that u hav set the present Webserver where u want to upload the project
        _addEntryToDatabase(projectFilename, SMCUI::ILookUpProjectUIHandler::SAVEPROJECT, SMCUI::ILookUpProjectUIHandler::PLANNER);

        CORE::IWorld *world = wmtnr->getWorldMap().begin()->second.get();

        if (world == NULL)
        {
            LOG_ERROR("World not valid");
            return false;
        }

        DB::writeObjectFile(world->getInterface<CORE::IObject>(), projectFilename);

        _lookUpUIHandler->closeDatabaseConnection();

        //Upload the file created in the Project Folder to the Server
        _uploadProjectFilesToServer(projectFilename, SMCUI::ILookUpProjectUIHandler::PLANNER);

        return true;
    }

    void ProjectManagerGUI::_clearUndoStack()
    {
        APP::IManager* managr = getGUIManager()->getInterface<APP::IManager>();

        if (managr != NULL)
        {
            APP::IApplication *app = managr->getApplication();

            if (app != NULL)
            {
                APP::IManager* uiIMgr = app->getManagerByClassname(VizUI::UIHandlerManager::ClassNameStr);
                if (NULL != uiIMgr)
                {
                    APP::IUIHandlerManager* uiHandlerMgr = uiIMgr->getInterface<APP::IUIHandlerManager>();
                    if (NULL != uiHandlerMgr)
                    {
                        uiHandlerMgr->clearUndoStack();
                    }
                }
            }
        }
    }

    void ProjectManagerGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                _lookUpUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ILookUpProjectUIHandler>(getGUIManager());

                CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer =
                    CORE::WorldMaintainer::instance();

                CORE::RefPtr<CORE::IComponent> component = worldMaintainer->getComponentByName("SettingComponent");

                if (component.valid())
                {
                    _globalSettingComponent = component->getInterface<ELEMENTS::ISettingComponent>();
                }
#ifdef WIN32
                CORE::RefPtr<CORE::IComponent> saveLoadcomponent =
                    worldMaintainer->getComponentByName("ProjectLoadAndSaveComponent");
#else //WIN32
                CORE::RefPtr<CORE::IComponent> saveLoadcomponent =
                    worldMaintainer->getComponentByClassname("SMCElements::ProjectLoadAndSaveComponent");
#endif //WIN32

                if (saveLoadcomponent.valid())
                {
                    _projectSaveAndLoadComponent = saveLoadcomponent->getInterface
                        <SMCElements::IProjectLoadAndSaveComponent>();
                }


                _subscribe(worldMaintainer.get(), *CORE::IWorld::WorldLoadedMessageType);
                _subscribe(worldMaintainer.get(), *CORE::IWorld::WorldRemovedMessageType);

                //Setting the application type based on the QtApplication name
                APP::IApplication *application = getGUIManager()->getInterface<APP::IManager>()->getApplication();

                QString mainWindowTitle = "GeorbIS";

                CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                QWidget* mainWindow = qtapp->getLayoutWidget();
                if (mainWindow)
                {
                    mainWindow->setWindowTitle(mainWindowTitle);
                    mainWindow->setGeometry(_windowPosX, _windowPosY, _normalWindowWidth, _normalWindowHeight);
                    //mainWindow->show();
                }

                loadFiles();

            }
            catch (...)
            {
                // XXX - Ignoring the message here?
            }
        }

        else if (messageType == *DB::IProjectLoadingStatusMessage::ProjectLoadingStatusMessageType)
        {
            DB::IProjectLoadingStatusMessage* projectLoadingStatusMessage =
                message.getInterface<DB::IProjectLoadingStatusMessage>();

            if (projectLoadingStatusMessage->getStatus() == DB::IProjectLoadingStatusMessage::PENDING)
            {
                emit _setProgressValue(projectLoadingStatusMessage->getProgress());
            }

            if (projectLoadingStatusMessage->getStatus() == DB::IProjectLoadingStatusMessage::SUCCESS)
            {
                emit _setProgressValue(100);
            }
        }
        else if (messageType == *DB::IProjectLoaderComponent::ProjectLoadedMessageType)
        {
            LOG_DEBUG("Received ProjectLoadedMessageType,..");

            _unsubscribeFromProjectLoaderComponentMessages();

            QObject* statusBar = _findChild("statusBar");
            if (statusBar)
            {
                QMetaObject::invokeMethod(statusBar, "close");
            }

            CORE::RefPtr<VizUI::ISelectionUIHandler> selUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            if (selUIHandler.valid())
            {
                selUIHandler->setSelectionState(true);
            }

            _loadingProject = false;

        }
        else if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            _resetPopUpGUI();
        }
        else if (messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            _performHardReset();
        }
        else
        {
            VizQt::GUI::update(messageType, message);
        }
    }

    void ProjectManagerGUI::_onSetProgressValue(int value)
    {
        QObject* statusBar = _findChild("statusBar");

        if ((value == 100) || (value < 0))
        {
            if (statusBar)
            {
                if (value == 100)
                {
                    QMetaObject::invokeMethod(statusBar, "setProgressStatus",
                        Q_ARG(QVariant, QVariant(QString("Project loaded."))),
                        Q_ARG(QVariant, QVariant(false)), Q_ARG(QVariant, QVariant(value)));
                }
                //QMetaObject::invokeMethod(statusBar, "setProgressStatus", 
                //    Q_ARG(QVariant, QVariant(QString(""))),
                //    Q_ARG(QVariant, QVariant(false)),Q_ARG(QVariant, QVariant(value)));

            }
        }
        else
        {
            if (statusBar)
            {
                QMetaObject::invokeMethod(statusBar, "setProgressStatus",
                    Q_ARG(QVariant, QVariant(QString("Loading Project..."))),
                    Q_ARG(QVariant, QVariant(true)), Q_ARG(QVariant, QVariant(value)));
            }
        }
    }

    void ProjectManagerGUI::_resetPopUpGUI()
    {
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if (popupLoader)
        {
            QMetaObject::invokeMethod(popupLoader, "unloadAll");
        }
    }

    void ProjectManagerGUI::_subscribeToProjectLoaderComponentMessages()
    {
        CORE::IComponent* projectLoadComponent = ELEMENTS::GetComponentFromMaintainer("ProjectLoaderComponent");
        _subscribe(projectLoadComponent, *DB::IProjectLoadingStatusMessage::ProjectLoadingStatusMessageType);
        _subscribe(projectLoadComponent, *DB::IProjectLoaderComponent::ProjectLoadedMessageType);
    }

    void ProjectManagerGUI::_unsubscribeFromProjectLoaderComponentMessages()
    {
        CORE::IComponent* projectLoadComponent = ELEMENTS::GetComponentFromMaintainer("ProjectLoaderComponent");
        _unsubscribe(projectLoadComponent, *DB::IProjectLoadingStatusMessage::ProjectLoadingStatusMessageType);
        _unsubscribe(projectLoadComponent, *DB::IProjectLoaderComponent::ProjectLoadedMessageType);
    }
    void ProjectManagerGUI::loadsmpTemplatesMenu()
    {
        // function will connect deleteTemplate call when template menu will load at QT end.
        QObject *smTemplateMenu = _findChild("smpTemplatesMenu");
        if (smTemplateMenu)
        {
            QObject::connect(smTemplateMenu, SIGNAL(deleteTemplate(QString)), this, SLOT(deleteTemplate(QString)));
        }
    }
    void ProjectManagerGUI::refreshTemplateComboBox()
    {
        QObject *smTemplateMenu = _findChild("smpTemplatesMenu");
        if (nullptr != smTemplateMenu)
        {
            // populate Templates

            // finding to be deleted entry in vector and erasing it

            std::vector<std::string>::iterator itr = std::find(_templateProjects.begin(), _templateProjects.end(), _strTemlateName.toStdString());
            if (itr != _templateProjects.end())
                _templateProjects.erase(itr);

            QList<QObject*> qTemplateProjectListModel;
            qTemplateProjectListModel.clear();
            for (unsigned int i = 0; i < _templateProjects.size(); i++)
            {
                qTemplateProjectListModel.append(new VizComboBoxElement(QString::fromStdString(_templateProjects[i]), QString::number(i)));
            }

            _setContextProperty("templateProjectsList", QVariant::fromValue(qTemplateProjectListModel));

            // selecting next available template
            // if no template is availbale then selecting base template as dafault template.

            if (_templateProjects.size() == 1 || _templateProjects[0] == "Default")
            {
                smTemplateMenu->setProperty("curTemplateComboBoxValue", QString::fromStdString("Default"));
                // when list is empty set null name in jason file.
                setDefaultTemplate("Default");
            }
            else
            {
                smTemplateMenu->setProperty("curTemplateComboBoxValue", QString::fromStdString(_templateProjects[0]));
                setDefaultTemplate(QString::fromStdString(_templateProjects[0]));
            }

        }
    }

    } // namespace SMCQt
