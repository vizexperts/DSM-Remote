/****************************************************************************
 *
 * File             : NetworkJoinGUI.h
 * Description      : NetworkJoinGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, CAIR, DRDO
 *****************************************************************************/

#include <SMCQt/NetworkJoinGUI.h>
#include <SMCQt/ProjectManagerGUI.h>

#include <VizQt/QtUtils.h>
#include <VizQt/NameValidator.h>

#include <HLA/NetworkCollaborationManager.h>

#include <Core/IStringMessage.h>
#include <Core/InterfaceUtils.h>
#include <Core/IAnimationComponent.h>
#include <Core/ITerrain.h>
#include <Core/IMessageFactory.h>
#include <Core/CoreRegistry.h>
#include <Core/IRasterData.h>
#include <Core/RasterData.h>
#include <Core/IWorldMaintainer.h>
#include <Core/WorldMaintainer.h>
#include <Core/AttributeTypes.h>

#include <Util/StringUtils.h>
#include <Util/FileUtils.h>

#include <App/IApplication.h>
#include <App/IUIHandlerManager.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#ifdef USE_QT4
    #include <QtGui/QPushButton>
    #include <QtGui/QLineEdit>
    #include <QtGui/QListWidget>
    #include <QtGui/QDialog>
    #include <QtGui/QMessageBox>
#else
    #include <QtWidgets/QPushButton>
    #include <QtWidgets/QLineEdit>
    #include <QtWidgets/QListWidget>
    #include <QtWidgets/QDialog>
    #include <QtWidgets/QMessageBox>
    
#endif

#include <QtCore/QString>

#include <VizUI/UIHandlerManager.h>


#include <Loader/IWorldReaderWriter.h>

#include <Terrain/RasterObject.h>

#include <Elements/ElementsPlugin.h>

#include <DB/ReadFile.h>

static const std::string defaultProjectFile = "../../tests/DefaultHLA.pro";

typedef LOADER::IWorldReaderWriter* (*CreateWorldLoaderPtr)();
typedef void (*DestroyWorldLoaderPtr)(LOADER::IWorldReaderWriter*);

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, NetworkJoinGUI);
    DEFINE_IREFERENCED(NetworkJoinGUI, VizQt::LayoutFileGUI);

    const std::string NetworkJoinGUI::LWProjectList="LWProjectList";
    const std::string NetworkJoinGUI::LEProjectName="LEProjectName";
    const std::string NetworkJoinGUI::PBSearchProjects="PBSearchProjects";
    const std::string NetworkJoinGUI::PBJoin="PBJoin";
    const std::string NetworkJoinGUI::PBCancel="PBCancel";
    
    NetworkJoinGUI::NetworkJoinGUI()
        : _joined(false)
    {
    }

    NetworkJoinGUI::~NetworkJoinGUI()
    {
        setActive(false);
    }

    void 
    NetworkJoinGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {   
                APP::IManager* mgr = getGUIManager()->getInterface<APP::IManager>();
                APP::IApplication* app = mgr->getApplication();
                mgr = app->getManagerByName(HLA::NetworkCollaborationManager::ClassNameStr);

                if(mgr)
                {
                    _netwrkCollaborationMgr = mgr->getInterface<HLA::INetworkCollaborationManager>();
                    if(!_netwrkCollaborationMgr.valid())
                    {
                        setComplete(false);
                    }
                    else
                    {
                        _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ProjectResignRequestReceivedMessageType.get());
                        if(_syncMode.compare("Event")==0)
                        {
                            _netwrkCollaborationMgr->setSynchronizationMode(HLA::IHLAComponent::Events);
                        }
                        else
                        {
                            _netwrkCollaborationMgr->setSynchronizationMode(HLA::IHLAComponent::Objects);
                        }
                    }
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ExistingProjectFoundMessageType.get())
        {
            CORE::IStringMessage* strMsg = message.getInterface<CORE::IStringMessage>();
            std::string str = strMsg->getString();
            _getChild<QListWidget>(LWProjectList)->addItem(QString::fromStdString(str));
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ProjectResignRequestReceivedMessageType.get())
        {
            QMessageBox msgBox;
            msgBox.setObjectName("NetworkJoinGUI_messageBox1");
            msgBox.setText(QString("A request has been made by the project publisher to destroy the project.\n\n") +
                                          QString("1. Select Resign to resign from the project.\n") + 
                                          QString("2. Select Cancel to cancel the request.\n") );                                            
                                          //QString("3. Select Cancel&Own to cancel the request and take the ownership of the Camera.\n") );

            QPushButton *resign = msgBox.addButton(tr("Resign"), QMessageBox::AcceptRole);
            resign->setObjectName("NetworkJoinGUI_messageBox1_resign");
            QPushButton *cancel = msgBox.addButton(QMessageBox::Cancel);
            cancel->setObjectName("NetworkJoinGUI_messageBox1_cancel");

            // Making the workflow simple. Either application can resign or cancel the request for destruction.
            //QPushButton *cancelAndOwn = msgBox.addButton(tr("Cancel&Own"), QMessageBox::RejectRole);

            msgBox.exec();

            if (msgBox.clickedButton() == resign) 
            {
                _handleJoinButtonClicked();
            } 
            else if (msgBox.clickedButton() == cancel) 
            {
                _netwrkCollaborationMgr->ignoreProjectDestructionRequest();
            }
        }
    }
            
    void 
    NetworkJoinGUI::initializeAttributes()
    {
        LayoutFileGUI::initializeAttributes();

        _addWidgetName(LWProjectList);
        _addWidgetName(LEProjectName);
        _addWidgetName(PBSearchProjects);
        _addWidgetName(PBJoin);
        _addWidgetName(PBCancel);

                // Add Mode attribute
        _addAttribute(new CORE::StringAttribute("Mode", "Mode",
            CORE::StringAttribute::SetFuncType(this, &NetworkJoinGUI::setSyncMode),
            CORE::StringAttribute::GetFuncType(this, &NetworkJoinGUI::getSyncMode),
            "Sync Mode Setting",
            "Sync Mode Setting"));
    }

    void 
    NetworkJoinGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }

        _parent->show();
        _parent->activateWindow ();
        _parent->raise();
        _parent->setFocus(Qt::OtherFocusReason);

        // If value is set properly then set focus for area UI handler
        // Check whether UI Handler is valid or not
        if(!getComplete())
        {
            return;
        }

        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                if(!_joined)
                    _getChild<QLineEdit>(LEProjectName)->setText("");
                _getChild<QListWidget>(LWProjectList)->hide();
                _getChild<QListWidget>(LWProjectList)->clear();
                qobject_cast<QDialog*>(_parent)->adjustSize();
            }
            else
            {
                _unsubscribe(_netwrkCollaborationMgr.get(), *HLA::INetworkCollaborationManager::ExistingProjectFoundMessageType.get());
            }
            LayoutFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void 
    NetworkJoinGUI::setSyncMode(const std::string &mode)
    {
        _syncMode = mode;
    }

    std::string 
    NetworkJoinGUI::getSyncMode()
    {
        return _syncMode;
    }

    void 
    NetworkJoinGUI::_handleJoinButtonClicked()
    {
        QPushButton *pb = _getChild<QPushButton>(PBJoin);
        QLineEdit* le = _getChild<QLineEdit>(LEProjectName);

        if(pb->text().contains("Join"))
        {   
            if(le->text().isEmpty())
            {
                showError( "Error Joining Project", "Project File name is empty");
                return;
            }

            std::string errorMessage;
            HLA::INetworkCollaborationManager::ApplicationState applicationState = _netwrkCollaborationMgr->getApplicationState();
            if(applicationState != HLA::INetworkCollaborationManager::NONE)
            {
                switch(applicationState)
                {
                case HLA::INetworkCollaborationManager::PUBLISHED:
                        errorMessage="Project Published.";
                        break;          
                    case HLA::INetworkCollaborationManager::PUBLISHED_AND_SCREENSHARED:
                        errorMessage="Project Published and Screen Shared.";
                        break;
                    case HLA::INetworkCollaborationManager::JOINED:
                        errorMessage="Project Already Joined to Another project.";
                        break;
                    case HLA::INetworkCollaborationManager::JOINED_AND_SCREENSHARED:
                        errorMessage="Project Already Joined to another Project and Screen Shared.";
                        break;
                }

                showError( "Can not process join request", errorMessage.c_str());
                return;
            }
            
            //Informing the user that the current progress will be lost 
            if(_netwrkCollaborationMgr->getSynchronizationMode()==HLA::IHLAComponent::Objects)
            {
                QMessageBox msgBox;
                msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
                
                msgBox.setText("Current Progress will be lost.");
                msgBox.setInformativeText("Do you want to save your changes?");

                msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
                msgBox.setDefaultButton(QMessageBox::Save);
                int ret = msgBox.exec();
            
                switch (ret) 
                {
                    case QMessageBox::Save:
                        _saveProjectAndLoadEmptyWorld();
                    break;

                   case QMessageBox::Discard:
                       _loadEmptyWorld();
                       break;

                   case QMessageBox::Cancel:
                       return;
                       break;

                   default:
                       return;
                   break;
                }
            }

            std::string projectName = le->text().toStdString();
            std::string errMsg;
            if(_netwrkCollaborationMgr->joinProject(projectName, errMsg))
            {
                QMessageBox::information(_parent, "Success", "Project Joined Successfully.");                    
                _joined=true;
                _joinedProjectNm=projectName;
                _getChild<QPushButton>(PBSearchProjects)->setEnabled(false);
                le->setEnabled(false);
                pb->setText("Resign");
                _netwrkCollaborationMgr->synchronizeWithProject();
            }
            else
                QMessageBox::information(_parent, "Failure", "Project Failed to Join.\n Possible Reason : " + QString::fromStdString(errMsg));                

        }
        else if(pb->text().contains("Resign"))
        {
            std::string errMsg;
            HLA::INetworkCollaborationManager::ApplicationState applicationState = _netwrkCollaborationMgr->getApplicationState();
            if(applicationState == HLA::INetworkCollaborationManager::JOINED_AND_SCREENSHARED)
            {
                showError( "Unable to Resign", "Screen is shared. Please release screen ownership before resignation.");
                return;
            }

            if(_netwrkCollaborationMgr->resignProject(errMsg))
            {
                QMessageBox::information(_parent, "Success", "Project Resigned Successfully.");
                _joined=false;
                _getChild<QPushButton>(PBSearchProjects)->setEnabled(true);
                le->setEnabled(true);
                pb->setText("Join");
            }
            else
                QMessageBox::information(_parent, "Failure", "Project Failed to Resign.\n Possible Reason : " + QString::fromStdString(errMsg));            
        }
    }

    void 
    NetworkJoinGUI::_handleCancelButtonClicked()
    {
        qobject_cast<QDialog *>(_parent)->close();
    }

    void 
    NetworkJoinGUI::_handleSearchProjectsButtonClicked()
    {
        _getChild<QListWidget>(LWProjectList)->clear();
        _getChild<QListWidget>(LWProjectList)->show();
        qobject_cast<QDialog*>(_parent)->adjustSize();
        _subscribe(_netwrkCollaborationMgr.get(), *HLA::INetworkCollaborationManager::ExistingProjectFoundMessageType.get());
        _netwrkCollaborationMgr->searchExistingProjects();
    }

    void 
    NetworkJoinGUI::_handleItemClicked(QListWidgetItem* item)
    {
        _getChild<QLineEdit>(LEProjectName)->setText(item->text());
    }

    void 
    NetworkJoinGUI::_loadAndSubscribeSlots()
    {
        LayoutFileGUI::_loadAndSubscribeSlots();
        if(!getComplete())
        {
            return;
        }
        
        _connect(LWProjectList,        SIGNAL(itemClicked(QListWidgetItem*)),    SLOT(_handleItemClicked(QListWidgetItem*)));
        _connect(PBSearchProjects,    SIGNAL(clicked()),                        SLOT(_handleSearchProjectsButtonClicked()));
        _connect(PBJoin,            SIGNAL(clicked()),                        SLOT(_handleJoinButtonClicked()));
        _connect(PBCancel,            SIGNAL(clicked()),                        SLOT(_handleCancelButtonClicked()));

        _getChild<QPushButton>(PBSearchProjects)->setVisible(false);
    }

    bool
    NetworkJoinGUI::_saveProjectAndLoadEmptyWorld()
    {
        // clear undo stack
        APP::IGUIManager *guiManager = getGUIManager();
        if(!guiManager)
            return false;

        APP::IManager* managr = guiManager->getInterface<APP::IManager>();
        if(!managr)
            return false;

        APP::IApplication *app = managr->getApplication();
        if(!app)
            return false;

        APP::IManager* uiIMgr = app->getManagerByClassname(VizUI::UIHandlerManager::ClassNameStr);
        if(NULL != uiIMgr)
        {
            APP::IUIHandlerManager* uiHandlerMgr = uiIMgr->getInterface<APP::IUIHandlerManager>();
            if(NULL != uiHandlerMgr)
            {
                uiHandlerMgr->clearUndoStack();
            }
        }
        
        //TBD -  get recent place or project  
        QString directory = "G:/";

        // open file dialog for saving
        QString fileName = VizQt::FileUtils::getSaveFileName(_parent, tr("Save File"), directory, tr("Projects/Files (*.pro)"));

        if(fileName != "")
        {
            // load library
            std::string libraryname = UTIL::getPlatformLibraryName("LOADER");
            osg::ref_ptr<osgDB::DynamicLibrary> dl = osgDB::DynamicLibrary::loadLibrary(std::string("../../lib/") + libraryname);

            if(!dl.valid())
            {
                LOG_ERROR("Unable to load library");
                return false;
            }

            // create loader functions
            CreateWorldLoaderPtr createWLoader     = (CreateWorldLoaderPtr)(dl->getProcAddress("CreateWorldLoader"));
            DestroyWorldLoaderPtr destroyWLoader   = (DestroyWorldLoaderPtr)(dl->getProcAddress("DestroyWorldLoader"));

            // get world maintainer instance
            CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();

            if(wmtnr == NULL)
            {
                LOG_ERROR("World maintainer not valid");
                return false;
            }
            wmtnr->setProjectFile(fileName.toStdString());

            // XXX - get first world in world map
            // this should be changed when support for multiple worlds are given
            CORE::IWorld *world = wmtnr->getWorldMap().begin()->second.get();

            if(world == NULL)
            {
                LOG_ERROR("World not valid");
                return false;
            }

            // Create an world loader
            LOADER::IWorldReaderWriter* wloader = createWLoader();                
            if(wmtnr == NULL)
            {
                LOG_ERROR("Unable to create loader");
                return false;
            }

            // write the project file
            CORE::RefPtr<CORE::NamedGroupAttribute> ngattr = new CORE::NamedGroupAttribute("");
            CORE::RefPtr<CORE::NamedAttribute> fileAttr = CORE::NamedAttribute::CreateFromType(CORE::DataType::STRING, "--output");    
            fileAttr->fromString(fileName.toStdString());
            ngattr->addAttribute(*fileAttr);
            wloader->write(world, *ngattr);
            destroyWLoader(wloader);

            wmtnr->setProjectFile(fileName.toStdString());
        }

        _loadEmptyWorld();
        return true;
    }

    bool
    NetworkJoinGUI::_loadEmptyWorld()
    {
        // get world maintainer instance
        CORE::IWorldMaintainer *wmtnr = CORE::WorldMaintainer::instance();
        if(wmtnr == NULL)
        {
            LOG_ERROR("World maintainer not valid");
            return false;
        }

        wmtnr->setProjectFile(defaultProjectFile);

        CORE::RefPtr<CORE::IObject> worldObject = DB::readObjectFile(defaultProjectFile);
        if(!worldObject.valid())
            return false;

        CORE::RefPtr<CORE::IWorld> world = worldObject->getInterface<CORE::IWorld>();
        if(!world.valid())
        {
            LOG_ERROR("Unable to create world from loader");
            return false;
        }


        if(!wmtnr->getWorldMap().empty())
        {
            // XXX - get first world in world map
            // this should be changed when support for multiple worlds are given
            CORE::IWorld *prevWorld = wmtnr->getWorldMap().begin()->second.get();

            if(prevWorld == NULL)
            {
                LOG_ERROR("Unable to remove world :  Cannot find previous map");
                return false;
            }

            CORE::IBase *baseWorld = prevWorld->getInterface<CORE::IBase>();

            if(baseWorld == NULL)
            {
                LOG_ERROR("IBase doesnt exist in world");
                return false;
            }

            wmtnr->clearWorlds();
        }

        wmtnr->addWorld(world.get());

        return true;
    }

}
