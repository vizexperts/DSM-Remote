/*****************************************************************************
*
* File             : KMeanGUI.cpp
* Description      : KMeanGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/


#include <SMCQt/KMeanGUI.h>

#include <osgDB/FileNameUtils>

#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IFilterStatusMessage.h>

#include <SMCUI/AddContentUIHandler.h>

#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/IUIHandlerManager.h>
#include <App/ApplicationRegistry.h>

#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>

#include <Util/Log.h>
#include <Util/StringUtils.h>

#include <VizQt/GUI.h>

#include <QFileDialog>

#include <QtCore/QFile>
#include <QLabel>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QMainWindow>

#include <QtUiTools/QtUiTools>

#include <iostream>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, KMeanGUI);
    DEFINE_IREFERENCED(KMeanGUI,CORE::Base);

    const std::string KMeanGUI::KMeanGUIObjectName = "KMeanGUI";

    KMeanGUI::KMeanGUI()
        :_stereoWizard(NULL)
    {
    }

    KMeanGUI::~KMeanGUI()
    {
        setActive( false );
    }

    void
        KMeanGUI::_loadAddContentUIHandler()
    {
        _addContentUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
    }

    // to load the connect and disconnect slot from the qml
    void KMeanGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_updateGUIStatus()),this, SLOT(_handleGUIStatus()),Qt::QueuedConnection);
    }


    void KMeanGUI::setActive(bool value)
    {
        if(getActive() == value)
            return;

        try
        {
            _reset();

            if(value)
            {               
                QObject* kMeansObject = _findChild(KMeanGUIObjectName);

                // connect the signal of various buttons in 
                if(kMeansObject)
                {
                    QObject::connect(kMeansObject, SIGNAL(browseInputImage()), 
                        this, SLOT(_handleInputBrowseButtonClicked()), Qt::UniqueConnection);

                    QObject::connect(kMeansObject, SIGNAL(viewImage(QString)),
                        this, SLOT(_viewImage(QString)), Qt::QueuedConnection);

                    QObject::connect(kMeansObject, SIGNAL(browseOutputImage()), 
                        this, SLOT(_handleoutputBrowseButtonClicked()), Qt::UniqueConnection);

                    QObject::connect(kMeansObject, SIGNAL(calculate()), 
                        this, SLOT(_handleStartButtonClicked()), Qt::UniqueConnection);

                    QObject::connect(kMeansObject, SIGNAL(cancelButtonClicked()), this,
                        SLOT(_handleCancelButtonClicked()), Qt::UniqueConnection);

                }
            }
            else
            {                
                _handleCancelButtonClicked();

            }
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void
        KMeanGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            { 
                _loadAddContentUIHandler();
            }
            catch(...)
            {}
        }

        // to check the filter status
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                emit _updateGUIStatus();
                emit showError("Filter Status", "Successfully applied filter", false);
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
            {
                QObject* kMeansObject = _findChild(KMeanGUIObjectName);
                if(kMeansObject)
                {
                    kMeansObject->setProperty("okButtonEnabled",true);
                    kMeansObject->setProperty("cancelButtonEnabled",false);
                    kMeansObject->setProperty("calculating",false);
                }
                emit showError("Filter Status", "Failed to apply filter");
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }

    // to browse the input image
    void 
        KMeanGUI::_handleInputBrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        std::string formatFilter = "Image (*.jpg *.jpeg *.bmp *.png *.tif *.tiff *.img)";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {
            QObject* kMeansObject = _findChild(KMeanGUIObjectName);
            if(kMeansObject != NULL)
            {
                kMeansObject->setProperty("inputImagePath",QVariant::fromValue(filename));
                kMeansObject->setProperty("inputButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }
    }

    void 
        KMeanGUI::_handleoutputBrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        std::string formatFilter = "Image (*.jpg *.jpeg *.bmp *.png *.tif *.tiff *.img)";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getSaveFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {

            QObject* kMeansObject = _findChild(KMeanGUIObjectName);
            if(kMeansObject != NULL)
            {
                kMeansObject->setProperty("outputImagePath",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in writing File", "Unknown Error", true);
            }
        }
    }


    void 
        KMeanGUI::closeStereoWizard(Stereo::MainWindowGUI* stereoWiz)
    {        
        if (stereoWiz)
        {
            delete stereoWiz;
            stereoWiz = NULL;
        }
    }

    void
        KMeanGUI::_viewImage(QString Image)
    {
        std::string filename = Image.toStdString();        
        if(filename != "")
        {                
            Stereo::MainWindowGUI* stereoWizard = new Stereo::MainWindowGUI(filename,Stereo::Image::NONE);
            _stereoWizard.push_back( stereoWizard);
            QObject::connect(stereoWizard,SIGNAL(applicationClosed(Stereo::MainWindowGUI*)),
                this,SLOT(closeStereoWizard(Stereo::MainWindowGUI*)), Qt::QueuedConnection);
            stereoWizard->showMaximized();
            stereoWizard->setWindowTitle(filename.c_str());
        }

    }

    //function called when calculate button is clicked
    void
        KMeanGUI::_handleStartButtonClicked()
    {
        QObject* kMeansObject = _findChild(KMeanGUIObjectName);

        if(kMeansObject)
        {

            // Pass the parameters to the visitor
            QString classesTemp = kMeansObject->property("classes").toString();
            if(classesTemp == "")
            {
                showError("Classes", "Please specify number of Classes");
                return;
            }
            unsigned int classes = classesTemp.toUInt();

            QString iterationsTemp = kMeansObject->property("iterations").toString();
            if(iterationsTemp == "")
            {
                showError("Iterations", "Please specify number of Iterations");
                return;
            }
            unsigned int iterations = iterationsTemp.toUInt();

            std::string output = kMeansObject->property("outputImagePath").toString().toStdString();
            if(output == "")
            {
                showError("outputName", "Please enter output field");
                return;
            }

            std::string input = kMeansObject->property("inputImagePath").toString().toStdString(); 
            if(input == "")
            {
                showError("InputFile", "Please enter input field");
                return;
            }

            kMeansObject->setProperty("okButtonEnabled",false);
            kMeansObject->setProperty("cancelButtonEnabled",true); 
            kMeansObject->setProperty("calculating",true);
            kMeansObject->setProperty("outputButtonEnabled",false);



            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
            CORE::IBase* applyNode = terrain->getInterface<CORE::IBase>();

            if(!_kmean.valid())
            {
                CORE::RefPtr<CORE::IVisitorFactory> vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::KMeansClusterType);
                _kmean = _visitor->getInterface<GISCOMPUTE::IKMeansCluster>();

            }

            _kmean->setInputImageURL(input);
            _kmean->setOutputImageURL(output);
            _kmean->setIterations(iterations);
            _kmean->setClasses(classes);



            _subscribe(_kmean.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

            applyNode->accept(*(_kmean->getInterface<CORE::IBaseVisitor>()));
        }


    }

    //function called when cancel button is clicked
    void
        KMeanGUI::_handleCancelButtonClicked()
    {        
        // to stop the filter
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }


    }

    // updates GUI according to the filter status
    void 
        KMeanGUI::_handleGUIStatus()
    {
        QObject* kMeansObject = _findChild(KMeanGUIObjectName);

        if(kMeansObject)
        {
            kMeansObject->setProperty("okButtonEnabled",true);
            kMeansObject->setProperty("cancelButtonEnabled",false);
            kMeansObject->setProperty("calculating",false);
            kMeansObject->setProperty("inputButtonEnabled",true);
            kMeansObject->setProperty("outputButtonEnabled",true);    
        }


    }

    void
        KMeanGUI::_reset()
    {
        QObject* kMeansObject = _findChild(KMeanGUIObjectName);

        if(kMeansObject)
        {
            kMeansObject->setProperty("inputImagePath","");
            kMeansObject->setProperty("outputImagePath","");
            kMeansObject->setProperty("iterations","");
            kMeansObject->setProperty("classes","");
            kMeansObject->setProperty("okButtonEnabled",true);
            kMeansObject->setProperty("cancelButtonEnabled",false);
            kMeansObject->setProperty("calculating",false);
            kMeansObject->setProperty("inputButtonEnabled",false);
            kMeansObject->setProperty("outputButtonEnabled",false);

        }
    }

} // namespace SMCQt
