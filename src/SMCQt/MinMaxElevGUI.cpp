/****************************************************************************
*
* File             : MinMaxElevGUI.h
* Description      : MinMaxElevGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <SMCQt/MinMaxElevGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/IMinMaxElevCalcVisitor.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, MinMaxElevGUI);
    DEFINE_IREFERENCED(MinMaxElevGUI,DeclarativeFileGUI);

    const std::string MinMaxElevGUI::MinMaxElevGUIObject = "MinMaxElev";

    MinMaxElevGUI::MinMaxElevGUI()
    {}

    MinMaxElevGUI::~MinMaxElevGUI()
    {}

    void MinMaxElevGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_updateMinMax()), this , SLOT(_onUpdateMinMax()), Qt::QueuedConnection);

    }

    void MinMaxElevGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IMinMaxElevUIHandler>(getGUIManager());

                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();

                    if(_minMaxElevMenuObject)
                    {
                        _minMaxElevMenuObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        _minMaxElevMenuObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        _minMaxElevMenuObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        _minMaxElevMenuObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }

                //  _handlePBAreaClicked(true);
            }
        }

        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
            }

            else
            {
                // set value to 100%
                if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
                {

                    // Get the min and max points
                    _minPoint = _uiHandler->getMinimumElevationPoint();
                    _maxPoint = _uiHandler->getMaximumElevationPoint();
                    emit _updateMinMax();
                }
                else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    //emit showError("Filter Status", "Failed to apply filter");

                    // reset min max values
                    //    emit _resetMinMax();

                }
                else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_STOPPED)
                {
                    // emit showInformation("Filter Status", "Filter Stopped");

                    // reset min max values
                    // emit _resetMinMax();

                }

            }
        }

        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void MinMaxElevGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                // TODO : Populate the type combobox
                _minMaxElevMenuObject = _findChild(MinMaxElevGUIObject);

                if(_minMaxElevMenuObject)
                {
                    // enable start button
                    _minMaxElevMenuObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    _minMaxElevMenuObject->setProperty("isStopButtonEnabled",false);

                    _minMaxElevMenuObject = _findChild(MinMaxElevGUIObject);

                    // start and stop button handlers
                    connect(_minMaxElevMenuObject, SIGNAL(startAnalysis()),this,SLOT(_handlePBStartClicked()),Qt::UniqueConnection);
                    connect(_minMaxElevMenuObject, SIGNAL(stopAnalysis()),this, SLOT(_handlePBStopClicked()),Qt::UniqueConnection);
                    connect(_minMaxElevMenuObject, SIGNAL(markPosition(bool)),this, SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);

                    QObject::connect(this, SIGNAL(_resetMinMax()), this , SLOT(_onReset()), Qt::QueuedConnection);
                }
            }
            else
            {
                _onReset();
                _handlePBStopClicked();

                _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

                // _areaHandler->removeCreatedArea();

                if(_area.valid())
                {
                    _removeObject(_area.get());
                    _area = NULL;
                }

            }

            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }

            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }


    //! 
    void MinMaxElevGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }

        if(_minMaxElevMenuObject)
        {
            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = _minMaxElevMenuObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = _minMaxElevMenuObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = _minMaxElevMenuObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() =  _minMaxElevMenuObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError( "Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
                        showError( "Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                }
            }



            _uiHandler->computeMinMaxElevation();
            _minMaxElevMenuObject->setProperty("calculating",true);
            _minMaxElevMenuObject->setProperty("isMarkSelected",false);
            _handlePBAreaClicked(false);
        }
    }

    void MinMaxElevGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();

        if(_minMaxElevMenuObject)
        {
            //disable stop button
            _minMaxElevMenuObject->setProperty("isStopButtonEnabled",false);
            _minMaxElevMenuObject->setProperty("isStartButtonEnabled",true);
            _minMaxElevMenuObject->setProperty("calculating",false); 
            _minMaxElevMenuObject->setProperty("isMarkSelected",false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
        } 
    }


    void MinMaxElevGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }


        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            if(_area.valid())
            {
                _removeObject(_area.get());
                _area = NULL;
            }
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler:: AREA_MODE_NONE);
            _areaHandler->setMode(SMCUI::IAreaUIHandler:: AREA_MODE_CREATE_RECTANGLE);
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            _area = _areaHandler->getCurrentArea();
            _areaHandler->setMode(SMCUI::IAreaUIHandler:: AREA_MODE_NO_OPERATION);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
    }


    void MinMaxElevGUI::_onReset()
    {
        if(_minMaxElevMenuObject)
        {
            _minMaxElevMenuObject->setProperty("isMarkSelected",false);
            _minMaxElevMenuObject->setProperty("bottomLeftLongitude","longitude");
            _minMaxElevMenuObject->setProperty("bottomLeftLatitude","latitude");
            _minMaxElevMenuObject->setProperty("topRightLongitude","longitude");
            _minMaxElevMenuObject->setProperty("topRightLatitude","latitude");

            //_handlePBAreaClicked(false);
        }
    }

    void MinMaxElevGUI::_onUpdateMinMax()
    {
        if(_minMaxElevMenuObject)
        {

            _minMaxElevMenuObject->setProperty("minLatitude",UTIL::ToString(_minPoint.x()).c_str());
            _minMaxElevMenuObject->setProperty("minLongitude",UTIL::ToString(_minPoint.y()).c_str());
            _minMaxElevMenuObject->setProperty("minAltitude",UTIL::ToString(_minPoint.z()).c_str());
            _minMaxElevMenuObject->setProperty("maxLatitude",UTIL::ToString(_maxPoint.x()).c_str());
            _minMaxElevMenuObject->setProperty("maxLongitude",UTIL::ToString(_maxPoint.y()).c_str());
            _minMaxElevMenuObject->setProperty("maxAltitude",UTIL::ToString(_maxPoint.z()).c_str());

            _minMaxElevMenuObject->setProperty("isStopButtonEnabled",false);
            _minMaxElevMenuObject->setProperty("isStartButtonEnabled",true);
            _minMaxElevMenuObject->setProperty("calculating",false);
            //_handlePBStopClicked();

        }
    }

}//namespace SMCQt
