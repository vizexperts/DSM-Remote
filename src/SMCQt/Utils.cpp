#include <SMCQt\Utils.h>
#include <Util/CoordinateConversionUtils.h>
#include <Util/DistanceBearingCalculationUtils.h>
#include <App/AccessElementUtils.h>
#include <VizUI/ICameraUIHandler.h>
#include <VizQt/IQtGUIManager.h>
#include <VizQt/GUI.h>
#include <VizQt/IQtGUI.h>

#include <Core/IPoint.h>
#include <Core/WorldMaintainer.h>
#include <Core/IGeoExtent.h>

#include <osg/ComputeBoundsVisitor>

#include <Elements/IModelFeatureObject.h>
#include <Elements/IModelResult.h>



namespace SMCQt
{
    void Utils::getModelLayerExtCenter(CORE::RefPtr<CORE::IObject> object, osg::Vec4d& extents, osg::Vec3d &center)
    {
        //get IBase interface to get osg node

        //calculating Bounding box for this object
        osg::ComputeBoundsVisitor cBVisitor;
        osg::BoundingBox &boundingBox(cBVisitor.getBoundingBox());

        osg::Node *currentNode = object->getOSGNode();

        osg::BoundingSphere bSphere = currentNode->getBound();

        extents = osg::Vec4(0, 0, bSphere.radius(), 0);

        osg::Vec3d ECEFCenter = UTIL::CoordinateConversionUtils::ECEFToGeodetic(bSphere.center());

        center = osg::Vec3d(ECEFCenter.y(), ECEFCenter.x(), ECEFCenter.z());
    }
}