/****************************************************************************
*
* File             : VectorLayerQueryBuilderGUI.h
* Description      : VectorLayerQueryBuilderGUI class definition
*
*****************************************************************************
* Copyright (c) 2013-2014, CAIR, DRDO
*****************************************************************************/
#include <iostream>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <Core/WorldMaintainer.h>
#include <SMCQt/VectorLayerQueryBuilderGUI.h>
#include <Elements/ElementsUtils.h>
#include <ogrsf_frmts.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, VectorLayerQueryBuilderGUI);
    DEFINE_IREFERENCED(VectorLayerQueryBuilderGUI, SMCQt::DeclarativeFileGUI);
    const std::string VectorLayerQueryBuilderGUI::VECTORQUERYBUILDEROBJECTNAME = "VectorQueryBuilder";
    VectorLayerQueryBuilderGUI::VectorLayerQueryBuilderGUI(): _toSearch("")
    {
    }

    VectorLayerQueryBuilderGUI::~VectorLayerQueryBuilderGUI()
    {

    }

    // XXX - Move these slots to a proper place
    void VectorLayerQueryBuilderGUI::setActive(bool value)
    {

        if (getActive() == value)
        {
            return;
        }

        if (value)
        {
            QObject* queryBuilderObject = _findChild(VECTORQUERYBUILDEROBJECTNAME);
            if (queryBuilderObject)
            {
                 connect(queryBuilderObject, SIGNAL(addToWhere(int)), this, SLOT(_handleAddToWhereButtonClicked(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(addValueToWhere(int)), this, SLOT(addValueToWhere(int)), Qt::UniqueConnection);

                connect(queryBuilderObject, SIGNAL(showAllValues(int)), this, SLOT(showAllValues(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(changeText(QString)), this, SLOT(changeText(QString)), Qt::UniqueConnection);

                connect(queryBuilderObject, SIGNAL(showSampleValues(int)), this, SLOT(showSampleValues(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(changeText(const QString&)), this, SLOT(changeText(const QString&)), Qt::UniqueConnection);


                connect(queryBuilderObject, SIGNAL(operatorButtonClicked(QString)), this, SLOT(_handleOperatorButtonClicked(QString)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(clearButtonClicked()), this, SLOT(_handleClearButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(okButtonClicked()), this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(closed()), this, SLOT(_queryBuilderClosed()), Qt::UniqueConnection);

                //**************
                _currentVectorShapeName = queryBuilderObject->property("shapeFileName").toString().toStdString();
                //!
                _populateFieldDefinition();



            }
        }
        else
        {

        }
        QtGUI::setActive(value);
    }
    void VectorLayerQueryBuilderGUI::addValueToWhere(int index)
    {
        QObject* VectorQueryBuilder = _findChild(VECTORQUERYBUILDEROBJECTNAME);
        if(VectorQueryBuilder)
        {
            if(index != -1)
            {
                QString fieldValue = _currentFieldValues.at(index);
                std::string whereClause = VectorQueryBuilder->property("sqlWhereText").toString().toStdString();
                whereClause.append("\'");
                whereClause.append(fieldValue.toStdString());
                whereClause.append("\' ");
                VectorQueryBuilder->setProperty("sqlWhereText", whereClause.c_str());
            }
        }
    }

    void VectorLayerQueryBuilderGUI::changeText(const QString& leToSearch)
    {
        std::string toSearch = leToSearch.toStdString();
        _toSearch = toSearch;
    }


    void VectorLayerQueryBuilderGUI::showAllValues(int index)
    {

        QObject* VectorQueryBuilder = _findChild(VECTORQUERYBUILDEROBJECTNAME);
        if(!VectorQueryBuilder)
            return;
        OGRLayer* resultLayer;

 
        if( _allFieldDefinitions.empty())
            return;

        if(!_vectorLayerQueryUIHandler.valid())
            return;

        std::string columnName = _allFieldDefinitions.at(index).toStdString();
        std::string queryStatement;


        /******
        
        ****/
        _vectorLayerQueryUIHandler->setShapeFilePathName(_currentVectorShapeName);
        //! Set table definition fields
        std::string layerName = _vectorLayerQueryUIHandler->getOGRLayerName();
        queryStatement = "SELECT DISTINCT \"" + columnName +"\" from \"" + layerName + "\"";
        _vectorLayerQueryUIHandler->setSQLQuery(queryStatement);

        resultLayer = _vectorLayerQueryUIHandler->executeQueryStatement();


        if (!resultLayer)
        {
            LOG_ERROR("Query unsuccesfull!! " + _allFieldDefinitions.at(index).toStdString());
            return;
        }
        int featureCount = resultLayer->GetFeatureCount();
        int i = 0;
        int role = 0;
        _currentFieldValues.clear();
        while (i < featureCount)
        {

            OGRFeature* feature = resultLayer->GetNextFeature();
            OGRFieldDefn* defn = feature->GetFieldDefnRef(role);
            OGRFieldType type = defn->GetType();
            switch (type)
            {
            case OFTInteger:
                _currentFieldValues.append(QString::number(feature->GetFieldAsInteger(role)));
                break;
            case OFTReal:
                _currentFieldValues.append(QString::number(feature->GetFieldAsDouble(role)));
                break;
            case OFTString:
                _currentFieldValues.append(QString::fromStdString(feature->GetFieldAsString(role)));
                break;
            default:
                break;
            }
            OGRFeature::DestroyFeature(feature);
            i++;
        }
 
        _setContextProperty("distinctValuesList", QVariant::fromValue(_currentFieldValues));
        _vectorLayerQueryUIHandler->reset();

    }

    void VectorLayerQueryBuilderGUI::showSampleValues(int index)
    {
        showAllValues(index);    
    }

    void VectorLayerQueryBuilderGUI::_queryBuilderClosed()
    {
        _allFieldDefinitions.clear();
        _currentFieldValues.clear();
        _setContextProperty("allFieldsList", QVariant::fromValue(NULL));
        _setContextProperty("distinctValuesList", QVariant::fromValue(NULL));
    }

    void VectorLayerQueryBuilderGUI::_populateFieldDefinition()
    {
        QObject* VectorQueryBuilder = _findChild(VECTORQUERYBUILDEROBJECTNAME);

        if (VectorQueryBuilder == NULL || _currentVectorShapeName.empty())
            return;

        std::string queryStatement;
        if(!_vectorLayerQueryUIHandler.valid())
        {
            LOG_ERROR("vectorLayerQueryUIHandler is not valid.");
            return;
        }

        if( !_allFieldDefinitions.empty())
            return;
        _shapeFileAttributeList = _vectorLayerQueryUIHandler->getFeatureAtribute(_currentVectorShapeName);
        for (std::vector<std::string>::const_iterator iter = _shapeFileAttributeList.begin(); iter != _shapeFileAttributeList.end(); ++iter)
        {
            std::string field = *iter;
            _allFieldDefinitions.append(QString::fromStdString(field));
        }
        _setContextProperty("allFieldsList", QVariant::fromValue(_allFieldDefinitions));
    }

    void VectorLayerQueryBuilderGUI::_handleTableOkButtonClicked()
    {
        ///_selectedFieldDefinitions.clear();
        //
    }

    void VectorLayerQueryBuilderGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                // set the remaining handlers for further use
                _vectorLayerQueryUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IVectorLayerQueryUIHandler>(getGUIManager());

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }

    }



    void VectorLayerQueryBuilderGUI::_handleOperatorButtonClicked(QString buttonName)
    {
        QObject* queryBuilderObject = _findChild(VECTORQUERYBUILDEROBJECTNAME);
        if(queryBuilderObject)
        {
            std::string whereClause = queryBuilderObject->property("sqlWhereText").toString().toStdString();
            whereClause.append(buttonName.toStdString());
            queryBuilderObject->setProperty("sqlWhereText", whereClause.c_str());
        }
    }


    void VectorLayerQueryBuilderGUI::_handleAddToWhereButtonClicked(int index)
    {
        QObject* queryBuilderObject = _findChild(VECTORQUERYBUILDEROBJECTNAME);
        if(queryBuilderObject)
        {
            if(index != -1)
            {
                //std::string uiType =  queryBuilderObject->property("uiType").toString().toStdString();
                std::string whereClause = queryBuilderObject->property("sqlWhereText").toString().toStdString();
                std::string columnName = _allFieldDefinitions.at(index).toStdString();
                whereClause.append("\"" + columnName + "\"");
                whereClause.append(" ");
                queryBuilderObject->setProperty("sqlWhereText", whereClause.c_str());
            }
        }
    }

    void VectorLayerQueryBuilderGUI::_handleOkButtonClicked()
    {
        QString whereClause;

        QString queryName;
        QObject* VectorQueryBuilder = _findChild(VECTORQUERYBUILDEROBJECTNAME);
        if(VectorQueryBuilder)
        {
            whereClause = VectorQueryBuilder->property("sqlWhereText").toString();

            queryName = VectorQueryBuilder->property("queryName").toString();
            if (queryName == "")
            {
                emit showError("Name", "Query  name not specified", true);
                return;
            }
            if (whereClause == "")
            {
                emit showError("where Clause Error", "where Clause not specified", true);
                return;
            }

            QObject* vectorLayerPopupObject= _findChild("VectorLayerQueryPopup");
            if (vectorLayerPopupObject)
            {
                vectorLayerPopupObject->setProperty("whereClause", QVariant::fromValue(whereClause));
                vectorLayerPopupObject->setProperty("createdQueryName", QVariant::fromValue(queryName));
                QMetaObject::invokeMethod(vectorLayerPopupObject, "queryBuilderOkClicked");
            }
        }



        _setContextProperty("allFieldsList", QVariant::fromValue(NULL));
        _setContextProperty("distinctValuesList", QVariant::fromValue(NULL));
        QMetaObject::invokeMethod(VectorQueryBuilder, "closePopup");

    }

    void VectorLayerQueryBuilderGUI::_handleClearButtonClicked()
    {
        QObject* queryBuilderObject = _findChild(VECTORQUERYBUILDEROBJECTNAME);
        if(queryBuilderObject)
        {
            //_selectedFieldDefinitions.clear();
            //_setContextProperty("selectedFieldsList", QVariant::fromValue(_selectedFieldDefinitions));
            queryBuilderObject->setProperty("sqlWhereText", "");
            //queryBuilderObject->setProperty("isMarkSelected", false);
            //queryBuilderObject->setProperty("bottomLeftLongitude", "");
            //queryBuilderObject->setProperty("bottomLeftLatitude", "");
            //queryBuilderObject->setProperty("topRightLongitude", "");
            //queryBuilderObject->setProperty("topRightLatitude", "");

        }
    }
}