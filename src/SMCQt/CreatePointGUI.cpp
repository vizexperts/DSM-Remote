/****************************************************************************
*
* File             : CreatePointGUI.h
* Description      : CreatePointGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/CreatePointGUI.h>
#include <SMCQt/PropertyObject.h>

#include <App/AccessElementUtils.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransactionFactory.h>
#include <App/IUndoTransaction.h>
#include <App/IUIHandler.h>

#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IClamper.h>
#include <Elements/ElementsUtils.h>

#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/WorldMaintainer.h>
#include <Core/InterfaceUtils.h>
#include <Core/IDeletable.h>
#include <Core/IText.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITemporary.h>
#include <Core/IFeatureObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IPoint.h>
#include <Core/IText.h>
#include <Core/CompositeObject.h>
//#include <Core/ITerrain.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <SMCElements/ILayerComponent.h>

#include <QFileDialog>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <iostream>
#include <GIS/IPoint.h>
#include <GIS/ITable.h>
#include <GIS/IFieldDefinition.h>
#include <GIS/IStringFieldDefinition.h>
#include <GIS/IDoubleFieldDefinition.h>
#include <GIS/FieldDefinitionType.h>
#include <SMCQt/VizComboBoxElement.h>
#include <boost/algorithm/string.hpp>
#include <Util/FileUtils.h>
#include <Util/GdalUtils.h>
#include <Util/StyleFileUtil.h>
#include <Elements/ElementsUtils.h>
#include <Elements/VizPlaceNode.h>
#include<Elements/FeatureObject.h> 
#include <serialization/StyleSheetParser.h>


namespace SMCQt
{

    DEFINE_META_BASE(SMCQt, CreatePointGUI);
    DEFINE_IREFERENCED(CreatePointGUI, SMCQt::DeclarativeFileGUI);

    const std::string CreatePointGUI::PointContextualMenuObjectName = "pointContextual";
    const std::string CreatePointGUI::MouseButtonClickedPropertyName = "mouseButtonClicked";
    const std::string CreatePointGUI::ContextualMenuObjectName = "contextualMenu";
    const std::string CreatePointGUI::HyperlinkContextualMenuObjectName = "hyperLinkContextual";
    const std::string CreatePointGUI::FeatureContextualObjectName = "featureContextual"; 

    CreatePointGUI::CreatePointGUI()
        :_cleanup(true),
        _selectedFeatureLayer(NULL),
        _addToDefault(true),
        _fontDir("")
    {

    }

    CreatePointGUI::~CreatePointGUI()
    {

    }

    void CreatePointGUI::_loadAndSubscribeSlots()
    {

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if (smpMenu)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)),
                this, SLOT(connectSMPMenu(QString)), Qt::UniqueConnection);
        }

        //This is done to play the Audio or Video
        QObject::connect(this, SIGNAL(play(QString)), this, SLOT(playAudioOrVideo(QString)), Qt::UniqueConnection);

        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }

    void CreatePointGUI::connectContextualMenu()
    {
        QObject* pointContextual = _findChild(PointContextualMenuObjectName);
        if (pointContextual)
        {
            if (!_pointEnabled){
                pointContextual->setProperty("labelEnabled", true);
            }
            QVariant mouseButtonClicked = pointContextual->property(MouseButtonClickedPropertyName.c_str());
            _mouseClickRight = (mouseButtonClicked.toInt() == Qt::RightButton);

            // setActive
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_ATTRIBUTES);
            CORE::RefPtr<CORE::IComponent> component = ELEMENTS::GetComponentFromMaintainer("MapSheetInfoComponent");
            _mapSheetComponent = component->getInterface<ELEMENTS::IMapSheetInfoComponent>();

            if (_mouseClickRight)
            {
                // populate contextual menu
                _populateContextualMenu();

                // assuming there is a Font folder present in user's appdata/terrainDSM folder (should be already present in Appdata installer)

                if (_fontDir.empty())
                {
                    UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
                    std::string appdataPath = temporaryInstance->getAppFolderPath();
                    _fontDir = appdataPath + "/Fonts";
                }

                // populates all the font name in _fontList
                _populateFontList();

                QObject::connect(pointContextual, SIGNAL(rename(QString)), this,
                    SLOT(rename(QString)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(setTextActive(bool)), this,
                    SLOT(setTextActive(bool)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(handleLatLongAltChanged(QString)), this,
                    SLOT(handleLatLongAltChanged(QString)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(deletePoint()), this,
                    SLOT(deletePoint()), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(clampingChanged(bool)), this,
                    SLOT(clampingChanged(bool)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(attachFile(QString)), this,
                    SLOT(attachFile(QString)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(playAudio()), this,
                    SLOT(playAudio()), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(playVideo()), this,
                    SLOT(playVideo()), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(showImage(QString)), this,
                    SLOT(showImage(QString)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(changeColor(QColor)), this,
                    SLOT(colorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(browseButtonClicked()), this,
                    SLOT(handleBrowseButtonClicked()), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(outlineModeChanged(bool)), this,
                    SLOT(outlineChanged(bool)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(changeOutlineColor(QColor)), this,
                    SLOT(outlineColorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(billboardModeChanged(bool)), this,
                    SLOT(billboardChanged(bool)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(changeBillboardColor(QColor)), this,
                    SLOT(billboardColorChanged(QColor)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(changeTextSize(double)), this,
                    SLOT(changeTextSize(double)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(editEnabled(bool)), this,
                    SLOT(editEnabled(bool)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(fontChanged(QString)), this,
                    SLOT(fontChanged(QString)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(alignmentChanged(int)), this,
                    SLOT(alignmentChanged(int)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(layoutChanged(int)), this,
                    SLOT(layoutChanged(int)), Qt::UniqueConnection);

                QObject::connect(pointContextual, SIGNAL(saveTextPoint(QString)), this,
                    SLOT(saveTextPoint(QString)), Qt::UniqueConnection);

            }
        }
        _subscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointUpdatedMessageType);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }


        showPointAttributes();
        QObject* featureContextual = _findChild(FeatureContextualObjectName); 
        if (featureContextual)
        {
            QObject::connect(featureContextual, SIGNAL(playAudioOrVideo(QString)), this, SLOT(playAudioOrVideo(QString)), Qt::UniqueConnection);

            QObject::connect(featureContextual, SIGNAL(openUrl(QString)), this, SLOT(openUrl(QString)), Qt::UniqueConnection);

            QObject::connect(featureContextual, SIGNAL(showImage(QString)), this, SLOT(showImage(QString)), Qt::UniqueConnection);
        }

        QObject* hyperlinkContextual = _findChild(HyperlinkContextualMenuObjectName);

        if (hyperlinkContextual)
        {

            QObject::connect(hyperlinkContextual, SIGNAL(playAudioOrVideo(QString)), this, SLOT(playAudioOrVideo(QString)), Qt::UniqueConnection);

            QObject::connect(hyperlinkContextual, SIGNAL(openUrl(QString)), this, SLOT(openUrl(QString)), Qt::UniqueConnection);

            QObject::connect(hyperlinkContextual, SIGNAL(showImage(QString)), this, SLOT(showImage(QString)), Qt::UniqueConnection);
            //populate Hyperlink Menu 

            //populate Hyperlink Menu 


            //populate doclist
            std::vector<std::string> docsVector;
            _pointUIHandler->getAssociatedFilePaths(docsVector, "VIZ_DOCS");

            _docList.clear();
            for (unsigned int i = 0; i < docsVector.size(); i++)
            {
                if (osgDB::fileExists(docsVector[i]))
                {
                    _docList.append(QString::fromStdString(docsVector[i]));
                }

            }

            _setContextProperty("docList", QVariant::fromValue(_docList));

            //populate urlList
            std::vector<std::string> urlVector;
            _pointUIHandler->getAssociatedFilePaths(urlVector, "VIZ_URLS");

            _urlList.clear();
            for (unsigned int i = 0; i < urlVector.size(); i++)
            {
                _urlList.append(QString::fromStdString(urlVector[i]));
            }

            _setContextProperty("urlList", QVariant::fromValue(_urlList));



            std::vector<std::string> imageVector;
            _pointUIHandler->getAssociatedImages(imageVector);

            std::string projectLocation = _getProjectLocation();

            _imageList.clear();
            for (unsigned int i = 0; i < imageVector.size(); i++)
            {

                if (osgDB::fileExists(imageVector[i]))
                {
                    _imageList.append(QString::fromStdString("file:///" + imageVector[i]));
                }
                else if (osgDB::fileExists(projectLocation + "/" + imageVector[i]))
                {
                    _imageList.prepend(QString::fromStdString("file:///" + projectLocation + "/" + imageVector[i]));
                }
            }
            _setContextProperty("imageList", QVariant::fromValue(_imageList));


        }


    }

    void CreatePointGUI::disconnectContextualMenu()
    {
        if (!_pointUIHandler.valid())
            return;

        _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
        _unsubscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointUpdatedMessageType);

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(true);
            selectionUIHandler->clearCurrentSelection();
        }
    }

    void CreatePointGUI::attachFile(QString type)
    {
        if (!_pointUIHandler.valid())
            return;

        std::string filters;

        if (!type.compare("Image"))
        {
            filters = "Images (*.jpeg *.jpg *.png *.bmp)";
        }
        else if (!type.compare("Audio"))
        {
            filters = "Audio (*.mp3 *wav)";
        }
        else if (!type.compare("Video"))
        {
            filters = "Video (*.mpeg *.mpg *.avi *.rmvb *.wmv *.dat *.mp4)";
        }

        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();

        QString filepath = QFileDialog::getOpenFileName(parent, type.toStdString().c_str(),
            "c:/", filters.c_str());

        std::string fileChosen = filepath.toStdString();;

        if (osgDB::fileExists(fileChosen))
        {
            _loadBusyBar(boost::bind(&CreatePointGUI::_copyAttachedFiles, this, fileChosen), "Loading files to project");

            QObject* pointContextual = _findChild(PointContextualMenuObjectName);
            if (pointContextual)
            {
                if (!type.compare("Image"))
                {
                    _pointUIHandler->addAssociatedImage(fileChosen);

                    _imageList.prepend(QString::fromStdString("file:///" + fileChosen));

                    _setContextProperty("imageList", QVariant::fromValue(_imageList));
                }

                else if (!type.compare("Video"))
                {
                    _pointUIHandler->setAssociatedVideoOrAudio(fileChosen, "VIZ_VIDEO");

                    pointContextual->setProperty("videoAttached", QVariant::fromValue(true));

                    QString fileName = QString::fromStdString(osgDB::getStrippedName(fileChosen));

                    pointContextual->setProperty("videoFileName", QVariant::fromValue(fileName));
                }
                else if (!type.compare("Audio"))
                {
                    _pointUIHandler->setAssociatedVideoOrAudio(fileChosen, "VIZ_AUDIO");

                    pointContextual->setProperty("audioAttached", QVariant::fromValue(true));

                    QString fileName = QString::fromStdString(osgDB::getStrippedName(fileChosen));

                    pointContextual->setProperty("audioFileName", QVariant::fromValue(fileName));
                }
            }
        }
    }

    void CreatePointGUI::_copyAttachedFiles(std::string fileChosen)
    {
        QObject* pointContextual = _findChild(PointContextualMenuObjectName);
        if (pointContextual)
        {
            std::string projectLocation = _getProjectLocation();
            projectLocation += "/" + osgDB::getSimpleFileName(fileChosen);
            projectLocation = osgDB::convertFileNameToNativeStyle(projectLocation);
            osgDB::copyFile(fileChosen, projectLocation);
        }
    }


    void CreatePointGUI::editEnabled(bool value)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (value)
            {
                _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_POINT);
            }
            else
            {
                _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_EDIT_ATTRIBUTES);
            }
        }
    }

    void CreatePointGUI::colorChanged(QColor color)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
                return;

            if (color.isValid())
            {
                osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

                CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
                if (point.valid())
                {
                    CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                    if (text.valid())
                    {
                        osg::Vec4  currentColor = text->getTextColor();
                        if (penColor != currentColor)
                        {
                            text->setTextColor(penColor);
                        }
                    }
                }
            }
        }
    }

    void CreatePointGUI::_populateFontList()
    {
        _fontsList.clear();

        std::string path = osgDB::convertFileNameToNativeStyle(_fontDir);
        if (!osgDB::fileExists(path))
        {
            LOG_ERROR("Font folder not found");
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            // checking for font file
            if (boost::iequals(osgDB::getFileExtension(currFileName), "ttf"))
            {
                _fontsList.push_back(osgDB::getNameLessExtension(currFileName));
            }
        }

        int count = 1;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select font", "0"));

        for (int i = 0; i < _fontsList.size(); i++)
        {
            std::string georbISServerName = _fontsList[i];
            comboBoxList.append(new VizComboBoxElement(georbISServerName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("fontsList", QVariant::fromValue(comboBoxList));

    }

    void CreatePointGUI::handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption = "Font Folder";

        // populating the font list again as per new font directory
        QString dirPath = QFileDialog::getExistingDirectory(parent, caption, directory);
        _fontDir = dirPath.toStdString();
        _populateFontList();
    }

    void CreatePointGUI::fontChanged(QString font)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
                return;

            if (font.isEmpty() || font == "Select font")
                return;

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                if (text.valid())
                {
                    font += ".ttf";
                    text->setFont(_fontDir + "\\" + font.toStdString());
                }
            }
        }
    }

    void CreatePointGUI::layoutChanged(int layoutId)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setLayout(static_cast<CORE::IText::Layout>(layoutId));
                }
            }
        }
    }

    void CreatePointGUI::alignmentChanged(int alignmentId)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setAlignment(static_cast<CORE::IText::Alignment>(alignmentId));
                }
            }
        }
    }

    void CreatePointGUI::outlineChanged(bool value)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setOutline(value);
                }
            }
        }
    }

    void CreatePointGUI::outlineColorChanged(QColor color)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
                return;

            if (color.isValid())
            {
                osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

                CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
                if (point.valid())
                {
                    CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                    if (text.valid())
                    {
                        osg::Vec4  currentColor = text->getOutlineColor();
                        if (penColor != currentColor)
                        {
                            text->setOutlineColor(penColor);
                        }
                    }
                }
            }
        }
    }

    void CreatePointGUI::billboardChanged(bool value)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setBillboard(value);
                }
            }
        }
    }


    void CreatePointGUI::billboardColorChanged(QColor color)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
                return;

            if (color.isValid())
            {
                osg::Vec4 penColor((float)color.red() / 255.0f, (float)color.green() / 255.0f,
                    (float)color.blue() / 255.0f, (float)color.alpha() / 255.0f);

                CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
                if (point.valid())
                {
                    CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                    if (text.valid())
                    {
                        osg::Vec4  currentColor = text->getTextBillboardColor();
                        if (penColor != currentColor)
                        {
                            text->setTextBillboardColor(penColor);
                        }
                    }
                }
            }
        }
    }


    void CreatePointGUI::changeTextSize(double textSize)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
                return;

            if (textSize <= 0)
                return;

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
                if (text.valid())
                {
                    text->setTextSize(textSize);
                }
            }
        }
    }

    std::string CreatePointGUI::_getProjectLocation()
    {
        std::string projectLocation;
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer =
            CORE::WorldMaintainer::instance();

        if (worldMaintainer.valid())
        {
            projectLocation = osgDB::getFilePath(worldMaintainer->getProjectFile());
        }

        return projectLocation;
    }

    void
        CreatePointGUI::playAudio()
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            std::string audioFile = _pointUIHandler->getAssociatedVideoOrAudio("VIZ_AUDIO");
            if (audioFile.empty())
            {
                emit showError("Audio File", "No Audio File Available");
                return;
            }

            std::string projectLocation = _getProjectLocation();
            projectLocation += "/" + osgDB::getSimpleFileName(audioFile);
            std::string fileName = osgDB::convertFileNameToNativeStyle(projectLocation);

            emit play(fileName.c_str());
        }
    }

    void
        CreatePointGUI::playVideo()
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            std::string videoFile = _pointUIHandler->getAssociatedVideoOrAudio("VIZ_VIDEO");
            if (videoFile.empty())
            {
                emit showError("Video File", "No Video File Available");
                return;
            }

            std::string projectLocation = _getProjectLocation();

            projectLocation += "/" + osgDB::getSimpleFileName(videoFile);
            std::string fileName = osgDB::convertFileNameToNativeStyle(projectLocation);

            emit play(fileName.c_str());
        }
    }

    void
        CreatePointGUI::showImage(QString fileName)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            QUrl urlString(fileName);
            QString file = urlString.fromPercentEncoding(fileName.toUtf8());
            std::string imageFile = file.toStdString();
            //imageFile = imageFile.substr(8);

            if (imageFile.empty())
            {
                emit showError("Image File", "No Image File Available");
                return;
            }
            emit play(imageFile.c_str());

        }
    }

    void CreatePointGUI::playAudioOrVideo(QString fileName)
    {        
        if (fileName.contains("file:/")){
            fileName.remove("file:/");
            fileName.remove("//");
        }
        std::string filePath = fileName.toStdString();
        //int erasingLen = 8; //for removing "file:\\\" in start we need for 8 characters
        //filePath.erase(0, erasingLen);
        //filePath = "hstart.exe /NOCONSOLE \"wscript.exe invisible.vbs \"" + filePath + "\"";
        ////filePath = "\"\" " + filePath; //need to add Double quote for 
        ////filePath = "start " + filePath;
        //system(filePath.c_str());
        std::stringstream str; 
        str << "cmd.exe /C " << '"' << filePath << '"';
        UTIL::GDALUtils::execSysCmd(str);

    }

    void CreatePointGUI::openUrl(QString fileName)
    {
        std::stringstream ss;
        //ss << "hstart.exe /NOCONSOLE \"wscript.exe invisible.vbs \"" << fileName.toStdString() << "\"\"";
        std::string command = ss.str();
        //system(command.c_str());
        std::stringstream str;
        str <<"cmd.exe /C "<< '"' << fileName.toStdString() << '"';
        UTIL::GDALUtils::execSysCmd(str);
    }

    void CreatePointGUI::showMultipleImages()
    {
        std::vector<std::string> imageVector;
        _pointUIHandler->getAssociatedImages(imageVector);

        std::string projectLocation = _getProjectLocation();

        for (unsigned int i = 0; i < imageVector.size(); i++)
        {

            if (osgDB::fileExists(imageVector[i]))
            {
                emit play(imageVector[i].c_str());
            }
            else if (osgDB::fileExists(projectLocation + "/" + imageVector[i]))
            {
                std::string filePath(projectLocation + "/" + imageVector[i]);
                emit play(filePath.c_str());
            }
        }
    }
    void CreatePointGUI::showPointAttributes()
    {
        if (!_mouseClickRight)
        {
            QObject* pointContextual = _findChild(PointContextualMenuObjectName);
            if (pointContextual != NULL)
            {

                const CORE::ISelectionComponent::SelectionMap& selectionMap = _selectionUIHandler->getCurrentSelection();

                if (selectionMap.empty())
                {
                    return;
                }

                CORE::ISelectable* selectable = selectionMap.begin()->second.get();
                if (selectable)
                {
                    _selectedFeature = selectable->getInterface<GIS::IFeature>();
                }
            }
        }

        if (_selectedFeature.valid())
        {
            GIS::ITable* table = _selectedFeature->getTable();
            const GIS::ITable::IFieldDefinitionList& definitionList = table->getFieldDefinitionList();

            int tableColumnCount = definitionList.size();

            _attributeList.clear();

            bool isDescriptionPresent = false;
            QString description;


            //if(!_mouseClickRight)
            //{


            //    QString name = QString::fromStdString(_selectedFeature->getInterface<CORE::IBase>()->getName());
            //    GIS::IGeometry* geometry = _selectedFeature->getGeometry();
            //    GIS::IPoint* point = geometry->getInterface<GIS::IPoint>();
            //    osg::Vec4d position = point->getValue();                    
            //    double altitudeD = position.z() - ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y());
            //    if(altitudeD < 0)
            //    {
            //        altitudeD = 0.0;
            //    }
            //    QString latitude = QString::number(position.y(), 'f', 6);;
            //    QString longitude = QString::number(position.x(), 'f', 6);
            //    QString altitude = QString::number(altitudeD, 'f', 6);
            //    _attributeList.append(new PropertyObject("Name ", "String", name, name.length()));
            //    _attributeList.append(new PropertyObject("Latitude", "Double", latitude, 1));
            //    _attributeList.append(new PropertyObject("Longitude", "Double", longitude, 1));
            //    _attributeList.append(new PropertyObject("Altitude", "Double", altitude, 1));


            //}

            for (int i = 0; i < tableColumnCount; i++)
            {
                const GIS::Field* field = _selectedFeature->getField(i);
                if (field)
                {
                    CORE::RefPtr<GIS::IFieldDefinition> fieldDefn = definitionList[i].get();
                    if (fieldDefn.valid())
                    {
                        const GIS::FieldDefinitionType& type = fieldDefn->getFieldDefinitionType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName = fieldName.trimmed();

                        if ((fieldName == "Description"))
                        {
                            description = QString::fromStdString(field->getAsString());
                            if (description.contains("<html>"))
                            {
                                isDescriptionPresent = true;
                                continue;
                            }
                        }
                        else if (fieldName.contains("VIZ_", Qt::CaseInsensitive))
                        {
                            continue;
                        }
                        else if (fieldName.compare("name", Qt::CaseInsensitive) == 0)
                        {
                            continue;
                        }
                        else if (fieldName.isEmpty())
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecision = 1;

                        if (type == GIS::StringFieldDefinitionType)
                        {
                            fieldType = "String";
                            fieldValue = QString::fromStdString(field->getAsString());
                            fieldValue = fieldValue.simplified();
                            fieldPrecision = fieldDefn->getInterface<GIS::IStringFieldDefinition>()->getLength();
                        }
                        else if (type == GIS::IntegerFieldDefinitionType)
                        {
                            fieldType = "Integer";
                            fieldValue = QString::number(field->getAsInt32());
                        }
                        else if (type == GIS::DoubleFieldDefinitionType)
                        {
                            fieldType = "Double";
                            fieldValue = QString::number(field->getAsReal64());
                            fieldPrecision = fieldDefn->getInterface<GIS::IDoubleFieldDefinition>()->getPrecision();
                        }

                        if (!fieldValue.isEmpty())
                        {
                            _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecision));
                        }
                    }
                }
            }

            if (_attributeList.empty() && !_mouseClickRight)
            {
                QObject* pointContextual = _findChild(PointContextualMenuObjectName);
                if (pointContextual)
                {
                    pointContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("pointAttributes");
            if (showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                if (isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this,
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);

            }
        }
        else
        {

            CORE::RefPtr<CORE::IMetadataRecord> tableRecord =
                _pointUIHandler->getMetadataRecord();

            if (!tableRecord.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
            if (!tableDefn.valid())
                return;

            int tableColumnCount = tableDefn->getFieldCount();

            _attributeList.clear();




            if (!_mouseClickRight)
            {

                CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
                if (point.valid())
                {
                    QString name = QString::fromStdString(point->getInterface<CORE::IBase>()->getName());
                    osg::Vec3d position = point->getValue();
                    double altitudeD = position.z() - ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y());
                    if (altitudeD < 0)
                    {
                        altitudeD = 0.0;
                    }
                    QString latitude = QString::number(position.y(), 'f', 6);;
                    QString longitude = QString::number(position.x(), 'f', 6);
                    QString altitude = QString::number(altitudeD, 'f', 6);
                    _attributeList.append(new PropertyObject("Name ", "String", name, name.length()));
                    _attributeList.append(new PropertyObject("Latitude", "Double", latitude, 1));
                    _attributeList.append(new PropertyObject("Longitude", "Double", longitude, 1));
                    _attributeList.append(new PropertyObject("Altitude", "Double", altitude, 1));
                    if (_mapSheetComponent.valid())
                    {
                        QString indianGR = QString::fromStdString(_mapSheetComponent->getMapSheetNumber(osg::Vec2d(position.y(), position.x())));
                        _attributeList.append(new PropertyObject("Indian GR", "String", indianGR, indianGR.length()));
                    }
                    _attributeList.append(new PropertyObject("HyperLink Text", "String", _pointUIHandler->getAssociatedText().c_str(), 1));
                    std::string uniqueId = point->getInterface<CORE::IBase>()->getUniqueID().toString() + ".json";
                    std::string objectFolderLocation = UTIL::StyleFileUtil::getObjectFolder();
                    jsonFile = objectFolderLocation + "/" + uniqueId;
                    std::string jsonContent;
                    if (osgDB::fileExists(jsonFile))
                    {
                        std::ifstream inFile(jsonFile.c_str());
                        if (!inFile.is_open())
                            return;
                        jsonContent.assign((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());
                    }
                    QObject *pointContextual = _findChild("pointContextual");
                    if (pointContextual)
                    {
                        pointContextual->setProperty("jsonString", QString::fromStdString(jsonContent));
                        std::string jsonSchemaFile = "../../data/Styles/StyleSchema.json";
                        QFileInfo schemafileInfo(jsonSchemaFile.c_str());
                        QString strFilePath = schemafileInfo.absoluteFilePath();
                        std::string schemaPath = strFilePath.toStdString();
                        std::ifstream schemaFile(schemaPath.c_str());
                        if (!schemaFile.is_open())
                            return;

                        std::string schemaData((std::istreambuf_iterator<char>(schemaFile)), std::istreambuf_iterator<char>());
                        pointContextual->setProperty("jsonSchemaContent", QString::fromStdString(schemaData));
                    }
                }
            }

            bool isDescriptionPresent = false;
            QString description;
            for (int i = 0; i < tableColumnCount; i++)
            {
                CORE::RefPtr<CORE::IMetadataField> field = tableRecord->getFieldByIndex(i);
                if (field)
                {
                    CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn = field->getMetadataFieldDef();
                    if (fieldDefn.valid())
                    {
                        CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                        QString fieldName = QString::fromStdString(fieldDefn->getName());
                        fieldName = fieldName.trimmed();

                        if ((fieldName == "Description"))
                        {
                            description = QString::fromStdString(field->toString());
                            if (description.contains("<html>"))
                            {
                                isDescriptionPresent = true;
                                continue;
                            }
                        }
                        else if (fieldName.contains("VIZ_", Qt::CaseInsensitive))
                        {
                            continue;
                        }
                        else if (fieldName.compare("name", Qt::CaseInsensitive) == 0)
                        {
                            continue;
                        }
                        else if (fieldName.isEmpty())
                        {
                            continue;
                        }

                        QString fieldType;
                        QString fieldValue;
                        int fieldPrecesion = 1;

                        switch (type)
                        {
                        case CORE::IMetadataFieldDefn::STRING:
                        {
                            fieldType = "String";
                            fieldValue = QString::fromStdString(field->toString());
                            fieldPrecesion = fieldDefn->getLength();
                        }
                        break;
                        case CORE::IMetadataFieldDefn::INTEGER:
                        {
                            fieldType = "Integer";
                            fieldValue = QString::number(field->toInt());
                        }
                        break;
                        case CORE::IMetadataFieldDefn::DOUBLE:
                        {
                            fieldType = "Double";
                            fieldValue = QString::number(field->toDouble());
                            fieldPrecesion = fieldDefn->getPrecision();
                        }
                        break;
                        }

                        _attributeList.append(new PropertyObject(fieldName, fieldType, fieldValue, fieldPrecesion));
                    }
                }
            }

            if (_attributeList.empty())
            {
                QObject* pointContextual = _findChild(PointContextualMenuObjectName);
                if (pointContextual)
                {
                    pointContextual->setProperty("attributesTabVisible", QVariant::fromValue(false));
                }

                return;
            }

            QObject* showAttributes = _findChild("pointAttributes");
            if (showAttributes != NULL)
            {
                _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                if (isDescriptionPresent)
                {
                    showAttributes->setProperty("description", QVariant::fromValue(description));
                }

                QObject::connect(showAttributes, SIGNAL(okButtonPressed()), this,
                    SLOT(handleAttributesOkButtonPressed()), Qt::UniqueConnection);

                QObject::connect(showAttributes, SIGNAL(cancelButtonPressed()), this,
                    SLOT(handleAttributesCancelButtonPressed()), Qt::UniqueConnection);

            }
        }
    }

    void CreatePointGUI::handleAttributesCancelButtonPressed()
    {
        showPointAttributes();
    }

    void CreatePointGUI::handleAttributesOkButtonPressed()
    {
        CORE::RefPtr<CORE::IMetadataRecord> tableRecord =
            _pointUIHandler->getMetadataRecord();

        if (!tableRecord.valid())
        {
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = tableRecord->getTableDefn();
        if (!tableDefn.valid())
            return;

        foreach(QObject* qobj, _attributeList)
        {
            PropertyObject* propObj = qobject_cast<PropertyObject*>(qobj);
            if (propObj)
            {
                CORE::RefPtr<CORE::IMetadataField> field =
                    tableRecord->getFieldByName(propObj->name().toStdString());
                if (field.valid())
                {
                    CORE::RefPtr<CORE::IMetadataFieldDefn> fieldDefn =
                        field->getMetadataFieldDef();

                    CORE::IMetadataFieldDefn::FieldType type = fieldDefn->getType();
                    switch (type)
                    {
                    case CORE::IMetadataFieldDefn::INTEGER:
                    {
                        field->fromInt(propObj->value().toInt());
                    }
                    break;
                    case CORE::IMetadataFieldDefn::DOUBLE:
                    {
                        field->fromDouble(propObj->value().toDouble());
                    }
                    break;
                    case CORE::IMetadataFieldDefn::STRING:
                    {
                        field->fromString(propObj->value().toStdString());
                    }
                    break;
                    }
                }
            }
        }

        CORE::IPoint *point = _pointUIHandler->getPoint();
        if (point)
        {
            CORE::IFeatureObject *featureObject = point->getInterface<CORE::IFeatureObject>();
            if (featureObject)
            {
                featureObject->update();
            }
        }
    }

    void CreatePointGUI::_populateContextualMenu()
    {
        QObject* pointContextual = _findChild(PointContextualMenuObjectName);
        if (pointContextual != NULL)
        {

            const CORE::ISelectionComponent::SelectionMap& selectionMap = _selectionUIHandler->getCurrentSelection();

            if (selectionMap.empty())
            {
                return;
            }

            CORE::ISelectable* selectable = selectionMap.begin()->second.get();
            if (selectable)
            {
                _selectedFeature = selectable->getInterface<GIS::IFeature>();
            }

            if (_selectedFeature.valid())
            {
                //! Name of the point
                QString name = QString::fromStdString(_selectedFeature->getInterface<CORE::IBase>()->getName());

                //! text active state
                bool textActive = false;

                //! text Size
                double currentTextSize = 12.0;


                //! text color
                osg::Vec4 textColor = osg::Vec4(1, 1, 1, 1);
                QColor qtextColor(int(textColor.r() * 255), int(textColor.g() * 255), int(textColor.b() * 255), int(textColor.a() * 255));

                bool clampingEnabled = true;

                GIS::IGeometry* geometry = _selectedFeature->getGeometry();
                GIS::IPoint* point = geometry->getInterface<GIS::IPoint>();
                osg::Vec4d position = point->getValue();
                double altitudeD = position.z() - ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y());
                if (altitudeD < 0)
                {
                    altitudeD = 0.0;
                }
                QString latitude = QString::number(position.y(), 'f', 6);;
                QString longitude = QString::number(position.x(), 'f', 6);
                QString altitude = QString::number(altitudeD, 'f', 6);
                //! keeping first 4 decimal places

                // populating attached files
                // checking weather the fields are available 
                pointContextual->setProperty("hyperlinkTabVisible", QVariant::fromValue(false));
                pointContextual->setProperty("name", QVariant::fromValue(name));
                pointContextual->setProperty("textActive", QVariant::fromValue(textActive));
                pointContextual->setProperty("latitude", QVariant::fromValue(latitude));
                pointContextual->setProperty("longitude", QVariant::fromValue(longitude));
                pointContextual->setProperty("altitude", QVariant::fromValue(altitude));
                if (_mapSheetComponent.valid())
                {
                    QString indianGR = QString::fromStdString(_mapSheetComponent->getMapSheetNumber(osg::Vec2d(position.y(), position.x())));
                    pointContextual->setProperty("grValue", QVariant::fromValue(indianGR));

                }

                pointContextual->setProperty("clampingEnabled", QVariant::fromValue(clampingEnabled));
                pointContextual->setProperty("textSize", QVariant::fromValue(currentTextSize));
                pointContextual->setProperty("textColor", QVariant::fromValue(qtextColor));
            }
            else
            {

                //Get the selected point
                CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
                if (!point.valid())
                {
                    return;
                }

                // Populate data for contextual tab

                //! Name of the point
                QString name = QString::fromStdString(point->getInterface<CORE::IBase>()->getName());

                //! text active state
                bool textActive = point->getInterface<CORE::IText>()->getTextActive();

                //! font name
                std::string font = osgDB::getNameLessExtension(osgDB::getSimpleFileName(point->getInterface<CORE::IText>()->getFont()));

                //! layout id
                int layout = point->getInterface<CORE::IText>()->getLayout();

                //! alignmentt id
                int alignment = point->getInterface<CORE::IText>()->getAlignment();

                //! is outline set
                bool isOutline = point->getInterface<CORE::IText>()->isOutline();
                
                //! is billboard set
                bool isBillBoard = point->getInterface<CORE::IText>()->isBillboard();
                
                //! outline color
                osg::Vec4 outlineColor = point->getInterface<CORE::IText>()->getOutlineColor();
                QColor qoutlineColor(int(outlineColor.r() * 255), int(outlineColor.g() * 255), int(outlineColor.b() * 255), int(outlineColor.a() * 255));
                
                //! billboard color
                osg::Vec4 billBoardColor = point->getInterface<CORE::IText>()->getTextBillboardColor();
                QColor qbillboardColor(int(billBoardColor.r() * 255), int(billBoardColor.g() * 255), int(billBoardColor.b() * 255), int(billBoardColor.a() * 255));
                
                //! text Size
                double currentTextSize = 0.0;
                currentTextSize = point->getInterface<CORE::IText>()->getTextSize();

                //! text color
                osg::Vec4 textColor = point->getInterface<CORE::IText>()->getTextColor();
                QColor qtextColor(int(textColor.r() * 255), int(textColor.g() * 255), int(textColor.b() * 255), int(textColor.a() * 255));

                //iconPath
                ELEMENTS::IIconHolder* iconHolder = point->getInterface<ELEMENTS::IIconHolder>();

                if (iconHolder)
                {
                    ELEMENTS::IIcon* icon = iconHolder->getIcon();
                    if (icon)
                    {
                        std::string iconPath = icon->getIconFilename();
                        iconPath = "file:/" + osgDB::getRealPath(iconPath);
                        pointContextual->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(iconPath)));
                    }
                }

                bool clampingEnabled = true;
                CORE::RefPtr<ELEMENTS::IClamper> clamper = point->getInterface<ELEMENTS::IClamper>();
                if (clamper.valid())
                {
                    clampingEnabled = clamper->clampOnPlacement();
                }

                //! Lat-Long-Alt of point
                osg::Vec3d position = point->getValue();
                double altitudeD = position.z() - ELEMENTS::GetAltitudeAtLongLat(position.x(), position.y());
                if (altitudeD < 0)
                {
                    altitudeD = 0.0;
                }
                QString latitude = QString::number(position.y(), 'f', 6);;
                QString longitude = QString::number(position.x(), 'f', 6);
                QString altitude = QString::number(altitudeD, 'f', 6);
                //! keeping first 4 decimal places

                // populating attached files
                // checking weather the fields are available 
                CORE::RefPtr<CORE::IMetadataRecordHolder> recordHolder =
                    point->getInterface<CORE::IMetadataRecordHolder>(true);
                CORE::RefPtr<CORE::IMetadataRecord> record = recordHolder->getMetadataRecord();

                if (record.valid())
                {
                    CORE::RefPtr<CORE::IMetadataField> metaField = record->getFieldByName("VIZ_AUDIO");
                    if (!metaField.valid())
                    {
                       pointContextual->setProperty("hyperlinkTabVisible", QVariant::fromValue(false));
                    }
                }

                std::vector<std::string> imageVector;
                _pointUIHandler->getAssociatedImages(imageVector);

                std::string projectLocation = _getProjectLocation();

                _imageList.clear();
                for (unsigned int i = 0; i < imageVector.size(); i++)
                {

                    if (osgDB::fileExists(imageVector[i]))
                    {
                        _imageList.append(QString::fromStdString("file:///" + imageVector[i]));
                    }
                    else if (osgDB::fileExists(projectLocation + "/" + imageVector[i]))
                    {
                        _imageList.prepend(QString::fromStdString("file:///" + projectLocation + "/" + imageVector[i]));
                    }
                }


                std::string audioFile = _pointUIHandler->getAssociatedVideoOrAudio("VIZ_AUDIO");

                if (audioFile != "")
                {
                    if (!osgDB::fileExists(audioFile))
                        audioFile = projectLocation + "/" + audioFile;

                    if (osgDB::fileExists(audioFile))
                    {
                        pointContextual->setProperty("audioAttached", QVariant::fromValue(true));

                        QString fileName = QString::fromStdString(osgDB::getStrippedName(audioFile));

                        pointContextual->setProperty("audioFileName", QVariant::fromValue(fileName));
                    }
                }

                std::string videoFile = _pointUIHandler->getAssociatedVideoOrAudio("VIZ_VIDEO");
                if (videoFile != "")
                {
                    if (!osgDB::fileExists(videoFile))
                        videoFile = projectLocation + "/" + videoFile;

                    if (osgDB::fileExists(videoFile))
                    {
                        pointContextual->setProperty("videoAttached", QVariant::fromValue(true));

                        QString fileName = QString::fromStdString(osgDB::getStrippedName(videoFile));

                        pointContextual->setProperty("videoFileName", QVariant::fromValue(fileName));
                    }
                }

                const std::string textPoint = _pointUIHandler->getAssociatedText();
                if (pointContextual)
                {
                    pointContextual->setProperty("pointText", QString::fromStdString(textPoint));
                }


                pointContextual->setProperty("name", QVariant::fromValue(name));
                pointContextual->setProperty("textActive", QVariant::fromValue(textActive));
                pointContextual->setProperty("latitude", QVariant::fromValue(latitude));
                pointContextual->setProperty("longitude", QVariant::fromValue(longitude));
                pointContextual->setProperty("altitude", QVariant::fromValue(altitude));
                if (_mapSheetComponent.valid())
                {
                    QString indianGR = QString::fromStdString(_mapSheetComponent->getMapSheetNumber(osg::Vec2d(position.y(), position.x())));
                    pointContextual->setProperty("grValue", QVariant::fromValue(indianGR));

                }
                pointContextual->setProperty("clampingEnabled", QVariant::fromValue(clampingEnabled));
                pointContextual->setProperty("fontType", QString::fromStdString(font));
                pointContextual->setProperty("textSize", QVariant::fromValue(currentTextSize));
                pointContextual->setProperty("textColor", QVariant::fromValue(qtextColor));
                pointContextual->setProperty("outlineColor", QVariant::fromValue(qoutlineColor));
                pointContextual->setProperty("billboardColor", QVariant::fromValue(qbillboardColor));
                pointContextual->setProperty("outline", QVariant::fromValue(isOutline));
                pointContextual->setProperty("billboard", QVariant::fromValue(isBillBoard));
                pointContextual->setProperty("layoutId", QVariant::fromValue(layout));
                pointContextual->setProperty("alignmentId", QVariant::fromValue(alignment));
                

                // pointContextual->setProperty("iconPath", QVariant::fromValue(QString::fromStdString(iconPath)));

                _setContextProperty("imageList", QVariant::fromValue(_imageList));
            }
        }
    }

    void CreatePointGUI::deletePoint()
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (!point.valid())
            {
                return;
            }

            CORE::IDeletable* deletable = point->getInterface<CORE::IDeletable>();
            if (!deletable)
            {
                return;
            }

            CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
            if (deletionUIHandler.valid())
            {
                deletionUIHandler->deleteObject(deletable);
            }
        }
    }

    void CreatePointGUI::clampingChanged(bool value)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                CORE::RefPtr<ELEMENTS::IClamper> clamper = point->getInterface<ELEMENTS::IClamper>();
                if (clamper.valid())
                {
                    clamper->setClampOnPlacement(value);
                }
            }

            if (!value)
            {
                handleLatLongAltChanged("altitude");
                return;
            }
        }
    }

    void CreatePointGUI::handleLatLongAltChanged(QString attribute)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            //! get current point
            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                QObject* pointContextual = _findChild(PointContextualMenuObjectName);
                if (pointContextual != NULL)
                {
                    bool isLatLongChanged = false;
                    double longitudeD = point->getX();
                    double latitudeD = point->getY();
                    double altitudeD = point->getZ() - ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD);

                    if (!attribute.compare("latitude", Qt::CaseInsensitive))
                    {
                        QString latitude = pointContextual->property("latitude").toString();
                        latitudeD = latitude.toDouble();
                        isLatLongChanged = true;
                    }
                    else if (!attribute.compare("longitude", Qt::CaseInsensitive))
                    {
                        QString longitude = pointContextual->property("longitude").toString();
                        longitudeD = longitude.toDouble();
                        isLatLongChanged = true;
                    }
                    else if (!attribute.compare("altitude", Qt::CaseInsensitive))
                    {
                        QString altitude = pointContextual->property("altitude").toString();
                        altitudeD = altitude.toDouble();
                    }
                    else if (!attribute.compare("gr", Qt::CaseInsensitive))
                    {
                        QString grValue = pointContextual->property("grValue").toString();
                        std::string mapVal, grVal;
                        QStringList tokens = grValue.split(" ");
                        if (tokens.size() == 2)
                        {
                            mapVal = tokens[0].toStdString();
                            grVal = tokens[1].toStdString();
                        }
                        if (_mapSheetComponent->isValid(mapVal, grVal))
                        {
                            osg::Vec2d longLat = _mapSheetComponent->getLatLongFromMapsheetAndGR(mapVal, grVal);
                            if (longLat.x() > -180 && longLat.x() < 180 && longLat.y() > -90 && longLat.y() < 90)
                            {
                                latitudeD = longLat.y();
                                longitudeD = longLat.x();
                                //changed lat long also
                                pointContextual->setProperty("latitude", QString::number(latitudeD, 'f', 6));
                                pointContextual->setProperty("longitude", QString::number(longitudeD, 'f', 6));
                            }

                        }
                        else
                        {   //if not correct revert Previous GR according lat long value
                            QString indianGR = QString::fromStdString(_mapSheetComponent->getMapSheetNumber(osg::Vec2d(latitudeD, longitudeD)));
                            pointContextual->setProperty("grValue", QVariant::fromValue(indianGR));
                            emit showError("Indian GR", "GR not valid");
                            return;
                        }
                    }

                    osg::Vec3 position(longitudeD, latitudeD, altitudeD + ELEMENTS::GetAltitudeAtLongLat(longitudeD, latitudeD));

                    point->setValue(position);
                }
            }
        }
    }

    void CreatePointGUI::connectSMPMenu(QString type)
    {
        if (type == "markingFeaturesMenu")
        {
            // Clear the current selection
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if (selectionUIHandler.valid())
            {
                selectionUIHandler->clearCurrentSelection();
            }

            QObject* markingFeaturesMenu = _findChild("markingFeaturesMenu");
            if (markingFeaturesMenu != NULL)
            {
                QObject::connect(markingFeaturesMenu, SIGNAL(setPointEnable(bool)),
                    this, SLOT(setPointEnable(bool)), Qt::UniqueConnection);
                QObject::connect(markingFeaturesMenu, SIGNAL(setLabelEnable(bool)),
                    this, SLOT(setLabelEnable(bool)), Qt::UniqueConnection);
            }
        }
        else
        {

        }
    }

    void CreatePointGUI::_resetForDefaultHandling()
    {
        _addToDefault = true;
        _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
        _pointUIHandler->setPointEnable(true);
        _pointEnabled = true;
        setActive(false);
    }

    void CreatePointGUI::addPointsToSelectedLayer(bool value)
    {
        if (value)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

            const CORE::ISelectionComponent::SelectionMap& map =
                selectionUIHandler->getCurrentSelection();

            CORE::ISelectionComponent::SelectionMap::const_iterator iter =
                map.begin();

            //XXX - using the first element in the selection
            if (iter != map.end())
            {
                _selectedFeatureLayer = iter->second->getInterface<CORE::IFeatureLayer>();
                _addToDefault = false;
            }
            _pointUIHandler->setPointEnable(true);
            _pointEnabled = true;
            setActive(true);
        }
        else
        {
            _resetForDefaultHandling();
        }
    }


    void CreatePointGUI::setPointEnable(bool value)
    {
        if (value){
            _pointUIHandler->setPointEnable(true);
            _pointEnabled = true;
        }
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if (getActive() != value)
        {
            if (selectionUIHandler.valid())
            {
                selectionUIHandler->clearCurrentSelection();
            }
        }
        setActive(value);
    }

    void CreatePointGUI::setLabelEnable(bool value){
        if (value){
            _pointUIHandler->setPointEnable(false);
            _pointEnabled = false;
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());

        if (getActive() != value)
        {
            if (selectionUIHandler.valid())
            {
                selectionUIHandler->clearCurrentSelection();
            }
        }
        setActive(value);
    }


    void CreatePointGUI::rename(QString newName)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                point->getInterface<CORE::IBase>()->setName(newName.toStdString());
                //            point->getInterface<CORE::IText>()->setText(newName.toStdString());
            }
        }
    }


    void CreatePointGUI::setTextActive(bool state)
    {
        if (_selectedFeature.valid())
        {
        }
        else
        {
            if (!_pointUIHandler.valid())
            {
                return;
            }

            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point.valid())
            {
                point->getInterface<CORE::IText>()->setTextActive(state);
            }
        }
    }

    void CreatePointGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _loadPointUIHandler2();
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            _performHardReset();
        }
        else if (messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            _populateAttributes();
            _createMetadataRecord();

            if (_addToDefault)
            {
                _pointUIHandler->addPointToCurrentSelectedLayer();
            }
            else
            {
                _pointUIHandler->addToLayer(_selectedFeatureLayer);
            }
        }
        else if (messageType == *SMCUI::IPointUIHandler2::PointUpdatedMessageType)
        {
            _populateContextualMenu();
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }

    void CreatePointGUI::_performHardReset()
    {
        _pointNumber = 1;
        _cleanup = true;

        setActive(false);

        if (_pointUIHandler.valid())
            _pointUIHandler->reset();
    }

    void CreatePointGUI::setActive(bool value)
    {
        // Check if _pointuihandler is valid
        if (!_pointUIHandler.valid())
        {
            // try to load
            _loadPointUIHandler2();
            // Check again..
            if (!_pointUIHandler.valid())
            {
                return;
            }
        }

        if (getActive() == value)
        {
            return;
        }

        if (value)
        {
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
            _pointUIHandler->setTemporaryState(true);
            _pointUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _subscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
        }
        else
        {
            _selectedFeature = NULL;
            _pointUIHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
            _pointUIHandler->setTemporaryState(false);
            _pointUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            _unsubscribe(_pointUIHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
        }
        QtGUI::setActive(value);
    }

    void CreatePointGUI::_loadPointUIHandler2()
    {
        _pointUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());
        _selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
    }

    void CreatePointGUI::_populateAttributes()
    {
        std::string name;
        if (_pointEnabled){
            name = "Point";
        }
        else{
            name = "Label";
        }

        std::string layerName = "";
        unsigned int numObjects = 0;

        if (_addToDefault)
        {
            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
            if (worldMaintainer.valid())
            {
                CORE::RefPtr<CORE::IComponent> component =
                    worldMaintainer->getComponentByName("LayerComponent");
                if (component.valid())
                {
                    CORE::RefPtr<SMCElements::ILayerComponent> layerComponent =
                        component->getInterface<SMCElements::ILayerComponent>();
                    if (layerComponent)
                    {
                        CORE::RefPtr<CORE::IFeatureLayer> layer;
                        if (_pointEnabled){
                            layer = layerComponent->getFeatureLayer("Point");
                        }
                        else{
                            layer = layerComponent->getFeatureLayer("Label");
                        }
                        if (layer.valid())
                        {
                            CORE::IBase *base = layer->getInterface<CORE::IBase>();
                            if (base)
                            {
                                layerName = base->getName();
                            }

                            CORE::ICompositeObject *compositeObject = layer->getInterface<CORE::ICompositeObject>();
                            if (compositeObject)
                            {
                                numObjects = compositeObject->getObjectMap().size();
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (_selectedFeatureLayer.valid())
            {
                CORE::IBase *base = _selectedFeatureLayer->getInterface<CORE::IBase>();
                if (base)
                {
                    layerName = base->getName();
                }

                CORE::ICompositeObject *compositeObject = _selectedFeatureLayer->getInterface<CORE::ICompositeObject>();
                if (compositeObject)
                {
                    numObjects = compositeObject->getObjectMap().size();
                }
            }
        }

        if (!layerName.empty())
        {
            name = layerName;
        }


        name += UTIL::ToString<unsigned int>(numObjects + 1);

        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        if (!point.valid())
        {
            return;
        }

        point->getInterface<CORE::IBase>()->setName(name);
        CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
        if (text.valid())
        {
            text->setText(name);
            text->setTextActive(true);
        }

        //updating the index
        ++_pointNumber;
    }

    void CreatePointGUI::_createMetadataRecord()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer =
            CORE::WorldMaintainer::instance();

        if (!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return;
        }

        CORE::RefPtr<CORE::IComponent> component =
            worldMaintainer->getComponentByName("DataSourceComponent");

        if (!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator =
            component->getInterface<CORE::IMetadataCreator>();

        if (!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return;
        }

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = NULL;
        if (_addToDefault)
        {
            tableDefn =
                _pointUIHandler->getCurrentSelectedLayerDefn();
        }
        else
        {
            try
            {
                if (_selectedFeatureLayer.valid())
                {
                    CORE::RefPtr<CORE::IMetadataTableHolder> holder =
                        _selectedFeatureLayer->getInterface<CORE::IMetadataTableHolder>(true);

                    if (holder.valid())
                    {
                        tableDefn = holder->getMetadataTable()->getMetadataTableDefn();
                    }
                }
            }
            catch (UTIL::Exception &e)
            {
                e.LogException();
            }
        }

        if (!tableDefn.valid())
        {
            LOG_ERROR("Invalid IMetadataTableDefn instance");
            return;
        }

        //create metadata record for the min max point
        CORE::RefPtr<CORE::IMetadataRecord> record =
            metadataCreator->createMetadataRecord();

        // set the table Definition to the records
        record->setTableDefn(tableDefn.get());

        //Set the value of record attribute equal to textfield value...
        CORE::RefPtr<CORE::IMetadataField> field =
            record->getFieldByName("NAME");
        if (field)
        {
            CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
            if (point != NULL)
            {
                field->fromString(point->getInterface<CORE::IBase>()->getName());
            }
        }
        _pointUIHandler->setMetadataRecord(record.get());
    }
    void CreatePointGUI::saveTextPoint(QString pointText)
    {
        if (_pointUIHandler.valid())
        {
            _pointUIHandler->setAssociatedText(pointText.toStdString());
        }
    }
    void CreatePointGUI::handleStyleApplyButton()
    {
        std::string currentSelectedJsonFile = jsonFile;
        std::ofstream outfile(currentSelectedJsonFile.c_str());

        QObject* templatelistPopup = _findChild("pointContextual");
        if (templatelistPopup != NULL)
        {
            std::string outString = templatelistPopup->property("jsonOutput").toString().toStdString();
            outfile << outString;
        }
        outfile.close();
        CORE::RefPtr<CORE::IPoint> point = _pointUIHandler->getPoint();
        
        if (point.valid())
        {
            CORE::RefPtr<SYMBOLOGY::IStyle> styleFile = DB::StyleSheetParser::readStyle(jsonFile);
            ELEMENTS::FeatureObject* featureNodeHolder = dynamic_cast<ELEMENTS::FeatureObject*>(point->getInterface<CORE::IObject>());
            if (featureNodeHolder != nullptr)
            {
                featureNodeHolder->setVizPlaceNodeStyle(styleFile);
                featureNodeHolder->setBillboard(true);
            }
        }
    }

    void CreatePointGUI::populateSymbolList()
    {
        QObject* pointContextualPopup = _findChild("pointContextual");
        if (pointContextualPopup != NULL)
        {
            QObject* modelDirTreeView = _findChild("modelDirTreeView");

            if (modelDirTreeView != NULL)
            {
                QObject::connect(modelDirTreeView, SIGNAL(clicked(int)), this,
                    SLOT(symbolFileSelected(int)), Qt::UniqueConnection);
            }
        }

        std::string symbolAppDataDirPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "\\data";
        QDir symbolAppDataDir(QString::fromStdString(symbolAppDataDirPath));


        _qmlDirectoryFileTreeModel = new QMLDirectoryTreeModel;
        _qmlDirectoryFileTreeModel->showExtensions(true);

        QFileInfo fileInfo(symbolAppDataDir.absolutePath());
        _qmlDirectoryFileTreeModelRootItem = new QMLDirectoryTreeModelItem(fileInfo);
        _qmlDirectoryFileTreeModelRootItem->setCheckable(false);

        _qmlDirectoryFileTreeModelRootItem->clear();
        _qmlDirectoryFileTreeModelRootItem->clear();

        _qmlDirectoryFileTreeModel->addItem(_qmlDirectoryFileTreeModelRootItem);

        _populateModelDir(symbolAppDataDir, _qmlDirectoryFileTreeModelRootItem);

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {
            _contextPropertiesSet.push_back("modelList");
            qtapp->getRootContext()->setContextProperty("modelList", _qmlDirectoryFileTreeModel);
        }
    }
    void CreatePointGUI::symbolFileSelected(int index)
    {
        if (index < 0)
            return;

        if (index >= _qmlDirectoryFileTreeModel->rowCount())
            return;

        QMLDirectoryTreeModelItem* item = dynamic_cast<QMLDirectoryTreeModelItem*>(_qmlDirectoryFileTreeModel->getItem(index));

        if (item != NULL)
        {
            QFileInfo currentSelectedFile = item->getFileInfo();

            if (currentSelectedFile.isFile())
            {
                QString qAbsolutePath = currentSelectedFile.absoluteFilePath();

                std::string modelAppDataDirPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "\\data";
                QDir modelAppDataDir(QString::fromStdString(modelAppDataDirPath));

                QString relatievePath = modelAppDataDir.relativeFilePath(qAbsolutePath);

                QObject* modelDirTreeView = _findChild("modelDirTreeView");
                if (modelDirTreeView != NULL)
                {
                    modelDirTreeView->setProperty("selectedFileRelativePath", QVariant::fromValue(relatievePath));
                }
            }
        }
    }
    bool CreatePointGUI::_populateModelDir(QDir dir, QMLDirectoryTreeModelItem* parentItem)
    {
        if (dir.exists())
        {
            dir.setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs);
            dir.setSorting(QDir::DirsFirst);

            QStringList filters;
            filters << "*.ive" << "*.png";
            dir.setNameFilters(filters);


            QFileInfoList list = dir.entryInfoList();

            for (int i = 0; i < list.size(); ++i)
            {
                QFileInfo fileInfo = list.at(i);
                QMLDirectoryTreeModelItem* item = new QMLDirectoryTreeModelItem(fileInfo);

                std::string path = fileInfo.absoluteFilePath().toStdString();

                //not to show folder/files with .txt in them

                //if (path.substr(path.find_last_of(".") + 1) == "txt")
                //    return false;


                item->setCheckable(false);

                if (parentItem != NULL)
                    parentItem->addItem(item);
                else
                    _qmlDirectoryFileTreeModel->addItem(item);

                if (fileInfo.isDir())
                {
                    if (!_populateModelDir(QDir(fileInfo.absoluteFilePath()), item))
                        _qmlDirectoryFileTreeModel->removeItem(item);
                }
            }
        }
        return true;
    }

    void CreatePointGUI::handleFontButton()
    {
        QString directory = "c:/";
        QString caption = "Font";

        // populating the font list again as per new font directory
        QString filters = "Font (*.ttf *.TTF)";
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filepath = QFileDialog::getOpenFileName(parent, caption,directory, filters);

        std::string fileChosen = osgDB::getSimpleFileName(filepath.toStdString());
        if (fileChosen!="")
        {
            QObject *pointContextual = _findChild("pointContextual");
            if (pointContextual)
            {
                pointContextual->setProperty("fontString", QString::fromStdString(fileChosen));
            }
        }
    }
} // namespace SMCQt
