/****************************************************************************
*
* File             : AddBookmarksGUI.h
* Description      : AddBookmarksGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/AddBookmarksGUI.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <VizUI/VizUIPlugin.h>
#include <Util/FileUtils.h>
#include <QJSONDocument>
#include <QJSONObject>
#include <Core/WorldMaintainer.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, AddBookmarksGUI);
    DEFINE_IREFERENCED(AddBookmarksGUI, DeclarativeFileGUI);

    const std::string AddBookmarksGUI::BookmarksMenuPane = "smpBookmarksMenu";
    const std::string AddBookmarksGUI::AddOrRenameBookmarksPopup = "addOrRenameBookmarksPopup";

    AddBookmarksGUI::AddBookmarksGUI() : _currentSelectedBookmarkName(""), _currentSelectedBookmarkFilePath(""), _currentIndexInTree(0)
    {}

    AddBookmarksGUI::~AddBookmarksGUI()
    {}

    void AddBookmarksGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                QObject* BookmarksGUIObject = _findChild(BookmarksMenuPane);
                if (BookmarksGUIObject)
                {
                    //registering slots 
                    connect(BookmarksGUIObject, SIGNAL(addNewBookmark()), this, SLOT(_addBookmark()), Qt::UniqueConnection);
                    connect(BookmarksGUIObject, SIGNAL(deleteBookmark()), this, SLOT(_deleteBookmark()), Qt::UniqueConnection);
                    connect(BookmarksGUIObject, SIGNAL(editBookmark()), this, SLOT(_editBookmark()), Qt::UniqueConnection);

                    if (!_cameraUIHandler.valid())
                    {
                        _cameraUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager());
                    }

                    //populate the list
                    QObject* bookmarksDirTreeView = _findChild("bookmarkDirTreeView");
                    if (bookmarksDirTreeView != NULL)
                    {

                        QObject::connect(bookmarksDirTreeView, SIGNAL(clicked(int)), this,
                            SLOT(_bookMarkClicked(int)), Qt::UniqueConnection);

                        QObject::connect(bookmarksDirTreeView, SIGNAL(doubleClicked(int)), this,
                            SLOT(_bookMarkDoubleClicked(int)), Qt::UniqueConnection);

                        _populateBookmarksList();
                        _bookMarkClicked(0);
                    }
                }
            }
            else
            {
                _reset();
            }
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }
 
    void AddBookmarksGUI::_addBookmark()
    {
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if (popupLoader)
        {
            // load add bookmark popup
            QMetaObject::invokeMethod(popupLoader, "load", Q_ARG(QVariant, QVariant("AddOrRenameBookmarksPopup.qml")));
        }

        // updating _currentBookmark struct with current camera position
        if (_cameraUIHandler.valid())
        {
            _cameraUIHandler->getViewPoint(_currentBookmark.longLatAlt, _currentBookmark.range, _currentBookmark.heading, _currentBookmark.pitch);
        }

        // making connections
        QObject* addOrRenameBookmarks = _findChild(AddOrRenameBookmarksPopup);
        if (addOrRenameBookmarks)
        {
            connect(addOrRenameBookmarks, SIGNAL(okButtonClicked(QString)), this, SLOT(_writeBookmark(QString)), Qt::UniqueConnection);
        }

    }

    void AddBookmarksGUI::_writeBookmark(QString name)
    {
        if (name == "")
        {
            emit showError("Empty", "Please give a name for bookmark");
            return;
        }

        bool present = _isAlreadyPresent(name.toStdString());

        if (present)
        {
            emit showError("Please change the name", "Bookmark with this name is already present", true);
            return;
        }


        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return;
        }

        std::string bookmarksFolder = osgDB::getFilePath(wmain->getProjectFile()) + "/Bookmarks";

        std::string path = osgDB::convertFileNameToNativeStyle(bookmarksFolder);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }
        std::string fileName = "/" + name.toStdString() + ".bookmark";
        QFile settingFile(QString::fromStdString(bookmarksFolder + fileName));

        if (!settingFile.open(QIODevice::WriteOnly))
        {
            emit showError("Save setting", "Could not open bookmark file for save");
            return;
        }

        QJsonObject settingObject;
        settingObject["Longitude"] = _currentBookmark.longLatAlt.x();
        settingObject["Latitude"] = _currentBookmark.longLatAlt.y();
        settingObject["Altitude"] = _currentBookmark.longLatAlt.z();
        settingObject["Range"] = _currentBookmark.range;
        settingObject["Heading"] = _currentBookmark.heading;
        settingObject["Pitch"] = _currentBookmark.pitch;

        QJsonDocument settingDoc(settingObject);
        settingFile.write(settingDoc.toJson());

        QObject* addOrRenameBookmarks = _findChild(AddOrRenameBookmarksPopup);
        if (addOrRenameBookmarks)
        {
            QMetaObject::invokeMethod(addOrRenameBookmarks, "closePopup");
        }

        emit showError("Saved", "Bookmark has been saved", true);
        _populateBookmarksList();
        _bookMarkClicked(0);

    }

   

    void AddBookmarksGUI::_populateBookmarksList()
    {

        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return;
        }

        std::string bookmarksDataDirPath = osgDB::getFilePath(wmain->getProjectFile()) + "\\Bookmarks";
        QDir bookmarksDataDir(QString::fromStdString(bookmarksDataDirPath));


        _qmlDirectoryBookmarksTreeModel = new QMLDirectoryTreeModel;

        QFileInfo fileInfo(bookmarksDataDir.absolutePath());
        _qmlDirectoryBookmarksTreeModelRootItem = new QMLDirectoryTreeModelItem(fileInfo);
        _qmlDirectoryBookmarksTreeModelRootItem->setCheckable(false);

        _qmlDirectoryBookmarksTreeModel->clear();
        _qmlDirectoryBookmarksTreeModelRootItem->clear();

        _qmlDirectoryBookmarksTreeModel->addItem(_qmlDirectoryBookmarksTreeModelRootItem);

        _populateDir(bookmarksDataDir, _qmlDirectoryBookmarksTreeModelRootItem);

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {
            qtapp->getRootContext()->setContextProperty("bookmarkListModel", _qmlDirectoryBookmarksTreeModel);
        }
    }

    bool AddBookmarksGUI::_populateDir(QDir dir, QMLDirectoryTreeModelItem* parentItem)
    {
        if (dir.exists())
        {
            dir.setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs);
            dir.setSorting(QDir::DirsFirst);

            QStringList filters;
            filters << "*.bookmark";
            dir.setNameFilters(filters);


            QFileInfoList list = dir.entryInfoList();

            for (int i = 0; i < list.size(); ++i)
            {
                QFileInfo fileInfo = list.at(i);
                QMLDirectoryTreeModelItem* item = new QMLDirectoryTreeModelItem(fileInfo);

                std::string path = fileInfo.absoluteFilePath().toStdString();

                //not to show folder/files with .txt in them

                if (path.substr(path.find_last_of(".") + 1) == "txt")
                    return false;


                item->setCheckable(false);

                if (parentItem != NULL)
                    parentItem->addItem(item);
                else
                    _qmlDirectoryBookmarksTreeModel->addItem(item);

                if (fileInfo.isDir())
                {
                    if (!_populateDir(QDir(fileInfo.absoluteFilePath()), item))
                        _qmlDirectoryBookmarksTreeModel->removeItem(item);
                }
            }
        }
        return true;
    }

    // checks all bookmarks files with the name passed and returns true if its present
    bool AddBookmarksGUI::_isAlreadyPresent(std::string name)
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return false;
        }

        std::string bookmarksFolder = osgDB::getFilePath(wmain->getProjectFile()) + "/Bookmarks";
        
        std::string path = osgDB::convertFileNameToNativeStyle(bookmarksFolder);
        if (!osgDB::fileExists(path))
        {
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == name)
            {
                return true;
            }
        }

        return false;
    }

    void AddBookmarksGUI::_editBookmark()
    {
        if ((_currentSelectedBookmarkName == "Bookmarks" && _currentIndexInTree == 0) || _currentSelectedBookmarkName == "")
        {
            emit showError("Invalid", "Please select a valid bookmark");
            return;
        }

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if (popupLoader)
        {
            QMetaObject::invokeMethod(popupLoader, "load", Q_ARG(QVariant, QVariant("AddOrRenameBookmarksPopup.qml")));
        }

        QObject* addOrRenameBookmarks = _findChild(AddOrRenameBookmarksPopup);
        if (addOrRenameBookmarks)
        {
            addOrRenameBookmarks->setProperty("title", "Rename Bookmark");
            addOrRenameBookmarks->setProperty("bookmarkName", QString::fromStdString(_currentSelectedBookmarkName));
            connect(addOrRenameBookmarks, SIGNAL(okButtonClicked(QString)), this, SLOT(_editExistingBookmark(QString)), Qt::UniqueConnection);
        }
    }

    void AddBookmarksGUI::_editExistingBookmark(QString newName)
    {
        bool present = _isAlreadyPresent(newName.toStdString());

        if (present)
        {
            emit showError("Please change the name", "Bookmark with this name is already present", true);
            return;
        }

        if (newName.isEmpty())
        {
            emit showError("Please change the name", "Bookmark name can not be empty", true);
            return;
        }

        size_t found = _currentSelectedBookmarkFilePath.find_last_of("/");
        if (found != std::string::npos)
        {
            std::string newFileName = _currentSelectedBookmarkFilePath.substr(0, found + 1) + newName.toStdString() + ".bookmark";

            // returns 0 if renamed successfully
            bool result = rename(_currentSelectedBookmarkFilePath.c_str(), newFileName.c_str());

            if (!result)
            {
                QObject* addOrRenameBookmarks = _findChild(AddOrRenameBookmarksPopup);
                if (addOrRenameBookmarks)
                {
                    QMetaObject::invokeMethod(addOrRenameBookmarks, "closePopup");
                }
                emit showError("Renamed", "Bookmark has been renamed successfully", true);
                _populateBookmarksList();
                _bookMarkClicked(0);
            }

            else
            {
                emit showError("Error", "Could not rename, please try again", true);
                return;
            }
        }
    }

    void AddBookmarksGUI::_deleteBookmark()
    {
        if ((_currentSelectedBookmarkName == "Bookmarks" && _currentIndexInTree == 0 ) || _currentSelectedBookmarkName == "")
        {
            emit showError("Invalid", "Please select a valid bookmark to delete");
            return;
        }

        bool present = _isAlreadyPresent(_currentSelectedBookmarkName);

        if (present)
        {
            emit question("Delete", "Do you really want to delete this bookmark?", false,
                "_okSlotForDeleting()", "_cancelSlotForDeleting()");
        }
    }

    void AddBookmarksGUI::_okSlotForDeleting()
    {
        // returns 0 if deleted successfully
        bool removed = remove(_currentSelectedBookmarkFilePath.c_str());
        if (!removed)
        {
            _populateBookmarksList();
            _bookMarkClicked(0);
        }
    }

    void AddBookmarksGUI::_cancelSlotForDeleting()
    {
        return;
    }

    void AddBookmarksGUI::_bookMarkClicked(int index)
    {
        if (index < 0)
            return;

        if (index >= _qmlDirectoryBookmarksTreeModel->rowCount())
            return;

        _currentIndexInTree = index;

        QMLDirectoryTreeModelItem* item = dynamic_cast<QMLDirectoryTreeModelItem*>(_qmlDirectoryBookmarksTreeModel->getItem(index));

        if (item != NULL)
        {
            QFileInfo currentSelectedFile = item->getFileInfo();
            _currentSelectedBookmarkName = osgDB::getNameLessExtension(currentSelectedFile.fileName().toStdString());

            if (currentSelectedFile.isFile())
            {
                _currentSelectedBookmarkFilePath = currentSelectedFile.absoluteFilePath().toStdString();
            }
        }
    }

    void AddBookmarksGUI::_bookMarkDoubleClicked(int index)
    {
        if (index <= 0)
        {
            emit showError("Invalid", "Please select a valid bookmark");
            return;
        }

        if (index >= _qmlDirectoryBookmarksTreeModel->rowCount())
            return;

        _currentIndexInTree = index;

        QMLDirectoryTreeModelItem* item = dynamic_cast<QMLDirectoryTreeModelItem*>(_qmlDirectoryBookmarksTreeModel->getItem(index));

        if (item != NULL)
        {
            QFileInfo currentSelectedFile = item->getFileInfo();
            _currentSelectedBookmarkName = osgDB::getNameLessExtension(currentSelectedFile.fileName().toStdString());

            if (currentSelectedFile.isFile())
            {
                _currentSelectedBookmarkFilePath = currentSelectedFile.absoluteFilePath().toStdString();
            }

            QFile settingFile(QString::fromStdString(_currentSelectedBookmarkFilePath));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                return;
            }

            QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
            if (jdoc.isNull())
            {
                return;
            }

            if (jdoc.isObject() && jdoc.object().keys().size() == 6)
            {
                QJsonObject obj = jdoc.object();
                Bookmark bookmark;
                bookmark.longLatAlt.x() = obj["Longitude"].toDouble();
                bookmark.longLatAlt.y() = obj["Latitude"].toDouble();
                bookmark.longLatAlt.z() = obj["Altitude"].toDouble();
                bookmark.range = obj["Range"].toDouble();
                bookmark.heading = obj["Heading"].toDouble();
                bookmark.pitch = obj["Pitch"].toDouble();

                if (_cameraUIHandler.valid())
                {
                    _cameraUIHandler->setViewPoint(bookmark.longLatAlt, bookmark.range, bookmark.heading, bookmark.pitch);
                }
            }

            else
            {
                emit showError("Invalid Parameters", "Bookmark is invalid");
                return;
            }
        }       
    }

    // resets variables
    void AddBookmarksGUI::_reset()
    {
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {
            _setContextProperty("bookmarkListModel", QVariant::fromValue(NULL));
        }

        if (_cameraUIHandler.valid())
        {
            _cameraUIHandler = NULL;
        }

        if (_qmlDirectoryBookmarksTreeModel)
        {
            delete _qmlDirectoryBookmarksTreeModel;
            _qmlDirectoryBookmarksTreeModel = NULL;
        }

        if (_qmlDirectoryBookmarksTreeModelRootItem)
        {

            delete _qmlDirectoryBookmarksTreeModelRootItem;
            _qmlDirectoryBookmarksTreeModelRootItem = NULL;
        }

        _currentIndexInTree = 0;
        _currentSelectedBookmarkName = "";
        _currentSelectedBookmarkFilePath = "";

    }

}//namespace SMCQt
