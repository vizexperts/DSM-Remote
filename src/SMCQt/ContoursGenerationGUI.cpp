/****************************************************************************
*
* File             : ContoursGenerationGUI.h
* Description      : ContoursGenerationGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/
//#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/ContoursGenerationGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>

#include <Util/FileUtils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ContoursGenerationGUI);
    DEFINE_IREFERENCED(ContoursGenerationGUI,DeclarativeFileGUI);

    const std::string ContoursGenerationGUI::ContoursAnalysisPopup = "ContoursAnalysis";

    ContoursGenerationGUI::ContoursGenerationGUI()
    {}

    ContoursGenerationGUI::~ContoursGenerationGUI()
    {}

    void ContoursGenerationGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(_handleProgressValueChanged(int)),Qt::QueuedConnection);
    }

    void ContoursGenerationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IContoursGenerationUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    QObject* ContoursAnalysisObject = _findChild(ContoursAnalysisPopup);
                    if(ContoursAnalysisObject)
                    {
                        ContoursAnalysisObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        ContoursAnalysisObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        ContoursAnalysisObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        ContoursAnalysisObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
             GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if(progress < 100)
                {
                   emit _setProgressValue(progress);
                }
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                    emit _setProgressValue(100);
            }
            else
            {
                 if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    emit showError("Filter Status", "Failed to apply filter");
                    //Again Set Calculate 
                    _handlePBStopClicked();
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void ContoursGenerationGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                _populateStyleList();

                QObject* ContoursAnalysisObject = _findChild(ContoursAnalysisPopup);
                if(ContoursAnalysisObject)
                {
                    // start and stop button handlers
                    connect(ContoursAnalysisObject, SIGNAL(startAnalysis()),this,SLOT(_handlePBStartClicked()),Qt::UniqueConnection);
                    connect(ContoursAnalysisObject, SIGNAL(stopAnalysis()),this, SLOT(_handlePBStopClicked()),Qt::UniqueConnection);
                    connect(ContoursAnalysisObject, SIGNAL(markPosition(bool)),this,SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);
                    connect(ContoursAnalysisObject, SIGNAL(setStyle(QString)), this, SLOT(_setStyle(QString)), Qt::UniqueConnection);

                    // enable start button
                    ContoursAnalysisObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    ContoursAnalysisObject->setProperty("isStopButtonEnabled",false);

                    //disable progress value
                    ContoursAnalysisObject->setProperty("progressValue",0);
                }
            }
            else
            {
                _reset();
                _handlePBStopClicked();
            }
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ContoursGenerationGUI::_handleProgressValueChanged(int value)
    {
        QObject* ContoursAnalysisObject = _findChild(ContoursAnalysisPopup);
        if(ContoursAnalysisObject)
        {
            ContoursAnalysisObject->setProperty("progressValue",QVariant::fromValue(value));
        } 
    }

    //! 
    void ContoursGenerationGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        QObject* ContoursAnalysisObject = _findChild(ContoursAnalysisPopup);
        if(ContoursAnalysisObject)
        {
            std::string name = ContoursAnalysisObject->property("outputName").toString().toStdString();

            if(name == "")
            {
                showError( "Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            ContoursAnalysisObject->setProperty("outputName",QString::fromStdString(name));
            _uiHandler->setOutputName(name);

            //get Elevation
            QString TempElevationVal=ContoursAnalysisObject->property("elevationInterval").toString();
            if(TempElevationVal == "")
            {
                showError( "Insufficient / Incorrect data", "Elevation Interval Value is not specified");
                return;
            }
            double ElevationIntVal = TempElevationVal.toDouble();

            if(ElevationIntVal <=0)
            {
                showError( "Insufficient / Incorrect data", "Please enter a Elevation Interval Value greater than 0");
                return;
            }
            _uiHandler->setElevationInterval(ElevationIntVal);

            //get offset
            QString TempOffSetVal=ContoursAnalysisObject->property("offSetValue").toString();
            if(TempOffSetVal == "")
            {
                showError( "Insufficient / Incorrect data", "OffSet Value is not specified");
                return;
            }
            double OffsetVal = TempOffSetVal.toDouble();;
            if(OffsetVal < 0)
            {
                showError( "Insufficient / Incorrect data", "Please enter a offSetValue Value  ");
                return;
            }
            _uiHandler->setOffset(OffsetVal);
            
            QColor rectColor = ContoursAnalysisObject->property("rectColor").value<QColor>();
            osg::Vec4d ocolor = osg::Vec4d(rectColor.red() / 255.0, rectColor.green() / 255.0, rectColor.blue() / 255.0, rectColor.alpha() / 255.0);
            _uiHandler->setUserDefineColor(ocolor);

            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = ContoursAnalysisObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = ContoursAnalysisObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = ContoursAnalysisObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() =  ContoursAnalysisObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError( "Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
                        showError( "Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                }
            }


            ContoursAnalysisObject->setProperty("progressValue",0); 
            _handlePBAreaClicked(false);
            // start filter

            _uiHandler->execute();
        }
    }

    void ContoursGenerationGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();
        QObject* ContoursAnalysisObject = _findChild(ContoursAnalysisPopup);
        if(ContoursAnalysisObject)
        {
            //disable stop button
            ContoursAnalysisObject->setProperty("isStopButtonEnabled",false);
            ContoursAnalysisObject->setProperty("isStartButtonEnabled",true);
            ContoursAnalysisObject->setProperty("progressValue",QVariant::fromValue(0));
        } 
    }

    void ContoursGenerationGUI::_handlePBRunInBackgroundClicked()
    {}

    void ContoursGenerationGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            
            QObject* ContoursAnalysisObject = _findChild(ContoursAnalysisPopup);
            if(ContoursAnalysisObject)
            {
                ContoursAnalysisObject->setProperty( "isMarkSelected" , false );
            }
        }
    }

    void ContoursGenerationGUI::_reset()
    {
        QObject* ContoursAnalysisObject = _findChild(ContoursAnalysisPopup);
        if(ContoursAnalysisObject)
        {
            ContoursAnalysisObject->setProperty("isMarkSelected",false);
            ContoursAnalysisObject->setProperty("bottomLeftLongitude","longitude");
            ContoursAnalysisObject->setProperty("bottomLeftLatitude","latitude");
            ContoursAnalysisObject->setProperty("topRightLongitude","longitude");
            ContoursAnalysisObject->setProperty("topRightLatitude","latitude");
            ContoursAnalysisObject->setProperty("outputName","outputName");
            ContoursAnalysisObject->setProperty("elevationInterval",0);
            ContoursAnalysisObject->setProperty("offSetValue",0);
            ContoursAnalysisObject->setProperty("progressValue",0);
            _handlePBAreaClicked(false);
        }
    }

    void ContoursGenerationGUI::_populateStyleList()
    {
        _readAllStyle();

        int count = 1;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select Style", "0"));

        if (_styleList.size() == 0)
        {
            _setContextProperty("styleList", QVariant::fromValue(comboBoxList));
            return;
        }

        for (int i = 0; i < _styleList.size(); i++)
        {
            std::string databaseName = _styleList[i];
            comboBoxList.append(new VizComboBoxElement(databaseName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("styleList", QVariant::fromValue(comboBoxList));
    }

    void ContoursGenerationGUI::_readAllStyle()
    {
        _styleList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        _styleFolderPath = appdataPath + "/Styles";

        std::string path = osgDB::convertFileNameToNativeStyle(_styleFolderPath);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            if (osgDB::getLowerCaseFileExtension(currFileName) == "json")
            {
                std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

                _styleList.push_back(fileNameWithoutExtension);
            }
        }

        std::string defaultStyleJosonPath = _styleFolderPath + "/" + "Line.json";
        _uiHandler->setStyle(defaultStyleJosonPath);
    }

    void ContoursGenerationGUI::_setStyle(QString value)
    {
        std::string styleJsonPath;
        if (value.toStdString() != "Select Style")
        {
            styleJsonPath = _styleFolderPath + "/" + value.toStdString() + ".json";
            _uiHandler->setStyle(styleJsonPath);
        }
        else //we send default style 
        {
            styleJsonPath = _styleFolderPath + "/" + "Line.json";
            _uiHandler->setStyle(styleJsonPath);
        }
    }

}//namespace SMCQt
