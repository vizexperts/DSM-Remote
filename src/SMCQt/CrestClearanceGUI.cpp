/****************************************************************************
*
* File             : CrestClearanceGUI.h
* Description      : CrestClearanceGUI class definition. This file has GUI level implementation for the crest clearance.
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/CrestClearanceGUI.h>
#include <Core/IPoint.h>
#include <App/AccessElementUtils.h>
#include <App/IApplication.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <Core/AttributeTypes.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <iostream>
#include <Util/CoordinateConversionUtils.h>
#include <Util/FileUtils.h>

#define EPISILON 0.00001

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CrestClearanceGUI);
    DEFINE_IREFERENCED(CrestClearanceGUI, DeclarativeFileGUI);

    const std::string CrestClearanceGUI::LaunchIconPath             = "/Symbols/Transmitter.png";
    const std::string CrestClearanceGUI::DirIconPath                = "/Symbols/Receiver.png";

    const std::string CrestClearanceGUI::CrestClearanceMenuObject   = "CrestClearanceMenuObject";

    CrestClearanceGUI::CrestClearanceGUI()
        : _lpPoint(NULL)
        , _tgtPoint(NULL) , _crestAdd(false)
    {
    }

    CrestClearanceGUI::~CrestClearanceGUI()
    {
    }

    void CrestClearanceGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {   
                // handler for mouse events
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IPointUIHandler>(getGUIManager());                

                // handler for crest clearance backend code.
                _crestClearanceHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::ICrestClearanceUIHandler>(getGUIManager());                

                // load radar icon 
                APP::IGUIManager* guiMgr = getGUIManager();
                CORE::IWorldMaintainer* wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(guiMgr);

                if(!wmain)
                {
                    return;
                }

                CORE::IComponent* comp = wmain->getComponentByName("IconModelLoaderComponent");
                if(!comp)
                {
                    return;
                }

                ELEMENTS::IIconLoader* iconLoader = comp->getInterface<ELEMENTS::IIconLoader>();
                if(!iconLoader)
                {
                    return;
                }

                std::string dataDir = UTIL::getDataDirPath();
                // create launch icon
                std::string launchIconName = "LaunchIcon";
                iconLoader->loadIcon(launchIconName, dataDir + LaunchIconPath);
                _launchIcon = iconLoader->getIconByName(launchIconName);

                // create direction icon
                std::string dirIconName = "DirIcon";
                iconLoader->loadIcon(dirIconName, dataDir + DirIconPath);
                _dirIcon = iconLoader->getIconByName(dirIconName);

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }

        // to check for point created message
        else if(messageType == *VizUI::IPointUIHandler::PointCreatedMessageType)
        {
            _processPoint();
        }

        // Check whether the distance calculation is completed
        else if(messageType == *SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType)
        {
            _crestClearanceHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _unsubscribe(_crestClearanceHandler.get(), *SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType);
        }

        // to check the filter status and set/remove the loading icons
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();


            // if the calculation is still pending
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if(progress < 100)
                {
                    // TBD - support for progress bar has to be added
                }
            }

            // if the path calculation is completed.
            else
            {
                if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
                {
                    emit changeToDefaultStateSignal();

                    _crestAdd = true;

                    QObject::disconnect( this, SIGNAL(changeToDefaultStateSignal()), this, 
                         SLOT(changeToDefaultStateSlot()) );
                }
                // if filter failure then there is no trajectory possible
                else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    if (filterMessage->getFailureReason() == GISCOMPUTE::FAILUREREASON_VELOCITY)
                    {
                        const double& maxVelocityCalc = _crestClearanceHandler->getMaxVelocity();
                        const double& minVelocityCalc = _crestClearanceHandler->getMinVelocity();

                        const double& maxSpeed = _crestClearanceHandler->getMaxSpeed();
                        const double& minSpeed = _crestClearanceHandler->getMinSpeed();

                        std::stringstream strstrm;
                        if ((minVelocityCalc < minSpeed && maxVelocityCalc < minSpeed) || (minVelocityCalc > maxSpeed && maxVelocityCalc > maxSpeed))
                        {
                            strstrm << "No trajectory possible. Min-Max velocity mismatch \r\n";
                            strstrm << "Calculated : (Min : " << minVelocityCalc << " Max : " << maxVelocityCalc << ") \r\n";
                            strstrm << " Original : (Min : " << minSpeed << " Max : " << maxSpeed << ")\r\n";
                            strstrm << " Decrease 'Angle Change' and try again";
                        }
                        else
                        {
                            strstrm << "No trajectory possible. In given angle difference, No velocity value lies between Min-Max range.\r\n";
                            strstrm << " Range : (Min : " << minSpeed << " Max : " << maxSpeed << ") \r\n";
                        }
                        emit showError("Filter Status", strstrm.str().c_str());
                    }
                    // If the failure type is displacement or range mismatch.
                    if (filterMessage->getFailureReason() == GISCOMPUTE::FAILUREREASON_DISPLACMENT)
                    {
                        const double& displacement = _crestClearanceHandler->getDisplacement();
                        const double& minRange = _crestClearanceHandler->getMinRange();
                        const double& maxRange = _crestClearanceHandler->getMaxRange();

                        std::stringstream strstrm;

                        strstrm << "No trajectory possible. In or Out of range \r\n";
                        strstrm << "Calculated displacement : (" << displacement << ") \r\n";
                        strstrm << " Original Range: (Min : " << minRange << " Max : " << maxRange << ")\r\n";
                        strstrm << " Decrease 'Angle Change' and try again";

                        emit showError("Filter Status", strstrm.str().c_str());
                    }
                    else
                    {
                        emit showError("Filter Status", "No trajectory possible");
                    }
                    emit changeToDefaultStateSignal();

                    QObject::disconnect( this, SIGNAL(changeToDefaultStateSignal()), this, 
                         SLOT(changeToDefaultStateSlot()) );
                }
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void CrestClearanceGUI::changeToDefaultStateSlot()
    {
        if(!_crestClearanceMenuObject)
            return;

        // filture sucess -> trajectory calculation sucessful
            if ( _state == LP_MARK_PPC || _state == DIR_MARK_PPC )
            {
                _crestClearanceMenuObject->setProperty( "crestClearanceProgressBarPPC", false );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
            }

            else if ( _state == LP_MARK_VAC || _state == TP_MARK_VAC )
            {
                _crestClearanceMenuObject->setProperty( "crestClearanceProgressBarVAC", false );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
            }
    }

    // this function handles different mouse clicks based on different states 
    void CrestClearanceGUI::_processPoint()
    {
        if(!_crestClearanceMenuObject)
            return;

        CORE::IPoint* point = _pointHandler->getPoint();
        QString pointX, pointY, pointZ;

        pointX = QString::number(point->getX());
        pointY = QString::number(point->getY());
        pointZ = QString::number(point->getZ());

        ELEMENTS::IIconHolder* iconHolder = point->getInterface<ELEMENTS::IIconHolder>();

        // process the points based upon the state and set the corresponding text fields
        if(_pointHandler.valid())
        {
            switch(_state)
            {
            case LP_MARK_PPC: 
                {
                    _crestClearanceMenuObject->setProperty( "lePPCLaunchPointLat", pointX );
                    _crestClearanceMenuObject->setProperty( "lePPCLaunchPointLong", pointY );
                    _crestClearanceMenuObject->setProperty( "lePPCLaunchPointAlt", pointZ );

                    _setLaunchPoint(point);

                    if(_launchIcon.valid() && iconHolder)
                    {
                        iconHolder->setIcon(_launchIcon.get());
                    }
                    else
                    {
                        LOG_DEBUG("Default Icon for launching point is used")
                    }
                    break;
                }

            case DIR_MARK_PPC: 
                {
                    _targetPos = point->getValue();

                    _crestClearanceMenuObject->setProperty( "lePPCDirectionX", pointY );
                    _crestClearanceMenuObject->setProperty( "lePPCDirectionY", pointX );
                    _crestClearanceMenuObject->setProperty( "lePPCDirectionZ", pointZ );

                    _setTargetPoint(point);

                    if(_dirIcon.valid() && iconHolder)
                    {
                        iconHolder->setIcon(_dirIcon.get());
                    }
                    else
                    {
                        LOG_DEBUG("Default Icon for direction marking is used")
                    }
                    break;
                }

            case LP_MARK_VAC: 
                {
                    _crestClearanceMenuObject->setProperty( "leVACLaunchPointLat", pointX );
                    _crestClearanceMenuObject->setProperty( "leVACLaunchPointLong", pointY );
                    _crestClearanceMenuObject->setProperty( "leVACLaunchPointAlt", pointZ );

                    _setLaunchPoint(point);

                    if(_launchIcon.valid() && iconHolder)
                    {
                        iconHolder->setIcon(_launchIcon.get());
                    }
                    else
                    {
                        LOG_DEBUG("Default Icon for launching point is used")
                    }
                    break;
                }

            case TP_MARK_VAC: 
                {
                    _crestClearanceMenuObject->setProperty( "leVACTargetPointX", pointX );
                    _crestClearanceMenuObject->setProperty( "leVACTargetPointY", pointY );
                    _crestClearanceMenuObject->setProperty( "leVACTargetPointZ", pointZ );

                    _setTargetPoint(point);

                    if(_dirIcon.valid() && iconHolder)
                    {
                        iconHolder->setIcon(_dirIcon.get());
                    }
                    else
                    {
                        LOG_DEBUG("Default Icon for target point is used")
                    }
                    break;
                }

            default:
                _removeObject(point->getInterface<CORE::IObject>());
                break;
            }
        }
    }

    // XXX - Move these slots to a proper place
    void CrestClearanceGUI::setActive(bool value)
    {
        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                _state = NO_MARK;
                _subscribe(_crestClearanceHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                _crestClearanceMenuObject = _findChild(CrestClearanceMenuObject);

                // connect the signal of various buttons in crest clearance qml
                if(_crestClearanceMenuObject)
                {
                    // Projectile Path Calculation signals
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandlePPCLPMarkButtonClicked(bool)), 
                        this, SLOT(_handlePPCLPMarkButtonClicked(bool)));
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandlePPCDirMarkButtonClicked(bool)), 
                        this, SLOT(_handlePPCDirMarkButtonClicked(bool)));
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandlePPCStartButtonClicked()), 
                        this, SLOT(_handlePPCStartButtonClicked()));
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandlePPCStopButtonClicked()), 
                        this, SLOT(_handlePPCStopButtonClicked()));

                    // Velocity and Angle Calculation signals
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandleVACLPMarkButtonClicked(bool)), 
                        this, SLOT(_handleVACLPMarkButtonClicked(bool)));
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandleVACTPMarkButtonClicked(bool)), 
                        this, SLOT(_handleVACTPMarkButtonClicked(bool)));
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandleVACStartButtonClicked()), 
                        this, SLOT(_handleVACStartButtonClicked()));
                    QObject::connect( _crestClearanceMenuObject, SIGNAL(qmlHandleVACStopButtonClicked()), 
                        this, SLOT(_handleVACStopButtonClicked()));
                }

            }
            else
            {
                _unsubscribe(_crestClearanceHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                _removeObject(_lpPoint.get());
                _removeObject(_tgtPoint.get());
                _crestClearanceHandler->stopFilter();
                _state = NO_MARK;

                _crestClearanceMenuObject = NULL;
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void CrestClearanceGUI::_handleVACStartButtonClicked()
    {
        if(!_crestClearanceMenuObject)
            return;

        if(_crestClearanceHandler.valid())
        {
            _subscribe(_crestClearanceHandler.get(), *SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType);
            _crestClearanceHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _crestClearanceHandler->setComputationType(SMCUI::ICrestClearanceUIHandler::ANGLE_AND_VELOCITY);

            // get he line edit widget for height
            QString lpHeight = _crestClearanceMenuObject->property("leVACLaunchPointHeight").toString();

            if(lpHeight ==  "")
            {
                emit showError( "Crest Clearance", "Altitude not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            double height = lpHeight.toDouble();

            // Pass the parameters to the crest clearance UI handler
            QString lpLatLE = _crestClearanceMenuObject->property("leVACLaunchPointLat").toString();
            QString lpLongLE = _crestClearanceMenuObject->property("leVACLaunchPointLong").toString();
            QString lpAltLE = _crestClearanceMenuObject->property("leVACLaunchPointAlt").toString();

            osg::Vec3d launchingpos(osg::Vec3d(lpLatLE.toDouble(), lpLongLE.toDouble(), lpAltLE.toDouble() ));

            if(launchingpos.length() < EPISILON)
            {
                emit showError( "Crest Clearance", "Invalid launching position specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                LOG_ERROR("Invalid launching position specified");
                return;
            }

            launchingpos.z() = launchingpos.z() + height;

            _crestClearanceHandler->setLaunchingPosition(launchingpos);

            // Pass the parameters to the crest clearance UI handler

            QString tpDirXLE = _crestClearanceMenuObject->property("leVACTargetPointX").toString();
            QString tpDirYLE = _crestClearanceMenuObject->property("leVACTargetPointY").toString();
            QString tpDirZLE = _crestClearanceMenuObject->property("leVACTargetPointZ").toString();

            // get he line edit widget for height
            lpHeight = _crestClearanceMenuObject->property("leVACTargetPointHeight").toString();
            if(lpHeight ==  "")
            {
                emit showError( "Crest Clearance", "Altitude not specified", true);
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }
            height = lpHeight.toDouble();

            osg::Vec3d targetpos(osg::Vec3d( tpDirXLE.toDouble(), tpDirYLE.toDouble(), tpDirZLE.toDouble() ));

            if((tpDirXLE == "") || (tpDirYLE == "")  || (tpDirZLE == ""))
            {
                emit showError( "Crest Clearance", "Invalid target point specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                LOG_ERROR("Invalid direction specified");
                return;
            }

            targetpos.z() = targetpos.z() + height;
            _crestClearanceHandler->setTargetPosition(targetpos);

            // Pass the parameters to the crest clearance UI handler
            QString minAngleLE = _crestClearanceMenuObject->property("leVACMinAngle").toString();


            if(minAngleLE ==  "")
            {
                emit showError("Crest Clearance", "Minimum Angle not specified", true);
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            double minangle  = minAngleLE.toDouble();
            if(minangle < (double)(-90.0))
            {
                emit showError( "Crest Clearance", "Minimum angle should be greater than -90.0", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            _crestClearanceHandler->setMinAngle(minangle);

            // Pass the parameters to the crest clearance UI handler
            QString maxAngleLE = _crestClearanceMenuObject->property("leVACMaxAngle").toString();


            if(maxAngleLE ==  "")
            {
                emit showError( "Crest Clearance", "Maximum Angle not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            double maxangle  = maxAngleLE.toDouble();
            if(maxangle > (double)90.0)
            {
                emit showError( "Crest Clearance", "Maximum angle should be less than 90.0", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            if( maxangle < minangle )
            {
                emit showError( "Crest Clearance", "Maximum angle should be greater than Minimum angle", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            _crestClearanceHandler->setMaxAngle(maxangle);

            // Pass the parameters to the crest clearance UI handler
            QString minSpeedLE = _crestClearanceMenuObject->property("leVACMinSpeed").toString();

            if( minSpeedLE ==  "" )
            {
                emit showError( "Crest Clearance", "Minimum Speed not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            _crestClearanceHandler->setMinSpeed( minSpeedLE.toDouble() );

            // Pass the parameters to the crest clearance UI handler
            QString maxSpeedLE = _crestClearanceMenuObject->property("leVACMaxSpeed").toString();

            if(maxSpeedLE ==  "")
            {
                emit showError( "Crest Clearance", "Maximum Speed not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            _crestClearanceHandler->setMaxSpeed( maxSpeedLE.toDouble() );

            if ( minSpeedLE.toDouble() > maxSpeedLE.toDouble() )
            {
                emit showError( "Crest Clearance", "Maximum Speed should be greater than Minimum speed", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            // Pass the parameters to the crest clearance UI handler
            QString stepSize = _crestClearanceMenuObject->property("leVACAngleStepSize").toString();

            if(stepSize ==  "")
            {
                emit showError( "Crest Clearance", "Step Size not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
                return;
            }

            _crestClearanceMenuObject->setProperty( "crestClearanceProgressBarVAC", true );
            QObject::connect( this, SIGNAL(changeToDefaultStateSignal()), this, 
                SLOT(changeToDefaultStateSlot()), Qt::QueuedConnection );

            _crestClearanceHandler->setAngleStepSize( stepSize.toDouble() );
            _crestClearanceHandler->execute();
        }
    }

    void CrestClearanceGUI::_handleVACStopButtonClicked()
    {
        if(!_crestClearanceMenuObject)
            return;

        if(!_crestClearanceHandler.valid())
            return;

        _crestClearanceHandler->stopFilter();

        _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultVAC" );
        _crestClearanceMenuObject->setProperty( "crestClearanceProgressBarVAC", false );

        QObject::disconnect( this, SIGNAL(changeToDefaultStateSlot()), this, 
                SLOT(changeToDefaultStateSignal()) );
    }

    void CrestClearanceGUI::_handlePPCStartButtonClicked()
    {
        if(!_crestClearanceMenuObject)
            return;

        // Subscribe to distance calculation message
        if(_crestClearanceHandler.valid())
        {
            _subscribe(_crestClearanceHandler.get(), *SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType);
            _crestClearanceHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _crestClearanceHandler->setComputationType(SMCUI::ICrestClearanceUIHandler::TRAJECTORY_CALCULATION);

            // get he line edit widget for height
            QString lpHeight = _crestClearanceMenuObject->property("lePPCLaunchPointHeight").toString();

            if(lpHeight == "")
            {
                emit showError( "Crest Clearance", "Launch Altitude not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
                return;
            }

            double height = lpHeight.toDouble();

            // Pass the parameters to the crest clearance UI handler
            QString lpLatLE = _crestClearanceMenuObject->property("lePPCLaunchPointLat").toString();
            QString lpLongLE = _crestClearanceMenuObject->property("lePPCLaunchPointLong").toString();
            QString lpAltLE = _crestClearanceMenuObject->property("lePPCLaunchPointAlt").toString();

            osg::Vec3d launchingpos(osg::Vec3d( lpLatLE.toDouble(), lpLongLE.toDouble(), lpAltLE.toDouble() ));

            if(launchingpos.length() < EPISILON)
            {
                emit showError( "Crest Clearance", "Invalid launching position specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
                LOG_ERROR("Invalid launching position specified");
                return;
            }

            launchingpos.z() = launchingpos.z() + height;
            _crestClearanceHandler->setLaunchingPosition(launchingpos);


            QString lpDirXLE = _crestClearanceMenuObject->property("lePPCDirectionX").toString();
            QString lpDirYLE = _crestClearanceMenuObject->property("lePPCDirectionY").toString();
            QString lpDirZLE = _crestClearanceMenuObject->property("lePPCDirectionZ").toString();

            osg::Vec3d targetPosition(osg::Vec3d( lpDirXLE.toDouble(), lpDirYLE.toDouble(), lpDirZLE.toDouble() ));

            if(targetPosition.length() < EPISILON)
            {
                emit showError( "Crest Clearance", "Invalid Target position specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
                LOG_ERROR("Invalid Target position specified");
                return;
            }
            _crestClearanceHandler->setTargetPosition(_targetPos);

            osg::Vec3d dir;
            dir = _targetPos - launchingpos;
            dir.normalize();
            _crestClearanceHandler->setDirection(dir);

            // Pass the parameters to the crest clearance UI handler
            QString lpAngleLE = _crestClearanceMenuObject->property("lePPCAngle").toString();

            if(lpAngleLE ==  "")
            {
                emit showError( "Crest Clearance", "Angle not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
                return;
            }

            double angle = lpAngleLE.toDouble();

            // this is check not necessary, as the value will be checked in the qml through validator

            if((angle < 0.0)||(angle > 90.0))
            {
                emit showError( "Crest Clearance", "Angle should lie between 0 to 90.0", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
                return;
            }

            _crestClearanceHandler->setLaunchingAngle(angle);

            // Pass the parameters to the crest clearance UI handler
            QString lpVelocityLE = _crestClearanceMenuObject->property("lePPCVelocity").toString();

            if(lpVelocityLE ==  "")
            {
                emit showError( "Crest Clearance", "Velocity not specified", true );
                _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
                return;
            }
            _crestClearanceHandler->setLaunchingSpeed( lpVelocityLE.toDouble() );
            _crestClearanceMenuObject->setProperty( "crestClearanceProgressBarPPC", true );

            QObject::connect( this, SIGNAL(changeToDefaultStateSignal()), this, 
                SLOT(changeToDefaultStateSlot()), Qt::QueuedConnection );

            _crestClearanceHandler->execute();
        }
    }

    void CrestClearanceGUI::_handlePPCStopButtonClicked()
    {
        if(!_crestClearanceMenuObject)
            return;

        if(!_crestClearanceHandler.valid())
            return;

        _crestClearanceMenuObject->setProperty( "crestClearanceState", "defaultPPC" );
        _crestClearanceHandler->stopFilter();
        _crestClearanceMenuObject->setProperty( "crestClearanceProgressBarPPC", false );

        QObject::disconnect( this, SIGNAL(changeToDefaultStateSlot()), this, 
                SLOT(changeToDefaultStateSignal()) );
    }

    void CrestClearanceGUI::_handlePPCLPMarkButtonClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = LP_MARK_PPC;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(LP_MARK_PPC == lastState)
            {
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    void CrestClearanceGUI::_handlePPCDirMarkButtonClicked(bool pressed)
    {

        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = DIR_MARK_PPC;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(DIR_MARK_PPC == lastState)
            {
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    void CrestClearanceGUI::_handleVACLPMarkButtonClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = LP_MARK_VAC;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(LP_MARK_VAC == lastState)
            {
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    void CrestClearanceGUI::_handleVACTPMarkButtonClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = TP_MARK_VAC;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(TP_MARK_VAC == lastState)
            {
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    // this function will activate the mouse events and set the point to create events when clicked
    void CrestClearanceGUI::_setPointHandlerEnabled(bool enable)
    {
        if(_pointHandler.valid())
        {
            if(enable)
            {
                _pointHandler->setProcessMouseEvents(true);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);                 
            }
            else
            {
                _pointHandler->setProcessMouseEvents(false);
                _pointHandler->setHandleMouseClicks(false);
                _pointHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark trans/recv position");
        }
    }

    // to remove the objects from the world
    void CrestClearanceGUI::_removeObject(CORE::IObject* obj)
    {
        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        if(world.valid() && obj)
        {
            try
            {
                world->removeObjectByID(&(obj->getInterface<CORE::IBase>(true)->getUniqueID()));
            }
            catch(...){}
        }
    }

    // to set the target point icon to a position specifed by mouse click
    void CrestClearanceGUI::_setTargetPoint(CORE::IPoint* point)
    {
        _removeObject(_tgtPoint.get());
        _tgtPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

    // to set the launching point icon to a position specifed by mouse click
    void CrestClearanceGUI::_setLaunchPoint(CORE::IPoint* point)
    {
        _removeObject(_lpPoint.get());
        _lpPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

} // namespace GUI
