/****************************************************************************
*
* File             : ColorElevationGUI.h
* Description      : ColorElevationGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <SMCQt/ColorElevationGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <GISCompute/IColorCodedElevFilterVisitor.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Elements/IColorMapComponent.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>
#include<GISCOMPUTE/IMinMaxElevCalcVisitor.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ColorElevationGUI);
    DEFINE_IREFERENCED(ColorElevationGUI,DeclarativeFileGUI);

    const std::string ColorElevationGUI::ColorEleAnalysisPopup = "ColorEleAnalysis";

    ColorElevationGUI::ColorElevationGUI()
    {}

    ColorElevationGUI::~ColorElevationGUI()
    {}

    void ColorElevationGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(_handleProgressValueChanged(int)),Qt::QueuedConnection);
    }

    void ColorElevationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IColorElevationUIHandler>(getGUIManager());
                _colorUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IColorMapUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    QObject* ColorElevationObject = _findChild(ColorEleAnalysisPopup);
                    if(ColorElevationObject)
                    {
                        ColorElevationObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        ColorElevationObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        ColorElevationObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        ColorElevationObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
             GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
             //Check for min mac Elv calculated or not
             CORE::RefPtr<CORE::IBase> base = message.getSender();
             CORE::RefPtr<GISCOMPUTE::IMinMaxElevCalcVisitor> minmaxVisitor = base->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>();
             if (minmaxVisitor.valid())
             {
                 
                 if (filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
                 {
                    //nothing
                 }
                 else if (filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
                 {
                     _uiHandler->execute();
                 }
                 else
                 {
                     //failed MinMaxEleVCalcVisitor So default Min/Max wil work
                     _uiHandler->execute();
                 }
                 
             }
             else
             {
                 if (filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
                 {
                     unsigned int progress = filterMessage->getProgress();
                     // do not update it to 100%
                     if (progress < 100)
                     {
                         emit _setProgressValue(progress);
                     }
                 }
                 else if (filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
                 {
                     emit _setProgressValue(100);
                 }
                 else
                 {
                     if (filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                     {
                         emit showError("Filter Status", "Failed to apply filter");
                         _handlePBStopClicked();
                     }
                 }
             }

        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void ColorElevationGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                QObject* ColorElevationObject = _findChild(ColorEleAnalysisPopup);
                if(ColorElevationObject)
                {
                    // start and stop button handlers
                    connect(ColorElevationObject, SIGNAL(startAnalysis()),this,SLOT(_handlePBStartClicked()),Qt::UniqueConnection);
                    connect(ColorElevationObject, SIGNAL(stopAnalysis()),this, SLOT(_handlePBStopClicked()),Qt::UniqueConnection);
                    connect(ColorElevationObject, SIGNAL(markPosition(bool)),this, 
                        SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);

                    _populateColorMap();

                    // enable start button
                    ColorElevationObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    ColorElevationObject->setProperty("isStopButtonEnabled",false);

                    //disable progress value
                    ColorElevationObject->setProperty("progressValue",0);
                }
            }
            else
            {
                _reset();
                _handlePBStopClicked();
            }
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }

            CORE::RefPtr<APP::IGUI> gui = getGUIManager()->getGUIByClassname("SMCQt::ColorPaletteGUI");
            if (gui.valid())
            {
                VizQt::IQtGUI* qtGUI = gui->getInterface<VizQt::IQtGUI>();
                QObject::connect(dynamic_cast<VizQt::QtGUI*>(qtGUI), SIGNAL(newColorMapAdded(const QString&)),
                    this, SLOT(_hadleNewColorMapAdded(const QString&)));
            }
            else
            {
                QObject::disconnect(this, SLOT(_hadleNewColorMapAdded(const QString&)));
            }

            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ColorElevationGUI::_handleProgressValueChanged(int value)
    {
        QObject* ColorElevationObject = _findChild(ColorEleAnalysisPopup);
        if(ColorElevationObject)
        {
            ColorElevationObject->setProperty("progressValue",QVariant::fromValue(value));
        } 
    }

    //! 
    void ColorElevationGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        //get name of colorMap
        QObject* ColorElevationObject = _findChild(ColorEleAnalysisPopup);
        if(ColorElevationObject)
        {
            std::string name = ColorElevationObject->property("outputName").toString().toStdString();

            if(name == "")
            {
                showError( "Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            ColorElevationObject->setProperty("outputName",QString::fromStdString(name));
            _uiHandler->setOutputName(name);
            int uid = ColorElevationObject->property("colorMapSelectedUid").toString().toInt();
            std::string mapName = ColorElevationObject->property("colorMapName").toString().toStdString();
            if (_colorUIHandler)
            {
                CORE::RefPtr<ELEMENTS::IColorMap> colorMap =_colorUIHandler->getColorMap(mapName);
                if (uid == 0)
                {
                    showError("Insufficient / Incorrect data", "Color map is not selected, select a  valid color map");
                    return;
                }
                if (uid > 0)
                {
                   // _uiHandler->setColorMapType(static_cast<vizElements::IColorMap::COLORMAP_TYPE>(uid));
                    _uiHandler->setColorMap(colorMap.get());
                }
                else
                {
                    showError("Insufficient / Incorrect data", "The selected color map is not valid");
                    return;
                }
            }
            //Map Step Size
            bool advanceOptionClicked = ColorElevationObject->property("isAdvanceOptionClicked").toBool();
            if (advanceOptionClicked)
            {
                int stepSizeVal = ColorElevationObject->property("stepSizeVal").toInt();

                if (stepSizeVal <= 0)
                {
                    showError("Insufficient / Incorrect data", "Map Size is not correct");
                    return;
                }
                _uiHandler->setMapSize(stepSizeVal);
            }
         


            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = ColorElevationObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = ColorElevationObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = ColorElevationObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() =  ColorElevationObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError( "Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
                        showError( "Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                   

                }
            }

            //RFE2
#ifdef SLOPE_ASPECT_ENABLE_ELEVATION_LAYER
            // set elevation object
            {
                TERRAIN::IElevationObject* elevObj = _getCurrentElevationObject();

                if(!elevObj)
                {
                    showError( "Elevation Object", "Select an elevation object");
                    return;
                }

                // check for validity of extents
                CORE::IGeoExtent* gExtent = elevObj->getInterface<CORE::IGeoExtent>();

                if(gExtent)
                {
                    osg::Vec4d elevExtents;
                    if(gExtent->getExtents(elevExtents))
                    {
                        if(extents.x() < elevExtents.x() || extents.y() < elevExtents.y() ||
                            extents.z() > elevExtents.z() || extents.w() > elevExtents.w())
                        {
                            showError( "Area Extents", "Specified area extent is "
                                "outside of selected elevation object");

                            return;
                        }
                    }
                }

                _uiHandler->setElevationObject(elevObj);
            }
#endif

            ColorElevationObject->setProperty("progressValue",0); 
            _handlePBAreaClicked(false);
            _uiHandler->computeMinMaxElevation();
            //wait untill response not get 
           // _uiHandler->execute();
        }
    }

    void ColorElevationGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();
        QObject* ColorElevationObject = _findChild(ColorEleAnalysisPopup);
        if(ColorElevationObject)
        {
            //disable stop button
            ColorElevationObject->setProperty("isStopButtonEnabled",false);
            ColorElevationObject->setProperty("isStartButtonEnabled",true);
            ColorElevationObject->setProperty("progressValue",QVariant::fromValue(0));
        } 
    }

    void ColorElevationGUI::_handlePBRunInBackgroundClicked()
    {}

    void ColorElevationGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            
            QObject* ColorElevationObject = _findChild(ColorEleAnalysisPopup);
            if(ColorElevationObject)
            {
                ColorElevationObject->setProperty( "isMarkSelected" , false );
            }
        }
    }

    void ColorElevationGUI::_hadleNewColorMapAdded(const QString& text)
    {
        _populateColorMap();
    }

    void ColorElevationGUI::_reset()
    {
        QObject* ColorElevationObject = _findChild(ColorEleAnalysisPopup);
        if(ColorElevationObject)
        {
            ColorElevationObject->setProperty("isMarkSelected",false);
            ColorElevationObject->setProperty("bottomLeftLongitude","longitude");
            ColorElevationObject->setProperty("bottomLeftLatitude","latitude");
            ColorElevationObject->setProperty("topRightLongitude","longitude");
            ColorElevationObject->setProperty("topRightLatitude","latitude");
            ColorElevationObject->setProperty("outputName","outputName");
            ColorElevationObject->setProperty("progressValue",0);
            ColorElevationObject->setProperty("stepSizeVal", 100);
            ColorElevationObject->setProperty("isAdvanceOptionClicked", false);
            _handlePBAreaClicked(false);
        }
    }

    void ColorElevationGUI::_populateColorMap()
    {
        APP::IGUIManager* guiMgr = getGUIManager();
        CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(guiMgr);
        if(!wmain)
        {
            return;
        }
        CORE::IComponent* comp = wmain->getComponentByName("ColorMapComponent");
        if(!comp)
        {
            return;
        }
        ELEMENTS::IColorMapComponent* colorMapComponent = comp->getInterface<ELEMENTS::IColorMapComponent>();
        const ELEMENTS::IColorMapComponent::ColorMapList& list = colorMapComponent->getColorMapList();
        QList<QObject*> colorMapList;
        //temporary Comment only One Color map Available
        colorMapList.append(new VizComboBoxElement("Create New Color Map","0"));
        //colorMapList.append(new VizComboBoxElement("Red-Green color map", "1"));
        int uid = 1;
        ELEMENTS::IColorMapComponent::ColorMapList::const_iterator iter = list.begin();
        for(; iter != list.end(); ++iter)
        {
            colorMapList.append(new VizComboBoxElement(QString::fromStdString(iter->first),QString::number(uid)));
            uid++;
        }
        _setContextProperty("colorMapListModel", QVariant::fromValue(colorMapList));

    }

}//namespace SMCQt
