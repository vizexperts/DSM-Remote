#include<SMCQt/StyleTemplateGUI.h>

#include <Util/FileUtils.h>
#include<Util/ShpFileUtils.h>
#include <SMCQt/VizComboBoxElement.h>
#include <serialization/SerializationUtils.h>
#include<Util/StyleFileUtil.h>
#include <Core/InterfaceUtils.h>
#include <Terrain/IModelObject.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <SMCUI/IAddContentUIHandler.h>
#include <Core/IFeatureObject.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, StyleTemplateGUI);

    const std::string StyleTemplateGUI::MODELLAYERSTYLEPOPUP = "modelLayerStylePopup";
    const std::string StyleTemplateGUI::ITEMRELOADPOPUP = "reloadObjectPopup";
    const std::string StyleTemplateGUI::TEMPLATELISTPOPUP = "templateListPopup";
    std::string StyleTemplateGUI::_currentSelectedJsonFile;

    void StyleTemplateGUI::populateStyleTemplateList()
    {

        QObject* templateListPopup = _findChild(TEMPLATELISTPOPUP);
        if (templateListPopup != NULL)
        {
            QObject* styleDirTreeView = _findChild("styleDirTreeView");

            if (styleDirTreeView != NULL)
            {
                QObject::connect(styleDirTreeView, SIGNAL(clicked(int)), this,
                    SLOT(jsonFileSelected(int)), Qt::UniqueConnection);
            }

			//! Also set the style json schema path
			std::string jsonSchemaFile = "../../data/Styles/StyleSchema.json";
			QFileInfo schemafileInfo(jsonSchemaFile.c_str());
            
            QString strFilePath = "file:/" + schemafileInfo.absoluteFilePath();
            templateListPopup->setProperty("jsonSchemaFile", QVariant::fromValue(strFilePath));

        }
        QObject* modelLayerStylePopup = _findChild(MODELLAYERSTYLEPOPUP);
        if (modelLayerStylePopup != NULL)
        {
            QObject* styleReloadDirTreeView = _findChild("styleReloadDirTreeView");

            if (styleReloadDirTreeView != NULL)
            {
                QObject::connect(styleReloadDirTreeView, SIGNAL(clicked(int)), this,
                    SLOT(jsonFileSelected(int)), Qt::UniqueConnection);
            }
        }

        std::string modelAppDataDirPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "\\Styles";
        QDir modelAppDataDir(QString::fromStdString(modelAppDataDirPath));


        _qmlDirectoryStyleTreeModel = new QMLDirectoryTreeModel;

        QFileInfo fileInfo(modelAppDataDir.absolutePath());
        _qmlDirectoryStyleTreeModelRootItem = new QMLDirectoryTreeModelItem(fileInfo);
        _qmlDirectoryStyleTreeModelRootItem->setCheckable(false);

        _qmlDirectoryStyleTreeModel->clear();
        _qmlDirectoryStyleTreeModelRootItem->clear();
        //_qmlCategoriesList.clear();

        _qmlDirectoryStyleTreeModel->addItem(_qmlDirectoryStyleTreeModelRootItem);

        _populateDir(modelAppDataDir, _qmlDirectoryStyleTreeModelRootItem);

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {
            _contextPropertiesSet.push_back("StyleListModel");
            qtapp->getRootContext()->setContextProperty("StyleListModel", _qmlDirectoryStyleTreeModel);
            //_setContextProperty("StyleCategoryList", QVariant::fromValue(_qmlCategoriesList));
        }    
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
        const CORE::ComponentMap& cmap = wmain->getComponentMap();
        _selectionComponent = _selectionComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
        const CORE::ISelectionComponent::SelectionMap& selectionMap = _selectionComponent->getCurrentSelection();

        if (!selectionMap.empty())
        {
            CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
            _currentSelection = selectable;


            CORE::RefPtr<TERRAIN::IModelObject> modelObject = _currentSelection->getInterface<TERRAIN::IModelObject>();
            if (modelObject!=NULL)
            {
                std::string setDefaltuStyleAs;

                switch (modelObject->getGeometryType())
                {
                case UTIL::ShpFileUtils::GeometryType::LINE:
                    setDefaltuStyleAs = "Set as Default Line Style";
                    break;

                case UTIL::ShpFileUtils::GeometryType::POINT:
                    setDefaltuStyleAs = "Set as Default Point Style";

                    break;

                case UTIL::ShpFileUtils::GeometryType::POLYGON:
                    setDefaltuStyleAs = "Set as Default Polygon Style";

                    break;
                }
                QObject* templatelistPopup = _findChild(TEMPLATELISTPOPUP);
                if (templatelistPopup != NULL)
                {
                    templateListPopup->setProperty("setDefaltuStyleAs", QString::fromStdString(setDefaltuStyleAs));
                }
            }
        }
    }

    void StyleTemplateGUI::populateModelList()
    {
        QObject* templateListPopup = _findChild(TEMPLATELISTPOPUP);
        if (templateListPopup != NULL)
        {
            QObject* modelDirTreeView = _findChild("modelDirTreeView");

            if (modelDirTreeView != NULL)
            {
                QObject::connect(modelDirTreeView, SIGNAL(clicked(int)), this,
                    SLOT(modelFileSelected(int)), Qt::UniqueConnection);
            }
        }

        std::string modelAppDataDirPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "\\data";
        QDir modelAppDataDir(QString::fromStdString(modelAppDataDirPath));


        _qmlDirectoryFileTreeModel = new QMLDirectoryTreeModel;
        _qmlDirectoryFileTreeModel->showExtensions(true);

        QFileInfo fileInfo(modelAppDataDir.absolutePath());
        _qmlDirectoryFileTreeModelRootItem = new QMLDirectoryTreeModelItem(fileInfo);
        _qmlDirectoryFileTreeModelRootItem->setCheckable(false);

        _qmlDirectoryFileTreeModelRootItem->clear();
        _qmlDirectoryFileTreeModelRootItem->clear();
        //_qmlCategoriesList.clear();

        _qmlDirectoryFileTreeModel->addItem(_qmlDirectoryFileTreeModelRootItem);

        _populateModelDir(modelAppDataDir, _qmlDirectoryFileTreeModelRootItem);

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtapp->getRootContext())
        {
            _contextPropertiesSet.push_back("modelList");
            qtapp->getRootContext()->setContextProperty("modelList", _qmlDirectoryFileTreeModel);
        }

    }

    void StyleTemplateGUI::jsonFileSelected(int index)
    {
        if (index < 0)
            return;

        if (index >= _qmlDirectoryStyleTreeModel->rowCount())
            return;


        QMLDirectoryTreeModelItem* item = dynamic_cast<QMLDirectoryTreeModelItem*>(_qmlDirectoryStyleTreeModel->getItem(index));

        if (item != NULL)
        {
            QFileInfo currentSelectedFile = item->getFileInfo();

            if (currentSelectedFile.isFile())
            {
                QString qAbsolutePath = currentSelectedFile.absoluteFilePath();
                std::string absolutePath = qAbsolutePath.toStdString();
                _currentSelectedJsonFile = absolutePath;
                std::string fileName = osgDB::getSimpleFileName(absolutePath);
                QString filePath = currentSelectedFile.filePath();
                std::string localFilePath = filePath.toStdString();

                //parse the json 
                std::ifstream inFile(absolutePath.c_str());
                if (!inFile.is_open())
                    return;

                //read content from file into a string stream 
                std::ostringstream buffer;
                buffer << inFile.rdbuf();

                std::string jsonContent = buffer.str();

                QObject* templatelistPopup = _findChild(TEMPLATELISTPOPUP);
                if (templatelistPopup != NULL)
                {
                    templatelistPopup->setProperty("jsonString", QString::fromStdString(jsonContent));
                    templatelistPopup->setProperty("jsonFileName", QString::fromStdString(fileName));
                }

            }
        }
    }

    void StyleTemplateGUI::modelFileSelected(int index)
    {
        if (index < 0)
            return;

        if (index >= _qmlDirectoryFileTreeModel->rowCount())
            return;

        QMLDirectoryTreeModelItem* item = dynamic_cast<QMLDirectoryTreeModelItem*>(_qmlDirectoryFileTreeModel->getItem(index));

        if (item != NULL)
        {
            QFileInfo currentSelectedFile = item->getFileInfo();

            if (currentSelectedFile.isFile())
            {
                QString qAbsolutePath = currentSelectedFile.absoluteFilePath();

                std::string modelAppDataDirPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + "\\data";
                QDir modelAppDataDir(QString::fromStdString(modelAppDataDirPath));

                QString relatievePath = modelAppDataDir.relativeFilePath(qAbsolutePath);

                QObject* modelDirTreeView = _findChild("modelDirTreeView");
                if (modelDirTreeView != NULL)
                {
                    modelDirTreeView->setProperty("selectedFileRelativePath", QVariant::fromValue(relatievePath));
                }
            }
        }
    }
    bool StyleTemplateGUI::_populateDir(QDir dir, QMLDirectoryTreeModelItem* parentItem)
    {
        if (dir.exists())
        {
            dir.setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs);
            dir.setSorting(QDir::DirsFirst);

            QStringList filters;
            filters << "*.json";
            dir.setNameFilters(filters);


            QFileInfoList list = dir.entryInfoList();

            for (int i = 0; i < list.size(); ++i)
            {
                QFileInfo fileInfo = list.at(i);
                QMLDirectoryTreeModelItem* item = new QMLDirectoryTreeModelItem(fileInfo);

                std::string path = fileInfo.absoluteFilePath().toStdString();

                //not to show folder/files with .txt in them

                if (path.substr(path.find_last_of(".") + 1) == "txt")
                    return false;


                item->setCheckable(false);

                if (parentItem != NULL)
                    parentItem->addItem(item);
                else
                    _qmlDirectoryStyleTreeModel->addItem(item);

                if (fileInfo.isDir())
                {
                    if (!_populateDir(QDir(fileInfo.absoluteFilePath()), item))
                        _qmlDirectoryStyleTreeModel->removeItem(item);
                }
            }
        }
        return true;
    }


    bool StyleTemplateGUI::_populateModelDir(QDir dir, QMLDirectoryTreeModelItem* parentItem)
    {
        if (dir.exists())
        {
            dir.setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::AllDirs);
            dir.setSorting(QDir::DirsFirst);

            QStringList filters;
            filters << "*.ive" << "*.png";
            dir.setNameFilters(filters);


            QFileInfoList list = dir.entryInfoList();

            for (int i = 0; i < list.size(); ++i)
            {
                QFileInfo fileInfo = list.at(i);
                QMLDirectoryTreeModelItem* item = new QMLDirectoryTreeModelItem(fileInfo);

                std::string path = fileInfo.absoluteFilePath().toStdString();

                //not to show folder/files with .txt in them

                //if (path.substr(path.find_last_of(".") + 1) == "txt")
                //    return false;


                item->setCheckable(false);

                if (parentItem != NULL)
                    parentItem->addItem(item);
                else
                    _qmlDirectoryFileTreeModel->addItem(item);

                if (fileInfo.isDir())
                {
                    if (!_populateModelDir(QDir(fileInfo.absoluteFilePath()), item))
                        _qmlDirectoryFileTreeModel->removeItem(item);
                }
            }
        }
        return true;
    }

    void StyleTemplateGUI::popupClosed()
    {
        //! unassign all the context properties
        auto contextPropertyIterator = _contextPropertiesSet.begin();
        while (contextPropertyIterator != _contextPropertiesSet.end())
        {
            _setContextProperty(*contextPropertyIterator, NULL);
            contextPropertyIterator++;
        }

        _contextPropertiesSet.clear();
    }

    bool StyleTemplateGUI::populateEnumList(QString name, QString parentName, QString value)
    {
        std::vector<std::string> strVector;
        std::string nameString = name.toStdString();
        std::string parentString = parentName.toStdString();
        std::string valueString = value.toStdString();

        DB::readEnumsValuesFromSymbol(parentString, nameString, strVector);

        //!  If str vector is empty this means that this is not a valid Enum, return false so that it can be treated as a string expression
        if (strVector.empty())
            return false;

        //Find index of valuestring
        int stringIndex = 0;
        for (int i = 0; i < strVector.size(); i++)
        {
            if (strVector[i] == valueString)
            {
                stringIndex = i;
                break;
            }
        }

        //bring the string to first location
        if (stringIndex != 0)
        {
            strVector[stringIndex] = strVector[0];
            strVector[0] = valueString;
        }

        //populate the list of templates
        QList<QObject*> styleSheetBoxList;

        for (int i = 0; i < strVector.size(); i++)
        {
            styleSheetBoxList.append(new VizComboBoxElement(strVector[i].c_str(), UTIL::ToString(i).c_str()));
        }

        //! 
        _contextPropertiesSet.push_back(nameString);
        _setContextProperty(nameString, QVariant::fromValue(styleSheetBoxList));

        return true;
    }

    void StyleTemplateGUI::setJsonEditorOutput()
    {
        std::ofstream outfile(_currentSelectedJsonFile.c_str());

        QObject* templatelistPopup = _findChild(TEMPLATELISTPOPUP);
        if (templatelistPopup != NULL)
        {
            std::string outString = templatelistPopup->property("jsonOutput").toString().toStdString();
            outfile << outString;
        }

        outfile.close();
    }

    void StyleTemplateGUI::handleSaveAsButtonClicked(QString fileName)
    {
        if (fileName.isEmpty())
        {
            emit showError(("SaveAs Style Error"), "Give a file Name");
            return;
        }

        std::string fileNameString = fileName.toStdString();
        std::string fileExtention = osgDB::getFileExtension(fileNameString);

        if (fileExtention != "json")
        {
            emit showError(("SaveAs Style Error"), "Not a valid Extension (only .json supported)");
            return;
        }

        //remove folder paths from file name - It would be saved to the current json file being edited 
        fileNameString = osgDB::getSimpleFileName(fileNameString);

        std::string filePath = osgDB::getFilePath(_currentSelectedJsonFile);
        std::string currentFileName = osgDB::getSimpleFileName(_currentSelectedJsonFile);

        if (currentFileName == fileNameString)
        {
            emit showError(("SaveAs Style Error"), "Overwrite not Allowed(Change File name or Press Save) ");
            return;
        }

        //append file path to file name 
        fileNameString = filePath + "/" + fileNameString;

        std::ofstream outfile(fileNameString.c_str());

        QObject* templatelistPopup = _findChild(TEMPLATELISTPOPUP);
        if (templatelistPopup != NULL)
        {
            std::string outString = templatelistPopup->property("jsonOutput").toString().toStdString();
            outfile << outString;
        }

        outfile.close();
    }
    
    void StyleTemplateGUI::setDefaultModelObject()
    {
        CORE::RefPtr<TERRAIN::IModelObject> modelObject = _currentSelection->getInterface<TERRAIN::IModelObject>();
        //std::string filePath = modelObject->getUrl();
        //UTIL::ShpFileUtils::GeometryType geometryType = modelObject->getGeometryType();
        switch (modelObject->getGeometryType())
        {
        case UTIL::ShpFileUtils::GeometryType::LINE:
            osgDB::copyFile(_currentSelectedJsonFile, UTIL::StyleFileUtil::getDefaultLineStyle());
            break;

        case UTIL::ShpFileUtils::GeometryType::POINT:
            osgDB::copyFile(_currentSelectedJsonFile, UTIL::StyleFileUtil::getDefaultPointStyle());
            break;

        case UTIL::ShpFileUtils::GeometryType::POLYGON:
            osgDB::copyFile(_currentSelectedJsonFile, UTIL::StyleFileUtil::getDefaultPolygonStyle());
            break;
        }
    }
    void StyleTemplateGUI::updateModelObject()
    {
        TERRAIN::IModelObject* modelObject = _currentSelection->getInterface<TERRAIN::IModelObject>();
        if (modelObject != NULL)
        {
            CORE::IObject* object = dynamic_cast<CORE::IObject*>(modelObject);
            SMCUI::IAddContentUIHandler* _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());
            _addContentUIHandler->updateModelObject(object, _currentSelectedJsonFile);
        }

        CORE::IFeatureObject *featureObject = _currentSelection->getInterface<CORE::IFeatureObject>();
        if (featureObject != NULL)
        {
            CORE::IObject* object = dynamic_cast<CORE::IObject*>(featureObject);
            std::string uniqueID = object->getInterface<CORE::IBase>(true)->getUniqueID().toString();
            std::string objectFolderPath = UTIL::StyleFileUtil::getObjectFolder();
            std::string jsonFile = objectFolderPath +"\\"+ uniqueID + ".json";
            osgDB::copyFile(_currentSelectedJsonFile, jsonFile);
            SMCUI::IAddContentUIHandler* _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());
            _addContentUIHandler->updateMarkingsObject(object, jsonFile);
        }
    }
}