

/****************************************************************************
*
* File             : AddWebDataGUI.h
* Description      : AddWebDataGUI class definition
*
*****************************************************************************
* Copyright (c) 2010-2011, VizExperts india Pvt. Ltd.                                       
*****************************************************************************/

#include <SMCQt/AddWebDataGUI.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <VizQt/NameValidator.h>
#include <SMCUI/IAddContentStatusMessage.h>
#include <SMCUI/AddContentUIHandler.h>

#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>
#include <Core/IWorld.h>
#include <Core/IObjectMessage.h>

#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/ApplicationRegistry.h>

#include <Elements/IMilitarySymbolTypeLoader.h>

#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>

#include <Util/Log.h>

#include <VizQt/GUI.h>

#include <sstream>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, AddWebDataGUI)
        DEFINE_IREFERENCED(AddWebDataGUI, DeclarativeFileGUI)

        void AddWebDataGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }
    }



    AddWebDataGUI::AddWebDataGUI()
    {

    }

    AddWebDataGUI::~AddWebDataGUI()
    {

    }

    void AddWebDataGUI::initialize()
    {
        _loadAddContentUIHandler();
    }

    void AddWebDataGUI::_handleYahooCBClicked(bool value)
    {
        //change datasetOption to "satellite" if u want to load yahoo satellite imagery
        std::string datasetOption = "map";
        if(value)
        {
            try
            {
                _addContentUIHandler->addYahooWebData(datasetOption);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();

                // Show error for raster data
                std::string title = "Add YahooData Error";
                std::string text = "Failed to load Yahoo Data, check console for more info";
                showError(title.c_str(), text.c_str());
                return;
            }
        }
        else
        {
            _addContentUIHandler->removeYahooWebData();
        }
    }

    void AddWebDataGUI::_handleBingCBClicked(bool value)
    {
        //change datasetOption to "satellite" if u want to load yahoo satellite imagery
        std::string imagerySet = "Aerial";
        if(value    )
        {
            try
            {
                _addContentUIHandler->addBingWebData(imagerySet);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();

                // Show error for raster data
                std::string title = "Add BingData Error";
                std::string text = "Failed to load Bing Data, check console for more info";
                showError( title.c_str(), text.c_str());
                return;
            }
        }
        else
        {
            _addContentUIHandler->removeBingWebData();
        }
    }


    void AddWebDataGUI::_handleOSMCBClicked(bool value)
    {
        std::string osmURL = "http://a.tile.openstreetmap.org/{z}/{x}/{y}.png";
        if(value)
        {
            try
            {
                _addContentUIHandler->addOSMWebData(osmURL);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();

                // Show error for raster data
                std::string title = "Add OSMData Error";
                std::string text = "Failed to load OSM Data, check console for more info";
                showError(title.c_str(), text.c_str());
                return;
            }
        }
        else
        {
            _addContentUIHandler->removeOSMWebData();
        }
    }

    void AddWebDataGUI::_handleWorldWindCBClicked(bool value)
    {
        //The following url loads Nasa data of some band but not proper 
        //std::string nasaURL = "http://www.nasa.network.com/wms";

        // Open the given link in QGIS WMS to get the name of all the layers present in this WMS service
        // and update the layer name in DBPlugins/ReaderWriterWeb.cpp to get that layer
        std::string nasaURL = "http://www.nasa.network.com/wms";
        if(value)
        {
            try
            {
                _addContentUIHandler->addWorldWindWebData(nasaURL);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();

                // Show error for raster data
                std::string title = "Add NasaData Error";
                std::string text = "Failed to load Nasa Data, check console for more info";
                showError(title.c_str(), text.c_str());
                return;
            }
        }
        else
        {
            _addContentUIHandler->removeWorldWindWebData();
        }
    }


    // ( Google Map and GoogleMap doesnot show in parallel , they are unique and One can ne Loaded at a time , See GoogleMaps)
    void 
        AddWebDataGUI::_handleGoogleStreetCBClicked(bool value)
    {
        if(value)
        {
            try
            {
                //_cbGoogleHybrid->setChecked(false);
                //_addContentUIHandler->removeGoogleData(SMCUI::IAddContentUIHandler::GOOGLE_HYBRID);

                if(!_addContentUIHandler->addGoogleData(SMCUI::IAddContentUIHandler::GOOGLE_STREETS))
                {
                    std::string title = "Add Google Data Error";
                    std::string text = "Failed to load Google Streets Data, check console for more info";
                    showError(title.c_str(), text.c_str());
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();

                // Show error for raster data
                std::string title = "Add Google Data Error";
                std::string text = "Failed to load Google Streets Data, check console for more info";
                showError(title.c_str(), text.c_str());
                return;
            }
        }
        else
        {
            _addContentUIHandler->removeGoogleData(SMCUI::IAddContentUIHandler::GOOGLE_STREETS);
        }
    }

    void 
        AddWebDataGUI::_handleGoogleHybridCBClicked(bool value)
    {
        if(value)
        {
            try
            {
                //_cbGoogleStreets->setChecked(false);
                //_addContentUIHandler->removeGoogleData(SMCUI::IAddContentUIHandler::GOOGLE_STREETS);

                if(!_addContentUIHandler->addGoogleData(SMCUI::IAddContentUIHandler::GOOGLE_HYBRID))
                {
                    std::string title = "Add Google Data Error";
                    std::string text = "Failed to load Google Hybrid Data, check console for more info";
                    showError(title.c_str(), text.c_str());
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();

                // Show error for raster data
                std::string title = "Add Google Data Error";
                std::string text = "Failed to load Google Hybrid Data, check console for more info";
                showError(title.c_str(), text.c_str());
                return;
            }
        }
        else
        {
            _addContentUIHandler->removeGoogleData(SMCUI::IAddContentUIHandler::GOOGLE_HYBRID);
        }
    }



    void AddWebDataGUI::_loadAddContentUIHandler()
    {
        _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
    }

    void AddWebDataGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded

        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Set the NetworkCollborationManager
            try
            { 
                initialize();
            }
            catch(...){}
        }
        else
        {

        }
    }

    void AddWebDataGUI::connectSmpMenu(QString type)
    {
        if(type == "addWebData")
        {
            QObject* webDataMenu = _findChild("addWebData");

            if(webDataMenu != NULL)
            {
                connect(webDataMenu, SIGNAL(addYahooMaps(bool)), this, SLOT(_handleYahooCBClicked(bool)));
                connect(webDataMenu, SIGNAL(addBingMaps(bool)), this, SLOT(_handleBingCBClicked(bool)));
                connect(webDataMenu, SIGNAL(addOSMaps(bool)), this, SLOT(_handleOSMCBClicked(bool)));
                connect(webDataMenu, SIGNAL(addNASAWW(bool)), this, SLOT(_handleWorldWindCBClicked(bool)));
                connect(webDataMenu, SIGNAL(addGoogleStreets(bool)), this, SLOT(_handleGoogleStreetCBClicked(bool)));
                connect(webDataMenu, SIGNAL(addGoogleHybrid(bool)), this, SLOT(_handleGoogleHybridCBClicked(bool)));

                // setting the already existing rasters info in "addWebDataMenu.selected" property
                CORE::RefPtr<CORE::IWorld> iworld = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
                CORE::RefPtr<CORE::IObject> iobject;
                QVariant seltag;

                int existing_layers = 0;

                iobject = iworld->getObjectByName("YahooData");
                webDataMenu->setProperty("yahooSelected", QVariant::fromValue(iobject.valid()));

                iobject = iworld->getObjectByName("BingData");
                webDataMenu->setProperty("bingSelected", QVariant::fromValue(iobject.valid()));

                iobject = iworld->getObjectByName("OSMData");
                webDataMenu->setProperty("opsSelected", QVariant::fromValue(iobject.valid()));

                iobject = iworld->getObjectByName("WorldWindData");
                webDataMenu->setProperty("worldWindSelected", QVariant::fromValue(iobject.valid()));

                iobject = iworld->getObjectByName("Google_Streets");
                webDataMenu->setProperty("googleStreetSelected", QVariant::fromValue(iobject.valid()));

                iobject = iworld->getObjectByName("Google_Hybrid");
                webDataMenu->setProperty("googleHybridSelected", QVariant::fromValue(iobject.valid()));

                QMetaObject::invokeMethod(webDataMenu, "syncModel");
            }
        }
    }
}
