/*****************************************************************************
*
* File             : CVAGUI.cpp
* Description      : CVAGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/


#include <SMCQt/CVAGUI.h>

#include <osgDB/FileNameUtils>
#include <osg/Vec4>

#include <Core/CoreRegistry.h>
#include <Core/ITerrain.h>
#include <Core/WorldMaintainer.h>
#include <Core/IComponent.h>
#include <Core/IVisitorFactory.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IFilterStatusMessage.h>

#include <SMCUI/AddContentUIHandler.h>

#include <App/AccessElementUtils.h>
#include <App/UIHandlerFactory.h>
#include <App/IUIHandlerManager.h>
#include <App/ApplicationRegistry.h>

#include <VizUI/UIHandlerType.h>
#include <VizUI/UIHandlerManager.h>

#include <Util/Log.h>
#include <Util/StringUtils.h>

#include <VizQt/GUI.h>

#include <QFileDialog>

#include <QtCore/QFile>

#include <QtUiTools/QtUiTools>


#include <QLabel>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QMainWindow>

#include <iostream>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CVAGUI);
    DEFINE_IREFERENCED(CVAGUI,CORE::Base);

    const std::string CVAGUI::ChangeDetectionPopup = "changeDetection";

    CVAGUI::CVAGUI()        
        :_enhancementType(Stereo::Image::NONE)
        ,_stereoWizard(NULL)
    {
    }

    CVAGUI::~CVAGUI()
    {
        setActive( false );
    }

    void CVAGUI::_loadAddContentUIHandler()
    {
        _addContentUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
    }

    // to load the connect and disconnect slot from the qml
    void CVAGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_updateGUIStatus()),this, SLOT(_handleGUIStatus()),Qt::QueuedConnection);
    }

    void CVAGUI::setActive(bool value)
    {
        try
        {            
            _reset();
            if(value)
            {               
                QObject* cvaObject = _findChild(ChangeDetectionPopup);

                // connect the signal of various buttons in CVA
                if(cvaObject)
                {
                    QObject::connect( cvaObject, SIGNAL(browseInput1Image()), 
                        this, SLOT(_handleInput1BrowseButtonClicked()), Qt::UniqueConnection);

                    QObject::connect( cvaObject, SIGNAL(browseInput2Image()), 
                        this, SLOT(_handleInput2BrowseButtonClicked()), Qt::UniqueConnection);

                    QObject::connect( cvaObject, SIGNAL(browseDistanceImage()), 
                        this, SLOT(_handleDistanceImageButtonClicked()), Qt::UniqueConnection);

                    QObject::connect( cvaObject, SIGNAL(browseAngleImage()), 
                        this, SLOT(_handleAngleImageButtonClicked()), Qt::UniqueConnection);

                    QObject::connect( cvaObject, SIGNAL(viewImage(QString)), 
                        this,SLOT(_viewImage(QString)), Qt::QueuedConnection);

                    QObject::connect( cvaObject, SIGNAL(calculate()), 
                        this, SLOT(_handleStartButtonClicked()), Qt::UniqueConnection);
                    QObject::connect( cvaObject, SIGNAL(stopButtonClicked()), 
                        this,SLOT(_handleStopButtonClicked()), Qt::UniqueConnection);
                }
            }
            else
            {                
                _handleStopButtonClicked();

            }
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void CVAGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            { 
                _loadAddContentUIHandler();
            }
            catch(...)
            {}
        }
        // to check the filter status
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                emit _updateGUIStatus(); 
                emit showError("Filter Status", "Successfully applied filter", false);
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
            {
                QObject* cvaObject = _findChild(ChangeDetectionPopup);
                if(cvaObject)
                {
                    cvaObject->setProperty("startButtonEnabled",true);
                    cvaObject->setProperty("stopButtonEnabled",false);
                    cvaObject->setProperty("calculating",false);
                }
                emit showError("Filter Status", "Failed to apply filter");
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }

    // to browse the input1 image
    void CVAGUI::_handleInput1BrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        std::string formatFilter = "Image (*.jpg *.jpeg *.bmp *.png *.tif *.tiff)";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {
            QObject* cvaObject = _findChild(ChangeDetectionPopup);
            if(cvaObject != NULL)
            {
                cvaObject->setProperty("input1ImagePath",QVariant::fromValue(filename));
                cvaObject->setProperty("input1ButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }
    }


    // to browse the input2 image
    void CVAGUI::_handleInput2BrowseButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        std::string formatFilter = "Image (*.jpg *.jpeg *.bmp *.png *.tif *.tiff)";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getOpenFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {
            QObject* cvaObject = _findChild(ChangeDetectionPopup);
            if(cvaObject != NULL)
            {
                cvaObject->setProperty("input2ImagePath",QVariant::fromValue(filename));
                cvaObject->setProperty("input2ButtonEnabled",true);
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }
    }  


    // to browse and save distance image 
    void CVAGUI::_handleDistanceImageButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        std::string formatFilter = "Image (*.jpg *.jpeg *.bmp *.png *.tif *.tiff)";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getSaveFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {
            QObject* cvaObject = _findChild(ChangeDetectionPopup);
            if(cvaObject != NULL)
            {
                cvaObject->setProperty("outputDistance",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }
    }  


    // to browse and save angle image
    void CVAGUI::_handleAngleImageButtonClicked()
    {
        std::string directory = "/home";
        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        std::string formatFilter = "Image (*.jpg *.jpeg *.bmp *.png *.tif *.tiff)";
        std::string label = std::string("Image files");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString filename = QFileDialog::getSaveFileName(parent,
            label.c_str(),
            _lastPath.c_str(),
            formatFilter.c_str(), 0);

        if(!filename.isEmpty())
        {
            QObject* cvaObject = _findChild(ChangeDetectionPopup);
            if(cvaObject != NULL)
            {
                cvaObject->setProperty("outputAngle",QVariant::fromValue(filename));
            }
            try
            {
                directory = osgDB::getFilePath(filename.toStdString());
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }

            catch(...)
            {
                emit showError("Error in Reading File", "Unknown Error", true);
            }
        }
    }  

    void CVAGUI::closeStereoWizard(Stereo::MainWindowGUI* stereoWiz)
    {        
        if (stereoWiz)
        {
            delete stereoWiz;
            stereoWiz = NULL;
        }
    }

    // to view the specified image
    void CVAGUI::_viewImage(QString imagePath)
    { 
        std::string filename = imagePath.toStdString();
        if(filename != "")
        {                
            Stereo::MainWindowGUI* stereoWizard = new Stereo::MainWindowGUI(filename,_enhancementType);
            _stereoWizard.push_back( stereoWizard);
            QObject::connect(stereoWizard,SIGNAL(applicationClosed(Stereo::MainWindowGUI*)),
                this,SLOT(closeStereoWizard(Stereo::MainWindowGUI*)), Qt::QueuedConnection);
            stereoWizard->showMaximized();
            stereoWizard->setWindowTitle(filename.c_str());
        }
    }


    //function called when start button is clicked
    void CVAGUI::_handleStartButtonClicked()
    {
        QObject* cvaObject = _findChild(ChangeDetectionPopup);

        if(cvaObject)
        {
            // Pass the parameters to the visitor
            std::string distanceName = cvaObject->property("outputDistance").toString().toStdString();
            std::string angleName = cvaObject->property("outputAngle").toString().toStdString();

            if(distanceName == "" || angleName == "" )
            {
                showError("outputName", "Please enter output field");
                return;
            }

            std::string input1 = cvaObject->property("input1ImagePath").toString().toStdString();
            std::string input2 = cvaObject->property("input2ImagePath").toString().toStdString();
            if(input1 == "" || input2 == "" )
            {
                std::string title = "Add Image File Error";
                std::string text = "filename missing";

                showError(title.c_str(), text.c_str());
                return;
            }
            cvaObject->setProperty("startButtonEnabled",false);
            cvaObject->setProperty("stopButtonEnabled",true);            
            cvaObject->setProperty("calculating",true);
            cvaObject->setProperty("outputDButtonEnabled",false);
            cvaObject->setProperty("outputAButtonEnabled",false);


            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
            CORE::IBase* applyNode = terrain->getInterface<CORE::IBase>();

            if(!_cva.valid())
            {
                CORE::RefPtr<CORE::IVisitorFactory> vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();
                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::ChangeDetectionCVAType);
                _cva = _visitor->getInterface<GISCOMPUTE::IChangeDetectionCVA>();

            }

            _cva->setInput1ImageURL(input1);
            _cva->setInput2ImageURL(input2);
            _cva->setAngleImageURL(distanceName);
            _cva->setDistanceImageURL(angleName);



            _subscribe(_cva.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

            applyNode->accept(*(_cva->getInterface<CORE::IBaseVisitor>()));
        }


    }


    //function called when stop button is clicked    
    void CVAGUI::_handleStopButtonClicked()
    {        

        QObject* cvaObject = _findChild(ChangeDetectionPopup);

        if(cvaObject)
        {
            cvaObject->setProperty("startButtonEnabled",true);
            cvaObject->setProperty("stopButtonEnabled",false);
            cvaObject->setProperty("calculating",false);
            cvaObject->setProperty("outputDButtonEnabled",false);
            cvaObject->setProperty("outputAButtonEnabled",false);

        }

        // to stop the filter
        if(_visitor)
        {
            GISCOMPUTE::IFilterVisitor* iVisitor  = 
                _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            iVisitor->stopFilter();
        }


    }

    // updates GUI according to the filter status
    void CVAGUI::_handleGUIStatus()
    {
        QObject* cvaObject = _findChild(ChangeDetectionPopup);

        if(cvaObject)
        {
            cvaObject->setProperty("startButtonEnabled",true);
            cvaObject->setProperty("stopButtonEnabled",false);
            cvaObject->setProperty("calculating",false);
            cvaObject->setProperty("outputDButtonEnabled",true);
            cvaObject->setProperty("outputAButtonEnabled",true);

        }
    }


    void CVAGUI::_reset()
    {
        QObject* cvaObject = _findChild(ChangeDetectionPopup);

        if(cvaObject)
        {
            cvaObject->setProperty("input1ImagePath","");
            cvaObject->setProperty("input2ImagePath","");
            cvaObject->setProperty("outputDistance","");
            cvaObject->setProperty("outputAngle","");
            cvaObject->setProperty("startButtonEnabled",true);
            cvaObject->setProperty("stopButtonEnabled",false);
            cvaObject->setProperty("input1ButtonEnabled",false);
            cvaObject->setProperty("input2ButtonEnabled",false);
            cvaObject->setProperty("outputDButtonEnabled",false);
            cvaObject->setProperty("outputAButtonEnabled",false);


        }
    }

} // namespace SMCQt





