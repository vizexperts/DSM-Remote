#include<SMCQt\CreateOEFeatureGUI.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/PropertyObject.h>
#include <Util/FileUtils.h>
#include <Util/StringUtils.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>
#include <Util/StyleFileUtil.h>
#include <SMCUI/IAddContentUIHandler.h>
#include <SMCQt/VizComboBoxElement.h>
#include <serialization/SerializationUtils.h>



namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateOEFeatureGUI);
    CreateOEFeatureGUI::CreateOEFeatureGUI()
    {
        int a = 10; 
    }
    const std::string CreateOEFeatureGUI::FeatureContextualMenuObjectName = "featureContextual";
    void CreateOEFeatureGUI::handleFeatureContextualOpen()
    {
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->setMouseClickSelectionState(false);
        }

        _populateContextualMenu(); 
    }

    void CreateOEFeatureGUI::handleFeatureContextualClose()
    {
        //setting name to avoid tab update after close 
        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
            selectionUIHandler->setMouseClickSelectionState(true);
        }
    }

    void CreateOEFeatureGUI::_populateContextualMenu()
    {
        QObject* pointContextual = _findChild(FeatureContextualMenuObjectName);
        if (pointContextual != NULL)
        {
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if (selectionUIHandler.valid())
            {

                const CORE::ISelectionComponent::SelectionMap& selectionMap = selectionUIHandler->getCurrentSelection();

                if (selectionMap.empty())
                {
                    return;
                }

                CORE::ISelectable* selectable = selectionMap.begin()->second.get();
                
                if (selectable)
                {
                    oeFeature = selectable->getInterface<TERRAIN::IOEFeatureObject>();
                    
                    CORE::RefPtr<TERRAIN::IModelObject> modelObject = oeFeature->getModelObject();
                    styleFilePath =  modelObject->getObjectStyleFile();
                    std::string jsonContent;
                    if (osgDB::fileExists(styleFilePath))
                    {
                        std::ifstream inFile(styleFilePath.c_str());
                        if (!inFile.is_open())
                            return;
                        jsonContent.assign((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());
                    }
                  
                    QObject *featureContextual = _findChild("featureContextual");
                    if (featureContextual)
                    {
                        featureContextual->setProperty("jsonString", QString::fromStdString(jsonContent));

                        std::string jsonSchemaFile = "../../data/Styles/StyleSchema.json";
                        QFileInfo schemafileInfo(jsonSchemaFile.c_str());
                        QString strFilePath = schemafileInfo.absoluteFilePath();
                        std::string schemaPath = strFilePath.toStdString();
                        std::ifstream schemaFile(schemaPath.c_str());
                        if (!schemaFile.is_open())
                            return;

                        std::string schemaData((std::istreambuf_iterator<char>(schemaFile)), std::istreambuf_iterator<char>());
                        featureContextual->setProperty("jsonSchemaContent", QString::fromStdString(schemaData));
                    }

                }

                if (oeFeature.valid())
                {
                    std::vector<std::pair<std::string, std::string>> featureAttributeList = oeFeature->getFeatureAttributeList();

                    QString fieldType = "String";
                    QString fieldValue;
                    int fieldPrecesion = 1;

                    if (featureAttributeList.size())
                    {
                        _imageList.clear();
                        _attributeList.clear(); 
                        _docList.clear();
                         _urlList.clear();

                        for (int i = 0; i < featureAttributeList.size(); i++)
                        {
                            std::string featureName = featureAttributeList[i].first;
                            std::string featureValue = featureAttributeList[i].second;
                            std::replace(featureValue.begin(), featureValue.end(), '\\', '/');

                            std::vector<std::string> hyperlinks; 

                            std::size_t found = featureName.find("hyp"); 
                            if ((found != std::string::npos) && featureValue.size())
                            {   
                                std::string delimeter = ";";

                                std::size_t pos = featureValue.find(delimeter.c_str());
                                UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(hyperlinks,
                                    featureValue, UTIL::IsDelimeter(';'));

                                for (int i = 0; i < hyperlinks.size(); i++)
                                {

                                    std::string fileExtention = osgDB::getFileExtension(hyperlinks[i]);

                                    // Checking for images / audio / videos resp
                                    // Checks are case insensitive
                                    if (boost::iequals(fileExtention, "jpg") || boost::iequals(fileExtention, "png") || boost::iequals(fileExtention, "jpeg") 
                                        || boost::iequals(fileExtention, "bmp") || boost::iequals(fileExtention, "mp3") || boost::iequals(fileExtention, "wav")
                                        || boost::iequals(fileExtention, "mp4") || boost::iequals(fileExtention, "avi")|| boost::iequals(fileExtention, "mpg")
                                        || boost::iequals(fileExtention, "mpeg") || boost::iequals(fileExtention, "mov") || boost::iequals(fileExtention, "flv")
                                        || boost::iequals(fileExtention, "wmv"))


                                    {
                                        if (boost::filesystem::exists(hyperlinks[i]))
                                        {
                                            _imageList.append(QString::fromStdString("file:///" + hyperlinks[i]));
                                        }
                                        else
                                        {
                                            std::string msg = "file: \"" + hyperlinks[i] + "\" not found";
                                            LOG_ERROR(msg.c_str());
                                        }
                                    }
                                    else if (hyperlinks[i].find("http") != std::string::npos)
                                    {
                                        _urlList.append(QString::fromStdString(hyperlinks[i]));
                                    }
                                    else if (hyperlinks[i] == "NULL")
                                    {
                                        //Do nothing
                                    }
                                    else
                                    {
                                        if (boost::filesystem::exists(hyperlinks[i]))
                                        {
                                            _docList.append(QString::fromStdString("file:///" + hyperlinks[i]));
                                        }
                                        else
                                        {
                                            std::string msg = "file: \"" + hyperlinks[i] + "\" not found";
                                            LOG_ERROR(msg.c_str());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _attributeList.append(new PropertyObject(featureAttributeList[i].first.c_str(), fieldType, featureAttributeList[i].second.c_str(), featureAttributeList[i].first.size()));
                            }
                        }
                        _setContextProperty("imageList", QVariant::fromValue(_imageList));
                        _setContextProperty("urlList", QVariant::fromValue(_urlList));
                        _setContextProperty("docList", QVariant::fromValue(_docList));
                        _setContextProperty("attributesModel", QVariant::fromValue(_attributeList));
                    }
                }
            }

        }

    }

    void CreateOEFeatureGUI::handleFeatureContextualSave()
    {
        std::string currentSelectedJsonFile = styleFilePath;
        std::ofstream outfile(currentSelectedJsonFile.c_str());

        QObject* templatelistPopup = _findChild("featureContextual");
        if (templatelistPopup != NULL)
        {
            std::string outString = templatelistPopup->property("jsonOutput").toString().toStdString();
            outfile << outString;
        }
        outfile.close();
        TERRAIN::IModelObject* modelObject = oeFeature->getModelObject();
        if (modelObject != NULL)
        {
            CORE::IObject* object = dynamic_cast<CORE::IObject*>(modelObject);
            SMCUI::IAddContentUIHandler* _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());
            //_addContentUIHandler->updateModelObject(modelObject, tempName);
            _addContentUIHandler->updateModelObject(object, currentSelectedJsonFile);
        }
    }

    bool CreateOEFeatureGUI::populateEnumList(QString name, QString parentName, QString value)
    {
        std::vector<std::string> strVector;
        std::string nameString = name.toStdString();
        std::string parentString = parentName.toStdString();
        std::string valueString = value.toStdString();

        DB::readEnumsValuesFromSymbol(parentString, nameString, strVector);

        //!  If str vector is empty this means that this is not a valid Enum, return false so that it can be treated as a string expression
        if (strVector.empty())
            return false;

        //Find index of valuestring
        int stringIndex = 0;
        for (int i = 0; i < strVector.size(); i++)
        {
            if (strVector[i] == valueString)
            {
                stringIndex = i;
                break;
            }
        }

        //bring the string to first location
        if (stringIndex != 0)
        {
            strVector[stringIndex] = strVector[0];
            strVector[0] = valueString;
        }

        //populate the list of templates
        QList<QObject*> styleSheetBoxList;

        for (int i = 0; i < strVector.size(); i++)
        {
            styleSheetBoxList.append(new VizComboBoxElement(strVector[i].c_str(), UTIL::ToString(i).c_str()));
        }

        //! 
        _contextPropertiesSet.push_back(nameString);
        _setContextProperty(nameString, QVariant::fromValue(styleSheetBoxList));

        return true;
    }
}