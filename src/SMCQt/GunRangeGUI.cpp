#include <SMCQt/GunRangeGUI.h>
#include <Core/IPoint.h>
#include <App/AccessElementUtils.h>
#include <App/IApplication.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <Core/AttributeTypes.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/ElementsUtils.h>
#include <iostream>
#include <Util/CoordinateConversionUtils.h>
#include <Util/FileUtils.h>

#include <osg/Geode>
#include <osg/ShapeDrawable>
#include <osg/Shape>
#include <osg/PolygonMode>
#include <osg/Depth>
#include <osg/PositionAttitudeTransform>
#include <Core/ITemporary.h>

#include <Database/IDatabase.h>
#include <Database/DatabaseUtils.h>
#include <DB/ReaderWriter.h>
#include <DB/ReaderWriterManager.h>
#include <Database/ISQLQueryResult.h>
#include <GIS/ISpatialiteDatabaseHolder.h>
#include <GIS/ISpatialDatabase.h>
#include <Database/ISQLQuery.h>

#include <Database/IDBTable.h>
#include <Database/IDBRecord.h>
#include <Database/IDBField.h>
#include <SMCQt/VizComboBoxElement.h>


#define EPISILON 0.00001

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, GunRangeGUI);
    DEFINE_IREFERENCED(GunRangeGUI, DeclarativeFileGUI);

    const std::string GunRangeGUI::gunIconPath = "/Symbols/gunfire.png";
    const std::string GunRangeGUI::TarIconPath = "/Symbols/Receiver.png";

    const std::string GunRangeGUI::GunRangeMenuObjectName = "GunRangeMenuObject";

    GunRangeGUI::GunRangeGUI()
    {      
    }


    CORE::RefPtr<DATABASE::ISQLQueryResult> executeStatement(CORE::RefPtr<DATABASE::IDatabase> db, std::string queryStatement)
    {
        if(!db.valid())
            return NULL;

        CORE::RefPtr<DATABASE::ISQLQuery>  query = db->createSQLQuery();
        if(!query.valid())
            return NULL;

        if(!query->exec(queryStatement))
            return NULL;

        CORE::RefPtr<DATABASE::ISQLQueryResult> result = query->getNextResult();

        return result;
    }


    GunRangeGUI::~GunRangeGUI()
    {
    }

    void GunRangeGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {   
                // handler for mouse events
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IPointUIHandler>(getGUIManager());                

                // handler for crest clearance backend code.
                _gunRangeHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::ICrestClearanceUIHandler>(getGUIManager());                

                // load radar icon 
                APP::IGUIManager* guiMgr = getGUIManager();
                CORE::IWorldMaintainer* wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(guiMgr);

                if(!wmain)
                {
                    return;
                }

                CORE::IComponent* comp = wmain->getComponentByName("IconModelLoaderComponent");
                if(!comp)
                {
                    return;
                }

                ELEMENTS::IIconLoader* iconLoader = comp->getInterface<ELEMENTS::IIconLoader>();
                if(!iconLoader)
                {
                    return;
                }

                std::string dataDir = UTIL::getDataDirPath();

                // create launch icon
                std::string gunIconName = "gunfire";
                iconLoader->loadIcon(gunIconName, dataDir + gunIconPath);
                _gunIcon = iconLoader->getIconByName(gunIconName);

                // create direction icon
                std::string dirIconName = "man";
                iconLoader->loadIcon(dirIconName, dataDir + TarIconPath);
                _tarIcon = iconLoader->getIconByName(dirIconName);

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }

        // to check for point created message
        else if(messageType == *VizUI::IPointUIHandler::PointCreatedMessageType)
        {
            _processPoint();
        }

        // Check whether the distance calculation is completed
        else if(messageType == *SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType)
        {
            _gunRangeHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _unsubscribe(_gunRangeHandler.get(), *SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType);
        }

        // to check the filter status and set/remove the loading icons
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();


            // if the calculation is still pending
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if(progress < 100)
                {
                    // TBD - support for progress bar has to be added
                }
            }

            else
            {
                if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
                {
                    emit changeToDefaultStateSignal();
                    _gunRangeHandler->toAddGun(true); 

                    QObject::disconnect( this, SIGNAL(changeToDefaultStateSignal()), this, 
                        SLOT(changeToDefaultStateSlot()) );
                }

                // if filter failure then there is no trajectory possible
                else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    // Warning messages added to handle trajectory calculation failure.
                    // If the failure type is velicty mismatch
                    if (filterMessage->getFailureReason() == GISCOMPUTE::FAILUREREASON_VELOCITY)
                    {
                        const double& maxVelocityCalc = _gunRangeHandler->getMaxVelocity();
                        const double& minVelocityCalc = _gunRangeHandler->getMinVelocity();

                        const double& maxSpeed = _gunRangeHandler->getMaxSpeed();
                        const double& minSpeed = _gunRangeHandler->getMinSpeed();

                        std::stringstream strstrm;
                        if ((minVelocityCalc < minSpeed && maxVelocityCalc < minSpeed) || (minVelocityCalc > maxSpeed && maxVelocityCalc > maxSpeed))
                        {
                            strstrm << "No trajectory possible. Min-Max velocity mismatch \r\n";
                            strstrm << "Calculated : (Min : " << minVelocityCalc << " Max : " << maxVelocityCalc << ") \r\n";
                            strstrm << " Original : (Min : " << minSpeed << " Max : " << maxSpeed << ")\r\n";
                            strstrm << " Decrease 'Angle Change' and try again OR Change weapon.";
                        }
                        else
                        {
                            strstrm << "No trajectory possible. In given angle difference, No velocity value lies between Min-Max range.\r\n";
                            strstrm << " Range : (Min : " << minSpeed << " Max : " << maxSpeed << ") \r\n";
                            strstrm << " Change weapon.";
                        }
                        emit showError("Filter Status", strstrm.str().c_str());
                    }
                    // If the failure type is displacement or range mismatch.
                    if (filterMessage->getFailureReason() == GISCOMPUTE::FAILUREREASON_DISPLACMENT)
                    {
                        const double& displacement = _gunRangeHandler->getDisplacement();
                        const double& minRange = _gunRangeHandler->getMinRange();
                        const double& maxRange = _gunRangeHandler->getMaxRange();

                        std::stringstream strstrm;

                        strstrm << "No trajectory possible. In or Out of range \r\n";
                        strstrm << "Calculated displacement : (" << displacement << ") \r\n";
                        strstrm << " Original Range: (Min : " << minRange << " Max : " << maxRange << ")\r\n";
                        strstrm << " Decrease 'Angle Change' and try again OR Change weapon.";

                        emit showError("Filter Status", strstrm.str().c_str());
                    }
                    else
                    {
                        emit showError("Filter Status", "No trajectory possible");
                    }
                    emit changeToDefaultStateSignal();

                    QObject::disconnect( this, SIGNAL(changeToDefaultStateSignal()), this, 
                        SLOT(changeToDefaultStateSlot()) );
                }
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void GunRangeGUI::changeToDefaultStateSlot()
    {
        _gunRangeMenuObject->setProperty( "progressBar", false );
        _gunRangeMenuObject->setProperty( "gunRangeState", "default" );

    }

    // this function handles different mouse clicks based on different states 
    void GunRangeGUI::_processPoint()
    {
        CORE::IPoint* point = _pointHandler->getPoint();
        QString pointX, pointY, pointZ;

        pointX = QString::number(point->getX());
        pointY = QString::number(point->getY());
        pointZ = QString::number(point->getZ());


        ELEMENTS::IIconHolder* iconHolder = point->getInterface<ELEMENTS::IIconHolder>();

        // process the points based upon the state and set the corresponding text fields
        if(_pointHandler.valid())
        {
            switch(_state)
            {
            case LP_MARK_PPC: 
                {
                    _gunRangeMenuObject->setProperty( "leGunPointLatId", pointX );
                    _gunRangeMenuObject->setProperty( "leGunPointLongID", pointY );
                    _gunRangeMenuObject->setProperty( "leGunPointAltId", pointZ );

                    _setLaunchPoint(point);

                    if(_gunIcon.valid() && iconHolder)
                    {
                        iconHolder->setIcon(_gunIcon.get());
                    }
                    else
                    {
                        LOG_DEBUG("Default Icon for launching point is used")
                    }
                    break;
                }

            case DIR_MARK_PPC: 
                {
                    _targetPos = point->getValue();

                    _gunRangeMenuObject->setProperty( "leDirectionX", pointY );
                    _gunRangeMenuObject->setProperty( "leDirectionY", pointX );
                    _gunRangeMenuObject->setProperty( "leDirectionZ", pointZ );

                    _setTargetPoint(point);

                    if(_tarIcon.valid() && iconHolder)
                    {
                        iconHolder->setIcon(_tarIcon.get());
                    }
                    else
                    {
                        LOG_DEBUG("Default Icon for direction marking is used")
                    }
                    break;
                }


            default:
                _removeObject(point->getInterface<CORE::IObject>());
                break;
            }
        }
    }

    // XXX - Move these slots to a proper place
    void GunRangeGUI::setActive(bool value)
    {
        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                _state = NO_MARK;
                _subscribe(_gunRangeHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                _gunRangeMenuObject = _findChild(GunRangeMenuObjectName);

                // connect the signal of various buttons in crest clearance qml
                if(_gunRangeMenuObject)
                {
                    // Projectile Path Calculation signals
                    QObject::connect( _gunRangeMenuObject, SIGNAL(qmlHandleGPMarkButtonClicked(bool)), 
                        this, SLOT(_handleGPMarkButtonClicked(bool)));
                    QObject::connect( _gunRangeMenuObject, SIGNAL(qmlHandleDirMarkButtonClicked(bool)), 
                        this, SLOT(_handleDirMarkButtonClicked(bool)));
                    QObject::connect( _gunRangeMenuObject, SIGNAL(qmlHandleStartButtonClicked()), 
                        this, SLOT(_handleStartButtonClicked()));
                    QObject::connect( _gunRangeMenuObject, SIGNAL(qmlHandleStopButtonClicked()), 
                        this, SLOT(_handleStopButtonClicked()));
                }

                //reading the sqlite database
                CORE::RefPtr<DATABASE::IDatabase> _database = DATABASE::ValidateDatabaseFile("Weapon.sqlite"); 

                if(!_database.valid())
                {                
                   emit showError("Weapon Range Analysis", "Weapon database not found.", true,
                            "_closeGUI()");  
                    return;
                }

                std::string metaQueryStatement = "select WeaponType,MinRange from Weapon_Config";
                CORE::RefPtr<DATABASE::ISQLQueryResult> metaResult = executeStatement(_database, metaQueryStatement);

                if(!metaResult.valid())
                {                
                   emit showError("Weapon Range Analysis", "Weapon database Invalid.", true,
                            "_closeGUI()");  
                    return;
                }

                CORE::RefPtr<DATABASE::IDBTable> tableName = metaResult->getResultTable();

                if(!tableName.valid())
                    return;

                int count = 0;
                QList<QObject*> comboBoxList;

                CORE::RefPtr<DATABASE::IDBRecord> record = tableName->getNextRecord();
                while(record.valid())
                {
                    DATABASE::IDBField* weaponType = record->getFieldByIndex(0);
                    std::string weaponName = weaponType->toString();
                    comboBoxList.append(new VizComboBoxElement(weaponName.c_str(), UTIL::ToString(count).c_str()));
                    record = tableName->getNextRecord();
                }

                _setContextProperty("WeaponListModel", QVariant::fromValue(comboBoxList));
            }
            else
            {
                _unsubscribe(_gunRangeHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                _removeObject(_lpPoint.get());
                _removeObject(_tgtPoint.get());
                _gunRangeHandler->stopFilter();
                _state = NO_MARK;

                CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
                group->removeChild(_spherePat);
                _spherePat = NULL;

            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }


    void GunRangeGUI::_closeGUI(){
         _gunRangeMenuObject = _findChild(GunRangeMenuObjectName);
           QMetaObject::invokeMethod(_gunRangeMenuObject, "closePopup");
    }

    void GunRangeGUI::_handleStartButtonClicked()
    {
        // Subscribe to distance calculation message
        if(_gunRangeHandler.valid())
        {
            _subscribe(_gunRangeHandler.get(), *SMCUI::ICrestClearanceUIHandler::CrestClearanceCompletedMessageType);
            _gunRangeHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            //  _gunRangeMenuObject->setProperty( "progressBar", true );

            QString lpHeight = _gunRangeMenuObject->property("leGunPointHeightId").toString();

            if(lpHeight == "")
            {
                emit showError( "Gun Range", "Launch Altitude not specified", true );
                _gunRangeMenuObject->setProperty( "gunRangeState", "default" );
                return;
            }

            double height = lpHeight.toDouble();
            QString lpLatLE = _gunRangeMenuObject->property("leGunPointLatId").toString();
            QString lpLongLE = _gunRangeMenuObject->property("leGunPointLongID").toString();
            QString lpAltLE = _gunRangeMenuObject->property("leGunPointAltId").toString();

            osg::Vec3d launchingpos(osg::Vec3d(lpLatLE.toDouble(), lpLongLE.toDouble(), lpAltLE.toDouble() ));

            if(launchingpos.length() < EPISILON)
            {
                emit showError( "Gun Range", "Invalid launching position specified", true );
                _gunRangeMenuObject->setProperty( "gunRangeState", "default" );
                LOG_ERROR("Invalid launching position specified");
                return;
            }

            launchingpos.z() = launchingpos.z() + height;

            _gunRangeHandler->setLaunchingPosition(launchingpos);


            QString tpDirXLE = _gunRangeMenuObject->property("leDirectionX").toString();
            QString tpDirYLE = _gunRangeMenuObject->property("leDirectionY").toString();
            QString tpDirZLE = _gunRangeMenuObject->property("leDirectionZ").toString();

            osg::Vec3d targetpos(osg::Vec3d( tpDirYLE.toDouble(), tpDirXLE.toDouble(), tpDirZLE.toDouble() ));

            if((tpDirXLE == "") || (tpDirYLE == "")  || (tpDirZLE == ""))
            {
                emit showError( "Gun Range", "Invalid target point specified", true );
                _gunRangeMenuObject->setProperty( "gunRangeState", "default" );
                LOG_ERROR("Invalid direction specified");
                return;
            }

            _gunRangeHandler->setTargetPosition(targetpos);

            QString gunType = _gunRangeMenuObject->property("leGunType").toString();
              
             std::string weaponType = gunType.toStdString();
        
             double _minAngle;
             double _maxAngle;
             double _minSpeed;
             double _maxSpeed;
             float _minRange;
             float _maxRange;
               
              CORE::RefPtr<DATABASE::IDatabase> _database = DATABASE::ValidateDatabaseFile("Weapon.sqlite");
              if(!_database.valid())
                      return;

              else
              {
                  std::string metaQueryStatement = "select MinAngle from Weapon_Config where WeaponType ='" + weaponType + "'";
                  CORE::RefPtr<DATABASE::ISQLQueryResult> metaResult = executeStatement(_database, metaQueryStatement);
                  
                  if(!metaResult.valid())
                    return;

                  CORE::RefPtr<DATABASE::IDBTable> tableName = metaResult->getResultTable();
                  if(!tableName.valid())
                  return;

                  CORE::RefPtr<DATABASE::IDBRecord> record = tableName->getNextRecord();
                  DATABASE::IDBField* field = record->getFieldByIndex(0);
                  _minAngle = field->toInt();

                  std::string metaQueryStatement1 = "select MaxAngle from Weapon_Config where WeaponType ='" + weaponType + "'";
                  metaResult = executeStatement(_database, metaQueryStatement1);
                  
                  if(!metaResult.valid())
                    return;

                  tableName = metaResult->getResultTable();
                  if(!tableName.valid())
                  return;

                  record = tableName->getNextRecord();
                  field = record->getFieldByIndex(0);
                  _maxAngle = field->toInt();

                  std::string metaQueryStatement2 = "select MinVelocity from Weapon_Config where WeaponType ='" + weaponType + "'";
                  metaResult = executeStatement(_database, metaQueryStatement2);

                  tableName = metaResult->getResultTable();
                  if(!tableName.valid())
                  return;

                  record = tableName->getNextRecord();
                  field = record->getFieldByIndex(0);
                  _minSpeed = field->toInt();

                  std::string metaQueryStatement3 = "select MaxVelocity from Weapon_Config where WeaponType ='" + weaponType + "'";
                  metaResult = executeStatement(_database, metaQueryStatement3);
                    
                  if(!metaResult.valid())
                    return;

                  tableName = metaResult->getResultTable();
                  if(!tableName.valid())
                  return;

                  record = tableName->getNextRecord();
                  field = record->getFieldByIndex(0);
                  _maxSpeed = field->toInt();

                  std::string metaQueryStatement4 = "select MinRange from Weapon_Config where WeaponType ='" + weaponType + "'";
                  metaResult = executeStatement(_database, metaQueryStatement4);
                
                  if(!metaResult.valid())
                    return;

                  tableName = metaResult->getResultTable();
                  if(!tableName.valid())
                  return;

                  record = tableName->getNextRecord();
                  field = record->getFieldByIndex(0);
                  _minRange = field->toInt();

                  std::string metaQueryStatement5 = "select MaxRange from Weapon_Config where WeaponType ='" + weaponType + "'";
                  CORE::RefPtr<DATABASE::ISQLQueryResult> metaResult5 = executeStatement(_database, metaQueryStatement5);
                
                  if(!metaResult5.valid())
                    return;

                  tableName = metaResult5->getResultTable();
                  if(!tableName.valid())
                  return;

                  record = tableName->getNextRecord();
                  field = record->getFieldByIndex(0);
                  _maxRange = field->toInt();

              }

            _gunRangeHandler->setMinAngle(_minAngle);
            _gunRangeHandler->setMaxAngle(_maxAngle);
            _gunRangeHandler->setMinSpeed(_minSpeed);
            _gunRangeHandler->setMaxSpeed(_maxSpeed);
            _gunRangeHandler->setWeaponName(weaponType);
            _gunRangeHandler->setMinRange(_minRange);
            _gunRangeHandler->setMaxRange(_maxRange);

            QString lestep = _gunRangeMenuObject->property("leStepSizeId").toString();
            double step = lestep.toDouble();

            _gunRangeHandler->setAngleStepSize(step);
            _gunRangeHandler->setComputationType(SMCUI::ICrestClearanceUIHandler::ANGLE_AND_VELOCITY);

            double longitudeD = lpLatLE.toDouble();
            double latitudeD = lpLongLE.toDouble();
            double altitudeD = lpAltLE.toDouble();
            osg::Vec3 pointECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(latitudeD, longitudeD, altitudeD ));

            osg::TessellationHints* hints = new osg::TessellationHints;
            hints->setDetailRatio(0.0);

            CORE::RefPtr<osg::Sphere>_sphere = new osg::Sphere();
            CORE::RefPtr<osg::Sphere>_sphere1 = new osg::Sphere();

            _sphere->setRadius(_minRange);
            _sphere1->setRadius(_maxRange);
            _sphere->setDataVariance(osg::Object::DYNAMIC);
            _sphere1->setDataVariance(osg::Object::DYNAMIC);

            osg::ref_ptr<osg::ShapeDrawable> shape = new osg::ShapeDrawable;
            osg::ref_ptr<osg::ShapeDrawable> shape1 = new osg::ShapeDrawable;

            shape->dirtyDisplayList();
            shape1->dirtyDisplayList();
            shape->setShape( _sphere.get() );
            shape1->setShape( _sphere1.get() );
            shape->setTessellationHints( hints );
            shape1->setTessellationHints( hints );

            osg::Vec4 color( 0.0, 0.0, 1.0, 0.3 );

            osg::Vec4 color1( 1.0, 0.0, 0.0, 0.3 );

            shape->setColor( color );
            shape1->setColor( color1 );

            if ( _spherePat != NULL )
            {

                CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
                group->removeChild(_spherePat);
                _spherePat = NULL;
            }

            _spherePat = new osg::PositionAttitudeTransform;


            CORE::RefPtr< osg::Geode > geode = new osg::Geode();
            CORE::RefPtr< osg::Geode > geode1 = new osg::Geode();
            geode->addDrawable( shape.get() );
            geode1->addDrawable( shape1.get() );

            _spherePat->addChild(geode.get());
            _spherePat->addChild(geode1.get());

            _spherePat->setPosition( pointECEF );

            //Making both the spheres transparent
            geode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
            geode->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
            geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);

            geode1->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
            geode1->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
            geode1->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);



            CORE::RefPtr<osg::Group> group = dynamic_cast<osg::Group*>(ELEMENTS::GetFirstWorldFromMaintainer()->getOSGGroup());
            group->addChild ( _spherePat );
            _spherePat->setNodeMask(CORE::TempObjNodeMask);

            _gunRangeMenuObject->setProperty( "progressBar", true );

            QObject::connect( this, SIGNAL(changeToDefaultStateSignal()), this, 
                SLOT(changeToDefaultStateSlot()), Qt::QueuedConnection );

            _gunRangeHandler->execute();
            _gunRangeMenuObject->setProperty( "gunRangeState", "default" );
            _gunRangeMenuObject->setProperty( "progressBar", false );

        }
    }

    void GunRangeGUI::_handleStopButtonClicked()
    {
        if(!_gunRangeHandler.valid())
        {
            return;
        }

        _gunRangeMenuObject->setProperty( "gunRangeState", "default" );
        _gunRangeHandler->stopFilter();
        _gunRangeMenuObject->setProperty( "progressBar", false );

        QObject::disconnect( this, SIGNAL(changeToDefaultStateSignal()), this, 
            SLOT(changeToDefaultStateSlot()) );
    }

    void GunRangeGUI::_handleGPMarkButtonClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = LP_MARK_PPC;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(LP_MARK_PPC == lastState)
            {
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    void GunRangeGUI::_handleDirMarkButtonClicked(bool pressed)
    {

        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = DIR_MARK_PPC;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(DIR_MARK_PPC == lastState)
            {
                _setPointHandlerEnabled(pressed);
            }
        }
    }



    // this function will activate the mouse events and set the point to create events when clicked
    void GunRangeGUI::_setPointHandlerEnabled(bool enable)
    {
        if(_pointHandler.valid())
        {
            if(enable)
            {
                _pointHandler->setProcessMouseEvents(true);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);                 
            }
            else
            {
                _pointHandler->setProcessMouseEvents(false);
                _pointHandler->setHandleMouseClicks(false);
                _pointHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark trans/recv position");
        }
    }

    // to remove the objects from the world
    void GunRangeGUI::_removeObject(CORE::IObject* obj)
    {
        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        if(world.valid() && obj)
        {
            try
            {
                world->removeObjectByID(&(obj->getInterface<CORE::IBase>(true)->getUniqueID()));
            }
            catch(...){}
        }
    }

    // to set the target point icon to a position specifed by mouse click
    void GunRangeGUI::_setTargetPoint(CORE::IPoint* point)
    {
        _removeObject(_tgtPoint.get());
        _tgtPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

    // to set the launching point icon to a position specifed by mouse click
    void GunRangeGUI::_setLaunchPoint(CORE::IPoint* point)
    {
        _removeObject(_lpPoint.get());
        _lpPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

} // namespace GUI
