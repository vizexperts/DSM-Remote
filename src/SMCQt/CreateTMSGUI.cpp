#include <SMCQt/CreateTMSGUI.h>

#include <QFileDialog>

#include <QtCore/QFile>

#include <App/AccessElementUtils.h>

#include <SMCUI/IAddContentStatusMessage.h>

#include <CORE/WorldMaintainer.h>

#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Core/IGISdata.h>
#include <osgDB/FileNameUtils>

namespace SMCQt
{
    const std::string CreateTMSGUI::CreateTMSPopup = "CreateTMS";

    DEFINE_META_BASE(SMCQt, CreateTMSGUI);
    DEFINE_IREFERENCED(CreateTMSGUI, DeclarativeFileGUI)
     

        CreateTMSGUI::CreateTMSGUI()
        :_bWebRasterData(false)
    {

    }

    CreateTMSGUI::~CreateTMSGUI()
    {

    }

    void 
        CreateTMSGUI::popupLoaded(QString type)
    {
        if (type == CreateTMSPopup.c_str())
        {
            QObject* createTMS = _findChild(CreateTMSPopup);
            if(createTMS != NULL)
            {
                QObject::connect( createTMS, SIGNAL(browseButtonClicked()), this ,SLOT(browseButtonPressedSlot()), Qt::UniqueConnection);
                QObject::connect( createTMS, SIGNAL(okButtonClicked()), this,SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);

            }
        }
    }

    void 
        CreateTMSGUI::_loadAndSubscribeSlots()
    {
        QObject* popupLoader = _findChild(SMP_FileMenu);

        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }

    void
        CreateTMSGUI::_loadAddContentUIHandler()
    {
        _addContentUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
    }

	void CreateTMSGUI::update(const CORE::IMessageType& messageType,
		const CORE::IMessage& message)
	{
		if (messageType == *CORE::IWorldMaintainer::TickMessageType)
		{
			SMCUI::IExportUIHandler::TMSCREATIONSTATUS status;
			_exportUIHandler->getStatus(status);

			if (status == SMCUI::IExportUIHandler::COMPLETED)
			{
				//stop with success prompt
                QObject* createTMS = _findChild(CreateTMSPopup);
				if (createTMS)
				{
					createTMS->setProperty("calculating", false);
					QMetaObject::invokeMethod(createTMS, "closePopup");
				}
				CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
				_unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);
				emit showError("Completed", "Cache has been successfully created");
			}

			else if (status == SMCUI::IExportUIHandler::ERRORINCREATION)
			{
                QObject* createTMS = _findChild(CreateTMSPopup);
				if (createTMS)
				{
					createTMS->setProperty("calculating", false);	
				}
				CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
				_unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);
				emit showError("Error", "Cache could not be created, please try again");
			}

		}
        // Check whether the polygon has been updated
        else if (messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if (_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if (line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first = line->getPoint(0)->getValue();
                    osg::Vec3d second = line->getPoint(2)->getValue();
                    QObject* createTMSObject = _findChild(CreateTMSPopup);
                    if (createTMSObject)
                    {
                        createTMSObject->setProperty("bottomLeftLongitude", UTIL::ToString(first.x()).c_str());
                        createTMSObject->setProperty("bottomLeftLatitude", UTIL::ToString(first.y()).c_str());
                        createTMSObject->setProperty("topRightLongitude", UTIL::ToString(second.x()).c_str());
                        createTMSObject->setProperty("topRightLatitude", UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
	}
    void 
        CreateTMSGUI::_handleOkButtonClicked()
    {
        try
        {   
            _exportUIHandler = vizApp::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IExportUIHandler>(getGUIManager());

            if(_exportUIHandler!= NULL )
            {
                
                std::string minLevel;
                std::string maxLevel;
                osg::Vec4 selectedArea(0.0, 0.0, 0.0, 0.0);

	            if (_directoryPath.isEmpty())
	            {
		            emit showError("Invalid input", "Please give directory for TMS creation");
		            return;
	            }

                QObject* createTMS = _findChild(CreateTMSPopup);
	            if (createTMS)
	            {

		            if ( (!createTMS->property("minLevel").toString().isEmpty()) && createTMS->property("maxLevel").toInt() >= 0)
		            {
			            minLevel = createTMS->property("minLevel").toString().toStdString();
		            }

		            else
		            {
			            emit showError("Invalid input", "Please enter min level >= 0");
			            return;
		            }

		            if ( (!createTMS->property("maxLevel").toString().isEmpty()) && createTMS->property("maxLevel").toInt() > 0)
		            {
			            maxLevel = createTMS->property("maxLevel").toString().toStdString();
		            }

		            else
		            {
			            emit showError("Invalid input", "Please enter max level > 0");
			            return;
		            }
                    if (_bWebRasterData)
                    {
                        if (createTMS->property("bottomLeftLongitude").toString().isEmpty() || createTMS->property("bottomLeftLatitude").toString().isEmpty() || createTMS->property("topRightLongitude").toString().isEmpty() || createTMS->property("topRightLatitude").toString().isEmpty())
                        {
                            emit showError("Invalid input", "No region seleted for web raster data.");
                            return;
                        }
                    }

		            if (createTMS->property("maxLevel").toInt() < createTMS->property("minLevel").toInt())
		            {
			            emit showError("Invalid input", "Max Level must be greater or equal to Min Level");
			            return;
		            }
		            createTMS->setProperty("calculating", true);
                    if (_bWebRasterData)
                    {
                        selectedArea.set(createTMS->property("bottomLeftLongitude").toFloat(), createTMS->property("bottomLeftLatitude").toFloat(), createTMS->property("topRightLongitude").toFloat(), createTMS->property("topRightLatitude").toFloat());
                    }
                }
	            CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
	            _subscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

	            _exportUIHandler->exportRasterAsTMS(_directoryPath.toStdString(), minLevel, maxLevel, selectedArea);
				
            }

        }
        catch(const vizUtil::Exception& e)
        {
            showError("Invalid Entry","Enter valid values");
            LOG_ERROR(e.What());
        }

    }


    void CreateTMSGUI::browseButtonPressedSlot()
    {
        std::string directory = "/home";

        try
        {
            directory = getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->getLastBrowsedDirectory();
        }
        catch(...){}

        std::string label = std::string("Open TMS Directory");
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        _directoryPath = QFileDialog::getExistingDirectory (parent, QString(label.c_str()), QString(directory.c_str()),  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        if(!_directoryPath.isEmpty())
        {
            QObject* createTMS = _findChild(CreateTMSPopup);
            if(createTMS != NULL)
            {
                createTMS->setProperty("selectedFileName",QVariant::fromValue(_directoryPath));
            }
            try
            {
                directory = _directoryPath.toStdString();
                getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->setLastBrowsedDirectory(directory);
            }
            catch(...){}
        }
    }

	void CreateTMSGUI::reset()
	{
		QObject* createTMS = _findChild(CreateTMSPopup);
        if (createTMS)
        {
	        _directoryPath = "";
	        createTMS->setProperty("selectedFileName", "");
	        createTMS->setProperty("minLevel", 0);
	        createTMS->setProperty("maxLevel", 1);
	        createTMS->setProperty("calculating", false);

	        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
	        _unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);
        }
        markArea(false);
        _bWebRasterData = false;
	}

    void CreateTMSGUI::setActive(bool value)
    {
        if (value == getActive())
        {
            return;
        }
        if (value)
        {
            QObject* areaAnalysis = _findChild(CreateTMSPopup);
            if (areaAnalysis)
            {
                QObject::connect(areaAnalysis, SIGNAL(markArea(bool)),
                    this, SLOT(markArea(bool)), Qt::UniqueConnection);
            }

            _bWebRasterData = false;
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            if (selectionUIHandler.valid())
            {
                CORE::ISelectionComponent::SelectionMap selMap =  selectionUIHandler->getCurrentSelection();
                if (selMap.size())
                {
                    
                    const TERRAIN::IRasterObject* raster = selMap.begin()->second->getInterface<TERRAIN::IRasterObject>();
                    if (raster)
                    {
                        std::string url = raster->getURL();
                        if (url != "")
                        {
                            std::string ext = osgDB::getFileExtension(url);
                            if (ext == "xyz" || ext == "wms" || ext == "bing")
                            {
                                _bWebRasterData = true;
                            }
                        }
                    }

                }
            }
            // when mark button is unchecked then dialog was getting closed,
            // removing clicked state of the button to avoid being reclicked when object when object is active.
            if (_bWebRasterData)
            {
                QObject* qtObject = _findChild("toolBox");
                if (qtObject)
                {
                    QMetaObject::invokeMethod(qtObject, "removeHighlight");
                }
            }
            QObject* createTMS = _findChild(CreateTMSPopup);
            if (createTMS)
            {
                createTMS->setProperty("areaGroupBoxVisible", _bWebRasterData);
            }
            
        }
        else
        {
            reset();
        }
         
        // TODO: SetFocus of uihandler to true and subscribe to messages
        DeclarativeFileGUI::setActive(value);
    }

    void CreateTMSGUI::markArea(bool pressed)
    {
        if (!_areaHandler.valid())
        {
            return;
        }

        if (pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            //_areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

            QObject* createTMSPopup = _findChild(CreateTMSPopup);
            if (createTMSPopup)
            {
                createTMSPopup->setProperty("isMarkSelected", false);
            }
        }
    }
}