/*****************************************************************************
*
* File             : ElementListGUI.cpp
* Description      : ElementListGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************/


#include <App/IApplication.h>
#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/SearchGUI.h>

#include <Core/InterfaceUtils.h>
#include <Core/IObjectMessage.h>
#include <Core/IMessage.h>
#include <Core/ICompositeObject.h>
#include <Core/ITemporary.h>
#include <Core/ISelectable.h>
#include <Core/IVisibility.h>
#include <Core/private/IVisibilityInternal.h>
#include <Core/ISelectionComponent.h>
#include <Core/GroupAttribute.h>
#include <Core/IFeatureObject.h>
#include <Core/AttributeTypes.h>
#include <Core/IVisibilityChangedMessage.h>
#include <Core/IFeatureLayer.h>
#include <Core/IMessageFactory.h>
#include <Core/WorldMaintainer.h>
#include <Core/IDeletable.h>
#include <Core/CoreRegistry.h>
#include <Core/IProperty.h>
#include <Core/IProxyObject.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>
#include <Terrain/IWMSRasterLayer.h>
#include <Terrain/IModelObject.h>

#include <Elements/ILOSObject.h>
#include <Elements/ITINObject.h>
#include <Elements/IGeoVrmlObject.h>
#include <Elements/IRadioLOSObject.h>
#include <Elements/IFireObject.h>
#include <Elements/IRadioCoverageObject.h>
#include <Elements/IModelResult.h>
#include <Elements/IDestructionZone.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/ElementsUtils.h>

#include <TacticalSymbols/ITacticalSymbol.h>

#include <serialization/ISerializationProxyObject.h>
#include <serialization/IProjectLoaderComponent.h>

#include <SMCQt/ElementListGUI.h>
#include <SMCQt/StyleTemplateGUI.h>

#include <VizUI/UIHandlerManager.h>
#include <VizUI/IDeletionUIHandler.h>


#include <SMCElements/IUserFolderComponent.h>
#include <SMCElements/IUserFolderItem.h>
#include <SMCElements/SMCElementsPlugin.h>

#include <SMCUI/IAddContentUIHandler.h>
#include <Terrain/IModelObject.h>
#include <GIS/IFeatureLayer.h>
#include <GIS/IDynamicProfileObject.h> 
#include <GIS/IFeature.h>
#include <GPS/IGPSSource.h>
#include <CORE/ITaggable.h>
#include <SMCQt/VizComboBoxElement.h>
#include <DGN/IDGNFolder.h>
#include <Util/FileUtils.h>
#include <QFileDialog>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ElementListGUI);

    const std::string ElementListGUI::ELEMENTLISTTREEVIEW = "elementListTreeView";
    const std::string ElementListGUI::ELEMENTLIST = "elementList";
    const std::string ElementListGUI::OPENATTRIBUTETABLEPOPUP = "openAttributeTablePopup";
    const std::string ElementListGUI::REORDERRASTERITEMPOPUP = "reorderRasterItemPopup";
    const std::string ElementListGUI::ITEMRENAMEPOPUP = "itemRenamePopup";
    const std::string ElementListGUI::ADDFOLDERPOPUP = "addFolderPopup";
    const std::string ElementListGUI::TOOLBOXOBJECTNAME = "toolBox";
    const std::string ElementListGUI::ITEMRELOADPOPUP = "reloadObjectPopup";
    const std::string ElementListGUI::DGNClassificationToolboxButton = "elementClassificationToolboxButton";

    ElementListGUI::ElementListGUI()
        :_localLayersItem(NULL)
        , _topElementIcon("../../gui/IconsSets/DSM_iconSet_Dark/ElementListIcons/layers.png")
        , _showLayerChild(true)
        , _dgnLayerCountInWorld(0)
        , _isReloading(false)
    {
        _elementIcons = new CORE::NamedGroupAttribute("Elementicons");
    }

    ElementListGUI::~ElementListGUI(){}

    void ElementListGUI::onRemovedFromGUIManager()
    {
        // Unsubscribing from world messages
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        _unsubscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        _unsubscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);

        _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::SelectionMessageType);
        _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ClearSelectionMessageType);

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void ElementListGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                CORE::RefPtr<VizQt::IQtGUIManager> qtmgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                if (qtmgr.valid())
                {
                    _treeModel = new ElementListTreeModel;

                    _initializeAllDefaultItems();

                    _treeModel->addItem(_localLayersItem);

                    QQmlContext* rootContext = qtmgr->getRootContext();
                    if (rootContext)
                    {
                        rootContext->setContextProperty("elementListTreeModel", _treeModel);
                    }
                    connect(_treeModel, SIGNAL(itemChangedSendNotify(CORE::RefPtr<const CORE::IObject>)),
                        SLOT(_itemChangedSendNotify(CORE::RefPtr<const CORE::IObject>)));

                    _elementListTreeView = _findChild(ELEMENTLISTTREEVIEW);
                    if (_elementListTreeView)
                    {
                        QObject::connect(_elementListTreeView, SIGNAL(clicked(int)), this, SLOT(_onItemClicked(int)));
                        QObject::connect(_elementListTreeView, SIGNAL(doubleClicked(int)), this, SLOT(_onItemDoubleClicked(int)));
                        QObject::connect(_elementListTreeView, SIGNAL(deleteKeyPressed(int)), this, SLOT(_onItemDeleteKeyPressed(int)));
                    }

                    // Subscribe for world added message to the world maintainer
                    CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                        APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
                    _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
                    _subscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);

                    const CORE::ComponentMap& cmap = wmain->getComponentMap();
                    _selectionComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
                    _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::SelectionMessageType);
                    _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ClearSelectionMessageType);

                    //! 
                    CORE::RefPtr<SMCElements::IUserFolderComponent> userFolderComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, SMCElements::IUserFolderComponent>(cmap);
                    if (userFolderComponent.valid())
                    {
                        _subscribe(userFolderComponent.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
                        _subscribe(userFolderComponent.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);
                        _subscribe(userFolderComponent.get(), *SMCElements::IUserFolderComponent::UserFolderConfigurationLoadedMessageType);
                        CORE::RefPtr<SMCElements::IUserFolderItem> rootFolderItem = userFolderComponent->getRootFolder();
                        if (rootFolderItem.valid())
                        {
                            rootFolderItem->getInterface<CORE::IBase>()->setName("Layers");
                            //_localLayersItem->setObject(rootFolderItem->getInterface<CORE::IObject>());
                        }
                    }

                    //connecting the search lineEdit
                    QObject* elementList = _findChild(ELEMENTLIST);
                    if (elementList)
                    {
                        QObject::connect(elementList, SIGNAL(search(QString)), this, SLOT(_searchElements(QString)));

                        QObject::connect(elementList, SIGNAL(openRenameItemPopup(int)), this, SLOT(_openRenameItemPopup(int)));
                    }
                    QObject* toolBoxObject = _findChild(TOOLBOXOBJECTNAME);
                    if (toolBoxObject)
                    {
                        QObject::connect(toolBoxObject, SIGNAL(openAttributeTablePopup()), this, SLOT(_openAttributeTablePopup()));
                        QObject::connect(toolBoxObject, SIGNAL(rasterMove(int)), this, SLOT(_rasterMove(int)));

                    }

                    // DGN Classification UI class
                    CORE::RefPtr<APP::IGUI> gui = getGUIManager()->getGUIByClassname("SMCQt::ElementClassificationGUI");
                    if (gui.valid())
                    {
                        _subscribe(gui.get(), *CORE::IVisibilityChangedMessage::VisibilityChangedMessageType);
                    }
                }
            }
            catch (...)
            {
                // XXX - Ignoring the message here?
            }
        }
        else if (messageType == *SMCElements::IUserFolderComponent::UserFolderConfigurationLoadedMessageType)
        {
            CORE::IComponent* userComp = ELEMENTS::GetComponentFromMaintainer("UserFolderComponent");
            if (userComp)
            {
                CORE::RefPtr<SMCElements::IUserFolderComponent> userFolderComponent = userComp->getInterface<SMCElements::IUserFolderComponent>();

                SMCElements::IUserFolderItem* rootFolder = userFolderComponent->getRootFolder();
                if (rootFolder != NULL)
                {
                    const SMCElements::IUserFolderItem::UserFolderCollection::UIDBaseMap& folderMap = rootFolder->getChildrenList();
                    SMCElements::IUserFolderItem::UserFolderCollection::UIDBaseMap::const_iterator iter = folderMap.begin();
                    while (iter != folderMap.end())
                    {
                        //check if the item is already added to the element list
                        if (_isReloading)
                        {
                            SMCQt::ElementListTreeModelItem* item = _findElementTree(_localLayersItem, iter->second->getInterface<CORE::IObject>());
                            if (!item)
                            {
                                // if element is not present then locate it in element list.
                                SMCQt::ElementListTreeModelItem* newItem = new SMCQt::ElementListTreeModelItem;
                                _populateElementTree(_localLayersItem, newItem, iter->second->getInterface<CORE::IObject>());
                            }
                            else
                            {
                                // if element is present then re-locating element in element list and deleting old element from list.
                                SMCQt::ElementListTreeModelItem* newItem = new SMCQt::ElementListTreeModelItem;
                                _populateElementTree(_localLayersItem, newItem, iter->second->getInterface<CORE::IObject>());
                                if (_treeModel)
                                {
                                    _treeModel->removeItem(item);
                                }
                            }
                        }
                        else
                        {
                            SMCQt::ElementListTreeModelItem* newItem = new SMCQt::ElementListTreeModelItem;
                            _populateElementTree(_localLayersItem, newItem, iter->second->getInterface<CORE::IObject>());
                        }

                        ++iter;
                    }
                }
                _isReloading = false;
            }
        }
        else if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            // Get the list of objects from the world
            CORE::RefPtr<CORE::IWorld> iworld =
                APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());

            // Subscribe to object removed and added messages
            _subscribe(iworld.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
            _subscribe(iworld.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);

            _localLayersItem->setObject(iworld->getInterface<CORE::IObject>());

            const CORE::ObjectMap& omap = iworld->getObjectMap();
            for (CORE::ObjectMap::const_iterator citer = omap.begin();
                citer != omap.end();
                ++citer)
            {
                CORE::RefPtr<CORE::IObject> object = citer->second;
                SMCQt::ElementListTreeModelItem* newItem = new SMCQt::ElementListTreeModelItem;

                _populateElementTree(_localLayersItem, newItem, object.get());
            }

            _treeModel->openItem(_treeModel->indexOf(_localLayersItem));

            //! Reset dgn classification tool box button
            QObject* qobject = _findChild(DGNClassificationToolboxButton);
            if (qobject)
            {
                qobject->setProperty("enabled", QVariant::fromValue(false));
            }

        }
        else if (messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            _treeModel->closeItem(_treeModel->indexOf(_localLayersItem));

            _emptyItemAndDeleteChild(_localLayersItem);
            _localLayersItem->setCheckState(Qt::Checked);

            _treeModel->closeItem(_treeModel->indexOf(_localLayersItem));
            _treeModel->openItem(_treeModel->indexOf(_localLayersItem));
            //_treeModel->emitDataChanged(0);
            //_treeModel->emitDataChanged(_treeModel->indexOf(_localLayersItem));

        }
        else if (messageType == *CORE::IObjectMessage::ObjectAddedMessageType)
        {
            CORE::RefPtr<CORE::IObjectMessage> omsg = message.getInterface<CORE::IObjectMessage>();
            CORE::RefPtr<const CORE::IObject> object = omsg->getObject();

            CORE::RefPtr<CORE::IBase> objBase = object->getInterface<CORE::IBase>();

            //check if the added object is temporary object or not
            CORE::RefPtr<CORE::ITemporary> temp = object->getInterface<CORE::ITemporary>();
            if (temp.valid())
            {
                if (temp->getTemporary())
                {
                    return;
                }
            }

            CORE::IObject* parent = object->getParent();

            if (!parent)
            {
                LOG_ERROR("Object does not have a valid parent, not adding to element list: " +
                    objBase->getClassname() + ":" + objBase->getName() + ":" + objBase->getUniqueID().toString());

                return;
            }

            //see if the parent is a feature layer, in that do not add the feature object
            if (!_showLayerChild)
            {
                CORE::RefPtr<CORE::IFeatureLayer> layer =
                    parent->getInterface<CORE::IFeatureLayer>();
                if (layer.valid() && layer->getFeatureLayerType() != CORE::IFeatureLayer::RESULT)
                {
                    return;
                }
            }

            LOG_DEBUG("Adding object to element list: " + objBase->getClassname() + ":" + objBase->getName() + ":" + objBase->getUniqueID().toString());

            //see if it is added to world
            CORE::IWorld* world = NULL;
            if (parent)
                world = parent->getInterface<CORE::IWorld>();

            //check whether DGN Classification enable or not
            CORE::RefPtr<DGN::IDGNFolder> dgFolderObject = object->getInterface<DGN::IDGNFolder>();

            if (dgFolderObject != NULL)
            {
                _dgnLayerCountInWorld++;
                QObject* object = _findChild(DGNClassificationToolboxButton);
                if (object)
                {
                    object->setProperty("enabled", QVariant::fromValue(true));
                }
            }
            //
            //XXX - Hack for Raster data since raster data has a composite object as a parent which is not added to the world.
            // So we need to query the world directly
            TERRAIN::IRasterObject* rasterObject = object->getInterface<TERRAIN::IRasterObject>();
            if (rasterObject != NULL)
            {
                world = object->getWorld();
            }

            SMCQt::ElementListTreeModelItem* parentItem = NULL;

            if (world)
            {
                parentItem = _localLayersItem;
            }
            else
            {
                parentItem = _findElementTree(_localLayersItem, parent);

                if (parentItem == NULL)
                    parentItem = _localLayersItem;
            }

            if (parentItem)
            {
                //check if the item is already added to the element list
                SMCQt::ElementListTreeModelItem* item = _findElementTree(parentItem, object.get());
                if (item)
                {
                    LOG_ERROR("Element list ObjectAddedMessage. Object already added. " + object->getInterface<CORE::IBase>()->getName());
                    return;
                }

                SMCQt::ElementListTreeModelItem* newItem = new SMCQt::ElementListTreeModelItem;
                _populateElementTree(parentItem, newItem, object.get());
            }

        }
        else if (messageType == *CORE::IObjectMessage::ObjectRemovedMessageType)
        {
            CORE::RefPtr<CORE::IObjectMessage> omsg = message.getInterface<CORE::IObjectMessage>();
            CORE::RefPtr<const CORE::IObject> object = omsg->getObject();
            //check whether DGN Classification enable or not
            CORE::RefPtr<DGN::IDGNFolder> dgFolderObject = object->getInterface<DGN::IDGNFolder>();

            if (dgFolderObject != NULL)
            {
                _dgnLayerCountInWorld--;
                QObject* qobject = _findChild(DGNClassificationToolboxButton);
                if (qobject && !_dgnLayerCountInWorld)
                {
                    qobject->setProperty("enabled", QVariant::fromValue(false));
                }
            }

            _removeElementTree(_localLayersItem, object.get());
        }
        else if (messageType == *CORE::ISelectionComponent::ClearSelectionMessageType)
        {
            _currentSelection = NULL;
            if (_elementListTreeView)
            {
                _elementListTreeView->setProperty("currentRow", QVariant::fromValue(-1));
            }
        }
        else if (messageType == *CORE::ISelectionComponent::SelectionMessageType)
        {
            const CORE::ISelectionComponent::SelectionMap& selectionMap =
                _selectionComponent->getCurrentSelection();

            if (selectionMap.empty())
            {
                return;
            }

            CORE::RefPtr<CORE::ISelectable> selectable =
                selectionMap.begin()->second.get();

            if (selectable != _currentSelection)
            {
                _currentSelection = selectable;
                CORE::RefPtr<CORE::IObject> selectedObject =
                    selectable->getInterface<CORE::IObject>();

                if (selectedObject.valid())
                {
                    ElementListTreeModelItem* item = NULL;

                    item = _findElementTree(_localLayersItem, selectedObject);

                    if (item)
                    {
                        int index = _treeModel->indexOf(item);
                        _elementListTreeView->setProperty("currentRow", QVariant::fromValue(index));
                    }
                }
            }
        }
        else if (messageType == *CORE::IVisibilityChangedMessage::VisibilityChangedMessageType)
        {
            CORE::RefPtr<CORE::IVisibilityChangedMessage> msg = message.getInterface<CORE::IVisibilityChangedMessage>();
            CORE::RefPtr<const CORE::IObject> object = msg->getObject();

            ElementListTreeModelItem* item = NULL;
            item = _findElementTree(_localLayersItem, object);
            int index = _treeModel->indexOf(item);
            if (item)
            {
                if (item->changingCheckStatus())
                {
                    return;
                }
                _treeModel->toggleManualCheckedState(index);
                //bool objectVisibility = msg->getVisibility();
                //if(objectVisibility)
                //{
                //    _treeModel->setCheckState(item,Qt::Checked);

                //}
                //else
                //{
                //    _treeModel->setCheckState(item,Qt::Unchecked);
                //}

                /*                int index = _treeModel->indexOf(item);
                                _treeModel->emitDataChanged(index);
                                while(index)
                                ElementListTreeModelItem* parentItem  = dynamic_cast<SMCQt::ElementListTreeModelItem*> (item->parent());
                                int parentIndex = _treeModel->indexOf(parentItem);
                                _treeModel->emitDataChanged(parentIndex);
                                */     //parent 
            }
        }
        else if (messageType == *CORE::IBase::NameChangedMessageType)
        {
            IBase* base = message.getSender();

            if (base != NULL)
            {
                CORE::IObject* object = base->getInterface<CORE::IObject>();

                if (object != NULL)
                {
                    ElementListTreeModelItem* item = NULL;
                    item = _findElementTree(_localLayersItem, object);

                    if (item != NULL)
                    {
                        int index = _treeModel->indexOf(item);
                        _treeModel->emitDataChanged(index);
                    }
                }
            }
        }
        else
        {
            VizQt::GUI::update(messageType, message);
        }
    }
    void ElementListGUI::_searchElements(QString elementName)
    {
        _treeModel->customizeTree(elementName, _localLayersItem, NULL);
    }

    void ElementListGUI::_emptyItemAndDeleteChild(SMCQt::ElementListTreeModelItem *item)
    {
        if (!item)
            return;

        item->clear();
    }
    void ElementListGUI::_handleSaveButtonClicked()
    {

        CORE::RefPtr<TERRAIN::IModelObject> modelLayer = _currentSelection->getInterface<TERRAIN::IModelObject>();
        if (modelLayer && _tableModel)
        {
            std::string filename = osgDB::convertFileNameToNativeStyle(modelLayer->getUrl());
            OGRLayer* layer = _tableModel->getOGRUpdateLayer();
            layer->ResetReading();
            bool isSHPFileGenerated = UTIL::ShpFileUtils::writeOGRtoShpFile(filename, layer);
            if (isSHPFileGenerated)
            {

                modelLayer->update();
            }
            else
            {
                emit showError("Shape File Write error", "Don't have Write permission.");
            }
        }
    }
    void ElementListGUI::_handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "";
        QString formatFilter;
        QString caption;
        formatFilter = "SHP File(*.shp)";
        caption = "Save Feature";
        _fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        if (!_currentSelection.valid())
        {
            LOG_ERROR("Current object not valid.");
            return;
        }
        std::string filename = osgDB::convertFileNameToNativeStyle(_fileName.toStdString());
        if (filename == "")
        {
            showError("Invalid Filename", "Please enter a valid filename");
            return;
        }

        CORE::RefPtr<TERRAIN::IModelObject> modelLayer = _currentSelection->getInterface<TERRAIN::IModelObject>();
        std::string jsonFile = "";
        std::string layerName = "";
        bool isExportAsShpSuccess = false;
        if (modelLayer &&  _tableModel)
        {
            OGRLayer* layer = _tableModel->getOGRUpdateLayer();
            layer->ResetReading();
            isExportAsShpSuccess = UTIL::ShpFileUtils::writeOGRtoShpFile(filename, layer);
            if (isExportAsShpSuccess)
            {
                if (filename == osgDB::convertFileNameToNativeStyle(modelLayer->getUrl()))
                {
                    modelLayer->update();
                }
                else
                {
                    //SuccessFully Emported
                }
            }
            else
            {
                emit showError("Shape File Write error", "Don't have Write permission.");
            }
        }

    }
    void ElementListGUI::_itemChangedSendNotify(CORE::RefPtr<const CORE::IObject> object)
    {
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*CORE::IVisibilityChangedMessage::VisibilityChangedMessageType);
        msg->getInterface<CORE::IVisibilityChangedMessage>()->setObject(object);
        notifyObservers(*CORE::IVisibilityChangedMessage::VisibilityChangedMessageType, *msg, CORE::IObservable::SYNCHRONOUS);
    }
    void ElementListGUI::_initializeAllDefaultItems()
    {
        _localLayersItem = new SMCQt::ElementListTreeModelItem;

        _localLayersItem->setIcon(QString::fromStdString(_topElementIcon));

        _localLayersItem->setName("Layers");

        // Local Layers Item
        _localLayersItem->setCheckable(true);
        _localLayersItem->setCheckState(Qt::Checked);
        _localLayersItem->setDragEnabled(false);
    }
    void ElementListGUI::_pupulateRightClickMenu(int index)
    {
        if (_treeModel == NULL)
        {
            // Log error
            return;
        }
        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_treeModel->getItem(index));


        if (item == NULL)
        {

            return;
        }


        CORE::RefPtr<const CORE::IObject> object = item->getObject();
        if (!object.valid())
        {
            return;
        }


        QList<QObject*> rightClickListModelMenu;


        CORE::RefPtr<SMCElements::IUserFolderItem> parentFolderItem = object->getInterface<SMCElements::IUserFolderItem>();
        if (index == 0 || parentFolderItem.valid())
        {
            rightClickListModelMenu.push_back(
                new VizComboBoxElement("Add folder", QString::number(ADD_FOLDER_UID))
                );
        }



        CORE::RefPtr<const CORE::IDeletable> deletable = object->getInterface<CORE::IDeletable>();
        if (deletable.valid())
        {
            rightClickListModelMenu.push_back(
                new VizComboBoxElement("Delete", QString::number(DELETE_UID))
                );
        }

        CORE::RefPtr<const CORE::IBase> base = object->getInterface<CORE::IBase>();
        if (base.valid())
        {
            rightClickListModelMenu.push_back(
                new VizComboBoxElement("Rename", QString::number(RENAME_UID))
                );
        }

        CORE::RefPtr<CORE::IVisibilityInternal> visInternal = object->getInterface<CORE::IVisibilityInternal>();
        if (visInternal.valid())
        {
            rightClickListModelMenu.push_back(
                new VizComboBoxElement("Scale visibility", QString::number(SCALE_VISIBILITY_UID))
                );
        }

        CORE::RefPtr<DB::ISerializationProxyObject> serProxyObject = object->getInterface<DB::ISerializationProxyObject>();
        if (serProxyObject.valid())
        {
            rightClickListModelMenu.push_back(
                new VizComboBoxElement("Reload", QString::number(RELOAD_UID))
                );
        }

        CORE::RefPtr<VizQt::IQtGUIManager> qtmgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        if (qtmgr.valid())
        {
            QQmlContext* rootContext = qtmgr->getRootContext();
            if (rootContext)
            {

                rootContext->setContextProperty("RightClickListModel", QVariant::fromValue(rightClickListModelMenu));
            }
        }
    }

    void ElementListGUI::_onItemClicked(int index)
    {
        if (_treeModel == NULL)
        {
            // Log error
            return;
        }

        // returning in case of -ve index
        if (0 > index)
            return;

        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_treeModel->getItem(index));

        if (item != NULL)
        {
            _pupulateRightClickMenu(index);
            // get the type of the item
            CORE::RefPtr<const CORE::IObject> object = item->getObject();

            if (object.valid())
            {
                CORE::RefPtr<CORE::ISelectable> selectable =
                    object->getInterface<CORE::ISelectable>();

                if (!_selectionUIHandler.valid())
                {
                    _findSelectionUIHandler();
                }

                _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::SelectionMessageType);
                _unsubscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ClearSelectionMessageType);

                if (selectable.valid())
                {
                    //Setting Selection Type Gui SelectionType
                    _selectionUIHandler->setObjectAsSelected(selectable.get(), CORE::ISelectionComponent::SELECTION_TYPE::GUI_SELECTION);
                }
                else
                {
                    _selectionUIHandler->clearCurrentSelection();
                }

                _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::SelectionMessageType);
                _subscribe(_selectionComponent.get(), *CORE::ISelectionComponent::ClearSelectionMessageType);

                _currentSelection = selectable;
            }
        }
    }

    void ElementListGUI::_onItemDoubleClicked(int index)
    {
        if (_treeModel == NULL)
        {
            // Log error
            return;
        }

        if (0 > index)
            return; // return for negative index.

        // get the item
        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_treeModel->getItem(index));

        if (item != NULL)
        {
            // get the type of the item
            CORE::RefPtr<const CORE::IObject> object = item->getObject();

            if (object.valid())
            {
                jumpToObject(object->getInterface<CORE::IObject>());
            }

        }
    }

    void ElementListGUI::_onItemDeleteKeyPressed(int index)
    {
        if (_treeModel == NULL)
        {
            // Log error
            return;
        }


        if (index < 0)
            return;

        // get the item
        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_treeModel->getItem(index));

        if (item)
        {
            // get the type of the item
            CORE::RefPtr<const CORE::IObject> object = item->getObject();

            if (!object.valid())
            {
                return;
                emit showError("Delete Object", "Object not deletable.");
            }

            CORE::RefPtr<CORE::IDeletable> deletable = object->getInterface<CORE::IDeletable>();

            if (!deletable)
            {
                return;
                emit showError("Delete Object", "Object not deletable.");
            }

            //! Ask for user confirmation
            QString msg = "Are you sure you want to delete \'" + QString::fromStdString(object->getInterface<CORE::IBase>()->getName()) + "\'?";
            emit criticalQuestion("Delete Object", msg, false, "deleteObject()", "cancelDeleteObject()");

            _itemToBeDeleted = deletable;
        }
    }

    void ElementListGUI::deleteObject()
    {
        if (_itemToBeDeleted == NULL)
            return;

        CORE::RefPtr<VizUI::IDeletionUIHandler> delUIHandler = APP::AccessElementUtils::
            getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());

        if (delUIHandler.valid())
        {
            //XXX this change done for prevent transaction base deletion beacuase in Place Search when delete Folder its keep in transation 
            //so cannot verify whether deleted or not so preventing Transaction through tag 
            CORE::RefPtr<CORE::ITaggable> tagObject = _itemToBeDeleted->getInterface<CORE::ITaggable>();
            if (tagObject.valid() && tagObject->hasTag(SMCQt::SearchGUI::TagToPreventTransaction))
            {
                _itemToBeDeleted->remove();
                _selectionComponent->clearCurrentSelection();
            }
            else
            {
                delUIHandler->deleteObject(_itemToBeDeleted);
            }

        }

        _itemToBeDeleted = NULL;
    }

    void ElementListGUI::cancelDeleteObject()
    {
        _itemToBeDeleted = NULL;
    }


    void ElementListGUI::createChildFolder(QString name)
    {
        //! Get the item at index
        if (_treeModel == NULL)
            return;


        // get the type of the item
        CORE::RefPtr<SMCElements::IUserFolderItem> parentFolderItem = NULL;

        if (_currentSelection.valid())
        {
            parentFolderItem = _currentSelection->getInterface<SMCElements::IUserFolderItem>();
        }


        if (!parentFolderItem.valid())
        {

            CORE::RefPtr<CORE::IComponent> userFolderComp = ELEMENTS::GetComponentFromMaintainer("UserFolderComponent");
            if (userFolderComp.valid())
            {

                CORE::RefPtr<SMCElements::IUserFolderComponent> userFolderComponent = userFolderComp->getInterface<SMCElements::IUserFolderComponent>();
                if (userFolderComponent.valid())
                {

                    parentFolderItem = userFolderComponent->getRootFolder();
                }
            }
        }


        if (!parentFolderItem.valid())
        {
            emit showError("Add Group", "Cannot add group. Parent group not available.");
            return;
        }


        //! Create a folder object
        CORE::IObject* newFolderRef = CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*SMCElements::SMCElementsRegistryPlugin::UserFolderType);


        if (newFolderRef != NULL)
        {
            parentFolderItem->addChild(newFolderRef);
            if (!name.isEmpty())
            {

                newFolderRef->getInterface<CORE::IBase>()->setName(name.toStdString());


            }
            else
            {
                newFolderRef->getInterface<CORE::IBase>()->setName("New Group");
            }
        }

        QObject* addFolderPopupObject = _findChild(ADDFOLDERPOPUP);
        if (addFolderPopupObject)
        {
            QMetaObject::invokeMethod(addFolderPopupObject, "closePopup");
        }
    }

    void ElementListGUI::_renameObject(int index, QString name)
    {
        //! Get the item at index
        if (_treeModel == NULL)
            return;

        if (index < 0)
            return;

        // get the item
        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_treeModel->getItem(index));

        if (item)
        {

            // get the type of the item
            CORE::RefPtr<const CORE::IObject> object = item->getObject();
            if (object.valid() && !name.isEmpty())
            {
                item->setName(name);
                object->getInterface<CORE::IBase>()->setName(name.toStdString());
                _treeModel->emitDataChanged(index);

            }
        }
        QObject* itemRenamePopupObject = _findChild(ITEMRENAMEPOPUP);
        if (itemRenamePopupObject)
        {
            QMetaObject::invokeMethod(itemRenamePopupObject, "closePopup");
        }

    }

    void ElementListGUI::_openRenameItemPopup(int index)
    {
        //! Get the item at index
        if (_treeModel == NULL)
            return;

        if (index < 0)
            return;
        bool showRenamePopup = false;
        std::string objectName = "";
        // get the item
        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_treeModel->getItem(index));

        if (item)
        {
            // get the type of the item
            CORE::RefPtr<const CORE::IObject> object = item->getObject();
            if (object.valid())
            {
                CORE::RefPtr<CORE::IBase> baseObject = object->getInterface<CORE::IBase>();
                objectName = baseObject->getName();
                showRenamePopup = true;

            }
        }

        if (showRenamePopup)
        {
            QObject* elementList = _findChild(ELEMENTLIST);
            if (!elementList)
                return;

            QMetaObject::invokeMethod(elementList, "openRenamePopup");
            QObject* itemRenamePopupObject = _findChild(ITEMRENAMEPOPUP);
            if (itemRenamePopupObject)
            {
                itemRenamePopupObject->setProperty("itemName", QString::fromStdString(objectName));
                itemRenamePopupObject->setProperty("currentIndex", QVariant::fromValue(index));
                QObject::connect(itemRenamePopupObject, SIGNAL(renameObject(int, QString)), this, SLOT(_renameObject(int, QString)));

            }
        }
    }

    void ElementListGUI::reloadModelObject()
    {
        //std::string tempName = templateName.toStdString();

        if (!_currentSelection.valid())
        {
            LOG_ERROR("Current object not valid.");
            return;
        }

        CORE::IObject* modelObject = _currentSelection->getInterface<CORE::IObject>();

        SMCUI::IAddContentUIHandler* _addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());
        //_addContentUIHandler->updateModelObject(modelObject, tempName);
        _addContentUIHandler->updateModelObject(modelObject, StyleTemplateGUI::_currentSelectedJsonFile);

    }

    void ElementListGUI::_openAttributeTablePopup()
    {
        if (!_currentSelection.valid())
        {
            LOG_ERROR("Current object not valid.");
            return;
        }
        CORE::RefPtr<TERRAIN::IModelObject> modelLayer = _currentSelection->getInterface<TERRAIN::IModelObject>();
        if (modelLayer)
        {
            OGRDataSource *ogrDS = OGRSFDriverRegistrar::Open(modelLayer->getUrl().c_str(), false);
            if (ogrDS == NULL)
            {
                LOG_ERROR("Unable to create OGRDataSource");
                return;
            }
            if (ogrDS->GetLayerCount() <= 0)
            {
                LOG_ERROR("Vector Layer not valid.");
                return;
            }
            int firstLayer = 0;
            OGRLayer *layer = ogrDS->GetLayer(firstLayer);
            _tableModel = new VectorTableModel(NULL, modelLayer->getUrl());


            QObject* toolBoxObject = _findChild(TOOLBOXOBJECTNAME);
            if (!toolBoxObject)
            {
                return;
            }
            QMetaObject::invokeMethod(toolBoxObject, "openATPopup");
            QObject* OpenAttributeTablePopup = _findChild(OPENATTRIBUTETABLEPOPUP);
            if (!OpenAttributeTablePopup)
            {
                return;
            }
            CORE::RefPtr<CORE::IBase> baseObject = _currentSelection->getInterface<CORE::IBase>();
            OpenAttributeTablePopup->setProperty("fileName", QString::fromStdString(baseObject->getName()));
            for (unsigned int i = 0; i < layer->GetLayerDefn()->GetFieldCount(); i++)
            {
                std::string fieldType = "String";
                std::string fieldName = "";

                OGRFieldDefn* defn = layer->GetLayerDefn()->GetFieldDefn(i);
                fieldName = defn->GetNameRef();
                OGRFieldType type = defn->GetType();
                switch (type)
                {
                case OFTInteger:
                    fieldType = "Int";
                    break;
                case OFTReal:
                    fieldType = "Double";
                    break;
                case OFTString:
                    fieldType = "String";
                    break;
                default:
                    break;
                }
                QMetaObject::invokeMethod(OpenAttributeTablePopup, "addTableViewColumn", Q_ARG(QVariant, QVariant::fromValue(QString::fromStdString(fieldName))), Q_ARG(QVariant, QVariant::fromValue(QString::fromStdString(fieldType))), Q_ARG(QVariant, QVariant::fromValue(QString::number(i))));

            }
            OGRDataSource::DestroyDataSource(ogrDS);
            QObject::connect(OpenAttributeTablePopup, SIGNAL(browseButtonClicked()), this, SLOT(_handleBrowseButtonClicked()));
            QObject::connect(OpenAttributeTablePopup, SIGNAL(saveButtonClicked()), this, SLOT(_handleSaveButtonClicked()));
            QObject::connect(OpenAttributeTablePopup, SIGNAL(closed()), this, SLOT(_popupClosed()), Qt::UniqueConnection);
            // connectivity of model to view
            CORE::RefPtr<VizQt::IQtGUIManager> qtmgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            if (qtmgr)
            {
                QQmlContext* rootContext = qtmgr->getRootContext();
                if (rootContext)
                {
                    rootContext->setContextProperty("queryTableModel", _tableModel);
                }
            }

        }

    }
    void ElementListGUI::_popupClosed()
    {
        if (_tableModel)
        {
            _tableModel->closePopup();
            CORE::RefPtr<VizQt::IQtGUIManager> qtmgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
            QQmlContext* rootContext = qtmgr->getRootContext();
            if (rootContext)
            {
                rootContext->setContextProperty("queryTableModel", NULL);
            }
            delete(_tableModel);
        }
    }
    void ElementListGUI::reloadCurrentObject()
    {
        if (!_currentSelection.valid())
        {
            LOG_ERROR("Current object not valid.");
            return;
        }

        QObject* itemReloadPopup = _findChild(ITEMRELOADPOPUP);

        if (!itemReloadPopup)
            return;

        QString newDatasource = itemReloadPopup->property("newDataSource").toString();

        QString fileSuffix = itemReloadPopup->property("fileSuffix").toString();
        if (!fileSuffix.isEmpty())
        {
            newDatasource += "." + fileSuffix;
        }

        //! If current selection is a proxy object
        CORE::RefPtr<CORE::IProxyObject> proxyObject = _currentSelection->getInterface<CORE::IProxyObject>();
        if (proxyObject.valid())
        {
            //! Find project loader component
            CORE::IComponent* plComponent = ELEMENTS::GetComponentFromMaintainer("ProjectLoaderComponent");
            if (plComponent)
            {
                CORE::RefPtr<DB::IProjectLoaderComponent> projectLoaderComponent =
                    plComponent->getInterface<DB::IProjectLoaderComponent>();

                if (projectLoaderComponent.valid())
                {
                    //! Reset the input stream
                    CORE::RefPtr<DB::ISerializationProxyObject> serProObj = proxyObject->getInterface<DB::ISerializationProxyObject>();
                    if (serProObj.valid())
                    {
                        serProObj->setDataSource(newDatasource.toStdString());
                        serProObj->resetInputStream();
                    }

                    projectLoaderComponent->addProxyObject(proxyObject.get());
                    _isReloading = true;
                }
            }

        }

    }

    void ElementListGUI::connectItemReloadPopup()
    {
        if (!_currentSelection.valid())
            return;

        QObject* itemReloadPopup = _findChild(ITEMRELOADPOPUP);

        if (!itemReloadPopup)
            return;

        //! Get serialization proxy object interface
        CORE::RefPtr<DB::ISerializationProxyObject> serProxyObject =
            _currentSelection->getInterface<DB::ISerializationProxyObject>();
        if (!serProxyObject.valid())
            return;

        //! Find the previous data source
        std::string datasource = serProxyObject->getDataSource();

        std::string extension = osgDB::getFileExtension(datasource);
        if (extension == "tms" || extension == "wfs")
        {
            datasource = osgDB::getNameLessExtension(datasource);
            itemReloadPopup->setProperty("fileSuffix", QVariant(QString::fromStdString(extension)));
        }
        else if (extension == "mssql" || extension == "orcl")
        {
            itemReloadPopup->setProperty("queryDatasource", QVariant::fromValue(true));
        }


        itemReloadPopup->setProperty("previousDataSource", QVariant(QString::fromStdString(datasource)));
        itemReloadPopup->setProperty("newDataSource", QVariant(QString::fromStdString(datasource)));
    }

    void ElementListGUI::browseNewDataSourceButtonClicked()
    {
        QObject* itemReloadPopup = _findChild(ITEMRELOADPOPUP);

        if (!itemReloadPopup)
            return;

        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();

        QString previousDataSource = itemReloadPopup->property("previousDataSource").toString();

        QFileInfo fileInfo(previousDataSource);

        QString directory = fileInfo.absoluteDir().absolutePath();
        QString caption = "Browse for new data source";
        QString filters = "(*." + fileInfo.suffix() + ");; All(*.*)";

        QString filepath = QFileDialog::getOpenFileName(parent, caption, directory, filters);

        itemReloadPopup->setProperty("newDataSource", QVariant(filepath));

    }

    void ElementListGUI::_populateElementTree(ElementListTreeModelItem* parent, ElementListTreeModelItem* item,
        const CORE::IObject* object)
    {
        try
        {
            if (!object)
            {
                return;
            }

            //! If this is a wrapper object, populate current item according to referenced object instaed, and return!!
            if (object->getInterface<SMCElements::IUserFolderObjectWrapper>())
            {
                const CORE::IObject* referencedObject = object->getInterface<SMCElements::IUserFolderObjectWrapper>()->getReferencedObject();

                //! remove this object from element list if present!!
                _removeElementTree(_localLayersItem, referencedObject);

                _populateElementTree(parent, item, referencedObject);

                return;
            }

            item->setDragEnabled(true);

            // Go through the list of objects and populate the tree

            // Set icon and name based on object type and name
            // Check the type of object and set the icon accordingly
            // XXX - Make this configurable
            QString iconName = "";
            CORE::NamedAttribute* attr = NULL;
            if (object->getInterface<TERRAIN::IRasterObject>())
            {
                attr = _elementIcons->getAttribute(TERRAIN::IRasterObject::getInterfaceName());

                //! In case of raster objects, Dont show the Base raster.
                //! Just check the raster ndex, skip current item if the index is zero.
                TERRAIN::IRasterObject* rasterObject = object->getInterface<TERRAIN::IRasterObject>();
                if (rasterObject != NULL)
                {
                    if (rasterObject->getRasterIndex() == 0)
                    {
                        //! Skip
                        delete item;
                        return;
                    }
                }
                else
                {
                    LOG_ERROR("Bad Code!!!");
                }
            }
            else if (object->getInterface<TERRAIN::IElevationObject>())
            {
                attr = _elementIcons->getAttribute(TERRAIN::IElevationObject::getInterfaceName());
            }
            else if (object->getInterface<ELEMENTS::IGeoVrmlObject>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::IGeoVrmlObject::getInterfaceName());
            }
            else if (object->getInterface<CORE::IFeatureObject>())
            {
                item->setDragEnabled(false);

                attr = _elementIcons->getAttribute(CORE::IFeatureObject::getInterfaceName());

                //special handling for the result layer
                CORE::IFeatureLayer *featureLayer = object->getInterface<CORE::IFeatureLayer>();
                if (featureLayer)
                {
                    item->setDragEnabled(true);

                    if (featureLayer->getFeatureLayerType() == CORE::IFeatureLayer::RESULT)
                    {
                        attr = _elementIcons->getAttribute(CORE::ICompositeObject::getInterfaceName());
                    }
                }

                //! Special handling for tactical symbols too,..!!
                TACTICALSYMBOLS::ITacticalSymbol* tacSymbol = object->getInterface<TACTICALSYMBOLS::ITacticalSymbol>();
                if (tacSymbol)
                {
                    item->setDragEnabled(true);
                }
            }
            else if (object->getInterface<ELEMENTS::ILOSObject>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::ILOSObject::getInterfaceName());
            }
            else if (object->getInterface<ELEMENTS::ITINObject>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::ITINObject::getInterfaceName());
            }
#ifdef WIN32
            else if (object->getInterface<GPS::IGPSSource>())
            {
                attr = _elementIcons->getAttribute(GPS::IGPSSource::getInterfaceName());
            }
#endif //WIn32
            else if (object->getInterface<ELEMENTS::IRadioLOSObject>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::IRadioLOSObject::getInterfaceName());
            }
            else if (object->getInterface<ELEMENTS::IFireObject>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::IFireObject::getInterfaceName());
            }
            else if (object->getInterface<ELEMENTS::IRadioCoverageObject>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::IRadioCoverageObject::getInterfaceName());
            }
            else if (object->getInterface<CORE::ICompositeObject>())
            {
                attr = _elementIcons->getAttribute(CORE::ICompositeObject::getInterfaceName());
            }
            else if (object->getInterface<ELEMENTS::IModelResult>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::IModelResult::getInterfaceName());
            }
            else if (object->getInterface<ELEMENTS::IDestructionZone>())
            {
                attr = _elementIcons->getAttribute(ELEMENTS::IDestructionZone::getInterfaceName());
            }
            else if (object->getInterface<CORE::IFeatureLayer>())
            {
                attr = _elementIcons->getAttribute(CORE::IFeatureLayer::getInterfaceName());
            }
            else if (object->getInterface<GIS::IFeatureLayer>())
            {
                attr = _elementIcons->getAttribute(GIS::IFeatureLayer::getInterfaceName());
            }
            else if (object->getInterface<TERRAIN::IWMSRasterLayer>())
            {
                attr = _elementIcons->getAttribute(TERRAIN::IWMSRasterLayer::getInterfaceName());
            }
            else if (object->getInterface<SMCElements::IUserFolderItem>())
            {
                attr = _elementIcons->getAttribute(SMCElements::IUserFolderItem::getInterfaceName());
            }
            else if (object->getInterface<DB::ISerializationProxyObject>())
            {

                CORE::RefPtr<CORE::IProxyObject> serProObj = object->getInterface<CORE::IProxyObject>();
                if (serProObj.valid())
                {
                    if (serProObj->getStatus() == CORE::IProxyObject::ERROR_IN_LOADING)
                    {
                        attr = _elementIcons->getAttribute(DB::ISerializationProxyObject::getInterfaceName() + "_Error");
                    }
                    else
                    {
                        attr = _elementIcons->getAttribute(DB::ISerializationProxyObject::getInterfaceName());
                    }
                }
            }
            else if (object->getInterface<TERRAIN::IModelObject>())
            {
                attr = _elementIcons->getAttribute(TERRAIN::IModelObject::getInterfaceName());
            }
            else if (object->getInterface<GIS::IDynamicProfileObject>())
            {
                attr = _elementIcons->getAttribute(GIS::IDynamicProfileObject::getInterfaceName());
            }
            // Check whether the attribute was defined or not
            if (attr)
            {
                iconName = QString::fromStdString(attr->toString());
            }
            else
            {
                iconName = QString("../../gui/IconsSets/DSM_iconSet_Dark/ElementListIcons/missing.png");
            }
            item->setIcon(iconName);

            CORE::IVisibility* checkable = object->getInterface<CORE::IVisibility>();
            if (checkable)
            {
                item->setCheckable(true);

                // XXX - need to check partial check condition
                if (checkable->getVisibility() && (parent->getCheckState() != Qt::Unchecked))
                {
                    item->setCheckState(Qt::Checked);
                }
                else
                {
                    item->setCheckState(Qt::Unchecked);
                }
            }
            else
            {
                item->setCheckable(false);
            }

            item->setObject(object);

            // Add to the parent
            parent->addItem(item);

            if (parent->getChildCount() == 1)
            {
                _treeModel->emitDataChanged(_treeModel->indexOf(parent));
            }

            // refresh model
            if (parent->isOpened())
            {
                int index = _treeModel->indexOf(parent);
                _treeModel->closeItem(index);
                _treeModel->openItem(index);
            }

            // subscribe to name changed message
            CORE::RefPtr<CORE::IBase> base = object->getInterface<CORE::IBase>();
            _subscribe(base.get(), *CORE::IBase::NameChangedMessageType);

            // check if the object is a composite object
            CORE::RefPtr<CORE::ICompositeObject> composite =
                object->getInterface<CORE::ICompositeObject>();

            if (composite.valid())
            {
                CORE::RefPtr<CORE::IFeatureLayer> layer = composite->getInterface<CORE::IFeatureLayer>();
                // only add childs for non-feature layer composite objects
                if (!layer.valid() || _showLayerChild)
                {
                    //XXX - we should have support for global messages
                    // Subscribe to object removed and added messages
                    _subscribe(composite.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
                    _subscribe(composite.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);

                    CORE::ICompositeObject::ObjectMap::const_iterator iter =
                        composite->getObjectMap().begin();

                    // add a item for each object in composite object
                    while (iter != composite->getObjectMap().end())
                    {
                        ElementListTreeModelItem* newItem = new ElementListTreeModelItem;
                        _populateElementTree(item, newItem, iter->second.get());
                        ++iter;
                    }
                }
                else if (layer.valid() && layer->getFeatureLayerType() == CORE::IFeatureLayer::RESULT)
                {
                    _subscribe(composite.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
                    _subscribe(composite.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);

                    CORE::ICompositeObject::ObjectMap::const_iterator iter =
                        composite->getObjectMap().begin();

                    // add a item for each object in composite object
                    while (iter != composite->getObjectMap().end())
                    {
                        ElementListTreeModelItem* newItem = new ElementListTreeModelItem;
                        _populateElementTree(item, newItem, iter->second.get());
                        ++iter;
                    }
                }
            }

            CORE::RefPtr<SMCElements::IUserFolderItem> userFolder = object->getInterface<SMCElements::IUserFolderItem>();
            if (userFolder.valid())
            {
                SMCElements::IUserFolderItem::UserFolderCollection::UIDBaseMap::const_iterator iter = userFolder->getChildrenList().begin();
                while (iter != userFolder->getChildrenList().end())
                {
                    ElementListTreeModelItem* newItem = new ElementListTreeModelItem;
                    _populateElementTree(item, newItem, iter->second.get());
                    ++iter;
                }
            }

        }
        catch (const UTIL::ReturnValueNullException& e)
        {
            e.LogException();
        }
    }


    bool ElementListGUI::_removeElementTree(ElementListTreeModelItem* item, const CORE::IObject* object)
    {
        ElementListTreeModelItem* removedItem = _findElementTree(item, object);
        if (removedItem)
        {
            _treeModel->removeItem(removedItem);
            _unsubscribeMessage(object);

            return true;
        }

        return false;
    }

    void ElementListGUI::_unsubscribeMessage(const CORE::IObject* object)
    {
        CORE::RefPtr<CORE::IBase> base = object->getInterface<CORE::IBase>();
        _unsubscribe(base.get(), *CORE::IBase::NameChangedMessageType);

        CORE::RefPtr<CORE::ICompositeObject> composite = object->getInterface<CORE::ICompositeObject>();

        if (composite.valid())
        {
            CORE::RefPtr<CORE::IFeatureLayer> layer = composite->getInterface<CORE::IFeatureLayer>();
            // only add childs for non-feature layer composite objects
            if (!layer.valid() || _showLayerChild)
            {
                //XXX - we should have support for global messages
                // Subscribe to object removed and added messages
                _unsubscribe(composite.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
                _unsubscribe(composite.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);

                CORE::ICompositeObject::ObjectMap::const_iterator iter =
                    composite->getObjectMap().begin();

                // unsubscribe message for each object in composite object
                while (iter != composite->getObjectMap().end())
                {
                    _unsubscribeMessage(iter->second.get());
                    ++iter;
                }
            }
            else if (layer.valid() && layer->getFeatureLayerType() == CORE::IFeatureLayer::RESULT)
            {
                _unsubscribe(composite.get(), *CORE::IObjectMessage::ObjectAddedMessageType);
                _unsubscribe(composite.get(), *CORE::IObjectMessage::ObjectRemovedMessageType);

                CORE::ICompositeObject::ObjectMap::const_iterator iter =
                    composite->getObjectMap().begin();

                // unsubscribe message for each object in composite object
                while (iter != composite->getObjectMap().end())
                {
                    _unsubscribeMessage(iter->second.get());
                    ++iter;
                }
            }
        }
    }

    ElementListTreeModelItem* ElementListGUI::_findElementTree(ElementListTreeModelItem* item, const CORE::IObject* object)
    {
        if (!object || !item)
        {
            return NULL;
        }

        const CORE::IObject* curObject = item->getObject();
        if (curObject == object)
        {
            return item;
        }

        foreach(QMLTreeModelItem* childItem, item->getChildren())
        {
            ElementListTreeModelItem* child = dynamic_cast<ElementListTreeModelItem*>(childItem);

            ElementListTreeModelItem* element = _findElementTree(child, object);
            if (element)
            {
                return element;
            }
        }
        return NULL;
    }

    void ElementListGUI::_findSelectionUIHandler()
    {
        // get selection UI Handler
        CORE::RefPtr<APP::IManager> managr = _manager->getInterface<APP::IManager>();

        if (managr.valid())
        {
            CORE::RefPtr<APP::IApplication> app = managr->getApplication();

            if (app.valid())
            {
                CORE::RefPtr<APP::IManager> uiIMgr =
                    app->getManagerByClassname(VizUI::UIHandlerManager::ClassNameStr);
                if (uiIMgr.valid())
                {
                    CORE::RefPtr<APP::IUIHandlerManager> uiHandlerMgr =
                        uiIMgr->getInterface<APP::IUIHandlerManager>();
                    if (uiHandlerMgr.valid())
                    {
                        _selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler,
                            APP::IGUIManager>(getGUIManager());
                    }
                }
            }
        }
    }

    void ElementListGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();

        std::string group = "ElementListGUI Attributes";

        // Add element icon
        _addAttribute(new CORE::GroupAttribute("ElementIcons", "ElementIcons",
            CORE::GroupAttribute::SetFuncType(this, &ElementListGUI::setElementIcons),
            CORE::GroupAttribute::GetFuncType(this, &ElementListGUI::getElementIcons),
            "Icons of objects",
            group));

        // Add top element icon
        _addAttribute(new CORE::StringAttribute("TopElementIcon", "TopElementIcon",
            CORE::StringAttribute::SetFuncType(this, &ElementListGUI::setTopLevelIcon),
            CORE::StringAttribute::GetFuncType(this, &ElementListGUI::getTopLevelIcon),
            "Icon of top element",
            group));

        // Add attribute to whether show layer childs or not
        _addAttribute(new CORE::BooleanAttribute("ShowLayerChild", "ShowLayerChild",
            CORE::BooleanAttribute::SetFuncType(this, &ElementListGUI::_setShowLayerChild),
            CORE::BooleanAttribute::GetFuncType(this, &ElementListGUI::_getShowLayerChild),
            "Show the layer childs",
            group));
    }

    void ElementListGUI::setElementIcons(const CORE::NamedGroupAttribute& groupattr)
    {
        _elementIcons->copyFrom(groupattr);
    }

    CORE::RefPtr<CORE::NamedGroupAttribute>
        ElementListGUI::getElementIcons() const
    {
        return _elementIcons;
    }

    void ElementListGUI::setTopLevelIcon(const std::string& iconfile)
    {
        _topElementIcon = iconfile;
    }

    const std::string&
        ElementListGUI::getTopLevelIcon() const
    {
        return _topElementIcon;
    }

    bool ElementListGUI::_getShowLayerChild() const
    {
        return _showLayerChild;
    }

    void ElementListGUI::_setShowLayerChild(bool value)
    {
        _showLayerChild = value;
    }

    void ElementListGUI::_handleRasterItemChanged(int itemIndex, int to)
    {
        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_treeModel->getItem(itemIndex));

        if (item)
        {
            CORE::RefPtr<const CORE::IObject> object = item->getObject();
            if (object.valid())
            {
                CORE::RefPtr<TERRAIN::IRasterObject> raster =
                    object->getInterface<TERRAIN::IRasterObject>();
                if (raster.valid())
                {
                    raster->setRasterIndex(to);
                }
            }
        }
    }
    void ElementListGUI::_rasterMove(int positionOption)
    {
        if (_currentSelection.valid())
        {
            CORE::RefPtr<const CORE::IObject> object = _currentSelection->getInterface<CORE::IObject>();
            if (object.valid())
            {
                CORE::RefPtr<TERRAIN::IRasterObject> raster = object->getInterface<TERRAIN::IRasterObject>();
                if (raster.valid())
                {
                    int rasIndex = raster->getRasterIndex();
                    osg::ref_ptr<const osgEarth::Map> map = ELEMENTS::GetMapFromTerrain();
                    switch (positionOption)
                    {
                    case 0:             //! Top
                        rasIndex = map->getNumImageLayers() - 1;
                        break;
                    case 1:             //! Up
                        rasIndex++;
                        break;
                    case 2:             //! Down
                        if (rasIndex > 1)
                            rasIndex--;
                        break;
                    case 3:             //! Bottom
                        rasIndex = 1;
                        break;
                    }
                    raster->setRasterIndex(rasIndex);
                    ////! Close the popup!!
                    //QObject* toolBoxObject = _findChild(TOOLBOXOBJECTNAME);
                    //if (toolBoxObject)
                    //{
                    //    QMetaObject::invokeMethod(toolBoxObject, "closeReorderRasterPopup");
                    //  
                    //}
                    return;
                }
            }



        }
    }

} // namespace SMCQt

