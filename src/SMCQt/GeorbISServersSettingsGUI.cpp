/****************************************************************************
*
* File             : GeorbISServersSettingsGUI.h
* Description      : GeorbISServersSettingsGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/GeorbISServersSettingsGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/AccessElementUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>

#include <Elements/ElementsUtils.h>

#include <Util/FileUtils.h>

#include <QFileDialog>
#include <QSettings>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>

#include <Util/FileUtils.h>
#include <QJSONDocument>
#include <QJSONObject>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, GeorbISServersSettingsGUI);
    DEFINE_IREFERENCED(GeorbISServersSettingsGUI, DeclarativeFileGUI);

    const std::string GeorbISServersSettingsGUI::GeorbISSettingsMenu = "georbISSettingsMenu";
    const std::string GeorbISServersSettingsGUI::GeorbISServerJsonLocation = "/GeorbISServerList/";

    GeorbISServersSettingsGUI::GeorbISServersSettingsGUI() : _featureExportUIHandler(NULL)
    {
    }

   GeorbISServersSettingsGUI::~GeorbISServersSettingsGUI()
    {
    }

    void GeorbISServersSettingsGUI::setActive(bool value)
    {
        if (getActive() == value)
            return;

        if (value)
        {
            QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);
            if (GeorbISSettingsObject)
            {
             
                _populateGeorbISServers();

                QObject::connect(GeorbISSettingsObject, SIGNAL(saveGeorbISServerParameters()), this,
                    SLOT(_saveButtonClicked()), Qt::UniqueConnection);

                QObject::connect(GeorbISSettingsObject, SIGNAL(deleteGeorbISServer(QString)), this,
                    SLOT(_deleteGeorbISServer(QString)), Qt::UniqueConnection);

                QObject::connect(GeorbISSettingsObject, SIGNAL(comboBoxClicked(QString)), this,
                    SLOT(_comboBoxClicked(QString)), Qt::UniqueConnection);

                QObject::connect(GeorbISSettingsObject, SIGNAL(testGeorbISServerConnection()), this,
                    SLOT(_testGeorbISServerConnection()), Qt::UniqueConnection);

            }
        }
        else
        {
            _georbISServersInstanceList.clear();
            _featureExportUIHandler = NULL;
            _serverUrl="";

            QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);
            if (GeorbISSettingsObject)
            {
                GeorbISSettingsObject->setProperty("testResultVisible", false);
            }

        }

        DeclarativeFileGUI::setActive(value);
    }

    void GeorbISServersSettingsGUI::_populateGeorbISServers()
    {
        _readAllGeorbISServerInstanceName();

        int count = 2;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select GeorbIS Server Instance", "0"));
        comboBoxList.append(new VizComboBoxElement("Create New GeorbIS Server Instance", "1"));

        for (int i = 0; i < _georbISServersInstanceList.size(); i++)
        {
            std::string georbISServerName = _georbISServersInstanceList[i];
            comboBoxList.append(new VizComboBoxElement(georbISServerName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("georbISServerList", QVariant::fromValue(comboBoxList));
    }

    void GeorbISServersSettingsGUI::_readAllGeorbISServerInstanceName()
    {
        _georbISServersInstanceList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string georbISServerFolder = appdataPath + GeorbISServerJsonLocation;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (!osgDB::fileExists(path))
        {
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _georbISServersInstanceList.push_back(fileNameWithoutExtension);
        }
    }

    bool GeorbISServersSettingsGUI::_getGeorbISServer(std::string georbISServer)
    {
  
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string georbISServerFolder = appdataPath + GeorbISServerJsonLocation;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (!osgDB::fileExists(path))
        {
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);
        std::string georbISServerExtension;
        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == georbISServer)
            {
                found = true;
                georbISServerExtension = osgDB::getLowerCaseFileExtension(currFileName);
            }
        }

        if (!found)
        {
            return false;
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + GeorbISServerJsonLocation + georbISServer + "." + georbISServerExtension));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                emit showError("Read setting", "Can not open file for reading", true);
                return false;
            }

            QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
            if (jdoc.isNull())
            {
                return false;
            }

            QJsonObject obj = jdoc.object();
 
            _serverParameter.host =   obj["Host"].toString().toStdString();          
            return true;
        }
        return false;
    }

   void GeorbISServersSettingsGUI::_testGeorbISServerConnection()
    {
        QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);

        if (GeorbISSettingsObject)
        {
            QString georbISServerHost = GeorbISSettingsObject->property("host").toString();

            if (georbISServerHost == "")
            {
                emit  showError("Cannot Test", "Please give the required parameters for testing");
                return;
            }
            _serverUrl="";
            _serverUrl = georbISServerHost.toStdString();

            if (_connectWithGeorbISServer())
            {
                GeorbISSettingsObject->setProperty("status", QString::fromStdString("GeorbIS Server Connection Successful"));
                GeorbISSettingsObject->setProperty("testResultVisible", true);
            }

            else
            {
                GeorbISSettingsObject->setProperty("status", QString::fromStdString("GeorbIS Server Connection Not Successful"));
                GeorbISSettingsObject->setProperty("testResultVisible", true);
            }
        }
    }


   bool GeorbISServersSettingsGUI::_connectWithGeorbISServer()
   {
       _featureExportUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager <SMCUI::IFeatureExportUIHandler>(getGUIManager());
       QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);
       QString georbISServerType;

       if (GeorbISSettingsObject)
       {
           georbISServerType = GeorbISSettingsObject->property("georbISServerTypeValue").toString();
       }
       if (_serverUrl=="")
            return false;

       std::string georbISServerStatus;
       if (_featureExportUIHandler->georbISServerCapability(_serverUrl))
       {
           return true;
       }
       else
       {
           return false;
       }

       return false;
   }

    void GeorbISServersSettingsGUI::_saveButtonClicked()
    {
        QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);
        if (GeorbISSettingsObject)
        {
            QString georbISServerName = GeorbISSettingsObject->property("georbISServerName").toString();
            QString georbISServerHost = GeorbISSettingsObject->property("host").toString();

            //values.push_back(georbISServerFolderName);
            if (georbISServerHost == "" || georbISServerName == "")
            {
                emit  showError("Cannot Save", "Please give the required parameters for saving");
                return;
            }

            bool isExist = _getGeorbISServer(georbISServerName.toStdString());
            if (isExist)
            {
                emit showError("Change Server name", "GeorbIS Server with this name already exists", true);
                return;
            }


            _writeGeorbISServerFile(georbISServerName, georbISServerHost);
            _readAllGeorbISServerInstanceName();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;
            comboBoxList.append(new VizComboBoxElement("Select GeorbIS Server Instance", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New GeorbIS Server Instance", "1"));

            for (int i = 0; i < _georbISServersInstanceList.size(); i++)
            {
                std::string georbISServerName = _georbISServersInstanceList[i];
                comboBoxList.append(new VizComboBoxElement(georbISServerName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("georbISServerList", QVariant::fromValue(comboBoxList));

            if (_georbISServersInstanceList.size() > 0)
            {
                GeorbISSettingsObject->setProperty("value", georbISServerName);
            }

        }
    }


    void GeorbISServersSettingsGUI::_deleteGeorbISServer(QString value)
    {
        
        QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);

        if (value.toStdString() == "Select GeorbIS Server Instance" || value.toStdString() == "Create New GeorbIS Server Instance")
        {
            emit  showError("Not a valid input", "Cannot delete GeorbIS Server with this name");
            return;
        }
        emit question("Delete georbIS Server  ", "Do you really want to delete?", false,
            "_okSlotForDeleting()", "_cancelSlotForDeleting()");

    }

    void GeorbISServersSettingsGUI::_okSlotForDeleting()
    {
        QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string georbISServerFolder = appdataPath + GeorbISServerJsonLocation;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        if (GeorbISSettingsObject)
        {
            QString lpName = GeorbISSettingsObject->property("value").toString();
            std::string extension=".server";

            if (lpName == "")
            {
                return;
            }

            std::string filename = lpName.toStdString() + extension;
            std::string filePath = georbISServerFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, georbISServerFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
                //  std::cout << "Deleted GeorbIS Server File from " << georbISServerFolder << std::endl;
            }

            _readAllGeorbISServerInstanceName();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select GeorbIS Server Instance", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New GeorbIS Server Instance", "1"));

            for (int i = 0; i < _georbISServersInstanceList.size(); i++)
            {
                std::string georbISServerName = _georbISServersInstanceList[i];
                comboBoxList.append(new VizComboBoxElement(georbISServerName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("georbISServerList", QVariant::fromValue(comboBoxList));

            std::string defaultName = "Select GeorbIS Server Instance";
            GeorbISSettingsObject->setProperty("value", QString::fromStdString(defaultName));
        }

    }

    void GeorbISServersSettingsGUI::_cancelSlotForDeleting()
    {
        return;
    }

    void GeorbISServersSettingsGUI::_writeGeorbISServerFile(QString name, QString values)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::stringstream insertStatement;

        std::string georbISServerFolder = appdataPath + GeorbISServerJsonLocation;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }

        std::string fileName;
        fileName = "/" + name.toStdString() + ".server";
        

        QFile settingFile(QString::fromStdString(georbISServerFolder + fileName));

        if (!settingFile.open(QIODevice::WriteOnly))
        {
            emit showError("Save setting", "Could not open georbIS Server file for save");
            return;
        }

        QJsonObject settingObject;
        settingObject["Host"] = values;
        
        QJsonDocument settingDoc(settingObject);
        settingFile.write(settingDoc.toJson());
    }


    void GeorbISServersSettingsGUI::_comboBoxClicked(QString value)
    {
        if (value.toStdString() == "Select GeorbIS Server Instance"  )
        {
            return;
        }

        else if (value.toStdString() == "Create New GeorbIS Server Instance")
        {
            QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);
            if (GeorbISSettingsObject)
            {
                GeorbISSettingsObject->setProperty("georbISServerName", "");
                GeorbISSettingsObject->setProperty("host", "");
            }
        }

        else
        {
            bool isExist = _getGeorbISServer(value.toStdString());
            
            if (!isExist)
            {
                emit showError("GeorbIS Server invalid", "GeorbIS Server file not found", true);
                return;
            }

            QObject* GeorbISSettingsObject = _findChild(GeorbISSettingsMenu);
            if (GeorbISSettingsObject)
            {
                GeorbISSettingsObject->setProperty("georbISServerName", value);
                GeorbISSettingsObject->setProperty("host", QString::fromStdString(_serverParameter.host));
                
            }

        }
    }

    void GeorbISServersSettingsGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                //_subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

} // namespace SMCQt