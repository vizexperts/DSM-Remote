/****************************************************************************
*
* File             : ColorCodedElevGUI.h
* Description      : ColorCodedElevGUI class definition
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/RadioLOSGUI.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <SMCUI/SMCUIPlugin.h>
#include <Core/Point.h>
#include <Core/CoreRegistry.h>
#include <Util/StringUtils.h>
#include <Elements/IconModelLoaderComponent.h>
#include <Elements/IIconHolder.h>
#include <Elements/ElementsPlugin.h>
#include <GISCompute/IFilterStatusMessage.h>
#include <QtCore/QVariant>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Util/FileUtils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, RadioLOSGUI);
    DEFINE_IREFERENCED(RadioLOSGUI, DeclarativeFileGUI);


    const std::string RadioLOSGUI::TransIconPath                = "/Symbols/Transmitter.png";
    const std::string RadioLOSGUI::RecvIconPath                 = "./Symbols/Receiver.png";

    const std::string RadioLOSGUI::RadioLOSAnalysisObjectName   = "RadioLOSAnalysis";



    RadioLOSGUI::RadioLOSGUI()
        : _state(NO_POS_MARK)
    {}

    RadioLOSGUI::~RadioLOSGUI()
    {}

    void RadioLOSGUI::connectRadioAnalysisLoader()
    {
        _radioLOSObject = _findChild(RadioLOSAnalysisObjectName);

        if(_radioLOSObject){

            // mark, start and stop button handlers
            connect(_radioLOSObject, SIGNAL(startAnalysis()),this,
                SLOT(_handlePBStartClicked()),Qt::UniqueConnection);

            connect(_radioLOSObject, SIGNAL(stopAnalysis()),this, 
                SLOT(_handlePBStopClicked()),Qt::UniqueConnection);

            connect(_radioLOSObject, SIGNAL(getDecisionSupport()),this, 
                SLOT(_handlePBGetDecisionButtonClicked()),Qt::UniqueConnection);

            connect(_radioLOSObject, SIGNAL(markRecvPosition(bool)),this, 
                SLOT(_handlePBMarkRecvPosClicked(bool)),Qt::UniqueConnection);

            connect(_radioLOSObject, SIGNAL(markTransPosition(bool)),this,
                SLOT(_handlePBMarkTransPosClicked(bool)),Qt::UniqueConnection);

            // Connect transmitter/receiver pos mark button

            connect(this, SIGNAL(_getDecisionButtonEnable(bool)), this ,
                SLOT(_setGetDecisionButtonEnable(bool)), Qt::QueuedConnection);

            connect(this, SIGNAL(_startButtonEnable(bool)), this , SLOT(_setStartButtonEnable(bool)), Qt::QueuedConnection);
            connect(this, SIGNAL(_stopButtonEnable(bool)), this , SLOT(_setStopButtonEnable(bool)), Qt::QueuedConnection);

            connect(this, SIGNAL(_showProgressBar(bool)), this, 
                SLOT(_onShowProgressBar(bool)),Qt::QueuedConnection);
            connect(this, SIGNAL(_transPosMarkButtonEnable(bool)), this, 
                SLOT(_setTransPosMarkButtonEnable(bool)), Qt::QueuedConnection);
            connect(this, SIGNAL(_recvPosMarkButtonEnable(bool)),this,
                SLOT(_setRecvPosMarkButtonEnable(bool)), Qt::QueuedConnection);

            connect(this, SIGNAL(_updateOutputFields(bool)), this , SLOT(_onUpdateOutputFields(bool)), Qt::QueuedConnection);
        }
    }

    void RadioLOSGUI::setActive(bool value)
    {
        if(getActive() == value)
            return;

        try{
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
                _state= NO_POS_MARK;

                connectRadioAnalysisLoader();
            }
            else
            {
                _removeObject(_transPos.get());
                _removeObject(_recvPos.get());
                _state = NO_POS_MARK;

                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);

                _setPointHandlerEnabled(false);

                //stop filter
                _uiHandler->stopFilter();
            }
            _uiHandler->getInterface<APP::IUIHandler>()->setFocus(value);
        }
        catch(const UTIL::Exception& e){
            e.LogException();
        }

        DeclarativeFileGUI::setActive(value);
    }

    void RadioLOSGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded

        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IPointUIHandler>(getGUIManager());
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IRadioLOSUIHandler>(getGUIManager());

                // load radar icon 
                APP::IGUIManager* guiMgr = getGUIManager();
                CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(guiMgr);

                if(!wmain)
                {
                    return;
                }

                CORE::IComponent* comp = wmain->getComponentByName("IconModelLoaderComponent");
                if(!comp)
                {
                    return;
                }

                ELEMENTS::IIconLoader* iconLoader = comp->getInterface<ELEMENTS::IIconLoader>();
                if(!iconLoader)
                {
                    return;
                }
                std::string dataDir =UTIL::getDataDirPath();

                std::string transIconName = "TransIcon";
                iconLoader->loadIcon(transIconName, dataDir + TransIconPath);
                _transIcon = iconLoader->getIconByName(transIconName);

                std::string recvIconName = "RecvIcon";
                iconLoader->loadIcon(recvIconName, dataDir + RecvIconPath);
                _recvIcon = iconLoader->getIconByName(recvIconName);

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {
            GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();

            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();

                // do not update it to 100%
                if(progress < 100)
                {
                    //emit _setProgressValue(progress);
                }
            }
            else
            {
                // set value to 100%
                if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
                {
                    emit _showProgressBar(false);
                    emit _updateOutputFields(true);
                }
                else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    emit showError("Filter Status", "Failed to apply filter");
                    emit _updateOutputFields(false);
                }

                emit _showProgressBar(false);
                emit _startButtonEnable(true);
                emit _stopButtonEnable(false);
                emit _transPosMarkButtonEnable(true);
                emit _recvPosMarkButtonEnable(true);
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *VizUI::IPointUIHandler::PointCreatedMessageType)
        {
            _processPoint();
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // signal handlers for enabling and disabling buttons on GUI popup
    void RadioLOSGUI::_setStartButtonEnable(bool value)
    {
        _radioLOSObject->setProperty("isStartButtonEnabled",value);
    }

    void RadioLOSGUI::_setStopButtonEnable(bool value)
    {
        _radioLOSObject->setProperty("isStopButtonEnabled",value);
    }

    void RadioLOSGUI::_setTransPosMarkButtonEnable(bool value)
    {
        _radioLOSObject->setProperty("isMarkTransEnabled",value);
    }

    void RadioLOSGUI::_setRecvPosMarkButtonEnable(bool value)
    {
        _radioLOSObject->setProperty("isMarkRecvEnabled",value);
    }

    void RadioLOSGUI::_setGetDecisionButtonEnable(bool value)
    {
        _radioLOSObject->setProperty("isGetDecisionEnabled",value);
    }

    void RadioLOSGUI::_onShowProgressBar(bool value)
    {
        _radioLOSObject->setProperty("isProgressBarVisible",value);
    }


    // signal handlers of "Mark" buttons 
    void RadioLOSGUI::_handlePBMarkRecvPosClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = RECV_POS_MARK;
            _radioLOSObject->setProperty("isMarkTransSelected", false);
            if(TRANS_POS_MARK == lastState)
            {
                //                emit _transPosMarkButtonEnable(false);
            }
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(RECV_POS_MARK == lastState)
            {   
                _state = NO_POS_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }
        _getDecisionButtonEnable(false);
    }

    void RadioLOSGUI::_handlePBMarkTransPosClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state= TRANS_POS_MARK;
            _radioLOSObject->setProperty("isMarkRecvSelected", false);
            if(RECV_POS_MARK == lastState)
            {
                //               emit _recvPosMarkButtonEnable(false);
            }
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(TRANS_POS_MARK == lastState)
            {
                _state = NO_POS_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }
        _getDecisionButtonEnable(false);
    }

    void RadioLOSGUI::_setPointHandlerEnabled(bool enable)
    {
        if(_pointHandler.valid())
        {
            if(enable)
            {
                _pointHandler->setProcessMouseEvents(true);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);                
            }
            else
            {
                _pointHandler->setProcessMouseEvents(false);
                _pointHandler->setHandleMouseClicks(false);
                _pointHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark trans/recv position");
        }
    }


    void RadioLOSGUI::_processPoint()
    {

        try
        {
            if(!_pointHandler.valid())
            {
                throw UTIL::Exception("Point UI handler not valid", __FILE__, __LINE__);
            }

            CORE::IPoint* point = _pointHandler->getPoint();
            QString pointX, pointY, pointZ;

            pointX = QString::number(point->getX());
            pointY = QString::number(point->getY());
            pointZ = QString::number(point->getZ());

            if(point == NULL)
            {
                throw UTIL::Exception("Point is not valid", __FILE__, __LINE__);
            }

            ELEMENTS::IIconHolder* iconHolder = point->getInterface<ELEMENTS::IIconHolder>();
            if(_pointHandler.valid())
            {
                switch(_state)
                {
                case TRANS_POS_MARK: 
                    {
                        _setTransPos(point);

                        _radioLOSObject->setProperty("transLongitude",pointX);
                        _radioLOSObject->setProperty("transLatitude",pointY);
                        _radioLOSObject->setProperty("transAltitude",pointZ);

                        if(_transIcon.valid() && iconHolder)
                        {
                            iconHolder->setIcon(_transIcon.get());
                        }
                        else
                        {
                            LOG_DEBUG("Default Icon for Transmitter is used")
                        }

                        break;
                    }

                case RECV_POS_MARK: 
                    {
                        _setRecvPos(point);

                        _radioLOSObject->setProperty("recvLongitude",pointX);
                        _radioLOSObject->setProperty("recvLatitude",pointY);
                        _radioLOSObject->setProperty("recvAltitude",pointZ);


                        if(_recvIcon.valid() && iconHolder)
                        {
                            iconHolder->setIcon(_recvIcon.get());
                        }
                        else
                        {
                            LOG_DEBUG("Default Icon for Receiver is used")
                        }
                        break;
                    }
                default:
                    break;

                }
            }
        }
        catch(...)
        {
        }
    }



    void RadioLOSGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        try
        {
            //disable start button
            //enable stop button
            //disable Decision Support button
            // disable Mark Buttons
            // show progress bar

            _radioLOSObject->setProperty("isMarkTransSelected", false);
            _radioLOSObject->setProperty("isMarkRecvSelected", false);


            _state = NO_POS_MARK;

            // disable Process Mouse Events
            _pointHandler->setProcessMouseEvents(false);

            // set Transmitter Position and Height
            {
                if(!_transPos.valid())
                {
                    throw UTIL::Exception("Mark the Transmitter Position", __FILE__, __LINE__);
                }

                double x = 0.0, y = 0.0, z = 0.0;

                if( _radioLOSObject->property("transLatitude").toString().toStdString().length()==0||
                    _radioLOSObject->property("transLongitude").toString().toStdString().length()==0||
                    _radioLOSObject->property("transAltitude").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Transmitter position parameters are not specified");

                    return;
                }

                x = _radioLOSObject->property("transLongitude").toDouble();
                y = _radioLOSObject->property("transLatitude").toDouble();
                z = _radioLOSObject->property("transAltitude").toDouble();

                if(x < -180 || x > 180 || y < -90 || y > 90)
                {
                    throw UTIL::Exception("Invalid Transmitter Postion", __FILE__, __LINE__);
                }

                if( _radioLOSObject->property("transHeight").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Transmitter Height parameter is not specified");
                    return;
                }

                double height = _radioLOSObject->property("transHeight").toDouble();
                if(height < 0.0)
                {
                    throw UTIL::Exception("Invalid Transmitter Height", __FILE__, __LINE__);
                }
                z += height;
                osg::Vec3d transPos(x, y, z);
                _transPos->getInterface<CORE::IPoint>()->setValue(transPos);
                _uiHandler->setTransmitterPosition(transPos);
                _uiHandler->setTransmitterHeight(height);

            }

            // set Receiver Position and Height
            {
                if(!_recvPos.valid())
                {
                    throw UTIL::Exception("Mark the Receiver Position", __FILE__, __LINE__);
                }

                double x = 0.0, y = 0.0, z = 0.0;

                if( _radioLOSObject->property("recvLatitude").toString().toStdString().length()==0 ||
                    _radioLOSObject->property("recvLongitude").toString().toStdString().length()==0||
                    _radioLOSObject->property("recvAltitude").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Reciever position parameters are not specified");

                    //emit _getDecisionButtonEnable(false);
                    //emit _transPosMarkButtonEnable(true);
                    //emit _recvPosMarkButtonEnable(true);
                    //emit _showProgressBar(false);
                    //emit _startButtonEnable(true);
                    //emit _stopButtonEnable(false);

                    return;
                }
                x = _radioLOSObject->property("recvLongitude").toDouble();
                y = _radioLOSObject->property("recvLatitude").toDouble();
                z = _radioLOSObject->property("recvAltitude").toDouble();

                if(x < -180 || x > 180 || y < -90 || y > 90)
                {
                    throw UTIL::Exception("Invalid Reciever Postion", __FILE__, __LINE__);
                }

                if( _radioLOSObject->property("recvHeight").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Reciever Height parameter is not specified");
                    return;
                }

                double height = _radioLOSObject->property("recvHeight").toDouble();

                if(height < 0.0)
                {
                    throw UTIL::Exception("Invalid Reciever Height", __FILE__, __LINE__);
                }
                z += height;
                osg::Vec3d recvPos(x, y, z);
                _recvPos->getInterface<CORE::IPoint>()->setValue(recvPos);
                _uiHandler->setReceiverPosition(recvPos);
                _uiHandler->setReceiverHeight(height);
            }

            // set Transmitter Icon object
            {
                if(!_transPos.valid())
                {
                    throw UTIL::Exception("Mark the Transmitter Position", __FILE__, __LINE__);
                }
                CORE::RefPtr<CORE::IPoint> point = _copyPoint(_transPos->getInterface<CORE::IPoint>());
                point->getInterface<ELEMENTS::IIconHolder>()->setIcon(_transIcon);
                _uiHandler->setTrasmitterIcon(point->getInterface<CORE::IObject>());
            }

            // set Receiver Icon object
            {
                if(!_recvPos.valid())
                {
                    throw UTIL::Exception("Mark the Receiver Position", __FILE__, __LINE__);
                }
                CORE::RefPtr<CORE::IPoint> point = _copyPoint(_recvPos->getInterface<CORE::IPoint>());
                point->getInterface<ELEMENTS::IIconHolder>()->setIcon(_recvIcon);
                _uiHandler->setReceiverIcon(point->getInterface<CORE::IObject>());
            }


            // set wave frequency in MHz
            {

                if( _radioLOSObject->property("waveFrequency").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Wave frequency parameter is not specified");
                    return;
                }

                double frequency = _radioLOSObject->property("waveFrequency").toDouble();
                if(frequency > 0.0)
                {
                    _uiHandler->setWaveFrequency(frequency);
                }
                else
                {
                    throw UTIL::Exception("Wave Frequency is invalid", __FILE__, __LINE__);
                }
            }


            // set effective earth radius factor
            {
                if( _radioLOSObject->property("kFactor").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","KFactor parameter is not specified");
                    return;
                }

                double kfactor = _radioLOSObject->property("kFactor").toDouble();
                if(kfactor > 0.0)
                {
                    _uiHandler->setEffectiveEarthRadiusFactor(kfactor);
                }
                else
                {
                    throw UTIL::Exception("Invalid KFactor", __FILE__, __LINE__);
                }
            }

            // set OutputName
            {
                std::string name = _radioLOSObject->property("outPutName").toString().toStdString();
                if(name == "")
                {
                    throw UTIL::Exception("Output Radio LOS name must be specified", __FILE__, __LINE__);
                }
                _uiHandler->setOutputName(name);
            }



            // set Tx/Rx Power in dB
            {
                if( _radioLOSObject->property("transPower").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","TransmitterPower parameter is not specified");
                    return;
                }

                double txPower = _radioLOSObject->property("transPower").toDouble();
                if( (txPower > 0.0) )
                {
                    _uiHandler->setTransmitterPower(txPower);
                }
                else
                {
                    throw UTIL::Exception("Invalid Transmission Power", __FILE__, __LINE__);
                }
            }

            // set Tx/Rx Antenna Gain in dB
            {
                if( _radioLOSObject->property("transGain").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Transmitter Gain parameter is not specified");
                    return;
                }

                if( _radioLOSObject->property("recvGain").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Reciever Gain parameter is not specified");
                    return;
                }

                double txGain = _radioLOSObject->property("transGain").toDouble();
                double rxGain = _radioLOSObject->property("recvGain").toDouble();
                _uiHandler->setAntennaGain(txGain, rxGain);
            }

            // set receiver senstivity in dB
            {
                if( _radioLOSObject->property("recvSensitivity").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Reciever sensitivity parameter is not specified");
                    return;
                }

                double rxSensitivity = _radioLOSObject->property("recvSensitivity").toDouble();
                if(rxSensitivity < 0.0)
                {
                    throw UTIL::Exception("Invalid Receiver Sensitivity", __FILE__, __LINE__);
                }
                else
                {
                    _uiHandler->setReceiverSensitivity(rxSensitivity);
                }
            }


            // set cable loss in dB/m
            {
                if( _radioLOSObject->property("cableLoss").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Cable Loss parameter is not specified");
                    return;
                }

                double cableLoss = _radioLOSObject->property("cableLoss").toDouble();
                if(cableLoss < 0.0)
                {
                    throw UTIL::Exception("Invalid Cable Loss", __FILE__, __LINE__);
                }
                else
                {
                    _uiHandler->setCableLoss(cableLoss);
                }
            }

            // set Range Step in m
            {
                if( _radioLOSObject->property("rangeStep").toString().toStdString().length()==0)
                {
                    showError("Insufficient data","Range Step parameter is not specified");
                    return;
                }

                double rangeStep = _radioLOSObject->property("rangeStep").toDouble();
                if(rangeStep > 0.0)
                {
                    _uiHandler->setRangeStep(rangeStep);
                }
                else
                {
                    throw UTIL::Exception("Invalid Range Step", __FILE__, __LINE__);
                }
            }

            emit _getDecisionButtonEnable(false);
            emit _transPosMarkButtonEnable(false);
            emit _recvPosMarkButtonEnable(false);
            emit _startButtonEnable(false);
            emit _stopButtonEnable(true);
            emit _showProgressBar(true);


            // execute Radio LOS
            _uiHandler->execute();
        }
        catch(UTIL::Exception& e)
        {
            // enable start button
            //disable stop button
            // disable getdecision button
            // hide progress Bar
            // enable trans/recv position mark button

            emit _startButtonEnable(true);
            emit _stopButtonEnable(false);

            emit _getDecisionButtonEnable(false);
            emit _showProgressBar(false);

            emit _transPosMarkButtonEnable(true);
            emit _recvPosMarkButtonEnable(true);

            // enable Process Mouse Events
            _pointHandler->setProcessMouseEvents(true);

            e.LogException();
            QString qError = QString::fromStdString(e.What());
            emit showError("Resampling Exception",qError);
        }
        catch(...)
        {
            // enable start button
            //disable stop button
            //disable Decision Support button
            // hide Busy Bar
            // enable trans/recv position mark button

            emit _startButtonEnable(true);
            emit _stopButtonEnable(false);

            emit _getDecisionButtonEnable(false);
            emit _showProgressBar(false);

            emit _transPosMarkButtonEnable(true);
            emit _recvPosMarkButtonEnable(true);

            // enable Process Mouse Events
            _pointHandler->setProcessMouseEvents(true);

            emit showError("RadioLOS Exception", "Unknown Error");
        }
    }

    void RadioLOSGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }

        try
        {
            _uiHandler->stopFilter();
        }
        catch(UTIL::Exception& e)
        {
            e.LogException();
        }
        catch(...)
        {
            LOG_ERROR("Stop Fail - Unknown Error")
        }
    }

    void RadioLOSGUI::_handlePBGetDecisionButtonClicked()
    {
        try
        {
            emit showError("Decision Support", _uiHandler->getDecisionSupport().c_str());
        }
        catch(...)
        {
        }
    }

    void RadioLOSGUI::_onUpdateOutputFields(bool set)
    {
        // on set ==  true, query for outputs as filter is successfull
        try
        {
            if(set)
            {
                if(_uiHandler->losClearance())
                {

                    _getDecisionButtonEnable(false);
                    // XXX we should not disable individually , since it's not working , doing for the moment

                    QString temp;

                    temp=QString::number(_uiHandler->getOutputReceiverPower());
                    _radioLOSObject->setProperty("opRecvPower",temp);

                    temp=QString::number(_uiHandler->getOutputFSL());
                    _radioLOSObject->setProperty("opFSL",temp);

                    temp=QString::number(_uiHandler->getOutputEIRP());
                    _radioLOSObject->setProperty("opEIRP",temp);

                    temp=QString::number(_uiHandler->getOutputSOM());
                    _radioLOSObject->setProperty("opSOM",temp);

                    _radioLOSObject->setProperty("showOutput",true);
                }
                else
                {
                    emit _getDecisionButtonEnable(true);
                    _radioLOSObject->setProperty("opRecvPower","");
                    _radioLOSObject->setProperty("opFSL","");
                    _radioLOSObject->setProperty("opEIRP","");
                    _radioLOSObject->setProperty("opSOM","");
                    emit showError("Filter Status", "Radio LOS Clearance is not available");
                    _radioLOSObject->setProperty("showOutput",false);
                }

            }
            // on set == false, you need to reset the output fields
            else
            {
                emit _showProgressBar(false);
                emit _getDecisionButtonEnable(false);
                _radioLOSObject->setProperty("opRecvPower","");
                _radioLOSObject->setProperty("opFSL","");
                _radioLOSObject->setProperty("opEIRP","");
                _radioLOSObject->setProperty("opSOM","");
            }
        }
        catch(...)
        {
        }
    }


    void RadioLOSGUI::_removeObject(CORE::IObject* obj)
    {
        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        if(world.valid() && obj)
        {
            try
            {
                world->removeObjectByID(&(obj->getInterface<CORE::IBase>(true)->getUniqueID()));
            }
            catch(...){}
        }
    }

    void RadioLOSGUI::_setTransPos(CORE::IPoint* point)
    {
        _removeObject(_transPos.get());
        _transPos = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

    void RadioLOSGUI::_setRecvPos(CORE::IPoint* point)
    {
        _removeObject(_recvPos.get());
        _recvPos = (point) ? point->getInterface<CORE::IObject>() : NULL;
    }

    CORE::IPoint* 
        RadioLOSGUI::_copyPoint(CORE::IPoint* point)
    {
        CORE::IObject* pointObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::
            ElementsRegistryPlugin::OverlayObjectType);
        CORE::IPoint* pointCopy = pointObject->getInterface<CORE::IPoint>();
        if(pointCopy && point)
        {
            pointCopy->setValue(point->getX(), point->getY(), point->getZ());
        }
        return pointCopy;
    }

} // namespace indiGUI




