/****************************************************************************
*
* File             : ProjectSettingsGUI.h
* Description      : ProjectSettingsGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/



#include <SMCQt/GuiSettingsGUI.h>
#include <Util/FileUtils.h>
#include <Util/StringUtils.h>
#include <QJSONDocument>
#include <QJSONObject> 
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt,GuiSettingsGUI);
    DEFINE_IREFERENCED(GuiSettingsGUI, DeclarativeFileGUI);

    const std::string GuiSettingsGUI::GUISettingsMenuObjectName = "guiSettingsMenu";

    GuiSettingsGUI::GuiSettingsGUI()
    {

    }

    GuiSettingsGUI::~GuiSettingsGUI()
    {

    }

    void GuiSettingsGUI::connectGuiSettingsMenu()
    {
        _guiSettingsMenu = _findChild(GUISettingsMenuObjectName);
        if(_guiSettingsMenu)
        {
            QObject::connect(_guiSettingsMenu, SIGNAL(setLatLongUnit(int)), this,
                SLOT(_latLongUnitChanged(int)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(setUIDockedState(bool)), this,
                SLOT(_dockTypeChanged(bool)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(setGRVisibilityState(int)), this,
                SLOT(_grVisibilityChanged(int)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(setUIScale(double)), this,
                SLOT(_uiScaleChanged(double)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(toggleMultiTouchMode(bool)), this,
                SLOT(_toggleMultiTouch(bool)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(setShowZone(bool)), this,
                SLOT(_toggleShowZone(bool)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(setShowLetter(bool)), this,
                SLOT(_toggleShowLetter(bool)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(setGRNumOfDigits(int)), this,
                SLOT(_setGRNumOfDigits(int)), Qt::UniqueConnection);
            QObject::connect(_guiSettingsMenu, SIGNAL(setMapSheet(bool)), this,
                SLOT(_setMapSheet(bool)), Qt::UniqueConnection);
            

        }

    }
    void GuiSettingsGUI::_uiScaleChanged(double value)
    {
        _writeSettings("UIScale",QString::number(value));
    }
    void GuiSettingsGUI::_latLongUnitChanged(int value)
    {
         _writeSettings("LatLongUnit",QString::number(value));

    }
    void GuiSettingsGUI::_dockTypeChanged(bool value)
    {
       _writeSettings("DockType",QString::number(value));

    }
    void GuiSettingsGUI::_grVisibilityChanged(int value)
    {
       _writeSettings("GrVisibility",QString::number(value));

    }

    void GuiSettingsGUI::_toggleShowZone(bool value)
    {
        _writeSettings("GrShowZone", QString::number(value));

    }

    void GuiSettingsGUI::_toggleShowLetter(bool value)
    {
        _writeSettings("GrShowLetter", QString::number(value));

    }

    void GuiSettingsGUI::_setGRNumOfDigits(int value)
    {
        _writeSettings("GrNumdigits", QString::number(value));

    }
    void GuiSettingsGUI::_setMapSheet(bool value)
    {
        _writeSettings("GrShowMapsheet", QString::number(value));

    }

    void GuiSettingsGUI::_writeSettings(QString key,QString value)
    {
        if(key.isEmpty()||value.isEmpty())
        return;

        //! Save in AppData
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        QFile settingFile(QString::fromStdString(appdataPath) + "/setting.json");
        QByteArray settingData;
        if(settingFile.open(QIODevice::ReadOnly))
        {
            settingData = settingFile.readAll();
            settingFile.close();
        }
        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);
        QJsonObject settingObject = settingDoc.object();
        settingObject.insert(key,value);
        settingDoc.setObject(settingObject);
        QJsonDocument settingDoctemp;
        settingDoctemp.setObject(settingObject);
        if(settingFile.open(QIODevice::WriteOnly))
        {
            settingFile.write(settingDoctemp.toJson());
        }

    }

    void GuiSettingsGUI::_toggleMultiTouch(bool startTouch)
    {
        getGUIManager()->getInterface<VizQt::IQtGUIManager>(true)->multiTouchInteractionActive(startTouch);
    }

}
