/*****************************************************************************
*
* File             : RasterUploadDialogGUI.cpp
* Description      : RasterUploadDialogGUI class definition
*
*****************************************************************************
* Copyright (c) 2010-2011, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <SMCQt/RasterUploadDialogGUI.h>
#include <SMCQt/VizComboBoxElement.h>
#include <QtGui/QIntValidator>
#include <QtCore/QFile>
#include <QtUiTools/QtUiTools>
#include <QFileDialog>
#include <Util/Log.h>
#include <App/UIHandlerFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/AccessElementUtils.h>
#include <SMCUI/SMCUIPlugin.h>
#include <SMCUI/AddContentStatusMessage.h>
#include <SMCUI/AddContentUIHandler.h>
#include <vector>
#include <VizDataUI/IDatabaseProcessingStatusMessage.h>

#include <QMessageBox>

#include <Database/IRasterDatabase.h>
#include <Core/ISelectionComponent.h>
#include <VizUI/ISelectionUIHandler.h>
#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>



namespace SMCQt
{
    RasterUploadDialogGUI::RasterUploadDialogGUI() :  _dbSelect(false),_tileWidth(64),_tileHeight(64)
    {

    }


    RasterUploadDialogGUI::~RasterUploadDialogGUI()
    {

    }


    void 
        RasterUploadDialogGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
                CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("SettingComponent");
                _globalSettingComponent = component->getInterface<ELEMENTS::ISettingComponent>();


            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *VizDataUI::IDatabaseProcessingStatusMessage::DatabaseProcessingStatusMessageType)
        {
            VizDataUI::IDatabaseProcessingStatusMessage* dbProcMessage = 
                message.getInterface<VizDataUI::IDatabaseProcessingStatusMessage>();

            if(dbProcMessage->getStatus() == VizDataUI::IDatabaseProcessingStatusMessage::SUCCESS)
            {

                showError("Success" , "Uploaded to Database",false) ; 
                /* VizQt::IQtGUIManager* mgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                if(mgr)
                {
                mgr->setApplicationBusy(false);
                }*/

            }
            else if(dbProcMessage->getStatus() == VizDataUI::IDatabaseProcessingStatusMessage::FAILURE)
            {
                showError("Fail" , "Uploading to Database failed") ;
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }



    void 
        RasterUploadDialogGUI::popupLoaded(QString type)
    {
        if(type == "postgisPopUp")
        {
            QObject* postgis = _findChild("postgisPopUp");

            if(postgis != NULL)
            {   

                QObject::connect( postgis, SIGNAL(okButtonClicked()), this,SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);
                QObject::connect( postgis, SIGNAL(selectDataType()), this,SLOT(_schemaSelected()), Qt::UniqueConnection);
                QObject::connect( postgis, SIGNAL(setParent()), this,SLOT(setParent()), Qt::UniqueConnection);
                std::string tempDatabaseName;
                ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::LAYERDATABASECONN);
                if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Database"))
                {
                    tempDatabaseName = settingAttrib.keyValuePair["Database"];
                }

                databaseName = tempDatabaseName.c_str();
                postgis->setProperty("databaseText", databaseName);
                _dbSelect = true;
                postgis->setProperty("dataTypeSelected", "");
                _displaySchema();
                _schemaSelected();
            }
        }
        else if (type=="createDataTpyeType")
        {
            QObject* createDataType = _findChild("createDataType");
        }


    }


    void RasterUploadDialogGUI::_displaySchema()
    {
        dataTypeList.clear();
        if (databaseName!="")
        {
            if(makeConnectionWithDatabase())
            {
                std::vector<std::string> schemaList = _dbUIHandler->getSchemaList();
                CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
                const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();
                CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();
                CORE::RefPtr<CORE::IObject> iObject = iter->second->getInterface<CORE::IObject>();
                CORE::RefPtr<TERRAIN::IRasterObject> rObject = iObject->getInterface<TERRAIN::IRasterObject>();
                CORE::RefPtr<TERRAIN::IElevationObject> eObject = iObject->getInterface<TERRAIN::IElevationObject>();

                for(unsigned int i = 0; i < schemaList.size(); i++)
                {
                    const char *dataType;
                    dataType = strchr(schemaList[i].c_str(),'_');
                    if (dataType != NULL)
                    {
                        dataType = dataType + 1;
                        std::string subStr = dataType;
                        unsigned int pos = subStr.find("_");
                        std::string dataCateog = subStr.substr(0,pos);
                        dataType = strchr(dataType,'_');
                        if(dataType != NULL)
                        {
                            dataType = dataType + 1;
                            if(strlen(dataType) != 0)
                            {   
                                const std::string str(dataType);
                                if(dataCateog == "raster" && rObject != NULL)
                                {

                                    dataTypeList.append(new VizComboBoxElement(QString::fromStdString(str), QString::number(i)));
                                    _dataType2Schema[std::string(dataType)] = schemaList[i];
                                }
                                else if(dataCateog == "elevation" && eObject != NULL)
                                {

                                    dataTypeList.append(new VizComboBoxElement(QString::fromStdString(str), QString::number(i)));
                                    _dataType2Schema[std::string(dataType)] = schemaList[i];
                                }
                            }
                        }
                    }
                }
                _setContextProperty("dataTypeList", QVariant::fromValue(dataTypeList));

                _dbSelect = true;
            }
        }   



    }
    void 
        RasterUploadDialogGUI::_loadAndSubscribeSlots()
    {     

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }

    void
        RasterUploadDialogGUI::onAddedToGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            _dbUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getGUIManager());
            _subscribe( _dbUIHandler.get(), *VizDataUI::IDatabaseProcessingStatusMessage::DatabaseProcessingStatusMessageType);

        }
        catch(...)
        {
        }

        DeclarativeFileGUI::onAddedToGUIManager();
    }

    void 
        RasterUploadDialogGUI::onRemovedFromGUIManager()
    {
        // Subscribe for application loaded message
        try
        {
            _dbUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getGUIManager());
            _unsubscribe( _dbUIHandler.get(), *VizDataUI::IDatabaseProcessingStatusMessage::DatabaseProcessingStatusMessageType);
        }
        catch(...)
        {}

        DeclarativeFileGUI::onRemovedFromGUIManager();
    }

    void
        RasterUploadDialogGUI::_loadAddContentUIHandler()
    {
        _addContentUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
    }

    void 
        RasterUploadDialogGUI::_handleOkButtonClicked()
    {

        if(_dbSelect)
        {
            QObject* postGIS = _findChild("postgisPopUp");
            if(postGIS != NULL)
            {                   
                _tileWidth= postGIS->property("tileWidth").toInt();
                if(_tileWidth==0)
                {
                    showError("Invalid Entry","Enter valid Tile Width");
                    return;
                }
                _tileHeight= postGIS->property("tileHeight").toInt();
                if(_tileHeight==0)
                {
                    showError("Invalid Entry","Enter valid Tile Height");
                    return;
                }
                _createCheckBox= postGIS->property("createCheckBox").toBool();
                _appendCheckBox= postGIS->property("appendCheckBox").toBool();
                std::string dataTypeName = getSchema().toStdString();
                std::string schemaName =  _dataType2Schema[dataTypeName];
                std::string tableName = getTableName().toStdString(); 
                std::string interpolation = "near";
                std::string resampling;    
                if(tableName.empty())
                {
                    showError("Invalid Entry","Enter valid values");
                    return;
                }

                if(interpolation == "Nearest Neighbour")
                {
                    resampling = "near";
                }
                else if(interpolation == "Bilinear")
                {
                    resampling = "bilinear";
                }
                else if(interpolation == "Cubic")
                {
                    resampling = "cubic";
                }
                else if(interpolation == "Cubic Spline")
                {
                    resampling = "cubicspline";
                }
                else if(interpolation == "Lanczos")
                {
                    resampling = "lanczos";
                }

                int tileWidth = _tileWidth;
                int tileHeight = _tileHeight;

                if(schemaName == "")
                {
                    schemaName = "public";
                }

                if(_createCheckBox)
                {
                    if(_dbUIHandler->validateTableName(tableName, schemaName))
                    {
                        VizQt::IQtGUIManager* mgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                        try
                        {
                            _dbUIHandler->uploadRaster(tableName, schemaName, 4326, vizDatabase::TABLE_CREATE, tileWidth, tileHeight, 0, 0 , true, true, true, resampling);
                            //showError("Success" , "Uploaded to Database",false) ;                          
                            postGIS->setProperty("categoryText ","");                    
                        }
                        catch(const vizUtil::Exception& e)
                        {
                            e.LogException();
                            std::string title = "Upload Raster Error";
                            std::string text = e.What();
                            showError( "Upload Raster Error", text.c_str());
                        }
                    }
                    else
                    {
                        showError( "Invalid Table Name", "Failed to validate Table Name");
                    }
                }
                else if(_appendCheckBox)
                {
                    if(!_dbUIHandler->validateTableName(tableName, schemaName))
                    {
                        VizQt::IQtGUIManager* mgr = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                        try
                        {
                            _dbUIHandler->uploadRaster(tableName, schemaName, 4326, 0, tileWidth,  tileHeight, vizDatabase::TABLE_APPEND, 0 , true, true, true, resampling);
                            postGIS->setProperty("categoryText ","");
                        }
                        catch(const vizUtil::Exception& e)
                        {
                            e.LogException();
                            // Show error for vector data
                            std::string title = "Upload Raster Error";
                            std::string text = e.What();
                            showError( "Upload Raster Error", text.c_str());
                        }

                    }
                    else
                    {
                        showError( "Invalid Table Name", "Table doesnt Exists in Database");
                    }
                }
            }
        }
        else
        {            
            showError( "Invalid Table Name","Please select Database to Upload");
        }

    }


    void
        RasterUploadDialogGUI::_schemaSelected()
    {
        QObject* postGIS = _findChild("postgisPopUp");
        if(postGIS != NULL)
        {
            _dataTypeSelect= postGIS->property("dataTypeSelected").toString();

        }
        std::string schema =  _dataType2Schema[_dataTypeSelect.toStdString()];
        if(schema != "")
        {
            std::string subStr = schema.substr(4);
            unsigned int pos = subStr.find("_");
            std::string dataCateog = subStr.substr(0,pos);

            postGIS->setProperty("dataCategory", dataCateog.c_str());
        }

    }

    void
        RasterUploadDialogGUI::setParent()
    {

        QObject* createDataType = _findChild("createDataType");
        if(createDataType != NULL)
        {
            createDataType->setProperty("rootQml", "raster");
        }
    }

    QString 
        RasterUploadDialogGUI::getTableName()
    {
        QObject* postGIS = _findChild("postgisPopUp");
        if(postGIS != NULL)
        {
            _dataTableName= postGIS->property("selectedTableName").toString();

        }
        return _dataTableName;
    }


    QString RasterUploadDialogGUI::getSchema()
    {
        QObject* postGIS = _findChild("postgisPopUp");
        if(postGIS != NULL)
        {

            _dataTypeSelect= postGIS->property("dataTypeSelected").toString();
        }
        return _dataTypeSelect;
    }


    int RasterUploadDialogGUI::getTileWidth()
    {

        QObject* postGIS = _findChild("postgisPopUp");
        if(postGIS != NULL)
        {
            _tileWidth= postGIS->property("tileWidth").toInt();

        }
        return _tileWidth;
    }

    int 
        RasterUploadDialogGUI::getTileHeight()
    {
        QObject* postGIS = _findChild("postgisPopUp");
        if(postGIS != NULL)
        {

            _tileHeight= postGIS->property("tileHeight").toInt();
        }
        return _tileHeight;
    }

    bool
        RasterUploadDialogGUI::_presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap, const std::string& keyword)
    {
        if(settingMap.find(keyword) != settingMap.end())
        {
            return true;
        }
        return false;
    }

    bool
        RasterUploadDialogGUI::makeConnectionWithDatabase()
    {

        _dbUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getGUIManager());

        return _checkConnectionWithDatabase();
    }

    bool
        RasterUploadDialogGUI::_checkConnectionWithDatabase()
    {
        _dbOptions.clear();
        _dbOptions = _getDatabaseCredential();
        if(_dbOptions.size() < 5)
            return false;

        _dbUIHandler->initialize(_dbOptions);
        std::string status;
        if(_dbUIHandler->testConnection(_dbOptions))
        {
            status = "Connected";
            _dbOptions.insert(std::pair<std::string, std::string>("Status", status));
            _dbUIHandler->initialize(_dbOptions);
            return true;
        }
        else
        {
            status = "Not Connected";
            _dbOptions.insert(std::pair<std::string, std::string>("Status", status));
            _dbUIHandler->initialize(_dbOptions);
            return false;
        }
        return false;
    }


    VizDataUI::IDatabaseUIHandler*
        RasterUploadDialogGUI::_getDatabaseUIHandler()
    {
        VizDataUI::IDatabaseUIHandler* dbUIHandler = NULL;
        if(!_dbUIHandler)
        {
            CORE::RefPtr<APP::IUIHandlerFactory> uf = APP::ApplicationRegistry::instance()->getUIHandlerFactory();
            const APP::IUIHandlerType *uhType = uf->getUIHandlerType("DatabaseUIHandler");
            CORE::RefPtr<APP::IUIHandler> _uih = uf->createUIHandler(*uhType);
            _dbUIHandler = _uih->getInterface<VizDataUI::IDatabaseUIHandler>();
        }
        return dbUIHandler;
    }

    std::map<std::string, std::string>
        RasterUploadDialogGUI::_getDatabaseCredential()
    {
        // Then Get the Database Information(credentials) of connected Database
        ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::LAYERDATABASECONN);

        //Filiing the credential in the dataabase options 
        std::map<std::string, std::string> dbOptions;

        std::map<std::string, std::string>::iterator iter = settingAttrib.keyValuePair.begin();
        while(iter != settingAttrib.keyValuePair.end())
        {
            dbOptions.insert(std::pair<std::string, std::string>(iter->first, iter->second));
            iter++;
        }

        return dbOptions;
    }
}
