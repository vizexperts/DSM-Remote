/*****************************************************************************
*
* File             : SMCQtPlugin.cpp
* Description      : SMCQtPlugin class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#ifdef WIN32
#include <windows.h>
#endif    // WIN32

#include <GUI/GUIType.h>
#include <Util/FileUtils.h>
#include <App/IApplication.h>
#include <App/ManagerType.h>
#include <App/ApplicationType.h>
#include <App/ApplicationRegistry.h>
#include <App/ManagerUtils.h>
#include <App/ApplicationUtils.h>

#include <SMCQt/SMCQtPlugin.h>

#include <VizQt/BasemapGUI.h>
#include <SMCQt/AddContentGUI.h>
#include <SMCQt/BottomPaneGUI.h>
#include <SMCQt/EnvironmentGUI.h>
#include <SMCQt/CreateLineGUI.h>
#include <SMCQt/CreatePointGUI.h>
#include <SMCQt/AddGPSGUI.h>
#include <SMCQt/ElementListGUI.h>
#include <SMCQt/SearchGUI.h>
#include <SMCQt/ContextualTabGUI.h>
#include <SMCQt/CreateMilitarySymbolGUI.h>
#include <SMCQt/CreatePolygonGUI.h>
#include <SMCQt/CreateLandmarkGUI.h>
#include <SMCQt/ProjectManagerGUI.h>
#include <SMCQt/StereoSettingsGUI.h>
#include <SMCQt/MilitaryPathAnimationGUI.h>
#include <SMCQt/UndoRedoGUI.h>
#include <SMCQt/TimelineGUI.h>
#include <SMCQt/LoginGUI.h>
#include <SMCQt/AppLauncherGUI.h>
#include <SMCQt/ResourceManagerGUI.h>
#include <SMCQt/MeasureDistanceGUI.h>
#include <SMCQt/MeasureSlopeGUI.h>
#include <SMCQt/MeasureAreaGUI.h>
#include <SMCQt/ElevationProfileGUI.h>

#include <SMCQt/NetworkPublishGUI.h>
#include <SMCQt/NetworkJoinGUI.h>
#include <SMCQt/ScreenShareGUI.h>
#include <SMCQt/PresentationGUI.h>
#include <SMCQt/SettingsGUI.h>
#include <SMCQt/ProjectSettingsGUI.h>
#include <SMCQt/GuiSettingsGUI.h>
#include <SMCQt/TourGUI.h>
#include <SMCQt/LOSGUI.h>
#include <SMCQt/SlopeAspectGUI.h>
#include <SMCQt/ColorElevationGUI.h>
#include <SMCQt/AlmanacGUI.h>
#include <SMCQt/ScreenShotGUI.h>
#include <SMCQt/RecordingGUI.h>
#include <SMCQt/CreateLayerGUI.h>
#include <SMCQt/FeatureExportGUI.h>

#include <SMCQt/ViewshedAnalysisGUI.h>
#include <SMCQt/SensorRangeAnalysisGUI.h>
#include <SMCQt/AddWebDataGUI.h>
#include <SMCQt/PointLayerPropertiesGUI.h>
#include <SMCQt/EventGUI.h>
#include <SMCQt/AddServerLayerGUI.h>
#include <SMCQt/DownloadRasterGUI.h>
#include <SMCQt/CreateFeatureGUI.h>
#include <SMCQt/CrestClearanceGUI.h>
#include <SMCQt/RadioLOSGUI.h>
#include <SMCQt/TTSGUI.h>
#include <SMCQt/WebCamGUI.h>
#include <SMCQt/CreateOpacitySliderGUI.h>
#include <SMCQt/VisibilityControllerGUI.h>
#include <SMCQt/RasterBandReorderGUI.h>
#include <SMCQt/GetInformationGUI.h>
#include <SMCQt/RasterEnhancementChangerGUI.h>

#include <SMCQt/CreateTMSGUI.h>
#include <SMCQt/RasterUploadDialogGUI.h>
#include <SMCQt/PGCreateDataTypeDialogGUI.h>

#include <SMCQt/RecordingGUI.h>
#include <SMCQt/CreateCursorGUI.h>
#include <SMCQt/PanSharpenGUI.h>
#include <SMCQt/CVAGUI.h>
#include <SMCQt/KMeanGUI.h>
#include <SMCQt/FieldOfViewGUI.h>
#include <SMCQt/CacheSettingsGUI.h>
#include <SMCQt/RasterWindowGUI.h>
#include <SMCQt/ReprojectImageGUI.h>
#include <SMCQt/ExtractROIGUI.h>
#include <SMCQt/GunRangeGUI.h>
#include <SMCQt/QueryBuilderGUI.h>
#include <SMCQt/QueryBuilderGISGUI.h>
#include <SMCQt/ElementClassificationGUI.h>
#include <SMCQt/RoadNetworkGUI.h>
#include <SMCQt/DatabaseLayerGUI.h>
#include <SMCQt/ViewBasedVisibilityGUI.h>
#include <SMCQt/LineLayerPropertiesGUI.h>
#include <SMCQt/MinMaxElevGUI.h>
#include <SMCQt/MeasureHeightGUI.h>
#include <SMCQt/ElevationDiffGUI.h>
#include <SMCQt/MeasurePerimeterGUI.h>
#include <SMCQt/SteepestPathGUI.h>


#include <SMCQt/ContoursGenerationGUI.h>
#include <SMCQt/HillshadeCalculationGUI.h>
#include <SMCQt/TerrainExportGUI.h>
#include <SMCQt/AnimationGUI.h>
#include <SMCQt/FloodingGUI.h>
#include <SMCQt/WatershedGenerationGUI.h>
#include <SMCQt/CutAndFillGUI.h>
#include <SMCQt/FlyAroundGUI.h>
#include <SMCQt/DatabaseSettingsGUI.h>
#include <SMCQt/GeorbISServersSettingsGUI.h>
#include <SMCQt/StyleTemplateGUI.h>
#include <SMCQt/CreateOEFeatureGUI.h> 

#include <SMCQt/VectorLayerQueryGUI.h>
#include <SMCQt/VectorLayerQueryBuilderGUI.h>

#include <SMCQt/CreateTacticalSymbolsGUI.h>
#include <SMCQt/ComputeServicesGUI.h>
#include <SMCQt/AddBookmarksGUI.h>
#include <SMCQt/ApplicationSettingsGUI.h>
#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/ColorLegendGUI.h>
#include <SMCQt/UserManualGUI.h>

//----------------------------------------------------------------------------
// Register all interfaces
//----------------------------------------------------------------------------

DEFINE_META_INTERFACE(SMCQt, IContextualTabGUI)

//----------------------------------------------------------------------------
// Define SMCQtRegistryPlugin and register plugin
//----------------------------------------------------------------------------

REGISTER_APPREGISTRY_PLUGIN(SMCQT_DLL_EXPORT, SMCQt, SMCQtAppRegistryPlugin);

namespace SMCQt
{
    //------------------------------------------------------------------------
    // SMCQt Application Registry Plugin
    //------------------------------------------------------------------------

    // Definitions
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::EnvironmentGUIType(
        new GUI::GUIType("EnvironmentGUI", SMCQtAppRegistryPlugin::StartID + 1));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::BottomPaneGUIType(
        new GUI::GUIType("BottomPaneGUI", SMCQtAppRegistryPlugin::StartID + 2));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AddContentGUIType(
        new GUI::GUIType("AddContentGUI", SMCQtAppRegistryPlugin::StartID + 3));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AddGPSGUIType(
        new GUI::GUIType("AddGPSGUI", SMCQtAppRegistryPlugin::StartID + 6));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateLineGUIType(
        new GUI::GUIType("CreateLineGUI", SMCQtAppRegistryPlugin::StartID + 7));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreatePointGUIType(
        new GUI::GUIType("CreatePointGUI", SMCQtAppRegistryPlugin::StartID + 8));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ElementListGUIType(
        new GUI::GUIType("ElementListGUI", SMCQtAppRegistryPlugin::StartID + 9));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::SearchGUIType(
        new GUI::GUIType("SearchGUI", SMCQtAppRegistryPlugin::StartID + 10));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ContextualTabGUIType(
        new GUI::GUIType("ContextualTabGUI", SMCQtAppRegistryPlugin::StartID + 11));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateMilitarySymbolGUIType(
        new GUI::GUIType("CreateMilitarySymbolGUI", SMCQtAppRegistryPlugin::StartID + 12));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreatePolygonGUIType(
        new GUI::GUIType("CreatePolygonGUI", SMCQtAppRegistryPlugin::StartID + 13));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateLandmarkGUIType(
        new GUI::GUIType("CreateLandmarkGUI", SMCQtAppRegistryPlugin::StartID + 14));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ProjectManagerGUIType(
        new GUI::GUIType("ProjectManagerGUI", SMCQtAppRegistryPlugin::StartID + 15));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::StereoSettingsGUIType(
        new GUI::GUIType("StereoSettingsGUI", SMCQtAppRegistryPlugin::StartID + 16));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::MilitaryPathAnimationGUIType(
        new GUI::GUIType("MilitaryPathAnimationGUI", SMCQtAppRegistryPlugin::StartID + 17));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::UndoRedoGUIType(
        new GUI::GUIType("UndoRedoGUI", SMCQtAppRegistryPlugin::StartID + 18));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::TimelineGUIType(
        new GUI::GUIType("TimelineGUI", SMCQtAppRegistryPlugin::StartID + 19));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::LoginGUIType(
        new GUI::GUIType("LoginGUI", SMCQtAppRegistryPlugin::StartID + 20));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ResourceManagerGUIType(
        new GUI::GUIType("ResourceManagerGUI", SMCQtAppRegistryPlugin::StartID + 21));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::NetworkJoinGUIType(
        new GUI::GUIType("NetworkJoinGUI", SMCQtAppRegistryPlugin::StartID + 22));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::NetworkPublishGUIType(
        new GUI::GUIType("NetworkPublishGUI", SMCQtAppRegistryPlugin::StartID + 23));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ScreenShareGUIType(
        new GUI::GUIType("ScreenShareGUI", SMCQtAppRegistryPlugin::StartID + 24));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::PresentationGUIType(
        new GUI::GUIType("PresentationGUI", SMCQtAppRegistryPlugin::StartID + 25));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::MeasureDistanceGUIType(
        new GUI::GUIType("MeasureDistanceGUI", SMCQtAppRegistryPlugin::StartID + 26));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::MeasureSlopeGUIType(
        new GUI::GUIType("MeasureSlopeGUI", SMCQtAppRegistryPlugin::StartID + 92));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::MeasureAreaGUIType(
        new GUI::GUIType("MeasureAreaGUI", SMCQtAppRegistryPlugin::StartID + 27));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ElevationProfileGUIType(
        new GUI::GUIType("ElevationProfileGUI", SMCQtAppRegistryPlugin::StartID + 28));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::TourGUIType(
        new GUI::GUIType("TourGUI", SMCQtAppRegistryPlugin::StartID + 30));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::SettingsGUIType(
        new GUI::GUIType("SettingsGUI", SMCQtAppRegistryPlugin::StartID + 31));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::LOSGUIType(
        new GUI::GUIType("LOSGUI", SMCQtAppRegistryPlugin::StartID + 32));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::SlopeAspectGUIType(
        new GUI::GUIType("SlopeAspectGUI", SMCQtAppRegistryPlugin::StartID + 33));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AlmanacGUIType(
        new GUI::GUIType("AlmanacGUI", SMCQtAppRegistryPlugin::StartID + 35));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ScreenshotGUIType(
        new GUI::GUIType("ScreenshotGUI", SMCQtAppRegistryPlugin::StartID + 36));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateLayerGUIType(
        new GUI::GUIType("CreateLayerGUI", SMCQtAppRegistryPlugin::StartID + 38));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::FeatureExportGUIType(
        new GUI::GUIType("FeatureExportGUI", SMCQtAppRegistryPlugin::StartID + 39));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AddWebDataGUIType(
        new GUI::GUIType("AddWebDataGUI", SMCQtAppRegistryPlugin::StartID + 40));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ViewshedAnalysisGUIType(
        new GUI::GUIType("ViewshedAnalysisGUI", SMCQtAppRegistryPlugin::StartID + 41));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::PointLayerPropertiesGUIType(
        new GUI::GUIType("PointLayerPropertiesGUI", SMCQtAppRegistryPlugin::StartID + 42));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::EventGUIType(
        new GUI::GUIType("EventGUI", SMCQtAppRegistryPlugin::StartID + 43));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ProjectSettingsGUIType(
        new GUI::GUIType("ProjectSettingsGUI", SMCQtAppRegistryPlugin::StartID + 44));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AddServerLayerGUIType(
        new GUI::GUIType("AddServerLayerGUI", SMCQtAppRegistryPlugin::StartID + 45));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CrestClearanceGUIType(
        new GUI::GUIType("CrestClearanceGUI", SMCQtAppRegistryPlugin::StartID + 46));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::RadioLOSGUIType(
        new GUI::GUIType("RadioLOSGUI", SMCQtAppRegistryPlugin::StartID + 47));
#ifdef WIN32
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::TTSGUIType(
        new GUI::GUIType("TTSGUI", SMCQtAppRegistryPlugin::StartID + 48));
#endif //WIN32
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateFeatureGUIType(
        new GUI::GUIType("CreateFeatureGUI", SMCQtAppRegistryPlugin::StartID + 49));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::WebCamGUIType(
        new GUI::GUIType("WebCamGUI", SMCQtAppRegistryPlugin::StartID + 50));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateOpacitySliderGUIType(
        new GUI::GUIType("CreateOpacitySliderGUI", SMCQtAppRegistryPlugin::StartID + 51));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::VisibilityControllerGUIType(
        new GUI::GUIType("VisibilityControllerGUI", SMCQtAppRegistryPlugin::StartID + 52));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateTMSGUIType(
        new GUI::GUIType("CreateTMSGUI", SMCQtAppRegistryPlugin::StartID + 53));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::RasterUploadDialogGUIType(
        new GUI::GUIType("RasterUploadDialogGUI", SMCQtAppRegistryPlugin::StartID + 54));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::PGCreateDataTypeDialogGUIType(
        new GUI::GUIType("PGCreateDataTypeDialogGUI", SMCQtAppRegistryPlugin::StartID + 55));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::RecordingGUIType(
        new GUI::GUIType("RecordingGUI", SMCQtAppRegistryPlugin::StartID + 56));


#ifdef USE_QT4
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AppLauncherGUIType(
        new GUI::GUIType("AppLauncherGUI", SMCQtAppRegistryPlugin::StartID + 57));
#endif
#ifdef WIN32
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateCursorGUIType(
        new GUI::GUIType("CreateCursorGUI", SMCQtAppRegistryPlugin::StartID + 58));
#endif//WIN32

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::FieldOfViewGUIType(
        new GUI::GUIType("FieldOfViewGUI", SMCQtAppRegistryPlugin::StartID + 60));


    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::PanSharpenGUIType(
        new GUI::GUIType("PanSharpenGUI", SMCQtAppRegistryPlugin::StartID + 61));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CVAGUIType(
        new GUI::GUIType("CVAGUI", SMCQtAppRegistryPlugin::StartID + 62));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::KMeanGUIType(
        new GUI::GUIType("KMeanGUI", SMCQtAppRegistryPlugin::StartID + 63));


    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::RasterBandReorderGUIType(
        new GUI::GUIType("RasterBandReorderGUI", SMCQtAppRegistryPlugin::StartID + 86));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::RasterEnhancementChangerGUIType(
        new GUI::GUIType("RasterEnhancementChangerGUI", SMCQtAppRegistryPlugin::StartID + 87));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::RasterWindowGUIType(
        new GUI::GUIType("RasterWindowGUI", SMCQtAppRegistryPlugin::StartID + 91));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ReprojectImageGUIType(
        new GUI::GUIType("ReprojectImageGUI", SMCQtAppRegistryPlugin::StartID + 93));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ExtractROIGUIType(
        new GUI::GUIType("ExtractROIGUI", SMCQtAppRegistryPlugin::StartID + 94));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::GunRangeGUIType(
        new GUI::GUIType("GunRangeGUI", SMCQtAppRegistryPlugin::StartID + 95));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::GetInformationGUIType(
        new GUI::GUIType("GetInformationGUI", SMCQtAppRegistryPlugin::StartID + 96));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::QueryBuilderGUIType(
        new GUI::GUIType("QueryBuilderGUI", SMCQtAppRegistryPlugin::StartID + 97));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ElementClassificationGUIType(
        new GUI::GUIType("ElementClassificationGUI", SMCQtAppRegistryPlugin::StartID + 98));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::RoadNetworkGUIType(
        new GUI::GUIType("RoadNetworkGUI", SMCQtAppRegistryPlugin::StartID + 102));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::DatabaseLayerGUIType(
        new GUI::GUIType("DatabaseLayerGUI", SMCQtAppRegistryPlugin::StartID + 104));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::QueryBuilderGISGUIType(
        new GUI::GUIType("QueryBuilderGISGUI", SMCQtAppRegistryPlugin::StartID + 105));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::SensorRangeAnalysisGUIType(
        new GUI::GUIType("SensorRangeAnalysisGUI", SMCQtAppRegistryPlugin::StartID + 106));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ViewBasedVisibilityGUIType(
        new GUI::GUIType("ViewBasedVisibilityGUI", SMCQtAppRegistryPlugin::StartID + 107));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::GuiSettingsGUIType(
        new GUI::GUIType("GuiSettingsGUI", SMCQtAppRegistryPlugin::StartID + 108));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::LineLayerPropertiesGUIType(
        new GUI::GUIType("LineLayerPropertiesGUI", SMCQtAppRegistryPlugin::StartID + 108));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::MinMaxElevGUIType(
        new GUI::GUIType("MinMaxElevGUI", SMCQtAppRegistryPlugin::StartID + 111));


    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::MeasureHeightGUIType(
        new GUI::GUIType("MeasureHeightGUI", SMCQtAppRegistryPlugin::StartID + 112));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ColorElevationGUIType(
        new GUI::GUIType("ColorElevationGUI", SMCQtAppRegistryPlugin::StartID + 110));
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ElevationDiffGUIType(
        new GUI::GUIType("ElevationDiffGUI", SMCQtAppRegistryPlugin::StartID + 113));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::MeasurePerimeterGUIType(
        new GUI::GUIType("MeasurePerimeterGUI", SMCQtAppRegistryPlugin::StartID + 114));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ContoursGenerationGUIType(
        new GUI::GUIType("ContoursGenerationGUI", SMCQtAppRegistryPlugin::StartID + 115));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::HillshadeCalculationGUIType(
        new GUI::GUIType("HillshadeCalculationGUI", SMCQtAppRegistryPlugin::StartID + 116));
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::FloodingGUIType(
        new GUI::GUIType("FloodingGUI", SMCQtAppRegistryPlugin::StartID + 117));
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::WatershedGenerationGUIType(
        new GUI::GUIType("WatershedGenerationGUI", SMCQtAppRegistryPlugin::StartID + 118));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CutAndFillGUIType(
        new GUI::GUIType("CutAndFillGUI", SMCQtAppRegistryPlugin::StartID + 119));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::SteepestPathGUIType(
        new GUI::GUIType("SteepestPathGUI", SMCQtAppRegistryPlugin::StartID + 120));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CacheSettingsGUIType(
        new GUI::GUIType("CacheSettingsGUI", SMCQtAppRegistryPlugin::StartID + 121));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::FlyAroundGUIType(
        new GUI::GUIType("FlyAroundGUI", SMCQtAppRegistryPlugin::StartID + 122));
	const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::DatabaseSettingsGUIType(
        new GUI::GUIType("DatabaseSettingsGUI", SMCQtAppRegistryPlugin::StartID + 123));	
	

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::VectorLayerQueryGUIType(
        new GUI::GUIType("VectorLayerQueryGUI", SMCQtAppRegistryPlugin::StartID + 124));
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::VectorLayerQueryBuilderGUIType(
        new GUI::GUIType("VectorLayerQueryBuilderGUI", SMCQtAppRegistryPlugin::StartID + 125));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateTacticalSymbolsGUIType(
        new GUI::GUIType("CreateTacticalSymbolsGUI", SMCQtAppRegistryPlugin::StartID + 129));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::GeorbISServersSettingsGUIType(
        new GUI::GUIType("GeorbISServersSettingsGUI", SMCQtAppRegistryPlugin::StartID + 126));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::StyleTemplateGUIType(
        new GUI::GUIType("StyleTemplateGUI", SMCQtAppRegistryPlugin::StartID + 127));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::CreateOEFeatureGUIType(
        new GUI::GUIType("CreateOEFeatureGUI", SMCQtAppRegistryPlugin::StartID + 128));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ComputeServicesGUIType(
        new GUI::GUIType("ComputeServicesGUI", SMCQtAppRegistryPlugin::StartID + 129));
        
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::TerrainExportGUIType(
        new GUI::GUIType("TerrainExportGUI", SMCQtAppRegistryPlugin::StartID + 130));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::DownloadRasterGUIType(
        new GUI::GUIType("DownloadRasterGUI", SMCQtAppRegistryPlugin::StartID + 131));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AddBookmarksGUIType(
        new GUI::GUIType("AddBookmarksGUI", SMCQtAppRegistryPlugin::StartID + 132));
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ApplicationSettingsGUIType(
        new GUI::GUIType("ApplicationSettingsGUI", SMCQtAppRegistryPlugin::StartID + 133));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::AnimationGUIType(
        new GUI::GUIType("AnimationGUI", SMCQtAppRegistryPlugin::StartID + 132));

    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ColorPaletteGUIType(
        new GUI::GUIType("ColorPaletteGUI", SMCQtAppRegistryPlugin::StartID + 134));
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::ColorLegendGUIType(
        new GUI::GUIType("ColorLegendGUI", SMCQtAppRegistryPlugin::StartID + 135));
    const CORE::RefPtr<APP::IGUIType> SMCQtAppRegistryPlugin::UserManualGUIType(
        new GUI::GUIType("UserManualGUI", SMCQtAppRegistryPlugin::StartID + 136));
    const std::string SMCQtAppRegistryPlugin::LibraryName = UTIL::getPlatformLibraryName("SMCQt");
    const std::string SMCQtAppRegistryPlugin::PluginName = "SMCQt";

    SMCQtAppRegistryPlugin::~SMCQtAppRegistryPlugin(){}

    void
        SMCQtAppRegistryPlugin::load(const APP::ApplicationRegistry& ar)
    {
        registerManagers(*(ar.getManagerFactory()));
        registerApplications(*(ar.getApplicationFactory()));
        registerGUIs(*(ar.getGUIFactory()));
    }

    void
        SMCQtAppRegistryPlugin::unload(const APP::ApplicationRegistry& ar)
    {
        deregisterManagers(*(ar.getManagerFactory()));
        deregisterApplications(*(ar.getApplicationFactory()));
        deregisterGUIs(*(ar.getGUIFactory()));
    }

    void
        SMCQtAppRegistryPlugin::registerManagers(APP::IManagerFactory& mf)
    {
    }

    void
        SMCQtAppRegistryPlugin::deregisterManagers(APP::IManagerFactory& mf)
    {
    }

    void
        SMCQtAppRegistryPlugin::registerApplications(APP::IApplicationFactory& af)
    {
    }

    void
        SMCQtAppRegistryPlugin::deregisterApplications(APP::IApplicationFactory& af)
    {
    }

    void
        SMCQtAppRegistryPlugin::registerGUIs(APP::IGUIFactory& factory)
    {
        factory.registerGUI(*SMCQtAppRegistryPlugin::EnvironmentGUIType, new CORE::Proxy<APP::IGUI, SMCQt::EnvironmentGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::BottomPaneGUIType, new CORE::Proxy<APP::IGUI, SMCQt::BottomPaneGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::AddContentGUIType, new CORE::Proxy<APP::IGUI, SMCQt::AddContentGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateLineGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreateLineGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreatePointGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreatePointGUI>());
#ifdef WIN32
        factory.registerGUI(*SMCQtAppRegistryPlugin::AddGPSGUIType, new CORE::Proxy<APP::IGUI, SMCQt::AddGPSGUI>());
#endif //WIN32
        factory.registerGUI(*SMCQtAppRegistryPlugin::ElementListGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ElementListGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::SearchGUIType, new CORE::Proxy<APP::IGUI, SMCQt::SearchGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ContextualTabGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ContextualTabGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateMilitarySymbolGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreateMilitarySymbolGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreatePolygonGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreatePolygonGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateLandmarkGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreateLandmarkGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ProjectManagerGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ProjectManagerGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::StereoSettingsGUIType, new CORE::Proxy<APP::IGUI, SMCQt::StereoSettingsGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::MilitaryPathAnimationGUIType, new CORE::Proxy<APP::IGUI, SMCQt::MilitaryPathAnimationGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::UndoRedoGUIType, new CORE::Proxy<APP::IGUI, SMCQt::UndoRedoGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::TimelineGUIType, new CORE::Proxy<APP::IGUI, SMCQt::TimelineGUI>());

        ///////////////// SMP gui
        factory.registerGUI(*SMCQtAppRegistryPlugin::LoginGUIType, new CORE::Proxy<APP::IGUI, SMCQt::LoginGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ResourceManagerGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ResourceManagerGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::NetworkJoinGUIType, new CORE::Proxy<APP::IGUI, SMCQt::NetworkJoinGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::NetworkPublishGUIType, new CORE::Proxy<APP::IGUI, SMCQt::NetworkPublishGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ScreenShareGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ScreenShareGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::PresentationGUIType, new CORE::Proxy<APP::IGUI, SMCQt::PresentationGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::MeasureDistanceGUIType, new CORE::Proxy<APP::IGUI, SMCQt::MeasureDistanceGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::MeasureSlopeGUIType, new CORE::Proxy<APP::IGUI, SMCQt::MeasureSlopeGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::MeasureAreaGUIType, new CORE::Proxy<APP::IGUI, SMCQt::MeasureAreaGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ElevationProfileGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ElevationProfileGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::TourGUIType, new CORE::Proxy<APP::IGUI, SMCQt::TourGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::SettingsGUIType, new CORE::Proxy<APP::IGUI, SMCQt::SettingsGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::LOSGUIType, new CORE::Proxy<APP::IGUI, SMCQt::LOSGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::SlopeAspectGUIType, new CORE::Proxy<APP::IGUI, SMCQt::SlopeAspectGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ColorElevationGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ColorElevationGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::AlmanacGUIType, new CORE::Proxy<APP::IGUI, SMCQt::AlmanacGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ScreenshotGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ScreenshotGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::RecordingGUIType, new CORE::Proxy<APP::IGUI, SMCQt::RecordingGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateLayerGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreateLayerGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::FeatureExportGUIType, new CORE::Proxy<APP::IGUI, SMCQt::FeatureExportGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::AddWebDataGUIType, new CORE::Proxy<APP::IGUI, SMCQt::AddWebDataGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ViewshedAnalysisGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ViewshedAnalysisGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::SensorRangeAnalysisGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::SensorRangeAnalysisGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::PointLayerPropertiesGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::PointLayerPropertiesGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::EventGUIType, new CORE::Proxy<APP::IGUI, SMCQt::EventGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ProjectSettingsGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::ProjectSettingsGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::GuiSettingsGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::GuiSettingsGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::AddServerLayerGUIType, new CORE::Proxy<APP::IGUI, SMCQt::AddServerLayerGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::DownloadRasterGUIType, new CORE::Proxy<APP::IGUI, SMCQt::DownloadRasterGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateOpacitySliderGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::CreateOpacitySliderGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateFeatureGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreateFeatureGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CrestClearanceGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CrestClearanceGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::RadioLOSGUIType, new CORE::Proxy<APP::IGUI, SMCQt::RadioLOSGUI>());
#ifdef WIN32
        factory.registerGUI(*SMCQtAppRegistryPlugin::TTSGUIType, new CORE::Proxy<APP::IGUI, SMCQt::TTSGUI>());
#endif //WIN32
        factory.registerGUI(*SMCQtAppRegistryPlugin::WebCamGUIType, new CORE::Proxy<APP::IGUI, SMCQt::WebCamGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::VisibilityControllerGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::VisibilityControllerGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateTMSGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreateTMSGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::RasterUploadDialogGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::RasterUploadDialogGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::PGCreateDataTypeDialogGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::PGCreateDataTypeDialogGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::FieldOfViewGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::FieldOfViewGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CacheSettingsGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::CacheSettingsGUI>());
		

#ifdef USE_QT4
        factory.registerGUI(*SMCQtAppRegistryPlugin::AppLauncherGUIType, new CORE::Proxy<APP::IGUI, SMCQt::AppLauncherGUI>());
#endif
#ifdef WIN32
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateCursorGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::CreateCursorGUI>());
#endif //WIN32
        factory.registerGUI(*SMCQtAppRegistryPlugin::PanSharpenGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::PanSharpenGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CVAGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::CVAGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::KMeanGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::KMeanGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::RasterBandReorderGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::RasterBandReorderGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::GetInformationGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::GetInformationGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::RasterEnhancementChangerGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::RasterEnhancementChangerGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::RasterWindowGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::RasterWindowGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ReprojectImageGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::ReprojectImageGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ExtractROIGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::ExtractROIGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::GunRangeGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::GunRangeGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::QueryBuilderGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::QueryBuilderGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ElementClassificationGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::ElementClassificationGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::RoadNetworkGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::RoadNetworkGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::DatabaseLayerGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::DatabaseLayerGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::QueryBuilderGISGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::QueryBuilderGISGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ViewBasedVisibilityGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::ViewBasedVisibilityGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::LineLayerPropertiesGUIType,
            new CORE::Proxy<APP::IGUI, SMCQt::LineLayerPropertiesGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::MinMaxElevGUIType, new CORE::Proxy<APP::IGUI, SMCQt::MinMaxElevGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::MeasureHeightGUIType, new CORE::Proxy<APP::IGUI, SMCQt::MeasureHeightGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ElevationDiffGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ElevationDiffGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::MeasurePerimeterGUIType, new CORE::Proxy<APP::IGUI, SMCQt::MeasurePerimeterGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::SteepestPathGUIType, new CORE::Proxy<APP::IGUI, SMCQt::SteepestPathGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ContoursGenerationGUIType, new CORE::Proxy<APP::IGUI, SMCQt::ContoursGenerationGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::HillshadeCalculationGUIType, new CORE::Proxy<APP::IGUI, SMCQt::HillshadeCalculationGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::TerrainExportGUIType, new CORE::Proxy<APP::IGUI, SMCQt::TerrainExportGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::AnimationGUIType, new CORE::Proxy<APP::IGUI, SMCQt::AnimationGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::FloodingGUIType, new CORE::Proxy<APP::IGUI, SMCQt::FloodingGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::WatershedGenerationGUIType, new CORE::Proxy<APP::IGUI, SMCQt::WatershedGenerationGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CutAndFillGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CutAndFillGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::FlyAroundGUIType,new CORE::Proxy<APP::IGUI, SMCQt::FlyAroundGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::DatabaseSettingsGUIType,new CORE::Proxy<APP::IGUI, SMCQt::DatabaseSettingsGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::GeorbISServersSettingsGUIType, new CORE::Proxy<APP::IGUI, SMCQt::GeorbISServersSettingsGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::StyleTemplateGUIType, new CORE::Proxy<APP::IGUI, SMCQt::StyleTemplateGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateOEFeatureGUIType, new CORE::Proxy<APP::IGUI, SMCQt::CreateOEFeatureGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::VectorLayerQueryGUIType, new CORE::Proxy<APP::IGUI, SMCQt::VectorLayerQueryGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::VectorLayerQueryBuilderGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::VectorLayerQueryBuilderGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::CreateTacticalSymbolsGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::CreateTacticalSymbolsGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ComputeServicesGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::ComputeServicesGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::AddBookmarksGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::AddBookmarksGUI>());

        factory.registerGUI(*SMCQtAppRegistryPlugin::ApplicationSettingsGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::ApplicationSettingsGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ColorPaletteGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::ColorPaletteGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::ColorLegendGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::ColorLegendGUI>());
        factory.registerGUI(*SMCQtAppRegistryPlugin::UserManualGUIType, new CORE::Proxy<APP::IGUI,
            SMCQt::UserManualGUI>());
        
    }

    void
        SMCQtAppRegistryPlugin::deregisterGUIs(APP::IGUIFactory& factory)
    {
#ifdef USE_QT4
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AppLauncherGUIType);
#endif

        factory.deregisterGUI(*SMCQtAppRegistryPlugin::EnvironmentGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::BottomPaneGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateLineGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreatePointGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AddContentGUIType);
#ifdef WIN32
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AddGPSGUIType);
#endif //WIN32
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ElementListGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::SearchGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ContextualTabGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateMilitarySymbolGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreatePolygonGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateLandmarkGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ProjectManagerGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::StereoSettingsGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::MilitaryPathAnimationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::UndoRedoGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::TimelineGUIType);
        /////////////// SMP GUI
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::LoginGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ResourceManagerGUIType);

        factory.deregisterGUI(*SMCQtAppRegistryPlugin::NetworkJoinGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::NetworkPublishGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ScreenShareGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::PresentationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::MeasureDistanceGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::MeasureSlopeGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::MeasureAreaGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ElevationProfileGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::TourGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::SettingsGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::LOSGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::SlopeAspectGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ColorElevationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AlmanacGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ScreenshotGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::RecordingGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateLayerGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::FeatureExportGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AddWebDataGUIType);

        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ViewshedAnalysisGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::SensorRangeAnalysisGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::PointLayerPropertiesGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::EventGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ProjectSettingsGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::GuiSettingsGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AddServerLayerGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::DownloadRasterGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateOpacitySliderGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateFeatureGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CrestClearanceGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::RadioLOSGUIType);
#ifdef WIN32
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::TTSGUIType);
#endif //WIN32
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::WebCamGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::VisibilityControllerGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::FieldOfViewGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CacheSettingsGUIType);
#ifdef WIN32
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateCursorGUIType);
#endif//WIN32
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::PanSharpenGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CVAGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::KMeanGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::GunRangeGUIType);


        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateTMSGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::RasterUploadDialogGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::PGCreateDataTypeDialogGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::RasterBandReorderGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::RasterEnhancementChangerGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::RasterWindowGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ReprojectImageGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ExtractROIGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::GetInformationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::QueryBuilderGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ElementClassificationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::RoadNetworkGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::DatabaseLayerGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::QueryBuilderGISGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ViewBasedVisibilityGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::LineLayerPropertiesGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::MinMaxElevGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::MeasureHeightGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ElevationDiffGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::MeasurePerimeterGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::SteepestPathGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ContoursGenerationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::HillshadeCalculationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::TerrainExportGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AnimationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::FloodingGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::WatershedGenerationGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CutAndFillGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::FlyAroundGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::DatabaseSettingsGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::GeorbISServersSettingsGUIType);

        factory.deregisterGUI(*SMCQtAppRegistryPlugin::StyleTemplateGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateOEFeatureGUIType);

        factory.deregisterGUI(*SMCQtAppRegistryPlugin::VectorLayerQueryGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::VectorLayerQueryBuilderGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::CreateTacticalSymbolsGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ComputeServicesGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::AddBookmarksGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ApplicationSettingsGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ColorPaletteGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::ColorLegendGUIType);
        factory.deregisterGUI(*SMCQtAppRegistryPlugin::UserManualGUIType);

    }

    const std::string& SMCQtAppRegistryPlugin::getLibraryName() const
    {
        return SMCQtAppRegistryPlugin::LibraryName;
    }

    const std::string&
        SMCQtAppRegistryPlugin::getPluginName() const
    {
        return SMCQtAppRegistryPlugin::PluginName;
    }

    //------------------------------------------------------------------------
    // SMCQt Application Plugin
    //------------------------------------------------------------------------
    const std::string SMCQtApplicationPlugin::LibraryName = UTIL::getPlatformLibraryName("SMCQt");
    const std::string SMCQtApplicationPlugin::PluginName = "SMCQt";

    SMCQtApplicationPlugin::~SMCQtApplicationPlugin(){}

    void SMCQtApplicationPlugin::load(const APP::IApplication& app)
    {
    }

    void SMCQtApplicationPlugin::unload(const APP::IApplication& app)
    {
    }

    const std::string& SMCQtApplicationPlugin::getLibraryName() const
    {
        return SMCQtApplicationPlugin::LibraryName;
    }

    const std::string& SMCQtApplicationPlugin::getPluginName() const
    {
        return SMCQtApplicationPlugin::PluginName;
    }
}

