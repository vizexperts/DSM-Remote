/****************************************************************************
*
* File             : ElevationProfileGUI.cpp
* Description      : ElevationProfileGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/ElevationProfileGUI.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Util/StringUtils.h>
#include <Util/CoordinateConversionUtils.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIconLoader.h>
#include <Elements/IRepresentation.h>
#include <Elements/ILineAlgebra.h>

#include <sstream>

#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>
#include <Core/ILine.h>
#include <Core/IVisibility.h>
#include <Core/ITemporary.h>


namespace SMCQt
{

    //----------------------------------------------------------------------------------------------------------------
    // Elevation profile GUI
    //----------------------------------------------------------------------------------------------------------------

    DEFINE_META_BASE(SMCQt, ElevationProfileGUI);

    ElevationProfileGUI::ElevationProfileGUI()
        : _markingEnabled(false)
    {}

    ElevationProfileGUI::~ElevationProfileGUI()
    {
    }

    void ElevationProfileGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        // connect to elevationProfileMenu loaded signal
        QObject* bottomLoader = _findChild("bottomLoader");
        if(bottomLoader)
        {
            QObject::connect(bottomLoader, SIGNAL(elevationProfileLoaded()), this, SLOT(connectMenu()),
                Qt::UniqueConnection);

            QObject::connect(this, SIGNAL(_updateGraphDisplay()), this, 
                SLOT(_onUpdateGraphDisplay()), Qt::QueuedConnection);
        }
    }

    void ElevationProfileGUI::connectMenu()
    {
        QObject* elevationProfile = _findChild("elevationProfile");
        if(elevationProfile)
        {
            QObject::connect(elevationProfile, SIGNAL(mark(bool)), this, 
                SLOT(markLine(bool)), Qt::UniqueConnection);

            QObject::connect(elevationProfile, SIGNAL(selectLine(bool)), this, 
                SLOT(selectLine(bool)), Qt::UniqueConnection);

            QObject::connect(elevationProfile, SIGNAL(start()), this,
                SLOT(startCalculation()), Qt::UniqueConnection);

            QObject::connect(elevationProfile, SIGNAL(stop()), this,
                SLOT(stopCalculation()), Qt::UniqueConnection);

            QObject::connect(elevationProfile, SIGNAL(closed()), this, 
                SLOT(menuClosed()), Qt::UniqueConnection);

            QObject::connect(elevationProfile, SIGNAL(showPointAtDistance(double)), this,
                SLOT(showPointAtDistance(double)), Qt::UniqueConnection);

            _elevationProfileHandler->getInterface<APP::IUIHandler>()->setFocus(true);
        }
    }

    void ElevationProfileGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _elevationProfileHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IElevationProfileUIHandler>(getGUIManager());       
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the elevation profile has been created
        else if(messageType == *SMCUI::IElevationProfileUIHandler::ProfileCalculatedMessageType)
        {
            _unsubscribe(_elevationProfileHandler.get(), *SMCUI::IElevationProfileUIHandler::ProfileCalculatedMessageType);
            emit _updateGraphDisplay();
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }

    void ElevationProfileGUI::menuClosed()
    {
//        stopCalculation();
        if(_markingEnabled)
        {
            markLine(false);
        }
        else
        {
            selectLine(false);
        }

        _elevationProfileHandler->getInterface<APP::IUIHandler>()->setFocus(false);

    }

    void ElevationProfileGUI::setActive(bool value)
    {
    }

    void ElevationProfileGUI::stopCalculation()
    {
        if(_elevationProfileHandler.valid())
        {
            _elevationProfileHandler->stop();
        }
    }

    void ElevationProfileGUI::startCalculation()
    {
        try        
        {
            _subscribe(_elevationProfileHandler.get(), *SMCUI::IElevationProfileUIHandler::ProfileCalculatedMessageType);

            QObject* elevationProfile = _findChild("elevationProfile");
            if(elevationProfile)
            {
                elevationProfile->setProperty("calculating", QVariant::fromValue(true));
            }

            _elevationProfileHandler->execute();
        }
        catch(const UTIL::Exception& e)
        {
            emit showError("ElevationProfile", QString::fromStdString(e.What()));

            QObject* elevationProfile = _findChild("elevationProfile");
            if(elevationProfile)
            {
                elevationProfile->setProperty("calculating", QVariant::fromValue(false));
            }
        }
    }

    void ElevationProfileGUI::_onUpdateGraphDisplay()
    {

        // Update min point
        osg::Vec3d minValue = _elevationProfileHandler->getMinimumElevationPoint();
        std::stringstream ss;
        ss << "Min :  " << minValue.z() << " m @ (" << minValue.y() << "N," << minValue.x() << "E" << ")";
        QString qminValueStr = QString::fromStdString(ss.str());

        // Update max point
        osg::Vec3d maxValue = _elevationProfileHandler->getMaximumElevationPoint();
        std::stringstream ssmax;
        ssmax << "Max :  " << maxValue.z() << " m @ (" << maxValue.y() << "N," << maxValue.x() << "E" << ")";
        QString qMaxValueStr = QString::fromStdString(ssmax.str());

        // Update average height
        double avgHeight = _elevationProfileHandler->getAverageElevation();
        std::stringstream ssavg;
        ssavg << "Avg :  " << avgHeight << " m";
        QString qAvgHeight = QString::fromStdString(ssavg.str());

        // Update curves
        const osg::Vec2dArray* points = _elevationProfileHandler->getDistanceAltitudePoints();
        std::vector<std::string> colorStrings = _elevationProfileHandler->getColorVector(); 

        if(points->size() == 0)
        {
            emit showError("Elevation Profile", "Error in computing profile.");
            QObject* elevationProfile = _findChild("elevationProfile");
            if(elevationProfile)
            {
                elevationProfile->setProperty("calculating", QVariant::fromValue(false));
            }
            return;
        }
        QVariantList xData;
        QVariantList yData;

        osg::Vec2dArray::const_iterator cIter = points->begin();

        for(; cIter != points->end(); cIter++){
            osg::Vec2d current = *cIter;
            xData.append(current.x());
            yData.append(current.y());
        }

        _setContextProperty("xData",QVariant::fromValue(xData));
        _setContextProperty("yData", QVariant::fromValue(yData));

        QVariantList colorArray;
        for (int i = 0; i < colorStrings.size(); i++)
        {
            colorArray.push_back(QVariant::fromValue(QString::fromStdString(colorStrings[i])));
        }
        _setContextProperty("colorArray", QVariant::fromValue(colorArray));

        QObject* elevationProfile = _findChild("elevationProfile");
        if(elevationProfile)
        {
            elevationProfile->setProperty("count", QVariant::fromValue(points->size()));
            elevationProfile->setProperty("yMin", QVariant::fromValue(minValue.z()));
            elevationProfile->setProperty("yMax", QVariant::fromValue(maxValue.z()));
            elevationProfile->setProperty("yAvg", QVariant::fromValue(avgHeight));
            elevationProfile->setProperty("minValueText", QVariant::fromValue(qminValueStr));
            elevationProfile->setProperty("maxValueText", QVariant::fromValue(qMaxValueStr));
            elevationProfile->setProperty("avgValueText", QVariant::fromValue(qAvgHeight));
            elevationProfile->setProperty("calculating", QVariant::fromValue(false));
            elevationProfile->setProperty("showGraph", QVariant::fromValue(true));
        }
    }

    void ElevationProfileGUI::showPointAtDistance(double distance)
    {
        try
        {
            if(!_currentPosition.valid())
            {
                _createCurrentPositionPoint();
            }

            //// Get the points
            osg::ref_ptr<const osg::Vec3dArray> revpoints = _elevationProfileHandler->getPoints();

            osg::ref_ptr<osg::Vec3dArray> points = new osg::Vec3dArray(); 

            //reverse lat lon 
            for (int p = 0; p < revpoints->size(); p++){
                points->push_back(osg::Vec3d((*revpoints)[p].y(), (*revpoints)[p].x(), (*revpoints)[p].z()));
            }

            double dis = (UTIL::CoordinateConversionUtils::GeodeticToECEF((*revpoints)[revpoints->size() - 1]) - UTIL::CoordinateConversionUtils::GeodeticToECEF((*revpoints)[0])).length(); 

            // Iterate to find the centre point        
            double d = 0.0;
            unsigned int i = 0;
            osg::Vec3d p1 = points->at(i);
            osg::Vec3d p2 = points->at(i + 1);
            osg::Vec3d ecefp1 = UTIL::CoordinateConversionUtils::GeodeticToECEF(p1);
            osg::Vec3d ecefp2 = UTIL::CoordinateConversionUtils::GeodeticToECEF(p2);
            for(;
                i < points->size() - 2;
                ++i)
            {
                p1 = points->at(i);
                p2 = points->at(i + 1);
                ecefp1 = UTIL::CoordinateConversionUtils::GeodeticToECEF(p1);
                ecefp2 = UTIL::CoordinateConversionUtils::GeodeticToECEF(p2);
                d += (ecefp2 - ecefp1).length();
                if(d > distance)
                {
                    d -= (ecefp2 - ecefp1).length();
                    break;
                }            
            }

            // Get interpolated value
            double p2w = distance - d;
            double p1w = (ecefp1 - ecefp2).length() - p2w;

            // Set point object position
            osg::Vec3d p(0.0, 0.0, 0.0);
            p.x() = (p2w * p2.x() + p1w * p1.x()) / (p1w + p2w);
            p.y() = (p2w * p2.y() + p1w * p1.y()) / (p1w + p2w);
            p.z() = (p2w * p2.z() + p1w * p1.z()) / (p1w + p2w);

            _currentPosition->setValue(p);

            std::stringstream ss;
            ss << "(" << p.x() << " , " << p.y() << " , " << p.z() << ")";

            _currentPosition->getInterface<CORE::IBase>()->setName(ss.str());

            CORE::IVisibility* visible =  _currentPosition->getInterface<CORE::IVisibility>();
            if(visible)
            {
                if(!visible->getVisibility())
                {
                    visible->setVisibility(true);
                }
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void ElevationProfileGUI::_createCurrentPositionPoint()
    {
        // Create overlay object
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
            CORE::CoreRegistry::instance()->getObjectFactory();
        CORE::RefPtr<CORE::IObject> cpObject = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

        CORE::RefPtr<CORE::IComponent> component = 
            CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");
        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = 
                component->getInterface<ELEMENTS::IIconLoader>();

            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = 
                cpObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolder.valid() && iconLoader.valid())
            {
                iconHolder->setIcon(iconLoader->getIconByName("circle"));
            }
        }

        if(cpObject.valid())
        {
            CORE::RefPtr<ELEMENTS::IRepresentation> representation = 
                cpObject->getInterface<ELEMENTS::IRepresentation>();
            if(representation.valid())
            {
                representation->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
            }
        }

        CORE::RefPtr<CORE::ITemporary> temp = cpObject->getInterface<CORE::ITemporary>();
        if(temp.valid())
        {
            temp->setTemporary(true);
        }

        CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        world->addObject(cpObject.get());

        _currentPosition = cpObject->getInterface<CORE::IPoint>(true);
    }

    void ElevationProfileGUI::markLine(bool value)
    {
        try{
            if(_elevationProfileHandler.valid())
            {
                _elevationProfileHandler->setMarking(value);

                QObject* elevationProfile = _findChild("elevationProfile");
                if(elevationProfile)
                {   
                    elevationProfile->setProperty("showGraph", QVariant::fromValue(false));
                }

                if(_currentPosition.valid())
                {
                    _currentPosition->getInterface<CORE::IVisibility>()->setVisibility(false);
                }

            }
        }
        catch(const UTIL::Exception& e)
        {
            emit showError("ElevationProfile", QString::fromStdString(e.What()));
        }
        _markingEnabled = value;
    }

    void ElevationProfileGUI::selectLine(bool value)
    {
        if(_elevationProfileHandler.valid())
        {
            _elevationProfileHandler->selectLine(value);

            QObject* elevationProfile = _findChild("elevationProfile");
            if(elevationProfile)
            {   
                elevationProfile->setProperty("showGraph", QVariant::fromValue(false));
            }
            if(_currentPosition.valid())
            {
                _currentPosition->getInterface<CORE::IVisibility>()->setVisibility(false);
            }
        }
    }

} // namespace SMCQt


