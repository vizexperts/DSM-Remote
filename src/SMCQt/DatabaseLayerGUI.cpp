#include <SMCQt/DatabaseLayerGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <Database/IDBRecord.h>
#include <Database/IDBTable.h>
#include <Database/IDBField.h>

#include <SMCUI/IAddContentStatusMessage.h>
#include <VizUI/ISelectionUIHandler.h>
#include <GIS/IHistogramStyle.h>

#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <Elements/ElementsUtils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, DatabaseLayerGUI);
    DEFINE_IREFERENCED(DatabaseLayerGUI, VizQt::QtGUI);

    DatabaseLayerGUI::DatabaseLayerGUI()
    {
        _oracleUILoaded = false;
        _incidentUILoaded = false;

        _defaultPalletColors.append(QColor(255,0, 0, 200));          //! Red
        _defaultPalletColors.append(QColor(0,255, 0, 200));          //! Lime
        _defaultPalletColors.append(QColor(0,0, 255, 200));          //! Blue
        _defaultPalletColors.append(QColor(255,255, 255, 200));      //! White
        _defaultPalletColors.append(QColor(255,255, 0, 200));        //! Yellow
        _defaultPalletColors.append(QColor(0,255, 255, 200));        //! Cyan/Aqua
        _defaultPalletColors.append(QColor(255,0, 255, 200));        //! Magenta
        _defaultPalletColors.append(QColor(192, 192, 192, 200));     //! Silver
        _defaultPalletColors.append(QColor(255, 69, 0, 200));        //! Orange
        _defaultPalletColors.append(QColor(128, 0, 0, 200));         //! Maroon
        _defaultPalletColors.append(QColor(85, 107, 47, 200));       //! Olive
        _defaultPalletColors.append(QColor(0, 128, 0, 200));         //! Green
        _defaultPalletColors.append(QColor(128, 0, 128, 200));       //! Purple
        _defaultPalletColors.append(QColor(0, 128, 128, 200));       //! Teal
        _defaultPalletColors.append(QColor(210,105, 30, 200));       //! Chocolate
        _defaultPalletColors.append(QColor(0, 0, 128, 200));         //! Navy
        _defaultPalletColors.append(QColor(0,255, 127, 200));        //! Spring green
        _defaultPalletColors.append(QColor(245,222, 179, 200));         //! Wheat
        _defaultPalletColors.append(QColor(255,250, 250, 200));      //! Snow
        _defaultPalletColors.append(QColor(255,105, 180, 200));      //! Hot pink
        _defaultPalletColors.append(QColor(255,228, 255, 200));      //! Misty Rose
        _defaultPalletColors.append(QColor(218,112, 214, 200));         //! Orchid
        _defaultPalletColors.append(QColor(0, 0, 0, 200));             //! Black
        _defaultPalletColors.append(QColor(250,128, 114, 200));         //! Salmon
        _defaultPalletColors.append(QColor(205,133, 63, 200));         //! Peru
    }

    DatabaseLayerGUI::~DatabaseLayerGUI()
    {}

    void DatabaseLayerGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(setProgressValue(int)),Qt::QueuedConnection);
    }

    void DatabaseLayerGUI::setProgressValue(int value)
    {        
      QObject* statusBar = _findChild("statusBar");
        if(value == 100)
        {
            if(statusBar)
            {
                QMetaObject::invokeMethod(statusBar, "setProgressStatus", 
                    Q_ARG(QVariant, QVariant(QString(""))),
                    Q_ARG(QVariant, QVariant(false)),Q_ARG(QVariant, QVariant(value)));
            }
        }
        else
        {
            if(statusBar)
            {
                QMetaObject::invokeMethod(statusBar, "setProgressStatus", 
                    Q_ARG(QVariant, QVariant(QString("Loading Vector data."))),
                    Q_ARG(QVariant, QVariant(false)),Q_ARG(QVariant, QVariant(value)));
            }
        }
    }

    void DatabaseLayerGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _databaseLayerLoaderUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IDatabaseLayerLoaderUIHandler>(getGUIManager());

                CORE::IComponent* comp = ELEMENTS::GetComponentFromMaintainer("IncidentLoaderComponent");
                if(comp)
                {
                    _incidentLoaderComponent = comp->getInterface<SMCElements::IIncidentLoaderComponent>();
                }

                _subscribe(_databaseLayerLoaderUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);
            }
            catch(const UTIL::Exception e)
            {
                e.LogException();
            }
        }
        else if(messageType == *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType)
        {
            SMCUI::IAddContentStatusMessage* addContentStatusMessage = message.getInterface<SMCUI::IAddContentStatusMessage>();

            int progressValue = addContentStatusMessage->getProgress();

            if(addContentStatusMessage->getStatus() == SMCUI::IAddContentStatusMessage::FAILURE)
            {   
                //Hide status bar 
                QObject* statusBar = _findChild("statusBar");
                if(statusBar)
                {
                    QMetaObject::invokeMethod(statusBar, "setStatus", 
                        Q_ARG(QVariant, QVariant(QString(""))),
                        Q_ARG(QVariant, QVariant(false)));
                    QMetaObject::invokeMethod(statusBar, "close");
                }
                emit showError("Data Loading Status", "Failed to Load Feature");
                
            }

            else
            {
                emit _setProgressValue(progressValue);
            }

        }
        DeclarativeFileGUI::update(messageType, message);
    }

    void DatabaseLayerGUI::popupLoaded(QString type)
    {
        if(type == "addDatabaseLayerPopup")
        {
            //!Populate List
            _populateUserspaceList("");
            _oracleUILoaded = true;
            _incidentUILoaded = false;
            //! Connect with slots
            QObject* addDatabaseLayerPopup = _findChild("addDatabaseLayerPopup");
            if(addDatabaseLayerPopup)
            {
                QObject::connect(addDatabaseLayerPopup, SIGNAL(addOracleTable()), 
                    this, SLOT(addOracleTable()), Qt::UniqueConnection);

                QObject::connect(addDatabaseLayerPopup, SIGNAL(userspaceChanged(QString)), 
                    this, SLOT(userspaceChanged(QString)), Qt::UniqueConnection);

                QObject::connect(addDatabaseLayerPopup, SIGNAL(closed()), 
                    this, SLOT(popupClosed()), Qt::UniqueConnection);
            }
        }
        else if(type == "QueryBuilderGIS")
        {
            QObject* QueryBuilderGIS = _findChild("QueryBuilderGIS");
            if(QueryBuilderGIS)
            {
                if(_oracleUILoaded)
                {
                    //! Set current userspace name and table name
                    QueryBuilderGIS->setProperty("userspaceName", QVariant::fromValue(QString::fromStdString(_currentUserspaceName)));
                    QueryBuilderGIS->setProperty("tableName", QVariant::fromValue(QString::fromStdString(_currentTableName)));
                    QueryBuilderGIS->setProperty("uiType", QVariant::fromValue(QString::fromStdString("oracle")));
                }
                else if(_incidentUILoaded)
                {
                    //! Set current userspace name and table name
                    QueryBuilderGIS->setProperty("incidentName", QVariant::fromValue(QString::fromStdString(_selectedIncident)));
                    //QueryBuilderGIS->setProperty("tableName", QVariant::fromValue(QString::fromStdString(_currentTableName)));
                    QueryBuilderGIS->setProperty("uiType", QVariant::fromValue(QString::fromStdString("incident")));
                }
            }
        }
        else if(type == "addIncidentDataPopup")
        {
            _oracleUILoaded = false;
            _incidentUILoaded = true;
            QObject* addDatabaseLayerPopup = _findChild("addIncidentDataPopup");

            _incidentGroupedQueryStack.clear();
            groupedQuerySelected(0, false, true);

            //! Today morning
            QDateTime qToDateTime(QDateTime::currentDateTime().date());
            QDateTime qFromDateTime = qToDateTime.addDays(-1);

            addDatabaseLayerPopup->setProperty("fromTime", QVariant::fromValue(qFromDateTime));
            addDatabaseLayerPopup->setProperty("toTime", QVariant::fromValue(qToDateTime));

            QList<QObject*> comboBoxList;

            if(_incidentLoaderComponent.valid())
            {
                const SMCElements::IIncidentLoaderComponent::IncidentTypeMap& incidentMap = _incidentLoaderComponent->getIncidentMap();
                SMCElements::IIncidentLoaderComponent::IncidentTypeMap::const_iterator incidentMapIter = incidentMap.begin();
                unsigned int count = 0;
                for(; incidentMapIter != incidentMap.end(); ++incidentMapIter)
                {
                    std::string incidentType = incidentMapIter->first;
                    if(count==0)
                    {
                        addIncidentType(incidentType.c_str());
                    }
                    comboBoxList.append(new VizComboBoxElement(incidentType.c_str(), QString::number(count++)));
                }
            }

            _setContextProperty("incidentTypeList", QVariant::fromValue(comboBoxList));

            QObject::connect(addDatabaseLayerPopup, SIGNAL(addIncidentLayer()), this, SLOT(addIncidentLayer()), Qt::UniqueConnection);
            QObject::connect(addDatabaseLayerPopup, SIGNAL(addIncidentType(QString)), this, SLOT(addIncidentType(QString)), Qt::UniqueConnection);
            QObject::connect(addDatabaseLayerPopup, SIGNAL(plottingTypeChanged(QString)), this, SLOT(plottingTypeChanged(QString)), Qt::UniqueConnection);
            QObject::connect(addDatabaseLayerPopup, SIGNAL(addAllIncidents()), this, SLOT(addAllIncidents()), Qt::UniqueConnection);
            QObject::connect(addDatabaseLayerPopup, SIGNAL(addGroupedQuery()), this, SLOT(addGroupedQuery()), Qt::UniqueConnection);
            QObject::connect(addDatabaseLayerPopup, SIGNAL(removeGroupedQuery(int)), this, SLOT(removeGroupedQuery(int)), Qt::UniqueConnection);
            QObject::connect(addDatabaseLayerPopup, SIGNAL(groupedQuerySelected(int)), this, SLOT(groupedQuerySelected(int)), Qt::UniqueConnection);
            QObject::connect(addDatabaseLayerPopup, SIGNAL(renameQuery()), this, SLOT(renameQuery()), Qt::UniqueConnection);

        }
    }

    void DatabaseLayerGUI::addGroupedQuery()
    {
        QObject* addDatabaseLayerPopup = _findChild("addIncidentDataPopup");
        if(!addDatabaseLayerPopup)
            return;
    
        groupedQuerySelected(_incidentGroupedQueryStack.size(), true);
    }

    void DatabaseLayerGUI::removeGroupedQuery(int index)
    {
        QObject* addDatabaseLayerPopup = _findChild("addIncidentDataPopup");
        if(!addDatabaseLayerPopup)
            return;

        if(index >= _incidentGroupedQueryStack.size())
            return;

        _incidentGroupedQueryStack.removeAt(index);
        _populateQueryNameList();
        if(_incidentGroupedQueryStack.size() == 0)
            return;

        if(index == 0)
        {
            groupedQuerySelected(0, false, true);
        }
        else
        {
            groupedQuerySelected(index-1, false, true);
        }
    }

    void DatabaseLayerGUI::groupedQuerySelected(int index, bool addingNew, bool selectionOnly)
    {
        QObject* addDatabaseLayerPopup = _findChild("addIncidentDataPopup");
        if(!addDatabaseLayerPopup)
            return;

        //! previous
        IncidentGroupedQuery groupedQuery;

        groupedQuery._incidentType = addDatabaseLayerPopup->property("incidentType").toString();
        groupedQuery._queryName = addDatabaseLayerPopup->property("queryName").toString();;
        groupedQuery._whereClause = addDatabaseLayerPopup->property("whereClause").toString();
        groupedQuery._toTime = addDatabaseLayerPopup->property("toTime").toDateTime();
        groupedQuery._fromTime = addDatabaseLayerPopup->property("fromTime").toDateTime();

        int previousGroupedQueryIndex = addDatabaseLayerPopup->property("previousGroupedQueryIndex").toInt();

        int palleteIndex = previousGroupedQueryIndex;

        if(palleteIndex >= _defaultPalletColors.size())
        {
            palleteIndex = palleteIndex%(_defaultPalletColors.size());
        }

        groupedQuery._queryColor = _defaultPalletColors[palleteIndex];

        if(addingNew)
        {
            _incidentGroupedQueryStack.push_back(groupedQuery);    
        }
        else if(!selectionOnly && previousGroupedQueryIndex < _incidentGroupedQueryStack.size())
        {
            _incidentGroupedQueryStack[previousGroupedQueryIndex] = groupedQuery;
        }

        if(index < _incidentGroupedQueryStack.size())
        {
            groupedQuery = _incidentGroupedQueryStack[index];

            _selectedIncident = groupedQuery._incidentType.toStdString();

            addDatabaseLayerPopup->setProperty("queryName", QVariant::fromValue(groupedQuery._queryName));
            addDatabaseLayerPopup->setProperty("incidentType", QVariant::fromValue(groupedQuery._incidentType));
            addDatabaseLayerPopup->setProperty("whereClause", QVariant::fromValue(groupedQuery._whereClause));
            addDatabaseLayerPopup->setProperty("toTime", QVariant::fromValue(groupedQuery._toTime));
            addDatabaseLayerPopup->setProperty("fromTime", QVariant::fromValue(groupedQuery._fromTime));
        }        

        _populateQueryNameList();
        addDatabaseLayerPopup->setProperty("previousGroupedQueryIndex", QVariant::fromValue(index));
    }

    void DatabaseLayerGUI::renameQuery(){
    }

    void DatabaseLayerGUI::addAllIncidents()
    { 
        QObject* addDatabaseLayerPopup = _findChild("addIncidentDataPopup");
        if(!addDatabaseLayerPopup)
            return;

        _incidentGroupedQueryStack.clear();

        if(_incidentLoaderComponent.valid()){
            const SMCElements::IIncidentLoaderComponent::IncidentTypeMap& incidentMap = _incidentLoaderComponent->getIncidentMap();
            SMCElements::IIncidentLoaderComponent::IncidentTypeMap::const_iterator incidentMapIter = incidentMap.begin();

            int i = 0;

            for(; incidentMapIter != incidentMap.end(); ++incidentMapIter)
            {
                std::string incidentType = incidentMapIter->first;
                std::string queryName =  incidentType;

                IncidentGroupedQuery groupedQuery;
                groupedQuery._queryName = QString::fromStdString(queryName); 
                groupedQuery._incidentType = QString::fromStdString(incidentType);
                groupedQuery._whereClause = addDatabaseLayerPopup->property("whereClause").toString();
                groupedQuery._toTime = addDatabaseLayerPopup->property("toTime").toDateTime();
                groupedQuery._fromTime = addDatabaseLayerPopup->property("fromTime").toDateTime();
                int palleteIndex = i;

                if(palleteIndex >= _defaultPalletColors.size())
                {
                    palleteIndex = palleteIndex%(_defaultPalletColors.size());
                }

                groupedQuery._queryColor = _defaultPalletColors[palleteIndex];
                _incidentGroupedQueryStack.push_back(groupedQuery); 

                i++;
            }

            groupedQuerySelected(_incidentGroupedQueryStack.size()-1, false, true);
        }
    }

    void DatabaseLayerGUI::plottingTypeChanged(QString plottingType)
    {
        if(!plottingType.compare(QString("Bar/Frequency"))){
           // addGroupedQuery();
        }
    }

    void DatabaseLayerGUI::addIncidentType(QString incidentName)
    {
        _selectedIncident = incidentName.toStdString();

        QObject* addIncidentPopup = _findChild("addIncidentDataPopup");
        if(addIncidentPopup)
        {
            std::string layerName = addIncidentPopup->property("layerName").toString().toStdString();

            addIncidentPopup->setProperty("layerName", _selectedIncident.c_str());
            addIncidentPopup->setProperty("queryName", _selectedIncident.c_str());

        }
    }

    void DatabaseLayerGUI::popupClosed()
    {
        _currentDatabaseName = "";
        _currentUserspaceName = "";
        _currentTableName = "";

        _setContextProperty("queryNameList", QVariant::fromValue(NULL));
        _setContextProperty("queryName", QVariant::fromValue(NULL));
        _setContextProperty("dbUserspaceListModel", QVariant::fromValue(NULL));
        _setContextProperty("dbTablenameListModel", QVariant::fromValue(NULL));
    }

    void DatabaseLayerGUI::userspaceChanged(QString userspaceName)
    {
        if(!_databaseLayerLoaderUIHandler.valid())
            return;

        _currentUserspaceName = userspaceName.toStdString();

        std::string queryStatement = "SELECT table_name from all_tables WHERE owner = '" + userspaceName.toStdString() +"' AND num_rows > 0 group by table_name order by table_name";

        CORE::RefPtr<DATABASE::ISQLQueryResult> result = _databaseLayerLoaderUIHandler->executeSQLQuery(queryStatement,SMCUI::IDatabaseLayerLoaderUIHandler::DBType_Oracle);
        if(!result.valid())
            return;

        CORE::RefPtr<DATABASE::IDBTable> table = result->getResultTable();
        if(!table.valid())
            return;

        QList<QObject*> comboBoxList;

        CORE::RefPtr<DATABASE::IDBRecord> record = NULL;
        int count = 0;
        while((record = table->getNextRecord()) != NULL)
        {
            DATABASE::IDBField* field = record->getFieldByIndex(0);

            std::string ownerName = field->toString();
            comboBoxList.append(new VizComboBoxElement(ownerName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("dbTablenameListModel", QVariant::fromValue(comboBoxList));
    }

    void DatabaseLayerGUI::_populateQueryNameList()
    {
        QObject* addDatabaseLayerPopup = _findChild("addIncidentDataPopup");
        if(!addDatabaseLayerPopup)
            return;
        int i = 0;
        QStringList queryNameList;
        queryNameList.clear();
        foreach(IncidentGroupedQuery incidentQuery, _incidentGroupedQueryStack)
        {
                queryNameList.push_back(incidentQuery._queryName);
                i++;
        };

        _setContextProperty("queryNameList", QVariant::fromValue(queryNameList));
    }

    void DatabaseLayerGUI::_populateUserspaceList(QString databaseName)
    {
        if(!_databaseLayerLoaderUIHandler.valid())
            return;

        _currentDatabaseName = databaseName.toStdString();

        std::string queryStatement = "SELECT owner FROM all_tables group by owner order by owner";

        CORE::RefPtr<DATABASE::ISQLQueryResult> result = _databaseLayerLoaderUIHandler->executeSQLQuery(queryStatement,SMCUI::IDatabaseLayerLoaderUIHandler::DBType_Oracle);
        if(!result.valid())
            return;

        CORE::RefPtr<DATABASE::IDBTable> table = result->getResultTable();
        if(!table.valid())
            return;

        QList<QObject*> comboBoxList;

        CORE::RefPtr<DATABASE::IDBRecord> record = NULL;
        int count = 0;
        while((record = table->getNextRecord()) != NULL)
        {
            DATABASE::IDBField* field = record->getFieldByIndex(0);

            std::string ownerName = field->toString();
            comboBoxList.append(new VizComboBoxElement(ownerName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("dbUserspaceListModel", QVariant::fromValue(comboBoxList));
    }

    void DatabaseLayerGUI::addOracleTable()
    {
        if(_databaseLayerLoaderUIHandler.valid())
        {

            CORE::RefPtr<DB::ReaderWriter::Options> options = new DB::ReaderWriter::Options();

            options->setMapValue("Offset", UTIL::ToString(10));
            options->setMapValue("Clamping", UTIL::ToString(true));
            options->setMapValue("Editable", UTIL::ToString(false));
            options->setMapValue("TileLoader", UTIL::ToString(false));

            std::string layerName;
            std::string uniqueColumnName;
            std::string sqlWhere;

            std::string bottomLeftLong;
            std::string bottomLeftLat;
            std::string topRightLong;
            std::string topRightLat;


            QObject* addDatabaseLayerPopup = _findChild("addDatabaseLayerPopup");
            if(addDatabaseLayerPopup)
            {
                layerName = addDatabaseLayerPopup->property("layerName").toString().toStdString();
                uniqueColumnName = addDatabaseLayerPopup->property("uidColumnName").toString().toStdString();
                sqlWhere = addDatabaseLayerPopup->property("whereClause").toString().toStdString();

                if(uniqueColumnName == "<None>")
                    uniqueColumnName = "";

                bool okX = true;
                bottomLeftLong = addDatabaseLayerPopup->property("bottomLeftLong").toString().toStdString();
                okX = !bottomLeftLong.empty();

                bool okY = true;
                bottomLeftLat = addDatabaseLayerPopup->property("bottomLeftLat").toString().toStdString();
                okY = !bottomLeftLat.empty();

                bool okZ = true;
                topRightLong = addDatabaseLayerPopup->property("topRightLong").toString().toStdString();
                okZ = !topRightLong.empty();

                bool okW = true;
                topRightLat = addDatabaseLayerPopup->property("topRightLat").toString().toStdString();
                okW = !topRightLat.empty();

                bool completedAreaSpecified = okX & okY & okZ & okW;
                bool partialAreaSpecified = okX | okY | okZ | okW;

                if(!completedAreaSpecified && partialAreaSpecified)
                {
                    showError( "Area Extents Error", "Please Enter Valid Extents Or Remain All Empty");
                    return;
                }

                if(completedAreaSpecified)
                {
                    options->setMapValue("bottomLeftLong", bottomLeftLong);
                    options->setMapValue("bottomLeftLat", bottomLeftLat);
                    options->setMapValue("topRightLong", topRightLong);
                    options->setMapValue("topRightLat", topRightLat);
                }
            }


            options->setMapValue("LayerName", layerName);

            options->setMapValue("UserspaceName", _currentUserspaceName);
            options->setMapValue("Tablename", _currentTableName);
            options->setMapValue("Where", sqlWhere);

            _databaseLayerLoaderUIHandler->loadLayer(options, SMCUI::IDatabaseLayerLoaderUIHandler::DBType_Oracle);

            QMetaObject::invokeMethod(addDatabaseLayerPopup, "closePopup");
        }

    }

    bool DatabaseLayerGUI::_validateIncidentTable(const SMCElements::IIncidentLoaderComponent::IncidentType& incidentType)
    {
        //! NP: Following is the thought process for the implementation of this function
        //! Check wether table exists
        //! Check wether all the referenced row exists
        //! But our pre built queries could be composed from multiple tables 

        //! So just try to tun the sql statement associated with the pre built query, Limited to a single record
        //! Do not try to fetch the records

        std::string queryStatement;
        queryStatement += "SELECT ";
        queryStatement += " TOP 1 ";
        std::map<std::string, std::string>::const_iterator selectIter = incidentType.select.begin();
        for(; selectIter != incidentType.select.end(); ++selectIter)
        {
            queryStatement += " " + selectIter->first + " as '" + selectIter->second + "',";
        }

        //! remove the last character
        queryStatement.erase(queryStatement.length() - 1, 1);

        queryStatement += " FROM " + incidentType.from;

        CORE::RefPtr<DATABASE::ISQLQueryResult> result = _databaseLayerLoaderUIHandler->executeSQLQuery(queryStatement,SMCUI::IDatabaseLayerLoaderUIHandler::DBType_MSSQL);
        if(!result.valid())
            return false;

        return true;
    }

    void DatabaseLayerGUI::addIncidentLayer()
    {
        if(!_incidentLoaderComponent.valid())
            return;

        if(!_databaseLayerLoaderUIHandler.valid())
            return;

        if(!_databaseLayerLoaderUIHandler->connectToDB(SMCUI::IDatabaseLayerLoaderUIHandler::DBType_MSSQL))
        {
            emit showError("Database Connection Error", "Not able to connect to database.");
            return;
        }
        
        QObject* statusBar = _findChild("statusBar");
        if(statusBar)
            {
                QMetaObject::invokeMethod(statusBar, "setStatus", 
                    Q_ARG(QVariant, QVariant(QString("Executing Query !!"))),
                    Q_ARG(QVariant, QVariant(false)));
            }

        CORE::RefPtr<DB::ReaderWriter::Options> options = new DB::ReaderWriter::Options();

        options->setMapValue("Offset", UTIL::ToString(10));
        options->setMapValue("Clamping", UTIL::ToString(true));
        options->setMapValue("Editable", UTIL::ToString(false));
        options->setMapValue("TileLoader", UTIL::ToString(false));

        QString dateTimeFormat = "yyyy-MM-dd hh:mm:ss";
        std::string layerName;
        std::string annotation;
        std::string startDate;
        std::string endDate;
        std::string whereClause;
        int plotType = 0;
        int previousGroupedQueryIndex = 0;
        std::string groupBy;
        std::string fromTime;
        std::string toTime;

        std::string bottomLeftLong;
        std::string bottomLeftLat;
        std::string topRightLong;
        std::string topRightLat;

        QObject* addIncidentLayerPopup = _findChild("addIncidentDataPopup");
        if(!addIncidentLayerPopup)
        {
            return;
        }

        layerName = addIncidentLayerPopup->property("layerName").toString().toStdString();
        plotType = addIncidentLayerPopup->property("plotType").toInt();
        previousGroupedQueryIndex = addIncidentLayerPopup->property("previousGroupedQueryIndex").toInt();

        bool okX = true;
        bottomLeftLong = addIncidentLayerPopup->property("bottomLeftLong").toString().toStdString();
        okX = !bottomLeftLong.empty();

        bool okY = true;
        bottomLeftLat = addIncidentLayerPopup->property("bottomLeftLat").toString().toStdString();
        okY = !bottomLeftLat.empty();

        bool okZ = true;
        topRightLong = addIncidentLayerPopup->property("topRightLong").toString().toStdString();
        okZ = !topRightLong.empty();

        bool okW = true;
        topRightLat = addIncidentLayerPopup->property("topRightLat").toString().toStdString();
        okW = !topRightLat.empty();

        bool completedAreaSpecified = okX & okY & okZ & okW;
        bool partialAreaSpecified = okX | okY | okZ | okW;

        if(!completedAreaSpecified && partialAreaSpecified)
        {
            showError( "Area Extents Error", "Please Enter Valid Extents Or Remain All Empty");
            return;
        }

        bool useGridReference = false;
        if(useGridReference)
        {
            //options->setMapValue("MatX", _databaseLayerLoaderUIHandler->getAttributeValue("Mapsheet"));
            //options->setMapValue("MatY", _databaseLayerLoaderUIHandler->getAttributeValue("Gr"));
            options->setMapValue("CoordType", "GridRef");
        }
        else
        {
            options->setMapValue("MatX", "Longitude");            
            options->setMapValue("MatY", "Latitude");
            options->setMapValue("CoordType", "LatLong");
        }

        const SMCElements::IIncidentLoaderComponent::IncidentTypeMap& incidentMap = _incidentLoaderComponent->getIncidentMap();

        std::string fullQuerySelectStatement;
        std::string fullQueryFromStatement;

        if (_incidentGroupedQueryStack.size() > 1)
        {
            //fullQuerySelectStatement += "SELECT a.Name as 'Name', a.Latitude as 'Latitude', a.Longitude as 'Longitude' ";
        }

        SMCElements::IIncidentLoaderComponent::IncidentTypeMap::const_iterator incidentMapIter;

        //! Number of sub queries added invariate
        int numGroupedQueriesAdded = 0;

        //! Query names and query colors used in case of grouped queries 
        std::string queryNames;
        std::string colorValues;
        std::string queryValues; 

        if (_incidentGroupedQueryStack.empty())
        {
            groupedQuerySelected(0, true);
        }
		
        for(int i = 0; i < _incidentGroupedQueryStack.size(); i++)
        {
            if((i < _incidentGroupedQueryStack.size()) && (i != previousGroupedQueryIndex))
            {
                IncidentGroupedQuery groupedQuery = _incidentGroupedQueryStack[i];
                incidentMapIter = incidentMap.find(groupedQuery._incidentType.toStdString());
                fromTime = groupedQuery._fromTime.toString(dateTimeFormat).toStdString();
                toTime = groupedQuery._toTime.toString(dateTimeFormat).toStdString();
                whereClause = groupedQuery._whereClause.toStdString();
            }
            else
            {
                incidentMapIter = incidentMap.find(_selectedIncident);
                fromTime = addIncidentLayerPopup->property("fromTime").toDateTime().toString(dateTimeFormat).toStdString();
                toTime = addIncidentLayerPopup->property("toTime").toDateTime().toString(dateTimeFormat).toStdString();
                whereClause = addIncidentLayerPopup->property("whereClause").toString().toStdString();
            }

            if(incidentMapIter == incidentMap.end())
            {
                return;
            }

            const SMCElements::IIncidentLoaderComponent::IncidentType& incidentType = incidentMapIter->second;

            //! Check wether the incident table is valid or not
            if(!_validateIncidentTable(incidentType))
            {
                continue;
            }

            std::string queryStatement;

            groupBy = addIncidentLayerPopup->property("groupBy").toString().toStdString();
            
            if(plotType == 0 && _incidentGroupedQueryStack.size() == 1)
            {
                queryStatement += "SELECT";
                std::map<std::string, std::string>::const_iterator selectIter = incidentType.select.begin();
                for(; selectIter != incidentType.select.end(); ++selectIter)
                {
                    queryStatement += " " + selectIter->first + " as '" + selectIter->second + "',";
                }

                //! remove the last character
                queryStatement.erase(queryStatement.length() - 1, 1);

            }
            else
            {
                if(numGroupedQueriesAdded != 0)
                {
                    //queryStatement += " left join ";
                    queryStatement += " full outer join ";
                }

                if(_incidentGroupedQueryStack.size() > 1)
                    queryStatement += "( ";

                queryStatement += "SELECT";
                std::map<std::string, std::string>::const_iterator selectIter = incidentType.select.begin();

                for(; selectIter != incidentType.select.end(); ++selectIter)
                {
                    if(selectIter->second == groupBy)
                    {
                        groupBy = selectIter->first;
                        std::string tableName = groupBy.substr(0, groupBy.find_first_of('.'));

                        queryStatement += " " + selectIter->first + " as 'Name'";
                        //if(i == 0)
                        {
                            queryStatement += " ," + tableName + ".latitude as 'Latitude'";
                            queryStatement += " ," + tableName + ".longitude as 'Longitude'";
                        }
                        break;
                    }
                }

                if(_incidentGroupedQueryStack.size() > 1)
                {
                    fullQuerySelectStatement += ", value" + UTIL::ToString(numGroupedQueriesAdded) + " ";
                }

                queryStatement += " , COUNT(*) as 'value" + UTIL::ToString(numGroupedQueriesAdded) +"' "; 
            }

            queryStatement += " FROM " + incidentType.from;

            //! Only use this condition in case of point scatter
            //! this would usually contain incident_master latitude and incident_master.longitude condition
            if(!incidentType.whereClause.empty() && plotType == 0)
            {
                if(!whereClause.empty())
                {
                    whereClause += " and ";
                }

                whereClause += incidentType.whereClause;
            }

            if(!fromTime.empty())
            {
                if(!whereClause.empty())
                    whereClause += " and ";
                whereClause += incidentType.tableName + "." + incidentType.dateColName + " >= '" + fromTime + "' ";
            }

            if(!toTime.empty())
            {
                if(!whereClause.empty())
                    whereClause += " and ";
                whereClause += incidentType.tableName + "." + incidentType.dateColName + " <= '" + toTime + "' ";
            }

            if(!bottomLeftLat.empty())      //! Assuming everything else is fine or else we wouldnt reach here.
            {
                if(!whereClause.empty())
                    whereClause += " and ";

                whereClause += incidentType.tableName + ".latitude >= '" + bottomLeftLat + "' ";
                whereClause += " and " + incidentType.tableName + ".longitude >= '" + bottomLeftLong + "' ";
                whereClause += " and " + incidentType.tableName + ".latitude <= '" + topRightLat + "' ";
                whereClause += " and " + incidentType.tableName + ".longitude <= '" + topRightLong + "' ";
            }
            
            queryStatement += " WHERE " + whereClause;

            if(plotType != 0)
            {
                std::string tableName = groupBy.substr(0, groupBy.find_first_of('.'));
                
                queryStatement += " and " + tableName + ".latitude is not NULL ";
                queryStatement += " and " + tableName + ".latitude != 0 ";
                queryStatement += " and " + tableName + ".longitude is not NULL ";
                queryStatement += " and " + tableName + ".longitude != 0 ";
                queryStatement += " GROUP BY " + groupBy;
                                
                //if(i == 0)
                {
                    queryStatement += " , " + tableName + ".latitude" + " , " + tableName + ".longitude";
                }

                if(_incidentGroupedQueryStack.size() > 1)
                {
                    char ch = 'a' + i;

                    queryStatement += " ) ";
                    queryStatement += ch;
                    if(i > 0)
                    {
                        /*queryStatement += " on (a.Name = ";
                        queryStatement += ch;
                        queryStatement += ".Name) ";*/
                        queryStatement += " on (a.Latitude = ";
                        queryStatement += ch;
                        queryStatement += ".Latitude ";
                        queryStatement += "AND ";

                        queryStatement += "a.Longitude = ";
                        queryStatement += ch;
                        queryStatement += ".Longitude )";
                    }
                }
            }
            else
            {
                queryStatement += " and " + incidentType.tableName + ".latitude is not NULL ";
                queryStatement += " and " + incidentType.tableName + ".latitude != 0 ";
                queryStatement += " and " + incidentType.tableName + ".longitude is not NULL ";
                queryStatement += " and " + incidentType.tableName + ".longitude != 0 ";
            }

            //! Create name and color list only in case of histogram representation
            if(plotType == 2)
            {
                IncidentGroupedQuery incident = _incidentGroupedQueryStack[i];

                queryNames += incident._queryName.toStdString() + ":";
                osg::Vec4 color((float)incident._queryColor.red() / 255.0f, (float)incident._queryColor.green() / 255.0f,
                    (float)incident._queryColor.blue() / 255.0f, (float)incident._queryColor.alpha() / 255.0f);

                colorValues += UTIL::ToString(color.r()) + "," + UTIL::ToString(color.g()) + "," + UTIL::ToString(color.b()) + "," + UTIL::ToString(color.a()) + ";";
                queryValues += "0 ";
            }

            fullQueryFromStatement += queryStatement;
            numGroupedQueriesAdded++;

        }

        if (_incidentGroupedQueryStack.size() > 1)
        {
            std::string selectStatement = "SELECT a.Name as \'Name\', ";

            std::string tableName = groupBy.substr(0, groupBy.find_first_of('.'));


            std::string latitude, longitude;
            char ch = 'a';
            for (int i = 0; i < numGroupedQueriesAdded; i++)
            {
                latitude += ch + i;
                latitude += ".Latitude ";
                if (i < _incidentGroupedQueryStack.size() - 1)
                    latitude += ", ";


                longitude += ch + i;
                longitude += ".Longitude ";
                if (i < _incidentGroupedQueryStack.size() - 1)
                    longitude += ", ";
            }
            selectStatement += "COALESCE(" + latitude + ") as 'Latitude', ";
            selectStatement += "COALESCE(" + longitude + ") as 'Longitude'";

            fullQuerySelectStatement.insert(0, selectStatement);
        }
        if(!fullQuerySelectStatement.empty())
        {
            fullQuerySelectStatement += " FROM ";
        }

        fullQuerySelectStatement += fullQueryFromStatement;

        LOG_DEBUG(fullQuerySelectStatement);

        if(plotType == 2)
        {
            options->setMapValue("DrawTechnique", "HistogramDrawProperty");

            options->setMapValue(GIS::IHistogramStyle::HISTOGRAM_NAMES, queryNames);
            options->setMapValue(GIS::IHistogramStyle::HISTOGRAM_COLOR, colorValues);
        }

        options->setMapValue("LayerName", layerName);
        options->setMapValue("LayerType", "PrebuiltQuery");

        incidentMapIter = incidentMap.find(_selectedIncident);
        if(incidentMapIter == incidentMap.end())
        {
            return;
        }

        const SMCElements::IIncidentLoaderComponent::IncidentType& incidentType = incidentMapIter->second;

        options->setMapValue("SkipList", incidentType.skipList);
        options->setMapValue("query", fullQuerySelectStatement);
        options->setMapValue(GIS::IHistogramStyle::HISTOGRAM_VALUE, queryValues); 
       _databaseLayerLoaderUIHandler->loadLayer(options, SMCUI::IDatabaseLayerLoaderUIHandler::DBType_MSSQL);
      
        QMetaObject::invokeMethod(addIncidentLayerPopup, "closePopup");

    }
}