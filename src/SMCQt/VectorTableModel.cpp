#include <SMCQt/VectorTableModel.h>

#include <Core/RefPtr.h>
#include <QDateTimeEdit>
#include <iostream>
#include<TERRAIN/IModelObject.h>
#include <Util/FileUtils.h>

namespace SMCQt
{
    
    VectorTableModel::VectorTableModel(QObject* parent, std::string url)
        :QAbstractTableModel(parent)
    {

        /* 
        Here Original SHP is loaded in Original OGR Layer and we also making a In memory Copy of original layer.
        so Temporary Change will not Effect on Original layer and When user want to save then dump in memory OGR Layer in New shp file or overwrite or Reload object
        
        */
        OGRDataSource *ogrDSForOrignal = OGRSFDriverRegistrar::Open(url.c_str(), false);
        int firstLayer = 0;
        if (ogrDSForOrignal && ogrDSForOrignal->GetLayerCount() > 0)
        {
            OGRLayer  *orignalOGRLayer = ogrDSForOrignal->GetLayer(0);
            //xxxxxxxxxxxxxxxxxxxxxxx
            std::string driverName = "MEMORY";
            OGRRegisterAll();
            OGRSFDriver* ogrDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(driverName.c_str());
            _ogrDSForMemory = ogrDriver->CreateDataSource("memData");
            char** options = new char*[3];
            char* option = CPLStrdup("OVERWRITE=TRUE");
            options[0] = option;
            char* option1 = CPLStrdup("LAUNDER=FALSE");
            options[1] = option1;
            options[2] = NULL;
            OGRSpatialReference ogrReference;
            ogrReference.importFromEPSG(4326);
            //xxxxxxxxxxxxxxxxxxxxxxx
            if (_ogrDSForMemory!= NULL)
            {
                _inMemoryOGRLayer = _ogrDSForMemory->CopyLayer(orignalOGRLayer, "InMemoryOgrLayer", options);
                _inMemoryOGRLayer->SyncToDisk();
                _inMemoryOGRLayer->ResetReading();
            }
            CPLFree(option);
            CPLFree(option1);
            delete[] options;
            OGRDataSource::DestroyDataSource(ogrDSForOrignal);
        }

        if(_inMemoryOGRLayer)
        {
                _roleHash = QAbstractTableModel::roleNames();
                OGRFeatureDefn* featureDefn = _inMemoryOGRLayer->GetLayerDefn();
                int fieldCount = featureDefn->GetFieldCount();
                for (unsigned int j = 0; j < fieldCount; j++)
                {  
                    OGRFieldDefn* fieldDefn = featureDefn->GetFieldDefn(j);
                    const char* fieldNameChar = fieldDefn->GetNameRef();
                    std::string fieleNameStr = std::string(fieldNameChar);
                    _roleNamesVector.push_back(fieleNameStr);
                    _roleMap[fieleNameStr] = j;
                    if (fieldDefn)
                    {
                        _roleHash.insert(_roleMap.find(fieleNameStr)->second, QByteArray(fieldNameChar));
                    }

                }
                _inMemoryOGRLayer->ResetReading();
        }
    }

    QHash<int,QByteArray> VectorTableModel::roleNames() const
    {
        return _roleHash;
    }

    int 
    VectorTableModel::rowCount(const QModelIndex &parent) const
    {
        _inMemoryOGRLayer->ResetReading();
        if(parent.isValid())
        {
            return 0;
        }
        if(_inMemoryOGRLayer)
        {
            int featureCount = _inMemoryOGRLayer->GetFeatureCount();
            if(featureCount>0)
            {
                return (int)featureCount;
            }
        }
        return 0;
    }
    QString  VectorTableModel::getData(int row,int column)
    {
        QModelIndex index = createIndex(row, 0);
       
        int role = column;
        QVariant result= data(index, role);
        if (result == NULL)
        {
            return "";
        }
        else
        {
            return result.toString();
        }
       
    }
    OGRLayer* VectorTableModel::getOGRUpdateLayer()
    {
        return _inMemoryOGRLayer;
    }
    bool VectorTableModel::setData(int row,int column,QString value)
    {   
        int fid = 0;
        int role = column;
        _inMemoryOGRLayer->ResetReading();
        OGRFeature* feature = NULL;
        for (int i = 0; i < row; i++)
        {
            feature = _inMemoryOGRLayer->GetNextFeature();
            OGRFeature::DestroyFeature(feature);
        }
        feature = _inMemoryOGRLayer->GetNextFeature();
        if (feature == NULL)
        {
            return false;
        }
        fid = feature->GetFID();;
        QVariant result;

        // checking the role in already generated roles
        for (unsigned i = 0; i<_roleNamesVector.size(); ++i)
        {
            bool ok = false;
            if (role == _roleMap.find(_roleNamesVector.at(i))->second)
            {
                //OGRFieldDefn* defn = feature->GetFieldDefnRef(role);
                OGRFieldType type = feature->GetFieldDefnRef(role)->GetType();
                std::string nameOfField = feature->GetFieldDefnRef(role)->GetNameRef();
                switch (type)
                {
                case OFTInteger:
                    {
                        int val = value.toInt(&ok);
                        if (ok)
                        {
                            feature->SetField(i, value.toInt());
                            _inMemoryOGRLayer->SetFeature(feature);
                            _inMemoryOGRLayer->SyncToDisk();
                            OGRFeature::DestroyFeature(feature);
                            return true;
                        }
                    }                
                    break;
                case OFTReal:
                    {
                        double val = value.toDouble(&ok);
                        if (ok)
                        {
                            feature->SetField(i, value.toDouble());
                            _inMemoryOGRLayer->SetFeature(feature);
                            _inMemoryOGRLayer->SyncToDisk();
                            OGRFeature::DestroyFeature(feature);
                            return true;
                        }
                    }
                    break;
                case OFTString:
                    {
                        double val = value.toDouble(&ok);
                        if (!ok)
                        {
                            feature->SetField(nameOfField.c_str(), value.toStdString().c_str());
                            _inMemoryOGRLayer->SetFeature(feature);
                            _inMemoryOGRLayer->SyncToDisk();
                            OGRFeature::DestroyFeature(feature);
                            return true;
                        }
                    }
                    break;
                default:
                    break;
                }
            }
        }   
        OGRFeature::DestroyFeature(feature);
        return false;

    }
    int
    VectorTableModel::columnCount(const QModelIndex &parent) const
    {
        if(_inMemoryOGRLayer)
        {
            return  _inMemoryOGRLayer->GetLayerDefn()->GetFieldCount();
        }
        
        return 0;
    }
    QString VectorTableModel::getColumnType(QString value) const
    {
        int val=_roleMap.find(value.toStdString())->second;
        if(!_inMemoryOGRLayer)
         return "";

        OGRFieldDefn* defn = _inMemoryOGRLayer->GetLayerDefn()->GetFieldDefn(val);
        OGRFieldType type = defn->GetType();
        if (type == OFTString)
        {
            return "String";
        }
        else if (type == OFTInteger)
        {
             return "Int";
        }
        else if (type == OFTReal)
        {
            return "Double";
        }
        return "";
    }
    int VectorTableModel::getColumnNumber(QString value) const
    {
        int val=_roleMap.find(value.toStdString())->second;
        return val;
    }
    QVariant
    VectorTableModel::data(const QModelIndex &index, int role) const
    {
        int row = index.row();
        int column = index.column();
        _inMemoryOGRLayer->ResetReading();
        OGRFeature* feature=NULL;  
        for (int i = 0; i < row; i++)
        {
            feature = _inMemoryOGRLayer->GetNextFeature();
            OGRFeature::DestroyFeature(feature);
        }
        feature = _inMemoryOGRLayer->GetNextFeature();
        if (feature == NULL)
        {
            return QVariant();
        }

        QVariant result;

        // checking the role in already generated roles
        for (unsigned i = 0; i<_roleNamesVector.size(); ++i)
        {
            if (role == _roleMap.find(_roleNamesVector.at(i))->second)
            {
                OGRFieldType type = feature->GetFieldDefnRef(role)->GetType();
                switch (type)
                {
                case OFTInteger:
                    result.setValue<int>(feature->GetFieldAsInteger(role));
                    break;
                case OFTReal:
                    result.setValue<double>(feature->GetFieldAsDouble(role));
                    break;
                case OFTString:
                    result.setValue<QString>(feature->GetFieldAsString(role));
                    break;
                case OFTDateTime:
                {
                    int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0, tzflag = 0;
                    if (feature->GetFieldAsDateTime(role, &year, &month, &day, &hour, &min, &sec, &tzflag))
                    {
                        QDate qDate(year, month, day);
                        QTime qTime(hour, min, sec);

                        QDateTime qDateTime(qDate, qTime);
                        result.setValue<QDateTime>(qDateTime);
                    }
                }
                break;
                default:
                    break;
                }
            }
        }
        OGRFeature::DestroyFeature(feature);
        return result;
    }
    
    Qt::ItemFlags
    VectorTableModel::flags(const QModelIndex &index) const
    {
        return Qt::ItemIsEnabled | Qt::ItemIsEditable;
    }

    VectorTableModel::~VectorTableModel()
    {
    }
    void VectorTableModel::sort(int column, Qt::SortOrder order)
    {
        //not need
    }
    void VectorTableModel::closePopup()
    {
        OGRDataSource::DestroyDataSource(_ogrDSForMemory);
        _roleMap.clear();
        _roleNamesVector.clear();
    }
}
