﻿#include <SMCQt/ProfileObject.h>
#include <iostream>

namespace SMCQt{

    const std::string ProfileObject::ditchProfilePopup = "ditchProfilePopup"; 
    const std::string ProfileObject::ditchPopup = "ditchPopup";
    const std::string ProfileObject::profileEditorPopup = "profileEditorPopup";

    ProfileObject::ProfileObject(QQuickItem* parent) :
        QQuickPaintedItem(parent)

    {
        _line.clear();
        setAcceptedMouseButtons(Qt::AllButtons);
    }

    void ProfileObject::setObjectName(QString objName)
    {
        _objectName = objName;
        update();
    }

    void ProfileObject::showPointstext(QVector<QPointF> points, QPainter* painter)
    {
        if (painter)
        {
            for (int i = 0; i < points.size(); i++)
            {
                QPointF point = points.at(i);

                float pointX = point.x() / width();
                float pointY = point.y() / height();

                pointX = pointX * 20 - 10;
                pointY = -(pointY * 20 - 10);

                std::string pointXString = std::to_string(pointX); 
                std::string pointYString = std::to_string(pointY); 
                pointXString.resize(5); 
                pointYString.resize(5); 

                std::string text = "  " + pointXString  +',' + pointYString;
                if (point.y() > 0)
                {
                    painter->drawText(QPoint(points.at(i).x(), points.at(i).y() + 2), QString::fromStdString(text));
                }

                else
                {
                    painter->drawText(QPoint(points.at(i).x(), points.at(i).y() - 2), QString::fromStdString(text));
                }
            }
        }
    }

    void ProfileObject::paint(QPainter* painter)
    {
        painter->setBrush(Qt::white);
        painter->drawRect(contentsBoundingRect());
        QPen x;
        x.setColor(Qt::darkGreen);
        x.setWidth(2);
        painter->setPen(x);

        if (_objectName.toStdString() == ditchProfilePopup)
        {
            painter->drawLine(QPoint(width() / 2, 0), QPoint(width()/2, height()));
            painter->drawLine(QPoint(0, height() / 2), QPoint(width(), height() / 2));

            x.setColor(Qt::red);
            painter->setPen(x);

            painter->drawText(QPoint(5, height() - 10), "(-10,-10)");
            painter->drawText(QPoint(5, 10), "(-10,10)");
            painter->drawText(QPoint(width()/2, height()/2), "(0,0)");
            painter->drawText(QPoint(width() - 40, height() - 10), "(10,-10)");
            painter->drawText(QPoint(width() - 40, 10), "(10,10)");

                if (!_clear)
                {
                    if (_line.size() >= 1)
                    {
                        x.setColor(Qt::blue);
                        x.setWidth(2);
                        painter->setPen(x);
                        painter->drawPolyline(_line.data(), _line.size());
                   
                        x.setColor(Qt::red);
                        painter->setPen(x);
                        showPointstext(_line,painter);

                        x.setWidth(6);
                        painter->setPen(x);
                        painter->drawPoints(_line.data(), _line.size());

                        if (_selectedPointIndex >= 0)
                        {
                            x.setColor(Qt::green);
                            x.setWidth(8);
                            _selectedPoint = _line[_selectedPointIndex];
                            painter->setPen(x);
                            painter->drawPoints(&_selectedPoint, 1);
                        }
                    }
                }

                else
                {
                    _clear = false;
                    _line.clear();
                }

        }

        else if (_objectName.toStdString() == ditchPopup)
        {
            painter->drawLine(QPoint(0, height() / 2), QPoint(width(), height() / 2));
            painter->drawText(QPoint(width() / 2, height() / 2), "50");
            painter->drawText(QPoint(0, height() / 2), "0");
            painter->drawText(QPoint(width() - 15, height() / 2), "100");

            if (!_clear)
            {
                if (_pointProfileMap.size() != 0)
                {
                    for (std::map<std::pair<float, float>, QString >::iterator itr = _pointProfileMap.begin(); itr != _pointProfileMap.end(); itr++)
                    {
                        QString pofileObjectName = itr->second;

                        bool active = _nameProfileMap[pofileObjectName].active(); 

                        if (!active)
                            continue; 

                        QPointF currentPoint(itr->first.first, itr->first.second);

                        QString color = (itr->first == _selectedPointPair) && _editGraph ? "black" : _nameProfileMap[pofileObjectName].color();
                        QColor currentPointColor(color);

                        x.setColor(currentPointColor);

                        int width = (itr->first == _selectedPointPair) && _editGraph ? 9 : 7;
                        x.setWidth(width);
                        painter->setPen(x);
                        painter->drawPoints(&currentPoint, 1);
                    }
                }
            }

            else
            {
                _clear = false;
                _pointProfileMap.clear(); 
            }
        }
        else if (_objectName.toStdString() == profileEditorPopup)
        {

            painter->drawText(QPoint(width() / 2, height() / 2), "(0,0)");

            painter->drawLine(QPoint(width() / 2, 0), QPoint(width() / 2, height()));
            painter->drawLine(QPoint(0, height() / 2), QPoint(width(), height() / 2));

            if (!_clear)
            {
                if (_line.size() != 0)
                {
                    painter->setPen(x);
                    x.setColor(Qt::blue);
                    x.setWidth(2);
                    painter->setPen(x);
                    painter->drawPolyline(_line.data(), _line.size());

                    x.setColor(Qt::black);
                    x.setWidth(6);
                    painter->setPen(x);
                    painter->drawPoints(_line.data(), _line.size());

                    x.setColor(_nameProfileMap[_currentVizProfileObject].color());
                    x.setWidth(5);
                    painter->setPen(x);
                    painter->drawPoints(_line.data(), _line.size());

                    if (_selectedPointIndex >= 0)
                    {
                        x.setColor(Qt::green);
                        x.setWidth(7);
                        _selectedPoint = _line[_selectedPointIndex];
                        painter->setPen(x);
                        painter->drawPoints(&_selectedPoint, 1);
                    }
                }
            }

            else
            {
                _clear = false;
                _line.clear();
            }
        }

    }

    void ProfileObject::setLineData(QList<QVariant> lineData)
    {
        _selectedPointIndex = -1; 
        _line.clear(); 
        for (int i = 0; i < lineData.size(); i++)
        {
            _line.push_back(QPointF((lineData[i].toPointF().x() + 10)*width() / 20, (-lineData[i].toPointF().y() + 10)*height() / 20));
        }
        update();
    }

    void ProfileObject::mousePressEvent(QMouseEvent* event)
    {
        _simpleClickPressed = true; 
    }

    void ProfileObject::mouseReleaseEvent(QMouseEvent* event)
    {

        QPointF point = mapToItem(this, event->localPos());
       
        if (_objectName.toStdString() == ditchProfilePopup && !_editGraph && _startDrawing)
        {
                _line.push_back(point);
                update();
        }

        else if (_objectName.toStdString() == ditchPopup )
        {
            if (_editGraph)
            {
                if (_simpleClickPressed)
                {
                    for (std::map<std::pair<float, float>, QString>::iterator itr = _pointProfileMap.begin(); itr != _pointProfileMap.end(); itr++)
                    {
                        if ((itr->first.first < point.x() + 10) && (itr->first.first > point.x() - 10) && (itr->first.second < point.y() + 10) && (itr->first.second > point.y() - 10))
                        {
                            if (_selectedPointPair == itr->first)
                            {
                                _selectedPointPair = std::pair<float, float>(-1,-1);
                            }
                            else{
                                _selectedPointPair = itr->first;
                                std::string xString = std::to_string(itr->first.first / width());
                                xString.resize(6);
                                setXProp(xString.c_str());
                            }
                            break;
                        }
                    }
                }
            }
            else
            {
                point.setY(height() / 2);
                _pointProfileMap.insert(std::pair<std::pair<float, float>, QString>(std::pair<float, float>(point.x(), point.y()), _currentVizProfileObject));
            }
                update();
            
        }
        else if (_objectName.toStdString() == profileEditorPopup || _objectName.toStdString() == ditchProfilePopup)
        {
            if (_simpleClickPressed)
            {
                for (int i = 0; i < _line.size(); i++)
                {
                    if ((_line[i].x() < point.x() + 10) && (_line[i].x() > point.x() - 10) && (_line[i].y() < point.y() + 10) && (_line[i].y() > point.y() - 10))
                    {
                        if (_selectedPointIndex == i)
                        {
                            _selectedPointIndex = -1; 
                        }
                        else{
                            _selectedPointIndex = i;
                            setProfileXY(_line[i]);
                        }
                        update();
                        break;
                    }
                }
            }
        }
    }

    void ProfileObject::editGraph(bool set)
    {
        _editGraph = set; 
        _selectedPointPair = std::pair<float, float>(-1, -1); 
        _selectedPointIndex = -1; 
        update(); 
    }

    QMap<QString, QVariant> ProfileObject::getPointProfileMap()
    {
        QMap<QString, QVariant> pointProfileMap; 
            
        for (std::map<std::pair<float, float>, QString>::iterator itr = _pointProfileMap.begin(); itr != _pointProfileMap.end(); itr++)
        {
            float pointXDistance = itr->first.first * 100 / width();
                std::string pointXDistancestr(std::to_string(pointXDistance)); 
                
                    pointProfileMap.insert(QString::fromStdString(pointXDistancestr), QVariant::fromValue(itr->second));
        }
        
        return pointProfileMap; 
    }

    void ProfileObject::setVisible(QString name, QString color, bool checked)
    {
        VizProfileObject object(name, color, checked);

        std::string objectName = object.name().toStdString();

        if (_nameProfileMap.find(object.name()) == _nameProfileMap.end())
        {
            _nameProfileMap.insert(std::pair<QString, VizProfileObject>(object.name(), object));
        }
        else
        {
            _nameProfileMap[name].setActive(checked);
        }
        update();
    }

    void ProfileObject::setVizProfileObject(QString name, QString color, bool active)
    {
        VizProfileObject object(name, color, active); 

        std::string objectName = object.name().toStdString(); 

        if (_nameProfileMap.find(object.name()) == _nameProfileMap.end())
        {
           _nameProfileMap.insert(std::pair<QString, VizProfileObject>(object.name(), object));
        }

        _currentVizProfileObject = object.name();
    }

    void ProfileObject::mouseMoveEvent(QMouseEvent *event)
    {
        _simpleClickPressed = false; 

        QPointF point = mapToItem(this, event->localPos());


        if (_objectName.toStdString() == ditchPopup)
        {
            if (_selectedPointPair != std::pair<float, float>(-1, -1))
            {
                point.setY(height() / 2);


                if (point.x() > width())
                    point.setX(width());

                if (point.x() < 0)
                    point.setX(0);


                std::pair<float, float> newPoint(point.x(), point.y());
                
                if (_selectedPointPair == newPoint)
                    return; 

                std::map<std::pair<float, float>, QString>::iterator itr = _pointProfileMap.find(_selectedPointPair);
                if (itr != _pointProfileMap.end()) {

                    _pointProfileMap.insert(std::pair<std::pair<float, float>, QString>(std::pair<float, float>(point.x(), point.y()), itr->second));
                    _pointProfileMap.erase(itr);

                    _selectedPointPair = newPoint;
                    std::string xString = std::to_string(point.x() / width());
                    xString.resize(6);
                    setXProp(xString.c_str());
                    update();
                }
            
            }
        }
        else
        {
            if (_selectedPointIndex >= 0)
            {
                if (point.x() > width())
                    point.setX(width());

                if (point.x() < 0)
                    point.setX(0); 

                if (point.y() > height())
                    point.setY(height()); 

                if (point.y() < 0)
                    point.setY(0); 

                _line[_selectedPointIndex] = point;

                setProfileXY(point);
                update();
            }
        }
    }

    void ProfileObject::clearGraph()
    {
        _clear = true;
        update();
    }

    void ProfileObject::startDrawing(bool start)
    {
        _startDrawing = start;
    }

    QList<QVariant> ProfileObject::getLineData()
    {
        QList<QVariant> pointsData;

        if (_line.size() >= 2)
        {
           
            for (int i = 0; i < _line.size(); i++)
            {
                QPointF point = _line.at(i);

                float pointX = point.x() / width();
                float pointY = point.y() / height();

                pointX = pointX * 20 - 10;
                pointY = -(pointY * 20 - 10);

                point.setX(pointX);
                point.setY(pointY);

                pointsData.push_back(point);
            }

            return pointsData;
        }

        return {-1};
    }

    QList<QVariant> ProfileObject::getCurrentPositionAlongLine()
    {
        if (_line.size() >= 1)
        {
            QList<QVariant> pointsData;
            for (int i = 0; i < _line.size(); i++)
            {
                QPointF point = _line.at(i);
                pointsData.push_back(point.x()*100/width());
            }
            
            return pointsData;
        }

        return{-1};

    }

    void ProfileObject::deleteTemplate(QString templateName)
    {

        std::map<std::pair<float, float>, QString>::iterator itr = _pointProfileMap.begin();
        while (1)
        {
            for (itr = _pointProfileMap.begin(); itr != _pointProfileMap.end(); itr++)
            {
                if (itr->second == templateName)
                {
                    break; 
                }
            }

            if (itr != _pointProfileMap.end())
                _pointProfileMap.erase(itr);
            else
                break; 
        }

        for (std::map<QString, VizProfileObject>::iterator itr = _nameProfileMap.begin(); itr != _nameProfileMap.end(); itr++)
        {
            if (itr->first == templateName)
            {
                _nameProfileMap.erase(itr);
                break; 
            }
        }

        update(); 
    }

    void ProfileObject::setGraphX(QString x)
    {

        if (_objectName.toStdString() == ditchPopup && _editGraph)
        {
            if (_selectedPointPair != std::pair<float, float>(-1, -1))
            {
                QPointF point;
                point.setY(height() / 2);
                if (x.isEmpty())
                    return; 

                std::string xString = x.toStdString(); 
                char* xp;
                double xDouble = strtod(xString.c_str(), &xp);
                if (*xp) {
                    return;
                }

                float xfloat = xDouble;

                if (xfloat > 1)
                {
                    xfloat = 1; 
                }
                if (xfloat < 0)
                {
                    xfloat = 0;
                }
                std::string xfloatString = std::to_string(xfloat);
                xfloatString.resize(6);
                setXProp(xfloatString.c_str());
                point.setX(xfloat * width());

                std::pair<float, float> newPoint(point.x(), point.y());

                if (_selectedPointPair == newPoint)
                    return;

                std::map<std::pair<float, float>, QString>::iterator itr = _pointProfileMap.find(_selectedPointPair);
                if (itr != _pointProfileMap.end()) {

                    _pointProfileMap.insert(std::pair<std::pair<float, float>, QString>(std::pair<float, float>(point.x(), point.y()), itr->second));
                    _pointProfileMap.erase(itr);

                    _selectedPointPair = newPoint;
                    update();
                }

            }
        }
    }

    void ProfileObject::setProfileXY(QString x, QString y)
    {
        if (_selectedPointIndex >= 0)
        {
            if (x.isEmpty() || y.isEmpty())
                return; 

            
            std::string xString = x.toStdString(); 
            std::string yString = y.toStdString(); 

            char* xp;
            double xDouble = strtod(xString.c_str(), &xp);
            if (*xp) {
                return; 
            }

            char* yp; 
            double yDouble = strtod(yString.c_str(), &yp);
            if (*yp){
                return; 
            }

            QPointF point(xDouble, yDouble);
            point.setX((point.x() + 10)*width() / 20);
            point.setY((-point.y() + 10)*height() / 20);

            if (point.x() > width())
            {
                point.setX(width());
            }

            if (point.x() < 0)
            {
                point.setX(0);
            }

            if (point.y() > height())
            {
                point.setY(height());
            }

            if (point.y() < 0)
            {
                point.setY(0);
            }

            _line[_selectedPointIndex] = point;

            setProfileXY(point); 
            update();
        }
    }

    void ProfileObject::setProfileXY(QPointF point)
    {
        float pointX = point.x() / width();
        float pointY = point.y() / height();

        point.setX(pointX * 20 - 10);
        point.setY(-(pointY * 20 - 10));

        std::string xString = std::to_string(point.x());
        std::string yString = std::to_string(point.y());
        xString.resize(6); 
        yString.resize(6); 
        setXProp(xString.c_str());
        setYProp(yString.c_str());
    }

    QString ProfileObject::xProp() const
    {
        return _x; 
    }

    void ProfileObject::setXProp(const QString& x)
    {
        if (x != _x)
        {
            _x = x;
            emit xPropChanged();
        }
    }

    QString ProfileObject::yProp() const
    {
        return _y; 
    }

    void ProfileObject::setYProp(const QString& y)
    {
        if (y != _y)
        {
            _y = y;
            emit yPropChanged();
        }
    }
}

    