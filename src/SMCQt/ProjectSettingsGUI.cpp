/****************************************************************************
*
* File             : ProjectSettingsGUI.h
* Description      : ProjectSettingsGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/ProjectSettingsGUI.h>

#include <App/AccessElementUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>

#include <Elements/ElementsUtils.h>
#include <Elements/IRepresentation.h>
#include <Elements/IProjectSettingsComponent.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ProjectSettingsGUI);
    DEFINE_IREFERENCED(ProjectSettingsGUI, DeclarativeFileGUI);

    ProjectSettingsGUI::ProjectSettingsGUI()
    {

    }

    ProjectSettingsGUI::~ProjectSettingsGUI()
    {

    }

    void
        ProjectSettingsGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if (smpLeftMenu != NULL)
        {
            QObject::connect(smpLeftMenu, SIGNAL(connectProjectSettingsMenu()), this,
                SLOT(connectProjectSettingsMenu()), Qt::UniqueConnection);
        }
    }

    void ProjectSettingsGUI::_setCurrentMode()
    {
        _projectSettingsMenu->setProperty("militarySymbolMode", _currentMode);
    }

    void ProjectSettingsGUI::connectProjectSettingsMenu()
    {
        _projectSettingsMenu = _findChild("projectSettingsMenu");
        if (_projectSettingsMenu)
        {
            QObject::connect(_projectSettingsMenu, SIGNAL(setMilitaryMode(int)), this,
                SLOT(militarySymbolModeChanged(int)), Qt::UniqueConnection);
            _currentMode = _projectSettingComponent->getMilitarySymbolRepresentationMode();
            _setCurrentMode();

            QObject::connect(_projectSettingsMenu, SIGNAL(setMilSymbolSize(QString)), this,
                SLOT(militarySymbolSizeChanged(QString)), Qt::UniqueConnection);
            //std::cout << _projectSettingComponent->getMilSymbolSize();
            _projectSettingsMenu->setProperty("symbolSize", QString::fromStdString(std::to_string((int)_projectSettingComponent->getMilSymbolSize())));
        }
    }
    void ProjectSettingsGUI::militarySymbolSizeChanged(QString size)
    {
        bool ok = true;
        _symbolSize = size.toDouble(&ok);
        if (ok)
            _projectSettingComponent->setMilSymbolSize(_symbolSize);
        else
            _projectSettingComponent->setMilSymbolSize(32.0f);
    }

    void ProjectSettingsGUI::militarySymbolModeChanged(int mode)
    {
        _currentMode = mode;
        _projectSettingComponent->setMilitarySymbolRepresentationMode((ELEMENTS::IRepresentation::RepresentationMode)(mode));
    }

    void ProjectSettingsGUI::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("ProjectSettingsComponent");
                if (component.valid())
                {
                    _projectSettingComponent = component->getInterface<ELEMENTS::IProjectSettingsComponent>();
                    _currentMode = _projectSettingComponent->getMilitarySymbolRepresentationMode();
                }
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }

    }
}

