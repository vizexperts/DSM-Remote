/****************************************************************************
*
* File             : DatabaseSettingsGUI.h
* Description      : DatabaseSettingsGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/DatabaseSettingsGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/AccessElementUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>

#include <Elements/ElementsUtils.h>

#include <Util/FileUtils.h>

#include <QFileDialog>
#include <QSettings>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>

#include <Util/FileUtils.h>
#include <QJSONDocument>
#include <QJSONObject>


namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, DatabaseSettingsGUI);
    DEFINE_IREFERENCED(DatabaseSettingsGUI, DeclarativeFileGUI);

    const std::string DatabaseSettingsGUI::DatabaseSettingsMenu = "databaseSettingsMenu";


   DatabaseSettingsGUI::DatabaseSettingsGUI() : _dbUIHandler(NULL)
    {
    }

   DatabaseSettingsGUI::~DatabaseSettingsGUI()
    {
    }

    void DatabaseSettingsGUI::setActive(bool value)
    {
        if (getActive() == value)
            return;

        if (value)
        {
            QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);
            if (DatabaseSettings)
            {
             
                _populateDatabases();

                QObject::connect(DatabaseSettings, SIGNAL(saveDatabaseParameters()), this,
                    SLOT(saveButtonClicked()), Qt::UniqueConnection);

                QObject::connect(DatabaseSettings, SIGNAL(deleteDatabase(QString)), this,
                    SLOT(deleteDatabase(QString)), Qt::UniqueConnection);

                QObject::connect(DatabaseSettings, SIGNAL(comboBoxClicked(QString)), this,
                    SLOT(comboBoxClicked(QString)), Qt::UniqueConnection);

                QObject::connect(DatabaseSettings, SIGNAL(testDatabaseConnection()), this,
                    SLOT(testDatabaseConnection()), Qt::UniqueConnection);

            }
        }
        else
        {
            _databaseInstanceList.clear();
            _dbUIHandler = NULL;
            _dbOptions.clear();

            QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);
            if (DatabaseSettings)
            {
                DatabaseSettings->setProperty("testResultVisible", false);
            }

        }

        DeclarativeFileGUI::setActive(value);
    }

    void DatabaseSettingsGUI::_populateDatabases()
    {
        _readAllDatabaseInstanceName();

        int count = 2;
        QList<QObject*> comboBoxList;

        comboBoxList.append(new VizComboBoxElement("Select Database Instance", "0"));
        comboBoxList.append(new VizComboBoxElement("Create New Database Instance", "1"));

        for (int i = 0; i < _databaseInstanceList.size(); i++)
        {
            std::string databaseName = _databaseInstanceList[i];
            comboBoxList.append(new VizComboBoxElement(databaseName.c_str(), UTIL::ToString(count).c_str()));
        }

        _setContextProperty("databaseList", QVariant::fromValue(comboBoxList));
    }

    void DatabaseSettingsGUI::_readAllDatabaseInstanceName()
    {
        _databaseInstanceList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string databaseFolder = appdataPath + "/Database";

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _databaseInstanceList.push_back(fileNameWithoutExtension);
        }
    }

   DatabaseSettingsGUI::DatabaseParameters& DatabaseSettingsGUI::_getDatabase(std::string database)
    {
  
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string databaseFolder = appdataPath + "/Database";

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting", "database folder not found", true);
            //making database to be invalid that is handled in parent function
            _dbFile.userName = "";
            _dbFile.passWord = " ";
            return _dbFile;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);
        std::string databaseExtension;
        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == database)
            {
                found = true;
                databaseExtension = osgDB::getLowerCaseFileExtension(currFileName);
            }
        }

        if (!found)
        {
            //emit showError("Read setting", "Database with this name not found", true);
            //making database to be invalid that is handled in parent function
            _dbFile.userName = "";
            _dbFile.passWord = "";
            return _dbFile;
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + "/Database/" + database + "." + databaseExtension));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                emit showError("Read setting", "Can not open file for reading", true);
                //making database to be invalid that is handled in parent function
                _dbFile.userName = "";
                return _dbFile;
            }

            QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
            if (jdoc.isNull())
            {
                _dbFile.userName = "";
                return _dbFile;
            }

            QJsonObject obj = jdoc.object();
 
            _dbFile.dbType = obj["Driver"].toString().toStdString();
            _dbFile.dbName = obj["DatabaseName"].toString().toStdString();
            _dbFile.userName = obj["Username"].toString().toStdString();
            _dbFile.passWord = obj["Password"].toString().toStdString();
            _dbFile.host = obj["Host"].toString().toStdString();
            _dbFile.port = obj["Port"].toString().toStdString();
           
            return _dbFile;
        }
    }

   void DatabaseSettingsGUI::testDatabaseConnection()
    {
        QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);

        if (DatabaseSettings)
        {
            QString databaseType = DatabaseSettings->property("databaseTypeValue").toString();
            QString databaseName = DatabaseSettings->property("databaseName").toString();
            QString databaseUsername = DatabaseSettings->property("userName").toString();
            QString databasePassword = DatabaseSettings->property("password").toString();
            QString databaseHost = DatabaseSettings->property("host").toString();
            QString databasePort = DatabaseSettings->property("port").toString();

            if (databaseType.toStdString() == "postgres")
            {

                if (databaseUsername == "" || databasePassword == "" || databaseHost == "" || databasePort == "")
                {
                    emit  showError("Cannot Test", "Please give the required parameters for testing");
                    return;
                }

                _dbOptions.clear();
                _dbOptions.insert(std::pair<std::string, std::string>("Database", databaseType.toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Host", databaseHost.toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Password", databasePassword.toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Port", databasePort.toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Username", databaseUsername.toStdString()));
            }

            else if (databaseType.toStdString() == "odbc")
            {
                if (databaseUsername == "" || databasePassword == "" || databaseName == "" )
                {
                    emit  showError("Cannot Test", "Please give the required parameters for testing");
                    return;
                }
                _dbOptions.clear();
                _dbOptions.insert(std::pair<std::string, std::string>("Driver", databaseType.toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Database", databaseName.toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Username", databaseUsername.toStdString()));
                _dbOptions.insert(std::pair<std::string, std::string>("Password", databasePassword.toStdString()));
               
            }

            if (_connectWithDatabase())
            {
                DatabaseSettings->setProperty("status", QString::fromStdString("Database Connection Successful"));
                DatabaseSettings->setProperty("testResultVisible", true);
            }

            else
            {
                DatabaseSettings->setProperty("status", QString::fromStdString("Database Connection Not Successful"));
                DatabaseSettings->setProperty("testResultVisible", true);
            }
        }
    }


   bool DatabaseSettingsGUI::_connectWithDatabase()
   {
       _dbUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizDataUI::IDatabaseUIHandler>(getGUIManager());
       QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);
       QString databaseType;

       if (DatabaseSettings)
       {
           databaseType = DatabaseSettings->property("databaseTypeValue").toString();
       }

       //for postgres list should be >= 5
       if (databaseType.toStdString() == "postgres")
       {
           if (_dbOptions.size() < 5)
               return false;
       }

       //for odbc list should be >= 4
       else if (databaseType.toStdString() == "odbc")
       {
           if (_dbOptions.size() < 4)
               return false;
       }

       _dbUIHandler->initialize(_dbOptions);

       std::string databaseStatus;
       if (_dbUIHandler->testConnection(_dbOptions))
       {
           databaseStatus = "Connected";
           _dbOptions.insert(std::pair<std::string, std::string>("Status", databaseStatus));
           _dbUIHandler->initialize(_dbOptions);
           return true;
       }
       else
       {
           databaseStatus = "Not Connected";
           _dbOptions.insert(std::pair<std::string, std::string>("Status", databaseStatus));
           _dbUIHandler->initialize(_dbOptions);
           return false;
       }

       return false;
   }

    void DatabaseSettingsGUI::saveButtonClicked()
    {
        QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);
        if (DatabaseSettings)
        {
            QString connectionName = DatabaseSettings->property("connectionName").toString();
            QString databaseName = DatabaseSettings->property("databaseName").toString();
            QString databaseType = DatabaseSettings->property("databaseTypeValue").toString();
            QString databaseUsername = DatabaseSettings->property("userName").toString();
            QString databasePassword = DatabaseSettings->property("password").toString();
            QString databaseHost = DatabaseSettings->property("host").toString();
            QString databasePort = DatabaseSettings->property("port").toString();

            std::vector<QString> values;

            values.push_back(databaseType);
            values.push_back(databaseName);
            values.push_back(databaseUsername);
            values.push_back(databasePassword);
            values.push_back(databaseHost);
            values.push_back(databasePort);

            if (databaseType.toStdString() == "postgres")
            {
                if (connectionName == "" || databaseUsername == "" || databasePassword == "" || databaseHost == "" || databasePort == "")
                {
                    emit  showError("Cannot Save", "Please give the required parameters for saving");
                    return;
                }
            }

            else if (databaseType.toStdString() == "odbc")
            {
                if (connectionName == "" || databaseUsername == "" || databasePassword == "" || databaseName == "")
                {
                    emit  showError("Cannot Save", "Please give the required parameters for saving");
                    return;
                }
            }

            DatabaseParameters database = _getDatabase(connectionName.toStdString());
            if (database.userName != "" && database.passWord != "")
            {
                emit showError("Change Connection name", "Database with this name already exists", true);
                return;
            }


            _writeDatabaseFile(connectionName, values);
            _readAllDatabaseInstanceName();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select Database Instance", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New Database Instance", "1"));

            for (int i = 0; i < _databaseInstanceList.size(); i++)
            {
                std::string databaseName = _databaseInstanceList[i];
                comboBoxList.append(new VizComboBoxElement(databaseName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("databaseList", QVariant::fromValue(comboBoxList));

            if (_databaseInstanceList.size() > 0)
            {
                DatabaseSettings->setProperty("value", connectionName);
            }

        }
    }


    void DatabaseSettingsGUI::deleteDatabase(QString value)
    {
        
        QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);

        if (value.toStdString() == "Select Database Instance" || value.toStdString() == "Create New Database Instance")
        {
            emit  showError("Not a valid input", "Cannot delete database with this name");
            return;
        }

        emit question("Delete Database  ", "Do you really want to delete?", false,
            "_okSlotForDeleting()", "_cancelSlotForDeleting()");

    }

    void DatabaseSettingsGUI::_okSlotForDeleting()
    {
        QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string databaseFolder = appdataPath + "/Database";

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        if (DatabaseSettings)
        {
            QString lpName = DatabaseSettings->property("value").toString();
            QString type = DatabaseSettings->property("databaseTypeValue").toString();

            std::string extension;
            if (type.toStdString() == "postgres")
            {
                extension = ".pgres";
            }

            else if (type.toStdString() == "odbc")
            {
                extension = ".odbc";
            }

            if (lpName == "")
            {
                return;
            }

            std::string filename = lpName.toStdString() + extension;
            std::string filePath = databaseFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, databaseFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
                //  std::cout << "Deleted database File from " << databaseFolder << std::endl;
            }

            _readAllDatabaseInstanceName();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select Database Instance", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New Database Instance", "1"));

            for (int i = 0; i < _databaseInstanceList.size(); i++)
            {
                std::string databaseName = _databaseInstanceList[i];
                comboBoxList.append(new VizComboBoxElement(databaseName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("databaseList", QVariant::fromValue(comboBoxList));

            std::string defaultName = "Select Database Instance";
            DatabaseSettings->setProperty("value", QString::fromStdString(defaultName));
        }

    }

    void DatabaseSettingsGUI::_cancelSlotForDeleting()
    {
        return;
    }

    void DatabaseSettingsGUI::_writeDatabaseFile(QString name, std::vector<QString>& values)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::stringstream insertStatement;

        std::string databaseFolder = appdataPath + "/Database";

        std::string path = osgDB::convertFileNameToNativeStyle(databaseFolder);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }

        std::string fileName;
        if (values[0].toStdString() == "postgres")
        {
            fileName = "/" + name.toStdString() + ".pgres";
        }

        else if (values[0].toStdString() == "odbc")
        {
            fileName = "/" + name.toStdString() + ".odbc";
        }
        

        QFile settingFile(QString::fromStdString(databaseFolder + fileName));

        if (!settingFile.open(QIODevice::WriteOnly))
        {
            emit showError("Save setting", "Could not open database file for save");
            return;
        }

        QJsonObject settingObject;
        settingObject["Driver"] = values[0];
        settingObject["DatabaseName"] = values[1];
        settingObject["Username"] = values[2];
        settingObject["Password"] = values[3];
        settingObject["Host"] = values[4];
        settingObject["Port"] = values[5];
        
        QJsonDocument settingDoc(settingObject);
        settingFile.write(settingDoc.toJson());
    }


    void DatabaseSettingsGUI::comboBoxClicked(QString value)
    {
        if (value.toStdString() == "Select Database Instance"  )
        {
            return;
        }

        else if (value.toStdString() == "Create New Database Instance")
        {
            QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);
            if (DatabaseSettings)
            {
                DatabaseSettings->setProperty("connectionName", "");
                DatabaseSettings->setProperty("databaseName", "");
                DatabaseSettings->setProperty("databaseTypeValue", QString::fromStdString("postgres"));
                DatabaseSettings->setProperty("userName", "");
                DatabaseSettings->setProperty("password", "");
                DatabaseSettings->setProperty("host", "");
                DatabaseSettings->setProperty("port", "");
            }
        }

        else
        {
            DatabaseParameters database = _getDatabase(value.toStdString());
            
            if (database.userName == "" && database.passWord != " ")
            {
                emit showError("Database invalid", "Database file not found", true);
                return;
            }

            QObject* DatabaseSettings = _findChild(DatabaseSettingsMenu);
            if (DatabaseSettings)
            {
                DatabaseSettings->setProperty("connectionName", value);
                DatabaseSettings->setProperty("databaseName", QString::fromStdString(database.dbName));
                DatabaseSettings->setProperty("databaseTypeValue", QString::fromStdString(database.dbType));
                DatabaseSettings->setProperty("userName", QString::fromStdString(database.userName));
                DatabaseSettings->setProperty("password", QString::fromStdString(database.passWord));
                DatabaseSettings->setProperty("host", QString::fromStdString(database.host));
                DatabaseSettings->setProperty("port", QString::fromStdString(database.port));
                
            }

        }
    }

    void DatabaseSettingsGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

} // namespace SMCQt