/****************************************************************************
*
* File             : CreateOpacitySliderGUI.h
* Description      : CreateOpacitySliderGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/CreateOpacitySliderGUI.h>
#include <SMCQt/QMLTreeModel.h>

#include <App/AccessElementUtils.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransactionFactory.h>
#include <App/IUndoTransaction.h>
#include <App/IUIHandler.h>
#include <App/AccessElementUtils.h>

#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/WorldMaintainer.h>
#include <Core/InterfaceUtils.h>
#include <Core/IDeletable.h>
#include <Core/IText.h>
#include <Core/WorldMaintainer.h>
#include <Core/ITemporary.h>
#include <Core/IFeatureObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IPoint.h>
#include <Core/IText.h>
#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/IClamper.h>
#include <Elements/IOverlayImage.h> 

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>

#include <iostream>

#include <osgEarth/ImageLayer>
#include <osgEarthAnnotation/ImageOverlay>

#include <Terrain/IRasterObject.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateOpacitySliderGUI);
    DEFINE_IREFERENCED(CreateOpacitySliderGUI, SMCQt::DeclarativeFileGUI);

    CreateOpacitySliderGUI::CreateOpacitySliderGUI()
    {

    }

    CreateOpacitySliderGUI::~CreateOpacitySliderGUI()
    {

    }

    void CreateOpacitySliderGUI::_loadAndSubscribeSlots()
    {
        //DeclarativeFileGUI::_loadAndSubscribeSlots();
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }
    }
    void
        CreateOpacitySliderGUI::setActive(bool value)
    {
        if (value == getActive())
        {
            return;
        }
        if (value)
        {
            QObject* opacitySliderObject = _findChild("opacitySliderObject");
            if (opacitySliderObject)
            {
                QObject::connect(opacitySliderObject, SIGNAL(changeSliderValue(double)),
                    this, SLOT(changeSliderValueHandler(double)), Qt::UniqueConnection);
                QObject::connect(opacitySliderObject, SIGNAL(okButtonChangeSliderValue(double)),
                    this, SLOT(okButtonChangeSliderValueHandler(double)), Qt::UniqueConnection);

                

                // set the opacity value 
                // this should be done to maintain the previous state value
                 _currentOpacityValue = _getCurrentOpacity();
                 //normalize 
                 opacitySliderObject->setProperty("sliderValue", _currentOpacityValue*100);
            }
        }
        else
        {
            _changeOpacity(_currentOpacityValue);
            _currentOpacityValue = 0.0;;
        }
        DeclarativeFileGUI::setActive(value);
    }
    void CreateOpacitySliderGUI::connectSmpMenu(QString type)
    {

    }

    // slot to accept the change in the opacity slider
    void CreateOpacitySliderGUI::changeSliderValueHandler(double value)
    {
        _changeOpacity(value);

    }
    void CreateOpacitySliderGUI::okButtonChangeSliderValueHandler(double value)
    {
        _currentOpacityValue = value;
        QObject* opacitySliderObject = _findChild("opacitySliderObject");
        if (opacitySliderObject)
        {
            QMetaObject::invokeMethod(opacitySliderObject, "closePopup");
        }

    }

    void CreateOpacitySliderGUI::_changeOpacity(const double& value)
    {
        if(!_selectionComponent.valid())
            return;

        const CORE::ISelectionComponent::SelectionMap& selectionMap = 
            _selectionComponent->getCurrentSelection();

        if(selectionMap.empty())
        {
            return;
        }

        CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
        CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

        if(!selectedObject)
            return;

        TERRAIN::IRasterObject* raster = selectedObject->getInterface<TERRAIN::IRasterObject>();
        if(raster)
        {
            raster->setOpacity(value);
            return;
        }

        ELEMENTS::IOverlayImage* overlay = selectedObject->getInterface<ELEMENTS::IOverlayImage>();
        if(overlay)
        {
            osg::ref_ptr<osgEarth::Annotation::ImageOverlay> overlayImage = overlay->getOverlayImage();
            if(overlayImage.valid())
            {
                overlayImage->setAlpha(value);
            }
            return;
        }

    }

    void CreateOpacitySliderGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());

                const CORE::ComponentMap& cmap = wmain->getComponentMap();
                _selectionComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }


    double CreateOpacitySliderGUI::_getCurrentOpacity()
    {   
        if(!_selectionComponent.valid())
            return 1.0;

        const CORE::ISelectionComponent::SelectionMap& selectionMap = 
            _selectionComponent->getCurrentSelection();

        if(selectionMap.empty())
        {
            return 1.0 ;
        }

        CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
        CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

        if(!selectedObject)
            return 1.0;

        TERRAIN::IRasterObject* raster = selectedObject->getInterface<TERRAIN::IRasterObject>();
        if(raster)
        {
            CORE::IRasterData *rasterData = raster->getRasterData();
            if(rasterData)
            {
                osg::ref_ptr<osgEarth::ImageLayer> layer = rasterData->getOEImageMapLayer();
                if(layer.valid())
                {
                    return layer->getOpacity();
                }
            }
        }

        ELEMENTS::IOverlayImage * overlay = selectedObject->getInterface<ELEMENTS::IOverlayImage>();
        if(overlay)
        {
            osg::ref_ptr<osgEarth::Annotation::ImageOverlay> overlayImage = overlay->getOverlayImage();
            if(overlayImage.valid())
                return overlayImage->getAlpha();
        }

        return 1.0;
    }

} // namespace SMCQt