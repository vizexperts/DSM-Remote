#include <App/AccessElementUtils.h>

#include <VizQt/QtUtils.h>
#include <VizQt/NameValidator.h>

#include <SMCQt/FeatureExportGUI.h>

#include <VizUI/ISelectionUIHandler.h>

#include <App/AccessElementUtils.h>

#include <QLineEdit>
#include <QPushButton>
#include <QDialog>
#include <QFileDialog>

#include <iostream>

#include <osgDB/FileNameUtils>
#include <SMCQt/VizComboBoxElement.h>
#include <Util/FileUtils.h>
#include <QJSONDocument>
#include <QJSONObject>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, FeatureExportGUI);
    const std::string FeatureExportGUI::ExportWindow = "ExportWindow";
    const std::string FeatureExportGUI::ExportGeoWindow = "ExportGeoWindow";
    const std::string FeatureExportGUI::GeorbISServerJsonLocation = "/GeorbISServerList/";
    FeatureExportGUI::FeatureExportGUI()
    {
    }

    FeatureExportGUI::~FeatureExportGUI()
    {
        setActive(false);
    }

    void
        FeatureExportGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if (popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void FeatureExportGUI::connectPopupLoader(QString type)
    {
        if (type == "ExportWindow")
        {
            setActive(true);
            _exportObject = _findChild(ExportWindow);
            if (_exportObject)
            {
                // start and browse button handlers
                setActive(true);
                connect(_exportObject, SIGNAL(browseButtonClicked()), this, SLOT(_handleBrowseButtonClicked()), Qt::UniqueConnection);
                connect(_exportObject, SIGNAL(exportButtonClicked()), this, SLOT(_handleExportButtonClicked()), Qt::UniqueConnection);
            }
        }
        if (type == "ExportGeoWindow") {
            setActive(true);
            _exportObject = _findChild(ExportGeoWindow);
            connect(_exportObject, SIGNAL(exportGeoButtonClicked()), this, SLOT(_handleExportGeoButtonClicked()), Qt::UniqueConnection);
            connect(_exportObject, SIGNAL(publishingServerChange(QString)), this, SLOT(_readGeorbISServerUrl(QString)), Qt::UniqueConnection);
            _serverIp = "";
            _fillGeorbISServerList();
            CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
            const CORE::ISelectionComponent::SelectionMap& map = selectionUIHandler->getCurrentSelection();
            CORE::ISelectionComponent::SelectionMap::const_iterator iter = map.begin();
            if (iter != map.end()) {
                CORE::IBase* obj = iter->second->getInterface<CORE::IBase>();
                _exportObject->setProperty("initialLayerName", obj->getName().c_str());
            }
        }
    }
    void FeatureExportGUI::_readGeorbISServerUrl(QString georbISSeverName)
    {
        _serverIp = "";
        if (georbISSeverName == "Choose publishing server")
        {
            return;
        }
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string georbISServerFolder = appdataPath + GeorbISServerJsonLocation;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting error", "GeorbIS server folder not found in Application", true);
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == georbISSeverName.toStdString())
            {
                found = true;
            }
        }

        if (!found)
        {
            emit showError("Read setting error", "GeorbIS server with this name not found in Application", true);
            return;
        }

        else
        {
            QFile settingFile(QString::fromStdString(georbISServerFolder + georbISSeverName.toStdString() + ".server"));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                emit showError("Read setting error", "Can not open file for reading", true);
                return;
            }

            QByteArray settingData = settingFile.readAll();
            QString setting(settingData);
            QJsonDocument jsonResponse = QJsonDocument::fromJson(setting.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            if (!jsonObject["Host"].toString().isEmpty())
            {
                std::string serverUrl = jsonObject["Host"].toString().toStdString();
                int startPort = serverUrl.find_last_of(":");
                _serverIp = serverUrl.substr(0, startPort);
                std::string urlWithoutIP = serverUrl.substr(startPort + 1);
                std::string serverPortNum = urlWithoutIP.substr(0, urlWithoutIP.find_first_of("/"));
                _portNum = serverPortNum;

                _featureExportUIHandler->setUploadingServerPort(serverPortNum);
                //check connectivity and give messagge
                if (!_featureExportUIHandler->georbISServerCapability(serverUrl))
                {
                    emit showError("Server Connection Error", "GeorbIS Server Not Connected", true);
                }
            }
        }
    }

    void
        FeatureExportGUI::_fillGeorbISServerList()
    {
        ////Fill Server List
        QList<QObject*> comboBoxList;
        int count = 1;
        comboBoxList.append(new VizComboBoxElement("Choose publishing server", "0"));

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string georbISServerFolder = appdataPath + GeorbISServerJsonLocation;

        std::string path = osgDB::convertFileNameToNativeStyle(georbISServerFolder);
        if (osgDB::fileExists(path))
        {
            osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

            for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
            {
                std::string currFileName = contents[currFileIndex];
                if ((currFileName == "..") || (currFileName == "."))
                    continue;

                std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
                comboBoxList.append(new VizComboBoxElement(fileNameWithoutExtension.c_str(), UTIL::ToString(count).c_str()));
                count++;
            }
        }
        _setContextProperty("publishingServerList", QVariant::fromValue(comboBoxList));
    }
    void
        FeatureExportGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                _loadFeatureExportUIHandler();
                if (_featureExportUIHandler.valid()){
                    /* read Setting File to decide if the fbx2_gltf_convert flag is true or not,
                    if true then convert the ive->fbx->gltf and then zip together ive and gltf and export the zip,
                    else there will be no conversion, and the ive is zipped and exported, there will be no  gltf.
                    */

                    std::string settingFileName = "../../config/SettingsFile.json";
                    QFile qFile(QString::fromStdString(settingFileName));
                    if (!qFile.open(QIODevice::ReadOnly)){
                        _featureExportUIHandler->setConvertModel(false);
                        return;
                    }
                    QJsonDocument jDoc = QJsonDocument::fromJson(qFile.readAll());
                    if (jDoc.isNull()){
                        _featureExportUIHandler->setConvertModel(false);
                        return;
                    }
                    QJsonObject jObj = jDoc.object();
                    bool convertModel = jObj["fbx_2_gltf_convert"].toBool();

                    _featureExportUIHandler->setConvertModel(convertModel);
                }
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void
        FeatureExportGUI::setActive(bool value)
    {
        // If value is set properly then set focus for area UI handler
        if (!_featureExportUIHandler.valid())
        {
            return;
        }

        if (!value)
        {
            _mode = EXPORT_MODE_NONE;
        }
        DeclarativeFileGUI::setActive(value);
    }

    void
        FeatureExportGUI::setMode(int mode)
    {
        _mode = (ExportMode)mode;
    }

    void FeatureExportGUI::_handleExportGeoButtonClicked()
    {
        _loadBusyBar(boost::bind(&FeatureExportGUI::_executePublishLayer, this), "Publishing Layer");
    }
    void FeatureExportGUI::_executePublishLayer()
    {
        _exportObject = _findChild(ExportGeoWindow);

        if (_exportObject)
        {
            if (_serverIp == "")
            {
                showError("Invalid Publishing Server", "Please enter a valid publishing server");
                return;
            }

            bool isSuccess = _featureExportUIHandler->exportGeoLayer(_serverIp);
            if (!isSuccess)
            {
                showError("Publishing Server Error", "Error in Publishing");
            }
            else{
                showError("Published To Server", "Layer successfully exported to server");
            }
        }

        _exportObject = _findChild(ExportGeoWindow);
        if (_exportObject)
            QMetaObject::invokeMethod(_exportObject, "closePopup");
    }

    void
        FeatureExportGUI::_handleExportButtonClicked()
    {
        _exportObject = _findChild(ExportWindow);
        if (_exportObject)
        {
            std::string filename = _exportObject->property("fileName").toString().toStdString();
            filename = osgDB::convertFileNameToNativeStyle(filename);
            std::string path = fileName.toStdString();
            if (filename == "")
            {
                showError("Invalid Filename", "Please enter a valid filename");
                return;
            }

            int pos = 0;
            bool result = false;
            if (_mode == EXPORT_MODE_FEATURE)
            {
                result = _featureExportUIHandler->exportFeature(filename);
            }
            else if (_mode == EXPORT_MODE_POINT_FEATURE)
            {
                std::string extension = osgDB::getFileExtension(filename);
                if (extension == "csv")
                {
                    result = _featureExportUIHandler->exportPointLayerToCSV(filename);
                }
                else
                {
                    result = _featureExportUIHandler->exportFeature(filename);
                }
            }
            if (_mode == EXPORT_MODE_ELEVATION)
            {
                result = _featureExportUIHandler->exportElevation(filename);
            }
#if 0
            if (_mode == EXPORT_ROUTE_OVERLAY)
            {
                result = _featureExportUIHandler->exportRoute(filename);
            }
#endif
            if (_mode == EXPORT_MODE_RASTER)
            {
                result = _featureExportUIHandler->exportRaster(filename);
            }
            if (_mode == EXPORT_MODE_TIN)
            {
                result = _featureExportUIHandler->exportFeatureToTin(filename);
            }
            if (_mode == EXPORT_MODE_GEOVRML)
            {
                result = _featureExportUIHandler->exportFeatureToGeoVrml(filename);
            }
            //RFE1
#if 0
            if(_mode == EXPORT_MODE_OVERLAY){
                result = _featureExportUIHandler->exportFeatureToOverlay(filename);
            }
            if (_mode == EXPORT_TRAJECTORY_OVERLAY){
                result = _featureExportUIHandler->exportTrajectory(filename);
            }
            if (_mode == EXPORT_FORMATION_OVERLAY)
            {
                result = _featureExportUIHandler->exportFormation(filename);
            }
#endif
            if (!result)
            {
                showError("Writing Failed", "Invalid path is given,Please give valid path");
                return;
            }
        }

        if (_exportObject)
        {
            QMetaObject::invokeMethod(_exportObject, "closePopup");
        }
    }

    void
        FeatureExportGUI::_handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "";
        QString formatFilter;
        QString caption;
        if (_mode == EXPORT_MODE_FEATURE)
        {
            QString selectedFilter;
            formatFilter = "SHP File (*.shp);;GMLFile(*.gml);;KML File(*.kml)";
            caption = "Save Feature";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }

        else if (_mode == EXPORT_MODE_POINT_FEATURE)
        {
            formatFilter = "SHP File (*.shp);;GML File(*.gml);;KML File(*.kml);;CSV File(*.csv)";
            caption = "Save Feature";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_MODE_ELEVATION)
        {
            formatFilter = "Tiff File (*.tif);;ASCII Grid File(*.grd)";
            caption = "Save Elevation";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_ROUTE_OVERLAY)
        {
            formatFilter = "SQlite File (*.sqlite)";
            caption = "Save Route";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_MODE_RASTER)
        {
            formatFilter = "Tiff File (*.tif)";
            caption = "Save Raster";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_MODE_TIN)
        {
            formatFilter = "TIN File (*.ive)";
            caption = "Save Tin";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_MODE_GEOVRML)
        {
            formatFilter = "GeoVrml File (*.wrl)";
            caption = "Save GeoVrml";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_MODE_OVERLAY)
        {
            formatFilter = "Overlay File (*.ovl)";
            caption = "Save Overlay";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_TRAJECTORY_OVERLAY)
        {
            formatFilter = "SQlite File (*.sqlite)";
            caption = "Save Trajectory";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        else if (_mode == EXPORT_FORMATION_OVERLAY)
        {
            formatFilter = "SQlite File (*.sqlite)";
            caption = "Save Formation";
            fileName = QFileDialog::getSaveFileName(parent, caption, directory, formatFilter, 0);
        }
        _exportObject = _findChild(ExportWindow);
        if (_exportObject)
        {
            _exportObject->setProperty("fileName", QVariant::fromValue(fileName));
            return;
        }
    }

    void
        FeatureExportGUI::_loadFeatureExportUIHandler()
    {
        _featureExportUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IFeatureExportUIHandler>(getGUIManager());
    }
} // namespace indiGUI