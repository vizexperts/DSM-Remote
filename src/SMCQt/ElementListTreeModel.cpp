#include <SMCQt/ElementListTreeModel.h>
#include <Core/IVisibility.h>
#include <Core/IBase.h>
#include <Core/IMessageFactory.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisibilityChangedMessage.h>
#include <Core/ICompositeObject.h>
#include <Elements/ElementsUtils.h>
#include <SMCElements/SMCElementsPlugin.h>
#include <SMCElements/IUserFolderItem.h>
#include <Terrain/IRasterObject.h>

namespace SMCQt
{
    const CORE::IObject* ElementListTreeModelItem::getObject() const
    {
        return _object.get();
    }

    void ElementListTreeModelItem::setObject(const CORE::IObject* object)
    {
        _object = object;

        if(_object.valid())
        {
            CORE::RefPtr<CORE::IBase> objBase = _object->getInterface<CORE::IBase>();
            //LOG_DEBUG("Element list item created for: " + objBase->getClassname() + ":" + objBase->getName() + ":" + objBase->getUniqueID().toString());        

            _name = QString::fromStdString(objBase->getName());
        }
    }

    QString ElementListTreeModelItem::getName() const
    {
        QString name("");

        if(_object.valid())
        {
            name = QString::fromStdString(_object->getInterface<CORE::IBase>()->getName()); 
        }
        else
        {
            name = _name;
        }

        return name;
    }

    void ElementListTreeModelItem::setName(QString name)
    {
        _name = name;
    }

    QString ElementListTreeModelItem::getSubName() const
    {
        QString subName("");
        subName = _subName;
        return subName;    
    }

    void ElementListTreeModelItem::setSubName(QString subName)
    {
        _subName = subName;
    }

    void ElementListTreeModelItem::setIcon(QString icon)
    {
        _iconPath = icon;
    }

    QString ElementListTreeModelItem::getIcon() const
    {
        return _iconPath;
    }

    bool ElementListTreeModelItem::changingCheckStatus() const
    {
        return _changingCheckState;
    }

    void ElementListTreeModelItem::setChangingCheckStatus(bool value)
    {
        _changingCheckState = value;
    }

    Qt::CheckState ElementListTreeModelItem::getCheckState()
    {
        if(_object.valid() && getChildCount() == 0 && !_changingCheckState)
        {
            CORE::IVisibility* visible = _object->getInterface<CORE::IVisibility>();
            if(visible)
            {
                _checkState = visible->getVisibility() ? Qt::Checked : Qt::Unchecked;
            }
        }

        return QMLTreeModelItem::getCheckState();
    }

    QHash<int,QByteArray> ElementListTreeModel::roleNames() const
    {
        QHash<int, QByteArray> roles = QMLTreeModel::roleNames();
        roles.insert(NameRole, QByteArray("name"));
        roles.insert(IconRole, QByteArray("iconPath"));

        return roles;
    }
    QVariant ElementListTreeModel::data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        if (index.row() > (_items.size()-1) )
            return QVariant();


        QMLTreeModelItem* qmlItem = _items.at(index.row());
        if(qmlItem == NULL)
            return QVariant();

        ElementListTreeModelItem *item = dynamic_cast<ElementListTreeModelItem*>(qmlItem);
        switch (role)
        {
        case Qt::DisplayRole:
        case NameRole:
            return QVariant::fromValue(item->getName());
        case IconRole:
            return QVariant::fromValue(item->getIcon());
        default:
            return QMLTreeModel::data(index, role);
        }
    }

    QString ElementListTreeModel::getItemName(int index) const
    {
        QString qstr;

        if(index > _items.size()-1 || index < 0)
            return qstr;

        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(_items.at(index));

        qstr = item->getName();

        return qstr;
    }
    void ElementListTreeModel::toggleManualCheckedState(int numIndex)
    {
        if(numIndex < 0)
            return;

        if(numIndex > (_items.size() - 1))
            return;

        if(!_items[numIndex]->isCheckable())
            return;
        
        if(_items[numIndex]->getManualCheckState() == Qt::Checked)
        {
            setCheckState(numIndex,  Qt::Unchecked);
        }
        else
        {
            setCheckState(numIndex, Qt::Checked);
        }

        //emit itemChanged(numIndex);
    }

    void ElementListTreeModel::toggleCheckedState(int numIndex)
    {
        if(numIndex < 0)
            return;

        if(numIndex > (_items.size() - 1))
            return;

        if(!_items[numIndex]->isCheckable())
            return;

        if(_items[numIndex]->getCheckState() == Qt::Checked)
        {
            setCheckState(numIndex,  Qt::Unchecked);
        }
        else
        {
            setCheckState(numIndex, Qt::Checked);
        }

        //emit itemChanged(numIndex);
    }

    void ElementListTreeModel::move(int numIndex, int newParentIndex)
    {
        if((numIndex < 0) || (newParentIndex < 0))
        {
            emitDataChanged(numIndex);
            return;
        }

        if((numIndex >= _items.size()) || (newParentIndex >= _items.size()))
        {
            emitDataChanged(numIndex);
            return;
        }

        ElementListTreeModelItem* itemToMove = dynamic_cast<ElementListTreeModelItem*>(_items.at(numIndex));

        if(!itemToMove)
        {
            emitDataChanged(numIndex);
            return;
        }

        ElementListTreeModelItem* itemParent = dynamic_cast<ElementListTreeModelItem*>(itemToMove->parent());
        ElementListTreeModelItem* newItemParent = dynamic_cast<ElementListTreeModelItem*>(_items.at(newParentIndex));

        if(itemParent && newItemParent)
        {
            int parentIndex = indexOf(itemParent);

            if(parentIndex != newParentIndex)
            {
                //! get corresponding objects
                CORE::RefPtr<const CORE::IObject> parentObject = itemParent->getObject();
                CORE::RefPtr<SMCElements::IUserFolderItem> previousParentFolder;
                if(parentObject.valid() /*&& parentIndex == 1*/)
                {
                    previousParentFolder = parentObject->getInterface<SMCElements::IUserFolderItem>();
                    //parentObject = ELEMENTS::GetFirstWorldFromMaintainer()->getInterface<CORE::IObject>();
                    //return;
                }

                CORE::RefPtr<const CORE::IObject> newParentObject = newItemParent->getObject();
                if(!newParentObject.valid())
                {
                    closeItem(parentIndex);
                    openItem(parentIndex);
                    return;
                }

                CORE::RefPtr<SMCElements::IUserFolderItem> newParentFolder = newParentObject->getInterface<SMCElements::IUserFolderItem>();
                if(!newParentFolder.valid())
                {
                    closeItem(parentIndex);
                    openItem(parentIndex);
                    return;
                }

                CORE::RefPtr<const CORE::IObject> objToMov = itemToMove->getObject();
                if(!objToMov.valid())
                {
                    closeItem(parentIndex);
                    openItem(parentIndex);
                    return;
                }

                CORE::RefPtr<CORE::IObject> objectToMove = objToMov->getInterface<CORE::IObject>();
                if(!objectToMove.valid())
                {
                    closeItem(parentIndex);
                    openItem(parentIndex);
                    return;
                }

                //! Check if current object is a folder object
                CORE::RefPtr<SMCElements::IUserFolderItem> folderItem = objectToMove->getInterface<SMCElements::IUserFolderItem>();
                    //! try to find IUserFolderObjectWrapper interface
                CORE::RefPtr<SMCElements::IUserFolderObjectWrapper> objectWrapperItem = objectToMove->getInterface<SMCElements::IUserFolderObjectWrapper>();
                if(folderItem.valid())
                {
                    if(newParentFolder.valid())
                    {
                        if(previousParentFolder.valid())
                        {
                            previousParentFolder->removeChild(objectToMove.get());
                        }
                        newParentFolder->addChild(objectToMove.get());
                    }
                }
                else if(objectWrapperItem.valid())
                {
                    if(newParentFolder.valid())
                    {
                        if(previousParentFolder.valid())
                        {
                            previousParentFolder->removeChild(objectToMove.get());
                        }
                        newParentFolder->addChild(objectToMove.get());
                    }

                }
                else
                {
                    //! Create an object wrapper and add the current object into it.
                    CORE::IObject* obj = CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*SMCElements::SMCElementsRegistryPlugin::UserFolderObjectWrpperType);
                    objectWrapperItem = obj->getInterface<SMCElements::IUserFolderObjectWrapper>();

                    objectWrapperItem->setReferencedObject(objectToMove->getInterface<CORE::IObject>());

                    if(newParentFolder.valid())
                    {
                        if(previousParentFolder.valid())
                        {
                            previousParentFolder->removeChild(obj);
                        }
                        newParentFolder->addChild(obj);
                    }
                }
            }
            else
            {
                closeItem(parentIndex);
                openItem(parentIndex);
            }
        }
    }

    bool ElementListTreeModel::customizeTree(QString searchString, SMCQt::QMLTreeModelItem *item,
        SMCQt::QMLTreeModelItem *parent)
    {
        if(!item)
        {
            return false;
        }

        ElementListTreeModelItem* elementListItem = dynamic_cast<ElementListTreeModelItem*>(item);
        if(!elementListItem)
        {
            return false;
        }

        bool isVisible = false;

        if(elementListItem->getName().contains(searchString, Qt::CaseInsensitive))
        {
            isVisible = true;
        }

        int index = indexOf(item);

        closeItem(index);

        openItem(index);

        if(searchString == "")
        {
            return false;
        }

        foreach(QMLTreeModelItem* childItem, item->getChildren())
        {
            bool childVisibility = customizeTree(searchString, childItem, item);

            isVisible = isVisible || childVisibility;
        }

        if(!isVisible && parent)
        {
            if((index > -1) && (index < _items.size()))
            {
                beginRemoveRows(QModelIndex(), index, index);
                _items.removeAt(index);
                endRemoveRows();
            }
        }

        return isVisible;
    }

    void ElementListTreeModel::updateCheckStatus(int numIndex)
    {
        if(numIndex < 0 || numIndex >= _items.count())
            return;

        QMLTreeModelItem* currentItem = _items[numIndex];

        QModelIndex modelIndex = index(numIndex);

        if(!currentItem)
            return;

        if(!dynamic_cast<ElementListTreeModelItem*>(currentItem)->changingCheckStatus())
        {
            bool foundChecked = false, foundUnchecked = false, foundPartial = false;

            foreach(QMLTreeModelItem *item, currentItem->getChildren())
            {
                Qt::CheckState childState = item->getCheckState();
                if(childState == Qt::Checked)
                {
                    foundChecked = true;
                }
                else if(childState == Qt::Unchecked)
                {
                    foundUnchecked = true;
                }
                else if(childState == Qt::PartiallyChecked)
                {
                    foundPartial = true;
                }
            }

            if(foundChecked && !foundUnchecked && !foundPartial)
            {
                setCheckState(numIndex, Qt::Checked);
            }
            else if(!foundChecked && foundUnchecked && !foundPartial)
            {
                setCheckState(numIndex, Qt::Unchecked);
            }
            else
            {
                setCheckState(numIndex, Qt::PartiallyChecked);
            }
        }
    }

    void ElementListTreeModel::setCheckState(int numIndex, Qt::CheckState state)
    {
        QModelIndex modelIndex = index(numIndex);

        ElementListTreeModelItem* currentItem = dynamic_cast<ElementListTreeModelItem*>(_items[numIndex]);

        if(currentItem->changingCheckStatus())
        {
            return;
        }

        currentItem->setChangingCheckStatus(true);

        currentItem->setCheckState(state);

        QMLTreeModelItem* parentItem = currentItem->parent();
        if(parentItem)
        {
            updateCheckStatus(_items.indexOf(parentItem));
        }

        if(state != Qt::PartiallyChecked)
        {
            foreach(QMLTreeModelItem* child, currentItem->getChildren())
            {
                int childIndex = _items.indexOf(child);

                if(childIndex == -1)
                    setCheckState(child, state);
                else
                    setCheckState(childIndex, state);
            }
        }

        if(currentItem->getObject())
        {
            CORE::RefPtr<CORE::IVisibility> visibility = currentItem->getObject()->getInterface<CORE::IVisibility>();
            if(visibility.valid())
            {
                // Added Third Case to Make Coposite Folder Visible when an object in it is partially clicked.
                if ((visibility->getVisibility() && (state == Qt::Unchecked)) || (!visibility->getVisibility() && (state == Qt::Checked)) || ((currentItem->getObject()->getInterface<CORE::ICompositeObject>() && state == Qt::PartiallyChecked)))
                {
                    visibility->setVisibility(!(state == Qt::Unchecked));
                    emit itemChangedSendNotify(visibility->getInterface<CORE::IObject>());
                }
            }
        }

        emit itemChanged(numIndex);

        emit dataChanged(modelIndex, modelIndex);

        currentItem->setChangingCheckStatus(false);
    }

    void ElementListTreeModel::setCheckState(QMLTreeModelItem* qmlTreeItem, Qt::CheckState state)
    {
        if(!qmlTreeItem)
            return;

        ElementListTreeModelItem* item = dynamic_cast<ElementListTreeModelItem*>(qmlTreeItem);

        if(!item)
            return;

        if(item->changingCheckStatus())
        {
            return;
        }

        item->setChangingCheckStatus(true);

        item->setCheckState(state);

        //! not needed
        QMLTreeModelItem* parentItem = item->parent();
        if(parentItem)
        {
            updateCheckStatus(_items.indexOf(parentItem));
        }

        if(state != Qt::PartiallyChecked)
        {
            foreach(QMLTreeModelItem* child, item->getChildren())
            {
                setCheckState(child, state);
            }
        }

        if(item->getObject())
        {
            CORE::RefPtr<CORE::IVisibility> visibility = item->getObject()->getInterface<CORE::IVisibility>();
            if(visibility.valid())
            {
                if((visibility->getVisibility()&& (state == Qt::Unchecked))||(!visibility->getVisibility()&& (state == Qt::Checked)))
                {
                    visibility->setVisibility(!(state == Qt::Unchecked));
                    emit itemChangedSendNotify(visibility->getInterface<CORE::IObject>());


                }
            }
        }

        item->setChangingCheckStatus(false);
    }

}   // SMCQt