/****************************************************************************
*
* File             : StereoSettingsGUI.h
* Description      : StereoSettingsGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/StereoSettingsGUI.h>
#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>
#include <serialization/StreamOperator.h>
#include <serialization/AsciiStreamOperator.h>
#include <DB/WriteFile.h>
#include <DB/ReadFile.h>
#include <DB/ReaderWriter.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <Elements/ElementsUtils.h>
#include <Util/StringUtils.h>
#include <Util/FileUtils.h>
#include <cmath>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, StereoSettingsGUI);
    DEFINE_IREFERENCED(StereoSettingsGUI, DeclarativeFileGUI);

    StereoSettingsGUI::StereoSettingsGUI()
    {}

    StereoSettingsGUI::~StereoSettingsGUI() 
    {}

    void StereoSettingsGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if(smpLeftMenu != NULL)
        {
            QObject::connect(smpLeftMenu, SIGNAL(connectStereoMenu()), this, SLOT(connectSmp()), Qt::UniqueConnection);
        }
    }

    void StereoSettingsGUI::connectSmp()
    {
        QObject* smpStereoMenu = _findChild("smpStereoMenu");
        if(smpStereoMenu != NULL)
        {

            bool stereoEnabled = osg::DisplaySettings::instance()->getStereo();
            smpStereoMenu->setProperty("stereoEnabled", QVariant::fromValue(stereoEnabled));

            osg::ref_ptr<osg::DisplaySettings> ds = osg::DisplaySettings::instance();
            std::string Seperation = UTIL::ToString(ds->getEyeSeparation());
            double eyeSeperation = UTIL::ToDouble(Seperation);
            bool toggleEyes = false;
            if(eyeSeperation < 0)
                toggleEyes = true;

            smpStereoMenu->setProperty("toggleEyes", QVariant::fromValue(toggleEyes));

            eyeSeperation = abs(eyeSeperation)*100;
            smpStereoMenu->setProperty("eyeSeperation",QVariant::fromValue(eyeSeperation));

            std::string Distance = UTIL::ToString(osg::DisplaySettings::instance()->getScreenDistance());
            double screenDistance = UTIL::ToDouble(Distance); ;
            smpStereoMenu->setProperty("screenDistance",QVariant::fromValue(screenDistance));

            std::string Width = UTIL::ToString(osg::DisplaySettings::instance()->getScreenWidth());
            double screenWidth = UTIL::ToDouble(Width);
            smpStereoMenu->setProperty("screenWidth", QVariant::fromValue(screenWidth));

            std::string Height = UTIL::ToString(osg::DisplaySettings::instance()->getScreenHeight());
            double screenHeight = UTIL::ToDouble(Height);
            smpStereoMenu->setProperty("screenHeight",QVariant::fromValue(screenHeight));

            osg::DisplaySettings::StereoMode stereoMode = osg::DisplaySettings::instance()->getStereoMode();
            smpStereoMenu->setProperty("mode",int(stereoMode)); 

            QObject::connect(smpStereoMenu, SIGNAL(setToggeEyes(bool)), this, 
                SLOT(toggleEyes(bool)), Qt::UniqueConnection);

            QObject::connect(smpStereoMenu, SIGNAL(setStereo(bool)), this, 
                SLOT(toggleStereo(bool)), Qt::UniqueConnection);

            QObject::connect(smpStereoMenu, SIGNAL(setMode(int)), this, 
                SLOT(setMode(int)), Qt::UniqueConnection);

            QObject::connect(smpStereoMenu, SIGNAL(updateEyeSeparation(double)), this,
                SLOT(updateEyeSeparation(double)), Qt::UniqueConnection);

            QObject::connect(smpStereoMenu, SIGNAL(updateScreenDistance(double)), this,
                SLOT(updateScreenDistance(double)), Qt::UniqueConnection);

            QObject::connect(smpStereoMenu, SIGNAL(updateScreenHeight(double)), this, 
                SLOT(updateScreenHeight(double)), Qt::UniqueConnection);

            QObject::connect(smpStereoMenu, SIGNAL(updateScreenWidth(double)), this,
                SLOT(updateScreenWidth(double)), Qt::UniqueConnection);

            QObject::connect(smpStereoMenu, SIGNAL(save()), this, SLOT(saveStereoSettings()), Qt::UniqueConnection);
        }
    }

    void StereoSettingsGUI::saveStereoSettings()
    {
        _writeSettings();
    }

    //! Called to toggle Stereo On/Off
    void StereoSettingsGUI::toggleStereo(bool state)
    {    
        osg::DisplaySettings::instance()->setStereo(state);
    }

    //! Called to toggle eyes 
    void StereoSettingsGUI::toggleEyes(bool state)
    {    
        osg::ref_ptr<osg::DisplaySettings> ds = osg::DisplaySettings::instance();
        double eyeSeperation = abs(ds->getEyeSeparation());
        if(state)
        {
            ds->setEyeSeparation(-1.0*eyeSeperation);
        }
        else
        {
            ds->setEyeSeparation(eyeSeperation);
        }
    }

    //! Called when stereo mode is changed
    void StereoSettingsGUI::setMode(int mode)
    {
        osg::DisplaySettings::instance()->setStereoMode(osg::DisplaySettings::StereoMode(mode));
    }

    //! Called when eye seperation is changed
    void StereoSettingsGUI::updateEyeSeparation(double value)
    {
        try
        {
            QObject* smpStereoMenu = _findChild("smpStereoMenu");
            if(smpStereoMenu)
            {
                bool toggleEyes = smpStereoMenu->property("toggleEyes").toBool();
                value = abs(value)/100;
                if(toggleEyes)
                {
                    osg::DisplaySettings::instance()->setEyeSeparation(-1.0*value);
                }
                else
                {
                    osg::DisplaySettings::instance()->setEyeSeparation(value);
                }
            }
        }
        catch(UTIL::Exception &e)
        {
            osg::DisplaySettings::instance()->setEyeSeparation(value);
            LOG_ERROR(e.What());
        }
    }

    //! Called when screen distance is changed
    void StereoSettingsGUI::updateScreenDistance(double value)
    {
        osg::DisplaySettings::instance()->setScreenDistance(value);
    }

    //! Called when screen width is changed
    void StereoSettingsGUI::updateScreenWidth(double value)
    {
        osg::DisplaySettings::instance()->setScreenWidth(value);
    }

    //! Called when screen height is changed
    void StereoSettingsGUI::updateScreenHeight(double value)
    {
        osg::DisplaySettings::instance()->setScreenHeight(value);
    }

    void StereoSettingsGUI::_writeSettings()
    {
        DB::BaseWrapper *wrapper   = DB::BaseWrapperManager::instance()->findWrapper("SettingComponent");
        if(wrapper)
        {
            CORE::IComponent *settingComponent = ELEMENTS::GetComponentFromMaintainer("SettingComponent");
            CORE::IBase* base = NULL;
            if(settingComponent)
            {
                base = settingComponent->getInterface<CORE::IBase>();

                std::stringstream inputstream;
                CORE::RefPtr<DB::OutputIterator> oi = new AsciiOutputIterator(&inputstream);

                DB::OutputStream os( NULL );
                os.start( oi.get(), DB::OutputStream::WRITE_ATTRIBUTE);
                wrapper->write(os, *base);

                std::ofstream filePtr;
                std::string settingConfigFile = _getAbsoluteSettingConfPath(ELEMENTS::ISettingComponent::SETTINGCONFIGFILE);
                filePtr.open(settingConfigFile.c_str(), std::ios::out);
                std::string dataToWrite = inputstream.str();
                filePtr << dataToWrite.c_str();
                filePtr.close();
            }
        }
    }

    void StereoSettingsGUI::initializeAttributes()
    {
        std::string groupName = "StereoSettingsGUI attributes";

        // Add enable stereo attribute
        _addAttribute(new CORE::BooleanAttribute("EnableStereo", "EnableStereo",
            CORE::BooleanAttribute::SetFuncType(this, &StereoSettingsGUI::toggleStereo),
            CORE::BooleanAttribute::GetFuncType(this, &StereoSettingsGUI::isStereoEnabled),
            "Enable Stereo",
            groupName));

        // Add stereo mode attribute
        _addAttribute(new CORE::IntAttribute("StereoMode", "StereoMode",
            CORE::IntAttribute::SetFuncType(this, &StereoSettingsGUI::setMode),
            CORE::IntAttribute::GetFuncType(this, &StereoSettingsGUI::getStereoMode),
            "Stereo Mode",
            groupName));

        // Add eye seperation attribute
        _addAttribute(new CORE::DoubleAttribute("EyeSeperation", "EyeSeperation",
            CORE::DoubleAttribute::SetFuncType(this, &StereoSettingsGUI::updateEyeSeparation),
            CORE::DoubleAttribute::GetFuncType(this, &StereoSettingsGUI::getEyeSeperation),
            "Eye Seperation",
            groupName));

        // Add screen distance attribute
        _addAttribute(new CORE::DoubleAttribute("ScreenDistance", "ScreenDistance",
            CORE::DoubleAttribute::SetFuncType(this, &StereoSettingsGUI::updateScreenDistance),
            CORE::DoubleAttribute::GetFuncType(this, &StereoSettingsGUI::getScreenDistance),
            "Screen Distance",
            groupName));

        // Add screen width attribute
        _addAttribute(new CORE::DoubleAttribute("ScreenWidth", "ScreenWidth",
            CORE::DoubleAttribute::SetFuncType(this, &StereoSettingsGUI::updateScreenWidth),
            CORE::DoubleAttribute::GetFuncType(this, &StereoSettingsGUI::getScreenWidth),
            "Screen Width",
            groupName));

        // Add screen height attribute
        _addAttribute(new CORE::DoubleAttribute("ScreenHeight", "ScreenHeight",
            CORE::DoubleAttribute::SetFuncType(this, &StereoSettingsGUI::updateScreenHeight),
            CORE::DoubleAttribute::GetFuncType(this, &StereoSettingsGUI::getScreenHeight),
            "Screen Height",
            groupName));

        DeclarativeFileGUI::initializeAttributes();
    }

    void StereoSettingsGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        //! NP:-On project load stereo setting(on/off) has to be applied again
        /////////////////////////////
        //!
        //! \var stereoInitialized
        //!
        //! \brief Weather stereo has been re initialized after World load
        //////////////////////////////
        static bool stereoInitialized = false;


        /////////////////////////////
        //!
        //! \var tick
        //!
        //! \brief tick counter. Resets on world load.
        //////////////////////////////
        static int tick = 0;

        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {   
                //! subscribe to world loaded message
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            //! reset static variables on world load
            stereoInitialized = false;
            tick = 0;

            CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
            _subscribe(wmain.get(), *CORE::WorldMaintainer::TickMessageType);
        }
        else if(messageType == *CORE::WorldMaintainer::TickMessageType)
        {
            //! Check if stereo has been initialized
            if(!stereoInitialized)
            {
                /////////////////////////////
                //!
                //! \var stereo
                //!
                //! \brief Static keeping the stereo value to be applied.
                //////////////////////////////
                static bool stereo = false;

                //! First tick after world load
                if(tick == 0)
                {
                    //! Get stereo value to be applied
                    stereo = osg::DisplaySettings::instance()->getStereo();

                    //! Set stereo to false
                    osg::DisplaySettings::instance()->setStereo(false);
                }

                //! fifth tick after world load
                if(tick == 5)
                {
                    //! Applying original stereo value
                    osg::DisplaySettings::instance()->setStereo(stereo);

                    //! Stereo setting re initialised succesfully!!
                    stereoInitialized = true;

                    CORE::IWorldMaintainer *wMaintainer = CORE::WorldMaintainer::instance();
                    _unsubscribe(wMaintainer, *CORE::WorldMaintainer::TickMessageType);
                }

                //! increment tick counter
                tick++;
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    std::string StereoSettingsGUI::_getAbsoluteSettingConfPath(const std::string& configFile)
    {
        std::string settingConfigFile = configFile;

        //Substituting the AppData path with the Real Path
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        //convert the APPDATA variable with the actual path
        std::string appdata = "${APPDATA}";
        if(settingConfigFile.find(appdata.c_str()) != std::string::npos)
        {
            int count = std::string(appdata.c_str()).length();
            settingConfigFile.replace(settingConfigFile.find(appdata.c_str()),
                count, appdataPath.c_str());
            settingConfigFile = osgDB::convertFileNameToNativeStyle(settingConfigFile);
        }

        return settingConfigFile;
    }

} // namespace SMCQt
