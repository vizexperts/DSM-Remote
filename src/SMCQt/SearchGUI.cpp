/****************************************************************************
*
* File             : SearchGUI.cpp
* Description      : SearchGUI class definition
*
*****************************************************************************/
#include <SMCQt/SearchGUI.h>

#include <Core/CoreRegistry.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/ITaggable.h>
#include <Core/IObjectMessage.h>
#include <Elements/IDrawTechnique.h>
#include <Core/IPenStyle.h>
#include <Core/IPoint.h>
#include <Core/ITemporary.h>
#include <Core/IText.h>
#include <Core/WorldMaintainer.h>
#include <Core/GroupAttribute.h>
#include <Core/AttributeTypes.h>
#include <Core/IBrushStyle.h>
#include <Elements/IRepresentation.h>
#include <Elements/IIconLoader.h>
#include <Elements/IIconHolder.h>
#include <Elements/IIcon.h>
#include <Elements/IClamper.h>
#include <Elements/ElementsPlugin.h>
#include <Elements/IIcon.h>

#include<VizUI/ICameraUIHandler.h>
#include<VizUI/UIHandler.h>

#include <Database/IDockDbInfo.h>
#include <Database/DockDbInfo.h>
#include <Database/DatabasePlugin.h>
#include <Database/IDBRecord.h>
#include <Database/IDBTable.h>
#include <Database/IDBField.h>

#include <App/AccessElementUtils.h>

#include <Util/Log.h>

#include <QRegExp>

#include <iostream>
#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataFieldDefn.h>
#include <Core/IMetadataRecordHolder.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataCreator.h>
#include <Elements/IClamper.h>
#include <Elements/ElementsUtils.h>
#include <App/IUIHandler.h>

#include <Util/StringUtils.h>

#include <SMCElements/ILayerComponent.h>
#include <Core/IMetadataRecord.h>
#include <Core/AttributeTypes.h>
#include <Core/IMetadataTable.h>
#include <Util/FileUtils.h>
#include <Util/StringUtils.h>
#include <QJSONDocument>
#include <QJSONObject> 
#include <Util/IndianGRUtil.h>
namespace SMCQt
{

    DEFINE_META_BASE(SMCQt, SearchGUI);
    DEFINE_IREFERENCED(SearchGUI, DeclarativeFileGUI);
    const std::string SearchGUI::BookMarkName="Recent search";
    const std::string SearchGUI::TagToPreventTransaction="PreventTransaction";
    SearchGUI::SearchGUI()
        : _geoJumpRange(100000)
    {
    }

    void SearchGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the LineUIHandler and set it
            try
            {   
                // Subscribe for world added message to the world maintainer
                CORE::RefPtr<CORE::IWorldMaintainer> wmain =
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("DockDbInfo");
                if(component.valid())
                {
                    _databaseComponent = component->getInterface<DATABASE::IDockDbInfo>();
                } 
                _databaseLayerLoaderUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IDatabaseLayerLoaderUIHandler>(getGUIManager());

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if (messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            _readUidOfRecentSearch();
        }
        else
        {
            VizQt::QtGUI::update(messageType, message);
        }
    }
    void SearchGUI::_writeUidOfRecentSearch()
    {
        std::string uniqueId = "";
        if (_searchBookmark.valid())
        {
            uniqueId = _searchBookmark->getInterface<CORE::IBase>()->getUniqueID().toString();
        }
        CORE::RefPtr<CORE::IWorldMaintainer> wmain =
            APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
        std::string ProjectFilePath = osgDB::getFilePath(wmain->getProjectFile());

        QFile settingFile(QString::fromStdString(ProjectFilePath) + "/ProjectSetting.conf");
        QByteArray settingData;
        if (settingFile.open(QIODevice::ReadOnly))
        {
            settingData = settingFile.readAll();
            settingFile.close();
        }

        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);
        QJsonObject settingObject = settingDoc.object();
        settingObject.insert("RecentSearchUid", QString::fromStdString(uniqueId));
        settingDoc.setObject(settingObject);
        QJsonDocument settingDoctemp;
        settingDoctemp.setObject(settingObject);
        if (settingFile.open(QIODevice::WriteOnly))
        {
            settingFile.write(settingDoctemp.toJson());
            settingFile.close();
        }
        return;
        
    }
    void SearchGUI::_readUidOfRecentSearch()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain =
            APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());
        std::string ProjectFilePath = osgDB::getFilePath(wmain->getProjectFile());

        QFile settingFile(QString::fromStdString(ProjectFilePath) + "/ProjectSetting.conf");
        if (!settingFile.open(QIODevice::ReadOnly))
        {
            return;
        }

        QByteArray settingData = settingFile.readAll();

        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

        QJsonObject settingObject = settingDoc.object();

        QString recentSearchUid = settingObject["RecentSearchUid"].toString();

        if (recentSearchUid.isEmpty())
        {
             settingFile.close();
             return;
        }
        else
        {
            _uniqueIDOfRecentSearch = new CORE::UniqueID(recentSearchUid.toStdString());
            settingFile.close();
        }
        
    }
    void SearchGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* elementList = _findChild("elementList");
        if(elementList)
        {
            QObject::connect(elementList, SIGNAL(search(QString)), this, SLOT(getGeoJumpInfo(QString)),
                Qt::UniqueConnection);
            QObject::connect(elementList, SIGNAL(selectPlace(int)), this, SLOT(selectPlace(int)), Qt::UniqueConnection);
        }
        CORE::RefPtr<CORE::IComponent> component = CORE::WorldMaintainer::instance()->getComponentByName("MapSheetInfoComponent");
        _mapSheetComponent= component->getInterface<ELEMENTS::IMapSheetInfoComponent>();
        
        clearList();
    }

    void SearchGUI::selectPlace(int index)
    {
        if((index < 0) || (index >= _placesList.size()))
            return;


        SearchObj* place = qobject_cast<SearchObj*>( _placesList[index]);
        if(place == NULL)
            return;

        float x = place->longitude(),y = place->latitude();

        if(x == 0 || y == 0)
        {
            _databaseComponent->getLongitudeLatitudeByPlace(place->name().toStdString(),x,y);
        }

        if( x == 0 || y == 0)
        {
            QObject* elementList = _findChild("elementList");
            if(elementList)
            {
                elementList->setProperty("searchText", QVariant::fromValue(place->name()));
            }

            return;
        }
        _createSearchBookmark();

        if(x==-1 && y== -1 ) //special test case for MapSheet area
        {
            osg::Vec4d extents=_mapSheetComponent->getExtents(place->name().toStdString());
            showExtentArea(extents,place->name().toStdString());
        }
        else
        {
            CORE::RefPtr<VizUI::ICameraUIHandler>  camUI = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager());
            osg::Vec3d osgPoint=osg::Vec3d(x, y,0.0);
            camUI->setViewPoint(osgPoint,_geoJumpRange,0.0,-90,3.0);

            showMarker(osgPoint,place->name().toStdString());
        }


    }
    void SearchGUI::_createSearchBookmark()
    {
        //! check whether search Bookmark already added in element list or not 
        if(!_searchBookmark.valid())
        {
            if (_uniqueIDOfRecentSearch.valid())
            {
                _searchBookmark = worldInstance()->getObjectByID(_uniqueIDOfRecentSearch);
            }
            //May Be _uniqueIDOfRecentSearch object Not Present In World
            if (!_searchBookmark.valid())
            {
                CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
                _searchBookmark = objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FolderType);
                _searchBookmark->getInterface<CORE::IBase>()->setName(BookMarkName);
                CORE::RefPtr<CORE::ITaggable> tagObject = _searchBookmark->getInterface<CORE::ITaggable>();
                tagObject->addTag(TagToPreventTransaction);
                worldInstance()->addObject(_searchBookmark.get());
                _writeUidOfRecentSearch();
            }

        }
    }

    void SearchGUI::clearList()
    {
        _placesList.clear();
        _addKeywordsToSuggestions("");
        _setContextProperty("placesModel", QVariant::fromValue(_placesList));
    }

    void SearchGUI::_helpMessage(std::vector<std::string> &str)
    {
        str.push_back("The syntax should be as follow");
        str.push_back("IA RV 123 786");
        str.push_back(" 10.3133 N/S 45.263 E/W");
        str.push_back(" 53.216 E/W  78.3133 N/S");
        str.push_back("23j12");
        str.push_back("23g12 234545");
        str.push_back(" Upto 5 significant figure");
        str.push_back(" 78 45' 56\" N/S 153 26' 34\" E/W ");
        str.push_back(" 153 26' 34\" E/W 78 45' 56\" N/S");
        str.push_back(" -90 <= latitude <= 90 ");
        str.push_back(" -180 <= longitude <= 180 ");
        
    }

    bool SearchGUI::_checkValidString(QString qst)
    {

        QRegExp rx("([qr]|[uvxyz])");
        int pos=rx.indexIn(qst);
        QRegExp rx1("[QR]|[UVXYZ])");
        int pos1=rx1.indexIn(qst);
        QRegExp rx2("([0-9])");
        int pos2=rx2.indexIn(qst);

        if( ( (pos >= 0) || (pos1 >=0) ) && (pos2>=0) )
        {
            return false;
        }

        return true;
    }

    bool SearchGUI::_stringContainAlphabet(QString qst)
    {
        QRegExp rx("^([a-z]|[A-Z]){0,}$");
        int pos=rx.indexIn(qst);
        if(pos>=0) return true;

        return false;
    }
    void SearchGUI:: _checkForMapGrAndLatLongExpr(QString str)
    {
        if(str.isEmpty()||(!_checkValidString(str)))
            return;
        str=str.trimmed(); //removing whitespace from start and End
        QString Exprss=str;

        //Note :: Regular Expression are Very Sensitive to a single Character
        //Regular Expression for "latitude longitude" or "longitude latitude"

        QRegExp rx1("^([0-9](\\.[0-9]{1,5}){0,1}|[1-8][0-9](\\.[0-9]{1,5}){0,1}|90((\\.[0]{1}){0,1}))(\\040{1})[NSns]{1}(\\040{1})([0-9]{0,2}((\\.\\d{0,5}){0,1})|1[0-7][0-9]((\\.\\d{0,5}){0,1})|180((\\.[0]{1}){0,1}))(\\040{1})[EWew]{1}$");

        QRegExp rx2("^([0-9]{0,2}((\\.\\d{0,5}){0,1})|1[0-7][0-9]((\\.\\d{0,5}){0,1})|180((\\.[0]{1}){0,1}))(\\040{1})[EWew]{1}(\\040{1})([0-9](\\.[0-9]{1,5}){0,1}|[1-8][0-9](\\.[0-9]{1,5}){0,1}|90((\\.[0]{1}){0,1}))(\\040{1})[NSns]{1}$");

        QRegExp grex("^([0-9]{1,3})[a-p]{1}([1-9]{1}|0[1-9]|1[0-6]){0,1}\\040([0-9]{6}|[0-9]{8}|[0-9]{10})$");

        QRegExp map("^([0-9]{1,3})[a-p]{1}([1-9]{1}|0[1-9]|1[0-6]){0,1}$");

        std::string mapVal,grVal;
        if((grex.indexIn(str)>=0))
        {

            QString MapGrExprss=str;
            QStringList tokens = MapGrExprss.split(" ");
            if(tokens.size() == 2)
            {
                mapVal=tokens[0].toStdString();
                grVal=tokens[1].toStdString();
            }
        }
        try
        {
            if((grex.indexIn(str)>=0) &&  _mapSheetComponent.valid() && _mapSheetComponent->isValid(mapVal,grVal))//GR Expression
            {
                double leLat,leLong;
                if(mapVal!="")
                {
                    osg::Vec2d longLat = _mapSheetComponent->getLatLongFromMapsheetAndGR(mapVal,grVal); //return long,lat value
                    leLat = longLat.y();
                    leLong = longLat.x(); //only naming convention change
                    _placesList.prepend(new SearchObj(str, leLat, leLong));

                 }
                return;
            }
            else if ((map.indexIn(str)>= 0) && _mapSheetComponent.valid() && _mapSheetComponent->isValid(str.toStdString())) // Map Expression
            {
                double latitude=-1,longitude=-1;
                _placesList.prepend( new SearchObj(str, longitude, latitude));
            }
            else if( (rx1.indexIn(str)>= 0) || (rx2.indexIn(str) >= 0) ) //Lat Long Expression
            {
                QRegExp rxNS("[NSns]{1,1}");
                QRegExp rxEW("[EWew]{1,1}");

                std::string st=Exprss.toStdString();

                if( ((rxNS.indexIn(str)) !=-1 ) && ( (rxEW.indexIn(str)) != -1)) // If the expression has got both  N/S and E/W direction given
                {
                    int posNS=rxNS.indexIn(str);
                    int posEW=rxEW.indexIn(str);
                    double latitude=0.0,longitude=0.0;
                    _extractLongLatGivenNAndE(posEW ,posNS, st,latitude,longitude);
                    _placesList.prepend( new SearchObj(str, longitude, latitude));
                    return;
                }
                return;
            }
            else
            {    
                double inputLatitude=0,inputLongitude=0;
               if(_checkForDMS(str,inputLatitude,inputLongitude))
               {
                    _placesList.prepend( new SearchObj(str, inputLongitude, inputLatitude));
                    return;

               }
               return;

            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }

        return;

    }

    void SearchGUI::_checkForKeywordsAndPopulateSuggestions(QString searchString)
    {
        if(!_databaseLayerLoaderUIHandler.valid())
            return;

        std::map<std::string, std::string>::const_iterator mapIter = _searchSpaceMap.begin();
        for(; mapIter != _searchSpaceMap.end(); mapIter++)
        {
            if(searchString.startsWith(QString::fromStdString(mapIter->first), Qt::CaseInsensitive))
            {
                //! extract the search string
                QString searchStr = searchString;
                searchStr.remove(QString::fromStdString(mapIter->first), Qt::CaseInsensitive);
                //searchStr = searchStr.simplified();
                QRegExp extractValidStr("^(\\W+)");
                int pos = extractValidStr.indexIn(searchStr);
                if(pos != -1)
                {
                    QString toRemove = extractValidStr.cap(1);
                    searchStr.remove(toRemove);

                    QString queryStatement = QString::fromStdString( mapIter->second);
                    queryStatement.replace("__arg__" , searchStr, Qt::CaseInsensitive);

                    //! Hard coded database to connect to!!!!
                    CORE::RefPtr<DATABASE::ISQLQueryResult> result = _databaseLayerLoaderUIHandler->executeSQLQuery(queryStatement.toStdString(), SMCUI::IDatabaseLayerLoaderUIHandler::DBType_MSSQL);
                     if(!result.valid())
                         return;

                    CORE::RefPtr<DATABASE::IDBTable> table = result->getResultTable();
                    if(!table.valid())
                        return;

                    CORE::RefPtr<DATABASE::IDBRecord> record = NULL;
                    while((record = table->getNextRecord()) != NULL)
                    {
                        DATABASE::IDBField* placeField = record->getFieldByName("place");
                        DATABASE::IDBField* latField = record->getFieldByName("latitude");
                        DATABASE::IDBField* longField = record->getFieldByName("longitude");

                        //if(!placeField || !latField || !longField)
                        //    return;

                        std::string placeName = placeField->toString();
                        double latitude = (latField == NULL) ?  1 : latField->toFloat();
                        double longitude = (longField == NULL) ?  1 : longField->toFloat();
                        _placesList.prepend( new SearchObj(QString::fromStdString(mapIter->first + ": " + placeName ), latitude, longitude)); 
                    }
                }

                //! Return since we have populated the keyword
                return;
            }
        }
    }

    void SearchGUI::_addKeywordsToSuggestions(QString searchString)
    {
        std::map<std::string, std::string>::const_iterator mapIter = _searchSpaceMap.begin();
        for(; mapIter != _searchSpaceMap.end(); mapIter++)
        {
            QString keyword = QString::fromStdString(mapIter->first);
            if(keyword.contains(searchString, Qt::CaseInsensitive))
            {
                _placesList.push_front( new SearchObj(keyword + ": ", 0, 0)); 
                //stringList.push_front(keyword + ": ");
            }
        }
    }

    void SearchGUI::initializeAttributes()
    {
        DeclarativeFileGUI::initializeAttributes();

        // add range color value
        _addAttribute(new CORE::GroupAttribute("SearchSpace", "SearchSpace",
            CORE::GroupAttribute::SetFuncType(this, &SearchGUI::_addKeyword),
            CORE::GroupAttribute::GetFuncType(this, &SearchGUI::_getKeyword),
            "SearchSpace",
            "SearchGUI Attributes"));        

        _addAttribute(new CORE::DoubleAttribute("GeoJumpRange", "GeoJumpRange",
            CORE::DoubleAttribute::SetFuncType(this, &SearchGUI::_setGeoJumpRange),
            CORE::DoubleAttribute::GetFuncType(this, &SearchGUI::_getGeoJumpRange),
            "GeoJumpRange",
            "SearchGUI Attributes"));        

    }

    void SearchGUI::_addKeyword(const CORE::NamedGroupAttribute &groupAttr)
    {
        const CORE::NamedStringAttribute* attKeyword= 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("Keyword"));

        const CORE::NamedStringAttribute* attQuery = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("Query"));

        if(!attKeyword || !attQuery )
            return;

        _searchSpaceMap[attKeyword->getValue()] = attQuery->getValue();
    }

    CORE::RefPtr<CORE::NamedGroupAttribute> SearchGUI::_getKeyword() const
    {
        return NULL;
    }

    void SearchGUI::_setGeoJumpRange(double value)
    {
        _geoJumpRange = value;
    }

    double SearchGUI::_getGeoJumpRange() const
    {
        return _geoJumpRange;
    }

 
    void SearchGUI::_getDefaultLayerAttributes(std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecesions) const
    {
        attrNames.push_back("NAME");
        attrTypes.push_back("Text");
        attrWidths.push_back(128);
        attrPrecesions.push_back(1);

        attrNames.push_back("Level");
        attrTypes.push_back("Text");
        attrWidths.push_back(5);
        attrPrecesions.push_back(1);

    }
    void SearchGUI::getGeoJumpInfo(QString location)
    {

        QString simpleLoc = location.simplified();

        if( (simpleLoc.isEmpty()) || (simpleLoc == "Location") || (simpleLoc == "Object"))
        {
            clearList();
            return;
        }

        _placesList.clear();

        std::string place = location.toStdString();

        std::string searchText = simpleLoc.toStdString();

        std::vector<std::string> strList;

        QString qst = simpleLoc;;

        replace( searchText.begin(), searchText.end(), ' ' ,'%');

        //There is not option available in the Database
        if((_databaseComponent->getRowCount(searchText)) != 0)
        {
            _databaseComponent->getSqlResult(searchText,strList);
        }
        _checkForKeywordsAndPopulateSuggestions(simpleLoc);       ///! Check for keywords
        _checkForMapGrAndLatLongExpr(qst);       ///! Check for Map GR Lat long
        checkIndianGR(qst.toStdString());
        if(strList.empty() && _placesList.isEmpty())
        {
            if( ! _checkValidString(qst) )
            {
                strList.push_back("Couldn't understand");
                if(isdigit(searchText[0]))
                    _helpMessage(strList);
            }
            else if (_stringContainAlphabet(qst))
            {
                strList.push_back("Couldn't understand");
            }
            else
            {
                strList.push_back("Invalid Expression");
                _helpMessage(strList);
            }
        } 


        for(int i = 0; i < strList.size(); i++)
        {
            _placesList.append(new SearchObj( QString::fromStdString(strList[i]), 0, 0));
        }

        _addKeywordsToSuggestions(location);

        _setContextProperty("placesModel", QVariant::fromValue(_placesList));
    }
    void SearchGUI::showExtentArea(osg::Vec4d osgExtent , std::string placeName) 
    {
        bool _temporary = false;
        bool _clamping = true;

        CORE::RefPtr<CORE::IObject> lineObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::LineFeatureObjectType);

        CORE::RefPtr<CORE::ILine> line =  lineObject->getInterface<CORE::ILine>();
        if (line.valid())
        {
            CORE::RefPtr<CORE::IPenStyle> pen = line->getInterface<CORE::IPenStyle>();
            pen->setPenColor(osg::Vec4(1.0, 1.0, 0.0, 1));
            pen->setPenWidth(2.0f);

            CORE::RefPtr<CORE::IBrushStyle> brushStyle = line->getInterface<CORE::IBrushStyle>();
            if (brushStyle.valid())
            {
                brushStyle->setBrushState((CORE::IBrushStyle::BrushState)(CORE::IBrushStyle::OFF | CORE::IBrushStyle::PROTECTED));
            }
            //// Setting the place name on top of the marker
            CORE::RefPtr<CORE::IText> text = line->getInterface<CORE::IText>();
            if (text.valid())
            {
                text->setText(placeName);
                text->setTextSize(20);
                text->setBold(true);
                text->setTextActive(true);

            }
        }
        CORE::RefPtr<ELEMENTS::IRepresentation> representation =
            lineObject->getInterface<ELEMENTS::IRepresentation>();

        if (representation.valid())
        {
            representation->setRepresentationMode(ELEMENTS::IRepresentation::TEXT);
        }
        line->addPoint(osg::Vec3d(osgExtent.y(), osgExtent.x(),0.0));
        line->addPoint(osg::Vec3d(osgExtent.w(), osgExtent.x(),0.0));
        line->addPoint(osg::Vec3d(osgExtent.w(), osgExtent.z(),0.0));
        line->addPoint(osg::Vec3d(osgExtent.y(), osgExtent.z(),0.0));
        line->addPoint(osg::Vec3d(osgExtent.y(), osgExtent.x(), 0.0));

        //set the current temporary state
        CORE::RefPtr<CORE::ITemporary> temp = lineObject->getInterface<CORE::ITemporary>();
        if(temp.valid())
        {
            temp->setTemporary(_temporary);
        }
        CORE::RefPtr<ELEMENTS::IClamper> clamp = lineObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            clamp->setClampOnPlacement(_clamping);
        }
        osg::ref_ptr<osg::Node> node = lineObject->getInterface<CORE::IObject>()->getOSGNode();
        if(node.valid())
        {
            osg::ref_ptr<osg::StateSet> stateSet = node->getOrCreateStateSet();
            stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF|osg::StateAttribute::PROTECTED);
        }
        if(_searchBookmark.valid())
        {
            _searchBookmark->getInterface<CORE::ICompositeObject>()->addObject(lineObject.get());
        }

        jumpToObject(lineObject.get());

    }

    void SearchGUI::showMarker(osg::Vec3d osgpoint , std::string placeName) 
    {

        bool _clamping = true;
        bool _temporary = false;
        CORE::RefPtr<CORE::IObject> pointObject =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*ELEMENTS::ElementsRegistryPlugin::OverlayObjectType);

        CORE::RefPtr<CORE::IPoint> point =  pointObject->getInterface<CORE::IPoint>();
        if(point.valid())
        {
            // Setting the place name on top of the marker
            CORE::RefPtr<CORE::IText> text = point->getInterface<CORE::IText>();
            if(text.valid())
            {
                text->setText(placeName);
                text->setTextSize(20);
                text->setBold(true);
                text->setTextActive(true);

            }
        }
        point->setValue(osgpoint);

        CORE::RefPtr<CORE::IComponent> component = CORE::WorldMaintainer::instance()->getComponentByName("IconModelLoaderComponent");

        if(component.valid())
        {
            CORE::RefPtr<ELEMENTS::IIconLoader> iconLoader = component->getInterface<ELEMENTS::IIconLoader>();
            CORE::RefPtr<ELEMENTS::IIconHolder> iconHolder = pointObject->getInterface<ELEMENTS::IIconHolder>();

            if(iconHolder.valid() && iconLoader.valid())
            {
                iconHolder->setIcon(iconLoader->getIconByName("circle"));
            }
        }
        //set the representation to icon mode
        CORE::RefPtr<ELEMENTS::IRepresentation> representation = 
            pointObject->getInterface<ELEMENTS::IRepresentation>();

        if(representation.valid())
        {
            representation->setRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT);
        }

        //set the current temporary state
        CORE::RefPtr<CORE::ITemporary> temp = pointObject->getInterface<CORE::ITemporary>();
        if(temp.valid())
        {
            temp->setTemporary(_temporary);
        }

        CORE::RefPtr<ELEMENTS::IClamper> clamp = pointObject->getInterface<ELEMENTS::IClamper>();
        if(clamp.valid())
        {
            clamp->setClampOnPlacement(_clamping);
        }

        if(_searchBookmark.valid())
        {
            _searchBookmark->getInterface<CORE::ICompositeObject>()->addObject(pointObject.get());
        }

    }




    CORE::IWorld* SearchGUI::worldInstance()
    {  
        CORE::WorldMaintainer* worldMaintainer = CORE::WorldMaintainer::instance();
        typedef std::map<const CORE::UniqueID*, CORE::RefPtr<CORE::IWorld> > WorldMap;
        WorldMap worldmap = worldMaintainer->getWorldMap();
        WorldMap::iterator iter = worldmap.begin();
        return iter->second.get();
    }


    //Extract the longitude and latitude given both the direction N/S and E/W
    bool SearchGUI::_extractLongLatGivenNAndE(int posEW , int posNS, std::string st,double &longitude,double &latitude)
    {
        if(posEW>posNS)
        {
            sscanf(st.c_str(),"%lf",&latitude); 
            if(latitude > 90 ) return false;
            sscanf((st.c_str()+posNS+1),"%lf",&longitude);
            if(longitude > 180) return false;
            if((strchr(st.c_str(),'S')) || (strchr(st.c_str(),'s')))
                latitude*=-1;
            if((strchr(st.c_str(),'W')) || (strchr(st.c_str(),'w')))
                longitude*=-1;
        }
        else
        {
            sscanf(st.c_str(),"%lf",&longitude);
            if(longitude > 180) return false;
            sscanf((st.c_str()+posEW+1),"%lf",&latitude);
            if(latitude > 90 ) return false;
            if((strchr(st.c_str(),'S')) || (strchr(st.c_str(),'s')))
                latitude*=-1;
            if((strchr(st.c_str(),'W')) || (strchr(st.c_str(),'w')))
                longitude*=-1;
        }

        return true;
    }


 
    bool SearchGUI::_checkForDMS(QString &qstring,double &longitude,double &latitude)
    {

        std::string str=qstring.toStdString();
        if(str.empty())
            return false;

        int length=str.length();
        latitude=longitude=0;

        //Note :: Regular Expression are Very Sensitive to a single Character
        //Regular Expression for DMS "longitude latitude"

        QRegExp rx1("^((([0-8]{0,1}[0-9])(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\'{1,1}){0,1})(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\\"{1,1}){0,1})(\\040{1})([NSns]{1})){1,1})(\\040{1})((([0-9]{0,2}|1[0-7][0-9]|180)(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\'{1,1}){0,1})(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\\"{1,1}){0,1})(\\040{1})([EWew]{1})){1,1})$");

        //Regular Expression for DMS "latitude longitude" 
        QRegExp rx2("^((([0-9]{0,2}|1[0-7][0-9]|180)(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\'{1,1}){0,1})(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\\"{1,1}){0,1})(\\040{1})([EWew]{1})){1,1})(\\040{1})((([0-8]{0,1}[0-9])(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\'{1,1}){0,1})(\\040{1})(([1-5]{0,1}[0-9]{1,1}(\\040{0,})\\\"{1,1}){0,1})(\\040{1})([NSns]{1})){1,1})$");


        int pos1=rx1.indexIn(qstring);
        int pos2=rx2.indexIn(qstring);

        //This Regular expression is for the conditioni case user didn't give and direction , by default it would take as N + E
        QRegExp rx3("^(((\\d{1,3})(\\040{0,})((\\d{1,2}\\'{1,1}){0,1})(\\040{0,})((\\d{1,2}\\\"{1,1}){0,1})(\\040{0,})([EWew]{0,0})){1,1})(((\\d{1,3})(\\040{0,})((\\d{1,2}\\'{1,1}){0,1})(\\040{0,})((\\d{1,2}\\\"{1,1}){0,1})(\\040{0,})([EWew]{0,0})){1,1})$");

        int pos3=rx2.indexIn(qstring);

        // This is to validate whether user has given only digit such as only 42 <= Invalid
        QRegExp rx4("^((\\d{1,})(\\040{0,})((\\d{1,}\\'{1,1}){0,1})(\\040{0,})((\\d{1,}\\\"{1,1}){0,1})(\\040{0,}([NSEWnsew]{0,1})){1,1})$");  
        int pos4=rx4.indexIn(qstring);

        try
        {
            if( (( pos1 >= 0 )  || ( pos2 >= 0 ) ) && ( pos4 < 0 )) //Check for the valid expression type
            {
                for(int i=0,c=0;( (i<length) && (c<=2) );)
                {

                    //Extracting the Degree from DD MM SS formated user input
                    float degree=0;
                    while( (i<length) && (isdigit(str.at(i))) && ( str.at(i) != 32) )
                    {
                        int digit=(int)(str.at(i)) - 48 ;
                        degree = (degree*10) + digit;
                        i++;
                    }
                    while( (i<length) && (str.at(i) == 32)  ) i++;
                    while( (i<length) && (str.at(i) == ':') ) i++;  // DMS can also represent as DD:MM:SS but not used here 
                    while( (i<length) && (str.at(i) == 32) ) i++;

                    int iter=i,digit;
                    bool valid;

                    iter=i;
                    digit=0;
                    valid = false;

                    while(iter < length) // Checking whether user has entered the minute value
                    {
                        if(((str.at(iter) == ':') || (str.at(iter) == '\''))  && (digit))
                        {
                            valid=true;
                            iter++;
                            break;
                        }
                        else if(isdigit(str.at(iter)))
                        {
                            if(!digit)
                            {
                                while(isdigit(str.at(iter)))
                                {
                                    digit=(digit*10)+ ((int)(str.at(iter)) - 48);
                                    iter++;
                                }
                            }
                            else
                            {
                                valid=false;
                                break;
                            }
                        }
                        else
                        {

                            if(isspace(str.at(iter)))
                                while((iter<length) && (isspace(str.at(++iter))) ) ;
                            else 
                                break;
                        }
                    }

                    float min=0;
                    if(valid)   //If minutes value is present set it
                    {
                        min=digit;
                        i=iter;
                        while( (i<length) && (str.at(i) == 32) ) i++;
                    }


                    iter=i;
                    digit=0;
                    valid = false;

                    while(iter < length) // Checking whether user has entered the second value 
                    {
                        if( (isalpha(str.at(iter)) || (str.at(iter) == '"')) && (digit))
                        {
                            valid =true;
                            iter++;
                            break;
                        }
                        if(isdigit(str.at(iter)))
                        {
                            if(!digit)
                            {
                                while((iter < length) && (isdigit(str.at(iter))) )
                                {
                                    digit=(digit*10)+ ((int)(str.at(iter)) - 48);
                                    iter++;
                                }
                            }
                            else
                            {
                                valid=false;
                                break;
                            }
                        }
                        else
                        {
                            if(isspace(str.at(iter)))
                                while((iter<length) && (isspace(str.at(++iter))) ); 
                            else
                                break;
                        }
                    }

                    float sec=0; 
                    if(valid)  //If the Second value is present set it
                    {
                        sec=digit;
                        i=iter;
                        while( (i < length) && (str.at(i) =='"') )   i++;
                    }


                    degree += ( ( min + (sec/60) ) /60 );  // Calculating the DMS to it's decimal equivalent

                    while( (i<length)&&(str.at(i) == 32) )  i++;

                    c++;
                    if(c == 1) // Assigning the value to the First expression Eg: 40 26'21"E 19 58'N ;  40 26'21"E <= 1st Epxresion ,   19 58'N <=2nd expression
                    {

                        if ((str.at(i) == 'N') || (str.at(i) == 'n')) { latitude=degree; i++; }
                        else if ((str.at(i) == 'S') || (str.at(i) == 's')) { latitude=degree*-1; i++; }
                        else if ((str.at(i) == 'E') || (str.at(i) == 'e')) { longitude=degree; i++; }
                        else if ((str.at(i) == 'W') || (str.at(i) == 'w')) { longitude=degree*-1; i++; }
                        else  latitude = degree;
                    }
                    else if(c==2)  // Assigning the value to the second expression 
                    {
                        if(i < length) // we are not allowed to access any array element which is not in it
                        {
                            if ((str.at(i) == 'N') || (str.at(i) == 'n'))
                            {
                                if(latitude) longitude=latitude ;
                                latitude=degree; 
                            }
                            else if ((str.at(i) == 'S') || (str.at(i) == 's'))
                            { 
                                if(latitude) longitude=latitude ;
                                latitude=degree*-1; 
                            }
                            else if ((str.at(i) == 'E') || (str.at(i) == 'e')) longitude=degree;
                            else if ((str.at(i) == 'W') || (str.at(i) == 'w')) longitude=degree*-1;
                            i++;
                        }
                        else
                        {
                            if(longitude) latitude=degree;
                            else

                                longitude=degree;
                        }

                    }
                    while( (i<length) && (str.at(i) == 32) )   i++;
                }
                if( ((longitude <= 180) && (longitude >= -180))  && ((latitude <= 90) && (latitude >=-90)) ) //Second validation b4 returning
                    return true;
                else
                    return false;
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
        return false;
    }

    void SearchGUI::checkIndianGR(std::string str)
    {
     osg::Vec2d latLong = UTIL::IndianGRUtil::convertGRToLatLong(str);
     if (latLong.x() != -1 && latLong.y() != -1)
     {
         _placesList.prepend(new SearchObj(QString::fromStdString(UTIL::IndianGRUtil::convertedGRString), latLong.x(), latLong.y()));
     }
    }
    SearchGUI::~SearchGUI(){}

} //End of namespace 
