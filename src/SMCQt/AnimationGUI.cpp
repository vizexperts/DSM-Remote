/****************************************************************************
*
* File             : AnimationGUI.cpp
* Description      : AnimationGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/AnimationGUI.h>
#include <SMCQt/IconObject.h>

#include <App/AccessElementUtils.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/CoreRegistry.h>
#include <Core/IAnimationComponent.h>
#include <Core/ITickMessage.h>
#include <Core/AttributeTypes.h>
#include <Core/IVisibility.h>

#include <Elements/ElementsUtils.h>

#include <Terrain/IElevationObject.h>
#include <Terrain/TerrainPlugin.h>
#include <Terrain/ITimedRasterObject.h>

#include <osgDB/FileNameUtils>
#include <VizQt/QtUtils.h>

#include <Util/GdalUtils.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/algorithm/string.hpp>
#include <QtWidgets/QFileDialog>
#include <QString>
#include <libjson.h>
#include <SMCQt/VizComboBoxElement.h>
namespace fs = boost::filesystem;

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, AnimationGUI);
    DEFINE_IREFERENCED(AnimationGUI, DeclarativeFileGUI);
    const std::string AnimationGUI::AnimationGUIPopup = "AnimationGUI";
    const std::string AnimationGUI::AddAnimationGUIPopup = "AddAnimationGUI";
    const std::string AnimationGUI::DEFAULT_RAMP_FILE = "../../config/ramp.txt";
    AnimationGUI::AnimationGUI()
    {
        animationGUIobj = _findChild(AnimationGUIPopup);
        
        if (animationGUIobj)
        {
            baseChangeTime  = animationGUIobj->property("baseSpeed").toDouble();
            double factor = animationGUIobj->property("speedFactor").toDouble();
            changeTime = baseChangeTime / factor;
        }
    }

    void AnimationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            timeCount++;
            if (timeCount > changeTime)
            {
                timeCount = 0;
                int listSize = _animationList.size();
                if (listSize != 0 && currentIndex%listSize >= 0)
                {
                    if (currentIndex == 0)
                    {
                        toggleVisibility(currentIndex);
                    }
                    else
                    {
                        toggleVisibility((currentIndex)%listSize);
                        toggleVisibility((currentIndex-1)%listSize);
                    }
                    currentIndex++;
                    if (!loop && currentIndex%listSize == 0)
                    {
                        stopAnimation();
                    }
                }

            }
        }
    }
    AnimationGUI::~AnimationGUI()
    {

    }
    void AnimationGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild("smpFileMenu");
        if (popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void AnimationGUI::populateSavedAnimationFiles()
    {
        // write map to a json file 
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return;
        }
        fs::path destDir = fs::path(wmain->getProjectFile()).parent_path() / "Animations";
        std::vector<std::string> jsonFiles;
        std::vector<std::string> exts({ "json" });
        UTIL::getFilesFromFolder(destDir.string(), jsonFiles, exts, false);

        _savedAnimationFiles.clear();
        _savedAnimationFiles.append(new VizComboBoxElement("select", "0"));
        for (auto& jsonFile : jsonFiles)
        {
            std::string name = fs::path(jsonFile).filename().stem().string();
            _savedAnimationFiles.append(new VizComboBoxElement(name.c_str(), jsonFile.c_str()));
        }
        _setContextProperty("savedAnimationFiles", QVariant::fromValue(_savedAnimationFiles));
    }

    void AnimationGUI::populateColorRampFiles()
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        fs::path ColorRampDir = fs::path(appdataPath) / "ColorRamps";
        std::vector<std::string> rampFiles;
        std::vector<std::string> exts({ "txt" });
        UTIL::getFilesFromFolder(ColorRampDir.string(), rampFiles, exts, false);

        _rampFiles.clear();
        _rampFiles.append(new VizComboBoxElement("DEFAULT", DEFAULT_RAMP_FILE.c_str()));
        for (auto& txtFile : rampFiles)
        {
            std::string name = fs::path(txtFile).filename().stem().string();
            _rampFiles.append(new VizComboBoxElement(name.c_str(), txtFile.c_str()));
        }
        _setContextProperty("rampFiles", QVariant::fromValue(_rampFiles));
    }

    void AnimationGUI::connectPopupLoader(QString type)
    {
        if (QString::compare(type, QString(AddAnimationGUIPopup.c_str())) == 0)
        {
            addAnimationGUIobj = _findChild(AddAnimationGUIPopup);
            if (addAnimationGUIobj)
            {
                connect(addAnimationGUIobj, SIGNAL(browseButtonClicked()), this, SLOT(browseButtonClicked()));
                //connect(addAnimationGUIobj, SIGNAL(addAnimation(QString, QString)), this, SLOT(addAnimation(QString, QString)));
            }
        }
        if (QString::compare(type, QString(AnimationGUIPopup.c_str())) == 0)
        {
            animationGUIobj = _findChild(AnimationGUIPopup);
            if (animationGUIobj)
            {
                _availableList.clear();
                _animationList.clear();
                //Get the objectMap form World
                vizCore::RefPtr<vizCore::IWorldMaintainer> worldMaintainer = vizCore::WorldMaintainer::instance();
                const vizCore::WorldMap& worldMap = worldMaintainer->getWorldMap();
                CORE::RefPtr<CORE::IWorld> iworld = NULL;
                if (worldMap.size() > 0)
                {
                    iworld = worldMap.begin()->second;
                }
                const CORE::ObjectMap& omap = iworld->getObjectMap();
                for (CORE::ObjectMap::const_iterator citer = omap.begin();
                    citer != omap.end();
                    ++citer)
                {
                    CORE::RefPtr<CORE::IObject> object = citer->second;
                    std::string name = object->getInterface<CORE::IBase>()->getName();
                    name = osgDB::getNameLessExtension(name);
                    if (object->getInterface<TERRAIN::IRasterObject>() 
                        || object->getInterface<TERRAIN::IElevationObject>())
                    {
                        std::string url = getURLFromIObject(object);
                        if (!url.empty())
                        {
                            std::string ext = osgDB::getFileExtension(url);
                            if (!boost::iequals(ext, "tms")
                                && !boost::iequals(name, "base"))
                            {
                                CORE::RefPtr<CORE::IBase> ibase = object->getInterface<CORE::IBase>();
                                if (ibase)
                                {
                                    _rasterObjMap[ibase->getName()] = object;
                                }
                            }
                        }
                    }
                }
                for (auto& it = _rasterObjMap.begin();
                    it != _rasterObjMap.end();
                    it++)
                {
                    _availableList.append(QString::fromStdString(it->first));
                }
                _setContextProperty("availableList", QVariant::fromValue(_availableList));
                _setContextProperty("animationList", QVariant::fromValue(_animationList));
                populateSavedAnimationFiles();
                populateColorRampFiles();
                connect(animationGUIobj, SIGNAL(addToAnimationList(int)), this, SLOT(addToAnimationList(int)));
                connect(animationGUIobj, SIGNAL(removeFromAnimationList(int)), this, SLOT(removeFromAnimationList(int)));
                connect(animationGUIobj, SIGNAL(moveInAnimationList(int, int)), this, SLOT(moveInAnimationList(int, int)));
                connect(animationGUIobj, SIGNAL(saveAnimation(QString)), this, SLOT(saveAnimation(QString)));
                connect(animationGUIobj, SIGNAL(playAnimation()), this, SLOT(playAnimation()));
                connect(animationGUIobj, SIGNAL(pauseAnimation()), this, SLOT(pauseAnimation()));
                connect(animationGUIobj, SIGNAL(stopAnimation()), this, SLOT(stopAnimation()));
                connect(animationGUIobj, SIGNAL(loopAnimation(bool)), this, SLOT(loopAnimation(bool)));
                connect(animationGUIobj, SIGNAL(changeAnimationSpeed(double)), this, SLOT(changeAnimationSpeed(double)));
                connect(animationGUIobj, SIGNAL(loadAnimation(QString)), this, SLOT(loadAnimation(QString)));
                connect(animationGUIobj, SIGNAL(addToRampFileList(QString)), this, SLOT(addToRampFileList(QString)));
            }
        }
    }
    void AnimationGUI::addToRampFileList(QString file)
    {
        fs::path from(file.toStdString());
        fs::path to = fs::path(UTIL::TemporaryFolder::instance()->getAppFolderPath()) / "ColorRamps"/ from.filename();        
        int i = 1;
        while (fs::exists(to))
        {
            to = to.parent_path() / (from.stem().string() + "_" + std::to_string(i++) + to.extension().string());
        }
        try
        {
            if (fs::exists(from))LOG_INFO("from exists");
            if (! fs::exists(from))LOG_INFO("to does not exists ");
            fs::copy_file(from, to);
        }
        catch (const fs::filesystem_error& e)
        {
            LOG_ERROR(e.what());
        }

        QString name = QString::fromStdString(to.stem().string());
        QString uid = file;
        _rampFiles.append(new VizComboBoxElement(name, uid));
        
        QObject* rampFilecomboBox = animationGUIobj->findChild<QObject*>("rampFilesComboBoxObject");
        if (rampFilecomboBox)
        {
            QMetaObject::invokeMethod(rampFilecomboBox, "selectOptionPrivate", Q_ARG(QString, name), Q_ARG(QString, uid));
        }
    }
    void AnimationGUI::browseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString filters("Raster File (*.tif *.tiff)");
        QString caption ( "Animation Files");
        QStringList filenames = QFileDialog::getOpenFileNames(parent, caption, directory, filters, 0);
        QString files = filenames.join(";");
        if (!files.isEmpty())
        {
            addAnimationGUIobj->setProperty("selectedFileName", QVariant::fromValue(files));
        }
    }
    /*void AnimationGUI::addAnimation(QString animationName, QString filename)
    {
        std::string filenameSTR = filename.toStdString();
        std::vector<std::string> filenames;
        boost::split(filenames, filenameSTR, boost::is_any_of(";"));

        CORE::RefPtr<CORE::IObject> iobj = 
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::TimedRasterObjectType);
        CORE::RefPtr<CORE::IBase> ibase = iobj->getInterface<CORE::IBase>();
        if (ibase)
        {
            ibase->setName(animationName.toStdString());
        }
        CORE::RefPtr<TERRAIN::ITimedRasterObject> timedRaster = iobj->getInterface<TERRAIN::ITimedRasterObject>();
        if (timedRaster)
        {
            bool checked = addAnimationGUIobj->property("generateColoredRelief").toBool();
            timedRaster->setGenerateColoredRelief(checked);
            timedRaster->setColorRampFile(rampFile);
            timedRaster->addRasters(filenames);
        }
        
        CORE::RefPtr<CORE::IWorld> world = ELEMENTS::GetFirstWorldFromMaintainer();
        world->addObject(iobj.get());
    }*/

    void AnimationGUI::addToAnimationList(int index)
    {
        animationGUIobj = _findChild(AnimationGUIPopup);
        if (animationGUIobj)
        {
            if (index != -1 && !_availableList.empty())
            {
                QString layerName = _availableList.at(index);
                _availableList.removeAt(index);
                _animationList.append(layerName);
            }
            _setContextProperty("availableList", QVariant::fromValue(_availableList));
            _setContextProperty("animationList", QVariant::fromValue(_animationList));
        }
    }

    void AnimationGUI::removeFromAnimationList(int index)
    {
        animationGUIobj = _findChild(AnimationGUIPopup);
        if (animationGUIobj)
        {
            if (index != -1)
            {
                QString layerName = _animationList.at(index);
                _animationList.removeAt(index);
                _availableList.append(layerName);
            }
            _setContextProperty("availableList", QVariant::fromValue(_availableList));
            _setContextProperty("animationList", QVariant::fromValue(_animationList));
        }
    }

    void AnimationGUI::moveInAnimationList(int from, int to)
    {
        animationGUIobj = _findChild(AnimationGUIPopup);
        if (animationGUIobj)
        {
            if (from != -1 && to != -1)
            {
                _animationList.move(from, to);
            }
            _setContextProperty("animationList", QVariant::fromValue(_animationList));
        }
    }

    void AnimationGUI::playAnimation()
    {

        //if playing for the first time. Not resuming a previously paused animation
        if (!resume)
        {
            for (auto& layerName : _animationList)
            {
                std::string name = layerName.toStdString();
            
                CORE::RefPtr<CORE::IObject> raster = _rasterObjMap[name];
                CORE::RefPtr<CORE::IVisibility> rasterVisibility = raster->getInterface<CORE::IVisibility>();
                if (rasterVisibility)
                {
                    rasterVisibility->setVisibility(false);
                }
            }
        // set the starting index 
            currentIndex = 0;
        }
        
        //jump to object 
        
        //subscribe to tick message
        CORE::RefPtr<CORE::IWorldMaintainer> wmain =
            APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

        if (wmain.valid())
        {
            _subscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);
        }
    }

    void AnimationGUI::pauseAnimation()
    {
        //unsubscribe to tick message
        CORE::RefPtr<CORE::IWorldMaintainer> wmain =
            APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

        if (wmain.valid())
        {
            _unsubscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);
        }

        //set resume capabilities
        resume = true;
    }
    void AnimationGUI::stopAnimation()
    {
        //Reset the visibility of layers.
        for (auto& layerName : _animationList)
        {
            std::string name = layerName.toStdString();

            CORE::RefPtr<CORE::IObject> raster = _rasterObjMap[name];
            CORE::RefPtr<CORE::IVisibility> rasterVisibility = raster->getInterface<CORE::IVisibility>();
            if (rasterVisibility)
            {
                rasterVisibility->setVisibility(true);
            }
        }
        
        //unsubscribe to tick message
        CORE::RefPtr<CORE::IWorldMaintainer> wmain =
            APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());

        if (wmain.valid())
        {
            _unsubscribe(wmain.get(), *CORE::IWorldMaintainer::TickMessageType);
        }

        //set resume capabilities
        resume = false;
        if (animationGUIobj)
        {
            animationGUIobj->setProperty("animationRunning", QVariant::fromValue(false));
        }

    }

    void AnimationGUI::loopAnimation(bool checked)
    {
        loop = checked;
    }
    
    void AnimationGUI::changeAnimationSpeed(double factor)
    {
        if (animationGUIobj)
        {
            double factor = animationGUIobj->property("speedFactor").toDouble();
            if (factor > 0)
                changeTime = baseChangeTime / factor;
            else if (factor < 0)
                changeTime = baseChangeTime * std::abs(factor);
            else
                changeTime = baseChangeTime; 
        }
    }

    void AnimationGUI::toggleVisibility(int index)
    {
        CORE::RefPtr<CORE::IObject> raster = _rasterObjMap[_animationList[index].toStdString()];
        CORE::RefPtr<CORE::IVisibility> rasterVisibilty = raster->getInterface<CORE::IVisibility>();
        if (rasterVisibilty)
        {
            if (rasterVisibilty->getVisibility())
            {
                rasterVisibilty->setVisibility(false);
            }
            else
            {
                rasterVisibilty->setVisibility(true);
            }
        }
    }

    void AnimationGUI::genColorRelief(std::map<std::string,std::string> & filemap, std::string rampFile)
    {
        std::string prefix = "";
        QList<QObject*>::iterator it = _rampFiles.begin();
        for (; it != _rampFiles.end(); it++)
        {
            VizComboBoxElement* e = (VizComboBoxElement*)(*it);
            if (e)
            {
                if (e->uID().toStdString() == rampFile)
                {
                    prefix = e->name().toStdString() + "_";
                }
            }
        }

        //generate color_<layername> color relief file in the project directory
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return;
        }
        for (auto& layername : _animationList)
        {
            CORE::RefPtr<CORE::IObject> object = _rasterObjMap[layername.toStdString()];
            std::string url = getURLFromIObject(object);
            if (!url.empty())
            {
                fs::path destDir = fs::path(wmain->getProjectFile()).parent_path() / "ColorRelief";
                
                std::string destFileName =  prefix + fs::path(url).filename().string();
                fs::path destFile = destDir / destFileName;
                fs::create_directories(destFile.parent_path());
                UTIL::GDALUtils::genColorRelief(url, destFile.string(), rampFile);
                filemap[destFileName] = destFile.string();
            }
        }        
    }

    bool AnimationGUI::_writeMapToFile(std::string jsonfile, std::map <std::string, std::string> filemap)
    {
        if (fs::exists(jsonfile) && fs::is_regular_file(jsonfile) && !UTIL::deleteFile(jsonfile))
        {
            LOG_ERROR("Cannot delete file : " + jsonfile);
            return false;
        }
        
        fs::create_directories(fs::path(jsonfile).parent_path());

        JSONNode mainNode(JSON_NODE);
        std::map<std::string, std::string>::const_iterator it(filemap.begin());
        for (; it != filemap.end(); ++it)
        {
            mainNode.push_back(JSONNode(it->first, it->second));
        }

        std::string value = mainNode.write_formatted();

        std::ofstream out(jsonfile.c_str());
        out << value;
        out.close();
        return true;
    }

    void AnimationGUI::saveAnimation(QString name)
    {
        bool generateColor = animationGUIobj->property("generateColor").toBool();
        if (generateColor)
        {
            LOG_INFO("Generating Color Files");
            QString rampFile = animationGUIobj->property("rampFile").toString();
            LOG_INFO("RampFile: " + rampFile.toStdString());
            std::map<std::string, std::string> colorFiles;
            genColorRelief(colorFiles, rampFile.toStdString());
            for (auto& layer : _animationList)
            {
                _availableList.append(layer);
            }
            _animationList.clear();
            std::map<std::string, std::string>::iterator it = colorFiles.begin();
            for (; it != colorFiles.end(); it++)
            {
                if (fs::exists(it->second) && fs::is_regular_file(it->second))
                {
                    addRasterToWorld(it->first, it->second);
                    _animationList.append(QString::fromStdString(it->first));
                }
            }
        }
        //create a map for layer name to filename 
        std::map<std::string, std::string> nameToFileMap;
        for (auto& layer : _animationList)
        {
            CORE::RefPtr<CORE::IObject> object = _rasterObjMap[layer.toStdString()];
            if (object)
            {
                std::string url = getURLFromIObject(object);
                if (!url.empty())
                    nameToFileMap[layer.toStdString()] = url;
            }
        }

        // write map to a json file 
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = CORE::WorldMaintainer::instance();
        if (!wmain)
        {
            LOG_ERROR("World Maintainer Not found");
            return;
        }
        fs::path destDir = fs::path(wmain->getProjectFile()).parent_path() / "Animations";
        fs::create_directories(destDir);
        std::string destFileName = fs::path ( fs::path(destDir) / (name.toStdString() + ".json")).string();
        _writeMapToFile(destFileName, nameToFileMap);

        _savedAnimationFiles.append(
            new VizComboBoxElement(QString::fromStdString(fs::path(destFileName).stem().string()),
                QString::fromStdString(destFileName)));
        _setContextProperty("savedAnimationFiles", QVariant::fromValue(_savedAnimationFiles));
        _setContextProperty("animationList", QVariant::fromValue(_animationList));
        _setContextProperty("availableList", QVariant::fromValue(_availableList));
    }   

    bool AnimationGUI::readFileToMap(std::string jsonFile, std::map<std::string, std::string>& filemap)
    {
        if (fs::exists(jsonFile) && fs::is_regular_file(jsonFile))
        {
            JSONNode mainNode(JSON_NODE);
            osgDB::ifstream inFile(jsonFile.c_str());
            //read content from file into a string stream 
            std::ostringstream buffer;
            buffer << inFile.rdbuf();
            //parse json file 
            try
            {
                mainNode = libjson::parse(buffer.str());
                for (JSONNode::iterator it = mainNode.begin();
                    it != mainNode.end();
                    it++)
                {
                    std::string name = it->name();
                    std::string value = it->as_string();
                    filemap[name] = value;
                }
                return true;
            }
            catch (std::invalid_argument& ex)
            {
                //throw exception
                LOG_ERROR(ex.what());
                return false;
            }
        }
        return false;
    }

    void AnimationGUI::addRasterToWorld(std::string name , std::string rasterFile)
    {
        //create Object and add to World;
        CORE::RefPtr<CORE::IObject> iobj =
            CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
        if (iobj)
        {
            CORE::RefPtr<TERRAIN::IRasterObject> rasterobj = iobj->getInterface<TERRAIN::IRasterObject>();
            if (rasterobj)
            {
                rasterobj->setURL(rasterFile);
            }
            CORE::RefPtr<CORE::IBase> ibase = iobj->getInterface<CORE::IBase>();
            if (ibase)
            {
                ibase->setName(name);
            }
            CORE::RefPtr<CORE::IWorld> world = ELEMENTS::GetFirstWorldFromMaintainer();
            world->addObject(iobj);
            _rasterObjMap[name] = rasterobj->getInterface<CORE::IObject>();
        }
        
    }
    void AnimationGUI::loadAnimation(QString filepath)
    {
        for (auto& layer : _animationList)
        {
            _availableList.append(layer);
        }
        _animationList.clear();

        std::map<std::string, std::string > filemap;
        readFileToMap(filepath.toStdString(), filemap);
        std::map<std::string, std::string >::iterator it = filemap.begin();
        for (; it != filemap.end();it++)
        {
            std::string name = it->first;
            std::string filepath = it->second;
            if (_rasterObjMap.find(name) != _rasterObjMap.end())
            {
                //Means already Loaded in the world
                _availableList.removeOne(QString::fromStdString(name));                
            }
            else
            {
                if (fs::exists(filepath) && fs::is_regular_file(filepath))
                    addRasterToWorld(name, filepath);
            }
            _animationList.append(QString::fromStdString(name));
        }
        _setContextProperty("animationList", QVariant::fromValue(_animationList));
        _setContextProperty("availableList", QVariant::fromValue(_availableList));
    }

    std::string AnimationGUI::getURLFromIObject(CORE::RefPtr<CORE::IObject> object)
    {
        CORE::RefPtr<TERRAIN::IRasterObject> rasterObj = object->getInterface<TERRAIN::IRasterObject>();
        CORE::RefPtr<TERRAIN::IElevationObject> elevationObj = object->getInterface<TERRAIN::IElevationObject>();
        std::string url = "";
        if (rasterObj)
        {
            url = rasterObj->getURL();
        }
        else
        {
            url = elevationObj->getURL();
        }
        return url;
    }
} // namespace SMCQt
