/******************************************************s**********************
*
* File             : AlmanacGUI.cpp
* Description      : AlmanacGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/AlmanacGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Core/IPoint.h>
#include <Core/IDeletable.h>
#include <Core/ICompositeObject.h>

#include <VizQt/QtUtils.h>

#include <Util/ValidatorUtils.h>
#include <Util/StringUtils.h>
#include <Util/FileUtils.h>

//! C++ related Header's
#include <iostream>

#include <QtCore/QVariant>
#include <QtCore/QString>
#include <osgDB/FileNameUtils>

Q_DECLARE_METATYPE(QList<SMCQt::VizComboBoxElement*>)

namespace SMCQt
{
    const std::string AlmanacGUI::TFLongitude           = "longitude";
    const std::string AlmanacGUI::TFLatitude            = "latitude";
    const std::string AlmanacGUI::TFMoonrise            = "moonRiseTime";
    const std::string AlmanacGUI::TFMoonset             = "moonSetTime";
    const std::string AlmanacGUI::TFSunrise             = "sunRiseTime";
    const std::string AlmanacGUI::TFSunset              = "sunSetTime";
    const std::string AlmanacGUI::DTAlmanac             = "dateAndTime";
    const std::string AlmanacGUI::TFMoonPhase           = "moonPhase";
    const std::string AlmanacGUI::CBTimeZoneList        = "timeZonesList";
    const std::string AlmanacGUI::IMGMoonPhaseViewer    = "moonPhaseImage";
    const std::string AlmanacGUI::AlmanacAnalysisPopup  = "almanacAnalysis";

    //XXX AG use proper indentation
    int AlmanacGUI::count = 0;

    DEFINE_META_BASE(SMCQt, AlmanacGUI);
    DEFINE_IREFERENCED(AlmanacGUI, DeclarativeFileGUI);


    AlmanacGUI::AlmanacGUI() : _pointUIHandler(NULL)
        , _almanacUIHandler(NULL)
        , _isSubscribed(false)
    {
        _initializeMoonPhaseImageDB();
        _initializeTimeZoneList();
        _gmtTime = (5 + (30/ 60.0));
    }

    AlmanacGUI::~AlmanacGUI()
    {
    }

    void AlmanacGUI::_initializeMoonPhaseImageDB()
    {
        _moonPhaseDatabase.clear();
        _moonPhaseDatabase.push_back("Frame0.png");
        _moonPhaseDatabase.push_back("Frame1.png");
        _moonPhaseDatabase.push_back("Frame2.png");
        _moonPhaseDatabase.push_back("Frame24.png");
        _moonPhaseDatabase.push_back("Frame3.png");
        _moonPhaseDatabase.push_back("Frame23.png");
        _moonPhaseDatabase.push_back("Frame4.png");
        _moonPhaseDatabase.push_back("Frame22.png");
        _moonPhaseDatabase.push_back("Frame5.png");
        _moonPhaseDatabase.push_back("Frame21.png");
        _moonPhaseDatabase.push_back("Frame6.png");
        _moonPhaseDatabase.push_back("Frame20.png");
        _moonPhaseDatabase.push_back("Frame7.png");
        _moonPhaseDatabase.push_back("Frame19.png");
        _moonPhaseDatabase.push_back("Frame8.png");
        _moonPhaseDatabase.push_back("Frame18.png");
        _moonPhaseDatabase.push_back("Frame9.png");
        _moonPhaseDatabase.push_back("Frame17.png");
        _moonPhaseDatabase.push_back("Frame10.png");
        _moonPhaseDatabase.push_back("Frame16.png");
        _moonPhaseDatabase.push_back("Frame11.png");
        _moonPhaseDatabase.push_back("Frame15.png");
        _moonPhaseDatabase.push_back("Frame11.png");
        _moonPhaseDatabase.push_back("Frame15.png");
        _moonPhaseDatabase.push_back("Frame13.png");
        _moonPhaseDatabase.push_back("Frame13.png");
        _moonPhaseDatabase.push_back("Frame13.png");
        _moonPhaseDatabase.push_back("Frame12.png");
        _moonPhaseDatabase.push_back("Frame14.png");
        _moonPhaseDatabase.push_back("Frame12.png");
        _moonPhaseDatabase.push_back("Frame14.png");
        _moonPhaseDatabase.push_back("Frame14.png");
        _moonPhaseDatabase.push_back("Frame14.png");
        _moonPhaseDatabase.push_back("Frame14.png");
        _moonPhaseDatabase.push_back("Frame14.png");
    }

    void AlmanacGUI::_initializeTimeZoneList()
    {
        //qDeleteAll(_timeZonesList.begin(), _timeZonesList.end()); //No need to call qDeleteAll will clear in clear funtion
        _timeZonesList.clear();

        _timeZonesList.append(new VizComboBoxElement("(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi","00"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-12:00) International Date Line West","01"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-11:00) Midway Island, Samoa","02"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-10:00) Hawaii","03"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-09:00) Alaska","04"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-08:00) Pacific Time (US and Canada); Tijuana","05"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-07:00) Mountain Time (US and Canada)","06"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-07:00) Chihuahua, La Paz, Mazatlan","07"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-07:00) Arizona","08"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-06:00) Central Time (US and Canada","09"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-06:00) Saskatchewan","10"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-06:00) Guadalajara, Mexico City, Monterrey","11"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-06:00) Central America","12"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-05:00) Eastern Time (US and Canada)","13"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-05:00) Indiana (East)","14"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-05:00) Bogota, Lima, Quito","15"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-04:00) Atlantic Time (Canada)","16"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-04:00) Caracas, La Paz","17"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-04:00) Santiago","18"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-03:30) Newfoundland and Labrador","19"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-03:00) Brasilia","20"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-03:00) Buenos Aires, Georgetown","21"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-03:00) Greenland","22"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-02:00) Mid-Atlantic","23"));
        _timeZonesList.append(new VizComboBoxElement("(GMT-01:00) Azores","24"));
        _timeZonesList.append(new VizComboBoxElement("(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London","25"));
        _timeZonesList.append(new VizComboBoxElement("(GMT) Casablanca, Monrovia","26"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb","27"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+01:00) Brussels, Copenhagen, Madrid, Paris","28"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vien","29"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+01:00) West Central Africa","30"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+02:00) Bucharest","31"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+02:00) Cairo","32"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+02:00) Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius","33"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+02:00) Athens, Istanbul, Minsk","34"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+02:00) Jerusalem","35"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+02:00) Harare, Pretoria","36"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+03:00) Moscow, St. Petersburg, Volgograd","37"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+03:00) Kuwait, Riyadh","38"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+03:00) Baghdad","39"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+03:30) Tehran","40"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+04:00) Abu Dhabi, Muscat","41"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+04:00) Baku, Tbilisi, Yerevan","42"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+04:30) Kabul","43"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+05:00) Ekaterinburg","44"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+05:00) Islamabad, Karachi, Tashkent","45"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+05:45) Kathmandu","46"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+06:00) Astana, Dhaka","47"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+06:00) Sri Jayawardenepura","48"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+06:00) Almaty, Novosibirsk","49"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+06:30) Yangon Rangoon","50"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+07:00) Bangkok, Hanoi, Jakarta","51"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+08:00) Beijing, Chongqing, Hong Kong SAR, Urumqi","52"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+08:00) Kuala Lumpur, Singapore","53"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+08:00) Taipei","54"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+08:00) Perth","55"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+09:00) Seoul","56"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+09:00) Osaka, Sapporo, Tokyo","57"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+09:00) Yakutsk","58"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+09:30) Darwin","59"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+09:30) Adelaide","60"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+10:00) Canberra, Melbourne, Sydney","61"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+10:00) Brisbane","62"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+10:00) Hobart","63"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+10:00) Guam, Port Moresby","64"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+11:00) Magadan, Solomon Islands, New Caledonia","65"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+12:00) Fiji Islands, Kamchatka, Marshall Islands","66"));
        _timeZonesList.append(new VizComboBoxElement("(GMT+12:00) Auckland, Wellington","67"));

    }


    void AlmanacGUI::setActive(bool value)
    {
        if(value)
        {
            QObject* almanacAnalysis = _findChild(AlmanacAnalysisPopup);

            connect( almanacAnalysis, SIGNAL(markPosition(bool)),this, SLOT(_handleMarkButtonClicked(bool)));

            // Invoke value change function when the latitude, longitude or date/time values change
            connect(almanacAnalysis, SIGNAL(positionEdited()),this, SLOT(_handlePositionChangedManually()));

            // Monitoring changes made to date time
            connect(almanacAnalysis, SIGNAL(dateTimeChanged(const QDateTime&)),this, SLOT(_handleDateTimeChanged(const QDateTime&)));
            connect(almanacAnalysis, SIGNAL(timeZoneChanged(QString)),this, SLOT(_handleTimeZoneChanged(QString)));

            _setContextProperty(CBTimeZoneList.c_str(), QVariant::fromValue(_timeZonesList));
        }
        else
        {
            _handleMarkButtonClicked(false);

            if(_point.valid())
            {
                _removeObject(_point.get());
                _point = NULL;
            }

            _setContextProperty(CBTimeZoneList.c_str(), QVariant::fromValue(NULL));
        }

        DeclarativeFileGUI::setActive(value);
    }

    void AlmanacGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the UIHandlers 
            try
            {
                _pointUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IPointUIHandler>(getGUIManager());

                // Query the almanac UIHandler
                _almanacUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAlmanacUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Handle if a new point is selected
        else if(messageType == *VizUI::IPointUIHandler::PointCreatedMessageType)
        {
            //remove the previously added point
            if(_point.valid())
            {
                _removeObject(_point.get());
                _point = NULL;
            }

            //! Update the location widget
            _point        = _pointUIHandler->getPoint();
            _longLat    = _point->getValue();

            _calculateAlmanac();

            //! set the point mode to point edit mode
            _pointUIHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_EDIT_POINT);
        }
        else if(messageType == *VizUI::IPointUIHandler::PointUpdatedMessageType)
        {
            _longLat = _point->getValue();

            _calculateAlmanac();
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    //! handler Time Zone Changed 
    void AlmanacGUI::_handleTimeZoneChanged(QString strInd)
    {
        //XXX AG inproper indentation
        //XXX AG Always give default vales to variables
        double gmtHours,gmtMinutes ;

        double gmtZoneSeconds = 0.0;

        //XXX Why cant use get the index as int. Why using QString for the index. QString seems to be content of the comboBoz
        //Rather than the index
        int index= strInd.toInt();
        QString currentTimeZone;

        QObject* almanacAnalysis = _findChild(AlmanacAnalysisPopup);
        if(!almanacAnalysis)
            return;

        if(_almanacUIHandler.valid())
        {
            if(almanacAnalysis->property(TFLongitude.c_str()).toString().isEmpty())
            {
                showError("Almanac", "Enter or Mark a Point");
                return;
            }

            if(almanacAnalysis->property(TFLatitude.c_str()).toString().isEmpty())
            {
                showError("Almanac", "Enter or Mark a Point");
                return;
            }

            _stringToTimeConversion( qobject_cast<VizComboBoxElement*>(_timeZonesList.at(index))->name().toStdString() ,gmtHours,gmtMinutes);
            _gmtTime = gmtHours + (gmtMinutes + gmtZoneSeconds)/60;
            _calculateAlmanac();
        }
    }

    //! string to Time Conversion 
    void AlmanacGUI::_stringToTimeConversion(const std::string& str, double& gmthours, double& gmtminutes)
    {
        bool negativeFlag = false;
        bool positiveFlag = false;
        int index = 0;
        std::string gmt;

        std::stringstream  stringStream;

        ///XXX AG why are you using char *
        char * writable = new char[str.size() + 1];
        std::copy(str.begin(), str.end(), writable);
        writable[str.size()] = '\0'; // don't forget the terminating 0

        while (writable[index] != '\0')
        {
            if( writable[index] == '(')
            {
                index++;
            }
            else if (writable[index] == '-')
            {
                gmt = stringStream.str();
                stringStream.str("");
                negativeFlag = true;
                index++;
            }
            else if(writable[index] == '+')
            {
                gmt = stringStream.str();
                stringStream.str("");
                positiveFlag = true;
                index++;
            }
            else if (writable[index] == ':')
            {
                if(negativeFlag)
                {
                    gmthours = -1 * UTIL::ToDouble(stringStream.str());
                }
                else if (positiveFlag)
                {
                    gmthours = UTIL::ToDouble(stringStream.str());
                }
                index++;
                stringStream.str("");
            }
            else if(writable[index] == ')')
            {
                if(negativeFlag || positiveFlag)
                {
                    gmtminutes = UTIL::ToDouble(stringStream.str());
                    index++;
                }
                else 
                {
                    gmthours = 0.0;
                    gmtminutes = 0.0;
                    index++;
                }
                break;
            }
            else
            {
                stringStream<<writable[index];
                index++;
            }
        } 
        // don't forget to free the string after finished using it
        delete[] writable;
    }


    void AlmanacGUI::_getTimeZone()
    {
        QDateTime dt1 = QDateTime::currentDateTime();

        QDateTime dt2 = dt1.toUTC();
        int utcOffset = dt1.utcOffset();
        dt1.setTimeSpec(Qt::UTC);

        int gmtZoneSeconds = dt2.secsTo(dt1);

        QChar sign = (gmtZoneSeconds >=0 ? QLatin1Char('+') : QLatin1Char('-'));

        if (gmtZoneSeconds < 0)
            gmtZoneSeconds = -gmtZoneSeconds;
        _gmtZoneMinutes = (gmtZoneSeconds % 3600) / 60;
        _gmtZoneHour = (gmtZoneSeconds / 3600);

        _gmtTime = _gmtZoneHour + (_gmtZoneMinutes + gmtZoneSeconds%60)/60 ;
    }

    void AlmanacGUI::_handleMarkButtonClicked( bool checked )
    {
        //! Get the checked state
        //! If checked then set the states of the pointUIHandler

        if(checked)
        {
            _pointUIHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_CREATE_POINT);
            _pointUIHandler->setTemporaryState(true);
            _pointUIHandler->setProcessMouseEvents(true);
            _pointUIHandler->setHandleMouseClicks(true);
            _pointUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);

            _subscribe(_pointUIHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);
            _subscribe(_pointUIHandler.get(), *VizUI::IPointUIHandler::PointUpdatedMessageType);

            _isSubscribed = true;
        }
        //! If unchecked then unset the states of the pointUIHandler
        else
        {
            _pointUIHandler->setMode(VizUI::IPointUIHandler::POINT_MODE_NONE);
            _pointUIHandler->setTemporaryState(false);
            _pointUIHandler->setProcessMouseEvents(false);
            _pointUIHandler->setHandleMouseClicks(false);
            _pointUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);

            _unsubscribe(_pointUIHandler.get(), *VizUI::IPointUIHandler::PointCreatedMessageType);
            _unsubscribe(_pointUIHandler.get(), *VizUI::IPointUIHandler::PointUpdatedMessageType);

            _isSubscribed = false;
        }

    }

    void AlmanacGUI::_handlePositionChangedManually()
    {
        QObject* almanacAnalysis = _findChild(AlmanacAnalysisPopup);
        if(!almanacAnalysis)
            return;

        _longLat.x() = almanacAnalysis->property(TFLongitude.c_str()).toString().toDouble() ;
        _longLat.y() = almanacAnalysis->property(TFLatitude.c_str()).toString().toDouble() ;

        if (UTIL::ValidatorUtils::IsValidLongitude(_longLat.x()) && UTIL::ValidatorUtils::IsValidLatitude(_longLat.y()))
        {
            ++count;
        }
        else
        {
            showError("Almanac", "Invalid Lat/Long value.");
            return;
        }

        //XXX AG why this is commented
        //! Minimum number of digits from 8 to 14 including decimal points  
        //if(count >= 8 && count <= 14)
        {
            _calculateAlmanac();
            count = 0;
        }
    }

    void AlmanacGUI::_handleDateTimeChanged(const QDateTime&)
    {
        QObject* almanacAnalysis = _findChild(AlmanacAnalysisPopup);
        if(!almanacAnalysis)
            return;

        if( almanacAnalysis->property(TFLongitude.c_str()).toString().isEmpty())
        {
            showError("Almanac", "Enter or Mark a Point");
            return;
        }

        if(almanacAnalysis->property(TFLatitude.c_str()).toString().isEmpty())
        {
            showError("Almanac", "Enter or Mark a Point");
            return;
        }

        // Setting today's date on the date-time box
        _calculateAlmanac();
    }

    void AlmanacGUI::_calculateAlmanac()
    {

        QObject* almanacAnalysis = _findChild(AlmanacAnalysisPopup);
        if(!almanacAnalysis)
            return;

        if(_almanacUIHandler.valid())
        {
            //! Set the Current Position 
            _almanacUIHandler->setPosition(_longLat);

            //! Get current Date and Time 
            QDateTime aldateTime = almanacAnalysis->property(DTAlmanac.c_str()).toDateTime() ;

            //! set Current Time
            _almanacUIHandler->setDateTime(VizQt::TimeUtils::QtToBoostDateTime(aldateTime));

            //! Set GMT time
            _almanacUIHandler->setGMTTime(_gmtTime);

            //! calculate SunRise/Set and Moon Rise/Set and MoooPhase at Given Point of Time
            _almanacUIHandler->computeAlmanac();

            //! get Calculated MoonRise/Set and Sun Rise/Set and MoonPhase Values
            _updateGUI(_almanacUIHandler->getAlmanacData());
        }
        else
        {
            return;
        }
    }

    void AlmanacGUI::_updateGUI(const SMCUI::AlmanacData&)
    {
        QObject* almanacAnalysis = _findChild(AlmanacAnalysisPopup);
        if(!almanacAnalysis)
            return;

        //! Update location
        almanacAnalysis->setProperty(TFLongitude.c_str(),QString::number(_longLat.x(), 'g', 8));
        almanacAnalysis->setProperty(TFLatitude.c_str(),QString::number(_longLat.y(), 'g', 8));

        //! Update date/time
        const SMCUI::AlmanacData& ad = _almanacUIHandler->getAlmanacData();
        QTime qtime;
        QString stringDateTime;

        //! Update moonrise time
        if(!ad.moonriseTime.is_not_a_date_time())
        {
            qtime = VizQt::TimeUtils::BoostToQtTime(ad.moonriseTime);
            stringDateTime = qtime.toString() + QString(" hours");
        }
        else
        {
            stringDateTime = "Invalid";
        }
        almanacAnalysis->setProperty(TFMoonrise.c_str(),stringDateTime);

        // Update moonset time
        if(!ad.moonsetTime.is_not_a_date_time())
        {
            qtime = VizQt::TimeUtils::BoostToQtTime(ad.moonsetTime);
            stringDateTime = qtime.toString() + QString(" hours");
        }
        else
        {
            stringDateTime = "Invalid";
        }
        almanacAnalysis->setProperty(TFMoonset.c_str(),stringDateTime);

        // Update sunrise time
        if(!ad.sunriseTime.is_not_a_date_time())
        {
            qtime = VizQt::TimeUtils::BoostToQtTime(ad.sunriseTime);
            stringDateTime = qtime.toString() + QString(" hours");
        }
        else
        {
            stringDateTime = "Invalid";
        }
        almanacAnalysis->setProperty(TFSunrise.c_str(),stringDateTime);


        // Update sunset time
        if(!ad.sunriseTime.is_not_a_date_time())
        {
            qtime = VizQt::TimeUtils::BoostToQtTime(ad.sunsetTime);
            stringDateTime = qtime.toString() + QString(" hours");
        }
        else
        {
            stringDateTime = "Invalid";
        }
        almanacAnalysis->setProperty(TFSunset.c_str(),stringDateTime);


        // Update moon phase
        QString foc = "Fraction of Cycle: ";
        foc += UTIL::ToString(ad.moonPhase * 100.0).c_str();
        foc += "%";
        almanacAnalysis->setProperty(TFMoonPhase.c_str(),foc);

        _setMoonPhaseImage(ad.moonPhase, ad.timeToNewMoon);
    }

    void AlmanacGUI::_setMoonPhaseImage(double moonPhase, unsigned int daysToNewMoon)
    {
        double moonphasePercentage; 
        moonphasePercentage = moonPhase * 100.0;
        unsigned int moonPhaseValue;
        moonPhaseValue = moonphasePercentage / 3;

        {
            std::string Path = UTIL::getDataDirPath() + "/MoonPhases/";

            if (daysToNewMoon < _moonPhaseDatabase.size())
            {
                Path.append(_moonPhaseDatabase.at(moonPhaseValue));
            }

            std::string qmlPath = "file:///" + osgDB::convertFileNameToUnixStyle(osgDB::getRealPath(Path));

            QObject* almanacAnalysis = _findChild(AlmanacAnalysisPopup);
            if(!almanacAnalysis)
                return;

            almanacAnalysis->setProperty(IMGMoonPhaseViewer.c_str(),QVariant::fromValue(QString::fromStdString(qmlPath)));
        }
    }


    //Check whether the GMT time contain the Valid characters
    bool AlmanacGUI::_checkForValidCharacters(std::string gmtTime)
    {
        int i=0;
        //Should only contain digits and ':' character(s) 
        for(i=0 ; (i < (int)gmtTime.length()) &&((gmtTime[i] >= 48 && gmtTime[i] <= 58) || (gmtTime[i] == ':')) ; i++);

        return (i == (int)gmtTime.length());
    }

    void AlmanacGUI::_getParameters(std::string value , double &param1 , double &param2, double &param3,char ch)
    {
        // XXX - use std::string instead of char
        int count = 0;
        int param;

        std::string parameter;
        std::string previousChar;
        for(unsigned int i = 0; i < value.size(); i++)
        {
            previousChar.push_back(value.at(i));
        }

        while(count++ < std::count(value.begin(), value.end() , ch) )
        {
            const char *location = strchr(previousChar.c_str(),ch);
            int length = location - previousChar.c_str() + 1;
            parameter.clear();
            for(int i = 0; i < length; i++)
            {
                parameter.push_back(previousChar.at(i));
            }
            previousChar.erase(0,length);
            sscanf(parameter.c_str(),"%d",&param);
            if(count == 1)
                param1 = param;
            else if(count == 2)
                param2 = param;
        }
        if(count == 3)
        {
            sscanf(previousChar.c_str(),"%d",&param);
            param3= param;
        }
        else if (count == 2)
        { 
            sscanf(previousChar.c_str(),"%d",&param);
            param2 = param;
        }
        else if (count == 1)
        {   
            sscanf(previousChar.c_str(),"%d",&param);
            param1 = param;
        }
    }
} 
