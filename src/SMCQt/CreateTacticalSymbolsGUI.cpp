/****************************************************************************
*
* File             : CreateTacticalSymbolsGUI.h
* Description      : CreateTacticalSymbolsGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/CreateTacticalSymbolsGUI.h>
#include <SMCQt/IconObject.h>

#include <App/Application.h>
#include <App/IUIHandler.h>
#include <App/AccessElementUtils.h>

#include <Core/IMetadataTableDefn.h>
#include <Core/IMetadataTableHolder.h>
#include <Core/IMetadataTable.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMetadataCreator.h>
#include <Core/IMetadataRecord.h>
#include <Core/IDeletable.h>
#include <Core/IBase.h>
#include <Core/IPenStyle.h>
#include <Core/IBrushStyle.h>

#include <VizUI/IDeletionUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>

#include <Elements/IModelFeatureObject.h>
#include <Elements/IRepresentation.h>
#include <Elements/IModelHolder.h>
#include <Elements/IModel.h>
#include <Elements/ElementsUtils.h>

#include <SMCElements/ILayerComponent.h>

#include <Loader/BaseTagHandler.h>
#include <Loader/ComponentConfigReaderWriter.h>
#include <Loader/IResourceReaderWriter.h>

#include <Util/StringUtils.h>
#include <Util/FileUtils.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>

#include <QFileInfo>

#include <SMCQt/VizComboBoxElement.h>

#include <serialization/AsciiStreamOperator.h>
#include <serialization/IProjectLoaderComponent.h>

#include <string>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CreateTacticalSymbolsGUI);
    DEFINE_IREFERENCED(CreateTacticalSymbolsGUI, DeclarativeFileGUI);

    //std::string CreateTacticalSymbolsGUI::LANDMARK_APPDATA_DIR = "/data/LandmarkSymbols/";
    //const std::string CreateTacticalSymbolsGUI::LandmarkContextualObjectName = "landmarkContextual";

    const std::string CreateTacticalSymbolsGUI::TacticalSymbolsPopupObjectName = "tacticalSymbolsPopup";
    const std::string CreateTacticalSymbolsGUI::TacticalSymbolsContextualTabObjectName = "tacticalSymbolsContextual";

    CreateTacticalSymbolsGUI::CreateTacticalSymbolsGUI()
    {
        //_qmlDirectoryTreeModel = NULL;

        _symbolDataDir = UTIL::getDataDirPath() + "/Symbols/TacticalSymbols/";
    }

    CreateTacticalSymbolsGUI::~CreateTacticalSymbolsGUI()
    {
    }

    void CreateTacticalSymbolsGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if (messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _tacticalSymbolsUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::ITacticalSymbolsUIHandler>(getGUIManager());
            }
            catch (...)
            { }
        }

        DeclarativeFileGUI::update(messageType, message);

    }

    void CreateTacticalSymbolsGUI::popupLoaded()
    {
        QObject* tacticalSymbolsPopup = _findChild(TacticalSymbolsPopupObjectName);
        if (tacticalSymbolsPopup)
        {
            //! Set data directory
            tacticalSymbolsPopup->setProperty("tacticalSymbolsDir", QString::fromStdString(_symbolDataDir));
            QObject::connect(tacticalSymbolsPopup, SIGNAL(mark(bool, QString)), this, SLOT(markTacticalSymbol(bool, QString)), Qt::UniqueConnection);
        }
    }

    void CreateTacticalSymbolsGUI::connectContextualMenu()
    {

        QObject* tacticalSymbolsContextual = _findChild(TacticalSymbolsContextualTabObjectName);
        if (!tacticalSymbolsContextual)
            return;

        if (!_tacticalSymbolsUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<TACTICALSYMBOLS::ITacticalSymbol> symbol = _tacticalSymbolsUIHandler->getCurrentSymbol();
        if (!symbol.valid())
        {
            return;
        }

        //! Name of the line
        QString name = QString::fromStdString(symbol->getInterface<CORE::IBase>()->getName());
        tacticalSymbolsContextual->setProperty("name", QVariant::fromValue(name));

        //! connect to property change handlers
        QObject::connect(tacticalSymbolsContextual, SIGNAL(rename(QString)), this,
            SLOT(rename(QString)), Qt::UniqueConnection);

        QObject::connect(tacticalSymbolsContextual, SIGNAL(deleteSymbol()), this, SLOT(deleteSymbol()), Qt::UniqueConnection);

        QObject::connect(tacticalSymbolsContextual, SIGNAL(editSymbol(bool)), this, SLOT(setEditMode(bool)), Qt::UniqueConnection);
    }

    void CreateTacticalSymbolsGUI::disconnectContextualMenu()
    {
        if (_tacticalSymbolsUIHandler.valid())
        {
            _tacticalSymbolsUIHandler->setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_NONE);
        }

        CORE::RefPtr<VizUI::ISelectionUIHandler> selectionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ISelectionUIHandler>(getGUIManager());
        if (selectionUIHandler.valid())
        {
            selectionUIHandler->clearCurrentSelection();
        }
    }

    void CreateTacticalSymbolsGUI::markTacticalSymbol(bool value, QString type)
    {
        std::string typeStr = type.toStdString();

        if (!_tacticalSymbolsUIHandler.valid())
            return;

        if (value)
        {
            QObject* tacticalSymbolsPopup = _findChild(TacticalSymbolsPopupObjectName);
            if (!tacticalSymbolsPopup)
                return;

            //! Get symbol name
            std::string symbolName = tacticalSymbolsPopup->property("symbolName").toString().toStdString();

            //! Get symbol affiliation
            int affiliation = tacticalSymbolsPopup->property("symbolAffiliation").toInt();

            //! Get symbol status
            int symbolStatus = tacticalSymbolsPopup->property("symbolStatus").toInt();

            _tacticalSymbolsUIHandler->setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_NONE);
            _tacticalSymbolsUIHandler->setSymbolPath(typeStr);
            _tacticalSymbolsUIHandler->setSymbolName(symbolName);
            _tacticalSymbolsUIHandler->setAffiliation(TACTICALSYMBOLS::ITacticalSymbol::Affiliation(affiliation));
            _tacticalSymbolsUIHandler->setPlanStatus(TACTICALSYMBOLS::ITacticalSymbol::PlanStatus(symbolStatus));
            _tacticalSymbolsUIHandler->setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_CREATE_TACTICALSYMBOL);
            _tacticalSymbolsUIHandler->getInterface<APP::IUIHandler>()->setFocus(true);
        }
        else
        {
            _tacticalSymbolsUIHandler->setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_NONE);
            _tacticalSymbolsUIHandler->getInterface<APP::IUIHandler>()->setFocus(false);
        }
    }

    void CreateTacticalSymbolsGUI::rename(QString newName)
    {
        if (!_tacticalSymbolsUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<TACTICALSYMBOLS::ITacticalSymbol> symbol = _tacticalSymbolsUIHandler->getCurrentSymbol();
        if (!symbol.valid())
        {
            return;
        }

        if (newName == "")
        {
            showError("Edit Polygon Name", "Polygon name cannot be blank.", true);

            QObject* tacticalSymbolsContextual = _findChild(TacticalSymbolsContextualTabObjectName);
            if (tacticalSymbolsContextual != NULL)
            {
                std::string oldName = symbol->getInterface<CORE::IBase>()->getName();
                tacticalSymbolsContextual->setProperty("name", QVariant::fromValue(QString::fromStdString(oldName)));
            }
            return;
        }

        symbol->getInterface<CORE::IBase>()->setName(newName.toStdString());

    }

    void CreateTacticalSymbolsGUI::deleteSymbol()
    {
        if (!_tacticalSymbolsUIHandler.valid())
        {
            return;
        }

        CORE::RefPtr<TACTICALSYMBOLS::ITacticalSymbol> symbol = _tacticalSymbolsUIHandler->getCurrentSymbol();
        if (!symbol.valid())
        {
            return;
        }

        CORE::IDeletable* deletable = symbol->getInterface<CORE::IDeletable>();
        if (!deletable)
        {
            return;
        }

        CORE::RefPtr<VizUI::IDeletionUIHandler> deletionUIHandler =
            APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::IDeletionUIHandler>(getGUIManager());
        if (deletionUIHandler.valid())
        {
            deletionUIHandler->deleteObject(deletable);
        }
    }

    void CreateTacticalSymbolsGUI::setEditMode(bool mode)
    {
        if (!_tacticalSymbolsUIHandler.valid())
        {
            return;
        }
        
        if (mode)
        {
            _tacticalSymbolsUIHandler->setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_EDIT);
        }
        else
        {
            _tacticalSymbolsUIHandler->setMode(SMCUI::ITacticalSymbolsUIHandler::MODE_NO_OPERATION);
        }
    }

}   // namespace SMCQt
