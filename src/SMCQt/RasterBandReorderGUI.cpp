/****************************************************************************
*
* File             : RasterBandReorderGUI.h
* Description      : RasterBandReorderGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/RasterBandReorderGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransaction.h>
#include <App/IUIHandler.h>

#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>
#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <Elements/ElementsPlugin.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>

#include <iostream>

#include <osgEarth/ImageLayer>
#include <osgEarthAnnotation/ImageOverlay>

#include <Terrain/IRasterObject.h>
#include <gdal_priv.h>
#include <SMCUI/IAddContentUIHandler.h>
#include <SMCQt/VizComboBoxElement.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, RasterBandReorderGUI);
    DEFINE_IREFERENCED(RasterBandReorderGUI, SMCQt::DeclarativeFileGUI);

    RasterBandReorderGUI::RasterBandReorderGUI()
    {

    }

    RasterBandReorderGUI::~RasterBandReorderGUI()
    {

    }

    void RasterBandReorderGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }
    }

    void RasterBandReorderGUI::connectSmpMenu(QString type)
    {
        if(type == "RasterBandReorder")
        {
            QObject* bandReorderObject = _findChild("rasterBandReorderObject");
            if(bandReorderObject)
            {
                QObject::connect(bandReorderObject, SIGNAL(handleApplyButton()), 
                    this, SLOT(handleApplyButton()), Qt::UniqueConnection);
                QObject::connect(bandReorderObject, SIGNAL(handleCancelButton()), 
                    this, SLOT(handleCancelButton()), Qt::UniqueConnection);
            }

            if(!_selectionComponent.valid())
                return;

            const CORE::ISelectionComponent::SelectionMap& selectionMap = 
                _selectionComponent->getCurrentSelection();

            if(selectionMap.empty())
            {
                return;
            }

            CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
            CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

            if(!selectedObject)
                return;

            TERRAIN::IRasterObject* raster = selectedObject->getInterface<TERRAIN::IRasterObject>();

            int noOFBands;
            if(raster)
            {
                std::string url = raster->getURL();
                if(url.empty())
                {
                    url = raster->getRuntimeURL();
                }
                std::string ext = osgDB::getFileExtension(url);

                if(ext == "")
                {
                    showError("Invalid Dataset", "This Feature currently doesnt works with directory loading");
                    bandReorderObject->setProperty( "rasterValid", QVariant::fromValue(false));
                    return;        

                }
                if(ext == "xyz" || ext == "yahoo" || ext == "wms" || ext == "bing" || ext == "google" )
                {
                    showError("Invalid Dataset", "This Feature doesnt works with Wed Data");
                    bandReorderObject->setProperty( "rasterValid", QVariant::fromValue(false));
                    return;        

                }
                if(ext == "tms" )
                {
                    showError("Invalid Dataset", "This Feature doesnt works with TMS Data");
                    bandReorderObject->setProperty( "rasterValid", QVariant::fromValue(false));
                    return;        

                }



                GDALDataset* dateset =  (GDALDataset*)GDALOpen(url.c_str(), GA_ReadOnly );

                if(dateset)
                {
                    noOFBands = dateset->GetRasterCount();
                    if( noOFBands < 3)
                    {
                        showError("Invalid No of Bands", "This Feature doesnt work when no of bands are less than 3");
                        bandReorderObject->setProperty( "rasterValid", QVariant::fromValue(false));
                        return;                            
                    }

                }


                QList<QObject*> comboBoxList;

                for( int i = 1; i <= noOFBands; i++ )
                {
                    comboBoxList.append(new VizComboBoxElement(UTIL::ToString(i).c_str(), UTIL::ToString(i).c_str()));
                }

                _setContextProperty("subdatasetListModel", QVariant::fromValue(comboBoxList));

                osg::Vec3d bandOrder = raster->getBandOrder();

                QObject* band1List = _findChild("band1ComboBox");
                QObject* band2List = _findChild("band2ComboBox");
                QObject* band3List = _findChild("band3ComboBox");

                if(band1List)
                {
                    band1List->setProperty( "value", QVariant::fromValue(bandOrder[0]));
                }

                if(band2List)
                {
                    band2List->setProperty( "value", QVariant::fromValue(bandOrder[1]));
                }

                if(band3List)
                {

                    band3List->setProperty( "value", QVariant::fromValue(bandOrder[2]));
                }


                bandReorderObject->setProperty( "rasterValid", QVariant::fromValue(true));
            }
            else
            {
                bandReorderObject->setProperty( "rasterValid", QVariant::fromValue(false));
            }
        }


    }

    void RasterBandReorderGUI::handleApplyButton()
    {
        if(!_selectionComponent.valid())
            return;

        const CORE::ISelectionComponent::SelectionMap& selectionMap = 
            _selectionComponent->getCurrentSelection();

        if(selectionMap.empty())
        {
            return;
        }

        CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
        CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

        if(!selectedObject)
            return;

        TERRAIN::IRasterObject* raster = selectedObject->getInterface<TERRAIN::IRasterObject>();
        if(raster)
        {
            QObject* band1List = _findChild("band1ComboBox");
            QObject* band2List = _findChild("band2ComboBox");
            QObject* band3List = _findChild("band3ComboBox");

            std::vector<int> bandOrder;
            bandOrder.resize(3);

            if(band1List)
            {
                bandOrder[0] = band1List->property("value").toInt();
            }

            if(band2List)
            {
                bandOrder[1] = band2List->property("value").toInt();
            }

            if(band3List)
            {

                bandOrder[2] = band3List->property("value").toInt();
            }

            SMCUI::IAddContentUIHandler* addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());
            addContentUIHandler->reorderRasterBands(selectedObject,bandOrder);

        }

        return;

    }

    void RasterBandReorderGUI::handleCancelButton()
    {

    }
    void RasterBandReorderGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());

                const CORE::ComponentMap& cmap = wmain->getComponentMap();
                _selectionComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }


} // namespace SMCQt