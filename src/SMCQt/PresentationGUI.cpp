/*****************************************************************************
*
* File             : PresentationGUI.h
* Description      : PresentationGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/PresentationGUI.h>
#include <SMCQt/IconObject.h>

#include <App/IApplication.h>
#include <App/IUIHandlerManager.h>
#include <App/AccessElementUtils.h>

#include <Core/IFeatureLayer.h>
#include <Core/CoreRegistry.h>
#include <Core/WorldMaintainer.h>

#include <osgDB/FileNameUtils>
#include <QFileDialog>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, PresentationGUI);
    DEFINE_IREFERENCED(PresentationGUI, DeclarativeFileGUI);

    PresentationGUI::PresentationGUI()
    {
    }

    PresentationGUI::~PresentationGUI(){}   

    void PresentationGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* bottomLoader = _findChild("bottomLoader");
        if(bottomLoader != NULL)
        {
            QObject::connect(bottomLoader, SIGNAL(pdfMenuLoaded()), this,
                SLOT(connectPDFMenu()), Qt::UniqueConnection);
        }

        QObject* mainPDFLoader = _findChild("mainPDFLoader");
        if(mainPDFLoader)
        {
            QObject::connect(mainPDFLoader, SIGNAL(saveFile(QString)), this,
                SLOT(uploadPdf(QString)), Qt::UniqueConnection);
        }

        QObject* smpLeftMenu = _findChild("smpLeftMenu");
        if(smpLeftMenu)
        {
            QObject::connect(smpLeftMenu, SIGNAL(browseForTextToSpeech()), this,
                SLOT(browseForTextToSpeech()), Qt::UniqueConnection);
        }
    }

    void PresentationGUI::connectPDFMenu()
    {
        QObject* pdfMenu = _findChild("pdfMenu");
        if(pdfMenu != NULL)
        {
            //QObject::connect(pdfMenu, SIGNAL(addNewPDF(QUrl)), this, 
            //    SLOT(browseButtonClicked(QUrl)), Qt::UniqueConnection);

            QObject::connect(pdfMenu, SIGNAL(addNewPDF()), this, 
                SLOT(browseButtonClicked()), Qt::UniqueConnection);

            QObject::connect(pdfMenu, SIGNAL(populatePDFList()), this, 
                SLOT(showPdfList()), Qt::UniqueConnection);

            QObject::connect(pdfMenu, SIGNAL(openDocument(QString)), this,
                SLOT(openDocument(QString)), Qt::UniqueConnection);

            QObject::connect(pdfMenu, SIGNAL(deleteDocument(QString)), this,
                SLOT(confirmDocumentDelete(QString)), Qt::UniqueConnection);
        }
    }

    void PresentationGUI::browseForTextToSpeech()
    {
        APP::IGUI* gui = getGUIManager()->getGUIByName("TTSGUI");
        if(gui)
        {
            VizQt::IQtGUI* qtGUI = gui->getInterface<VizQt::IQtGUI>();
            if(qtGUI)
            {
                qtGUI->setActive(true);
            }
        }
    }

    void PresentationGUI::openDocument(QString fileName)
    {
        if(!_presentationComponent.valid())
            return;

        SMCElements::IPresentationComponent::pdfMap mapPdf = _presentationComponent->getPdfMapCollection();
        std::map<std::string, std::string>::const_iterator iter = mapPdf.find(fileName.toStdString());
        if(iter != mapPdf.end())
        {
            QString filePath = QString::fromStdString(iter->second);

            QString fileName = QString::fromStdString(osgDB::getStrippedName(iter->second));

            QObject* mainPDFLoader = _findChild("mainPDFLoader");
            if(mainPDFLoader)
            {
                QMetaObject::invokeMethod(mainPDFLoader, "loadPDF", 
                    Q_ARG(QVariant, QVariant(fileName)),
                    Q_ARG(QVariant, QVariant(filePath)),
                    Q_ARG(QVariant, QVariant(false)));
            }
        }
        else
        {
            emit showError("Presentation", "Invalid File");
        }
    }

    void PresentationGUI::confirmDocumentDelete(QString documentName)
    {
        _documentToDelete = documentName;

        emit question("Delete Document", "Do you want to delete document?", false,
            "deleteDocumentOkButtonPressed()", "deleteDocumentCancelButtonPressed()");
    }

    void PresentationGUI::showEncryptedPDFError()
    {
        emit showError("Encrypted PDF are not supported", "Please give a non encrypted PDF to load");
    }

    void PresentationGUI::showCorruptedPDFError()
    {
        emit showError("Corrupted PDF", "Please give a valid PDF to load");
    }

    void PresentationGUI::deleteDocumentOkButtonPressed()
    {
        if(!_presentationComponent.valid())
            return;

        if(!_documentToDelete.size())
            return;

        SMCElements::IPresentationComponent::pdfMap mapPdf = _presentationComponent->getPdfMapCollection();
        std::map<std::string, std::string>::const_iterator iter = mapPdf.find(_documentToDelete.toStdString());
        if(iter != mapPdf.end())
        {
            // delete pdf from map and server
            _presentationComponent->deletePdfDocument(_documentToDelete.toStdString());

            showPdfList();
        }
        else
        {
            emit showError("Presentation", "Invalid File");
        }

    }


    void PresentationGUI::showPdfList()
    {
        if(!_presentationComponent.valid())
            return;

        //Populating the list
        QList<QObject*> pdfListModel;

        std::map<std::string, std::string> mapPdf = _presentationComponent->getPdfMapCollection();
        std::map<std::string, std::string>::const_iterator iter = mapPdf.begin();
        while(iter != mapPdf.end())
        {
            pdfListModel.append(new IconObject(QString::fromStdString(iter->first), 
                "../../Symbols/unknown.png"));

            iter++;
        }

        _setContextProperty("pdfListModel", QVariant::fromValue(pdfListModel));
    }

    void PresentationGUI::uploadPdf(QString document)
    {
        if(document.isEmpty())
            return;

        if(!_presentationComponent.valid())
            return;

        std::string filePath = document.toStdString();
        std::string fileName = osgDB::getStrippedName(filePath);

        std::map<std::string, std::string> mapPdf = _presentationComponent->getPdfMapCollection();
        std::map<std::string, std::string>::const_iterator iter = mapPdf.find(fileName);
        if(iter != mapPdf.end())
        {
            emit showError("Presentation", "File Already Uploaded");
            return;
        }

        emit showError("Presentation", "File Saved", false);

        _presentationComponent->addPdfDocument(fileName, filePath);

        showPdfList();
    }

    void PresentationGUI::browseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();

        static QString lastDir = "c:/";

        QString filePath = QFileDialog::getOpenFileName(parent, tr("Open PDF File"), 
            lastDir, tr("PDFFile (*.pdf )"), 0);

        if(filePath.isEmpty())
            return;

        QString fileName = QString::fromStdString(osgDB::getStrippedName(filePath.toStdString()));

        QObject* mainPDFLoader = _findChild("mainPDFLoader");
        if(mainPDFLoader)
        {
            QMetaObject::invokeMethod(mainPDFLoader, "loadPDF", 
                Q_ARG(QVariant, QVariant(fileName)),
                Q_ARG(QVariant, QVariant(filePath)),
                Q_ARG(QVariant, QVariant(true)));
        }

    }

    void PresentationGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
                CORE::RefPtr<CORE::IComponent> component = wm->getComponentByName("PresentationComponent");
                if(component.valid())
                {
                    _presentationComponent = component->getInterface<SMCElements::IPresentationComponent>();
                }
            }
            catch(...)
            {

            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

} // namespace indiGUI


