#include <SMCQt/ResourcesList.h>
//#include <QtDeclarative/QDeclarativeContext>

namespace SMCQt
{
    ResourceCategory::ResourceCategory(QString name, QString path)
        : _name(name)
        , _path(path)
    {

    }

    void ResourceCategory::addSymbol(QString symbolName)
    {
        _children.append(symbolName);
    }

    QString ResourceCategory::getName() const
    {
        return _name;
    }

    void ResourceCategory::setName(QString name)
    {
        if(_name != name)
        {
            _name = name;
        }
    }

    QString ResourceCategory::getPath() const
    {
        return _path;
    }

    void ResourceCategory::setPath(QString path)
    {
        if(_path != path)
        {
            _path = path;
        }
    }

    QStringList ResourceCategory::children() const
    {
        return _children;
    }

    ResourcesList::ResourcesList(QObject* parent) : QAbstractListModel(parent)
    {
#ifdef QT4
        setRoleNames(roleNames());
#endif
    }

    int ResourcesList::rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent)
        return _categories.size();
    }

    QHash<int, QByteArray> ResourcesList::roleNames() const
    {
        QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
        roles.insert(CategoryNameRole, QByteArray("categoryName"));
        roles.insert(ChildrenRole, QByteArray("children123"));
        roles.insert(CountRole, QByteArray("count"));
        roles.insert(CategoryPathRole, QByteArray("path"));

        return roles;
    }

    void ResourcesList::clear()
    {
        _categories.clear();
    }

    void ResourcesList::addCategory(ResourceCategory* category)
    {
        _categories << category; 
    }

    QVariant ResourcesList::data(const QModelIndex &index, int role) const
    {
        if(!index.isValid())
            return QVariant();

        if(index.row() >(_categories.size() - 1))
            return QVariant();

        ResourceCategory *category = _categories.at(index.row());

        switch(role)
        {
        case Qt::DisplayRole:   
        case CategoryNameRole: 
            return QVariant::fromValue(category->getName());
        case ChildrenRole:
            return QVariant::fromValue(category->children());
        case CountRole:
            return QVariant::fromValue(category->children().size());
        case CategoryPathRole:
            return QVariant::fromValue(category->getPath());
        default:
            return QVariant();
        }

    }

}   //namespace SMCQt