#include <SMCQt/ClassificationObject.h>

namespace SMCQt{

    ClassificationObject::ClassificationObject(QObject* parent) : QObject(parent)
    {

    }

    ClassificationObject::ClassificationObject(const QString &name, const QString &type, const bool& state, QObject *parent)
		: QObject(parent), m_name(name), m_type(type), m_state(state)
    {
    }

	bool ClassificationObject::state() const
	{
		return m_state;
	}

	void ClassificationObject::setState(const bool& state)
	{
		if (state != m_state)
		{
			m_state = state;
			emit stateChanged();
		}
	}

    QString ClassificationObject::name() const
    {
        return m_name;
    }

    void ClassificationObject::setName(const QString &name)
    {
        if (name != m_name)
        {
            m_name = name;
            emit nameChanged();
        }
    }

    QString ClassificationObject::type() const
    {
        return m_type;
    }
    void ClassificationObject::setType(const QString &type)
    {
        if (type != m_type)
        {
            m_type = type;
            emit typeChanged();
        }
    }

}
