/****************************************************************************
 *
 * File             : RecordingGUI.h
 * Description      : RecordingGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCQt/RecordingGUI.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <VizQt/QtUtils.h>

#include <iostream>
#include <sstream>
#include <string.h>

#ifdef USE_QT4
#include <QtGui/QFileDialog>
#include <QtGui/QLineEdit>
#include <QtGui/QGroupBox>
#include <QtGui/QDateEdit>
#include <QtGui/QTabWidget>
#else
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QTabWidget>
#endif

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, RecordingGUI)
    DEFINE_IREFERENCED(RecordingGUI, DeclarativeFileGUI)
    
    RecordingGUI::RecordingGUI() 
    {
    }
    
    RecordingGUI::~RecordingGUI()
    {
    }

    void 
    RecordingGUI::popupLoaded(QString type)
    {
        QObject* recordPopup = _findChild("recordMenu");
        if(recordPopup != NULL)
        {
            QObject::connect(recordPopup, SIGNAL(browseButtonClicked()), this, SLOT(_handleBrowseButtonClicked()), Qt::UniqueConnection);
            QObject::connect(recordPopup, SIGNAL(startCaptureClicked(QString)), this, SLOT(_startCaptureClicked(QString)), Qt::UniqueConnection);
            QObject::connect(recordPopup, SIGNAL(stopCaptureClicked()), this, SLOT(_stopCaptureClicked()), Qt::UniqueConnection);
            QObject::connect(recordPopup, SIGNAL(changeResolution(QString)), this, SLOT(_changeResolution(QString)), Qt::UniqueConnection);
        }

        //default widht and height
        _width  = 1920;
        _height = 1080;
    }

    void 
    RecordingGUI::_loadAndSubscribeSlots()
    {
        QObject* popupLoader = _findChild(SMP_FileMenu);

        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }

    void
    RecordingGUI::_loadRecordingUIHandler()
    {
        _recordingUIHandler = 
            APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IRecordingUIHandler>(getGUIManager());
    }

    void RecordingGUI::_handleBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        std::string directory = "";
        std::string formatFilter = "Avi File (*.avi)";
        std::string caption = "Avi Files";
        QString path = QFileDialog::getSaveFileName(parent, caption.c_str(), directory.c_str(), formatFilter.c_str(), 0);

        QObject* recordPopup = _findChild("recordMenu");
        if(recordPopup != NULL)
        {
            recordPopup->setProperty("selectedFile",QVariant::fromValue(path));
        }
    }

    void 
        RecordingGUI::_startCaptureClicked(QString filename)// , QString width, QString height)
    {
        try
        {             
            std::string filepath = filename.toStdString();

            if(filepath.empty())
            {
                std::string title = "Insufficient / Incorrect data";
                std::string text = "Output video file name is not specified, please browse/specify a file to save as";

                showError(title.c_str(), text.c_str());
                return;
            }

            QObject* recordPopup = _findChild("recordMenu");
            if(recordPopup != NULL)
            {
                recordPopup->setProperty("isStartButtonEnabled",false);
            }
            if(_recordingUIHandler.valid())
            {
                _recordingUIHandler->startRecording(filepath, _width , _height);
            }

        }
        catch(UTIL::Exception &e)
        {
            std::string errorMessage = e.What();
            emit showError("Error in Reading File", QString::fromStdString(errorMessage), true);
        }
        catch(...)
        {
            emit showError("Error in Reading File", "Unknown Error", true);
        }
    }

    void
    RecordingGUI::_stopCaptureClicked()
    {
        _recordingUIHandler->stopRecording();
    }

    void
    RecordingGUI::_changeResolution(QString resolution)  
    {
        std::string strRes = resolution.toStdString();
        std::istringstream ss(strRes);

        ss >> _width;
        ss.ignore(10, 'x');
        ss >> _height;
    }

    void
    RecordingGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            { 
                _loadRecordingUIHandler();
            }
            catch(...)
            {}
        }
    }

}
