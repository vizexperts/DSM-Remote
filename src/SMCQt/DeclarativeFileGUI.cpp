/****************************************************************************
*
* File             : LayoutFileGUI.h
* Description      : LayoutFileGUI class definition
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <Core/InterfaceUtils.h>
#include <Core/WorldMaintainer.h>
#include <Core/IGeoExtent.h>
#include <Core/IPoint.h>
#include <Elements/IModelResult.h>
#include <Util/DistanceBearingCalculationUtils.h>
#include <Util/CoordinateConversionUtils.h>
#include <VizUI/ICameraUIHandler.h>
#include <Core/ITerrain.h>
#include <VizQt/IQtGUIManager.h>
#include <Core/IFeatureLayer.h>
#include <Elements/IModelFeatureObject.h>
#include <osg/ComputeBoundsVisitor>

#include <SMCQt/Utils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, DeclarativeFileGUI);

    const std::string DeclarativeFileGUI::SMP_RightMenu = "smpMenu";
    const std::string DeclarativeFileGUI::SMP_FileMenu = "smpFileMenu";
    const std::string DeclarativeFileGUI::SMP_QuickLaunchGUI = "QuickLaunchGUI";

    DeclarativeFileGUI::DeclarativeFileGUI()
    {}

    DeclarativeFileGUI::~DeclarativeFileGUI()
    {
    }

    void DeclarativeFileGUI::onAddedToGUIManager()
    {
        _loadAndSubscribeSlots();

        CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
        _subscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();

        QQmlContext* rootContext = qtapp->getRootContext();
        if (rootContext)
        {
            rootContext->setContextProperty(_contextPropertyName.c_str(), this);
        }

        VizQt::QtGUI::onAddedToGUIManager();
    }

    void DeclarativeFileGUI::onRemovedFromGUIManager()
    {
        CORE::RefPtr<APP::IApplication> app = getGUIManager()->getInterface<APP::IManager>(true)->getApplication();
        _unsubscribe(app.get(), *APP::IApplication::ApplicationConfigurationLoadedType);

        VizQt::QtGUI::onRemovedFromGUIManager();
    }

    void DeclarativeFileGUI::_loadAndSubscribeSlots()
    {
        QObject::connect(this, SIGNAL(showError(QString, QString, bool, QString)), this,
            SLOT(_showError(QString, QString, bool, QString)), Qt::QueuedConnection);

        QObject::connect(this, SIGNAL(criticalQuestion(QString, QString, bool, QString, QString)), this,
            SLOT(_criticalQuestion(QString, QString, bool, QString, QString)), Qt::QueuedConnection);

        QObject::connect(this, SIGNAL(question(QString, QString, bool, QString, QString)), this,
            SLOT(_question(QString, QString, bool, QString, QString)), Qt::QueuedConnection);

    }

    void DeclarativeFileGUI::initializeAttributes()
    {
        VizQt::QtGUI::initializeAttributes();
    }

    QObject* DeclarativeFileGUI::_findChild(const std::string& objectName)
    {
        if (!_manager)
        {
            LOG_ERROR("GUI Manager not valid for " + getClassname() + " trying to find QObject named " + objectName);
            return NULL;
        }

        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());
        if (item)
        {
            QObject* obj = item->findChild<QObject*>(objectName.c_str());

            if (obj)
            {
                return obj;
            }
        }

        return NULL;
    }

    void DeclarativeFileGUI::_setContextProperty(const std::string& propertyName, const QVariant& var)
    {
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();

        QQmlContext* rootContext = qtapp->getRootContext();
        if (rootContext)
        {
            rootContext->setContextProperty(propertyName.c_str(), var);
        }
    }

    void DeclarativeFileGUI::_showError(QString title, QString text, bool critical, QString okSlot)
    {
        QObject* errorLoader = _findChild("errorLoader");
        if (errorLoader)
        {
            _errorTitle = title;
            _errorDescription = text;
            _okSlot = okSlot;
            _cancelSlot = "";
            _critical = critical;
            _buttonText = false;

            QObject::connect(errorLoader, SIGNAL(connectOkCancel()),
                this, SLOT(connectErrorMessage()), Qt::UniqueConnection);

            LOG_ERROR(_errorTitle.toStdString() + " : " + _errorDescription.toStdString());

            errorLoader->setProperty("load", QVariant::fromValue(true));
        }
    }

    void DeclarativeFileGUI::connectErrorMessage()
    {
        QObject* errorWindow = _findChild("errorWindow");
        if (errorWindow)
        {
            // only the properties are set, no signal handlers to go back to the calling code.
            errorWindow->setProperty("titleText", QVariant::fromValue(_errorTitle));
            errorWindow->setProperty("messageText", QVariant::fromValue(_errorDescription));
            errorWindow->setProperty("critical", QVariant::fromValue(_critical));

            bool cancelButtonVisible = false;

            if (_buttonText)
            {
                errorWindow->setProperty("okButtonText", "Yes");
                errorWindow->setProperty("cancelButtonText", "No");
                if (_closeVisible)
                {
                    errorWindow->setProperty("closeButtonVisible", true);
                    errorWindow->setProperty("closeButtonText", "Cancel");
                }
                else
                {
                    errorWindow->setProperty("closeButtonVisible", false);
                }
            }

            else
            {
                errorWindow->setProperty("okButtonText", "Ok");
                errorWindow->setProperty("closeButtonVisible", false);
                errorWindow->setProperty("cancelButtonVisible", QVariant::fromValue(cancelButtonVisible));
            }
            //See defination of SLOT

            if (_okSlot.size())
            {
                std::string okSlot = "1" + _okSlot.toStdString();
                QObject::connect(errorWindow, SIGNAL(okButtonClicked()), this, okSlot.c_str(), Qt::UniqueConnection);
            }
            if (_cancelSlot.size())
            {
                std::string cancelSlot = "1" + _cancelSlot.toStdString();
                cancelButtonVisible = QObject::connect(errorWindow, SIGNAL(cancelButtonClicked()),
                    this, cancelSlot.c_str(), Qt::UniqueConnection);
            }

            QObject::connect(errorWindow, SIGNAL(close()), this, SLOT(errorWindowClosed()), Qt::UniqueConnection);

            // reset variables
            _errorTitle = "";
            _errorDescription = "";
            _critical = false;
            _okSlot = "";
            _cancelSlot = "";

            QObject* errorLoader = _findChild("errorLoader");

            QObject::disconnect(errorLoader, SIGNAL(connectOkCancel()),
                this, SLOT(connectErrorMessage()));
        }
    }

    void DeclarativeFileGUI::errorWindowClosed()
    {
        QObject* errorWindow = _findChild("errorWindow");
        if (errorWindow)
        {
            QObject::disconnect(errorWindow, 0, this, 0);
        }

        QObject* errorLoader = _findChild("errorLoader");
        if (errorLoader)
        {
            errorLoader->setProperty("load", QVariant::fromValue(false));
        }
    }

    void DeclarativeFileGUI::_criticalQuestion(QString title, QString text,
        bool closeVisible, QString okSlot, QString cancelSlot)
    {
        QObject* errorLoader = _findChild("errorLoader");
        if (errorLoader)
        {
            _errorTitle = title;
            _errorDescription = text;

            _okSlot = okSlot;
            _cancelSlot = cancelSlot;
            _critical = true;
            _buttonText = true;
            _closeVisible = closeVisible;
            QObject::connect(errorLoader, SIGNAL(connectOkCancel()),
                this, SLOT(connectErrorMessage()), Qt::UniqueConnection);

            LOG_INFO(_errorTitle.toStdString() + " : " + _errorDescription.toStdString());

            errorLoader->setProperty("load", QVariant::fromValue(true));
        }
    }

    void DeclarativeFileGUI::_question(QString title, QString text,
        bool closeVisible, QString okSlot, QString cancelSlot)
    {
        QObject* errorLoader = _findChild("errorLoader");
        if (errorLoader)
        {
            _errorTitle = title;
            _errorDescription = text;

            _okSlot = okSlot;
            _cancelSlot = cancelSlot;
            _buttonText = true;
            _closeVisible = closeVisible;
            _critical = false;

            QObject::connect(errorLoader, SIGNAL(connectOkCancel()),
                this, SLOT(connectErrorMessage()), Qt::UniqueConnection);

            LOG_INFO(_errorTitle.toStdString() + " : " + _errorDescription.toStdString());

            errorLoader->setProperty("load", QVariant::fromValue(true));

        }
    }

    void DeclarativeFileGUI::_loadBusyBar(const UTIL::ThreadPool::Functor0_T& threadFunc,
        std::string message, bool autoHide)
    {
        // load busy bar

        QObject* statusBar = _findChild("statusBar");
        if (statusBar)
        {
            QMetaObject::invokeMethod(statusBar, "setStatus",
                Q_ARG(QVariant, QVariant::fromValue(QString::fromStdString(message))),
                Q_ARG(QVariant, QVariant::fromValue(true)));
        }

        if (autoHide)
        {
            QObject::connect(this, SIGNAL(executed()), this, SLOT(_hideBusyBar()), Qt::QueuedConnection);
        }

        _busyFunction = threadFunc;

        CORE::WorldMaintainer* wm = CORE::WorldMaintainer::instance();

        UTIL::ThreadPool* tp = wm->getComputeThreadPool();

        if (tp)
        {
            tp->invoke(boost::bind(&DeclarativeFileGUI::_executeBusyBar, this));
        }
    }

    void DeclarativeFileGUI::_executeBusyBar()
    {
        try
        {
            (_busyFunction)();
        }
        catch (...)
        {
        }

        emit executed();
    }

    void DeclarativeFileGUI::_hideBusyBar()
    {
        QObject* statusBar = _findChild("statusBar");
        if (statusBar)
        {
            QMetaObject::invokeMethod(statusBar, "close");
        }

        QObject::disconnect(this, SIGNAL(executed()), this, SLOT(_hideBusyBar()));
    }

    QWidget* DeclarativeFileGUI::getWidget() const
    {
        return NULL;
    }

    double
        _getHeightAtPosition(const osg::Vec2d& longlat)
    {
        const CORE::WorldMap& map = CORE::WorldMaintainer::instance()->getWorldMap();

        CORE::WorldMap::const_iterator itw = map.begin();
        CORE::RefPtr<CORE::IWorld> world = itw->second;
        if (world)
        {
            CORE::ITerrain* terrain = world->getTerrain();
            double elevation = 0.0;
            if (terrain->getElevationAt(longlat, elevation))
            {
                return elevation;;
            }
        }
        return 0.0f;
    }


    void DeclarativeFileGUI::jumpToObject(CORE::RefPtr<CORE::IObject> object)
    {
        if (!object.valid())
            return;

        osg::Vec3d center;
        osg::Vec4d extent;

        //If it is 3D Model or layer contains Models, we use different method for Center and Extents
        CORE::RefPtr<ELEMENTS::IModelFeatureObject> isModelInLayer;
        CORE::RefPtr<CORE::ICompositeObject> compObject = object->getInterface<CORE::ICompositeObject>();
        CORE::RefPtr<ELEMENTS::IModelFeatureObject> isModel = object->getInterface<ELEMENTS::IModelFeatureObject>();
        
        //If this is feature layer
        if (compObject.valid())
        {
            //get extent and center for this layer
			const CORE::ICompositeObject::ObjectMap &  modelLayer = compObject->getObjectMap();
			// Checking for empty map
			if (!modelLayer.empty())
				isModelInLayer = modelLayer.begin()->second->getInterface<ELEMENTS::IModelFeatureObject>();
		}

        
        
        if ( (isModelInLayer.valid() && compObject.valid()) || isModel.valid() )
        {
            Utils::getModelLayerExtCenter(object, extent, center);
        }
        else
        {
            //otherwise getting extents from IGoExtents

            CORE::IGeoExtent* gExt = object->getInterface<CORE::IGeoExtent>();
            if (!gExt)
            {
                LOG_INFO("Object " + object->getInterface<CORE::IBase>()->getName() + " does not support GeoJump");
                return;
            }
            else
            {
                LOG_INFO("Jumping to Object " + object->getInterface<CORE::IBase>()->getName());
            }

            if (!(gExt->getCenter(center) && gExt->getExtents(extent))) 
            {
                return;
            }
        }


        if (center != osg::Vec3d(0.0, 0.0, 0.0))
        {

            double dx, dy;
            double objectAspectRatio;
                
            if (isModel.valid() || isModelInLayer.valid())
            {
                //In case of Model, extents is radius stored in "z" variable of extent
                dx = extent.z() - extent.x();
                dy = 0;
                //dummy aspect ratio of 3D object 
                objectAspectRatio = 0;
            }
            else
            {
                UTIL::DistanceBearingCalculationUtils::DistnBearingResult dx1 =
                    UTIL::DistanceBearingCalculationUtils::CalculateHaversineDistance(osg::Vec3d(extent.x(), extent.y(), 0.0f), osg::Vec3d(extent.z(), extent.y(), 0.0f));
                UTIL::DistanceBearingCalculationUtils::DistnBearingResult dx2 =
                    UTIL::DistanceBearingCalculationUtils::CalculateHaversineDistance(osg::Vec3d(extent.x(), extent.w(), 0.0f), osg::Vec3d(extent.z(), extent.w(), 0.0f));

                dx = std::max(dx1.distance, dx2.distance);

                UTIL::DistanceBearingCalculationUtils::DistnBearingResult dy1 =
                    UTIL::DistanceBearingCalculationUtils::CalculateHaversineDistance(osg::Vec3d(extent.x(), extent.w(), 0.0f), osg::Vec3d(extent.x(), extent.y(), 0.0f));
                UTIL::DistanceBearingCalculationUtils::DistnBearingResult dy2 =
                    UTIL::DistanceBearingCalculationUtils::CalculateHaversineDistance(osg::Vec3d(extent.z(), extent.w(), 0.0f), osg::Vec3d(extent.z(), extent.y(), 0.0f));

                dy = std::max(dy1.distance, dy2.distance);

                objectAspectRatio = dx / dy;

            }

            double distance;
            double offset = 0;// 800;
            double cameraAspectRatio;

            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
            CORE::RefPtr<APP::IManager> uiManager = _manager->getInterface<APP::IManager>();
            if (uiManager.valid())
            {
                osg::ref_ptr<osgViewer::View> view = uiManager->getApplication()->getCompositeViewer()->getView(0);
                if (view.valid())
                {
                    osg::ref_ptr<osg::Camera> camera = view->getCamera();
                    if (camera.valid())
                    {
                        double width = (double)(view->getCamera()->getViewport()->width());
                        double height = (double)(view->getCamera()->getViewport()->height());
                        cameraAspectRatio = (double)(width / height);
                    }
                }
            }

            //if Object's Aspect ratio is greater then camera's aspect ratio, then fit camera width wise
            //in case of 3D model, we are using boinding box radius, so alwasy handled in first case
            //else height wise
            if ((objectAspectRatio > cameraAspectRatio) || isModel.valid() || isModelInLayer.valid())//(dx > dy)
            {
                if (isModel.valid() || isModelInLayer.valid()) //for model, we are getting much less axis aligned BB
                {
                    distance = dx;
                }
                else 
                {
                    distance = dx * 145;
                    offset = 400;
                }
            }
            else
            {
                if (isModel.valid() || isModelInLayer.valid())
                {
                    distance = dy * 0.5;
                }
                else
                {
                    distance = dy * 550;
                    offset = 400;
                }
            }


            CORE::RefPtr<VizUI::ICameraUIHandler> camerauihandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager());
            double fovValue = camerauihandler->getFieldOfView();

            if ((objectAspectRatio > cameraAspectRatio) && !isModel.valid() && !isModelInLayer.valid()) {
                CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
                CORE::RefPtr<APP::IManager> uiManager = _manager->getInterface<APP::IManager>();
                if (uiManager.valid())
                {
                    //Getting the settingComponent
                    CORE::IComponent *component = worldMaintainer->getComponentByName("SettingComponent");
                    if (component)
                    {
                        fovValue = fovValue / cameraAspectRatio;
                    }
                }
            }


            double centerZ = 0;// _getHeightAtPosition(osg::Vec2d(center.x(), center.y()));
            double height = std::tan(osg::DegreesToRadians(90.0 - fovValue/2)) * distance;
            double range = centerZ + height + offset;

            // If Distance is zero that indicates that it is a point. Check if it derives from IPoint
            // Get its Altitude and place the camera at the Height + Diameter + some constant value
            if (distance == 0.0)
            {
                CORE::IPoint* pnt = object->getInterface<CORE::IPoint>();
                CORE::IPoint* featureLayerPoint = NULL;
                if (!pnt){
                    //check whether it is Feature Layer with one point
                    CORE::RefPtr<CORE::ICompositeObject> compObject = object->getInterface<CORE::ICompositeObject>();
                    if (compObject.valid())
                    {
                        const CORE::ICompositeObject::ObjectMap &  modelLayer = compObject->getObjectMap();
                        // Checking for empty map
                        if (!modelLayer.empty())
                            featureLayerPoint = modelLayer.begin()->second->getInterface<CORE::IPoint>();
                    }
                }
                if (pnt || featureLayerPoint)
                {
                    if (pnt)
                    {
                        center = pnt->getValue();
                        centerZ = pnt->getZ();
                    }
                    else
                    {
                        center = featureLayerPoint->getValue();
                        centerZ = featureLayerPoint->getZ();
                    }

                    range = 0;
                }
                //checking for the model result
                ELEMENTS::IModelResult *modelResult = object->getInterface<ELEMENTS::IModelResult>();
                if (modelResult)
                {
                    range = center.z() + offset;
                }
            }

            //if range is less then 45.5 then make it to this
            if (range < 45.5)
            {
                range = 45.5;
            }

            CORE::RefPtr<VizUI::ICameraUIHandler> camUIHandler =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ICameraUIHandler>(getGUIManager());
            if (camUIHandler.valid())
            {
                camUIHandler->setViewPoint(center, range, 0, -89, 2);
            }
        }
    }
} // namespace GUI