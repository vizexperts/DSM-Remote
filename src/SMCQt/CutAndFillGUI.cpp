/****************************************************************************
*
* File             : CutAndFillGUI.h
* Description      : CutAndFillGUI class definition
*
*****************************************************************************
* Copyright (c) 2015-2016, VizExperts India Pvt. Ltd.
*****************************************************************************/
//#include <SMCQt/ColorPaletteGUI.h>
#include <SMCQt/CutAndFillGUI.h>
#include <App/IApplication.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ICompositeObject.h>
#include <Core/ITerrain.h>
#include <App/AccessElementUtils.h>
#include <SMCQt/QMLTreeModel.h>
#include <VizUI/VizUIPlugin.h>
#include <Terrain/IElevationObject.h>
#include <SMCQt/VizComboBoxElement.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/IGeoExtent.h>
#include <GISCompute/IFilterStatusMessage.h>
#include<iostream>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, CutAndFillGUI);
    DEFINE_IREFERENCED(CutAndFillGUI,DeclarativeFileGUI);

    const std::string CutAndFillGUI::CutAndFillAnalysisPopup = "CutAndFillAnalysis";

    CutAndFillGUI::CutAndFillGUI()
    {
        
    }

    CutAndFillGUI::~CutAndFillGUI()
    {}

    void CutAndFillGUI::_loadAndSubscribeSlots()
    {   
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(_handleProgressValueChanged(int)),Qt::QueuedConnection);
    }

    void CutAndFillGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                _uiHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::ICutAndFillUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    QObject* CutAndFillAnalysisObject = _findChild(CutAndFillAnalysisPopup);
                    if(CutAndFillAnalysisObject)
                    {
                        CutAndFillAnalysisObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        CutAndFillAnalysisObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        CutAndFillAnalysisObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        CutAndFillAnalysisObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
        else if(messageType == *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType)
        {   
             GISCOMPUTE::IFilterStatusMessage* filterMessage = 
                message.getInterface<GISCOMPUTE::IFilterStatusMessage>();
            if(filterMessage->getStatus() == GISCOMPUTE::FILTER_PENDING)
            {
                unsigned int progress = filterMessage->getProgress();
                // do not update it to 100%
                if(progress < 100)
                {
                   emit _setProgressValue(progress);
                }
            }
            else if(filterMessage->getStatus() == GISCOMPUTE::FILTER_SUCCESS)
            {
                    emit _setProgressValue(100);
            }
            else
            {
                 if(filterMessage->getStatus() == GISCOMPUTE::FILTER_FAILURE)
                {
                    emit showError("Filter Status", "Failed to apply filter");
                    //Again Set Calculate 
                    _handlePBStopClicked();
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    // XXX - Move these slots to a proper place
    void CutAndFillGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }
        /*Focus UI Handler and activate GUI*/
        try
        {
            if(value)
            {
                QObject* CutAndFillAnalysisObject = _findChild(CutAndFillAnalysisPopup);
                if(CutAndFillAnalysisObject)
                {
                    int i=0;
                    QList<QObject*> eleInputAnalysisList;
                    eleInputAnalysisList.append(new VizComboBoxElement("Select an elevation data (Only tiff Support)",QString::number(i)));
                    CORE::RefPtr<CORE::IWorld> iworld = 
                        APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
                        const CORE::ObjectMap& omap = iworld->getObjectMap();
                        for(CORE::ObjectMap::const_iterator citer = omap.begin();
                            citer != omap.end();
                            ++citer)
                        {
                            CORE::RefPtr<CORE::IObject> object = citer->second;
                            if(object->getInterface<TERRAIN::IElevationObject>())
                            {
                                std::string layerUniqueId = object->getInterface<CORE::IBase>()->getUniqueID().toString();
                                std::string ElevLayerName="";
                                ElevLayerName=object->getInterface<CORE::IBase>()->getName();
                                eleInputAnalysisList.append(new VizComboBoxElement(QString::fromStdString(ElevLayerName), QString::fromStdString(layerUniqueId)));
                            }
                        }
                        _setContextProperty("ElevInputListModel", QVariant::fromValue(eleInputAnalysisList));

                    // start and stop button handlers
                    connect(CutAndFillAnalysisObject, SIGNAL(startAnalysis()),this,SLOT(_handlePBStartClicked()),Qt::UniqueConnection);
                    connect(CutAndFillAnalysisObject, SIGNAL(stopAnalysis()),this, SLOT(_handlePBStopClicked()),Qt::UniqueConnection);
                    connect(CutAndFillAnalysisObject, SIGNAL(markPosition(bool)),this,SLOT(_handlePBAreaClicked(bool)),Qt::UniqueConnection);

                    // enable start button
                    CutAndFillAnalysisObject->setProperty("isStartButtonEnabled",true);

                    //disable stop button
                    CutAndFillAnalysisObject->setProperty("isStopButtonEnabled",false);

                    //disable progress value
                    CutAndFillAnalysisObject->setProperty("progressValue",0);
                }
            }
            else
            {
                _reset();
                _handlePBStopClicked();
            }
            APP::IUIHandler* uiHandler = _uiHandler->getInterface<APP::IUIHandler>();
            uiHandler->setFocus(value);
            if(value)
            {
                _subscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            else
            {
                _unsubscribe(_uiHandler.get(), *GISCOMPUTE::IFilterStatusMessage::FilterStatusMessageType);
            }
            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }

    void CutAndFillGUI::_handleProgressValueChanged(int value)
    {
        QObject* CutAndFillAnalysisObject = _findChild(CutAndFillAnalysisPopup);
        if(CutAndFillAnalysisObject)
        {
            CutAndFillAnalysisObject->setProperty("progressValue",QVariant::fromValue(value));
        } 
    }

    //! 
    void CutAndFillGUI::_handlePBStartClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        QObject* CutAndFillAnalysisObject = _findChild(CutAndFillAnalysisPopup);
        if(CutAndFillAnalysisObject)
        {
            std::string FirstUid = CutAndFillAnalysisObject->property("analysisFirstElevSelectedUid").toString().toStdString();
            if(FirstUid == "0")
            {
                CutAndFillAnalysisObject->setProperty("isStartButtonEnabled", true);
                showError( "Insufficient / Incorrect data", "1st input elevation data not set");
                return;
            }
            std::string SecondUid = CutAndFillAnalysisObject->property("analysisSecondElevSelectedUid").toString().toStdString();
            if(SecondUid == "0")
            {
                CutAndFillAnalysisObject->setProperty("isStartButtonEnabled", true);
                showError("Insufficient / Incorrect data", "2nd input elevation data not set");
                return;
            }
            std::string FirstElevName = CutAndFillAnalysisObject->property("firstElevInputName").toString().toStdString();

            if(FirstElevName == "")
            {
                CutAndFillAnalysisObject->setProperty("isStartButtonEnabled", true);
                showError("Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            
            std::string SecondElevName = CutAndFillAnalysisObject->property("secondElevInputName").toString().toStdString();

            if(SecondElevName == "")
            {
                CutAndFillAnalysisObject->setProperty("isStartButtonEnabled", true);
                showError("Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            //std::string firstElev;
            //size_t FirstPos = FirstElevName.find(".xml");
            //if(FirstPos < FirstElevName.length())
            //{
            //    firstElev = FirstElevName.substr(0,FirstPos);
            //}

            //std::string secondElev;
            //size_t SecondPos = SecondElevName.find(".xml");
            //if(SecondPos < SecondElevName.length())
            //{
            //    secondElev = SecondElevName.substr(0,SecondPos);
            //}
            CORE::UniqueID* uid = new CORE::UniqueID(FirstUid);

            CORE::RefPtr<CORE::IWorld> iworld = 
            APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            CORE::RefPtr<CORE::IObject> FirstObject = iworld->getObjectByID(uid);
            if (!FirstObject.valid())
            {
                CutAndFillAnalysisObject->setProperty("isStartButtonEnabled", true);
                showError("Insufficient / Incorrect data", "tiff file not Present in Project");
                return;
            }
            TERRAIN::IElevationObject* FirstElevObj=FirstObject->getInterface<TERRAIN::IElevationObject>();
            CORE::UniqueID* secondUid = new CORE::UniqueID(SecondUid);
            CORE::RefPtr<CORE::IObject> SecondObject = iworld->getObjectByID(secondUid);
            if (!SecondObject.valid())
            {
                CutAndFillAnalysisObject->setProperty("isStartButtonEnabled", true);
                showError("Insufficient / Incorrect data", "tiff file not Present in Project");
                return;
            }
            TERRAIN::IElevationObject* SecondElevObj=SecondObject->getInterface<TERRAIN::IElevationObject>();

            if(FirstElevObj && SecondElevObj )
            {
                _uiHandler->setInputElevationObject (FirstElevObj,SecondElevObj);
            }
            else
            {
                CutAndFillAnalysisObject->setProperty("isStartButtonEnabled", true);
                showError("Insufficient / Incorrect data", "Courrupted Data");
                return;
            }
            int Uid=3; //Hardcoded Color Set RGB in Filter R,G,B color not giving efficient output
           _uiHandler->setColor(static_cast<vizGISCompute::ICutAndFillFilterVisitor::COLOR>(Uid));
            //get transparency
            QString TempTranparencyVal=CutAndFillAnalysisObject->property("transparencyVal").toString();
            if(TempTranparencyVal == "")
            {
                showError( "Insufficient / Incorrect data", "Transparency Value is not specified");
                return;
            }
            double Transparency = TempTranparencyVal.toDouble();;
            if(Transparency < 0)
            {
                showError( "Insufficient / Incorrect data", "Please enter a Transparency Value  ");
                return;
            }
            //normilization 
            Transparency=Transparency/255;
            //Note in Filter Transparency using as Alpha so passing transparency = 1-Transparency
            Transparency=1-Transparency;
            _uiHandler->setTransparency (Transparency);


            std::string OutputName = CutAndFillAnalysisObject->property("outputName").toString().toStdString();

            if(OutputName == "")
            {
                showError( "Insufficient / Incorrect data", "Output name is not specified");
                return;
            }
            CutAndFillAnalysisObject->setProperty("outputName",QString::fromStdString(OutputName));
            _uiHandler->setOutputName(OutputName);



            osg::Vec4d extents;

            // set area
            {
                bool okx = true;
                extents.x() = CutAndFillAnalysisObject->property("bottomLeftLongitude").toString().toFloat(&okx);

                bool oky = true;
                extents.y() = CutAndFillAnalysisObject->property("bottomLeftLatitude").toString().toDouble(&oky);

                bool okz = true;
                extents.z() = CutAndFillAnalysisObject->property("topRightLongitude").toString().toDouble(&okz);

                bool okw = true;
                extents.w() =  CutAndFillAnalysisObject->property("topRightLatitude").toString().toDouble(&okw);

                bool completeAreaSpecified = okx & oky & okz & okw;

                if(!completeAreaSpecified)// && partialAreaSpecified)
                {
                    showError( "Insufficient / Incorrect data", "Area extent field is missing");
                    return;
                }

                if(completeAreaSpecified)
                {
                    if(extents.x() >= extents.z() || extents.y() >= extents.w())
                    {
                        showError( "Insufficient / Incorrect data", "Area extents are not valid. "
                            "Bottom left coordinates must be less than upper right.");
                        return;
                    }
                    _uiHandler->setExtents(extents);
                }
            }

            CutAndFillAnalysisObject->setProperty("progressValue",0); 
            _handlePBAreaClicked(false);
            // start filter

            _uiHandler->execute();
        }
    }

    void CutAndFillGUI::_handlePBStopClicked()
    {
        if(!_uiHandler.valid())
        {
            return;
        }
        _uiHandler->stopFilter();
        QObject* CutAndFillAnalysisObject = _findChild(CutAndFillAnalysisPopup);
        if(CutAndFillAnalysisObject)
        {
            //disable stop button
            CutAndFillAnalysisObject->setProperty("isStopButtonEnabled",false);
            CutAndFillAnalysisObject->setProperty("isStartButtonEnabled",true);
            CutAndFillAnalysisObject->setProperty("progressValue",QVariant::fromValue(0));
        } 
    }

    void CutAndFillGUI::_handlePBRunInBackgroundClicked()
    {}

    void CutAndFillGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            //            _areaHandler->setHandleMouseClicks(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);

            //           _areaHandler->setBorderWidth(3.0);
            //            _areaHandler->setBorderColor(osg::Vec4(1.0, 1.0, 0.0, 1.0));
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            //_areaHandler->setHandleMouseClicks(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
            
            QObject* CutAndFillAnalysisObject = _findChild(CutAndFillAnalysisPopup);
            if(CutAndFillAnalysisObject)
            {
                CutAndFillAnalysisObject->setProperty( "isMarkSelected" , false );
            }
        }
    }
    void CutAndFillGUI::_reset()
    {
        QObject* CutAndFillAnalysisObject = _findChild(CutAndFillAnalysisPopup);
        if(CutAndFillAnalysisObject)
        {
            CutAndFillAnalysisObject->setProperty("isMarkSelected",false);
            CutAndFillAnalysisObject->setProperty("bottomLeftLongitude","longitude");
            CutAndFillAnalysisObject->setProperty("bottomLeftLatitude","latitude");
            CutAndFillAnalysisObject->setProperty("topRightLongitude","longitude");
            CutAndFillAnalysisObject->setProperty("topRightLatitude","latitude");


            CutAndFillAnalysisObject->setProperty("outputName","");

            CutAndFillAnalysisObject->setProperty("transparencyVal",0);
            CutAndFillAnalysisObject->setProperty("progressValue",0);
            _handlePBAreaClicked(false);
        }
    }

}//namespace SMCQt
