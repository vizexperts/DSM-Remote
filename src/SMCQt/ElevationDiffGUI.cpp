/****************************************************************************
*
* File             : ElevationDiffGUI.h
* Description      : ElevationDiffGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/


#include <Core/WorldMaintainer.h>
#include <Core/ITerrain.h>
#include <Core/IWorld.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouse.h>
#include <VizUI/ISelectionUIHandler.h>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>

#include <SMCQt/ElevationDiffGUI.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ElevationDiffGUI);
    const std::string ElevationDiffGUI::ElevationDiffObjectName = "eleDiffAnalysis";

    ElevationDiffGUI::ElevationDiffGUI()
    {
    }

    ElevationDiffGUI::~ElevationDiffGUI()
    {
    }

     void ElevationDiffGUI::setEleDiffEnable(bool value)
    {
        if(getActive() == value)
            return;

        if(value)
        {
            QObject* eleDiffAnalysisObject = _findChild(ElevationDiffObjectName);
            if(eleDiffAnalysisObject)
            {
                _elevationDiffUIHandler =  APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IElevationDiffUIHandler>(getGUIManager());
                connect(eleDiffAnalysisObject, SIGNAL(markViewPointPosition(bool)),this,SLOT(_handlePBMarkPointFirstClicked(bool)),Qt::UniqueConnection);
                connect(eleDiffAnalysisObject, SIGNAL(markLookAtPosition(bool)),this,SLOT(_handlePBMarkPointSecondClicked(bool)),Qt::UniqueConnection);
            }
        }
        else
        {
            QObject* eleDiffAnalysisObject = _findChild(ElevationDiffObjectName);
            if(eleDiffAnalysisObject)
            {
                eleDiffAnalysisObject->setProperty("isMarkViewPointSelected",false);
                eleDiffAnalysisObject->setProperty("isMarkLookAtSelected",false);
                eleDiffAnalysisObject->setProperty("value","");
            }
            _state = NO_MARK;
            //_elevationDiffUIHandler->reset();
            _elevationDiffUIHandler = NULL;
            _removeObject(_point1.get());
            _removeObject(_point2.get());

            _point1 = NULL;
            _point2 = NULL;

            _setPointHandlerEnabled(false);

        }

        DeclarativeFileGUI::setActive(value);

    }

    void ElevationDiffGUI::_eleDiffCalculation()
    {      
        if(_point1.valid() && _point2.valid())
        {
            CORE::IPoint* FromPoint = _point1->getInterface<CORE::IPoint>();
            CORE::IPoint* ToPoint   = _point2->getInterface<CORE::IPoint>();
            _elevationDiffUIHandler->setPoint1(FromPoint);
            _elevationDiffUIHandler->setPoint2(ToPoint);
            //calcualte Function
            _elevationDiffUIHandler->execute();
            std::string eleVal=_elevationDiffUIHandler->getEleDiffText();
            assert(eleVal.c_str() != NULL);
            QObject* eleDiffAnalysis = _findChild(ElevationDiffObjectName);
            if(eleDiffAnalysis)
            {
                eleDiffAnalysis->setProperty("value", QVariant::fromValue(QString::fromStdString(eleVal)));
                  
            }
        }

    }
    void ElevationDiffGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the PointUIHandler and set it
            try
            {   
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());                
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            _processPoint();
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void ElevationDiffGUI::_processPoint()
    {
        if(!_pointHandler.valid())
        {
            LOG_ERROR("Point UI handler not valid");
        }

        CORE::IPoint* point = _pointHandler->getPoint();

        if(point == NULL)
        {
            LOG_ERROR("Point is not valid");
        }

        CORE::IObject *object = point->getInterface<CORE::IObject>();

        if(object == NULL)
        {
            LOG_ERROR("Point Object is not valid");
        }

        switch(_state)
        {
        case POINT1_MARK: 
            {
                // delete previous point 
                _removeObject(_point1.get());
                _point1 = NULL;

                // get pointer of new point
                _point1 = (point) ? point->getInterface<CORE::IObject>() : NULL;
                _point1->getInterface<CORE::IBase>()->setName("Point 1");
                break;
            }

        case POINT2_MARK: 
            {
                // delete previous point 
                _removeObject(_point2.get());
                _point2 = NULL; 

                // get pointer of new point
                _point2 = (point) ? point->getInterface<CORE::IObject>() : NULL;
                _point2->getInterface<CORE::IBase>()->setName("Point 2");
                break;
            }
        default:
            break;
        }
        _eleDiffCalculation();
    }
    void ElevationDiffGUI::_handlePBMarkPointFirstClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = POINT1_MARK;
            if(POINT2_MARK == lastState)
            {
                QObject* SlopeAnalysis = _findChild(ElevationDiffObjectName);
                if(SlopeAnalysis)
                {
                    SlopeAnalysis->setProperty("isMarkLookAtSelected",false);
                }
            }
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(POINT1_MARK == lastState)
            {   
                _state = NO_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    void ElevationDiffGUI  ::_handlePBMarkPointSecondClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = POINT2_MARK;
            if(POINT1_MARK == lastState)
            {
                QObject* SlopeAnalysis = _findChild(ElevationDiffObjectName);
                if(SlopeAnalysis)
                {
                    SlopeAnalysis->setProperty("isMarkViewPointSelected",false);
                }
            }
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(POINT2_MARK == lastState)
            {
                _state = NO_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }

    }
    void ElevationDiffGUI::_setPointHandlerEnabled(bool enable)
    {
        if(_pointHandler.valid())
        {
            if(enable)
            {
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);                
            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark trans/recv position");
        }
    }
}
