/****************************************************************************
*
* File             : RasterEnhancementChangerGUI.h
* Description      : RasterEnhancementChangerGUI class definition
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************/

#include <SMCQt/RasterEnhancementChangerGUI.h>

#include <App/AccessElementUtils.h>
#include <App/IUndoTransactionFactory.h>
#include <App/ApplicationRegistry.h>
#include <App/IUndoTransaction.h>
#include <App/IUIHandler.h>

#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>
#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/IDeletionUIHandler.h>

#include <Elements/ElementsPlugin.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>

#include <iostream>

#include <osgEarth/ImageLayer>
#include <osgEarthAnnotation/ImageOverlay>

#include <Terrain/IRasterObject.h>
#include <Terrain/IMultiBandRasterObject.h>
#include <gdal_priv.h>
#include <SMCUI/IAddContentUIHandler.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, RasterEnhancementChangerGUI);
    DEFINE_IREFERENCED(RasterEnhancementChangerGUI, SMCQt::DeclarativeFileGUI);

    RasterEnhancementChangerGUI::RasterEnhancementChangerGUI()
    {

    }

    RasterEnhancementChangerGUI::~RasterEnhancementChangerGUI()
    {

    }

    void RasterEnhancementChangerGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();
        CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
        QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());

        QObject* smpMenu = _findChild(SMP_RightMenu);
        if(smpMenu != NULL)
        {
            QObject::connect(smpMenu, SIGNAL(loaded(QString)), this, SLOT(connectSmpMenu(QString)), Qt::UniqueConnection);
        }
    }

    void RasterEnhancementChangerGUI::connectSmpMenu(QString type)
    {
        if(type == "RasterEnhancementChanger")
        {
            QObject* enhancementChangerObject = _findChild("rasterEnhancementChangerObject");
            if(enhancementChangerObject)
            {
                QObject::connect(enhancementChangerObject, SIGNAL(handleApplyButton()), 
                    this, SLOT(handleApplyButton()), Qt::UniqueConnection);
                QObject::connect(enhancementChangerObject, SIGNAL(handleCancelButton()), 
                    this, SLOT(handleCancelButton()), Qt::UniqueConnection);
            }

            if(!_selectionComponent.valid())
                return;

            const CORE::ISelectionComponent::SelectionMap& selectionMap = 
                _selectionComponent->getCurrentSelection();

            if(selectionMap.empty())
            {
                return;
            }

            CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
            CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

            if(!selectedObject)
                return;

            TERRAIN::IRasterObject* raster = selectedObject->getInterface<TERRAIN::IRasterObject>();

            if(raster)
            {
                std::string url = raster->getURL();
                if(url.empty())
                {
                    url = raster->getRuntimeURL();
                }
                std::string ext = osgDB::getFileExtension(url);

                if(ext == "")
                {
                    showError("Invalid Dataset", "This Feature currently doesnt works with directory loading");
                    enhancementChangerObject->setProperty( "rasterValid", QVariant::fromValue(false));
                    return;        

                }
                if(ext == "xyz" || ext == "yahoo" || ext == "wms" || ext == "bing" || ext == "google" )
                {
                    showError("Invalid Dataset", "This Feature doesnt works with Wed Data");
                    enhancementChangerObject->setProperty( "rasterValid", QVariant::fromValue(false));
                    return;        

                }
                if(ext == "tms" )
                {
                    showError("Invalid Dataset", "This Feature doesnt works with TMS Data");
                    enhancementChangerObject->setProperty( "rasterValid", QVariant::fromValue(false));
                    return;        

                }

                CORE::IRasterData* rasterData = raster->getRasterData();
                enhancementChangerObject->setProperty( "rasterValid", QVariant::fromValue(true));

                TERRAIN::IRasterObject::EnhancementType enhanceType = raster->getEnhancementType();

                if(enhanceType == TERRAIN::IRasterObject::NONE)
                {
                    enhancementChangerObject->setProperty( "noneEnabled", QVariant::fromValue(true));
                    enhancementChangerObject->setProperty( "minmaxEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "sdEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "histeqEnabled", QVariant::fromValue(false));

                }
                else if(enhanceType == TERRAIN::IRasterObject::MIN_MAX)
                {
                    enhancementChangerObject->setProperty( "noneEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "minmaxEnabled", QVariant::fromValue(true));
                    enhancementChangerObject->setProperty( "sdEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "histeqEnabled", QVariant::fromValue(false));
                }
                else if(enhanceType == TERRAIN::IRasterObject::STANDARD_DEVIATION)
                {
                    enhancementChangerObject->setProperty( "noneEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "minmaxEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "sdEnabled", QVariant::fromValue(true));
                    enhancementChangerObject->setProperty( "histeqEnabled", QVariant::fromValue(false));
                }
                else if(enhanceType == TERRAIN::IRasterObject::HISTOGRAM_EQUALIZATION)
                {
                    enhancementChangerObject->setProperty( "noneEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "minmaxEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "sdEnabled", QVariant::fromValue(false));
                    enhancementChangerObject->setProperty( "histeqEnabled", QVariant::fromValue(true));
                }

            }
        }
    }

    void RasterEnhancementChangerGUI::handleApplyButton()
    {
        if(!_selectionComponent.valid())
            return;

        const CORE::ISelectionComponent::SelectionMap& selectionMap = 
            _selectionComponent->getCurrentSelection();

        if(selectionMap.empty())
        {
            return;
        }

        CORE::RefPtr<CORE::ISelectable> selectable = selectionMap.begin()->second.get();
        CORE::IObject* selectedObject = selectable->getInterface<CORE::IObject>();

        if(!selectedObject)
            return;

        TERRAIN::IRasterObject* raster = selectedObject->getInterface<TERRAIN::IRasterObject>();
        if(raster)
        {
            CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>() ;
            QObject* item = qobject_cast<QObject*>(qtapp->getRootComponent());

            QObject* enhancementChangerObject = _findChild("rasterEnhancementChangerObject");

            SMCUI::IAddContentUIHandler* addContentUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

            if(addContentUIHandler)
            {
                bool noneEnabled = enhancementChangerObject->property("noneEnabled").toBool();
                bool minmaxEnabled = enhancementChangerObject->property("minmaxEnabled").toBool();
                bool sdEnabled = enhancementChangerObject->property("sdEnabled").toBool();
                bool histeqEnabled = enhancementChangerObject->property("histeqEnabled").toBool();

                if(noneEnabled)
                {
                    addContentUIHandler->changeRasterEnhancementType(selectedObject,TERRAIN::IRasterObject::NONE);
                }
                else if(minmaxEnabled)
                {
                    addContentUIHandler->changeRasterEnhancementType(selectedObject,TERRAIN::IRasterObject::MIN_MAX);
                }
                else if(sdEnabled)
                {
                    addContentUIHandler->changeRasterEnhancementType(selectedObject,TERRAIN::IRasterObject::STANDARD_DEVIATION);
                }
                else if(histeqEnabled)
                {
                    addContentUIHandler->changeRasterEnhancementType(selectedObject,TERRAIN::IRasterObject::HISTOGRAM_EQUALIZATION);
                }
            }

        }

        return;

    }

    void RasterEnhancementChangerGUI::handleCancelButton()
    {

    }
    void RasterEnhancementChangerGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager<APP::IGUIManager>(getGUIManager());

                const CORE::ComponentMap& cmap = wmain->getComponentMap();
                _selectionComponent = CORE::InterfaceUtils::getFirstOf<CORE::IComponent, CORE::ISelectionComponent>(cmap);
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            SMCQt::DeclarativeFileGUI::update(messageType, message);
        }
    }


} // namespace SMCQt