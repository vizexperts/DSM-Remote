#include <SMCQt/QMLDirectoryTreeModel.h>

namespace SMCQt
{
    QHash<int, QByteArray> QMLDirectoryTreeModel::roleNames() const
    {
        QHash<int, QByteArray> roles = QMLTreeModel::roleNames();
        roles.insert(NameRole, QByteArray("name"));
        roles.insert(IsDirRole, QByteArray("isDir"));

        return roles;
    }

    QVariant QMLDirectoryTreeModel::data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        if (index.row() > (_items.size() - 1))
            return QVariant();

        QMLDirectoryTreeModelItem *item = dynamic_cast<QMLDirectoryTreeModelItem*>(_items.at(index.row()));

        switch (role)
        {
        case Qt::DisplayRole:
        case NameRole:
            if (!_showExtensions)
            {
                return QVariant::fromValue(item->getFileInfo().baseName());
            }
            else
            {
                return QVariant::fromValue(item->getFileInfo().fileName());
            }
        case IsDirRole:
            return QVariant::fromValue(item->getFileInfo().isDir());
        default:
            return QMLTreeModel::data(index, role);
        }
    }

    bool QMLDirectoryTreeModel::customizeTree(QString searchString, SMCQt::QMLTreeModelItem *item,
        SMCQt::QMLTreeModelItem *parent)
    {
        if (!item)
        {
            return false;
        }

        QMLDirectoryTreeModelItem* elementListItem = dynamic_cast<QMLDirectoryTreeModelItem*>(item);
        if (!elementListItem)
        {
            return false;
        }

        bool isVisible = false;

        if (elementListItem->getFileInfo().baseName().contains(searchString, Qt::CaseInsensitive))
        {
            isVisible = true;
        }

        int index = indexOf(item);

        closeItem(index);

        openItem(index);

        if (searchString == "")
        {
            return false;
        }

        foreach(QMLTreeModelItem* childItem, item->getChildren())
        {
            bool childVisibility = customizeTree(searchString, childItem, item);

            isVisible = isVisible || childVisibility;
        }

        if (!isVisible && parent)
        {
            if ((index > -1) && (index < _items.size()))
            {
                beginRemoveRows(QModelIndex(), index, index);
                _items.removeAt(index);
                endRemoveRows();
            }
        }

        return isVisible;
    }

    //void QMLDirectoryTreeModel::intialize(QDir directory)
    //{
    //    _directory = directory;

    //    _populateDir(_directory, item, )
    //}

}