/****************************************************************************
*
* File             : QueryBuilderGISGUI.h
* Description      : QueryBuilderGISGUI class definition
*
*****************************************************************************
* Copyright (c) 2013-2014, CAIR, DRDO
*****************************************************************************/
#include <iostream>
#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <Core/WorldMaintainer.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <SMCQt/QueryBuilderGISGUI.h>
#include <Database/IDBRecord.h>
#include <Database/IDBTable.h>
#include <Database/IDBField.h>
#include <Elements/ElementsUtils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, QueryBuilderGISGUI);
    DEFINE_IREFERENCED(QueryBuilderGISGUI, SMCQt::DeclarativeFileGUI);

    QueryBuilderGISGUI::QueryBuilderGISGUI(): _toSearch("")
    {
    }

    QueryBuilderGISGUI::~QueryBuilderGISGUI()
    {
        setActive(false);
    }

    void QueryBuilderGISGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this, SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }
    }

    void QueryBuilderGISGUI::connectPopupLoader(QString type)
    {
        if(type == "QueryBuilderGIS")
        {
            setActive(true);
            QObject* queryBuilderObject = _findChild("QueryBuilderGIS");
            if(queryBuilderObject)
            {
                _currentUIType =  queryBuilderObject->property("uiType").toString().toStdString();
                connect(queryBuilderObject, SIGNAL(addToWhere(int)), this, SLOT(_handleAddToWhereButtonClicked(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(addValueToWhere(int)), this, SLOT(addValueToWhere(int)), Qt::UniqueConnection);

                connect(queryBuilderObject, SIGNAL(showAllValues(int)), this, SLOT(showAllValues(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(changeText(QString)), this, SLOT(changeText(QString)), Qt::UniqueConnection);

                connect(queryBuilderObject, SIGNAL(showSampleValues(int)), this, SLOT(showSampleValues(int)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(changeText(const QString&)), this, SLOT(changeText(const QString&)), Qt::UniqueConnection);

                connect(queryBuilderObject, SIGNAL(markPosition(bool)), this, SLOT(_handlePBAreaClicked(bool)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(operatorButtonClicked(QString)), this, SLOT(_handleOperatorButtonClicked(QString)), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(clearButtonClicked()), this, SLOT(_handleClearButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(okButtonClicked()), this, SLOT(_handleOkButtonClicked()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(populateFieldDefinitions()), this, SLOT(_populateFiledDefinition()), Qt::UniqueConnection);
                connect(queryBuilderObject, SIGNAL(closed()), this, SLOT(_queryBuilderClosed()), Qt::UniqueConnection);
                //connect(queryBuilderObject, SIGNAL(selectedFieldDeletion(int)), this, SLOT(_handleDeleteFromSelectedFields(int )), Qt::UniqueConnection);


                //!
                _populateFiledDefinition();

                // connectivity of all the buttons
                //connect(queryBuilderObject, SIGNAL(addToSelectedFields(int)), this, SLOT(_handleAddToSelectButtonClicked(int)), Qt::UniqueConnection);
            }
        }
        if(type == "QueryBuilderGISTable")
        {
            QObject* QueryBuilderGISTableObject = _findChild("QueryBuilderGISTable");
            if(QueryBuilderGISTableObject)
            {
                connect(QueryBuilderGISTableObject, SIGNAL(tableOkButtonClicked()), this, SLOT(_handleTableOkButtonClicked()), Qt::UniqueConnection);
                // connect signal and slots for QueryBuilderGISTable
            }
        }
    }

    void QueryBuilderGISGUI::addValueToWhere(int index)
    {
        QObject* QueryBuilderGIS = _findChild("QueryBuilderGIS");
        if(QueryBuilderGIS)
        {
            if(index != -1)
            {
                QString fieldValue = _currentFieldValues.at(index);
                std::string whereClause = QueryBuilderGIS->property("sqlWhereText").toString().toStdString();
                whereClause.append("\'");
                whereClause.append(fieldValue.toStdString());
                whereClause.append("\' ");
                QueryBuilderGIS->setProperty("sqlWhereText", whereClause.c_str());
            }
        }
    }

    void QueryBuilderGISGUI::changeText(const QString& leToSearch)
    {
        std::string toSearch = leToSearch.toStdString();
        _toSearch = toSearch;
    }

    std::string QueryBuilderGISGUI::_getDatabaseColumnName(int index)
    {
            std::string columnDisplayName = _allFieldDefinitions.at(index).toStdString();
                
            SMCElements::IIncidentLoaderComponent::IncidentTypeMap::const_iterator iter = _incidentLoaderComponent->getIncidentMap().find(_currentIncidentName);
            if(iter == _incidentLoaderComponent->getIncidentMap().end())
            {
                emit showError("Query Builder", "Incident type not found.");
                LOG_ERROR("Incident type " + _currentIncidentName + " not found");

                return "";
            }

            const SMCElements::IIncidentLoaderComponent::IncidentType& incidentType = iter->second;

            std::map<std::string, std::string>::const_iterator selectIter = incidentType.select.begin();            
            for(; selectIter != incidentType.select.end(); ++selectIter)
            {
                if(selectIter->second == columnDisplayName)
                    break;
            }
            
            return selectIter->first;
    }

    void QueryBuilderGISGUI::showAllValues(int index)
    {

        QObject* QueryBuilderGIS = _findChild("QueryBuilderGIS");
        if(!QueryBuilderGIS)
            return;
        CORE::RefPtr<DATABASE::ISQLQueryResult> result;

        if(_currentUIType == "oracle")
        {
            if(_currentUserspaceName.empty() || _currentTableName.empty() || _allFieldDefinitions.empty())
                return;

            if(!_databaseLayerLoaderUIHandler.valid())
                return;

            std::string columnName = _allFieldDefinitions.at(index).toStdString();
            std::string queryStatement;

            //! Set table definition fields
            queryStatement = "SELECT DISTINCT " + columnName +  " FROM " + _currentUserspaceName + "." + _currentTableName;
            result = _databaseLayerLoaderUIHandler->executeSQLQuery(queryStatement, SMCUI::IDatabaseLayerLoaderUIHandler::DBType_Oracle);

        }
        else if(_currentUIType == "incident")
        {
            if(_currentIncidentName.empty())
                return;


            if(!_incidentLoaderComponent.valid())
                return;

            QString columnName = QString(_getDatabaseColumnName(index).c_str());

            QStringList columnNameTokens = columnName.split(".");
            if(columnNameTokens.length() != 2)
            {
                LOG_ERROR("Column name error." + columnName.toStdString() + " is not valid.");
                return;
            }

            std::string table = columnNameTokens.at(0).toStdString();
            std::string column = columnNameTokens.at(1).toStdString();

            std::string queryStatement;
           // queryStatement = "SELECT DISTINCT " + column + " FROM " + table + " ORDER BY " + column;
            queryStatement = "SELECT DISTINCT " + column + " FROM " + table + " WHERE " + column + " LIKE " + "'%" + _toSearch + "%'" ;
            result = _databaseLayerLoaderUIHandler->executeSQLQuery(queryStatement, SMCUI::IDatabaseLayerLoaderUIHandler::DBType_MSSQL);
        }

        if(!result.valid())
        {
            LOG_ERROR("Query unsuccesfull!! " + _allFieldDefinitions.at(index).toStdString());
            return;
        }

        CORE::RefPtr<DATABASE::IDBTable> table = result->getResultTable();
        if(!table.valid())
        {
            LOG_ERROR("No values!! " + _allFieldDefinitions.at(index).toStdString());
            return;
        }

        _currentFieldValues.clear();

        int count = 0;

        CORE::RefPtr<DATABASE::IDBRecord> record = NULL;
        while((record = table->getNextRecord()) != NULL)
        {
            DATABASE::IDBField* field = record->getFieldByIndex(0);
            switch(field->getFieldType())
            {
            case DATABASE::FieldData::STRING :
                _currentFieldValues.append(QString::fromStdString(field->toString()) );
                break;
            case DATABASE::FieldData::FLOAT :
                _currentFieldValues.append(QString::number(field->toFloat()));
                break;
            case DATABASE::FieldData::DOUBLE :
                _currentFieldValues.append(QString::number(field->toDouble()));
                break;
            case DATABASE::FieldData::INT :
                _currentFieldValues.append(QString::number(field->toInt()));
                break;
            case DATABASE::FieldData::INT64:
                _currentFieldValues.append(QString::number(field->toInt64()));
                break;
            }
            ++count;
        }

        _setContextProperty("distinctValuesList", QVariant::fromValue(_currentFieldValues));

    }

    void QueryBuilderGISGUI::showSampleValues(int index)
    {
        showAllValues(index);    
    }

    void QueryBuilderGISGUI::_queryBuilderClosed()
    {
        _handlePBAreaClicked(false);
        _allFieldDefinitions.clear();
        _currentFieldValues.clear();
        _setContextProperty("allFieldsList", QVariant::fromValue(NULL));
        _setContextProperty("distinctValuesList", QVariant::fromValue(NULL));
    }

    void QueryBuilderGISGUI::_populateFiledDefinition()
    {
        QObject* QueryBuilderGIS = _findChild("QueryBuilderGIS");

        if(QueryBuilderGIS == NULL)
            return;
        if(_currentUIType == "oracle")
        {
            _currentUserspaceName = QueryBuilderGIS->property("userspaceName").toString().toStdString();
            _currentTableName = QueryBuilderGIS->property("tableName").toString().toStdString();
            //_currentIncidentName = QueryBuilderGIS->property("incidentName").toString().toStdString();

            std::string queryStatement;
            CORE::RefPtr<DATABASE::ISQLQueryResult> result;
            //! Set table definition fields
            if(!_databaseLayerLoaderUIHandler.valid())
            {
                LOG_ERROR("DatabaseLayerLoaderUIHandler is not valid.");
                return;
            }

            if(_currentUserspaceName.empty() || _currentTableName.empty() || !_allFieldDefinitions.empty())
                return;

            queryStatement = "SELECT column_name from all_tab_columns WHERE owner = '" + _currentUserspaceName +"' AND table_name = '" + _currentTableName +"'";
            result = _databaseLayerLoaderUIHandler->executeSQLQuery(queryStatement, SMCUI::IDatabaseLayerLoaderUIHandler::DBType_Oracle);

            if(!result.valid())
                return;

            CORE::RefPtr<DATABASE::IDBTable> table = result->getResultTable();
            if(!table.valid())
                return;

            _allFieldDefinitions.clear();

            CORE::RefPtr<DATABASE::IDBRecord> record = NULL;
            while((record = table->getNextRecord()) != NULL)
            {
                DATABASE::IDBField* field = record->getFieldByIndex(0);

                _allFieldDefinitions.append(QString::fromStdString(field->toString()) );
            }

        }
        else if(_currentUIType == "incident")
        {
            _currentIncidentName = QueryBuilderGIS->property("incidentName").toString().toStdString();

            //! Set table definition fields
            if(!_incidentLoaderComponent.valid())
            {
                LOG_ERROR("IncidentLayerLoaderComponent is not valid.");
                return;
            }

            if(_currentIncidentName.empty())
                return;

            SMCElements::IIncidentLoaderComponent::IncidentTypeMap::const_iterator iter = _incidentLoaderComponent->getIncidentMap().find(_currentIncidentName);
            if(iter == _incidentLoaderComponent->getIncidentMap().end())
            {
                emit showError("Query Builder", "Incident type not found.");
                LOG_ERROR("Incident type " + _currentIncidentName + " not found");

                return;
            }

            const SMCElements::IIncidentLoaderComponent::IncidentType& incidentType = iter->second;

            std::map<std::string, std::string>::const_iterator selectIter = incidentType.select.begin();            
            for(; selectIter != incidentType.select.end(); ++selectIter)
            {
                if(incidentType.skipList.find(selectIter->second) == std::string::npos)
                    _allFieldDefinitions.append(QString::fromStdString(selectIter->second));
            }
        }

        _setContextProperty("allFieldsList", QVariant::fromValue(_allFieldDefinitions));
    }

    void QueryBuilderGISGUI::_handleTableOkButtonClicked()
    {
        ///_selectedFieldDefinitions.clear();
        //
    }

    void QueryBuilderGISGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                // set the remaining handlers for further use
                _databaseLayerLoaderUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IDatabaseLayerLoaderUIHandler>(getGUIManager());
                CORE::IComponent* comp = ELEMENTS::GetComponentFromMaintainer("IncidentLoaderComponent");
                if(comp)
                {
                    _incidentLoaderComponent = comp->getInterface<SMCElements::IIncidentLoaderComponent>();
                }
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());

            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // check whether the polygon has been updated
        else if(messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if(_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if(line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first    = line->getPoint(0)->getValue();
                    osg::Vec3d second   = line->getPoint(2)->getValue();
                    QObject* queryBuilderObject = _findChild("QueryBuilderGIS");
                    if(queryBuilderObject)
                    {
                        queryBuilderObject->setProperty("bottomLeftLongitude",UTIL::ToString(first.x()).c_str());
                        queryBuilderObject->setProperty("bottomLeftLatitude",UTIL::ToString(first.y()).c_str());
                        queryBuilderObject->setProperty("topRightLongitude",UTIL::ToString(second.x()).c_str());
                        queryBuilderObject->setProperty("topRightLatitude",UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }
    }

    void QueryBuilderGISGUI::_handlePBAreaClicked(bool pressed)
    { 
        if(!_areaHandler.valid())
        {
            return;
        }

        if(pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

            QObject* queryBuilderObject = _findChild("QueryBuilderGIS");
            if(queryBuilderObject)
            {
                queryBuilderObject->setProperty( "isMarkSelected" , false );
            }
        }
    }

    void QueryBuilderGISGUI::_handleOperatorButtonClicked(QString buttonName)
    {
        QObject* queryBuilderObject = _findChild("QueryBuilderGIS");
        if(queryBuilderObject)
        {
            std::string whereClause = queryBuilderObject->property("sqlWhereText").toString().toStdString();
            whereClause.append(buttonName.toStdString());
            queryBuilderObject->setProperty("sqlWhereText", whereClause.c_str());
        }
    }


    void QueryBuilderGISGUI::_handleAddToWhereButtonClicked(int index)
    {
        QObject* queryBuilderObject = _findChild("QueryBuilderGIS");
        if(queryBuilderObject)
        {
            if(index != -1)
            {
                //std::string uiType =  queryBuilderObject->property("uiType").toString().toStdString();
                std::string whereClause = queryBuilderObject->property("sqlWhereText").toString().toStdString();
                whereClause.append(_getDatabaseColumnName(index));

                whereClause.append(" ");
                queryBuilderObject->setProperty("sqlWhereText", whereClause.c_str());
            }
        }
    }

    void QueryBuilderGISGUI::_handleOkButtonClicked()
    {
        QString whereClause;
        osg::Vec4d extents;

        QObject* QueryBuilderGIS = _findChild("QueryBuilderGIS");
        if(QueryBuilderGIS)
        {
            whereClause = QueryBuilderGIS->property("sqlWhereText").toString();

            bool okX = true;
            extents.x() = QueryBuilderGIS->property("bottomLeftLongitude").toString().toDouble(&okX);

            bool okY = true;
            extents.y() = QueryBuilderGIS->property("bottomLeftLatitude").toString().toDouble(&okY);

            bool okZ = true;
            extents.z() = QueryBuilderGIS->property("topRightLongitude").toString().toDouble(&okZ);

            bool okW = true;
            extents.w() = QueryBuilderGIS->property("topRightLatitude").toString().toDouble(&okW);

            bool completedAreaSpecified = okX & okY & okZ & okW;
            bool partialAreaSpecified = okX | okY | okZ | okW;

            if(!completedAreaSpecified && partialAreaSpecified)
            {
                showError( "Area Extents Error", "Please Enter Valid Extents Or Remain All Empty");
                return;
            }

            if(completedAreaSpecified)
            {
                if(extents.x() >= extents.z() || extents.y() >= extents.w())
                {
                    showError("Area Extents Error", "Bottom left coordinates must be less than upper right.");
                    return;
                }
            }
            std::string uiType =  QueryBuilderGIS->property("uiType").toString().toStdString();
            QObject* addDatabaseLayerPopup;
            if(uiType == "oracle")
                addDatabaseLayerPopup = _findChild("addDatabaseLayerPopup");
            else if(uiType == "incident")
                addDatabaseLayerPopup = _findChild("addIncidentDataPopup");

            if(addDatabaseLayerPopup)
            {
                addDatabaseLayerPopup->setProperty("whereClause", QVariant::fromValue(whereClause));

                if(completedAreaSpecified)
                {
                    addDatabaseLayerPopup->setProperty("bottomLeftLong", QVariant::fromValue(extents.x()));
                    addDatabaseLayerPopup->setProperty("bottomLeftLat", QVariant::fromValue(extents.y()));
                    addDatabaseLayerPopup->setProperty("topRightLong", QVariant::fromValue(extents.z()));
                    addDatabaseLayerPopup->setProperty("topRightLat", QVariant::fromValue(extents.w()));
                }
            }
        }


        _setContextProperty("allFieldsList", QVariant::fromValue(NULL));
        _setContextProperty("distinctValuesList", QVariant::fromValue(NULL));
    }

    void QueryBuilderGISGUI::_handleClearButtonClicked()
    {
        QObject* queryBuilderObject = _findChild("QueryBuilderGIS");
        if(queryBuilderObject)
        {
            //_selectedFieldDefinitions.clear();
            //_setContextProperty("selectedFieldsList", QVariant::fromValue(_selectedFieldDefinitions));
            queryBuilderObject->setProperty("sqlWhereText", "");
            queryBuilderObject->setProperty("isMarkSelected", false);
            queryBuilderObject->setProperty("bottomLeftLongitude", "");
            queryBuilderObject->setProperty("bottomLeftLatitude", "");
            queryBuilderObject->setProperty("topRightLongitude", "");
            queryBuilderObject->setProperty("topRightLatitude", "");

        }
    }
}