/****************************************************************************
*
* File             : SettingsGUI.h
* Description      : SettingsGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/SettingsGUI.h>

#include <App/AccessElementUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>
#include <serialization/StreamOperator.h>
#include <serialization/AsciiStreamOperator.h>

#include <DB/WriteFile.h>
#include <DB/ReadFile.h>

#include <DB/ReaderWriter.h>

#include <Elements/ElementsUtils.h>

#include <Util/FileUtils.h>

#include <QFileDialog>
#include <QSettings>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, SettingsGUI);
    DEFINE_IREFERENCED(SettingsGUI, DeclarativeFileGUI);

    const std::string SettingsGUI::UserSettingsMenu = "userSettingsMenu";

    SettingsGUI::SettingsGUI()
    {
    }

    SettingsGUI::~SettingsGUI()
    {
    }

    void SettingsGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(connectPopupLoader(QString)), Qt::UniqueConnection);
        }

    }

    void SettingsGUI::connectPopupLoader(QString type)
    {
        if(QString::compare(type, QString::fromStdString(UserSettingsMenu) , Qt::CaseInsensitive) == 0)
        {
            QObject* userSettingsMenu = _findChild(UserSettingsMenu);
            if(userSettingsMenu)
            {
                populateProjectSettingsMenu();

                QObject::connect(userSettingsMenu, SIGNAL(okButtonClicked()), this,
                    SLOT(okButtonClicked()), Qt::UniqueConnection);

                QObject::connect(userSettingsMenu, SIGNAL(browseRasterData()), this,
                    SLOT(browseRasterData()), Qt::UniqueConnection);

                QObject::connect(userSettingsMenu, SIGNAL(browseElevationData()), this,
                    SLOT(browseElevationData()), Qt::UniqueConnection);
            }
        }
    }

    void SettingsGUI::populateProjectSettingsMenu()
    {
        if(!_globalSettingComponent.valid())
            return;

        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            _setProjectType();

            _setProjectSetting();

            _setLayerDatabaseSetting();

            _setWebserverSetting();

            _setLocalDataSetting();

            userSettingsMenu->setProperty("reloadProjectsList", QVariant::fromValue(false));
        }
    }

    void SettingsGUI::_setProjectType()
    {
        if(!_globalSettingComponent.valid())
            return;

        ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = _globalSettingComponent->
            getGlobalSetting(ELEMENTS::ISettingComponent::PROJECTISLOCAL);

        bool isLocal = true;

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "isLocal"))
            isLocal = (settingAttrib.keyValuePair["isLocal"] == "true");

        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            userSettingsMenu->setProperty("isProjectLocal", isLocal);
        }

    }

    void SettingsGUI::_setProjectSetting()
    {
        if(!_globalSettingComponent.valid())
            return;

        std::string projectDatabaseName;
        std::string projectDatabaseUsername;
        std::string projectDatabasePassword;
        std::string projectDatabaseHost;
        std::string projectDatabasePort;

        //Get the Project Settings
        ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = 
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::PROJECTDATABASECONN);

        //Set the Project setting
        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Database"))
            projectDatabaseName = settingAttrib.keyValuePair["Database"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Host"))
            projectDatabaseHost = settingAttrib.keyValuePair["Host"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Port"))
            projectDatabasePort = settingAttrib.keyValuePair["Port"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Username"))
            projectDatabaseUsername = settingAttrib.keyValuePair["Username"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Password"))
            projectDatabasePassword = settingAttrib.keyValuePair["Password"];

        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            userSettingsMenu->setProperty("projectDatabaseName", projectDatabaseName.c_str());
            userSettingsMenu->setProperty("projectDatabaseUsername", projectDatabaseUsername.c_str());
            userSettingsMenu->setProperty("projectDatabasePassword", projectDatabasePassword.c_str());
            userSettingsMenu->setProperty("projectDatabaseHost", projectDatabaseHost.c_str());
            userSettingsMenu->setProperty("projectDatabasePort", projectDatabasePort.c_str());
        }

    }

    void SettingsGUI::_setLayerDatabaseSetting()
    {
        if(!_globalSettingComponent.valid())
            return;

        std::string layerDatabaseName;
        std::string layerDatabaseUsername;
        std::string layerDatabasePassword;
        std::string layerDatabaseHost;
        std::string layerDatabasePort;

        //Get the Project Settings
        ELEMENTS::ISettingComponent::SettingsAttributes settingAttrib = 
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::LAYERDATABASECONN);

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Database"))
            layerDatabaseName = settingAttrib.keyValuePair["Database"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Host"))
            layerDatabaseHost = settingAttrib.keyValuePair["Host"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Port"))
            layerDatabasePort = settingAttrib.keyValuePair["Port"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Username"))
            layerDatabaseUsername = settingAttrib.keyValuePair["Username"];

        if(_presentInGlobalSettingMap(settingAttrib.keyValuePair, "Password"))
            layerDatabasePassword = settingAttrib.keyValuePair["Password"];

        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            userSettingsMenu->setProperty("layerDatabaseName", layerDatabaseName.c_str());
            userSettingsMenu->setProperty("layerDatabaseUsername", layerDatabaseUsername.c_str());
            userSettingsMenu->setProperty("layerDatabasePassword", layerDatabasePassword.c_str());
            userSettingsMenu->setProperty("layerDatabaseHost", layerDatabaseHost.c_str());
            userSettingsMenu->setProperty("layerDatabasePort", layerDatabasePort.c_str());
        }
    }

    void SettingsGUI::_setWebserverSetting()
    {
        if(!_globalSettingComponent.valid())
            return;

        std::string webserverServer;
        std::string webserverPath;
        std::string webserverResource;
        std::string webserverUsername;
        std::string webserverPassword;

        ELEMENTS::ISettingComponent::SettingsAttributes setting = 
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if(_presentInGlobalSettingMap(setting.keyValuePair, "Server"))
            webserverServer = setting.keyValuePair["Server"];

        if(_presentInGlobalSettingMap(setting.keyValuePair, "Username"))
            webserverUsername = setting.keyValuePair["Username"];

        if(_presentInGlobalSettingMap(setting.keyValuePair, "Password"))
            webserverPassword = setting.keyValuePair["Password"];

        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            userSettingsMenu->setProperty("webserverServer", webserverServer.c_str());
            userSettingsMenu->setProperty("webserverUsername", webserverUsername.c_str());
            userSettingsMenu->setProperty("webserverPassword", webserverPassword.c_str());
        }
    }

    void SettingsGUI::_setLocalDataSetting()
    {
        if(!_globalSettingComponent.valid())
            return;

        std::string localRasterData;
        std::string localElevationData;
        std::string localVectorData;

        ELEMENTS::ISettingComponent::SettingsAttributes setting = 
            _globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::LOCAL_DATA_LOCATION);

        if(_presentInGlobalSettingMap(setting.keyValuePair, "Raster"))
            localRasterData = setting.keyValuePair["Raster"];

        if(_presentInGlobalSettingMap(setting.keyValuePair, "Elevation"))
            localElevationData = setting.keyValuePair["Elevation"];

        if(_presentInGlobalSettingMap(setting.keyValuePair, "Vector"))
            localVectorData = setting.keyValuePair["Vector"];

        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            userSettingsMenu->setProperty("localRasterLocation", localRasterData.c_str());
            userSettingsMenu->setProperty("localElevationLocation", localElevationData.c_str());
            //userSettingsMenu->setProperty("localVectorLocation", localVectorData.c_str());
        }
    }

    void SettingsGUI::okButtonClicked()
    {
        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        {
            {
                // read values from gui and apply
                _loadBusyBar(boost::bind(&SettingsGUI::_executeWriteSettings, this), "Saving settings.", true);
            }
        }
    }

    void SettingsGUI::browseRasterData()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";

        QString caption = "Raster Folder";

        QString filepath = QFileDialog::getExistingDirectory(parent,caption,directory);
        if(rasterLastPath.empty())
            rasterCurrentPath = ""+filepath.toStdString();
        else
        {
            if(rasterCurrentPath.empty())
                rasterCurrentPath = rasterLastPath+";"+filepath.toStdString();
            else
                rasterCurrentPath = rasterCurrentPath+";"+filepath.toStdString();
        }

        rasterLastPath = filepath.toStdString();
        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            std::string localRasterLocation = 
                userSettingsMenu->property("localRasterLocation").toString().toStdString();

            if(localRasterLocation.empty())
            {
                rasterCurrentPath = rasterLastPath;
                rasterCurrentPath = osgDB::convertFileNameToNativeStyle(rasterCurrentPath);
                userSettingsMenu->setProperty("localRasterLocation",
                    QVariant::fromValue(QString::fromStdString(rasterCurrentPath)));
            }
            else
            {
                rasterCurrentPath =  localRasterLocation + ";" + rasterLastPath;
                rasterCurrentPath = osgDB::convertFileNameToNativeStyle(rasterCurrentPath);
                userSettingsMenu->setProperty("localRasterLocation",
                    QVariant::fromValue(QString::fromStdString(rasterCurrentPath)));
            }
        }
    }

    void SettingsGUI::browseElevationData()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption = "Elevation Folder";

        QString filepath = QFileDialog::getExistingDirectory(parent,caption,directory);

        if(elevationLastPath.empty())
            elevationCurrentPath = ""+filepath.toStdString();
        else
        {
            if(elevationCurrentPath.empty())
                elevationCurrentPath = elevationLastPath+";"+filepath.toStdString();
            else
                elevationCurrentPath = elevationCurrentPath+";"+filepath.toStdString();
        }

        elevationLastPath = filepath.toStdString();

        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            std::string localElevationLocation = userSettingsMenu->property("localElevationLocation").toString().toStdString();
            if(localElevationLocation.empty())
            {
                elevationCurrentPath = elevationLastPath;
                elevationCurrentPath = osgDB::convertFileNameToNativeStyle(elevationCurrentPath);
                userSettingsMenu->setProperty("localElevationLocation",
                    QVariant::fromValue(QString::fromStdString(elevationCurrentPath)));
            }
            else
            {
                elevationCurrentPath =  localElevationLocation + ";" + elevationLastPath;
                elevationCurrentPath = osgDB::convertFileNameToNativeStyle(elevationCurrentPath);
                userSettingsMenu->setProperty("localElevationLocation",
                    QVariant::fromValue(QString::fromStdString(elevationCurrentPath)));
            }
            //elevationCurrentPath = osgDB::convertFileNameToNativeStyle(elevationCurrentPath);
            //userSettingsMenu->setProperty("localElevationLocation", 
            //QVariant::fromValue(QString::fromStdString(elevationCurrentPath)));
        }
    }

    void SettingsGUI::_executeWriteSettings()
    {
        QObject* userSettingsMenu = _findChild(UserSettingsMenu);
        if(userSettingsMenu)
        {
            bool isLocal = userSettingsMenu->property("isProjectLocal").toBool();

            QString projectDatabaseName = userSettingsMenu->property("projectDatabaseName").toString();
            QString projectDatabaseUsername = userSettingsMenu->property("projectDatabaseUsername").toString();
            QString projectDatabasePassword = userSettingsMenu->property("projectDatabasePassword").toString();
            QString projectDatabaseHost = userSettingsMenu->property("projectDatabaseHost").toString();
            QString projectDatabasePort = userSettingsMenu->property("projectDatabasePort").toString();

            QString layerDatabaseName = userSettingsMenu->property("layerDatabaseName").toString();
            QString layerDatabaseUsername = userSettingsMenu->property("layerDatabaseUsername").toString();
            QString layerDatabasePassword = userSettingsMenu->property("layerDatabasePassword").toString();
            QString layerDatabaseHost = userSettingsMenu->property("layerDatabaseHost").toString();
            QString layerDatabasePort = userSettingsMenu->property("layerDatabasePort").toString();

            QString webserverServer = userSettingsMenu->property("webserverServer").toString();
            QString webserverUsername = userSettingsMenu->property("webserverUsername").toString();
            QString webserverPassword = userSettingsMenu->property("webserverPassword").toString();

            QString localRasterData = userSettingsMenu->property("localRasterLocation").toString();
            QString localElevationData= userSettingsMenu->property("localElevationLocation").toString();
            //QString localVectorData = userSettingsMenu->property("localVectorLocation").toString();

            // set these values to the component

            if(!_globalSettingComponent.valid())
            {
                LOG_ERROR("Settings component not valid.");
                return;
            }

            _globalSettingComponent->setLocalProject(isLocal);

            if( (!projectDatabaseName.isEmpty()) && (!projectDatabaseHost.isEmpty()) && 
                (!projectDatabasePort.isEmpty()) && (!projectDatabaseUsername.isEmpty()) ) 
            {
                _globalSettingComponent->setProjectSetting(projectDatabaseName.toStdString()
                    ,  projectDatabaseUsername.toStdString(), projectDatabasePassword.toStdString(),
                    projectDatabaseHost.toStdString(), projectDatabasePort.toStdString());
            }

            if( (!webserverServer.isEmpty()) )
            {
                _globalSettingComponent->setWebServerSetting(webserverServer.toStdString(),
                    webserverUsername.toStdString(), webserverPassword.toStdString());
            }

            if( (!layerDatabaseName.isEmpty()) && (! layerDatabasePort.isEmpty() ) && 
                ( !layerDatabaseHost.isEmpty()) )
            {
                _globalSettingComponent->setLayerDatabaseSetting(layerDatabaseName.toStdString()
                    , layerDatabaseUsername.toStdString(), layerDatabasePassword.toStdString(),
                    layerDatabaseHost.toStdString(), layerDatabasePort.toStdString());
            }

            {
                _globalSettingComponent->setLocalDataSetting(localRasterData.toStdString(),
                    localElevationData.toStdString());
            }

            _writeSettings();
        }
    }

    void SettingsGUI::_writeSettings()
    {
        DB::BaseWrapper *wrapper   = DB::BaseWrapperManager::instance()->findWrapper("SettingComponent");
        if(wrapper)
        {
            CORE::IComponent *settingComponent = ELEMENTS::GetComponentFromMaintainer("SettingComponent");
            CORE::IBase* base = NULL;
            if(settingComponent)
            {
                base = settingComponent->getInterface<CORE::IBase>();

                std::stringstream inputstream;
                CORE::RefPtr<DB::OutputIterator> oi = new AsciiOutputIterator(&inputstream);

                DB::OutputStream os( NULL );
                os.start( oi.get(), DB::OutputStream::WRITE_ATTRIBUTE);
                wrapper->write(os, *base);

                std::ofstream filePtr;
                std::string settingConfigFile = _getAbsoluteSettingConfPath
                    (ELEMENTS::ISettingComponent::SETTINGCONFIGFILE);
                filePtr.open(settingConfigFile.c_str(), std::ios::out);
                std::string dataToWrite = inputstream.str();
                filePtr << dataToWrite.c_str();
                filePtr.close();
            }
        }
    }

    bool SettingsGUI::_readSettings()
    {
        std::string settingConfigFile = _getAbsoluteSettingConfPath
            (ELEMENTS::ISettingComponent::SETTINGCONFIGFILE);

        if(!osgDB::fileExists(settingConfigFile))
            return false;

        DB::BaseWrapper *wrapper   = DB::BaseWrapperManager::instance()->findWrapper("SettingComponent");
        if(wrapper)
        {
            CORE::IComponent *settingComponent = ELEMENTS::GetComponentFromMaintainer("SettingComponent");
            CORE::IBase* base = NULL;
            if(settingComponent)
            {
                base = settingComponent->getInterface<CORE::IBase>();

                //This is for reading
                std::stringstream outputStream;

                outputStream << std::ifstream(settingConfigFile.c_str(), std::ios::in).rdbuf();
                std::string stringWritten = outputStream.str();

                DB::InputIterator* ii = new AsciiInputIterator(&outputStream);

                DB::InputStream is( NULL );
                is.start( ii, true);

                wrapper->read(is, *base);
                QObject* loginScreen = _findChild("loginScreen");
                if (loginScreen != NULL)
                {
                    bool isLocal = _globalSettingComponent->isLocalProject();
                    if (isLocal)
                    {
                        loginScreen->setProperty("isLocal", BOOL(isLocal));
                    }
                    else
                    {
                        loginScreen->setProperty("isLocal", BOOL(isLocal));
                        std::string ip = _globalSettingComponent->getIPAddress();
                        loginScreen->setProperty("ipAddress", QString::fromStdString(ip));

                    }
                }
            }
        }
        if(!_checkWeatherAllSettinsArePresent())
        {
            emit showError("Project Setting", "Ensure all settings are correct");
            return false;
        }

        return true;
    }

    void SettingsGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("SettingComponent");
                _globalSettingComponent = component->getInterface<ELEMENTS::ISettingComponent>();

                _readSettings();
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    std::string SettingsGUI::_getAbsoluteSettingConfPath(const std::string& configFile)
    {
        std::string settingConfigFile = configFile;

        //Substituting the AppData path with the Real Path
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        //convert the APPDATA variable with the actual path
        std::string appdata = "${APPDATA}";
        if(settingConfigFile.find(appdata.c_str()) != std::string::npos)
        {
            int count = std::string(appdata.c_str()).length();
            settingConfigFile.replace(settingConfigFile.find(appdata.c_str()),
                count, appdataPath.c_str());
            settingConfigFile = osgDB::convertFileNameToNativeStyle(settingConfigFile);
        }

        return settingConfigFile;
    }

    bool SettingsGUI::_checkWeatherAllSettinsArePresent()
    {
        if(!_globalSettingComponent.valid())
            return false;

        bool value = _globalSettingComponent->isAllSettingsPresent();
        return value;
    }


    bool SettingsGUI::_presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap,
        const std::string& keyword)
    {
        if(settingMap.find(keyword) != settingMap.end())
        {
            return true;
        }
        return false;
    }

} // namespace SMCQt

