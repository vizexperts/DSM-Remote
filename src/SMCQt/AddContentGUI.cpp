#include <SMCQt/AddContentGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <VizUI/ISelectionUIHandler.h>
#include <Core/ISelectionComponent.h>

#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>

#include <App/AccessElementUtils.h>

#include <Util/BaseExceptions.h>
#include <Util/CoordinateConversionUtils.h>
#include <Util/ShpFileUtils.h> 
#include <Util/FileUtils.h> 
#include <Util/GdalUtils.h>

#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/ReadFile>

#include <Core/AttributeTypes.h>
#include <Core/IMessage.h>
#include <Core/IMessageType.h>
#include <Core/CoreRegistry.h>
#include <Core/IObjectMessage.h>
#include <Core/IGeoExtent.h>
#include <Core/WorldMaintainer.h>

#include <App/IUIHandlerFactory.h>
#include <App/ApplicationRegistry.h>

#include <Terrain/TerrainPlugin.h>
#include <Terrain/IWMSRasterLayer.h>
#include <Terrain/IModelObject.h>

#include <SMCUI/IAddVectorUIHandler.h>
#include <SMCUI/IAddContentStatusMessage.h>

#include <SMCUI/IAddExcelUIHandler.h>
#include <SMCUI/IAddContentComputeMinMaxLODMessage.h>

#include <DB/ReaderWriter.h>

#include <Elements/ElementsUtils.h>

#include <QFileDialog>

#include <GIS/IOSGOperationQueueHolder.h>

#include <curl/curl.h>

#include <Util/ProjectionUtils.h>
#include <SMCQt/ElementListTreeModel.h>

namespace SMCQt
{

    DEFINE_META_BASE(SMCQt, AddContentGUI);
    DEFINE_IREFERENCED(AddContentGUI, CORE::Base);

    AddContentGUI::AddContentGUI()
        : _minMarking(false)
        , _maxMarking(false)
        , _subdataset(-1)
    {
    }

    AddContentGUI::~AddContentGUI()
    {}

    void AddContentGUI::addRasterLayerPopupLoaded()
    {
        _addLayerModeState = "Raster";
        fileSelectedFromQTBrowser = "";

        QObject* addLayerPopup = _findChild("addRasterLayerPopup");
        if (addLayerPopup != NULL)
        {
            QObject::connect(addLayerPopup, SIGNAL(addLayer(QString, QString, int, int, bool, QString,QString)),
                this, SLOT(addRasterLayer(QString, QString, int, int, bool, QString,QString)), Qt::UniqueConnection);

            QObject::connect(addLayerPopup, SIGNAL(computeMinMaxLod(QString)), this, SLOT(computeMinMaxLod(QString)), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(browseButtonClicked(bool)), this, SLOT(browseButtonClicked(bool)), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(minMarkClicked(bool)), this, SLOT(_handleMinMarkClicked(bool)), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(maxMarkClicked(bool)), this, SLOT(_handleMaxMarkClicked(bool)), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(selectSubdataset()), this, SLOT(_subdatasetSelected()), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(createCRSCombobox()), this, SLOT(createCRSCombobox()), Qt::UniqueConnection);
        }
    }

    ////////////////////////////////////////////////////////////////
    /////////For Excel Files Handling///////////////////////////////
    void AddContentGUI::addExcelDataPopupLoaded()
    {
        QObject* addExcelPopup = _findChild("AddExcelPopUp");
        if (addExcelPopup)
        {
            _addLayerModeState = "Excel";
            _currSelectedFullFileName = "";
            _attributeList.clear();
            _hyperlinkList.clear();
            addExcelPopup->setProperty("layerName", "");
            addExcelPopup->setProperty("pathName", "");
            addExcelPopup->setProperty("showAttributes", false);
            addExcelPopup->setProperty("showHyperlinkOptions", false);
            addExcelPopup->setProperty("latitudeIndex", 0);
            addExcelPopup->setProperty("longitudeIndex", 1);
            addExcelPopup->setProperty("unitIndex", 1);

            QStringList hyperlinkNameList;
            hyperlinkNameList.clear();

            QList<QObject*> hyperlinkList;
            hyperlinkList.clear();

            QList<QObject*> comboBoxList;
            comboBoxList.clear();

            _setContextProperty("attributeList", QVariant::fromValue(comboBoxList));
            _setContextProperty("hyperlinkNameList", QVariant::fromValue(hyperlinkNameList));
            _setContextProperty("hyperlinkList", QVariant::fromValue(hyperlinkList));

            QObject::connect(addExcelPopup, SIGNAL(browseButtonClicked()),
                this, SLOT(browseButtonClicked()), Qt::UniqueConnection);

            QObject::connect(addExcelPopup, SIGNAL(latitudeComboboxClicked()),
                this, SLOT(latlongComboboxClicked()), Qt::UniqueConnection);

            QObject::connect(addExcelPopup, SIGNAL(longitudeComboboxClicked()),
                this, SLOT(latlongComboboxClicked()), Qt::UniqueConnection);

            QObject::connect(addExcelPopup, SIGNAL(hyperlinkComboboxClicked(QString,bool)),
                this, SLOT(hyperlinkComboboxClicked(QString,bool)), Qt::UniqueConnection);

            QObject::connect(addExcelPopup, SIGNAL(selectHyperlinkFromList(int)),
                this, SLOT(selectHyperlinkFromList(int)), Qt::UniqueConnection);

            QObject::connect(addExcelPopup, SIGNAL(removeHyperlinkFromList(int)),
                this, SLOT(removeHyperlinkFromList(int)), Qt::UniqueConnection);
            
            QObject::connect(addExcelPopup, SIGNAL(addExcelFile(QString)),
                this, SLOT(addExcelFile(QString)), Qt::UniqueConnection);
        }
    }

    void AddContentGUI::latlongComboboxClicked()
    {
        QObject* addLayerPopup = NULL;
        addLayerPopup = _findChild("AddExcelPopUp");
        if (addLayerPopup)
        {
            std::string latitude = addLayerPopup->property("latitudeValue").toString().toStdString();
            std::string longitude = addLayerPopup->property("longitudeValue").toString().toStdString();

            if (latitude != "" && longitude != "")
            {
                int count = 0;
                QList<QObject*> hyperlinkList;
                bool defaultSet = false;
                std::string defaultValue;
                for (int i = 0; i < _attributeList.size(); i++)
                {
                    std::string attribute = _attributeList[i];
                    if (attribute != latitude && attribute != longitude)
                    {
                        if (!defaultSet)
                        {
                            defaultValue = attribute;
                            defaultSet = true;
                        }
                        hyperlinkList.append(new VizComboBoxElement(attribute.c_str(), UTIL::ToString(count).c_str()));
                    }
                }

                _setContextProperty("hyperlinkList", QVariant::fromValue(hyperlinkList));
                addLayerPopup->setProperty("hyperlinkvalue", QString::fromStdString(defaultValue));
                addLayerPopup->setProperty("showHyperlinkOptions", true);
                
            }
        }
    }

    void AddContentGUI::selectHyperlinkFromList(int index)
    {
        QObject* addLayerPopup = NULL;
        addLayerPopup = _findChild("AddExcelPopUp");
        if (addLayerPopup)
        {
            addLayerPopup->setProperty("hyperlinkListIndex", index);
        }
    }

    void AddContentGUI::hyperlinkComboboxClicked(QString value,bool add)
    {
        QObject* addLayerPopup = NULL;
        addLayerPopup = _findChild("AddExcelPopUp");
        if (addLayerPopup)
        {
            std::vector<std::string>::iterator it;
            it = std::find(_hyperlinkList.begin(), _hyperlinkList.end(), value.toStdString());

            if (it == _hyperlinkList.end())
            {
                _hyperlinkList.push_back(value.toStdString());
            }

            int count = 0;
            QStringList hyperlinkNameList;
         
            for (it = _hyperlinkList.begin(); it != _hyperlinkList.end(); it++)
            {
                hyperlinkNameList.push_back(QString::fromStdString(*it));
            }

            _setContextProperty("hyperlinkNameList", QVariant::fromValue(hyperlinkNameList));
            addLayerPopup->setProperty("hyperlinkListIndex", _hyperlinkList.size() - 1);
        }
            
    }

    void AddContentGUI::removeHyperlinkFromList(int index)
    {
        QObject* addLayerPopup = NULL;
        addLayerPopup = _findChild("AddExcelPopUp");
        if (addLayerPopup)
        {

            std::vector<std::string>::iterator it;
            std::string value = _hyperlinkList[index];
            it = std::find(_hyperlinkList.begin(), _hyperlinkList.end(), value);
            if (it != _hyperlinkList.end())
            {
                _hyperlinkList.erase(it);
            }


            int count = 0;
            QStringList hyperlinkNameList;
            
            for (it = _hyperlinkList.begin(); it != _hyperlinkList.end(); it++)
            {
                hyperlinkNameList.push_back(QString::fromStdString(*it));
            }

            _setContextProperty("hyperlinkNameList", QVariant::fromValue(hyperlinkNameList));
            addLayerPopup->setProperty("hyperlinkListIndex", _hyperlinkList.size() - 1);
        }
    }


    void AddContentGUI::addExcelFile(QString name)
    {
        QObject* _excelObject = _findChild("AddExcelPopUp");

        if (_currSelectedFullFileName.empty())
        {
            showError("Path", "Path not specified");
            return;
        }

        bool fileExists = osgDB::fileExists(_currSelectedFullFileName);

        if (!fileExists)
        {
            showError("Path Error", "Path is invalid,please give some valid path");
            return;
        }

        std::string layerName = _excelObject->property("layerName").toString().toStdString();
        if (layerName.empty())
        {
            showError("Layer Name", "Layer name should not be empty");
            return;
        }

        try
        {
            std::string extention = osgDB::getLowerCaseFileExtension(_currSelectedFullFileName);
            if (extention.compare("csv") == 0)
            {
                if (!_cvsHandler.valid())
                {
                    emit showError("Application Error", "Unable to load File", true);
                    return;
                }

                QObject* addLayerPopup = NULL;
                addLayerPopup = _findChild("AddExcelPopUp");
                if (addLayerPopup)
                {
                    std::string latitude = addLayerPopup->property("latitudeValue").toString().toStdString();
                    std::string longitude = addLayerPopup->property("longitudeValue").toString().toStdString();

                    int latIndex = 0;
                    int longIndex = 1;

                    for (int i = 0; i < _attributeList.size(); i++)
                    {
                        if (latitude == _attributeList[i])
                        {
                            latIndex = i;
                        }

                        else if (longitude == _attributeList[i])
                        {
                            longIndex = i;
                        }
                    }

                    int unitIndex = addLayerPopup->property("unitIndex").toInt();

                    bool status = _cvsHandler->readFile(layerName, _currSelectedFullFileName, _hyperlinkList, latIndex, longIndex, unitIndex);
                    if (!status)
                    {
                        emit showError("Invalid Input", "Unable to load CSV file", true);
                        return;
                    }
                }
            }
            else
            {
                CORE::RefPtr<SMCUI::IAddExcelUIHandler> excelHandler =
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddExcelUIHandler>(getGUIManager());

                if (!excelHandler.valid())
                {
                    emit showError("Application Error", "Unable to load File", true);
                    return;
                }

                excelHandler->readFile(layerName, _currSelectedFullFileName);
            }
        }
        catch (UTIL::Exception &e)
        {

            std::string errorMessage = e.What();
            emit showError("Error in Reading File", QString::fromStdString(errorMessage), true);
        }
        catch (...)
        {
            emit showError("Error in Reading File", "Unknown Error", true);
        }

        if (_excelObject)
        {
            QMetaObject::invokeMethod(_excelObject, "closePopup");
        }
    }
    /////////For Excel Files Ends///////////////////////////////

    void AddContentGUI::addVectorLayerPopupLoaded()
    {   
        _vectorAttributeFilterList.clear(); 

        _addLayerModeState = "Vector";
        QObject* addLayerPopup = _findChild("addVectorLayerPopup");
        if (addLayerPopup != NULL)
        {
            QObject::connect(addLayerPopup, SIGNAL(addLayer(QString, QString, bool)), this, 
                SLOT(addNewVectorLayer(QString, QString, bool)), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(browseButtonClicked()), this, SLOT(browseButtonClicked()), 
                Qt::UniqueConnection);
            
            std::string StyleFolder = UTIL::getStyleDataPath();
            
            std::vector<std::string> styleTemplates;
            UTIL::getFilesFromFolder(StyleFolder, styleTemplates); 
            
            //populate the list of templates
            QList<QObject*> styleSheetBoxList;

            for (int i = 0; i < styleTemplates.size(); i++)
            {
                std::string templateFile = UTIL::getFileName(styleTemplates[i]);
                templateFile = templateFile.substr(0, templateFile.find("."));
                styleSheetBoxList.append(new VizComboBoxElement(templateFile.c_str(), UTIL::ToString(i).c_str()));
            }
            _setContextProperty("styleSheetList", QVariant::fromValue(styleSheetBoxList));
        }
    }

    void AddContentGUI::addElevationLayerPopupLoaded()
    {
        _addLayerModeState = "Elevation";
        QObject* addLayerPopup = _findChild("addElevationLayerPopup");
        if (addLayerPopup != NULL)
        {
            QObject::connect(addLayerPopup, SIGNAL(addLayer(QString, QString)), this, SLOT(addElevationData(QString, QString)), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(browseButtonClicked(bool)), this, SLOT(browseButtonClicked(bool)), Qt::UniqueConnection);
        }
    }

    void AddContentGUI::addDGNLayerPopupLoaded()
    {
        _addLayerModeState = "DGN";
        QObject* addLayerPopup = _findChild("addDGNLayerPopup");
        if (addLayerPopup != NULL)
        {
            QObject::connect(addLayerPopup, SIGNAL(addLayer(QString, QString, QString, bool, bool)), this,
                SLOT(addDGNLayer(QString, QString, QString, bool, bool)));
            QObject::connect(addLayerPopup, SIGNAL(browseButtonClicked()), this,
                SLOT(browseButtonClicked()), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(mapsheetnumberchanged(QString)), this,
                SLOT(_dgnMapSheetNumberChanged(QString)), Qt::UniqueConnection);
            QObject::connect(addLayerPopup, SIGNAL(orclBrowseButtonClicked()), this,
                SLOT(orclBrowseButtonClicked()), Qt::UniqueConnection);
        }
    }

    void AddContentGUI::setProgressValue(int value)
    {
        QObject* statusBar = _findChild("statusBar");

        if ((value == 100) || (value <= 0))
        {
            if (statusBar)
            {
                CORE::IComponent* component = ELEMENTS::GetComponentFromMaintainer("TileLoaderComponent");
                if (component)
                {
                    GIS::IOSGOperationQueueHolder* queueHolder = component->getInterface<GIS::IOSGOperationQueueHolder>();
                    if (queueHolder)
                    {
                        if (queueHolder->getOperationQueueStatus() == GIS::IOSGOperationQueueHolder::BUSY)
                        {
                            _subscribe(component, *GIS::IOSGOperationQueueHolder::OperationQueueStatusChangedMessageType);
                        }
                        else
                        {
                            QMetaObject::invokeMethod(statusBar, "setProgressStatus",
                                Q_ARG(QVariant, QVariant(QString(""))),
                                Q_ARG(QVariant, QVariant(false)), Q_ARG(QVariant, QVariant(value)));
                        }
                    }
                }
            }
        }
        else
        {
            if (statusBar)
            {
                if (_addLayerModeState == "DGN")
                {
                    QMetaObject::invokeMethod(statusBar, "setProgressStatus",
                        Q_ARG(QVariant, QVariant(QString("Loading DGN data."))),
                        Q_ARG(QVariant, QVariant(false)), Q_ARG(QVariant, QVariant(value)));
                }
                if (_addLayerModeState == "Vector")
                {
                    QMetaObject::invokeMethod(statusBar, "setProgressStatus",
                        Q_ARG(QVariant, QVariant(QString("Loading Vector data."))),
                        Q_ARG(QVariant, QVariant(false)), Q_ARG(QVariant, QVariant(value)));
                }
            }
            _closePopup();
        }

    }

    void AddContentGUI::orclBrowseButtonClicked()
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption = "Browse for Oracle dump file";
        QString filters = "Oracle Dump File (*.dmp)";

        if (_lastPath.empty())
            _lastPath = directory.toStdString();
        QString fileName = QFileDialog::getOpenFileName(parent, caption,
            _lastPath.c_str(), filters);
        //files = fileName;
        _lastPath = osgDB::getFilePath(fileName.toStdString());

        if (fileName.isEmpty())
            return;

        QObject* addLayerPopup = NULL;
        addLayerPopup = _findChild("addDGNLayerPopup");
        if (addLayerPopup)
        {
            addLayerPopup->setProperty("orclSelectedFileName", QVariant::fromValue(fileName));
        }

    }

    void AddContentGUI::browseButtonClicked(bool directoryChecked)
    {
        QWidget* parent = getGUIManager()->getInterface<VizQt::IQtGUIManager>()->getLayoutWidget();
        QString directory = "c:/";
        QString caption;
        QString filters;
        caption = "Browse for layer file";
        std::string label;
        if (_lastPath.empty())
            _lastPath = directory.toStdString();

        //keeping the history whether the Directory is checked or not 
        _isDirectoryChecked = directoryChecked;

        if (_addLayerModeState == "DGN")
        {
            filters = "DGN File (*.dgn)";
        }
        else if (_addLayerModeState == "Elevation")
        {
            label = std::string("Elevation File");
            filters = "Elevation File (*.tif *.tiff *.xml *.dt2 *.grd *.adf *.dt0 *.dt1 *.dem )";
        }
        else if (_addLayerModeState == "Vector")
        {
            filters = "Vector File (*.shp *.gml *.pg *.kml *.000 *.enc)";
        }
        else if (_addLayerModeState == "Raster")
        {
            label = std::string("Raster File");
            filters = "All(*.*);; GeoTiff (*.tif *.tiff);;TMS (*.xml);; More(*.vrt *.ecw *.lf2 *.LF2 *.adf *.he5 *.h5 *.img *.jpg *.jpeg *.bmp *.png)";
            //filters = "Raster File (*.tif *.tiff *.vrt *.xml *.ecw *.lf2 *.LF2 *.adf *.hdf *.he5 *.h5 *.img *.jpg *.jpeg *.bmp)";
        }
        else if (_addLayerModeState == "Excel")
        {
            label = std::string("Excel CVS Folder");
            filters = "Excel File (*.csv *.xls *.xlsx)";
        }

        QString files;
        if ((_addLayerModeState == "Elevation") || (_addLayerModeState == "Raster"))
        {
            if (directoryChecked)
            {
                std::string label;

                if (_addLayerModeState == "Elevation")
                    label = std::string("Elevation Folder");
                else
                    label = std::string("Raster Folder");

                QString filepath = QFileDialog::getExistingDirectory(parent, label.c_str(), _lastPath.c_str());

                _lastPath = filepath.toStdString();

                files = filepath;
            }
            else
            {
                QStringList fileName = QFileDialog::getOpenFileNames(parent, caption,
                    _lastPath.c_str(), filters);

                if (fileName.size())
                {
                    _lastPath = osgDB::getFilePath(fileName.at(0).toStdString());
                }
                else
                {
                    _lastPath = directory.toStdString();
                }

                files = fileName.join(";");

                if (fileName.size() == 1)
                {
                    //For hdf files display the subsets from which user can select to load
                    QString filename = fileName.at(0);

                    std::string ext = osgDB::getLowerCaseFileExtension(filename.toStdString());
                    if (ext == "hdf" || ext == "h5" || ext == "he5" || ext == "001"/*ceos ext*/)
                    {
                        std::vector<std::string> subsetList;
                        _addContentUIHandler->getGdalSubsets(filename.toStdString(), subsetList);

                        QList<QObject*> comboBoxList;
                        int count = 0;
                        for (std::vector<std::string>::const_iterator iter = subsetList.begin(); iter != subsetList.end(); ++iter, count++)
                        {
                            std::string element = *iter;
                            if (element != "")
                            {
                                comboBoxList.append(new VizComboBoxElement(element.c_str(), UTIL::ToString(count).c_str()));
                            }
                        }

                        _setContextProperty("subdatasetListModel", QVariant::fromValue(comboBoxList));
                    }
                }

                else
                {
                    _multipleFileSelection = 1;
                }

            }
        }
        else
        {
            QString fileName = QFileDialog::getOpenFileName(parent, caption,
                _lastPath.c_str(), filters);
            files = fileName;
            _lastPath = osgDB::getFilePath(files.toStdString());
            _currSelectedFullFileName = fileName.toStdString();
        }

        if (files.isEmpty())
            return;
        
        std::string coordSys = ""; 

        QObject* addLayerPopup = NULL;

        if (_addLayerModeState == "Raster")
        {
            addLayerPopup = _findChild("addRasterLayerPopup");
            fileSelectedFromQTBrowser = files;
            coordSys = "CRS: " + UTIL::GDALUtils::getRasterCoodinateSystem(files.toStdString());
        }
        else if ((_addLayerModeState == "Elevation"))
        {
            addLayerPopup = _findChild("addElevationLayerPopup");
            coordSys = "CRS: " + UTIL::GDALUtils::getRasterCoodinateSystem(files.toStdString());
        }
        else if (_addLayerModeState == "Vector")
        {
            addLayerPopup = _findChild("addVectorLayerPopup");
            coordSys ="CRS: " + UTIL::GDALUtils::getVectorCoodinateSystem(files.toStdString()); 
        }
        else if (_addLayerModeState == "DGN")
        {
            addLayerPopup = _findChild("addDGNLayerPopup");
        }
        else if (_addLayerModeState == "Excel")
        {
            addLayerPopup = _findChild("AddExcelPopUp");
            if (addLayerPopup)
            {
                std::string extension = osgDB::getLowerCaseFileExtension(_currSelectedFullFileName);
                
                if (extension.compare("xls")==0 || extension.compare("xlsx")==0)
                {
                    UTIL::TemporaryFolder *temporaryFolder = UTIL::TemporaryFolder::instance();
                    std::string scriptPath = "Excel_to_CSV_All_Worksheets.vbs";
                    std::string excelFilePath = "\"" + _currSelectedFullFileName + "\"";
                    std::string tempFolderPath = "\"" + temporaryFolder->getPath() + "\\\"";

                    std::string commandToExecute = scriptPath + " " + excelFilePath + " " + tempFolderPath;
                    //int status = system(commandToExecute.c_str());

                    std::stringstream tempStream; 
                    tempStream << "cmd.exe /C "  << commandToExecute;

                    bool status = UTIL::GDALUtils::execSysCmd(tempStream); 
                    if (status == true)
                    {
                        std::stringstream sheetName("");
                        std::vector<std::string> hyperlink;
                        sheetName << temporaryFolder->getPath() << "\\sheet" << 1 << ".csv";
                        _currSelectedFullFileName = sheetName.str();
                        extension = osgDB::getLowerCaseFileExtension(_currSelectedFullFileName);
                    }
                }

                if (extension.compare("csv") == 0)
                {
                    _cvsHandler =
                        APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddCSVUIHandler>(getGUIManager());

                    if (!_cvsHandler.valid())
                    {
                        emit showError("Application Error", "Unable to load File", true);
                        return;
                    }

                    _cvsHandler->getAttributesList(_attributeList, _currSelectedFullFileName);
                    //populate the server list
                    int count = 0;
                    QList<QObject*> comboBoxList;
                    bool latitudeSet = false;
                    bool longitudeSet = false;
                    std::string defaultLatitude;
                    std::string defaultLongitude;

                    for (int i = 0; i < _attributeList.size(); i++)
                    {
                        
                        std::string attribute = _attributeList[i];
                        if (!latitudeSet && !longitudeSet)
                        {
                            defaultLatitude = attribute;
                            latitudeSet = true;
                        }

                        else if (latitudeSet && !longitudeSet)
                        {
                            defaultLongitude = attribute;
                            longitudeSet = true;
                        }
                        comboBoxList.append(new VizComboBoxElement(attribute.c_str(), UTIL::ToString(count).c_str()));
                    }

                    _setContextProperty("attributeList", QVariant::fromValue(comboBoxList));
                    addLayerPopup->setProperty("latitudeValue", QString::fromStdString(defaultLatitude));
                    addLayerPopup->setProperty("longitudeValue", QString::fromStdString(defaultLongitude));
                    addLayerPopup->setProperty("latitudeIndex", 0);
                    addLayerPopup->setProperty("longitudeIndex", 1);
                    addLayerPopup->setProperty("showAttributes", true);
                    latlongComboboxClicked();
                }
            }
        }

        if (addLayerPopup)
        {
            addLayerPopup->setProperty("selectedFileName", QVariant::fromValue(files));
            addLayerPopup->setProperty("coordSystem", QString::fromStdString(coordSys));
        }

        if (_addLayerModeState == "DGN")
        {
            dgnFilePathChanged(addLayerPopup, files);
        }
        else
        {
            filePathChanged(addLayerPopup, files);
        }
    }

    void AddContentGUI::_handleMinMarkClicked(bool flag)
    {
        if (_minMarking == flag)
            return;

        if (flag)
        {
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());

            _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
            _minMarking = true;
            _maxMarking = false;
        }
        else
        {
            _minMarking = false;
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());

            _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
        }
    }

    void AddContentGUI::_handleMaxMarkClicked(bool flag)
    {
        if (_maxMarking == flag)
            return;

        if (flag)
        {
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());

            _subscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
            _maxMarking = true;
            _minMarking = false;
        }
        else
        {
            _maxMarking = false;
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());

            _unsubscribe(tph.get(), *VizUI::IMouseMessage::HandledMousePressedMessageType);
        }
    }

    void AddContentGUI::_subdatasetSelected()
    {
        QObject* addLayerPopup = _findChild("addRasterLayerPopup");
        if (addLayerPopup)
        {
            _subdataset = addLayerPopup->property("subdatasetSelectedUid").toInt() + 1;
        }
    }

    void AddContentGUI::_closePopup()
    {
        QObject* addLayerPopup;

        if (_addLayerModeState == "Raster")
        {
            addLayerPopup = _findChild("addRasterLayerPopup");
            //fileSelectedFromQTBrowser = files;
        }
        else if ((_addLayerModeState == "Elevation"))
        {
            addLayerPopup = _findChild("addElevationLayerPopup");
        }
        else if (_addLayerModeState == "Vector")
        {
            addLayerPopup = _findChild("addVectorLayerPopup");
        }
        else if (_addLayerModeState == "DGN")
        {
            addLayerPopup = _findChild("addDGNLayerPopup");
        }
        else if (_addLayerModeState == "Excel")
        {
            addLayerPopup = _findChild("AddExcelPopUp");
        }

        if (addLayerPopup)
        {
            QMetaObject::invokeMethod(addLayerPopup, "closePopup");
        }
    }

    bool AddContentGUI::_checkForMultipleFiles(QString files)
    {
        std::vector<std::string> tokens;
        UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokens, files.toStdString(), UTIL::IsDelimeter(';'));
        if (tokens.size() > 1)
            return true;
        else
            return false;

        return false;
    }

    void AddContentGUI::computeMinMaxLod(QString fileName)
    {
        if (!_addContentUIHandler.valid())
            return;

        std::string fileLocation = fileName.toStdString();

        if (fileLocation.empty())
        {
            showError("Information", "Source file not specified, please browse any valid file.");
            return;
        }

        fileLocation = osgDB::convertFileNameToNativeStyle(fileLocation);


        if (_checkForMultipleFiles(fileName))
        {
            QString error = "Computing LOD is supported for one file";
            emit showError("Loading Data", error);
            return;
        }

        _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessageType);

        _addContentUIHandler->computeMinMaxLOD(fileLocation, 0, 0);

        if (_addLayerModeState == "Raster")
        {
            QObject* addLayerPopup = _findChild("addRasterLayerPopup");
            addLayerPopup->setProperty("minimumLOD", QVariant::fromValue(0));
            addLayerPopup->setProperty("maximumLOD", QVariant::fromValue(0));
        }
    }

    //This is done specific to tweak the setting of the LOD
    void AddContentGUI::filePathChanged(QObject* addLayerPopup, QString filePath)
    {
        std::string fileLocation = filePath.toStdString();
        fileLocation = osgDB::convertFileNameToNativeStyle(fileLocation);

        std::string fileName = "";
        bool multipleFileSelected = false;

        std::vector<std::string> tokens;
        UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokens, fileLocation, UTIL::IsDelimeter(';'));

        if (tokens.size() > 1)
            multipleFileSelected = true;

        for (std::vector<std::string>::const_iterator citer = tokens.begin(); citer != tokens.end(); ++citer)
        {

            std::string extension = osgDB::getLowerCaseFileExtension(*citer);
            std::string filenameIter;
            //QFileInfo fileInfo(filename.c_str());

            if (extension == "xml")
            {
                filenameIter = osgDB::getSimpleFileName(osgDB::getFilePath(*citer)) + "_" + osgDB::getStrippedName(*citer);
            }
            else
            {
                filenameIter = osgDB::getSimpleFileName((*citer));
            }

            if (extension == "jpg" || extension == "jpeg" || extension == "jpeg" || extension == "png")
            {
                addLayerPopup->setProperty("showGeoExtents", QVariant::fromValue(true));
            }
            else
            {
                addLayerPopup->setProperty("showGeoExtents", QVariant::fromValue(false));
            }


            if (fileName.empty())
                fileName += filenameIter;
            else
                fileName += ";" + filenameIter;
        }

        if (_isDirectoryChecked == true)
        {
            fileName = osgDB::getSimpleFileName(filePath.toStdString());
            _isDirectoryChecked = false;
        }

        addLayerPopup->setProperty("layerName", QVariant::fromValue(QString::QString(fileName.c_str())));

        if (_addLayerModeState == "Raster")
        {
            if (multipleFileSelected)
            {
                addLayerPopup->setProperty("minimumLOD", QString::fromStdString(""));
                addLayerPopup->setProperty("maximumLOD", QString::fromStdString(""));
                addLayerPopup->setProperty("lodEditable", QVariant::fromValue(false));
            }
            else
            {
                addLayerPopup->setProperty("lodEditable", QVariant::fromValue(true));
            }
        }

        if (multipleFileSelected)
        {
            addLayerPopup->setProperty("layerNameReadOnly", QVariant::fromValue(true));
        }
        return;
    }

    //Adding the Raster Data
    void AddContentGUI::addRasterLayer(QString layerName, QString filePath, int minLevel,
        int maxLevel, bool overrrideExtend, QString enhanceType,QString projectionType)
    {

        LOG_ERROR(enhanceType.toStdString());


        if (filePath.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Choose a file to add.");
            return;
        }

        if (layerName.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Layer name should not be empty.");
            return;
        }

        std::string fileLocation = filePath.toStdString();
        fileLocation = osgDB::convertFileNameToNativeStyle(fileLocation);

        if (_multipleFileSelection != 1)
        {
            bool fileExists = osgDB::fileExists(fileLocation);

            if (!fileExists)
            {
                showError("Path Error", "Path is invalid . Please give some valid path");
                return;
            }
        }

        if (!_addLayerModeState.compare("Raster"))
        {
            bool isFolder = (osgDB::fileType(fileLocation) == osgDB::DIRECTORY);

            QObject* addLayerPopup = _findChild("addRasterLayerPopup");
            bool lodEnabled;
            if (addLayerPopup == NULL)
                lodEnabled = false;
            else
                lodEnabled = addLayerPopup->property("lodEnabled").toBool();

            if (!lodEnabled)
            {
                _addRasterData(layerName, fileLocation.c_str(), overrrideExtend, 0, 99, enhanceType,projectionType);
            }
            else
            {
                if (!_checkForMultipleFiles(filePath) && !isFolder)
                {
                    _rasterLayerName = layerName.toStdString();
                    _rasterFile = fileLocation;
                    _currentMinLod = minLevel;
                    _currentMaxLod = maxLevel;
                    _overrideRasterExtend = overrrideExtend;
                    _enhanceType = enhanceType.toStdString();
                    _projectionType = projectionType.toStdString();
                    computeMinMaxLod(filePath);
                    return;
                }
            }
        }
    }

    void AddContentGUI::_addRasterData(QString layerName, QString fileLocation,
        bool overrideExtend, int minlevel, int maxlevel, QString enhanceType,QString projectionType)
    {

        std::string filePath = fileLocation.toStdString();

        //if there is any problem in calculating min and max lods, 
        // this is to set those values to default                  
        if (minlevel == 0 && maxlevel == 0) { minlevel = 0; maxlevel = 99; }

        _overrideRasterExtend = overrideExtend;

        try
        {
            if (osgDB::fileType(filePath) == osgDB::DIRECTORY)
            {
                osg::Vec4d ext(0, 0, 0, 0);

                _addContentUIHandler->addRasterData(layerName.toStdString(), filePath, ext,
                    _overrideRasterExtend, minlevel, 99, _subdataset, enhanceType.toStdString(),projectionType.toStdString());
                _closePopup();
                return;
            }
        }
        catch (const UTIL::FileNotFoundException& e)
        {
            // give a error
            emit showError("Folder is Empty", "The selected folder is empty.");
            LOG_ERROR(e.What());
            return;
        }

        try
        {

            std::vector<std::string> tokens;
            std::vector<std::string> layerNameTokens;

            UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokens, filePath, UTIL::IsDelimeter(';'));
            UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(layerNameTokens,
                layerName.toStdString(), UTIL::IsDelimeter(';'));

            if (tokens.size() == 1)
            {
                std::string filename = tokens[0];
                osg::Vec4d extents;

                if (_overrideRasterExtend)
                {
                    QObject* addLayerPopup;
                    addLayerPopup = _findChild("addRasterLayerPopup");

                    if (addLayerPopup != NULL)
                    {
                        QString xmin, xmax, ymin, ymax;

                        xmin = addLayerPopup->property("xMin").toString();
                        xmax = addLayerPopup->property("xMax").toString();
                        ymin = addLayerPopup->property("yMin").toString();
                        ymax = addLayerPopup->property("yMax").toString();

                        if (xmin.isEmpty() || ymin.isEmpty() || xmax.isEmpty() || ymax.isEmpty())
                        {
                            showError("Enter All Fields", "Please enter a Valid Latitude/Longitude");
                            return;
                        }

                        extents.x() = xmin.toDouble();
                        extents.y() = ymin.toDouble();
                        extents.z() = xmax.toDouble();
                        extents.w() = ymax.toDouble();

                        if (extents.x() > 180.0 || extents.x() < -180.0 || extents.z() > 180.0 || extents.z() < -180.0)
                        {
                            showError("Enter Valid Longitude", "Please enter Longitude value between -180.0 and 180.0");
                            return;
                        }
                        if (extents.w() > 90.0 || extents.w() < -90.0 || extents.y() > 90.0 || extents.y() < -90.0)
                        {
                            showError("Enter Valid Latitude", "Please enter Latitude value between -90.0 and 90.0");
                            return;
                        }
                        if ((extents.x() + 0.01) >= extents.z() || (extents.y() + 0.01) >= extents.w())
                        {
                            showError("Enter Valid Longitude and Latitude", "Left Bottom should not be greater than Right Top");
                            return;
                        }
                    }

                }
                else
                {
                    extents = _addContentUIHandler->defaultExtentsQuery(filename);
                }

                _addContentUIHandler->addRasterData(layerNameTokens[0], filename, extents,
                    _overrideRasterExtend, minlevel, 99, _subdataset, enhanceType.toStdString(),projectionType.toStdString());

                _setContextProperty("subdatasetListModel", QVariant::fromValue(NULL));
            }
            else
            {
                for (unsigned int currIndex = 0; currIndex < tokens.size(); ++currIndex)

                {
                    std::string filename = tokens[currIndex];
                    osg::Vec4d extents = _addContentUIHandler->defaultExtentsQuery(filename);

                    _addContentUIHandler->addRasterData(layerNameTokens[currIndex], filename, extents,
                        _overrideRasterExtend, minlevel, 99, _subdataset, enhanceType.toStdString(),projectionType.toStdString());
                }
            }
        }
        catch (const UTIL::Exception& e)
        {
            emit showError("Error in reading File", "Check whether the file is valid");
            LOG_ERROR(e.What());
            return;
        }
        _closePopup();
    }

    //Adding the Elevation and Vector data
    void AddContentGUI::addElevationData(QString layerName, QString filePath)
    {
        if (filePath.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Choose a file to add.");
            return;
        }

        if (layerName.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Layer name should not be empty.");
            return;
        }

        std::string fileLocation = filePath.toStdString();
        fileLocation = osgDB::convertFileNameToNativeStyle(fileLocation);
        bool fileExists = osgDB::fileExists(fileLocation);

        if (_multipleFileSelection != 1)
        {
            if (!fileExists)
            {
                showError("Path Error", "Path is invalid . Please give some valid path");
                return;
            }
        }

        if (!_addLayerModeState.compare("Elevation"))
        {
            unsigned int minlevel = 0, maxlevel = 99;
            bool overrideExtend = false;
            bool isFolder = (osgDB::fileType(fileLocation) == osgDB::DIRECTORY);

            try
            {
                if (isFolder)
                {
                    osg::Vec4d ext(0, 0, 0, 0);
                    _addContentUIHandler->addElevationData(layerName.toStdString(), fileLocation,
                        ext, overrideExtend, minlevel, maxlevel);
                    _closePopup();
                    return;
                }
            }
            catch (const UTIL::FileNotFoundException& e)
            {
                // give a error
                emit showError("Folder is Empty", "The selected folder is empty.");
                LOG_ERROR(e.What());
                return;
            }

            std::vector<std::string> tokens;
            std::vector<std::string> layerNametokens;
            UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(tokens, fileLocation, UTIL::IsDelimeter(';'));
            UTIL::StringTokenizer<UTIL::IsDelimeter>::tokenize(layerNametokens, layerName.toStdString(),
                UTIL::IsDelimeter(';'));

            for (unsigned int currIndex = 0; currIndex < tokens.size(); ++currIndex)
            {
                std::string filename = tokens[currIndex];
                std::string layername = layerNametokens[currIndex];

                osg::Vec4 extents = _addContentUIHandler->defaultExtentsQuery(filename);
                osg::Vec4d ext(extents.x(), extents.y(), extents.z(), extents.w());
                try{
                    _addContentUIHandler->addElevationData(layername, filename, ext, overrideExtend, minlevel, maxlevel);
                }
                catch (const SMCUI::IAddContentUIHandler::AddRasterException& ex){
                    emit showError("File Invalid", QString::fromStdString(ex.What()));
                    return;
                }
            }
            _closePopup();
        }
    }
    //for vector layer with osgearth style 
    void AddContentGUI::addNewVectorLayer(QString layerName, QString filePath, bool editable)
    {
        if( filePath.isEmpty() )
        {
            emit showError(QString::fromStdString("Add " +_addLayerModeState + " Data"), "Choose a file to add.");
            return;
        }

        if(layerName.isEmpty())
        {
            emit showError(QString::fromStdString("Add " +_addLayerModeState + " Data"), "Layer name should not be empty.");
            return;
        }

        std::string fileLocation = filePath.toStdString();
        osgDB::convertFileNameToNativeStyle(fileLocation);
        

        if(_multipleFileSelection !=1)
        {
            bool fileExists = osgDB::fileExists(fileLocation);
            if(!fileExists)
            {
                showError("Path Error","Path is invalid . Please give some valid path");
                return;
            }
        }
        
        QObject* vectorLayerObject = _findChild("addVectorLayerPopup");
        
        if(_addLayerModeState=="Vector")
        {
            QFileInfo fileInfo(fileLocation.c_str());
            if(!fileInfo.exists())
            {
                return;
            }

            std::string fileName = fileInfo.baseName().toStdString();
            CORE::RefPtr<DB::ReaderWriter::Options>  options = new DB::ReaderWriter::Options;
            if (vectorLayerObject->property("showAdvancedOptions").toBool())
            {
                QString qStyleSheetName = vectorLayerObject->property("styleSheet").toString();

                std::string styleSheetName = qStyleSheetName.toStdString();
                //calculate absolute path of file 
                styleSheetName += ".json";
                std::string StyleFolder = UTIL::getStyleDataPath();
                std::string jsonFile = StyleFolder + "/" + styleSheetName;
                options->setMapValue("StyleSheet", jsonFile);
            }

            //always parsing kml through editable
            std::string ext = osgDB::getFileExtension(fileLocation);
            if (ext == "kml"){
                editable = true;
            }

            options->setMapValue("Symbology", std::to_string(!editable));
            try
            {                                  
                _addContentUIHandler->addVectorData(layerName.toStdString(),fileLocation, options);               
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
                return;
            }
        }

        _closePopup();

    }
    //Only for vector layer includes both editable and non editable modes
    void AddContentGUI::addVectorLayer(QString layerName, QString filePath, bool clampingOn, bool editable)
    {
        if (filePath.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Choose a file to add.");
            return;
        }

        if (layerName.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Layer name should not be empty.");
            return;
        }

        std::string fileLocation = filePath.toStdString();
        fileLocation = osgDB::convertFileNameToNativeStyle(fileLocation);

        if (_multipleFileSelection != 1)
        {
            bool fileExists = osgDB::fileExists(fileLocation);
            if (!fileExists)
            {
                showError("Path Error", "Path is invalid . Please give some valid path");
                return;
            }
        }

        if (_addLayerModeState == "Vector")
        {
            QFileInfo fileInfo(fileLocation.c_str());
            if (!fileInfo.exists())
            {
                return;
            }

            std::string fileName = fileInfo.baseName().toStdString();

            bool useTileLoader = true;

            QObject* addVectorLayerPopup = _findChild("addVectorLayerPopup");
            if (addVectorLayerPopup)
            {
                useTileLoader = addVectorLayerPopup->property("useTileLoader").toBool();
            }

            std::string ext = osgDB::getFileExtension(fileLocation);
            try
            {

                if (ext == "kml")
                {
                    CORE::RefPtr<SMCUI::IAddVectorUIHandler>   addVectorGUIHandler;
                    addVectorGUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                        <SMCUI::IAddVectorUIHandler>(getGUIManager());
                    osgEarth::Features::Style style;
                    addVectorGUIHandler->addVectorData(fileLocation, style);
                }
                else
                {
                    CORE::RefPtr<DB::ReaderWriter::Options>  options = new DB::ReaderWriter::Options;
                    options->setMapValue("Clamping", UTIL::ToString(clampingOn));
                    options->setMapValue("Editable", UTIL::ToString(editable));
                    options->setMapValue("TileLoader", UTIL::ToString(useTileLoader));

                    _addContentUIHandler->addVectorData(layerName.toStdString(), fileLocation, options);
                }
            }
            catch (const UTIL::Exception& e)
            {
                e.LogException();
                return;
            }
        }

        _closePopup();

    }


    //Adding the DGN Data
    void AddContentGUI::addDGNLayer(QString layerName, QString filePath, QString orclFilePath, bool extentsSpecified, bool reloadCache)
    {
        if (filePath.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Choose a file to add.");
            return;
        }

        if (layerName.isEmpty())
        {
            emit showError(QString::fromStdString("Add " + _addLayerModeState + " Data"), "Layer name should not be empty.");
            return;
        }

        if (!_addLayerModeState.compare("DGN"))
        {
            std::string fileLocation = filePath.toStdString();
            if (fileLocation.find("file:///") != std::string::npos)
                fileLocation = fileLocation.substr(fileLocation.find("file:///") + strlen("file:///"));
            fileLocation = osgDB::convertFileNameToNativeStyle(fileLocation);

            bool fileExists = osgDB::fileExists(fileLocation);

            if (!fileExists)
            {
                showError("Path Error", "Path is invalid . Please give some valid path");
                return;
            }

            std::string orclFileLocation = orclFilePath.toStdString();
            if (orclFileLocation.find("file:///") != std::string::npos)
                orclFileLocation = orclFileLocation.substr(orclFileLocation.find("file:///") + strlen("file:///"));
            orclFileLocation = osgDB::convertFileNameToNativeStyle(orclFileLocation);

            fileExists = osgDB::fileExists(orclFileLocation);

            /*if (!fileExists)
            {
            showError("Path Error", "Path is invalid . Please give some valid path");
            return;
            }*/

            QObject* addLayerPopup = NULL;
            addLayerPopup = _findChild("addDGNLayerPopup");

            if (!extentsSpecified)
            {
                QVariant mapSheetNumber;
                mapSheetNumber = addLayerPopup->property("mapSheetNumber");
                if (!_validMapSheetName(mapSheetNumber.toString().toStdString()))
                {
                    showError("Insufficient/incorrect data", "Invalid Map sheet Number");
                    return;
                }
            }

            QString latitude, longitude;
            if (addLayerPopup->property("latitude").toString().toStdString().length() == 0 ||
                addLayerPopup->property("longitude").toString().toStdString().length() == 0)
            {
                showError("Insufficient/incorrect data", "Map center parameters are not specified");
                return;
            }

            latitude = addLayerPopup->property("latitude").toString();
            longitude = addLayerPopup->property("longitude").toString();

            double dgnLatitude = latitude.toDouble();
            double dgnLongitude = longitude.toDouble();

            if ((dgnLatitude > 90.0) || (dgnLatitude < -90.0))
                return;

            if ((dgnLongitude > 180.0) || (dgnLongitude < -180.0))
                return;

            QObject* statusBar = _findChild("statusBar");
            if (statusBar)
            {
                QMetaObject::invokeMethod(statusBar, "setStatus",
                    Q_ARG(QVariant, QVariant(QString("Loading DGN data."))),
                    Q_ARG(QVariant, QVariant(true)));
            }

            _addDGNDataONTerrain(layerName.toStdString(), fileLocation, orclFileLocation, dgnLatitude,
                dgnLongitude, reloadCache);
        }
    }


    void AddContentGUI::dgnFilePathChanged(QObject* addLayerPopup, QString filePath)
    {
        std::string fileLocation = filePath.toStdString();

        if (fileLocation.find("file:///") != std::string::npos)
            fileLocation = fileLocation.substr(fileLocation.find("file:///") + strlen("file:///"));
        fileLocation = osgDB::convertFileNameToNativeStyle(fileLocation);

        QFileInfo fileInfo(fileLocation.c_str());
        if (!fileInfo.exists())
        {
            return;
        }

        std::string mapSheetName = fileInfo.baseName().toStdString();
        //Change Layer Name from File name to Map Name
        if (_validMapSheetName(mapSheetName))
        {
            addLayerPopup->setProperty("layerName", QVariant::fromValue(QString::QString(mapSheetName.c_str())));

        }
        else
        {
            addLayerPopup->setProperty("layerName", QString::fromStdString(osgDB::getSimpleFileName
                (osgDB::getFilePath(fileLocation))));
        }

        /*addLayerPopup->setProperty("layerNameReadOnly", QVariant::fromValue(false));*/

        if (!_validMapSheetName(mapSheetName))
        {
            showError("Invalid MapSheetName", "Enter a valid Mapsheet Number or Geographical Extents");
            return;
        }
        addLayerPopup->setProperty("mapSheetNumber", QVariant::fromValue(QString::QString(mapSheetName.c_str())));

        _dgnMapSheetNumberChanged(QString::fromStdString(mapSheetName));
    }

    void AddContentGUI::_dgnMapSheetNumberChanged(QString mapSheetName)
    {
        std::string mapSheetNumber = mapSheetName.toStdString();
        if (_validMapSheetName(mapSheetNumber))
        {
            QObject* addLayerPopup;
            addLayerPopup = _findChild("addDGNLayerPopup");

            _getDGNExtents(addLayerPopup, mapSheetNumber);
        }
        else
        {
            //showError("Invalid MapSheet Number","Enter a valid mapsheet number or give Geographical Extents");
        }
    }

    void AddContentGUI::_getDGNExtents(QObject* addLayerPopup, const std::string& mapSheetNumber)
    {
        double dgnLatitude, dgnLongitude;
        //set the latitude, longitude and layer names in their corresponding fields
        if (_validMapSheetName(mapSheetNumber))
        {
            osg::Vec2d center = _getMapCenter(mapSheetNumber);
            dgnLatitude = center.x();
            dgnLongitude = center.y();

            addLayerPopup->setProperty("longitude", QVariant::fromValue(dgnLongitude));
            addLayerPopup->setProperty("latitude", QVariant::fromValue(dgnLatitude));
        }
        else
        {
            addLayerPopup->setProperty("longitude", "");
            addLayerPopup->setProperty("latitude", "");
        }
    }

    bool AddContentGUI::_validMapSheetName(const std::string& mapSheetName)
    {
        return _mapSheetInfoComponent->isValid(mapSheetName);
    }

    osg::Vec2d AddContentGUI::_getMapCenter(const std::string& mapSheetName)
    {
        return _mapSheetInfoComponent->getCenter(mapSheetName);
    }

    void AddContentGUI::_addDGNDataONTerrain(const std::string& fileName, const std::string& filePath, const std::string& orclFilePath,
        const double& latitude, const double& longitude, bool reloadCache)
    {

        osg::Vec2d extent;
        extent.x() = longitude;
        extent.y() = latitude;
        try
        {
            _addContentUIHandler->addDGNData(fileName, filePath, orclFilePath, extent, reloadCache);
        }
        catch (UTIL::Exception e)
        {
            showError("Exception", QString::fromStdString(e.ToString()));
            return;
        }
        _closePopup();
    }

    void AddContentGUI::_loadAndSubscribeSlots()
    {
        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }

        QObject::connect(this, SIGNAL(_setProgressValue(int)),this, SLOT(setProgressValue(int)),Qt::QueuedConnection);

        DeclarativeFileGUI::_loadAndSubscribeSlots();
    }

    void AddContentGUI::popupLoaded(QString type)
    {
        if(type == "addDGNLayerPopup")
        {
            addDGNLayerPopupLoaded();
        }
        else if(type == "addRasterLayerPopup")
        {
            addRasterLayerPopupLoaded();
        }
        else if(type == "addElevationLayerPopup")
        {
            addElevationLayerPopupLoaded();
        }
        else if(type == "addVectorLayerPopup")
        {
            addVectorLayerPopupLoaded();
        }
        else if(type == "AddExcelPopUp")
        {
            addExcelDataPopupLoaded();
        }
    }

    void AddContentGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the BasemapUIHandler and set it
            try
            {
                _addContentUIHandler = 
                    APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAddContentUIHandler>(getGUIManager());

                _subscribe(_addContentUIHandler.get(), *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType);

                CORE::RefPtr<CORE::IComponent> component = 
                    CORE::WorldMaintainer::instance()->getComponentByName("MapSheetInfoComponent");

                if(component.valid())
                {
                    _mapSheetInfoComponent = component->getInterface<ELEMENTS::IMapSheetInfoComponent>();
                    component = NULL;
                }

                _wmsTreeModel = new WmsTreeModel();
                _childItem = new WmsTreeModelItem();
                _childItem->setName("Layers");
                _childItem->setCheckable(true);
                _childItem->setCheckState(Qt::Checked);

                _wmsTreeModel->addItem(_childItem);

                CORE::RefPtr<VizQt::IQtGUIManager> qtapp = getGUIManager()->getInterface<VizQt::IQtGUIManager>();
                QQmlContext* rootContext  = qtapp->getRootContext();
                if(rootContext)
                {
                    rootContext->setContextProperty("geoTreeModel", _wmsTreeModel);
                }

            }
            catch(...)
            {
                // XXX - Ignoring the message here?
            }
        }
        else if(messageType == *SMCUI::IAddContentStatusMessage::AddContentStatusMessageType)
        {
            SMCUI::IAddContentStatusMessage* addContentStatusMessage = 
                message.getInterface<SMCUI::IAddContentStatusMessage>();

            int progressValue =_addContentUIHandler->getProgressValue();

            if(addContentStatusMessage->getStatus() == SMCUI::IAddContentStatusMessage::PENDING)
            {
                emit _setProgressValue(progressValue);
            }

            if(addContentStatusMessage->getStatus() == SMCUI::IAddContentStatusMessage::FAILURE)
            {
                emit _setProgressValue(progressValue);
                emit showError("Data Loading Status", "Failed to Load File");
            }

            if(addContentStatusMessage->getStatus() == SMCUI::IAddContentStatusMessage::SUCCESS)
            {
                emit _setProgressValue(progressValue);
            }

            if (addContentStatusMessage->getStatus() == SMCUI::IAddContentStatusMessage::SKIPPED_FILES)
            {
                QObject* popupLoader = _findChild(SMP_FileMenu);
                if (popupLoader)
                {
                    // load add bookmark popup
                    QObject* addLayerPopup = _findChild("addRasterLayerPopup");
                    if (addLayerPopup)
                        QMetaObject::invokeMethod(addLayerPopup, "closePopup");
                    _skippedList.clear();
                    std::vector<std::string>& vecList = addContentStatusMessage->getSkippedFileList();
                    if (!vecList.empty())
                    {
                        //! Listing File names for Skipped UI.
                        for (int i = 0; i < vecList.size(); ++i)
                        {
                            boost::filesystem::path filePath(vecList[i].c_str());
                            std::string fileName =  filePath.filename().string();
                            QObject *object = new SkippedTableModel(std::to_string(i + 1).c_str(), fileName.c_str());
                            _skippedList.append(object);
                        }

                        QMetaObject::invokeMethod(popupLoader, "load", Q_ARG(QVariant, QVariant("SkippedFileList.qml")));
                        _setContextProperty("skippedlayerModel", QVariant::fromValue(_skippedList));
                    }
                }
            }
        }
        else if(messageType == *GIS::IOSGOperationQueueHolder::OperationQueueStatusChangedMessageType)
        {
            CORE::IComponent* component = ELEMENTS::GetComponentFromMaintainer("TileLoaderComponent");
            if(component)
            {
                GIS::IOSGOperationQueueHolder* queueHolder = component->getInterface<GIS::IOSGOperationQueueHolder>();
                if(queueHolder)
                {
                    if(queueHolder->getOperationQueueStatus() == GIS::IOSGOperationQueueHolder::IDLE)
                    {
                        QObject* statusBar = _findChild("statusBar");

                        QMetaObject::invokeMethod(statusBar, "setStatus", 
                            Q_ARG(QVariant, QVariant(QString(""))),
                            Q_ARG(QVariant, QVariant(false)));
                    }
                }
            }
            
        }
        else if (messageType == *VizUI::IMouseMessage::HandledMousePressedMessageType)
        {

            // Query TerrainUIHandler interface
            CORE::RefPtr<VizUI::ITerrainPickUIHandler> tph =
                APP::AccessElementUtils::getUIHandlerUsingManager<VizUI::ITerrainPickUIHandler>(getGUIManager());
            if(tph.valid())
            {
                // get terrain position
                osg::Vec3d pos;
                if(tph->getMousePickedPosition(pos, true))
                {
                    std::stringstream  xvalue, yvalue;
                    xvalue << pos.x();
                    yvalue << pos.y();

                    if(_minMarking)
                    {
                        QObject* addLayerPopup = _findChild("addRasterLayerPopup");
                        addLayerPopup->setProperty("xMin",QVariant::fromValue(QString::fromStdString(xvalue.str())));
                        addLayerPopup->setProperty("yMin",QVariant::fromValue(QString::fromStdString(yvalue.str())));
                    }

                    else if(_maxMarking)
                    {
                        QObject* addLayerPopup = _findChild("addRasterLayerPopup");
                        addLayerPopup->setProperty("xMax",QVariant::fromValue(QString::fromStdString(xvalue.str())));
                        addLayerPopup->setProperty("yMax",QVariant::fromValue(QString::fromStdString(yvalue.str())));
                    }
                }
            }
        }
        else if(messageType == *SMCUI::IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODMessageType)
        {
            SMCUI::IAddContentComputeMinMaxLODMessage* addContentMessage = 
                message.getInterface<SMCUI::IAddContentComputeMinMaxLODMessage>();

            if (!addContentMessage)
                return;

            if (addContentMessage->getStatus() == SMCUI::IAddContentStatusMessage::SUCCESS)
            {
                unsigned int minLOD = addContentMessage->getMinLOD();
                unsigned int maxLOD = addContentMessage->getMaxLOD();

                //This is for checking whether the User has entered the Value of LOD Within the Permissible Range
                if ((_currentMinLod < minLOD) || (_currentMaxLod > maxLOD))
                {
                    std::stringstream ss;
                    ss << "The Permisable LOD is between " << minLOD << " and " << maxLOD << ". Please try again";
                    emit showError("Min Max LOD ", ss.str().c_str());
                }
                else if (_currentMinLod > _currentMaxLod)
                {
                    std::stringstream ss;
                    ss << "Minimum LOD must be less than or equal to Maximum LOD";
                    emit showError("Min Max LOD", ss.str().c_str());
                }
                else
                {
                    _addRasterData(_rasterLayerName.c_str(), _rasterFile.c_str(), _overrideRasterExtend, _currentMinLod, _currentMaxLod, _enhanceType.c_str(),_projectionType.c_str());
                }

                if (_addLayerModeState == "Raster")
                {
                    QObject* addLayerPopup = _findChild("addRasterLayerPopup");
                    addLayerPopup->setProperty("minimumLOD", QVariant::fromValue(minLOD));
                    addLayerPopup->setProperty("maximumLOD", QVariant::fromValue(maxLOD));
                }
            }
        }
        else
        {
            VizQt::GUI::update(messageType, message);
        }

    }

    void AddContentGUI::_populateGeoserverLists() 
    {

        foreach(QMLTreeModelItem* item, _childItem->getChildren())
        {
            _wmsTreeModel->removeItem(item);
        }

        std::string server = "http://localhost:8080/geoserver/vizexperts/wms";
        CORE::RefPtr<ELEMENTS::ISettingComponent> settingComp = ELEMENTS::GetComponentFromMaintainer("SettingComponent")->getInterface<ELEMENTS::ISettingComponent>();
        if (settingComp.valid())
        {
            ELEMENTS::ISettingComponent::SettingsAttributes att =
                settingComp->getGlobalSetting(ELEMENTS::ISettingComponent::GEOSERVER);

            std::string host, workspace;

            host = att.keyValuePair["Host"];
            workspace = att.keyValuePair["Workspace"];

            if (!host.empty() && !workspace.empty())
                server = host + "/" + workspace + "/wms";
        }

        //create the capability url
        std::string capabilities = server + "?service=wms&version=1.1.1&request=GetCapabilities";

        //query the capabilty url
        osg::ref_ptr<osgEarth::Util::WMSCapabilities> wmsCapabilities = osgEarth::Util::WMSCapabilitiesReader::read(capabilities, NULL);

        if (!wmsCapabilities.valid())
        {
            showError("Error", "Error in connecting to the Server. \nPlease check if the server address is correct and server is online.");
            return;
        }

        const osgEarth::Util::WMSLayer::LayerList& layerList = wmsCapabilities->getLayers();
        _populate(((osgEarth::Util::WMSLayer*)*layerList.begin())->getLayers(), _childItem);

        _wmsTreeModel->openItem(_wmsTreeModel->indexOf(_childItem));

    }

    void AddContentGUI::_populate(const osgEarth::Util::WMSLayer::LayerList& layerList, WmsTreeModelItem* _childItem) {
        osgEarth::Util::WMSLayer::LayerList::const_iterator iter = layerList.begin();

        while (iter != layerList.end())
        {
            osg::ref_ptr<osgEarth::Util::WMSLayer> wmsLayer = (*iter).get();
            WmsTreeModelItem* item = new WmsTreeModelItem();
            item->setName(wmsLayer->getName().c_str());
            item->setTitle(wmsLayer->getTitle().c_str());
            item->setAbstraction(wmsLayer->getAbstract().c_str());
            item->setCheckState(Qt::Unchecked);

            _childItem->addItem(item);

            if (_childItem->getChildCount() == 1)
                _wmsTreeModel->emitDataChanged(_wmsTreeModel->indexOf(_childItem));
            if (_childItem->isOpened())
            {
                int index = _wmsTreeModel->indexOf(_childItem);
                _wmsTreeModel->closeItem(index);
                _wmsTreeModel->openItem(index);
            }

            _populate(wmsLayer->getLayers(), item);
            iter++;
        }
    }


    void AddContentGUI::_getLayerNames(std::list<WmsTreeModelItem*>& layerItems, WmsTreeModelItem* item)
    {
        if (item == _childItem) {
            foreach(QMLTreeModelItem* i, _childItem->getChildren())
                _getLayerNames(layerItems, (WmsTreeModelItem*)i);
            return;
        }
        if (item->getCheckState() == Qt::Checked)
        {
            layerItems.push_back(item);
        }
        foreach(QMLTreeModelItem* childItem, item->getChildren())
        {
            WmsTreeModelItem* child = dynamic_cast<WmsTreeModelItem*>(childItem);
            _getLayerNames(layerItems, child);
        }

    }

    void AddContentGUI::addLayers()
    {
        std::list<WmsTreeModelItem*> layerItems;
        _getLayerNames(layerItems, _childItem);

        std::string server = "http://localhost:8080/geoserver/vizexperts/wms";
        CORE::RefPtr<ELEMENTS::ISettingComponent> settingComp =
            ELEMENTS::GetComponentFromMaintainer("SettingComponent")->getInterface<ELEMENTS::ISettingComponent>();
        if (settingComp.valid())
        {
            ELEMENTS::ISettingComponent::SettingsAttributes att =
                settingComp->getGlobalSetting(ELEMENTS::ISettingComponent::GEOSERVER);

            std::string host, workspace;

            host = att.keyValuePair["Host"];
            workspace = att.keyValuePair["Workspace"];

            if (!host.empty() && !workspace.empty())
                server = host + "/" + workspace + "/wms";
        }

        std::string layerNames;
        for (std::list<WmsTreeModelItem*>::iterator itr = layerItems.begin(); itr != layerItems.end(); itr++)
        {
            WmsTreeModelItem* item = *itr;
            QString name = item->getName();
            if (!name.isEmpty())
            {
                std::string s = name.toStdString();
                if (layerNames.empty())
                    layerNames = s;
                else
                    layerNames += "," + s;
            }
        }

        CORE::RefPtr<CORE::IObjectFactory> objectFactory = CORE::CoreRegistry::instance()->getObjectFactory();
        CORE::RefPtr<CORE::IObject> object = objectFactory->createObject(*TERRAIN::TerrainRegistryPlugin::WMSRasterLayerType);

        CORE::RefPtr<TERRAIN::IWMSRasterLayer> wmsLayer = object->getInterface<TERRAIN::IWMSRasterLayer>();
        wmsLayer->setURL(server);
        wmsLayer->setWMSLayers(layerNames);

        CORE::RefPtr<CORE::IBase> base = object->getInterface<CORE::IBase>();

        base->setName("GeorbIS Layers");

        CORE::RefPtr<CORE::IWorld> world =
            APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());

        if (world.valid())
        {
            world->addObject(object.get());
        }
    }

    void AddContentGUI::remLayers() {
        std::list<WmsTreeModelItem*> layerItems;
        _getLayerNames(layerItems, _childItem);

        for (std::list<WmsTreeModelItem*>::iterator itr = layerItems.begin(); itr != layerItems.end(); itr++) {
            WmsTreeModelItem* item = *itr;
            QString name = item->getName();
            std::string abs = item->getAbstraction().toStdString();
            std::transform(abs.begin(), abs.end(), abs.begin(), ::tolower);
            curlRemLayer(name.toStdString(), abs.find("layer-group") != std::string::npos);
        }

        _populateGeoserverLists();
    }

    void AddContentGUI::curlRemLayer(std::string layer, bool layerGroup)
    {
        std::string layerName = layer + ".xml";
        CURL *curl;
        CURLcode res;

        curl = curl_easy_init();
        if (curl)
        {
            std::string lg = layerGroup ? "layergroups" : "layers";
            std::string url("http://localhost:8080/geoserver");
            std::string user = "admin", pass = "geoserver";

            CORE::RefPtr<ELEMENTS::ISettingComponent> settingComp =
                ELEMENTS::GetComponentFromMaintainer("SettingComponent")->getInterface<ELEMENTS::ISettingComponent>();

            if (settingComp.valid())
            {
                ELEMENTS::ISettingComponent::SettingsAttributes att =
                    settingComp->getGlobalSetting(ELEMENTS::ISettingComponent::GEOSERVER);
                std::string host, un, p;
                host = att.keyValuePair["Host"];
                un = att.keyValuePair["Username"];
                p = att.keyValuePair["Password"];

                if (!un.empty() && !p.empty())
                {
                    user = un;
                    pass = p;
                }

                if (!host.empty())
                    url = host;
            }

            url += "/rest/" + lg + "/" + layerName;

            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(curl, CURLOPT_USERNAME, user.c_str());
            curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.c_str());

            struct curl_slist *headers = NULL;
            headers = curl_slist_append(headers, "Content-Type: application/xml");
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "delete");
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

            /* Perform the request, res will get the return code */
            res = curl_easy_perform(curl);
            /* Check for errors */
            if (res != CURLE_OK)
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));

            /* free the header list */
            curl_slist_free_all(headers);

            curl_easy_cleanup(curl);
        }
    }

    void AddContentGUI::createCRSCombobox()
    {
        std::map<std::string, std::string> crsMap;
        ElementListTreeModel *root = new ElementListTreeModel();


        QList<QObject*> crsList;
        for (int i = 0; i < 3; i++)
        {
            crsMap.insert(std::pair<std::string, std::string>(std::make_pair("i"+i, "i"+i)));
            std::map<std::string, std::string>::iterator itr;

            for (itr=crsMap.begin(); itr!=crsMap.end(); itr++)
            {
                crsList.append(new VizComboBoxElement(UTIL::ToString(i).c_str(), UTIL::ToString(i).c_str()));
            }
        }
        QObject *addRasterPopUp = _findChild("addRasterLayerPopup");
            if (addRasterPopUp)
            {
                _setContextProperty("crsListModel", QVariant::fromValue(crsList));
            }
    }
} // namespace SMCQt
