/****************************************************************************
 *
 * File             : ScreenShareGUI.h
 * Description      : ScreenShareGUI class definition
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************/

#include <SMCQt/ScreenShareGUI.h>

#include <VizQt/NameValidator.h>

#include <App/IApplication.h>

#include <HLA/NetworkCollaborationManager.h>

#include <Core/IStringMessage.h>

#include <QtCore/QString>
#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>
#include <QDialog>
#include <QMessageBox>
#include <QLabel>
#include <QProgressBar>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, ScreenShareGUI);
    DEFINE_IREFERENCED(ScreenShareGUI, VizQt::LayoutFileGUI);
    
    const std::string ScreenShareGUI::LBMessage="LBMessage";
    const std::string ScreenShareGUI::LEName="LEName";
    const std::string ScreenShareGUI::PBRequest="PBRequest";
    const std::string ScreenShareGUI::LBRequestMessage="LBRequestMessage";
    const std::string ScreenShareGUI::PBCancelRequest="PBCancelRequest";
    const std::string ScreenShareGUI::PBProgress="PBProgress";
    const std::string ScreenShareGUI::PBTakeOwnerShip="PBTakeOwnerShip";
    const std::string ScreenShareGUI::PBSubscribeScreen="PBSubscribeScreen";

    const std::string ScreenShareGUI::MsgScreenSubscribed="Screen subscribed to owner : ";
    const std::string ScreenShareGUI::MsgRequestScreenSharing="Request for Sharing your screen";
    
    ScreenShareGUI::ScreenShareGUI()
        : _requestPending(false)
    {
    }

    ScreenShareGUI::~ScreenShareGUI()
    {
        setActive(false);
    }

    void 
    ScreenShareGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Set the NetworkCollborationManager
            try
            {   
                APP::IManager* mgr = getGUIManager()->getInterface<APP::IManager>();
                APP::IApplication* app = mgr->getApplication();
                mgr = app->getManagerByName(HLA::NetworkCollaborationManager::ClassNameStr);

                if(mgr)
                {
                    _netwrkCollaborationMgr = mgr->getInterface<HLA::INetworkCollaborationManager>();
                    if(!_netwrkCollaborationMgr.valid())
                    {
                        setComplete(false);
                    }
                    else
                    {
                        _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareRequestReceivedMessageType.get());                        
                        _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareRequestCancelledMessageType.get());
                        _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareRequestRejectedMessageType.get());
                    }
                }
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        //! ensure that this message is recieved in main thread. Make it an asynchronous delivery
        else if(messageType == *HLA::INetworkCollaborationManager::ScreenShareResponseReceivedMessageType)
        {            
            _getChild<QProgressBar>(PBProgress)->hide();
            _getChild<QLabel>(LBRequestMessage)->hide();                
            _getChild<QPushButton>(PBRequest)->setEnabled(false);
            _getChild<QPushButton>(PBCancelRequest)->setEnabled(false);
            QMessageBox::information(_parent, "Success", QString::fromStdString("Request Granted.\n\nYour screen is successfully shared across the project."));                        
            qobject_cast<QDialog *>(_parent)->close();
            _requestPending=false;
            _unsubscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareResponseReceivedMessageType);
        }        
        else if(messageType == *HLA::INetworkCollaborationManager::ScreenShareRequestReceivedMessageType.get())
        {
            QMessageBox::StandardButton btnClicked = QMessageBox::question
                (_parent, "Screen Share Request", QString("A request has been made by other application to share its own screen.\n\n") +
                                                  QString("Note : Allowing it will stop your screen share and subscribe this application to it's screen.\n\n") + 
                                                  QString("Do you want to allow it ?"), QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Ok);

            if(btnClicked == QMessageBox::Ok)            
                _netwrkCollaborationMgr->grantScreenShareRequest();
            
            else 
                _netwrkCollaborationMgr->ignoreScreenShareRequest();
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ScreenShareRequestCancelledMessageType.get())
        { 
            emit showError("Screen Share Status", "The request was cancelled by the requesting application.\n\nHence, your screen is still shared.");                  
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ScreenShareRequestRejectedMessageType.get())
        {
            emit showError("Error in sharing Screen", "Request was cancelled by the user."); 

            //Cancelling the request as the attributes are not available
            _handleCancelRequestClicked();
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ScreenShareStopSuccessfulMessageType.get())
        {
            emit showInformation("Stop Screen share status", "Screen Sharing is successfully stopped."); 
            _setInitialGUIState();
        }
        else if(messageType == *HLA::INetworkCollaborationManager::ScreenShareStopFailedMessageType.get())
        {
            emit showError("Error in stop sharing Screen", "Sharing can not be stopped. Please try again later"); 
            _setInitialGUIState();
        }
        else
        {
            LayoutFileGUI::update(messageType, message);
        }
    }
            
    void 
    ScreenShareGUI::initializeAttributes()
    {
        LayoutFileGUI::initializeAttributes();

        _addWidgetName(LBMessage);
        _addWidgetName(LEName);
        _addWidgetName(PBRequest);
        _addWidgetName(LBRequestMessage);
        _addWidgetName(PBCancelRequest);
        _addWidgetName(PBProgress);
        _addWidgetName(PBTakeOwnerShip);
        _addWidgetName(PBSubscribeScreen);
    }

    void 
    ScreenShareGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }

        _parent->show();
        _parent->activateWindow ();
        _parent->raise();
        _parent->setFocus(Qt::OtherFocusReason);

        // If value is set properly then set focus for area UI handler
        // Check whether UI Handler is valid or not
        if(!getComplete())
        {
            return;
        }

        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                if(_setInitialGUIState())
                {
                    qobject_cast<QDialog*>(_parent)->adjustSize();                    
                }
                else
                {
                    qobject_cast<QDialog *>(_parent)->reject();
                    return;
                }
            }
            else
            {   
                if(_requestPending)
                {
                    _handleCancelRequestClicked();
                }
            }
            LayoutFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }
    
    void 
    ScreenShareGUI::_loadAndSubscribeSlots()
    {
        LayoutFileGUI::_loadAndSubscribeSlots();
        if(!getComplete())
        {
            return;
        }

        _connect(PBRequest, SIGNAL(clicked()), SLOT(_handleRequestButtonClicked()));        
        _connect(PBCancelRequest, SIGNAL(clicked()), SLOT(_handleCancelRequestClicked())); 
        _connect(PBTakeOwnerShip, SIGNAL(clicked()), SLOT(_handleOwnerShipTaken()));
        _connect(PBSubscribeScreen, SIGNAL(clicked()), SLOT(_handleSubscribeButtonClicked()));

    }

    void
    ScreenShareGUI::_handleSubscribeButtonClicked()
    {
        if(_netwrkCollaborationMgr->isSubscribedToScreenShare())
        {
            _netwrkCollaborationMgr->unsubscribeToScreenShare();
            _getChild<QPushButton>(PBSubscribeScreen)->setText("Subscribe to shared screen");
        }
        else
        {
            _netwrkCollaborationMgr->subscribeToScreenShare();
            _getChild<QPushButton>(PBSubscribeScreen)->setText("Unsubscibe to shared screen");
        }
    }

    void
    ScreenShareGUI::_handleOwnerShipTaken()
    {
        HLA::INetworkCollaborationManager::ApplicationState currState=_netwrkCollaborationMgr->getApplicationState();

        switch(currState)
        {
            case HLA::INetworkCollaborationManager::PUBLISHED: 
            {
                _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareResponseReceivedMessageType);
                _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareRequestRejectedMessageType);

                _netwrkCollaborationMgr->requestScreenShare(true);
                break;
            }

            case HLA::INetworkCollaborationManager::JOINED:
            {
                if(_netwrkCollaborationMgr->isSubscribedToScreenShare())
                    _netwrkCollaborationMgr->subscribeToScreenShare();
                else
                    _netwrkCollaborationMgr->unsubscribeToScreenShare();

                break;
            }
        }

    }

    bool
    ScreenShareGUI::_setInitialGUIState()
    {
        _getChild<QProgressBar>(PBProgress)->hide();
        _getChild<QLabel>(LBRequestMessage)->hide();

        bool result = false;

        HLA::INetworkCollaborationManager::ApplicationState currState=_netwrkCollaborationMgr->getApplicationState();
        switch(currState)
        {
            case HLA::INetworkCollaborationManager::NONE:            
            {
                showError( "Error", QString("Project is neither Joined nor Published.\n\n") +
                                                        QString("1.) Either Publish your project for sharing your screen.\n") +
                                                        QString("2.) Or Join a project and request for screen share.") );
                break;
            }

            case HLA::INetworkCollaborationManager::PUBLISHED:
            {
                _getChild<QLabel>(LBMessage)->setText(QString::fromStdString(MsgScreenSubscribed));
                _getChild<QLineEdit>(LEName)->setText(QString::fromStdString(_netwrkCollaborationMgr->getProjectName()));

                _getChild<QPushButton>(PBRequest)->setEnabled(true);
                _getChild<QPushButton>(PBRequest)->setText(QString::fromStdString(MsgRequestScreenSharing));

                _getChild<QPushButton>(PBTakeOwnerShip)->setVisible(true);
                _getChild<QPushButton>(PBSubscribeScreen)->setVisible(true);
                
                if(_netwrkCollaborationMgr->isSubscribedToScreenShare())
                    _getChild<QPushButton>(PBSubscribeScreen)->setText("Unsubscibe to shared screen");
                else
                    _getChild<QPushButton>(PBSubscribeScreen)->setText("Subscribe to shared screen");

                _getChild<QPushButton>(PBCancelRequest)->setEnabled(false);

                result=true;
                break;
            }    

            case HLA::INetworkCollaborationManager::PUBLISHED_AND_SCREENSHARED:
            {
                _getChild<QLineEdit>(LEName)->setText(QString::fromStdString(_netwrkCollaborationMgr->getProjectName()));

                _getChild<QPushButton>(PBRequest)->setEnabled(false);
                _getChild<QPushButton>(PBRequest)->setText("Camera is already owned"); 

                _getChild<QPushButton>(PBTakeOwnerShip)->setVisible(false);
                _getChild<QPushButton>(PBCancelRequest)->setVisible(false);
                _getChild<QPushButton>(PBSubscribeScreen)->setVisible(false);

                result = true;
                break;
            }
            case HLA::INetworkCollaborationManager::JOINED:
            {
                _getChild<QLabel>(LBMessage)->setText(QString::fromStdString(MsgScreenSubscribed));
                _getChild<QLineEdit>(LEName)->setText(QString::fromStdString(_netwrkCollaborationMgr->getProjectName()));

                _getChild<QPushButton>(PBRequest)->setText(QString::fromStdString(MsgRequestScreenSharing));  
                _getChild<QPushButton>(PBRequest)->setEnabled(true);

                _getChild<QPushButton>(PBTakeOwnerShip)->setVisible(false);

                _getChild<QPushButton>(PBSubscribeScreen)->setVisible(true);
                if(_netwrkCollaborationMgr->isSubscribedToScreenShare())
                    _getChild<QPushButton>(PBSubscribeScreen)->setText("Unsubscibe to shared screen");
                else
                    _getChild<QPushButton>(PBSubscribeScreen)->setText("Subscribe to shared screen");

                _getChild<QPushButton>(PBCancelRequest)->setEnabled(false);

                result=true;
                break;
            }
            case HLA::INetworkCollaborationManager::JOINED_AND_SCREENSHARED:
            {
                _getChild<QLabel>(LBMessage)->setText(QString::fromStdString(MsgScreenSubscribed));
                _getChild<QLineEdit>(LEName)->setText(QString::fromStdString(_netwrkCollaborationMgr->getProjectName()));

                _getChild<QPushButton>(PBRequest)->setEnabled(true);
                _getChild<QPushButton>(PBRequest)->setText("Cancel Screen Share");

                _getChild<QPushButton>(PBSubscribeScreen)->setVisible(false);
                _getChild<QPushButton>(PBTakeOwnerShip)->setVisible(false);
                _getChild<QPushButton>(PBCancelRequest)->setVisible(false);

                result = true;
                break;
            }
            default :
                break;
        }

        return result;
    }

    void 
    ScreenShareGUI::_handleRequestButtonClicked()
    {
        HLA::INetworkCollaborationManager::ApplicationState currState=_netwrkCollaborationMgr->getApplicationState();
        if(currState == HLA::INetworkCollaborationManager::JOINED || currState == HLA::INetworkCollaborationManager::PUBLISHED)
        {
            _getChild<QPushButton>(PBRequest)->setEnabled(false);
            
            _getChild<QLabel>(LBRequestMessage)->show();
            _getChild<QProgressBar>(PBProgress)->show();

            _getChild<QPushButton>(PBCancelRequest)->setEnabled(true);
            _getChild<QPushButton>(PBCancelRequest)->setVisible(true);

            _requestPending=true;

            _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareResponseReceivedMessageType);
            _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareRequestRejectedMessageType);
            
            _netwrkCollaborationMgr->requestScreenShare();
        }
        else if(currState == HLA::INetworkCollaborationManager::JOINED_AND_SCREENSHARED || currState == HLA::INetworkCollaborationManager::PUBLISHED_AND_SCREENSHARED)
        {
            _getChild<QPushButton>(PBRequest)->setEnabled(false);

            _netwrkCollaborationMgr->stopScreenSharing();

            _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareStopSuccessfulMessageType);
            _subscribe(_netwrkCollaborationMgr, *HLA::INetworkCollaborationManager::ScreenShareStopFailedMessageType);
        }
    }
    
    void 
    ScreenShareGUI::_handleCancelRequestClicked()
    {        
        _netwrkCollaborationMgr->cancelScreenShareRequest();
        _getChild<QProgressBar>(PBProgress)->hide();
        _getChild<QLabel>(LBRequestMessage)->hide();
        _getChild<QPushButton>(PBRequest)->setEnabled(true);
        _getChild<QPushButton>(PBCancelRequest)->setEnabled(false);
    }
}
