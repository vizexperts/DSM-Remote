#include <SMCQt/VectorLayerQueryGUI.h>
#include <SMCQt/VizComboBoxElement.h>
#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>
#include <Util/FileUtils.h>
#include <Elements/ElementsUtils.h>
#include<TERRAIN/IModelObject.h>
#include <QJSONDocument>
#include <QJSONObject>
#include <QtCore/QJsonArray>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, VectorLayerQueryGUI);
    DEFINE_IREFERENCED(VectorLayerQueryGUI, VizQt::QtGUI);
    const std::string VectorLayerQueryGUI::VECTORLAYERQUERYPOPUP = "VectorLayerQueryPopup";
    const std::string VectorLayerQueryGUI::VECTORQUERYBUILDEROBJECTNAME = "VectorQueryBuilder";
    VectorLayerQueryGUI::VectorLayerQueryGUI()
        :_queryStoragePath("/VectorQueryData/Query/")
        , _queryGroupStoragePath("/VectorQueryData/QueryGroup/")
    {

    }

    VectorLayerQueryGUI::~VectorLayerQueryGUI()
    {}

    void VectorLayerQueryGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* popupLoader = _findChild(SMP_FileMenu);
        if(popupLoader)
        {
            QObject::connect(popupLoader, SIGNAL(popupLoaded(QString)), this,
                SLOT(popupLoaded(QString)), Qt::UniqueConnection);
        }
    }


    void VectorLayerQueryGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {
                _vectorLayerQueryUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IVectorLayerQueryUIHandler>(getGUIManager());
                _areaHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IAreaUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception e)
            {
                e.LogException();
            }
        }
        // check whether the polygon has been updated
        else if (messageType == *SMCUI::IAreaUIHandler::AreaUpdatedMessageType)
        {
            if (_areaHandler.valid())
            {
                // Get the polygon
                CORE::RefPtr<CORE::IPolygon> polygon = _areaHandler->getCurrentArea();
                // Pass the polygon points to the surface area calc handler
                CORE::RefPtr<CORE::IClosedRing> line = polygon->getExteriorRing();
                if (line.valid() && line->getPointNumber() > 1)
                {
                    // Get 0th and 2nd point since line is drawn clockwise
                    osg::Vec3d first = line->getPoint(0)->getValue();
                    osg::Vec3d second = line->getPoint(2)->getValue();
                    QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
                    if (vectorLayerQueryPopupObject)
                    {
                        vectorLayerQueryPopupObject->setProperty("bottomLeftLongitude", UTIL::ToString(first.x()).c_str());
                        vectorLayerQueryPopupObject->setProperty("bottomLeftLatitude", UTIL::ToString(first.y()).c_str());
                        vectorLayerQueryPopupObject->setProperty("topRightLongitude", UTIL::ToString(second.x()).c_str());
                        vectorLayerQueryPopupObject->setProperty("topRightLatitude", UTIL::ToString(second.y()).c_str());
                    }
                }
            }
        }

        DeclarativeFileGUI::update(messageType, message);
    }


    void VectorLayerQueryGUI::popupLoaded(QString type)
    {
        if(type == "VectorQueryBuilder")
        {
            QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
            QString selectedLayerUid = vectorLayerQueryPopupObject->property("selectedLayerUid").toString();
            QString layerName = vectorLayerQueryPopupObject->property("selectedLayerName").toString();
            if (selectedLayerUid == "" || (layerName == "Select Vector Layer") || (layerName == ""))
            {
                emit showError("Select Vector Layor", "Please Choose Valid Vector Layor", true);
                vectorLayerQueryPopupObject->setProperty("selectedQueryName", QString::fromStdString("Select Query"));
                vectorLayerQueryPopupObject->setProperty("editQueryButton", QVariant::fromValue(false));
                return;
            }
            CORE::UniqueID* uid = new CORE::UniqueID(selectedLayerUid.toStdString());
            CORE::RefPtr<CORE::IWorld> iworld =
                APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            CORE::RefPtr<CORE::IObject> object = iworld->getObjectByID(uid);
            if (!object.valid())
            {
                emit showError("Vector Loading Error !", "Layer not present in Project", true);

                vectorLayerQueryPopupObject->setProperty("selectedQueryName", QString::fromStdString("Select Query"));
                vectorLayerQueryPopupObject->setProperty("editQueryButton", QVariant::fromValue(false));
                return;
            }
            CORE::RefPtr<TERRAIN::IModelObject> modelObject = object->getInterface<TERRAIN::IModelObject>();
            if (modelObject.valid())
            {
                QObject* VectorQueryBuilderObject = _findChild(VECTORQUERYBUILDEROBJECTNAME);
                VectorQueryBuilderObject->setProperty("shapeFileName", QVariant::fromValue(QString::fromStdString(modelObject->getUrl())));
                QObject::connect(VectorQueryBuilderObject, SIGNAL(closed()), this, SLOT(_resetQueryEdit()), Qt::UniqueConnection);

            }

        }
        else if (type == "VectorLayerQueryPopup")
        {

            QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
            if (vectorLayerQueryPopupObject)
            {
                _clearGroupedQueryStruct();
                _readAllQuery();
                int counter = 2;
                QList<QObject*> queryComboBoxList;

                queryComboBoxList.append(new VizComboBoxElement("Select Query", "0"));
                queryComboBoxList.append(new VizComboBoxElement("Create New Query", "1"));

                for (int i = 0; i < _queryList.size(); i++)
                {
                    std::string queryName = _queryList[i];
                    queryComboBoxList.append(new VizComboBoxElement(queryName.c_str(), UTIL::ToString(counter).c_str()));
                    counter++;
                }

                _setContextProperty("createdQueryNameList", QVariant::fromValue(queryComboBoxList));
                //populate the list
                _readAllQueryGroup();

                int count = 2;
                QList<QObject*> comboBoxList;

                comboBoxList.append(new VizComboBoxElement("Select Query Group", "0"));
                comboBoxList.append(new VizComboBoxElement("Create New Query Group", "1"));

                for (int i = 0; i < _queryGroupList.size(); i++)
                {
                    std::string queryGroupName = _queryGroupList[i];
                    comboBoxList.append(new VizComboBoxElement(queryGroupName.c_str(), UTIL::ToString(count).c_str()));
                    count++;
                }

                _setContextProperty("queryGroupList", QVariant::fromValue(comboBoxList));
                QString defaultName = "Select Query Group";
                if (_queryGroupList.size()>0)
                {
                    defaultName = _queryGroupList[_queryGroupList.size() - 1].c_str();
                }
                vectorLayerQueryPopupObject->setProperty("selectedQueryGroupName", defaultName);
                QString queryGroupName = vectorLayerQueryPopupObject->property("selectedQueryGroupName").toString();
                _editExistingQueryGroup(defaultName);


            }
            _addVectorLayerStyleList();
            _populateVectorListByType("Point"); //by default Point Geomertry
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(saveQueryGroup()), this, SLOT(_handleSaveButtonClicked()), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(addGroupedQuery()), this, SLOT(addGroupedQuery()), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(removeQueryFromList(int)), this, SLOT(_removeQueryFromList(int)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(deleteSelectedQuery(QString)), this, SLOT(_removedQuery(QString)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(groupedQuerySelected(int)), this, SLOT(groupedQuerySelected(int)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(editQueryGroup(QString)), this, SLOT(_editExistingQueryGroup(QString)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(changeLayerType(QString)), this, SLOT(_populateVectorListByType(QString)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(deleteSelectedQueryGroup(QString)), this, SLOT(_deleteQueryGroup(QString)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(runButtonClick()), this, SLOT(_handleRunButtonClicked()), Qt::UniqueConnection); // Exist
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(selectOrEditQuery(QString)), this, SLOT(_editExistingQuery(QString)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(markPosition(bool)), this, SLOT(_handlePBAreaClicked(bool)), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(closed()), this, SLOT(_vectorLayorPopupClose()), Qt::UniqueConnection);
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(clearArea()), this, SLOT(_handleClearAreaButton()), Qt::UniqueConnection);
        }
    }
    void VectorLayerQueryGUI::_vectorLayorPopupClose()
    {
        _handlePBAreaClicked(false);
        _clearGroupedQueryStruct();
        _queryGroupList.clear();
        _queryList.clear();
    }
    void VectorLayerQueryGUI::_handleClearAreaButton()
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (vectorLayerQueryPopupObject)
        {
            vectorLayerQueryPopupObject->setProperty("isMarkSelected", false);
            _handlePBAreaClicked(false);
            vectorLayerQueryPopupObject->setProperty("bottomLeftLongitude", "");
            vectorLayerQueryPopupObject->setProperty("bottomLeftLatitude", "");
            vectorLayerQueryPopupObject->setProperty("topRightLongitude", "");
            vectorLayerQueryPopupObject->setProperty("topRightLatitude", "");
        }
    }
    void VectorLayerQueryGUI::_handlePBAreaClicked(bool pressed)
    {
        if (!_areaHandler.valid())
        {
            return;
        }

        if (pressed)
        {
            _areaHandler->_setProcessMouseEvents(true);
            _areaHandler->setTemporaryState(true);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(true);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_CREATE_RECTANGLE);
            _subscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);
        }
        else
        {
            _areaHandler->_setProcessMouseEvents(false);
            _areaHandler->setTemporaryState(false);
            _areaHandler->getInterface<APP::IUIHandler>()->setFocus(false);
            _areaHandler->setMode(SMCUI::IAreaUIHandler::AREA_MODE_NONE);
            _unsubscribe(_areaHandler.get(), *SMCUI::IAreaUIHandler::AreaUpdatedMessageType);

            QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
            if (vectorLayerQueryPopupObject)
            {
                vectorLayerQueryPopupObject->setProperty("isMarkSelected", false);
            }
        }
    }
    void VectorLayerQueryGUI::_populateVectorListByType(QString value)
    {

        UTIL::ShpFileUtils::GeometryType geometryType;

        if (value == "Point")
        {
            geometryType = UTIL::ShpFileUtils::POINT;
        }
        else if (value == "Line")
        {
            geometryType = UTIL::ShpFileUtils::LINE;
        }
        else if (value == "Polygon")
        {
            geometryType = UTIL::ShpFileUtils::POLYGON;
        }
        else if (value == "Unknown")
        {
            geometryType = UTIL::ShpFileUtils::UNKNOWN;
        }

        QList<QObject*> vectorLayerList;
        int i = 0;
        vectorLayerList.append(new VizComboBoxElement("Select Vector Layer", QString::number(i)));
        CORE::RefPtr<CORE::IWorld> iworld =
            APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        const CORE::ObjectMap& omap = iworld->getObjectMap();
        for (CORE::ObjectMap::const_iterator citer = omap.begin();
            citer != omap.end();
            ++citer)
        {
            CORE::RefPtr<CORE::IObject> object = citer->second;

            CORE::RefPtr<TERRAIN::IModelObject> modelObject = object->getInterface<TERRAIN::IModelObject>();
            if (modelObject.valid())
            {
                if (modelObject->getGeometryType() == geometryType)
                {
                    std::string VectorLayerName = "";
                    VectorLayerName = object->getInterface<CORE::IBase>()->getName();
                    std::string layerUniqueId = object->getInterface<CORE::IBase>()->getUniqueID().toString();
                    vectorLayerList.append(new VizComboBoxElement(QString::fromStdString(VectorLayerName), QString::fromStdString(layerUniqueId)));
                }
            }

            CORE::RefPtr<CORE::ICompositeObject> folderObject = object->getInterface<CORE::ICompositeObject>();
            if (folderObject.valid())
            {
                const CORE::ObjectMap& folderMap = folderObject->getObjectMap();
                for (CORE::ObjectMap::const_iterator citer = folderMap.begin();
                    citer != folderMap.end();
                    ++citer)
                {
                    CORE::RefPtr<CORE::IObject> obj = citer->second;
                    CORE::RefPtr<TERRAIN::IModelObject> modelObj =
                        obj->getInterface<TERRAIN::IModelObject>();
                    if (modelObj.valid())
                    {
                        if (modelObj->getGeometryType() == geometryType)
                        {
                            std::string VectorLayerName = "";
                            VectorLayerName = obj->getInterface<CORE::IBase>()->getName();
                            std::string layerUniqueId = obj->getInterface<CORE::IBase>()->getUniqueID().toString();
                            vectorLayerList.append(new VizComboBoxElement(QString::fromStdString(VectorLayerName), QString::fromStdString(layerUniqueId)));
                        }
                    }
                }
            }

        }

        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (vectorLayerQueryPopupObject)
        {
            vectorLayerQueryPopupObject->setProperty("selectedLayerName", "Select Vector Layer");
        }
        _setContextProperty("vectorLayerList", QVariant::fromValue(vectorLayerList));
    }
    void VectorLayerQueryGUI::_deleteQueryGroup(QString value)
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (value.toStdString() == "Select Query Group" || value.toStdString() == "Create New Query Group")
        {
            emit  showError("Invalid Query Group selection", "Please Choose Valid Query Group");
            vectorLayerQueryPopupObject->setProperty("editQueryGroupButton", false);
            return;
        }

        emit question("Delete Query Group", "Do you really want to delete?", false,
            "_okSlotForDeleting()", "_cancelSlotForDeleting()");

    }
    void VectorLayerQueryGUI::_cancelSlotForDeleting()
    {
        return;
    }
    void VectorLayerQueryGUI::_okSlotForDeleting()
    {
       
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        vectorLayerQueryPopupObject->setProperty("editQueryGroupButton", false);
        vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", false);
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();
        std::string queryGroupFolder = appdataPath + _queryGroupStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryGroupFolder);
        if (!osgDB::fileExists(path))
        {
            emit  showError("Deletion Error", "Query Group Folder doesn't Exist in Application");
            return;
        }

        if (vectorLayerQueryPopupObject)
        {
            QString lpName = vectorLayerQueryPopupObject->property("selectedQueryGroupName").toString();
            if (lpName == "")
            {
                return;
            }

            std::string filename = lpName.toStdString() + ".queryGroup";
            std::string filePath = queryGroupFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, queryGroupFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
            }
            _readAllQueryGroup();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select Query Group", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New Query Group", "1"));

            for (int i = 0; i < _queryGroupList.size(); i++)
            {
                std::string queryGroupName = _queryGroupList[i];
                comboBoxList.append(new VizComboBoxElement(queryGroupName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("queryGroupList", QVariant::fromValue(comboBoxList));

            QString defaultName = "Select Query Group";
            if (_queryGroupList.size()>0)
            {
                defaultName = _queryGroupList[_queryGroupList.size() - 1].c_str();
            }
            vectorLayerQueryPopupObject->setProperty("selectedQueryGroupName", defaultName);
            QString queryGroupName = vectorLayerQueryPopupObject->property("selectedQueryGroupName").toString();
            _showParameterGrid(queryGroupName);
        }

    }
    // to decide whether to show parameters grid or not
    void VectorLayerQueryGUI::_showParameterGrid(QString selectedValue)
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);

        if (selectedValue.toStdString() == "Select Query Group")
        {
            if (vectorLayerQueryPopupObject)
            {
                vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", QVariant::fromValue(false));
            }
        }

        else if (selectedValue.toStdString() == "Create New Query Group")
        {
            if (vectorLayerQueryPopupObject)
            {
                vectorLayerQueryPopupObject->setProperty("outputName", "");
                vectorLayerQueryPopupObject->setProperty("queryGroupName", "");
                _setContextProperty("queryNameList", QVariant::fromValue(NULL));
                vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", QVariant::fromValue(true));
            }
        }
    }
    void VectorLayerQueryGUI::_clearGroupedQueryStruct()
    {
        _vectorGroupedQueryWithArea._vectorGroupedQueryStack.clear();
        _vectorGroupedQueryWithArea._bottomLeftLong.clear();
        _vectorGroupedQueryWithArea._bottomLeftLat.clear();
        _vectorGroupedQueryWithArea._topRightLong.clear();
        _vectorGroupedQueryWithArea._topRightLat.clear();
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (vectorLayerQueryPopupObject)
        {
            vectorLayerQueryPopupObject->setProperty("selectedLayerType", "Point");
            _populateVectorListByType("Point");
            vectorLayerQueryPopupObject->setProperty("selectedLayerUid", QVariant::fromValue(0));
            vectorLayerQueryPopupObject->setProperty("selectedQueryName", "Select Query");
            vectorLayerQueryPopupObject->setProperty("styleForSelectedLayer", "Select Vector Style");     
            vectorLayerQueryPopupObject->setProperty("bottomLeftLongitude","");
            vectorLayerQueryPopupObject->setProperty("bottomLeftLatitude", "");
            vectorLayerQueryPopupObject->setProperty("topRightLongitude", "");
            vectorLayerQueryPopupObject->setProperty("topRightLatitude", "");

        }



    }

    void VectorLayerQueryGUI::_editExistingQueryGroup(QString selectedValue)
    {

        _clearGroupedQueryStruct();
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        bool editChecked = vectorLayerQueryPopupObject->property("editQueryGroupButton").toBool();

        if (editChecked)
        {
            if (selectedValue.toStdString() == "Select Query Group" || selectedValue.toStdString() == "Create New Query Group")
            {
                emit showError("Invalid Query Group selection", "Select a valid Query Group to edit");
                if (vectorLayerQueryPopupObject)
                {
                    vectorLayerQueryPopupObject->setProperty("selectedQueryGroupName", "Select Query Group");
                    vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", QVariant::fromValue(false));
                    vectorLayerQueryPopupObject->setProperty("editQueryGroupButton", QVariant::fromValue(false));
                } 
                return;
            }
        }
        else
        {
            if (selectedValue.toStdString() == "Select Query Group")
            {
                vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", QVariant::fromValue(false));
                vectorLayerQueryPopupObject->setProperty("editQueryGroupButton", QVariant::fromValue(false));
                return;
            }
            else if (selectedValue.toStdString() == "Create New Query Group")
            {
                _showParameterGrid(selectedValue);
                _handlePBAreaClicked(false);
                return;
            }
        }


        bool isExist=_getQueryGroup(selectedValue.toStdString());
        if (!isExist)
        {
            vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", QVariant::fromValue(false));
            return;
        }

        if (vectorLayerQueryPopupObject && _vectorGroupedQueryWithArea._vectorGroupedQueryStack.size()>0)
        {
            groupedQuerySelected(0, false, true);
            if (!editChecked)
            {
                vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", QVariant::fromValue(false));
            }
            else
            {
                vectorLayerQueryPopupObject->setProperty("openQueryGroupParameterGrid", QVariant::fromValue(true));
            }
            QString queryGroupName = vectorLayerQueryPopupObject->property("selectedQueryGroupName").toString();
            vectorLayerQueryPopupObject->setProperty("queryGroupName", queryGroupName);

        }

    }
    bool VectorLayerQueryGUI::_getQueryGroup(std::string QueryGroupName)
    {
        _clearGroupedQueryStruct();
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string queryGroupFolder = appdataPath + _queryGroupStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryGroupFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting error", "Query Group folder not found in Application", true);
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == QueryGroupName)
            {
                found = true;
            }
        }

        if (!found)
        {
            emit showError("Read setting error", "Query Group with this name not found in Application", true);
            return false;
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + _queryGroupStoragePath + QueryGroupName + ".queryGroup"));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                emit showError("Read setting error", "Can not open file from Application for reading ", true);
                return false;
            }

            //reading JSON
            QByteArray settingData = settingFile.readAll();
            QString setting(settingData);
            QJsonDocument jsonResponse = QJsonDocument::fromJson(setting.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            QJsonObject queryGroupRootObject = jsonObject["QueryGroup"].toObject();
            if (!queryGroupRootObject.isEmpty())
            {
                QJsonObject extentObject = queryGroupRootObject["Extent"].toObject();
                if (!extentObject.isEmpty())
                {
                    if (!extentObject["BottomLeftLat"].toString().isEmpty() && !extentObject["BottomLeftLong"].toString().isEmpty() && !extentObject["TopRightLat"].toString().isEmpty() && !extentObject["TopRightLong"].toString().isEmpty())
                    {
                        _vectorGroupedQueryWithArea._bottomLeftLat   = extentObject["BottomLeftLat"].toString();
                        _vectorGroupedQueryWithArea._bottomLeftLong  = extentObject["BottomLeftLong"].toString();
                        _vectorGroupedQueryWithArea._topRightLat     = extentObject["TopRightLat"].toString();
                        _vectorGroupedQueryWithArea._topRightLong    = extentObject["TopRightLong"].toString();
                    }

                }

                QJsonArray jsonArray = queryGroupRootObject["QueryArray"].toArray();
                foreach(const QJsonValue & value, jsonArray)
                {
                    VectorGroupedQuery groupedQuery;
                    QJsonObject obj = value.toObject();
                    if (!obj["LayerType"].toString().isEmpty())
                    {
                        groupedQuery._layerType = obj["LayerType"].toString();
                    }
                    if (!obj["LayerUniqueId"].toString().isEmpty())
                    {
                        groupedQuery._layerUniqueId = obj["LayerUniqueId"].toString();
                    }
                    if (!obj["Layer"].toString().isEmpty())
                    {
                        groupedQuery._layer = obj["Layer"].toString();
                    }

                    if (!obj["QueryName"].toString().isEmpty())
                    {
                        groupedQuery._queryName = obj["QueryName"].toString();
                        if (_getQuery(groupedQuery._queryName.toStdString()))
                        {
                            groupedQuery._whereClause = _currentQueryWhereClause;
                        }
                        else
                        {
                            //Empty Query Filling Data
                            emit showError("Read setting error", "Associate Query Deleted or Not readable : " + groupedQuery._queryName, true);
                        }

                    }
                    if (!obj["Style"].toString().isEmpty())
                    {
                        groupedQuery._style = obj["Style"].toString();
                    }
                    _vectorGroupedQueryWithArea._vectorGroupedQueryStack.push_back(groupedQuery);
                }

            }
        }
        return true;
    }
    void VectorLayerQueryGUI::_addVectorLayerStyleList()
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (vectorLayerQueryPopupObject != NULL)
        {

            std::string StyleFolder = UTIL::getStyleDataPath();

            std::vector<std::string> styleTemplates;
            UTIL::getFilesFromFolder(StyleFolder, styleTemplates);

            //populate the list of templates
            QList<QObject*> styleSheetBoxList;

            styleSheetBoxList.append(new VizComboBoxElement("Select Vector Style", UTIL::ToString(0).c_str()));
            for (int i = 1; i < styleTemplates.size(); i++)
            {
                std::string templateFile = UTIL::getFileName(styleTemplates[i]);
                templateFile = templateFile.substr(0, templateFile.find("."));
                styleSheetBoxList.append(new VizComboBoxElement(templateFile.c_str(), UTIL::ToString(i).c_str()));
            }
            _setContextProperty("styleSheetList", QVariant::fromValue(styleSheetBoxList));
        }
    }
    void VectorLayerQueryGUI::addGroupedQuery()
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if(!vectorLayerQueryPopupObject)
            return;
    
        groupedQuerySelected(_vectorGroupedQueryWithArea._vectorGroupedQueryStack.size(), true, false);

    }
    void VectorLayerQueryGUI::updateGroupedQuery()
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (!vectorLayerQueryPopupObject)
            return;

        groupedQuerySelected(_vectorGroupedQueryWithArea._vectorGroupedQueryStack.size(), true, true);

    }
    void VectorLayerQueryGUI::_removeQueryFromList(int index)
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if(!vectorLayerQueryPopupObject)
            return;

        if (index >= _vectorGroupedQueryWithArea._vectorGroupedQueryStack.size())
            return;

        _vectorGroupedQueryWithArea._vectorGroupedQueryStack.removeAt(index);
        _populateQueryNameList();
        if (_vectorGroupedQueryWithArea._vectorGroupedQueryStack.size() == 0)
            return;

        if(index == 0)
        {
            groupedQuerySelected(0, false, true);
        }
        else
        {
            groupedQuerySelected(index-1, false, true);
        }
    }

    void VectorLayerQueryGUI::groupedQuerySelected(int index, bool addingNew, bool selectionOnly)
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if(!vectorLayerQueryPopupObject)
            return;

        //! previous
        
        VectorGroupedQuery groupedQuery;
        QString layerType = vectorLayerQueryPopupObject->property("selectedLayerType").toString();
        if (layerType == "" && addingNew)
        {
            emit showError("layer Type", "Layer type not specified", true);
            return;
        }
        groupedQuery._layerType = layerType;
        QString selectedLayerUid = vectorLayerQueryPopupObject->property("selectedLayerUid").toString();
        QString layer = vectorLayerQueryPopupObject->property("selectedLayerName").toString();
        if ((layer == "" || (layer == "Select Vector Layer")) && addingNew)
        {
            emit showError("Invalid Layer selection", "Please Choose Valid Vector Layor", true);
            return;
        }

        groupedQuery._layer = layer;
        groupedQuery._layerUniqueId = selectedLayerUid;

        QString queryName = vectorLayerQueryPopupObject->property("selectedQueryName").toString();
        if (((queryName == "") || (queryName == "Select Query") || (queryName == "Create New Query")) && addingNew)
        {
            emit showError("Invalid Query selection", "Please Choose Valid Query selection", true);
            return;
        }
        groupedQuery._queryName = queryName;

        QString style = vectorLayerQueryPopupObject->property("styleForSelectedLayer").toString();
        if (style != "Select Vector Style")
        groupedQuery._style = style;



        //Extent
        osg::Vec4d extents;
        bool okX = true;
        extents.x() = vectorLayerQueryPopupObject->property("bottomLeftLongitude").toString().toDouble(&okX);

        bool okY = true;
        extents.y() = vectorLayerQueryPopupObject->property("bottomLeftLatitude").toString().toDouble(&okY);

        bool okZ = true;
        extents.z() = vectorLayerQueryPopupObject->property("topRightLongitude").toString().toDouble(&okZ);

        bool okW = true;
        extents.w() = vectorLayerQueryPopupObject->property("topRightLatitude").toString().toDouble(&okW);
        bool completedAreaSpecified = okX & okY & okZ & okW;
        bool partialAreaSpecified = okX | okY | okZ | okW;


        if (!completedAreaSpecified && partialAreaSpecified)
        {
            showError("Area Extents Error", "Please Enter Valid Extents Or Remain All Empty");
            return;
        }

        if (completedAreaSpecified)
        {
            if (extents.x() >= extents.z() || extents.y() >= extents.w())
            {
                showError("Area Extents Error", "Bottom left coordinates must be less than upper right.");
                return;
            }
        }
        //Special TestCase Handling If all area Empty Clear Area
        if ((extents.x() ==0) && (extents.z()==0) && (extents.y() ==0) && (extents.w()==0))
        {
            completedAreaSpecified = true;
        }
        int previousGroupedQueryIndex = vectorLayerQueryPopupObject->property("previousGroupedQueryIndex").toInt();


        if (addingNew && !selectionOnly)
        {
            _vectorGroupedQueryWithArea._vectorGroupedQueryStack.push_back(groupedQuery); 
            if (completedAreaSpecified)
            {
                _vectorGroupedQueryWithArea._bottomLeftLong = extents.x() != 0? QString::number(extents.x()): "";
                _vectorGroupedQueryWithArea._bottomLeftLat = extents.y() != 0 ? QString::number(extents.y()) : "";
                _vectorGroupedQueryWithArea._topRightLong = extents.z() != 0 ? QString::number(extents.z()) : "";
                _vectorGroupedQueryWithArea._topRightLat = extents.w() != 0 ? QString::number(extents.w()) : "";
            }

        }
        else if (addingNew && selectionOnly)
        {
            //Update Current selected Query With new Data
            _vectorGroupedQueryWithArea._vectorGroupedQueryStack[previousGroupedQueryIndex] = groupedQuery;
            if (completedAreaSpecified)
            {
                _vectorGroupedQueryWithArea._bottomLeftLong = extents.x() != 0?QString::number(extents.x()) : "";
                _vectorGroupedQueryWithArea._bottomLeftLat = extents.y() != 0 ? QString::number(extents.y()) : "";
                _vectorGroupedQueryWithArea._topRightLong = extents.z() != 0 ? QString::number(extents.z()) : "";
                _vectorGroupedQueryWithArea._topRightLat = extents.w() != 0 ? QString::number(extents.w()) : "";
            }
        }
        else if(!selectionOnly && previousGroupedQueryIndex < _vectorGroupedQueryWithArea._vectorGroupedQueryStack.size())
        {
            _vectorGroupedQueryWithArea._vectorGroupedQueryStack[previousGroupedQueryIndex] = groupedQuery;
            if (completedAreaSpecified)
            {
                _vectorGroupedQueryWithArea._bottomLeftLong = extents.x() != 0 ? QString::number(extents.x()) : "";
                _vectorGroupedQueryWithArea._bottomLeftLat = extents.y() != 0 ? QString::number(extents.y()) : "";
                _vectorGroupedQueryWithArea._topRightLong = extents.z() != 0 ? QString::number(extents.z()) : "";
                _vectorGroupedQueryWithArea._topRightLat = extents.w() != 0 ? QString::number(extents.w()) : "";
            }
        }

        if(index < _vectorGroupedQueryWithArea._vectorGroupedQueryStack.size())
        {
            //groupedQuery = _vectorGroupedQueryWithArea._vectorGroupedQueryStack[index];
            vectorLayerQueryPopupObject->setProperty("selectedLayerType", QVariant::fromValue(_vectorGroupedQueryWithArea._vectorGroupedQueryStack[index]._layerType));
            vectorLayerQueryPopupObject->setProperty("selectedLayerName", QVariant::fromValue(_vectorGroupedQueryWithArea._vectorGroupedQueryStack[index]._layer));
            vectorLayerQueryPopupObject->setProperty("selectedLayerUid", QVariant::fromValue(_vectorGroupedQueryWithArea._vectorGroupedQueryStack[index]._layerUniqueId));
            vectorLayerQueryPopupObject->setProperty("selectedQueryName", QVariant::fromValue(_vectorGroupedQueryWithArea._vectorGroupedQueryStack[index]._queryName));
            if (!_vectorGroupedQueryWithArea._vectorGroupedQueryStack[index]._style.isEmpty())
            {
                vectorLayerQueryPopupObject->setProperty("styleForSelectedLayer", QVariant::fromValue(_vectorGroupedQueryWithArea._vectorGroupedQueryStack[index]._style));
            }      
            else
            {
                vectorLayerQueryPopupObject->setProperty("styleForSelectedLayer", "Select Vector Style");
            }
            vectorLayerQueryPopupObject->setProperty("bottomLeftLongitude", QVariant::fromValue(_vectorGroupedQueryWithArea._bottomLeftLong));
            vectorLayerQueryPopupObject->setProperty("bottomLeftLatitude", QVariant::fromValue(_vectorGroupedQueryWithArea._bottomLeftLat));
            vectorLayerQueryPopupObject->setProperty("topRightLongitude", QVariant::fromValue(_vectorGroupedQueryWithArea._topRightLong));
            vectorLayerQueryPopupObject->setProperty("topRightLatitude", QVariant::fromValue(_vectorGroupedQueryWithArea._topRightLat));
        }        

        _populateQueryNameList();
        vectorLayerQueryPopupObject->setProperty("previousGroupedQueryIndex", QVariant::fromValue(index));
    }



    void VectorLayerQueryGUI::popupClosed()
    {
        _setContextProperty("queryNameList", QVariant::fromValue(NULL));
        _setContextProperty("selectedQueryName", QVariant::fromValue(NULL));

    }


    void VectorLayerQueryGUI::_populateQueryNameList()
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if(!vectorLayerQueryPopupObject)
            return;
        int i = 0;
        QStringList queryNameList;
        queryNameList.clear();
        foreach(VectorGroupedQuery vectorQuery, _vectorGroupedQueryWithArea._vectorGroupedQueryStack)
        {
            queryNameList.push_back(vectorQuery._queryName);
                i++;
        };

        _setContextProperty("queryNameList", QVariant::fromValue(queryNameList));
    }
     //function for saving the QueryGroup
    void VectorLayerQueryGUI::_handleSaveButtonClicked()
    {
        VectorGroupedQuery  queryGroupVariable;
        QString lpName;

        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (vectorLayerQueryPopupObject)
        {
            lpName = vectorLayerQueryPopupObject->property("queryGroupName").toString();
            if (lpName == "")
            {
                emit showError("Query Group name invalid", "Query Group name not specified", true);
                return;
            }
            if ((lpName == "Select Query Group") || (lpName == "Create New Query Group"))
            {
                emit showError("Query Group name invalid", "Please enter name other than " + lpName, true);
                return;
            }

            if (_vectorGroupedQueryWithArea._vectorGroupedQueryStack.size() == 0)
            {
                emit showError("Error in Saving", "Add Atleast One Query in List", true);
                return;
            }
            bool editChecked = vectorLayerQueryPopupObject->property("editQueryGroupButton").toBool();

            bool present = _isAlreadyPresent(lpName.toStdString());

            if (present && !editChecked)
            {
                emit showError("Please Change the name", "Query Group with this name is already present", true);
                return;
            }
            if (editChecked)//overwrite or new add tempalte Handling
            {
                QString selectedQueryGroupName = vectorLayerQueryPopupObject->property("selectedQueryGroupName").toString();
                if (!selectedQueryGroupName.isEmpty())
                {
                    if (lpName == selectedQueryGroupName)
                    {
                        bool present = _isAlreadyPresent(selectedQueryGroupName.toStdString());
                        if (!present)
                        {
                            emit showError("Error in Saving", "Query Group Folder Doesn't Exist", true);
                            return;
                        }
                        else
                        {
                            //Update selected _currentQueryWhereClause with Current Data
                            //New Tempalte Added handling
                            updateGroupedQuery(); //fillup with Current Update Data
                        }
                    }
                    else
                    {
                        //validation Check
                        QString layerType = vectorLayerQueryPopupObject->property("selectedLayerType").toString();
                        if (layerType == "")
                        {
                            emit showError("layer Type", "Layer type not specified", true);
                            return;
                        }
                        QString selectedLayerUid = vectorLayerQueryPopupObject->property("selectedLayerUid").toString();
                        QString layer = vectorLayerQueryPopupObject->property("selectedLayerName").toString();
                        if (layer == "" || (layer == "Select Vector Layer"))
                        {
                            emit showError("Invalid Layer selection", "Please Choose Valid Vector Layor", true);
                            return;
                        }
                        QString queryName = vectorLayerQueryPopupObject->property("selectedQueryName").toString();
                        if ((queryName == "") || (queryName == "Select Query") || (queryName == "Create New Query"))
                        {
                            emit showError("Invalid Query selection", "Please Choose Valid Query selection", true);
                            return;
                        }

                        updateGroupedQuery(); //fillup with Current Update Data
                    }
                }
            }



            _writeQueryGroup(lpName);
            _readAllQueryGroup();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select Query Group", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New Query Group", "1"));

            for (int i = 0; i < _queryGroupList.size(); i++)
            {
                std::string queryGroupName = _queryGroupList[i];
                comboBoxList.append(new VizComboBoxElement(queryGroupName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("queryGroupList", QVariant::fromValue(comboBoxList));
            QString defaultName = "Select Query Group";
            if (_queryGroupList.size()>0)
            {
                defaultName = lpName;
                //defaultName = _queryGroupList[_queryGroupList.size() - 1].c_str();
            }
            vectorLayerQueryPopupObject->setProperty("selectedQueryGroupName", defaultName);
            QString queryGroupName = vectorLayerQueryPopupObject->property("selectedQueryGroupName").toString();
            _editExistingQueryGroup(defaultName);
        }


    }
    // internal function to write in JSON
    void VectorLayerQueryGUI::_writeQueryGroup(QString name)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::stringstream insertStatement;

        std::string queryGroupFolder = appdataPath + _queryGroupStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryGroupFolder);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }
        std::string fileName = "/" + name.toStdString() + ".queryGroup";
        //Delete File and Rewrite Again
        if (_isAlreadyPresent(name.toStdString()))
        {
            UTIL::deleteFile(queryGroupFolder + fileName);
            //delete file
        }
        QFile settingFile(QString::fromStdString(queryGroupFolder + fileName));

        QByteArray settingData;
        if (settingFile.open(QIODevice::ReadOnly))
        {
            settingData = settingFile.readAll();
            settingFile.close();
        }
        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);

        //**** writing in JSON
        QJsonObject queryGroupRootObject; // root 
        QJsonObject queryGroupObject; // root of extent and Query

        QJsonObject exntetObject; //for storing Extents
        {
            exntetObject.insert("BottomLeftLat", _vectorGroupedQueryWithArea._bottomLeftLat);
            exntetObject.insert("BottomLeftLong", _vectorGroupedQueryWithArea._bottomLeftLong);
            exntetObject.insert("TopRightLat", _vectorGroupedQueryWithArea._topRightLat);
            exntetObject.insert("TopRightLong", _vectorGroupedQueryWithArea._topRightLong);

            queryGroupObject.insert("Extent", exntetObject);
        }
        // Query
        QJsonObject queryObject;
        QJsonArray queryArray;
        for (int i = 0; i < _vectorGroupedQueryWithArea._vectorGroupedQueryStack.size(); i++)
        {
            queryObject.insert("LayerType", _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._layerType);
            queryObject.insert("LayerUniqueId", _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._layerUniqueId);
            queryObject.insert("Layer", _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._layer);
            queryObject.insert("QueryName", _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._queryName);
            queryObject.insert("Style", _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._style);
            queryArray.push_back(queryObject);
        }
        queryGroupObject.insert("QueryArray", queryArray);


        queryGroupRootObject.insert("QueryGroup", queryGroupObject);
        settingDoc.setObject(queryGroupRootObject);
        //**********
       /* settingDoctemp.setObject(settingObject);*/
        if (!settingFile.open(QIODevice::WriteOnly))
        {
            emit showError("Save setting error", "Could not open Query Group file for save");
            return;
        }

        settingFile.write(settingDoc.toJson());
    }

    //internal function to read from JSON and populate the comboboz list
    void VectorLayerQueryGUI::_readAllQueryGroup()
    {
        _queryGroupList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();
        
        std::string queryGroupFolder = appdataPath + _queryGroupStoragePath;


        std::string path = osgDB::convertFileNameToNativeStyle(queryGroupFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _queryGroupList.push_back(fileNameWithoutExtension);
        }

    }
    // checks all QuerygRoup files with the name passed and returns true if its present
    bool VectorLayerQueryGUI::_isQueryAlreadyPresent(std::string name)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string queryGroupFolder = appdataPath + _queryStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryGroupFolder);
        if (!osgDB::fileExists(path))
        {
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == name)
            {
                return true;
            }
        }

        return false;
    }
    // checks all QuerygRoup files with the name passed and returns true if its present
    bool VectorLayerQueryGUI::_isAlreadyPresent(std::string name)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string queryGroupFolder = appdataPath + _queryGroupStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryGroupFolder);
        if (!osgDB::fileExists(path))
        {
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == name)
            {
                return true;
            }
        }

        return false;
    }

    void VectorLayerQueryGUI::_removedQuery(QString value)
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (value.toStdString() == "Select Query" || value.toStdString() == "Create New Query")
        {
            emit  showError("Not a valid input Query", "Cannot delete query with this name");
            vectorLayerQueryPopupObject->setProperty("editQueryButton", false);
            return;
        }

        emit question("Delete Query", "Do you really want to delete?", false,
            "_okSlotForQueryDeleting()", "_cancelSlotForDeleting()");
    }
    void VectorLayerQueryGUI::_resetQueryEdit()
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        QString lpName = vectorLayerQueryPopupObject->property("selectedQueryName").toString();
        QString defaultName = "Select Query";
        if (_queryList.size()>0)
        {
            defaultName = _queryList[_queryList.size() - 1].c_str();
        }
        vectorLayerQueryPopupObject->setProperty("selectedQueryName", defaultName);
        vectorLayerQueryPopupObject->setProperty("editQueryButton", false);

    }
    void VectorLayerQueryGUI::_okSlotForQueryDeleting()
    {

        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        vectorLayerQueryPopupObject->setProperty("editQueryButton", false);// editing false
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string queryFolder = appdataPath + _queryStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        if (vectorLayerQueryPopupObject)
        {
            QString lpName = vectorLayerQueryPopupObject->property("selectedQueryName").toString();
            if (lpName == "")
            {
                return;
            }

            std::string filename = lpName.toStdString() + ".query";
            std::string filePath = queryFolder + "/" + filename;
            std::string fileExist = osgDB::findFileInDirectory(filename, queryFolder);

            if (!fileExist.empty())
            {
                UTIL::deleteFile(filePath);
            }
            _readAllQuery();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select Query", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New Query", "1"));

            for (int i = 0; i < _queryList.size(); i++)
            {
                std::string queryGroupName = _queryList[i];
                comboBoxList.append(new VizComboBoxElement(queryGroupName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("createdQueryNameList", QVariant::fromValue(comboBoxList));

            QString defaultName = "Select Query";
            if (_queryList.size()>0)
            {
                defaultName = _queryList[_queryList.size() - 1].c_str();
            }
            vectorLayerQueryPopupObject->setProperty("selectedQueryName", defaultName);
            QString queryGroupName = vectorLayerQueryPopupObject->property("selectedQueryName").toString();
        }

    }
    void VectorLayerQueryGUI::_readAllQuery()
    {
        _queryList.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string queryFolder = appdataPath + _queryStoragePath;


        std::string path = osgDB::convertFileNameToNativeStyle(queryFolder);
        if (!osgDB::fileExists(path))
        {
            // folder not present
            return;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        for (unsigned int currFileIndex = 0; currFileIndex < contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;

            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);

            _queryList.push_back(fileNameWithoutExtension);
        }

    }
    void VectorLayerQueryGUI::_editExistingQuery(QString selectedQuery)
    {
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        bool editChecked = vectorLayerQueryPopupObject->property("editQueryButton").toBool();
        if (editChecked)
        {
            if (selectedQuery.toStdString() == "Select Query" || selectedQuery.toStdString() == "Create New Query")
            {
                emit showError("Invalid Query Selection", "please choose valid Query");
                if (vectorLayerQueryPopupObject)
                {
                    vectorLayerQueryPopupObject->setProperty("selectedQueryName", QString::fromStdString("Select Query"));
                    vectorLayerQueryPopupObject->setProperty("editQueryButton", QVariant::fromValue(false));
                }
                return;
            }
        }
        else
        {
            if (selectedQuery.toStdString() == "Select Query")
            {
                return;
            }
        }
        if (editChecked || selectedQuery.toStdString() == "Create New Query")
        {
            QString selectedLayerUid = vectorLayerQueryPopupObject->property("selectedLayerUid").toString();
            QString layerName = vectorLayerQueryPopupObject->property("selectedLayerName").toString();
            if (selectedLayerUid == "" || (layerName == "Select Vector Layer") || (layerName == ""))
            {
                emit showError("Select Vector Layor", "Please Choose Valid Vector Layor", true);
                vectorLayerQueryPopupObject->setProperty("selectedQueryName", QString::fromStdString("Select Query"));
                vectorLayerQueryPopupObject->setProperty("editQueryButton", QVariant::fromValue(false));
                return;
            }
            CORE::UniqueID* uid = new CORE::UniqueID(selectedLayerUid.toStdString());
            CORE::RefPtr<CORE::IWorld> iworld =
                APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
            CORE::RefPtr<CORE::IObject> object = iworld->getObjectByID(uid);
            if (!object.valid())
            {
                emit showError("Vector Loading Error !", "Layer not present in Project", true);

                vectorLayerQueryPopupObject->setProperty("selectedQueryName", QString::fromStdString("Select Query"));
                vectorLayerQueryPopupObject->setProperty("editQueryButton", QVariant::fromValue(false));
                return;
            }
            QMetaObject::invokeMethod(vectorLayerQueryPopupObject, "openQueryBuilderPopup");
            QObject::connect(vectorLayerQueryPopupObject, SIGNAL(okButtonClicked()), this, SLOT(_queryBuiledrOkButtonClicked()), Qt::UniqueConnection);
        }
        if (selectedQuery.toStdString() != "Create New Query")
        {
            bool isExist = _getQuery(selectedQuery.toStdString());
            if (!isExist)
            {
                vectorLayerQueryPopupObject->setProperty("editQueryButton", QVariant::fromValue(false));
                return;
            }
            if (editChecked)
            {
                QString selectedQueryName = vectorLayerQueryPopupObject->property("selectedQueryName").toString();

                QObject* VectorQueryBuilderObject = _findChild(VECTORQUERYBUILDEROBJECTNAME);
                VectorQueryBuilderObject->setProperty("queryName", selectedQueryName);

                VectorQueryBuilderObject->setProperty("sqlWhereText", QVariant::fromValue(_currentQueryWhereClause));
            }

        }

    }
    void VectorLayerQueryGUI::_queryBuiledrOkButtonClicked()
    {
        QString lpName;

        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (vectorLayerQueryPopupObject)
        {
            lpName = vectorLayerQueryPopupObject->property("createdQueryName").toString();
            if (lpName == "")
            {
                emit showError("Queryy name Error", "Query name not specified", true);
                return;
            }
            bool editChecked = vectorLayerQueryPopupObject->property("editQueryButton").toBool();

            bool present = _isQueryAlreadyPresent(lpName.toStdString());

            if (present && !editChecked)
            {
                emit showError("Save setting error", "Query with this name is already present", true);
                return;
            }

            //fiill data
            QString whereClause = vectorLayerQueryPopupObject->property("whereClause").toString();
            if (whereClause == "")
            {
                emit showError("whereClause error", "where Clause not specified", true);
                return;
            }
            if (editChecked)//overwrite or new add tempalte Handling
            {
                QString selectedQueryName = vectorLayerQueryPopupObject->property("selectedQueryName").toString();
                if (selectedQueryName.isEmpty())
                {
                    if (lpName == selectedQueryName)
                    {
                        bool present = _isAlreadyPresent(selectedQueryName.toStdString());
                        if (!present)
                        {
                            emit showError("Save setting error", "Query Folder Doesn't exist in application", true);
                            return;
                        }
                    }
                    else
                    {
                        //New Tempalte Added handling
                    }
                }
            }

            _currentQueryWhereClause = whereClause;
            _writeQuery(lpName);
            _readAllQuery();

            //populate the list again
            int count = 2;
            QList<QObject*> comboBoxList;

            comboBoxList.append(new VizComboBoxElement("Select Query", "0"));
            comboBoxList.append(new VizComboBoxElement("Create New Query", "1"));

            for (int i = 0; i < _queryList.size(); i++)
            {
                std::string queryName = _queryList[i];
                comboBoxList.append(new VizComboBoxElement(queryName.c_str(), UTIL::ToString(count).c_str()));
            }


            _setContextProperty("createdQueryNameList", QVariant::fromValue(comboBoxList));

            if (_queryList.size() > 0)
            {
                vectorLayerQueryPopupObject->setProperty("selectedQueryName", lpName);
            }
            //reset Data
            vectorLayerQueryPopupObject->setProperty("sqlWhereText", "");
        }


    }
    // internal function to write in JSON
    void VectorLayerQueryGUI::_writeQuery(QString name)
    {
        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::stringstream insertStatement;

        std::string queryFolder = appdataPath + _queryStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryFolder);
        if (!osgDB::fileExists(path))
        {
            osgDB::makeDirectory(path);
        }

        std::string fileName = "/" + name.toStdString() + ".query";
        //Delete File and Rewrite Again
        if (_isQueryAlreadyPresent(name.toStdString()))
        {
            UTIL::deleteFile(queryFolder + fileName);
            //delete file
        }
        QFile settingFile(QString::fromStdString(queryFolder + fileName));

        QByteArray settingData;
        if (settingFile.open(QIODevice::ReadOnly))
        {
            settingData = settingFile.readAll();
            settingFile.close();
        }
        QJsonDocument settingDoc = QJsonDocument::fromJson(settingData);
        QJsonObject settingObject = settingDoc.object();


        settingObject.insert("WhereClause", _currentQueryWhereClause);
        settingDoc.setObject(settingObject);

        if (!settingFile.open(QIODevice::WriteOnly))
        {
            emit showError("Save setting Error", "Could not open Query file for save");
            return;
        }

        settingFile.write(settingDoc.toJson());
    }
    bool VectorLayerQueryGUI::_getQuery(std::string QueryName)
    {
        _currentQueryWhereClause.clear();

        UTIL::TemporaryFolder* temporaryInstance = UTIL::TemporaryFolder::instance();
        std::string appdataPath = temporaryInstance->getAppFolderPath();

        std::string queryFolder = appdataPath + _queryStoragePath;

        std::string path = osgDB::convertFileNameToNativeStyle(queryFolder);
        if (!osgDB::fileExists(path))
        {
            emit showError("Read setting error", "QueryGroup folder not found in Application", true);
            return false;
        }

        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(path);

        bool found = false;
        for (unsigned int currFileIndex = 0; currFileIndex<contents.size(); ++currFileIndex)
        {
            std::string currFileName = contents[currFileIndex];
            if ((currFileName == "..") || (currFileName == "."))
                continue;
            std::string fileNameWithoutExtension = osgDB::getNameLessExtension(currFileName);
            if (fileNameWithoutExtension == QueryName)
            {
                found = true;
            }
        }

        if (!found)
        {
            emit showError("Read setting error", "Query with this name not found in Application", true);
            return false;
        }

        else
        {
            QFile settingFile(QString::fromStdString(appdataPath + _queryStoragePath + QueryName + ".query"));

            if (!settingFile.open(QIODevice::ReadOnly))
            {
                emit showError("Read setting error", "Can not open file for reading", true);
                return false;
            }

            QByteArray settingData = settingFile.readAll();
            QString setting(settingData);
            QJsonDocument jsonResponse = QJsonDocument::fromJson(setting.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            if (!jsonObject["WhereClause"].toString().isEmpty())
            {
                _currentQueryWhereClause = jsonObject["WhereClause"].toString();
            }
            return true;
        }
        return true;
    }
    void VectorLayerQueryGUI::_handleRunButtonClicked()
    {
        _loadBusyBar(boost::bind(&VectorLayerQueryGUI::_executeQueryGroup, this), "Executing Query");
    }
    void VectorLayerQueryGUI::_executeQueryGroup()
    {
        if (!_vectorLayerQueryUIHandler.valid())
        {
            emit showError("Internal Error", "Ui Handler Not Ready", true);
            return;
        }
        QString failureQueryName = "";
        QObject* vectorLayerQueryPopupObject = _findChild(VECTORLAYERQUERYPOPUP);
        if (vectorLayerQueryPopupObject)
        {
            QString outputName = vectorLayerQueryPopupObject->property("outputName").toString();
            if (outputName == "")
            {
                emit showError("Enter Output Name", "Output layer name not specified", true);
                return;
            }
            _vectorLayerQueryUIHandler->setVectorQueryOutputName(outputName.toStdString());
        }
        QString selectedQueryGroupName = vectorLayerQueryPopupObject->property("selectedQueryGroupName").toString();
        if (selectedQueryGroupName.toStdString() == "Select Query Group" || selectedQueryGroupName.toStdString() == "Create New Query Group")
        {
            emit  showError("Invalid Query Group Selection", "Please Choose Valid Query Group");
            return;
        }

         CORE::RefPtr<CORE::IWorld> iworld =
            APP::AccessElementUtils::getWorldFromManager<APP::IGUIManager>(getGUIManager());
        for (int i = 0; i < _vectorGroupedQueryWithArea._vectorGroupedQueryStack.size(); i++)
        {
            CORE::UniqueID* uid = new CORE::UniqueID(_vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._layerUniqueId.toStdString());
            CORE::RefPtr<CORE::IObject> object = iworld->getObjectByID(uid);
            if (!object.valid())
            {
                emit showError("Vector Loading Error !", _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._layer+" Layer not present in Project", true);
                continue;
            }
            CORE::RefPtr<TERRAIN::IModelObject> modelObject = object->getInterface<TERRAIN::IModelObject>();
            if (modelObject.valid())
            {
                _vectorLayerQueryUIHandler->setShapeFilePathName(modelObject->getUrl());
                std::string queryStatement;
                queryStatement = "SELECT * from \"" + _vectorLayerQueryUIHandler->getOGRLayerName() + "\" where " + _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._whereClause.toStdString();
                _vectorLayerQueryUIHandler->setSQLQuery(queryStatement);
            }
            _vectorLayerQueryUIHandler->setLayerName(_vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._layer.toStdString() + "-" + _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._queryName.toStdString());


            double bottomLeftLong;
            double bottomLeftLat;
            double topRightLong;
            double topRightLat;

            //
            bool okX = true;
            bottomLeftLong = _vectorGroupedQueryWithArea._bottomLeftLong.toDouble();
            okX = !(bottomLeftLong <= 0);

            bool okY = true;
            bottomLeftLat = _vectorGroupedQueryWithArea._bottomLeftLat.toDouble();
            okY = !(bottomLeftLat <= 0);

            bool okZ = true;
            topRightLong = _vectorGroupedQueryWithArea._topRightLong.toDouble();
            okZ = !(topRightLong <= 0);

            bool okW = true;
            topRightLat = _vectorGroupedQueryWithArea._topRightLat.toDouble();
            okW = !(topRightLong <= 0);

            bool completedAreaSpecified = okX & okY & okZ & okW;
            bool partialAreaSpecified = okX | okY | okZ | okW;

            if (!completedAreaSpecified && partialAreaSpecified)
            {
                showError("Area Extents Error", "Please Enter Valid Extents Or Remain All Empty");
                return;
            }
            osg::Vec4d extents(bottomLeftLong, bottomLeftLat, topRightLong, topRightLat);
            _vectorLayerQueryUIHandler->setExtents(extents);
            std::string styleSheetName = _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._style.toStdString();
            if (!styleSheetName.empty())
            {
                //calculate absolute path of file 
                styleSheetName += ".json";
                std::string StyleFolder = UTIL::getStyleDataPath();
                std::string jsonFile = StyleFolder + "/" + styleSheetName;
                _vectorLayerQueryUIHandler->setStyleSheet(jsonFile);
            }
            bool result =_vectorLayerQueryUIHandler->execute(completedAreaSpecified);
            if (!result)
            {
                failureQueryName = failureQueryName + _vectorGroupedQueryWithArea._vectorGroupedQueryStack[i]._queryName +" ";
            }
            _vectorLayerQueryUIHandler->reset();
        }
        if (!failureQueryName.isEmpty())
        {
            emit showError("Query Execution Fail", " Query Execution Fail For " + failureQueryName, true);
        }
        _vectorLayerQueryUIHandler->resetFolderObject();
        QMetaObject::invokeMethod(vectorLayerQueryPopupObject, "closePopup");
    }

}