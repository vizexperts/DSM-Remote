/****************************************************************************
*
* File             : TimelineGUI.h
* Description      : TimelineGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#include <SMCQt/TimelineGUI.h>
#include <SMCQt/QMLTreeModel.h>

#include <App/AccessElementUtils.h>

#include <VizUI/ICameraUIHandler.h>

#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/IAnimationComponent.h>
#include <Core/ITickMessage.h>

#include <VizQt/QtUtils.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, TimelineGUI);
    DEFINE_IREFERENCED(TimelineGUI, DeclarativeFileGUI);

    TimelineGUI::TimelineGUI()
    {
    }

    TimelineGUI::~TimelineGUI()
    {
    }

    void TimelineGUI::_loadAndSubscribeSlots()
    {
        DeclarativeFileGUI::_loadAndSubscribeSlots();

        QObject* timelineLoader = _findChild("timelineLoader");
        if(timelineLoader != NULL)
        {
            QObject::connect(timelineLoader, SIGNAL(timelineLoaded(bool)), this, 
                SLOT(setActive(bool)), Qt::UniqueConnection);
        }
    }

    void TimelineGUI::setActive(bool value)
    {
        if(value == getActive())
        {
            return;
        }

        if(value)
        {
            // populate GUI
            _populateTimeline();

            //Connect Slots for timeline
            QObject* timeline = _findChild("timeline");
            if(timeline != NULL)
            {
                QObject::connect(timeline, SIGNAL(setStartEndDateTime(QDateTime,QDateTime)), 
                    this, SLOT(setStartEndDateTime(QDateTime,QDateTime)), Qt::UniqueConnection);

                QObject::connect(timeline, SIGNAL(slower()), this, SLOT(slower()), Qt::UniqueConnection);
                QObject::connect(timeline, SIGNAL(play()), this, SLOT(play()), Qt::UniqueConnection);
                QObject::connect(timeline, SIGNAL(pause()), this, SLOT(pause()), Qt::UniqueConnection);
                QObject::connect(timeline, SIGNAL(stop()), this, SLOT(stop()), Qt::UniqueConnection);
                QObject::connect(timeline, SIGNAL(faster()), this, SLOT(faster()), Qt::UniqueConnection);

                QObject::connect(timeline, SIGNAL(timeSliderValueChanged(int)), 
                    this, SLOT(timeSliderValueChanged(int)), Qt::UniqueConnection);
            }

            //Ensure that the animation is stopped before closing the project
            QObject* smpFileMenu = _findChild("smpFileMenu");
            if(smpFileMenu)
            {
                QObject::connect(smpFileMenu, SIGNAL(closeProject()),this, SLOT(stop()), Qt::UniqueConnection);
            }
        }
        else
        {
            //Animation Component is not stopped when GUI is closed, So calling the SLOT
            stop();
            //No need to disconnect to slots as timeline gets destroyed
            //Slots disconnect on their own
        }

        _animationUIHandler->getInterface<APP::IUIHandler>()->setFocus(value);

        QtGUI::setActive(value);
    }

    void TimelineGUI::slower()
    {
        _animationUIHandler->setTimeScale(_animationUIHandler->getTimeScale()/2.0);
    }

    void TimelineGUI::faster()
    {
        _animationUIHandler->setTimeScale(_animationUIHandler->getTimeScale()*2.0);
    }

    void TimelineGUI::play()
    {
        _animationUIHandler->play();
    }

    void TimelineGUI::pause()
    {
        _animationUIHandler->pause();
    }

    void TimelineGUI::stop()
    {
        _animationUIHandler->stop();
    }

    void TimelineGUI::timeSliderValueChanged(int value)
    {
        const boost::posix_time::ptime& startTime = _animationUIHandler->getStartDateTime();
        boost::posix_time::ptime currentTime = startTime + boost::posix_time::seconds(value);

        QDateTime qDateTime;
        VizQt::TimeUtils::BoostToQtDateTime(currentTime, qDateTime);

        _animationUIHandler->setCurrentDateTime(currentTime);
    }

    void TimelineGUI::setStartEndDateTime(QDateTime qStartDateTime, QDateTime qEndDateTime)
    {
        boost::posix_time::ptime startDateTime(VizQt::TimeUtils::QtToBoostDateTime(qStartDateTime));
        boost::posix_time::ptime endDateTime(VizQt::TimeUtils::QtToBoostDateTime(qEndDateTime));

        _animationUIHandler->setStartEndDateTime(startDateTime, endDateTime);

        _populateTimeline();

        _animationUIHandler->setTimeScale((endDateTime - startDateTime).total_seconds() * 0.01);

    }

    void TimelineGUI::_populateTimeline()
    {
        QObject* timeline = _findChild("timeline");
        if(timeline != NULL)
        {
            QDateTime qStartDateTime;
            QDateTime qEndDateTime;

            boost::posix_time::ptime startTime = _animationUIHandler->getStartDateTime();

            if(startTime.is_special())
                startTime = _animationUIHandler->getCurrentDateTime();

            VizQt::TimeUtils::BoostToQtDateTime(startTime, qStartDateTime);

            boost::posix_time::ptime endTime = _animationUIHandler->getEndDateTime();

            if(endTime.is_special())
                endTime = _animationUIHandler->getCurrentDateTime();

            VizQt::TimeUtils::BoostToQtDateTime(endTime, qEndDateTime);

            // set start/end date/time
            timeline->setProperty("startDate", QVariant::fromValue(qStartDateTime));
            timeline->setProperty("endDate", QVariant::fromValue(qEndDateTime));

            boost::posix_time::time_duration duration = (endTime - startTime);
            int totalSeconds = duration.total_seconds();

            bool animationRunning = _animationUIHandler->isAnimationRunning();
            timeline->setProperty("animationRunning", QVariant::fromValue(animationRunning));

            // set Slider range
            QObject* timelineSlider = timeline->findChild<QObject*>("timelineSlider");
            if(timelineSlider != NULL)
            {
                timelineSlider->setProperty("minimumValue", QVariant::fromValue(0));
                timelineSlider->setProperty("maximumValue", QVariant::fromValue(totalSeconds));
                timelineSlider->setProperty("value", QVariant::fromValue(0));
            }
        }
    }
    void TimelineGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            try
            {   
                CORE::RefPtr<CORE::IWorldMaintainer> wmain = 
                    APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
                _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);

                _animationUIHandler = APP::AccessElementUtils::getUIHandlerUsingManager
                    <SMCUI::IAnimationUIHandler>(getGUIManager());
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        else if(messageType == *CORE::IAnimationComponent::TimeChangeMessageType)
        {
            //update current date time
            CORE::ITimeChangeMessage* tcm = message.getInterface<CORE::ITimeChangeMessage>();

            const boost::posix_time::ptime& currentTime = tcm->getCurrentClockTime();

            QDateTime qCurrentDateTime;
            VizQt::TimeUtils::BoostToQtDateTime(currentTime, qCurrentDateTime);

            QObject* smpFileMenu = _findChild("smpFileMenu");
            if(smpFileMenu)
            {
                smpFileMenu->setProperty("currentMissionDateTime", QVariant::fromValue(qCurrentDateTime));
            }

            QObject* timelineSlider = _findChild("timelineSlider");
            if(timelineSlider != NULL)
            {
                double elapsedTime = tcm->getElapsedTime();
                timelineSlider->setProperty("value", QVariant::fromValue(elapsedTime));
            }
        }
        else if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {        
            CORE::IWorldMaintainer* wmain = APP::AccessElementUtils::getWorldMaintainerFromManager(getGUIManager());
            if(wmain)
            {
                CORE::RefPtr<CORE::IComponent> component = wmain->getComponentByName("AnimationComponent");
                if(component.valid())
                {
                    CORE::RefPtr<CORE::IAnimationComponent> animationComponent = 
                        component->getInterface<CORE::IAnimationComponent>();
                    if(animationComponent.valid())
                    {
                        _subscribe(animationComponent.get(), *CORE::IAnimationComponent::TimeChangeMessageType);
                    }
                }
            }
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }
} // namespace SMCQt
