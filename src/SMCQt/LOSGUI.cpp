/****************************************************************************
*
* File             : LOSGUI.cpp
* Description      : LOSGUI class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/
#include <SMCQt/LOSGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>

#include <Core/IDeletable.h>
#include <Core/ICompositeObject.h>
#include <Core/IPoint.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>
#include <Core/WorldMaintainer.h>

#include <SMCUI/SMCUIPlugin.h>

#include <App/IApplication.h>
#include <App/AccessElementUtils.h>
#include <App/IUIHandler.h>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/ILOSVisitor.h>

#include <Util/CoordinateConversionUtils.h>

#include <math.h>

namespace SMCQt
{
    DEFINE_META_BASE(SMCQt, LOSGUI);
    DEFINE_IREFERENCED(LOSGUI,  DeclarativeFileGUI);

    const std::string LOSGUI::LOSAnalysisObjectName = "LOSAnalysis";

    LOSGUI::LOSGUI()
        : _state(NO_MARK),
        _currentLOSMode(LOSType::NONE),
        _firstObjectCreated(false),
        _isFromUser(true)
    {}

    LOSGUI::~LOSGUI()
    {}

    void LOSGUI::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        // Check whether the application has been loaded
        if (messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            if (_visitor->getCurrentLinearLOS() != nullptr){
                //getting current end and start point
                osgEarth::Util::LinearLineOfSightNode* linearLOS = _visitor->getCurrentLinearLOS();
                
                osgEarth::GeoPoint startGeo = linearLOS->getStart();
                osgEarth::GeoPoint endGeo = linearLOS->getEnd();


                //getting IPoint of current viewerPoint and vieweingPoint
                if (_viewerPoint.valid() && _vieweingPoint.valid()) {
                    CORE::RefPtr<CORE::IPoint> viewerPoint = _viewerPoint->getInterface<CORE::IPoint>();
                    CORE::RefPtr<CORE::IPoint> vieweingPoint = _vieweingPoint->getInterface<CORE::IPoint>();

                    _calculateDistance();

                    if ((fabs(viewerPoint->getX() - startGeo.x()) > 0.0001) || (fabs(viewerPoint->getY() - startGeo.y()) > 0.0001) || (fabs(viewerPoint->getZ() - startGeo.z()) > 0.0001))
                    {
                        viewerPoint->setX(startGeo.x());
                        viewerPoint->setY(startGeo.y());
                        viewerPoint->setZ(startGeo.z());
                        _losMenuObject = _findChild(LOSAnalysisObjectName);
                        bool isViewPointChecked = _losMenuObject->property("isMarkViewSelected").toBool();
                        //if (isViewPointChecked)
                        _handlePBMarkViewPointClicked(isViewPointChecked);
                    }
                    
                    else if ((fabs(vieweingPoint->getX() - endGeo.x()) > 0.00001) || (fabs(vieweingPoint->getY() - endGeo.y()) > 0.00001) || (fabs(vieweingPoint->getZ() - endGeo.z()) > 0.00001))
                    {
                        vieweingPoint->setX(endGeo.x());
                        vieweingPoint->setY(endGeo.y());
                        vieweingPoint->setZ(endGeo.z());
                        _losMenuObject = _findChild(LOSAnalysisObjectName);
                        bool isLookatAtPointChecked = _losMenuObject->property("isMarkLookAtSelected").toBool();
                        //if (isLookatAtPointChecked)
                        _handlePBMarkLookAtClicked(isLookatAtPointChecked);
                    }
                }
            }

            if (_visitor->getCurrentRadialLOS() != nullptr)
            {
                //get projected Center of RadialLOS
                osgEarth::Util::RadialLineOfSightNode* radialLOS = _visitor->getCurrentRadialLOS();
                osgEarth::GeoPoint centerGeo = radialLOS->getCenter();

                //getting IPoint of current viewerPoint and vieweingPoint
                if ( _viewerPoint.valid() ) {
                    CORE::RefPtr<CORE::IPoint> viewerPoint = _viewerPoint->getInterface<CORE::IPoint>();

                    _calculateDistance();

                    if ((fabs(viewerPoint->getX() - centerGeo.x()) > 0.0001) || (fabs(viewerPoint->getY() - centerGeo.y()) > 0.0001) || (fabs(viewerPoint->getZ() - centerGeo.z()) > 0.0001))
                    {
                        _handlePBMarkViewPointClicked(true);
                    }
                }

            }
        }
        else if(messageType == *APP::IApplication::ApplicationConfigurationLoadedType)
        {
            // Query the PointUIHandler and set it
            try
            {   
                _pointHandler = APP::AccessElementUtils::getUIHandlerUsingManager<SMCUI::IPointUIHandler2>(getGUIManager());                
            }
            catch(const UTIL::Exception& e)
            {
                e.LogException();
            }
        }
        // Check whether the polygon has been updated
        else if(messageType == *SMCUI::IPointUIHandler2::PointCreatedMessageType)
        {
            _processPoint();
            _handlePBMarkViewPointClicked(false);
            _handlePBMarkLookAtClicked(false);
        }
        else
        {
            DeclarativeFileGUI::update(messageType, message);
        }
    }

    void LOSGUI::_calculateDistance()
    {
        CORE::RefPtr<CORE::IPoint> firstPoint = _viewerPoint->getInterface<CORE::IPoint>();
        CORE::RefPtr<CORE::IPoint> secondPoint = _vieweingPoint->getInterface<CORE::IPoint>();

        if (firstPoint.valid() && secondPoint.valid())
        {
            _losMenuObject = _findChild(LOSAnalysisObjectName);

            if (_losMenuObject)
            {
                osg::Vec3d first = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(firstPoint->getY(), firstPoint->getX(), firstPoint->getZ()));
                osg::Vec3d second = UTIL::CoordinateConversionUtils::GeodeticToECEF(osg::Vec3d(secondPoint->getY(), secondPoint->getX(), secondPoint->getZ()));

                first.z() += _losMenuObject->property("heightOffset").toDouble();
                second.z() += _losMenuObject->property("lookAtHeightOffset").toDouble();

                // calculating distance between viewer and lookAt points
                double distance = (second - first).length();
                _losMenuObject->setProperty("aerialDistance", QString::number(distance, 'f', 4)+ " metres");
            }
        }
    }

    void LOSGUI::_processPoint()
    {
        if(!_pointHandler.valid())
        {
            LOG_ERROR("Point UI handler not valid");
        }

        CORE::IPoint* point = _pointHandler->getPoint();

        if(point == NULL)
        {
            LOG_ERROR("Point is not valid");
        }

        CORE::IObject *object = point->getInterface<CORE::IObject>();

        if(object == NULL)
        {
            LOG_ERROR("Point Object is not valid");
        }

        QString pointX, pointY, pointZ;

        pointX = QString::number(point->getX(),'f',6);
        pointY = QString::number(point->getY(),'f',6);
        pointZ = QString::number(point->getZ(),'f',6);


        //getting terrain
        CORE::RefPtr<CORE::IWorldMaintainer> maintainer = CORE::WorldMaintainer::instance();
        CORE::RefPtr<CORE::IWorld> world = maintainer->getWorldMap().begin()->second;

        if (world == NULL) {
            return;
        }
        CORE::ITerrain* terrain = world->getTerrain();

        //getting GeodeticSRS value for using it in GeoPoint constructor
        osgEarth::MapNode* mapNode = terrain->getMapNode();

        const osgEarth::SpatialReference* mapSRS = mapNode->getMapSRS();
        const osgEarth::SpatialReference* geoSRS = mapSRS->getGeodeticSRS();


        switch(_state)
        {
        case VIEWPOINT_MARK: 
            {

                //updating startPoint of LinearLOS
                if (_currentLOSMode == LOSType::LINEAR)
                {
                    osgEarth::Util::LinearLineOfSightNode *linearLOS = _visitor->getCurrentLinearLOS();
                    linearLOS->setStart(osgEarth::GeoPoint(geoSRS, point->getX(), point->getY(), point->getZ()));
                }
                else if(_currentLOSMode == LOSType::CIRCULAR)//For Circular LOS
                {
                    osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
                    radialLOS->setCenter(osgEarth::GeoPoint(geoSRS, point->getX(), point->getY(), point->getZ()));

                    CORE::RefPtr<CORE::IPoint> secondPoint = _vieweingPoint->getInterface<CORE::IPoint>();
                    if (secondPoint.valid())
                    {
                        osg::Vec3d first = osg::Vec3d(point->getX(), point->getY(), point->getZ());
                        osg::Vec3d second = osg::Vec3d(secondPoint->getX(), secondPoint->getY(), secondPoint->getZ());

                        double radius = 0;
                        osg::Vec3d lookAt(0, 0, 0);

                        _visitor->configureRadiaLOS(first, second, radius, lookAt);

                        // setting radius and orientation angle of circular LOS
                        radialLOS->setRadius(radius);
                        radialLOS->setLookAt(lookAt);
                    }
                }

                _visitor->setObserverPosition(osg::Vec3d(point->getX(), point->getY(), point->getZ()));

                // delete previous point 
                _removeObject(_viewerPoint.get());
                _viewerPoint = NULL;
                
                // get pointer of new point
                _viwerActualAlt=point->getZ();
                _viewerPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
                _viewerPoint->getInterface<CORE::IBase>()->setName("View Point");
                //set the property of viewPoint position
                _isFromUser = false;
                _losMenuObject->setProperty("viewPointLongitude",pointX);
                _losMenuObject->setProperty("viewPointLatitude",pointY);
                _losMenuObject->setProperty("viewPointAltitude",pointZ);
                _isFromUser = true;

                //if DirMark is clicked also if it is clicked first time, then create LOS Object
                if (!_firstObjectCreated && _vieweingPoint.valid())
                {
                    _execute();
                    _calculateDistance();
                    _firstObjectCreated = true;
                    if (_visitor->getLOSType() == GISCOMPUTE::ILOSVisitor::LOS_LINE)
                    {
                        _currentLOSMode = LOSType::LINEAR;
                    }
                    else
                    {
                        _currentLOSMode = LOSType::CIRCULAR;
                    }
                }

                break;
            }

        case DIR_MARK: 
            {
                if (_viewerPoint.valid())
                {
                    CORE::RefPtr<CORE::IPoint> firstPoint = _viewerPoint->getInterface<CORE::IPoint>();

                    //updating startPoint of LinearLOS
                    if (_currentLOSMode == LOSType::LINEAR)
                    {
                        osgEarth::Util::LinearLineOfSightNode *linearLOS = _visitor->getCurrentLinearLOS();
                        linearLOS->setEnd(osgEarth::GeoPoint(geoSRS, point->getX(), point->getY(), point->getZ()));
                    }
                    else if (_currentLOSMode == LOSType::CIRCULAR)//for circular LOS
                    {
                        osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();

                        //calculating update radius
                        osg::Vec3d first = osg::Vec3d(firstPoint->getX(), firstPoint->getY(), firstPoint->getZ());
                        osg::Vec3d second = osg::Vec3d(point->getX(), point->getY(), point->getZ());
                        

                        double radius = 0;
                        osg::Vec3d lookAt(0, 0, 0);
                        _visitor->configureRadiaLOS(first, second, radius, lookAt);

                        // setting radius and orientation angle of circular LOS
                        radialLOS->setRadius(radius);
                        radialLOS->setLookAt(lookAt);
                    }
                }

                _visitor->setObservationPoint(osg::Vec3d(point->getX(), point->getY(), point->getZ()));


                // delete previous point 
                _removeObject(_vieweingPoint.get());
                _vieweingPoint = NULL;

                // get pointer of new point
                _vieweingPoint = (point) ? point->getInterface<CORE::IObject>() : NULL;
                _vieweingPoint->getInterface<CORE::IBase>()->setName("Look At");

                //set the property of loolAt position
                _isFromUser = false;
                _losMenuObject->setProperty("lookAtLongitude",pointX);
                _losMenuObject->setProperty("lookAtLatitude",pointY);
                _losMenuObject->setProperty("lookAtAltitude",pointZ);
                _isFromUser = true;

                //if Viewing point is clicked also if it is clicked first time, then create LOS Object
                if (!_firstObjectCreated && _viewerPoint.valid())
                {
                    _execute();
                    _calculateDistance();
                    _firstObjectCreated = true;
                    if (_visitor->getLOSType() == GISCOMPUTE::ILOSVisitor::LOS_LINE)
                    {
                        _currentLOSMode = LOSType::LINEAR;
                    }
                    else
                    {
                        _currentLOSMode = LOSType::CIRCULAR;
                    }
                }

                break;
            }
        default:
            break;
        }

        //_calculateAngle();
    }


    // XXX - Move these slots to a proper place
    void LOSGUI::setActive(bool value)
    {
        if(getActive() == value)
            return;

        // Focus UI Handler and activate GUI
        try
        {
            if(value)
            {
                _reset();
                CORE::IVisitorFactory* vfactory = CORE::CoreRegistry::instance()->getVisitorFactory();

                _visitor = vfactory->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::LOSVisitorType)
                    ->getInterface<GISCOMPUTE::ILOSVisitor>();  

                //register this Object for ITickMessageType for WorldMaintainer
                CORE::RefPtr<CORE::IReferenced> observer = this->getInterface(CORE::IObserver::getInterfaceName());
                CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
                CORE::RefPtr<CORE::IObservable> wmObservable = wm->getInterface<CORE::IObservable>();
                wmObservable->registerObserver(observer->getInterface<CORE::IObserver>(), *CORE::IWorldMaintainer::TickMessageType);

                _losMenuObject = _findChild(LOSAnalysisObjectName);

                if(_losMenuObject)
                {
                    connect(_losMenuObject, SIGNAL(markLookAtPosition(bool)),this, 
                        SLOT(_handlePBMarkLookAtClicked(bool)),Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(markViewPointPosition(bool)),this, 
                        SLOT(_handlePBMarkViewPointClicked(bool)),Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(changeLosType(int)),this, 
                        SLOT(_changeLosType(int)),Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(circularFill(bool)), this,
                        SLOT(_changeCircularFill(bool)), Qt::UniqueConnection);
                  
                    connect(_losMenuObject, SIGNAL(circularSectorAngle(double)), this,
                        SLOT(_changeCircularSectorAngle(double)), Qt::UniqueConnection);
                    
                    connect(_losMenuObject, SIGNAL(transperancyValue(double)), this,
                        SLOT(_changeTransperancy(double)), Qt::UniqueConnection);

                    connect(_losMenuObject, SIGNAL(numberOfSpokes(int)), this,
                        SLOT(_numberOfSpokes(int)), Qt::UniqueConnection);

                    //real-time value updating handler
                    connect(_losMenuObject, SIGNAL(viewerHeight(double)), this,
                        SLOT(_changeViewerHeight(double)), Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(lookAtHeight(double)), this,
                        SLOT(_changeLookAtHeight(double)), Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(viewerLat(double)), this,
                        SLOT(_changeViewerLat(double)), Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(viewerLong(double)), this,
                        SLOT(_changeViewerLong(double)), Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(lookAtLat(double)), this,
                        SLOT(_changeLookAtLat(double)), Qt::UniqueConnection);
                    connect(_losMenuObject, SIGNAL(lookAtLong(double)), this,
                        SLOT(_changeLookAtLong(double)), Qt::UniqueConnection);
                }

                //make currentLOSMode to None
                _currentLOSMode = LOSType::NONE;

                //Currently first object not created from this menue so marking this
                _firstObjectCreated = false;
            }
            else
            {
                _visitor = NULL;
                _removeObject(_viewerPoint.get());
                _removeObject(_vieweingPoint.get());

                _viewerPoint = NULL;
                _vieweingPoint = NULL;

                _setPointHandlerEnabled(false);

                _reset();

                //deregister this message for ITickMessage from WorldMaintainer
                CORE::RefPtr<CORE::IReferenced> observer = this->getInterface(CORE::IObserver::getInterfaceName());
                CORE::RefPtr<CORE::IWorldMaintainer> wm = CORE::WorldMaintainer::instance();
                CORE::RefPtr<CORE::IObservable> wmObservable = wm->getInterface<CORE::IObservable>();
                wmObservable->unregisterObserver(observer->getInterface<CORE::IObserver>(), *CORE::IWorldMaintainer::TickMessageType);

                //make currentLOSMode to None
                _currentLOSMode = LOSType::NONE;

            }

            // TODO: SetFocus of uihandler to true and subscribe to messages
            DeclarativeFileGUI::setActive(value);
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }
    void LOSGUI::_changeLosType(int losType)
    {
        _visitor->setLOSType(static_cast<vizGISCompute::ILOSVisitor::LOS_TYPE>(losType));
       
        if (_currentLOSMode == LOSType::LINEAR)
        {
            _currentLOSMode = LOSType::CIRCULAR;
            if (_visitor->getCurrentLinearLOS() != nullptr) {
                //Removing current LinearLOS
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
                CORE::RefPtr<CORE::IObject> losObject = _visitor->getCurrentObject();
                losObject->getInterface<CORE::IDeletable>()->remove();

                //making current Linear LOS null
                _visitor->setCurrentLinearLOSNull();

                //making new circularLOS
                _execute();
                _calculateDistance();
            }
        }
        else if (_currentLOSMode == LOSType::CIRCULAR)
        {
            _currentLOSMode = LOSType::LINEAR;
            if (_visitor->getCurrentRadialLOS() != nullptr) {
                //Removing current RadialLOS
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
                CORE::RefPtr<CORE::IObject> losObject = _visitor->getCurrentObject();
                losObject->getInterface<CORE::IDeletable>()->remove();

                //making current Radial LOS null
                _visitor->setCurrentRadialLOSNull();

                //making new circularLOS
                _execute();
                _calculateDistance();
            }
        }
    }
    void LOSGUI::_handlePBMarkViewPointClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = VIEWPOINT_MARK;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(VIEWPOINT_MARK == lastState)
            {   
                _state = NO_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }
    }

    void LOSGUI::_handlePBMarkLookAtClicked(bool pressed)
    {
        PointSelectionState lastState = _state;
        if(pressed)
        {
            _state = DIR_MARK;
            _setPointHandlerEnabled(pressed);
        }
        else 
        {
            if(DIR_MARK == lastState)
            {
                _state = NO_MARK;
                _setPointHandlerEnabled(pressed);
            }
        }

    }

    void LOSGUI::_changeCircularFill(bool value) {
        if (_visitor->getCurrentRadialLOS() != nullptr)
        {
            osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
            radialLOS->setFill(value);
        }
    }

    void LOSGUI::_changeCircularSectorAngle(double value) {
        if (_visitor->getCurrentRadialLOS() != nullptr)
        {
            _visitor->setSectorAngle(osg::PI * (value / 180));
            osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
            radialLOS->setSectorAngle(_visitor->getSectorAngle());
        }
    }

    void LOSGUI::_numberOfSpokes(int value)
    {
         if (_visitor->getCurrentRadialLOS() != nullptr)
        {
            osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
            radialLOS->setNumSpokes(value);
        }
    }

    void LOSGUI::_changeTransperancy(double value)
    {
        if (_visitor->getCurrentRadialLOS() != nullptr)
        {
            osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
            radialLOS->setGoodColor(osg::Vec4f(0.0f, 1.0f, 0.0f, (1.0 - value)));
            radialLOS->setBadColor(osg::Vec4f(1.0f, 0.0f, 0.0f, (1.0 - value)));
        }
    }

    void LOSGUI::_setPointHandlerEnabled(bool enable)
    {
        if(_pointHandler.valid())
        {
            if(enable)
            {
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(true);
                _pointHandler->setTemporaryState(true);
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_CREATE_POINT);
                _subscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);                
            }
            else
            {
                _pointHandler->setMode(SMCUI::IPointUIHandler2::POINT_MODE_NONE);
                _pointHandler->getInterface<APP::IUIHandler>()->setFocus(false);
                _unsubscribe(_pointHandler.get(), *SMCUI::IPointUIHandler2::PointCreatedMessageType);
            }
        }
        else
        {
            emit showError("Filter Error", "initiate mark trans/recv position");
        }
    }

    void LOSGUI::_changeViewerHeight(double value) 
    {
        _visitor->setHeightOffset(value);
        if (_currentLOSMode == LOSGUI::LOSType::LINEAR)
        {
            osgEarth::Util::LinearLineOfSightNode* linearLOS = _visitor->getCurrentLinearLOS();
            linearLOS->setStartHeightOffset(value);
        }

        if (_currentLOSMode == LOSGUI::LOSType::CIRCULAR)
        {
            osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
            radialLOS->setHeightOffset(value);
          
            CORE::RefPtr<CORE::IPoint> viewer = _viewerPoint->getInterface<CORE::IPoint>();
            CORE::RefPtr<CORE::IPoint> lookAt = _vieweingPoint->getInterface<CORE::IPoint>();
            if (lookAt.valid() && viewer.valid())
            {
                osg::Vec3d first = osg::Vec3d(viewer->getX(), viewer->getY(), viewer->getZ());
                osg::Vec3d second = osg::Vec3d(lookAt->getX(), lookAt->getY(), lookAt->getZ());

                double radius = 0;
                osg::Vec3d lookAt(0, 0, 0);

                _visitor->configureRadiaLOS(first, second, radius, lookAt);

                // setting radius and orientation angle of circular LOS
                radialLOS->setRadius(radius);
                radialLOS->setLookAt(lookAt);
            }
        }

    }

    void LOSGUI::_changeLookAtHeight(double value) 
    {
        _visitor->setLookAtHeightOffset(value);
        if (_currentLOSMode == LOSGUI::LOSType::LINEAR)
        {
            osgEarth::Util::LinearLineOfSightNode* linearLOS = _visitor->getCurrentLinearLOS();
            linearLOS->setEndHeightOffset(value);
        }

    }

    void LOSGUI::_changeViewerLat(double value)
    {
        if (_isFromUser)
        {
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
            CORE::ITerrain* terrain = world->getTerrain();
                
            if (_currentLOSMode == LOSGUI::LOSType::LINEAR)
            {
                osgEarth::Util::LinearLineOfSightNode* linearLOS = _visitor->getCurrentLinearLOS();
                osgEarth::GeoPoint startGeo = linearLOS->getStart();

                // updating lat value and updating our point's position also
                startGeo.y() = value;
                _viewerPoint->getInterface<CORE::IPoint>()->setValue(startGeo.x(), startGeo.y(), startGeo.z());

                // setting updated position to los
                linearLOS->setStart(startGeo);

            } 
            else if (_currentLOSMode == LOSGUI::LOSType::CIRCULAR)
            {
                osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
                osgEarth::GeoPoint centerGeo = radialLOS->getCenter();
            
                centerGeo.y() = value;
                _viewerPoint->getInterface<CORE::IPoint>()->setValue(centerGeo.x(), centerGeo.y(), centerGeo.z());

                radialLOS->setCenter(centerGeo);
            }
        }
    }

    void LOSGUI::_changeViewerLong(double value)
    {
        if (_isFromUser)
        {
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
            CORE::ITerrain* terrain = world->getTerrain();

            if (_currentLOSMode == LOSGUI::LOSType::LINEAR)
            {
                osgEarth::Util::LinearLineOfSightNode* linearLOS = _visitor->getCurrentLinearLOS();
                osgEarth::GeoPoint startGeo = linearLOS->getStart();

                // updating long value and updating our point's position also
                startGeo.x() = value;
                _viewerPoint->getInterface<CORE::IPoint>()->setValue(startGeo.x(), startGeo.y(), startGeo.z());

                linearLOS->setStart(startGeo);
            }
            else if (_currentLOSMode == LOSGUI::LOSType::CIRCULAR)
            {
                osgEarth::Util::RadialLineOfSightNode *radialLOS = _visitor->getCurrentRadialLOS();
                osgEarth::GeoPoint centerGeo = radialLOS->getCenter();

                // updating long value and updating our point's position also
                centerGeo.x() = value;
                _viewerPoint->getInterface<CORE::IPoint>()->setValue(centerGeo.x(), centerGeo.y(), centerGeo.z());

                radialLOS->setCenter(centerGeo);
            }
        }
    }

    void LOSGUI::_changeLookAtLat(double value)
    {
        if (_isFromUser)
        {
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
            CORE::ITerrain* terrain = world->getTerrain();

            if (_currentLOSMode == LOSGUI::LOSType::LINEAR)
            {
                osgEarth::Util::LinearLineOfSightNode* linearLOS = _visitor->getCurrentLinearLOS();
                osgEarth::GeoPoint endGeo = linearLOS->getEnd();

                endGeo.y() = value;
                _vieweingPoint->getInterface<CORE::IPoint>()->setValue(endGeo.x(), endGeo.y(), endGeo.z());
               
                linearLOS->setEnd(endGeo);
            }
        }
    }

    void LOSGUI::_changeLookAtLong(double value)
    {
        if (_isFromUser)
        {
            CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
            CORE::ITerrain* terrain = world->getTerrain();

            if (_currentLOSMode == LOSGUI::LOSType::LINEAR)
            {
                osgEarth::Util::LinearLineOfSightNode* linearLOS = _visitor->getCurrentLinearLOS();
                osgEarth::GeoPoint endGeo = linearLOS->getEnd();

                endGeo.x() = value;
                _vieweingPoint->getInterface<CORE::IPoint>()->setValue(endGeo.x(), endGeo.y(), endGeo.z());

                linearLOS->setEnd(endGeo);
            }
        }
    }

    void LOSGUI::_execute()
    {
        try
        {
            GISCOMPUTE::IFilterVisitor* fv = _visitor->getInterface<GISCOMPUTE::IFilterVisitor>();

            if(fv == NULL)
                return;

            if(fv->getFilterStatus() != GISCOMPUTE::FILTER_PENDING)
            {
                // Get the terrain handle and pass the visitor to terrain
                CORE::RefPtr<CORE::IWorld> world = APP::AccessElementUtils::getWorldFromManager(getGUIManager());
                CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
                CORE::RefPtr<CORE::IBase> terrainobj = terrain->getInterface<CORE::IBase>();
                terrainobj->accept(*_visitor->getInterface<CORE::IBaseVisitor>());
            }
        }
        catch(const UTIL::Exception& e)
        {
            e.LogException();
        }
    }


    void LOSGUI::_reset()
    {
        _losMenuObject = _findChild(LOSAnalysisObjectName);

        if( _losMenuObject != NULL)
        {
            _isFromUser = false;
            _losMenuObject->setProperty("isMarkViewSelected", false);
            _losMenuObject->setProperty("isMarkLookAtSelected", false);
            _losMenuObject->setProperty("viewPointLatitude","");
            _losMenuObject->setProperty("viewPointLongitude","");
            _losMenuObject->setProperty("viewPointAltitude","");
            _losMenuObject->setProperty("lookAtLongitude","");
            _losMenuObject->setProperty("lookAtLatitude","");
            _losMenuObject->setProperty("lookAtAltitude","");
            _losMenuObject->setProperty("isStartButtonEnabled",true);
            _losMenuObject->setProperty("isStopButtonEnabled",false);
            _isFromUser = true;
        }
        _state = NO_MARK;
        _viwerActualAlt=0.0;
    }

} // namespace SMCQt
