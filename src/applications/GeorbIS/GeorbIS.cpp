#ifndef _DEBUG
#ifdef WIN32
#include <windows.h>
#endif //WIN32
#include <stdio.h>
#ifdef WIN32
#include <io.h>
#endif //WIN32
#include <fcntl.h>
#include <fstream>
#endif

#include <VizUI/UIHandlerManager.h>
#include <VizUI/IBasemapUIHandler.h>
#include <VizQt/QtGUIManager.h>
#include <Elements/World.h>
#include <Core/CoreRegistry.h>
#include <Core/IVisitorFactory.h>
#include <App/WorldManager.h>

#include <osgDB/DynamicLibrary>
#include <osgViewer/Viewer>
#include <osg/ArgumentParser>
#include <osgEarthUtil/LogarithmicDepthBuffer>

#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IMinMaxElevCalcVisitor.h>

#include <Loader/IApplicationReaderWriter.h>
#include <Util/FileUtils.h>
#include <Loader/IWorldReaderWriter.h>
#include <Core/IWorld.h>

#include <App/ApplicationRegistry.h>
#include <App/IUIHandlerManager.h>
#include <Util/Log.h>
#include <Util/AbstractFactory.h>

#include <GISCompute/ISteepPathCalcVisitor.h>
#include <Core/InterfaceUtils.h>
#include <Core/NamedAttribute.h>
#include <Elements/IIntersectionComponent.h>
#include <osgGA/StateSetManipulator>
#ifdef USE_QT4
#include <QtGui/QSplashScreen>
#include <QtGui/QApplication>
#else
#include <QtWidgets/QApplication>
#include <QtWidgets/QSplashScreen>
#endif
#include <VizQt/IQtApplication.h>
#include <QtGui/QPixmap>

#include <iostream>
#include <stdlib.h>
#include <fstream>

#include <DB/ReadFile.h>

#include <QtCore/QDir>
#include <QtCore/QDateTime>

#include <QtCore/QJSONDocument>
#include <QtCore/QJSONObject>

#include <SMCElements/ITourComponent.h>
#include <Elements/ISettingComponent.h>
#include <Elements/ElementsUtils.h>

#include <OSVRController/OSVRControllerPlugin.h>
#include <OSVRController/IOSVRControllerManager.h>

#ifdef _MSC_VER
#    ifdef NDEBUG
#        pragma comment(linker, "/SUBSYSTEM:WINDOWS")
#    else
#        pragma comment(linker, "/SUBSYSTEM:CONSOLE")
#    endif
#endif

//For Hidding the Console
//ForInfo on SUBSYTEM >> http://msdn.microsoft.com/en-us/library/fcc1zstk(v=vs.71).aspx
//mainCRTStartup is the main point of entry in Windows Application
//#ifndef _DEBUG
//    #pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
//#endif

void setupLogging();

void setupInstallerMode();

#ifndef _DEBUG
// function to set up the console in winmain app.
void SetupConsole();

#ifdef USE_QT4
void myMessageOutput(QtMsgType type, const char *msg);
#else
void myMessageOutput(QtMsgType type, const QMessageLogContext &, const QString &);
#endif

#endif

#ifdef WIN32
#ifdef _DEBUG
int main(int argc, char* argv[])
#else //_DEBUG

// this is a global file handle that will be allocated to a console
//or debug log based upon the flags set in %DSM_DIR%/config/startupParameters.txt
FILE* hf_out = NULL;
char* libvar;
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow )
#endif //_DEBUG

#else //WIN32
int main(int argc, char* argv[])
#endif //WIN32
{       

    //------------------------------------------------------------------------
    // Load application using an XML configuration file
    //------------------------------------------------------------------------
    CORE::RefPtr<CORE::NamedGroupAttribute> ngattr = new CORE::NamedGroupAttribute("");    
	
#ifdef WIN32
#ifdef _DEBUG
	osg::ArgumentParser arguments(&argc, argv);

	QApplication qapp(argc, argv);
#else //_DEBUG

	//! Flag to show the console.
	//! Note: If the application is running in debug mode,
	//! this flag will be ignored and console will show up.
	bool console = false;

	//! Flag to enable the Debug log.
	//! Note: If the application is running in debug mode,
	//! this flag will be ignored.
	bool debugLog = false;

	if (console || debugLog)
	{
#ifdef USE_QT4
		qInstallMsgHandler(myMessageOutput);
#else //USE_QT4
		qInstallMessageHandler(myMessageOutput);
#endif //USE_QT4
	}
	osg::ArgumentParser arguments(&__argc, __argv);

	QApplication qapp(__argc, __argv);

	// set up the console and debug flags
	std::ifstream startupParametersFileHandle("../../config/startupParameters.txt");
	if (startupParametersFileHandle)
	{
		char startupParametersTemp[1024];
		while (!startupParametersFileHandle.eof())
		{
			// print name
			startupParametersFileHandle >> startupParametersTemp;
			if (strcmp(startupParametersTemp, "console") == 0)
			{
				startupParametersFileHandle >> startupParametersTemp;
				if (strcmp(startupParametersTemp, "true") == 0)
				{
					console = true;
				}
			}

			else if (strcmp(startupParametersTemp, "log") == 0)
			{
				startupParametersFileHandle >> startupParametersTemp;
				if (strcmp(startupParametersTemp, "true") == 0)
				{
					debugLog = true;
				}
			}
		}
	}
	else
	{
		std::cout << "There is no ./config/startupParameters.txt file" << std::endl;
		exit(1);
	}
	// close the file handel
	startupParametersFileHandle.close(); 
	
	// check if enable console first. So higher priority for console flag.
	if (console)
	{
		SetupConsole();
		printf("\n       \t\t\t--- GeorbIS Console ---\n\n");
	}

	// else check to enable to debug log
	// debug log will not have logging messages of log4j logging system, but console will have.
	else if (debugLog)
	{
		UTIL::TemporaryFolder* temp = UTIL::TemporaryFolder::instance();
		std::string fileName = temp->getAppFolderPath() + "/" + "GeorbISLog.txt";

		hf_out = fopen(fileName.c_str(), "wt+");

		setvbuf(hf_out, NULL, _IONBF, 1024);

		// redirecting the buffers to the file handle
		*stdout = *hf_out;
		*stderr = *hf_out;
		printf("\n       \t\t\t--- GeorbIS Debug Log ---\n\n");
	}

#endif //_DEBUG

#else //WIN32
	osg::ArgumentParser arguments(&argc, argv);

	QApplication qapp(argc, argv);
#endif //WIn32

	std::string developmentMode = UTIL::getEnvironmentVariable("Development");
    if (developmentMode != "true")
    {
       setupInstallerMode();
    }
	else
	{
		std::cout <<"Not setting environment \n";
	}

    std::string configFile("../../config/georbISconfig.xml");
    std::string defaultproject("../../tests/Default.pro");

    //CORE::RefPtr<CORE::NamedAttribute> fileAttr = CORE::NamedAttribute::CreateFromType(CORE::DataType::STRING, "--file");    
    //fileAttr->fromString(configFile);
    //ngattr->addAttribute(*fileAttr);

	//! Create HTML log.
    setupLogging();

    QPixmap pixmap;
    std::string bmpImage = UTIL::getDataDirPath() +  "/resource/GeorbIS_Splash.bmp";
    pixmap.load(bmpImage.c_str());
    QSplashScreen* splashscreen = new QSplashScreen(pixmap);
    splashscreen->show();

    QCoreApplication::setOrganizationName("VizExperts");
    QCoreApplication::setOrganizationDomain("vizexperts.com");
    QCoreApplication::setApplicationName("GeorbIS");

    CORE::RefPtr<CORE::NamedAttribute> configAttr = CORE::NamedAttribute::CreateFromType(CORE::DataType::STRING, "--appconfig");    
    configAttr->fromString(configFile);
    ngattr->addAttribute(*configAttr);

    CORE::RefPtr<CORE::NamedAttribute> projectConfigAttr = CORE::NamedAttribute::CreateFromType(CORE::DataType::STRING, "--project");    

    ////
    std::string projectFile;
    if(arguments.read("--project", projectFile))
    {
        defaultproject = projectFile;
    }
    ///

    //projectConfigAttr->fromString(defaultproject);
    ngattr->addAttribute(*projectConfigAttr);

    std::string libraryname = UTIL::getPlatformLibraryName(STR(NAMESPACE(Loader)));
    osg::ref_ptr<osgDB::DynamicLibrary> dl = osgDB::DynamicLibrary::loadLibrary(libraryname);

    typedef LOADER::IApplicationReaderWriter* (*CreateApplicationLoaderPtr)();
    typedef void (*DestroyApplicationLoaderPtr)(LOADER::IApplicationReaderWriter*);

    CreateApplicationLoaderPtr createLoader     = (CreateApplicationLoaderPtr)(dl->getProcAddress("CreateApplicationLoader"));
    DestroyApplicationLoaderPtr destroyLoader   = (DestroyApplicationLoaderPtr)(dl->getProcAddress("DestroyApplicationLoader"));
    LOADER::IApplicationReaderWriter* loader = createLoader();
    CORE::RefPtr<APP::IApplication> application = NULL;

    CORE::RefPtr<APP::IApplicationFactory> af = APP::ApplicationRegistry::instance()->getApplicationFactory();

    // XXX - Throw exception ApplicationTypeNotFound in applicationfactory if applicationtype is invalid
    const APP::IApplicationType* at = af->getApplicationType("QtApplication");


    application  = af->createApplication(*at);

    CORE::RefPtr<VizQt::IQtApplication> qtApplication = application->getInterface<VizQt::IQtApplication>();
    if(qtApplication.valid())
    {
        qtApplication->setQApplication(&qapp, false);
        //qtApplication->setSplashScreen(splashscreen);
        if(arguments.read("--osg"))
        {
            qtApplication->setUseQt(false);
        }
        //std::string projectFileName
        //if(argumets.read(""))
    }

    //try
    {

        application = loader->read(*ngattr, application);

        if(qtApplication.valid() && qtApplication->useQt())
        {
			CORE::RefPtr<VizQt::IQtGUIManager> qtapp = application->getManagerByName("QtGUIManager")->getInterface<VizQt::IQtGUIManager>();
			CORE::RefPtr<OSVRCONTROLLER::IOSVRControllerManager> OSVRManagerObj = application->getManagerByName("OSVRControllerManager")->getInterface<OSVRCONTROLLER::IOSVRControllerManager>();
			QWidget* mainWindow = qtapp->getLayoutWidget();
			QView* mainWindowView = qtapp->getLayoutView();
			VizQt::VizQuickView* mainWindowVizView = static_cast<VizQt::VizQuickView*> (mainWindowView);
			OSVRManagerObj->setGraphicsWidget(mainWindowVizView->_gw);

            mainWindow->setWindowTitle("GeorbIS");
            std::string iconFile = ":/icons/georbis.png";
             
            QIcon GeorbISIcon(QString(iconFile.c_str()));

            if (GeorbISIcon.isNull())
            {
                std::cout << "icon not found." << std::endl;
            }
            else
            {
                mainWindow->setWindowIcon(GeorbISIcon);
            }
        }
    }
    /*catch(UTIL::Exception& e)
    {
    e.LogException(UTIL::Log::LOG_ERROR);
    destroyLoader(loader);
    return 0;
    }*/

    destroyLoader(loader);

    // XXX - Doing this because composite viewer changes
    // XXX - Ideally should be done inside qtguimanager or a qt application
    CORE::RefPtr<CORE::IObservable> aobservable = application->getInterface<CORE::IObservable>();
    if(aobservable.valid())
    {
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*APP::IApplication::ApplicationConfigurationLoadedType);
        aobservable->notifyObservers(*APP::IApplication::ApplicationConfigurationLoadedType, *msg);
    }

    ELEMENTS::ISettingComponent* settingComponent= 
        dynamic_cast<ELEMENTS::ISettingComponent*>(
        ELEMENTS::GetComponentFromMaintainer("SettingComponent"));
    if(settingComponent != NULL)
    {
        if(!qtApplication->useQt())
        {
            settingComponent->setAppMode(ELEMENTS::ISettingComponent::APP_MODE_OSG);
        }
    }

    {
        // Create WorldManager
        CORE::RefPtr<APP::IManager> mgr = application->getManagerByClassname(STR(APP::WorldManager));
        CORE::RefPtr<APP::WorldManager> wmgr = dynamic_cast<APP::WorldManager*>(mgr.get());
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = wmgr->getWorldMaintainer();

        typedef LOADER::IWorldReaderWriter* (*CreateWorldLoaderPtr)();
        typedef void (*DestroyWorldLoaderPtr)(LOADER::IWorldReaderWriter*);

        CreateWorldLoaderPtr createWLoader     = (CreateWorldLoaderPtr)(dl->getProcAddress("CreateWorldLoader"));
        DestroyWorldLoaderPtr destroyWLoader   = (DestroyWorldLoaderPtr)(dl->getProcAddress("DestroyWorldLoader"));

        CORE::RefPtr<CORE::IObject> worldObject = DB::readObjectFile(defaultproject);
        wmain->setProjectFile(defaultproject);
        if(!worldObject.valid())
        {
            std::cerr << "Unable to load project file. Check whether the file exists and is a valid XML file." << std::endl;
            return 1;
        }

        CORE::RefPtr<CORE::IWorld> iworld = worldObject->getInterface<CORE::IWorld>();

        if(!iworld.valid())
        {
            std::cerr << "Unable to load project file. Check whether the file exists and is a valid XML file." << std::endl;
            return 1;
        }


        wmain->clearWorlds();
        wmain->addWorld(iworld);

        osg::ref_ptr<osgViewer::View> view = application->getCompositeViewer()->getView(0);

        // XXX - Should be done somewhere else
        ELEMENTS::IIntersectionComponent* intersectComp = 
            dynamic_cast<ELEMENTS::IIntersectionComponent*>(
            ELEMENTS::GetComponentFromMaintainer("IntersectionComponent"));

        if(intersectComp)
        {
            intersectComp->setView(view.get());
        }

        //osg::ref_ptr<osgViewer::View> viewer = dynamic_cast<osgViewer::View*>(view.get());
        view->setSceneData(wmain->getOSGNode());
        view->getCamera()->setClearColor(osg::Vec4(0.0, 0.0, 0.0, 1.0));

        std::string val = UTIL::getEnvironmentVariable("OSG_NEAR_FAR_RATIO");

        double nearFarRatio = atof(val.c_str());

        // changed Near Far plane 
        
        view->getCamera()->setNearFarRatio(nearFarRatio);

        std::string tourToPlayOnStart;
        if(arguments.read("--tourName", tourToPlayOnStart))
        {
            std::cout << tourToPlayOnStart;
            //! Set tour name to play on load
            SMCElements::ITourComponent* tourComponent= 
                dynamic_cast<SMCElements::ITourComponent*>(
                wmain->getComponentByName("TourComponent"));
            if(tourComponent)
            {
                tourComponent->setTourToPlayOnStart(tourToPlayOnStart);
            }

        }

        //! Stereo Mode
        std::string stereoMode;
        osg::DisplaySettings* ds = osg::DisplaySettings::instance();
        if(arguments.read("--stereoMode", stereoMode))
        {
            unsigned int mode = UTIL::ToType<unsigned int>(stereoMode);
            ds->setStereo(true);
            ds->setStereoMode(osg::DisplaySettings::StereoMode(mode));
        }

        //! Eye seperation
        std::string eyeSeperation;
        if(arguments.read("--eyeSeperation", eyeSeperation))
        {
            ds->setEyeSeparation(UTIL::ToType<double>(eyeSeperation));
        }

        //! Screen Distance
        std::string screenDistance;
        if(arguments.read("--screenDistance", screenDistance))
        {
            ds->setScreenDistance(UTIL::ToType<double>(screenDistance));
        }

        //! Screen Height
        std::string screenHeight;
        if(arguments.read("--screenHeight", screenHeight))
        {
            ds->setScreenHeight(UTIL::ToType<double>(screenHeight));
        }

        //! Screen Width
        std::string screenWidth;
        if(arguments.read("--screenWidth", screenWidth))
        {
            ds->setScreenWidth(UTIL::ToType<double>(screenWidth));
        }

        CORE::RefPtr<DB::ReaderWriter::Options> options = 
            new DB::ReaderWriter::Options();
        options->setMapValue("Offset", UTIL::ToString(50));
        options->setMapValue("Clamping", UTIL::ToString(true));
        options->setMapValue("Editable", UTIL::ToString(false));

        //CORE::RefPtr<CORE::IObject> worldshp = DB::readFeature("E:/Release/shp/doda.shp", options.get());
        //iworld->addObject(worldshp.get());

        /*osg::CullStack::CullingMode cullingMode = view->getCamera()->getCullingMode();*/
        /*cullingMode &= osg::CullStack::SMALL_FEATURE_CULLING;*/
        /*cullingMode &= osg::CullStack::VIEW_FRUSTUM_CULLING;*/
        /*view->getCamera()->setCullingMode( cullingMode );*/
        //view->getCamera()->setSmallFeatureCullingPixelSize(-1.0f);


        //viewer->getCamera()->setComputeNearFarMode(osg::CullSettings::COMPUTE_NEAR_FAR_USING_PRIMITIVES);
        //osgGA::StateSetManipulator* st = new osgGA::StateSetManipulator(viewer->getCamera()->getOrCreateStateSet());
        //st->setKeyEventCyclePolygonMode(osgGA::GUIEventAdapter::KEY_Shift_L);
        //viewer->addEventHandler(st);
    }

#ifdef TEST_SIMULATION_COMPONENT
    // Create WorldManager
    CORE::RefPtr<APP::IManager> mgr = application->getManagerByClassname(STR(APP::WorldManager));
    CORE::RefPtr<APP::WorldManager> wmgr = dynamic_cast<APP::WorldManager*>(mgr.get());
    CORE::RefPtr<CORE::IWorldMaintainer> wmain = wmgr->getWorldMaintainer();

    CORE::RefPtr<CORE::IComponent> icomponent = wmain->getComponentByName("SimulationComponent");

    CORE::RefPtr<SIM::ISimulationComponent> simComponent = icomponent->getInterface<SIM::ISimulationComponent>();

    simComponent->startThread();
#endif //TEST_SIMULATION_COMPONENT
   
    CORE::RefPtr<VizQt::IQtGUIManager> qtapp = application->getManagerByName("QtGUIManager")->getInterface<VizQt::IQtGUIManager>();
    QWidget* mainWindow = qtapp->getLayoutWidget();
    if (mainWindow)
    {
        mainWindow->show();
        splashscreen->finish(mainWindow);
    }

    application->start();

    return 0;
}


void setupInstallerMode() 
{
    //finding the working directory
    char buffer[MAX_PATH];
    GetCurrentDirectory(MAX_PATH, buffer);
    std::string pwd(buffer);

    //setting required path that are relative to PWD of installer's installer directory

    std::string georbisBinDirectory = pwd;
	
	UTIL::setEnvironmentVariable("DSM_DIR", georbisBinDirectory);
    georbisBinDirectory = UTIL::getEnvironmentVariable("DSM_DIR");
	
    std::string gdal_data = georbisBinDirectory + "..\\..\\..\\data\\gdal\\";
    UTIL::setEnvironmentVariable("GDAL_DATA", gdal_data);
    gdal_data = UTIL::getEnvironmentVariable("GDAL_DATA");
	
    std::string portico_dir = georbisBinDirectory + "\\portico\\";
    UTIL::setEnvironmentVariable("PORTICO_DIR", portico_dir);
    portico_dir = UTIL::getEnvironmentVariable("PORTICO_DIR");

    std::string rti_rid = georbisBinDirectory + "..\\..\\..\\data\\HLA\\RTI.rid\\";
    UTIL::setEnvironmentVariable("RTI_RID_FILE", rti_rid);
    rti_rid = UTIL::getEnvironmentVariable("RTI_RID_FILE");

    UTIL::setEnvironmentVariable("RTI_HOME", portico_dir);
    std::string rti_home = UTIL::getEnvironmentVariable("RTI_HOME");

    std::string java_home = portico_dir + "\\jre\\";
    UTIL::setEnvironmentVariable("JAVA_HOME", java_home);
    java_home = UTIL::getEnvironmentVariable("JAVA_HOME");

    std::string rti_java_bin = portico_dir + "\\jre\\bin\\server\\";
    UTIL::setEnvironmentVariable("RTI_JAVA_BIN", rti_java_bin);
    rti_java_bin = UTIL::getEnvironmentVariable("RTI_JAVA_BIN");

    std::string rti_jre_bin = portico_dir + "\\jre\\bin\\";
    UTIL::setEnvironmentVariable("RTI_JRE_BIN", rti_jre_bin);
    rti_jre_bin = UTIL::getEnvironmentVariable("RTI_JRE_BIN");

    std::string rti_bin = portico_dir + "\\bin\\";
    UTIL::setEnvironmentVariable("RTI_BIN", rti_bin);
    rti_bin = UTIL::getEnvironmentVariable("RTI_BIN");

    std::string path = UTIL::getEnvironmentVariable("PATH");
    path = rti_home + ";" + rti_java_bin + ";" + rti_bin + ";" + rti_jre_bin + ";" + path;

    UTIL::setEnvironmentVariable("PATH", path);
	UTIL::setEnvironmentVariable("TERRAINSDK_DATA_DIR", georbisBinDirectory);

    // setting osg and tkp related parameters present in config/ApplicationWideSettings.json

    std::string configFolder = georbisBinDirectory + "..\\..\\..\\config\\";

    std::string configPath = osgDB::convertFileNameToNativeStyle(configFolder);
    if (!osgDB::fileExists(configPath))
    {
        //config folder not present
        return;
    }

   
    else
    {
		std::string settingJson = configPath + "ApplicationWideSettings.json";
        QFile settingFile(QString::fromStdString(settingJson));

        if (!settingFile.open(QIODevice::ReadOnly))
        {
            // settingsJson not found in config folder
            return;
        }

        QJsonDocument jdoc = QJsonDocument::fromJson(settingFile.readAll());
        if (jdoc.isNull())
        {
            return;
        }

        QJsonObject obj = jdoc.object();
        QStringList keyList = obj.keys();
        
        for (int i = 0; i < keyList.size(); i++){
            QString key = keyList[i];

            std::string value = obj[key].toString().toStdString();
            UTIL::setEnvironmentVariable(key.toStdString(), value);
        }

        //std::string osgNearFarMode = obj["OSG_COMPUTE_NEAR_FAR_MODE"].toString().toStdString();
        //UTIL::setEnvironmentVariable("OSG_COMPUTE_NEAR_FAR_MODE", osgNearFarMode);

        //std::string osgNotifyLevel = obj["OSG_NOTIFY_LEVEL"].toString().toStdString();
        //UTIL::setEnvironmentVariable("OSG_NOTIFY_LEVEL", osgNotifyLevel);

        //std::string osgEarthNotifyLevel = obj["OSGEARTH_NOTIFY_LEVEL"].toString().toStdString();
        //UTIL::setEnvironmentVariable("OSGEARTH_NOTIFY_LEVEL", osgEarthNotifyLevel);

        //std::string osgNumDatabaseThreads = obj["OSG_NUM_DATABASE_THREADS"].toString().toStdString();
        //UTIL::setEnvironmentVariable("OSG_NUM_DATABASE_THREADS", osgNumDatabaseThreads);

        //std::string osgNumHTTPDatabaseThreads = obj["OSG_NUM_HTTP_DATABASE_THREADS"].toString().toStdString();
        //UTIL::setEnvironmentVariable("OSG_NUM_HTTP_DATABASE_THREADS", osgNumHTTPDatabaseThreads);

        //std::string tkpTileRangeFactor = obj["TKP_SDK_TILE_RANGE_FACTOR"].toString().toStdString();
        //UTIL::setEnvironmentVariable("TKP_SDK_TILE_RANGE_FACTOR", tkpTileRangeFactor);

        //std::string tkpLogLevel = obj["TERRAINSDK_LOG_LEVEL"].toString().toStdString();
        //UTIL::setEnvironmentVariable("TERRAINSDK_LOG_LEVEL", tkpLogLevel);

        //nearFarRatio= obj["NEAR_FAR_RATIO"].toString().toDouble();
    }
}

void setupLogging()
{
    /// Log file Settings
    std::time_t rawtime;
    std::tm* timeinfo;
    char timeBuffer[80];
    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);
    std::strftime(timeBuffer,80,"%Y_%m_%d_%H_%M_%S",timeinfo);
    std::string currentTime(timeBuffer);

    UTIL::LogFile::SetTitle("GeorbIS Logs");
    std::string logFolderPath = UTIL::TemporaryFolder::instance()->getAppFolderPath() + std::string("/Logs");
    std::string logFileName = logFolderPath + std::string("/GeorbIS_") + currentTime + ".html";

    //! Create Logs directory if not present already
    osgDB::makeDirectoryForFile(logFileName);

    //! Remove logs prior to last 7 days,.
    QDir appDataDir(QString::fromStdString(logFolderPath));
    QStringList htmlFilters;
    //! Filter out all the html logs created by this application
    htmlFilters << "GeorbIS_*.html";
    appDataDir.setNameFilters(htmlFilters);
    //! Sort them in reveresed order of modified time(earliest first)
    QFileInfoList fileInfoList = appDataDir.entryInfoList(QDir::NoFilter, (QDir::Time | QDir::Reversed));
    QDate aWeekAgo = QDate::currentDate().addDays(-7);
    foreach(QFileInfo fileinfo, fileInfoList)
    {
        if(fileinfo.lastModified().date() < aWeekAgo)
        {
            //! Remove the file if it is later than a week ago
            appDataDir.remove(fileinfo.fileName());
        }
        else
        {
            //! If found the first occurance of a later(than a week ago) file, break the loop.
            break;
        }
    }

    /// \todo   Fix in the base log class, next line should not be required
    UTIL::Log::GetInstance();
    UTIL::LogFile::SetFileName(logFileName);
    LOG_INFO("GeorbIS Started");
#ifdef _DEBUG
    UTIL::Log::GetInstance().SetLogLevel(UTIL::Log::LOG_DEBUG);
    UTIL::Log::GetInstance().SetOutputStreamBit(UTIL::Log::STANDARD);
#endif
}

#ifndef _DEBUG
// function to set up the console in winmain app.
void SetupConsole()
{
#ifdef WIN32
    //alloc console to allocate console on windows
    if(AllocConsole())
    {
        //attach std output redirection to console
        HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
        int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
        hf_out = _fdopen(hCrt, "w");
        setvbuf(hf_out, NULL, _IONBF, 128);

        // redirecting the buffers to the file handle
        *stdout = *hf_out;
        *stderr = *hf_out;
    }
#endif //WIN32
}

// function for redirecting qml output to the winMain custom created console.

void myMessageOutput(QtMsgType type, const QMessageLogContext & context, const QString &msg)
{
#ifdef WIN32
    //in this function, you can write the message to any stream!
    fprintf( hf_out, "%s\n\n", msg.toStdString().c_str());
#endif //WIn32
}
#endif
