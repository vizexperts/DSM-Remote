#include <Stereo/StereoSceneGenerator.h>
#include <Stereo/ImageReaderWriter.h>
#include <Stereo/TileKey.h>
#include <iostream>
#include <osg/MatrixTransform>
#include <osg/ShapeDrawable>
//#include <gdal_priv.h>

#define TILEWIDTH 256
#define TILEHEIGHT 256
#define MAXLOD 10u
#define MIN_TILE_RANGE_FACTOR 6

namespace Stereo
{
    osgDB::RegisterReaderWriterProxy<ImageReaderWriter> g_ImageReaderWriter;

    StereoSceneGenerator::StereoSceneGenerator():
    _rotationAngle(0.0),
    _translationX(0.0),
    _translationY(0.0)
    {


        osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface(); 
        if (!wsi) 
        { 
            osg::notify(osg::NOTICE)<<"Error, no WindowSystemInterface available, cannot create windows."<<std::endl; 
            return; 
        } 
        wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0),_screenWidth, _screenHeight); 

    }

    StereoSceneGenerator::~StereoSceneGenerator()
    {

    }


    osg::Node * 
    StereoSceneGenerator::loadImage(std::string fileUrl)
    {

        _leftImage = new Stereo::Image();

        if(!_leftImage->loadImage(fileUrl))
        {
            std::cout<<"Failed to Load left Image"<<std::endl;
            return NULL;
        }

        _leftImage->isRight(false);


        osgDB::ReaderWriter* readerWriter = osgDB::Registry::instance()->getReaderWriterForExtension("StereoViz_Image");
        if (!readerWriter)
        {
            std::cout<<"Error: StereoViz_Image plugin not available, cannot preceed with database creation"<<std::endl;
        }

        ImageReaderWriter* imageRW = dynamic_cast<ImageReaderWriter*>(readerWriter);

        if(imageRW)
        {
            imageRW->setSceneGenerator(this);
        }

        osg::Group* leftGroup = createRootNode(_leftImage);
        leftGroup->setNodeMask(0x01);


        osg::Group * imageGroup = new osg::Group;

        imageGroup->addChild(leftGroup);
        return imageGroup;
    }

    osg::Node * 
        StereoSceneGenerator::loadImage(std::string fileUrl,Stereo::Image::EnhancementType enhancementType)
    {

        _leftImage = new Stereo::Image();
        _leftImage->setEnhancementType(enhancementType);

        if(!_leftImage->loadImage(fileUrl))
        {
            std::cout<<"Failed to Load left Image"<<std::endl;
            return NULL;
        }

        _leftImage->isRight(false);


        osgDB::ReaderWriter* readerWriter = osgDB::Registry::instance()->getReaderWriterForExtension("StereoViz_Image");
        if (!readerWriter)
        {
            std::cout<<"Error: StereoViz_Image plugin not available, cannot preceed with database creation"<<std::endl;
        }

        ImageReaderWriter* imageRW = dynamic_cast<ImageReaderWriter*>(readerWriter);

        if(imageRW)
        {
            imageRW->setSceneGenerator(this);
        }

        osg::Group* leftGroup = createRootNode(_leftImage);
        //leftGroup->setNodeMask(0x01);


        osg::Group * imageGroup = new osg::Group;

        imageGroup->addChild(leftGroup);
        return imageGroup;
    }

    osg::Group* 
    StereoSceneGenerator::loadStereoImages(std::string leftImageUrl,std::string rightImageUrl, double rotationAngle, double translationX, double translationY)
    {
        _leftImage = new Stereo::Image();
        _rightImage = new Stereo::Image();

        if(!_leftImage->loadImage(leftImageUrl))
        {
            std::cout<<"Failed to Load left Image"<<std::endl;
            return NULL;
        }

        if(!_rightImage->loadImage(rightImageUrl))
        {
            std::cout<<"Failed to Load Right Image"<<std::endl;
            return NULL;
        }

        _rotationAngle = rotationAngle;
        _translationX = translationX;
        _translationY = translationY;

        _leftImage->isRight(false);
        _rightImage->isRight(true);

        osgDB::ReaderWriter* readerWriter = osgDB::Registry::instance()->getReaderWriterForExtension("StereoViz_Image");
        if (!readerWriter)
        {
            std::cout<<"Error: StereoViz_Image plugin not available, cannot preceed with database creation"<<std::endl;
        }

        ImageReaderWriter* imageRW = dynamic_cast<ImageReaderWriter*>(readerWriter);

        if(imageRW)
        {
            imageRW->setSceneGenerator(this);
        }
        

        double epiPolarRotationInRadians = (M_PI/180.0) * rotationAngle;
        osg::MatrixTransform* leftEpiPolarTransf = new osg::MatrixTransform(osg::Matrix::rotate(epiPolarRotationInRadians,0.0,1.0,0.0));
        leftEpiPolarTransf->addChild(createRootNode(_leftImage));
        leftEpiPolarTransf->setNodeMask(0x01);

        osg::MatrixTransform* rightEpiPolarTransf = new osg::MatrixTransform(osg::Matrix::rotate(epiPolarRotationInRadians,0.0,1.0,0.0));
        rightEpiPolarTransf->addChild(createRootNode(_rightImage));
        
        
        osg::MatrixTransform* transf = new osg::MatrixTransform(osg::Matrix::translate(translationX,0.0,translationY));
        transf->addChild(rightEpiPolarTransf);
        transf->setNodeMask(0x02);

        osg::Group * imageGroup = new osg::Group;        
        imageGroup->addChild(leftEpiPolarTransf);
        imageGroup->addChild(transf);

        return imageGroup;

    }

    osg::Group* 
    StereoSceneGenerator::createRootNode(Image* sImage)
    {
        TileKey key(0,0,0);

        double quadMinX,quadMinY,quadMaxX,quadMaxY;


        sImage->getExtentsForTile(key,quadMinX,quadMinY,quadMaxX,quadMaxY);

        osg::Image* image = _createImageForQuad(sImage,quadMinX,quadMinY,quadMaxX,quadMaxY);
        osg::Node* geode = _createSectorForOsgImage(image,quadMinX,quadMinY,quadMaxX,quadMaxY);
        osg::Group* rootNode = new osg::Group;
        addTile(sImage,key,geode,rootNode);

        return rootNode;
    }

    void 
    StereoSceneGenerator::addTile(Image* sImage, const TileKey& key, osg::Node* geode, osg::Group* parent)
    {
        char url[200];
        sprintf(url,"%d/%d/%d/%d.StereoViz_Image",key.getLOD(),key.getTileX(),key.getTileY(),sImage->isRight());

        osg::Node* result = 0L;

        if(key.getLOD() < MAXLOD)
        {
            osg::BoundingSphere bs = geode->getBound();

            float maxRange = FLT_MAX;

            double quadMinX,quadMinY,quadMaxX,quadMaxY;

            sImage->getExtentsForTile(key,quadMinX,quadMinY,quadMaxX,quadMaxY);
            osg::Vec3d ll(quadMinX,quadMinY,0.0), ur(quadMaxX,quadMaxY,0.0);
            double radius = (ur - ll).length() / 2.0;
            float minRange = (float)(radius * MIN_TILE_RANGE_FACTOR);

            osg::PagedLOD* plod = new osg::PagedLOD;

            plod->setCenter( bs.center() );
            plod->addChild( geode );
            plod->setRangeMode( osg::LOD::DISTANCE_FROM_EYE_POINT );
            plod->setFileName( 1, url );

            plod->setRange( 0, minRange, maxRange );                
            plod->setRange( 1, 0, minRange ); 

             result = plod;
        }
        else
        {
            result = geode;
        }

        parent->addChild( result );

    }

    osg::Node* 
    StereoSceneGenerator::createNode(const TileKey& parentKey, bool isRight)
    {
        osg::Node* geodes[4];

        Image* sImage = NULL;

        if(isRight)
        {
            sImage = _rightImage;
        }
        else
        {
            sImage = _leftImage;
        }

        for( unsigned i = 0; i < 4; ++i )
        {
            TileKey child = parentKey.createChildKey( i );
            double quadMinX,quadMinY,quadMaxX,quadMaxY;
            sImage->getExtentsForTile(child,quadMinX,quadMinY,quadMaxX,quadMaxY);
            osg::Image* image = _createImageForQuad(sImage,quadMinX,quadMinY,quadMaxX,quadMaxY);
            osg::Node* geode = _createSectorForOsgImage(image,quadMinX,quadMinY,quadMaxX,quadMaxY);
            geodes[i] = geode;
        }

        osg::Group* root = new osg::Group;

        for( unsigned i = 0; i < 4; ++i )
        {
            TileKey child = parentKey.createChildKey( i );
            addTile( sImage,child,geodes[i],root);
        }

        return root;
    }

    void 
    StereoSceneGenerator::setRotationAngle(double rotationAngle)
    {
        _rotationAngle = rotationAngle;
    }

    double 
    StereoSceneGenerator::getRotationAngle()
    {
        return _rotationAngle;
    }

    void 
    StereoSceneGenerator::setTranslationX(double translationX)
    {
        _translationX = translationX;
    }

    double 
    StereoSceneGenerator::getTranslationX()
    {
        return _translationX;
    }

    void 
    StereoSceneGenerator::setTranslationY(double translationY)
    {
        _translationY = translationY;
    }

    double 
    StereoSceneGenerator::getTranslationY()
    {
        return _translationY;
    }

    void 
    StereoSceneGenerator::setLeftImage(Stereo::Image* leftImage)
    {
        _leftImage = leftImage;

    }

    Stereo::Image* 
    StereoSceneGenerator::getLeftImage()
    {
        return _leftImage;
    }

    void 
    StereoSceneGenerator::setRightImage(Stereo::Image* rightImage)
    {
        _rightImage =  rightImage;
    }

    Stereo::Image* 
    StereoSceneGenerator::getRightImage()
    {
        return _rightImage;
    }

    osg::Group* 
    StereoSceneGenerator::_createQuadsAtLevel(Image* sImage, int level)
    {
        
        double minX,maxX,minY,maxY;
        sImage->getBounds(minX,minY,maxX,maxY);

         int numOFQuadsInX = pow(2.0f,float(level));
        int numOFQuadsInY = pow(2.0f,float(level));
        int numOFQuadsInXFittingOnScreen = numOFQuadsInX;
        int numOFQuadsInYFittingOnScreen = numOFQuadsInY;

        int maxXQuadsOnDisplay = ceil(float(_screenWidth) / float(TILEWIDTH));
        int maxYQuadsOnDisplay = ceil(float(_screenHeight) / float(TILEHEIGHT));

        

        if(numOFQuadsInX > maxXQuadsOnDisplay)
        {
            numOFQuadsInXFittingOnScreen = maxXQuadsOnDisplay+2;
        }

        if(numOFQuadsInY > maxYQuadsOnDisplay)
        {
            numOFQuadsInYFittingOnScreen = maxYQuadsOnDisplay+2;
        }

        double quadWidth = (maxX - minX)/double(numOFQuadsInX);
        double quadHeight = (maxY - minY)/double(numOFQuadsInY);

        double quadMinX,quadMaxX;
        double quadMinY,quadMaxY;

        osg::ref_ptr<osg::Group> quadGroup = new osg::Group();

        for(int j = numOFQuadsInY/2-numOFQuadsInYFittingOnScreen/2, yCount = 0; yCount < numOFQuadsInYFittingOnScreen; j++, yCount++)
        {
            osg::ref_ptr<osg::MatrixTransform> transformation = new osg::MatrixTransform;
            for(int i = numOFQuadsInX/2 - numOFQuadsInXFittingOnScreen/2, xCount = 0; xCount < numOFQuadsInXFittingOnScreen; i++, xCount++)
            {
                quadMinX = minX + double(i) * quadWidth;
                quadMaxX = minX + double(i+1) * quadWidth;
                quadMinY = minY + double(j) * quadHeight;
                quadMaxY = minY + double(j+1) * quadHeight;

                

                osg::ref_ptr<osg::Image> image = _createImageForQuad(sImage,quadMinX,quadMinY,quadMaxX,quadMaxY);
                /*osg::ref_ptr<osg::Geode>*/ osg::Node* geode = _createSectorForOsgImage(image,quadMinX,quadMinY,quadMaxX,quadMaxY);
                transformation->addChild(geode);                
            }
            quadGroup->addChild(transformation);
        }

        return quadGroup.release();

    }

    osg::Image* 
    StereoSceneGenerator::_createImageForQuad(Image* sImage, double minX, double minY, double maxX, double maxY)
    {

        GDALDataset* _srcDS = sImage->getGdalDataset();

        if (!_srcDS)
        {
            std::cout << "Failed to load gdal dataset" << std::endl;
            return NULL;
        }

        double* geotransform;

        geotransform = sImage->getGeoExtents();

        int target_width = TILEWIDTH;
        int target_height = TILEHEIGHT;
        int tile_offset_left = 0;
        int tile_offset_top = 0;

        int off_x = int((minX - geotransform[0]) / geotransform[1]);
        int off_y = int((maxY - geotransform[3]) / geotransform[5]);
        int width = int(((maxX - geotransform[0]) / geotransform[1]) - off_x);
        int height = int(((minY - geotransform[3]) / geotransform[5]) - off_y);

        if (off_x + width > _srcDS->GetRasterXSize())
        {
            int oversize_right = off_x + width - _srcDS->GetRasterXSize();
            target_width = target_width - int(float(oversize_right) / width * target_width);
            width = _srcDS->GetRasterXSize() - off_x;
        }

        if (off_x < 0)
        {
            int oversize_left = -off_x;
            tile_offset_left = int(float(oversize_left) / width * target_width);
            target_width = target_width - int(float(oversize_left) / width * target_width);
            width = width + off_x;
            off_x = 0;
        }

        if (off_y + height > _srcDS->GetRasterYSize())
        {
            int oversize_bottom = off_y + height - _srcDS->GetRasterYSize();
            target_height = target_height - (int)osg::round(float(oversize_bottom) / height * target_height);
            height = _srcDS->GetRasterYSize() - off_y;
        }


        if (off_y < 0)
        {
            int oversize_top = -off_y;
            tile_offset_top = int(float(oversize_top) / height * target_height);
            target_height = target_height - int(float(oversize_top) / height * target_height);
            height = height + off_y;
            off_y = 0;
        }

        //Return if parameters are out of range.
        if (width <= 0 || height <= 0 || target_width <= 0 || target_height <= 0)
        {
            return 0;
        }


        osg::ref_ptr<osg::Image> image;

        GDALRasterBand* bandRed = NULL;
        GDALRasterBand* bandGreen = NULL;
        GDALRasterBand* bandBlue = NULL;
        GDALRasterBand* bandAlpha = NULL;
        GDALRasterBand* bandGray = NULL;

        if (_srcDS->GetRasterCount() == 3)
        {
            bandRed   = _srcDS->GetRasterBand( 1 );
            bandGreen = _srcDS->GetRasterBand( 2 );
            bandBlue  = _srcDS->GetRasterBand( 3 );
        }
        //RGBA = 4 bands
        else if (_srcDS->GetRasterCount() == 4)
        {
            bandRed   = _srcDS->GetRasterBand( 1 );
            bandGreen = _srcDS->GetRasterBand( 2 );
            bandBlue  = _srcDS->GetRasterBand( 3 );
            bandAlpha = _srcDS->GetRasterBand( 4 );
        }
        //Gray = 1 band
        else if (_srcDS->GetRasterCount() == 1)
        {
            bandGray = _srcDS->GetRasterBand( 1 );
        }
        //Gray + alpha = 2 bands
        else if (_srcDS->GetRasterCount() == 2)
        {
            bandGray  = _srcDS->GetRasterBand( 1 );
            bandAlpha = _srcDS->GetRasterBand( 2 );
        }

            //The pixel format is always RGBA to support transparency
        
#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
            GLenum pixelFormat = GL_RGBA;
            _enhancementType = sImage->getEnhancementType();

            GDALDataType _type = sImage->getRasterType();
            if (bandRed && bandGreen && bandBlue)
            {
                
                if(_type == GDT_Byte)
                {

                    unsigned char *red = new unsigned char[target_width * target_height];
                    unsigned char *green = new unsigned char[target_width * target_height];
                    unsigned char *blue = new unsigned char[target_width * target_height];
                    unsigned char *alpha = new unsigned char[target_width * target_height];

                    //Initialize the alpha values to 255.
                    memset(alpha, 255, target_width * target_height);

                    image = new osg::Image;
                    image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, GL_UNSIGNED_BYTE);
                    memset(image->data(), 0, image->getImageSizeInBytes());

                    //Nearest interpolation just uses RasterIO to sample the imagery and should be very fast.
                    //if (!*_options.interpolateImagery() || _options.interpolation() == INTERP_NEAREST)
                    {
                        bandRed->RasterIO(GF_Read, off_x, off_y, width, height, red, target_width, target_height, GDT_Byte, 0, 0);
                        bandGreen->RasterIO(GF_Read, off_x, off_y, width, height, green, target_width, target_height, GDT_Byte, 0, 0);
                        bandBlue->RasterIO(GF_Read, off_x, off_y, width, height, blue, target_width, target_height, GDT_Byte, 0, 0);

                        if (bandAlpha)
                        {
                            bandAlpha->RasterIO(GF_Read, off_x, off_y, width, height, alpha, target_width, target_height, GDT_Byte, 0, 0);
                        }

                        for (int src_row = 0, dst_row = tile_offset_top;
                            src_row < target_height;
                            src_row++, dst_row++)
                        {
                            for (int src_col = 0, dst_col = tile_offset_left;
                                src_col < target_width;
                                ++src_col, ++dst_col)
                            {
                                double r16 = red[src_col + src_row * target_width];
                                double g16 = green[src_col + src_row * target_width];
                                double b16 = blue[src_col + src_row * target_width];

                                r16 = sImage->enhanceRedBandPixel(r16,_enhancementType);
                                g16 = sImage->enhanceGreenBandPixel(g16,_enhancementType);
                                b16 = sImage->enhanceBlueBandPixel(b16,_enhancementType);

                                if(r16 < 0)
                                {
                                    r16 = 0;
                                }

                                if(g16 < 0)
                                {
                                    g16 = 0;
                                }

                                if(b16 < 0)
                                {
                                    b16 = 0;
                                }

                                if(r16 > 255)
                                {
                                    r16 = 255;
                                }

                                if(g16 > 255)
                                {
                                    g16 = 255;
                                }

                                if(b16 >255)
                                {
                                    b16 = 255;
                                }
                                unsigned char r = (unsigned char)(r16);
                                unsigned char g = (unsigned char)(g16);
                                unsigned char b = (unsigned char)(b16);
                                unsigned char a = alpha[src_col + src_row * target_width];
                                *(image->data(dst_col, dst_row) + 0) = r;
                                *(image->data(dst_col, dst_row) + 1) = g;
                                *(image->data(dst_col, dst_row) + 2) = b;                            
                                if (!isValidValue( r, bandRed)    ||
                                    !isValidValue( g, bandGreen)  || 
                                    !isValidValue( b, bandBlue)   ||
                                    (bandAlpha && !isValidValue( a, bandAlpha )))
                                {
                                    a = 0.0f;
                                }                            
                                *(image->data(dst_col, dst_row) + 3) = a;
                            }
                        }

                        image->flipVertical();
                    }
                    //else
                    //{
                    //    //Sample each point exactly
                    //    for (unsigned int c = 0; c < (unsigned int)tileSize; ++c)
                    //    {
                    //        double geoX = xmin + (dx * (double)c); 
                    //        for (unsigned int r = 0; r < (unsigned int)tileSize; ++r)
                    //        {
                    //            double geoY = ymin + (dy * (double)r); 
                    //            *(image->data(c,r) + 0) = (unsigned char)getInterpolatedValue(bandRed,  geoX,geoY,false); 
                    //            *(image->data(c,r) + 1) = (unsigned char)getInterpolatedValue(bandGreen,geoX,geoY,false); 
                    //            *(image->data(c,r) + 2) = (unsigned char)getInterpolatedValue(bandBlue, geoX,geoY,false); 
                    //            if (bandAlpha != NULL) 
                    //                *(image->data(c,r) + 3) = (unsigned char)getInterpolatedValue(bandAlpha,geoX, geoY, false); 
                    //            else 
                    //                *(image->data(c,r) + 3) = 255; 
                    //        }
                    //    }
                    //}

                    delete []red;
                    delete []green;
                    delete []blue;
                    delete []alpha;
                }
                else if (_type == GDT_UInt32)
                {
                    unsigned char *red = new unsigned char[target_width * target_height];
                    unsigned char *green = new unsigned char[target_width * target_height];
                    unsigned char *blue = new unsigned char[target_width * target_height];
                    unsigned char *alpha = new unsigned char[target_width * target_height];

                    unsigned int *red16 = new unsigned int[target_width * target_height];
                    unsigned int *green16 = new unsigned int[target_width * target_height];
                    unsigned int *blue16 = new unsigned int[target_width * target_height];

                    //Initialize the alpha values to 255.
                    memset(alpha, 255, target_width * target_height);

                    image = new osg::Image;
                    image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, GL_UNSIGNED_BYTE);
                    memset(image->data(), 0, image->getImageSizeInBytes());

                    double _scaleByForRed = sImage->getRedScaleValue();
                    double _scaleByForGreen = sImage->getGreenScaleValue();
                    double _scaleByForBlue = sImage->getBlueScaleValue();


                    //Nearest interpolation just uses RasterIO to sample the imagery and should be very fast.
                    //if (!*_options.interpolateImagery() || _options.interpolation() == INTERP_NEAREST)
                    {
                        bandRed->RasterIO(GF_Read, off_x, off_y, width, height, red16, target_width, target_height, _type, 0, 0);
                        bandGreen->RasterIO(GF_Read, off_x, off_y, width, height, green16, target_width, target_height, _type, 0, 0);
                        bandBlue->RasterIO(GF_Read, off_x, off_y, width, height, blue16, target_width, target_height, _type, 0, 0);

                        if (bandAlpha)
                        {
                            bandAlpha->RasterIO(GF_Read, off_x, off_y, width, height, alpha, target_width, target_height, GDT_Byte, 0, 0);
                        }

                        for (int src_row = 0, dst_row = tile_offset_top;
                            src_row < target_height;
                            src_row++, dst_row++)
                        {
                            for (int src_col = 0, dst_col = tile_offset_left;
                                src_col < target_width;
                                ++src_col, ++dst_col)
                            {
                                double r16 = red16[src_col + src_row * target_width];
                                double g16 = green16[src_col + src_row * target_width];
                                double b16 = blue16[src_col + src_row * target_width];

                                r16 = sImage->enhanceRedBandPixel(r16,_enhancementType);
                                g16 = sImage->enhanceGreenBandPixel(g16,_enhancementType);
                                b16 = sImage->enhanceBlueBandPixel(b16,_enhancementType);

                                if(r16 < 0)
                                {
                                    r16 = 0;
                                }

                                if(g16 < 0)
                                {
                                    g16 = 0;
                                }

                                if(b16 < 0)
                                {
                                    b16 = 0;
                                }

                                if(r16 > 255)
                                {
                                    r16 = 255;
                                }

                                if(g16 > 255)
                                {
                                    g16 = 255;
                                }

                                if(b16 >255)
                                {
                                    b16 = 255;
                                }

                                unsigned char r = (unsigned char)(r16);
                                unsigned char g = (unsigned char)(g16);
                                unsigned char b = (unsigned char)(b16);
                                unsigned char a = alpha[src_col + src_row * target_width];

                                *(image->data(dst_col, dst_row) + 0) = r;
                                *(image->data(dst_col, dst_row) + 1) = g;
                                *(image->data(dst_col, dst_row) + 2) = b;                            
                                if (!isValidValue( r, bandRed)    ||
                                    !isValidValue( g, bandGreen)  || 
                                    !isValidValue( b, bandBlue)   ||
                                    (bandAlpha && !isValidValue( a, bandAlpha )))
                                {
                                    a = 0.0f;
                                }                            
                                *(image->data(dst_col, dst_row) + 3) = a;
                            }
                        }

                        image->flipVertical();
                    }
                    //else
                    //{
                    //    //Sample each point exactly
                    //    for (unsigned int c = 0; c < (unsigned int)tileSize; ++c)
                    //    {
                    //        double geoX = xmin + (dx * (double)c); 
                    //        for (unsigned int r = 0; r < (unsigned int)tileSize; ++r)
                    //        {
                    //            double geoY = ymin + (dy * (double)r); 
                    //            *(image->data(c,r) + 0) = (unsigned char)getInterpolatedValue(bandRed,  geoX,geoY,false); 
                    //            *(image->data(c,r) + 1) = (unsigned char)getInterpolatedValue(bandGreen,geoX,geoY,false); 
                    //            *(image->data(c,r) + 2) = (unsigned char)getInterpolatedValue(bandBlue, geoX,geoY,false); 
                    //            if (bandAlpha != NULL) 
                    //                *(image->data(c,r) + 3) = (unsigned char)getInterpolatedValue(bandAlpha,geoX, geoY, false); 
                    //            else 
                    //                *(image->data(c,r) + 3) = 255; 
                    //        }
                    //    }
                    //}

                    delete []red;
                    delete []green;
                    delete []blue;
                    delete []alpha;

                    delete []red16;
                    delete []green16;
                    delete []blue16;
                }
                else if(_type == GDT_UInt16)
                {
                        unsigned char *red = new unsigned char[target_width * target_height];
                        unsigned char *green = new unsigned char[target_width * target_height];
                        unsigned char *blue = new unsigned char[target_width * target_height];
                        unsigned char *alpha = new unsigned char[target_width * target_height];

                        unsigned short *red16 = new unsigned short[target_width * target_height];
                        unsigned short *green16 = new unsigned short[target_width * target_height];
                        unsigned short *blue16 = new unsigned short[target_width * target_height];

                        //Initialize the alpha values to 255.
                        memset(alpha, 255, target_width * target_height);

                        image = new osg::Image;
                        image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, GL_UNSIGNED_BYTE);
                        memset(image->data(), 0, image->getImageSizeInBytes());

                        double _scaleByForRed = sImage->getRedScaleValue();
                        double _scaleByForGreen = sImage->getGreenScaleValue();
                        double _scaleByForBlue = sImage->getBlueScaleValue();

                        //Nearest interpolation just uses RasterIO to sample the imagery and should be very fast.
                        //if (!*_options.interpolateImagery() || _options.interpolation() == INTERP_NEAREST)
                        {
                            bandRed->RasterIO(GF_Read, off_x, off_y, width, height, red16, target_width, target_height, _type, 0, 0);
                            bandGreen->RasterIO(GF_Read, off_x, off_y, width, height, green16, target_width, target_height, _type, 0, 0);
                            bandBlue->RasterIO(GF_Read, off_x, off_y, width, height, blue16, target_width, target_height, _type, 0, 0);

                            if (bandAlpha)
                            {
                                bandAlpha->RasterIO(GF_Read, off_x, off_y, width, height, alpha, target_width, target_height, GDT_Byte, 0, 0);
                            }

                            for (int src_row = 0, dst_row = tile_offset_top;
                                src_row < target_height;
                                src_row++, dst_row++)
                            {
                                for (int src_col = 0, dst_col = tile_offset_left;
                                    src_col < target_width;
                                    ++src_col, ++dst_col)
                                {
                                    double r16 = red16[src_col + src_row * target_width];
                                    double g16 = green16[src_col + src_row * target_width];
                                    double b16 = blue16[src_col + src_row * target_width];

                                    r16 = sImage->enhanceRedBandPixel(r16,_enhancementType);
                                    g16 = sImage->enhanceGreenBandPixel(g16,_enhancementType);
                                    b16 = sImage->enhanceBlueBandPixel(b16,_enhancementType);

                                    if(r16 < 0)
                                    {
                                        r16 = 0;
                                    }

                                    if(g16 < 0)
                                    {
                                        g16 = 0;
                                    }

                                    if(b16 < 0)
                                    {
                                        b16 = 0;
                                    }

                                    if(r16 > 255)
                                    {
                                        r16 = 255;
                                    }

                                    if(g16 > 255)
                                    {
                                        g16 = 255;
                                    }

                                    if(b16 >255)
                                    {
                                        b16 = 255;
                                    }


                                    unsigned char r = (unsigned char)(osg::round(r16));
                                    unsigned char g = (unsigned char)(osg::round(g16));
                                    unsigned char b = (unsigned char)(osg::round(b16));
                                    unsigned char a = alpha[src_col + src_row * target_width];

                                    *(image->data(dst_col, dst_row) + 0) = r;
                                    *(image->data(dst_col, dst_row) + 1) = g;
                                    *(image->data(dst_col, dst_row) + 2) = b;                            
                                    if (!isValidValue( r, bandRed)    ||
                                        !isValidValue( g, bandGreen)  || 
                                        !isValidValue( b, bandBlue)   ||
                                        (bandAlpha && !isValidValue( a, bandAlpha )))
                                    {
                                        a = 0.0f;
                                    }                            
                                    *(image->data(dst_col, dst_row) + 3) = a;
                                }
                            }

                            image->flipVertical();
                        }
                        //else
                        //{
                        //    //Sample each point exactly
                        //    for (unsigned int c = 0; c < (unsigned int)tileSize; ++c)
                        //    {
                        //        double geoX = xmin + (dx * (double)c); 
                        //        for (unsigned int r = 0; r < (unsigned int)tileSize; ++r)
                        //        {
                        //            double geoY = ymin + (dy * (double)r); 
                        //            *(image->data(c,r) + 0) = (unsigned char)getInterpolatedValue(bandRed,  geoX,geoY,false); 
                        //            *(image->data(c,r) + 1) = (unsigned char)getInterpolatedValue(bandGreen,geoX,geoY,false); 
                        //            *(image->data(c,r) + 2) = (unsigned char)getInterpolatedValue(bandBlue, geoX,geoY,false); 
                        //            if (bandAlpha != NULL) 
                        //                *(image->data(c,r) + 3) = (unsigned char)getInterpolatedValue(bandAlpha,geoX, geoY, false); 
                        //            else 
                        //                *(image->data(c,r) + 3) = 255; 
                        //        }
                        //    }
                        //}

                        delete []red;
                        delete []green;
                        delete []blue;
                        delete []alpha;

                        delete []red16;
                        delete []green16;
                        delete []blue16;
                }

            }
#else

            if (bandRed && bandGreen && bandBlue)
            {
                GLenum pixelFormat = GL_RGBA;

                unsigned char *red = new unsigned char[target_width * target_height];
                unsigned char *green = new unsigned char[target_width * target_height];
                unsigned char *blue = new unsigned char[target_width * target_height];
                unsigned char *alpha = new unsigned char[target_width * target_height];

                //Initialize the alpha values to 255.
                memset(alpha, 255, target_width * target_height);

                image = new osg::Image;
                image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, GL_UNSIGNED_BYTE);
                memset(image->data(), 0, image->getImageSizeInBytes());


                bandRed->RasterIO(GF_Read, off_x, off_y, width, height, red, target_width, target_height, GDT_Byte, 0, 0);
                bandGreen->RasterIO(GF_Read, off_x, off_y, width, height, green, target_width, target_height, GDT_Byte, 0, 0);
                bandBlue->RasterIO(GF_Read, off_x, off_y, width, height, blue, target_width, target_height, GDT_Byte, 0, 0);

                if (bandAlpha)
                {
                    bandAlpha->RasterIO(GF_Read, off_x, off_y, width, height, alpha, target_width, target_height, GDT_Byte, 0, 0);
                }

                for (int src_row = 0, dst_row = tile_offset_top;
                    src_row < target_height;
                    src_row++, dst_row++)
                {
                    for (int src_col = 0, dst_col = tile_offset_left;
                        src_col < target_width;
                        ++src_col, ++dst_col)
                    {
                        unsigned char r = red[src_col + src_row * target_width];
                        unsigned char g = green[src_col + src_row * target_width];
                        unsigned char b = blue[src_col + src_row * target_width];
                        unsigned char a = alpha[src_col + src_row * target_width];
                        *(image->data(dst_col, dst_row) + 0) = r;
                        *(image->data(dst_col, dst_row) + 1) = g;
                        *(image->data(dst_col, dst_row) + 2) = b;                            
/*                            if (!isValidValue( r, bandRed)    ||
                            !isValidValue( g, bandGreen)  || 
                            !isValidValue( b, bandBlue)   ||
                            (bandAlpha && !isValidValue( a, bandAlpha )))
                        {
                            a = 0.0f;
                        }   */                         
                        *(image->data(dst_col, dst_row) + 3) = a;
                    }
                }

                image->flipVertical();

                delete []red;
                delete []green;
                delete []blue;
                delete []alpha;
            }
#endif

#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
            else if (bandGray)
            {
                
                if(_type == GDT_Byte)
                {

                    unsigned char *gray = new unsigned char[target_width * target_height];
                    unsigned char *alpha = new unsigned char[target_width * target_height];

                    //Initialize the alpha values to 255.
                    memset(alpha, 255, target_width * target_height);

                    image = new osg::Image;
                    image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, GL_UNSIGNED_BYTE);
                    memset(image->data(), 0, image->getImageSizeInBytes());


                    //if (!*_options.interpolateImagery() || _options.interpolation() == INTERP_NEAREST)
                    {
                        bandGray->RasterIO(GF_Read, off_x, off_y, width, height, gray, target_width, target_height, GDT_Byte, 0, 0);

                        if (bandAlpha)
                        {
                            bandAlpha->RasterIO(GF_Read, off_x, off_y, width, height, alpha, target_width, target_height, GDT_Byte, 0, 0);
                        }

                        for (int src_row = 0, dst_row = tile_offset_top;
                            src_row < target_height;
                            src_row++, dst_row++)
                        {
                            for (int src_col = 0, dst_col = tile_offset_left;
                                src_col < target_width;
                                ++src_col, ++dst_col)
                            {

                                double g16 = gray[src_col + src_row * target_width];
                                g16 =  sImage->enhanceGreyBandPixel(g16,_enhancementType);

                                if(g16 < 0)
                                {
                                    g16 = 0;
                                }

                                if(g16 > 255)
                                {
                                    g16 = 255;
                                }

                                unsigned char g = g16;
                                //unsigned char g = gray[src_col + src_row * target_width];
                                unsigned char a = alpha[src_col + src_row * target_width];
                                *(image->data(dst_col, dst_row) + 0) = g;
                                *(image->data(dst_col, dst_row) + 1) = g;
                                *(image->data(dst_col, dst_row) + 2) = g;                            
                                if (!isValidValue( g, bandGray) ||
                                   (bandAlpha && !isValidValue( a, bandAlpha)))
                                {
                                    a = 0.0f;
                                }
                                *(image->data(dst_col, dst_row) + 3) = a;
                            }
                        }

                        image->flipVertical();
                    }
    /*                else
                    {
                        for (int c = 0; c < tileSize; ++c) 
                        { 
                            double geoX = xmin + (dx * (double)c); 

                            for (int r = 0; r < tileSize; ++r) 
                            { 
                                double geoY   = ymin + (dy * (double)r); 
                                float  color = getInterpolatedValue(bandGray,geoX,geoY,false); 

                                *(image->data(c,r) + 0) = (unsigned char)color; 
                                *(image->data(c,r) + 1) = (unsigned char)color; 
                                *(image->data(c,r) + 2) = (unsigned char)color; 
                                if (bandAlpha != NULL) 
                                    *(image->data(c,r) + 3) = (unsigned char)getInterpolatedValue(bandAlpha,geoX,geoY,false); 
                                else 
                                    *(image->data(c,r) + 3) = 255; 
                            }
                        }
                    }*/

                    delete []gray;
                    delete []alpha;
                }
                else if (_type == GDT_Float32)
                {
                    unsigned char *gray = new unsigned char[target_width * target_height];
                    unsigned char *alpha = new unsigned char[target_width * target_height];
                    unsigned int *gray32 = new unsigned int[target_width * target_height];

                    //Initialize the alpha values to 255.
                    memset(alpha, 255, target_width * target_height);

                    image = new osg::Image;
                    image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, GL_UNSIGNED_BYTE);
                    memset(image->data(), 0, image->getImageSizeInBytes());

                    double _scaleBy = sImage->getGreyScaleValue();

                    //if (!*_options.interpolateImagery() || _options.interpolation() == INTERP_NEAREST)
                    {
                        //bandGray->RasterIO(GF_Read, off_x, off_y, width, height, gray, target_width, target_height, GDT_Byte, 0, 0);
                        bandGray->RasterIO(GF_Read, off_x, off_y, width, height, gray32, target_width, target_height, _type, 0, 0);


                        if (bandAlpha)
                        {
                            bandAlpha->RasterIO(GF_Read, off_x, off_y, width, height, alpha, target_width, target_height, GDT_Byte, 0, 0);
                        }

                        for (int src_row = 0, dst_row = tile_offset_top;
                            src_row < target_height;
                            src_row++, dst_row++)
                        {
                            for (int src_col = 0, dst_col = tile_offset_left;
                                src_col < target_width;
                                ++src_col, ++dst_col)
                            {
                                double g32 = gray32[src_col + src_row * target_width];

                                g32 = sImage->enhanceGreyBandPixel(g32,_enhancementType);

                                if(g32 < 0)
                                {
                                    g32 = 0;
                                }

                                if(g32 >255)
                                {
                                    g32 = 255;
                                }

                                unsigned char g = g32;
                                unsigned char a = alpha[src_col + src_row * target_width];
                                *(image->data(dst_col, dst_row) + 0) = g;
                                *(image->data(dst_col, dst_row) + 1) = g;
                                *(image->data(dst_col, dst_row) + 2) = g;                            
                                if (!isValidValue( g, bandGray) ||
                                   (bandAlpha && !isValidValue( a, bandAlpha)))
                                {
                                    a = 0.0f;
                                }
                                *(image->data(dst_col, dst_row) + 3) = a;
                            }
                        }

                        image->flipVertical();
                    }
                    //else
                    //{
                    //    for (int c = 0; c < tileSize; ++c) 
                    //    { 
                    //        double geoX = xmin + (dx * (double)c); 

                    //        for (int r = 0; r < tileSize; ++r) 
                    //        { 
                    //            double geoY   = ymin + (dy * (double)r); 
                    //            float  color = getInterpolatedValue(bandGray,geoX,geoY,false); 

                    //            *(image->data(c,r) + 0) = (unsigned char)color; 
                    //            *(image->data(c,r) + 1) = (unsigned char)color; 
                    //            *(image->data(c,r) + 2) = (unsigned char)color; 
                    //            if (bandAlpha != NULL) 
                    //                *(image->data(c,r) + 3) = (unsigned char)getInterpolatedValue(bandAlpha,geoX,geoY,false); 
                    //            else 
                    //                *(image->data(c,r) + 3) = 255; 
                    //        }
                    //    }
                    //}

                    delete []gray;
                    delete []alpha;
                    delete []gray32;
                }
                else if(_type == GDT_UInt16)
                {

                    unsigned char *gray = new unsigned char[target_width * target_height];
                    unsigned char *alpha = new unsigned char[target_width * target_height];
                    unsigned short *gray16 = new unsigned short[target_width * target_height];

                    //Initialize the alpha values to 255.
                    memset(alpha, 255, target_width * target_height);

                    image = new osg::Image;
                    image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, GL_UNSIGNED_BYTE);
                    memset(image->data(), 0, image->getImageSizeInBytes());
                    double _scaleBy = sImage->getGreyScaleValue();

                    //if (!*_options.interpolateImagery() || _options.interpolation() == INTERP_NEAREST)
                    {
                        //bandGray->RasterIO(GF_Read, off_x, off_y, width, height, gray, target_width, target_height, GDT_Byte, 0, 0);
                        bandGray->RasterIO(GF_Read, off_x, off_y, width, height, gray16, target_width, target_height, _type, 0, 0);


                        if (bandAlpha)
                        {
                            bandAlpha->RasterIO(GF_Read, off_x, off_y, width, height, alpha, target_width, target_height, GDT_Byte, 0, 0);
                        }

                        for (int src_row = 0, dst_row = tile_offset_top;
                            src_row < target_height;
                            src_row++, dst_row++)
                        {
                            for (int src_col = 0, dst_col = tile_offset_left;
                                src_col < target_width;
                                ++src_col, ++dst_col)
                            {
                                double g16 = gray16[src_col + src_row * target_width];
                                g16 =  sImage->enhanceGreyBandPixel(g16,_enhancementType);

                                if(g16 < 0)
                                {
                                    g16 = 0;
                                }

                                if(g16 > 255)
                                {
                                    g16 =255;
                                }

                                unsigned char g = g16;

                                unsigned char a = alpha[src_col + src_row * target_width];
                                *(image->data(dst_col, dst_row) + 0) = g;
                                *(image->data(dst_col, dst_row) + 1) = g;
                                *(image->data(dst_col, dst_row) + 2) = g;                            
                                if (!isValidValue( g, bandGray) ||
                                   (bandAlpha && !isValidValue( a, bandAlpha)))
                                {
                                    a = 0.0f;
                                }
                                *(image->data(dst_col, dst_row) + 3) = a;
                            }
                        }

                        image->flipVertical();
                    }
                    //else
                    //{
                    //    for (int c = 0; c < tileSize; ++c) 
                    //    { 
                    //        double geoX = xmin + (dx * (double)c); 

                    //        for (int r = 0; r < tileSize; ++r) 
                    //        { 
                    //            double geoY   = ymin + (dy * (double)r); 
                    //            float  color = getInterpolatedValue(bandGray,geoX,geoY,false); 

                    //            *(image->data(c,r) + 0) = (unsigned char)color; 
                    //            *(image->data(c,r) + 1) = (unsigned char)color; 
                    //            *(image->data(c,r) + 2) = (unsigned char)color; 
                    //            if (bandAlpha != NULL) 
                    //                *(image->data(c,r) + 3) = (unsigned char)getInterpolatedValue(bandAlpha,geoX,geoY,false); 
                    //            else 
                    //                *(image->data(c,r) + 3) = 255; 
                    //        }
                    //    }
                    //}

                    delete []gray;
                    delete []alpha;
                    delete []gray16;
                }

            }
#else
            else if (bandGray)
            {
                GLenum pixelFormat = GL_LUMINANCE;
                GLenum dataType;
                GDALDataType type = GDT_Byte;//_srcDS->GetRasterBand(1)->GetRasterDataType();
                int size = GDALGetDataTypeSize(type)/8;
                unsigned char *gray = new unsigned char[target_width * target_height * size];

                if(type == GDT_Byte)
                {
                    dataType = GL_UNSIGNED_BYTE;
                }
                else if(type == GDT_UInt16)
                {
                    dataType = GL_UNSIGNED_SHORT;
                }



                image = new osg::Image;
                image->allocateImage(TILEWIDTH, TILEHEIGHT, 1, pixelFormat, dataType);
                memset(image->data(), 0, image->getImageSizeInBytes());

                {
                    bandGray->RasterIO(GF_Read, off_x, off_y, width, height, gray, target_width, target_height, type, 0, 0);

                    memcpy(image->data(), gray, target_width * target_height * size);

                    image->flipVertical();

                }
                delete []gray;

            }
#endif
        return image.release();

    }

    osg::Node*
    StereoSceneGenerator::_createSectorForOsgImage(osg::Image* image, double minX, double minY, double maxX, double maxY)
    {

        // set up the texture.
        osg::Texture2D* texture = new osg::Texture2D;
        texture->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR);
        texture->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
        texture->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::CLAMP_TO_EDGE);
        texture->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::CLAMP_TO_EDGE);
        texture->setResizeNonPowerOfTwoHint(false);
        texture->setImage(image);

        // set up the drawstate.
        osg::StateSet* dstate = new osg::StateSet;
        dstate->setMode(GL_CULL_FACE,osg::StateAttribute::OFF);
        dstate->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
        dstate->setTextureAttributeAndModes(0, texture,osg::StateAttribute::ON);
        dstate->setMode(GL_BLEND,osg::StateAttribute::ON);

        // set up the geoset.
        osg::Geometry* geom = new osg::Geometry;
        geom->setStateSet(dstate);

        osg::Vec3Array* coords = new osg::Vec3Array();
        osg::Vec2Array* tcoords = new osg::Vec2Array();

        double CORD_SCALE = 1.0;
        coords->push_back(osg::Vec3(CORD_SCALE * minX,0.0f,CORD_SCALE * minY));
        coords->push_back(osg::Vec3(CORD_SCALE * minX,0.0f,CORD_SCALE * maxY));
        coords->push_back(osg::Vec3(CORD_SCALE * maxX,0.0f,CORD_SCALE * minY));
        coords->push_back(osg::Vec3(CORD_SCALE * maxX,0.0f,CORD_SCALE * maxY));

        tcoords->push_back(osg::Vec2(0, 0));
        tcoords->push_back(osg::Vec2(0, 1));
        tcoords->push_back(osg::Vec2(1, 0));
        tcoords->push_back(osg::Vec2(1, 1));

        osg::Vec4Array* colors = new osg::Vec4Array();
        colors->push_back(osg::Vec4(1.0f,1.0f,1.0f,1.0f));

        osg::DrawArrays* elements = new osg::DrawArrays(osg::PrimitiveSet::TRIANGLE_STRIP,0,coords->size());
        


        geom->setVertexArray(coords);
        geom->setTexCoordArray(0,tcoords);
        geom->setColorArray(colors);
        geom->setColorBinding(osg::Geometry::BIND_OVERALL);

        geom->addPrimitiveSet(elements);

        // set up the geode.
        osg::ref_ptr<osg::Geode> geode = new osg::Geode;
        geode->addDrawable(geom);        

        return geode.release();
    }

    void 
    StereoSceneGenerator::_createImageFromGdal(Image* sImage)
    {
        GDALAllRegister();
        GDALDataset* _srcDS = NULL;

        _srcDS = (GDALDataset*)GDALOpen(sImage->getUrl().c_str(), GA_ReadOnly ); 

        if (!_srcDS)
        {
            std::cout << "Failed to load gdal dataset" << std::endl;
            return;
        }

        GDALRasterBand* bandGray = _srcDS->GetRasterBand(1);

        if(!bandGray)
        {
            std::cout<<"cannot get rasterband"<<std::endl;
            return ;
        }

        double geoExtents[6];

        _srcDS->GetGeoTransform(geoExtents);

        sImage->setGeoExtents(geoExtents);
        int target_width = _srcDS->GetRasterXSize();
        int target_height = _srcDS->GetRasterYSize();

        sImage->setWidth(target_width);
        sImage->setHeight(target_height);

        unsigned char *gray = new unsigned char[target_width * target_height];

        
        osg::ref_ptr<osg::Image> image;
        image = new osg::Image;
        GLenum pixelFormat = GL_LUMINANCE;
        image->allocateImage(target_width, target_height, 1, pixelFormat, GL_UNSIGNED_BYTE);
        memset(image->data(), 0, image->getImageSizeInBytes());
        int off_x = 0;
        int off_y = 0;
        int width = target_width;
        int height = target_height;

        bandGray->RasterIO(GF_Read, off_x, off_y, width, height, gray, target_width, target_height, GDT_Byte, 0, 0);
        memcpy(image->data(),gray,target_width * target_height*sizeof(unsigned char));

        image->flipVertical();

        sImage->setOsgImage(image);
        sImage->isGeoImage(true);

        delete []gray;

    }
    osg::Geode* 
    StereoSceneGenerator::createSectorForImage(Stereo::Image* sImage, osg::TexMat* texmat/*, float s,float t, float radius, float height, float length*/)
    {

        // set up the texture.
        osg::Texture2D* texture = new osg::Texture2D;
        texture->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR);
        texture->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
        texture->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::CLAMP_TO_EDGE);
        texture->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::CLAMP_TO_EDGE);
        texture->setResizeNonPowerOfTwoHint(false);
        texture->setImage(sImage->getOsgImage());

        // set up the drawstate.
        osg::StateSet* dstate = new osg::StateSet;
        dstate->setMode(GL_CULL_FACE,osg::StateAttribute::OFF);
        dstate->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
        dstate->setTextureAttributeAndModes(0, texture,osg::StateAttribute::ON);
        //dstate->setTextureAttribute(0, texmat);
        //dstate->setMode(GL_ALPHA,osg::StateAttribute::ON);
        dstate->setMode(GL_BLEND,osg::StateAttribute::ON);

        // set up the geoset.
        osg::Geometry* geom = new osg::Geometry;
        geom->setStateSet(dstate);

        osg::Vec3Array* coords = new osg::Vec3Array();
        osg::Vec2Array* tcoords = new osg::Vec2Array();


        unsigned int width = sImage->getWidth();
        unsigned int height = sImage->getHeight();

        double minX,minY,maxX,maxY;

        sImage->getBounds(minX,minY,maxX,maxY);
       
        float aspectRatio = float(width)/float(height);

        if(minX < -180 || minY < -90 || maxX > 180 || maxY > 90 || !sImage->isGeoImage())
        {

            if(aspectRatio <= 1.0)
            {
                coords->push_back(osg::Vec3(- (1.0f/(2.0 *aspectRatio)),0.0f,-0.5));
                coords->push_back(osg::Vec3(- (1.0f/(2.0 *aspectRatio)),0.0f,0.5));
                coords->push_back(osg::Vec3((1.0f/(2.0 *aspectRatio)),0.0f,-0.5));
                coords->push_back(osg::Vec3((1.0f/(2.0 *aspectRatio)),0.0f,0.5));
            }
            else
            {
                coords->push_back(osg::Vec3(-0.5,0.0f,- (1.0f/(2.0 *aspectRatio))));
                coords->push_back(osg::Vec3(-0.5,0.0f,(1.0f/(2.0 * aspectRatio))));
                coords->push_back(osg::Vec3(0.5,0.0f,- (1.0f/(2.0 *aspectRatio))));
                coords->push_back(osg::Vec3(0.5,0.0f,(1.0f/(2.0 *aspectRatio))));
            }

        }
        else
        {
            float CORD_SCALE = 1.0;
            coords->push_back(osg::Vec3(CORD_SCALE * minX/180.0,0.0f,CORD_SCALE * minY/180.0));
            coords->push_back(osg::Vec3(CORD_SCALE * minX/180.0,0.0f,CORD_SCALE * maxY/180.0));
            coords->push_back(osg::Vec3(CORD_SCALE * maxX/180.0,0.0f,CORD_SCALE * minY/180.0));
            coords->push_back(osg::Vec3(CORD_SCALE * maxX/180.0,0.0f,CORD_SCALE * maxY/180.0));
        }

        tcoords->push_back(osg::Vec2(0, 0));
        tcoords->push_back(osg::Vec2(0, 1));
        tcoords->push_back(osg::Vec2(1, 0));
        tcoords->push_back(osg::Vec2(1, 1));

        osg::Vec4Array* colors = new osg::Vec4Array();
        colors->push_back(osg::Vec4(1.0f,1.0f,1.0f,1.0f));

        osg::DrawArrays* elements = new osg::DrawArrays(osg::PrimitiveSet::QUAD_STRIP,0,coords->size());
        

        geom->setVertexArray(coords);
        geom->setTexCoordArray(0,tcoords);
        geom->setColorArray(colors);
        geom->setColorBinding(osg::Geometry::BIND_OVERALL);

        geom->addPrimitiveSet(elements);

        // set up the geode.
        osg::Geode* geode = new osg::Geode;
        geode->addDrawable(geom);

        return geode;

    }

#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
    bool 
    StereoSceneGenerator::isValidValue(float v, GDALRasterBand* band)
    {

        float bandNoData = -32767.0f;
        int success;
        float value = band->GetNoDataValue(&success);
        if (success)
        {
            bandNoData = value;
        }

        ////Check to see if the value is equal to the bands specified no data
        //if (bandNoData == v) return false;
        ////Check to see if the value is equal to the user specified nodata value
        //if (getNoDataValue() == v) return false;        

        ////Check to see if the user specified a custom min/max
        //if (v < getNoDataMinValue()) return false;
        //if (v > getNoDataMaxValue()) return false;

        //Check within a sensible range
        if (v < -32000) return false;
        if (v > 32000) return false;

        return true;
    }
#endif

   

}
