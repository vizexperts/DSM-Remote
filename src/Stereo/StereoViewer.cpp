#include <Stitcher/PanoramicViewer.h>
#include<Stitcher/PanoramicEventHandler.h>
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/Texture>
#include <osg/TextureRectangle>
#include <osg/Geode>
#include <osg/Group>
#include <iostream>
#include <osg/TexMat>
#include <osgGA/TrackballManipulator>


namespace Stitcher
{

    PanoramicViewer::PanoramicViewer(osg::ArgumentParser& arguments) : QWidget(),osgViewer::Viewer(arguments)
    {

		//QWidget* widget = addPanoramicViewWidget("E:/DataDownload/ARC_Tests/air.jpeg",createCamera(0,0,100,100));


		//if(widget)
		//{
		//	QGridLayout* grid = new QGridLayout;
		//	grid->addWidget(widget, 0, 0);
		//	setLayout(grid);
		//}
		

        //connect(&_timer, SIGNAL(timeout()), this, SLOT(update()) );
        //_timer.start(10);
    }

	PanoramicViewer::~PanoramicViewer()
	{

	}

	void
	PanoramicViewer::initializeWidget()
	{
		osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
		traits->windowName = "";
		traits->windowDecoration = false;
		traits->x = 0;
		traits->y = 0;
		traits->width = ds->getScreenWidth();
		traits->height = ds->getScreenHeight();
		traits->doubleBuffer = true;
		traits->alpha = ds->getMinimumNumAlphaBits();
		traits->stencil = ds->getMinimumNumStencilBits();
		traits->sampleBuffers = ds->getMultiSamples();
		traits->samples = ds->getNumMultiSamples();

		//osg::ref_ptr<osg::GraphicsContext> gc = osg::GraphicsContext::createGraphicsContext(traits.get());
		osgQt::GraphicsWindowQt* gwqt = new osgQt::GraphicsWindowQt(traits.get());
		getCamera()->setGraphicsContext( gwqt );
		osgQt::GraphicsWindowQt* gw = dynamic_cast<osgQt::GraphicsWindowQt*>( getCamera()->getGraphicsContext() );

		QWidget* widget = gw ? gw->getGLWidget() : NULL;

		//QGridLayout* grid = new QGridLayout;
		//grid->addWidget(widget, 0, 0);
		//setLayout(grid);

        connect(&_timer, SIGNAL(timeout()), this, SLOT(update()) );
        _timer.start(10);

	}


    QWidget* 
	PanoramicViewer::addPanoramicViewWidget(std::string fileUrl, osg::Camera* camera)
    {
  //      osgViewer::View* view = new osgViewer::View;
  //      view->setCamera( camera );
  //      addView( view );
  //      
		//PanoramicEventHandler* peh = new PanoramicEventHandler();
		//view->addEventHandler(peh);

		//// now the windows have been realized we switch off the cursor to prevent it
		//// distracting the people seeing the stereo images.
		//double fovy, aspectRatio, zNear, zFar;
		//view->getCamera()->getProjectionMatrixAsPerspective(fovy, aspectRatio, zNear, zFar);

		//float radius = 1.0f;
	 //   float height = 2*radius*tan(osg::DegreesToRadians(fovy)*0.5f);
	 //   float length = osg::PI*radius;  // half a cylinder.

	 //   // use a texture matrix to control the placement of the image.
	 //   osg::TexMat* texmatImage = new osg::TexMat;

	 //   osg::ref_ptr<osg::Switch> rootNode;
		//rootNode = createScene(fileUrl,texmatImage,radius,height,length);


		//view->getCamera()->setCullMask(0xffffffff);
		//view->getCamera()->setCullMaskLeft(0x00000001);
		//view->getCamera()->setCullMaskRight(0x00000002);
		////view->setUpViewOnSingleScreen(0);
		//

		//view->setSceneData(rootNode);
		//////view->addEventHandler( new osgViewer::StatsHandler );
		//////view->setCameraManipulator( new osgGA::TrackballManipulator );


		//int offsetX = 0, offsetY = 0;
		//float timeDelayBetweenSlides = 5.0f;
		//bool autoSteppingActive = false;
		//peh->set(rootNode.get(),offsetX,offsetY,texmatImage,timeDelayBetweenSlides,autoSteppingActive);

		//osg::Matrix homePosition;
		//homePosition.makeLookAt(osg::Vec3(0.0f,0.0f,0.0f),osg::Vec3(0.0f,1.0f,0.0f),osg::Vec3(0.0f,0.0f,1.0f));

		//view->getCamera()->setViewMatrix(homePosition);

  //      osgQt::GraphicsWindowQt* gw = dynamic_cast<osgQt::GraphicsWindowQt*>( camera->getGraphicsContext() );
  //      return gw ? gw->getGLWidget() : NULL;
		return NULL;
    }

    osg::Camera* 
	PanoramicViewer::createCamera( int x, int y, int w, int h, const std::string& name, bool windowDecoration)
    {
/*        osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
        osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
        traits->windowName = name;
        traits->windowDecoration = windowDecoration;
        traits->x = x;
        traits->y = y;
        traits->width = ds->getScreenWidth();
        traits->height = ds->getScreenHeight();
        traits->doubleBuffer = true;
        traits->alpha = ds->getMinimumNumAlphaBits();
        traits->stencil = ds->getMinimumNumStencilBits();
        traits->sampleBuffers = ds->getMultiSamples();
        traits->samples = ds->getNumMultiSamples()*/;
        
        //osg::ref_ptr<osg::Camera> camera = new osg::Camera;
        //camera->setGraphicsContext( new osgQt::GraphicsWindowQt(traits.get()) );
        //camera->setViewport( new osg::Viewport(0, 0, traits->width, traits->height) );
        //camera->setProjectionMatrixAsPerspective(
        //    30.0f, static_cast<double>(traits->width)/static_cast<double>(traits->height), 1.0f, 10000.0f );
        //return camera.release();
		return NULL;
    }

	void 
	PanoramicViewer::paintEvent( QPaintEvent* event )
    { 
		frame(); 
	}

	osg::Geode* 
	PanoramicViewer::createSectorForImage(osg::Image* image, osg::TexMat* texmat, float s,float t, float radius, float height, float length)
	{
		bool flip = image->getOrigin()==osg::Image::TOP_LEFT;

		int numSegments = 20;
		float Theta = length/radius;
		float dTheta = Theta/(float)(numSegments-1);
	    
		float ThetaZero = height*s/(t*radius);
	    
		// set up the texture.
		osg::Texture2D* texture = new osg::Texture2D;
		texture->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR);
		texture->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
		texture->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::CLAMP_TO_BORDER);
		texture->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::CLAMP_TO_BORDER);
		texture->setResizeNonPowerOfTwoHint(false);
		texture->setImage(image);

		// set up the drawstate.
		osg::StateSet* dstate = new osg::StateSet;
		dstate->setMode(GL_CULL_FACE,osg::StateAttribute::OFF);
		dstate->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
		dstate->setTextureAttributeAndModes(0, texture,osg::StateAttribute::ON);
		dstate->setTextureAttribute(0, texmat);

		// set up the geoset.
		osg::Geometry* geom = new osg::Geometry;
		geom->setStateSet(dstate);

		osg::Vec3Array* coords = new osg::Vec3Array();
		osg::Vec2Array* tcoords = new osg::Vec2Array();
	   
		int i;
		float angle = -Theta/2.0f;
		for(i=0;
			i<numSegments;
			++i, angle+=dTheta)
		{
			coords->push_back(osg::Vec3(sinf(angle)*radius,cosf(angle)*radius,height*0.5f)); // top
			coords->push_back(osg::Vec3(sinf(angle)*radius,cosf(angle)*radius,-height*0.5f)); // bottom.
	        
			tcoords->push_back(osg::Vec2(angle/ThetaZero+0.5f, flip ? 0.0f : 1.0f)); // top
			tcoords->push_back(osg::Vec2(angle/ThetaZero+0.5f, flip ? 1.0f : 0.0f)); // bottom.

		}

		osg::Vec4Array* colors = new osg::Vec4Array();
		colors->push_back(osg::Vec4(1.0f,1.0f,1.0f,1.0f));

		osg::DrawArrays* elements = new osg::DrawArrays(osg::PrimitiveSet::QUAD_STRIP,0,coords->size());

	    

		geom->setVertexArray(coords);
		geom->setTexCoordArray(0,tcoords);
		geom->setColorArray(colors);
		geom->setColorBinding(osg::Geometry::BIND_OVERALL);

		geom->addPrimitiveSet(elements);

		// set up the geode.
		osg::Geode* geode = new osg::Geode;
		geode->addDrawable(geom);

		return geode;

	}


	osg::Group * 
	PanoramicViewer::loadImage(std::string fileUrl, osg::TexMat* texmatImage, float radius, float height, float length)
	{
		osg::ref_ptr<osg::Image> image = osgDB::readImageFile(fileUrl);

		if (image.valid())
		{
			osg::ImageStream* streamImage = dynamic_cast<osg::ImageStream*>(image.get());
			if (streamImage) streamImage->play();

			osg::Geode* geodeImage = createSectorForImage(image.get(),texmatImage,image->s(),image->t(), radius, height, length);
			geodeImage->setNodeMask(0x01);

			osg::Group * imageGroup = new osg::Group;

			imageGroup->addChild(geodeImage);
			return imageGroup;
		}
		else
		{
			std::cout << "Warning: Unable to load both image files, '"<<image<<"', required for stereo imaging."<<std::endl;
			return 0;
		}
	}



	osg::Switch* 
	PanoramicViewer::createScene(std::string fileUrl, osg::TexMat* texmatImage,float radius, float height, float length)
	{
		osg::Switch* sw = new osg::Switch;

		// load the images.
			osg::Group * imageGroup = loadImage(fileUrl,texmatImage, radius,  height, length);
			if (imageGroup) sw->addChild(imageGroup);

		if (sw->getNumChildren()>0)
		{
			// select first child.
			sw->setSingleChildOn(0);
		}

		return sw;
	}

}