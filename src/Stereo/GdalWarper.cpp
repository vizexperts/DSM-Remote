#include <Stereo/GdalWarper.h>

namespace Stereo
{

    GdalWarper::GdalWarper()
        :dfMinX(0.0),
        dfMinY(0.0),
        dfMaxX(0.0),
        dfMaxY(0.0),
        dfXRes(0.0),
        dfYRes(0.0),
        bTargetAlignedPixels(0),
        nForcePixels(0),
        nForceLines(0),
        bQuiet(0),
        bEnableDstAlpha(0),
        bEnableSrcAlpha(0),
        bVRT(0),
        bOverwrite(1)

    {


    }

    GdalWarper::~GdalWarper()
    {


    }


    void 
    GdalWarper::setOutputFormat(std::string format)
    {
        outputFormat = format;

    }

    std::string 
    GdalWarper::getOutputFormat()
    {
        return outputFormat;

    }

    void 
    GdalWarper::setWarpMethod(std::string method)
    {

        warpMethod = method;
    }

    std::string 
    GdalWarper::getWarpMethod()
    {
        return warpMethod;

    }

    void 
    GdalWarper::setDataType(std::string type)
    {
        dataType = type;
    }

    std::string 
    GdalWarper::getDataType()
    {
        return dataType;

    }

    void 
    GdalWarper::setInputImageUrl(std::string url)
    {
        inputImageUrl = url;


    }

    std::string 
    GdalWarper::getInputImageUrl()
    {
        return inputImageUrl;

    }

    void 
    GdalWarper::setOutputImageUrl(std::string url)
    {
        outputImageUrl = url;

    }

    std::string 
    GdalWarper::getOutputImageUrl()
    {
        return outputImageUrl;

    }

    void
    GdalWarper::start()
    {
        int returnCode = warp();
        if(returnCode == 1)
        {
            emit sucessfull("Processing Sucessfull");
        }
        else if(returnCode == -1)
        {            
            emit failed("Error occured while Processing image");
        }
    }

    int
    GdalWarper::warp()
    {
        GDALDatasetH    hDstDS;
        const char         *pszFormat = "GTiff";
        int bFormatExplicitelySet = FALSE;
        char              **papszSrcFiles = NULL;
        char               *pszDstFilename = NULL;
        int                 bCreateOutput = FALSE, i;
        void               *hTransformArg, *hGenImgProjArg=NULL, *hApproxArg=NULL;
        char               **papszWarpOptions = NULL;
        double             dfErrorThreshold = 0.125;
        double             dfWarpMemoryLimit = 0.0;
        GDALTransformerFunc pfnTransformer = NULL;
        char                **papszCreateOptions = NULL;
        GDALDataType        eOutputType = GDT_Unknown, eWorkingType = GDT_Unknown; 
        GDALResampleAlg     eResampleAlg = GRA_NearestNeighbour;
        const char          *pszSrcNodata = NULL;
        const char          *pszDstNodata = NULL;
        int                 bMulti = FALSE;
        char                **papszTO = NULL;
        char                *pszCutlineDSName = NULL;
        char                *pszCLayer = NULL, *pszCWHERE = NULL, *pszCSQL = NULL;
        void                *hCutline = NULL;
        int                  bHasGotErr = FALSE;
        int                  bCropToCutline = FALSE;
        int                  bCopyMetadata = TRUE;
        int                  bCopyBandInfo = TRUE;
        const char           *pszMDConflictValue = "*";

        papszSrcFiles = CSLAddString( papszSrcFiles, inputImageUrl.c_str() );
        papszSrcFiles = CSLAddString( papszSrcFiles, outputImageUrl.c_str() );

        pszDstFilename = papszSrcFiles[CSLCount(papszSrcFiles)-1];
        papszSrcFiles[CSLCount(papszSrcFiles)-1] = NULL;


        GDALAllRegister();

        //choosing the method for the warp(using RPC as method for this application)
        papszTO = CSLSetNameValue( papszTO, "METHOD", warpMethod.c_str() );

        //choosing the format of the outputfile(gtiff for this application)
        pszFormat = outputFormat.c_str();
        bFormatExplicitelySet = TRUE;
        bCreateOutput = TRUE;
        if( EQUAL(pszFormat,"VRT") )
            bVRT = TRUE;


        int    iType;
            
        for( iType = 1; iType < GDT_TypeCount; iType++ )
        {
            if( GDALGetDataTypeName((GDALDataType)iType) != NULL
                && EQUAL(GDALGetDataTypeName((GDALDataType)iType),
                         dataType.c_str()))
            {
                eOutputType = (GDALDataType) iType;
            }
        }

        bCreateOutput = TRUE;

        /* -------------------------------------------------------------------- */
        /*      Does the output dataset already exist?                          */
        /* -------------------------------------------------------------------- */

            /* FIXME ? source filename=target filename and -overwrite is definitely */
            /* an error. But I can't imagine of a valid case (without -overwrite), */
            /* where it would make sense. In doubt, let's keep that dubious possibility... */
            if ( CSLCount(papszSrcFiles) == 1 &&
                 strcmp(papszSrcFiles[0], pszDstFilename) == 0 && bOverwrite)
            {
                fprintf(stderr, "Source and destination datasets must be different.\n");
                emit failed("Source and destination datasets must be different");
                return 0;
                //GDALExit( 1 );
            }

            CPLPushErrorHandler( CPLQuietErrorHandler );
            hDstDS = GDALOpen( pszDstFilename, GA_Update );
            CPLPopErrorHandler();

            if( hDstDS != NULL && bOverwrite )
            {
                GDALClose(hDstDS);
                hDstDS = NULL;
            }

            if( hDstDS != NULL && bCreateOutput )
            {
                fprintf( stderr, 
                         "Output dataset %s exists,\n"
                         "but some commandline options were provided indicating a new dataset\n"
                         "should be created.  Please delete existing dataset and run again.\n",
                         pszDstFilename );
                return -1;
                //GDALExit( 1 );
            }

            /* Avoid overwriting an existing destination file that cannot be opened in */
            /* update mode with a new GTiff file */
            if ( hDstDS == NULL && !bOverwrite )
            {
                CPLPushErrorHandler( CPLQuietErrorHandler );
                hDstDS = GDALOpen( pszDstFilename, GA_ReadOnly );
                CPLPopErrorHandler();
                
                if (hDstDS)
                {
                    fprintf( stderr, 
                             "Output dataset %s exists, but cannot be opened in update mode\n",
                             pszDstFilename );
                    GDALClose(hDstDS);
                    emit failed("Output dataset exists, but cannot be opened in update mode\n");
                    return 0;
                    //GDALExit( 1 );
                }
            }

        ///* -------------------------------------------------------------------- */
        ///*      If we have a cutline datasource read it and attach it in the    */
        ///*      warp options.                                                   */
        ///* -------------------------------------------------------------------- */
        //    if( pszCutlineDSName != NULL )
        //    {
        //        LoadCutline( pszCutlineDSName, pszCLayer, pszCWHERE, pszCSQL,
        //                     &hCutline );
        //    }

        #ifdef OGR_ENABLED
            if ( bCropToCutline && hCutline != NULL )
            {
                OGRGeometryH hCutlineGeom = OGR_G_Clone( (OGRGeometryH) hCutline );
                OGRSpatialReferenceH hCutlineSRS = OGR_G_GetSpatialReference( hCutlineGeom );
                const char *pszThisTargetSRS = CSLFetchNameValue( papszTO, "DST_SRS" );
                OGRCoordinateTransformationH hCT = NULL;
                if (hCutlineSRS == NULL)
                {
                    /* We suppose it is in target coordinates */
                }
                else if (pszThisTargetSRS != NULL)
                {
                    OGRSpatialReferenceH hTargetSRS = OSRNewSpatialReference(NULL);
                    if( OSRImportFromWkt( hTargetSRS, (char **)&pszThisTargetSRS ) != CE_None )
                    {
                        fprintf(stderr, "Cannot compute bounding box of cutline.\n");
                        GDALExit(1);
                    }

                    hCT = OCTNewCoordinateTransformation(hCutlineSRS, hTargetSRS);

                    OSRDestroySpatialReference(hTargetSRS);
                }
                else if (pszThisTargetSRS == NULL)
                {
                    if (papszSrcFiles[0] != NULL)
                    {
                        GDALDatasetH hSrcDS = GDALOpen(papszSrcFiles[0], GA_ReadOnly);
                        if (hSrcDS == NULL)
                        {
                            fprintf(stderr, "Cannot compute bounding box of cutline.\n");
                            GDALExit(1);
                        }

                        OGRSpatialReferenceH  hRasterSRS = NULL;
                        const char *pszProjection = NULL;

                        if( GDALGetProjectionRef( hSrcDS ) != NULL
                            && strlen(GDALGetProjectionRef( hSrcDS )) > 0 )
                            pszProjection = GDALGetProjectionRef( hSrcDS );
                        else if( GDALGetGCPProjection( hSrcDS ) != NULL )
                            pszProjection = GDALGetGCPProjection( hSrcDS );

                        if( pszProjection == NULL )
                        {
                            fprintf(stderr, "Cannot compute bounding box of cutline.\n");
                            GDALExit(1);
                        }

                        hRasterSRS = OSRNewSpatialReference(NULL);
                        if( OSRImportFromWkt( hRasterSRS, (char **)&pszProjection ) != CE_None )
                        {
                            fprintf(stderr, "Cannot compute bounding box of cutline.\n");
                            GDALExit(1);
                        }

                        hCT = OCTNewCoordinateTransformation(hCutlineSRS, hRasterSRS);

                        OSRDestroySpatialReference(hRasterSRS);

                        GDALClose(hSrcDS);
                    }
                    else
                    {
                        fprintf(stderr, "Cannot compute bounding box of cutline.\n");
                        GDALExit(1);
                    }
                }

                if (hCT)
                {
                    OGR_G_Transform( hCutlineGeom, hCT );

                    OCTDestroyCoordinateTransformation(hCT);
                }

                OGREnvelope sEnvelope;
                OGR_G_GetEnvelope(hCutlineGeom, &sEnvelope);

                dfMinX = sEnvelope.MinX;
                dfMinY = sEnvelope.MinY;
                dfMaxX = sEnvelope.MaxX;
                dfMaxY = sEnvelope.MaxY;
                
                OGR_G_DestroyGeometry(hCutlineGeom);
            }
        #endif
            
        /* -------------------------------------------------------------------- */
        /*      If not, we need to create it.                                   */
        /* -------------------------------------------------------------------- */
            int   bInitDestSetForFirst = FALSE;

            void* hUniqueTransformArg = NULL;
            GDALDatasetH hUniqueSrcDS = NULL;

            if( hDstDS == NULL )
            {
                if (!bQuiet && !bFormatExplicitelySet)
                    CheckExtensionConsistency(pszDstFilename, pszFormat);

                hDstDS = GDALWarpCreateOutput( papszSrcFiles, pszDstFilename,pszFormat,
                                               papszTO, &papszCreateOptions, 
                                               eOutputType, &hUniqueTransformArg,
                                               &hUniqueSrcDS);
                bCreateOutput = TRUE;

                if( CSLFetchNameValue( papszWarpOptions, "INIT_DEST" ) == NULL 
                    && pszDstNodata == NULL )
                {
                    papszWarpOptions = CSLSetNameValue(papszWarpOptions,
                                                       "INIT_DEST", "0");
                    bInitDestSetForFirst = TRUE;
                }
                else if( CSLFetchNameValue( papszWarpOptions, "INIT_DEST" ) == NULL )
                {
                    papszWarpOptions = CSLSetNameValue(papszWarpOptions,
                                                       "INIT_DEST", "NO_DATA" );
                    bInitDestSetForFirst = TRUE;
                }

                CSLDestroy( papszCreateOptions );
                papszCreateOptions = NULL;
            }
         
            if( hDstDS == NULL )
            {
                return -1;
                //GDALExit( 1 );
            }
                

        /* -------------------------------------------------------------------- */
        /*      Loop over all source files, processing each in turn.            */
        /* -------------------------------------------------------------------- */
            int iSrc;

            for( iSrc = 0; papszSrcFiles[iSrc] != NULL; iSrc++ )
            {
                GDALDatasetH hSrcDS;
               
        /* -------------------------------------------------------------------- */
        /*      Open this file.                                                 */
        /* -------------------------------------------------------------------- */
                if (hUniqueSrcDS)
                    hSrcDS = hUniqueSrcDS;
                else
                    hSrcDS = GDALOpen( papszSrcFiles[iSrc], GA_ReadOnly );
            
                if( hSrcDS == NULL )
                {
                    return -1;
                    //GDALExit( 2 );
                }

        /* -------------------------------------------------------------------- */
        /*      Check that there's at least one raster band                     */
        /* -------------------------------------------------------------------- */
                if ( GDALGetRasterCount(hSrcDS) == 0 )
                {     
                    fprintf(stderr, "Input file %s has no raster bands.\n", papszSrcFiles[iSrc] );
                    emit failed("Input file  has no raster bands.");
                    return 0;
                    //GDALExit( 1 );
                }

                if( !bQuiet )
                    printf( "Processing input file %s.\n", papszSrcFiles[iSrc] );

        /* -------------------------------------------------------------------- */
        /*      Get the metadata of the first source DS and copy it to the      */
        /*      destination DS. Copy Band-level metadata and other info, only   */
        /*      if source and destination band count are equal. Any values that */
        /*      conflict between source datasets are set to pszMDConflictValue. */
        /* -------------------------------------------------------------------- */
                if ( bCopyMetadata )
                {
                    char **papszMetadata = NULL;
                    const char *pszSrcInfo = NULL;
                    const char *pszDstInfo = NULL;
                    GDALRasterBandH hSrcBand = NULL;
                    GDALRasterBandH hDstBand = NULL;

                    /* copy metadata from first dataset */
                    if ( iSrc == 0 )
                    {
                        CPLDebug("WARP", "Copying metadata from first source to destination dataset");
                        /* copy dataset-level metadata */
                        papszMetadata = GDALGetMetadata( hSrcDS, NULL );
                        if ( CSLCount(papszMetadata) > 0 ) {
                            if ( GDALSetMetadata( hDstDS, papszMetadata, NULL ) != CE_None )
                                 fprintf( stderr, 
                                          "Warning: error copying metadata to destination dataset.\n" );
                        }
                        /* copy band-level metadata and other info */
                        if ( GDALGetRasterCount( hSrcDS ) == GDALGetRasterCount( hDstDS ) )              
                        {
                            for ( int iBand = 0; iBand < GDALGetRasterCount( hSrcDS ); iBand++ )
                            {
                                hSrcBand = GDALGetRasterBand( hSrcDS, iBand + 1 );
                                hDstBand = GDALGetRasterBand( hDstDS, iBand + 1 );
                                /* copy metadata */
                                papszMetadata = GDALGetMetadata( hSrcBand, NULL);              
                                if ( CSLCount(papszMetadata) > 0 )
                                    GDALSetMetadata( hDstBand, papszMetadata, NULL );
                                /* copy other info (Description, Unit Type) - what else? */
                                if ( bCopyBandInfo ) {
                                    pszSrcInfo = GDALGetDescription( hSrcBand );
                                    if(  pszSrcInfo != NULL && strlen(pszSrcInfo) > 0 )
                                        GDALSetDescription( hDstBand, pszSrcInfo );  
                                    pszSrcInfo = GDALGetRasterUnitType( hSrcBand );
                                    if(  pszSrcInfo != NULL && strlen(pszSrcInfo) > 0 )
                                        GDALSetRasterUnitType( hDstBand, pszSrcInfo );  
                                }
                            }
                        }
                    }
                    /* remove metadata that conflicts between datasets */
                    else 
                    {
                        CPLDebug("WARP", 
                                 "Removing conflicting metadata from destination dataset (source #%d)", iSrc );
                        /* remove conflicting dataset-level metadata */
                        RemoveConflictingMetadata( hDstDS, GDALGetMetadata( hSrcDS, NULL ), pszMDConflictValue );
                        
                        /* remove conflicting copy band-level metadata and other info */
                        if ( GDALGetRasterCount( hSrcDS ) == GDALGetRasterCount( hDstDS ) )              
                        {
                            for ( int iBand = 0; iBand < GDALGetRasterCount( hSrcDS ); iBand++ )
                            {
                                hSrcBand = GDALGetRasterBand( hSrcDS, iBand + 1 );
                                hDstBand = GDALGetRasterBand( hDstDS, iBand + 1 );
                                /* remove conflicting metadata */
                                RemoveConflictingMetadata( hDstBand, GDALGetMetadata( hSrcBand, NULL ), pszMDConflictValue );
                                /* remove conflicting info */
                                if ( bCopyBandInfo ) {
                                    pszSrcInfo = GDALGetDescription( hSrcBand );
                                    pszDstInfo = GDALGetDescription( hDstBand );
                                    if( ! ( pszSrcInfo != NULL && strlen(pszSrcInfo) > 0  &&
                                            pszDstInfo != NULL && strlen(pszDstInfo) > 0  &&
                                            EQUAL( pszSrcInfo, pszDstInfo ) ) )
                                        GDALSetDescription( hDstBand, "" );  
                                    pszSrcInfo = GDALGetRasterUnitType( hSrcBand );
                                    pszDstInfo = GDALGetRasterUnitType( hDstBand );
                                    if( ! ( pszSrcInfo != NULL && strlen(pszSrcInfo) > 0  &&
                                            pszDstInfo != NULL && strlen(pszDstInfo) > 0  &&
                                            EQUAL( pszSrcInfo, pszDstInfo ) ) )
                                        GDALSetRasterUnitType( hDstBand, "" );  
                                }
                            }
                        }
                    }          
                }

        /* -------------------------------------------------------------------- */
        /*      Warns if the file has a color table and something more          */
        /*      complicated than nearest neighbour resampling is asked          */
        /* -------------------------------------------------------------------- */
         
                if ( eResampleAlg != GRA_NearestNeighbour &&
                     GDALGetRasterColorTable(GDALGetRasterBand(hSrcDS, 1)) != NULL)
                {
                    if( !bQuiet )
                        fprintf( stderr, "Warning: Input file %s has a color table, which will likely lead to "
                                "bad results when using a resampling method other than "
                                "nearest neighbour. Converting the dataset prior to 24/32 bit "
                                "is advised.\n", papszSrcFiles[iSrc] );
                }

        /* -------------------------------------------------------------------- */
        /*      Do we have a source alpha band?                                 */
        /* -------------------------------------------------------------------- */
                if( GDALGetRasterColorInterpretation( 
                        GDALGetRasterBand(hSrcDS,GDALGetRasterCount(hSrcDS)) ) 
                    == GCI_AlphaBand 
                    && !bEnableSrcAlpha )
                {
                    bEnableSrcAlpha = TRUE;
                    if( !bQuiet )
                        printf( "Using band %d of source image as alpha.\n", 
                                GDALGetRasterCount(hSrcDS) );
                }

        /* -------------------------------------------------------------------- */
        /*      Create a transformation object from the source to               */
        /*      destination coordinate system.                                  */
        /* -------------------------------------------------------------------- */
                if (hUniqueTransformArg)
                    hTransformArg = hGenImgProjArg = hUniqueTransformArg;
                else
                    hTransformArg = hGenImgProjArg =
                        GDALCreateGenImgProjTransformer2( hSrcDS, hDstDS, papszTO );
                
                if( hTransformArg == NULL )
                {
                    return -1;
                    //GDALExit( 1 );
                }
                
                pfnTransformer = GDALGenImgProjTransform;

        /* -------------------------------------------------------------------- */
        /*      Warp the transformer with a linear approximator unless the      */
        /*      acceptable error is zero.                                       */
        /* -------------------------------------------------------------------- */
                if( dfErrorThreshold != 0.0 )
                {
                    hTransformArg = hApproxArg = 
                        GDALCreateApproxTransformer( GDALGenImgProjTransform, 
                                                     hGenImgProjArg, dfErrorThreshold);
                    pfnTransformer = GDALApproxTransform;
                }

        /* -------------------------------------------------------------------- */
        /*      Clear temporary INIT_DEST settings after the first image.       */
        /* -------------------------------------------------------------------- */
                if( bInitDestSetForFirst && iSrc == 1 )
                    papszWarpOptions = CSLSetNameValue( papszWarpOptions, 
                                                        "INIT_DEST", NULL );

        /* -------------------------------------------------------------------- */
        /*      Setup warp options.                                             */
        /* -------------------------------------------------------------------- */
                GDALWarpOptions *psWO = GDALCreateWarpOptions();

                psWO->papszWarpOptions = CSLDuplicate(papszWarpOptions);
                psWO->eWorkingDataType = eWorkingType;
                psWO->eResampleAlg = eResampleAlg;

                psWO->hSrcDS = hSrcDS;
                psWO->hDstDS = hDstDS;

                psWO->pfnTransformer = pfnTransformer;
                psWO->pTransformerArg = hTransformArg;

                if( !bQuiet )
                    psWO->pfnProgress = GDALTermProgress;

                if( dfWarpMemoryLimit != 0.0 )
                    psWO->dfWarpMemoryLimit = dfWarpMemoryLimit;

        /* -------------------------------------------------------------------- */
        /*      Setup band mapping.                                             */
        /* -------------------------------------------------------------------- */
                if( bEnableSrcAlpha )
                    psWO->nBandCount = GDALGetRasterCount(hSrcDS) - 1;
                else
                    psWO->nBandCount = GDALGetRasterCount(hSrcDS);

                psWO->panSrcBands = (int *) CPLMalloc(psWO->nBandCount*sizeof(int));
                psWO->panDstBands = (int *) CPLMalloc(psWO->nBandCount*sizeof(int));

                for( i = 0; i < psWO->nBandCount; i++ )
                {
                    psWO->panSrcBands[i] = i+1;
                    psWO->panDstBands[i] = i+1;
                }

        /* -------------------------------------------------------------------- */
        /*      Setup alpha bands used if any.                                  */
        /* -------------------------------------------------------------------- */
                if( bEnableSrcAlpha )
                    psWO->nSrcAlphaBand = GDALGetRasterCount(hSrcDS);

                if( !bEnableDstAlpha 
                    && GDALGetRasterCount(hDstDS) == psWO->nBandCount+1 
                    && GDALGetRasterColorInterpretation( 
                        GDALGetRasterBand(hDstDS,GDALGetRasterCount(hDstDS))) 
                    == GCI_AlphaBand )
                {
                    if( !bQuiet )
                        printf( "Using band %d of destination image as alpha.\n", 
                                GDALGetRasterCount(hDstDS) );
                        
                    bEnableDstAlpha = TRUE;
                }

                if( bEnableDstAlpha )
                    psWO->nDstAlphaBand = GDALGetRasterCount(hDstDS);

        /* -------------------------------------------------------------------- */
        /*      Setup NODATA options.                                           */
        /* -------------------------------------------------------------------- */
                if( pszSrcNodata != NULL && !EQUALN(pszSrcNodata,"n",1) )
                {
                    char **papszTokens = CSLTokenizeString( pszSrcNodata );
                    int  nTokenCount = CSLCount(papszTokens);

                    psWO->padfSrcNoDataReal = (double *) 
                        CPLMalloc(psWO->nBandCount*sizeof(double));
                    psWO->padfSrcNoDataImag = (double *) 
                        CPLMalloc(psWO->nBandCount*sizeof(double));

                    for( i = 0; i < psWO->nBandCount; i++ )
                    {
                        if( i < nTokenCount )
                        {
                            CPLStringToComplex( papszTokens[i], 
                                                psWO->padfSrcNoDataReal + i,
                                                psWO->padfSrcNoDataImag + i );
                        }
                        else
                        {
                            psWO->padfSrcNoDataReal[i] = psWO->padfSrcNoDataReal[i-1];
                            psWO->padfSrcNoDataImag[i] = psWO->padfSrcNoDataImag[i-1];
                        }
                    }

                    CSLDestroy( papszTokens );

                    psWO->papszWarpOptions = CSLSetNameValue(psWO->papszWarpOptions,
                                                       "UNIFIED_SRC_NODATA", "YES" );
                }

        /* -------------------------------------------------------------------- */
        /*      If -srcnodata was not specified, but the data has nodata        */
        /*      values, use them.                                               */
        /* -------------------------------------------------------------------- */
                if( pszSrcNodata == NULL )
                {
                    int bHaveNodata = FALSE;
                    double dfReal = 0.0;

                    for( i = 0; !bHaveNodata && i < psWO->nBandCount; i++ )
                    {
                        GDALRasterBandH hBand = GDALGetRasterBand( hSrcDS, i+1 );
                        dfReal = GDALGetRasterNoDataValue( hBand, &bHaveNodata );
                    }

                    if( bHaveNodata )
                    {
                        if( !bQuiet )
                        {
                            if (CPLIsNan(dfReal))
                                printf( "Using internal nodata values (eg. nan) for image %s.\n",
                                        papszSrcFiles[iSrc] );
                            else
                                printf( "Using internal nodata values (eg. %g) for image %s.\n",
                                        dfReal, papszSrcFiles[iSrc] );
                        }
                        psWO->padfSrcNoDataReal = (double *) 
                            CPLMalloc(psWO->nBandCount*sizeof(double));
                        psWO->padfSrcNoDataImag = (double *) 
                            CPLMalloc(psWO->nBandCount*sizeof(double));
                        
                        for( i = 0; i < psWO->nBandCount; i++ )
                        {
                            GDALRasterBandH hBand = GDALGetRasterBand( hSrcDS, i+1 );

                            dfReal = GDALGetRasterNoDataValue( hBand, &bHaveNodata );

                            if( bHaveNodata )
                            {
                                psWO->padfSrcNoDataReal[i] = dfReal;
                                psWO->padfSrcNoDataImag[i] = 0.0;
                            }
                            else
                            {
                                psWO->padfSrcNoDataReal[i] = -123456.789;
                                psWO->padfSrcNoDataImag[i] = 0.0;
                            }
                        }
                    }
                }

        /* -------------------------------------------------------------------- */
        /*      If the output dataset was created, and we have a destination    */
        /*      nodata value, go through marking the bands with the information.*/
        /* -------------------------------------------------------------------- */
                if( pszDstNodata != NULL )
                {
                    char **papszTokens = CSLTokenizeString( pszDstNodata );
                    int  nTokenCount = CSLCount(papszTokens);

                    psWO->padfDstNoDataReal = (double *) 
                        CPLMalloc(psWO->nBandCount*sizeof(double));
                    psWO->padfDstNoDataImag = (double *) 
                        CPLMalloc(psWO->nBandCount*sizeof(double));

                    for( i = 0; i < psWO->nBandCount; i++ )
                    {
                        if( i < nTokenCount )
                        {
                            CPLStringToComplex( papszTokens[i], 
                                                psWO->padfDstNoDataReal + i,
                                                psWO->padfDstNoDataImag + i );
                        }
                        else
                        {
                            psWO->padfDstNoDataReal[i] = psWO->padfDstNoDataReal[i-1];
                            psWO->padfDstNoDataImag[i] = psWO->padfDstNoDataImag[i-1];
                        }
                        
                        GDALRasterBandH hBand = GDALGetRasterBand( hDstDS, i+1 );
                        int bClamped = FALSE, bRounded = FALSE;

        #define CLAMP(val,type,minval,maxval) \
            do { if (val < minval) { bClamped = TRUE; val = minval; } \
            else if (val > maxval) { bClamped = TRUE; val = maxval; } \
            else if (val != (type)val) { bRounded = TRUE; val = (type)(val + 0.5); } } \
            while(0)

                        switch(GDALGetRasterDataType(hBand))
                        {
                            case GDT_Byte:
                                CLAMP(psWO->padfDstNoDataReal[i], GByte,
                                      0.0, 255.0);
                                break;
                            case GDT_Int16:
                                CLAMP(psWO->padfDstNoDataReal[i], GInt16,
                                      -32768.0, 32767.0);
                                break;
                            case GDT_UInt16:
                                CLAMP(psWO->padfDstNoDataReal[i], GUInt16,
                                      0.0, 65535.0);
                                break;
                            case GDT_Int32:
                                CLAMP(psWO->padfDstNoDataReal[i], GInt32,
                                      -2147483648.0, 2147483647.0);
                                break;
                            case GDT_UInt32:
                                CLAMP(psWO->padfDstNoDataReal[i], GUInt32,
                                      0.0, 4294967295.0);
                                break;
                            default:
                                break;
                        }
                            
                        if (bClamped)
                        {
                            printf( "for band %d, destination nodata value has been clamped "
                                   "to %.0f, the original value being out of range.\n",
                                   i + 1, psWO->padfDstNoDataReal[i]);
                        }
                        else if(bRounded)
                        {
                            printf("for band %d, destination nodata value has been rounded "
                                   "to %.0f, %s being an integer datatype.\n",
                                   i + 1, psWO->padfDstNoDataReal[i],
                                   GDALGetDataTypeName(GDALGetRasterDataType(hBand)));
                        }

                        if( bCreateOutput )
                        {
                            GDALSetRasterNoDataValue( 
                                GDALGetRasterBand( hDstDS, psWO->panDstBands[i] ), 
                                psWO->padfDstNoDataReal[i] );
                        }
                    }

                    CSLDestroy( papszTokens );
                }

        /* -------------------------------------------------------------------- */
        /*      If we have a cutline, transform it into the source              */
        /*      pixel/line coordinate system and insert into warp options.      */
        /* -------------------------------------------------------------------- */
                //if( hCutline != NULL )
                //{
                //    TransformCutlineToSource( hSrcDS, hCutline, 
                //                              &(psWO->papszWarpOptions), 
                //                              papszTO );
                //}

        /* -------------------------------------------------------------------- */
        /*      If we are producing VRT output, then just initialize it with    */
        /*      the warp options and write out now rather than proceeding       */
        /*      with the operations.                                            */
        /* -------------------------------------------------------------------- */
                if( bVRT )
                {
                    if( GDALInitializeWarpedVRT( hDstDS, psWO ) != CE_None )
                    {
                        return -1;
                        //GDALExit( 1 );
                    }

                    GDALClose( hDstDS );
                    GDALClose( hSrcDS );

                    /* The warped VRT will clean itself the transformer used */
                    /* So we have only to destroy the hGenImgProjArg if we */
                    /* have wrapped it inside the hApproxArg */
                    if (pfnTransformer == GDALApproxTransform)
                    {
                        if( hGenImgProjArg != NULL )
                            GDALDestroyGenImgProjTransformer( hGenImgProjArg );
                    }

                    GDALDestroyWarpOptions( psWO );

                    CPLFree( pszDstFilename );
                    //CSLDestroy( argv );
                    CSLDestroy( papszSrcFiles );
                    CSLDestroy( papszWarpOptions );
                    CSLDestroy( papszTO );
            
                    GDALDumpOpenDatasets( stderr );
                
                    GDALDestroyDriverManager();
                
                    return 1;
                }

        /* -------------------------------------------------------------------- */
        /*      Initialize and execute the warp.                                */
        /* -------------------------------------------------------------------- */
                GDALWarpOperation oWO;

                if( oWO.Initialize( psWO ) == CE_None )
                {
                    CPLErr eErr;
                    if( bMulti )
                        eErr = oWO.ChunkAndWarpMulti( 0, 0, 
                                               GDALGetRasterXSize( hDstDS ),
                                               GDALGetRasterYSize( hDstDS ) );
                    else
                        eErr = oWO.ChunkAndWarpImage( 0, 0, 
                                               GDALGetRasterXSize( hDstDS ),
                                               GDALGetRasterYSize( hDstDS ) );
                    if (eErr != CE_None)
                        bHasGotErr = TRUE;
                }

        /* -------------------------------------------------------------------- */
        /*      Cleanup                                                         */
        /* -------------------------------------------------------------------- */
                if( hApproxArg != NULL )
                    GDALDestroyApproxTransformer( hApproxArg );
                
                if( hGenImgProjArg != NULL )
                    GDALDestroyGenImgProjTransformer( hGenImgProjArg );
                
                GDALDestroyWarpOptions( psWO );

                GDALClose( hSrcDS );
            }

        /* -------------------------------------------------------------------- */
        /*      Final Cleanup.                                                  */
        /* -------------------------------------------------------------------- */
            CPLErrorReset();
            GDALFlushCache( hDstDS );
            if( CPLGetLastErrorType() != CE_None )
                bHasGotErr = TRUE;
            GDALClose( hDstDS );
            
            CPLFree( pszDstFilename );
            //CSLDestroy( argv );
            CSLDestroy( papszSrcFiles );
            CSLDestroy( papszWarpOptions );
            CSLDestroy( papszTO );

            //GDALDumpOpenDatasets( stderr );

            GDALDestroyDriverManager();
            
        #ifdef OGR_ENABLED
            if( hCutline != NULL )
                OGR_G_DestroyGeometry( (OGRGeometryH) hCutline );
            OGRCleanupAll();
        #endif

            return (bHasGotErr) ? -1 : 1;



    }


    int 
    GdalWarper::GDALExit( int nCode )
    {
      const char  *pszDebug = CPLGetConfigOption("CPL_DEBUG",NULL);
      if( pszDebug && (EQUAL(pszDebug,"ON") || EQUAL(pszDebug,"") ) )
      {  
        GDALDumpOpenDatasets( stderr );
        CPLDumpSharedList( NULL );
      }

      GDALDestroyDriverManager();
      exit( nCode );
    }

    /* -------------------------------------------------------------------- */
    /*                      CheckExtensionConsistency()                     */
    /*                                                                      */
    /*      Check that the target file extension is consistant with the     */
    /*      requested driver. Actually, we only warn in cases where the     */
    /*      inconsistency is blatant (use of an extension declared by one   */
    /*      or several drivers, and not by the selected one)                */
    /* -------------------------------------------------------------------- */

    void 
    GdalWarper::CheckExtensionConsistency(const char* pszDestFilename,
                                   const char* pszDriverName)
    {

        char* pszDestExtension = CPLStrdup(CPLGetExtension(pszDestFilename));
        if (pszDestExtension[0] != '\0')
        {
            int nDriverCount = GDALGetDriverCount();
            CPLString osConflictingDriverList;
            for(int i=0;i<nDriverCount;i++)
            {
                GDALDriverH hDriver = GDALGetDriver(i);
                const char* pszDriverExtension = 
                    GDALGetMetadataItem( hDriver, GDAL_DMD_EXTENSION, NULL );   
                if (pszDriverExtension && EQUAL(pszDestExtension, pszDriverExtension))
                {
                    if (GDALGetDriverByName(pszDriverName) != hDriver)
                    {
                        if (osConflictingDriverList.size())
                            osConflictingDriverList += ", ";
                        osConflictingDriverList += GDALGetDriverShortName(hDriver);
                    }
                    else
                    {
                        /* If the request driver allows the used extension, then */
                        /* just stop iterating now */
                        osConflictingDriverList = "";
                        break;
                    }
                }
            }
            if (osConflictingDriverList.size())
            {
                fprintf(stderr,
                        "Warning: The target file has a '%s' extension, which is normally used by the %s driver%s,\n"
                        "but the requested output driver is %s. Is it really what you want ?\n",
                        pszDestExtension,
                        osConflictingDriverList.c_str(),
                        strchr(osConflictingDriverList.c_str(), ',') ? "s" : "",
                        pszDriverName);
            }
        }

        CPLFree(pszDestExtension);
    }

    GDALDatasetH 
    GdalWarper::GDALWarpCreateOutput( char **papszSrcFiles, const char *pszFilename, 
                          const char *pszFormat, char **papszTO, 
                          char ***ppapszCreateOptions, GDALDataType eDT,
                          void ** phTransformArg,
                          GDALDatasetH* phSrcDS)


    {
        GDALDriverH hDriver;
        GDALDatasetH hDstDS;
        void *hTransformArg;
        GDALColorTableH hCT = NULL;
        double dfWrkMinX=0, dfWrkMaxX=0, dfWrkMinY=0, dfWrkMaxY=0;
        double dfWrkResX=0, dfWrkResY=0;
        int nDstBandCount = 0;
        std::vector<GDALColorInterp> apeColorInterpretations;

        /* If (-ts and -te) or (-tr and -te) are specified, we don't need to compute the suggested output extent */
        int    bNeedsSuggestedWarpOutput = 
                      !( ((nForcePixels != 0 && nForceLines != 0) || (dfXRes != 0 && dfYRes != 0)) &&
                         !(dfMinX == 0.0 && dfMinY == 0.0 && dfMaxX == 0.0 && dfMaxY == 0.0) );

        *phTransformArg = NULL;
        *phSrcDS = NULL;

    /* -------------------------------------------------------------------- */
    /*      Find the output driver.                                         */
    /* -------------------------------------------------------------------- */
        hDriver = GDALGetDriverByName( pszFormat );
        if( hDriver == NULL 
            || GDALGetMetadataItem( hDriver, GDAL_DCAP_CREATE, NULL ) == NULL )
        {
            int    iDr;
            
            printf( "Output driver `%s' not recognised or does not support\n", 
                    pszFormat );
            printf( "direct output file creation.  The following format drivers are configured\n"
                    "and support direct output:\n" );

            for( iDr = 0; iDr < GDALGetDriverCount(); iDr++ )
            {
                GDALDriverH hDriver = GDALGetDriver(iDr);

                if( GDALGetMetadataItem( hDriver, GDAL_DCAP_CREATE, NULL) != NULL )
                {
                    printf( "  %s: %s\n",
                            GDALGetDriverShortName( hDriver  ),
                            GDALGetDriverLongName( hDriver ) );
                }
            }
            printf( "\n" );
            GDALExit( 1 );
        }

    /* -------------------------------------------------------------------- */
    /*      For virtual output files, we have to set a special subclass     */
    /*      of dataset to create.                                           */
    /* -------------------------------------------------------------------- */
        if( bVRT )
            *ppapszCreateOptions = 
                CSLSetNameValue( *ppapszCreateOptions, "SUBCLASS", 
                                 "VRTWarpedDataset" );

    /* -------------------------------------------------------------------- */
    /*      Loop over all input files to collect extents.                   */
    /* -------------------------------------------------------------------- */
        int     iSrc;
        char    *pszThisTargetSRS = (char*)CSLFetchNameValue( papszTO, "DST_SRS" );
        if( pszThisTargetSRS != NULL )
            pszThisTargetSRS = CPLStrdup( pszThisTargetSRS );

        for( iSrc = 0; papszSrcFiles[iSrc] != NULL; iSrc++ )
        {
            GDALDatasetH hSrcDS;
            const char *pszThisSourceSRS = CSLFetchNameValue(papszTO,"SRC_SRS");

            hSrcDS = GDALOpen( papszSrcFiles[iSrc], GA_ReadOnly );
            if( hSrcDS == NULL )
                GDALExit( 1 );

    /* -------------------------------------------------------------------- */
    /*      Check that there's at least one raster band                     */
    /* -------------------------------------------------------------------- */
            if ( GDALGetRasterCount(hSrcDS) == 0 )
            {
                fprintf(stderr, "Input file %s has no raster bands.\n", papszSrcFiles[iSrc] );
                GDALExit( 1 );
            }

            if( eDT == GDT_Unknown )
                eDT = GDALGetRasterDataType(GDALGetRasterBand(hSrcDS,1));

    /* -------------------------------------------------------------------- */
    /*      If we are processing the first file, and it has a color         */
    /*      table, then we will copy it to the destination file.            */
    /* -------------------------------------------------------------------- */
            if( iSrc == 0 )
            {
                nDstBandCount = GDALGetRasterCount(hSrcDS);
                hCT = GDALGetRasterColorTable( GDALGetRasterBand(hSrcDS,1) );
                if( hCT != NULL )
                {
                    hCT = GDALCloneColorTable( hCT );
                    if( !bQuiet )
                        printf( "Copying color table from %s to new file.\n", 
                                papszSrcFiles[iSrc] );
                }

                for(int iBand = 0; iBand < nDstBandCount; iBand++)
                {
                    apeColorInterpretations.push_back(
                        GDALGetRasterColorInterpretation(GDALGetRasterBand(hSrcDS,iBand+1)) );
                }
            }

    /* -------------------------------------------------------------------- */
    /*      Get the sourcesrs from the dataset, if not set already.         */
    /* -------------------------------------------------------------------- */
            if( pszThisSourceSRS == NULL )
            {
                const char *pszMethod = CSLFetchNameValue( papszTO, "METHOD" );

                if( GDALGetProjectionRef( hSrcDS ) != NULL 
                    && strlen(GDALGetProjectionRef( hSrcDS )) > 0
                    && (pszMethod == NULL || EQUAL(pszMethod,"GEOTRANSFORM")) )
                    pszThisSourceSRS = GDALGetProjectionRef( hSrcDS );
                
                else if( GDALGetGCPProjection( hSrcDS ) != NULL
                         && strlen(GDALGetGCPProjection(hSrcDS)) > 0 
                         && GDALGetGCPCount( hSrcDS ) > 1 
                         && (pszMethod == NULL || EQUALN(pszMethod,"GCP_",4)) )
                    pszThisSourceSRS = GDALGetGCPProjection( hSrcDS );
                else if( pszMethod != NULL && EQUAL(pszMethod,"RPC") )
                    pszThisSourceSRS = SRS_WKT_WGS84;
                else
                    pszThisSourceSRS = "";
            }

            if( pszThisTargetSRS == NULL )
                pszThisTargetSRS = CPLStrdup( pszThisSourceSRS );
            
    /* -------------------------------------------------------------------- */
    /*      Create a transformation object from the source to               */
    /*      destination coordinate system.                                  */
    /* -------------------------------------------------------------------- */
            hTransformArg = 
                GDALCreateGenImgProjTransformer2( hSrcDS, NULL, papszTO );
            
            if( hTransformArg == NULL )
            {
                CPLFree( pszThisTargetSRS );
                GDALClose( hSrcDS );
                return NULL;
            }
            
            GDALTransformerInfo* psInfo = (GDALTransformerInfo*)hTransformArg;

    /* -------------------------------------------------------------------- */
    /*      Get approximate output definition.                              */
    /* -------------------------------------------------------------------- */
            if( bNeedsSuggestedWarpOutput )
            {
                double adfThisGeoTransform[6];
                double adfExtent[4];
                int    nThisPixels, nThisLines;

                if ( GDALSuggestedWarpOutput2( hSrcDS, 
                                            psInfo->pfnTransform, hTransformArg, 
                                            adfThisGeoTransform, 
                                            &nThisPixels, &nThisLines, 
                                            adfExtent, 0 ) != CE_None )
                {
                    CPLFree( pszThisTargetSRS );
                    GDALClose( hSrcDS );
                    return NULL;
                }
                
                if ( CPLGetConfigOption( "CHECK_WITH_INVERT_PROJ", NULL ) == NULL )
                {
                    double MinX = adfExtent[0];
                    double MaxX = adfExtent[2];
                    double MaxY = adfExtent[3];
                    double MinY = adfExtent[1];
                    int bSuccess = TRUE;
                    
                    /* Check that the the edges of the target image are in the validity area */
                    /* of the target projection */
        #define N_STEPS 20
                    int i,j;
                    for(i=0;i<=N_STEPS && bSuccess;i++)
                    {
                        for(j=0;j<=N_STEPS && bSuccess;j++)
                        {
                            double dfRatioI = i * 1.0 / N_STEPS;
                            double dfRatioJ = j * 1.0 / N_STEPS;
                            double expected_x = (1 - dfRatioI) * MinX + dfRatioI * MaxX;
                            double expected_y = (1 - dfRatioJ) * MinY + dfRatioJ * MaxY;
                            double x = expected_x;
                            double y = expected_y;
                            double z = 0;
                            /* Target SRS coordinates to source image pixel coordinates */
                            if (!psInfo->pfnTransform(hTransformArg, TRUE, 1, &x, &y, &z, &bSuccess) || !bSuccess)
                                bSuccess = FALSE;
                            /* Source image pixel coordinates to target SRS coordinates */
                            if (!psInfo->pfnTransform(hTransformArg, FALSE, 1, &x, &y, &z, &bSuccess) || !bSuccess)
                                bSuccess = FALSE;
                            if (fabs(x - expected_x) > (MaxX - MinX) / nThisPixels ||
                                fabs(y - expected_y) > (MaxY - MinY) / nThisLines)
                                bSuccess = FALSE;
                        }
                    }
                    
                    /* If not, retry with CHECK_WITH_INVERT_PROJ=TRUE that forces ogrct.cpp */
                    /* to check the consistency of each requested projection result with the */
                    /* invert projection */
                    if (!bSuccess)
                    {
                        CPLSetConfigOption( "CHECK_WITH_INVERT_PROJ", "TRUE" );
                        CPLDebug("WARP", "Recompute out extent with CHECK_WITH_INVERT_PROJ=TRUE");

                        if( GDALSuggestedWarpOutput2( hSrcDS, 
                                            psInfo->pfnTransform, hTransformArg, 
                                            adfThisGeoTransform, 
                                            &nThisPixels, &nThisLines, 
                                            adfExtent, 0 ) != CE_None )
                        {
                            CPLFree( pszThisTargetSRS );
                            GDALClose( hSrcDS );
                            return NULL;
                        }
                    }
                }

        /* -------------------------------------------------------------------- */
        /*      Expand the working bounds to include this region, ensure the    */
        /*      working resolution is no more than this resolution.             */
        /* -------------------------------------------------------------------- */
                if( dfWrkMaxX == 0.0 && dfWrkMinX == 0.0 )
                {
                    dfWrkMinX = adfExtent[0];
                    dfWrkMaxX = adfExtent[2];
                    dfWrkMaxY = adfExtent[3];
                    dfWrkMinY = adfExtent[1];
                    dfWrkResX = adfThisGeoTransform[1];
                    dfWrkResY = ABS(adfThisGeoTransform[5]);
                }
                else
                {
                    dfWrkMinX = MIN(dfWrkMinX,adfExtent[0]);
                    dfWrkMaxX = MAX(dfWrkMaxX,adfExtent[2]);
                    dfWrkMaxY = MAX(dfWrkMaxY,adfExtent[3]);
                    dfWrkMinY = MIN(dfWrkMinY,adfExtent[1]);
                    dfWrkResX = MIN(dfWrkResX,adfThisGeoTransform[1]);
                    dfWrkResY = MIN(dfWrkResY,ABS(adfThisGeoTransform[5]));
                }
            }

            if (iSrc == 0 && papszSrcFiles[1] == NULL)
            {
                *phTransformArg = hTransformArg;
                *phSrcDS = hSrcDS;
            }
            else
            {
                GDALDestroyGenImgProjTransformer( hTransformArg );
                GDALClose( hSrcDS );
            }
        }

    /* -------------------------------------------------------------------- */
    /*      Did we have any usable sources?                                 */
    /* -------------------------------------------------------------------- */
        if( nDstBandCount == 0 )
        {
            CPLError( CE_Failure, CPLE_AppDefined,
                      "No usable source images." );
            CPLFree( pszThisTargetSRS );
            return NULL;
        }

    /* -------------------------------------------------------------------- */
    /*      Turn the suggested region into a geotransform and suggested     */
    /*      number of pixels and lines.                                     */
    /* -------------------------------------------------------------------- */
        double adfDstGeoTransform[6] = { 0, 0, 0, 0, 0, 0 };
        int nPixels = 0, nLines = 0;
        
        if( bNeedsSuggestedWarpOutput )
        {
            adfDstGeoTransform[0] = dfWrkMinX;
            adfDstGeoTransform[1] = dfWrkResX;
            adfDstGeoTransform[2] = 0.0;
            adfDstGeoTransform[3] = dfWrkMaxY;
            adfDstGeoTransform[4] = 0.0;
            adfDstGeoTransform[5] = -1 * dfWrkResY;

            nPixels = (int) ((dfWrkMaxX - dfWrkMinX) / dfWrkResX + 0.5);
            nLines = (int) ((dfWrkMaxY - dfWrkMinY) / dfWrkResY + 0.5);
        }

    /* -------------------------------------------------------------------- */
    /*      Did the user override some parameters?                          */
    /* -------------------------------------------------------------------- */
        if( dfXRes != 0.0 && dfYRes != 0.0 )
        {
            if( dfMinX == 0.0 && dfMinY == 0.0 && dfMaxX == 0.0 && dfMaxY == 0.0 )
            {
                dfMinX = adfDstGeoTransform[0];
                dfMaxX = adfDstGeoTransform[0] + adfDstGeoTransform[1] * nPixels;
                dfMaxY = adfDstGeoTransform[3];
                dfMinY = adfDstGeoTransform[3] + adfDstGeoTransform[5] * nLines;
            }
            
            if ( bTargetAlignedPixels )
            {
                dfMinX = floor(dfMinX / dfXRes) * dfXRes;
                dfMaxX = ceil(dfMaxX / dfXRes) * dfXRes;
                dfMinY = floor(dfMinY / dfYRes) * dfYRes;
                dfMaxY = ceil(dfMaxY / dfYRes) * dfYRes;
            }

            nPixels = (int) ((dfMaxX - dfMinX + (dfXRes/2.0)) / dfXRes);
            nLines = (int) ((dfMaxY - dfMinY + (dfYRes/2.0)) / dfYRes);
            adfDstGeoTransform[0] = dfMinX;
            adfDstGeoTransform[3] = dfMaxY;
            adfDstGeoTransform[1] = dfXRes;
            adfDstGeoTransform[5] = -dfYRes;
        }

        else if( nForcePixels != 0 && nForceLines != 0 )
        {
            if( dfMinX == 0.0 && dfMinY == 0.0 && dfMaxX == 0.0 && dfMaxY == 0.0 )
            {
                dfMinX = dfWrkMinX;
                dfMaxX = dfWrkMaxX;
                dfMaxY = dfWrkMaxY;
                dfMinY = dfWrkMinY;
            }

            dfXRes = (dfMaxX - dfMinX) / nForcePixels;
            dfYRes = (dfMaxY - dfMinY) / nForceLines;

            adfDstGeoTransform[0] = dfMinX;
            adfDstGeoTransform[3] = dfMaxY;
            adfDstGeoTransform[1] = dfXRes;
            adfDstGeoTransform[5] = -dfYRes;

            nPixels = nForcePixels;
            nLines = nForceLines;
        }

        else if( nForcePixels != 0 )
        {
            if( dfMinX == 0.0 && dfMinY == 0.0 && dfMaxX == 0.0 && dfMaxY == 0.0 )
            {
                dfMinX = dfWrkMinX;
                dfMaxX = dfWrkMaxX;
                dfMaxY = dfWrkMaxY;
                dfMinY = dfWrkMinY;
            }

            dfXRes = (dfMaxX - dfMinX) / nForcePixels;
            dfYRes = dfXRes;

            adfDstGeoTransform[0] = dfMinX;
            adfDstGeoTransform[3] = dfMaxY;
            adfDstGeoTransform[1] = dfXRes;
            adfDstGeoTransform[5] = -dfYRes;

            nPixels = nForcePixels;
            nLines = (int) ((dfMaxY - dfMinY + (dfYRes/2.0)) / dfYRes);
        }

        else if( nForceLines != 0 )
        {
            if( dfMinX == 0.0 && dfMinY == 0.0 && dfMaxX == 0.0 && dfMaxY == 0.0 )
            {
                dfMinX = dfWrkMinX;
                dfMaxX = dfWrkMaxX;
                dfMaxY = dfWrkMaxY;
                dfMinY = dfWrkMinY;
            }

            dfYRes = (dfMaxY - dfMinY) / nForceLines;
            dfXRes = dfYRes;

            adfDstGeoTransform[0] = dfMinX;
            adfDstGeoTransform[3] = dfMaxY;
            adfDstGeoTransform[1] = dfXRes;
            adfDstGeoTransform[5] = -dfYRes;

            nPixels = (int) ((dfMaxX - dfMinX + (dfXRes/2.0)) / dfXRes);
            nLines = nForceLines;
        }

        else if( dfMinX != 0.0 || dfMinY != 0.0 || dfMaxX != 0.0 || dfMaxY != 0.0 )
        {
            dfXRes = adfDstGeoTransform[1];
            dfYRes = fabs(adfDstGeoTransform[5]);

            nPixels = (int) ((dfMaxX - dfMinX + (dfXRes/2.0)) / dfXRes);
            nLines = (int) ((dfMaxY - dfMinY + (dfYRes/2.0)) / dfYRes);

            dfXRes = (dfMaxX - dfMinX) / nPixels;
            dfYRes = (dfMaxY - dfMinY) / nLines;

            adfDstGeoTransform[0] = dfMinX;
            adfDstGeoTransform[3] = dfMaxY;
            adfDstGeoTransform[1] = dfXRes;
            adfDstGeoTransform[5] = -dfYRes;
        }

    /* -------------------------------------------------------------------- */
    /*      Do we want to generate an alpha band in the output file?        */
    /* -------------------------------------------------------------------- */
        if( bEnableSrcAlpha )
            nDstBandCount--;

        if( bEnableDstAlpha )
            nDstBandCount++;

    /* -------------------------------------------------------------------- */
    /*      Create the output file.                                         */
    /* -------------------------------------------------------------------- */
        if( !bQuiet )
            printf( "Creating output file that is %dP x %dL.\n", nPixels, nLines );

        hDstDS = GDALCreate( hDriver, pszFilename, nPixels, nLines, 
                             nDstBandCount, eDT, *ppapszCreateOptions );
        
        if( hDstDS == NULL )
        {
            CPLFree( pszThisTargetSRS );
            return NULL;
        }

    /* -------------------------------------------------------------------- */
    /*      Write out the projection definition.                            */
    /* -------------------------------------------------------------------- */
        GDALSetProjection( hDstDS, pszThisTargetSRS );
        GDALSetGeoTransform( hDstDS, adfDstGeoTransform );

        if (*phTransformArg != NULL)
            GDALSetGenImgProjTransformerDstGeoTransform( *phTransformArg, adfDstGeoTransform);

    /* -------------------------------------------------------------------- */
    /*      Try to set color interpretation of source bands to target       */
    /*      dataset.                                                        */
    /*      FIXME? We should likely do that for other drivers than VRT      */
    /*      but it might create spurious .aux.xml files (at least with HFA, */
    /*      and netCDF)                                                     */
    /* -------------------------------------------------------------------- */
        if( bVRT )
        {
            int nBandsToCopy = (int)apeColorInterpretations.size();
            if ( bEnableSrcAlpha )
                nBandsToCopy --;
            for(int iBand = 0; iBand < nBandsToCopy; iBand++)
            {
                GDALSetRasterColorInterpretation(
                    GDALGetRasterBand( hDstDS, iBand + 1 ),
                    apeColorInterpretations[iBand] );
            }
        }
        
    /* -------------------------------------------------------------------- */
    /*      Try to set color interpretation of output file alpha band.      */
    /* -------------------------------------------------------------------- */
        if( bEnableDstAlpha )
        {
            GDALSetRasterColorInterpretation( 
                GDALGetRasterBand( hDstDS, nDstBandCount ), 
                GCI_AlphaBand );
        }

    /* -------------------------------------------------------------------- */
    /*      Copy the color table, if required.                              */
    /* -------------------------------------------------------------------- */
        if( hCT != NULL )
        {
            GDALSetRasterColorTable( GDALGetRasterBand(hDstDS,1), hCT );
            GDALDestroyColorTable( hCT );
        }

        CPLFree( pszThisTargetSRS );
        return hDstDS;
    }

    void 
    GdalWarper::RemoveConflictingMetadata( GDALMajorObjectH hObj, char **papszMetadata, 
                               const char *pszValueConflict )
    {
        if ( hObj == NULL ) return;

        char *pszKey = NULL; 
        const char *pszValueRef; 
        const char *pszValueComp; 
        char ** papszMetadataRef = CSLDuplicate( papszMetadata );
        int nCount = CSLCount( papszMetadataRef ); 

        for( int i = 0; i < nCount; i++ ) 
        { 
            pszValueRef = CPLParseNameValue( papszMetadataRef[i], &pszKey ); 
            pszValueComp = GDALGetMetadataItem( hObj, pszKey, NULL );
            if ( ( pszValueRef == NULL || pszValueComp == NULL ||
                   ! EQUAL( pszValueRef, pszValueComp ) ) &&
                 ! EQUAL( pszValueComp, pszValueConflict ) ) {
                GDALSetMetadataItem( hObj, pszKey, pszValueConflict, NULL ); 
            }
            CPLFree( pszKey ); 
        } 

        CSLDestroy( papszMetadataRef );
    }



}
