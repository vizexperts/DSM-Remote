#include <Stereo/Image.h>
#include <iostream>

namespace Stereo
{

    /**
    * Finds a raster band based on color interpretation 
    */
    static GDALRasterBand* findBandByColorInterp(GDALDataset *ds, GDALColorInterp colorInterp)
    {


        for (int i = 1; i <= ds->GetRasterCount(); ++i)
        {
            if (ds->GetRasterBand(i)->GetColorInterpretation() == colorInterp) return ds->GetRasterBand(i);
        }
        return 0;
    }

    Image::Image()
        : _osgImage(NULL),
        _isGeoImage(false),
        _imageDataset(NULL),
#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
      _maxPixelValue(0),
      _minPixelValue(0),
      _meanPixelValue(0),
      _standardDeviation(0),
      _scaleBy(1),

      _maxPixelValueForGrey(0),
      _minPixelValueForGrey(0),
      _meanPixelValueForGrey(0),
      _standardDeviationForGrey(0),
      _scaleByForGrey(1),
      _shiftByForGrey(0),

      _maxPixelValueForRed(0),
      _minPixelValueForRed(0),
      _meanPixelValueForRed(0),
      _standardDeviationForRed(0),
      _scaleByForRed(1),

      _maxPixelValueForBlue(0),
      _minPixelValueForBlue(0),
      _meanPixelValueForBlue(0),
      _standardDeviationForBlue(0),
      _scaleByForBlue(1),

      _maxPixelValueForGreen(0),
      _minPixelValueForGreen(0),
      _meanPixelValueForGreen(0),
      _standardDeviationForGreen(0),
      _scaleByForGreen(1),
      _sdScaleFactor(2.0),

      _hasNoDataValueGrey(0),
      _hasNoDataValueRed(0),
      _hasNoDataValueGreen(0),
      _hasNoDataValueBlue(0),
      _numOfBins(256),
      _bandRedLUT(NULL),
      _bandGreenLUT(NULL),
      _bandBlueLUT(NULL),
      _bandGreyLUT(NULL),
      _bandsLUT(NULL),
      _type(GDT_Byte),
      _enhancementType(MIN_MAX),
#endif
        _noOfBands(0)
    {

    }
    
    Image::~Image()
    {

    }

    void
    Image::setName(std::string name)
    {
        _name = name;

    }
    
    std::string 
    Image::getName()
    {
        return _name;

    }

    void
    Image::setUrl(std::string url)
    {
        _url = url;
    }
    std::string 
    Image::getUrl()
    {
        return _url;

    }


    void 
    Image::setGeoExtents(double* extents)
    {
        memcpy(_geoExtents,extents,sizeof(double) * 6);

    }
    
    double* 
    Image::getGeoExtents()
    {
        return _geoExtents;

    }
    
    void 
    Image::setBounds(double minX, double minY, double maxX, double maxY)
    {
        _minX = minX;
        _minY = minY;
        _maxX = maxX;
        _maxY = maxY;

    }

    void 
    Image::getBounds(double& minX, double& minY, double& maxX, double& maxY)
    {
        minX = _minX;
        minY = _minY;
        maxX = _maxX;
        maxY = _maxY;

    }

    void 
    Image::setOsgImage(osg::Image* image)
    {
        _osgImage = image;

    }
    osg::Image* 
    Image::getOsgImage()
    {
        return _osgImage.get();
    }

    void
    Image::setWidth(unsigned int width)
    {
        _width = width;

    }
    
    unsigned int
    Image::getWidth()
    {
        return _width;

    }

    void 
    Image::setHeight(unsigned int height)
    {
        _height = height;
    }
    
    unsigned int 
    Image::getHeight()
    {
        return _height;
    }

    bool
    Image::isGeoImage()
    {
        return _isGeoImage;
    }

    void  
    Image::isGeoImage(bool value)
    {
        _isGeoImage = value;
    }

    GDALDataset* 
    Image::getGdalDataset()
    {
        return _imageDataset;

    }
            
    void 
    Image::setGdalDataSet(GDALDataset* dataset)
    {
        _imageDataset = dataset;

    }
    
    Image::EnhancementType 
    Image::getEnhancementType()
    {
        return _enhancementType;
    }
    
    void
    Image::setEnhancementType(EnhancementType enhancementType)
    {
        _enhancementType = enhancementType;
    }

    bool 
    Image::loadImage(std::string url)
    {
        GDALAllRegister();

        _url = url;

        _imageDataset = (GDALDataset*)GDALOpen(url.c_str(), GA_ReadOnly ); 

        if (!_imageDataset)
        {
            std::cout << "Failed to load gdal dataset" << std::endl;
            return false;
        }

        _noOfBands = _imageDataset->GetRasterCount();

        _width = _imageDataset->GetRasterXSize();
        _height = _imageDataset->GetRasterYSize();

        _imageDataset->GetGeoTransform(_geoExtents);

        std::string proj = _imageDataset->GetProjectionRef();
        if(proj == "")
        {
            _minX = 0;
            _minY = 0;
            _maxX = _width;
            _maxY = _height;
            _geoExtents[3] = _height;
            _geoExtents[5] = -1;
            _isGeoImage = false;

        }
        else
        {
            _minX = _geoExtents[0];
            _maxY = _geoExtents[3];
            _maxX = _minX + _width *  _geoExtents[1];
            _minY = _maxY + _height * _geoExtents[5];

            _isGeoImage = true;
        }

#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
        ////Implemented by Raghavendra to add support for 16 bit and 32 bit images

           // _enhancementType = _options.enhancementType().value();

        if(_enhancementType == HISTOGRAM_EQUALIZATION)
            {

                double             *padfScaleMin = NULL;
                double             *padfScaleMax = NULL;

                GDALProgressFunc    pfnProgress = GDALTermProgress;
                ComputeEqualizationLUTs( _imageDataset, _numOfBins, 
                                         &padfScaleMin, &padfScaleMax, 
                                         &_bandsLUT, pfnProgress );

                
                if(_imageDataset->GetRasterCount() >= 3)
                {
                    _bandRedLUT = _bandsLUT[0];
                    _bandGreenLUT = _bandsLUT[1];
                    _bandBlueLUT = _bandsLUT[2];
                }
                else if(_imageDataset->GetRasterCount() == 1)
                {
                    _bandGreyLUT = _bandsLUT[0];
                }


            }

            GDALRasterBand* bandRed = findBandByColorInterp(_imageDataset, GCI_RedBand);
            GDALRasterBand* bandGreen = findBandByColorInterp(_imageDataset, GCI_GreenBand);
            GDALRasterBand* bandBlue = findBandByColorInterp(_imageDataset, GCI_BlueBand);
            GDALRasterBand* bandAlpha = findBandByColorInterp(_imageDataset, GCI_AlphaBand);

            GDALRasterBand* bandGray = findBandByColorInterp(_imageDataset, GCI_GrayIndex);

            GDALRasterBand* bandPalette = findBandByColorInterp(_imageDataset, GCI_PaletteIndex);

            if (!bandRed && !bandGreen && !bandBlue && !bandAlpha && !bandGray && !bandPalette)
            {
                //We couldn't find any valid bands based on the color interp, so just make an educated guess based on the number of bands in the file
                //RGB = 3 bands
                if (_imageDataset->GetRasterCount() == 3)
                {
                    bandRed   = _imageDataset->GetRasterBand( 1 );
                    bandGreen = _imageDataset->GetRasterBand( 2 );
                    bandBlue  = _imageDataset->GetRasterBand( 3 );
                }
                //RGBA = 4 bands
                else if (_imageDataset->GetRasterCount() == 4)
                {
                    bandRed   = _imageDataset->GetRasterBand( 1 );
                    bandGreen = _imageDataset->GetRasterBand( 2 );
                    bandBlue  = _imageDataset->GetRasterBand( 3 );
                    bandAlpha = _imageDataset->GetRasterBand( 4 );
                }
                //Gray = 1 band
                else if (_imageDataset->GetRasterCount() == 1)
                {
                    bandGray = _imageDataset->GetRasterBand( 1 );
                }
                //Gray + alpha = 2 bands
                else if (_imageDataset->GetRasterCount() == 2)
                {
                    bandGray  = _imageDataset->GetRasterBand( 1 );
                    bandAlpha = _imageDataset->GetRasterBand( 2 );
                }
            }

        if(bandGray)
        {
            GDALDataType type = _imageDataset->GetRasterBand(1)->GetRasterDataType();
            double pdfMin;
            double pdfMax;
            double pdfMean;
            double padfStdDev;

            _noDataValueGrey = bandGray->GetNoDataValue(&_hasNoDataValueGrey);
            if(!_hasNoDataValueGrey)
            {
                //_noDataValueGrey = getNoDataValue();
            }

            if(_noDataValueGrey != (float)SHRT_MIN)
            {

                _hasNoDataValueGrey = 1;
                bandGray->SetNoDataValue(_noDataValueGrey);
            }
            bandGray->GetStatistics (false, true,&pdfMin,&pdfMax,&pdfMean,&padfStdDev );

            _minPixelValueForGrey = pdfMin;
            _maxPixelValueForGrey = pdfMax;
            _meanPixelValueForGrey = pdfMean;
            _standardDeviationForGrey = padfStdDev;

            _scaleByForGrey = _maxPixelValueForGrey;
            _shiftByForGrey = _minPixelValueForGrey;

            _type = type;
        }


        if(bandRed && bandGreen && bandBlue)
        {
            GDALDataType type = _imageDataset->GetRasterBand(1)->GetRasterDataType();
            double pdfMinRed;
            double pdfMaxRed;
            double pdfMeanRed;
            double padfStdDevRed;

            _noDataValueRed = bandRed->GetNoDataValue(&_hasNoDataValueRed);
            if(!_hasNoDataValueRed)
            {
                //_noDataValueRed = getNoDataValue();
            }

            if(_noDataValueRed != (float)SHRT_MIN)
            {
                _hasNoDataValueRed = 1;
                bandRed->SetNoDataValue(_noDataValueRed);
            }

            bandRed->GetStatistics (false, true,&pdfMinRed,&pdfMaxRed,&pdfMeanRed,&padfStdDevRed );

            _type = type;
            _minPixelValueForRed = pdfMinRed;
            _maxPixelValueForRed = pdfMaxRed;
            _meanPixelValueForRed = pdfMeanRed;
            _standardDeviationForRed = padfStdDevRed;

            _scaleByForRed = _maxPixelValueForRed;

            

            double pdfMinBlue;
            double pdfMaxBlue;
            double pdfMeanBlue;
            double padfStdDevBlue;

            _noDataValueBlue = bandBlue->GetNoDataValue(&_hasNoDataValueBlue);
            if(!_hasNoDataValueBlue)
            {
                //_noDataValueBlue = getNoDataValue();
            }

            if(_noDataValueBlue != (float)SHRT_MIN)
            {
                _hasNoDataValueBlue = 1;
                bandBlue->SetNoDataValue(_noDataValueBlue);
            }

            bandBlue->GetStatistics (true, true,&pdfMinBlue,&pdfMaxBlue,&pdfMeanBlue,&padfStdDevBlue );

            _minPixelValueForBlue = pdfMinBlue;
            _maxPixelValueForBlue = pdfMaxBlue;
            _meanPixelValueForBlue = pdfMeanBlue;
            _standardDeviationForBlue = padfStdDevBlue;

            _scaleByForBlue = _maxPixelValueForBlue;

                        

            double pdfMinGreen;
            double pdfMaxGreen;
            double pdfMeanGreen;
            double padfStdDevGreen;

            _noDataValueGreen = bandGreen->GetNoDataValue(&_hasNoDataValueGreen);
            if(!_hasNoDataValueGreen)
            {
            //    _noDataValueGreen = getNoDataValue();
            }

            if(_noDataValueGreen != (float)SHRT_MIN)
            {
                _hasNoDataValueGreen = 1;
                bandGreen->SetNoDataValue(_noDataValueGreen);
            }
            bandGreen->GetStatistics (true, true,&pdfMinGreen,&pdfMaxGreen,&pdfMeanGreen,&padfStdDevGreen );

            _minPixelValueForGreen = pdfMinGreen;
            _maxPixelValueForGreen = pdfMaxGreen;
            _meanPixelValueForGreen = pdfMeanGreen;
            _standardDeviationForGreen = padfStdDevGreen;

            _scaleByForGreen = _maxPixelValueForGreen; 

                
        }


#endif

        return true;

    }

    int
    Image::ComputeEqualizationLUTs( GDALDatasetH hDataset, int nLUTBins,
                         double **ppadfScaleMin, double **ppadfScaleMax, 
                         int ***ppapanLUTs, 
                         GDALProgressFunc pfnProgress )

{
    int iBand;
    int nBandCount = GDALGetRasterCount(hDataset);
    int nHistSize = 0;
    int *panHistogram = NULL;

    // For now we always compute min/max
    *ppadfScaleMin = (double *) CPLCalloc(sizeof(double),nBandCount);
    *ppadfScaleMax = (double *) CPLCalloc(sizeof(double),nBandCount);

    *ppapanLUTs = (int **) CPLCalloc(sizeof(int *),nBandCount);

/* ==================================================================== */
/*      Process all bands.                                              */
/* ==================================================================== */
    for( iBand = 0; iBand < nBandCount; iBand++ )
    {
        GDALRasterBandH hBand = GDALGetRasterBand( hDataset, iBand+1 );
        CPLErr eErr;

/* -------------------------------------------------------------------- */
/*      Get a reasonable histogram.                                     */
/* -------------------------------------------------------------------- */
        eErr =
            GDALGetDefaultHistogram( hBand, 
                                     *ppadfScaleMin + iBand,
                                     *ppadfScaleMax + iBand,
                                     &nHistSize, &panHistogram, 
                                     TRUE, pfnProgress, NULL );

        if( eErr != CE_None )
            return FALSE;

        panHistogram[0] = 0; // zero out extremes (nodata, etc)
        panHistogram[nHistSize-1] = 0;

/* -------------------------------------------------------------------- */
/*      Total histogram count, and build cumulative histogram.          */
/*      We take care to use big integers as there may be more than 4    */
/*      Gigapixels.                                                     */
/* -------------------------------------------------------------------- */
        GIntBig *panCumHist = (GIntBig *) CPLCalloc(sizeof(GIntBig),nHistSize);
        GIntBig nTotal = 0;
        int iHist;

        for( iHist = 0; iHist < nHistSize; iHist++ )
        {
            panCumHist[iHist] = nTotal + panHistogram[iHist]/2;
            nTotal += panHistogram[iHist];
        }

        CPLFree( panHistogram );

        if( nTotal == 0 )
        {
            CPLError( CE_Warning, CPLE_AppDefined, 
                      "Zero value entries in histogram, results will not be meaningful." );
            nTotal = 1;
        }

/* -------------------------------------------------------------------- */
/*      Now compute a LUT from the cumulative histogram.                */
/* -------------------------------------------------------------------- */
        int *panLUT = (int *) CPLCalloc(sizeof(int),nLUTBins);
        int iLUT;

        for( iLUT = 0; iLUT < nLUTBins; iLUT++ )
        {
            iHist = (iLUT * nHistSize) / nLUTBins;
            int nValue = (int) ((panCumHist[iHist] * nLUTBins) / nTotal);

            panLUT[iLUT] = MAX(0,MIN(nLUTBins-1,nValue));

        } 

        (*ppapanLUTs)[iBand] = panLUT;
    }

    return TRUE;
}

    void 
    Image::getExtentsForTile(const TileKey& key, double& minX, double& minY, double& maxX, double& maxY)
    {
        double tileExtentWidth = (_maxX - _minX)/pow(2.0,double(key.getLOD()));
        double tileExtentHeight = (_maxY - _minY)/pow(2.0,double(key.getLOD()));

        minX = _minX + double(key.getTileX()) * tileExtentWidth;
        maxX = _minX + double(key.getTileX()+1) * tileExtentWidth;
        minY = _minY + double(key.getTileY()) * tileExtentHeight;
        maxY = _minY + double(key.getTileY()+1) * tileExtentHeight;

    }

    bool 
    Image::isRight()
    {
        return _isRight;
    }

    void 
    Image::isRight(bool value)
    {
        _isRight = value;
    }

#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
    void 
    Image::setGreyScaleValue(double scaleBy)
    {
        _scaleBy = scaleBy;
    }

    double 
    Image::getGreyScaleValue()
    {
        return _scaleBy;
    }

    void 
    Image::setRedScaleValue(double scaleBy)
    {
        _scaleByForRed = scaleBy;
    }

    double 
    Image::getRedScaleValue()
    {

        return _scaleByForRed;
    }

    void 
    Image::setGreenScaleValue(double scaleBy)
    {
        _scaleByForGreen = scaleBy;
    }

    double 
    Image::getGreenScaleValue()
    {
        return _scaleByForGreen;
    }

    void 
    Image::setBlueScaleValue(double scaleBy)
    {
        _scaleByForBlue;        
    }

    double 
    Image::getBlueScaleValue()
    {
        return _scaleByForBlue;

    }

    GDALDataType 
    Image::getRasterType()
    {

        return _type;
    }
#endif

#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE

    double Image::enhanceGreyBandPixel(double pixelValue, EnhancementType enhanceType)
    {

        if(enhanceType == NONE)
        {
            if(_hasNoDataValueGrey && pixelValue == _noDataValueGrey)
            {

                return pixelValue;
            }
            else
            {
                
                double enhancedPixel = pixelValue;

                if(_type == GDT_UInt16)
                {
                    enhancedPixel = (pixelValue/pow(2.0,16.0)) * 255.0;
                }
                else if(_type == GDT_UInt32)
                {
                    enhancedPixel = (pixelValue/pow(2.0,32.0)) * 255.0;
                }
                if(floor(enhancedPixel) <= 0)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 0)
                    {
                        enhancedPixel = 1.0;
                    }
                    else
                    {
                        enhancedPixel = 0.0;
                    }
                }

                if(floor(enhancedPixel) >= 255)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 255)
                    {
                        enhancedPixel = 254.0;
                    }
                    else
                    {
                        enhancedPixel = 255.0;
                    }
                }

                return enhancedPixel;
            }
        }
        else if(enhanceType == MIN_MAX)
        {
            if(_hasNoDataValueGrey && pixelValue == _noDataValueGrey)
            {

                return pixelValue;
            }
            else
            {
                
                double enhancedPixel = 255.0 + ((pixelValue - _maxPixelValueForGrey)/(_maxPixelValueForGrey - _minPixelValueForGrey)) * 255.0;
                if(floor(enhancedPixel) <= 0)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 0)
                    {
                        enhancedPixel = 1.0;
                    }
                    else
                    {
                        enhancedPixel = 0.0;
                    }
                }

                if(floor(enhancedPixel) >= 255)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 255)
                    {
                        enhancedPixel = 254.0;
                    }
                    else
                    {
                        enhancedPixel = 255.0;
                    }
                }
                return enhancedPixel;
            }

        }
        else if(enhanceType == STANDARD_DEVIATION)
        {
            if(_hasNoDataValueGrey && pixelValue == _noDataValueGrey)
            {

                return pixelValue;
            }
            else
            {
                double minValue = _meanPixelValueForGrey - _standardDeviationForGrey * _sdScaleFactor;
                if(minValue < 0)
                {
                    minValue = 0;
                }

                double maxValue = _meanPixelValueForGrey + _standardDeviationForGrey * _sdScaleFactor;
                double defaultMax = 255.0;
                if(_type == GDT_UInt16)
                {
                    defaultMax = pow(2.0,16.0);
                }
                else if(_type == GDT_UInt32)
                {
                    defaultMax = pow(2.0,32.0);
                }
                if(maxValue > defaultMax)
                {
                    maxValue = defaultMax;
                }

                double enhancedPixel = 255.0 + ((pixelValue - maxValue)/(maxValue - minValue)) * 255.0;

                if(floor(enhancedPixel) <= 0)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 0)
                    {
                        enhancedPixel = 1.0;
                    }
                    else
                    {
                        enhancedPixel = 0.0;
                    }
                }

                if(floor(enhancedPixel) >= 255)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 255)
                    {
                        enhancedPixel = 254.0;
                    }
                    else
                    {
                        enhancedPixel = 255.0;
                    }
                }

                return enhancedPixel;
            }

        }
        else if(enhanceType == HISTOGRAM_EQUALIZATION)
        {
            if(_hasNoDataValueGrey && pixelValue == _noDataValueGrey)
            {

                return pixelValue;
            }
            else
            {
                double enhancedPixel;
                int index;
                double min,max;
                double halfBin = (_maxPixelValueForGrey - _minPixelValueForGrey) /(2 * _numOfBins);
                min = _minPixelValueForGrey- halfBin;
                max = _maxPixelValueForGrey + halfBin;

                double scale = _numOfBins / (max - min);

                index = (int) floor((pixelValue - min) * scale);

                if(index < 0)
                {
                    index = 0;
                }

                if(index >= _numOfBins)
                {
                    index = _numOfBins -1;
                }

                if(_bandGreyLUT)
                {
                     enhancedPixel = _bandGreyLUT[index];
                }
                else
                {
                    enhancedPixel = 0;
                }

                if(floor(enhancedPixel) <= 0)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 0)
                    {
                        enhancedPixel = 1.0;
                    }
                    else
                    {
                        enhancedPixel = 0.0;
                    }
                }

                if(floor(enhancedPixel) >= 255)
                {
                    if(_hasNoDataValueGrey && _noDataValueGrey == 255)
                    {
                        enhancedPixel = 254.0;
                    }
                    else
                    {
                        enhancedPixel = 255.0;
                    }
                }
                return enhancedPixel;
            }
        }
    }

    double Image::enhanceRedBandPixel(double pixelValue, EnhancementType enhanceType)
    {

        if(enhanceType == NONE)
        {
            double enhancedPixel = pixelValue;

            if(_type == GDT_UInt16)
            {
                enhancedPixel = (pixelValue/pow(2.0,16.0)) * 255.0;
            }
            else if(_type == GDT_UInt32)
            {
                enhancedPixel = (pixelValue/pow(2.0,32.0)) * 255.0;
            }
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }

            return enhancedPixel;
        }
        else if(enhanceType == MIN_MAX)
        {
            double enhancedPixel = 255.0 + ((pixelValue - _maxPixelValueForRed)/(_maxPixelValueForRed - _minPixelValueForRed)) * 255.0;
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;

        }
        else if(enhanceType == STANDARD_DEVIATION)
        {
            double minValue = _meanPixelValueForRed - _standardDeviationForRed * _sdScaleFactor;
            if(minValue < 0)
            {
                minValue = 0;
            }
            double maxValue = _meanPixelValueForRed + _standardDeviationForRed * _sdScaleFactor;
            double defaultMax = 255.0;
            if(_type == GDT_UInt16)
            {
                defaultMax = pow(2.0,16.0);
            }
            else if(_type == GDT_UInt32)
            {
                defaultMax = pow(2.0,32.0);
            }

            if(maxValue > defaultMax)
            {
                maxValue = defaultMax;
            }
            double enhancedPixel = 255.0 + ((pixelValue - maxValue)/(maxValue - minValue)) * 255.0;
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;

        }
        else if(enhanceType == HISTOGRAM_EQUALIZATION)
        {
            double enhancedPixel;
            int index;
            double min,max;
            double halfBin = (_maxPixelValueForRed - _minPixelValueForRed) /(2 * _numOfBins);
            min = _minPixelValueForRed - halfBin;
            max = _maxPixelValueForRed + halfBin;

            double scale = _numOfBins / (max - min);

            index = (int) floor((pixelValue - min) * scale);

            if(index < 0)
            {
                index = 0;
            }

            if(index >= _numOfBins)
            {
                index = _numOfBins -1;
            }

            if(_bandRedLUT)
            {
                enhancedPixel = _bandRedLUT[index];
            }
            else
            {
                enhancedPixel = 0;
            }
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueRed && _noDataValueRed == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }

            return enhancedPixel;
        }
    }

    double Image::enhanceGreenBandPixel(double pixelValue, EnhancementType enhanceType)
    {

        if(enhanceType == NONE)
        {
            double enhancedPixel = pixelValue;

            if(_type == GDT_UInt16)
            {
                enhancedPixel = (pixelValue/pow(2.0,16.0)) * 255.0;
            }
            else if(_type == GDT_UInt32)
            {
                enhancedPixel = (pixelValue/pow(2.0,32.0)) * 255.0;
            }
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }

            return enhancedPixel;
        }
        else if(enhanceType == MIN_MAX)
        {
            double enhancedPixel = 255.0 + ((pixelValue - _maxPixelValueForGreen)/(_maxPixelValueForGreen - _minPixelValueForGreen)) * 255.0;
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;

        }
        else if(enhanceType == STANDARD_DEVIATION)
        {
            double minValue = _meanPixelValueForGreen - _standardDeviationForGreen * _sdScaleFactor;
            if(minValue < 0)
            {
                minValue = 0;
            }

            double maxValue = _meanPixelValueForGreen + _standardDeviationForGreen * _sdScaleFactor;
            double defaultMax = 255.0;
            if(_type == GDT_UInt16)
            {
                defaultMax = pow(2.0,16.0);
            }
            else if(_type == GDT_UInt32)
            {
                defaultMax = pow(2.0,32.0);
            }

            if(maxValue > defaultMax)
            {
                maxValue = defaultMax;
            }

            double enhancedPixel = 255.0 + ((pixelValue - maxValue)/(maxValue - minValue)) * 255.0;
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;

        }
        else if(enhanceType == HISTOGRAM_EQUALIZATION)
        {
            double enhancedPixel;
            int index;
            double min,max;
            double halfBin = (_maxPixelValueForGreen - _minPixelValueForGreen) /(2 * _numOfBins);
            min = _minPixelValueForGreen - halfBin;
            max = _maxPixelValueForGreen + halfBin;

            double scale = _numOfBins / (max - min);

            index = (int) floor((pixelValue - min) * scale);

            if(index < 0)
            {
                index = 0;
            }

            if(index >= _numOfBins)
            {
                index = _numOfBins -1;
            }

            if(_bandGreenLUT)
            {
                enhancedPixel = _bandGreenLUT[index];
            }
            else
            {
                enhancedPixel = 0;
            }
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueGreen && _noDataValueGreen == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }

            return enhancedPixel;
        }
    }

    double Image::enhanceBlueBandPixel(double pixelValue, EnhancementType enhanceType)
    {

        if(enhanceType == NONE)
        {
            double enhancedPixel = pixelValue;

            if(_type == GDT_UInt16)
            {
                enhancedPixel = (pixelValue/pow(2.0,16.0)) * 255.0;
            }
            else if(_type == GDT_UInt32)
            {
                enhancedPixel = (pixelValue/pow(2.0,32.0)) * 255.0;
            }
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;
        }
        else if(enhanceType == MIN_MAX)
        {
            double enhancedPixel = 255.0 + ((pixelValue - _maxPixelValueForBlue)/(_maxPixelValueForBlue - _minPixelValueForBlue)) * 255.0;
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;

        }
        else if(enhanceType == STANDARD_DEVIATION)
        {
            double minValue = _meanPixelValueForBlue - _standardDeviationForBlue * _sdScaleFactor;
            if(minValue < 0)
            {
                minValue = 0;
            }
            double maxValue = _meanPixelValueForBlue + _standardDeviationForBlue * _sdScaleFactor;
            double defaultMax = 255.0;
            if(_type == GDT_UInt16)
            {
                defaultMax = pow(2.0,16.0);
            }
            else if(_type == GDT_UInt32)
            {
                defaultMax = pow(2.0,32.0);
            }

            if(maxValue > defaultMax)
            {
                maxValue = defaultMax;
            }
            double enhancedPixel = 255.0 + ((pixelValue - maxValue)/(maxValue - minValue)) * 255.0;
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;

        }
        else if(enhanceType == HISTOGRAM_EQUALIZATION)
        {
            double enhancedPixel;
            int index;
            double min,max;
            double halfBin = (_maxPixelValueForBlue - _minPixelValueForBlue) /(2 * _numOfBins);
            min = _minPixelValueForBlue - halfBin;
            max = _maxPixelValueForBlue + halfBin;

            double scale = _numOfBins / (max - min);

            index = (int) floor((pixelValue - min) * scale);

            if(index < 0)
            {
                index = 0;
            }

            if(index >= _numOfBins)
            {
                index = _numOfBins -1;
            }

            if(_bandBlueLUT)
            {
                enhancedPixel = _bandBlueLUT[index];
            }
            else
            {
                enhancedPixel = 0;
            }
            if(floor(enhancedPixel) <= 0)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 0)
                {
                    enhancedPixel = 1.0;
                }
                else
                {
                    enhancedPixel = 0.0;
                }
            }

            if(floor(enhancedPixel) >= 255)
            {
                if(_hasNoDataValueBlue && _noDataValueBlue == 255)
                {
                    enhancedPixel = 254.0;
                }
                else
                {
                    enhancedPixel = 255.0;
                }
            }
            return enhancedPixel;
        }
    }
#endif
}
