#include <Stereo/TileKey.h>


namespace Stereo
{
    TileKey TileKey::INVALID( 0, 0, 0);

    TileKey::TileKey( unsigned int lod, unsigned int tile_x, unsigned int tile_y)
    {
        _x = tile_x;
        _y = tile_y;
        _lod = lod;
    }

    TileKey::TileKey( const TileKey& rhs ) :
        _key( rhs._key ),
        _lod(rhs._lod),
        _x(rhs._x),
        _y(rhs._y)
    {

    }

    void
    TileKey::getTileXY(unsigned int& out_tile_x,
                       unsigned int& out_tile_y) const
    {
        out_tile_x = _x;
        out_tile_y = _y;
    }

    void
    TileKey::getPixelExtents(unsigned int& xmin,
                             unsigned int& ymin,
                             unsigned int& xmax,
                             unsigned int& ymax,
                             const unsigned int &tile_size) const
    {
        xmin = _x * tile_size;
        ymin = _y * tile_size;
        xmax = xmin + tile_size;
        ymax = ymin + tile_size; 
    }

    TileKey
    TileKey::createChildKey( unsigned int quadrant ) const
    {
        unsigned int lod = _lod + 1;
        unsigned int x = _x * 2;
        unsigned int y = _y * 2;

        if (quadrant == 1)
        {
            x+=1;
        }
        else if (quadrant == 2)
        {
            y+=1;
        }
        else if (quadrant == 3)
        {
            x+=1;
            y+=1;
        }
        return TileKey( lod, x, y);
    }

    TileKey
    TileKey::createParentKey() const
    {
        if (_lod == 0) return TileKey::INVALID;

        unsigned int lod = _lod - 1;
        unsigned int x = _x / 2;
        unsigned int y = _y / 2;
        return TileKey( lod, x, y);
    }

    TileKey
    TileKey::createNeighborKey( int xoffset, int yoffset ) const
    {
        //unsigned tx, ty;
        //getProfile()->getNumTiles( _lod, tx, ty );

        //int sx = (int)_x + xoffset;
        //unsigned x =
        //    sx < 0        ? (unsigned)((int)tx + sx) :
        //    sx >= (int)tx ? (unsigned)sx - tx :
        //    (unsigned)sx;

        //int sy = (int)_y + yoffset;
        //unsigned y =
        //    sy < 0        ? (unsigned)((int)ty + sy) :
        //    sy >= (int)ty ? (unsigned)sy - ty :
        //    (unsigned)sy;

        ////OE_NOTICE << "Returning neighbor " << x << ", " << y << " for tile " << str() << " offset=" << xoffset << ", " << yoffset << std::endl;

        return TileKey( 0,0,0 );
    }


}
