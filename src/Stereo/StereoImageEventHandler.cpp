
#include<Stereo/StereoImageEventHandler.h>
#include <osg/ImageStream>
#include <osg/Geode>
#include <osg/Switch>

namespace Stereo
{
	StereoImageEventHandler::StereoImageEventHandler():
		_switch(0),
		_texmatImage(0),
		_firstTraversal(true),
		_activeSlide(0),
		_previousTime(-1.0f),
		_timePerSlide(5.0),
		_autoSteppingActive(false),
		_scale(1.0),
		_px(0.5),
		_py(0.5)
	{
	}

	void 
	StereoImageEventHandler::set(osg::Node* sw, float offsetX, float offsetY, osg::TexMat* texmatImage, float timePerSlide, bool autoSteppingActive)
	{
		_switch = sw;
		_switch->setUpdateCallback(this);

		_texmatImage = texmatImage;

		_timePerSlide = timePerSlide;
		_autoSteppingActive = autoSteppingActive;    

		initTexMatrices();

	}


	bool 
	StereoImageEventHandler::handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter&)
	{
		switch(ea.getEventType())
		{
			case(osgGA::GUIEventAdapter::KEYDOWN):
			{
				if ((ea.getKey()=='w') || (ea.getKey()==osgGA::GUIEventAdapter::KEY_Up)) 
				{
					scaleImage(0.95f);
					return true;
				}
				else if ((ea.getKey()=='s') || (ea.getKey()==osgGA::GUIEventAdapter::KEY_Down)) 
				{
					scaleImage(1.05f);
					return true;
				}
				else if((ea.getKey()=='d') || (ea.getKey()==osgGA::GUIEventAdapter::KEY_Right))
				{

					float dx = 0.05;
					float dy = 0.0;
		            
					_px += dx;
					_py += dy;
		            
					translateImage(dx,dy);            
					return true;
				}
				else if((ea.getKey()=='a') || (ea.getKey()==osgGA::GUIEventAdapter::KEY_Left))
				{

					float dx = -0.05;
					float dy = 0.0;
		            
					_px += dx;
					_py += dy;
		            
					translateImage(dx,dy);            
					return true;
				}
				else if (ea.getKey()==' ')
				{
					initTexMatrices();
					return true;
				}
				return false;
			}
			case(osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON):
			{
				_px = ea.getXnormalized();
				_py = ea.getYnormalized();
			}
			case(osgGA::GUIEventAdapter::DRAG):
			{
	            
				float dx = ea.getXnormalized()-_px;
				float dy = ea.getYnormalized()-_py;
	            
				_px = ea.getXnormalized();
				_py = ea.getYnormalized();
	            
				translateImage(dx,dy);            
				return true;
			}
			//case(osgGA::GUIEventAdapter::SCROLL):
			//{
			//	if(ea.getScrollingMotion() == osgGA::GUIEventAdapter::SCROLL_DOWN)
			//	{
			//			scaleImage(0.95f);
			//			return true;
			//	}
			//	else if(ea.getScrollingMotion() == osgGA::GUIEventAdapter::SCROLL_UP)
			//	{
			//			scaleImage(1.05f);
			//			return true;
			//	}
			//	return false;

			//}
			//case(osgGA::GUIEventAdapter::SCROLL_DOWN):
			//{
			//			scaleImage(1.01f);
			//			return true;
			//}

			default:
				return false;
		}

	}

	void 
	StereoImageEventHandler::scaleImage(float s)
	{
		_scale = s;
		_texmatImage->setMatrix(_texmatImage->getMatrix()*osg::Matrix::translate(-0.5f,-0.5f,0.0f)*osg::Matrix::scale(s,s,1.0f)*osg::Matrix::translate(0.5f,0.5f,0.0f));
	}

	void 
	StereoImageEventHandler::translateImage(float rx,float ry)
	{
		const float scale = _scale;
		_texmatImage->setMatrix(_texmatImage->getMatrix()*osg::Matrix::translate(-rx*scale,-ry*scale,0.0f));
	}

	void 
	StereoImageEventHandler::initTexMatrices()
	{
		_texmatImage->setMatrix(osg::Matrix::translate(0.0f,0.0f,0.0f));
		scaleImage(1.25);
		scaleImage(1.25);
		scaleImage(1.25);
		scaleImage(1.25);
	}
}