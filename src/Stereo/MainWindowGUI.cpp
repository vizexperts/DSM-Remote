#include <Stereo/MainWindowGUI.h>
#include <Stereo/StereoInputGUI.h>
#include <Stereo/ImageCameraManipulator.h>
#include <Stereo/PreprocessInputGUI.h>

#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <Util/StringUtils.h>

#if USE_QT4
#include <QtGui/QFileDialog>
#include <QtGui/QValidator>
#include <QtCore/QFileInfo>
#include <QtUiTools/QtUiTools>
#include <QtGui/QToolBar>
#include <QtGui/QProgressBar>
#include <osgGA/TrackballManipulator>
#include <stereo\GdalWarper.h>
#include <QtGui/QStatusBar>
#include <Qt\QThread.h>
#include <QtGui/QKeyEvent>
#include <QtGui/QMessageBox>
#else
#include <QtWidgets/QFileDialog>
#include <QtGui/QValidator>
#include <QtCore/QFileInfo>
#include <QtUiTools/QtUiTools>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QProgressBar>
#include <osgGA/TrackballManipulator>
#include <Stereo/GdalWarper.h>
#include <QtWidgets/QStatusBar>
#include <QtCore/QThread>
#include <QtGui/QKeyEvent>
#include <QtWidgets/QMessageBox>
#include <Stereo/MainWindowUI.h>
#include <Stereo/BusybarDialog.h>
#endif



namespace Stereo
{


    MainWindowFilter::MainWindowFilter()
                :_mainWindow(NULL)
                , _gw(NULL)
                ,_fullscreen(false)
    {

    
    }

    void
    MainWindowFilter::setMainWindow(QMainWindow* mainWindow)
    {
        _mainWindow = mainWindow;
    }


    void 
    MainWindowFilter::setGraphicsWindow(osgViewer::GraphicsWindow* gw)
    {
        _gw = gw;
    }

    bool 
    MainWindowFilter::eventFilter(QObject* object, QEvent* event)
    {
        if(event->type() == QEvent::MouseButtonPress || event->type() == QEvent::MouseButtonRelease || event->type() == QEvent::MouseMove)
        {
            return true;

        }
        if((object == _mainWindow && event->type() == QEvent::KeyPress) || event->type() == QEvent::MouseButtonPress)
        {
            QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
            if(keyEvent->key() == Qt::Key_F11)
            {
                if(!_fullscreen)
                {
                    QWidget* menuWidget = _mainWindow->menuWidget();
                    if(menuWidget)
                    {
                        menuWidget->hide();
                    }

                    QStatusBar* statusBar = _mainWindow->statusBar();
                    if(statusBar)
                    {
                        statusBar->hide();
                    }

                    _mainWindow->showFullScreen();
                    _fullscreen = true;
                }
                else
                {
                    QWidget* menuWidget = _mainWindow->menuWidget();
                    if(menuWidget)
                    {
                        menuWidget->show();
                    }

                    QStatusBar* statusBar = _mainWindow->statusBar();
                    if(statusBar)
                    {
                        statusBar->show();
                    }
                    _mainWindow->showMaximized();
                    _fullscreen = false;
                }

                return true;
            }
            else if(keyEvent->key() == Qt::Key_Delete)
            {
                _gw->getEventQueue()->keyPress(osgGA::GUIEventAdapter::KEY_Delete);
                return true;
            }
   //         else if(keyEvent->key() == Qt::Key_J)
   //         {
            //    MainWindowGUI* myWindow = static_cast<MainWindowGUI*>(_mainWindow);
            //    osg::Group* rootNode = dynamic_cast<osg::Group*>(myWindow->getViewer()->getView(0)->getSceneData());

            //    if(rootNode)
            //    {
            //        osg::MatrixTransform* transf = dynamic_cast<osg::MatrixTransform*>(rootNode->getChild(1));

            //        if(transf)
            //        {
            //            transf->setMatrix(transf->getMatrix() * osg::Matrix::translate(osg::Vec3d(-0.005,0.0,0.0)));
            //        }
            //    }

            //}
   //         else if(keyEvent->key() == Qt::Key_K)
   //         {
            //    MainWindowGUI* myWindow = static_cast<MainWindowGUI*>(_mainWindow);
            //    osg::Group* rootNode = dynamic_cast<osg::Group*>(myWindow->getViewer()->getView(0)->getSceneData());

            //    if(rootNode)
            //    {
            //        osg::MatrixTransform* transf = dynamic_cast<osg::MatrixTransform*>(rootNode->getChild(1));

            //        if(transf)
            //        {
            //            transf->setMatrix(transf->getMatrix() * osg::Matrix::translate(osg::Vec3d(0.005,0.0,0.0)));
            //        }
            //    }
            //}
        }
       return false;
    }

    MainWindowGUI::MainWindowGUI(QWidget *parent)
        :_viewer(NULL),
        _stereoSceneGenerator(NULL),
        _lastPath("/home")
    {
#ifdef USE_QT4
        QUiLoader loader;
                
        QFile file("../../data/gui/StereoViz/StitcherGUI.ui");
        file.open(QFile::ReadOnly);
        if(!file.isOpen())
        {
            std::cout<<"Unable to read the MainwindowGUI UI File"<<std::endl;
            return;
        }    

        ui_MainWindowGUIWidget = loader.load(&file, this);

        file.close();
#else

        ui_MainWindowGUIWidget = new QWidget();
        MainWindow_Form form;
        form.setupUi(ui_MainWindowGUIWidget);
#endif
        _loadButton               = ui_MainWindowGUIWidget->findChild<QPushButton*>("loadButton");
        _stereoButton            = ui_MainWindowGUIWidget->findChild<QPushButton*>("loadStereoButton");
        _stereoFileButton            = ui_MainWindowGUIWidget->findChild<QPushButton*>("loadStereoFileButton");
        _preprocessButton        = ui_MainWindowGUIWidget->findChild<QPushButton*>("preprocessButton");
        _saveStereoConfigButton        = ui_MainWindowGUIWidget->findChild<QPushButton*>("saveStereoButton");
        _gridLayout                = ui_MainWindowGUIWidget->findChild<QGridLayout*>("gridLayout");
        _hLayout                = ui_MainWindowGUIWidget->findChild<QHBoxLayout*>("horizontalLayout");

        if ( !_loadButton || !_stereoButton || !_stereoFileButton || !_gridLayout || !_preprocessButton ||!_hLayout || !_saveStereoConfigButton)
        {
            std::cout<<"Unable to initialize widgets. Check UI File"<<std::endl;
        }

        _stereoSceneGenerator =  new Stereo::StereoSceneGenerator;

        _initializeViewer();
        connect(_loadButton, SIGNAL(clicked()), this, SLOT(_handleLoadButtonClicked()));
        connect(_stereoButton, SIGNAL(clicked()), this, SLOT(_handleStereoButtonClicked()));
        connect(_stereoFileButton, SIGNAL(clicked()), this, SLOT(_handleStereoFileButtonClicked()));
        connect(_preprocessButton, SIGNAL(clicked()), this, SLOT(_handlePreProcessButtonClicked()));
        connect(_saveStereoConfigButton, SIGNAL(clicked()), this, SLOT(_handleSaveStereoConfigButtonClicked()));

#ifdef USE_QT4
        QFile file2("../../data/gui/StereoViz/BusybarDialog.ui");
        file2.open(QFile::ReadOnly);
        if(!file2.isOpen())
        {
            std::cout<<"Unable to read the BusyBarDialog UI File"<<std::endl;
            return;
        }    

        _busyDialog = loader.load(&file2, this);
        file2.close();
#else

        _busyDialog =  new QDialog();
        BusyBar_Dialog dialog;
        dialog.setupUi(_busyDialog);

#endif
        QObject::connect(this, SIGNAL(_showBusyBar(bool)), _busyDialog, SLOT(setVisible(bool)), Qt::QueuedConnection);

        this->setWindowTitle("StereoViz");
        std::string iconFile = ":/icons/VizLogo.ico";
        QIcon Icon(QString(iconFile.c_str()));
        this->setWindowIcon(Icon);

    }
    
    MainWindowGUI::MainWindowGUI(std::string url,Stereo::Image::EnhancementType enhancementType ,QWidget *parent)
        :_viewer(NULL),
        _stereoSceneGenerator(NULL),
         _lastPath("/home")
    {
        QUiLoader loader;
                
        QFile file(":/StereoViz/StitcherGUI.ui");
        file.open(QFile::ReadOnly);
        if(!file.isOpen())
        {
            std::cout<<"Unable to read the MainwindowGUI UI File"<<std::endl;
            return;
        }    

#ifdef USE_QT4
        QUiLoader loader;
                
        QFile file("../../data/gui/StereoViz/StitcherGUI.ui");
        file.open(QFile::ReadOnly);
        if(!file.isOpen())
        {
            std::cout<<"Unable to read the MainwindowGUI UI File"<<std::endl;
            return;
        }    

        ui_MainWindowGUIWidget = loader.load(&file, this);

        file.close();
#else

        ui_MainWindowGUIWidget = new QWidget();
        MainWindow_Form form;
        form.setupUi(ui_MainWindowGUIWidget);
#endif
        
        _loadButton               = ui_MainWindowGUIWidget->findChild<QPushButton*>("loadButton");
        _stereoButton            = ui_MainWindowGUIWidget->findChild<QPushButton*>("loadStereoButton");
        _stereoFileButton            = ui_MainWindowGUIWidget->findChild<QPushButton*>("loadStereoFileButton");
        _preprocessButton        = ui_MainWindowGUIWidget->findChild<QPushButton*>("preprocessButton");
        _saveStereoConfigButton        = ui_MainWindowGUIWidget->findChild<QPushButton*>("saveStereoButton");
        _gridLayout                = ui_MainWindowGUIWidget->findChild<QGridLayout*>("gridLayout");
        _hLayout                = ui_MainWindowGUIWidget->findChild<QHBoxLayout*>("horizontalLayout");

        if ( !_loadButton || !_stereoButton || !_stereoFileButton || !_gridLayout || !_preprocessButton ||!_hLayout || !_saveStereoConfigButton)
        {
            std::cout<<"Unable to initialize widgets. Check UI File"<<std::endl;
        }

        _stereoSceneGenerator =  new Stereo::StereoSceneGenerator;

        _initializeViewer(url,enhancementType);
        connect(_loadButton, SIGNAL(clicked()), this, SLOT(_handleLoadButtonClicked()));
        connect(_stereoButton, SIGNAL(clicked()), this, SLOT(_handleStereoButtonClicked()));
        connect(_stereoFileButton, SIGNAL(clicked()), this, SLOT(_handleStereoFileButtonClicked()));
        connect(_preprocessButton, SIGNAL(clicked()), this, SLOT(_handlePreProcessButtonClicked()));
        connect(_saveStereoConfigButton, SIGNAL(clicked()), this, SLOT(_handleSaveStereoConfigButtonClicked()));


#ifdef USE_QT4
        QFile file2("../../data/gui/StereoViz/BusybarDialog.ui");
        file2.open(QFile::ReadOnly);
        if(!file2.isOpen())
        {
            std::cout<<"Unable to read the BusyBarDialog UI File"<<std::endl;
            return;
        }    

        _busyDialog = loader.load(&file2, this);
        file2.close();
#else

        _busyDialog =  new QDialog();
        BusyBar_Dialog dialog;
        dialog.setupUi(_busyDialog);

#endif

        QObject::connect(this, SIGNAL(_showBusyBar(bool)), _busyDialog, SLOT(setVisible(bool)), Qt::QueuedConnection);

        this->setWindowTitle("StereoViz");
        std::string iconFile = ":/icons/VizLogo.ico";
        QIcon Icon(QString(iconFile.c_str()));
        this->setWindowIcon(Icon);

    }

    MainWindowGUI::~MainWindowGUI()
    {
        //delete _viewer;
        //delete _eventHandler;
    }

    void
    MainWindowGUI::_initializeViewer()
    {
        _viewer =  new Stereo::StereoViewerGUI;

        std::string defaultFile = ":/StereoViz/StereoVizLogo.png";

        _viewer->setGeometry(10,10,400,200);



        osg::ref_ptr<osg::Node> rootNode;

        rootNode = _stereoSceneGenerator->loadImage(defaultFile);

        if(rootNode)
        {
            _viewer->getView(0)->setSceneData(rootNode);
        }

        ImageCameraManipulator* imageCameraManipulator = new ImageCameraManipulator;
        imageCameraManipulator->setSceneGenerator(_stereoSceneGenerator);

        _viewer->getView(0)->setFusionDistance(osgUtil::SceneView::USE_FUSION_DISTANCE_VALUE,1.0);
        _viewer->getView(0)->setCameraManipulator(imageCameraManipulator);
        
        double fovy, aspectRatio, zNear, zFar;
        _viewer->getView(0)->getCamera()->getProjectionMatrixAsPerspective(fovy, aspectRatio, zNear, zFar);
        _viewer->getView(0)->getCamera()->setProjectionMatrixAsPerspective(fovy, 1.0, zNear, zFar);

        osg::DisplaySettings::instance()->setStereo(false);

        QToolBar* menuWidget = new QToolBar;
        menuWidget->addWidget(_loadButton);
        menuWidget->addWidget(_stereoButton);
        menuWidget->addWidget(_stereoFileButton);
        menuWidget->addWidget(_preprocessButton);
        _saveStereoAction =  menuWidget->addWidget(_saveStereoConfigButton);

        _saveStereoAction->setVisible(false);
        
        this->addToolBar(Qt::RightToolBarArea,menuWidget);

        this->setCentralWidget(_viewer);

        _mainWindowFilter =  new MainWindowFilter;
        _mainWindowFilter->setMainWindow(this);
        osgViewer::GraphicsWindow* gw = dynamic_cast<osgViewer::GraphicsWindow*>(_viewer->getView(0)->getCamera()->getGraphicsContext());
        if(gw)
        {
            _mainWindowFilter->setGraphicsWindow(gw);
        }

        this->installEventFilter(_mainWindowFilter);


    }
    
    void
    MainWindowGUI::_initializeViewer(std::string url,Stereo::Image::EnhancementType enhancementType)
    {
        _viewer =  new Stereo::StereoViewerGUI;

        std::string defaultFile = url;//"../../data/gui/StereoViz/StereoVizLogo.png";

        _viewer->setGeometry(0,0,400,200);

        double fovy, aspectRatio, zNear, zFar;
        _viewer->getView(0)->getCamera()->getProjectionMatrixAsPerspective(fovy, aspectRatio, zNear, zFar);
        _viewer->getView(0)->getCamera()->setProjectionMatrixAsPerspective(fovy, 1.0, zNear, zFar);

        //osg::DisplaySettings::instance()->setStereo(false);
   

        osg::ref_ptr<osg::Node> rootNode;

        rootNode = _stereoSceneGenerator->loadImage(defaultFile,enhancementType);

        if(rootNode)
        {
            _viewer->getView(0)->setSceneData(rootNode);
        }

        _viewer->getView(0)->setFusionDistance(osgUtil::SceneView::USE_FUSION_DISTANCE_VALUE,1.0);
        _viewer->getView(0)->setCameraManipulator( new ImageCameraManipulator);

        //QToolBar* menuWidget = new QToolBar;
        //menuWidget->addWidget(_loadButton);
        //menuWidget->addWidget(_stereoButton);
        //menuWidget->addWidget(_preprocessButton);
        //menuWidget->setMaximumHeight(100);
        //this->addToolBar(Qt::BottomToolBarArea,menuWidget);

        this->setCentralWidget(_viewer);

        _mainWindowFilter =  new MainWindowFilter;
        _mainWindowFilter->setMainWindow(this);
        osgViewer::GraphicsWindow* gw = dynamic_cast<osgViewer::GraphicsWindow*>(_viewer->getView(0)->getCamera()->getGraphicsContext());
        if(gw)
        {
            _mainWindowFilter->setGraphicsWindow(gw);
        }

        this->installEventFilter(_mainWindowFilter);


    }


    void 
    MainWindowGUI::_handleLoadButtonClicked()
    {
        std::string formatFilter = "(*.tif *.tiff *.vrt)";
        std::string label = std::string("Image files");
        QString filename = QFileDialog::getOpenFileName(this,
                                                        label.c_str(),
                                                        _lastPath.c_str(),
                                                        formatFilter.c_str(), 0, QFileDialog::DontUseNativeDialog);

        //directory = osgDB::getFilePath(filename.toStdString());

        if(filename != "")
        {
            try 
            {
                _lastPath = osgDB::getFilePath(filename.toStdString());
            }
            catch(...)
            {

            }

            double fovy, aspectRatio, zNear, zFar;
            _viewer->getView(0)->getCamera()->getProjectionMatrixAsPerspective(fovy, aspectRatio, zNear, zFar);
            float radius = 1.0f;
            float height = 2*radius*tan(osg::DegreesToRadians(fovy)*0.5f);
            float length = osg::PI*radius;  // half a cylinder.

            osg::DisplaySettings::instance()->setStereo(false);
            
            setApplicationBusy(true);
            osg::ref_ptr<osg::Node> rootNode;
            rootNode = rootNode = _stereoSceneGenerator->loadImage(filename.toStdString());
            setApplicationBusy(false);

            _viewer->getView(0)->setSceneData(rootNode);

            _saveStereoAction->setVisible(false);
        }
    }

    void 
    MainWindowGUI::_handleStereoButtonClicked()
    {
        StereoInputGUI* stereoInputDialog = new StereoInputGUI();
        QObject::connect(stereoInputDialog, SIGNAL(_selectedInputs(std::string, std::string, double, osg::DisplaySettings::StereoMode)),
            this, SLOT(_stereoInputsSelected(std::string, std::string, double, osg::DisplaySettings::StereoMode)));
        if(!stereoInputDialog->exec())
        {
            stereoInputDialog->deleteLater();
            return;
        }

    }

    void 
    MainWindowGUI::_handleStereoFileButtonClicked()
    {
        std::string formatFilter = "(*.stereo)";
        std::string label = std::string("Stereo Config File");
        QString filename = QFileDialog::getOpenFileName(this,
                                                        label.c_str(),
                                                        _lastPath.c_str(),
                                                        formatFilter.c_str(), 0, QFileDialog::DontUseNativeDialog);

        if(filename != "")
        {
            try 
            {
                _lastPath = osgDB::getFilePath(filename.toStdString());
            }
            catch(...)
            {

            }

            _stereoConfigFileSelected(filename.toStdString());
        }

    }

    void 
    MainWindowGUI::_handlePreProcessButtonClicked()
    {

        PreprocessInputGUI* preprocessInputDialog = new PreprocessInputGUI(this);
        QObject::connect(preprocessInputDialog, SIGNAL(_selectedInputs(QString, QString)), this, SLOT(_processingInputsSelected(QString, QString)),Qt::AutoConnection);
        if(!preprocessInputDialog->exec())
        {
            preprocessInputDialog->deleteLater();
            return;
        }

    }

    void 
    MainWindowGUI::_handleSaveStereoConfigButtonClicked()
    {
        std::string leftURL = _stereoSceneGenerator->getLeftImage()->getUrl();
        std::string rightURL = _stereoSceneGenerator->getRightImage()->getUrl();

        std::string leftFileName = osgDB::getSimpleFileName(leftURL);
        std::string rightFileName = osgDB::getSimpleFileName(rightURL);

        std::string leftFilePath =  osgDB::getFilePath(leftURL);
        std::string rightFilePath =  osgDB::getFilePath(rightURL);

        if(leftFilePath != rightFilePath)
        {
            QMessageBox::warning(this, "Save Stereo Config", "Left and Right Image should be in same folder");            
            return;
        }

        std::string stereoFileName = leftFilePath + "\\" + osgDB::getStrippedName(leftFileName) + "_" + osgDB::getStrippedName(rightFileName) + ".stereo";

        FILE* stereoFile;

        stereoFile = fopen(stereoFileName.c_str(),"w");

        if(!stereoFile)
        {
            QMessageBox::warning(this, "Save Stereo Config", "Failed to create stereo file");            
            return;
        }

        fprintf(stereoFile,"TKP_STEREO_CONFIG\n");
        fprintf(stereoFile,"LeftFile %s\n",leftFileName.c_str());
        fprintf(stereoFile,"RightFile %s\n",rightFileName.c_str());
        fprintf(stereoFile,"Rotation %f\n",_stereoSceneGenerator->getRotationAngle());
        fprintf(stereoFile,"TranslationX %f\n",_stereoSceneGenerator->getTranslationX());
        fprintf(stereoFile,"TranslationY %f\n",_stereoSceneGenerator->getTranslationY());

        fclose(stereoFile);

        QMessageBox::information(this, "Save Stereo Config", "Saved Successfully");
        return;

    }

    void 
    MainWindowGUI::_stereoInputsSelected(std::string inputImage1, std::string inputImage2, double rotationAngle, osg::DisplaySettings::StereoMode sMode)
    {
        osg::ref_ptr<osg::Node> rootNode;
        rootNode = _stereoSceneGenerator->loadStereoImages(inputImage1,inputImage2,rotationAngle);
        _viewer->getView(0)->setSceneData(rootNode);
        _viewer->getView(0)->setFusionDistance(osgUtil::SceneView::USE_FUSION_DISTANCE_VALUE,1.0);
        osg::DisplaySettings::instance()->setStereo(true);
        osg::DisplaySettings::instance()->setStereoMode(sMode);
        double seperation = osg::DisplaySettings::instance()->getEyeSeparation();
        //osg::DisplaySettings::instance()->setEyeSeparation(10.0 * seperation);

        _saveStereoAction->setVisible(true);

    }

    void 
    MainWindowGUI::_processingInputsSelected(QString inputImage, QString outputImage)
    {
        GdalWarper* warper = new GdalWarper();

        warper->setInputImageUrl(inputImage.toStdString());
        warper->setOutputImageUrl(outputImage.toStdString());
        warper->setDataType("BYTE");
        warper->setOutputFormat("gtiff");
        warper->setWarpMethod("RPC");

        QThread* thread = new QThread;

        warper->moveToThread(thread);
        connect(thread, SIGNAL(started()), warper, SLOT(start()));
        connect(warper, SIGNAL(sucessfull(QString)), this, SLOT(_operationSucessfull(QString)));
        connect(warper, SIGNAL(failed(QString)), this, SLOT(_operationFailed(QString)));
        thread->start();
        setApplicationBusy(true);



    }

    void  
    MainWindowGUI::_stereoConfigFileSelected(std::string inputFile)
    {

        std::ifstream ifs;
        ifs.open (inputFile.c_str(), std::ifstream::in);

        if(!ifs)
        {
            QMessageBox::critical(this, "Load Stereo File", "Unable to open stereo file");
            return;
        }

        std::string validationText;
        ifs>>validationText;

        if(validationText != "TKP_STEREO_CONFIG")
        {
            QMessageBox::critical(this, "Load Stereo File", "Invalid Stereo Config file");
            return;
        }

        std::string dummy;
        std::string leftImage;
        std::string rightImage;
        std::string rotationAngle;
        std::string translationX;
        std::string translationY;

        ifs>>dummy;
        ifs>>leftImage;
        ifs>>dummy;
        ifs>>rightImage;
        ifs>>dummy;
        ifs>>rotationAngle;
        ifs>>dummy;
        ifs>>translationX;
        ifs>>dummy;
        ifs>>translationY;

        ifs.close();

        std::string inputImage1,inputImage2;
        std::string inputPath = osgDB::getFilePath(inputFile);

        inputImage1 = inputPath + "\\" + leftImage;
        inputImage2 = inputPath + "\\" + rightImage;

        osg::DisplaySettings::StereoMode sMode = DEFAULT_STEREO_BEHAVIOUR;

        osg::ref_ptr<osg::Node> rootNode;
        rootNode = _stereoSceneGenerator->loadStereoImages(inputImage1,inputImage2,UTIL::ToDouble(rotationAngle),UTIL::ToDouble(translationX),UTIL::ToDouble(translationY));
        _viewer->getView(0)->setSceneData(rootNode);
        _viewer->getView(0)->setFusionDistance(osgUtil::SceneView::USE_FUSION_DISTANCE_VALUE,1.0);
        osg::DisplaySettings::instance()->setStereo(true);
        osg::DisplaySettings::instance()->setStereoMode(sMode);
        double seperation = osg::DisplaySettings::instance()->getEyeSeparation();

        _saveStereoAction->setVisible(true);

        return;


    }


    void 
    MainWindowGUI::_operationSucessfull(QString message)
    {
        setApplicationBusy(false);
        QMessageBox::information(this, "Operation Finished", message);            
        

    }

    void 
    MainWindowGUI::_operationFailed(QString message)
    {
        setApplicationBusy(false);
        QMessageBox::critical(this, "Operation Finished", message);

    }

    void
    MainWindowGUI::setApplicationBusy(bool visibility)
    {
        emit _showBusyBar(visibility);
    }

    Stereo::StereoViewerGUI*
    MainWindowGUI::getViewer()
    {
        return _viewer;

    }

    void 
    MainWindowGUI::closeEvent(QCloseEvent *event)
    {
        QMessageBox msgBox(this);
        
        msgBox.setText("Do you really want to exit?");
        msgBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No );

        int ret = msgBox.exec();

        switch(ret)
        {
            case QMessageBox::Yes:
            {

               event->accept();
               emit applicationClosed( this );
            }
            break;

            case QMessageBox::No:
                event->ignore();
            break;
        }
    }

}


