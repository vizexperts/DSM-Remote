#include <Stereo/ImageCameraManipulator.h>

#include<osg/Node>
#include<osg/MatrixTransform>
#include<osgViewer/View>


namespace Stereo
{

    bool ImageCameraManipulator::performMovementLeftMouseButton( const double eventTimeDelta, const double dx, const double dy )
    {
        float scale = -0.3f * _distance * getThrowScale( eventTimeDelta );
        panModel( dx*scale, dy*scale );
        return true;

    }

    bool ImageCameraManipulator::performMovementRightMouseButton( const double eventTimeDelta, const double dx, const double dy )
    {
        float scale = -0.3f * _distance * getThrowScale( eventTimeDelta );
        panModel( dx*scale, dy*scale );
        return true;

    }
        
    bool ImageCameraManipulator::handleMouseWheel( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us )
    {
        osgGA::GUIEventAdapter::ScrollingMotion sm = ea.getScrollingMotion();
    
        // handle centering
        if( _flags & SET_CENTER_ON_WHEEL_FORWARD_MOVEMENT )
        {    
            if( ((sm == osgGA::GUIEventAdapter::SCROLL_DOWN && _wheelZoomFactor > 0.)) ||
                ((sm == osgGA::GUIEventAdapter::SCROLL_UP   && _wheelZoomFactor < 0.)) )
            {
                if( getAnimationTime() <= 0. )
                {
                    // center by mouse intersection (no animation)
                    setCenterByMousePointerIntersection( ea, us );
                }
                else
                {
                    // start new animation only if there is no animation in progress
                    if( !isAnimating() )
                        startAnimationByMousePointerIntersection( ea, us );
    
                }    
            }
        }
    
        switch( sm )
        {
            // mouse scroll up event
            case osgGA::GUIEventAdapter::SCROLL_UP:
            {
                // perform zoom
                zoomModel( -_wheelZoomFactor, false );
                us.requestRedraw();
                us.requestContinuousUpdate( isAnimating() || _thrown );
                return true;
            }
    
            // mouse scroll down event
            case osgGA::GUIEventAdapter::SCROLL_DOWN:
            {
                // perform zoom
                zoomModel( _wheelZoomFactor, false );
                us.requestRedraw();
                us.requestContinuousUpdate( isAnimating() || _thrown );
                return true;
            }
    
            // unhandled mouse scrolling motion
            default:
                return false;
       }
    }

    /// Handles GUIEventAdapter::KEYDOWN event.
    bool ImageCameraManipulator::handleKeyDown( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us )
    {
        if( ea.getKey() == osgGA::GUIEventAdapter::KEY_Space )
        {
            flushMouseEventStack();
            _thrown = false;
            home(ea,us);
            return true;
        }
        else if(ea.getKey() == osgGA::GUIEventAdapter::KEY_J)
        {
            
            osgViewer::View* view = dynamic_cast<osgViewer::View*>(us.asView());
            if(view)
            {

                osg::Group* rootNode = dynamic_cast<osg::Group*>(view->getSceneData());

                if(rootNode)
                {
                    if(rootNode->getChild(1)!=NULL)
                    {
                        osg::BoundingSphere bound = rootNode->getBound();
                        double radius = bound.radius();
                        double step = radius/1000.0;
                        osg::MatrixTransform* transf = dynamic_cast<osg::MatrixTransform*>(rootNode->getChild(1));

                        if(transf)
                        {
                            transf->setMatrix(transf->getMatrix() * osg::Matrix::translate(osg::Vec3d(-step,0.0,0.0)));

                            osg::Vec3d translation = transf->getMatrix().getTrans();
                            _sceneGenerator->setTranslationX(translation[0]);
                        }
                    }
                }
            }
            return true;

        }
        else if(ea.getKey() == osgGA::GUIEventAdapter::KEY_L)
        {
            osgViewer::View* view = dynamic_cast<osgViewer::View*>(us.asView());
            if(view)
            {

                osg::Group* rootNode = dynamic_cast<osg::Group*>(view->getSceneData());

                if(rootNode)
                {
                    if(rootNode->getChild(1)!=NULL)
                    {
                        osg::MatrixTransform* transf = dynamic_cast<osg::MatrixTransform*>(rootNode->getChild(1));
                        osg::BoundingSphere bound = rootNode->getBound();
                        double radius = bound.radius();
                        double step = radius/1000.0;

                        if(transf)
                        {
                            osg::BoundingSphere bound = transf->getBound();
                            
                            transf->setMatrix(transf->getMatrix() * osg::Matrix::translate(osg::Vec3d(step,0.0,0.0)));

                            osg::Vec3d translation = transf->getMatrix().getTrans();
                            _sceneGenerator->setTranslationX(translation[0]);
                        }
                    }
                }
            }
            return true;
        }
        else if(ea.getKey() == osgGA::GUIEventAdapter::KEY_I)
        {
            
            osgViewer::View* view = dynamic_cast<osgViewer::View*>(us.asView());
            if(view)
            {

                osg::Group* rootNode = dynamic_cast<osg::Group*>(view->getSceneData());

                if(rootNode)
                {
                    if(rootNode->getChild(1)!=NULL)
                    {
                        osg::BoundingSphere bound = rootNode->getBound();
                        double radius = bound.radius();
                        double step = radius/1000.0;

                        osg::MatrixTransform* transf = dynamic_cast<osg::MatrixTransform*>(rootNode->getChild(1));

                        if(transf)
                        {
                            transf->setMatrix(transf->getMatrix() * osg::Matrix::translate(osg::Vec3d(0.0,0.0,step)));

                            osg::Vec3d translation = transf->getMatrix().getTrans();
                            _sceneGenerator->setTranslationY(translation[2]);
                        }
                    }
                }
            }
            return true;

        }
        else if(ea.getKey() == osgGA::GUIEventAdapter::KEY_K)
        {
            osgViewer::View* view = dynamic_cast<osgViewer::View*>(us.asView());
            if(view)
            {

                osg::Group* rootNode = dynamic_cast<osg::Group*>(view->getSceneData());

                if(rootNode)
                {
                    if(rootNode->getChild(1)!=NULL)
                    {
                        osg::BoundingSphere bound = rootNode->getBound();
                        double radius = bound.radius();
                        double step = radius/1000.0;
                        osg::MatrixTransform* transf = dynamic_cast<osg::MatrixTransform*>(rootNode->getChild(1));

                        if(transf)
                        {
                            transf->setMatrix(transf->getMatrix() * osg::Matrix::translate(osg::Vec3d(0.0,0.0,-step)));

                            osg::Vec3d translation = transf->getMatrix().getTrans();
                            _sceneGenerator->setTranslationY(translation[2]);
                        }
                    }
                }
            }
            return true;
        }

        return false;
    }
}

