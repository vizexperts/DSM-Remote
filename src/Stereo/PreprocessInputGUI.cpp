#include <Stereo/PreprocessInputGUI.h>

#include <QtCore/QFile>
#ifdef USE_QT4
    #include <QtGui/QFileDialog>
    #include <QtCore/QFile>
    #include <QtUiTools/QtUiTools>
    #include <QtGui/QFileDialog>
    #include <QtGui/QMessageBox>
#else
    #include <QtWidgets/QFileDialog>
    #include <QtUiTools/QtUiTools>
    #include <QtWidgets/QMessageBox>
    
#endif



#include <osg/io_utils>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/WriteFile>
#include <osg/ArgumentParser>

namespace Stereo
{
   
    PreprocessInputGUI::PreprocessInputGUI(QWidget *parent): QDialog(parent), 
                                                             _lastPath("/home")
    {
//#ifdef USE_QT4
        QUiLoader loader;
                
        QFile file(":/StereoViz/PreProcessInputDialog.ui");
        file.open(QFile::ReadOnly);
        if(!file.isOpen())
        {
            std::cout<<"Unable to read the AddRasterDialog UI File"<<std::endl;
            return;
        }

         ui_PreprocessInputGUIWidget = loader.load(&file, this);
        file.close();

//#else
//        ui_PreprocessInputGUIWidget = new QWidget();
//        PreProcessInput_Form form;
//        form.setupUi(ui_PreprocessInputGUIWidget);
//#endif
        
        //ui_PreprocessInputGUIWidget->show();

        _buttonBox          = ui_PreprocessInputGUIWidget->findChild<QDialogButtonBox*>("buttonBox");
        _inputName          = ui_PreprocessInputGUIWidget->findChild<QLineEdit*>("input");
        _outputName          = ui_PreprocessInputGUIWidget->findChild<QLineEdit*>("output");
        _inputBrowseButton  = ui_PreprocessInputGUIWidget->findChild<QToolButton*>("inputBrowse");
        _outputBrowseButton  = ui_PreprocessInputGUIWidget->findChild<QToolButton*>("outputBrowse");

    /*    _buttonBox          = qFindChild<QDialogButtonBox*>(this, "buttonBox");
        _inputName          = qFindChild<QLineEdit*>(this, "input");
        _outputName          = qFindChild<QLineEdit*>(this, "output");
        _inputBrowseButton  = qFindChild<QToolButton*>(this, "inputBrowse");
        _outputBrowseButton  = qFindChild<QToolButton*>(this, "outputBrowse");*/

        if ( !_inputName || !_outputName  || !_inputBrowseButton  || !_outputBrowseButton)
        {
            std::cout<<"Unable to initialize widgets. Check stereo input UI File"<<std::endl;
        }

        this->setWindowTitle("Image Pre-Processing");

        this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
        std::string iconFile = ":/icons/VizLogo.ico";
        QIcon Icon(QString(iconFile.c_str()));
        this->setWindowIcon(Icon);
        makeConnections();
    }
    
    PreprocessInputGUI::~PreprocessInputGUI()
    {

    }

    void PreprocessInputGUI::makeConnections()
    {
            connect(_inputBrowseButton, SIGNAL(clicked()), SLOT(_handleInputBrowseButtonClicked()));
            connect(_outputBrowseButton, SIGNAL(clicked()), SLOT(_handleOutputBrowseButtonClicked()));
            connect(_buttonBox, SIGNAL(accepted()), SLOT(_handleOkButtonClicked()));
            connect(_buttonBox, SIGNAL(rejected()), SLOT(_handleCancelButtonClicked()));
    }

    void PreprocessInputGUI::_handleInputBrowseButtonClicked()
    {
        std::string formatFilter = "Image (*.tif *.tiff)";
        std::string label = std::string("Raw Image");
        QString filename = QFileDialog::getOpenFileName(this,
                                                    label.c_str(),
                                                    _lastPath.c_str(),
                                                    formatFilter.c_str(), 0, QFileDialog::DontUseNativeDialog);

        if(!filename.isEmpty())
        {
            PreprocessInputGUI::_inputName->setText(filename);
            try
            {
                _lastPath = osgDB::getFilePath(filename.toStdString());
            }
            catch(...){}
        }
    }

    void PreprocessInputGUI::_handleOutputBrowseButtonClicked()
    {
        std::string formatFilter = "Image (*.tif *.tiff)";
        std::string label = std::string("Processed Image");
        QString filename = QFileDialog::getSaveFileName(this,
                                                    label.c_str(),
                                                    _lastPath.c_str(),
                                                    formatFilter.c_str(), 0, QFileDialog::DontUseNativeDialog);

        if(!filename.isEmpty())
        {
            PreprocessInputGUI::_outputName->setText(filename);
            try
            {
                _lastPath = osgDB::getFilePath(filename.toStdString());
            }
            catch(...){}
        }
    }


    void 
    PreprocessInputGUI::_handleCancelButtonClicked()
    {
        _clear();
        setVisible(false);
    }

    void 
    PreprocessInputGUI::_handleOkButtonClicked()
    {
        std::string inputImage = _inputName->text().toStdString();
        std::string outputImage = _outputName->text().toStdString();

        if(inputImage == "")
        {
            QMessageBox::critical(this, "Processing Image", "Input Image Field is Empty");
            return;
        }

        if(outputImage == "")
        {

            QMessageBox::critical(this, "Processing Image", "Output Image Field is Empty");
            return;
        }

        std::string rpcFileName = osgDB::getNameLessExtension(inputImage) + "_RPC.TXT";

        if(!osgDB::fileExists(rpcFileName))
        {
            std::string error = "Unable to find " + rpcFileName;
            QMessageBox::critical(this, "Processing Image", error.c_str());
            return;
        }



        try
        {
           emit _selectedInputs(_inputName->text(), _outputName->text());
        }
        catch(...)
        {
            QMessageBox::critical(this, "Processing Image", "Error occured while Processing image");
            return;
        }

        _handleCancelButtonClicked();
    }


    void
    PreprocessInputGUI::_clear()
    {
        _inputName->clear();
        _outputName->clear();
    }


}
