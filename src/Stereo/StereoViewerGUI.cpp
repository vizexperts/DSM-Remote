#include <Stereo/StereoViewerGUI.h>


namespace Stereo
{
    StereoViewerGUI::StereoViewerGUI(osgViewer::ViewerBase::ThreadingModel threadingModel) : QWidget()
    {
        setThreadingModel(threadingModel);

        //XXX setting frame schere to ON_DEMAND so that events
        //are independent of cutoff time which was causing lag
        //at start of the manipulator
        setRunFrameScheme(osgViewer::ViewerBase::ON_DEMAND);

        QWidget* widget = initializeViewWidget();

         //_loadButton = new QPushButton("Load Image");
         //_StereoButton = new QPushButton("Stereo");
        //button->setGeometry(50,50,100,100);

        if(widget)
        {
            QGridLayout* grid = new QGridLayout;
            grid->addWidget(widget, 0, 0);        
            setLayout(grid);
        }
        //QWidget* menuWidget = new QWidget;
        //QHBoxLayout* menuGrid = new QHBoxLayout;
        //menuGrid->addWidget(_loadButton);
        //menuGrid->addWidget(_StereoButton);
        //menuWidget->setLayout(menuGrid);

        //layout()->addWidget(menuWidget);
        //layout()->addWidget(button);

        connect(&_timer, SIGNAL(timeout()), this, SLOT(update()) );
        _timer.start(10);
    }

    StereoViewerGUI::~StereoViewerGUI()
    {

    }

    QWidget* 
    StereoViewerGUI::initializeViewWidget()
    {

        osgViewer::View* view = new osgViewer::View;
        addView( view );

        view->getCamera()->setCullMask(0xffffffff);
        view->getCamera()->setCullMaskLeft(0x00000001);
        view->getCamera()->setCullMaskRight(0x00000002);
        view->getCamera()->setClearColor( osg::Vec4(0.0, 0.0, 0.0, 1.0) );


        osg::DisplaySettings* ds = osg::DisplaySettings::instance().get();
        osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
        traits->windowName = "";
        traits->windowDecoration = true;
        traits->x = 0;
        traits->y = 0;
        traits->width = 100;
        traits->height = 100;
        traits->doubleBuffer = true;
        traits->alpha = 8;//ds->getMinimumNumAlphaBits();
        traits->stencil = ds->getMinimumNumStencilBits();
        traits->sampleBuffers = ds->getMultiSamples();
        traits->samples = ds->getNumMultiSamples();
        traits->quadBufferStereo = true;
        
        view->getCamera()->setGraphicsContext( new osgQt::GraphicsWindowQt(traits.get()) );
        view->getCamera()->setViewport( new osg::Viewport(0, 0, traits->width, traits->height) );

        osgQt::GraphicsWindowQt* gw = dynamic_cast<osgQt::GraphicsWindowQt*>( view->getCamera()->getGraphicsContext() );

        

        return gw ? gw->getGLWidget() : NULL;
    }

    void 
    StereoViewerGUI::paintEvent( QPaintEvent* event )
    { 
        frame(); 
    }
}