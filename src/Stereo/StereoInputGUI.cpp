#include <Stereo/StereoInputGUI.h>
#include <Stereo/MainWindowGUI.h>

#if USE_QT4
    #include <QtGui/QFileDialog>
    #include <QtCore/QFile>
    #include <QtUiTools/QtUiTools>
    #include <QtGui/QFileDialog>
    #include <QtGui/QMessageBox>
#else
    #include <QtWidgets/QFileDialog>
    #include <QtCore/QFile>
    #include <QtUiTools/QtUiTools>
    #include <QtWidgets/QFileDialog>
    #include <QtWidgets/QMessageBox>
    #include <Stereo/StereoInputDialog.h>
#endif


#include <osg/io_utils>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/WriteFile>
#include <osg/ArgumentParser>

namespace Stereo
{
   
    StereoInputGUI::StereoInputGUI(QWidget *parent) : _lastPath("/home")
    {
//#ifdef USE_QT4
        QUiLoader loader;
                
        QFile file(":/StereoViz/StereoInputDialog.ui");
        file.open(QFile::ReadOnly);
        if(!file.isOpen())
        {
            std::cout<<"Unable to read the AddRasterDialog UI File"<<std::endl;
            return;
        }

        ui_StereoInputGUIWidget = loader.load(&file, this);
        file.close();
//#else
//
//        ui_StereoInputGUIWidget = new QWidget(parent);
//        StereoInput_Form form;
//        form.setupUi(ui_StereoInputGUIWidget);
//        ui_StereoInputGUIWidget->show();
//#endif
        //ui_StereoInputGUIWidget->show();

        _buttonBox           =  ui_StereoInputGUIWidget->findChild<QDialogButtonBox*>("buttonBox");
        _inputName1          = ui_StereoInputGUIWidget->findChild<QLineEdit*>("input1");
        _inputName2          = ui_StereoInputGUIWidget->findChild<QLineEdit*>("input2");
        _inputRotation          = ui_StereoInputGUIWidget->findChild<QLineEdit*>("rotationInput");
        _inputBrowseButton1  = ui_StereoInputGUIWidget->findChild<QToolButton*>("inputBrowse1");
        _inputBrowseButton2  = ui_StereoInputGUIWidget->findChild<QToolButton*>("inputBrowse2");

    /*    _buttonBox          = qFindChild<QDialogButtonBox*>(this, "buttonBox");
        _inputName1          = qFindChild<QLineEdit*>(this, "input1");
        _inputName2          = qFindChild<QLineEdit*>(this, "input2");
        _inputBrowseButton1  = qFindChild<QToolButton*>(this, "inputBrowse1");
        _inputBrowseButton2  = qFindChild<QToolButton*>(this, "inputBrowse2");*/

        if ( !_inputName1 || !_inputName2  || !_inputBrowseButton2  || !_inputBrowseButton1   || !_inputRotation)
        {
            std::cout<<"Unable to initialize widgets. Check stereo input UI File"<<std::endl;
        }

        if(_inputRotation)
        {
            _inputRotation->setText("-90.0");
        }
        
        this->setWindowTitle("Stereo Image Pair");

        this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
        std::string iconFile = ":/icons/VizLogo.ico";
        QIcon Icon(QString(iconFile.c_str()));
        this->setWindowIcon(Icon);
        makeConnections();
    }
    
    StereoInputGUI::~StereoInputGUI()
    {

    }

    void StereoInputGUI::makeConnections()
    {
            connect(_inputBrowseButton1, SIGNAL(clicked()), SLOT(_handleInputBrowse1ButtonClicked()));
            connect(_inputBrowseButton2, SIGNAL(clicked()), SLOT(_handleInputBrowse2ButtonClicked()));
            connect(_buttonBox, SIGNAL(accepted()), SLOT(_handleOkButtonClicked()));
            connect(_buttonBox, SIGNAL(rejected()), SLOT(_handleCancelButtonClicked()));
    }

    void StereoInputGUI::_handleInputBrowse1ButtonClicked()
    {
        std::string formatFilter = "Image (*.jpg *.jpeg *.png *.tif *.tiff)";
        std::string label = std::string("Left Image file");
        QString filename = QFileDialog::getOpenFileName(this,
                                                    label.c_str(),
                                                    _lastPath.c_str(),
                                                    formatFilter.c_str(), 0, QFileDialog::DontUseNativeDialog);

        if(!filename.isEmpty())
        {
            StereoInputGUI::_inputName1->setText(filename);
            try
            {
                _lastPath = osgDB::getFilePath(filename.toStdString());
            }
            catch(...){}
        }
    }

    void StereoInputGUI::_handleInputBrowse2ButtonClicked()
    {
        std::string formatFilter = "Image (*.jpg *.jpeg *.png *.tif *.tiff)";
        std::string label = std::string("Right Image file");
        QString filename = QFileDialog::getOpenFileName(this,
                                                    label.c_str(),
                                                    _lastPath.c_str(),
                                                    formatFilter.c_str(), 0, QFileDialog::DontUseNativeDialog);

        if(!filename.isEmpty())
        {
            StereoInputGUI::_inputName2->setText(filename);
            try
            {
                _lastPath = osgDB::getFilePath(filename.toStdString());
            }
            catch(...){}
        }
    }


    void 
    StereoInputGUI::_handleCancelButtonClicked()
    {
        _clear();
        setVisible(false);
    }

    void 
    StereoInputGUI::_handleOkButtonClicked()
    {
        std::string inputImage1 = _inputName1->text().toStdString();
        std::string inputImage2 = _inputName2->text().toStdString();
        double rotationAngle = _inputRotation->text().toDouble();

        try
        {
            emit _selectedInputs(inputImage1, inputImage2, rotationAngle, DEFAULT_STEREO_BEHAVIOUR);
        }
        catch(...)
        {
            QMessageBox::critical(this, "Stereo Image pair loading", "Error occured while loading stereo image pair");
            return;
        }

        _handleCancelButtonClicked();
    }


    void
    StereoInputGUI::_clear()
    {
        _inputName1->clear();
        _inputName2->clear();
    }


}
