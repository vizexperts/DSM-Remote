#include <SMCElements/ProjectSettingsComponent.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <SMCElements/ILayerComponent.h>
#include <Core/IFeatureLayer.h>
#include <Core/ICompositeObject.h>
#include <Elements/IMilitarySymbol.h>

namespace SMCElements
{
    DEFINE_IREFERENCED(ProjectSettingsComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, ProjectSettingsComponent);

    ProjectSettingsComponent::ProjectSettingsComponent()
        : _militarySymbolRepresentationMode(ELEMENTS::IRepresentation::ICON_AND_TEXT)
    {
        _addInterface(SMCElements::IProjectSettingsComponent::getInterfaceName());
    }

    ProjectSettingsComponent::~ProjectSettingsComponent()
    {

    }

    void ProjectSettingsComponent::onAddedToWorldMaintainer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = getWorldMaintainer();
        _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);
    }

    void ProjectSettingsComponent::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {

    }

    ELEMENTS::IRepresentation::RepresentationMode 
    ProjectSettingsComponent::getMilitarySymbolRepresentationMode() const
    {
        return _militarySymbolRepresentationMode;
    }

    void ProjectSettingsComponent::setMilitarySymbolRepresentationMode
        (ELEMENTS::IRepresentation::RepresentationMode mode)
    {
        if(_militarySymbolRepresentationMode != mode)
        {

            _militarySymbolRepresentationMode = mode;

            CORE::RefPtr<CORE::IComponent> component = 
                _wm->getComponentByName("LayerComponent");
            if(!component.valid())
            {
                return;
            }

            CORE::RefPtr<SMCElements::ILayerComponent> layerComponent =
                component->getInterface<SMCElements::ILayerComponent>();

            const ILayerComponent::LayerMap& layerMap = layerComponent->getLayerMap();
            ILayerComponent::LayerMap::const_iterator iter = layerMap.begin();
            for(; iter != layerMap.end(); iter++ )
            {
                CORE::RefPtr<CORE::IFeatureLayer> layer = iter->second;

                if(!layer.valid())
                {
                    continue;
                }

                CORE::IFeatureLayer::FeatureLayerType featureLayerType = layer->getFeatureLayerType();
                if(featureLayerType != CORE::IFeatureLayer::MILITARY_UNIT_POINT)
                {
                    continue;
                }

                CORE::RefPtr<CORE::ICompositeObject> composite = 
                    layer->getInterface<CORE::ICompositeObject>();

                if(!composite.valid())
                {
                    continue;
                }

                const CORE::ICompositeObject::ObjectMap& objMap = composite->getObjectMap();
                CORE::ICompositeObject::ObjectMap::const_iterator iter = objMap.begin();

                for(; iter != objMap.end(); iter++)
                {
                    CORE::RefPtr<CORE::IObject> obj = iter->second;
                    if(obj.valid())
                    {
                        CORE::RefPtr<ELEMENTS::IRepresentation> represent = 
                            obj->getInterface<ELEMENTS::IRepresentation>();
                        if(represent.valid())
                        {
                            represent->setRepresentationMode(_militarySymbolRepresentationMode);
                        }
                    }
                }
            }
        }
    }
}

