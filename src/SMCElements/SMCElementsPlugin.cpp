/*****************************************************************************
*
* File             : SMCElementsPlugin.cpp
* Description      : SMCElementsPlugin class definition
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************/

#ifdef WIN32
#include <windows.h>
#endif    // WIN32

#include <SMCElements/SMCElementsPlugin.h>
#include <Core/CoreRegistry.h>
#include <Core/ObjectType.h>
#include <Core/VisitorType.h>
#include <Core/WorldType.h>
#include <Core/ComponentType.h>
#include <Core/Proxy.h>
#include <Core/ComponentUtils.h>
#include <Core/MessageType.h>
#include <Core/PropertyType.h>
#include <Core/CoreMessage.h>
#include <Core/RefType.h>
#include <Util/FileUtils.h>
#include <Core/ICoreRegistryPlugin.h>
#include <Core/MessageUtils.h>

#include <SMCElements/LayerComponent.h>
#include <SMCElements/ResourceManagerComponent.h>
#include <SMCElements/TourComponent.h>
#include <SMCElements/PresentationComponent.h>
#include <SMCElements/ProjectLoadAndSaveComponent.h>
#include <SMCElements/ProjectSettingsComponent.h>
#include <SMCElements/PresetComponent.h>
#include <SMCElements/UserFolderComponent.h>
#include <SMCElements/UserFolder.h>
#include <SMCElements/UserFolderObjectWrapper.h>
#include <SMCElements/IncidentLoaderComponent.h>
//#include <SMCElements/CheckpointPathTourComponent.h>

//----------------------------------------------------------------------------
// Register all interfaces
//----------------------------------------------------------------------------
DEFINE_META_INTERFACE(SMCElements, ILayerComponent);
DEFINE_META_INTERFACE(SMCElements, IDataSourceComponent);
DEFINE_META_INTERFACE(SMCElements, IResourceManagerComponent);
DEFINE_META_INTERFACE(SMCElements, ITourComponent);
DEFINE_META_INTERFACE(SMCElements, IPresentationComponent);
DEFINE_META_INTERFACE(SMCElements, IProjectLoadAndSaveComponent);
DEFINE_META_INTERFACE(SMCElements, IPresetComponent);
DEFINE_META_INTERFACE(SMCElements, IUserFolderComponent);
DEFINE_META_INTERFACE(SMCElements, IUserFolderItem);
DEFINE_META_INTERFACE(SMCElements, IUserFolderObjectWrapper);
DEFINE_META_INTERFACE(SMCElements, IIncidentLoaderComponent);
//DEFINE_META_INTERFACE(SMCElements, ICheckpointPathTourComponent);

//----------------------------------------------------------------------------
// Define ElementsRegistryPlugin and register plugin
//----------------------------------------------------------------------------

REGISTER_PLUGIN(SMCELEMENTS_DLL_EXPORT, SMCElements, SMCElementsRegistryPlugin)

namespace SMCElements
{
    // Object definitions
    const CORE::RefPtr<CORE::IObjectType> SMCElementsRegistryPlugin::UserFolderType
        (new CORE::ObjectType("UserFolder", SMCElementsRegistryPluginStartId + 100));

    const CORE::RefPtr<CORE::IObjectType> SMCElementsRegistryPlugin::UserFolderObjectWrpperType
        (new CORE::ObjectType("UserFolderObjectWrapper", SMCElementsRegistryPluginStartId + 101));

    // Component definitions
    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::LayerComponentType
        (new CORE::ComponentType("LayerComponent", SMCElementsRegistryPluginStartId + 1));

    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::ResourceManagerComponentType
        (new CORE::ComponentType("ResourceManagerComponent", SMCElementsRegistryPluginStartId + 3));

    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::TourComponentType
        (new CORE::ComponentType("TourComponent", SMCElementsRegistryPluginStartId + 5));

    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::PresentationComponentType
        (new CORE::ComponentType("PresentationComponent", SMCElementsRegistryPluginStartId + 6));

    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::ProjectLoadAndSaveComponentType
        (new CORE::ComponentType("ProjectLoadAndSaveComponent", SMCElementsRegistryPluginStartId + 7));

    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::PresetComponentType
        (new CORE::ComponentType("PresetComponent", SMCElementsRegistryPluginStartId + 8));

    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::UserFolderComponentType
        (new CORE::ComponentType("UserFolderComponent", SMCElementsRegistryPluginStartId + 9));

    const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::IncidentLoaderComponentType
        (new CORE::ComponentType("IncidentLoaderComponent", SMCElementsRegistryPluginStartId + 10));

    //const CORE::RefPtr<CORE::IComponentType> SMCElementsRegistryPlugin::CheckpointPathTourComponentType
    //    (new CORE::ComponentType("CheckpointPathTourComponent", SMCElementsRegistryPluginStartId + 9));

    // Message definitions
    //const CORE::RefPtr<CORE::IMessageType> ICheckpointPathTourComponent::CPTourStartedMessageType
    //    (new CORE::MessageType("CPTourStartedMessage", SMCElementsRegistryPlugin::SMCElementsRegistryPluginStartId + 50));

    //const CORE::RefPtr<CORE::IMessageType> ICheckpointPathTourComponent::CPTourEndedMessageType
    //    (new CORE::MessageType("CPTourEndedMessage", SMCElementsRegistryPlugin::SMCElementsRegistryPluginStartId + 51));

    const CORE::RefPtr<CORE::IMessageType> ITourComponent::SequenceTourChangedMessageType
        (new CORE::MessageType("SequenceTourChangedMessage", SMCElementsRegistryPlugin::SMCElementsRegistryPluginStartId + 52));

    const CORE::RefPtr<CORE::IMessageType> IUserFolderComponent::UserFolderConfigurationLoadedMessageType
        (new CORE::MessageType("UserFolderConfigurationLoadedMessage", SMCElementsRegistryPlugin::SMCElementsRegistryPluginStartId + 53));

    // property definitions

    // Reference Definitions

    const std::string SMCElementsRegistryPlugin::LibraryName   = UTIL::getPlatformLibraryName("SMCElements");
    const std::string SMCElementsRegistryPlugin::PluginName    = "SMCElements";

    SMCElementsRegistryPlugin::~SMCElementsRegistryPlugin(){}

    void SMCElementsRegistryPlugin::load(const CORE::CoreRegistry& cr)
    {
        registerInterfaces(*(cr.getInterfaceRegistry()));
        registerMessages(*(cr.getMessageFactory()));
        registerObjects(*(cr.getObjectFactory()));
        registerVisitors(*(cr.getVisitorFactory()));
        registerWorlds(*(cr.getWorldFactory()));
        registerComponents(*(cr.getComponentFactory()));
        registerProperties(*(cr.getPropertyFactory()));
        registerReference(*(cr.getRefFactory()));
    }

    void SMCElementsRegistryPlugin::unload(const CORE::CoreRegistry& cr)
    {
        deregisterInterfaces(*(cr.getInterfaceRegistry()));
        deregisterMessages(*(cr.getMessageFactory()));
        deregisterObjects(*(cr.getObjectFactory()));
        deregisterObjects(*(cr.getObjectFactory()));
        deregisterVisitors(*(cr.getVisitorFactory()));
        deregisterWorlds(*(cr.getWorldFactory()));
        deregisterComponents(*(cr.getComponentFactory()));
        deregisterProperties(*(cr.getPropertyFactory()));
        deregisterReference(*(cr.getRefFactory()));
    }

    void SMCElementsRegistryPlugin::registerInterfaces(CORE::InterfaceRegistry& ir)
    {
    }

    void SMCElementsRegistryPlugin::deregisterInterfaces(CORE::InterfaceRegistry& ir)
    {}

    void SMCElementsRegistryPlugin::registerMessages(CORE::IMessageFactory& mf)
    {
        //mf.registerMessage(*ICheckpointPathTourComponent::CPTourStartedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        //mf.registerMessage(*ICheckpointPathTourComponent::CPTourEndedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*ITourComponent::SequenceTourChangedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
        mf.registerMessage(*IUserFolderComponent::UserFolderConfigurationLoadedMessageType, new CORE::MessageProxy<CORE::CoreMessage>());
    }

    void SMCElementsRegistryPlugin::deregisterMessages(CORE::IMessageFactory& mf)
    {
        //mf.deregisterMessage(*ICheckpointPathTourComponent::CPTourStartedMessageType);
        //mf.deregisterMessage(*ICheckpointPathTourComponent::CPTourStartedMessageType);
        mf.deregisterMessage(*ITourComponent::SequenceTourChangedMessageType);
        mf.deregisterMessage(*IUserFolderComponent::UserFolderConfigurationLoadedMessageType);
    }

    void SMCElementsRegistryPlugin::registerObjects(CORE::IObjectFactory& of)
    {
        of.registerObject(*SMCElementsRegistryPlugin::UserFolderType, new CORE::ObjectProxy<UserFolder>());
        of.registerObject(*SMCElementsRegistryPlugin::UserFolderObjectWrpperType, new CORE::ObjectProxy<UserFolderObjectWrapper>());
    }

    void SMCElementsRegistryPlugin::deregisterObjects(CORE::IObjectFactory& of)
    {
        of.deregisterObject(*SMCElementsRegistryPlugin::UserFolderType);
        of.deregisterObject(*SMCElementsRegistryPlugin::UserFolderObjectWrpperType);
    }

    void SMCElementsRegistryPlugin::registerVisitors(CORE::IVisitorFactory& vf)
    {
    }

    void SMCElementsRegistryPlugin::deregisterVisitors(CORE::IVisitorFactory& vf)
    {
    }

    void SMCElementsRegistryPlugin::registerWorlds(CORE::IWorldFactory& factory)
    {
    }

    void SMCElementsRegistryPlugin::deregisterWorlds(CORE::IWorldFactory& factory)
    {
    }

    void SMCElementsRegistryPlugin::registerComponents(CORE::IComponentFactory& cf)
    {
        cf.registerComponent(*SMCElementsRegistryPlugin::LayerComponentType, new CORE::ComponentProxy<LayerComponent>());
        cf.registerComponent(*SMCElementsRegistryPlugin::ResourceManagerComponentType, new CORE::ComponentProxy<ResourceManagerComponent>());
        cf.registerComponent(*SMCElementsRegistryPlugin::TourComponentType, new CORE::ComponentProxy<TourComponent>());
        cf.registerComponent(*SMCElementsRegistryPlugin::PresentationComponentType, new CORE::ComponentProxy<PresentationComponent>());
        cf.registerComponent(*SMCElementsRegistryPlugin::ProjectLoadAndSaveComponentType, new CORE::ComponentProxy<ProjectLoadAndSaveComponent>());
        cf.registerComponent(*SMCElementsRegistryPlugin::PresetComponentType, new CORE::ComponentProxy<PresetComponent>());
        cf.registerComponent(*SMCElementsRegistryPlugin::UserFolderComponentType, new CORE::ComponentProxy<UserFolderComponent>());
        cf.registerComponent(*SMCElementsRegistryPlugin::IncidentLoaderComponentType, new CORE::ComponentProxy<IncidentLoaderComponent>());
        //cf.registerComponent(*SMCElementsRegistryPlugin::CheckpointPathTourComponentType, new CORE::ComponentProxy<CheckpointPathTourComponent>());
    }

    void SMCElementsRegistryPlugin::deregisterComponents(CORE::IComponentFactory& cf)
    {
        cf.deregisterComponent(*SMCElementsRegistryPlugin::LayerComponentType);
        cf.deregisterComponent(*SMCElementsRegistryPlugin::ResourceManagerComponentType);
        cf.deregisterComponent(*SMCElementsRegistryPlugin::TourComponentType);
        cf.deregisterComponent(*SMCElementsRegistryPlugin::PresentationComponentType);
        cf.deregisterComponent(*SMCElementsRegistryPlugin::ProjectLoadAndSaveComponentType);
        cf.deregisterComponent(*SMCElementsRegistryPlugin::PresetComponentType);
        cf.deregisterComponent(*SMCElementsRegistryPlugin::UserFolderComponentType);
        cf.deregisterComponent(*SMCElementsRegistryPlugin::IncidentLoaderComponentType);
        //cf.deregisterComponent(*SMCElementsRegistryPlugin::CheckpointPathTourComponentType);
    }

    void SMCElementsRegistryPlugin::registerProperties(CORE::IPropertyFactory& pf)
    {
    }

    void SMCElementsRegistryPlugin::deregisterProperties(CORE::IPropertyFactory& pf)
    {
    }

    void SMCElementsRegistryPlugin::registerReference(CORE::IRefFactory& rf)
    {
    }

    void SMCElementsRegistryPlugin::deregisterReference(CORE::IRefFactory& rf)
    {
    }

    const std::string& SMCElementsRegistryPlugin::getLibraryName() const
    {
        return SMCElementsRegistryPlugin::LibraryName;
    }

    const std::string& SMCElementsRegistryPlugin::getPluginName() const
    {
        return SMCElementsRegistryPlugin::PluginName;
    }
}


