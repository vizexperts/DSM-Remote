#include <SMCElements/CheckpointPathTourComponent.h>

#include <Core/IPoint.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMessageFactory.h >
#include <Core/CoreRegistry.h>
#include <Core/CommonUtilityFunctions.h>
#include <Core/IObservable.h>

#include <Elements/ElementsUtils.h>
#include <Elements/ITimeBasedCheckpointPath.h>
#include <Elements/IPath.h>


#include <Util/CoordinateConversionUtils.h>

namespace SMCElements
{
    double bearingBetweenPoints(osg::Vec3d point1, osg::Vec3d point2)
    {
        double dLong    = osg::DegreesToRadians((point2.x() - point1.x()));
        double lat1     = osg::DegreesToRadians(point1.y());
        double lat2     = osg::DegreesToRadians(point2.y());

        double y = sin(dLong)*cos(lat2);
        double x = cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(dLong);

        double bearing = osg::RadiansToDegrees(atan2(y, x));

        if(bearing < 0) bearing = fmod((bearing+ 360), 360);

        return bearing;
    }

    DEFINE_IREFERENCED(CheckpointPathTourComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, CheckpointPathTourComponent);

    CheckpointPathTourComponent::CheckpointPathTourComponent() 
        :_currentPathController(NULL)
    {
        _addInterface(SMCElements::ICheckpointPathTourComponent::getInterfaceName());
    }

    CheckpointPathTourComponent::~CheckpointPathTourComponent()
    {}

    void CheckpointPathTourComponent::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            CORE::IComponent* component = ELEMENTS::GetComponentFromMaintainer("AnimationComponent");
            if(component)
            {
                _animationComponent = component->getInterface<CORE::IAnimationComponent>();
            }

            component = ELEMENTS::GetComponentFromMaintainer("CameraComponent");
            if(component)
            {
                _cameraComponent = component->getInterface<ELEMENTS::ICameraComponent>();
            }
        }
        else if(messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            //! play
            if(!_cameraComponent.valid())
                return;

            if(!_currentPathController.valid())
                return;

            CORE::IPoint* point = _currentPathController->getInterface<CORE::IPoint>();

            ELEMENTS::ITimeBasedCheckpointPath* cpPath = 
                _currentPathController->getPath()->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();

            //! Position
            osg::Vec3d longLatAlt = point->getValue();

            //! Range
            double range = point->getZ() + (point->getInterface<CORE::IObject>()->getOSGNode()->getBound().radius()*2) + 400;

            //! Heading
            osg::Vec3d futurePoint = cpPath->getFuturePoint();
            futurePoint = UTIL::CoordinateConversionUtils::ECEFToGeodetic(futurePoint);
            futurePoint = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(futurePoint);
            double heading2 = bearingBetweenPoints(longLatAlt, futurePoint);

            double pitch = -60;

            _cameraComponent->setViewPoint(longLatAlt, range, heading2, pitch);

        }
    }

    void CheckpointPathTourComponent::onAddedToWorldMaintainer()
    {
        CORE::IWorldMaintainer* maintainer = CORE::WorldMaintainer::instance();

        _subscribe(maintainer, *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(maintainer, *CORE::IWorld::WorldRemovedMessageType);
    }

    void CheckpointPathTourComponent::onRemovedFromWorldMaintainer()
    {
        CORE::IWorldMaintainer *maintainer = getWorldMaintainer();

        _unsubscribe(maintainer, *CORE::IWorld::WorldLoadedMessageType);
        _unsubscribe(maintainer, *CORE::IWorld::WorldRemovedMessageType);
    }

    void CheckpointPathTourComponent::reset()
    {
        _currentPathController = NULL;

        //! Reset animation component time
        if(_animationComponent.valid())
        {
            _animationComponent->setStartEndDateTime(_previousStartTime, _previousEndTime);
            _animationComponent->setCurrentDateTime(_previousCurrentTime);
        }

        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        //! send Tour ended message
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ICheckpointPathTourComponent::CPTourEndedMessageType);
        msg->setSender(this);

        notifyObservers(*ICheckpointPathTourComponent::CPTourEndedMessageType, *msg, CORE::IObservable::ASYNCHRONOUS);
        
    }

    unsigned int CheckpointPathTourComponent::getElapsedTime()
    {
        unsigned int elapsedTime = 0;

        if(_animationComponent.valid())
        {
            //Get current time
            boost::posix_time::ptime currentTime = _animationComponent->getCurrentDateTime();

            //! Get path
            CORE::RefPtr<ELEMENTS::IPath> path = _currentPathController->getPath();
            if(path.valid())
            {
                ELEMENTS::ITimeBasedCheckpointPath* cpPath = path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
                if(cpPath)
                {
                    const ELEMENTS::CheckpointList& cpList = cpPath->getCheckpointList();
                    boost::posix_time::ptime startTime = (*(cpList.begin()))->getArrivalTime();

                    elapsedTime = (currentTime - startTime).total_seconds();
                }
            }
        }

        return elapsedTime;
    }

    unsigned int CheckpointPathTourComponent::getTotalTime()
    {
        unsigned int totalTime = 0;

        if(_currentPathController.valid())
        {
            //! Get path
            CORE::RefPtr<ELEMENTS::IPath> path = _currentPathController->getPath();

            if(path.valid())
            {
                ELEMENTS::ITimeBasedCheckpointPath* cpPath = path->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
                if(cpPath)
                {
                    const ELEMENTS::CheckpointList& cpList = cpPath->getCheckpointList();
                    boost::posix_time::ptime startTime = (*(cpList.begin()))->getArrivalTime();
                    boost::posix_time::ptime endTime = (*(cpList.back())).getArrivalTime();

                    totalTime = (endTime - startTime).total_seconds();
                }
            }
        }

        return totalTime;
    }

    void CheckpointPathTourComponent::startTourForPath(vizElements::IPathController* pathController)
    {
        if(!pathController)
            return;

        _currentPathController = pathController;

        //! Get path
        CORE::RefPtr<ELEMENTS::IPath> pathObject = pathController->getPath();
        if(!pathObject.valid())
            return;

        //! get time based checkpoint path interface
        ELEMENTS::ITimeBasedCheckpointPath* cpPath = pathObject->getInterface<ELEMENTS::ITimeBasedCheckpointPath>();
        if(!cpPath)
            return;

        //! store previous values
        _previousStartTime = _animationComponent->getStartDateTime();
        _previousEndTime = _animationComponent->getEndDateTime();
        _previousCurrentTime = _animationComponent->getCurrentDateTime();

        //! subscribe to tick message
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _subscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        //! send Tour Started message
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ICheckpointPathTourComponent::CPTourStartedMessageType);
        msg->setSender(this);

        notifyObservers(*ICheckpointPathTourComponent::CPTourStartedMessageType, *msg, CORE::IObservable::ASYNCHRONOUS);

        //! set Start/end time 
        //! Get Checkpoint list
        const ELEMENTS::CheckpointList& cpList = cpPath->getCheckpointList();
        boost::posix_time::ptime startTime = (*(cpList.begin()))->getArrivalTime();
        boost::posix_time::ptime endTime = (*(cpList.back())).getArrivalTime();
        _animationComponent->setStartEndDateTime(startTime, endTime);

        //! create an inset view

        _animationComponent->play();
    }

}