#include <SMCElements/ResourceManagerComponent.h>
#include <Core/WorldMaintainer.h>
#include <App/AccessElementUtils.h>
#include <Elements/IMilitarySymbol.h>

namespace SMCElements
{
    DEFINE_IREFERENCED(ResourceManagerComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, ResourceManagerComponent);

    ResourceManagerComponent::ResourceManagerComponent()
    {
        _addInterface(SMCElements::IResourceManagerComponent::getInterfaceName());
    }


    ResourceManagerComponent::~ResourceManagerComponent()
    {

    }

    void ResourceManagerComponent::onAddedToWorldMaintainer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = getWorldMaintainer();
        _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);
    }

    void ResourceManagerComponent::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            // load resources from project file
        }
        else if(messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            clearResources();
        }
    }

    void ResourceManagerComponent::clearResources()
    {
        _mapResourceToInstances.clear();
    }

    void ResourceManagerComponent::addResource(const std::string& fullResourceName)
    {
        MapResourceToInstances::iterator iter = _mapResourceToInstances.find(fullResourceName);

        if(iter != _mapResourceToInstances.end())
        {
            iter->second++;
        }
        else
        {
            _mapResourceToInstances[fullResourceName] = 1;
        }
    }

    const SMCElements::IResourceManagerComponent::MapResourceToInstances &
    ResourceManagerComponent::getResourceMap() const
    {
        return _mapResourceToInstances;
    }

}   // namespace SMCElements
