/*****************************************************************************
 *
 * File             : SocketListner.cpp
 * Description      : SocketListner class definition
 * 
 *****************************************************************************
 * Copyright (c) 2010-2011, VizExperts India Pvt. Ltd.                                       
 *****************************************************************************/
//Network
#include <SMCElements/SocketListner.h>

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include "wtypes.h"

int horizontal, vertical;


#define buflen 148 

///////////////////////////////////////////////////////////////////////////
//
//! \brief  Constructor
//
///////////////////////////////////////////////////////////////////////////

SocketListner::SocketListner(std::string IP, int PORT, std::string name)
    :_IP(IP)
    ,_port(PORT)
    ,_name(name)
    ,_prevHoriz(0)
    ,_prevVert(0)
{
    //sent command to execution command map
   _msgToAppMap.clear();
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  Destructor
//
///////////////////////////////////////////////////////////////////////////

SocketListner::~SocketListner()
{
    //_done = true;
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  initialize
//
///////////////////////////////////////////////////////////////////////////

bool 
SocketListner::initialize()
{
    //start the listening thread
    _done = false;
    start();

    return true;
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  TriggerMipsOnResolution
//
///////////////////////////////////////////////////////////////////////////

void
SocketListner::TriggerMipsOnResolution()
{
   // sometimes when user switches between 2D and 3D mode some mips dont come online
    // this is a bug with MIPS 
    // to overcome this problem we force the MIPS units to readjust whenever resolution change is detected
    // this code may not be needed if MIPS gets fixed.

   RECT desktop;

   int horizontal, vertical;

   // Get a handle to the desktop window
   const HWND hDesktop = GetDesktopWindow();
   // Get the size of screen to the variable desktop
   GetWindowRect(hDesktop, &desktop);
   // The top left corner will have coordinates (0,0)
   // and the bottom right corner will have coordinates
   // (horizontal, vertical)
   //get current resolution
   horizontal = desktop.right;
   vertical = desktop.bottom;
    
   //compare with the previous resolution
   if(_prevHoriz == 0 || _prevVert == 0)
   {
       _prevHoriz = horizontal;
       _prevVert = vertical;
   }

   //if resolution change is detected then connect to each MIPS and send command for reset
   // !! Important : Use valid MIPS ip here if IP is different then this code wont work
   // currently IP's are hardcoded this can be modified to read from command line or text file.
   
   if(horizontal != _prevHoriz || vertical != _prevVert)
   {
       Sleep(3000);
#if 0
       connectToMIPS("192.168.1.200");
       connectToMIPS("192.168.1.201");
       connectToMIPS("192.168.1.202");
       connectToMIPS("192.168.1.203");
       connectToMIPS("192.168.1.204");
       connectToMIPS("192.168.1.205");
#endif 
       _prevHoriz = horizontal;
       _prevVert = vertical;
       std::cout<<"resolution changed trigerring "<<horizontal<<"x"<<vertical<<std::endl;
   }
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  connectToMIPS
//
///////////////////////////////////////////////////////////////////////////

bool
SocketListner::connectToMIPS(std::string IP)
{
    if(IP.length() == 0)
    {
        std::cout<<"invalid partner ip\n";
        return false;
    }
    
    unsigned int mipsConnectSocket = socket(PF_INET, SOCK_STREAM,0);

    if(mipsConnectSocket == INVALID_SOCKET)
    {
        std::cout << "failed intiating socket with partner" <<std::endl;
        return false;
    }

    struct sockaddr_in thataddr;

    memset(&thataddr,0,sizeof(thataddr));
    
    thataddr.sin_addr.s_addr = inet_addr(IP.c_str());
    thataddr.sin_family = AF_INET;
    thataddr.sin_port = htons(40400);
    
    int i = connect(mipsConnectSocket,(struct sockaddr *)&thataddr,sizeof(thataddr));
    
    if(i<0)
    {
        //i = WSAGetLastError();
        std::cout << "failed connecting to mips" <<IP.c_str()<<std::endl;
        return false;
    }
    else
    {
        std::cout << "connected to mips" <<IP.c_str()<<std::endl;
    }

    bool nodelayval=true;
    setsockopt(mipsConnectSocket, IPPROTO_TCP, TCP_NODELAY, (const char *) &nodelayval, sizeof(BOOL));
    
    //command to ask mips to get online again
    std::string data = ":mode 0";

    const char *pBuffer = data.c_str();

    int bytes = data.length() + 1;
    
    i = 0;
    int j;
    while (i < bytes)
    {
        j = send(mipsConnectSocket, pBuffer+i, bytes-i, 0);
        if(j<0)
        {
            break;
        }
        i += j;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  populateMap
//
///////////////////////////////////////////////////////////////////////////

void 
SocketListner::populateMap(std::string msg, std::string app)
{
    _msgToAppMap.insert(std::pair<std::string, std::string>(msg, app));
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  isDone
//
///////////////////////////////////////////////////////////////////////////

bool 
SocketListner::isDone()
{
    return _done;
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  quit
//
///////////////////////////////////////////////////////////////////////////

void 
SocketListner::quit()
{
    _done = true;
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  run
//
///////////////////////////////////////////////////////////////////////////

void 
SocketListner::run()
{
    WSADATA wsaData;

    if(WSAStartup(0x101,&wsaData)!=0)
        return;
    
    //check if this thread is just a resolution detect trigger then keep looping to detect resolution and dont use network
    if(_name == "ResolutionDetect")
    {
        //set MIPS env to control triggering
        putenv ("STOP_MIPS_TRIGGER=false");

        while(!_done) 
        {
            mipsTriggerEnv = getenv ("STOP_MIPS_TRIGGER");
            
            //if MIPS trigger environment variable is set to true then dont trigger MIPS
            if(std::string(mipsTriggerEnv) != "true")
            {
                TriggerMipsOnResolution();
            }

            //wait for 100 miliseconds before next trigger
            Sleep(100);
        }
        
        WSACleanup();

        return;
    }
    
    //create listening socket to listen commands on the wall system
    if(_IP.length() <= 0)
    {
        std::cout << "invalid host ip" <<std::endl;
        return;
    }
    
    sockaddr_in SockAddr, ClAddr;

    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = htons(_port);
    SockAddr.sin_addr.s_addr =  inet_addr(_IP.c_str());;

    s_sock = socket(AF_INET,SOCK_STREAM,0);
    
    if(s_sock == INVALID_SOCKET)
    {
        _done = true;
        return;
    }

    if(bind(s_sock,(sockaddr*)&SockAddr,sizeof(SockAddr))!=0)
    {
        _done = true;
        return;
    }
    
    if(listen(s_sock,10)!=0)
    {
        _done = true;
        return;
    }

    std::cout<<"ready on: "<<_port<<std::endl;

    //If thread is AMXListner thread then connect to MIPS for sending 2D 3D command
    // firt time when AMX Listner connects reset MIPS 
    // XXX : this is probably a hack need to verify 
    // doesnt cause any harm to the system

    if(_name == "AMXListner")
    {
#if 0
        connectToMIPS("192.168.1.200");
        connectToMIPS("192.168.1.201");
        connectToMIPS("192.168.1.202");
        connectToMIPS("192.168.1.203");
        connectToMIPS("192.168.1.204");
        connectToMIPS("192.168.1.205");
#endif 
    }

    int Clength=sizeof(ClAddr);
    
    while(!_done)
    {
        c_sock = accept(s_sock,(struct sockaddr*)&ClAddr,&Clength);
        
        char msg[buflen];
        std::string arg, Msg;

        std::cout<<"connected on: "<<_port<<std::endl;

        while(!_done) 
        {
            int i = recv(c_sock,msg, buflen, 0);
            
            //std::cout<<i<<std::endl;

            if(i<=0)  //loop until GUI client sends requests
            {
                break;
            }
                    
            Msg = std::string(msg);

            std::cout<<WSAGetLastError()<<std::endl;

            std::cout<<Msg.c_str()<<std::endl;
            
            std::map<std::string, std::string>::iterator iter = _msgToAppMap.begin();
            
            //check what message is recieved on the socket and act accordingly
            while(iter != _msgToAppMap.end())
            {
                if(Msg.find(iter->first) != std::string::npos)
                {
                    std::string app = iter->second;
                    std::cout<<"launching: "<<app.c_str()<<std::endl;
                    
                    //if message is a shutdown message the execute system shutdown
                    // this can be changed to hibernate or code can be modified so that on recieving shutdown command a batch script is run
                    if(Msg.find("SHUTDOWN") != std::string::npos)
                    {
                        _done = true;
                        system("shutdown /f /h");
                        break;
                    }

                    //if the message is to start DSM then use incoming message as scenario name and run the batch script for DSM with msg as argument 
                    if(Msg.find("DSTLauncher") != std::string::npos)
                    {
                        Sleep(2000);
                        std::string DSTcmd = app+" "+Msg;
                        system(DSTcmd.c_str());
                    }
                    else
                    {
                        system(app.c_str());
                    }
                    
                    //if AMX controller has initiated 2D 3D mode switching then stop MIPS trigger and connect to MIPS to reset it.
                    // resume the trigger after reset 
                    // trigger needs to be resumed as sometimes change of resolution can be through some application and not necessarily through 2D 3D switching
                    if(iter->first == "3DMODE" || iter->first == "2DMODE")
                    {
                        putenv ("STOP_MIPS_TRIGGER=true");

                        //wait for 8 seconds before desktop resolution has actually reset, projectors have switched mode, DVI switcher has switched inputs 
                        Sleep(8000);
                        connectToMIPS("192.168.1.200");
                        connectToMIPS("192.168.1.201");
                        connectToMIPS("192.168.1.202");
                        connectToMIPS("192.168.1.203");
                        connectToMIPS("192.168.1.204");
                        connectToMIPS("192.168.1.205");

                        putenv ("STOP_MIPS_TRIGGER=false");
                    }
                    iter = _msgToAppMap.end();
                }
                else
                {
                    iter++;
                }
            }
            
            Msg = "";

            Sleep(200);
        }
    }
    closesocket(c_sock);
    closesocket(s_sock);
    WSACleanup();

    _done = true;
}