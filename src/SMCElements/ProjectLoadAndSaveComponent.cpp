#include <SMCElements/ProjectLoadAndSaveComponent.h>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <ZipUtil/ZipAndUnzipUtil.h> 
#endif//WIN32

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>

#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>

#include <Core/AttributeUtils.h>
#include <Core/AttributeTypes.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMessageType.h>

#include <serialization/ObjectWrapper.h>
#include <serialization/InputStream.h>
#include <serialization/OutputStream.h>
#include <DB/WriteFile.h>
#include <DB/ReadFile.h>
#include <Curl/EasyCurl.h>

#include <Elements/ISettingComponent.h>

#include <SMCElements/IProjectLoadAndSaveComponent.h>

namespace SMCElements
{
    DEFINE_IREFERENCED(ProjectLoadAndSaveComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, ProjectLoadAndSaveComponent);

    ProjectLoadAndSaveComponent::ProjectLoadAndSaveComponent():_mode(IProjectLoadAndSaveComponent::SERVER)
    {
        _addInterface(SMCElements::IProjectLoadAndSaveComponent::getInterfaceName());
    }

    ProjectLoadAndSaveComponent::~ProjectLoadAndSaveComponent()
    {
    }

    //LocationToSave -> the location/Weblink where the Zip file need to be saved
    //Project --> is the location where the File contents are present need to be zipped
    //Filename ->meaning just the name not extension
    void
    ProjectLoadAndSaveComponent::saveProject(const std::string& fileName, 
                                             const std::string& projectFileOrFolder, 
                                             const std::string& locationToSave, 
                                             const bool& needToZip)
    {
        //Path has the project files to save
        std::string projectPath = osgDB::convertFileNameToNativeStyle(projectFileOrFolder);

        //This contain the File name which need to be uploaded
        std::string fileToSave = projectPath;

        //Contain the location on the server where data need to be uploaded
        std::string fileLocationToSave = locationToSave;

        osgDB::FileType type = osgDB::fileType(projectPath);
        if(type != osgDB::DIRECTORY)
        {
            projectPath = osgDB::getFilePath(projectPath);
        }

        if(needToZip)
        {
            std::string zipFile = osgDB::getNameLessAllExtensions(fileName) + ".zip";
            fileToSave =  projectPath + "/" + zipFile;
            fileLocationToSave = locationToSave + "/" + zipFile;

            _zipProject(fileToSave, projectPath);
        }

        if(_mode == IProjectLoadAndSaveComponent::SERVER)
        {
            _uploadOnServer(fileToSave, fileLocationToSave);
        }
    }

    //locationToLoad -> the location where the Zip file need to be unzipped
    //projectZip --> is the location of the Zip File /  or the WebLink to download from Webserver
    void
    ProjectLoadAndSaveComponent::loadProject(const std::string& projectFile, const std::string& location)
    {

        std::string fileName;
        std::string locationToLoad = location;

        //Check whether the Location to Load is Directory or a file 
        osgDB::FileType type = osgDB::fileType(location);
        if(type != osgDB::DIRECTORY)
        {
            locationToLoad = osgDB::getFilePath(location);
        }

        if(getMode()==SERVER)
        {
            fileName = locationToLoad + "/" + osgDB::getSimpleFileName(projectFile);
            _downloadFromServer(fileName, projectFile);
        }

        if(osgDB::getLowerCaseFileExtension(fileName) == "zip")
        {
            _extractZippedFile(fileName, locationToLoad);
        }
    }

    void
    ProjectLoadAndSaveComponent::_uploadOnServer(const std::string& projectZip, const std::string& link)
    {
        CORE::RefPtr<EasyCurl::EasyCurl> easyCurl = new EasyCurl::EasyCurl;

        EasyCurl::EasyCurl::Options* options = new EasyCurl::EasyCurl::Options;
        
        options->username = _webserverUsername;
        options->password = _webserverPassword;

        easyCurl->write(link, projectZip, options);

        if(easyCurl.valid())
            easyCurl = NULL;

        delete options;
    }

    bool ProjectLoadAndSaveComponent::saveDefaultTemplate(const std::string& fileLocation, const std::string& link){
        
        CORE::RefPtr<EasyCurl::EasyCurl> easyCurl = new EasyCurl::EasyCurl;

        EasyCurl::EasyCurl::Options* options = new EasyCurl::EasyCurl::Options;

        options->username = _webserverUsername;
        options->password = _webserverPassword;
        if (!osgDB::fileExists(fileLocation))
            return false;

        EasyCurl::EasyCurl::ReaderWriterStatus status = easyCurl->write(link, fileLocation, options);

        if (easyCurl.valid())
            easyCurl = NULL;

        delete options;

        if (status == EasyCurl::EasyCurl::ReaderWriterStatus::FILE_SAVED || status == EasyCurl::EasyCurl::ReaderWriterStatus::FILE_LOADED)
            return true;

        else
            return false;

    }

    void ProjectLoadAndSaveComponent::downloadServerSettingFile(const std::string& toSaveLocation, const std::string& link){
        CORE::RefPtr<EasyCurl::EasyCurl> easyCurl = new EasyCurl::EasyCurl;

        EasyCurl::EasyCurl::Options* options = new EasyCurl::EasyCurl::Options;

        options->username = _webserverUsername;
        options->password = _webserverPassword;

        std::stringstream stringbuffer;
        EasyCurl::EasyCurl::StreamObject sp(&stringbuffer, toSaveLocation);

        easyCurl->read(std::string(""), link, sp, toSaveLocation, NULL);

        if (easyCurl.valid())
            easyCurl = NULL;

        delete options;
    }

    void
    ProjectLoadAndSaveComponent::_getWebServerNameAndPassword()
    {
        //Get the reference of the Setting Component
        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = CORE::WorldMaintainer::instance();
        CORE::RefPtr<CORE::IComponent> component = worldMaintainer->getComponentByName("SettingComponent");
        ELEMENTS::ISettingComponent* globalSettingComponent = component->getInterface<ELEMENTS::ISettingComponent>();

        //Get the location of the project on the Server
        ELEMENTS::ISettingComponent::SettingsAttributes presentWebServerSetting = 
            globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        ELEMENTS::ISettingComponent::SettingsAttributes setting = globalSettingComponent->getGlobalSetting(ELEMENTS::ISettingComponent::WEBSERVER);

        if(setting.keyValuePair.find("Username") != setting.keyValuePair.end())
            _webserverUsername = setting.keyValuePair["Username"];

        if(setting.keyValuePair.find("Password") != setting.keyValuePair.end())
            _webserverPassword = setting.keyValuePair["Password"];
    }

    //Link is the link of the FileName to down from Server
    void
    ProjectLoadAndSaveComponent::_downloadFromServer(const std::string& projectFile,  const std::string& link)
    {
        CORE::RefPtr<EasyCurl::EasyCurl> easyCurl = new EasyCurl::EasyCurl;

        EasyCurl::EasyCurl::Options* options = new EasyCurl::EasyCurl::Options;
        
        options->username = _webserverUsername;
        options->password = _webserverPassword;

        std::stringstream stringbuffer;
        EasyCurl::EasyCurl::StreamObject sp(&stringbuffer, projectFile);

        easyCurl->read(std::string(""), link, sp, projectFile, NULL);

        if(easyCurl.valid())
            easyCurl = NULL;

        delete options;
    }

    //Lcoation is the Folder to ZIP 
    void 
    ProjectLoadAndSaveComponent::_zipProject(const std::string& zipFileName, const std::string& location)
    {
        if(location.empty())
            return;

        std::string projectFolder = osgDB::convertFileNameToNativeStyle(location);
        osgDB::DirectoryContents contents  = osgDB::getDirectoryContents(projectFolder);
        int size = contents.size();

        std::vector<std::string> folderContent;

        int index = 0;
        while(index < size)
        {
            std::string fileName = contents[index++];
            if((fileName == "..") || (fileName == "."))
                continue;

            std::string fileToUpload = projectFolder + "/" + fileName;

            folderContent.push_back(fileToUpload);
        }
        // copy contents to the zip file

        ZipUtil::ZipAndUnzipUtil::addFilesToZip(folderContent, zipFileName);
#ifdef WIN32
//        ZipUtil::ZipAndUnzipUtil::addFilesToZip(folderContent, zipFileName);
#endif //WIN32
    }

    //Location here is where to extract the ZipFile
    void 
    ProjectLoadAndSaveComponent::_extractZippedFile(const std::string& zipFile, const std::string& location)
    {
        std::string zipFileLocation = osgDB::convertFileNameToNativeStyle(zipFile);
        std::string pathToExtract = osgDB::convertFileNameToNativeStyle(location);
        ZipUtil::ZipAndUnzipUtil::unzipFile(zipFileLocation, pathToExtract);
#ifdef WIN32
  //      ZipUtil::ZipAndUnzipUtil::unzipFile(zipFileLocation, pathToExtract);
#endif //WIN32
    }

    void 
    ProjectLoadAndSaveComponent::initializeAttributes()
    {
        Component::initializeAttributes();

        /*_addAttribute(new CORE::BooleanAttribute("isLocalProject", "isLocalProject",
                        CORE::BooleanAttribute::SetFuncType(this, &ProjectLoadAndSaveComponent::setIsProjectLocal),
                        CORE::BooleanAttribute::GetFuncType(this, &ProjectLoadAndSaveComponent::getIsProjectLocal),
                        "isLocalProject",
                        "ProjectLoadAndSaveComponent attributes"));*/

        //std::string groupName = "ProjectDatabase";
        //_addAttribute(new CORE::GroupAttribute("ProjectDatabase", "ProjectDatabase",
        //                CORE::GroupAttribute::SetFuncType(this, &SettingComponent::addProjectDatabaseCredential),
        //                CORE::GroupAttribute::GetFuncType(this, &SettingComponent::setProjectDatabaseCredential),
        //                std::string("ProjectDatabase"), groupName));

        _getWebServerNameAndPassword();
         
    }

    bool
    ProjectLoadAndSaveComponent::isWebserverAlive(const std::string& link)
    {
        CORE::RefPtr<EasyCurl::EasyCurl> easyCurl = new EasyCurl::EasyCurl;

        EasyCurl::EasyCurl::Options* options = new EasyCurl::EasyCurl::Options;
        
        options->username = _webserverUsername;
        options->password = _webserverPassword;

        bool isAlive = false;
        isAlive =  easyCurl->isWebserverReachable(link, options);

        if(easyCurl.valid())
            easyCurl = NULL;

        delete options;

        return isAlive;

    }

    void 
    ProjectLoadAndSaveComponent::setMode(IProjectLoadAndSaveComponent::Mode mode)
    {
        _mode = mode;
    }

    IProjectLoadAndSaveComponent::Mode 
    ProjectLoadAndSaveComponent::getMode()
    {
        return _mode;
    }
}
