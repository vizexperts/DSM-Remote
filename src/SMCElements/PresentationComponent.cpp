#include <SMCElements/PresentationComponent.h>

#include <Core/WorldMaintainer.h>

#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>

namespace SMCElements
{
    DEFINE_IREFERENCED(PresentationComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, PresentationComponent);

    PresentationComponent::PresentationComponent()
    {
        _addInterface(SMCElements::IPresentationComponent::getInterfaceName());
    }

    PresentationComponent::~PresentationComponent()
    {
    }

    void 
    PresentationComponent::addPdfDocument(const std::string& documentName, const std::string& documentPath)
    {
        if(documentName.empty() && documentPath.empty())
            return;

        std::string projectPdfLocation = documentPath;

        bool fileExist = osgDB::fileExists(projectPdfLocation);
        if(!fileExist)
        {
            //Check Whether the PDF is present inside the Project Folder created in TEMP
            CORE::IWorldMaintainer *wmain = CORE::WorldMaintainer::instance();
            std::string projectFile = wmain->getProjectFile();

            projectPdfLocation = osgDB::getFilePath(projectFile);
            projectPdfLocation += "/" + documentPath;
            projectPdfLocation = osgDB::convertFileNameToNativeStyle(projectPdfLocation);

            fileExist = osgDB::fileExists(projectPdfLocation);
        }
        else
        {
            _copyPdfDocumentInProjectFolder(projectPdfLocation);
        }

        if(!fileExist)
            return;

        _mapPdf[documentName] = projectPdfLocation;
        _mapPdfAndRelativePath[documentName] = osgDB::getSimpleFileName(documentPath);
    }

    void PresentationComponent::deletePdfDocument(const std::string& documentName)
    {
        if(documentName.empty())
        {
            return;
        }

#ifdef WIN32
        const pdfMap::const_iterator iter1 = _mapPdf.find(documentName);
#else //WIN32
        pdfMap::iterator iter1 = _mapPdf.find(documentName);
#endif //WIN32
        if(iter1 != _mapPdf.end())
        {
            _mapPdf.erase(iter1);
        }

#ifdef WIN32
        const pdfMap::const_iterator iter2 = _mapPdfAndRelativePath.find(documentName);
#else //WIN32
        pdfMap::iterator iter2 = _mapPdfAndRelativePath.find(documentName);
#endif // WIN32
        if(iter2 != _mapPdfAndRelativePath.end())
        {
            _mapPdfAndRelativePath.erase(iter2);
        }

    }

    const SMCElements::IPresentationComponent::pdfMap&
    PresentationComponent::getPdfMapCollection() const
    {
        return _mapPdf;
    }

    const SMCElements::IPresentationComponent::pdfMap&
    PresentationComponent::getPdfMapToSave() const
    {
        return _mapPdfAndRelativePath;
    }

    void
    PresentationComponent::reset()
    {
        _mapPdf.clear();
    }

    //Move the Pdf to the Project Directory which is presently created in Temp Folder
    bool
    PresentationComponent::_copyPdfDocumentInProjectFolder(const std::string& pdfPath)
    {
        if(!osgDB::fileExists(pdfPath))
            return false;

        CORE::IWorldMaintainer *wmain = CORE::WorldMaintainer::instance();
        std::string projectFile = wmain->getProjectFile();
        std::string destinationFile = osgDB::getFilePath(projectFile) + "/" + osgDB::getSimpleFileName(pdfPath);
        destinationFile = osgDB::convertFileNameToNativeStyle(destinationFile);

        osgDB::copyFile(pdfPath, destinationFile) ;

        return true;
    }

    void 
    PresentationComponent::onAddedToWorldMaintainer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = getWorldMaintainer();
        _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);
    }

    void
    PresentationComponent::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        
        if(messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            reset();
        }
    }
}
