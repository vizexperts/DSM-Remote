/*****************************************************************************
 *
 * File             : WallLauncher.cpp
 * Description      : WallLauncher class definition
 * 
 *****************************************************************************
 * Copyright (c) 2010-2011, VizExperts India Pvt. Ltd.                                       
 *****************************************************************************/
#ifdef WIN32
//DMS Network
#include <SMCElements/WallLauncher.h>

#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

///////////////////////////////////////////////////////////////////////////
//
//! \brief  Constructor
//
///////////////////////////////////////////////////////////////////////////

WallLauncher::WallLauncher(std::string IP,int PORT)
    :_IP(IP)
    ,_port(PORT)
    ,_done(false)
{
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  Destructor
//
///////////////////////////////////////////////////////////////////////////

WallLauncher::~WallLauncher()
{
    stop();

    closesocket(c_sock);
    closesocket(s_sock);
    WSACleanup();
}

bool 
WallLauncher::startListening()
{
    if(_initialize())
    {
        start();
        return true;
    }
    return false;
}

bool 
WallLauncher::hasNext()
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
    return (!_requests.empty());
}

std::string 
WallLauncher::fetchNextRequest()
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
    if(!_requests.empty())
    {
        std::string nextReq = _requests.front();
        _requests.pop();
        return nextReq;
    }
    return "";
}

///////////////////////////////////////////////////////////////////////////
//
//! \brief  run
//
///////////////////////////////////////////////////////////////////////////

void 
WallLauncher::run()
{        
    sockaddr_in ClAddr;
    int Clength=sizeof(ClAddr);
    c_sock = accept(s_sock,(struct sockaddr*)&ClAddr,&Clength);
    
    if(c_sock != INVALID_SOCKET)
    {
        std::cout<<"Socket is connected "<<std::endl;
        // Make the socket Nonblocking
        u_long iMode=1;
        int retCode=ioctlsocket(c_sock, FIONBIO, &iMode); 
        
        if(retCode == 0)
        {
            const int requestBufferLength = 500;
            
            while(!_done) 
            {    
                char msg[requestBufferLength];
                
                retCode=recv(c_sock, msg, requestBufferLength, 0);    //loop until GUI client sends requests
                
                if(retCode == 0 || WSAGetLastError()==WSAECONNRESET)
                {
                    //BugFix: Abhishek- adding shutdown as socket was not closing properly before
                    shutdown(c_sock,SD_BOTH);
                    shutdown(s_sock,SD_BOTH);
                    closesocket(c_sock);
                    closesocket(s_sock);
                    int error = WSAGetLastError();
                    if(!_initialize())
                    {
                        std::cout << "Failed to initialize the socket. Restart the process.\n";
                        _done = true;
                        return;
                    }
                    c_sock = accept(s_sock,(struct sockaddr*)&ClAddr,&Clength);
    
                    if(c_sock == INVALID_SOCKET)
                    {
                        std::cout << "Failed to accept the connection. Restart the process.\n";
                        _done = true;
                        return;
                    }
                }
                
                if(retCode == SOCKET_ERROR)
                {
                    int errorCode = WSAGetLastError();
                    _processSocketError(errorCode);
                    continue;
                }

                if(retCode > 0)
                {
                    char actualMessage[1024] = "";
                    memcpy(actualMessage, msg, retCode);
                    actualMessage[retCode] = '\0';
                    std::cout<<"Message received is "<<actualMessage<<std::endl;
                    std::string Msg = std::string(actualMessage);
                
                    if(Msg.length() != 0)
                    {
                        OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
                        _requests.push(Msg);
                    }
                }
            }
            
            OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
            _cleanUpCondition.signal();

            return;
        }
    }

    // If any failure occurs make the _bDone  variable to true, as thread is no longer running
    _done = true;
}

void 
WallLauncher::stop()
{
    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
    if(!_done)
    {
        while(!_requests.empty())
            _requests.pop();

        _done = true;
        _cleanUpCondition.wait(&_mutex, 3000);
    }
}


///////////////////////////////////////////////////////////////////////////
//
//! \brief  initialize
//
///////////////////////////////////////////////////////////////////////////

bool 
WallLauncher::_initialize()
{
    if(_IP.length() <= 0)
    {
        // LOG_ERROR("Host IP Address : " + _IP + " is invalid.");
        return false;
    }
    
    WSADATA wsaData;

    if(WSAStartup(0x101,&wsaData)!=0)
        return false;
    
    sockaddr_in SockAddr;

    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = htons(_port);
    SockAddr.sin_addr.s_addr =  inet_addr(_IP.c_str());;

    s_sock = socket(AF_INET,SOCK_STREAM,0);
    bool addrReuse=true;
    setsockopt(s_sock, SOL_SOCKET, SO_REUSEADDR, (const char *) &addrReuse, sizeof(BOOL));
    
    if(s_sock == INVALID_SOCKET)
        return false;

    if(bind(s_sock,(sockaddr*)&SockAddr,sizeof(SockAddr))!=0)
    {
        int error = WSAGetLastError();
        return false;
    }
    
    if(listen(s_sock,10)!=0)
    {
        return false;
    }
    
    std::cout<<"Sockert started listneing! "<<std::endl;

    return true;
}

void
WallLauncher::_processSocketError(int errorCode)
{
    switch(errorCode)
    {
        case WSANOTINITIALISED:
        {
            std::cout << "WSANOTINITIALISED.";
        }
        break;
        case WSAENETDOWN:
        {
            std::cout << "WSAENETDOWN.";
        }
        break;
        case WSAEFAULT:
        {
            std::cout << "WSAEFAULT.";
        }
        break;
        case WSAENOTCONN:
        {
            std::cout << "WSAENOTCONN.";
        }
        break;
        case WSAEINTR:
        {
            std::cout << "WSAEINTR.";
        }
        break;
        case WSAEINPROGRESS:
        {
            std::cout << "WSAEINPROGRESS.";
        }
        break;
        case WSAENETRESET:
        {
            std::cout << "WSAENETRESET.";
        }
        break;
        case WSAENOTSOCK:
        {
            std::cout << "WSAENOTSOCK.";
        }
        break;
        case WSAEOPNOTSUPP:
        {
            std::cout << "WSAEOPNOTSUPP.";
        }
        break;
        case WSAESHUTDOWN:
        {
            std::cout << "WSAESHUTDOWN.";
        }
        break;
        case WSAEWOULDBLOCK:
        {
            // std::cout << "WSAEWOULDBLOCK.\n";
        }
        break;
        case WSAEMSGSIZE:
        {
            std::cout << "WSAEMSGSIZE.";
        }
        break;
        case WSAEINVAL:
        {
            std::cout << "WSAEINVAL.";
        }
        break;
        case WSAECONNABORTED:
        {
            std::cout << "WSAECONNABORTED.";
        }
        break;
        case WSAETIMEDOUT:
        {
            std::cout << "WSAETIMEDOUT.";
        }
        break;
        case WSAECONNRESET:
        {
            std::cout << "WSAECONNRESET.";
        }
        break;
    };
}
#endif //WIN32
