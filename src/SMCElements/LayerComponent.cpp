#include <SMCElements/LayerComponent.h>

#include <Elements/ElementsPlugin.h>
#include <Elements/FeatureLayerUtils.h>

#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>
#include <Core/IText.h>
#include <Core/IWorld.h>
#include <Core/IObjectMessage.h>

#include <App/AccessElementUtils.h>

#include <iostream>

namespace SMCElements{

    DEFINE_META_BASE(SMCElements, LayerComponent);
    DEFINE_IREFERENCED(LayerComponent, CORE::Component);


    LayerComponent::LayerComponent()   
    {
        _addInterface(SMCElements::ILayerComponent::getInterfaceName());
        _addInterface(SMCElements::IDataSourceComponent::getInterfaceName());
    }

    LayerComponent::~LayerComponent()
    {}

    void LayerComponent::onAddedToWorldMaintainer()
    {
        CORE::RefPtr<CORE::IWorldMaintainer> wmain = getWorldMaintainer();

        _subscribe(wmain.get(), *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(wmain.get(), *CORE::IWorld::WorldRemovedMessageType);
    }

    void LayerComponent::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            _layerMap.clear();
            const CORE::WorldMap& worldMap = getWorldMaintainer()->getWorldMap();
            CORE::RefPtr<CORE::IWorld> iworld = NULL;
            if(worldMap.size() > 0)
            {
                iworld = worldMap.begin()->second;
            }
            if(iworld.valid())
            {
                const CORE::ObjectMap& objMap = iworld->getObjectMap();
                CORE::ObjectMap::const_iterator iter = objMap.begin();
                for(; iter != objMap.end(); ++iter)
                {
                    CORE::RefPtr<CORE::IFeatureLayer> layer = 
                        iter->second.get()->getInterface<CORE::IFeatureLayer>();
                    if(layer.valid())
                    {
                        CORE::IBase *base = layer->getInterface<CORE::IBase>();
                        _layerMap[base->getName()] = layer;

                        CORE::UniqueID *uniqueID = &(const_cast<CORE::UniqueID&>(base->getUniqueID()));
                        //_mapNameToID[base->getName()]  =  uniqueID;
                    }

                    CORE::RefPtr<ELEMENTS::IFeatureDataSource> datasource = 
                        iter->second->getInterface<ELEMENTS::IFeatureDataSource>();

                    if(datasource.valid())
                    {
                        CORE::IBase *base = datasource->getInterface<CORE::IBase>();
                        _featureDataSourceMap[base->getName()] = datasource;
                    }
                }

                _subscribe(iworld, *CORE::IObjectMessage::ObjectAddedMessageType);
                _subscribe(iworld, *CORE::IObjectMessage::ObjectRemovedMessageType);
            }
        }
        else if(messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            _layerMap.clear();
            //_mapNameToID.clear();
            _featureDataSourceMap.clear();
        }
        else if(messageType == *CORE::IObjectMessage::ObjectAddedMessageType)
        {
            CORE::IObjectMessage *objectMessage = message.getInterface<CORE::IObjectMessage>();
            if(objectMessage)
            {
                CORE::IObject *object = objectMessage->getObject();
                if(object)
                {
                    CORE::IFeatureLayer *featureLayer = object->getInterface<CORE::IFeatureLayer>();
                    if(featureLayer)
                    {
                        _layerMap[object->getInterface<CORE::IBase>()->getName()] = featureLayer;

                        //const CORE::UniqueID &addedObjectID = object->getInterface<CORE::IBase>()->getUniqueID();

                        ////Checking if the object  has same ID is was created with
                        //MapNameToID::iterator itr = _mapNameToID.begin();
                        //for(; itr!=_mapNameToID.end(); ++itr)
                        //{
                        //    CORE::UniqueID *layerID = itr->second.get();
                        //    if(addedObjectID == *layerID)
                        //    {
                        //        //Means this object is our Object
                        //        _layerMap[object->getInterface<CORE::IBase>()->getName()] = featureLayer;
                        //        break;
                        //    }
                        //}
                    }

                    ELEMENTS::IFeatureDataSource *datasource = object->getInterface<ELEMENTS::IFeatureDataSource>();
                    if(datasource)
                    {
                        _featureDataSourceMap[object->getInterface<CORE::IBase>()->getName()] = datasource;
                    }
                }
            }
        }
        else if(messageType == *CORE::IObjectMessage::ObjectRemovedMessageType)
        {
            CORE::IObjectMessage *objectMessage = message.getInterface<CORE::IObjectMessage>();
            if(objectMessage)
            {
                CORE::IObject *object = objectMessage->getObject();
                if(object)
                {
                    //Checking whether we have this object in our map
                    LayerMap::iterator iteratorToDelete = _layerMap.end();
                    LayerMap::iterator itr                = _layerMap.begin();

                    for(; itr!=_layerMap.end(); ++itr)
                    {
                        if(object==itr->second->getInterface<CORE::IObject>())
                        {
                            iteratorToDelete = itr;
                            break;
                        }
                    }

                    if(iteratorToDelete!=_layerMap.end())
                    {
                        _layerMap.erase(iteratorToDelete);
                    }

                    //Checking whether we have this object in our map
                    FeatureDataSourceMap::iterator iterdelete        = _featureDataSourceMap.end();
                    FeatureDataSourceMap::iterator itrloop            = _featureDataSourceMap.begin();

                    for(; itrloop!=_featureDataSourceMap.end(); ++itrloop)
                    {
                        if(object == itrloop->second->getInterface<CORE::IObject>())
                        {
                            iterdelete = itrloop;
                            break;
                        }
                    }

                    if(iterdelete != _featureDataSourceMap.end())
                    {
                        _featureDataSourceMap.erase(iterdelete);
                    }
                }
            }
        }
    }


    CORE::RefPtr<CORE::IFeatureLayer> LayerComponent::getOrCreateFeatureLayer(const std::string& name, bool addToWorld)
    {
        LayerMap::iterator it;      
        it = _layerMap.find(name);      // find element mapped to name if exists
        CORE::RefPtr<CORE::IFeatureLayer> featureLayer;
        if(it == _layerMap.end())       // if layer is not found
        {
            //create feature layer
            CORE::RefPtr<CORE::IObject> featureLayerObject = createFeatureLayer(name);
            featureLayer = featureLayerObject->getInterface<CORE::IFeatureLayer>();
            //            featureLayer->setLayerAttributeActive("NAME");
            // add it to the map
            _layerMap.insert(std::pair<std::string, CORE::RefPtr<CORE::IFeatureLayer> >(name, featureLayer));

            //add the layer to the world
            if(addToWorld)
            {
                CORE::RefPtr<CORE::IObject> ilobject = featureLayer->getInterface<CORE::IObject>();
                CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                    CORE::WorldMaintainer::instance();
                if(!worldMaintainer.valid())
                {
                    LOG_ERROR("World Maintainer is not valid");
                    return NULL;
                }

                CORE::WorldMap map = worldMaintainer->getWorldMap();
                CORE::WorldMap::iterator itw;
                itw=map.begin();

                CORE::RefPtr<CORE::IWorld> iworld = itw->second;
                iworld->addObject(featureLayerObject.get());  
            }
        }
        else
        {
            featureLayer = it->second;
        }
        return featureLayer;
    }

    CORE::RefPtr<CORE::IFeatureLayer> LayerComponent::getOrCreateFeatureLayer(const std::string& layerName,
        const std::string& layerType,
        std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes,
        std::vector<int>& attrWidths,
        std::vector<int>& attrPrecisions,
        bool addToWorld)
    {
        LayerMap::iterator it;      
        it = _layerMap.find(layerName);      // find element mapped to name if exists
        CORE::RefPtr<CORE::IFeatureLayer> featureLayer;
        if(it == _layerMap.end())       // if layer is not found
        {
            //create feature layer
            CORE::RefPtr<CORE::IObject> featureLayerObject = 
                createFeatureLayer(layerName,layerType,attrNames,attrTypes,attrWidths,attrPrecisions);

            featureLayer = featureLayerObject->getInterface<CORE::IFeatureLayer>();
            // add it to the map

            _layerMap[layerName]     = featureLayer;
            CORE::UniqueID *uniqueID = &(const_cast<CORE::UniqueID&>(featureLayer->getInterface<CORE::IBase>()->getUniqueID()));
            //_mapNameToID[layerName]  =  uniqueID;

            if(addToWorld)
            {
                //add the layer to the world
                CORE::RefPtr<CORE::IObject> ilobject = featureLayer->getInterface<CORE::IObject>();
                CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                    CORE::WorldMaintainer::instance();

                if(!worldMaintainer.valid())
                {
                    LOG_ERROR("World Maintainer is not valid");
                    return NULL;
                }
                CORE::WorldMap map = worldMaintainer->getWorldMap();
                CORE::WorldMap::iterator itw;
                itw=map.begin();

                CORE::RefPtr<CORE::IWorld> iworld = itw->second;
                iworld->addObject(featureLayerObject.get()); 
            }
        }
        else
        {
            featureLayer = it->second;
        }
        return featureLayer;
    }

    CORE::RefPtr<CORE::IObject>
        LayerComponent::_createFeatureDataSource(const std::string& name)
    {
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
            CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IObject> featureDatasourceObject = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureDataSourceType);

        CORE::RefPtr<CORE::IBase> base = 
            featureDatasourceObject->getInterface<CORE::IBase>();


        // set the name of the layer
        base->setName(name);

        return featureDatasourceObject;
    }

    CORE::RefPtr<CORE::IObject> 
        LayerComponent::createFeatureLayer(const std::string& name)
    {
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
            CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return NULL;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent")
                return NULL;
        }

        CORE::RefPtr<CORE::IObject> featureLayerObject = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);
        CORE::RefPtr<CORE::ICompositeObject> featureLayerComposite = NULL;

        if(!featureLayerObject.valid())
        {
            LOG_ERROR("Failed to create FeatureLayer instance");
            return NULL;
        }

        featureLayerComposite = featureLayerObject->getInterface<CORE::ICompositeObject>();

        if(!featureLayerComposite.valid())
        {
            LOG_ERROR("ICompositeObject interface not found in FeatureLayer");
            return NULL;
        }

        CORE::RefPtr<CORE::IBase> featureLayerBase = 
            featureLayerObject->getInterface<CORE::IBase>();

        if(!featureLayerBase.valid())
        {
            LOG_ERROR("IBase interface not found in FeatureLayer");
            return NULL;
        }

        CORE::RefPtr<CORE::IMetadataTableHolder> metadataTableHolder = 
            featureLayerObject->getInterface<CORE::IMetadataTableHolder>();

        if(!metadataTableHolder.valid())
        {
            LOG_ERROR("IMetadataTableHolder interface not found in FeatureLayer");
            return NULL;
        }

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = 
            metadataTableHolder->getMetadataTable();

        if(!metadataTable.valid())
        {
            LOG_ERROR("IMetadataTable interface not found in FeatureLayer");
            return NULL;
        }
        // set the name of the layer
        featureLayerBase->setName(name);

        // set the layer type
        CORE::IFeatureLayer::FeatureLayerType featureLayerType = 
            CORE::IFeatureLayer::UNKNOWN;

        metadataTable->setFeatureLayerType(featureLayerType);

        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataCreator->createMetadataTableDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Unable to create a IMetadataTableDefn instance");
            //            return NULL;
        }
        metadataTable->setMetadataTableDefn(tableDefn.get(),featureLayerObject->getInterface<CORE::IBase>()->getUniqueID().toString());

        OGRLayer* metadataOGRLayer = metadataTable->getInterface<TERRAIN::IOGRLayerHolder>()->getOGRLayer();

        if(!metadataOGRLayer)
        {
            LOG_ERROR("Invalid OGRLayer in metadataTable");
            //            return NULL;
        }

        return featureLayerObject;
    }
    CORE::RefPtr<CORE::IObject> LayerComponent::createFeatureLayer(const std::string& layerName,
        const std::string& layerType,
        std::vector<std::string>& attrNames,
        std::vector<std::string>& attrTypes,
        std::vector<int>& attrWidths,
        std::vector<int>& attrPrecisions)
    {
        CORE::RefPtr<CORE::IObjectFactory> objectFactory = 
            CORE::CoreRegistry::instance()->getObjectFactory();

        CORE::RefPtr<CORE::IObject> featureLayer = 
            objectFactory->createObject(*ELEMENTS::ElementsRegistryPlugin::FeatureLayerType);

        CORE::RefPtr<CORE::IMetadataTableHolder> tableHolder = 
            featureLayer->getInterface<CORE::IMetadataTableHolder>();

        if(!tableHolder.valid())
        {
            LOG_ERROR("IMetadataTableHolder interface not found in FeatureLayer");
            return false;
        }

        CORE::RefPtr<CORE::IMetadataTable> metadataTable = 
            tableHolder->getMetadataTable();

        if(!metadataTable.valid())
        {
            LOG_ERROR("Invalid IMetadataTable instance");
            return false;
        }

        CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
            CORE::WorldMaintainer::instance();

        if(!worldMaintainer.valid())
        {
            LOG_ERROR("World Maintainer is not valid");
            return false;
        }

        CORE::RefPtr<CORE::IComponent> component = 
            worldMaintainer->getComponentByName("DataSourceComponent");

        if(!component.valid())
        {
            LOG_ERROR("DataSourceComponent is not found");
            return false;
        }

        CORE::RefPtr<CORE::IMetadataCreator> metadataCreator = 
            component->getInterface<CORE::IMetadataCreator>();

        if(!metadataCreator.valid())
        {
            LOG_ERROR("IMetadataCreator interface not found in DataSourceComponent");
            return false;
        }

        // create a new table definition
        CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn = 
            metadataCreator->createMetadataTableDefn();

        if(!tableDefn.valid())
        {
            LOG_ERROR("Unable to create a IMetadataTableDefn instance");
            return false;
        }

        for(int i = 0; i < (int)attrNames.size(); i++)
        {

            // create fields to store name, latitude, longitude and height
            CORE::RefPtr<CORE::IMetadataFieldDefn> field = 
                metadataCreator->createMetadataFieldDefn();

            field->setName(attrNames.at(i));
            std::string fieldType = attrTypes.at(i);
            if(fieldType == "Text")
            {
                field->setType(CORE::IMetadataFieldDefn::STRING);
            }
            else if(fieldType == "Decimal")
            {
                field->setType(CORE::IMetadataFieldDefn::DOUBLE);
            }
            else if(fieldType == "Integer")
            {
                field->setType(CORE::IMetadataFieldDefn::INTEGER);
            }

            field->setLength(attrWidths.at(i));
            field->setPrecision(attrPrecisions.at(i));

            // add the field to the table definition
            tableDefn->addFieldDefn(field.get());
        }

        CORE::IFeatureLayer::FeatureLayerType featureLayerType = 
            CORE::IFeatureLayer::UNKNOWN;
        if(layerType == "Point")
        {
            featureLayerType = CORE::IFeatureLayer::POINT;
        }
        else if(layerType == "Line")
        {
            featureLayerType = CORE::IFeatureLayer::LINE;
        }
        else if( layerType == "Fence" )
        {
            featureLayerType = CORE::IFeatureLayer::FENCE;
        }
        else if(layerType == "Polygon" || layerType == "Area")
        {
            featureLayerType = CORE::IFeatureLayer::POLYGON;
        }
        else if(layerType == "Military Layer")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT;
        }
        else if(layerType == "Model")
        {
            featureLayerType = CORE::IFeatureLayer::MODEL;
        }
        else if(layerType == "Multi Point")
        {
            featureLayerType = CORE::IFeatureLayer::MULTIPOINT;
        }
        else if(layerType == "Multi Line")
        {
            featureLayerType = CORE::IFeatureLayer::MULTILINE;
        }
        else if(layerType == "Multi Polygon")
        {
            featureLayerType = CORE::IFeatureLayer::MULTIPOLYGON;
        }
        else if(layerType == "MilitaryLayerPoint")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_POINT;
        }
        else if(layerType == "MilitaryLayerLine")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_LINE;
        }
        else if(layerType == "MilitaryLayerPolygon")
        {
            featureLayerType = CORE::IFeatureLayer::MILITARY_UNIT_POLYGON;
        }
        else if(layerType == "Route")
        {
            featureLayerType = CORE::IFeatureLayer::ROUTE;
        }
        else if(layerType == "Trajectory")
        {
            featureLayerType = CORE::IFeatureLayer::TRAJECTORY;
        }
        else if(layerType == "FormationLayer")
        {
            featureLayerType = CORE::IFeatureLayer::FORMATION;
        }
        else
        {
            featureLayerType = CORE::IFeatureLayer::UNKNOWN;
        }

        metadataTable->setFeatureLayerType(featureLayerType);

        CORE::RefPtr<CORE::IBase> base = featureLayer->getInterface<CORE::IBase>();
        if(base.valid())
        {
            base->setName(layerName);
        }

        // set the field definition to the MetadataTable
        metadataTable->setMetadataTableDefn(tableDefn.get(),featureLayer->getInterface<CORE::IBase>()->getUniqueID().toString());
        return featureLayer;

    }

    CORE::RefPtr<CORE::IFeatureLayer> LayerComponent::getFeatureLayer(const std::string& name)
    {
        LayerMap::iterator it;      
        it = _layerMap.find(name);      // find element mapped to name if exists
        CORE::RefPtr<CORE::IFeatureLayer> featureLayer;
        if(it == _layerMap.end()) // layernot found
        {
            return NULL;
        }
        else
        {
            featureLayer = it->second;
            return featureLayer;
        }
    }

    const SMCElements::ILayerComponent::LayerMap& LayerComponent::getLayerMap() const
    {
        return _layerMap;
    }

    ELEMENTS::IFeatureDataSource* 
        LayerComponent::getOrCreateFeatureDataSource(const std::string& name)
    {
        FeatureDataSourceMap::iterator itr;      
        itr = _featureDataSourceMap.find(name);      // find element mapped to name if exists
        if(itr == _featureDataSourceMap.end())       // if layer is not found
        {
            //create feature layer
            CORE::RefPtr<CORE::IObject> featureDataSourceObject = _createFeatureDataSource(name);
            ELEMENTS::IFeatureDataSource* featureDatasource = featureDataSourceObject->getInterface<ELEMENTS::IFeatureDataSource>();

            // add it to the map
            _featureDataSourceMap[name] = featureDatasource;

            CORE::RefPtr<CORE::IWorldMaintainer> worldMaintainer = 
                CORE::WorldMaintainer::instance();

            CORE::WorldMap map = worldMaintainer->getWorldMap();
            CORE::WorldMap::iterator itw;
            itw = map.begin();

            CORE::RefPtr<CORE::IWorld> iworld = itw->second;
            iworld->addObject(featureDataSourceObject.get()); 

            return featureDatasource;
        }
        else
        {
            return itr->second.get();
        }
    }

    ELEMENTS::IFeatureDataSource* 
        LayerComponent::getFeatureDataSource(const std::string& name)
    {
        FeatureDataSourceMap::iterator it;      
        it = _featureDataSourceMap.find(name);
        if(it != _featureDataSourceMap.end()) 
        {
            return it->second;
        }
        else
        {
            return NULL;
        }
    }

    const IDataSourceComponent::FeatureDataSourceMap& 
        LayerComponent::getFeatureDataSourceMap() const
    {
        return _featureDataSourceMap;
    }
}
