#include <SMCElements/IncidentLoaderComponent.h>
#include <SMCElements/SMCElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>
#include <Core/GroupAttribute.h>

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>
#include <iostream>
namespace SMCElements
{
    DEFINE_IREFERENCED(IncidentLoaderComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, IncidentLoaderComponent);

    IncidentLoaderComponent::IncidentLoaderComponent()
    {
        _addInterface(IIncidentLoaderComponent::getInterfaceName());
    }

    IncidentLoaderComponent::~IncidentLoaderComponent()
    {}

    void IncidentLoaderComponent::initializeAttributes()
    {
        CORE::Base::initializeAttributes();

        // add range color value
        _addAttribute(new CORE::GroupAttribute("Incident", "Incident",
            CORE::GroupAttribute::SetFuncType(this, &IncidentLoaderComponent::_addIncident),
            CORE::GroupAttribute::GetFuncType(this, &IncidentLoaderComponent::_getIncident),
            "Incident",
            "IncidentLoaderComponent Attributes"));
    }

    void IncidentLoaderComponent::_addIncident(const CORE::NamedGroupAttribute &groupAttr)
    {
        const CORE::NamedStringAttribute* attType = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("Type"));

        const CORE::NamedStringAttribute* attTableName = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("TableName"));

        const CORE::NamedStringAttribute* attDateColName = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("DateColName"));

        const CORE::NamedGroupAttribute* attSelect = 
            dynamic_cast<const CORE::NamedGroupAttribute*>(groupAttr.getAttribute("Select"));

        const CORE::NamedStringAttribute* attFrom = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("From"));

        const CORE::NamedStringAttribute* attWhere = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("Where"));

        const CORE::NamedStringAttribute* attSkip = 
            dynamic_cast<const CORE::NamedStringAttribute*>(groupAttr.getAttribute("SkipList"));

        if(!attType || !attSelect || !attFrom || !attWhere || !attSkip || !attTableName || !attDateColName)
            return;

        IIncidentLoaderComponent::IncidentType incident;

        incident.type = attType->getValue();

        incident.tableName = attTableName->getValue();

        incident.dateColName = attDateColName->getValue();

        CORE::NamedGroupAttribute::AttributeList::const_iterator selectIter = attSelect->getAtributeList().begin();
        for(; selectIter != attSelect->getAtributeList().end(); ++selectIter)
        {
            const CORE::NamedStringAttribute* attribute = dynamic_cast<const CORE::NamedStringAttribute*>(selectIter->second.get());
            incident.select[attribute->getName()] = attribute->getValue();
        }

        incident.from = attFrom->getValue();

        incident.whereClause = attWhere->getValue();

        incident.skipList = attSkip->getValue();

        _incidentTypeMap[incident.type] = incident;
    }

    CORE::RefPtr<CORE::NamedGroupAttribute> IncidentLoaderComponent::_getIncident() const
    {
        return NULL;
    }

    const IIncidentLoaderComponent::IncidentTypeMap& IncidentLoaderComponent::getIncidentMap() const
    {
        return _incidentTypeMap;
    }

}
