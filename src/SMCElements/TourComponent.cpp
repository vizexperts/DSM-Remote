#include <Core/WorldMaintainer.h>
#include <Core/IAnimationComponent.h>
#include <Core/TickMessage.h>

#include <Elements/ElementsUtils.h>

#include <SMCElements/TourComponent.h>
#include <Core/IMessageFactory.h>
#include <Core/CoreRegistry.h>
#include <Core/CommonUtilityFunctions.h>
#include <Util/CoordinateConversionUtils.h>
#include <Util/Exception.h>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgGA/CameraManipulator>

//std headers
#include <fstream>

//Boost Headers
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <serialization/IProjectLoaderComponent.h>
#include <Elements/ISettingComponent.h>
#include <Elements/ElementsUtils.h>

namespace fs = boost::filesystem;

namespace SMCElements
{
    DEFINE_IREFERENCED(TourComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, TourComponent);

    TourComponent::TourComponent() : _tourDuration(0),
        _recorderState(Recorder_None),
        _currentActiveTourName(""),
        _tourWriteVersion(2),
        _tourReadVersion(2),
        _speedFactor(1),
        _recorderType(CONTINUOUS),
        _tourToPlayOnStart(""),
        _isSequencePlaying(false),
        _useIterator(false)
    {
        _addInterface(SMCElements::ITourComponent::getInterfaceName());
    }

    TourComponent::~TourComponent()
    {
    }

    bool TourComponent::_isTourWithNameAlreadyPresent(const std::string &tourName)
    {
        if(_mapTourNameToFile.find(tourName)!=_mapTourNameToFile.end())
            return false;

        return true;
    }

    void TourComponent::loadTour(const std::string &tourName)
    {
        if(_currentActiveTourName==tourName)
            return; //Tour already loaded

        //..... reset
        reset();

        std::string fileName = "";
        std::string fullTourName = "";
        std::string fullCPName = "";
        //In case Sequence :: We need to look into the Sequence Dir
        if (_isSequencePlaying)
        {
            std::string jsonFile = _getSequenceJson();
            JSONNode mainNode(JSON_NODE);
            _getJsonObject(jsonFile, mainNode);
            JSONNode::iterator it = mainNode.find(_currentActiveSequenceName);
            if (it == mainNode.end())
            {
                UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "Unable to locate tour file",
                    __FILE__, __LINE__);
            }
            
            std::string tourFileNameForSeq = it->find(tourName)->as_string();
            fileName = fs::path(tourFileNameForSeq).filename().string();
            fullTourName = tourFileNameForSeq;
            
            fullCPName = (fs::path (fullTourName).parent_path() /  _getCPNameFromTourName(tourFileNameForSeq)).string();
        }
        else
        {
            auto& fileItr = _mapTourNameToFile.find(tourName);
            if (fileItr == _mapTourNameToFile.end())
            {
                UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "Unable to locate tour file",
                    __FILE__, __LINE__);
            }
            fileName = fileItr->second;
            fullTourName = _getTourFullNameWithPath(fileName);
            fullCPName = _getTourFullNameWithPath(_getCPNameFromTourName(fileName));
        }
    
        // this is written for backward compatibility with old tour file formats 
        // if there is no tourCP file for the tour, then create a new file and open empty cp file 
        if(!osgDB::fileExists(fullCPName))
        {
            if (!_createNewCheckPointFile(fileName))
                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to open Tour file",
                __FILE__, __LINE__);

        }

        //Trying to open the current tour
        std::ifstream infile(fullTourName.c_str());

        if(!infile.good())
        {
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to open Tour file",
                __FILE__, __LINE__);
        }

        _mapTimeToFrameV1.clear();
        _mapTimeToFrameV2.clear();
        _currentActiveTourName = tourName;

        unsigned int version = _readVersion(infile);

        _tourReadVersion = version;

        if(version == 1)
        {
            //Trying to read the file 
            //Reading the header (Not a valid Header)
            if(!_readHeaderFromFile(infile))
            {
                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "File corrupted.",
                    __FILE__, __LINE__);
            }

            while (infile.good())
            {
                if(_readFrameFromFileV1(infile, _elapsedDuration, _currFrameV1))
                {
                    //Inserting only valid frames
                    _mapTimeToFrameV1[_elapsedDuration] = _currFrameV1;
                }
            }
        }
        else if(version == 2)
        {
            //Trying to read the file 
            //Reading the header (Not a valid Header)
            if(!_readHeaderFromFile(infile))
            {
                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "File corrupted.",
                    __FILE__, __LINE__);
            }
            while(infile.good())
            {
                if(_readFrameFromFileV2(infile, _elapsedDuration, _currFrameV2))
                {
                    //Inserting only valid frames
                    _mapTimeToFrameV2[_elapsedDuration] = _currFrameV2;
                }
            }
        }        

        //........... loading cp file .............
        _loadCPFile(fileName);
    }

    void TourComponent::_loadCPFile(std::string fileName)
    {
        fileName = _getCPNameFromTourName(fileName);

        std::string fullCPName;
        if (_isSequencePlaying)
        {
            fullCPName = _getTourFullNameWithPath((fs::path(_currentActiveSequenceName) / (fileName)).string());
        }
        else
        {
            fullCPName = _getTourFullNameWithPath(fileName);
        }


        std::ifstream cpInfile(fullCPName.c_str());
        if( !cpInfile.good())
        {
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to open Tour file",
                __FILE__, __LINE__);
        }

        float speedFactor;

        _mapTimeToSpeed.clear();
        while(cpInfile.good())
        {
            if(_readSpeedFromFile(cpInfile, _elapsedDuration, speedFactor))
            {
                _mapTimeToSpeed[_elapsedDuration] = speedFactor;
            }
        }
        _lastCheckPointItr = _mapTimeToSpeed.begin();

        cpInfile.close();
    }

    bool TourComponent::_createNewCheckPointFile(std::string fileName)
    {
        //! getting full file path (in current project directory)
        fileName = _getCPNameFromTourName(fileName);
        std::string fullFileName = _getTourFullNameWithPath(fileName);

        //! output stream (creates a new file)
        std::ofstream cpOutFile(fullFileName.c_str());

        if(!cpOutFile.good())
        {
            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to open Tour file",
                __FILE__, __LINE__);
        }

        cpOutFile.clear();
        _writeHeaderToCPFile(cpOutFile);
        cpOutFile.close();
        return true;
    }

    void TourComponent::_writeHeaderToCPFile(std::ofstream &cpOutFile)
    {
        //! header data
        int time = 0;
        float speed = 1.0;

        //! default entry of the file
        cpOutFile<< time << " " 
            << speed << std::endl;
    }

    void TourComponent::saveSpeedProfile()
    {
        std::string fileName = _getCPNameFromTourName(_mapTourNameToFile[_currentActiveTourName]); 
        std::string cpFileFullName = _getTourFullNameWithPath(fileName);
        std::ofstream outFile(cpFileFullName.c_str());

        if(!outFile.good())
        {
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to save CheckPoint file on disk",
                __FILE__, __LINE__);
        }

        outFile.clear();

        //Writing content of the tour
        MapTimeToSpeed::const_iterator startItr = _mapTimeToSpeed.begin();

        //Means we don't have any data to save
        if(startItr==_mapTimeToSpeed.end())
        {
            _writeHeaderToCPFile(outFile);
            outFile.close();
            return;
        }

        //Writing Data
        for(;startItr!=_mapTimeToSpeed.end(); ++startItr)
        {
            outFile << startItr->first << " " << startItr->second << std::endl;
        }

        //Closing the file
        outFile.close();
    }


    bool TourComponent::_readSpeedFromFile(std::ifstream &cpInFile, 
        unsigned int &currFrameDuration,
        float &speedFactor)
    {
        if(!cpInFile.good())
        {
            return false;
        }

        //reading the line
        std::string currLine;
        std::getline(cpInFile, currLine);

        //stringstream
        std::stringstream ss(currLine);

        ss >> currFrameDuration;

        if(ss.bad())
            return false;

        ss >> speedFactor;

        return true;
    }


    unsigned int TourComponent::_readVersion(std::ifstream& infile)
    {
        unsigned int version = 1;

        //reading the line
        std::string currLine;
        std::getline(infile, currLine);
        std::stringstream ss(currLine);

        if (ss.bad())
        {
            return version;
        }

        ss >> version;

        //in case cannot transfer any data
        if(ss.bad())
        {
            version = 1;
            infile.clear();
            infile.seekg(0);
            return version;
        }

        //in case for version 1 of file
        if(version > MAX_FILE_VERSION)
        {
            version = 1;
            infile.clear();
            infile.seekg(0);
            return version;
        }

        return version;

    }

    bool TourComponent::_readHeaderFromFile(std::ifstream &inFile)
    {
        try
        {
            //Readind the tour Name
            if(!inFile.good())
                return false;

            //reading the line
            std::string currLine;
            std::getline(inFile, currLine);
            std::stringstream ss(currLine);

            std::string boostTime;

            ////Reading the tour Name
            //if(ss.bad())
            //    return false;
            //ss >> _currentActiveTourName;

            //Reading the tour start time
            if(ss.bad())
                return false;
            ss >> boostTime;

            _tourStartTime = boost::posix_time::from_iso_string(boostTime);

            //Reading the tour Duration
            if(ss.bad())
                return false;

            ss >> _tourDuration;

            return true;
        }
        catch(...)
        {
        }

        return false;
    }

    bool TourComponent::_readFrameFromFileV1(std::ifstream &inFile, 
        unsigned int &duration,
        Frame &frame)
    {
        try
        {
            //Readind the tour Time
            if(!inFile.good())
                return false;

            //reading the line
            std::string currLine;
            std::getline(inFile, currLine);

            //stringstream
            std::stringstream ss(currLine);

            unsigned int currFrameDuration;
            std::string boostTime;
            double x=0.0;
            double y=0.0;
            double z=0.0;
            double range=0.0;
            double heading=0.0;
            double pitch=0.0;

            ss >> currFrameDuration;
            if(ss.bad())
                return false;

            ss >> boostTime;
            if(ss.bad())
                return false;

            ss >> x;
            if(ss.bad())
                return false;

            ss >> y;
            if(ss.bad())
                return false;

            ss >> z;
            if(ss.bad())
                return false;

            ss >> heading;
            if(ss.bad())
                return false;

            ss >> pitch;
            if(ss.bad())
                return false;

            ss >> range;
            if(ss.bad())
                return false;

            frame.cameraProperties.position.set(x, y, z);

            frame.cameraProperties.heading    = heading;
            frame.cameraProperties.pitch    = pitch;
            frame.cameraProperties.range    = range;
            frame.animationTime                = boost::posix_time::from_iso_string(boostTime);
            duration                        = currFrameDuration;

            return true;
        }
        catch(...)
        {
        }

        return false;
    }

    bool 
        TourComponent::_readFrameFromFileV2(std::ifstream &inFile, 
        unsigned int &duration,
        FrameV2 &frame)
    {
        try
        {
            //Readind the tour Time
            if(!inFile.good())
                return false;

            //reading the line
            std::string currLine;
            std::getline(inFile, currLine);

            //stringstream
            std::stringstream ss(currLine);

            unsigned int currFrameDuration;
            std::string boostTime;

            double eyex = 0.0;
            double eyey = 0.0;
            double eyez = 0.0;

            double centerx = 0.0;
            double centery = 0.0;
            double centerz = 0.0;

            double upx = 0.0;
            double upy = 0.0;
            double upz = 0.0;

            ss >> currFrameDuration;
            if(ss.bad())
                return false;

            ss >> boostTime;
            if(ss.bad())
                return false;

            ss >> eyex >> eyey >> eyez;
            if(ss.bad())
                return false;

            ss >> centerx >> centery >> centerz;
            if(ss.bad())
                return false;

            ss >> upx >> upy >> upz;
            if(ss.bad())
                return false;


            duration = currFrameDuration;

            frame.animationTime = boost::posix_time::from_iso_string(boostTime);

            frame.viewport.makeLookAt(osg::Vec3d(eyex, eyey, eyez), 
                osg::Vec3d(centerx, centery, centerz),
                osg::Vec3d(upx, upy, upz));

            return true;
        }
        catch(...)
        {
        }

        return false;
    }


    bool TourComponent::_readSpeed(unsigned int currStep)
    {
        MapTimeToSpeed::iterator itr = _mapTimeToSpeed.begin();

        if(itr == _mapTimeToSpeed.end())
            return false;

        while(itr!=_mapTimeToSpeed.end() && itr->first <= currStep)
            itr++;

        itr--;

        if(_lastCheckPointItr->first != itr->first){
            _speedFactor = itr->second;  
            _lastCheckPointItr = itr;
        }
        return true;
    }

    bool 
        TourComponent::_readFrameV1(unsigned int currStep,
        Frame &frame)
    {
        //Getting camera Properties for the map
        MapTimeToFrame::iterator itr      = _mapTimeToFrameV1.begin();
        if(itr==_mapTimeToFrameV1.end())
            return false;

        if(_useIterator)
        {
            itr = _mapTimeToFrameV1Iter;
        }

        MapTimeToFrame::iterator aHeadItr = itr;
        MapTimeToFrame::iterator preItr   = itr;

        for(;itr!=_mapTimeToFrameV1.end(); ++itr)
        {
            if(currStep < itr->first)
            {
                aHeadItr = itr;
                break; //Means we have ahead of the time
            }

            preItr = itr;
        }

        if(_useIterator)
        {
            _mapTimeToFrameV1Iter = preItr;
        }


        //Means we have traversed the full Map. We are after tour end
        if(preItr==_mapTimeToFrameV1.end())
        {
            MapTimeToFrame::reverse_iterator itr = _mapTimeToFrameV1.rbegin();
            frame =  itr->second;
            return true;
        }

        //Interpolating the camera properties based on two near by time
        //Doing Linear Interpolation
        if(aHeadItr==_mapTimeToFrameV1.begin())
        {
            //Means tour has not just started
            frame = aHeadItr->second;
            return true;
        }
#if 1
        frame = preItr->second;
        return true;
#else
        //This condition potentially means we are at mid. So we have to interpolate
        unsigned int p2 = aHeadItr->first;
        unsigned int p1 = preItr->first;

        ITourComponent::Frame f1 = preItr->second;
        ITourComponent::Frame f2 = (aHeadItr)->second;

        //Now interpolating
        int diffBetMinMax  = (p2-p1);
        double diffBetCurrMin = (currStep-p1);

        //Means duplicate entry
        if(diffBetMinMax==0)
        {
            frame = aHeadItr->second;
            return true;
        }

        frame.animationTime = f1.animationTime;

        frame.cameraProperties.position = f1.cameraProperties.position + 
            ((f2.cameraProperties.position-f1.cameraProperties.position)*diffBetCurrMin)/diffBetMinMax;

        frame.cameraProperties.heading = f1.cameraProperties.heading + 
            ((f2.cameraProperties.heading-f1.cameraProperties.heading)*diffBetCurrMin)/diffBetMinMax;

        frame.cameraProperties.range = f1.cameraProperties.range + 
            ((f2.cameraProperties.range-f1.cameraProperties.range)*diffBetCurrMin)/diffBetMinMax;

        frame.cameraProperties.pitch = f1.cameraProperties.pitch + 
            ((f2.cameraProperties.pitch-f1.cameraProperties.pitch)*diffBetCurrMin)/diffBetMinMax;

        return true;
#endif
    }

    bool 
        TourComponent::_readFrameV2(unsigned int currStep,
        FrameV2 &frame)
    {
        //Getting camera Properties for the map

        MapTimeToFrameV2::iterator itr      = _mapTimeToFrameV2.begin();
        if(itr==_mapTimeToFrameV2.end())
            return false;

        if(_useIterator)
        {
            itr = _mapTimeToFrameV2Iter;
        }

        //for(unsigned int frameNum=1; frameNum<=_currFrameNumber && itr!=_mapTimeToFrameV2.end(); ++frameNum)
        //{
        //    itr++;
        //}

        //if(itr != _mapTimeToFrameV2.end())
        //{
        //    frame                = itr->second;
        //    _elapsedDuration    = itr->first;

        //    return true;
        //}

        //return false;

        MapTimeToFrameV2::iterator aHeadItr = itr;
        MapTimeToFrameV2::iterator preItr   = itr;

        for(;itr!=_mapTimeToFrameV2.end(); ++itr)
        {
            if(currStep < itr->first)
            {
                aHeadItr = itr;
                break; //Means we have ahead of the time
            }

            preItr = itr;
        }

        if(_useIterator)
        {
            _mapTimeToFrameV2Iter = preItr;
        }

        //Means we have traversed the full Map. We are after tour end
        if(itr==_mapTimeToFrameV2.end())
        {
            MapTimeToFrameV2::reverse_iterator itr = _mapTimeToFrameV2.rbegin();
            frame =  itr->second;
            return true;
        }

        //Interpolating the camera properties based on two near by time
        //Doing Linear Interpolation
        if(aHeadItr==_mapTimeToFrameV2.begin())
        {
            //Means tour has not just started
            frame = aHeadItr->second;
            return true;
        }

        //This condition potentially means we are at mid. So we have to interpolate
        unsigned int p2 = aHeadItr->first;
        unsigned int p1 = preItr->first;

        FrameV2 f1 = preItr->second;
        FrameV2 f2 = aHeadItr->second;

        //Now interpolating
        int diffBetMinMax  = (p2 - p1);
        double diffBetCurrMin = (currStep - p1);

        //Means duplicate entry
        if(diffBetMinMax == 0)
        {
            frame = aHeadItr->second;
            return true;
        }

        frame.animationTime = f1.animationTime + ((f2.animationTime - f1.animationTime)/(diffBetMinMax)) * diffBetCurrMin;

        osg::Vec3d position1 = f1.viewport.getTrans();
        osg::Quat rotation1  = f1.viewport.getRotate();

        osg::Vec3d position2 = f2.viewport.getTrans();
        osg::Quat rotation2  = f2.viewport.getRotate();

        double ratio = diffBetCurrMin/diffBetMinMax;

        //linearly interpolate the position and rotation. Using Quat in rotation as it gives better results
        osg::Vec3d position        = position1 * (1 - ratio) + position2 * ratio;
        osg::Quat rotation;
        rotation.slerp(ratio, rotation1, rotation2);

        //set the position and rotation
        osg::Matrixd viewport;
        viewport.setTrans(position);
        viewport.setRotate(rotation);

        frame.viewport = viewport;

        return true;

    }

    const SMCElements::ITourComponent::MapTourToFile &
        TourComponent::getTourMap() const
    {
        return _mapTourNameToFile;
    }

    const SMCElements::ITourComponent::MapTimeToSpeed& TourComponent::getCurrentCheckpointMap()const
    {
        return _mapTimeToSpeed;
    }

    bool
        TourComponent::addExternalTour(const std::string &tourName,
        const std::string &tourFileName,
        bool overrideContentIfPresent)
    {
        if(!overrideContentIfPresent)
        {
            if(_isTourWithNameAlreadyPresent(tourName))
                return false;
        }

        _mapTourNameToFile[tourName] = tourFileName;
        return true;
    }

    void
        TourComponent::unloadTour()
    {
        reset();
    }

    void TourComponent::reset()
    {
        _currentActiveTourName  = "";
        _elapsedDuration        = 0;
        _tourDuration           = 0;

        _mapTimeToFrameV1.clear();
        _mapTimeToFrameV2.clear();
        _mapTimeToSpeed.clear();

        _prevFrameV1.makeInvalid();
        _currFrameV1.makeInvalid();

        _prevFrameV2.invalidate();
        _currFrameV2.invalidate();

        _mode             = ITourComponent::Tour_None;
        _recorderState    = ITourComponent::Recorder_None;
        _playerState      = ITourComponent::Player_None;

        //_unsubscribe to tick message
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);
    }

    void TourComponent::resetAll()
    {
        reset();

        _isSequencePlaying = false;
        _currentActiveSequenceName = "";
        _currentActiveTourPositionInSequence = 0;
    }

    //! Will reset the component completely
    void TourComponent::_hardReset()
    {
        resetAll();

        _mapTourNameToFile.clear();
        _mapSequenceToTourList.clear();
    }

    //! will start the recording from startting
    bool TourComponent::startRecording(bool overridePreviousProgress)
    {
        if(_mode == Tour_Playing)
            return false;

        //CHecking the state of the recorder
        if( _recorderState==ITourComponent::Recorder_Recording &&
            !overridePreviousProgress)
            return false; //can't override the current Recording

        //If recording is paused we will start recording 
        //again but we don't update the start tour
        //time
        if( _recorderState!=ITourComponent::Recorder_Pause)
        {
            //Clear all obsolete frames
            _mapTimeToFrameV1.clear();
            _mapTimeToFrameV2.clear();

            _currFrameV2.invalidate();
            _prevFrameV2.invalidate();

            _tourStartTime        = _animationComponent->getCurrentDateTime();
            _elapsedDuration    = 0;
        }

        _mode          = ITourComponent::Tour_Recording;
        _recorderState = ITourComponent::Recorder_Recording;

        //subscribe to tick message for updating the frame data
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _subscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        return true;
    }

    //! to pause the current recording
    bool 
        TourComponent::pauseRecording()
    {
        if(_mode != Tour_Recording)
            return false;

        //CHecking the state of the recorder
        if(_recorderState!=ITourComponent::Recorder_Pause &&
            _recorderState!=ITourComponent::Recorder_Recording)
            return false; //No recording in progress

        _recorderState = ITourComponent::Recorder_Pause;

        //Unsubscribing from the tick
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        return true;
    }

    //! to stop the current Recoring
    bool 
        TourComponent::stopRecording()
    {
        if(_mode != Tour_Recording)
            return false;

        //CHecking the state of the recorder
        if(_recorderState==ITourComponent::Recorder_None)
            return false; //No tour running

        if(_recorderState==ITourComponent::Recorder_Stop)
            return true; //Recorder is already stopped

        _recorderState = ITourComponent::Recorder_Stop;
        _mode          = ITourComponent::Tour_None;

        //Unsubscribing from the tick
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        return true;
    }


    std::string TourComponent::_getTourFullNameWithPath(std::string fileName)
    {
        //! getting current project name
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        std::string currProjectFileName = worldMaintainer->getProjectFile();

        //Getting the directory from the fileName
        std::string filePath = osgDB::getFilePath(currProjectFileName);
        if(filePath.empty())
        {
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                "Unable to locate project directory",
                __FILE__, __LINE__);
        }

        return (filePath + "/" + fileName);
    }

    std::string TourComponent::_getSequenceJson()
    {
        //! getting current project name
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        std::string currProjectFileName = worldMaintainer->getProjectFile();
        //Getting the directory from the fileName
        std::string filePath = osgDB::getFilePath(currProjectFileName);
        if (filePath.empty())
        {
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                "Unable to locate project directory",
                __FILE__, __LINE__);
        }

        return (fs::path(filePath) / "sequence.json").string();
    }
    void TourComponent::_getJsonObject(std::string jsonFile, JSONNode& mainNode)
    {
        osgDB::ifstream inFile(jsonFile.c_str());
        if (!inFile.is_open())
        {
            std::string value = mainNode.write_formatted();
            osgDB::ofstream out(jsonFile.c_str());
            out << value;
            out.close();
        }
        else
        {
            //read content from file into a string stream 
            std::ostringstream buffer;
            buffer << inFile.rdbuf();
            //parse json file 
            try
            {
                mainNode = libjson::parse(buffer.str());
            }
            catch (std::invalid_argument& ex)
            {
                //throw exception
            }
        }
    }
    
    void TourComponent::_updateJson(std::string jsonFile, JSONNode mainNode)
    {
        std::string value = mainNode.write_formatted();
        osgDB::ofstream out(jsonFile.c_str());
        out << value;
        out.close();
    }


#if 1
    //This code has to be removed after sometime
    void TourComponent::reloadOldSequence(const std::string& sequenceName, const std::vector<std::string>& sequence)
    {
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        std::string currProjectFileName = worldMaintainer->getProjectFile();
        //Getting the directory from the fileName
        std::string filePath = osgDB::getFilePath(currProjectFileName);
        if (filePath.empty())
        {
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                "Unable to locate project directory",
                __FILE__, __LINE__);
        }

        JSONNode mainNode(JSON_NODE);

        std::string jsonFile = _getSequenceJson();
        _getJsonObject(jsonFile, mainNode);
        fs::path sequenceDir = (fs::path(filePath) / sequenceName);
        fs::create_directories(sequenceDir);
        //copy all the tour files forming the sequence to the sequence folder
        for (auto& tour : sequence)
        {
            auto& iter = _mapTourNameToFile.find(tour);
            if (iter != _mapTourNameToFile.end())
            {
                std::string tourFileName;
                tourFileName = iter->second;

                std::string seqsrcFile = _getTourFullNameWithPath(tourFileName);
                std::string seqdestFile = (sequenceDir / tourFileName).string();
                if (!fs::exists(seqdestFile))
                    fs::copy_file(seqsrcFile, seqdestFile);

                std::string cpfilename = _getCPNameFromTourName(seqsrcFile);
                std::string cpsrcFile = _getTourFullNameWithPath(cpfilename);
                std::string cpdestFile = (sequenceDir / cpfilename).string();
                if (!fs::exists(cpdestFile))
                    fs::copy_file(cpsrcFile, cpdestFile);

                JSONNode seqnode(JSON_NODE);
                
                    seqnode.set_name(sequenceName);
                    seqnode.push_back(JSONNode(tour, seqdestFile));
                    mainNode.push_back(seqnode);              

            }
            else
            {
                //Should never occur for older projects
            }
        }

        _updateJson(jsonFile, mainNode);

    }


#endif

    void TourComponent::reloadSequence(const std::string& sequenceName, const std::vector<std::string>& sequence)
    {
        MapSequenceToTourList::iterator iter = _mapSequenceToTourList.find(sequenceName);
        if (iter != _mapSequenceToTourList.end())
        {
            //! Sequence with this name already exists
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OVERWRITE_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Tour with same name already exists",
                __FILE__, __LINE__);
        }

        _mapSequenceToTourList[sequenceName] = sequence;
    }

    void TourComponent::saveSequence(const std::string& sequenceName, const std::vector<std::string>& sequence)
    {
        if (sequence.size() <= 0)
        {
            //! Sequence with this name already exists
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OVERWRITE_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Sequence can not be empty",
                __FILE__, __LINE__);
        }
        MapSequenceToTourList::iterator iter = _mapSequenceToTourList.find(sequenceName);
        if(iter != _mapSequenceToTourList.end())
        {
            //! Sequence with this name already exists
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OVERWRITE_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Tour with same name already exists",
                __FILE__, __LINE__);
        }
        //! getting current project name
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        std::string currProjectFileName = worldMaintainer->getProjectFile();
        //Getting the directory from the fileName
        std::string filePath = osgDB::getFilePath(currProjectFileName);
        if (filePath.empty())
        {
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                "Unable to locate project directory",
                __FILE__, __LINE__);
        }
        JSONNode mainNode(JSON_NODE);
        
        std::string jsonFile = _getSequenceJson();
        _getJsonObject(jsonFile, mainNode);
        fs::path sequenceDir = (fs::path(filePath) / sequenceName);
        fs::create_directories(sequenceDir);
        //copy all the tour files forming the sequence to the sequence folder
        for (auto& tour : sequence)
        {
            auto& iter = _mapTourNameToFile.find(tour);
            
            std::string tourFileName;

            if (iter != _mapTourNameToFile.end())
            {
                tourFileName = iter->second;
                
                std::string seqsrcFile = _getTourFullNameWithPath(tourFileName);
                std::string seqdestFile = (sequenceDir / tourFileName).string();
                if (!fs::exists(seqdestFile))
                    fs::copy_file(seqsrcFile, seqdestFile);

                std::string cpfilename = _getCPNameFromTourName(seqsrcFile);
                std::string cpsrcFile = _getTourFullNameWithPath(cpfilename);
                std::string cpdestFile = (sequenceDir / cpfilename).string();
                if (!fs::exists(cpdestFile))
                    fs::copy_file(cpsrcFile, cpdestFile);

                JSONNode::iterator iter = mainNode.find(sequenceName);
                JSONNode seqnode(JSON_NODE);
                if (iter != mainNode.end())
                {
                    iter->push_back(JSONNode(tour, seqdestFile));
                }
                else{
                    seqnode.set_name(sequenceName);
                    seqnode.push_back(JSONNode(tour, seqdestFile));
                    mainNode.push_back(seqnode);
                }

            }
            else
            {
                //Will Never Occur
            }           

        }
        
        _updateJson(jsonFile, mainNode);

        _mapSequenceToTourList[sequenceName] = sequence;

    }
    void TourComponent::deleteSequence(const std::string& sequenceName)
    {
        MapSequenceToTourList::iterator iter= _mapSequenceToTourList.find(sequenceName);
        if(iter != _mapSequenceToTourList.end())
        {
            _mapSequenceToTourList.erase(iter);
            std::string jsonFile = _getSequenceJson();
            JSONNode mainNode(JSON_NODE);
            _getJsonObject(jsonFile, mainNode);
            mainNode.erase(mainNode.find(sequenceName));
            _updateJson(jsonFile, mainNode);
            //! getting current project name
            CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
            std::string currProjectFileName = worldMaintainer->getProjectFile();
            //Getting the directory from the fileName
            std::string filePath = osgDB::getFilePath(currProjectFileName);
            if (filePath.empty())
            {
                throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                    "Unable to locate project directory",
                    __FILE__, __LINE__);
            }
            try
            {
                fs::remove_all(fs::path(filePath) / sequenceName);
                
            }
            catch (...)
            {
                LOG_ERROR("Can't Delete , File or Directory is in USE!");
            }
        }
        else
        {
            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Tour sequence with name does not exist",
                __FILE__, __LINE__);
        }
    }

    void TourComponent::playSequence(const std::string& sequenceName)
    {
        MapSequenceToTourList::iterator iter = _mapSequenceToTourList.find(sequenceName);
        if(iter == _mapSequenceToTourList.end())
        {
            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Tour sequence with name does not exist",
                __FILE__, __LINE__);
        }

        //! play
        _currentActiveSequenceName = sequenceName;
        //_currentActiveTourPositionInSequence = 0;

        _isSequencePlaying = true;
        
        if (iter->second.size() > 0)
        {
            playTour(iter->second[_currentActiveTourPositionInSequence]);
        }

    }

    void TourComponent::playNextTourInCurrentSequence()
    {
        if(_currentActiveSequenceName.empty())
            return;

        MapSequenceToTourList::iterator iter = _mapSequenceToTourList.find(_currentActiveSequenceName);
        if(iter != _mapSequenceToTourList.end())
        {
            _currentActiveTourPositionInSequence++;

            if(iter->second.size() > _currentActiveTourPositionInSequence)
            {
                stopTour();

                playTour(iter->second[_currentActiveTourPositionInSequence]);

                //! send message to update gui
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITourComponent::SequenceTourChangedMessageType);
                msg->setSender(this);

                notifyObservers(*ITourComponent::SequenceTourChangedMessageType, *msg);
            }
            else
            {
                stopTour();
                _currentActiveTourPositionInSequence = 0;
            }
        }
    }

    void TourComponent::playPreviousTourInCurrentSequence()
    {
        if(_currentActiveSequenceName.empty())
            return;

        MapSequenceToTourList::iterator iter = _mapSequenceToTourList.find(_currentActiveSequenceName);
        if(iter != _mapSequenceToTourList.end())
        {
            _currentActiveTourPositionInSequence--;

            if(_currentActiveTourPositionInSequence > 0)
            {
                playTour(iter->second[_currentActiveTourPositionInSequence]);

                //! send message to update gui
                CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
                CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*ITourComponent::SequenceTourChangedMessageType);
                msg->setSender(this);

                notifyObservers(*ITourComponent::SequenceTourChangedMessageType, *msg);
            }
            else
            {
                playTour(iter->second[0]);
            }
        }
    }

    const ITourComponent::MapSequenceToTourList& TourComponent::getSequenceList() const
    {
        return _mapSequenceToTourList;
    }

    //! Name by which user has to save the tour
    //! flag to control the override action
    void 
        TourComponent::saveCurrentRecorded(const std::string &tourName, 
        bool overrideExisting)
    {
        if(!overrideExisting)
        {
            if(!_isTourWithNameAlreadyPresent(tourName))
            {
                UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OVERWRITE_ERROR);

                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "Tour with same name already exists",
                    __FILE__, __LINE__);
            }
        }

        //Creating the fileName to save the data
        CORE::RefPtr<CORE::UniqueID> uniqueID = CORE::UniqueID::CreateUniqueID();
        std::string uniqueFileName = uniqueID->toString();

        //Appending the extension of the tour file
        uniqueFileName += ".tour";

        //Now saving the file to the current project directory
        std::string tourFileFullName = _getTourFullNameWithPath(uniqueFileName);
        std::ofstream outFile(tourFileFullName.c_str());

        if(!outFile.good())
        {
            UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_OPEN_ERROR);

            throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                "Unable to save recording on disk",
                __FILE__, __LINE__);
        }

        //Updating the tour name
        _currentActiveTourName = tourName;

        if(_tourWriteVersion == 1)
        {
            //Writing content of the tour
            MapTimeToFrame::const_iterator startItr    = _mapTimeToFrameV1.begin();

            //Means we don't have any data to save
            if(startItr==_mapTimeToFrameV1.end())
            {
                outFile.clear();
                outFile.close();
                return;
            }

            if(!_writeHeaderToFileV1(outFile))
            {
                UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_WRITE_ERROR);

                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "Unable to open record file for writing",
                    __FILE__, __LINE__);
            }

            //Writing Data
            for(;startItr!=_mapTimeToFrameV1.end(); ++startItr)
            {
                _writeFrameToFileV1(outFile,startItr->first, 
                    startItr->second, true);
            }

            //Writing the last frame explicitily(To keep last frame duration)
            MapTimeToFrame::const_reverse_iterator revItr = _mapTimeToFrameV1.rbegin();
            _writeFrameToFileV1(outFile, revItr->first, revItr->second, true);

            //! setting the duration of the tour
            _tourDuration = revItr->first;
            _tourReadVersion = 1;
        }
        else if(_tourWriteVersion == 2)
        {

            //Writing content of the tour
            MapTimeToFrameV2::const_iterator startItr    = _mapTimeToFrameV2.begin();

            //Means we don't have any data to save
            if(startItr == _mapTimeToFrameV2.end())
            {
                outFile.clear();
                outFile.close();
                throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                    "No Frames Recorded",
                    __FILE__, __LINE__);
                return;
            }

            outFile.setf(std::ios_base::floatfield, std::ios_base::fixed);
            outFile.precision(16);

            if(!_writeHeaderToFileV2(outFile))
            {
                UTIL::FileReadWriteExceptionType::setFileErrorMode(UTIL::FileReadWriteExceptionType::FILE_WRITE_ERROR);

                throw UTIL::Exception(UTIL::FileReadWriteExceptionType::FILE_READWRITE_EXCEPTION_TYPE,
                    "Unable to open record file for writing",
                    __FILE__, __LINE__);
            }

            //Writing Data
            for(;startItr!=_mapTimeToFrameV2.end(); ++startItr)
            {
                _writeFrameToFileV2(outFile,startItr->first, 
                    startItr->second, true);
            }

            //Writing the last frame explicitily(To keep last frame duration)
            MapTimeToFrameV2::const_reverse_iterator revItr = _mapTimeToFrameV2.rbegin();
            _writeFrameToFileV2(outFile, revItr->first, revItr->second, true);

            //! setting the duration of the tour
            _tourDuration = revItr->first;
            _tourReadVersion = 2;
        }

        //! just create cp file, we don't have any data to save currently in CP file
        _createNewCheckPointFile(uniqueFileName);
        _loadCPFile(uniqueFileName);
        //Closing the file
        outFile.close();

        //Making entry into the tourMap (Assuming that we have file on same location as the project)
        _mapTourNameToFile[tourName] = uniqueFileName;
    }

    std::string TourComponent::_getCPNameFromTourName(std::string tourFileName)
    {
        std::string cpFileName = osgDB::getStrippedName(tourFileName);
        return cpFileName + ".tourcp";
    }

    bool TourComponent::createCheckPoint(unsigned int duration, double speedFactor)
    {
        // if there is already an entry at this duration, delete that
        if(_mapTimeToSpeed.find(duration) != _mapTimeToSpeed.end())
            _mapTimeToSpeed.erase(duration);

        // insert new check point 
        _mapTimeToSpeed[duration] = speedFactor;

        return true;
    }

    bool TourComponent::deleteCheckPoint(unsigned int duration)
    {
        if(_mapTimeToSpeed.find(duration) != _mapTimeToSpeed.end())
            _mapTimeToSpeed.erase(duration);

        return true;
    }

    bool TourComponent::resetSpeedProfile()
    {
        // clearing all checkpoints 
        _mapTimeToSpeed.clear();

        // inserting default entry into time to speed map
        _mapTimeToSpeed[0]=1.0;

        saveSpeedProfile();

        return true;
    }

    void
        TourComponent::onAddedToWorldMaintainer()
    {
        CORE::IWorldMaintainer *maintainer = getWorldMaintainer();

        _subscribe(maintainer, *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(maintainer, *CORE::IWorld::WorldRemovedMessageType);

        CORE::Component::onAddedToWorldMaintainer();
    }

    void
        TourComponent::onRemovedFromWorldMaintainer()
    {
        CORE::IWorldMaintainer *maintainer = getWorldMaintainer();

        _unsubscribe(maintainer, *CORE::IWorld::WorldLoadedMessageType);
        _unsubscribe(maintainer, *CORE::IWorld::WorldRemovedMessageType);

        CORE::Component::onRemovedFromWorldMaintainer();
    }

    void 
        TourComponent::update(const CORE::IMessageType& messageType, 
        const CORE::IMessage& message)
    {
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            //Refreshing the components
            CORE::IComponent *component = ELEMENTS::GetComponentFromMaintainer("AnimationComponent");
            if(component)
                _animationComponent = component->getInterface<CORE::IAnimationComponent>();

            component = ELEMENTS::GetComponentFromMaintainer("CameraComponent");
            if(component)
                _cameraComponent = component->getInterface<VIEW::ICameraComponent>();

            CORE::RefPtr<CORE::IComponent> projectLoaderComponent =
                ELEMENTS::GetComponentFromMaintainer("ProjectLoaderComponent");
            if(projectLoaderComponent.valid())
            {
                _subscribe(projectLoaderComponent.get(), *DB::IProjectLoaderComponent::ProjectLoadedMessageType);
            }

            //Refresing the states
            reset();
        }
        else if(messageType == *DB::IProjectLoaderComponent::ProjectLoadedMessageType)
        {
            CORE::RefPtr<ELEMENTS::ISettingComponent> settingComponent = 
                dynamic_cast<ELEMENTS::ISettingComponent*>(ELEMENTS::GetComponentFromMaintainer("SettingComponent"));
            if(!settingComponent.valid())
            {
                return;
            }

            if(settingComponent->getAppMode() == ELEMENTS::ISettingComponent::APP_MODE_QT)
            {
                return;
            }

            if(!_tourToPlayOnStart.empty())
            {
                playTour(_tourToPlayOnStart);
            }
        }
        else if(messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            _hardReset();
        }
        else if(messageType == *CORE::IWorldMaintainer::TickMessageType)
        {
            try
            {
                //Checking if we are at recording state
                if(_mode == ITourComponent::Tour_Recording)
                {
                    if(_recorderState == ITourComponent::Recorder_Recording)
                    {
                        if(_tourWriteVersion == 1)
                        {
                            //Saving the frame
                            _cameraComponent->getViewPoint(_currFrameV1.cameraProperties.position,
                                _currFrameV1.cameraProperties.range,
                                _currFrameV1.cameraProperties.heading,
                                _currFrameV1.cameraProperties.pitch);

                            _currFrameV1.animationTime = _animationComponent->getCurrentDateTime();

                            //Getting the deltaTime from last tick
                            //XXX this is bit hacky in sense that 
                            //during pause condition this may not give the current
                            //read delta but difference will le less so skipping
                            float deltaTime = message.getInterface<CORE::ITickMessage>()->getDeltaRealTime();
                            _elapsedDuration += deltaTime*1000000;

                            _mapTimeToFrameV1[_elapsedDuration] = _currFrameV1;
                        }
                        else if(_tourWriteVersion == 2)
                        {
                            if(_recorderType == CONTINUOUS)
                            {
                                osg::ref_ptr<osgGA::CameraManipulator> cameraManipulator = 
                                    _cameraComponent->getCameraManipulator();

                                _currFrameV2.viewport = cameraManipulator->getMatrix();
                                _currFrameV2.animationTime = _animationComponent->getCurrentDateTime();

                                //Getting the deltaTime from last tick
                                //XXX this is bit hacky in sense that 
                                //during pause condition this may not give the current
                                //read delta but difference will le less so skipping
                                float deltaTime = message.getInterface<CORE::ITickMessage>()->getDeltaRealTime();
                                _elapsedDuration += deltaTime*1000000;
                                /*_elapsedDuration += 100*/

                                _mapTimeToFrameV2[_elapsedDuration] = _currFrameV2;
                            }
                        }
                    }
                }
                else if(_mode == ITourComponent::Tour_Playing)
                {
                    if(_playerState==ITourComponent::Player_Playing)
                    {

                        //! setting flag
                        _useIterator = true;

                        //Applying the value to the camera component
                        float deltaTime = 0.0;

                        deltaTime = message.getInterface<CORE::ITickMessage>()->getDeltaRealTime();
                        _elapsedDuration += deltaTime * _speedFactor * 1000000;

                        if(_elapsedDuration<_tourDuration)
                        {
                            _setCurrentFrame(_elapsedDuration);  
                            _setCurrentSpeed(_elapsedDuration);
                        }
                        else
                        {    
                            //Tour has finished
                            if(_isSequencePlaying)
                            {
                                //! load next tour
                                playNextTourInCurrentSequence();
                            }
                            else
                            {
                                stopTour();
                            }
                        }

                        //! unsetting flag
                        _useIterator = false;

                    }
                }
            }
            catch(...)
            {
            }
        }

        CORE::Component::update(messageType, message);
    }

    void TourComponent::setTourCurrentTime(unsigned int time)
    {
        _elapsedDuration = time;
        _setCurrentFrame(_elapsedDuration);
        _setCurrentSpeed(_elapsedDuration);
    }

    void TourComponent::previewSnap(unsigned int time)
    {
        _setCurrentFrame(time);
    }

    //!
    bool TourComponent::_writeHeaderToFileV1(std::ofstream &ofstream)
    {
        //Writing the header tag ( tourName StartTime tourDuration )
        if(!ofstream.good())
            return false;

        try
        {
            ofstream    << boost::posix_time::to_iso_string(_tourStartTime) << " "
                << _elapsedDuration << std::endl;
        }
        catch(...)
        {
            return false;
        }

        return true;
    }

    bool 
        TourComponent::_writeHeaderToFileV2(std::ofstream &ofstream)
    {
        const unsigned int VERSION = 2;
        //Writing the header tag ( tourName StartTime tourDuration )
        if(!ofstream.good())
            return false;

        try
        {
            //write the version first
            ofstream << VERSION << '\n';

            ofstream    << boost::posix_time::to_iso_string(_tourStartTime) << " "
                << _elapsedDuration << std::endl;
        }
        catch(...)
        {
            return false;
        }

        return true;
    }

    //!
    bool 
        TourComponent::_writeFrameToFileV1(std::ofstream &ofstream,
        unsigned int time,
        const Frame &frame,
        bool forceFul)
    {
        //Writing the frame tag(Duration AnimationComponentTime CameraProperties)
        if(!ofstream.good())
            return false;

        try
        {
            //Will write only different frames unless specified
            if(forceFul||_prevFrameV1 != frame)
            {
                ofstream << time << " "
                    << boost::posix_time::to_iso_string(frame.animationTime) << " "
                    << frame.cameraProperties.position << " "
                    << frame.cameraProperties.heading << " "
                    << frame.cameraProperties.pitch << " "
                    << frame.cameraProperties.range << std::endl;

                _prevFrameV1 = frame;
            }
        }
        catch(...)
        {
            //Reporting the corrupt frame
            return false;
        }

        return true;
    }

    bool 
        TourComponent::_writeFrameToFileV2(std::ofstream &ofstream,
        unsigned int time,
        const FrameV2 &frame,
        bool forceFul)
    {
        //Writing the frame tag(Duration AnimationComponentTime CameraProperties)
        if(!ofstream.good())
            return false;

        try
        {
            osg::Vec3d eye, center, up;
            frame.viewport.getLookAt(eye, center, up);

            //Will write only different frames unless specified
            if(forceFul || _prevFrameV2 != frame)
            {
                ofstream << time << " "

                    << boost::posix_time::to_iso_string(frame.animationTime) << " "

                    << eye.x() << " " 
                    << eye.y() << " " 
                    << eye.z() << " "

                    << center.x() << " " 
                    << center.y() << " " 
                    << center.z() << " "

                    << up.x() << " " 
                    << up.y() << " " 
                    << up.z() 

                    << std::endl;

                _prevFrameV2 = frame;
            }
        }
        catch(...)
        {
            //Reporting the corrupt frame
            return false;
        }

        return true;
    }

    unsigned int
        TourComponent::getTourDuration() const
    {
        return _tourDuration;
    }

    ITourComponent::TourMode 
        TourComponent::getTourMode() const
    {
        return _mode;
    }

    void TourComponent::deleteTour(const std::string& tourName)
    {
#ifdef WIN32
        const SMCElements::ITourComponent::MapTourToFile::const_iterator iter = _mapTourNameToFile.find(tourName);
#else //WIN32
        SMCElements::ITourComponent::MapTourToFile::iterator iter = _mapTourNameToFile.find(tourName);
#endif //WIN32
        if(iter != _mapTourNameToFile.end())
        {
            _mapTourNameToFile.erase(iter);
        }
    }

    void
        TourComponent::playTour(const std::string &tourName)
    {
        if(_mode == Tour_Recording)
        {
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                "Unable to start tour. Recording in progress",
                __FILE__, __LINE__);
        }

        //Playing the tour
        if(!tourName.empty())
        {
            loadTour(tourName);
        }

        //Else trying to play the loaded tour
        if(_playerState!=ITourComponent::Player_Pause)
        {
            _elapsedDuration    = 0;
        }

        _mode                = ITourComponent::Tour_Playing;
        _playerState        = ITourComponent::Player_Playing;

        //! disable auto play and start the animation component
        _animationComponent->setAutoPlay(false);
        _animationComponent->play();

        _mapTimeToFrameV1Iter = _mapTimeToFrameV1.begin();
        _mapTimeToFrameV2Iter = _mapTimeToFrameV2.begin();


        //subscribing from the tick
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _subscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        // set the manipulator to dummy manipulator
        if(_tourReadVersion == 1)
        {
            _cameraComponent->setMode(VIEW::ICameraComponent::EARTH_MANIPULATOR);
        }
        else if(_tourReadVersion == 2)
        {
            _cameraComponent->setMode(VIEW::ICameraComponent::NONE);
        }
    }

    bool 
        TourComponent::pauseTour()
    {
        if(_mode != Tour_Playing)
            return false;

        if(_playerState==Player_Stop)
            return false;

        //! pause the animation component
        _animationComponent->pause();

        _playerState = Player_Pause;

        // enable the earth manipulator
        if(_tourReadVersion == 2)
        {
            _cameraComponent->setMode(VIEW::ICameraComponent::EARTH_MANIPULATOR);
        }

        //subscribing from the tick 
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        return true;
    }

    bool 
        TourComponent::stopTour()
    {
        if(_mode != Tour_Playing)
            return false;

        //! stop the animation component and enable auto play again
        _animationComponent->stop();
        _animationComponent->setAutoPlay(true);

        _mode         = Tour_None;
        _playerState = Player_Stop;

        // set the mode to earth manipulator
        _cameraComponent->setMode(VIEW::ICameraComponent::EARTH_MANIPULATOR);

        //unsubscribing from the tick
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
        _unsubscribe(worldMaintainer, *CORE::IWorldMaintainer::TickMessageType);

        _elapsedDuration = 0;
        _isSpeedChanged = false;

        //_isSequencePlaying = false;
        //_currentActiveSequenceName = "";

        return true;
    }

    ITourComponent::RecorderState 
        TourComponent::getRecorderStatus() const
    {
        return _recorderState;
    }

    ITourComponent::PlayerState   
        TourComponent::getPlayerStatus() const
    {
        return _playerState;
    }

    std::string TourComponent::getCurrentTourName() const
    {
        return _currentActiveTourName;
    }

    std::string TourComponent::getCurrentSequenceName() const
    {
        return _currentActiveSequenceName;
    }

    void TourComponent::setSequenceName(const std::string& oldSequenceName, const std::string& newSequenceName)
    {
        MapSequenceToTourList::iterator iter = _mapSequenceToTourList.find(oldSequenceName);
        if( iter == _mapSequenceToTourList.end() )
        {
            std::string message = "Sequence with name \'" + oldSequenceName + "\' not found.";
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                message,__FILE__, __LINE__);
        }

        //! check for new name validity
        MapSequenceToTourList::iterator iter2 = _mapSequenceToTourList.find(newSequenceName);
        if(iter2 != _mapSequenceToTourList.end())
        {
            std::string message = "Sequence with name \'" + newSequenceName + "\' already exists.";
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                message,__FILE__, __LINE__);
        }

        // save and erase
        std::vector<std::string> sequence = (*iter).second;
        _mapSequenceToTourList.erase( iter );

        _mapSequenceToTourList[newSequenceName] = sequence;
    }

    bool TourComponent::setTourName( std::string oldTourName, std::string newTourName)
    {
        MapTourToFile::iterator iter = _mapTourNameToFile.find(oldTourName);
        if( iter == _mapTourNameToFile.end() )
        {
            std::string message = "Tour with name \'" + oldTourName + "\' not found.";
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                message,__FILE__, __LINE__);
        }

        //! check for new name validity
        MapTourToFile::iterator iter2 = _mapTourNameToFile.find(newTourName);
        if(iter2 != _mapTourNameToFile.end())
        {
            std::string message = "Tour with name \'" + newTourName + "\' already exists.";
            throw UTIL::Exception(UTIL::BaseExceptionType::GENERAL_EXCEPTION,
                message,__FILE__, __LINE__);
        }

        // save and erase
        std::string tourFileName = (*iter).second;
        _mapTourNameToFile.erase( iter );

        _mapTourNameToFile[newTourName] = tourFileName;

        return true;
    }

    unsigned int TourComponent::getTourCurrentTime() const
    {
        return _elapsedDuration;
    }

    void 
        TourComponent::_setCurrentSpeed(const unsigned int deltaTime)
    {
        if(_readSpeed(deltaTime))
        {
            return;
        }
    }
    void TourComponent::_setCurrentFrame(const unsigned int deltaTime)
    {
        if(_tourReadVersion == 1)
        {
            if(_readFrameV1(deltaTime, _currFrameV1))
            {
                _cameraComponent->setViewPoint(_currFrameV1.cameraProperties.position,
                    _currFrameV1.cameraProperties.range,
                    _currFrameV1.cameraProperties.heading,
                    _currFrameV1.cameraProperties.pitch, 
                    deltaTime);

                _animationComponent->setCurrentDateTime(_currFrameV1.animationTime);
            }
        }
        else if(_tourReadVersion == 2)
        {
            if(_readFrameV2(deltaTime, _currFrameV2))
            {
                _cameraComponent->getCameraManipulator()->setByMatrix(_currFrameV2.viewport);
                _animationComponent->setCurrentDateTime(_currFrameV2.animationTime);
            }
        }
    }

    float
        TourComponent::getSpeedFactor() const
    {
        return _speedFactor;
    }

    void
        TourComponent::setSpeedFactor(float speedFactor)
    {
        _speedFactor = speedFactor;
    }

    void
        TourComponent::increaseSpeedFactor()
    {
        if(_speedFactor < 16.0)
        {
            _speedFactor *= 2;
        }
    }

    void
        TourComponent::decreaseSpeedFactor()
    {
        if(_speedFactor > (1/8.0))
        {
            _speedFactor *= 0.5;
        }
    }

    void 
        TourComponent::setRecorderType(RecorderType type)
    {
        _recorderType = type;
    }

    ITourComponent::RecorderType 
        TourComponent::getRecorderType() const
    {
        return _recorderType;
    }


    void 
        TourComponent::recordKeyframe()
    {
        if(_recorderType == USER_KEYFRAME && _mode != Tour_Playing)
        {
            osg::ref_ptr<osgGA::CameraManipulator> cameraManipulator = 
                _cameraComponent->getCameraManipulator();


            if(!_currFrameV2.isInvalid())
            {
                osg::Matrixd previousViewport = _currFrameV2.viewport;

                osg::Vec3d previousPosition = previousViewport.getTrans();
                osg::Quat previousRotation = previousViewport.getRotate();

                double previousHeight = _getHeightAtPosition(previousPosition);

                _currFrameV2.viewport = cameraManipulator->getMatrix();
                osg::Vec3d currentPosition = _currFrameV2.viewport.getTrans();
                osg::Quat currentRotation = _currFrameV2.viewport.getRotate();

                double currentHeight = _getHeightAtPosition(currentPosition);

                double minHeight = std::min(previousHeight, currentHeight);

                double distance = (currentPosition - previousPosition).length();
                double angle = 0;

                double SPEED_FACTOR = 1;

                double speed = minHeight * SPEED_FACTOR;
                double speedTime = distance / speed;

                osg::Quat result = previousRotation * currentRotation.inverse();
                double rotationInAngle = osg::RadiansToDegrees(2 * acosf(result.w()));

                if(rotationInAngle > 180)
                {
                    rotationInAngle = 360 - rotationInAngle;
                }

                // 10 degree in a sec
                double ROTATION_FACTOR = 0.1;
                double rotationTime = rotationInAngle * ROTATION_FACTOR;

                double time = std::max(rotationTime, speedTime);

                _elapsedDuration += time * 1000000;

                _currFrameV2.animationTime = _animationComponent->getCurrentDateTime();
            }
            else
            {
                _currFrameV2.viewport = cameraManipulator->getMatrix();
                _currFrameV2.animationTime = _animationComponent->getCurrentDateTime();
                _elapsedDuration = 0;
            }

            _mapTimeToFrameV2[_elapsedDuration] = _currFrameV2;
        }
    }

    double
        TourComponent::_getHeightAtPosition(const osg::Vec3d& position)
    {
        const CORE::WorldMap& map = CORE::WorldMaintainer::instance()->getWorldMap();

        CORE::WorldMap::const_iterator itw = map.begin();
        CORE::RefPtr<CORE::IWorld> world = itw->second;

        osg::Vec3d end(0, 0, 0);
        osg::Vec3d start = position;

        //checking intersection with all object in the world
        osg::ref_ptr<osgUtil::LineSegmentIntersector> li = new osgUtil::LineSegmentIntersector(start, end);
        osgUtil::IntersectionVisitor iv(li.get());
        iv.setTraversalMask(0xffffffff);
        world->getOSGGroup()->accept(iv);
        if (li->containsIntersections())
        {
            osg::Vec3d intersectionPoint = li->getFirstIntersection().getWorldIntersectPoint();
            return (position - intersectionPoint).length();
        }
        else
        {
            osg::Vec3d geodaticValue = UTIL::CoordinateConversionUtils::ECEFToGeodetic(position);
            return geodaticValue.z();
        }
    }

    void TourComponent::setTourToPlayOnStart(std::string tourName)
    {
        _tourToPlayOnStart = tourName;
    }

    std::string TourComponent::getTourNameToPlayOnStart() const
    {
        return _tourToPlayOnStart;
    }
}
