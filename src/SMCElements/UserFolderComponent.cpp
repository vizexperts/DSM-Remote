#include <SMCElements/UserFolderComponent.h>
#include <SMCElements/UserFolder.h>
#include <SMCElements/SMCElementsPlugin.h>
#include <Core/WorldMaintainer.h>
#include <Core/CoreRegistry.h>

#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <serialization/IProjectLoaderComponent.h>
#include <serialization/IProjectLoadingStatusMessage.h>
#include <Elements/ElementsUtils.h>

//#include <Core/IObject.h>
#include <serialization/AsciiStreamOperator.h>

#include <App/AccessElementUtils.h>
#include <App/IApplication.h>

namespace SMCElements
{

    DEFINE_IREFERENCED(UserFolderComponent, CORE::Component);
    DEFINE_META_BASE(SMCElements, UserFolderComponent);

    UserFolderComponent::UserFolderComponent() :_isReloading(false)
    {
        _addInterface(IUserFolderComponent::getInterfaceName());

        _foldersFilename = "UserFolders.pro";
        _sendMessages = true;
    }

    UserFolderComponent::~UserFolderComponent()
    {

    }

    void UserFolderComponent::update(const CORE::IMessageType &messageType, const CORE::IMessage &message)
    {
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {

            _rootFolder = CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*SMCElements::SMCElementsRegistryPlugin::UserFolderType)->getInterface<SMCElements::IUserFolderItem>();

            CORE::RefPtr<DB::IProjectLoaderComponent> projLoaderComp = ELEMENTS::GetComponentFromMaintainer("ProjectLoaderComponent")->getInterface<DB::IProjectLoaderComponent>();
            if(projLoaderComp.valid())
            {
                _subscribe(projLoaderComp.get(), *DB::IProjectLoadingStatusMessage::ProjectLoadingStatusMessageType);
            }
        }
        else if(messageType == *DB::IProjectLoadingStatusMessage::ProjectLoadingStatusMessageType)
        {

            DB::IProjectLoadingStatusMessage* projectLoadingStatusMessage = 
                message.getInterface<DB::IProjectLoadingStatusMessage>();

            if(projectLoadingStatusMessage->getStatus() == DB::IProjectLoadingStatusMessage::SUCCESS)
            {
                LOG_DEBUG("Received ProjectLoadingStatusMessageType Success..");
                osg::ref_ptr<UTIL::ThreadPool> tp = _wm->getComputeThreadPool();

                _sendMessages = false;
                _isReloading =  projectLoadingStatusMessage->getReloading();
                tp->invoke(boost::bind(&UserFolderComponent::_readFoldersFile, this));

            }
            
        }
        else if (messageType == *CORE::IWorld::WorldRemovedMessageType)
        {
            _rootFolder = NULL;
        }

        CORE::Component::update(messageType, message);
    }

    void UserFolderComponent::onAddedToWorldMaintainer()
    {
        _subscribe(_wm, *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(_wm, *CORE::IWorld::WorldRemovedMessageType);

        CORE::Component::onAddedToWorldMaintainer();
    }

    bool UserFolderComponent::sendObjectMessages() const
    {
        return _sendMessages;
    }

    void UserFolderComponent::onRemovedFromWorldMaintainer()
    {
        _unsubscribe(_wm, *CORE::IWorld::WorldLoadedMessageType);
        _unsubscribe(_wm, *CORE::IWorld::WorldRemovedMessageType);

        CORE::Component::onRemovedFromWorldMaintainer();
    }

    IUserFolderItem* UserFolderComponent::getRootFolder() const
    {
        return _rootFolder.get();
    }

    std::string UserFolderComponent::getFoldersFilename() const
    {
        return _foldersFilename;
    }

    void UserFolderComponent::_readFoldersFile()
    {
        std::string projectFolder = osgDB::getFilePath(_wm->getProjectFile());

        std::string filename = projectFolder + "/" + _foldersFilename;

        if(!osgDB::fileExists(filename))
        {
            _sendMessages = true;
            return;
        }

        std::ios::openmode mode = std::ios::in;
        std::ifstream istream(filename.c_str(), mode);

        std::string header;
        istream >> header;

        CORE::RefPtr<DB::InputIterator> ii = NULL;
        if(header == "#Ascii")
        {
            ii = new AsciiInputIterator(&istream);
        }
        else
        {
            _sendMessages = true;
            return;
        }

        DB::InputStream is(NULL);
        is.start(ii.get());

        while(is.checkStream())
        {
            CORE::IBase* object = is.readBase();
            if(object && object->getInterface<SMCElements::IUserFolderItem>())
            {
                if (_isReloading)
                {

                    CORE::RefPtr<CORE::IObject> srcObj = _rootFolder->getInterface<CORE::IObject>();
                    CORE::RefPtr<CORE::IObject> refenceObj = object->getInterface<CORE::IObject>();
                    _compareAndAssignHeirarchy(srcObj, refenceObj);
                    _isReloading = false;
                }
                else
                {
                    _rootFolder = object->getInterface<SMCElements::IUserFolderItem>();
                }
            }

            is.advanceToCurrentEndBracket();
        }

        _sendMessages = true;

        //! Send configuration loaded message type
        CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
        CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*SMCElements::IUserFolderComponent::UserFolderConfigurationLoadedMessageType);
        msg->setSender(this);
        notifyObservers(*SMCElements::IUserFolderComponent::UserFolderConfigurationLoadedMessageType, *msg, CORE::IObservable::ASYNCHRONOUS);

    }
    void UserFolderComponent::_compareAndAssignHeirarchy(CORE::RefPtr<CORE::IObject> &srcObj, const CORE::RefPtr<CORE::IObject> &refenceObj) const
    {
        if (!srcObj.valid())
        {
            if (!refenceObj.valid())
                return;
            // Assigning  refrence object to source object.
            srcObj = refenceObj;
            return;
        }

        CORE::RefPtr<SMCElements::IUserFolderObjectWrapper> srcUserFolderObjectWrapper = srcObj->getInterface<SMCElements::IUserFolderObjectWrapper>();
        CORE::RefPtr<SMCElements::IUserFolderObjectWrapper> refUserFolderObjectWrapper = refenceObj->getInterface<SMCElements::IUserFolderObjectWrapper>();

        // assinging refrence object to src folder wrapper.
        if (refUserFolderObjectWrapper.valid() && refUserFolderObjectWrapper->getReferencedObject())
        {
            if (srcUserFolderObjectWrapper.valid())
                srcUserFolderObjectWrapper->setReferencedObject(refUserFolderObjectWrapper->getReferencedObject());
            else
                srcUserFolderObjectWrapper = refUserFolderObjectWrapper;
            return;
        }

        CORE::RefPtr<SMCElements::IUserFolderItem> srcUserFolderItem = srcObj->getInterface<SMCElements::IUserFolderItem>();
        CORE::RefPtr<SMCElements::IUserFolderItem> refUserFolderItem = refenceObj->getInterface<SMCElements::IUserFolderItem>();


        if (!srcUserFolderItem.valid())
        {
            if (!refUserFolderItem.valid())
                return;
            // Assigning  refrence object to source object.
            srcUserFolderItem = refUserFolderItem;
            return;
        }

        const IUserFolderItem::UserFolderCollection::UIDBaseMap& srcObjMap = srcUserFolderItem->getChildrenList();
        const IUserFolderItem::UserFolderCollection::UIDBaseMap& refenceObjMap = refUserFolderItem->getChildrenList();

        SMCElements::IUserFolderItem::UserFolderCollection::UIDBaseMap::const_iterator srciter = srcObjMap.begin();
        SMCElements::IUserFolderItem::UserFolderCollection::UIDBaseMap::const_iterator refiter = refenceObjMap.begin();


        while (refiter != refenceObjMap.end())
        {
            bool bPresent = false;
            srciter = srcObjMap.begin();
            while (srciter != srcObjMap.end())
            {
                if (*refiter->first == *srciter->first)
                {
                    bPresent = true;
                    break;
                }
                ++srciter;
            }
            if (!bPresent)
            {
                srcUserFolderItem->addChild(refiter->second);
            }
            ++refiter;
        }


        srciter = srcObjMap.begin();
        refiter = refenceObjMap.begin();
        while (srciter != srcObjMap.end() && refiter != refenceObjMap.end())
        {
            CORE::RefPtr<CORE::IObject> srcPassObj = srciter->second;
            CORE::RefPtr<CORE::IObject> refPassObj = refiter->second;
            _compareAndAssignHeirarchy(srcPassObj, refPassObj);
            ++srciter;
            ++refiter;
        }
    }
}