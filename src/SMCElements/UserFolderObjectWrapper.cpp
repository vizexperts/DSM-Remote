#include <SMCElements/UserFolderObjectWrapper.h>
#include <iostream>

namespace SMCElements
{
    DEFINE_META_BASE(SMCElements, UserFolderObjectWrapper);
    DEFINE_IREFERENCED(UserFolderObjectWrapper, CORE::Object);

    UserFolderObjectWrapper::UserFolderObjectWrapper()
    {
        _addInterface(SMCElements::IUserFolderObjectWrapper::getInterfaceName());
    }

    UserFolderObjectWrapper::~UserFolderObjectWrapper()
    {
    }

    std::string UserFolderObjectWrapper::getName() const
    {
        if(_object.valid())
        {
            return _object->getInterface<CORE::IBase>()->getName();
        }

        return "";
    }

    void UserFolderObjectWrapper::setName(const std::string& name)
    {
        if(_object.valid())
        {
            _object->getInterface<CORE::IBase>()->setName(name);
        }
    }

    osg::Node* UserFolderObjectWrapper::getOSGNode() const
    {
        return NULL;
    }

    void UserFolderObjectWrapper::setReferencedObject(CORE::IObject *referencedObject)
    {
        _object = referencedObject;
    }

    CORE::IObject* UserFolderObjectWrapper::getReferencedObject() const
    {
        return _object.get();
    }
}