#include <Core/Referenced.h>
#include <Core/Base.h>
#include <Core/IBaseUtils.h>
#include <Core/Component.h>
#include <Core/IMessageType.h>
#include <Core/CoreRegistry.h>
#include <Core/AttributeUtils.h>
#include <Core/AttributeTypes.h>

#include <Util/CoordinateConversionUtils.h>
#include <Util/StringUtils.h>

#include <iostream>

#include <SMCElements/PresetComponent.h>
#include <SMCElements/SMCElementsPlugin.h>

#include <OpenThreads/ScopedLock>

#include <Os/osSelect.h>
#include <Os/osSocket.h>

#include <stdio.h>

#ifdef WIN32
#include <io.h>
#endif //WIN32

#include <fcntl.h>
#include <iostream>
#include <string>

#ifdef WIN32
#include <windows.h>
#endif //WIN32

#include <osg/ref_ptr>


OS::SocketTCP *tcpSock = NULL ;
OS::SocketUDP *udpSock = NULL;

namespace SMCElements
{
    DEFINE_META_BASE(SMCElements, PresetComponent);
    DEFINE_IREFERENCED(PresetComponent, CORE::Component);

    PresetComponent::PresetComponent()
        : _startTCPConnection(false)
        , _startUDPConnection(false)
        , _stopTCPConnection(false)
        , _stopUDPConnection(false)
        , _connectionAllowed(false)
#ifdef WIN32
        , _sockListner(false)
#endif //WIN32
    {
        _serverIP = "192.168.0.2";
        _portNumber = "4242";

         tp = NULL;
        _addInterface(SMCElements::IPresetComponent::getInterfaceName());
    }

    void 
    PresetComponent::initializeAttributes()
    {
        Component::initializeAttributes();

        _addAttribute(new CORE::StringAttribute("IPADDRESS", "IPADDRESS",
                        CORE::StringAttribute::SetFuncType(this, &PresetComponent::setIPAddress),
                        CORE::StringAttribute::GetFuncType(this, &PresetComponent::getIPAddress),
                        "IPADDRESS",
                        "IPADDRESS attributes"));

        _addAttribute(new CORE::StringAttribute("PORTNUMBER", "PORTNUMBER",
                        CORE::StringAttribute::SetFuncType(this, &PresetComponent::setPortNumber),
                        CORE::StringAttribute::GetFuncType(this, &PresetComponent::getPortNumber),
                        "PORTNUMBER",
                        "PORTNUMBER attributes"));
    }

     //Set the port number
     void 
     PresetComponent::setPortNumber(const std::string& port)
     {
         // OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
         _portNumber = port;
     }

    std::string 
    PresetComponent::getPortNumber()
    {
        return _portNumber;
    }

    void 
    PresetComponent::setIPAddress(const std::string& ipAddress)
    {
        _serverIP = ipAddress;
    }
    
    std::string 
    PresetComponent::getIPAddress()
    {
        return _serverIP;
    }

    void 
    PresetComponent::onAddedToWorldMaintainer()
    {
        CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();

        _subscribe(worldMaintainer, *CORE::IWorld::WorldLoadedMessageType);
        _subscribe(worldMaintainer, *CORE::IWorld::WorldRemovedMessageType);
    }

    void 
    PresetComponent::onRemovedFromWorldMaintainer()
    {
        /*CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();

        _unsubscribe(worldMaintainer, *CORE::IWorld::WorldLoadedMessageType);
        _unsubscribe(worldMaintainer, *CORE::IWorld::WorldRemovedMessageType);*/
    }

    void 
    PresetComponent::update(const CORE::IMessageType& messageType, const CORE::IMessage& message)
    {
        if(messageType == *CORE::IWorld::WorldLoadedMessageType)
        {
            _ConnectionSetupStatus();

            CORE::IWorldMaintainer *worldMaintainer = CORE::WorldMaintainer::instance();
            _unsubscribe(worldMaintainer, *CORE::IWorld::WorldLoadedMessageType);
        }
    }
     //Start the socket connection
     void 
     PresetComponent::start()
     {
         _ConnectionSetupStatus();
         return;
     }

     //Stop the previous socket connection
     void 
     PresetComponent::stop()
     {
         OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);

         _startTCPConnection = false;
         _startUDPConnection = false;
         _stopTCPConnection  = true;
         _stopUDPConnection  = true;

     }

     //Responsible to set the connection based on connection type
     void
     PresetComponent::_ConnectionSetupStatus()
     {
        
        if(_wmain == NULL)
        {
            _wmain = CORE::WorldMaintainer::instance();
        }

        //Wait till the precious socket connection is stopped
        //Checking for either the thread is actually running or just the value of _connectionAllowed is changed manually
        int count = 0;
        while(_connectionAllowed)
        {
            count++;
            if(count > 10000000)
            {
                _connectionAllowed = false;
                _cleanUp();
            }
        }
       
        //if((_socketType == TCP))
        //{
            _connectionAllowed = true;
            _startTCPConnection = true;  
            _stopTCPConnection = false;
            _stopUDPConnection = true;
            _startUDPConnection = false;
            
            tp = CORE::WorldMaintainer::instance()->getComputeThreadPool();
            tp->invoke(boost::bind(&PresetComponent::_setupTCPConnection, this));
        //}
        //else if((_socketType == UDP))
        //{
        //    _connectionAllowed = true;
        //    _startUDPConnection = true;
        //    _stopUDPConnection = false;
        //    _stopTCPConnection = true;
        //    _startTCPConnection = false;

        //    tp = _wmain->getComputeThreadPool();
        //    tp->invoke(boost::bind(&PresetComponent::_setupUDPConnection, this,_serverIP,indiUtil::ToDouble(_portNumber)));

        //}
        //else
        //{
        //    //  return 
        //}
     }

     //Responsible to send message to the subscribed observers
    void
    PresetComponent::_sendMessage(CORE::IMessageType* messageType)
    {

        /*indiCore::RefPtr<indiCore::IMessageFactory> mf = indiCore::CoreRegistry::instance()->getMessageFactory();
        indiCore::RefPtr<indiCore::IMessage> msg = mf->createMessage(*messageType);
        notifyObservers(*messageType, *msg);*/
    }

    //clean up the socket resources and reset the socket variables
    void 
    PresetComponent::_cleanUp()
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
         _startTCPConnection = false;
         _stopTCPConnection = true;
         _connectionAllowed = false;
         if(tcpSock)
             tcpSock->close();
         tcpSock = NULL;

         _startUDPConnection = false;
         _stopUDPConnection = true;
         _connectionAllowed = false;
         if(udpSock)
                udpSock->close();
         udpSock = NULL;
    }

    //Check whether the established connection need to be stopped
    bool 
    PresetComponent::_needToDisconnect()
    {
        //OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
           _cleanUp();
           return true;
    }


    //Set up the TCP connection
    void 
    PresetComponent::_setupTCPConnection()
    {
        int portNumber = atoi(_portNumber.c_str());
#ifdef WIN32
        if(!_sockListner)
            _sockListner = new WallLauncher(_serverIP, portNumber);

        if(_sockListner->startListening())
        {
            std::cout << "Socket Listening.\n";
            while(true)
            {
                std::string req;
                if(_sockListner->hasNext())
                {
                    req = _sockListner->fetchNextRequest();
                    _processRequest(req);
                    std::cout << "The request received : " << req << "\n"; 
                }
                else
                {
#ifdef WIN32
                    Sleep(2000);
#else //WIN32
                    usleep(2000000);
#endif //WIN32
                }
            }
        }
#endif //WIN32
    }

    void
    PresetComponent::_processRequest(const std::string &req)
    {
        if(req=="1")
        {
            /*system("nircmd mutesysvol 0");*/
            osg::DisplaySettings::instance()->setStereo(true);
            osg::DisplaySettings::instance()->setStereoMode(osg::DisplaySettings::StereoMode(osg::DisplaySettings::QUAD_BUFFER));
        }
        else if(req=="2")
        {
            /*system("nircmd mutesysvol 1");*/
            osg::DisplaySettings::instance()->setStereo(false);
        }
    }


     //Set up the UDP connection
     void
     PresetComponent::_setupUDPConnection(std::string _serverIP,double _portNumber)
     {

         //while(_connectionAllowed)
         //{
         //    try{
         //           if(_needToDisconnect())
         //                   return;

         //           if((udpSock ==  NULL))
         //           {
         //               udpSock = new OS::SocketUDP(_serverIP.c_str(), (unsigned short)_portNumber);
         //               if(!udpSock->isValid())
         //               {
         //                    if(_needToDisconnect())
         //                           return;

         //                    //Send message that the connection cann't be eastablished and exit from the thread
         //                    _sendMessage(indiElements::IWeather::ConnectionFailedMessageType.get());
         //                    _cleanUp();
         //                    return;
         //               }
         //               else
         //               {
         //                     //Send message that the connection is eastablished 
         //                    _sendMessage(indiElements::IWeather::ConnectionSuccessfulMessageType.get());
         //                    OpenThreads::ScopedLock<OpenThreads::Mutex> lock(_mutex);
         //                    _startUDPConnection = true;

         //                    if(_needToDisconnect())
         //                        return;
         //               }
         //           }

         //            //The connection does exist so take the socket and receive the data
         //           else
         //           {
         //              //About to start the exchange of message between the server and this client
         //               while(_startUDPConnection && !_stopUDPConnection && _connectionAllowed)
         //               {
         //                   WeatherParameter weather;

         //                   if(udpSock == NULL)
         //                   {
         //                       _connectionAllowed = false;
         //                        return;
         //                   }
         //                    //The message couldn't be send , so notify the Observers about that and close the connection
         //                   if(!udpSock->send(&weather,sizeof(weather)))
         //                   {
         //                       _sendMessage(indiElements::IWeather::ConnectionFailedMessageType.get());
         //                       _cleanUp();
         //                       return;
         //                   }
         //                   else
         //                   {
         //                      //If new data has arrived check whether the data is differnt from the last received data
         //                       if(udpSock->recv(&weather,sizeof(weather)))
         //                       {
         //                             if( (_initialParam.position != weather.position) || (std::strcmp(_initialParam.pressureType,weather.pressureType))
         //                                 || (std::strcmp(_initialParam.humidity, weather.humidity))|| (std::strcmp(_initialParam.temperatureType,weather.temperatureType)) ||
         //                                 (std::strcmp(_initialParam.wind_speedType,weather.wind_speedType)) || (std::strcmp(_initialParam.temperature, weather.temperature)) ||
         //                                 (std::strcmp(_initialParam.pressure,weather.pressure)) || (std::strcmp(_initialParam.wind_speed, weather.wind_speed)) )
         //                             {
         //                                 _getValue(weather);
         //                                 _sendMessage(indiElements::IWeather::WeatherValueChangedMessageType.get());
         //                             }
         //                       }
         //                       else
         //                       {
         //                            //No data is received , so notify the Observers about that and close the connection
         //                            _sendMessage(indiElements::IWeather::ConnectionFailedMessageType.get());
         //                            _cleanUp();
         //                             return;
         //                       }
         //                    }
         //                 }

         //               if(_needToDisconnect())
         //                    return;
         //            }
         //       }
         //      //Check for any unexpected exception
         //      catch(...)
         //      {
         //          _cleanUp();
         //          return;
         //      }
         // }
         // return;
     }
} //namespace INDIELEMENT
