#include <SMCElements/UserFolder.h>
#include <Elements/ElementsUtils.h>
#include <Core/IMessageFactory.h>
#include <Core/CoreRegistry.h>
#include <Core/IObjectMessage.h>

namespace SMCElements
{
    DEFINE_META_BASE(SMCElements, UserFolder);
    DEFINE_IREFERENCED(UserFolder, CORE::Object);

    UserFolder::UserFolder() 
        : _visible(true)
    {
        _addInterface(SMCElements::IUserFolderItem::getInterfaceName());
        _addInterface(CORE::IVisibility::getInterfaceName());
        _addInterface(CORE::IDeletable::getInterfaceName());
        _addInterface(CORE::ISelectable::getInterfaceName());
    }

    void UserFolder::addChild(CORE::IObject* child)
    {
        if(child == NULL)
            return;

        _children.add(child);
        child->setParent(this);

        if(!_folderComp.valid())
        {
            CORE::RefPtr<CORE::IComponent> comp = ELEMENTS::GetComponentFromMaintainer("UserFolderComponent");
            _folderComp = comp->getInterface<SMCElements::IUserFolderComponent>(); 
        }

        if(_folderComp.valid() && _folderComp->sendObjectMessages())
        {
            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*CORE::IObjectMessage::ObjectAddedMessageType);
            msg->getInterface<CORE::IObjectMessage>()->setObject(child);
            msg->setSender(_folderComp.get()->getInterface<CORE::IBase>());

            _folderComp->getInterface<CORE::IObservable>()->notifyObservers(*CORE::IObjectMessage::ObjectAddedMessageType, *msg, CORE::IObservable::ASYNCHRONOUS);
        }
    }

    void UserFolder::remove()
    {
        if(_parent != NULL)
        {
            _parent->getInterface<SMCElements::IUserFolderItem>()->removeChild(this);
        }
    }

    bool UserFolder::removeChild(CORE::IObject* child)
    {
        if(child == NULL)
            return false;

        _children.remove(child);

        if(!_folderComp.valid())
        {
            CORE::RefPtr<CORE::IComponent> comp = ELEMENTS::GetComponentFromMaintainer("UserFolderComponent");
            _folderComp = comp->getInterface<SMCElements::IUserFolderComponent>(); 
        }

        if(_folderComp.valid() && _folderComp->sendObjectMessages())
        {
            CORE::RefPtr<CORE::IMessageFactory> mf = CORE::CoreRegistry::instance()->getMessageFactory();
            CORE::RefPtr<CORE::IMessage> msg = mf->createMessage(*CORE::IObjectMessage::ObjectRemovedMessageType);
            msg->getInterface<CORE::IObjectMessage>()->setObject(child);
            msg->setSender(_folderComp.get()->getInterface<CORE::IBase>());

            _folderComp->getInterface<CORE::IObservable>()->notifyObservers(*CORE::IObjectMessage::ObjectRemovedMessageType, *msg, CORE::IObservable::ASYNCHRONOUS);
        }

        return false;
    }

    void UserFolder::setSelected(bool selected)
    {
        _selected = selected;
    }

    bool UserFolder::getSelected() const
    {
        return _selected;
    }


    const IUserFolderItem::UserFolderCollection::UIDBaseMap& UserFolder::getChildrenList() const
    {
        return _children.getUIDBaseMap();
    }

    osg::Node* UserFolder::getOSGNode() const
    {
        return NULL;
    }

    void UserFolder::setVisibility(bool visible)
    {
        _visible = visible;
    }

    bool UserFolder::getVisibility() const
    {
        return _visible;
    }

}