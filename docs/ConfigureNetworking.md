Environment variable and settings decription

- Environment variaables

-- SDK/DSM Specific

--- TERRAINSDK_APP_FOLDER : (directory url) Set the directory to be used for daving user project, styles and settings data. If not set, APPDATA is used.
e.g. set TERRAINSDK_APP_FOLDER=C:\path\to\some\directory

--- TKP_SDK_TILE_RANGE_FACTOR : (int) , 2- 8 , default : 4

--- TERRAINSDK_LOG_LEVEL : (enum) Same as osg log level variable,
ALWAYS
ERROR
WARN
INFO
DEBUG

-- OSG specific variables

--- OSG_COMPUTE_NEAR_FAR_MODE : (enum) Specify near far computation mode for culling
DO_NOT_COMPUTE_NEAR_FAR
COMPUTE_NEAR_FAR_USING_BOUNDING_VOLUMES
COMPUTE_NEAR_FAR_USING_PRIMITIVES

--- OSG_NEAR_FAR_RATIO : Float value specifying near far ratio , default : 0.000001

--- OSG_DISPLAY_TYPE : (enum)
MONITOR
POWERWALL
REALITY_CENTER
HEAD_MOUNTED_DISPLAY

--- OSG_STEREO_MODE (enum)
OSG_STEREO_MODE
QUAD_BUFFER
ANAGLYPHIC
HORIZONTAL_SPLIT
VERTICAL_SPLIT
LEFT_EYE
RIGHT_EYE
HORIZONTAL_INTERLACE
VERTICAL_INTERLACE

--- OSG_STEREO (enum)
OFF
ON

http://trac.openscenegraph.org/projects/osg//wiki/Support/UserGuides/EnvironmentVariables

--- Osgearth specific variables

http://docs.osgearth.org/en/latest/references/envvars.html