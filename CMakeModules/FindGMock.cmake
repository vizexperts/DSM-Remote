# Locate boost
# This module defines
# gmock_LIBRARY
# gmock_FOUND, if false
# GMOCK_INCLUDE_DIR, where to find the headers
#
# $GMOCK_DIR is an environment variable that would
# correspond to the ./configure --prefix=$GMOCK_DIR
#
# Created by Udit

FIND_PATH(GMOCK_INCLUDE_DIR gmock.h
        $ENV{GMOCK_DIR}/include
        NO_DEFAULT_PATH
        )

MACRO(FIND_GMOCK_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{GMOCK_DIR} ${BUILD})

ENDMACRO(FIND_GMOCK_LIBRARY )

# optimized
IF (WIN32)
	FIND_GMOCK_LIBRARY(GMOCK_LIBRARY            gmock          release)
    FIND_GMOCK_LIBRARY(GMOCK_LIBRARY_DEBUG      gmock          debug)
ELSE (WIN32)
    FIND_GMOCK_LIBRARY(GMOCK_LIBRARY            gmock          release)
    FIND_GMOCK_LIBRARY(GMOCK_LIBRARY_DEBUG      gmock          debug)
ENDIF (WIN32)

SET(GMOCK_FOUND "NO")
IF(GMOCK_LIBRARY AND GMOCK_INCLUDE_DIR)
   SET(GMOCK_FOUND "YES")
ENDIF(GMOCK_LIBRARY AND GMOCK_INCLUDE_DIR)
