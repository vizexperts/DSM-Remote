# Created by Nishant Singh

FIND_PATH(DGNLIB_INCLUDE_DIR dgnlib.h
    $ENV{DGNLIB_DIR}/include
    NO_DEFAULT_PATH
)

MACRO(FIND_DGNLIB_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{DGNLIB_DIR} ${BUILD})

ENDMACRO(FIND_DGNLIB_LIBRARY )

IF (WIN32)
    # Find release (optimized) libs
   FIND_DGNLIB_LIBRARY(DGNLIB_LIBRARY              dgnlib_i        release)
    # Find debug libs
    FIND_DGNLIB_LIBRARY(DGNLIB_LIBRARY_DEBUG        dgnlib_i        debug)
ELSE (WIN32)
    # Find release (optimized) libs
    FIND_DGNLIB_LIBRARY(DGNLIB_LIBRARY              dgn             release)
    # Find debug libs
    FIND_DGNLIB_LIBRARY(DGNLIB_LIBRARY_DEBUG        dgn             debug)
ENDIF (WIN32)

SET(DGNLIB_FOUND "NO")
IF(DGNLIB_LIBRARY AND DGNLIB_INCLUDE_DIR)
    SET(DGNLIB_FOUND "YES")
ENDIF(DGNLIB_LIBRARY AND DGNLIB_INCLUDE_DIR)
