# Locate FMOD
# This module defines
# FMOD_FOUND, if false, do not try to link to Fmod
# FMOD_INCLUDE_DIR, where to find the headers
#
# $FMOD_DIR is an environment variable that would
# correspond to the ./configure --prefix=$FMOD_DIR
#
# Created by Nishant

FIND_PATH(FMOD_INCLUDE_DIR fmod.h
	    
        $ENV{FMOD_DIR}/include
        NO_DEFAULT_PATH
        )

MACRO(FIND_FMOD_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{FMOD_DIR} ${BUILD})

ENDMACRO(FIND_FMOD_LIBRARY )

# optimized
IF (WIN32)
    # FIND_FMOD_LIBRARY(FMOD_LIBRARY             fmodex_vc    release)
    FIND_FMOD_LIBRARY(FMOD_LIBRARY             fmod64_vc    release)
    # FIND_FMOD_LIBRARY(FMOD_LIBRARY_DEBUG       fmodex_vc    release)	
    FIND_FMOD_LIBRARY(FMOD_LIBRARY_DEBUG       fmod64_vc    release)
ELSE (WIN32)
    FIND_FMOD_LIBRARY(FMOD_LIBRARY             fmodex64     release)
    FIND_FMOD_LIBRARY(FMOD_LIBRARY_DEBUG       fmodex64     release)	
ENDIF (WIN32)

SET(FMOD_FOUND "NO")
IF(FMOD_LIBRARY AND FMOD_INCLUDE_DIR)
    SET(FMOD_FOUND "YES")
ELSE(FMOD_LIBRARY AND FMOD_INCLUDE_DIR)
     MESSAGE("FMOD Not Found")
ENDIF(FMOD_LIBRARY AND FMOD_INCLUDE_DIR)
