# Locate OSG
# This module defines
# OSG_EARTH_LIBRARY
# OSG_EARTH_FOUND, if false, do not try to link to OSG
# OSG_EARTH_INCLUDE_DIR, where to find the headers
#
# OSGEARTH_DIR is an environment variable that would
# correspond to the ./configure --prefix=$OSGEARTH_DIR
#
# Created by Vaibhav Bansal

###### headers ######
FIND_PATH(OSG_EARTH_INCLUDE_DIR osgEarth/Map    

    $ENV{OSGEARTH_DIR}/include
    NO_DEFAULT_PATH

)


###### libraries ######
MACRO(FIND_OSG_EARTH_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)
	
    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{OSGEARTH_DIR} ${BUILD})

ENDMACRO(FIND_OSG_EARTH_LIBRARY )

MACRO(FIND_OSG_EARTH_PLUGIN MYLIBRARY MYLIBRARYNAME)

	FIND_FILE(${MYLIBRARY}
			${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        $ENV{OSGEARTH_LIB}
        $ENV{OSGEARTH_DIR}/lib
        $ENV{OSGEARTH_DIR}/lib/${OS}/$ENV{CONPILER}/$ENV{OSSARCH}/release
        $ENV{OSGEARTH_DIR}/lib/${OS}/$ENV{CONPILER}/$ENV{OSSARCH}/debug  
	)

ENDMACRO(FIND_OSG_EARTH_PLUGIN )

FIND_OSG_EARTH_LIBRARY(OSG_EARTH_LIBRARY                         osgEarth               release)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_UTIL_LIBRARY	                 osgEarthUtil           release)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_ANNOTATION_LIBRARY	             osgEarthAnnotation     release)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_SYMBOLOGY_LIBRARY	             osgEarthSymbology      release)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_FEATURES_LIBRARY	             osgEarthFeatures       release)
#FIND_OSG_EARTH_LIBRARY(OSG_EARTH_WEBVIEW_LIBRARY	             osgEarthWebView        release)

FIND_OSG_EARTH_LIBRARY(OSG_EARTH_LIBRARY_DEBUG	                 osgEarthd               debug)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_UTIL_LIBRARY_DEBUG	             osgEarthUtild           debug)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_ANNOTATION_LIBRARY_DEBUG	     osgEarthAnnotationd     debug)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_SYMBOLOGY_LIBRARY_DEBUG	     osgEarthSymbologyd      debug)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_FEATURES_LIBRARY_DEBUG	         osgEarthFeaturesd       debug)
#FIND_OSG_EARTH_LIBRARY(OSG_EARTH_WEBVIEW_LIBRARY_DEBUG           osgEarthWebViewd        debug)

IF(UNIX)
FIND_OSG_EARTH_PLUGIN(OSG_EARTH_ENGINE_PLUGIN_LIBRARY            osgdb_osgearth_engine_osgterrain )
ENDIF(UNIX)

SET( OSG_EARTH_FOUND "NO" )
IF( OSG_EARTH_LIBRARY AND OSG_EARTH_INCLUDE_DIR )
    SET( OSG_EARTH_FOUND "YES" )
ENDIF( OSG_EARTH_LIBRARY AND OSG_EARTH_INCLUDE_DIR )


