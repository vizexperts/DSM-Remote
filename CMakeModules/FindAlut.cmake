# Locate ALUT
# This module defines
# ALUT_THREAD_LIBRARY
# ALUT_FOUND, if false, do not try to link to gdal
# ALUT_INCLUDE_DIR, where to find the headers
#
# $ALUT_DIR is an environment variable that would
# correspond to the ./configure --prefix=$ALUT_DIR
#
# Created by Nishant

FIND_PATH(ALUT_INCLUDE_DIR AL/alut.h
            $ENV{ALUT_DIR}/include
            NO_DEFAULT_PATH
            )

MACRO(FIND_ALUT_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB( ${MYLIBRARY} ${MYLIBRARYNAME} $ENV{ALUT_DIR} ${BUILD})

ENDMACRO(FIND_ALUT_LIBRARY )

# optimized
IF (WIN32)
    FIND_ALUT_LIBRARY(ALUT_LIBRARY             alut     debug)
    FIND_ALUT_LIBRARY(ALUT_LIBRARY_DEBUG       alut     release)
ELSE (WIN32)
ENDIF (WIN32)

SET(ALUT_FOUND "NO")
IF(ALUT_LIBRARY AND ALUT_INCLUDE_DIR)
    SET(ALUT_FOUND "YES")
ELSE(ALUT_LIBRARY AND ALUT_INCLUDE_DIR)
     MESSAGE("ALUT Not Found")
ENDIF(ALUT_LIBRARY AND ALUT_INCLUDE_DIR)