# Locate LIBJSON
# This module defines
# LIBJSON_LIBRARY
# LIBJSON_FOUND, if false, do not try to link to LIBJSON
# LIBJSON_INCLUDE_DIR, where to find the headers
#
# $LIBJSON_DIR is an environment variable that would
# correspond to the ./configure --prefix=$LIBJSON_DIR
#

FIND_PATH(LIBJSON_INCLUDE_DIR libjson.h
    
    $ENV{LIBJSON_DIR}/include
   	NO_DEFAULT_PATH
)

MACRO(FIND_LIBJSON_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{LIBJSON_DIR} ${BUILD})

ENDMACRO(FIND_LIBJSON_LIBRARY )

IF (WIN32)
	# Find release (optimized) libs
	FIND_LIBJSON_LIBRARY(LIBJSON_LIBRARY 			statLibJson 		release)
	# Find debug libs
	FIND_LIBJSON_LIBRARY(LIBJSON_LIBRARY_DEBUG 		statLibJson 		debug)
ELSE (WIN32)
	# Find release (optimized) libs
	FIND_LIBJSON_LIBRARY(LIBJSON_LIBRARY 			json 			release)
	# Find debug libs
	FIND_LIBJSON_LIBRARY(LIBJSON_LIBRARY_DEBUG 		json 			debug)	
ENDIF (WIN32)

SET(LIBJSON_FOUND "NO")
IF(LIBJSON_LIBRARY AND LIBJSON_INCLUDE_DIR)
    SET(LIBJSON_FOUND "YES")
ENDIF()
