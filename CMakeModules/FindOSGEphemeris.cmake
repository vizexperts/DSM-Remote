# Locate OSG_EPHEMERIS
# This module defines
# OSG_EPHEMERIS_LIBRARY
# OSG_EPHEMERIS_FOUND, if false, do not try to link to OSG_EPHEMERIS
# OSG_EPHEMERIS_INCLUDE_DIR, where to find the headers
#
# $OSG_EPHEMERIS_DIR is an environment variable that would
# correspond to the ./configure --prefix=$OSG_EPHEMERIS_DIR
#
# Created by Sumit Pandey.

FIND_PATH(OSG_EPHEMERIS_INCLUDE_DIR osgEphemeris/DateTime.h
    
    $ENV{OSG_EPHEMERIS_DIR}/include
    NO_DEFAULT_PATH
)

MACRO(FIND_OSG_EPHEMERIS_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)
	
	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{OSG_EPHEMERIS_DIR} ${BUILD})

ENDMACRO(FIND_OSG_EPHEMERIS_LIBRARY )

IF (WIN32)
# Find release (optimized) libs
FIND_OSG_EPHEMERIS_LIBRARY(OSG_EPHEMERIS_LIBRARY 			osgEphemeris 		release)
# Find debug libs
FIND_OSG_EPHEMERIS_LIBRARY(OSG_EPHEMERIS_LIBRARY_DEBUG 		osgEphemerisd 		debug)
ELSE (WIN32)
# Find release (optimized) libs
FIND_OSG_EPHEMERIS_LIBRARY(OSG_EPHEMERIS_LIBRARY 			osgEphemeris 		release)
# Find debug libs
FIND_OSG_EPHEMERIS_LIBRARY(OSG_EPHEMERIS_LIBRARY_DEBUG 		osgEphemeris 		debug)
ENDIF (WIN32)

SET(OSG_EPHEMERIS_FOUND "NO")
IF(OSG_EPHEMERIS_LIBRARY AND OSG_EPHEMERIS_INCLUDE_DIR)
    SET(OSG_EPHEMERIS_FOUND "YES")
ENDIF(OSG_EPHEMERIS_LIBRARY AND OSG_EPHEMERIS_INCLUDE_DIR)
