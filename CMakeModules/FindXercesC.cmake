# Locate Xerces-c
# This module defines
# XERCES_LIBRARY
# XERCES_FOUND, if false, do not try to link to xerces-c
# XERCES_INCLUDE_DIR, where to find the headers
#
# $XERCES_DIR is an environment variable that would
# correspond to the ./configure --prefix=$XERCES_DIR
#
# Created by Robert Osfield.

FIND_PATH(XERCES_INCLUDE_DIR xercesc
    
    $ENV{XERCESC_DIR}/include
    NO_DEFAULT_PATH
)

MACRO(FIND_XERCES_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{XERCESC_DIR} ${BUILD})

ENDMACRO(FIND_XERCES_LIBRARY)

SET(XERCES_LIST Xerces xerces-c_3 xerces-c_3D xerces-c)
FIND_XERCES_LIBRARY(XERCES_LIBRARY 							xerces-c_3 			release)
FIND_XERCES_LIBRARY(XERCES_LIBRARY_DEBUG 					xerces-c_3D 		debug)

FIND_XERCES_LIBRARY(XERCES_LIBRARY_DEBUG xerces-c_3D 		libxerces-c.so 		debug)

SET(XERCES_FOUND "NO")
IF(XERCES_LIBRARY AND XERCES_INCLUDE_DIR)
    SET(XERCES_FOUND "YES")
ENDIF(XERCES_LIBRARY AND XERCES_INCLUDE_DIR)
