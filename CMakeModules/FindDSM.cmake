# Locate DSM
# This module defines
# DTCORE_LIBRARY
# DSM_EXT_DIR
# DSM_FOUND, if false, do not try to link to gdal
# DSM_INCLUDE_DIR, where to find the headers
#
# $DSM_DIR is an environment variable that would
# correspond to the ./configure --prefix=$DSM
#

FIND_PATH(DSM_DIR include/indiQt/AddContentGUI.h
    PATHS
    $ENV{DSM_ROOT}
    $ENV{DSM_DIR}
)

FIND_PATH(DSM_INCLUDE_DIR indiQt/AddContentGUI.h
    PATHS
    ${DSM_DIR}/include
)

FIND_FILE(DSM_LIB_DIR NAMES lib
    PATHS
    ${DSM_DIR}
    NO_DEFAULT_PATH
)

SET(SMCUI_LIBRARY SMCUI)
SET(SMCQT_LIBRARY SMCQt)
SET(STEREO_LIBRARY Stereo)
SET(SMCELEMENTS_LIBRARY SMCElements)
SET(STEREOOSGQT_LIBRARY stereoOsgQt)
SET(COMPUTESERVICES_LIBRARY ComputeServices)

SET(DSM_FOUND "NO")
IF(DSM_INCLUDE_DIR AND DSM_LIB_DIR)
    SET(DSM_FOUND "YES")
ENDIF(DSM_INCLUDE_DIR AND DSM_LIB_DIR)