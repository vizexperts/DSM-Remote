# Locate boost
# This module defines
# BOOST_THREAD_LIBRARY
# BOOST_FOUND, if false, do not try to link to gdal
# BOOST_INCLUDE_DIR, where to find the headers
#
# $BOOST_DIR is an environment variable that would
# correspond to the ./configure --prefix=$BOOST_DIR
#
# Created by Nishant

FIND_PATH(BOOST_INCLUDE_DIR boost/any.hpp
            $ENV{BOOST_DIR}/include
            NO_DEFAULT_PATH
        )

MACRO(FIND_BOOST_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB( ${MYLIBRARY} ${MYLIBRARYNAME} $ENV{BOOST_DIR} ${BUILD})
	
ENDMACRO(FIND_BOOST_LIBRARY )

# optimized
IF (WIN32)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY             boost_thread-vc120-mt-1_57          release)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY_DEBUG       boost_thread-vc120-mt-gd-1_57       debug)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY           boost_date_time-vc120-mt-1_57       release)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY_DEBUG     boost_date_time-vc120-mt-gd-1_57    debug)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY         boost_filesystem-vc120-mt-1_57      release)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY_DEBUG   boost_filesystem-vc120-mt-gd-1_57   debug)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY             boost_system-vc120-mt-1_57          release)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY_DEBUG       boost_system-vc120-mt-gd-1_57       debug)
	FIND_BOOST_LIBRARY(BOOST_REGEX_LIBRARY              boost_regex-vc120-mt-1_57           release)
    FIND_BOOST_LIBRARY(BOOST_REGEX_LIBRARY_DEBUG        boost_regex-vc120-mt-gd-1_57        debug)
ELSE (WIN32)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY             boost_thread                        release)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY_DEBUG       boost_thread                        debug)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY           boost_date_time                     release)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY_DEBUG     boost_date_time                     debug)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY         boost_filesystem                    release)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY_DEBUG   boost_filesystem                    debug)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY             boost_system                        release)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY_DEBUG       boost_system                        debug)
    FIND_BOOST_LIBRARY(BOOST_TIMER_LIBRARY              boost_timer                         release)
    FIND_BOOST_LIBRARY(BOOST_TIMER_LIBRARY_DEBUG        boost_timer                         debug)
	FIND_BOOST_LIBRARY(BOOST_REGEX_LIBRARY              boost_regex                         release)
    FIND_BOOST_LIBRARY(BOOST_REGEX_LIBRARY_DEBUG        boost_regex                         debug)
ENDIF (WIN32)

SET(BOOST_FOUND "NO")
IF(BOOST_LIBRARY AND BOOST_INCLUDE_DIR)
    SET(BOOST_FOUND "YES")
ENDIF(BOOST_LIBRARY AND BOOST_INCLUDE_DIR)
