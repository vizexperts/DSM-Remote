# Locate PQLABS
# This module defines
# PQLABS_LIBRARY
# PQLABS_FOUND, if false, do not try to link to PQLABS
# PQLABS_INCLUDE_DIR, where to find the headers
#
# $PQLABS_DIR is an environment variable that would
# correspond to the ./configure --prefix=$PQLABS_DIR
#
# Created by Robert Osfield.

FIND_PATH(PQLabs_INCLUDE_DIR PQMTClient.h
    
    $ENV{PQLABS_DIR}/include
    NO_DEFAULT_PATH
)

MACRO(FIND_PQLABS_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{PQLABS_DIR} ${BUILD})

ENDMACRO(FIND_PQLABS_LIBRARY )

# Find release (optimized) libs
FIND_PQLABS_LIBRARY(PQLABS_LIBRARY 		PQMTClient 		release)

SET(PQLABS_FOUND "NO")
IF(PQLABS_LIBRARY AND PQLABS_INCLUDE_DIR)
    SET(PQLABS_FOUND "YES")
ENDIF(PQLABS_LIBRARY AND PQLABS_INCLUDE_DIR)
