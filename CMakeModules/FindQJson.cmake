# Locate QJSON
# This module defines
# QJSON_LIBRARY
# QJSON_FOUND, if false, do not try to link to QJSON
# QJSON_INCLUDE_DIR, where to find the headers
#
# $QJSON_DIR is an environment variable that would
# correspond to the ./configure --prefix=$QJSON_DIR
#
# Created by Robert Osfield.

FIND_PATH(QJSON_INCLUDE_DIR qjson_export.h
    
    $ENV{QJSON_DIR}/include    
    NO_DEFAULT_PATH
)

MACRO(FIND_QJSON_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{QJSON_DIR} ${BUILD})

ENDMACRO(FIND_QJSON_LIBRARY )

# Find release (optimized) libs
FIND_QJSON_LIBRARY(QJSON_LIBRARY 				qjson 		release)
# Find debug libs
FIND_QJSON_LIBRARY(QJSON_LIBRARY_DEBUG 			qjson 		debug)

SET(QJSON_FOUND "NO")
IF(QJSON_LIBRARY AND QJSON_INCLUDE_DIR)
    SET(QJSON_FOUND "YES")
ENDIF(QJSON_LIBRARY AND QJSON_INCLUDE_DIR)
