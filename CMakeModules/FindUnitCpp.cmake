# Locate CPP_UNIT
# This module defines
# UNIT_CPP_LIBRARY
# UNIT_CPP_FOUND, if false, do not try to link to gdal
# UNIT_CPP_INCLUDE_DIR, where to find the headers
#
# $UNIT_CPP_DIR is an environment variable that would
# correspond to the ./configure --prefix=$UNIT_CPP_DIR
#
# Created by Nishant

FIND_PATH(UNIT_CPP_INCLUDE_DIR cppunit/TestFixture.h
    
        $ENV{UNIT_CPP_DIR}/include
        NO_DEFAULT_PATH
)

MACRO(FIND_UNIT_CPP_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{UNIT_CPP_DIR} ${BUILD})

ENDMACRO(FIND_UNIT_CPP_LIBRARY )

# optimized
FIND_UNIT_CPP_LIBRARY(UNIT_CPP_LIBRARY              cppunit         release)
FIND_UNIT_CPP_LIBRARY(UNIT_CPP_LIBRARY_DEBUG        cppunit         debug)

SET(UNIT_CPP_FOUND "NO")
IF(UNIT_CPP_LIBRARY AND UNIT_CPP_INCLUDE_DIR)
    SET(UNIT_CPP_FOUND "YES")
ENDIF(UNIT_CPP_LIBRARY AND UNIT_CPP_INCLUDE_DIR)