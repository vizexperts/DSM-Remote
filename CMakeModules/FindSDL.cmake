# Locate SDL
# This module defines
# SDL_LIBRARY
# SDL_FOUND, if false, do not try to link to CURL
# SDL_INCLUDE_DIR, where to find the headers
#
# $SDL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$SDL_DIR

FIND_PATH(SDL_INCLUDE_DIR SDL.h

    $ENV{SDL_DIR}/include
    NO_DEFAULT_PATH
)

MACRO(FIND_SDL_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{SDL_DIR} ${BUILD})

ENDMACRO(FIND_SDL_LIBRARY )

# Find release (optimized) libs
FIND_SDL_LIBRARY(SDL_LIBRARY            SDL2        release)
# Find debug libs
FIND_SDL_LIBRARY(SDL_LIBRARY_DEBUG      SDL2        debug)

SET(SDL_FOUND "NO")
IF(SDL_LIBRARY AND SDL_INCLUDE_DIR)
    SET(SDL_FOUND "YES")
ENDIF(SDL_LIBRARY AND SDL_INCLUDE_DIR)
