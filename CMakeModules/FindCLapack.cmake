# Locate clapack
# This module defines
# CLAPACK_LIBRARY
# CLAPACK_FOUND, if false, do not try to link to gdal
# CLAPACK_INCLUDE_DIR, where to find the headers
#
# $CLAPACK_DIR is an environment variable that would
# correspond to the ./configure --prefix=$CLAPACK_DIR
#
# Created by Kumar Saurabh

FIND_PATH(CLAPACK_INCLUDE_DIR clapack.h
            $ENV{CLAPACK_DIR}/include
            NO_DEFAULT_PATH
            )

MACRO(FIND_CLAPACK_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{CLAPACK_DIR} ${BUILD})
	
ENDMACRO(FIND_CLAPACK_LIBRARY )

# Find release (optimized) libs
FIND_CLAPACK_LIBRARY(CLAPACK_LIBRARY                lapack                  release)
FIND_CLAPACK_LIBRARY(CLAPACK_BLAS_LIBRARY           blas                    release)
IF (WIN32)
    FIND_CLAPACK_LIBRARY(CLAPACK_F2C_LIBRARY        libf2c                  release)
ELSE (WIN32)
    FIND_CLAPACK_LIBRARY(CLAPACK_F2C_LIBRARY        f2c                     release)
ENDIF (WIN32)


# Find debug libsind	
FIND_CLAPACK_LIBRARY(CLAPACK_LIBRARY_DEBUG          lapackd                 debug)
FIND_CLAPACK_LIBRARY(CLAPACK_F2C_LIBRARY_DEBUG      libf2cd                 debug)
FIND_CLAPACK_LIBRARY(CLAPACK_BLAS_LIBRARY_DEBUG     blasd                   debug)


SET(CLAPACK_FOUND "NO")
IF(CLAPACK_LIBRARY AND CLAPACK_INCLUDE_DIR)
    SET(CLAPACK_FOUND "YES")
ENDIF(CLAPACK_LIBRARY AND CLAPACK_INCLUDE_DIR)