# - try to find POPPLER
# Once done this will define
#
#  POPPLER_FOUND - system has POPPLER
#  POPPLER_CFLAGS - the POPPLER CFlags
#  POPPLER_INCLUDE_DIR - the POPPLER include directories
#  POPPLER_LIBRARY - Link these to use POPPLER
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

FIND_PATH(POPPLER_INCLUDE_DIR poppler-qt5.h
    
    $ENV{POPPLER_DIR}/include/poppler/qt5
	NO_DEFAULT_PATH
)

MACRO(FIND_POPPLER_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        ${POPPLER_DIR}/lib/win/${COMPILER}/${OSSARCH}/${OSSBUILD}
        $ENV{POPPLER_DIR}/lib/win/$ENV{COMPILER}/$ENV{OSSARCH}/$ENV{OSSBUILD}
    )
	
ENDMACRO(FIND_POPPLER_LIBRARY )

# Find release (optimized) libs
FIND_POPPLER_LIBRARY(POPPLER_LIBRARY poppler-qt5)

SET(POPPLER_FOUND "NO")
IF(POPPLER_LIBRARY AND POPPLER_INCLUDE_DIR)
    SET(POPPLER_FOUND "YES")
ENDIF(POPPLER_LIBRARY AND POPPLER_INCLUDE_DIR)
