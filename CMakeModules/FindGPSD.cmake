# Locate gdal
# This module defines
# GPSD_LIBRARY
# GPSD_FOUND, if false, do not try to link to gdal
# GPSD_INCLUDE_DIR, where to find the headers
#
# $GPSD_DIR is an environment variable that would
# correspond to the ./configure --prefix=$GPSD_DIR
#
# Created by Robert Osfield.

FIND_PATH(GPSD_INCLUDE_DIR viz_types.h
        
        $ENV{GPSD_DIR}/include
        NO_DEFAULT_PATH            
        
        )

MACRO(FIND_GPSD_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{GPSD_DIR} ${BUILD})

    IF( NOT ${MYLIBRARY}_DEBUG)
        IF(MYLIBRARY)
            SET(${MYLIBRARY}_DEBUG ${MYLIBRARY})
        ENDIF(MYLIBRARY)
    ENDIF( NOT ${MYLIBRARY}_DEBUG)

ENDMACRO(FIND_GPSD_LIBRARY )

# optimized
#FIND_GPSD_LIBRARY(GPSD_LIBRARY             libgps      release)
#FIND_GPSD_LIBRARY(GPSD_LIBRARY_DEBUG       libgps      debug)
SET(GPSD_FOUND "NO")                
IF(GPSD_LIBRARY AND GPSD_INCLUDE_DIR)
    SET(GPSD_FOUND "YES")
ENDIF(GPSD_LIBRARY AND GPSD_INCLUDE_DIR)
