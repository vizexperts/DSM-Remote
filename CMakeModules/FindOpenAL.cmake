# Locate OpenAL
# This module defines
# OpenAL_THREAD_LIBRARY
# OpenAL_FOUND, if false, do not try to link to gdal
# OpenAL_INCLUDE_DIR, where to find the headers
#
# $OpenAL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$OpenAL_DIR
#
# Created by Nishant

FIND_PATH(OpenAL_INCLUDE_DIR al.h
        
        $ENV{OpenAL_DIR}/include
        NO_DEFAULT_PATH
        )

MACRO(FIND_OpenAL_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{OpenAL_DIR} ${BUILD})

ENDMACRO(FIND_OpenAL_LIBRARY )

# optimized
IF (WIN32)
    FIND_OpenAL_LIBRARY(OpenAL_LIBRARY                  OpenAL32        release)
	FIND_OpenAL_LIBRARY(OpenAL_LIBRARY_DEBUG            OpenAL32        debug)
ELSE (WIN32)
ENDIF (WIN32)

SET(OpenAL_FOUND "NO")
IF(OpenAL_LIBRARY AND OpenAL_INCLUDE_DIR)
    SET(OpenAL_FOUND "YES")
ELSE(OpenAL_LIBRARY AND OpenAL_INCLUDE_DIR)
     MESSAGE("OpenAL Not Found")
ENDIF(OpenAL_LIBRARY AND OpenAL_INCLUDE_DIR)
