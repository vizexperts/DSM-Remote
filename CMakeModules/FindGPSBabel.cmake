# Locate gdal
# This module defines
# GPSBABEL_LIBRARY
# GPSBABEL_FOUND, if false, do not try to link to gdal
# GPSBABEL_INCLUDE_DIR, where to find the headers
#
# $GPSBABEL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$GPSBABEL_DIR
#
# Created by Robert Osfield.

FIND_PATH(GPSBABEL_INCLUDE_DIR defs.h
            
            $ENV{GPSBABEL_DIR}/include
            NO_DEFAULT_PATH
        )


MACRO(FIND_GPSBABEL_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{GPSBABEL_DIR} ${BUILD})
    
ENDMACRO(FIND_GPSBABEL_LIBRARY )

# optimized
FIND_GPSBABEL_LIBRARY(GPSBABEL_LIBRARY          GPSBabel    release)
FIND_GPSBABEL_LIBRARY(GPSBABEL_LIBRARY_DEBUG    GPSBabel    debug)

SET(GPSBABEL_FOUND "NO")
IF(GPSBABEL_LIBRARY AND GPSBABEL_INCLUDE_DIR)
    SET(GPSBABEL_FOUND "YES")
ENDIF(GPSBABEL_LIBRARY AND GPSBABEL_INCLUDE_DIR)
