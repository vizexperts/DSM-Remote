# Locate POSTGRESQL
# This module defines
# POSTGRESQL_LIBRARY
# POSTGRESQL_FOUND, if false, do not try to link to POSTGRESQL
# POSTGRESQL_INCLUDE_DIR, where to find the headers
#
# $POSTGRESQL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$POSTGRESQL_DIR
#
# Created by Robert Osfield.

FIND_PATH(POSTGRESQL_INCLUDE_DIR libpq-fe.h
    
    $ENV{POSTGRESQL_DIR}/include
    NO_DEFAULT_PATH
)

MACRO(FIND_POSTGRESQL_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{POSTGRESQL_DIR} ${BUILD})

ENDMACRO(FIND_POSTGRESQL_LIBRARY )

IF (WIN32)
# Find release (optimized) libs
FIND_POSTGRESQL_LIBRARY(POSTGRESQL_LIBRARY          libpq       release)
#FIND_POSTGRESQL_LIBRARY(ICHARSETBCC_LIBRARY        libcharset-bcc)
#FIND_POSTGRESQL_LIBRARY(ICHARSET_LIBRARY           libcharset)
#FIND_POSTGRESQL_LIBRARY(POSTGRESQLBCC_LIBRARY      libiconv-bcc)
ELSE (WIN32)
FIND_POSTGRESQL_LIBRARY(POSTGRESQL_LIBRARY          pq          release)
ENDIF (WIN32)

SET(POSTGRESQL_FOUND "NO")
IF(POSTGRESQL_LIBRARY AND POSTGRESQL_INCLUDE_DIR)
    SET(POSTGRESQL_FOUND "YES")
else()
    message(STATUS "POSTGRES NOT FOUND!!!!!!")
ENDIF(POSTGRESQL_LIBRARY AND POSTGRESQL_INCLUDE_DIR)
