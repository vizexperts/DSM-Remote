# Locate boost
# This module defines
# GTEST_LIBRARY
# GTEST_FOUND, if false
# GTEST_INCLUDE_DIR, where to find the headers
#
# $GTEST_DIR is an environment variable that would
# correspond to the ./configure --prefix=$GTEST_DIR
#
# Created by Nishant

FIND_PATH(GTEST_INCLUDE_DIR gtest/gtest.h
            
            $ENV{GTEST_DIR}/include
            NO_DEFAULT_PATH
            )

MACRO(FIND_GTEST_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{GTEST_DIR} ${BUILD})

ENDMACRO(FIND_GTEST_LIBRARY )

# optimized
IF (WIN32)
    FIND_GTEST_LIBRARY(GTEST_LIBRARY            gtest       release)
    FIND_GTEST_LIBRARY(GTEST_LIBRARY_DEBUG      gtest       debug)
ELSE (WIN32)
    FIND_GTEST_LIBRARY(GTEST_LIBRARY            gtest_dll   release)
    FIND_GTEST_LIBRARY(GTEST_LIBRARY_DEBUG      gtest_dlld  debug)
ENDIF (WIN32)

SET(GTEST_FOUND "NO")
IF(GTEST_LIBRARY AND GTEST_INCLUDE_DIR)
    SET(GTEST_FOUND "YES")
ENDIF(GTEST_LIBRARY AND GTEST_INCLUDE_DIR)
