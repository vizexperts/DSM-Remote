# Locate CURL
# This module defines
# CURL_LIBRARY
# CURL_FOUND, if false, do not try to link to CURL
# CURL_INCLUDE_DIR, where to find the headers
#
# $CURL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$CURL_DIR
#
# Created by Robert Osfield.
FIND_PATH(CURL_INCLUDE_DIR curl/curl.h

	$ENV{CURL_DIR}/include
    NO_DEFAULT_PATH
)

MACRO(FIND_CURL_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

	FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{CURL_DIR} ${BUILD})

ENDMACRO(FIND_CURL_LIBRARY)

IF (WIN32)

	# Find release (optimized) libs
	FIND_CURL_LIBRARY(CURL_LIBRARY 			libcurl_imp 			release)
	# Find debug libs
	FIND_CURL_LIBRARY(CURL_LIBRARY_DEBUG 	libcurl_imp 			debug)
	
ELSE ()

	# Find release (optimized) libs
	FIND_CURL_LIBRARY(CURL_LIBRARY 			curl 					release)
	# Find debug libs
	FIND_CURL_LIBRARY(CURL_LIBRARY_DEBUG 	curl 					debug)

ENDIF ()

SET(CURL_FOUND "NO")
IF(CURL_LIBRARY AND CURL_INCLUDE_DIR)
	SET(CURL_FOUND "YES")
ENDIF(CURL_LIBRARY AND CURL_INCLUDE_DIR)
