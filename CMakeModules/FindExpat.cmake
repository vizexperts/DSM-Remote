# Locate gdal
# This module defines
# EXPAT_LIBRARY
# EXPAT_FOUND, if false, do not try to link to gdal
# EXPAT_INCLUDE_DIR, where to find the headers
#
# $EXPAT_DIR is an environment variable that would
# correspond to the ./configure --prefix=$EXPAT_DIR
#
# Created by Robert Osfield.

FIND_PATH(EXPAT_INCLUDE_DIR expat_config.h
            $ENV{EXPAT_DIR}/include
            NO_DEFAULT_PATH
			)
			
MACRO(FIND_EXPAT_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{EXPAT_DIR} ${BUILD})
	
ENDMACRO(FIND_EXPAT_LIBRARY )

# optimized
IF (WIN32)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY                expat       release)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY_DEBUG          expat       debug)
ELSE (WIN32)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY                expat       release)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY_DEBUG          expat       debug)
ENDIF (WIN32)

SET(EXPAT_FOUND "NO")
IF(EXPAT_LIBRARY AND EXPAT_INCLUDE_DIR)
    SET(EXPAT_FOUND "YES")
ENDIF(EXPAT_LIBRARY AND EXPAT_INCLUDE_DIR)
