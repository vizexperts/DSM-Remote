# Locate ng6 RTI
# This module defines
# RTI_LIBRARY
# RTI_FOUND, if false, do not try to link to OSG
# RTI_INCLUDE_DIR, where to find the headers

# Locate OSG
# This module defines
# RTI_LIBRARY
# RTI_FOUND, if false, do not try to link to OSG
# RTI_INCLUDE_DIR, where to find the headers
#
# $RTI_DIR is an environment variable that would
# correspond to the ./configure --prefix=$RTI_DIR
#
# Created by Robert Osfield.

#Definition required in portico c++ lib
ADD_DEFINITIONS(-DRTI_USES_STD_FSTREAM)

FIND_PATH(RTI_INCLUDE_DIR RTI.hh	    
    
    $ENV{RTI_DIR}/include/hla13
    NO_DEFAULT_PATH	
)

MACRO(FIND_RTI_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

   FIND_LIB(${MYLIBRARY} ${MYLIBRARYNAME} $ENV{RTI_DIR} ${BUILD})

ENDMACRO(FIND_RTI_LIBRARY )

IF(WIN32)
    FIND_RTI_LIBRARY(RTI_LIBRARY 					libRTI-NG64 	release)
	FIND_RTI_LIBRARY(RTI_FEDTIME_LIBRARY 			libFedTime64 	release)

	FIND_RTI_LIBRARY(RTI_LIBRARY_DEBUG 				libRTI-NG64d 	debug)
	FIND_RTI_LIBRARY(RTI_FEDTIME_LIBRARY_DEBUG 		libFedTime64d 	debug)
ELSE(WIN32)
	FIND_RTI_LIBRARY(RTI_LIBRARY 					RTI-NG64 		release)
	FIND_RTI_LIBRARY(RTI_FEDTIME_LIBRARY 			FedTime64  		release)
	
	FIND_RTI_LIBRARY(RTI_LIBRARY_DEBUG 				RTI-NG64d 		debug)
	FIND_RTI_LIBRARY(RTI_FEDTIME_LIBRARY_DEBUG 		FedTime64d 		debug)
ENDIF(WIN32)

SET(RTI_FOUND "NO")
# AND 
IF(${RTI_INCLUDE_DIR})    
    SET(RTI_FOUND "YES")
ENDIF(${RTI_INCLUDE_DIR})
#AND RTI_INCLUDE_DIR

