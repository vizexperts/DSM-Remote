:: arg 1 is whether OSS svn checkout is needed
:: arg 2 is complete OSS directory path to be stored on  local system
:: arg 3 is nobuild
:: arg 4 is release or debug option
:: arg 5 is package only or nopackage

:: set OSSROOT Here
set OSSROOT=G:\root\trunk
	
:: set COMPILER Here
set COMPILER=vc12

:: set development mode
set Development=true

:: Set your /path/to/terrain kit sdk
set TERRAINSDK_DIR=G:\TKP_SDK
set TERRAINSDK_DATA_DIR=G:\DSM_Data\trunk

:: Set OSVR Directory
set OSVR_DIR=C:\Program Files\OSVR\SDK

:: Store current directory
FOR /F "tokens=1" %%i in ('cd') do set CURRENT_DIR=%%i

if "%1" NEQ "checkoutOSS" goto build

if "%2" EQU "" exit /B

set EARLIER_PATH=%PATH%
set PATH=%CURRENT_DIR%\tools\svn_commandLine;%PATH%

mkdir %2

cd /D %2

call %CURRENT_DIR%\svnCheckout.bat

set PATH=%EARLIER_PATH%

cd /D %CURRENT_DIR%

set OSSROOT=%2

:build
copy /Y tools\command_line_silent\*.* x64\bin

:: if OSS was checkedout then override oss root
if "%1" EQU "checkoutOSS" set OSSROOT=%2\consulting\OSS

:: Portico directory
set PORTICO_DIR=%OSSROOT%\portico\2.0.1

::Set PostgreSQL directory
set POSTGRESQL_DIR=%OSSROOT%\PostgreSQL\9.1

set OSSARCH=x64

if "%1" EQU "debug" set DBG=1
if "%2" EQU "debug" set DBG=1
if "%4" EQU "debug" set DBG=1

:: Set debug\release option
set OSSBUILD=release

if DEFINED DBG (set OSSBUILD=debug)

:: Call common env file
call DSM_env_Common

:: Call Advanced Config file
call DSM_Advanced_Config

IF exist x64\DSM.sln goto build_x64
call gen_cmake_from_scratch.bat

: build_x64

if "%1" EQU "nobuild" goto package
if "%3" EQU "nobuild" goto package

set OSARCH64=AMD64

set BUILD_CFG=Release

if DEFINED DBG (set BUILD_CFG=Debug)

call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x64
echo "Starting Build for all Projects with proposed changes"
echo .  
devenv "x64\DSM.sln" /build %BUILD_CFG% 
echo . 
echo "All builds completed." 

:package

if "%1" EQU "nopackage" goto end
if "%2" EQU "nopackage" goto end
if "%3" EQU "nopackage" goto end
if "%5" EQU "nopackage" goto end

cd %CURRENT_DIR%\package/Windows
call GeorbISSetup.bat


:end
rem all went well!
cd "%CURRENT_DIR%"