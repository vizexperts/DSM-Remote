:: arg 1 is whether cvs of cvs co consulting/OSS is needed
:: arg 2 is cvs username if cvspass file is saved else username:password if different
:: arg 3 is complete cvs co consulting/OSS directory path
:: arg 4 is nobuild
:: arg 5 is release or debug option
:: arg 6 is x64 or win32
:: arg 7 is package only or nopackage
:: arg 8 whether to create installer for TKP or DSM

:: set OSSROOT Here
set OSSROOT=C:\OSS_DSM_GGN\consulting\OSS

:: Store current directory
FOR /F "tokens=1" %%i in ('cd') do set CURRENT_DIR=%%i

if "%1" NEQ "checkoutOSS" goto build

if "%2" EQU "" exit /B

if "%3" EQU "" exit /B

:: USE YOUR USERNAME HERE
set CVSROOT=:pserver:%2@192.168.0.4/cvsroot/repository

set EARLIER_PATH=%PATH%
set PATH=%CURRENT_DIR%\tools\cvs_commandLine;%PATH%

cd /D %3

call %CURRENT_DIR%\cvsCheckout.bat

set PATH=%EARLIER_PATH%

cd /D %CURRENT_DIR%

:build

:: if OSS was checkedout then override oss root
if "%1" EQU "checkoutOSS" set OSSROOT=%3\consulting\OSS

if "%1" EQU "win32" goto win32
if "%2" EQU "win32" goto win32
if "%3" EQU "win32" goto win32
if "%6" EQU "win32" goto win32

rem build 32 and 64 bit versions
:x64

:: Portico directory
set PORTICO_DIR=C:\Program Files\Portico\portico-2.0.0

::Set PostgreSQL directory
set POSTGRESQL_DIR=C:\Program Files\PostgreSQL\9.1

set OSSARCH=x64

:: Set your /path/to/terrain kit sdk
set TERRAINSDK_DIR=C:\Teamcity\TKSDK_GGN
set TERRAINSDK_INCLUDE_DIR=%TERRAINSDK_DIR%\include
set TERRAINSDK_LIB_DIR=%TERRAINSDK_DIR%\%OSSARCH%\lib
set TERRAINSDK_BIN=%TERRAINSDK_DIR%\%OSSARCH%\bin;%TERRAINSDK_DIR%\%OSSARCH%\bin\vizDBPlugins;

if "%1" EQU "debug" set DBG=1
if "%2" EQU "debug" set DBG=1
if "%5" EQU "debug" set DBG=1

:: Set debug\release option
set OSSBUILD=release

if DEFINED DBG (set OSSBUILD=debug)

:: Call common env file
call DSM_env_Common

IF exist x64\DSM.sln goto build_x64
call gen_cmake_from_scratch.bat

: build_x64

if "%1" EQU "nobuild" goto package
if "%4" EQU "nobuild" goto package

set OSARCH64=AMD64

set VCBUILD_DEFAULT_CFG=Release

if DEFINED DBG (set VCBUILD_DEFAULT_CFG=Debug)

rem call vsvars so vcbuild is in the path
call "%VS90COMNTOOLS%vsvars32.bat"

cd %CURRENT_DIR%\x64
msbuild DSM.sln /t:Rebuild /p:Configuration=%VCBUILD_DEFAULT_CFG% /p:Platform=x64 /M:%NUMBER_OF_PROCESSORS%
if errorlevel 1 exit /B %ERRORLEVEL%

goto package

:win32
set EARLIER_PATH=%PATH%

:: Portico directory
set PORTICO_DIR=C:\Program Files (x86)\Portico\portico-1.0.2

::Set PostgreSQL directory
set POSTGRESQL_DIR=C:\Program Files (x86)\PostgreSQL\9.1

set OSSARCH=win32

:: Set your /path/to/terrain kit sdk
set TERRAINSDK_DIR=C:\BuildAgent\work\TKP_SDK_Qt5
set TERRAINSDK_INCLUDE_DIR=%TERRAINSDK_DIR%\include
set TERRAINSDK_LIB_DIR=%TERRAINSDK_DIR%\%OSSARCH%\lib
set TERRAINSDK_BIN=%TERRAINSDK_DIR%\%OSSARCH%\bin;%TERRAINSDK_DIR%\%OSSARCH%\bin\vizDBPlugins;

if "%1" EQU "debug" set DBG=1
if "%2" EQU "debug" set DBG=1
if "%5" EQU "debug" set DBG=1

:: Set debug\release option
set OSSBUILD=release

if DEFINED DBG (set OSSBUILD=debug)

:: Call common env file
call DSM_env_Common

IF exist win32\DSM.sln goto build_win32
call gen_cmake_from_scratch.bat

: build_win32

if "%1" EQU "nobuild" goto x64
if "%4" EQU "nobuild" goto x64

set OSARCH64=AMD64

set VCBUILD_DEFAULT_CFG=Release

if DEFINED DBG (set VCBUILD_DEFAULT_CFG=Debug)

rem call vsvars so vcbuild is in the path
call "%VS90COMNTOOLS%vsvars32.bat"

if DEFINED DBG (set VCBUILD_DEFAULT_CFG=Debug)
cd %CURRENT_DIR%\win32
msbuild DSM.sln /t:Rebuild /p:Configuration=%VCBUILD_DEFAULT_CFG% /p:Platform=Win32 /M:%NUMBER_OF_PROCESSORS%
if errorlevel 1 exit /B %ERRORLEVEL%

:package

if "%1" EQU "nopackage" goto end
if "%2" EQU "nopackage" goto end
if "%3" EQU "nopackage" goto end
if "%4" EQU "nopackage" goto end
if "%7" EQU "nopackage" goto end

set INSTALLER_TO_CREATE="DSM"
if "%1" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%1" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%1" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

if "%2" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%2" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%2" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

if "%3" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%3" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%3" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

if "%4" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%4" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%4" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

if "%5" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%5" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%5" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

if "%6" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%6" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%6" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

if "%7" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%7" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%7" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

if "%8" EQU "Imagyst" set INSTALLER_TO_CREATE=Imagyst
if "%8" EQU "GeorbIS-Pro" set INSTALLER_TO_CREATE=GeorbIS-Pro
if "%8" EQU "GeorbIS-Vu" set INSTALLER_TO_CREATE=GeorbIS-Vu

cd %CURRENT_DIR%\package/Windows
if "%INSTALLER_TO_CREATE%"=="Imagyst" (
	call ImageystSetup.bat
	goto end
	)
if "%INSTALLER_TO_CREATE%"=="GeorbIS-Pro" (
	call GeorbISProSetup.bat
	goto end
	)
if "%INSTALLER_TO_CREATE%"=="GeorbIS-Vu" (
	call GeorbISVuSetup.bat
	) ELSE (
	call DSMSetup.bat
)

:end
rem all went well!
REM cd "%CURRENT_DIR%"

REM if "%INSTALLER_TO_CREATE%"=="GeorbIS-Pro" (
REM copy /Y Package\Windows\GeorbIS-Pro_Installer.exe C:\GeorbIS-Pro\%BUILD_NUMBER%
REM goto finish
REM )
REM if "%INSTALLER_TO_CREATE%"=="GeorbIS-Vu" (
REM copy /Y Package\Windows\GeorbIS-Vu_Installer.exe C:\GeorbIS-Vu\%BUILD_NUMBER%
REM goto finish
REM )
REM if "%INSTALLER_TO_CREATE%"=="Imagyst" (
REM copy /Y Package\Windows\Imagyst_Installer.exe C:\Imagyst\%BUILD_NUMBER%
REM goto finish
REM ) ELSE (
REM copy /Y Package\Windows\DSM_Installer.exe C:\DSM\%BUILD_NUMBER%
REM )

:finish