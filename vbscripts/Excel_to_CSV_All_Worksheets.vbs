rem  XLS_To_CSV.vbs
rem =============================================================
rem  convert all NON-empty worksheets in an Excel file to csv
rem  CSV file names will default to Sheet names
rem  output folder defaults to the folder where the script resides or
rem  if path is specified with the input file, that path is used
rem  
rem  input parameter 1:  Excel path\file in argument 1 
rem                     (if path is not specified, the current path is defaulted)
rem  
rem ============================================================

Dim strExcelFileName
Dim strCSVFileName

strExcelFileName = WScript.Arguments.Item(0)

Set fso = CreateObject ("Scripting.FileSystemObject")

strScript 	  = Wscript.ScriptFullName
strScriptPath = fso.GetAbsolutePathName(strScript & "\..")

Set objXL = CreateObject("Excel.Application")
Set objWorkBook = objXL.Workbooks.Open(strExcelFileName)
objXL.DisplayAlerts = False

rem loop over worksheets
currNumber=1
For Each sheet In objWorkBook.Sheets
 if objXL.Application.WorksheetFunction.CountA(sheet.Cells) <> 0 Then
	sheet.SaveAs WScript.Arguments.Item(1) & "sheet" & currNumber & ".csv", 6
	currNumber=currNumber+1
End If
Next

objWorkBook.Close
objXL.quit
Set objXL = Nothing
Set objWorkBook = Nothing
Set fso = Nothing