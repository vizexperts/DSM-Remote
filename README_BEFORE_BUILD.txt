1. Build instructions for Windows
---------------------------------
    1.1 Setting up the machine
    --------------------------
	Install the mentioned softwares from the web or local software store
        1.1.1 Install Visual Studio 2013 compiler (64-bit)
        
		1.1.2 Install PostgreSQL 9.1 (64-bit) version from the following link:
              http://www.enterprisedb.com/products-services-training/pgdownload#windows
			  Keep the password as asdf123 and leave other things as default.
			  setup the POSTGRESQL_DIR environment variable to correct path in the winbuild.bat file
			  example : set POSTGRESQL_DIR=C:\Program Files\PostgreSQL\9.1
        
		1.1.3 Install cmake from the following link, during installation choose
              to set cmake directory in your PATH.
              http://www.cmake.org/cmake/resources/software.html
		
		1.1.4 The following OpenSource Packages must be checked out from the OSS store under OSS directory
			  
			OSS\ALUT\freealut-1.1.0
			OSS\Boost\1.44.0
			OSS\CLapack\3.2.1
			OSS\curl\7.23.1
			OSS\dgnlib
			OSS\ecw\4.2
			OSS\expat\2.0.1
			OSS\ffmpeg\N-40301
			OSS\FMOD\4.44.47
			OSS\freetype\2.3.5
			OSS\gdal\1.10.0(CustomChanges)_HDFsupport
			OSS\geos\3.3.8
			OSS\GeoTrans\2.4.2
			OSS\giflib\4.1.4
			OSS\gmock\gmock-1.6.0
			OSS\GPSBabel\1.3.6
			OSS\gtest\1.7.0
			OSS\hdf\HDF5
			OSS\iconv\1.9.2
			OSS\jpeg
			OSS\libpq\9.0
			OSS\openAL\1.1
			OSS\opencv\openCV2.3.1
			OSS\OpenVRML\0.18.6
			OSS\OSG\3.2.0
			OSS\osgEarth\osgEarth2.5Trunk_OSG_3.2.0
			OSS\OSGEphemeris\OSG_320
			OSS\png
			OSS\poppler\poppler-0.24.1
			OSS\PQLabs\PQClient
			OSS\Proj\4.7
			OSS\QJson
			OSS\Qt\5.1.2
			OSS\qwt\5.2
			OSS\SDL
			OSS\spatialite\4.1
			OSS\sqlite\3.8.2
			OSS\taudem
			OSS\tiff
			OSS\TTB
			OSS\TTB\tbb42_20131003oss
			OSS\xercesc\3.1.1
			OSS\zlib
			
			Setup the OSSROOT environment variable to correct path in the winbuild.bat file if you dont want winbuild to checkout OSS for you
			for example: set OSSROOT=F:\Tree\consulting\OSS
			
		1.1.5 Make sure the SDK has be checked out and already compiled in debug and release mode for the respective platform. Set the path to SDK
			  for example: set TERRAINSDK_DIR=F:\Tree-svn\Products\TKP_SDK
			  
    1.2 Build
    ---------
		- cd to the root directory of the source tree.
		
		- Setup the OSSROOT environment variable to correct path in the winbuild.bat file.
		
        - run the following command from CMD:
		
              winbuild.bat [option1] [option2] [option3] ....
			  
		      option 1 is nobuild
			  option 2 is release or debug option
			  option 3 is package or nopackage

			  All the options need to be mentioned in above order only
			  
			  
        - when build complete, the installed packages for all products will
          be under the packager/windows directory