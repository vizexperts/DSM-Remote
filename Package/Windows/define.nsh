#product name
!define NAME "$%NAME%"
#company name
!define COMPANY "$%COMPANY%"
#company URL
!define URL "$%URL%"
#product version
!define VERSION "$%VERSION%"
#installer Icon
!define INSTALL_ICON "$%INSTALL_ICON%"
#Uninstaller icon
!define UNINSTALL_ICON "$%UNINSTALL_ICON%"
#legal trademark
!define LEGAL_TRADEMARK "$%LEGAL_TRADEMARK%"
#legal copyright
!define LEGAL_COPYRIGHT "$%LEGAL_COPYRIGHT%"
#header image for the installer
!define HEADER_IMAGE "$%HEADER_IMAGE%"
#splash screen image for installer
!define SPLASH_IMAGE "$%SPLASH_IMAGE%"
#license file name
!define LICENSE_FILE "$%LICENSE_FILE%"
#define readme file
!define README_FILE "$%README_FILE%"
#define Uninstaller name
!define UNINSTALER_NAME "$%UNINSTALER_NAME%"
#define support URL 
!define SUPPORT_URL "$%SUPPORT_URL%"
#define product URL
!define PRODUCT_URL "$%PRODUCT_URL%"
#define product icon
!define PRODUCT_ICON "$%PRODUCT_ICON%"
#define the main product file from where folders or files are to be copyed
!define MAIN_PATH "$%MAIN_PATH%"