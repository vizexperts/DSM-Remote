set TKP_DIR=%CD%

::Setting GDAL
set GDAL_DATA=%TKP_DIR%\data\gdal

::Setting OpenVRML
set OPENVRML_DATADIR=%TKP_DIR%\data\OpenVRML\data
set OPENVRML_NODE_PATH=%TKP_DIR%\data\OpenVRML\node
set OPENVRML_SCRIPT_PATH=%TKP_DIR%\data\OpenVRML\script

::Setting Portico
set PORTICO_DIR=C:\Program Files (x86)\Portico\portico-1.0.2
set RTI_RID_FILE=%TKP_DIR%\data\HLA\RTI.rid
set RTI_HOME=%PORTICO_DIR%
set JAVA_HOME=%RTI_HOME%\jre

::Setting Path Variable
set PATH=%TKP_DIR%\bin\win32\;%RTI_HOME%\bin\vc9;%JAVA_HOME%\bin\server;%PATH%
set PATH=%TKP_DIR%\VideoRecording;%PATH%

set OSG_COMPUTE_NEAR_FAR_MODE=COMPUTE_NEAR_FAR_USING_PRIMITIVES
set OSG_NOTIFY_LEVEL=ALWAYS
set OSGEARTH_NOTIFY_LEVEL=ALWAYS

::This facilitates multithreaded data retrieval from web servers
set OSG_NUM_DATABASE_THREADS=8
set OSG_NUM_HTTP_DATABASE_THREADS=6

cd bin\win32
start TKProcessor.exe
exit