#include necessary files
!include MUI2.nsh
!include Sections.nsh

#include the definition file
!include define.nsh

#include custom function file
!include VizFunctions.nsh

#name of the installer
Name "${NAME}"

#variable to store start menu page id
Var StartMenuFolder

#output file name for the installer
OutFile "${NAME}_Installer.exe"

#setup requires admin rights
RequestExecutionLevel admin

#turn on XP style setup
XPStyle on

#turn on CRC check when installer is initialized
CRCCheck on

#set the install icon
!define MUI_ICON "${INSTALL_ICON}"

#set the unisntall icon
!define MUI_UNICON "${UNINSTALL_ICON}"

#show header image
!define MUI_HEADERIMAGE

#header image file
!define MUI_HEADERIMAGE_BITMAP "${HEADER_IMAGE}"

#show warning when user aborts the setup
!define MUI_ABORTWARNING

#show radio button in the license page
!define MUI_LICENSEPAGE_RADIOBUTTONS

#show readme checkbox on the finish page
!define MUI_FINISHPAGE_SHOWREADME "$INSTDIR\docs\DSMInstallationNotes.docx"

#show checkbox to run the application
!define MUI_FINISHPAGE_RUN "$INSTDIR\TKPLauncher.exe"



#some startmenu setup
#start menu registry root
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
#path for the start menu key
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\${COMPANY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "StartMenu"
#default start menu folder
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${COMPANY}"

#add version keys to the product
VIAddVersionKey "ProductName" "${NAME}"
VIAddVersionKey "Comments" "${COMMENT}"
VIAddVersionKey "CompanyName" "${COMPANY}"
VIAddVersionKey "LegalTrademarks" "${LEGAL_TRADEMARK}"
VIAddVersionKey "LegalCopyright" "${LEGAL_COPYRIGHT}"
VIAddVersionKey "FileDescription" "${NAME}"
VIAddVersionKey "FileVersion" "${VERSION}"
VIAddVersionKey "ProductVersion" "${VERSION}"
#add version no to the product
VIProductVersion "${VERSION}"

#set the compression algorithm. lzma is the fastest and has best compression ratio.
SetCompressor lzma

#set the default installation directory
InstallDir "$PROGRAMFILES\${COMPANY}\${NAME}"

#insert Welcome Page
!insertmacro MUI_PAGE_WELCOME
#insert License Page
!insertmacro MUI_PAGE_LICENSE "${LICENSE_FILE}"
#insert component selection page
!insertmacro MUI_PAGE_COMPONENTS
#insert directory selection page
!insertmacro MUI_PAGE_DIRECTORY
#insert a startmenu selection page
!insertmacro MUI_PAGE_STARTMENU "${NAME}" $StartMenuFolder
#insert installation progress page
!insertmacro MUI_PAGE_INSTFILES
#insert finish page
!insertmacro MUI_PAGE_FINISH

#insert uninstallation confirmation page
!insertmacro MUI_UNPAGE_CONFIRM
#insert uninnstallation progress page
!insertmacro MUI_UNPAGE_INSTFILES
#insert finish page
!insertmacro MUI_UNPAGE_FINISH

#language file for the setup
!insertmacro MUI_LANGUAGE "English"

Section -Prerequisites
SectionEnd

#DSM section
Section "!${NAME}" Main 
	#set the path for section
	SetOutPath $INSTDIR
	#File to install
	
	File license.rtf
	File "${INSTALL_ICON}"
    File "${UNINSTALL_ICON}"
	File TKPLauncher.exe
	#add the folders here
	File /r "${MAIN_PATH}\VideoRecording"
	File /r "${MAIN_PATH}\Additional_Installers"
	File /r "${MAIN_PATH}\bin"
	File /r "${MAIN_PATH}\data"
    File /r "${MAIN_PATH}\config"
    File /r "${MAIN_PATH}\tests"
    File /r "${MAIN_PATH}\docs"
	
	StrCmp $%OSSARCH% "win32" win32
		IfFileExists $PROGRAMFILES64\Portico\portico-2.0.0\*.* +2 0
		Exec '"$INSTDIR\Additional_Installers\portico-2.0.0-win64.exe" /S'
		Goto done
	win32:
		IfFileExists $PROGRAMFILES\Portico\portico-1.0.2\*.* +2 0
		Exec '"$INSTDIR\Additional_Installers\portico-1.0.2-win32-standalone.exe" /S'
	done:

	#write a unisntaller
	WriteUninstaller "$INSTDIR\${UNINSTALER_NAME}"
    #add "Add and Remove" Info
    !insertmacro ADD_ADDANDREMOVEINFO
    #add start menu shortcuts
    !insertmacro MUI_STARTMENU_WRITE_BEGIN "${NAME}"
        #create the start menu folder
        CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
        #add a start menu uninstall shortcut
		CreateShortCut "$SMPROGRAMS\$StartMenuFolder\TKPLauncher.lnk" "$INSTDIR\TKPLauncher.exe" "" "$INSTDIR\${INSTALL_ICON}" 0
        CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\${UNINSTALER_NAME}"
		CreateShortCut "$DESKTOP\LaunchTKP.lnk" "$INSTDIR\TKPLauncher.exe" "" "$INSTDIR\${INSTALL_ICON}" 0
    !insertmacro MUI_STARTMENU_WRITE_END
   	
    #add product registry
    !insertmacro ADD_PRODUCT_REGISTRY
	
	#add model registry
	!insertmacro ADD_FILE_EXTENSION ".ive" "ivefile"
	!insertmacro ADD_FILE_EXTENSION ".flt" "fltfile"
	!insertmacro ADD_FILE_EXTENSION ".DAE" "daefile"
	!insertmacro ADD_FILE_EXTENSION ".3DS" "3dsfile"
	!insertmacro ADD_FILE_EXTENSION ".FBX" "fbxfile"
	!insertmacro ADD_MODEL_REGISTRY ".obj" "objfile"

SectionEnd

#uninstaller section
Section "Uninstall"
	#delete the uninstaller first - it is recommended
	Delete "$INSTDIR\${UNINSTALER_NAME}"
	#delete the readme.rtf
	Delete "$INSTDIR\Readme.rtf"
	Delete "$INSTDIR\license.rtf"
	Delete "$INSTDIR\${INSTALL_ICON}"
	Delete "$INSTDIR\${UNINSTALL_ICON}"
	Delete "$INSTDIR\TKPLauncher.exe"
	
	#remove the folders
	RMDir /r "$INSTDIR\bin"
	RMDir /r "$INSTDIR\data"
    RMDir /r "$INSTDIR\config"
	RMDir /r "$INSTDIR\tests"
    RMDir /r "$INSTDIR\docs"
	RMDir /r "$INSTDIR\VideoRecording"
	RMDir /r "$INSTDIR"
    
    #remove the Startup Folder
    RMDir /r "$SMPROGRAMS\$StartMenuFolder"
	Delete  "$SMPROGRAMS\..\..\..\..\..\..\Desktop\LaunchTKP.lnk"
	!insertmacro REMOVE_ADDANDREMOVEINFO
	
	!insertmacro REMOVE_FILE_EXTENSION ".ive" "ivefile"
	!insertmacro REMOVE_FILE_EXTENSION ".flt" "fltfile"
	!insertmacro REMOVE_FILE_EXTENSION ".DAE" "daefile"
	!insertmacro REMOVE_FILE_EXTENSION ".3DS" "3dsfile"
	!insertmacro REMOVE_FILE_EXTENSION ".FBX" "fbxfile"
	!insertmacro DELETE_MODEL_REGISTRY	".obj" "objfile"
SectionEnd

#create some strings
LangString DESC_Main ${LANG_ENGLISH} "SMCreator files"

#set descrption for the sections
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT "${Main}" "$(DESC_Main)"
!insertmacro MUI_FUNCTION_DESCRIPTION_END

#function called when installer is initialized
Function .onInit
        # the plugins dir is automatically deleted when the installer exits
        InitPluginsDir
		#create a splash screen
        File /oname=$PLUGINSDIR\splash.bmp "${SPLASH_IMAGE}"
        #optional spalsh screen sound
        #File /oname=$PLUGINSDIR\splash.wav ${SPLASH_SOUND}

        advsplash::show 1000 600 400 -1 $PLUGINSDIR\splash

        Pop $0          ; $0 has '1' if the user closed the splash screen early,
                        ; '0' if everything closed normally, and '-1' if some error occurred.
						
		#delete the temprory file
		Delete "$PLUGINSDIR\splash.bmp"
		
		# set section 'Main' as selected and read-only
		IntOp $0 ${SF_SELECTED} | ${SF_RO}
		SectionSetFlags ${Main} $0
FunctionEnd