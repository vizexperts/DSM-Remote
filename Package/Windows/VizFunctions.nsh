#macro to add "Add and Remove" Register values
!macro ADD_ADDANDREMOVEINFO
    #create a Registry for the product
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall" "${NAME}" ""
    #add display name
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "DisplayName" "${NAME}"
    #add uninstaller path
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "UninstallString" "$INSTDIR\${UNINSTALER_NAME}"
    #add Installation directory
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "InstallLocation" "$INSTDIR"
    #add display icon
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "DisplayIcon" "$INSTDIR\${UNINSTALL_ICON}"
    #add publisher name
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "Publisher" "${COMPANY}"
    #add support url
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "HelpLink" "${SUPPORT_URL}"
    #add product url
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "URLInfoAbout" "${PRODUCT_URL}"
    #add product version
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "DisplayVersion" "${VERSION}"
    #specifying that product cannot be modified
    WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "NoModify" 1
    #specifying that product cannot be repaired
    WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}" "NoRepair" 1    
!macroend

#macro to remove "Add and Remove" Info
!macro REMOVE_ADDANDREMOVEINFO
    DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${NAME}"
!macroend

#macro to add product registry
!macro ADD_PRODUCT_REGISTRY
    #path of the install dir
    WriteRegStr HKLM "Software\${COMPANY}\${NAME}" "InstallDir" "$INSTDIR"
    #product version no
    WriteRegStr HKLM "Software\${COMPANY}\${NAME}" "Version" "${VERSION}"
    #company name
    WriteRegStr HKLM "Software\${COMPANY}\${NAME}" "Company" "${COMPANY}"
    #company url
    WriteRegStr HKLM "Software\${COMPANY}\${NAME}" "URL" "${URL}"
!macroend

#macro to remove product registry
!macro DELETE_PRODUCT_REGISTRY
    DeleteRegKey HKLM "Software\${COMPANY}\${NAME}"
!macroend


#macro to add  registry
!macro ADD_MODEL_REGISTRY EXT EXT_FILE
	#Preview Path for .obj model
    ReadRegStr $R0 HKCR "${EXT}" ""
	WriteRegStr HKCR "${EXT}" "${EXT_FILE}_backup" "$R0"
	WriteRegStr HKCR "${EXT}" "" "${EXT_FILE}"
	WriteRegStr HKCR "${EXT_FILE}\shell\Preview Model\command" "" '"$INSTDIR\bin\x64\osgviewer.exe" "%1"'
	
!macroend

#macro to remove product registry
!macro DELETE_MODEL_REGISTRY EXT EXT_FILE
	ReadRegStr $R0 HKCR "${EXT}" `${EXT_FILE}_backup`
	WriteRegStr HKCR "${EXT}" "" "$R0"
	DeleteRegKey HKCR `${EXT_FILE}`
	DeleteRegKey HKCR '${EXT_FILE}_backup'
!macroend

#macro to find Python
!macro FIND_PYTHON PythonVersion
    #create a variable
	!ifndef FIND_PYTHON_VAR
		!define FIND_PYTHON_VAR
		Var /GLOBAL PythonInstallDir
	!endif
    #read the path of the python install path for the version provided
    ReadRegStr $PythonInstallDir HKLM "Software\Python\PythonCore\${PythonVersion}\InstallPath" ""
!macroend

#mscro to find PyQt4
!macro FIND_PYQT4 PythonVersion
    #create a variable
	!ifndef FIND_PYQT4_VAR
		!define FIND_PYQT4_VAR
		Var /GLOBAL PyQt4InstallDir
	!endif
    #read the path of the python install path for the version provided
    ReadRegStr $PyQt4InstallDir HKLM "Software\PyQt4\Py${PythonVersion}\InstallPath" ""
!macroend

#mscro to find open office
!macro FIND_OPENOFFICE
    #create a variable
	!ifndef FIND_OPENOFFICE_VAR
		!define FIND_OPENOFFICE_VAR
		Var /GLOBAL OpenOfficeInstallDir
		Var /GLOBAL OpenOfficeUREDir
		Var /GLOBAL OpenOfficeBasisDir
		Var /GLOBAL OpenOfficeProgramDir
	!endif
    #read the path of the Openoffice.org
    ReadRegStr $OpenOfficeInstallDir HKLM "Software\OpenOffice.org\Layers\OpenOffice.org\3" "OFFICEINSTALLLOCATION"
    ReadRegStr $OpenOfficeUREDir HKLM "Software\OpenOffice.org\Layers\OpenOffice.org\3" "UREINSTALLLOCATION"
    ReadRegStr $OpenOfficeBasisDir HKLM "Software\OpenOffice.org\Layers\OpenOffice.org\3" "BASISINSTALLLOCATION"
    ReadRegStr $OpenOfficeProgramDir HKLM "Software\OpenOffice.org\UNO\InstallPath" ""
!macroend

#mscro to find portico
!macro FIND_PORTICO
    #create a variable
	!ifndef FIND_PORTICO_VAR
		!define FIND_PORTICO_VAR
		Var /GLOBAL PorticoInstallDir
		Var /GLOBAL PorticoClientBin
	!endif
    #read the path of the python install path for the version provided
    StrCpy $PorticoInstallDir "$PROGRAMFILES\Portico\portico-1.0.1"
    StrCpy $PorticoClientBin "$PROGRAMFILES\Portico\portico-1.0.1\jre\bin\client"
    #check if portico path exist
    IfFileExists $PorticoInstallDir 0 +2
            #check if portico client path exist
            IfFileExists $PorticoClientBin +3 0
            #if it doesnt exist then set both path to null string
        StrCpy $PorticoInstallDir ""
        StrCpy $PorticoClientBin ""
        
    
!macroend

#macro to add file extension
!macro ADD_EXTENSION EXT EXT_DESCRIPTION EXT_ICON EXT_COMMAND
	#add extension to the registry
	WriteRegStr HKCR "${EXT}" "" "${NAME}${EXT}"
	#
	WriteRegStr HKCR "${NAME}${EXT}" "" "${EXT_DESCRIPTION}"
	#set the icon for the extension
	WriteRegStr HKCR "${NAME}${EXT}\DefaultIcon" "" "${EXT_ICON}"
	#set the default value of context menu to open
	WriteRegStr HKCR "${NAME}${EXT}\shell" "" "Open"
	WriteRegStr HKCR "${NAME}${EXT}\shell\Open" "" "Open"
	#set the command to be executed when extension is clicked to open
	WriteRegStr HKCR "${NAME}${EXT}\shell\Open\command" "" '${EXT_COMMAND}'
!macroend

#macro to remove file extension
!macro REMOVE_EXTENSION EXT
	DeleteRegKey HKCR "${EXT}"
	DeleteRegKey HKCR "${NAME}${EXT}"
!macroend

#macro to add file extension with different key
!macro ADD_FILE_EXTENSION EXT EXT_FILE
	#add extension to the registry
	WriteRegStr HKCR "${EXT}" "" "${EXT_FILE}"
	
	#add extensionfile and set the keys to the registry
	WriteRegStr HKCR "${EXT_FILE}\shell\Preview Model\command" "" '"$INSTDIR\bin\x64\osgviewer.exe" "%1"'
!macroend

!macro REMOVE_FILE_EXTENSION EXT EXT_FILE
	DeleteRegKey HKCR "${EXT}"
	DeleteRegKey HKCR "${EXT_FILE}"
!macroend