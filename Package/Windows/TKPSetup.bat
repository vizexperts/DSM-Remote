:: Set paths and create directory
call userInput.bat TK

::product name
set NAME=TKP

::company name
set COMPANY=VizExperts

::company URL
set URL=http://www.vizexperts.com

::product version
set VERSION=%TKP_VERSION%

::installer Icon
set INSTALL_ICON=VizLogo.ico

::Uninstaller icon
set UNINSTALL_ICON=Vizuninstall.ico

::legal trademark
set LEGAL_TRADEMARK=Trademark

::legal copyright
set LEGAL_COPYRIGHT=Copyright

::header image for the installer
set HEADER_IMAGE=%TERRAINSDK_DATA_DIR%/data/resource/VizStrip.bmp

::splash screen image for installer
set SPLASH_IMAGE=%TERRAINSDK_DATA_DIR%/data/resource/TKPSplash.gif

::license file name
set LICENSE_FILE=license.rtf

::define readme file
set README_FILE=Readme.rtf

::define Uninstaller name
set UNINSTALER_NAME=Uninstall.exe

::define support URL 
set SUPPORT_URL=http://support.vizexperts.com

::define product URL
set PRODUCT_URL=http://www.vizexperts.com

::define product icon
set PRODUCT_ICON=VizLogo.ico

::execute the nsis script
makensis /V4 TKPSetup.nsi