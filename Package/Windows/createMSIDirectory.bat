@echo off

::Setting the Command Line Arguments
set APP_TYPE=%1%

::Setting Build Folder based on Application Type
set BUILDFOLDER="GeorbIS"
	
:: Remove old directory and create the MSI directory
rmdir /S %BUILDFOLDER% /Q
mkdir %BUILDFOLDER%      

::Create the directory structure
cd %BUILDFOLDER%

::Creating the bin Structure
mkdir bin
mkdir bin\%OSSARCH%
mkdir bin\%OSSARCH%\mediaservice
mkdir bin\%OSSARCH%\designer
mkdir bin\%OSSARCH%\imageformats
mkdir bin\%OSSARCH%\platforms
mkdir bin\%OSSARCH%\gdalplugins
mkdir bin\%OSSARCH%\portico
mkdir bin\%OSSARCH%\portico\bin
mkdir bin\%OSSARCH%\portico\jre
mkdir bin\%OSSARCH%\portico\lib

::Creating the data Structure
mkdir data\gdal
mkdir VideoRecording
mkdir vbscripts
mkdir config
mkdir tests
mkdir docs

::Copying QT debug
if "%OSSBUILD%"=="release" (
call ..\CopyQT.bat bin\%OSSARCH%
) 
	
::Copying QT debug
if "%OSSBUILD%"=="debug" (
call ..\CopyQT_debug.bat bin\%OSSARCH%
)
	
::Copying the SDK Dependencies
call ..\CopyTKSDK.bat bin\%OSSARCH%

::Copying OSS
call ..\CopyOSS.bat bin\%OSSARCH%

::Copying OSVR
call ..\CopyOSVR.bat bin\%OSSARCH%

::Copying Data from OSS
call ..\CopyData.bat data

::Copying the Application DLLS
robocopy /S %SMCREATOR_DIR%\%OSSARCH%\bin\%OSSBUILD%  				bin\%OSSARCH% *.dll   	  /MT:25

:: Copying other Data from the GeorbIS
robocopy /S %TERRAINSDK_DATA_DIR%\3rdParty\%OSSARCH%\imports 	    bin\%OSSARCH% 		  	  /MT:25
robocopy /S %SMCREATOR_DIR%\%OSSARCH%\bin         					bin\%OSSARCH% *.earth     /MT:25
robocopy /S %SMCREATOR_DIR%\vbscripts  			                    bin\%OSSARCH% 		  	  /MT:25
robocopy /S %SMCREATOR_DIR%\data\GoogleData    					    bin\%OSSARCH% 		  	  /MT:25
robocopy /S %SMCREATOR_DIR%\tools\command_line_silent		    	bin\%OSSARCH% 		  	  /MT:25
robocopy /S %SMCREATOR_DIR%\config 				    			    config		  		  	  /MT:25
robocopy /S %SMCREATOR_DIR%\tests									tests		  		  	  /MT:25
robocopy /S %SMCREATOR_DIR%\vbscripts					    		vbscripts	  		      /MT:25

if "%APP_TYPE%"=="GeorbIS" (
robocopy /S %TERRAINSDK_DATA_DIR%\VideoRecording 					VideoRecording            /MT:25
robocopy /S %RTI_BIN%												bin\%OSSARCH%\portico\bin /MT:25
robocopy /S %RTI_DIR%\jre											bin\%OSSARCH%\portico\jre /MT:25
robocopy /S %RTI_DIR%\lib\win\%COMPILER%\%OSSARCH%\%OSSBUILD%		bin\%OSSARCH%\portico\lib /MT:25
		
) 

:: xcopy /S /EXCLUDE:..\excludeList.txt %SMCREATOR_DIR%\data 	data
:: xcopy /S /EXCLUDE:..\excludeList.txt %TERRAINSDK_DATA_DIR%\data 	data

robocopy /S %SMCREATOR_DIR%\data 		data /MT:25 /xd CVS VectorData resource\images GoogleData gui 
robocopy /S %TERRAINSDK_DATA_DIR%\data  data /MT:25 /xd CVS VectorData resource\images GoogleData gui 

if "%APP_TYPE%"=="GeorbIS" (
copy %TERRAINSDK_DATA_DIR%\docs\GeorbISInstallationNotes.pdf  			docs
copy %TERRAINSDK_DATA_DIR%\docs\GeorbISUserGuide.pdf					docs
copy %TERRAINSDK_DATA_DIR%\docs\ServerConfiguration.pdf					docs
copy %SMCREATOR_DIR%\%OSSARCH%\bin\%OSSBUILD%\GeorbIS*.exe 				bin\%OSSARCH%
)

:: Come up one directory
cd ..
