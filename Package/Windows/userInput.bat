:: Copy project binaries and dependency files to the MSI directory
set ARG1=%1%

call createMSIDirectory.bat %ARG1% > createMSIDirectory.log 2>&1 

:: Set NSIS path
set PATH=C:\Program Files (x86)\NSIS;%PATH%

::define the main product file from where folders or files are to be copied
FOR /F "tokens=1" %%i in ('cd') do set WINPACKAGE_DIR=%%i

::Setting the Main Path based on Installer
set MAIN_PATH=%WINPACKAGE_DIR%\GeorbIS

set GeorbIS_VERSION=1.0.0.0

