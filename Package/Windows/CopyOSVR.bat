::Path where to copy all the Files
set PATH_TO_COPY="%1"

:: Copy all dependencies to the DSM directory
copy "%OSVR_DIR%\bin\osvrAnalysisPluginKit.dll" 									%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrClient.dll" 												%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrClientKit.dll" 											%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrCommon.dll" 												%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrConnection.dll" 											%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrJointClientKit.dll" 										%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrPluginHost.dll" 											%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrPluginKit.dll"											%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrRenderManager.dll" 										%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrServer.dll" 												%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrUSBSerial.dll" 											%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrUtil.dll" 												%PATH_TO_COPY%
copy "%OSVR_DIR%\bin\osvrVRPNServer.dll" 											%PATH_TO_COPY%