
::Path where to robocopy all the Files
set PATH_TO_COPY_QT="%1"

:: ICU related DLL required for QT 
copy %QT_BIN%\icudt53.dll		  										    %PATH_TO_COPY_QT%
copy %QT_BIN%\icuin53.dll		  										    %PATH_TO_COPY_QT%
copy %QT_BIN%\icuuc53.dll		  										    %PATH_TO_COPY_QT%

:: Copying all the required Qt files
copy %QT_BIN%\Qt5OpenGL.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Core.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Declarative.dll		  									%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Gui.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Quick.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Qml.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Widgets.dll		  										%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Network.dll		  										%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Svg.dll		  										    %PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5MultimediaQuick_p.dll		  							    %PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Multimedia.dll		  										%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5MultimediaWidgets.dll		  								%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Xml.dll													%PATH_TO_COPY_QT%

robocopy %QT_DIR%\plugins\designer											%PATH_TO_COPY_QT%\designer *.dll     /MT:25

robocopy %QT_DIR%\plugins\imageformats										%PATH_TO_COPY_QT%\imageformats *.dll /MT:25

robocopy %QT_DIR%\plugins\platforms											%PATH_TO_COPY_QT%\platforms *.dll    /MT:25

robocopy %QT_DIR%\plugins\mediaservice										%PATH_TO_COPY_QT%\mediaservice *.dll /MT:25

robocopy %QT_DIR%\plugins\audio                                             %PATH_TO_COPY_QT%\audio *.dll     /MT:25

robocopy /S %QT_DIR%\qml                            						%PATH_TO_COPY_QT% 					 /MT:25    

