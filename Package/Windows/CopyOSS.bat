::Path where to copy all the Files
set PATH_TO_COPY="%1"

:: Copy all dependencies to the DSM directory
robocopy %SQLITE_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %SPATIALITE_BIN% 										%PATH_TO_COPY% *.dll /MT:25
robocopy %ICONV_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %GEOS_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %OSG_BIN% 												%PATH_TO_COPY% *.dll /MT:25
:: copy osgconv.exe which is required to convert ive to fbx
robocopy %OSG_BIN% 												%PATH_TO_COPY% osgconv.exe /MT:25
robocopy %OSG_PLUGIN_DIR% 										%PATH_TO_COPY% *.dll /MT:25
robocopy %XERCESC_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %GDAL_BIN%												%PATH_TO_COPY% *.exe /MT:25
robocopy %GDAL_BIN%							 					%PATH_TO_COPY% *.dll /MT:25

::Copygin OSGEarth Related Files
robocopy %OSGEARTH_BIN% 										%PATH_TO_COPY% *.dll /MT:25
robocopy %CURL_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %EXPAT_BIN%	 										%PATH_TO_COPY% *.dll /MT:25
robocopy %LIBPROJ_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %TIFF_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %BOOST_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %JPEG_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %PQLABS_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %ZLIB_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %PNG_BIN% 												%PATH_TO_COPY% *.dll /MT:25
robocopy %FFMPEG_BIN%							 				%PATH_TO_COPY% *.exe /MT:25
robocopy %FFMPEG_BIN%							 				%PATH_TO_COPY% *.dll /MT:25
robocopy %OSG_EPHEMERIS_BIN% 									%PATH_TO_COPY% *.dll /MT:25
robocopy %FREETYPE_BIN% 										%PATH_TO_COPY% *.dll /MT:25
robocopy %GIF_BIN% 												%PATH_TO_COPY% *.dll /MT:25
robocopy %DGNLIB_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %POSTGRESQL_BIN%										%PATH_TO_COPY% *.dll /MT:25
robocopy %ECW_BIN% 												%PATH_TO_COPY% *.dll /MT:25
robocopy %GPSBABEL_BIN% 										%PATH_TO_COPY% *.dll /MT:25
robocopy %GEOTRANS_BIN% 										%PATH_TO_COPY% *.dll /MT:25
robocopy %FMOD_BIN%							 					%PATH_TO_COPY% *.dll /MT:25
robocopy %HDF4_BIN% 											%PATH_TO_COPY% *.dll /MT:25
robocopy %HDF5_BIN%							 					%PATH_TO_COPY% *.dll /MT:25
robocopy %EASYWS_BIN% 			             					%PATH_TO_COPY% *.dll /MT:25
robocopy %FBX_BIN%												%PATH_TO_COPY% *.dll /MT:25	
robocopy %DAE_BIN%												%PATH_TO_COPY% *.dll /MT:25	
robocopy %OSG_GIF_CUSTOM_BIN%									%PATH_TO_COPY% *.dll /MT:25	
robocopy %THRIFT_BIN%											%PATH_TO_COPY% *.dll /MT:25	
robocopy %OPENSSL_BIN%											%PATH_TO_COPY% *.dll /MT:25	
robocopy %TBB_BIN%												%PATH_TO_COPY% *.dll /MT:25	
::robocopy %RTI_JAVA_BIN%											%PATH_TO_COPY% *.dll /MT:25	::
robocopy %OSVR_DIR%/bin											%PATH_TO_COPY% *.dll /MT:25	

::Teigha Copying
robocopy %TEIGHA_BIN%							 				%PATH_TO_COPY% *.dll /MT:25	
robocopy %TEIGHA_BIN%					 					    %PATH_TO_COPY% *.tx /MT:25	

::copying OPENCV
robocopy %OPENCV2_BIN%									 		%PATH_TO_COPY% *.dll /MT:25

::Copying SDL(For JoyStick)
robocopy %SDL_BIN%						 						%PATH_TO_COPY% *.dll /MT:25

::Copying POPPLER LIB

robocopy %POPPLER_BIN%						 					%PATH_TO_COPY% *.dll /MT:25