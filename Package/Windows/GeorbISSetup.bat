:: Set paths and create directory
call userInput.bat GeorbIS

::product name
set NAME=GeorbIS

IF "%OSSBUILD%" == "debug" set NAME=GeorbISd

::company name
set COMPANY=VizExperts

::company URL
set URL=http://www.vizexperts.com

::product version
set VERSION=%GeorbIS_VERSION%

::installer Icon
set INSTALL_ICON=GeorbIS.ico

::Uninstaller icon
set UNINSTALL_ICON=GeorbIS.ico

::legal trademark
set LEGAL_TRADEMARK=Trademark

::legal copyright
set LEGAL_COPYRIGHT=Copyright

::header image for the installer
set HEADER_IMAGE=%TERRAINSDK_DATA_DIR%\data\resource\GeorbIS_Strip.bmp

::splash screen image for installer
set SPLASH_IMAGE=%TERRAINSDK_DATA_DIR%\data\resource\GeorbIS_Splash.bmp

::set install side strip
set SIDE_STRIP_IN_IMAGE=%TERRAINSDK_DATA_DIR%\data\resource\GeorbIS_Side_Strip.bmp

::set uninstall side strip
set SIDE_STRIP_UNIN_IMAGE=%TERRAINSDK_DATA_DIR%\data\resource\GeorbIS_Side_Strip.bmp

::license file name
set LICENSE_FILE=license.rtf

::define readme file
set README_FILE=Readme.rtf

::license checker
set LICENSE_CHECKER=LicenseChecker.exe

::define Uninstaller name
set UNINSTALER_NAME=Uninstall.exe

::define support URL 
set SUPPORT_URL=http://support.vizexperts.com

::define product URL
set PRODUCT_URL=http://www.vizexperts.com

::define product icon
set PRODUCT_ICON=GeorbIS.ico

::execute the nsis script
makensis /V4 GeorbISSetup.nsi