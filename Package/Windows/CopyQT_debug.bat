::Path where to robocopy all the Files
set PATH_TO_COPY_QT="%1"

:: ICU related DLL required for QT 
copy %QT_BIN%\icudt53.dll		  										    %PATH_TO_COPY_QT%
copy %QT_BIN%\icuin53.dll		  										    %PATH_TO_COPY_QT%
copy %QT_BIN%\icuuc53.dll		  										    %PATH_TO_COPY_QT%

:: Copying all the required Qt files
copy %QT_BIN%\Qt5OpenGLd.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Cored.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Declaratived.dll		  									%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Guid.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Quickd.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Qmld.dll		  											%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Widgetsd.dll		  										%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Networkd.dll		  										%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Svgd.dll		  										    %PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5MultimediaQuick_pd.dll		  							    %PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5Multimediad.dll		  										%PATH_TO_COPY_QT%
copy %QT_BIN%\Qt5MultimediaWidgetsd.dll		  								%PATH_TO_COPY_QT%

robocopy %QT_DIR%\plugins\designer											%PATH_TO_COPY_QT%\designer *.dll     /MT:25

robocopy %QT_DIR%\plugins\imageformats										%PATH_TO_COPY_QT%\imageformats *.dll /MT:25

robocopy %QT_DIR%\plugins\platforms											%PATH_TO_COPY_QT%\platforms *.dll    /MT:25

robocopy %QT_DIR%\plugins\mediaservice										%PATH_TO_COPY_QT%\mediaservice *.dll /MT:25

robocopy /S %QT_DIR%\qml                            						%PATH_TO_COPY_QT% 					 /MT:25    

