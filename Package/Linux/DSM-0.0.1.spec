#RPM Spec file for DSM v1.0

%define _tmppath        %{_topdir}/tmp
%define _prefix         /usr/local
%define _defaultdocdir  %{_prefix}/share/doc
%define _mandir         %{_prefix}/man

%define name      dsm
%define summary   DSM software
%define version   1.0
%define release   rhel6.3
%define license   Proprietary
%define group     Development/Languages
%define source    DSM
%define url       http://www.vizexperts.com
%define vendor    VizExperts India Private Limited
%define packager  VizExperts (support@vizexperts.com)
%define buildroot %{_tmppath}/%{name}-root


Name:      %{name}
Version:   %{version}
Release:   %{release}
Packager:  %{packager}
Vendor:    %{vendor}
License:   %{license}
Summary:   %{summary}
Group:     %{group}
Source:    %{source}
URL:       %{url}
Prefix:    %{_prefix}
Buildroot: %{buildroot}

%description
DSM project RPMs for Centre for Artificial Intelligence and Robotics.

%prep
#%setup -q

%files
%doc
%defattr(-,root,root)
%{_prefix}/DSM/bin/linuxx86_64/*
%{_prefix}/DSM/lib/linuxx86_64/*.so*
%{_prefix}/DSM/tests/*
%{_prefix}/DSM/config/*
%{_prefix}/DSM/data/*
%defattr(-,root,root)
