#RPM Spec file for DSM v1.0

%define _tmppath        %{_topdir}/tmp
%define _prefix         /usr/local
%define _defaultdocdir  %{_prefix}/share/doc
%define _mandir         %{_prefix}/man

%define name      dsm-deps
%define summary   Dependancy libraries of DSM Software
%define version   1.0
%define release   rhel6.3
%define license   GPL
%define group     Development/Languages
%define source    VizExperts 
%define url       http://www.vizexperts.com
%define vendor    VizExperts India Private Limited
%define packager  VizExperts (support@vizexperts.com)
%define buildroot %{_tmppath}/%{name}-root

Name:      %{name}
Version:   %{version}
Release:   %{release}
Packager:  %{packager}
Vendor:    %{vendor}
License:   %{license}
Summary:   %{summary}
Group:     %{group}
Source:    %{source}
URL:       %{url}
Prefix:    %{_prefix}
BuildRoot: %{buildroot}

%description
DSM software dependancy libraries.

%prep
#%setup -q

%files
%defattr(-,root,root)
%{_prefix}/DSM/lib/linuxx86_64/*
%{_prefix}/DSM/bin/linuxx86_64/*
%{_prefix}/DSM/data/*
%defattr(-,root,root)
