:: Setting current directory as DSM directory
FOR /F "tokens=1" %%i in ('cd') do set SMCREATOR_DIR=%%i

::3rd Party folder for OsgDB SVG Plugin
set THIRD_PARTY_BIN=%TERRAINSDK_DATA_DIR%\3rdParty
::set QML_IMPORT_PATH=%THIRD_PARTY_BIN%\imports

::Qt Path
set QT_DIR=%OSSROOT%\qt\5.4.1\msvc2013_64_opengl
set QT_BIN=%QT_DIR%\bin
set QT_PLUGIN_PATH=%QT_DIR%\plugins
set QML2_IMPORT_PATH=%QT_DIR%\qml

:: set Terrain sdk
set TERRAINSDK_LIB=%TERRAINSDK_DIR%\%OSSARCH%\lib\%OSSBUILD%
set TERRAINSDK_BIN=%TERRAINSDK_DIR%\%OSSARCH%\bin\%OSSBUILD%

:: Set boost directory
set BOOST_DIR=%OSSROOT%\boost\1.57
set BOOST_BIN=%BOOST_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: EasyWS directory
set EASYWS_DIR=%OSSROOT%\easyws\13
set EASYWS_BIN=%EASYWS_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

::FlexPublisher Library
set FLEXPUBLISHER_DIR=%OSSROOT%\flexera\11.12
set FLEXPUBLISHER_BIN=%FLEXPUBLISHER_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

:: Set SQLite directory
set SQLITE_DIR=%OSSROOT%\sqlite\3.8.2
set SQLITE_BIN=%SQLITE_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set Spatialite root dir and lib dir
set SPATIALITE_DIR=%OSSROOT%\spatialite\4.1
set SPATIALITE_BIN=%SPATIALITE_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set Proj root dir and lib dir
set LIBPROJ_DIR=%OSSROOT%\proj\4.8.0
set LIBPROJ_BIN=%LIBPROJ_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set ICONV root dir and lib dir
set ICONV_DIR=%OSSROOT%\iconv\1.14
set ICONV_BIN=%ICONV_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set GEOS root dir and lib dir
set GEOS_DIR=%OSSROOT%\geos\3.3.8_with_custom_changes_for_2013
set GEOS_BIN=%GEOS_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set Xercesc root dir and lib dir
set XERCESC_DIR=%OSSROOT%\xercesc\3.1.1
set XERCESC_BIN=%XERCESC_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: set OSG root dir and lib dir
:: While changing osg version , make sure that you are also changing the osg-plugin version
set OSG_DIR=%OSSROOT%\osg\3.2.1
set OSG_BIN=%OSG_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%
set OSG_PLUGIN_DIR=%OSG_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%\osgPlugins-3.2.1

:: Set OPENTHREADS root dir and lib dir
set OPENTHREADS_DIR=%OSG_DIR%

:: Set gdal root dir and lib dir
set GDAL_DIR=%OSSROOT%\gdal\1.11.0_(Custom_With_HDF_Support)
set GDAL_BIN=%GDAL_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%
set GDAL_DATA=%GDAL_DIR%\data

::Set HDF4 root dir and lib dir
set HDF4_DIR=%OSSROOT%\hdf4\4.2.11
set HDF4_BIN=%HDF4_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

::Set HDF5 root dir and lib dir
set HDF5_DIR=%OSSROOT%\hdf5\1.8.16
set HDF5_BIN=%HDF5_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set osgEarth root dir and lib dir
:: OSG version used is 3.2.1 for compiling OSGEARTH 2.6
set OSGEARTH_DIR=%OSSROOT%\osgearth\2.6_withSymbologyChanges
set OSGEARTH_BIN=%OSGEARTH_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set the curl directory
set CURL_DIR=%OSSROOT%\curl\7.45.0
set CURL_BIN=%CURL_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set expat variables
set EXPAT_DIR=%OSSROOT%\expat\2.1.0
set EXPAT_BIN=%EXPAT_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

::set osgEphemeris variables
set OSG_EPHEMERIS_DIR=%OSSROOT%\osgEphemeris\Build_With_OSG-3.2.1
set OSG_EPHEMERIS_BIN=%OSG_EPHEMERIS_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set tiff directory
set TIFF_DIR=%OSSROOT%\tiff\4.0.6
set TIFF_BIN=%TIFF_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set jpeg directory
set JPEG_DIR=%OSSROOT%\jpeg\jpeg-8b
set JPEG_BIN=%JPEG_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%
set JPEG_BIN_RELEASE=%JPEG_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

:: Set zlib directory
set ZLIB_DIR=%OSSROOT%\zlib\1.2.8
set ZLIB_BIN=%ZLIB_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set png directory
set PNG_DIR=%OSSROOT%\png\1.4.3_(with_zlib_1.2.8)
set PNG_BIN=%PNG_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: FFMPEG binary path
set FFMPEG_DIR=%OSSROOT%\ffmpeg\N-40301
set FFMPEG_BIN=%FFMPEG_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

::Set OPENCV PATH
set OPENCV2_DIR=%OSSROOT%\openCV\2.4.11
set OPENCV2_BIN=%OPENCV2_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: FBX SDK Dir
set FBX_DIR=%OSSROOT%\fbx\2015.1
set FBX_BIN=%FBX_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: COLLADA Dir
set DAE_DIR=%OSSROOT%\collada\2.2
set DAE_BIN=%DAE_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

::OSG Custom GIF Plugin Dir
set OSG_GIF_CUSTOM_BIN=%OSSROOT%\osgDB_gifCustomPlugin

:: GPSBabel library
set GPSBABEL_DIR=%OSSROOT%\GPSBabel\1.3.6
set GPSBABEL_BIN=%GPSBABEL_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: PQLabs
set PQLABS_DIR=%OSSROOT%\pqlabs\pqclient
set PQLABS_BIN=%PQLABS_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

:: Geo trans library
set GEOTRANS_DIR=%OSSROOT%\GeoTrans\2.4.2
set GEOTRANS_BIN=%GEOTRANS_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: set freetype binary path
set FREETYPE_DIR=%OSSROOT%\freetype\2.3.5
set FREETYPE_BIN=%FREETYPE_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

:: Set gif directory
set GIF_DIR=%OSSROOT%\giflib\4.1.4
set GIF_BIN=%GIF_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

::Set teigha directory
set TEIGHA_DIR=%OSSROOT%\teigha\4.1.1.0
set TEIGHA_BIN=%TEIGHA_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%


::Set DGNLib directory
set DGNLIB_DIR=%OSSROOT%\dgnlib
set DGNLIB_BIN=%DGNLIB_DIR%\lib\%OSSARCH%

::Set PostgreSQL Path
set POSTGRESQL_DIR=%OSSROOT%\libpq\9.2.14
set POSTGRESQL_BIN=%POSTGRESQL_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

::Set Thrift Path
set THRIFT_DIR=%OSSROOT%\thrift\thrift-0.9.3
set THRIFT_LIB=%THRIFT_DIR%\lib\win\%COMPILER%\%OSSARCH%\%OSSBUILD%
set THRIFT_INC=%THRIFT_DIR%\include
set THRIFT_BIN=%THRIFT_DIR%\bin\win

::Set OpenSSL Path
set OPENSSL_DIR=%OSSROOT%\OpenSSL\openssl-1.1.0
set OPENSSL_LIB=%OPENSSL_DIR%\lib\%OSSARCH%\%OSSBUILD%
set OPENSSL_INC=%OPENSSL_DIR%\include
set OPENSSL_BIN=%OPENSSL_DIR%\bin\%OSSARCH%\%OSSBUILD%

::Set OpenSSL Path
set OPENSSL_DIR=%OSSROOT%\OpenSSL\openssl-1.1.0
set OPENSSL_BIN=%OPENSSL_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: ECW binaries
set ECW_DIR=%OSSROOT%\ecw\5.2.1
set ECW_BIN=%ECW_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: TBB Binaries
set TBB_DIR=%OSSROOT%\tbb\4.4
set TBB_BIN=%ECW_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Portico directory
set RTI_HOME=%PORTICO_DIR%
set RTI_DIR=%RTI_HOME%
set RTI_BIN=%RTI_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%
set RTI_JAVA_BIN=%RTI_DIR%\jre\bin\server
set JAVA_HOME=%RTI_DIR%\jre
set RTI_RID_FILE=../../data/HLA/RTI.rid

:: FMOD Directory
set FMOD_DIR=%OSSROOT%\fmod\4.9
set FMOD_BIN=%FMOD_DIR%\bin\win\%COMPILER%\%OSSARCH%\release

:: Set the SDL directory
set SDL_DIR=%OSSROOT%\sdl\2.0.3
set SDL_BIN=%SDL_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set the LIBJSON directory
set LIBJSON_DIR=%OSSROOT%\LibJSON\7.6.1

:: Set POPPLER Library
set POPPLER_DIR=%OSSROOT%\poppler
set POPPLER_LIB=%POPPLER_DIR%\lib\win\%COMPILER%\%OSSARCH%\%OSSBUILD%
set POPPLER_BIN=%POPPLER_DIR%\bin\win\%COMPILER%\%OSSARCH%\%OSSBUILD%

:: Set Dependency Binary Paths
set PATH=%OSG_BIN%;%GDAL_BIN%;%CURL_BIN%;%EXPAT_BIN%;%XERCESC_BIN%;%GEOS_BIN%;%ICONV_BIN%;%LIBPROJ_BIN%;%SPATIALITE_BIN%;%TIFF_BIN%;%BOOST_BIN%;%PATH%
set PATH=%SMCREATOR_DIR%\%OSSARCH%\lib\%OSSBUILD%;%SMCREATOR_DIR%\vbscripts;%OSGEARTH_BIN%;%QT_BIN%;%SQLITE_BIN%;%PATH%
set PATH=%QWT_BIN%;%ZLIB_BIN%;%PNG_BIN%;%GNUPLOT_BIN%;%GS_BIN%;%PATH%
set PATH=%FFMPEG_BIN%;%CLAPACK_DIR%;%POSTGRESQL_BIN%;%PATH%
set PATH=%OSG_EPHEMERIS_BIN%;%OSG_PLUGIN_DIR%;%PATH%
set PATH=%XULRUNNER_BIN%;%OPENVRML_BIN%;%OPENVRML_LIB%;%OPENVRML_INC%;%PATH%
set PATH=%FREETYPE_LIB%;%TAUDEM_BIN%;%MPICH_BIN%;%PATH%
set PATH=%GPSBABEL_BIN%;%PATH%
set PATH=%RTI_JAVA_BIN%;%RTI_BIN%;%RTI_JRE_BIN%;%ECW_BIN%;%PQLIB_BIN%;%GIF_BIN%;%DGNLIB_BIN%;%TEIGHA_BIN%;%PATH%
set PATH=%CGAL_BIN%;;%HDF4_BIN%;%HDF5_BIN%;%PATH%
set PATH=%TERRAINSDK_LIB%;%TERRAINSDK_BIN%;%THIRD_PARTY_BIN%;%PATH%
set PATH=%PQLABS_BIN%;%TBB_BIN%;%FLEXPUBLISHER_BIN%;%PATH%
set PATH=%PATH%;%OPENCV2_BIN%;
set PATH=%PATH%;%GEOTRANS_BIN%;%ALUT_LIB%;%OpenAL_LIB%;%FMOD_BIN%;%SDL_BIN%;%EASYWS_BIN%
set PATH=%PATH%;%FBX_BIN%;%OSG_GIF_CUSTOM_BIN%;%DAE_BIN%
set PATH=%PATH%;%OSVR_DIR%/bin
set PATH=%PATH%;%POPPLER_BIN%
set PATH=%PATH%;%FREETYPE_BIN%

:: We have to make sure that we are adding JPEG DLL from OSS Root first in the path
:: to avoid load from other sources like portico which causes application launch to fail
set PATH=%JPEG_BIN%;%JPEG_BIN_RELEASE%;%PATH%
