/********************************************************************************
** Form generated from reading UI file 'PreProcessInputDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.1.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef PREPROCESSINPUTDIALOG_H
#define PREPROCESSINPUTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class PreProcessInput_Form
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QToolButton *outputBrowse;
    QLineEdit *input;
    QLineEdit *output;
    QLabel *label;
    QToolButton *inputBrowse;
    QLabel *label_2;
    QDialogButtonBox *buttonBox;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->resize(434, 195);
        layoutWidget = new QWidget(Form);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(5, 4, 421, 131));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        outputBrowse = new QToolButton(layoutWidget);
        outputBrowse->setObjectName(QStringLiteral("outputBrowse"));

        gridLayout->addWidget(outputBrowse, 1, 3, 1, 1);

        input = new QLineEdit(layoutWidget);
        input->setObjectName(QStringLiteral("input"));

        gridLayout->addWidget(input, 0, 2, 1, 1);

        output = new QLineEdit(layoutWidget);
        output->setObjectName(QStringLiteral("output"));

        gridLayout->addWidget(output, 1, 2, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 2);

        inputBrowse = new QToolButton(layoutWidget);
        inputBrowse->setObjectName(QStringLiteral("inputBrowse"));

        gridLayout->addWidget(inputBrowse, 0, 3, 1, 1);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(Form);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(270, 150, 156, 23));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", 0));
        outputBrowse->setText(QApplication::translate("Form", "...", 0));
        input->setText(QString());
        input->setPlaceholderText(QApplication::translate("Form", "Input Raw Image", 0));
        output->setText(QString());
        output->setPlaceholderText(QApplication::translate("Form", "Output Processed Image", 0));
        label->setText(QApplication::translate("Form", "Output Image", 0));
        inputBrowse->setText(QApplication::translate("Form", "...", 0));
        label_2->setText(QApplication::translate("Form", "Input Image", 0));
    } // retranslateUi

};

//namespace Ui {
//    class Form: public PreProcessInput_Form {};
//} // namespace Ui

QT_END_NAMESPACE

#endif // PREPROCESSINPUTDIALOG_H
