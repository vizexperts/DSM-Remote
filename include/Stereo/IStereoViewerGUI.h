#ifndef STITCHVIEWERGUI_H
#define STITCHVIEWERGUI_H
#include <QtOpenGL/QGLWidget>
#include <QtGui/QDialog>

namespace Stitcher
{

	class StitchViewerGUI : public QWidget
	{
		Q_OBJECT
		public:
			StitchViewerGUI(QWidget *parent = 0);
			~StitchViewerGUI();

	};
}
#endif