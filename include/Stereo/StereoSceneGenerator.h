#ifndef _STEREOSCENEGENERATOR_H
#define _STEREOSCENEGENERATOR_H

#include <Stereo/export.h>
#include <Stereo/Image.h>
#include <osg/ImageStream>
#include <osg/Geode>
#include <osg/Notify>
#include <osg/MatrixTransform>
#include <osg/Switch>
#include <osg/TexMat>
#include <osg/Texture2D>
#include <osgDB/ReadFile>
#include <osg/PagedLOD>
#include <osgTerrain/Locator>


namespace Stereo
{

    class STEREO_DLL_EXPORT StereoSceneGenerator
    {
        public:
            StereoSceneGenerator();
            ~StereoSceneGenerator();
            osg::Node* loadImage(std::string fileUrl);
            osg::Node* loadImage(std::string fileUrl,Stereo::Image::EnhancementType);
            osg::Group* loadStereoImages(std::string leftImageUrl,std::string rightImageUrl,double rotationAngle = 0.0, double translationX = 0.0, double translationY = 0.0);
            osg::Group* createRootNode(Image* sImage);
            void addTile(Image* sImage, const TileKey& key, osg::Node* geode, osg::Group* parent);
            osg::Node* createNode(const TileKey& parentKey, bool isRight);

            void setRotationAngle(double rotationAngle);
            double getRotationAngle();

            void setTranslationX(double translationX);
            double getTranslationX();

            void setTranslationY(double translationY);
            double getTranslationY();
            
            void setLeftImage(Stereo::Image* leftImage);
            Stereo::Image* getLeftImage();

            void setRightImage(Stereo::Image* rightImage);
            Stereo::Image* getRightImage();

        protected:
            void _createImageFromGdal(Image* sImage);
            osg::Geode* createSectorForImage(Image* sImage, osg::TexMat* texmat);
            osg::Group* _createQuadsAtLevel(Image* sImage, int level);
            osg::Image* _createImageForQuad(Image* sImage, double minX, double minY, double maxX, double maxY);
            osg::Node* _createSectorForOsgImage(osg::Image* image, double minX, double minY, double maxX, double maxY);
            void tessellateSurfaceGeometry(const std::vector<int>& indices, const osg::Vec3Array* surfaceCoords,
                osg::Vec3Array* normalCoords, osg::DrawElements* elements, osgTerrain::Locator* locator, int subRows, int subColumns);
#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
            bool isValidValue(float v, GDALRasterBand* band);
            
            Stereo::Image::EnhancementType _enhancementType;
#endif

        protected:
            osg::ref_ptr<Stereo::Image> _leftImage;
            osg::ref_ptr<Stereo::Image> _rightImage;
            unsigned int _screenHeight;
            unsigned int _screenWidth;
            double         _rotationAngle;
            double         _translationX;
            double         _translationY;
            
    };

}

#endif
