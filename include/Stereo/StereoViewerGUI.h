#ifndef _STEREOVIEWERGUI_H
#define _STEREOVIEWERGUI_H


#include <Stereo/export.h>
#include <osgViewer/CompositeViewer>
#include <osgViewer/ViewerEventHandlers>
#include <StereoOsgQt/GraphicsWindowQt>
#include <QtCore/QTimer>
#ifdef USE_QT4
#include <QtGui/QApplication>
#include <QtGui/QGridLayout>
#include <QtGui/QPushButton>
#else 
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#endif


namespace Stereo
{

    class STEREO_DLL_EXPORT StereoViewerGUI : public QWidget, public osgViewer::CompositeViewer 
    {
        //Q_OBJECT

        public:
            StereoViewerGUI(osgViewer::ViewerBase::ThreadingModel threadingModel=osgViewer::CompositeViewer::SingleThreaded);

            ~StereoViewerGUI();

        protected:
            QWidget* initializeViewWidget();
            virtual void paintEvent( QPaintEvent* event );

        protected:
            QTimer _timer;
    };
}
#endif
