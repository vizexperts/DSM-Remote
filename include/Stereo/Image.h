#ifndef _IMAGE_H
#define _IMAGE_H

#include <Stereo/export.h>
#include <Stereo/TileKey.h>
#include <osg/Image>
#include <osg/Referenced>
#include <gdal_priv.h>

#define CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE

namespace Stereo
{

    class STEREO_DLL_EXPORT Image : public osg::Referenced
    {
        public:
            Image();
            ~Image();

            enum EnhancementType
            {
                NONE,
                MIN_MAX,
                STANDARD_DEVIATION,
                HISTOGRAM_EQUALIZATION

            };

            void setName(std::string name);
            std::string getName();

            void setUrl(std::string url);
            std::string getUrl();

            void setGeoExtents(double* extents);
            double* getGeoExtents();

            void setBounds(double minX, double minY, double maxX, double maxY);
            void getBounds(double& minX, double& minY, double& maxX, double& maxY);

            void setOsgImage(osg::Image* image);
            osg::Image* getOsgImage();

            void setWidth(unsigned int width);
            unsigned int getWidth();

            void setHeight(unsigned int height);
            unsigned int getHeight();

            void isGeoImage(bool value);
            bool isGeoImage();

            bool loadImage(std::string url);

            GDALDataset* getGdalDataset();
            void setGdalDataSet(GDALDataset* dataset);

            void getExtentsForTile(const TileKey& key, double& minX, double& minY, double& maxX, double& maxY);

            bool isRight();
            void isRight(bool value);

#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
            void setGreyScaleValue(double scaleBy);
            double getGreyScaleValue();

            void setRedScaleValue(double scaleBy);
            double getRedScaleValue();

            void setGreenScaleValue(double scaleBy);
            double getGreenScaleValue();

            void setBlueScaleValue(double scaleBy);
            double getBlueScaleValue();

            void setEnhancementType(EnhancementType enhancementType);
            EnhancementType getEnhancementType();
            
            double enhanceGreyBandPixel(double pixelValue, EnhancementType enhanceType);
            double enhanceRedBandPixel(double pixelValue, EnhancementType enhanceType);
            double enhanceGreenBandPixel(double pixelValue, EnhancementType enhanceType);
            double enhanceBlueBandPixel(double pixelValue, EnhancementType enhanceType);
            

            GDALDataType getRasterType();
#endif
            
        protected:
            std::string _name;
            std::string _url;
            double _geoExtents[6];
            double _minX;
            double _maxX;
            double _minY;
            double _maxY;
            osg::ref_ptr<osg::Image> _osgImage;
            GDALDataset* _imageDataset;
            int _noOfBands;
            unsigned int _width;
            unsigned int _height;
            bool _isGeoImage;
            bool _isRight;
            int ComputeEqualizationLUTs( GDALDatasetH hDataset, int nLUTBins,
                         double **ppadfScaleMin, double **ppadfScaleMax, 
                         int ***ppapanLUTs, 
                         GDALProgressFunc pfnProgress );


#ifdef CUSTOM_CHANGES_FOR_16BIT_GRAYSCALE
    GDALDataType _type;
    double             _minPixelValue;
    double             _maxPixelValue;
    double             _meanPixelValue;
    double             _standardDeviation;
    double             _scaleBy;

    double             _minPixelValueForRed;
    double             _maxPixelValueForRed;
    double             _meanPixelValueForRed;
    double             _standardDeviationForRed;
    double             _scaleByForRed;
    double             _shiftByForRed;

    double             _minPixelValueForGreen;
    double             _maxPixelValueForGreen;
    double             _meanPixelValueForGreen;
    double             _standardDeviationForGreen;
    double             _scaleByForGreen;
    double             _shiftByForGreen;

    double           _sdScaleFactor;
    
    double             _minPixelValueForBlue;
    double             _maxPixelValueForBlue;
    double             _meanPixelValueForBlue;
    double             _standardDeviationForBlue;
    double             _scaleByForBlue;
    double             _shiftByForBlue;

    double           _maxPixelValueForGrey;
    double           _minPixelValueForGrey;
    double           _meanPixelValueForGrey;
    double           _standardDeviationForGrey;
    double           _scaleByForGrey;
    double           _shiftByForGrey;

    int              _hasNoDataValueGrey;
    double             _noDataValueGrey;
    int              _hasNoDataValueRed;
    double             _noDataValueRed;
    int              _hasNoDataValueGreen;
    double             _noDataValueGreen;
    int                 _hasNoDataValueBlue;
    double             _noDataValueBlue;
    
    int*             _bandRedLUT;
    int*             _bandGreenLUT;
    int*             _bandBlueLUT;

    int*             _bandGreyLUT;

    int**             _bandsLUT;

    int                 _numOfBins;

    EnhancementType  _enhancementType;
   
    
#endif
    };

}

#endif
