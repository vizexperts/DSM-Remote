#pragma once

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#   if defined(STEREO_LIBRARY)
#      define STEREO_DLL_EXPORT __declspec(dllexport)
#   else
#      define STEREO_DLL_EXPORT __declspec(dllimport)
#   endif 
#else
#   if defined(STEREO_LIBRARY)
#      define STEREO_DLL_EXPORT __attribute__ ((visibility("default")))
#   else
#      define STEREO_DLL_EXPORT
#   endif 
#endif
