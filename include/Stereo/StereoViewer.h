#ifndef _PANORAMICVIEWER_H
#define _PANORAMICVIEWER_H


#include <Stitcher\export.h>
#include <osgViewer/CompositeViewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgQt/GraphicsWindowQt>
#include <QtCore/QTimer>
#include <QtGui/QApplication>
#include <QtGui/QGridLayout>
#include <osg/ImageStream>
#include <osg/Geode>
#include <osg/Notify>
#include <osg/MatrixTransform>
#include <osg/Switch>
#include <osg/TexMat>
#include <osg/Texture2D>

namespace Stitcher
{

	class Stitcher_DLL_EXPORT PanoramicViewer : public QWidget, public osgViewer::Viewer 
	{
		//Q_OBJECT

		public:
			PanoramicViewer(osg::ArgumentParser& arguments);
			void initializeWidget();
			~PanoramicViewer();

		protected:
			QWidget* addPanoramicViewWidget(std::string fileUrl,osg::Camera* camera);
			osg::Camera* createCamera( int x, int y, int w, int h, const std::string& name="", bool windowDecoration=false );
			virtual void paintEvent( QPaintEvent* event );
			osg::Switch* createScene(std::string fileUrl, osg::TexMat* texmatImage,float radius, float height, float length);
			osg::Group * loadImage(std::string fileUrl, osg::TexMat* texmatImage, float radius, float height, float length);
			osg::Geode* createSectorForImage(osg::Image* image, osg::TexMat* texmat, float s,float t, float radius, float height, float length);
			

		protected:
			QTimer _timer;

	};
}
#endif