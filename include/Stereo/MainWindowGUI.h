#pragma once

#include <Stereo/export.h>
#include <Stereo/StereoViewerGUI.h>
#include <Stereo/StereoSceneGenerator.h>

#ifdef USE_QT4 
#include <QtGui/QMainWindow>
#include <QtDeclarative/QDeclarativeView>
#else
#include <QtWidgets/QMainWindow>
//#include <QtDeclarative/QDeclarativeView>
#endif

namespace Stereo
{
    #define DEFAULT_STEREO_BEHAVIOUR osg::DisplaySettings::QUAD_BUFFER

    class MainWindowFilter : public QObject
    {
        public:
            //constructor
            MainWindowFilter();
            //public functions
            void setMainWindow(QMainWindow* mainWindow);

            void setGraphicsWindow(osgViewer::GraphicsWindow* gw);

            bool eventFilter(QObject* object, QEvent* event);
        private:

            QMainWindow* _mainWindow;

            osgViewer::GraphicsWindow* _gw;

            bool _fullscreen;
    };


    class STEREO_DLL_EXPORT MainWindowGUI : public QMainWindow
    {
        Q_OBJECT

        public:
           
            MainWindowGUI(QWidget* parent = NULL);
            MainWindowGUI(std::string url,Stereo::Image::EnhancementType, QWidget* parent = NULL);
            ~MainWindowGUI();

            QWidget *ui_MainWindowGUIWidget; 

            void setApplicationBusy(bool);

            Stereo::StereoViewerGUI* getViewer();

        signals:

            void _showBusyBar(bool);
            void applicationClosed();
            void applicationClosed(Stereo::MainWindowGUI*);

        protected:
            void _initializeViewer();
            void _initializeViewer(std::string url,Stereo::Image::EnhancementType);
            void closeEvent(QCloseEvent *event);

        private slots:
            void _handleLoadButtonClicked();
            void _handleStereoButtonClicked();
            void _handleStereoFileButtonClicked();
            void _handlePreProcessButtonClicked();
            void _stereoInputsSelected(std::string inputImage1, std::string inputImage2,double rotationAngle,osg::DisplaySettings::StereoMode);
            void _processingInputsSelected(QString inputImage, QString outputImage);
            void _stereoConfigFileSelected(std::string inputFile);
            void _handleSaveStereoConfigButtonClicked();
            void _operationSucessfull(QString);
            void _operationFailed(QString);

        protected:
            QTimer _timer;
            QPushButton* _loadButton;
            QPushButton* _stereoButton;
            QPushButton* _stereoFileButton;
            QPushButton* _preprocessButton;
            QPushButton* _saveStereoConfigButton;
            QGridLayout* _gridLayout;
            QHBoxLayout* _hLayout;
            QDialog* _busyDialog;
            Stereo::StereoViewerGUI* _viewer;
            Stereo::StereoSceneGenerator* _stereoSceneGenerator;
            MainWindowFilter* _mainWindowFilter;

            QAction*        _saveStereoAction;

            /// This is used in getting last used path
            std::string _lastPath;
    };

}
