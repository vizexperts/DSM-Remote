/********************************************************************************
** Form generated from reading UI file 'StereoInputDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.1.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef STEREOINPUTDIALOG_H
#define STEREOINPUTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class StereoInput_Form
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QToolButton *inputBrowse2;
    QLineEdit *input1;
    QLineEdit *input2;
    QLabel *label;
    QToolButton *inputBrowse1;
    QLabel *label_2;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->setWindowModality(Qt::NonModal);
        Form->resize(444, 189);
        buttonBox = new QDialogButtonBox(Form);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(275, 154, 156, 23));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        layoutWidget = new QWidget(Form);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 8, 421, 131));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        inputBrowse2 = new QToolButton(layoutWidget);
        inputBrowse2->setObjectName(QStringLiteral("inputBrowse2"));

        gridLayout->addWidget(inputBrowse2, 1, 3, 1, 1);

        input1 = new QLineEdit(layoutWidget);
        input1->setObjectName(QStringLiteral("input1"));

        gridLayout->addWidget(input1, 0, 2, 1, 1);

        input2 = new QLineEdit(layoutWidget);
        input2->setObjectName(QStringLiteral("input2"));

        gridLayout->addWidget(input2, 1, 2, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 2);

        inputBrowse1 = new QToolButton(layoutWidget);
        inputBrowse1->setObjectName(QStringLiteral("inputBrowse1"));

        gridLayout->addWidget(inputBrowse1, 0, 3, 1, 1);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);


        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", 0));
        inputBrowse2->setText(QApplication::translate("Form", "...", 0));
        input1->setText(QString());
        input1->setPlaceholderText(QApplication::translate("Form", "Input Left Stereo Image", 0));
        input2->setText(QString());
        input2->setPlaceholderText(QApplication::translate("Form", "Input Right Stereo Image", 0));
        label->setText(QApplication::translate("Form", "Right Image", 0));
        inputBrowse1->setText(QApplication::translate("Form", "...", 0));
        label_2->setText(QApplication::translate("Form", "Left Image", 0));
    } // retranslateUi

};

namespace Ui {
    class Form: public StereoInput_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // STEREOINPUTDIALOG_H
