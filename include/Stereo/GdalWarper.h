#ifndef GDALWARPER_HEADER
#define GDALWARPER_HEADER

#include <gdalwarper.h>
#include <cpl_string.h>
#include <ogr_spatialref.h>
#include <ogr_api.h>
#include <gdal_priv.h>
#include <osg/Referenced>
#include <vector>
#include <QObject>

namespace Stereo
{
    /////////////////////////////////////////////////////////
    //
    //The code for all the functions in GdalWarper class is taken
    //from the gdalwarp application
    //
    /////////////////////////////////////////////////////////
    class GdalWarper : public QObject
    {
        Q_OBJECT

        public:
            GdalWarper();
            ~GdalWarper();

            void setOutputFormat(std::string format);
            std::string getOutputFormat();

            void setWarpMethod(std::string method);
            std::string getWarpMethod();

            void setDataType(std::string type);
            std::string getDataType();

            void setInputImageUrl(std::string url);
            std::string getInputImageUrl();

            void setOutputImageUrl(std::string url);
            std::string getOutputImageUrl();

            int warp();

        public Q_SLOTS:
           void start();

        Q_SIGNALS:
           void sucessfull(QString message);
           void failed(QString message);



        protected:
            //void LoadCutline( const char *pszCutlineDSName, const char *pszCLayer, 
            //     const char *pszCWHERE, const char *pszCSQL, 
            //     void **phCutlineRet );

            //void TransformCutlineToSource( GDALDatasetH hSrcDS, void *hCutline,
            //                  char ***ppapszWarpOptions, char **papszTO );

            void CheckExtensionConsistency(const char* pszDestFilename,
                               const char* pszDriverName);
            GDALDatasetH GDALWarpCreateOutput( char **papszSrcFiles, const char *pszFilename, 
                                  const char *pszFormat, char **papszTO,
                                  char ***ppapszCreateOptions, GDALDataType eDT,
                                  void ** phTransformArg,
                                  GDALDatasetH* phSrcDS );
            void RemoveConflictingMetadata( GDALMajorObjectH hObj, char **papszMetadata,
                                       const char *pszValueConflict );
            int GDALExit( int nCode );

        protected:

            double            dfMinX;
            double            dfMinY;
            double            dfMaxX;
            double            dfMaxY;
            double            dfXRes;
            double            dfYRes;
            int                bTargetAlignedPixels;
            int                nForcePixels;
            int                nForceLines;
            int                bQuiet;
            int                bEnableDstAlpha;
            int                bEnableSrcAlpha;
            int                bVRT;
            int             bOverwrite;
            std::string     outputFormat;
            std::string        warpMethod;
            std::string        dataType;
            std::string        inputImageUrl;
            std::string        outputImageUrl;




    };

}
#endif