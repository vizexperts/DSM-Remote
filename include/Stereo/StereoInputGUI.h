#ifndef STEREOINPUTGUI_H
#define STEREOINPUTGUI_H

#ifdef USE_QT4
#include <QtGui/QWidget>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>
#include <QtGui/QDialog>
#else 
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QDialog>
#endif

#include <osg/DisplaySettings>

namespace Stereo
{
    class StereoInputGUI : public QDialog
    {
        Q_OBJECT

        QDialogButtonBox *_buttonBox;
        QLineEdit        *_inputName1;
        QLineEdit        *_inputName2;
        QLineEdit        *_inputRotation;
        QToolButton      *_inputBrowseButton1;
        QToolButton      *_inputBrowseButton2;
    public:

        StereoInputGUI(QWidget *parent = 0);

        QWidget *ui_StereoInputGUIWidget;

    signals:

        void _selectedInputs(std::string inputImage1, std::string inputImage2, double rotationAngle, osg::DisplaySettings::StereoMode sMode);

    private slots:
            
        void _handleInputBrowse1ButtonClicked();
        void _handleInputBrowse2ButtonClicked();
        void _handleCancelButtonClicked();
        void _handleOkButtonClicked();

   protected:

        virtual ~StereoInputGUI();
        virtual void makeConnections();
        void _clear();

        /// This is used in getting last used path
        std::string _lastPath;
    };
}






#endif
