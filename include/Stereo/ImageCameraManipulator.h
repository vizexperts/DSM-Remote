#ifndef _ImageCameraEVENTHANDLER_H
#define _ImageCameraEVENTHANDLER_H

#include <Stereo/export.h>
#include<osgGA/OrbitManipulator>
#include <Stereo/StereoSceneGenerator.h>

namespace Stereo
{
    class STEREO_DLL_EXPORT ImageCameraManipulator: public osgGA::OrbitManipulator
    {
        public:
            ImageCameraManipulator( int flags = DEFAULT_SETTINGS )
            {
                setVerticalAxisFixed(true);
            }
            ImageCameraManipulator( const ImageCameraManipulator& om,
                const osg::CopyOp& copyOp = osg::CopyOp::SHALLOW_COPY ) : OrbitManipulator( om, copyOp )
            {}
            ~ImageCameraManipulator(){};

            void setSceneGenerator(StereoSceneGenerator* sceneGenerator){_sceneGenerator = sceneGenerator;}

        protected:
            virtual bool performMovementLeftMouseButton(const double eventTimeDelta, const double dx, const double dy);
            virtual bool performMovementRightMouseButton(const double eventTimeDelta, const double dx, const double dy);
            virtual bool handleKeyDown( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us );
            virtual bool handleMouseWheel( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us );


        protected:

            StereoSceneGenerator* _sceneGenerator;

    };
}
#endif
