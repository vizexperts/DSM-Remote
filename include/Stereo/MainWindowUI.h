/********************************************************************************
** Form generated from reading UI file 'StitcherGUI.ui'
**
** Created by: Qt User Interface Compiler version 5.1.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef STITCHERGUI_H
#define STITCHERGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class MainWindow_Form
{
public:
    QGroupBox *groupBox;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *preprocessButton;
    QPushButton *loadButton;
    QPushButton *stereoButton;
	QPushButton *stereoFileButton;
	QPushButton *saveStereoConfigButton;
    QSpacerItem *horizontalSpacer_2;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->resize(832, 689);
        groupBox = new QGroupBox(Form);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 600, 811, 80));
        horizontalLayoutWidget = new QWidget(groupBox);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 20, 791, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        preprocessButton = new QPushButton(horizontalLayoutWidget);
        preprocessButton->setObjectName(QStringLiteral("preprocessButton"));

        horizontalLayout->addWidget(preprocessButton);

        loadButton = new QPushButton(horizontalLayoutWidget);
        loadButton->setObjectName(QStringLiteral("loadButton"));

        horizontalLayout->addWidget(loadButton);

        stereoButton = new QPushButton(horizontalLayoutWidget);
        stereoButton->setObjectName(QStringLiteral("loadStereoButton"));

        stereoFileButton = new QPushButton(horizontalLayoutWidget);
        stereoFileButton->setObjectName(QStringLiteral("loadStereoFileButton"));

        saveStereoConfigButton = new QPushButton(horizontalLayoutWidget);
        saveStereoConfigButton->setObjectName(QStringLiteral("saveStereoButton"));

        horizontalLayout->addWidget(stereoButton);
		horizontalLayout->addWidget(stereoFileButton);
		horizontalLayout->addWidget(saveStereoConfigButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        gridLayoutWidget = new QWidget(Form);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 831, 581));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", 0));
        groupBox->setTitle(QString());
        preprocessButton->setText(QApplication::translate("Form", "Pre Process Image", 0));
        loadButton->setText(QApplication::translate("Form", "Load Single Image", 0));
        stereoButton->setText(QApplication::translate("Form", "Load Stereo Pair", 0));
		stereoFileButton->setText(QApplication::translate("Form", "Load Stereo File", 0));
		saveStereoConfigButton->setText(QApplication::translate("Form", "Save Stereo Config", 0));
    } // retranslateUi

};

//namespace Ui {
//    class Form: public MainWindow_Form {};
//} // namespace Ui

QT_END_NAMESPACE

#endif // STITCHERGUI_H
