#ifndef _ImageReaderWriter_H
#define _ImageReaderWriter_H

#include <Stereo/export.h>
#include <Stereo/StereoSceneGenerator.h>
#include <osgGA/OrbitManipulator>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <osgDB/Registry>

namespace Stereo
{
    class STEREO_DLL_EXPORT ImageReaderWriter : public osgDB::ReaderWriter
    {
        public:
            ImageReaderWriter(){};
            virtual const char* className() const { return "ImageReaderWriter"; }
            ~ImageReaderWriter(){};

            virtual bool acceptsExtension(const std::string& extension) const
            {
                return    osgDB::equalCaseInsensitive( extension, "StereoViz_Image" );
            }

            virtual ReadResult readNode(const std::string& uri, const Options* options) const
            {
                if ( "StereoViz_Image" == osgDB::getFileExtension(uri) )
                {
                    std::string tileDef = osgDB::getNameLessExtension(uri);
                    unsigned int lod, x, y,isRight;
                    sscanf(tileDef.c_str(), "%d/%d/%d/%d", &lod, &x, &y, &isRight);

                    TileKey key( lod, x, y);

                    osg::ref_ptr< osg::Node > node = _sceneGenerator->createNode( key,bool(isRight) );

                    if ( !node.valid() )
                    {
                        return ReadResult::FILE_NOT_FOUND;
                    }

                    return ReadResult( node.get(), ReadResult::FILE_LOADED );

                }
                else
                {
                    return ReadResult::FILE_NOT_HANDLED;
                }
            }

            void setSceneGenerator(StereoSceneGenerator* sceneGenerator){_sceneGenerator = sceneGenerator;}

        protected:

            StereoSceneGenerator* _sceneGenerator;


    };

    REGISTER_OSGPLUGIN(StereoViz_Image, ImageReaderWriter)
}
#endif
