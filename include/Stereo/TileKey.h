#ifndef _TILEKEY_H
#define _TILEKEY_H

#include <Stereo/export.h>
#include <osg/Image>
#include <osg/Referenced>
#include <gdal_priv.h>


namespace Stereo
{

    class STEREO_DLL_EXPORT TileKey
    {
        public:   
            TileKey() { };
            TileKey(unsigned int lod, unsigned int tile_x, unsigned int tile_y);
            TileKey( const TileKey& rhs );
            virtual ~TileKey() { };
            static TileKey INVALID;

            TileKey createChildKey( unsigned int quadrant ) const;
            TileKey createParentKey() const;
            TileKey createNeighborKey( int xoffset, int yoffset ) const;
            unsigned getLOD() const { return _lod; }
            unsigned int getTileX() const { return _x; }
            unsigned int getTileY() const { return _y; }
            void getTileXY(unsigned int& out_tile_x,unsigned int& out_tile_y) const;
            void getPixelExtents(unsigned int& out_minx,unsigned int& out_miny,unsigned int& out_maxx,unsigned int& out_maxy,const unsigned int& tile_size) const;

        protected:
            std::string _key;
            unsigned int _lod;
            unsigned int _x;
            unsigned int _y;
    };
}
#endif
