#ifndef _StereoImageEVENTHANDLER_H
#define _StereoImageEVENTHANDLER_H

#include <Stereo\export.h>
#include<OsgGA/GUIEventHandler>
#include <osg/TexMat>

namespace Stereo
{
	class STEREO_DLL_EXPORT StereoImageEventHandler : public osgGA::GUIEventHandler
	{
	public:

		StereoImageEventHandler();
		~StereoImageEventHandler() {}
	    
		//META_Object(StitchWizApp,StereoImageEventHandler);


		void set(osg::Node* sw, float offsetX, float offsetY, osg::TexMat* texmatImage, float timePerSlide, bool autoSteppingActive);

		virtual bool handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter&);
	    
		void scaleImage(float s);

		void translateImage(float rx,float ry);

		void initTexMatrices();

	protected:

		StereoImageEventHandler(const StereoImageEventHandler&,const osg::CopyOp&) {}

		osg::ref_ptr<osg::Node>   _switch;
		osg::ref_ptr<osg::TexMat>   _texmatImage;
		float                        _radius;
		float                        _height;
		float                        _length;
		bool                        _firstTraversal;
		unsigned int                _activeSlide;
		double                      _previousTime;
		double                      _timePerSlide;
		bool                        _autoSteppingActive;
		float						_scale;
		float						_px;
		float						_py;
	        
	};
}

#endif