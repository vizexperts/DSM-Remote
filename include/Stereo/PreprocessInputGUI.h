#ifndef PREPROCESSINPUTGUI_H
#define PREPROCESSINPUTGUI_H

#ifdef USE_QT4
#include <QtGui\QWidget>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QToolButton>
#include <QtGui/QDialog>
#else 
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QDialog>
#include <Stereo/PreProcessInputDialog.h>
#endif

namespace Stereo
{
    class PreprocessInputGUI : public QDialog
    {
        Q_OBJECT

        QDialogButtonBox *_buttonBox;
        QLineEdit        *_inputName;
        QLineEdit        *_outputName;
        QToolButton      *_inputBrowseButton;
        QToolButton      *_outputBrowseButton;
    public:

        PreprocessInputGUI(QWidget *parent = 0);

        QWidget *ui_PreprocessInputGUIWidget;

    signals:

            void _selectedInputs(QString inputImage, QString outputImage);

    private slots:
            
        void _handleInputBrowseButtonClicked();
        void _handleOutputBrowseButtonClicked();
        void _handleCancelButtonClicked();
        void _handleOkButtonClicked();

   protected:

        virtual ~PreprocessInputGUI();
        //! Loads the .ui file and subscribes signals to slots
        virtual void makeConnections();
        void _clear();

        /// This is used in getting last used path
        std::string _lastPath;
    };
}






#endif
