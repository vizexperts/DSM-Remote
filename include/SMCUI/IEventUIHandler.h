#ifndef SMCUI_IEVENTUIHANDLER_H_
#define SMCUI_IEVENTUIHANDLER_H_

/*****************************************************************************
*
* File             : IEventUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : IEventUIHandler class
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013-2014, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <Core/IMetadataRecord.h>
#include <SMCUI/export.h>
#include <Core/IFeatureLayer.h>
#include <Core/IMetadataTableDefn.h>
#include <boost/date_time/posix_time/ptime.hpp>

namespace CORE
{
    class IPoint;
    class IObject;
}

namespace EFFECTS
{
    class IEvent;
}

namespace SMCUI
{
    class IEventUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IEventUIHandler);

       //! Point updated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> EventUpdatedMessageType;

        //! Point created message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> EventCreatedMessageType;

    public:

        enum EventMode
        {
            EVENT_MODE_NONE,
            EVENT_MODE_CREATE_POINT,
            EVENT_MODE_EDIT_POINT,
            EVENT_MODE_EDIT_ATTRIBUTES
        };

        //@{
        /** Set/Get offsets */
        virtual void setOffset(double offset) =0;
        virtual double getOffset() const = 0;
        //@}

        //! set the current mode
        virtual void setMode(EventMode mode) = 0;

        //! get the current mode
        virtual EventMode getMode() = 0;

        //! set the current selected line as current line
        virtual void setSelectedEventAsCurrentPoint() = 0;

        //! update the current line
        virtual void update() = 0;

        //! remove the created point
        virtual void removeCreatedEvent() = 0;

        //! set temporary state for the point
        virtual void setTemporaryState(bool state) = 0;

        //! get temporary state for the point
        virtual bool getTemporaryState() const = 0;

        //! set clamping state for the point
        virtual void setClampingState(bool state) = 0;

        //! get clamping state for the point
        virtual bool getClampingState() const = 0;

        //! Creates default point
        virtual void create(bool addToWorld = true) = 0;

        //! Reset the handler
        virtual void reset() = 0;

        virtual CORE::IFeatureLayer* getOrCreateEventLayer(const std::string& name) = 0;

        //! add a event
        virtual CORE::RefPtr<EFFECTS::IEvent> addEvent(const std::string& name, const std::string& type,
            const boost::posix_time::ptime& startTime, const boost::posix_time::ptime& endTime) = 0;
       
    };
}   // namespace SMCUI

#endif
