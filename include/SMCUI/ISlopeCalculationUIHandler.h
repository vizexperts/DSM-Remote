#ifndef SMCUI_ISLOPECALCULATIONUIHANDLER_H_
#define SMCUI_ISLOPECALCULATIONUIHANDLER_H_

/*****************************************************************************
* File              : ISlopeCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for slope calculation
* Author            : Mohan Singh
* Author email      : mohan@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <Core/IPoint.h>

namespace SMCUI
{
    //! UIHandler for slope ui handler
    class ISlopeCalculationUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ISlopeCalculationUIHandler);

    public:

        // Slope Calculated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> SlopeCalculationCompletedMessageType;

    public:

         //@{
        /** Set/get point1 of the line to be processed */
        virtual void setPoint1(CORE::IPoint* point1)=0;
        virtual CORE::IPoint* getPoint1()=0;
        //@}

        //@{
        /** Set/get point1 of the line to be processed */
        virtual void setPoint2(CORE::IPoint* point2)=0;
        virtual CORE::IPoint* getPoint2()=0;
        //@}
        virtual void reset()=0;
        //! Executes the filter
        virtual void execute() = 0;
        //return  slope in String Data Type
        virtual std::string getSlopeText()=0;

    };
} // namespace SMCUI

#endif  // SMCUI_ISLOPECALCULATIONUIHANDLER_H_
