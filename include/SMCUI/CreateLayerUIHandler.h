#pragma once

/*****************************************************************************
 *
 * File             : CreateLayerUIHandler.h
 * Version          : 1.0
 * Module           : UI
 * Description      : CreateLayerUIHandler class declaration
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : UI interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>

#include <VizUI/UIHandler.h>

#include <SMCUI/ICreateLayerUIHandler.h>

namespace SMCUI
{
    // Interface to create a new layer
    class CreateLayerUIHandler : public VizUI::UIHandler, 
                                 public ICreateLayerUIHandler
    {
        DECLARE_IREFERENCED;
        public:

            //! Constructor
            CreateLayerUIHandler();

            //! Returns the UniqueID of the feature layer
            const CORE::UniqueID* getLayerUniqueID() const;

            //! check the layer name availability
            bool checkLayerNameAvailability(const std::string& name);

            //! create a layer
            bool createLayer(const std::string& layerName,
                             const std::string& layerType,
                             std::vector<std::string>& attrNames,
                             std::vector<std::string>& attrTypes,
                             std::vector<int>& attrWidths,
                             std::vector<int>& attrPrecisions);

    };

} // namespace UI

