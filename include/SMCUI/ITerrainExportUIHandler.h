#ifndef SMCUI_ITERRAINEXPORTUIHANDLER_H_
#define SMCUI_ITERRAINEXPORTUIHANDLER_H_

/******************************************************************************
* File              : ITerrainExportUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Slope aspect filter UI Handler
* Author            : Kamal Grover
* Author email      : kamal@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <GISCompute/IHillshadeFilterVisitor.h>

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI
{
    class ITerrainExportUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ITerrainExportUIHandler);
        //! Progress updated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> ProgressValueUpdatedMessageType;
    public:

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;

        /** Set the extents of the area */
        virtual void setCenter(osg::Vec3d center) = 0;
        virtual osg::Vec3d getCenter() const = 0;

        /** Set the extents of the area */
        virtual void setExtentsUTM(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtentsUTM() const = 0;

        virtual void setSideLength(int sidelength) = 0;
        virtual int getSideLength() = 0;

        virtual void setLongSpan(double longspan) = 0;
        virtual double getLongSpan() = 0;

        virtual void setLatSpan(double latspan) = 0;
        virtual double getLatSpan() = 0;

        virtual void setTileSize(int tilesize) = 0;
        virtual int getTileSize() = 0;

        virtual void setOutDir(std::string outDir) = 0;
        virtual std::string getOutDir() = 0;

        virtual void setTempDir(std::string tempDir) = 0;
        virtual std::string getTempDir() = 0;

        virtual void setClassificationMap(std::map < std::string, std::string >& map) = 0;
        virtual std::map < std::string, std::string >& getClassificationMap() = 0;
        //@}
        virtual int verifyAndCountLayers() = 0;

        virtual std::string getLastCompletedLayer() = 0;
        virtual void cancelExport(bool hasCancelled) = 0; 
        virtual void execute(int* progress) = 0;

        virtual void populateVectorAttributes(std::string vectorFile, std::vector < std::string>& abbtibuteList) = 0;

        virtual void generateRasterFromVector(std::string inputFile, std::vector<std::string>& attributes, std::string outDir, osg::Vec4d extents, osg::Vec2d resolution, int* statusUpdate) = 0;
    };

} // namespace SMCUI

#endif // SMCUI_ITERRAINEXPORTUIHANDLER_H_
