#ifndef SMCUI_ELEVATIONPROFILEVISITOR_H_
#define SMCUI_ELEVATIONPROFILEVISITOR_H_

/*****************************************************************************
* File              : ElevationProfileUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for surface area filter
* Author            : Rahul Srivastava
* Author email      : rahul@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <osg/Array>
#include <GISCompute/IPointListClampingVisitor.h>
#include <GISCompute/IColorTerrainProfileVisitor.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/IElevationProfileUIHandler.h>
#include <Core/ISelectionComponent.h>
#include <Core/ILine.h>
#include <Core/IVisibility.h>

namespace SMCUI
{
    //! interface used for calculating area of a polygon
    class ElevationProfileUIHandler : public IElevationProfileUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        ElevationProfileUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Sets/gets the points of the polygons to be processed */
        void setPoints(osg::Vec3dArray* points);
        const osg::Vec3dArray* getPoints() const;
        const osg::Vec2dArray* getDistanceAltitudePoints() const;
        //@}

        //! Returns the name of the generated graph file
        const std::string& getGraphFilename();

        osg::Vec3d getMinimumElevationPoint() const;
        osg::Vec3d getMaximumElevationPoint() const;
        double getAverageElevation() const;

        //! Resets values of the visitor
        void setFocus(bool flag);

        //! Subscribe to filter messages
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Executes the filter
        void execute();

        //! Clears the line
        void clearLine();

        void stop();

        CORE::RefPtr<CORE::ILine> getLine() const;

        //! set marking state
        void setMarking(bool value);

        void selectLine(bool value);

        std::vector<std::string> getColorVector();
    protected:

        ~ElevationProfileUIHandler();

        //! Generates a graph using the point list
        void _generateGraph();

        //! reset params
        void _reset();

        void _createPointsAt(osg::ref_ptr<osg::Vec3dArray> pointList);

        void createPointAt(osg::Vec3d position);

        void _removeObject(CORE::IObject *object);

    protected:

        //! ISurfaceAreaVisitor 
        CORE::RefPtr<GISCOMPUTE::IColorTerrainProfileVisitor> _clampingVisitor;

        //! Graph file name
        std::string _filename;

        //! Point list
        osg::ref_ptr<osg::Vec3dArray> _pointList;

        //! Distance vs Height point list
        osg::ref_ptr<osg::Vec2dArray> _distanceAltitudePoints;

        //! Graph generation script
        //std::string _graphGenerationScript;

        //! Minimum height point
        osg::Vec3d _minPoint;
        //! Maximum height point
        osg::Vec3d _maxPoint;
        //! Average height
        double _avgHeight;

        //!
        CORE::RefPtr<CORE::ILine> _line;

        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

        std::vector<CORE::RefPtr<CORE::IVisibility>> selectedObjectVector;
    };
} // namespace SMCUI

#endif  // SMCUI_ELEVATIONPROFILEVISITOR_H_
