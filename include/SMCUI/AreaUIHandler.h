#ifndef SMCUI_AREAUIHANDLER_H_
#define SMCUI_AREAUIHANDLER_H_


/******************************************************************************
* File              : AreaUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for UI handler of Area
* Author            : Kumar Saurabh Arora
* Author email      : saurabh@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <SMCUI/IAreaUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Core/IPolygon.h>
#include <Core/ILine.h>
#include <osgUtil/LineSegmentIntersector>
#include <Core/IFeatureLayer.h>
#include <GISCompute/ISurfaceAreaCalcVisitor.h>
#include <Elements/IMultiPoint.h>
#include <osgEarth/SpatialReference>

namespace CORE
{
    class IPoint;
}
namespace VizUI
{
    class IMouse;
}

namespace SMCUI
{

    class PolygonUpdateCallback : public osg::NodeCallback
    {
    public:

        PolygonUpdateCallback(){}

        virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);

        //! polygon representing the area
        CORE::RefPtr<CORE::IPolygon> area;

        //! represents exterior ring of the polygon
        CORE::RefPtr<CORE::ILine> line;

        //! Lock for updating the queue
        OpenThreads::Mutex  mutex;

        //! status of polygon
        bool dirty;
    };


    //! Handles GUI inputs for Area
    class SMCUI_DLL_EXPORT AreaUIHandler
        : public IAreaUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        AreaUIHandler();

        //! Set/Get AreaMode
        virtual bool setMode(AreaMode mode);
        virtual AreaMode getMode() const;

        //! Set/Get Color of Area
        virtual void setColor(osg::Vec4 color);
        virtual osg::Vec4 getColor() const;

        //! Set/Get Extents
        virtual void setExtents(osg::Vec4d extents);

        // Get Clicked Point
        virtual osg::Vec3d getClickedPoint();

        //returns extents in xmin, ymin, xmax, ymax format 
        // that is equivalent to bottom left x, bottom left y , top right x, top right y format
        virtual osg::Vec4d getExtents();

        virtual osg::Vec4d getExtentsUTM();
        //! Set/Get Extents
        virtual void setFixedRectangleWidthHeight(osg::Vec2d widthHeihgt);
        virtual osg::Vec2d getFixedRectangleWidthHeight();
        
        virtual std::vector<int>& getSelectedVertexList();
        virtual void setSelectedVertexList(std::vector<int> _selectedPointList);

        //! function called when added to UI Manager
        void onAddedToManager();

        //! set the focus of the UI Handler
        void setFocus(bool value);

        //! update function
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        double getArea() const
        {
            if (_visitor != NULL)
                return _visitor->getArea();
            else
                return 0.0;
        }


        //! Returns the current area instance
        CORE::IPolygon* getCurrentArea() const;

        void addToLayer(CORE::IFeatureLayer *featureLayer);

        //! set the current selected area as current line
        void setSelectedAreaAsCurrentArea();

        void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions) const;

        //! set the metadata record to the current area
        void setMetadataRecord(CORE::IMetadataRecord* record);

        //! get the metadata record of the current area
        CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const;

        //! update the current area
        void update();

        //! add area to current selected layer
        void addAreaToCurrentSelectedLayer();

        //! get current selected layer definition
        CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn();

        //! remove the created area
        void removeCreatedArea();

        //! delete the last point from the area
        void deleteLastPointFromArea();

        //! set temporary state for the area
        void setTemporaryState(bool state);

        //! get temporary state for the area
        bool getTemporaryState() const;

        void initializeAttributes();

        //@{
        /** Set/Get offsets */
        void setOffset(double offset);
        double getOffset() const;
        //@}

        void reset();

        //! get Number of points in the Polygon
        unsigned int getNumPoints() const;

        std::string jsonFile;
    protected:

        enum DIRECTION
        {
            TOP = 0,
            TOP_RIGHT = 45,
            RIGHT = 90,
            BOTTOM_RIGHT = 135,
            BOTTOM = 180,
            BOTTOM_LEFT = 225,
            LEFT = 270,
            TOP_LEFT = 315,

        };
        void _addToLayer(CORE::IFeatureLayer *featureLayer);

        void _createMoveUndoTransaction(unsigned int index);

        //! current mode of Area Handler
        AreaMode _mode;

        //! handle mouse button click
        void _handleMouseClickedIntersection(int button, const osg::Vec3d& longLatAlt);

        //! handle mouse dragged
        void _handleMouseDragged(int button, const osg::Vec3d& longLatAlt);

        //! handle mouse release
        void _handleMouseReleased(int button);

        void _deleteCurrentSelectedArea();

        //! Updates the area feature
        void _updateArea();

        void _resetMultiPoints();

        //! delete Point at index
        bool _deletePointAtIndex(int index);

        //! move point at index to position(long, lat, alt)
        void _movePointAtIndex(unsigned int index, osg::Vec3 position);

        void _addPoint(osg::Vec3 position);

        void _insertPoint(unsigned int index, osg::Vec3 position);

        void _setProcessMouseEvents(bool value);

        bool _getProcessMouseEvents() const;

        void _handleRectangleMode(osg::Vec3d longLatAlt);

        void _handleRectangleMode(osg::Vec4d extents);

        //returns extents in blx, bly, trx, try format
        osg::Vec4d _calculateExtents(osg::Vec3d center);

        void _setArea(osg::Vec4d _extents);
        
    private:

        
        CORE::RefPtr<CORE::IFeatureLayer> getOrCreateAreaLayer();

        //! create the line for the polygon
        void _createLine();

        //! create a polygon and add it to the scene
        void _createPolygon();

        //! polygon representing the area
        CORE::RefPtr<CORE::IPolygon> _area;

        //! line representing
        CORE::RefPtr<CORE::ILine> _line;

        //! offset for the area
        double _offset;

        GISCOMPUTE::ISurfaceAreaCalcVisitor* _visitor;

        //! index of point under editing
        int _currentSelectedPoint;

        //! callback to add output of the filter in the layer
        CORE::RefPtr<PolygonUpdateCallback> _updateCallback;

        //! multipoint for editing
        CORE::RefPtr<ELEMENTS::IMultiPoint> _multiPoint;

        //! temporary state
        bool _temporary;

        bool _dragged;

        bool _processMouseEvents;

        bool _handleButtonRelease;

        //clicked point
        osg::Vec3d _clickedVertex;

        // vertex used to make a rectangle
        osg::Vec3d _firstVertex;
        osg::Vec3d _secondVertex;

        bool _firstVertexSet;
        osg::Vec4d _fixedRectExtents;
        osg::Vec4d _fixedRectExtentsUTM;
        osg::Vec2d _fixedRectangle;
        std::vector<int> _selectedPointList;
    };

} // namespace SMCUI
#endif // SMCUI_AREAUIHANDLER_H_
