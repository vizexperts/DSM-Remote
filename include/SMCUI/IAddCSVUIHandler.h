#pragma once

/*****************************************************************************
 *
 * File             : IAddCSVUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : IAddCSVUIHandler class
 * Author           : Abhinav Goyal
 * Author email     : abhinav@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <SMCUI/export.h>

#include <App/IUIHandler.h>

#include <Util/BaseExceptions.h>


//! \brief  Handles add content gui events from the GUI
namespace SMCUI 
{
    class IAddCSVUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAddVectorUIHandler);

        public:

            virtual bool readFile(const std::string &layerName, const std::string &url, std::vector<std::string>& hyperlinkList, int latIndex, int longIndex, int unitIndex) = 0;
            virtual void getAttributesList(std::vector<std::string>& vStrings, std::string& url) = 0;
    };
} // namespace SMCUI


