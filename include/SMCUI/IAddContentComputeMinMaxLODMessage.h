#ifndef SMCUI_IAddContentComputeMinMaxLODMessage_H
#define SMCUI_IAddContentComputeMinMaxLODMessage_H

/******************************************************************************
 * File              : IAddContentComputeMinMaxLODMessage.h
 * Version           : 1.0
 * Module            : SMCUI
 * Description       : IAddContentComputeMinMaxLODMessage class declaration
 * Author            : Raghavendra Polinki
 * Author email      : rp@vizexperts.com
 * Reference         : 
 * Inspected by      : 
 * Inspection date   :  
 
 *******************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *******************************************************************************
 *
 * Revision log (latest on the top): 
 *
 ******************************************************************************/

#include <SMCUI/export.h>
#include <Core/CoreMessage.h>
#include <Core/IMessageType.h>

namespace SMCUI
{
    //! IFilter Status message type
    class IAddContentComputeMinMaxLODMessage : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAddContentComputeMinMaxLODMessage);
        public:

            // Add Content message types
            static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> AddContentComputeMinMaxLODMessageType;

            enum AddContentComputeMinMaxLODStatus
            {
                IDLE,
                SUCCESS,
                FAILURE,
                PENDING,
                PAUSED,
                STOPPED,
                STOP_INITIATED
            };

        public:

            //@{
            /** Sets/Gets the add content status */
            virtual void setStatus(AddContentComputeMinMaxLODStatus status) = 0;
            virtual AddContentComputeMinMaxLODStatus getStatus() const = 0;
            //@}

            virtual void setProgress(unsigned int progress) = 0;
            virtual unsigned int getProgress() = 0;

            virtual void setMinLOD(unsigned int minLOD) = 0;
            virtual unsigned int getMinLOD() = 0;

            virtual void setMaxLOD(unsigned int minLOD) = 0;
            virtual unsigned int getMaxLOD() = 0;


        protected:

            ~IAddContentComputeMinMaxLODMessage(){}
    };
}

#endif // SMCUI_IADDCONTENTSTATUSMESSAGE_H
