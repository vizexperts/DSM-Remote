#pragma once

/******************************************************************************
* File              : TacticalSymbolsUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : UIHandler for creating and editing Tactical Symbols
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright (c) 2010-2011 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IFeatureLayer.h>
#include <Core/ITerrain.h>
#include <Core/ISelectable.h>

#include <Elements/IMultiPoint.h>

#include <SMCUI/ITacticalSymbolsUIHandler.h>

#include <VizUI/MouseHandler.h>

#include <TacticalSymbols/ITacticalSymbol.h>

#include <VizUI/ITerrainPickUIHandler.h>

//#include <ogrsf_frmts.h>

namespace SMCUI
{
    class SelectionDecoration
    {

    public:

        osg::Node* getOSGNode();

        void setSelectable(CORE::RefPtr<CORE::ISelectable> selectable);

        void initialize();

        void reset();

        CORE::RefPtr<ELEMENTS::IMultiPoint> boundaryControlPoints();
        CORE::RefPtr<ELEMENTS::IMultiPoint> rotationPoint();

        bool findNearestControlPoint(osg::Vec3d point);

        int getCurrentSelectedPointIndex();
        //void setCurrentHighlighted

        //! move point at index to position(long, lat, alt)
        void moveCurrentPointTo(osg::Vec3 longLatAlt);

        void doRotate(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3 longLatAlt);

        void resizeBounds(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3 longLatAlt);

        void rotatePoint(osg::Vec2d& point, const osg::Vec2d& center, double angle);

        void moveBounds(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3 longLatAlt);

    private:

        osg::ref_ptr<osg::Group> _group;

        osg::ref_ptr<osg::Geode> _geode;
        osg::ref_ptr<osg::PositionAttitudeTransform> _pat;
        osg::ref_ptr<osg::Geometry> _boundaryQuad;
        osg::ref_ptr<osg::Geometry> _rotationAxis;

        CORE::RefPtr<ELEMENTS::IMultiPoint> _boundaryControlPoints;
        CORE::RefPtr<ELEMENTS::IMultiPoint> _rotationPoint;
        CORE::RefPtr<ELEMENTS::IMultiPoint> _translationPoint;

        CORE::RefPtr<CORE::ISelectable> _selection;

        int _currentSelectedPoint = -1;

    };

    class SMCUI_DLL_EXPORT TacticalSymbolsUIHandler
        : public ITacticalSymbolsUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        TacticalSymbolsUIHandler();

        bool setMode(Mode mode);

        Mode getMode() const;

        void setSymbolName(const std::string& name);
        std::string getSymbolName() const;

        void setAffiliation(TACTICALSYMBOLS::ITacticalSymbol::Affiliation affiliation) ;
        TACTICALSYMBOLS::ITacticalSymbol::Affiliation getAffiliation()const;

        void setPlanStatus(TACTICALSYMBOLS::ITacticalSymbol::PlanStatus status) ;
        TACTICALSYMBOLS::ITacticalSymbol::PlanStatus getPlanStatus()const;

        void setSymbolPath(const std::string& str);

        //! Returns the current line instance
        TACTICALSYMBOLS::ITacticalSymbol* getCurrentSymbol() const;        

        //! Called when UIHandler added to manager
        void onAddedToManager();

        //@{
        //** Set focus after deleting the line object */
        void setFocus(bool value);
        //@}

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! set the current selected line as current line
        void setSelectedSymbolAsCurrentSymbol();

        void _reset();

        void inline _addToWorld(osg::ref_ptr<osg::Node> node)
        {
            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->addChild(node);
        }

        void inline _removeFromWorld(osg::ref_ptr<osg::Node> node) 
        {
            getWorldInstance()->getTerrain()->getOSGNode()->asGroup()->removeChild(node);
        }

        //! setProcessMouse events
        void _setProcessMouseEvents(bool value);

        // get process mouse events
        bool _getProcessMouseEvents() const;


    protected:

        //! Handles the intersection
        void _handleMouseClickedIntersection(int button, const osg::Vec3d& longLatAlt);

        void _handleMouseReleased(int button);

        void _handleMouseDragged(int button, const osg::Vec3d& longLatAlt);

        void _resetDecoration();

        void _setCreateSymbolMode();

        void _setEditMode();

        void _setModeNone();

        void _setModeNoOperation();

        void _initializeBoundsToView(TACTICALSYMBOLS::ITacticalSymbol::Bounds& bounds, osg::Vec3d longLatAlt);

        void _subscribeToMouseMessage(const CORE::IMessageType& messageType);
        void _unsubscribeToMouseMessage(const CORE::IMessageType& messageType);

    protected:

        //! current mode of Line Handler
        Mode _mode;

        //! reference of current Tactical symbol object
        CORE::RefPtr<TACTICALSYMBOLS::ITacticalSymbol> _tacticalSymbol;

        //! Symbol name
        std::string _name;

        TACTICALSYMBOLS::ITacticalSymbol::Affiliation _affiliation = TACTICALSYMBOLS::ITacticalSymbol::FRIEND;
        TACTICALSYMBOLS::ITacticalSymbol::PlanStatus _planStatus = TACTICALSYMBOLS::ITacticalSymbol::PLANNED;;

        SelectionDecoration _selectionDecoration;

    private:

        TACTICALSYMBOLS::ITacticalSymbol* createTacticalSymbolObject();

        CORE::IObject* createFolderObject();

        CORE::RefPtr<CORE::IFeatureLayer> getOrCreateSymbolLayer();

        std::string _symbolFilePath;

        bool _dragged;

        bool _processMouseEvents;

        bool _handleButtonRelease;

        CORE::RefPtr<VizUI::ITerrainPickUIHandler> _terrainPickUIHandler;

    };

} // namespace SMCUI

