#pragma once

/*****************************************************************************
 *
 * File             : IAddVectorGUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : IAddVectorGUIHandler class
 * Author           : Abhinav Goyal
 * Author email     : abhinav@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <SMCUI/export.h>

#include <App/IUIHandler.h>

#include <Util/BaseExceptions.h>

#include <Core/IMessageType.h>

#include <osg/Vec2d>

#include <osgEarthSymbology/Style>

//! \brief  Handles add content gui events from the GUI
namespace SMCUI 
{
    class IAddVectorUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAddVectorUIHandler);

        public:

            virtual void addVectorData(const std::string &url, osgEarth::Symbology::Style &style) = 0;

            virtual void setOffset(double offset) = 0;
            virtual double getOffset() const = 0;

            virtual void setClamping(bool clamping) = 0;
            virtual bool getClamping() const = 0;

            virtual void setEditable(bool editable) = 0;
            virtual bool getEditable() const = 0;

            virtual void setIconFileName(std::string iconFileName="") = 0;
            virtual std::string getIconFileName() const = 0;

            virtual void createProfileFromVector(std::vector<osg::Vec2d> pointVec)= 0; 

            virtual void setDistanceProfileObject(std::string profileName, std::map<double, std::string> distanceProfileObject) = 0;

            virtual void setProfileTexture(std::string texturefile) = 0; 

    };
} // namespace SMCUI


