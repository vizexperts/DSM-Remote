#ifndef SMCUI_ISLOPEASPECTUIHANDLER_H_
#define SMCUI_ISLOPEASPECTUIHANDLER_H_

/******************************************************************************
* File              : ISlopeAspectUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Slope aspect filter UI Handler
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>

#include <GISCompute/ISlopeAspectFilterVisitor.h>
#include <Elements/IColorMap.h>
namespace ELEMENTS
{
    class IColorMap;
}

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI 
{
    class ISlopeAspectUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ISlopeAspectUIHandler);

    public:

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;
        //@}

        //! set computation type
        virtual void setComputeType(GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType type) = 0;

        virtual void setElevationObject(TERRAIN::IElevationObject* obj) = 0;

        virtual void setOutputName(std::string& name) = 0;
        //Set Map size
        virtual void setMapSize(int mapSize) = 0;
        //! Set Color Map Type
        virtual  void setColorMapType(ELEMENTS::IColorMap::COLORMAP_TYPE colorMapType) = 0;
        virtual void execute() = 0;

        virtual void stopFilter() = 0;
        virtual void setColorMap(ELEMENTS::IColorMap* colorMap) = 0;
    };

} // namespace SMCUI

#endif // SMCUI_ISLOPEASPECTUIHANDLER_H_
