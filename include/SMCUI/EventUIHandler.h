#ifndef SMCUI_EVENTUIHANDLER_H_
#define SMCUI_EVENTUIHANDLER_H_


/******************************************************************************
* File              : EventUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for UI handler of Event
* Author            : Nishant Singh
* Author email      : ns@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <SMCUI/IEventUIHandler.h>
#include <VizUI/UIHandler.h>
#include <osgUtil/LineSegmentIntersector>
#include <osg/Geode>
#include <osg/Geometry>
#include <Core/Intersections.h>
#include <Core/ICompositeObject.h>
#include <Effects/IEventComponent.h>


namespace CORE
{
    class IMetadataRecord;
    class IFeatureLayer;
}

namespace SMCUI 
{
    //! Handles GUI inputs for creating Events
    class SMCUI_DLL_EXPORT EventUIHandler : public IEventUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        EventUIHandler();

        //@{
        /** Set/Get offsets */
        virtual void setOffset(double offset);
        virtual double getOffset() const;
        //@}

        //! On gaining focus, create a new Event
        virtual void setFocus(bool value);

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        CORE::IFeatureLayer* getOrCreateEventLayer(const std::string& name);

        //! Function called when added to UI Manager
        void onAddedToManager();

        void initializeAttributes();

        //! set the current mode
        void setMode(IEventUIHandler::EventMode mode);

        //! get the current mode
        IEventUIHandler::EventMode getMode();

        //! set the current selected line as current line
        void setSelectedEventAsCurrentPoint();

        //! set the metadata record to the current line
        void setMetadataRecord(CORE::IMetadataRecord* record);

        //! get the metadata record of the current line
        CORE::IMetadataRecord* getMetadataRecord() const;

        //! update the current line
        void update();

        //! get current selected layer definition
        CORE::IMetadataTableDefn* getCurrentSelectedLayerDefn();

        void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions) const;

        //! remove created point
        void removeCreatedEvent();

        //! set temporary state for the point
        void setTemporaryState(bool state);

        //! get temporary state for the point
        bool getTemporaryState() const;

        //! set clamping state for the point
        void setClampingState(bool state);

        //! get clamping state for the point
        bool getClampingState() const;

        void create(bool addToWorld=true);
        //void del();

        //! Resets the handler
        void reset();

        //add a event
        CORE::RefPtr<EFFECTS::IEvent> addEvent(const std::string& name, const std::string& type,
            const boost::posix_time::ptime& startTime, const boost::posix_time::ptime& endTime);

    protected:

        void _addToLayer(CORE::IFeatureLayer *featureLayer);

        void _createUndoMoveTransaction();

        void _performHardReset();

        ~EventUIHandler();

        void _handleButtonDraggedIntersection(int button, const osg::Vec3d& longLatAlt);

        void _handleButtonReleasedIntersection(int button, const osg::Vec3d& longLatAlt);

        void _handleButtonDoubleClickedIntersection(int button, CORE::RefPtr<CORE::Intersection> intersection);

        CORE::RefPtr<CORE::IObject> _createFeatureLayer(const std::string& layerName,
            std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes,
            std::vector<int>& attrWidths,
            std::vector<int>& attrPrecisions,
            CORE::ICompositeObject* composite);

    private:

        //! point
        CORE::RefPtr<CORE::IPoint> _point;

        //! Offset for the point
        double _offset;

        //! mode
        IEventUIHandler::EventMode _mode;

        //! temporary state
        bool _temporary;

        //! clamping state
        bool _clamping;

        //! name
        bool _dragged;

        //! event type name
        std::string _eventTypeName;

        //! Event Component
        CORE::RefPtr<EFFECTS::IEventComponent> _eventComponent;

    };

} // namespace SMCUI

#endif // SMCUI_EVENTUIHANDLER_H_
