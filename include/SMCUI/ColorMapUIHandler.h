#ifndef SMCUI_COLORMAPUIHANDLER_H_
#define SMCUI_COLORMAPUIHANDLER_H_


/******************************************************************************
* File              : ColorMapUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class 
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <SMCUI/IColorMapUIHandler.h>
#include <VizUI/UIHandler.h>

#include <Elements/IColorMapComponent.h>

namespace CORE
{
    class IPoint;
}

namespace LOADER
{
    class IColorMapReaderWriter;
}

namespace SMCUI
{
    class SMCUI_DLL_EXPORT ColorMapUIHandler 
        : public IColorMapUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        ColorMapUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! create color map
        CORE::RefPtr<ELEMENTS::IColorMap> getColorMap(const std::string& name);


        //! write color map
        bool exportColorMap(ELEMENTS::IColorMap*, const std::string& name);

    protected:

        ~ColorMapUIHandler();

    protected:

        // color map component
        CORE::RefPtr<ELEMENTS::IColorMapComponent> _colorMapComponent;

        // color map reader writer
        LOADER::IColorMapReaderWriter* _loader;
    };

} // namespace SMCUI

#endif // SMCUI_COLORMAPUIHANDLER_H_

