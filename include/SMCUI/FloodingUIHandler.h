#ifndef SMCUI_FLOODINGUIHANDLER_H_
#define SMCUI_FLOODINGUIHANDLER_H_


/******************************************************************************
* File              : FloodingUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class 
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/IFloodingUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Terrain/IElevationObject.h>


namespace SMCUI
{
    class SMCUI_DLL_EXPORT FloodingUIHandler 
        : public IFloodingUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        FloodingUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        virtual bool getFocus() const;

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Set the extents of the area */
        void setExtents(osg::Vec4d extents);
        osg::Vec4d getExtents() const;
        //@}
         //@{
            //! set the river shapefile
            void setBreachPoint(const osg::Vec3d& point);
            osg::Vec3d getBreachPoint() const;
         //@}

        //@{
            //! To specify rise in water level
            void setWaterLevelRise(double rise);
            double getWaterLevelRise() const;

        //@}
                    //@{
            //! To get filled Voulme
            double getFilledVolume();
             //@}
        //! start on ok
        virtual void execute();

        //!
        void stopFilter();

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //!
        void setElevationObject(TERRAIN::IElevationObject* obj);

        //!
        void setOutputName(std::string& name);

    protected:

        ~FloodingUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

    protected:

        //! minmax visitor
        CORE::RefPtr<CORE::IBaseVisitor> _visitor;

        //! Array containing area points
        osg::ref_ptr<osg::Vec2Array> _extents;

        //!
        CORE::RefPtr<TERRAIN::IElevationObject> _elevObj;

        //!
        std::string name;
        //offset 
        osg::Vec3d  _breachPoint;
        double      _rise;
        double      _filledVolume;

    };

} // namespace SMCUI

#endif // SMCUI_FLOODINGUIHANDLER_H_

