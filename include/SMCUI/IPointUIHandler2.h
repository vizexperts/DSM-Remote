#pragma once

/*****************************************************************************
*
* File             : IPointUIHandler2.h
* Version          : 1.0
* Module           : SMCUI
* Description      : IPointUIHandler2 class
* Author           : Sumit Pandey
* Author email     : sumitp@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

namespace CORE
{
    class IPoint;
    class IMetadataRecord;
    class IMetadataTableDefn;
    class IFeatureLayer;
}

namespace SMCUI 
{
    class IPointUIHandler2 : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IPointUIHandler2);
    public:

        //! Point updated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> PointUpdatedMessageType;

        //! Point created message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> PointCreatedMessageType;

    public:



        enum PointMode
        {
            POINT_MODE_NONE,
            POINT_MODE_CREATE_POINT,
            POINT_MODE_EDIT_POINT,
            POINT_MODE_EDIT_ATTRIBUTES
        };

        //@{
        /** Sets/Gets Point */
        virtual void setPoint(CORE::IPoint* Point) = 0;
        virtual CORE::IPoint* getPoint() const = 0;
        //@}

        //@{
        /** Set/Get offsets */
        virtual void setOffsetFromConfig(double offset) =0;
        virtual double getOffsetFromConfig() const = 0;
        //@}

        //! set the current mode
        virtual void setMode(PointMode mode) = 0;

        //! get the current mode
        virtual PointMode getMode() = 0;

        //! set the current selected line as current line
        virtual void setSelectedPointAsCurrentPoint() = 0;

        //! set the metadata record to the current line
        virtual void setMetadataRecord(CORE::IMetadataRecord* record) = 0;

        //! get the metadata record of the current line
        virtual CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const = 0;

        virtual void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions)const = 0;

        //! update the current line
        virtual void update() = 0;

        virtual void addToLayer(CORE::IFeatureLayer *featureLayer) = 0;

        //! add point to current selected layer
        virtual void addPointToCurrentSelectedLayer() = 0;

        //! get current selected layer definition
        virtual CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn() = 0;

        //! remove the created point
        virtual void removeCreatedPoint() = 0;

        //! set temporary state for the point
        virtual void setTemporaryState(bool state) = 0;

        //! get temporary state for the point
        virtual bool getTemporaryState() const = 0;

        //! Set Clamping from UIHandler config
        virtual void setClampingStateFromConfig(bool clamp) = 0;

        //! Get Clamping from UIHandler Config
        virtual bool getClampingStateFromConfig() const = 0;

        //! Creates default point
        virtual void create(bool addToWorld=true) = 0;
        //virtual void del() = 0;

        //! Reset the handler
        virtual void reset() = 0;

//        virtual void setAssociatedImages(const std::vector<std::string>& imageVector, const std::string& attributeToSet) = 0;

        virtual  void getAssociatedImages(std::vector<std::string> &images) = 0;

        virtual void addAssociatedImage(const std::string& image/*, const std::string& attributeToSet*/) = 0;

        virtual std::string getAssociatedVideoOrAudio(const std::string& attributeToSet) = 0 ;

        virtual void setAssociatedVideoOrAudio(const std::string& file, const std::string& attributeToSet) = 0;

        virtual void getAssociatedFilePaths(std::vector<std::string> &associatedfiles, std::string fieldName) =0; 

        virtual void setIcon(const std::string& iconFile) = 0;

        virtual void setPointEnable(const bool val) = 0;
        virtual bool getPointEnable() const = 0;

        // Getter and setter function for associated text with point.
        virtual void setAssociatedText(const std::string& textInfo) = 0;
        virtual const std::string getAssociatedText() = 0;
    };
} // namespace SMCUI
