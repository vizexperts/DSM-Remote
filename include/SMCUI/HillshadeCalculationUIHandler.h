#ifndef SMCUI_HILLSHADECALCULATIONUIHANDLER_H_
#define SMCUI_HILLSHADECALCULATIONUIHANDLER_H_


/******************************************************************************
* File              : HillshadeCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/IHillshadeCalculationUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Terrain/IElevationObject.h>


namespace SMCUI
{
    class SMCUI_DLL_EXPORT HillshadeCalculationUIHandler
        : public IHillshadeCalculationUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        HillshadeCalculationUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        virtual bool getFocus() const;

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Set the extents of the area */
        void setExtents(osg::Vec4d extents);
        osg::Vec4d getExtents() const;
        //@}
        //@{
        /** Set the zenith angle of the illumination source */
        void setZenithAngle(double angle);
        double getZenithAngle() const;
        //@}

        //@{
        /**   Set the Azimuth angle of the illumination source */

        void setAzimuthAngle(double angle);
        double getAzimuthAngle()const;
        //@}

        //@{
        /**   the transparency coefficient */

        void setTransparency(double transparency);
        double getTransparency()const;
        //@}
        /** Set the color*/
        void setUserDefineColor(osg::Vec4d ocolor);
        osg::Vec4d getUserDefineColor() const;
        //@
        //! start on ok
        virtual void execute();

        //!
        void stopFilter();

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //!
        void setElevationObject(TERRAIN::IElevationObject* obj);

        //!
        void setOutputName(std::string& name);

    protected:

        ~HillshadeCalculationUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

    protected:

        //! minmax visitor
        CORE::RefPtr<CORE::IBaseVisitor> _visitor;

        //! Array containing area points
        osg::ref_ptr<osg::Vec2Array> _extents;

        //!
        CORE::RefPtr<TERRAIN::IElevationObject> _elevObj;

        //!
        std::string name;
        //offset 
        double _zenithAngle;
        double _azimuthAngle;
        double _transparencyValue;
        osg::Vec4d _color;
    };

} // namespace SMCUI

#endif // SMCUI_HILLSHADECALCULATIONUIHANDLER_H_

