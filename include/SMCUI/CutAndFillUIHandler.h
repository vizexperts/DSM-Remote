#ifndef SMCUI_CUTANDFILLUIHANDLER_H_
#define SMCUI_CUTANDFILLUIHANDLER_H_


/******************************************************************************
* File              : CutAndFillUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class 
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/ICutAndFillUIHandler.h>
#include <GISCompute/ICutAndFillFilterVisitor.h>
#include <VizUI/UIHandler.h>
#include <Terrain/IElevationObject.h>


namespace SMCUI
{
    class SMCUI_DLL_EXPORT CutAndFillUIHandler 
        : public ICutAndFillUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        CutAndFillUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        virtual bool getFocus() const;

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}
            //! Set Color
        void setColor(GISCOMPUTE::ICutAndFillFilterVisitor::COLOR color);
        //GISCOMPUTE::ICutAndFillFilterVisitor::COLOR getColor() const ;
        //! get/set stream shapefile
        void setOutputName(std::string& name);


         //! set area extent in longitude/latitude
        void setExtents(osg::Vec4d extents);
        osg::Vec4d getExtents() const;


        //! To specify the Transparency
        void setTransparency(double transparency);
        double getTransparency() const;

        //! start on ok
        virtual void execute();

        //!
        void stopFilter();

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //!
        void setElevationObject(TERRAIN::IElevationObject* obj);

        //!
        void setInputElevationObject(TERRAIN::IElevationObject* objFirst,TERRAIN::IElevationObject* objSecond);
    protected:

        ~CutAndFillUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

    protected:

        //! minmax visitor
        CORE::RefPtr<CORE::IBaseVisitor> _visitor;

        //! area extent
        osg::ref_ptr<osg::Vec2Array> _areaExtent;

        //! Transparency
        double _transparency;

        //!
        CORE::RefPtr<TERRAIN::IElevationObject> _elevObj;
        //!
        CORE::RefPtr<TERRAIN::IElevationObject> _firstInputElevObj;
                //!
        CORE::RefPtr<TERRAIN::IElevationObject> _secondInputElevObj;
        //! output name
        std::string _name;
        GISCOMPUTE::ICutAndFillFilterVisitor::COLOR _color;

    };

} // namespace SMCUI

#endif // SMCUI_CUTANDFILLUIHANDLER_H_

