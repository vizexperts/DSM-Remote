#ifndef SMCUI_FLYAROUNDUIHANDLER_H_
#define SMCUI_FLYAROUNDUIHANDLER_H_


/******************************************************************************
* File              : FlyAroundUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class for fly around
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/
// include files

#include <VizUI/UIHandler.h>
#include <SMCUI/IFlyAroundUIHandler.h>
#include <osg/Geode>



namespace SMCUI
{
    class SMCUI_DLL_EXPORT FlyAroundUIHandler 
        : public IFlyAroundUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        FlyAroundUIHandler();

       // to start the flyAround
        void start(FlyAround flyAround, CORE::RefPtr<VizUI::ICameraUIHandler> cameraUIHandler);

        // to stop the flyAround
        void stop();

        // to pause the flyAround
        void pause(Mode preview);

        // set the selected point over which flyAround is to be done
		void setPointCoordinates(CORE::IPoint* point) ;

        // Handles tick message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        // creates geometry for current selected flyAround
        void visualise(FlyAround flyAround);

        // doubles the speed
        void increaseSpeed();

        // divides the speed by 2
        void decreaseSpeed();

        //function to close flythrough and setting camera at geoJump level
        void closePopUp();

    protected:

        ~FlyAroundUIHandler();

    protected:

        CORE::RefPtr<CORE::IPoint> _point;
        
        CORE::RefPtr<VizUI::ICameraUIHandler> _cameraUIHandler;
        
        CORE::RefPtr<osg::PositionAttitudeTransform> _arcPat;

        FlyAround _flyAround;

        osg::Vec3 _longLatPos;

        bool _pause;

        bool _stopPreview;

        float _angularDistance;

        float _pitch;

        float _range;

        int _totalAngle;

        float _omega;

    };

}

#endif // SMCUI_FLYAROUNDUIHANDLER_H_

