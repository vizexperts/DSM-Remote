#pragma once

/*****************************************************************************
 *
 * File             : ICreateFeatureUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : ICreateFeatureUIHandler class declaration
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : SMCUI interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <SMCUI/export.h>

#include <Core/IBaseUtils.h>
#include <Core/IFeatureLayer.h>
#include <Core/RefPtr.h>

namespace CORE
{
    class IFeatureObject;
}

namespace SMCUI
{
    // Interface to create a new layer
    class ICreateFeatureUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ICreateFeatureUIHandler);

        public:

            ////! Returns the UniqueID of the feature layer
            //virtual const CORE::UniqueID* getLayerUniqueID() const = 0;

            ////! check if the layer name is available or not
            //virtual bool checkLayerNameAvailability(const std::string& name) = 0;

            ////! create a layer
            //virtual bool createLayer(const std::string& layerName,
            //                         const std::string& layerType,
            //                         std::vector<std::string>& attrNames,
            //                         std::vector<std::string>& attrTypes,
            //                         std::vector<int>& attrWidths,
            //                         std::vector<int>& attrPrecisions) = 0;

            virtual CORE::RefPtr<CORE::IFeatureLayer> getFeatureLayer(std::string type) = 0;
    };

} // namespace indiUI

