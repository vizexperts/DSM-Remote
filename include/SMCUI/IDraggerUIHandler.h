#ifndef SMCUI_IDRAGGERUIHANDLER_H_
#define SMCUI_IDRAGGERUIHANDLER_H_

/*****************************************************************************
*
* File             : IDeletionUIHandler.h
* Version          : 1.1
* Module           : SMCUI
* Description      : IDraggerUIHandler interface declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : VizUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*****************************************************************************/

#include <SMCUI/export.h>

#include <Core/IReferenced.h>
#include <Core/RefPtr.h>
#include <Core/IPoint.h>
#include <Core/IMessageType.h>

#include <Elements/IModelHolder.h>

#include <osg/Vec3d>
#include <osg/Quat>

namespace osg
{
    class Node;
    class Geode;
}


///////////////////////////////////////////////////////////////////////////////
//!
//! \file IDraggerUIHandler.h
//!
//! \brief IDraggerUIHandler interface declaration
//!
///////////////////////////////////////////////////////////////////////////////
namespace SMCUI 
{
    ///////////////////////////////////////////////////////////////////////////
    //!
    //! \interface IDraggerUIHandler
    //!
    ///////////////////////////////////////////////////////////////////////////
    class IDraggerUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IDraggerUIHandler);

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \brief DraggerUpdatedMessage
        //!
        ///////////////////////////////////////////////////////////////////////////////
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> DraggerUpdatedMessageType;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \enum DragStage
        //!
        //! \brief represents current drag stage
        //!
        ///////////////////////////////////////////////////////////////////////////////
        enum DragStage
        {
            START,
            MOVE,
            FINISH
        };

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \enum DraggerMode
        //!
        //! \brief represents three different types of draggers available
        //!
        ///////////////////////////////////////////////////////////////////////////////
        enum DraggerMode
        {
            DRAGGER_MODE_NONE = 0,
            DRAGGER_MODE_TRANSLATE = 1,
            DRAGGER_MODE_ROTATE = 2,
            DRAGGER_MODE_SCALE = 3
        };

    public:

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn void setModel(CORE::RefPtr<ELEMENTS::IModelFeatureObject> object)
        //!
        //! \brief convenience funtion that takes position rotation and scale of the object 
        //!        and initializes the dragger accordingly
        //! \param object Currently selected modelFeatureObject
        //!
        ///////////////////////////////////////////////////////////////////////////////
        virtual void setModel(CORE::RefPtr<ELEMENTS::IModelHolder> object) = 0;

    };

} // namespace SMCUI
#endif // SMCUI_IDRAGGERUIHANDLER_H_