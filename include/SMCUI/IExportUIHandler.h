#ifndef SMCUI_IEXPORTUIHANDLER_H_
#define SMCUI_IEXPORTUIHANDLER_H_

/*****************************************************************************
 *
 * File             : IExportUIHandler.h
 * Version          : 1.0
 * Module           : UI
 * Description      : IExportUIHandler class
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : UI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <osg/Vec4>

namespace SMCUI 
{
    class IExportUIHandler : public vizCore::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IExportUIHandler);
        public:

        enum TMSCREATIONSTATUS
        {
            IDLE,
            ERRORINCREATION,
            PROGRESS,
            COMPLETED
        };


            typedef vizUtil::Exception TMSCreationException;
            typedef vizUtil::Exception ProjectExportException;
           //! export the file
            virtual bool exportRaster(const std::string& filename) = 0;
            virtual bool exportRasterAsTMS(const std::string& directoryPath, const std::string& minLevel, const std::string& maxLevel, const osg::Vec4& selectedArea) = 0;
            virtual bool exportProject(const std::string& projectFileName) = 0;
			virtual void getStatus(TMSCREATIONSTATUS& status) = 0;
    };
} // namespace UI

#endif // SMCUI_IEXPORTUIHANDLER_H_
