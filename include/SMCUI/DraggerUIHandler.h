#ifndef SMCUI_DRAGGERUIHANDLER_H_
#define SMCUI_DRAGGERUIHANDLER_H_

/*****************************************************************************
*
* File             : TerrainPickUIHandler.h
* Version          : 1.1
* Module           : SMCUI
* Description      : DraggerUIHandler interface declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : Part of TERRAINKIT Framework
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/
#include <SMCUI/export.h>

#include <SMCUI/IDraggerUIHandler.h>
#include <Core/IReferenced.h>
#include <VizUI/UIHandler.h>
#include <Core/IPoint.h>
#include <SMCUI/DraggerContainer.h>

namespace SMCUI
{
    //! Class used for making object dragger
    class SMCUI_DLL_EXPORT DraggerUIHandler : 
        public IDraggerUIHandler, 
        public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        DraggerUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //! Subscribe to filter messages
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Toggles dragger visibility and sets initial position to center of earth
        void setFocus(bool value);

        //@{
        /** Set/Get drag stage of the element */
        void setDragStage(DragStage);
        DragStage getDragStage() const;
        //@}

        void initializeAttributes();

        //@{
        /** Set/Get Dragger Size in Screen */
        void setDraggerSize(const double scale);
        double getDraggerSize() const;
        //@}

        //@{
        /** Is Dragger Fixed In Screen*/
        void setFixedInScreen(const bool scale);
        bool getFixedInScreen() const;
        //@}

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn void sendDraggerUpdatedMessage()
        //!
        //! \brief Convenience function to DraggerUpdatedMessageType
        //!
        ///////////////////////////////////////////////////////////////////////////////////
        void sendDraggerUpdatedMessage();

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn double getElevationAt(osg::Vec2d position)
        //! 
        //! \brief Convenience function to get Elevation at given position
        //! 
        //! \param position LatLong at which elevation is needed
        //! 
        //!  \return elevation at position
        //! 
        ///////////////////////////////////////////////////////////////////////////////////
        double getElevationAt(osg::Vec2d position);

        ///////////////////////////////////////////////////////////////////////////////////
        //! 
        //! \fn osg::Camera* getCamera()
        //! 
        //!  \brief Convenience function to get Elevation at given position
        //!         Used only if CustomScaleDragger does scaling in ScreenCoordinates
        //! 
        //!  \see USE_SCREEN_COORD_FOR_SCALING in CustomDragger.cpp
        //! 
        //!  \param position LatLong at which elevation is needed
        //! 
        //!  \return osg::Camera associated to current view
        //! 
        ///////////////////////////////////////////////////////////////////////////////////
        osg::Camera* getCamera();

        ///////////////////////////////////////////////////////////////////////////////////
        //! 
        //!  \fn void setModel(CORE::RefPtr<ELEMENTS::IModelFeatureObject> object)
        //! 
        //!  \brief convenience funtion that takes position rotation and scale of the object 
        //!         and initializes the dragger accordingly
        //!         overloaded virtual function of IDraggerUIHandler
        //! 
        //!  \param object Currently selected modelFeatureObject
        //! 
        ///////////////////////////////////////////////////////////////////////////////////
        void setModel(CORE::RefPtr<ELEMENTS::IModelHolder> object);

    protected:

        ///////////////////////////////////////////////////////////////////////////////////
        //! 
        //! \brief Default Destructor
        //! 
        ///////////////////////////////////////////////////////////////////////////////////
        ~DraggerUIHandler(){}

    protected:

        ///////////////////////////////////////////////////////////////////////////////////
        //! 
        //! \var _draggerContainer Composit Dragger that manages all other draggers and dragger switching
        //! 
        ///////////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<SMCUI::DraggerContainer> _draggerContainer;

        //!Fixed Size of dragger in screen
        double _size;

        //!Is Fixed in screen
        bool _fixedInScreen;

        //! Drag Stage of the dragger
        IDraggerUIHandler::DragStage _dragStage;


    };
} // namespace SMCUI

#endif  // SMCUI_DRAGGERUIHANDLER_H_
