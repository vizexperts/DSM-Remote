#ifndef VIZUI_ISQLQUERYUIHANDLER_H_
#define VIZUI_ISQLQUERYUIHANDLER_H_

/*****************************************************************************
 *
 * File             : ISQLQueryUIHandler.h
 * Version          : 1.1
 * Module           : Core
 * Description      : ISQLQueryUIHandler interface declaration
 * Author           : Mohan Singh
 * Author email     : mohan@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2013-2014, CAIR, DRDO                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 */

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Vec4d>

namespace CORE
{
    class IFeatureLayer;
    class ISQLQueryResult;
}

namespace SMCUI
{

    class ISQLQueryUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, CORE, ISQLQueryUIHandler);

        public:

            //! run SQL query on the feature layer
            virtual CORE::ISQLQueryResult* executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& whereClause) = 0;
            virtual CORE::ISQLQueryResult* executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& fieldNames, 
                                                                const std::string& whereClause)= 0;
            virtual CORE::ISQLQueryResult* executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& fieldNames, 
                                                    const std::string& whereClause, const osg::Vec4d& extents)= 0;
            virtual CORE::ISQLQueryResult* executeSQLQuery2(CORE::IFeatureLayer* layer, const std::string& query1,
                                                    const std::string& query2)=0;                                                
            virtual CORE::ISQLQueryResult* executeSQLQuery2(CORE::IFeatureLayer* layer, const std::string& query)=0;                                                

            //! get the current SQL query result
            virtual CORE::ISQLQueryResult* getSQLQueryResult() const = 0;

            //! clear the current SQL query result
            virtual void clearSQLQueryResult() = 0;

            //! save SQL query result
            virtual void saveSQLQueryResult() = 0;
    };
}

#endif // VIZUI_ISQLQUERYUIHANDLER_H_

