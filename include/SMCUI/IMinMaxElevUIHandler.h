#pragma once

/******************************************************************************
* File              : IMinMaxElevUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Min Max Elevation GUI Handler
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2010-2011 CAIR, DRDO
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>

#include <GISCompute/IMinMaxElevCalcVisitor.h>

namespace SMCUI 
{
    //! Minimum-maximum elevation UIHandler
    class IMinMaxElevUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IMinMaxElevUIHandler);
        public:

            // Messages
            static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> MinMaxCalculatedMessageType;

            //@{
            /** Set the extents of the area */
            virtual void setExtents(osg::Vec4d extents) = 0;
            virtual osg::Vec4d getExtents() const = 0;
            //@}

            //@{
            //! Returns the minimum and maximum elevation values
            virtual osg::Vec3d getMinimumElevationPoint() const = 0;
            virtual osg::Vec3d getMaximumElevationPoint() const = 0;
            //@}

            //! Computes the min-max elevation
            virtual void computeMinMaxElevation(bool multiThreaded = true) = 0;

            //! stop computing min-max points
            virtual void stopFilter() = 0;
    };

} // namespace SMCUI


