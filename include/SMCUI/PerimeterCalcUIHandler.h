#ifndef SMCUI_PERIMETERCALCVISITOR_H_
#define SMCUI_PERIMETERCALCVISITOR_H_

/*****************************************************************************
* File              : PerimeterCalcUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for perimeter filter
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <GISCompute/export.h>
#include <osg/Array>
#include <GISCompute/IPerimeterCalcVisitor.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/IPerimeterCalcUIHandler.h>

namespace SMCUI
{
    //! interface used for calculating perimeter of a polygon
    class PerimeterCalcUIHandler : public IPerimeterCalcUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        PerimeterCalcUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Sets/gets the points of the polygons to be processed */
        void setPoints(osg::Vec3dArray* points);
        osg::Vec3dArray* getPoints();
        //@}

        //@{
        /** Sets/gets the type of perimeter to compute */
        void setComputationType(ComputationType type);
        ComputationType getComputationType();
        //@}

        //! Returns the value of the perimeter in sq km
        double getPerimeter();

        //! Resets values of the visitor
        void setFocus(bool flag);

        //! Subscribe to filter messages
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Executes the filter
        void execute();

        //! Stops the execution of the filter
        void stop();

    protected:

        ~PerimeterCalcUIHandler();

    protected:

        //! IPerimeterCalcVisitor 
       
        CORE::RefPtr<GISCOMPUTE::IPerimeterCalcVisitor> _visitor;


    };
} // namespace SMCUI

#endif  // SMCUI_PERIMETERCALCVISITOR_H_
