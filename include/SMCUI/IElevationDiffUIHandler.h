#ifndef SMCUI_IELEVATIONDIFFUIHANDLER_H_
#define SMCUI_IELEVATIONDIFFUIHANDLER_H_

/*****************************************************************************
* File              : IElevationDiffUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Elevation Difference calculation
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <Core/IPoint.h>

namespace SMCUI
{
    //! UIHandler for Elevation Difference ui handler
    class IElevationDiffUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IElevationDiffUIHandler);

    public:

        // Difference Calculated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> ElevationDiffCalculationCompletedMessageType;

    public:

         //@{
        /** Set/get point1 of the line to be processed */
        virtual void setPoint1(CORE::IPoint* point1)=0;
        virtual CORE::IPoint* getPoint1()=0;
        //@}

        //@{
        /** Set/get point1 of the line to be processed */
        virtual void setPoint2(CORE::IPoint* point2)=0;
        virtual CORE::IPoint* getPoint2()=0;
        //@}
        //reset Member Variable
        virtual void reset()=0;
        //! Executes the filter
        virtual void execute() = 0;

        //returns the elevation difference value as string
        virtual std::string getEleDiffText()=0;

    };
} // namespace SMCUI

#endif  // SMCUI_IELEVATIONDIFFUIHANDLER_H_
