#pragma once

/******************************************************************************
* File              : ITacticalSymbolsUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : UIHandler Interface for Creating and editing Tactical Symbols
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright (c) 2012-2012 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <Core/IObject.h>
#include <osg/Vec4>
#include <osg/Vec3>

#include <TacticalSymbols/ITacticalSymbol.h>

namespace SMCUI
{
    class ITacticalSymbolsUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ITacticalSymbolsUIHandler);

    public:
        enum Mode
        {
            MODE_NONE,
            MODE_CREATE_TACTICALSYMBOL,
            MODE_EDIT,
            MODE_NO_OPERATION
        };

        virtual bool setMode(Mode mode) = 0;

        virtual Mode getMode() const = 0;

        virtual void setAffiliation(TACTICALSYMBOLS::ITacticalSymbol::Affiliation affiliation) = 0;
        virtual TACTICALSYMBOLS::ITacticalSymbol::Affiliation getAffiliation()const = 0;

        virtual void setPlanStatus(TACTICALSYMBOLS::ITacticalSymbol::PlanStatus status) = 0;
        virtual TACTICALSYMBOLS::ITacticalSymbol::PlanStatus getPlanStatus()const = 0;

        //! Set Current symbol name
        virtual void setSymbolName(const std::string& name) = 0;

        //! Get Current symbol name
        virtual std::string getSymbolName() const = 0;

        //! Returns the current line instance
        virtual TACTICALSYMBOLS::ITacticalSymbol* getCurrentSymbol() const = 0;

        //! set the current selected line as current line
        virtual void setSelectedSymbolAsCurrentSymbol() = 0;

        //! Set SVG file path
        virtual void setSymbolPath(const std::string& filepath) = 0;

        //! reset UIHandler to add a new line
        virtual void _reset() = 0;

        //! setProcessMouse events
        virtual void _setProcessMouseEvents(bool value) = 0;

        // get process mouse events
        virtual bool _getProcessMouseEvents() const = 0;
    };

} // namespace SMCUI

