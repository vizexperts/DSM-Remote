#pragma once

/******************************************************************************
* File              : PointUIHandler2.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for UI handler of Point
* Author            : Sumit Pandey
* Author email      : sumitp@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <SMCUI/IPointUIHandler2.h>
#include <VizUI/UIHandler.h>
#include <osgUtil/LineSegmentIntersector>
#include <osg/Geode>
#include <osg/Geometry>
#include <Core/IPoint.h>
#include <Core/ILine.h>
#include <Core/Intersections.h>
#include <Elements/ISettingComponent.h>



namespace CORE
{
    class IMetadataRecord;
    class IFeatureLayer;
}

namespace SMCUI 
{
    //! Handles GUI inputs for creating Points
    class SMCUI_DLL_EXPORT PointUIHandler2 : public IPointUIHandler2, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        PointUIHandler2();

        //@{
        /** Sets/Gets Point */
        virtual void setPoint(CORE::IPoint* Point);
        virtual CORE::IPoint* getPoint() const;
        //@}

        //@{
        /** Set/Get offsets */
        virtual void setOffsetFromConfig(double offset);
        virtual double getOffsetFromConfig() const;
        //@}

        //! On gaining focus, create a new Point
        virtual void setFocus(bool value);

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        CORE::RefPtr<CORE::IFeatureLayer>  getOrCreatePointLayer();

        //! Function called when added to UI Manager
        void onAddedToManager();

        void initializeAttributes();

        //! set the current mode
        void setMode(IPointUIHandler2::PointMode mode);

        //! get the current mode
        IPointUIHandler2::PointMode getMode();

        //! set the current selected line as current line
        void setSelectedPointAsCurrentPoint();

        //! set the metadata record to the current line
        void setMetadataRecord(CORE::IMetadataRecord* record);

        //! get the metadata record of the current line
        CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const;

        //! update the current line
        void update();

        void addToLayer(CORE::IFeatureLayer *featureLayer);

        //! add point to current selected layer
        void addPointToCurrentSelectedLayer();

        //! get current selected layer definition
        CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn();

        void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions)const;

        //! remove created point
        void removeCreatedPoint();

        //! Set Clamping from UIHandler config
        void setClampingStateFromConfig(bool clamp);

        //! Get Clamping from UIHandler Config
        bool getClampingStateFromConfig() const;

        //! set temporary state for the point
        void setTemporaryState(bool state);

        //! get temporary state for the point
        bool getTemporaryState() const;

        void create(bool addToWorld=true);
        //void del();

        //! Resets the handler
        void reset();

        void PointUIHandler2::getAssociatedFilePaths(std::vector<std::string> &associatedfiles, std::string fieldName); 

        void getAssociatedImages(std::vector<std::string> &images);

//        void setAssociatedImages(const std::vector<std::string>& imageVector, const std::string& attributeToSet);

        void addAssociatedImage(const std::string& image/*, const std::string& attributeToSet*/);

        std::string getAssociatedVideoOrAudio(const std::string& attributeToSet);

        void setAssociatedVideoOrAudio(const std::string& file, const std::string& attributeToSet);

        void setIcon(const std::string& iconFile);

        // setter getter ti check if the Point is been selected or Label
        void setPointEnable(const bool val);

        bool getPointEnable() const;

        // Getter and setter function for associated text with point.        
        void setAssociatedText(const std::string& textInfo);
        const std::string getAssociatedText();

        std::string jsonFile;

    protected:

        void _addToLayer(CORE::IFeatureLayer *featureLayer);

        void _createUndoMoveTransaction(const osg::Vec3d& longLatAlt);

        void _performHardReset();

        ~PointUIHandler2();

        void _handleButtonReleasedIntersection(int button, const osg::Vec3d& longLatAlt);

        //internal function for setting default clamping state through "Setting Component"
        void _setDefaultClamping();

        //internal function for setting default offset value through "Setting Component"
        void _setDefaultOffset();

        void _setValueToAttribute(const std::string& attribute, const std::string& textValue);

        void _copyAttribute(CORE::IMetadataRecord* record);

    private:

        //! point
        CORE::RefPtr<CORE::IPoint> _point;

        //! Offset for the point
        double _offset;

        //! mode
        IPointUIHandler2::PointMode _mode;

        //! temporary state
        bool _temporary;

        //! clamping state
        bool _clamping;

        //! whether clamp attribute is present in config or not
        bool _clampingFromConfig;

        //! whether offset attribute is present in config or not
        bool _offsetFromConfig;

        CORE::RefPtr<ELEMENTS::ISettingComponent> _settingComponent;

        //bool _dragged;

        bool _handleButtonRelease;

        // flag to know if its Point is enable or not
        bool _pointEnable;
    };

} // namespace SMCUI
