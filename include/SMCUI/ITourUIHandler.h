#ifndef SMCUI_ITOURUIHANDLER_H_
#define SMCUI_ITOURUIHANDLER_H_

/*****************************************************************************
*
* File             : ITourUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : ITourUIHandler class
* Author           : Abhinav Goyal
* Author email     : abhinav@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013-2014, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/RefPtr.h>

#include <SMCUI/export.h>

#include <SMCElements/ITourComponent.h>

namespace SMCUI 
{
    class ITourUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ITourUIHandler);

    public:

        //! Reset the tour recording
        virtual bool startTourRecording() = 0;

        //! stops the tour recording (User can save after stop)
        virtual bool stopTourRecording() = 0;

        //! pause the tour recording
        virtual bool pauseTourRecording() = 0;

        //! for saving the current recorded tour
        //! tour Name is the unique name for the tour(It is not fileName)
        virtual bool saveCurrentRecordedTour(const std::string &tourName, bool overwrite=false) = 0;

        //! Save a tour sequence 
        virtual void saveSequence(const std::string& sequenceName, const std::vector<std::string>& sequence) = 0;

        virtual void deleteSequence(const std::string& sequencename) = 0;

        virtual void playSequence(const std::string& sequenceName) = 0;

        virtual void playNextTourInCurrentSequence() = 0;

        virtual void playPreviousTourInCurrentSequence() = 0;

        virtual void setSequenceName(const std::string& oldSequenceName, const std::string& newSequenceName) = 0;

        //! for deleting the saved tour
        virtual bool deleteTour(const std::string& tourName) = 0;

        // for playing the tour
        virtual bool playTour(const std::string &tourName) = 0;

        // Pause current playing tour
        virtual bool pauseTour() = 0;

        // Stop the current playing tour
        virtual bool stopTour() = 0;

        // get the duration of current loaded tour of tour component
        virtual unsigned int getTourDuration() = 0;

        // set the tour duration(To provide seek functionality while tour is playing)
        virtual void setTourCurrTime(unsigned int currTime) = 0;

        //get the state of the tour
        virtual SMCElements::ITourComponent::TourMode getTourState() const = 0;

        //get the state of the tour in case it is in playMode
        virtual SMCElements::ITourComponent::PlayerState getPlayerState() const = 0;

        virtual SMCElements::ITourComponent::RecorderState getRecordState() const = 0;

        virtual const SMCElements::ITourComponent::MapTourToFile& getTourMap() const = 0;

        //! give list of all available sequence
        virtual const SMCElements::ITourComponent::MapSequenceToTourList &getSequenceList() const = 0;

        virtual std::string getCurrentTourName() const = 0;

        virtual std::string getCurrentSequenceName() const = 0;

        virtual bool setTourName( std::string oldTourName, std::string newTourName ) = 0;

        virtual unsigned int getTourCurrentTime() const = 0;

        //! get the speed factor
        virtual float getSpeedFactor() const = 0;

        //! set the speed factor
        virtual void setSpeedFactor(float speedFactor) = 0;

        //! double the current speed factor
        virtual void increaseSpeedFactor() = 0;

        //! half the current speed factor
        virtual void decreaseSpeedFactor() = 0;

        //! set the recorder type
        virtual void setRecorderType(SMCElements::ITourComponent::RecorderType type) = 0;

        //! get the recorder type
        virtual SMCElements::ITourComponent::RecorderType getRecorderType() const = 0;

        //! record a frame while in User Keyframe mode
        virtual void recordKeyframe() = 0;

        virtual void reset() = 0;

        virtual void resetAll() = 0;

        //check point related functions
        virtual void createNewCheckPoint(unsigned int duration)=0;
        virtual void editCheckPoint(unsigned int oldDuration, unsigned int newDuration, double newSpeed)=0;
        virtual void deleteCheckPoint(unsigned int duration)=0;
        virtual void previewAtCheckPoint(unsigned int duration) = 0; 
        virtual void resetSpeedProfile()=0;
        virtual void saveSpeedProfile() = 0;
        virtual SMCElements::ITourComponent::MapTimeToSpeed getCheckPoints()=0;

    };
} // namespace SMCUI

#endif // SMCUI_IPOINTUIHANDLER_H_
