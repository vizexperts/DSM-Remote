#ifndef SMCUI_IADDCONTENTSTATUSMESSAGE_H
#define SMCUI_IADDCONTENTSTATUSMESSAGE_H

/******************************************************************************
 * File              : IAddContentStatusMessage.h
 * Version           : 1.0
 * Module            : SMCUI
 * Description       : IAddContentStatusMessage class declaration
 * Author            : Gaurav Garg
 * Author email      : gaurav@vizexperts.com
 * Reference         : 
 * Inspected by      : 
 * Inspection date   :  
 
 *******************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *******************************************************************************
 *
 * Revision log (latest on the top): 
 *
 ******************************************************************************/

#include <SMCUI/export.h>
#include <Core/CoreMessage.h>
#include <Core/IMessageType.h>

namespace SMCUI
{
    //! IFilter Status message type
    class IAddContentStatusMessage : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAddContentStatusMessage);
        public:

            // Add Content message types
            static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> AddContentStatusMessageType;

            enum AddContentStatus
            {
                IDLE,
                SUCCESS,
                FAILURE,
                PENDING,
                PAUSED,
                STOPPED,
                STOP_INITIATED,
                SKIPPED_FILES
            };

        public:

            //@{
            /** Sets/Gets the add content status */
            virtual void setStatus(AddContentStatus status) = 0;
            virtual AddContentStatus getStatus() const = 0;
            //@}

            virtual void setProgress(unsigned int progress) = 0;
            virtual unsigned int getProgress() = 0;

            virtual void setSkippedFileList(std::vector<std::string>& skippedFiles) = 0;
            virtual const std::vector<std::string>& getSkippedFileList() const = 0;
            virtual std::vector<std::string>& getSkippedFileList() = 0;

        protected:

            ~IAddContentStatusMessage(){}
    };
}

#endif // SMCUI_IADDCONTENTSTATUSMESSAGE_H
