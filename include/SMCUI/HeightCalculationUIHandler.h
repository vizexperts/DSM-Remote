#pragma once
/*****************************************************************************
* File              : HeightCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : HeightCalculationUIHandler class 
* Author            : Venkata Balakrushna Beesetti
* Author email      : balakrishna@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <SMCUI/IHeightCalculationUIHandler.h>
#include <Core/IReferenced.h>
#include <GISCompute/export.h>
#include <VizUI/UIHandler.h>
#include <Elements/IObjectDragger.h>
#include <string>
#include <osgManipulator/Translate2DDragger>
#include <osg/NodeCallback>
#include <iostream>
#include <Core/IPoint.h>
#include <Util/CoordinateConversionUtils.h>

namespace SMCUI
{
   
    class heightManipulator : public osg::Group
    {
    public:
        heightManipulator() : _draggerSize(240.0f), _active(true) {}
        
        heightManipulator( const heightManipulator& copy, const osg::CopyOp& copyop=osg::CopyOp::SHALLOW_COPY )
        :   osg::Group(copy, copyop),
            _dragger(copy._dragger), _draggerSize(copy._draggerSize), _active(copy._active)
        {}
        
        META_Node( osgManipulator, heightManipulator );
        
        void setDragger( osgManipulator::Dragger* dragger )
        {
            _dragger = dragger;
            if ( !containsNode(dragger) ) addChild( dragger );
        }
        
        osgManipulator::Dragger* getDragger() { return _dragger.get(); }
        const osgManipulator::Dragger* getDragger() const { return _dragger.get(); }
        
        void setDraggerSize( float size ) { _draggerSize = size; }
        float getDraggerSize() const { return _draggerSize; }
        
        void setActive( bool b ) { _active = b; }
        bool getActive() const { return _active; }
        
        void traverse( osg::NodeVisitor& nv )
        {
            if ( _dragger.valid() )
            {
                if ( _active && nv.getVisitorType()==osg::NodeVisitor::CULL_VISITOR )
                {
                    osgUtil::CullVisitor* cv = static_cast<osgUtil::CullVisitor*>(&nv);
                    
                    float pixelSize = cv->pixelSize(_dragger->getBound().center(), 0.48f);
                    if ( pixelSize!=_draggerSize )
                    {
                        float pixelScale = pixelSize>0.0f ? _draggerSize/pixelSize : 1.0f;
                        osg::Vec3d scaleFactor(pixelScale, pixelScale, pixelScale);
                        
                        osg::Vec3 trans, scale;
                        osg::Quat rot, so;
                        _dragger->getMatrix().decompose(trans, rot, scale, so);
                        _dragger->setMatrix(osg::Matrix::rotate(rot) * osg::Matrix::scale(scaleFactor) 
                            * osg::Matrix::translate(trans));

                        //_dragger->setMatrix( osg::Matrix::scale(scaleFactor) * osg::Matrix::translate(trans) );
                    }
                }
            }
            osg::Group::traverse(nv);
        }
        
    protected:
        osg::ref_ptr<osgManipulator::Dragger> _dragger;
        float _draggerSize;
        bool _active;
    };

    //! Interface used for calculating Height calculationUIHandler
    class HeightCalculationUIHandler : 
        public SMCUI::IHeightCalculationUIHandler, 
        public VizUI::UIHandler
    {
        //! Update callback for updating the UIHandler's position
        class PointUpdateCallback : public osgManipulator::DraggerCallback
        {
            public:

                PointUpdateCallback(HeightCalculationUIHandler* hcUIHandler)
                    : _hcUIHandler(hcUIHandler)
                {}

                bool receive(const osgManipulator::MotionCommand& command)
                {
                    // Get latest dragger position
                    _hcUIHandler->updateDraggerPosition();
                    switch(command.getStage())
                    {
                        case osgManipulator::MotionCommand::START:
                            _hcUIHandler->setDragStage(IHeightCalculationUIHandler::START);
                            break;
                        case osgManipulator::MotionCommand::MOVE:
                            _hcUIHandler->setDragStage(IHeightCalculationUIHandler::MOVE);
                            break;
                        case osgManipulator::MotionCommand::FINISH:
                            _hcUIHandler->setDragStage(IHeightCalculationUIHandler::FINISH);
                            break;
                    }
                    _hcUIHandler->sendDraggerPositionUpdatedMessage();
                    return true;
                }

            protected:

                ~PointUpdateCallback()
                {
                }

                //! Height Calculation UIHandler
                HeightCalculationUIHandler* _hcUIHandler;
        };

        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        public:

            HeightCalculationUIHandler();

            //@{
            /**  Set/Get position of the element */
            void setPosition(const osg::Vec3d& position);
            const osg::Vec3d& getPosition() const;
            //@}

            //@{
            /** Set/Get Dragger Name */
            void setDraggerName(const std::string& draggerName);
            std::string getDraggerName() const;
            //@}

            //! Sends DraggerPositionUpdated message to all observers
            void sendDraggerPositionUpdatedMessage();

            //! Updates dragger position by reading dragger parameters
            void updateDraggerPosition();

            //! Toggles dragger visibility and sets initial position to center of earth
            void setFocus(bool value);

            //@{
            /** Set/Get drag stage of the element */
            void setDragStage(DragStage);
            DragStage getDragStage() const;
            //@}

            void initializeAttributes();

            //@{
            /** Set/Get drag stage of the element */
            void setScale(double);
            double getScale() const;
            //@}

        protected:

            ~HeightCalculationUIHandler();

        protected:

            //! Dragger transform
            osg::ref_ptr<osg::MatrixTransform> _selection;

            //! Add a dragger
            osg::ref_ptr<osgManipulator::Translate2DDragger> _pointDragger;

            //! Point update callback
            osg::ref_ptr<PointUpdateCallback> _pointUpdateCallback;

            //! OSG position
            osg::Vec3d _geoPos;

            //! Scale of the dragger
            double _scale;

            //! Drag Stage of the dragger
            IHeightCalculationUIHandler::DragStage _dragStage;

            CORE::RefPtr<heightManipulator> _heightDragger;
    };
} // namespace SMCUI

