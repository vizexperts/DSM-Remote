#pragma once

/******************************************************************************
* File              : IGridDisplayUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for GUI Fence Handler
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <osg/Node>
#include <DB/ReaderWriter.h>
namespace SMCUI 
{
    class IGridDisplayUIHandler: public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IGridDisplayUIHandler);

    public:

        //@{
        virtual void setGridDisplay(bool value) = 0;
        virtual bool getGridDisplay() const = 0;
        //@}
        //@{ set/get function for diplay Indian Grid
        virtual void setIndianGridDisplay(bool value)=0;
        virtual bool getIndianGridDisplay() const=0;
        //@}

    };
} // namespace SMCUI
