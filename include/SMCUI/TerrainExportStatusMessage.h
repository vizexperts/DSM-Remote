#ifndef SMCUI_TERRAINEXPORTSTATUSMESSAGE_H
#define SMCUI_TERRAINEXPORTSTATUSMESSAGE_H

/******************************************************************************
 * File              : TerrainExportStatusMessage
 * Version           : 1.0
 * Module            : SMCUI
 * Description       : TerrainExportStatusMessage class declaration
 * Author            : Kamal Grover
 * Author email      : kamal@vizexperts.com
 * Reference         : 
 * Inspected by      : 
 * Inspection date   :  
 
 *******************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *******************************************************************************
 *
 * Revision log (latest on the top): 
 *
 ******************************************************************************/

#include <SMCUI/export.h>
#include <Core/CoreMessage.h>
#include <SMCUI/ITerrainExportStatusMessage.h>

namespace SMCUI
{
    //! Filter Status message type
    class TerrainExportStatusMessage
        : public CORE::CoreMessage
        , public SMCUI::ITerrainExportStatusMessage
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        public:

            TerrainExportStatusMessage();

            //@{
            /** Sets/Gets the filter status */
            void setStatus(LayerStatus status);
            LayerStatus getStatus() const;
            //@}

            void setProgress(unsigned int progress);
            unsigned int getProgress();

             void setLayerName(std::string layername) ;
             std::string getLayerName() ;

        protected:

            ~TerrainExportStatusMessage();

        protected:

            //! Filter status
            LayerStatus _status;

            //! progress
            unsigned int _progress;

            std::string _layername;
    };
}

#endif // SMCUI_TERRAINEXPORTSTATUSMESSAGE_H
