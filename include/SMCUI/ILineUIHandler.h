#pragma once

/******************************************************************************
* File              : ILineUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for GUI Line Handler
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2012 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Vec4>
#include <osg/Vec3>

namespace CORE
{
    class ILine;
    class IMetadataRecord;
    class IMetadataTableDefn;
    class IFeatureLayer;
}

namespace SMCUI 
{
    class ILineUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ILineUIHandler);

    public:

        //! Line updated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> LineUpdatedMessageType;

        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> LineCreatedMessageType;

        // XXX - Default mode of Line Handler is add point
        enum LineMode
        {
            LINE_MODE_NONE,
            LINE_MODE_CREATE_LINE,
            LINE_MODE_NO_OPERATION,
            LINE_MODE_EDIT
        };

        virtual bool setMode(LineMode mode) = 0;

        virtual LineMode getMode() const = 0;

        //! Set Color of Current Line
        virtual void setColor(osg::Vec4 color) = 0;

        //! get Color of Current Line
        virtual osg::Vec4  getColor() const = 0;

        //! Set Width of Current Line
        virtual void setWidth(double width) = 0;

        //! Get Width of Current Line
        virtual double getWidth() const = 0;

        virtual std::vector<int>& getSelectedVertexList() = 0;
        virtual void setSelectedVertexList( std::vector<int> _selectedPointList ) = 0;

        //! Returns the current line instance
        virtual CORE::ILine* getCurrentLine() const = 0;

        //! set the current selected line as current line
        virtual void setSelectedLineAsCurrentLine() = 0;

        //! set the metadata record to the current line
        virtual void setMetadataRecord(CORE::IMetadataRecord* record) = 0;

        virtual void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions)const = 0;

        //! get the metadata record of the current line
        virtual CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const = 0;

        //! update the current line
        virtual void update() = 0;

        virtual void addToLayer(CORE::IFeatureLayer *featureLayer) = 0;

        //! Adds the line to the default layer created by handler itself
        virtual void addLineToDefaultLayer() = 0;

        //! add line to current selected layer
        virtual void addLineToCurrentSelectedLayer() = 0;

        //! get current selected layer definition
        virtual CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn() = 0;

        //! remove the created line
        virtual void removeCreatedLine() = 0;

        //! delete the last point from the line
        virtual void deleteLastPointFromLine() = 0;

        //! set temporary state for the line
        virtual void setTemporaryState(bool state) = 0;

        //! get temporary state for the line
        virtual bool getTemporaryState() const = 0;

        //! set clamping state for the point
        virtual void setClampingState(bool state) = 0;

        //! get clamping state for the point
        virtual bool getClampingState() const = 0;

        //! set a line as the current line
        virtual void setCurrentLine(CORE::ILine* line) = 0;

        //! reset UIHandler to add a new line
        virtual void _reset() = 0;

        //! delete current selected line
        virtual void _deleteCurrentSelectedLine() = 0;

        //! delete Point at index
        virtual bool _deletePointAtIndex(unsigned int index) = 0;

        //! move point at index
        virtual void _movePointAtIndex(unsigned int index, osg::Vec3d position) = 0;

        //! setProcessMouse events
        virtual void _setProcessMouseEvents(bool value) = 0;

        // get process mouse events
        virtual bool _getProcessMouseEvents() const =0;
    };

} // namespace SMCUI

