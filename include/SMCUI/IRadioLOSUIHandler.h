#ifndef SMCUI_IRADIOLOSUIHANDLER_H_
#define SMCUI_IRADIOLOSUIHANDLER_H_

/******************************************************************************
* File              : IRadioLOSUIHandler
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Radio LOS UI Handler
* Author            : Kumar Saurabh Arora
* Author email      : saurabh@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>

namespace CORE
{
    class IObject;
}

namespace SMCUI 
{
    class IRadioLOSUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IRadioLOSUIHandler);

        public:

            //! set Transmitter Position
            virtual void setTransmitterPosition(const osg::Vec3d& point) = 0;

            //! set Receiver Position
            virtual void setReceiverPosition(const osg::Vec3d& point) = 0;

            //! set Transmitter and Receiver Height
            virtual void setTransmitterHeight(double transHeight) = 0;
            virtual void setReceiverHeight(double recvHeight) = 0;

            //! set Transmitter Icon object
            virtual void setTrasmitterIcon(CORE::IObject* transObj) = 0;

            //! set Receiver Icon object
            virtual void setReceiverIcon(CORE::IObject* recvObj) = 0;

            //! set wave frequency in MHz
            virtual void setWaveFrequency(double frequency) = 0;

            //! set effective earth radius factor
            virtual void setEffectiveEarthRadiusFactor(double kFactor) = 0;

            //! set output name
            virtual void setOutputName(std::string& name) = 0;

            //! set Range Step
            virtual void setRangeStep(double rangeStep) = 0;

            //! set Tx Power in dB
            virtual void setTransmitterPower(double txPower) = 0;

            //! set Tx/Rx Antenna Gain in dB
            virtual void setAntennaGain(double txGain, double rxGain) = 0;

            //! set receiver senstivity in dB
            virtual void setReceiverSensitivity(double rxSensitivity) = 0;

            //! set cable loss in dB
            virtual void setCableLoss(double cableLoss) = 0;

            //! get outputs
            virtual bool losClearance() = 0;
            virtual double getOutputReceiverPower() = 0;
            virtual double getOutputSOM() = 0;
            virtual double getOutputFSL() = 0;
            virtual double getOutputEIRP() = 0;
            virtual std::string& getDecisionSupport() = 0;

            //! execute Filter
            virtual void execute(bool multiThreaded = true) = 0;

            //! stop Filter
            virtual void stopFilter() = 0;


    };

} // namespace SMCUI
#endif // SMCUI_IRADIOLOSUIHANDLER_H_

