#pragma once

/******************************************************************************
* File              : AlmanacUIHandler.h
* Version           : 1.0
* Module            : UI
* Description       : AlmanacUIHandler class 
* Author            : Rahul Srivastava
* Author email      : rahul@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <VizUI/UIHandler.h>

#include <SMCUI/IAlmanacUIHandler.h>

#include <Util/MoonPhaseCalculator.h>
#include <Util/SunRiseSetCalculator.h>

namespace SMCUI
{
    // XXX - Make this thread-safe
    //! Implements the IAlmanacUIHandler interface
    class AlmanacUIHandler : public IAlmanacUIHandler, 
                             public VizUI:: UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
       
        public:
            
            AlmanacUIHandler();

            //! Sets the position at which almanac needs to be calculated
            void setPosition(const osg::Vec3& longlatAlt);

            //! Gets the position at which almanac needs to be calculated
            const osg::Vec3 getPosition() const;

            //! Sets the date and time at which almanac needs to be calculated
            void setDateTime(const boost::posix_time::ptime& stime);

            //! Gets the date and time at which almanac needs to be calculated
            const boost::posix_time::ptime& getDateTime() const;

            //! Computes the almanac parameters
            void computeAlmanac();

            //! Returns the alamanc parameters
            const AlmanacData& getAlmanacData() const;

            //! Set/Get GMT Time
            void setGMTTime(const double& gmtTime);

            double getGMTTime();

        protected:

            ~AlmanacUIHandler();

            void
            _populateAlmanacData(const UTIL::MoonPhaseCalculator::MoonData* const,
                                 const UTIL::SunData&);

        protected:

            //! Current position at which almanac parameters need to be calculated
            osg::Vec3 _longlatAlt;

            //! Current date and time
            boost::posix_time::ptime _dateTime;

            //! Almanac data
            AlmanacData _almanacData;

            //! MoonPhaseCalculator
            UTIL::MoonPhaseCalculator _mpc;

            //! SunRiseSetCalculator
            UTIL::SunRiseSetCalculator _srsc;

            double _gmtTime;
    };
} // namespace UI
