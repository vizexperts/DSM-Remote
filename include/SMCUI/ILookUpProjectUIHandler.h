#pragma once

/******************************************************************************
* File              : ILookUpProjectUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for UI Line Handler
* Author            : Somnath Singh
* Author email      : somnath@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2012 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <map>
#include <vector>

#include <VizDataUI/IDatabaseUIHandler.h>

namespace SMCUI 
{
    class ILookUpProjectUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ILookUpProjectUIHandler);

    public:

        struct DatabaseCredential
        {
            //std::string connectionName;
            std::string database;
            std::string hostname;
            std::string port;
            std::string username;
            std::string password;
            std::string creatorSchemaName;
            std::string creatortableName;
            std::string plannerSchemaName;
            std::string plannertableName;
            std::string tkSchemaName;
            std::string tkTableName;
            std::string templateSchemaName;
            std::string templateTableName;
        };

        enum ProjectType
        {
            CREATOR,
            PLANNER, 
            TK,
            TEMPLATE
        };

        enum NewOrSaveType
        {
            NEWPROJECT,
            SAVEPROJECT,
            SAVEASPROJECT,
            EXPORTASPLAN
        };

        virtual bool makeConnectionWithDatabase(const SMCUI::ILookUpProjectUIHandler::DatabaseCredential& credential) = 0;

        virtual void closeDatabaseConnection() = 0;

        virtual std::vector<std::pair<std::string, std::string> > getProjectListFromProjectMap(const ProjectType& type) = 0;

        virtual void addOrUpdateProjectName(const std::string& projectName, const std::string& uid, const ILookUpProjectUIHandler::NewOrSaveType& newOrSave,
            const ILookUpProjectUIHandler::ProjectType& type) = 0;

        virtual std::string getNextProjectName(const ILookUpProjectUIHandler::ProjectType& type) = 0 ;

        virtual std::string getProjectNameByUID(const std::string& uid, const ILookUpProjectUIHandler::ProjectType& type) = 0;

        virtual std::string getProjectUIDByName(const std::string& projectName, const ILookUpProjectUIHandler::ProjectType& type) = 0;

        virtual bool isProjectAlreadyPresent(const std::string& projectName, const ILookUpProjectUIHandler::ProjectType& type) = 0;

        virtual void deleteProjectByUID(const std::string& uid, const ILookUpProjectUIHandler::ProjectType& type) = 0;

        virtual void deleteProjectByName(const std::string& projectName, const ILookUpProjectUIHandler::ProjectType& type) = 0;

        // get the list of last four updated projects
        virtual std::vector<std::string> getRecentProjectsList(const ProjectType& type) = 0;

        // get the project list with date created
        //<projectName, createdTimeString>
        virtual std::vector<std::pair<std::string, std::string> > getProjectListWithCreationTime(const ProjectType& type) = 0;
    };

} // namespace SMCUI

