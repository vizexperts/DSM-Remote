#ifndef SMCUI_IANIMATIONUIHANDLER_H_
#define SMCUI_IANIMATIONUIHANDLER_H_

/*****************************************************************************
*
* File             : IAnimationUIHAndler.h
* Version          : 1.1
* Module           : SMCUI
* Description      : IAnimationUIHandler interface declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Elements/ICheckpoint.h>

namespace SMCUI
{
    class IAnimationUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAnimationUIHandler);

    public: 
        virtual void setStartEndDateTime(const boost::posix_time::ptime& startTime, const boost::posix_time::ptime& endTime) = 0;
        virtual void setCurrentDateTime(const boost::posix_time::ptime& currentTime) = 0;

        virtual const boost::posix_time::ptime& getStartDateTime() = 0;
        virtual const boost::posix_time::ptime& getEndDateTime() = 0;
        virtual const boost::posix_time::ptime& getCurrentDateTime() = 0;

        virtual void play() = 0;
        virtual void pause() = 0;
        virtual void stop() = 0;

        virtual double getTimeScale()const = 0;
        virtual void setTimeScale(double newTimeScale) = 0;

        virtual bool isAnimationRunning() const = 0;
    };
}

#endif  // SMCUI_IANIMATIONUIHANDLER_H_