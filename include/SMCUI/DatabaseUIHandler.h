#ifndef TKUI_DATABASEUIHANDLER_H_
#define TKUI_DATABASEUIHANDLER_H_

/*****************************************************************************
 *
 * File             : DatabaseUIHandler.h
 * Version          : 1.0
 * Module           : TKUI
 * Description      : DatabaseUIHandler class
 * Author           : Raghavendra Polinki
 * Author email     : rp@vizexperts.com
 * Reference        : TKUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <SMCUI/IDatabaseUIHandler.h>
#include <SMCUI/export.h>
#include <Core/Referenced.h>
#include <Core/IBaseUtils.h>
#include <Database/IDatabase.h>
#include <Database/IRasterDatabase.h>
#include <Core/IObject.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/IDatabaseProcessingStatusMessage.h>

namespace SMCUI
{
    class SMCUI_DLL_EXPORT DatabaseUIHandler : public SMCUI::IDatabaseUIHandler, public VizUI::UIHandler
    {
        //DECLARE_META_INTERFACE(TKUI_DLL_EXPORT, TKUI, DatabaseUIHandler);
        /// \todo Naresh : Semicolon is not needed 
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

        public:
            //! constructor
            DatabaseUIHandler();

            //! destructor
            ~DatabaseUIHandler();
            
            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \initialize database member
            //!
            ///////////////////////////////////////////////////////////////////////////
            void initialize(const std::string& databaseName, const std::string& portName, const std::string& userName,
                               const std::string& password, const std::string& hostName);

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief upload vector object to database
            //! 
            //! \return true if uploaded successfully
            //!
            ///////////////////////////////////////////////////////////////////////////
            bool uploadVector(std::string table, std::string schema);

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief upload raster object to database
            //! 
            //! \return true if uploaded successfully
            //!
            ///////////////////////////////////////////////////////////////////////////
            bool uploadRaster(std::string table, std::string schema, int SRID, int bandNo, int tileWidth,
                  int tileHeight, int insertType, int overViewFactor, bool createIndex, bool VacumeAnalyze, bool setConstraints, std::string resampling);

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief loading vector from Datbase to TKP
            //! 
            //! \return true if loading is sucessful
            //!
            ///////////////////////////////////////////////////////////////////////////
            bool addVector(std::string schema, std::string table);

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief loading raster from Datbase to TKP
            //! 
            //! \return true if loading is sucessful
            //!
            ///////////////////////////////////////////////////////////////////////////
            bool addRaster(std::string schema, std::string table);

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief Creates an SQL query for the database
            //! 
            //! \return the pointer to the created SQL query
            //!
            ///////////////////////////////////////////////////////////////////////////
            vizDatabase::ISQLQuery* createSQLQuery();

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief tests connection for the database
            //!
            ///////////////////////////////////////////////////////////////////////////
            bool testConnection(std::string values[6]);

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief disconnects the existing connection
            //!
            ///////////////////////////////////////////////////////////////////////////
            bool disconnect();

            /// \todo Naresh : parameter desciption needs to be added. 
            /// \todo Naresh : All getter functions should be const. Getter functuion should not be able to modify data members
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief get the list of schemas available in the database
            //! 
            //! \returns a vector of with all the schemas starting with "tkp_" in the
            //!         database
            //!
            ///////////////////////////////////////////////////////////////////////////
            std::vector<std::string> getSchemaList();

            /// \todo Naresh : parameter desciption needs to be added. 
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief checks for if table name already exits
            //! 
            //! \returns false if table exits else true
            //!
            ///////////////////////////////////////////////////////////////////////////
            bool validateTableName(std::string table, std::string schema);

            /// \todo Naresh : parameter desciption needs to be added.
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief update function is called when the a message is received from 
            //! the observalbles
            //! 
            //!
            //////////////////////////////////////////////////////////////////////////
            void update(const vizCore::IMessageType& messageType, const vizCore::IMessage& message);

            /// \todo Naresh : We should follow same commenting style every where
                        //@{
            /** Set/Get function for UIHandlerManager */
            void setManager(vizApp::IUIHandlerManager* manager);
            vizApp::IUIHandlerManager* getManager() const; 
            //@}

            // \todo Naresh : We should follow same commenting style every where
                        //@{
            //! Function called when added to UI Manager
            void onAddedToManager();
            //! Function called when removed from UI Manager
            void onRemovedFromManager();
            //@}

            // \todo Naresh : parameter desciption needs to be added.
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief function to get the list of vector tables and schemas in the database
            //! Note: Only schema's starting with tkp_ are shown
            //! 
            //!
            //////////////////////////////////////////////////////////////////////////
            void getVectorTablesAndSchemas(std::vector<std::string> &schemaList, std::vector<std::string> &tableList, 
                std::vector<std::string> &geomTypeList, std::vector<std::string> &dataTypeList, std::vector<std::string> &sridList);

            // \todo Naresh : parameter desciption needs to be added.
            ///////////////////////////////////////////////////////////////////////////
            //!
            //! \brief function to get the list of Raster tables and schemas in the database
            //! Note: Only schema's starting with tkp_ are shown
            //! 
            //!
            //////////////////////////////////////////////////////////////////////////
            void getRasterTablesAndSchemas(std::vector<std::string> &schemaList, std::vector<std::string> &tableList, 
                std::vector<std::string> &pixelTypeList, std::vector<std::string> &dataTypeList, std::vector<std::string> &sridList);

        protected:

            //! Uploads raster in a seperate thread
            void _mtUploadRaster(vizCore::IObject *object, vizDatabase::RasterUploadOptions options);

            //! Add Vector in a seperate thread
            void _mtAddVector(std::string schema, std::string table);

            //! Add Vector in a seperate thread
            void _mtAddRaster(std::string schema, std::string table);

            //!
            void _notifyStatusToObservers(SMCUI::IDatabaseProcessingStatusMessage::DatabaseProcessingStatus, unsigned int progress);

            //! Database to which objects to be uplaoded
            vizCore::RefPtr<vizDatabase::IDatabase> _database;

    };
}
#endif