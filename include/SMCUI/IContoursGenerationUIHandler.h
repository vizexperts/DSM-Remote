#ifndef SMCUI_ICONTOURSGENERATIONUIHANDLER_H_
#define SMCUI_ICONTOURSGENERATIONUIHANDLER_H_

/******************************************************************************
* File              : IContoursGenerationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Slope aspect filter UI Handler
* Author            : Gaurav Gar
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <GISCompute/IContourFilterVisitor.h>

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI 
{
    class IContoursGenerationUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IContoursGenerationUIHandler);

    public:

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;
        //@}

        virtual void setElevationObject(TERRAIN::IElevationObject* obj) = 0;

        virtual void setOutputName(std::string& name) = 0;
        //@{
        /** Set the Elevation Interval */

        virtual void setElevationInterval(double ElevationIntVal)=0;
        virtual double getElevationInterval() const=0;
                //@}
        //@{
        /** Set the offset Val */

        virtual void setOffset(double offset)=0;
        virtual double getOffset()const=0;
                        //@}
                /** Set the color Val */

        virtual void setUserDefineColor(osg::Vec4d ocolor)=0;
        virtual osg::Vec4d getUserDefineColor()const=0;
                        //@}

        virtual void execute() = 0;

        virtual void stopFilter() = 0;

        virtual void setStyle(const std::string &value) = 0;
        virtual std::string getStyle() = 0;
    };

} // namespace SMCUI

#endif // SMCUI_ICONTOURSGENERATIONUIHANDLER_H_
