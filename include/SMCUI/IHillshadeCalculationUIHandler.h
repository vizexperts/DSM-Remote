#ifndef SMCUI_IHILLSHADECALCULATIONUIHANDLER_H_
#define SMCUI_IHILLSHADECALCULATIONUIHANDLER_H_

/******************************************************************************
* File              : IHillshadeCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Slope aspect filter UI Handler
* Author            : Gaurav Gar
* Author email      : gaurav@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <GISCompute/IHillshadeFilterVisitor.h>

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI
{
    class IHillshadeCalculationUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IHillshadeCalculationUIHandler);

    public:

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;
        //@}

        virtual void setElevationObject(TERRAIN::IElevationObject* obj) = 0;

        virtual void setOutputName(std::string& name) = 0;
        //@{
        /** Set the zenith angle of the illumination source */

        virtual void setZenithAngle(double angle) = 0;
        virtual double getZenithAngle() const = 0;
        //@}

        //@{
        /**   Set the Azimuth angle of the illumination source */

        virtual void setAzimuthAngle(double angle) = 0;
        virtual double getAzimuthAngle()const = 0;
        //@}

        //@{
        /**   the transparency coefficient */

        virtual void setTransparency(double transparency) = 0;
        virtual double getTransparency()const = 0;
        //@}
        /** Set the color*/
        virtual void setUserDefineColor(osg::Vec4d ocolor) = 0;
        virtual  osg::Vec4d getUserDefineColor() const = 0;
        //@
        virtual void execute() = 0;

        virtual void stopFilter() = 0;
    };

} // namespace SMCUI

#endif // SMCUI_IHILLSHADECALCULATIONUIHANDLER_H_
