#ifndef SMCUI_IMILITARYSYMBOLPOINTUIHANDLER_H_
#define SMCUI_IMILITARYSYMBOLPOINTUIHANDLER_H_

/*****************************************************************************
*
* File             : IMilitarySymbolPointUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : IMilitarySymbolPointUIHandler class
* Author           : Gaurav Garg
* Author email     : gg@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

namespace CORE
{
    class IMetadataRecord;
    class IMetadataTableDefn;
    class IFeatureLayer;
}

namespace ELEMENTS
{
    class IMilitarySymbol;
    class IMilitaryRange;
    class IMilitarySymbolType;
    class IOverlay;
}

namespace SMCUI 
{
    class IMilitarySymbolPointUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IMilitarySymbolPointUIHandler);
    public:

        //! Point updated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> MilitarySymbolPointUpdatedMessageType;

        //! Point created message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> MilitarySymbolPointCreatedMessageType;

        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> MilitarySymbolPointDeletedMessageType;

        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> MilitarySymbolPointSelectedMessageType;
    public:

        enum MilitarySymbolMode
        {
            MILITARY_MODE_NONE,
            MILITARY_MODE_CREATE_UNIT,
            MILITARY_MODE_EDIT_POSITION,
            MILITARY_MODE_EDIT_ATTRIBUTE
        };

        virtual CORE::IFeatureLayer* getOrCreateMilitarySymbolLayer(const std::string &symbolName, bool addToWorld = true) = 0;

        //@{
        /** Sets/Gets Point */
        virtual void setMilitarySymbol(ELEMENTS::IMilitarySymbol* unit) = 0;
        virtual ELEMENTS::IMilitarySymbol* getMilitarySymbol() const = 0;
        //@}

        virtual void setMilitaryRange(ELEMENTS::IMilitaryRange* range) = 0;
        virtual ELEMENTS::IMilitaryRange* getMilitaryRange() const = 0;

        virtual void setMilitarySymbolType(std::string milSymbol) = 0;
        virtual std::string getMilitarySymbolType() const = 0;

        //! set the current mode
        virtual void setMode(MilitarySymbolMode mode) = 0;

        //! get the current mode
        virtual MilitarySymbolMode getMode() = 0;

        //! set the height offset value with respect to terrain
        virtual void setOffsetFromConfig(double offset) =0;
        
        //! get the height offset value with respect to terrain
        virtual double getOffsetFromConfig() const = 0;

        //! Set Clamping from UIHandler config
        virtual void setClampingStateFromConfig(bool clamp) = 0;

        //! Get Clamping from UIHandler Config
        virtual bool getClampingStateFromConfig() const = 0;

        //! set the current selected line as current line
        virtual void setSelectedUnitAsCurrentUnit() = 0;

        //! set the metadata record to the current line
        virtual void setMetadataRecord(CORE::IMetadataRecord* record) = 0;

        //! get the metadata record of the current line
        virtual CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const = 0;

        //! update the current line
        virtual void update() = 0;

        //! add point to current selected layer
        virtual void addUnitToCurrentSelectedLayer() = 0;

        //! get current selected layer definition
        virtual CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn() = 0;

        //! remove the created point
        virtual void removeCreatedUnit() = 0;

        //! set the temporary state
        virtual void setTemporaryState(bool value) = 0;

        //! get the temporary state
        virtual bool getTemporaryState() const = 0;

        //! create the millitary unit
        virtual void createUnit(bool addToWorld=true) = 0;

        //! reset UIHandler*
        virtual void reset() = 0;

        //! helper funtion for GUI to get iconPath for a military symbol type
        virtual std::string getQMLIconPath(CORE::RefPtr<ELEMENTS::IMilitarySymbolType> milSymbolType) const = 0;

        //! Utility function to create military symbol of given type
        virtual CORE::RefPtr<ELEMENTS::IMilitarySymbol> createSymbol(const std::string& type) = 0;

        //! Utility functio to add given symbol to its respective layer
        virtual void addSymbolToLayer(CORE::RefPtr<ELEMENTS::IMilitarySymbol> symbol) = 0;
    };
} // namespace SMCUI

#endif // SMCUI_IMILITARYSYMBOLPOINTUIHANDLER_H_
