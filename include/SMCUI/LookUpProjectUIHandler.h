#pragma once
/******************************************************************************
* File              : LookUpProjectUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Implementation class for UI Line Handler
* Author            : Somnath Singh
* Author email      : somnath@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2012 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/
#include <Core/Base.h>
#include <SMCUI/ILookUpProjectUIHandler.h>
#include <VizUI/UIHandler.h>

#include <Core/WorldMaintainer.h>
#include <Core/IMessageFactory.h>
#include <Elements/ISettingComponent.h>
namespace SMCUI 
{
    class LookUpProjectUIHandler : public SMCUI::ILookUpProjectUIHandler,
        public VizUI::UIHandler
    {

        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    private:


        struct CreatorPlannerSchemaTable
        {
            std::string creatorSchemaName;
            std::string creatortableName;
            std::string plannerSchemaName;
            std::string plannertableName;
            std::string tkSchemaName;
            std::string tkTableName;

            std::string templateSchemaName;
            std::string templateTableName;
        };

        bool _checkConnectionWithDatabase();

        bool _checkForExistingSchema(const std::string& schemaName);

        std::vector<std::pair<std::string, std::string> > _getPresentProjects(const std::string& schemaName, const std::string& tableName);

        void _setDatabaseCredential(const SMCUI::ILookUpProjectUIHandler::DatabaseCredential& credential);

        bool _createProjectSchema(const std::string& schemaName,const std::string& tableName);

        void _getPresentSchemaAndTable(std::string& schemaName, std::string& tableName, const LookUpProjectUIHandler::ProjectType& type);

        std::vector<std::string> _getRecentProjects(const std::string& schemaName, const std::string& tableName);

        std::vector<std::pair<std::string, std::string> > 
            _getPresentProjectsWithCreationTime(const std::string& schemaName, const std::string& tableName);

    public:

        LookUpProjectUIHandler();

        void onAddedToManager();

        bool makeConnectionWithDatabase(const SMCUI::ILookUpProjectUIHandler::DatabaseCredential& credential);

        void closeDatabaseConnection();

        //std::map<std::string, std::string> 
        std::vector<std::pair<std::string, std::string> > getProjectListFromProjectMap(const LookUpProjectUIHandler::ProjectType& type);

        void addOrUpdateProjectName(const std::string& projectName, const std::string& uid, const SMCUI::LookUpProjectUIHandler::NewOrSaveType& newOrSave,
            const LookUpProjectUIHandler::ProjectType& type);

        //Get the next project name that need to be saved in
        // This is done after the connection is made to the Database
        std::string getNextProjectName(const LookUpProjectUIHandler::ProjectType& type);

        std::string getProjectNameByUID(const std::string& uid, const LookUpProjectUIHandler::ProjectType& type);

        std::string getProjectUIDByName(const std::string& projectName, const LookUpProjectUIHandler::ProjectType& type);

        bool isProjectAlreadyPresent(const std::string& projectName, const ILookUpProjectUIHandler::ProjectType& type);

        void deleteProjectByUID(const std::string& uid, const ILookUpProjectUIHandler::ProjectType& type);

        void deleteProjectByName(const std::string& projectName, const ILookUpProjectUIHandler::ProjectType& type);

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        // get the list of last four updated projects
        std::vector<std::string> getRecentProjectsList(const ProjectType& type);

        // get the project list with date created
        //<projectName, createdTimeString>
        std::vector<std::pair<std::string, std::string> > getProjectListWithCreationTime(const ProjectType& type);

    protected:

        ~LookUpProjectUIHandler();

        // Options for new connection
        std::map<std::string, std::string> _dbOptions;

        std::string _tableNameSaveLoad;
        std::string _schemaNameSaveLoad;

        std::string _presentDatabaseConnection;

        //Contains the map of the Database Connections
        std::map<std::string, CORE::RefPtr<VizDataUI::IDatabaseUIHandler> > _dbHandlerMap;
        std::map<std::string, CreatorPlannerSchemaTable*> _cpSchemaTableMap;

        CORE::RefPtr<ELEMENTS::ISettingComponent> _globalSettingComponent;
    };

} // namespace SMCUI

