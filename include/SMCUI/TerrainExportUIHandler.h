#ifndef SMCUI_TERRAINEXPORTUIHANDLER_H_
#define SMCUI_TERRAINEXPORTUIHANDLER_H_


/******************************************************************************
* File              : TerrainExportUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class
* Author            : Kamal Grover
* Author email      : kamal@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/ITerrainExportUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Terrain/IElevationObject.h>
#include <boost/filesystem.hpp>
#include <ogrsf_frmts.h>
#include <ogr_geometry.h>
#include <ogr_spatialref.h>
#include <Util/Log.h>
#include <Util/FileUtils.h>
#include <sqlite3.h>
#include <spatialite.h>
#include <Core/IFeatureLayer.h>
#include <gdal.h>
#include <gdal_priv.h>
namespace fs = boost::filesystem;

namespace SMCUI
{
    class SMCUI_DLL_EXPORT TerrainExportUIHandler
        : public ITerrainExportUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        TerrainExportUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        virtual bool getFocus() const;

        //@{
        /** Called to subscribe/ unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Set the extents of the area */
        void setExtents(osg::Vec4d extents);

        osg::Vec4d getExtents() const;

        void setExtentsUTM(osg::Vec4d extentsUTM);

        osg::Vec4d getExtentsUTM() const;

        //@}
        /** Set the center point */
        void setCenter(osg::Vec3d center);
        osg::Vec3d getCenter() const;

        void setSideLength(int sidelength);
        int getSideLength();
        void setLongSpan(double longspan);
        double getLongSpan();
        void setLatSpan(double latspan);
        double getLatSpan();
        void setTileSize(int tilesize);
        int getTileSize();
        void setOutDir(std::string outDir);
        std::string getOutDir();
        void setTempDir(std::string tempDir);
        std::string getTempDir();

        void setClassificationMap(std::map < std::string, std::string >& map);
        std::map < std::string, std::string >& getClassificationMap();
        enum LayerStatus
        {
            FAILED,
            INPROGRESS,
            CANCELED,
            COMPLETED
        };
        typedef std::pair <CORE::RefPtr<CORE::IObject>, LayerStatus> Fileinfo;
        std::map < std::string, Fileinfo >& getStatusMap();
        int verifyAndCountLayers();
        std::string getLastCompletedLayer();
        void cancelExport(bool hasUserCancelled);
        //! start on ok
        virtual void execute(int* progress);

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
        
        void populateVectorAttributes(std::string vectorFile, std::vector < std::string>& abbtibuteList);
        void generateRasterFromVector(std::string inputFile, std::vector<std::string>& attributes, std::string outDir, osg::Vec4d extents, osg::Vec2d resolution, int* statusUpdate);
        void generateRasterAttributeBased(std::string inputFile, std::vector<std::string>& attributes, std::string outDir, osg::Vec4d extents, osg::Vec2d resolution, int* statusUpdate);

    private:
        inline bool hasUserCancelled(std::string filename)
        {
            LOG_INFO("...ENTRY... " + fs::path(filename).stem().string());
            if (_cancelExport == true)
            {
                //OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                _statusMap[filename].second = LayerStatus::CANCELED;
                LOG_INFO("User has clicked cancel... exiting gracefuly");
                return true;
            }
            LOG_INFO("...EXIT... " + fs::path(filename).stem().string());
            return false;
        }
        inline bool dirExists(std::string dir)
        {
            if (!fs::exists(dir) || !fs::is_directory(dir))
            {
                LOG_ERROR("Dir not found " + dir);
                return false;
            }
            return true;
        }
        inline bool removeDirElseUpdateMap(std::string dir, std::string key)
        {
            LOG_INFO("...ENTRY... " + fs::path(key).stem().string());
            if (fs::exists(dir) && fs::is_directory(dir))
            {
                try
                {
                    LOG_INFO("<<<<<Remove Dir>>>>> " + dir);
                    fs::remove_all(dir);
                }
                catch (...)
                {
                    //OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                    _statusMap[key].second = LayerStatus::FAILED;
                    LOG_ERROR("Can not remove Dir " + dir);
                    return false;
                }
            }
            LOG_INFO("...EXIT... " + fs::path(key).stem().string());
            return true;
        }
        inline bool filesExistsElseUpdateMap(std::string filename)
        {
            LOG_INFO("...ENTRY... " + fs::path(filename).stem().string());
            if (!fs::exists(filename) || !fs::is_regular_file(filename))
            {
                //OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                _statusMap[filename].second = LayerStatus::FAILED;
                LOG_ERROR("Source File not available, Filename : " + filename);
                return false;
            }
            LOG_INFO("...EXIT... " + fs::path(filename).stem().string());
            return true;
        }
        inline bool removeFileElseUpdateMap(std::string filename, std::string key)
        {
            LOG_INFO("...ENTRY... " + fs::path(filename).stem().string());
            if (fs::exists(filename) && fs::is_regular_file(filename) && !UTIL::deleteFile(filename))
            {
                //OpenThreads::ScopedLock<OpenThreads::Mutex> slock(_mutex);
                _statusMap[key].second = LayerStatus::FAILED;
                LOG_ERROR("Cannot delete file : " + filename);
                return false;
            }
            LOG_INFO("...EXIT... " + fs::path(filename).stem().string());
            return true;
        }
        inline bool fileExists(std::string filename)
        {
            if (!fs::exists(filename) || !fs::is_regular_file(filename))
            {
                LOG_ERROR("File not found " + filename);
                return false;
            }
            return true;
        }
        inline bool deleteFile(std::string filename)
        {
            if (fs::exists(filename) && fs::is_regular_file(filename) && !UTIL::deleteFile(filename))
            {
                LOG_ERROR("Cannot delete file : " + filename);
                return false;
            }
            return true;
        }
    protected:
        ~TerrainExportUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

        void _updateRasterProgress();

        bool _exportElevationData(std::string infile, std::string outDir, std::string tempDir, int* progress);
        bool _exportRasterData(std::string infile, std::string outDir, std::string tempDir, int* progress, int res);
        bool _exportVectorData(std::string infile, int* progress);
        bool _exportVectorDataAsMask(std::string infile, std::string outDir, std::string tempDir, int* progress);
        bool _exportVectorDataAsJson(std::string infile, std::string outDir, std::string tempDir, int* progress);
        bool _cropRasterWGS84(std::string infile, std::string& outfile, std::string outDir);
        //TODO :: Move this Function to util
        bool _writeInfoFile(std::string jsonfile, std::map<std::string, std::string> infomap);
        bool _writeMarkings(CORE::RefPtr<CORE::IFeatureLayer> layer, std::string filename);
        osg::Vec4d _getOtherDiagonal(osg::Vec4d extents);
        double _getVerticalRange(std::string infile);

        /* ------------------------------------------------------------------------------------------*/
        /*      Verify source window dimensions.                                                     */
        /*Description: cropping the infile with the given will result in atleast 1 pixel image or not*/
        /* -------------------------------------------------------------------- ---------------------*/
        bool _willBeAtleast1Pixel(std::string infile, osg::Vec4d extents);
        bool _willBeAtleast1PixelWGS84(GDALDataset* dataset, osg::Vec4d extents);
        double _getSpanWestEast(osg::Vec4d ext);
        double _getSpanNorthSouth(osg::Vec4d ext);
        osg::Vec2i _getOutResolution(osg::Vec2i inReso);

        bool _offsetAllFiles(std::string dir);
        bool _offsetFile(std::string filename);
        bool _gaussianSmooth(std::string srcfile, std::string destfile, int max_kernel_length);
        //extents format ulx uly, brz, bry
        bool _transform(std::vector<OGRPoint*>& points, osg::Vec4d extents);
        bool _clearTempFiles(std::string dir, std::vector<std::string>& ext, bool isRecursive);
        bool _isOverlappingInterval(osg::Vec2d u, osg::Vec2d v);
        bool _isOverlapping(osg::Vec4d extents1, osg::Vec4d extents2);
        bool TerrainExportUIHandler::_isWithin(osg::Vec4d ext1, osg::Vec4d ext2);
        bool TerrainExportUIHandler::_isWithinInterval(osg::Vec2d u, osg::Vec2d v);
        void _updateProgress(int* progress, std::string layer);



        void _dumpExtentsShape();

        OpenThreads::Mutex _mutex;
        //BottomLeftLongLat, TopRightLongLat
        osg::Vec4d _extents;
        //different from _extents
        //UpperLeftLongLat, BottomRightLongLat
        osg::Vec4d _extentsUTM;
        osg::Vec3d _center;
        double _longspan = 10081.0f;
        double _latspan = 10081.0f;
        int _sideLength = 10000;
        int _tilesize = 2017;
        std::string _outDir = "";
        std::string _tempDir = "";
        bool _cancelExport = false;
        std::map<std::string, std::string> _classificationMap;
        std::string _lastCompletedLayer = "";
        std::map<std::string, Fileinfo> _statusMap;
        bool _rasterExportInProgress = false; 

    };

} // namespace SMCUI

#endif // SMCUI_TERRAINEXPORTUIHANDLER_H_

