#ifndef SMCUI_IPERIMETERCALCUIHANDLER_H_
#define SMCUI_IPERIMETERCALCUIHANDLER_H_

/*****************************************************************************
* File              : IPerimeterCalcUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for perimeter filter
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

namespace SMCUI
{
    //! UIHandler for perimeter ui handler
    class IPerimeterCalcUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IPerimeterCalcUIHandler);

    public:

        // perimeter message
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> AreaCalculationCompletedMessageType;

    public:

        enum ComputationType
        {
            //! for normal perimeter of the polygon
            AERIAL,  
            //! for perimeter of projection of polygon on the terrain
            PROJECTED,
            //! for an invalid visitor
            INVALID
        };

        //@{
        /** Sets/gets the points of the polygons to be processed */
        virtual void setPoints(osg::Vec3dArray* points) = 0;
        virtual osg::Vec3dArray* getPoints() = 0;
        //@}

        //@{
        /** Sets/gets the type of perimeter to compute */
        virtual void setComputationType(ComputationType type) = 0;
        virtual ComputationType getComputationType() = 0;
        //@}

        virtual double getPerimeter() = 0;

        //! Executes the filter
        virtual void execute() = 0;

        //! Stops the execution of the filter 
        virtual void stop() = 0;

    };
} // namespace SMCUI

#endif  // SMCUI_IPERIMETERCALCUIHANDLER_H_
