#ifndef SMCUI_OVERLAYUIHANDLER_H_
#define SMCUI_OVERLAYUIHANDLER_H_

/*****************************************************************************
 *
 * File             : OverlayUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : OverlayUIHandler class
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2012-2013, VizExperts India Private Limited (unpublished)
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <VizUI/UIHandler.h>
#include <SMCUI/IOverlayUIHandler.h>
#include <Elements/IOverlay.h>

namespace SMCUI 
{
    class OverlayUIHandler : public VizUI::UIHandler
                           , public SMCUI::IOverlayUIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

        public:

            //! Constructor
            OverlayUIHandler();

            //! create an overlay
            ELEMENTS::IOverlay* createOverlay(const std::string& overlayName, 
                                                  const std::string& authorName, 
                                                  const std::string& operationName,
                                                  const std::string& operationTaskReference,
                                                  const std::string& operationType,
                                                  unsigned int operationNumber,
                                                  const boost::posix_time::ptime& creationTime);

            //! find the overlay in the world
            ELEMENTS::IOverlay* findOverlay(const std::string& overlayName);

            //! set selected Overlay as current Overlay
            void setSelectedOverlayAsCurrentOverlay();

            //! add layer to the current Overlay
            bool createLayer(const std::string& layerName,
                             const std::string& layerType,
                             std::vector<std::string>& attrNames,
                             std::vector<std::string>& attrTypes,
                             std::vector<int>& attrWidths,
                             std::vector<int>& attrPrecisions);

            //! get the current Overlay
            ELEMENTS::IOverlay* getCurrentOverlay() const;

            //! set the current overlay
            void setCurrentOverlay(ELEMENTS::IOverlay* overlay);

        protected:

            //! Overlay Object
            CORE::RefPtr<ELEMENTS::IOverlay> _overlay;

    };
} // namespace SMCUI

#endif // SMCUI_OVERLAYUIHANDLER_H_
