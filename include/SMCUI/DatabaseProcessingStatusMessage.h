#ifndef TKUI_DATABASEPROCESSINGSTATUSMESSAGE_H
#define TKUI_DATABASEPROCESSINGSTATUSMESSAGE_H

/******************************************************************************
 * File              : DatabaseProcessingStatusMessage
 * Version           : 1.0
 * Module            : TKUI
 * Description       : DatabaseProcessingStatusMessage class declaration
 * Author            : Raghavendra Polinki
 * Author email      : rp@vizexperts.com
 * Reference         : 
 * Inspected by      : 
 * Inspection date   :  
 
 *******************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *******************************************************************************
 *
 * Revision log (latest on the top): 
 *
 ******************************************************************************/

#include <SMCUI/export.h>
#include <Core/CoreMessage.h>
#include <SMCUI/IDatabaseProcessingStatusMessage.h>

namespace SMCUI
{
    //! Filter Status message type
    class DatabaseProcessingStatusMessage 
        : public CORE::CoreMessage
        , public SMCUI::IDatabaseProcessingStatusMessage
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        public:

            DatabaseProcessingStatusMessage();

            //@{
            /** Sets/Gets the filter status */
            void setStatus(DatabaseProcessingStatus status);
            DatabaseProcessingStatus getStatus() const;
            //@}

            void setProgress(unsigned int progress);
            unsigned int getProgress();

        protected:

            ~DatabaseProcessingStatusMessage();

        protected:

            //! Filter status
            DatabaseProcessingStatus _status;

            //! progress
            unsigned int _progress;
    };
}

#endif // TKUI_DATABASEPROCESSINGSTATUSMESSAGE_H
