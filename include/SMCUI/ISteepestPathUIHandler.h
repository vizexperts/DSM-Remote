#ifndef SMCUI_ISTEEPESTPATHUIHANDLER_H_
#define SMCUI_ISTEEPESTPATHUIHANDLER_H_

/******************************************************************************
* File              : ISteepestPathUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Steepest Path filter UI Handler
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/IMessageType.h>
#include <Core/ILine.h>
#include <GISCompute/ISteepPathCalcVisitor.h>

namespace SMCUI 
{
    class ISteepestPathUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ISteepestPathUIHandler);

    public:

        public:
            /** Play states for steep computation, 
            GUI can query for state to update buttons*/
            enum PlayState 
            {
                STEEPCOMPUTATION_IDLE,
                STEEPCOMPUTATION_PLAYING,
                STEEPCOMPUTATION_PAUSED,
                STEEPCOMPUTATION_STOPPED,
                STEEPCOMPUTATION_PLAYCOMPLETE,
                STEEPCOMPUTATION_EXIT
            };

            //@{
            /** set/get focus*/
            virtual void setFocus(bool value)= 0;
            //@}

            //@{
            /** returns true, if startingpoint is within Area specified otherwise false*/
            virtual bool setStartingPoint(const osg::Vec2d & point)=0;
            //@}

            //@{
            /** start point for whole steep computation session*/
            virtual osg::Vec2d& getStartingPoint()=0;
            //@}

            //@{
            /** set output name*/
            virtual void setOutputName(std::string& name) = 0;
            //@}

            //@{
            /** returns true, if Area Specified, contains the specified starting point,
            otherwise it returns false*/
            virtual bool setExtent(const osg::Vec2dArray & extents)=0;
            //@}

            //@{
            /** start computing steep and notifying at some interval, async update*/
            virtual bool play(bool multiThreaded = true)=0;
            //@}
            
            //@{
            /** stop computing steep and notifying for output generated,
            Also update the current point as that of starting point*/
            virtual bool stop()=0;
            //@}

            //@{
            /** Query Current play state- idle, playing, stopped, paused*/
            virtual PlayState getPlayState()=0;
            //@}

            //! set/get Max Level
            virtual void setMaxLevel(double maxLevel)=0;
            virtual double getMaxLevel()=0;

            //! set/get cell size factor
            virtual void setCellSizeFactor(double cellSizeFactor)=0;
            virtual double getCellSizeFactor()=0;

            //! set/get update Interval
            virtual void setUpdateInterval(double updateInterval)=0;
            virtual double getUpdateInterval()=0;

            //!  get Output Line
            virtual CORE::ILine* getOutput() const = 0;
            // get Output  Points
            virtual osg::Vec3dArray *getOutputPoints() const = 0;


    };

} // namespace SMCUI

#endif // SMCUI_ISTEEPESTPATHUIHANDLER_H_
