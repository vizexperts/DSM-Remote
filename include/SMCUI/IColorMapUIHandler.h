#ifndef SMCUI_ICOLORMAPUIHANDLER_H_
#define SMCUI_ICOLORMAPUIHANDLER_H_

/******************************************************************************
* File              : IColorMapUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Min Max Elevation GUI Handler
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Elements/IColorMap.h>

namespace SMCUI
{
    class IColorMapUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IColorMapUIHandler);

    public:

        //! create color map
        virtual CORE::RefPtr<ELEMENTS::IColorMap> getColorMap(const std::string& name) = 0;


        //! write color map
        virtual bool exportColorMap(ELEMENTS::IColorMap*, const std::string& name) = 0;

    protected:

        virtual ~IColorMapUIHandler() {}
    };

} // namespace SMCUI

#endif // SMCUI_ICOLORMAPUIHANDLER_H_
