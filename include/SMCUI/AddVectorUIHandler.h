#pragma once

/******************************************************************************
* File              : AddVectorUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : AddVectorUIHandler Implementation Class
* Author            : Abhinav Goyal
* Author email      : abhinav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
* Copyright (c) 2013-2014, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/ITerrain.h>
//#include <Core/IFeatureLayer.h>
#include <Core/IMetadataCreator.h>


#include <VizUI/UIHandler.h>

#include <SMCUI/IAddVectorUIHandler.h>

#include <osgEarthUtil/FeatureQueryTool>
#include <osgEarthUtil/AnnotationEvents>

namespace CORE
{
    class IPoint;
    class ILine;
    class IPolygon;
    class ISelectionComponent;
    class IWorld;
    class IFeatureLayer;
}

namespace SMCUI {

       // XXX - should not be exported
    class SMCUI_DLL_EXPORT AddVectorUIHandler : public IAddVectorUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
       
        public:
            
            AddVectorUIHandler();

            //@{
            //! Function called when added to UI Manager
            void onAddedToManager();
            //! Function called when removed from UI Manager
            void onRemovedFromManager();
            //@}
            
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            void addVectorData(const std::string &url, osgEarth::Symbology::Style &style) ;

            void setOffset(double offset);
            double getOffset() const;

            void setClamping(bool clamping);
            bool getClamping() const;

            void setEditable(bool editable) ;
            bool getEditable() const ;

            void setIconFileName(std::string iconFileName="") ;
            std::string getIconFileName() const;

            void createProfileFromVector(std::vector<osg::Vec2d> pointVec);

            void setDistanceProfileObject(std::string profileName, std::map<double, std::string> distanceProfileObject);

            void setProfileTexture(std::string texturefile);
        protected:

            void _configureFeatureQueryTool();
            CORE::IObject* _createVectorObject();
            
        protected:

            //! Creating object of particular type from the factory
            CORE::IObject* _createObjectOfTypeFromFactory(CORE::IObjectType &objectType);

            //! For customizing the osgEarth line annotation Node to Viz Object
            CORE::IObject* _createLineObjectfromFeature(osgEarth::Features::Feature *feature);

            //! For customizing the osgEarth polygon annotation Node to Viz Object
            CORE::IObject* _createPolygonObjectFromFeature(osgEarth::Features::Feature *feature);

            //! For customizing the osgEarth point feature to viz Object
            CORE::IObject* _createPointObjectFromFeature(osgEarth::Features::Feature *feature);

            //! For customizing the osgEarth placeNode to Viz Object
            CORE::IObject* _createPointObjectFromPlaceNode(osgEarth::Annotation::AnnotationNode *annoNode);

            void _updateMetaDataObject(CORE::IObject *object, CORE::IFeatureLayer *featureLayer, osg::Node *node);

            bool _createFeatureObjectFromGroupNode(osg::Node *node, const std::string& folderName, CORE::ICompositeObject* featureDatasource);

            CORE::IObject *_createObjectFromNode(osg::Node *node);

            CORE::IFeatureLayer* _getOrCreateFeatureLayer(const std::string& folderName, CORE::IFeatureLayer::FeatureLayerType layerType, CORE::ICompositeObject* featureDatasource);

            CORE::IFeatureLayer::FeatureLayerType _getFeatureLayerType(osg::Node *node);

            ~AddVectorUIHandler();

            void _addKMLToWorld(const std::string &url);
            void _updateNodeMask(osg::Node *node);

            std::string _iconFileName;
            
            bool _isClamp;
            bool _isEditable;

            double _verticalOffset;

            CORE::RefPtr<CORE::IObjectFactory> _objectFactory;

            CORE::RefPtr<CORE::IMetadataCreator> _metaDataCreator;

            typedef std::map<std::string, CORE::RefPtr<CORE::IFeatureLayer> > RasterLayerMap;
            typedef std::map<std::string, RasterLayerMap> KMLFolderMap;

            KMLFolderMap _kmlFolderMap;

            std::map<double, std::string> _distanceProfileObject;
            std::string _profileName; 
            std::string _textureFile; 
    };
} 
// namespace SMCUI

