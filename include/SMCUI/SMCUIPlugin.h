#ifndef SMCUI_SMCUIPLUGIN_H_
#define SMCUI_SMCUIPLUGIN_H_

/*****************************************************************************
 *
 * File             : SMCUIPlugin.h
 * Version          : 1.1
 * Module           : SMCUI
 * Description      : SMCUIPlugin class declaration
 * Author           : Gaurav Garg
 * Author email     : gaurav@vizexperts.com
 * Reference        : Part of SMCreator Framework
 * Inspected by     :
 * Inspection date  :
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <App/IAppRegistryPlugin.h>
#include <App/IUIHandlerType.h>
#include <App/IManagerType.h>
#include <App/IUndoTransactionType.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <Core/ICoreRegistryPlugin.h>

namespace APP
{
    class ApplicationRegistry;
    class IUIHandlerFactory;
    class IManagerFactory;
    class IUndoTransactionFactory;
}


namespace CORE
{
    class InterfaceRegistry;
    class IMessageFactory;
    class IObjectFactory;
    class IVisitorFactory;
    class IComponentFactory;
}

namespace SMCUI
{

    //! Registers all objects, interfaces and messages defined in CORE
    class SMCUIRegistryPlugin : public APP::IAppRegistryPlugin
    {
        public:

            //@{
            /** Library IDs */
            static const unsigned short SMCUIRegistryPluginStartId = 3001;
            //@}

            //@{
            /** UIHandler types */


            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AddContentUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AddCSVUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AddExcelUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AddGPSUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AddVectorUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AlmanacUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AnimationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> AreaUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ColorElevationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ColorMapUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ContoursGenerationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> CreateFeatureUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> CreateLayerUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> CrestClearanceUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> CutAndFillUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> DatabaseLayerLoaderUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> DistanceCalculationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> DraggerUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ElevationDiffUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ElevationProfileUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> EventUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ExportUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> FeatureExportUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> FloodingUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> FlyAroundUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> GridDisplayUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> HeightCalculationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> HillshadeCalculationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> LandmarkUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> LineUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> LookUpProjectUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> MilitaryPathAnimationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> MilitarySymbolPointUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> MinMaxElevUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> OverlayUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> PerimeterCalcUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> PointUIHandler2Type;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> RadioLOSUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> RecordingUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> SlopeAspectUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> SlopeCalculationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> SQLQueryUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> SteepestPathUIHandlerType;

            
            
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> TerrainExportUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> WatershedGenerationUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> SurfaceAreaCalcUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> TourUIHandlerType;
			static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> TacticalSymbolsUIHandlerType;
			static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ExportToGeoserverUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> VectorLayerQueryUIHandlerType;
            static const SMCUI_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> VisibilityControllerUIHandlerType;

           //@}

            //@{
            /** UndoTransaction types */
            //@}

            //! UIHandlerManager type


            //! Library name string
            static const std::string LibraryName;

            //! Plugin name string
            static const std::string PluginName;

        public:

            ~SMCUIRegistryPlugin();

             //@{
            /** Plugin information */
            const std::string& getPluginName() const;
            const std::string& getLibraryName() const;
            //@}

            //@{
            /** Load/Unload the library */
            virtual void load(const APP::ApplicationRegistry& cr);
            virtual void unload(const APP::ApplicationRegistry& cr);
            //@}

        protected:

            //@{
            /** Register/Deregister applications */
            virtual void registerUIHandlers(APP::IUIHandlerFactory& af);
            virtual void deregisterUIHandlers(APP::IUIHandlerFactory& af);
            //@}

            //@{
            /** Register/Deregister managers */
            virtual void registerManagers(APP::IManagerFactory& mf);
            virtual void deregisterManagers(APP::IManagerFactory& mf);
            //@}

            //! register transactions
            virtual void registerTransactions(APP::IUndoTransactionFactory& tf);

            //! unregister transactions
            virtual void deregisterTransactions(APP::IUndoTransactionFactory& tf);
    };

    //-------------------------------------------------------------------------
    
    //! \brief  Registers all objects, interfaces and messages defined in SMCUICore
    //! Defines ApplicationLoadedMessageType
    class SMCUICoreRegistryPlugin : public CORE::ICoreRegistryPlugin
    {
        public:

            //! Starting ID for app core registry plugin
            static const unsigned int StartID = 4000;

            //! Library name string
            static const std::string LibraryName;

            //! Plugin name string
            static const std::string PluginName;

        public:

            ~SMCUICoreRegistryPlugin();

             //@{
            /** Plugin information */
            const std::string& getPluginName() const;
            const std::string& getLibraryName() const;
            //@}

            //@{
            /** Load/Unload the library */
            virtual void load(const CORE::CoreRegistry& cr);
            virtual void unload(const CORE::CoreRegistry& cr);
            //@}

        protected:

            //@{
            /** Register/Deregister messages */
            virtual void registerMessages(CORE::IMessageFactory& mf);
            virtual void deregisterMessages(CORE::IMessageFactory& mf);
            //@}
    };
}

#endif  // SMCUI_SMCUIPLUGIN_H_
