#pragma once

/*****************************************************************************
* File              : IHeightCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : HeightCalculationUIHandler Interface class for calculating Height from sealevel or relative to Terrain
* Author            : Venkata Balakrushna Beesetti
* Author email      : balakrishna@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
*  Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*
*  All rights reserved. This notice is intended as a precaution against
*  inadvertent publication and does not imply publication or any waiver
*  of confidentiality. The year included in the foregoing notice is the
*  year of creation of the work. No part of this work may be used,
*  reproduced, or transmitted in any form or by any means without the prior
*  written permission of VizExperts India Private Limited.
*
******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file   SMCUI/IHeightCalculationUIHandler.h
//!
//! \brief  IHeightCalculationUIHandler class declaration
//!
////////////////////////////////////////////////////////////////////////////////

#include <Core/IReferenced.h>
#include <Core/RefPtr.h>
#include <SMCUI/export.h>
#include <osg/Vec3d>
#include <Core/IPoint.h>
#include <Core/IMessageType.h>

namespace osg
{
    class Node;
    class Geode;
}


namespace SMCUI 
{
    //! interface used for calculating Height from seaLevel or relative to terrain
    class IHeightCalculationUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IHeightCalculationUIHandler);

        /*
            enum ComputationType
            {
                //! Height calculation from Mean Sea Level
                MEANSEALEVEL,
                //! Height Calculation from Relative To Terrain
                RELATIVETOTERRAIN,
            }; */

        // Represents the current drag stage
        enum DragStage
        {
            START,
            MOVE,
            FINISH
        };

        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> DraggerUpdated;

        public:

        //@{
        /** Sets/gets the type of area to compute */
//        virtual void setComputationType(ComputationType type) = 0;
//        virtual ComputationType getComputationType() = 0;
        //@}

            //@{
            /** Set/Get position of the element */
            virtual void setPosition(const osg::Vec3d& position) = 0;
            virtual const osg::Vec3d& getPosition() const = 0;
            //@}

            //@{
            /** Set/Get drag stage of the element */
            virtual void setDragStage(DragStage) = 0;
            virtual DragStage getDragStage() const = 0;
            //@}
    };

} // namespace SMCUI


