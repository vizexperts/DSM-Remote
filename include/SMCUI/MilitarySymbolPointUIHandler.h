#ifndef SMCUI_MILITARYSYMBOLPOINTUIHANDLER_H_
#define SMCUI_MILITARYSYMBOLPOINTUIHANDLER_H_

/*****************************************************************************
*
* File             : MilitarySymbolPointUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : MilitarySymbolPointUIHandler class
* Author           : Gaurav Garg
* Author email     : gg@vizexperts.com
* Reference        : SMCUImodule
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <VizUI/UIHandler.h>
#include <SMCUI/IMilitarySymbolPointUIHandler.h>
#include <Elements/IMilitarySymbol.h>
#include <Elements/IMilitaryRange.h>
#include <osgUtil/LineSegmentIntersector>
#include <Core/Intersections.h>
#include <Elements/ISettingComponent.h>

namespace CORE
{
    class IFeatureLayer;
}

namespace SMCUI 
{
    class MilitarySymbolPointUIHandler : public VizUI::UIHandler
        , public IMilitarySymbolPointUIHandler
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;
    public:

        //! Constructor
        MilitarySymbolPointUIHandler();


        CORE::IFeatureLayer* getOrCreateMilitarySymbolLayer(const std::string &symbolName, bool addToWorld = true);

        //@{
        /** Sets/Gets Unit */
        void setMilitarySymbol(ELEMENTS::IMilitarySymbol* unit);
        ELEMENTS::IMilitarySymbol* getMilitarySymbol() const;
        //@}

        void setMilitaryRange(ELEMENTS::IMilitaryRange* unit);
        ELEMENTS::IMilitaryRange* getMilitaryRange() const;

        void setMilitarySymbolType(std::string fullName);
        std::string getMilitarySymbolType() const;

        //! Initializes the following attributes:-
        //!     # Clamp State for Symbol
        //!     # HeightOffset for Symbol
        void initializeAttributes();

        //! set the height offset
        void setOffsetFromConfig(double offset);
        
        //! get the height offset
        double getOffsetFromConfig() const;

        //! set the current mode
        void setMode(MilitarySymbolMode mode);

        //! get the current mode
        MilitarySymbolMode getMode();

        //! set the current selected line as current line
        void setSelectedUnitAsCurrentUnit();

        //! set the metadata record to the current line
        void setMetadataRecord(CORE::IMetadataRecord* record);

        //! get the metadata record of the current line
        CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const;

        //! update the current line
        void update();

        //! add point to current selected layer
        void addUnitToCurrentSelectedLayer();

        //! get current selected layer definition
        CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn();

        //! remove the created point
        void removeCreatedUnit();

        //! set the focus of the UI handler
        void setFocus(bool value);

        //! function called when added to Manager
        void onAddedToManager();

        //! function called when it recieves a message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! set the temporary state
        void setTemporaryState(bool value);

        //! get the temporary state
        bool getTemporaryState() const;

        //! Create a Point and add it to the scene
        void createUnit(bool addToWorld=true);

        //! Set Clamping from UIHandler config
        void setClampingStateFromConfig(bool clamp);

        //! Get Clamping from UIHandler Config
        bool getClampingStateFromConfig() const;

        void reset();

        //! helper funtion for GUI to get iconPath for a military symbol type
        std::string getQMLIconPath(CORE::RefPtr<ELEMENTS::IMilitarySymbolType> milSymbolType) const;

        //! Utility function to create military symbol of given type
        CORE::RefPtr<ELEMENTS::IMilitarySymbol> createSymbol(const std::string& type) ;

        //! Utility functio to add given symbol to its respective layer
        void addSymbolToLayer(CORE::RefPtr<ELEMENTS::IMilitarySymbol> symbol);


    protected:

        //! Destructor
        ~MilitarySymbolPointUIHandler();

        void _createUndoMoveTransaction();

        void _handleButtonDraggedIntersection(int button, const osg::Vec3d& longLatAlt);

        void _handleButtonReleasedIntersection(int button, const osg::Vec3d& longLatAlt);

        void _handleButtonDoubleClickedIntersection(int button, CORE::RefPtr<CORE::Intersection> intersection);

        //internal function for setting default clamping state through "Setting Component"
        void _setDefaultClamping();

        //internal function for setting default offset value through "Setting Component"
        void _setDefaultOffset();

        //!
        CORE::IFeatureLayer* _getSelectedLayer();

    private:

        //! point
        CORE::RefPtr<ELEMENTS::IMilitarySymbol> _unit;

        CORE::RefPtr<ELEMENTS::IMilitaryRange> _sphere;

        std::string _layerName;

        //! Offset for the Symbol
        double _offset;

        //! First vertex value
        osg::Vec3d _vertex;

        //! mode
        IMilitarySymbolPointUIHandler::MilitarySymbolMode _mode;

        //! temporary state
        bool _temporary;

        bool _dragged;

        bool _handleButtonRelease;

        //! Clamping Based on UIHandlerConfig
        bool _clamping;

        //! whether clamp attribute is present in config or not
        bool _clampingFromConfig;

        //! whether offset attribute is present in config or not
        bool _offsetFromConfig;

        CORE::RefPtr<ELEMENTS::ISettingComponent> _settingComponent;

    };
} // namespace SMCUI

#endif // SMCUI_MILITARYSYMBOLPOINTUIHANDLER_H_
