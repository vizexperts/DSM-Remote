#ifndef SMCUI_EXPORTUIHANDLER_H_
#define SMCUI_EXPORTUIHANDLER_H_

/*****************************************************************************
 *
 * File             : ExportUIHandler.h
 * Version          : 1.0
 * Module           : UI
 * Description      : ExportUIHandler class
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : UI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <VizUI/UIHandler.h>
#include <SMCUI/IExportUIHandler.h>
#include <Core/IFeatureObject.h>
#include <osg/Vec4>

namespace boost
{
    namespace filesystem 
    {
        class path;
    }
}
namespace SMCUI 
{
    class ExportUIHandler : public VizUI::UIHandler
                                 , public IExportUIHandler
    {
        DECLARE_IREFERENCED
        DECLARE_META_BASE
        public:

            //! Constructor
            ExportUIHandler();

           //! export the file
            bool exportRaster(const std::string& filename);
            bool exportRasterAsTMS(const std::string& directoryPath, const std::string& minLevel, const std::string& maxLevel, const osg::Vec4& selectedArea);
            bool exportProject(const std::string& projectFileName);
            void getStatus(TMSCREATIONSTATUS& status);
        protected:
            
            //! Creates TMS Repository in a seperate thread. 
            void _mtExportRasterAsTMS(const std::string& directoryPath, const std::string& minLevel, const std::string& maxLevel, const osg::Vec4& selectedArea);
            
            //! Moving Folder to remote folder or different folder.
            void _copyFoldertoRemote(const std::string& srcdirectoryPath, const std::string& dstdirectoryPath);

            //! Copy folder to another directory
            bool _copyDir(boost::filesystem::path const & source, boost::filesystem::path const & destination);

            //! get the current selected feature
            template <class T>
            T* _getCurrentSelectedItem();

            TMSCREATIONSTATUS _status;
    };
} // namespace UI

#endif // SMCUI_EXPORTUIHANDLER_H_
