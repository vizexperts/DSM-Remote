#pragma once

/******************************************************************************
* File              : AddContentUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : IAddContentUIHandler interface
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <SMCUI/IAddContentUIHandler.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/IAddContentStatusMessage.h>
#include <SMCUI/IAddContentComputeMinMaxLODMessage.h>

#include <Core/ITerrain.h>

#include <osg/NodeCallback>

#include <OpenThreads/Mutex>

namespace CORE
{
    class ITerrain;
}

namespace SMCUI {

    static int _progressValue;
    void setProgress(int value);
    int getProgress();

    // XXX - should not be exported
    class SMCUI_DLL_EXPORT AddContentUIHandler : public IAddContentUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        AddContentUIHandler();

        void addImageOverlay(const std::string &url, osg::Vec4d &extents, std::string rotation, int subdataset);
        void getGdalSubsets(const std::string &url, std::vector<std::string> &list);

        void addRasterData(const std::string &layerName, const std::string &url);

        void addMultibandRasterData(std::string layerName, std::string url1, std::string url2, std::string url3,
            osg::Vec4d &extents,
            bool override_extents,
            unsigned int minLevel,
            unsigned int maxLevel,
            bool hasNoData,
            double noDataValue);

        void addMultibandHDFData(const std::string &layerName,
            const std::string &url,
            osg::Vec4d &extents,
            bool override_extents,
            unsigned int minLevel,
            unsigned int maxLevel,
            std::vector<int> subdatasetVector, bool hasNoData, double noDataValue);

        void addRasterData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel
            , int subdataset, const std::string &enhanceType, const std::string &projectionType);

        void addElevationData(const std::string &layerName, const std::string &url);
        void addElevationData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel);

        void addVectorData(const std::string &layerName, const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options);

        void addDGNData(const std::string &layerName, const std::string &url, const std::string &orclUrl, osg::Vec2d extents, bool reloadCache);

        void computeMinMaxLOD(std::string fileName, unsigned int minLevel, unsigned int maxLevel);

        osg::Vec4 defaultExtentsQuery(const std::string& fileName);

        void setImageDraggerVisibility(bool value);

        void setImageRotateVisibility(bool value);

        void initializeAttributes();

        //@{
        //! Function called when added to UI Manager
        void onAddedToManager();
        //! Function called when removed from UI Manager
        void onRemovedFromManager();
        //@}

        // Handles tick message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void setOffset(double offset);
        double getOffset() const;

        void setClamping(bool clamping);
        bool getClamping() const;

        void setProgressValue(int value);
        int getProgressValue();

        void addOSMWebData(std::string url);
        void removeOSMWebData();

        void addYahooWebData(std::string datasetOption);
        void removeYahooWebData();

        void addWorldWindWebData(std::string url);
        void removeWorldWindWebData();

        void addBingWebData(std::string url);
        void removeBingWebData();

        bool addGoogleData(const GoogleDataType& type);
        void removeGoogleData(const GoogleDataType& type);

        void reorderRasterBands(CORE::IObject* object, const std::vector<int>& bandOrder);

        void changeRasterEnhancementType(CORE::IObject* object, const TERRAIN::IRasterObject::EnhancementType enhanceType);

        //! \brief updates modelObject with new style Template
        //! \param IObject
        //! \param template Name 
        void updateModelObject(CORE::IObject* object, std::string templateName);

        //! 
        //! \brief Adding KML super overlay images. The image reference is there in KML file or KMZ file. 
        //! \param fileName to be loaded
        //! 
        void addKMLSuperOverlay(const std::string &fileName);

        void updateMarkingsObject(CORE::IObject *object, std::string jsonFile);
    protected:

        ~AddContentUIHandler();

        // update modelObject in separate thread 
        void _updateModelObject(CORE::IObject* object, std::string templateName); 

        // Load imageoverlay in a seperate thread
        void _mtAddImageOverlay(const std::string &url, osg::Vec4d &extents, std::string rotation, int subdataset);

        //! Loads raster data in a separate thread
        void _mtAddRasterData(const std::string &layerName, const std::string &url);

        //! Loads raster data in a separate thread
        void _mtAddRasterData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel
            , int subdataset, const std::string &enhanceType, const std::string &projectionType, bool hasNoData = false, double noDataValue = 0.0);

        //! Loads raster data in a separate thread
        void _mtAddMultiBandRasterData(const std::string &layerName, const std::string &url1, const std::string &url2, const std::string &url3, TERRAIN::IRasterObject::EnhancementType enhanceType,
            osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel, int subdataset, bool hasNoData, double noDataValue);

        //! Loads elevation data in a separate thread
        void _mtAddElevationData(const std::string &layerName, const std::string &url);

        //! Loads elevation data in a separate thread
        void _mtAddElevationData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel);

        //! Loads vector data in non editable mode using osgearth vector functionality
        void _addOsgEarthVectorData(const std::string &layerName, const std::string &url, bool clamping, std::string iconFileName = "");

        void _addNewOsgEarthVectorData(const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options);

        //! Loads vector data in a separate thread
        void _mtAddVectorData(const std::string& layerName, const std::string& url, CORE::RefPtr<DB::ReaderWriter::Options> options);

        //!
        void _mtAddDGNData(const std::string &layerName, const std::string &url, const std::string &orclUrl, osg::Vec2d extent, bool reloadCache);

        void _mtComputeMinMaxLOD(std::string fileName, unsigned int minLevel, unsigned int maxLevel);

        //!
        void _notifyStatusToObservers(IAddContentStatusMessage::AddContentStatus status, unsigned int progress);

        void _notifyComputeMinMaxToObservers(IAddContentComputeMinMaxLODMessage::AddContentComputeMinMaxLODStatus status, unsigned int minLOD, unsigned int maxLOD, unsigned int progress);

    private:

        //! XXX - terrain instance for the application
        CORE::RefPtr<CORE::ITerrain> _terrain;

        //! Current URL to load
        std::string _url;

        //! Scoped lock for URL
        OpenThreads::Mutex _mutex;

        bool isGoogleStreetDataAdded;
        bool isGoogleHybridDataAdded;

        //!
        double _offset;
        bool _clamping;

        //!
        CORE::RefPtr<CORE::IObject> _object;

        void(*progressFunctPtr)(int);

        int _progressValueForGUI;
    };
} // namespace SMCUI

