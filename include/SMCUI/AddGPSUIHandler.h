#ifndef _SMCUI_AddGPSUIHandler_H_
#define _SMCUI_AddGPSUIHandler_H_

#ifdef WIN32
/******************************************************************************
* File              : AddGPSUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : AddGPSUIHandler interface
* Author            : Gaurav Garg
 * Author email     : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <SMCUI/IAddGPSUIHandler.h>
#include <VizUI/UIHandler.h>
#include <GPS/IGPSSource.h>

namespace SMCUI
{    
    //! Implements the IAddGPSUIHandler interface
    class AddGPSUIHandler 
        : public IAddGPSUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
       
        public:
            
            AddGPSUIHandler();
            
            //! Loads the  GPS source
            void loadSource(const std::string& fileName = std::string());

            //old Version 
            //void loadSource();

            //! Sets the GPS source type
            void setCurrentType(Type sourceType);

            //! Gets the named attribute of the source
            CORE::Attribute* getSourceAttribute(const std::string& attrname) const;

        protected:

            ~AddGPSUIHandler();

        protected:

            //! Type of Source
            IAddGPSUIHandler::Type _type;

            //! Current source
            CORE::RefPtr<GPS::IGPSSource> _source;
    };
} // namespace SMCUI

#endif //WIN32

#endif // _SMCUI_AddGPSUIHandler_H_

