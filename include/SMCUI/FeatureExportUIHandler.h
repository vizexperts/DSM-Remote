#pragma once

/*****************************************************************************
 *
 * File             : FeatureExportUIHandler.h
 * Version          : 1.0
 * Module           : UI
 * Description      : FeatureExportUIHandler class
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : UI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <Core/IFeatureObject.h>

#include <VizUI/UIHandler.h>

#include <SMCUI/IFeatureExportUIHandler.h>

#include <osg/Vec4>

namespace SMCUI 
{
    class FeatureExportUIHandler : public VizUI::UIHandler
                                 , public IFeatureExportUIHandler
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

        public:

            //! Constructor
            FeatureExportUIHandler();

           //! export the file
            //bool exportGeoFeature(std::string& exportLayerName);
            //bool exportGeoRaster(std::string& exportLayerName);
            bool exportFeature(const std::string& filename);

            bool exportRaster(const std::string& filename);

            bool exportElevation(const std::string& filename);

            bool exportFeatureToTin(const std::string& filename);

            bool exportFeatureToGeoVrml(const std::string& filename);

            bool exportFeatureToRaster(const std::string& filename, const osg::Vec4& color, const osg::Vec4& nodataColor, double resolution);

            bool exportLineToCSV(const std::string& filename);

            bool exportPointLayerToCSV(const std::string& filename);

            bool exportGeoLayer(std::string& serverIp);
            //@{
            /** Set/Get GeorbISServer Port*/
            void setGeorbISServerPort(const int port);
            int getGeorbISServerPort() const;
            void setUploadingServerPort(const std::string& port);
            std::string getUploadingServerPort() const;
            //@}
            /**
            * request to fetch georbISServer Folder Dir
            * param serverIp string argument denoting server ip(i.e  https://192.168.0.1 )
            * /return param string folder Dir where service file will store
            */
            std::string georbISServerFolderDir(std::string& serverIp);

            // write to string use by curl as callback
            static  size_t write_to_string(void *ptr, size_t size, size_t count, void *stream);
            /**
            * request to check capability
            * param serverUrl string argument denoting server Url (i.e  https://192.168.0.1:19091/maps )
            * /return param boolean true if georbis server otherwise false
            */
            bool georbISServerCapability(std::string& serverUrl);

            void initializeAttributes();

            void setConvertModel(bool value);


        protected:
            void zipModel(std::string gltfPath, std::string fbxPath, std::string filename);

            //! get the current selected feature
            template <class T>
            T* _getCurrentSelectedItem();

            void _createOutputFile(const std::string& fullTempFilename, const std::string& filename, const osg::Vec4& color, const osg::Vec4&nodataColor, double resolution);

//            std::string dbname,pghost,port,user,pass;
            //variable to store GeoribIS server port from config
            int _georbISServerPort;
            std::string _uploadingServerPort;

            // flag to determine if to convert a model to gltf or not
            // this flag is used because we fbx2gltf converter was not working properly,
            // need to be fixed.
            bool convertModel;
    };
} // namespace UI

