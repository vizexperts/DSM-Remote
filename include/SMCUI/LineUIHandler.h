#pragma once

/******************************************************************************
* File              : LineUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for UI handler
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2010-2011 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <Core/Point.h>
#include <Core/Line.h>
#include <Core/IFeatureLayer.h>
#include <Core/IFeatureObject.h>
#include <Core/IPenStyle.h>
#include <Core/ICompositeObject.h>

#include <Elements/IMultiPoint.h>

#include <SMCUI/ILineUIHandler.h>

#include <VizUI/MouseHandler.h>

#include <osgUtil/LineSegmentIntersector>

#include <ogrsf_frmts.h>


namespace SMCUI 
{
    class SMCUI_DLL_EXPORT LineUIHandler 
        : public ILineUIHandler 
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        static const osg::Vec4 DefaultColor;
        static const double DefaultWidth;

        LineUIHandler();

        virtual bool setMode(LineMode mode);

        virtual LineMode getMode() const;

        //! Set Color of Current Line
        virtual void setColor(osg::Vec4 color);

        //! Get Color of Current Line
        virtual osg::Vec4 getColor() const;

        //! Set Width of Current Line
        virtual void setWidth(double width);

        //! Get Width of current Line
        virtual double getWidth() const;

        virtual std::vector<int>& getSelectedVertexList();
        virtual void setSelectedVertexList( std::vector<int> _selectedPointList );

        //@{
        /** Set/Get offsets */
        void setOffset(double offset);
        double getOffset() const;
        //@}

        //! Returns the current line instance
        CORE::ILine* getCurrentLine() const;

        //! Called when UIHandler added to manager
        void onAddedToManager();

        //@{
        //** Set focus after deleting the line object */
        void setFocus(bool value);
        //@}

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

        //! set the current selected line as current line
        void setSelectedLineAsCurrentLine();

        void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions) const;

        //! set the metadata record to the current line
        void setMetadataRecord(CORE::IMetadataRecord* record);

        //! get the metadata record of the current line
        CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const;

        //! update the current line
        void update();

        //! add line to current selected layer
        void addLineToCurrentSelectedLayer();

        void addLineToDefaultLayer();

        void addToLayer(CORE::IFeatureLayer *featureLayer);

        //! get current selected layer definition
        CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn();

        //! remove the created line
        void removeCreatedLine();

        //! delete last point from the line
        void deleteLastPointFromLine();

        //! set temporary state for the line
        void setTemporaryState(bool state);

        //! get temporary state for the line
        bool getTemporaryState() const;

        //! set clamping state for the point
        void setClampingState(bool state);

        //! get clamping state for the point
        bool getClampingState() const;

        //! set a line as the current line
        void setCurrentLine(CORE::ILine* line);

        void _reset();

        //! delete Point at index
        bool _deletePointAtIndex(unsigned int index);

        //! move point at index to position(long, lat, alt)
        void _movePointAtIndex(unsigned int index, osg::Vec3d position);

        // add point
        void _addPoint(osg::Vec3d position);

        // insert point
        void _insertPoint(unsigned int index, osg::Vec3 position);

        //! setProcessMouse events
        void _setProcessMouseEvents(bool value);

        // get process mouse events
        bool _getProcessMouseEvents() const;
        std::string jsonFile;


    protected:

        //! Handles the intersection
        void _handleMouseClickedIntersection(int button, const osg::Vec3d& longLatAlt);

        void _handleMouseReleased(int button);

        void _handleMouseDragged(int button, const osg::Vec3d& longLatAlt);

        void _resetMultiPoints();

        void _deleteCurrentSelectedLine();

        void _setCreateLineMode();

        void _setEditMode();

        void _setModeNone();

        void _setModeNoOperation();

        void _createEditPointUndoTransaction(unsigned int index);

        void _addLineToLayer(CORE::IFeatureLayer *featureLayer);

    protected:

        //! current mode of Line Handler
        LineMode _mode;

        //! reference of LineFeatureObject
        CORE::RefPtr<CORE::ILine> _line;

        //! Line color
        osg::Vec4 _lineColor;

        //! Line width
        double _lineWidth;

        //! Offset for the area
        double _offset;

        //! current selected Point
        int _currentSelectedPoint;

        CORE::RefPtr<ELEMENTS::IMultiPoint> _multiPoint;

    private:

        CORE::ILine* createLineFeatureObject();

        CORE::IObject* createFolderObject();

        CORE::RefPtr<CORE::IFeatureLayer> getOrCreateLineLayer();

        //! temporary state
        bool _temporary;

        //! clamping state
        bool _clamping;

        bool _dragged;

        bool _processMouseEvents;

        bool _handleButtonRelease;

        std::vector<int> _selectedPointList;
    };

} // namespace SMCUI

