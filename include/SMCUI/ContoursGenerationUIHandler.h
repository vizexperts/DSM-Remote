#ifndef SMCUI_CONTOURSGENERATIONUIHANDLER_H_
#define SMCUI_CONTOURSGENERATIONUIHANDLER_H_


/******************************************************************************
* File              : ContoursGenerationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class 
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/IContoursGenerationUIHandler.h>
#include <GISCompute/IContourFilterVisitor.h>
#include <VizUI/UIHandler.h>
#include <Terrain/IElevationObject.h>


namespace SMCUI
{
    class SMCUI_DLL_EXPORT ContoursGenerationUIHandler 
        : public IContoursGenerationUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        ContoursGenerationUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        virtual bool getFocus() const;

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Set the extents of the area */
        void setExtents(osg::Vec4d extents);
        osg::Vec4d getExtents() const;
        //@}
       //@{
        /** Set the ElevationInterval of the area */
        void setElevationInterval(double ElevationIntVal);
        double getElevationInterval() const;
        //@}
        /** Set the ElevationInterval of the area */
        void setOffset(double offset);
        double getOffset() const;
        //@}
        /** Set the color*/
        void setUserDefineColor(osg::Vec4d ocolor);
        osg::Vec4d getUserDefineColor() const;
        //@}
        //! start on ok
        virtual void execute();

        //!
        void stopFilter();

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //!
        void setElevationObject(TERRAIN::IElevationObject* obj);

        //!
        void setOutputName(std::string& name);

        void setStyle(const std::string &value);
        std::string getStyle();

    protected:

        ~ContoursGenerationUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

    protected:

        //! minmax visitor
        CORE::RefPtr<CORE::IBaseVisitor> _visitor;

        //! Array containing area points
        osg::ref_ptr<osg::Vec2Array> _extents;

        //!
        CORE::RefPtr<TERRAIN::IElevationObject> _elevObj;

        //!
        std::string name;
        //offset 
        double _elevationInterval;
        double _offSetVal;
        osg::Vec4d _color;

        // path of selected json styling
        std::string _styleJsonPath;
    };

} // namespace SMCUI

#endif // SMCUI_CONTOURSGENERATIONUIHANDLER_H_

