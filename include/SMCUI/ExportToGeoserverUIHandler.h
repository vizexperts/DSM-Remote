#pragma once

/******************************************************************************
* File                     : ExportToGeoserverUIHandler.h
* Version               : 1.0
* Module               : SMCUI
* Description         : UIHandler for publishing layers to geoserver
* Author                : Nitish Puri
* Author email       : nitish@vizexperts.com
* Reference          :
* Inspected by      :
* Inspection date  :

*******************************************************************************
* Copyright (c) 2012-2012 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/IExportToGeoserverUIHandler.h>

namespace SMCUI
{
    //! Used for publishing layers to Geoserver,
    //! Not being used anywhere right now
    class SMCUI_DLL_EXPORT ExportToGeoserverUIHandler
        : public IExportToGeoserverUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        ExportToGeoserverUIHandler();

        bool exportGeoFeature(std::string& exportLayerName);
        bool exportGeoRaster(std::string& exportLayerName);

    protected:

        void checkVecStore(std::string& path, std::string& ws, std::string& user, std::string& pass);
        void pushFeatureLayerToGeoserver(std::string& path, std::string& ws, std::string& user, std::string& pass, std::string& featureName, std::string& title);
        void changeFeatureLayerStyle(std::string& path, std::string& ws, std::string& user, std::string& pass, std::string& featureName, std::string& styleName);

        // write to string use by curl as callback
        static  size_t write_to_string(void *ptr, size_t size, size_t count, void *stream);

        std::string dbname, pghost, port, user, pass;

        //! get the current selected feature
        template <class T>
        T* _getCurrentSelectedItem();
        static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream);
    };

} // namespace SMCUI

