#ifndef SMCUI_IFLYAROUNDUIHANDLER_H_
#define SMCUI_IFLYAROUNDUIHANDLER_H_

/******************************************************************************
* File              : IFlyAroundUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for FlyAround UI Handler
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <Core/IPoint.h>
#include <VizUI/ICameraUIHandler.h>


namespace SMCUI 
{
    class IFlyAroundUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IFlyAroundUIHandler);
   
    public:

        //! Point updated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> ShowPlayButtonMessageType;

    public:

        /*
        struct to save flyAround parameters
        */
		struct FlyAround
		{
            std::string name;
			float distance;
			float height;
			double startAngle;
			double arcAngle;
            int reverse = 1;
			bool loop = false;
            float lookAtHeight;
		};

        enum Mode
        {
            Stop_Preview,
            Stop,
            Pause
        };

        /*
        Start function  to start the flyAround
        @param 1 - takes flyAround struct and @param 2 takes cameraUIHandler's pointer
        */
        virtual void start(FlyAround flyAround, CORE::RefPtr<VizUI::ICameraUIHandler> cameraUIHandler) = 0;

        /*
        Stop function  to stop the flyAround and resetting its initial state
        */
        virtual void stop () = 0;
		
        /*
        Pause function  to pause the flyAround
        */
        virtual void pause(Mode preview) = 0;

        /*
        Sets current selected IPoint over which flyAround is to be done
        @param 1 - takes IPoint
        */
		virtual void setPointCoordinates (CORE::IPoint* point) = 0;

        /*
        visualise function creates geometry of the flyAround arc and its corresponding distance and height lines from start point in the arc
        @param 1 - FlyAround
        */
        virtual void visualise(FlyAround flyAround) = 0;

        /*
        increaseSpeed function doubles up the current speed of flyAround
        */
        virtual void increaseSpeed() = 0;

        /*
        decreaseSpeed function divides up the current speed of flyAround by 2
        */
        virtual void decreaseSpeed() = 0;

        /*
        function to close flythrough and setting camera at geoJump level 
        */
        virtual void closePopUp() = 0;
        
    };

} // namespace SMCUI

#endif // SMCUI_IFLYAROUNDUIHANDLER_H_
