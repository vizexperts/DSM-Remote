#ifndef SMCUI_STEEPESTPATHUIHANDLER_H_
#define SMCUI_STEEPESTPATHUIHANDLER_H_


/******************************************************************************
* File              : SteepestPathUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class 
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/ISteepestPathUIHandler.h>
#include <VizUI/UIHandler.h>
#include <osgUtil/LineSegmentIntersector>
#include <osg/NodeCallback>
#include <osg/Array>
#include <queue>
#include <OpenThreads/ScopedLock>
#include <OpenThreads/Mutex>


namespace SMCUI
{
    class LineUpdateCallback : public osg::NodeCallback
    {
        public:

            LineUpdateCallback(){}            

            virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);

            //! osgArray Queue
            std::queue<osg::ref_ptr<osg::Vec3dArray> > arrayList;

            //! Lock for updating the queue
            OpenThreads::Mutex  mutex;

            //! Line
            CORE::RefPtr<CORE::ILine> line;

            //! offset for line output, xml configurable inputs
            double _offset;
    };

    
    class SMCUI_DLL_EXPORT SteepestPathUIHandler 
        : public ISteepestPathUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        SteepestPathUIHandler();
        
        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** returns true, if startingpoint is within Area specified otherwise false*/
        virtual bool setStartingPoint(const osg::Vec2d & point);
        //@}


        //@{
        /** set output name*/
        virtual void setOutputName(std::string& name);
        //@}


        //@{
        /** returns true, if Area Specified, contains the specified starting point,
        otherwise it returns false*/
        virtual bool setExtent(const osg::Vec2dArray& extents);
        //@}


        //@{
        /** end point for last notification for steep computation*/
        virtual osg::Vec2d& getCurrentPoint();
        //@}

        //@{
        /** get Starting point*/
        virtual osg::Vec2d& getStartingPoint();
        //@}

        virtual bool play(bool multiThreaded);

        virtual bool stop();

        //! Returns the play state of the filter
        virtual PlayState getPlayState();

        // Processes messages sent from observables
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //@{
        /** Sets focus of handler */
        void setFocus(bool value);
        //@}

        void initializeAttributes();

        virtual void setMaxLevel(double maxLevel);
        virtual double getMaxLevel();
        virtual void setCellSizeFactor(double cellSizeFactor);
        virtual double getCellSizeFactor();
        virtual void setUpdateInterval(double updateInterval);
        virtual double getUpdateInterval();
        virtual void setLineOffset(double offset);
        virtual double getLineOffset();

        //! get Output Line
        CORE::ILine* getOutput() const;

       // get Output  Points
       osg::Vec3dArray  *getOutputPoints() const ;

    protected:

        ~SteepestPathUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

        //! start Computing Steepes path
        void _computeSteepPathCalElevation();

    protected:

        //! add steepest path to the world
            void _addSteepestPathToScene(const osg::Vec3dArray& steepPointsArr);

            //! steepest path visitor
            CORE::RefPtr<GISCOMPUTE::ISteepPathCalcVisitor> _steepestPathVisitor;

            //! Line in which data is added
            CORE::RefPtr<CORE::ILine> _line;

            //! Output Line points 
            CORE::RefPtr<CORE::ILine> _outputLine;

            //! Line update callback
            osg::ref_ptr<LineUpdateCallback> _lineCallback;

            //! is it first update
            bool _bFirstUpdate;

            //! update Interval
            unsigned int _updateInterval;

            //! max Level for Pit Declaration
            unsigned short int _maxLevel;

            //! cell size factor to point find steepest path
            unsigned short int _cellSizeFactor;

            // output name
            std::string _name;

            //! offset for line output, xml configurable inputs
            double _offset;

            bool _bOnTickFirstUpdate;

            //! Lock for check the status of _bFirstUpdate, _bOnTickFirstUpdate
            OpenThreads::Mutex  _mutex;

            osg::ref_ptr<osg::Vec3dArray> _arrayofPoints;

    };

} // namespace SMCUI

#endif // SMCUI_STEEPESTPATHUIHANDLER_H_

