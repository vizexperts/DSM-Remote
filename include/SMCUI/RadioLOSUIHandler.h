#ifndef SMCUI_RADIOLOSUIHANDLER_H
#define SMCUI_RADIOLOSUIHANDLER_H


/******************************************************************************
* File              : RadioLOSUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Radio LOS UI handler
* Author            : Kumar Saurabh Arora
* Author email      : saurabh@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <Core/ILine.h>

#include <VizUI/UIHandler.h>
#include <SMCUI/IRadioLOSUIHandler.h>
#include <GISCompute/IRadioLOSVisitor.h>

#include <osg/NodeCallback>
#include <osg/Array>
#include <queue>
#include <iostream>
#include <OpenThreads/ScopedLock>
#include <OpenThreads/Mutex>

#ifdef INDIGISCOMPUTE_RADIOLOS_VERIFICATION
    #define UI_RADIOLOS_VERIFICATION 1
    #ifdef UI_RADIOLOS_VERIFICATION
        #include <fstream>
    #endif
#endif

namespace SMCUI 
{
    //-------------------------------------------------------------------------
    // Updatecallback for Line
    //-------------------------------------------------------------------------

    class RadioLOSCallback : public osg::NodeCallback
    {
        public:

            RadioLOSCallback() :
              obj(NULL)
            {
            }            

            virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);

            //!
            CORE::RefPtr<CORE::IObject> obj;

            //! Lock for updating the queue
            OpenThreads::Mutex  mutex;

    };

    class SMCUI_DLL_EXPORT RadioLOSUIHandler
        : public IRadioLOSUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        public:

            //! Constructor
            RadioLOSUIHandler();


            //@{
            /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
            void onAddedToManager();
            void onRemovedFromManager();
            //@}


            //! set Transmitter Position
            virtual void setTransmitterPosition(const osg::Vec3d& point);

            //! set Receiver Position
            virtual void setReceiverPosition(const osg::Vec3d& point);

            //! set Transmitter and Receiver Height
            virtual void setTransmitterHeight(double transHeight);
            virtual void setReceiverHeight(double recvHeight);

            //! set Transmitter Icon object
            virtual void setTrasmitterIcon(CORE::IObject* transObj);

            //! set Receiver Icon object
            virtual void setReceiverIcon(CORE::IObject* recvObj);

            //! set wave frequency in MHz
            virtual void setWaveFrequency(double frequency);

            //! set effective earth radius factor
            virtual void setEffectiveEarthRadiusFactor(double kFactor);

            //! set output name
            virtual void setOutputName(std::string& name);

            //! set Range Step
            virtual void setRangeStep(double rangeStep);

            //! set Tx Power in dB
            virtual void setTransmitterPower(double txPower);

            //! set Tx/Rx Antenna Gain in dB
            virtual void setAntennaGain(double txGain, double rxGain);

            //! set receiver senstivity in dB
            virtual void setReceiverSensitivity(double rxSensitivity);

            //! set cable loss in dB
            virtual void setCableLoss(double cableLoss);

            // get Outputs
            virtual bool losClearance();
            virtual double getOutputReceiverPower();
            virtual double getOutputSOM();
            virtual double getOutputFSL();
            virtual double getOutputEIRP();
            virtual std::string& getDecisionSupport();

            //! Processes messages sent from observables
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            //@{
            /** Sets focus of handler */
            void setFocus(bool value);
            //@}

            //! intialize attributes
            void initializeAttributes();

            //! set/get Height Increment Step
            void setHeightIncrementStep(double heightIncrementStep);
            double getHeightIncrementStep();


            //! Stops Filter
            void stopFilter();

            //! start Computing RadioLOS
            void execute(bool multiThreaded);


    protected:

            //! Resets visitor and all other params
            void _reset();

    private:



            //! radio LOS Visitor
            CORE::RefPtr<GISCOMPUTE::IRadioLOSVisitor> _radioLOSVisitor;

            //! update callback
            osg::ref_ptr<RadioLOSCallback> _radioLOSCallback;

            //! transmitter position
            osg::Vec3d _transmitterPos;

            //! receiver position
            osg::Vec3d _receiverPos;

            //! transmitter/receiver icon object
            CORE::RefPtr<CORE::IObject> _transObj;
            CORE::RefPtr<CORE::IObject> _recvObj;

            //! Tx Power in dB
            double _txPower;

            //! Tx/Rx Antenna Gain in dB
            double _txAntennaGain;
            double _rxAntennaGain;

            //! cable loss in dB per m
            double _cableLoss;

            //! receiver sensitivity in dB
            double _rxSensitivity;

            //! wave frequency in MHz
            double _frequency;

            //! k factor
            double _kFactor;

            //! range step
            double _rangeStep;

            //! output name
            std::string _outputName;

            //! height increment step
            double _heightIncrementStep;

            //! transmitter height
            double _transHeight;

            //! receiver height
            double _recvHeight;

    };

} // namespace SMCUI
#endif // SMCUI_RADIOLOSUIHANDLER_H_

