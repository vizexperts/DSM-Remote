#ifndef SMCUI_IMILITARYPATHANIMATIONUIHANDLER_H_
#define SMCUI_IMILITARYPATHANIMATIONUIHANDLER_H_

/*****************************************************************************
*
* File             : IMilitaryPathAnimationUIHandler.h
* Version          : 1.1
* Module           : SMCUI
* Description      : IMilitaryPathAnimationUIHandler interface declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Elements/ICheckpoint.h>
#include <Core/IMessageType.h>
#include <Elements/IMilitarySymbol.h>
#include <Core/ILine.h>
#include <Core/IObject.h>
#include <osgEarthSymbology/Geometry>
#include <osgEarthFeatures/Feature>

namespace SMCUI
{
    class IMilitaryPathAnimationUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IMilitaryPathAnimationUIHandler);

    public: 
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> MilitaryPathAttachedMessageType;

        enum AnimationMode
        {
            MODE_NONE = 0,
            MODE_ASSOCIATE_PATH = 1,
            MODE_REMOVE_PATH = 2,
            MODE_CHECKPOINT = 3
        };

        virtual void setMode(AnimationMode mode) = 0;

        virtual unsigned int getNumCheckpoints() = 0;

        virtual double getPathLength() = 0;

        virtual ELEMENTS::ICheckpoint* getCheckpoint(unsigned int cpID=0) = 0;

        virtual void removeCheckPoint(ELEMENTS::ICheckpoint* cp) = 0;

        virtual bool addCheckpoint(const boost::posix_time::ptime& arrivalTime, 
            const boost::posix_time::ptime& departureTime, double distance) = 0;

        virtual bool addCheckpoint(const boost::posix_time::ptime& arrivalTime,
            const boost::posix_time::ptime& departureTime, double distance, std::string animationType) = 0;

        virtual bool addCheckpoint(ELEMENTS::ICheckpoint* cp) = 0;

        virtual void highlightPointAtDistance(double distance) = 0;

        //! set the trail visibility while mission playing
        virtual void setTrailVisibility(bool value) = 0;

        //! get the trail visibility
        virtual bool getTrailVisibility() const = 0;

        //! Set checkpoint visibility
        virtual void setCheckpointVisibility(bool value) = 0;

        //! Get Checkpoint visibility
        virtual bool getCheckpointVisibility() const= 0;

        // get Object
        virtual CORE::RefPtr<ELEMENTS::IMilitarySymbol> getUnit() const= 0;

        // ! If file selected type is gpx than this function will give URL of the file.
        virtual const std::string& getGpxFileURL() const = 0;

        // ! If file selected type is gpx than this function will give ILine  inerface of line.
        virtual CORE::RefPtr<CORE::ILine> getLineFromGpx() const = 0;

        virtual CORE::IObject* createLineObjectfromFeature(const osgEarth::Features::Feature *feature) = 0;
    };
}   // namespace SMCUI
#endif // 
