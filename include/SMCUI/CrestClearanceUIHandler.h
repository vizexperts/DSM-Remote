#ifndef SMCUI_CREST_CLEARANCE_HANDLER_H_
#define SMCUI_CREST_CLEARANCE_HANDLER_H_

/*****************************************************************************
* File              : CrestClearanceUIHandler.h
* Version           : 1.0
* Module            : indiGISCompute
* Description       : Implementation class for crest clearance filter's UI handling
* Author            : Sumit Pandey
* Author email      : sumitp@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <GISCompute/export.h>
#include <osg/Array>
#include <osg/NodeCallback>
#include <GISCompute/ICrestClearanceVisitor.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/ICrestClearanceUIHandler.h>

#include <Core/ITerrain.h>
#include <Core/ILine.h>
#include <Core/ICompositeObject.h>
#include <Core/IMetadataTableDefn.h>


namespace SMCUI
{
    // RFE1 :  
    class CrestLineUpdateCallback : public osg::NodeCallback
    {
        public:

            CrestLineUpdateCallback(){}

            virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);

            void createMetadataRecord(CORE::ILine* lineObj, unsigned int index);

            //! osgArray Queue
            std::vector<GISCOMPUTE::ICrestClearanceVisitor::TrajectoryData> trajectoryList;

            //! Lock for updating the queue
            OpenThreads::Mutex  mutex;

            //! instance of world
            CORE::RefPtr<CORE::ICompositeObject> output;

            //! instance of table defn associated with the metatable
            CORE::RefPtr<CORE::IMetadataTableDefn> tableDefn;
    };

    
    //! interface used for calculating area of a polygon
    class CrestClearanceUIHandler : public SMCUI::ICrestClearanceUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

        public:

            CrestClearanceUIHandler();

            //@{
            /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
            void onAddedToManager();
            void onRemovedFromManager();
            //@}

            //@{
            /** Sets/gets the launching point */
            void setLaunchingPosition(osg::Vec3d point);
            osg::Vec3d getLaunchingPosition();
            //@}

            //@{
            /** Sets/gets the projection of direction of projectile on the ground */
            void setDirection(osg::Vec3d dir);
            osg::Vec3d getDirection();
            //@}

            //@{
            /** Sets/gets the target point */
            void setTargetPosition(osg::Vec3d point);
            osg::Vec3d getTargetPosition();
            //@}

            //@{
            /** Sets/gets the angle for TRAJECTORY_CALCULATION analysis */
            void setLaunchingAngle(double speed);
            double getLaunchingAngle();
            //@}

            //@{
            /** Sets/gets the velocity for TRAJECTORY_CALCULATION analysis */
            void setLaunchingSpeed(double speed);
            double getLaunchingSpeed();
            //@}

            //@{
            /** Sets/gets the stepsize for angle for ANGLE_AND_VELOCITY analysis */
            void setAngleStepSize(double delta);
            double getAngleStepSize();
            //@}

            //@{
            /** Sets/gets the min speed for ANGLE_AND_VELOCITY analysis */
            void setMinSpeed(double minSpeed);
            double getMinSpeed();
            //@}

            //@{
            /** Sets/gets the max speed for ANGLE_AND_VELOCITY analysis */
            void setMaxSpeed(double maxSpeed);
            double getMaxSpeed();
            //@}

            //@{
            /** Sets/gets the min speed for ANGLE_AND_VELOCITY analysis */
            void setMinAngle(double minAngle);
            double getMinAngle();
            //@}

            //@{
            /** Sets/gets the max speed for ANGLE_AND_VELOCITY analysis */
            void setMaxAngle(double maxSpeed);
            double getMaxAngle();
            //@}
        
            //@{
            /** Sets/gets the max speed for ANGLE_AND_VELOCITY analysis */

            
            //@{
            /** Sets/gets the weapons name for ANGLE_AND_VELOCITY analysis */
            void setWeaponName(std::string weaponName);
            std::string getWeaponName();
            //@}


            //@{
            /** Sets/gets the type of area to compute */
            void setComputationType(ComputationType type);
            ComputationType getComputationType();
            //@}

            //@{
            /** Set if to add object to world */
            void toAddGun(bool value);
            //@}

            void toAddCrest(bool value);

            //! Resets values of the visitor
            void setFocus(bool flag);

            //! Subscribe to filter messages
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            //! Executes the filter
            void execute();

            //! Stops the filter
            void stopFilter();


            //@{
            /** Sets/gets the min range of weapon*/
            void setMinRange(const double& minRange);
            double getMinRange() const;

            //@{
            /** Sets/gets the max range of weapon*/
            void setMaxRange(const double& maxRange);
            double getMaxRange() const;

            //@{
            /** get the calculated min max Velocity and displacement when trajectory calculation fails**/
            double getMinVelocity() const;
            double getMaxVelocity() const;
            double getDisplacement() const;

        protected:
        
            ~CrestClearanceUIHandler();
            //    RFE1 :  
            void _setUpLayerObject();
            void _createLayer(CORE::ICompositeObject* output);

        protected:
                
            //! ICrestClearanceVisitor 
            CORE::RefPtr<GISCOMPUTE::ICrestClearanceVisitor> _visitor;

            //! WeaponConfig 
            GISCOMPUTE::ICrestClearanceVisitor::WeaponConfig _weaponConfig;

            // RFE1 :  
             //! callback to add output of the filter in the layer
            CORE::RefPtr<CrestLineUpdateCallback> _lineCallback;

            //! callback to add output of the filter in the layer
            CORE::RefPtr<CrestLineUpdateCallback> _invalidLineCallback;

            // ! 
            CORE::RefPtr<CORE::ICompositeObject> _output;

            // !
            CORE::RefPtr<CORE::ICompositeObject> _invalidOutput;

            //! maintaining a count variable to set the name
            int _count;
            std::string _weaponName;

            bool _gunAdd;
            bool _crestAdd;
            

    };
} // namespace SMCUI

#endif  // SMCUI_CREST_CLEARANCE_HANDLER_H_
