#ifndef SMCUI_IDISTANCECALCULATIONUIHANDLER_H_
#define SMCUI_IDISTANCECALCULATIONUIHANDLER_H_

/*****************************************************************************
* File              : IDistanceCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for line distance calculation
* Author            : Sumit Pandey
* Author email      : sumitp@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

namespace SMCUI
{
    //! UIHandler for surface area ui handler
    class IDistanceCalculationUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IDistanceCalculationUIHandler);

    public:

        // Distance Calculated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> DistanceCalculationCompletedMessageType;

    public:

        enum ComputationType
        {
            //! for aerial distance calculation
            AERIAL,  
            //! for projected distance calculation
            PROJECTED,
            //! for an invalid visitor
            INVALID
        };

        //@{
        /** Sets/gets the points of the polygons to be processed */
        virtual void setPoints(osg::Vec3dArray* points) = 0;
        virtual osg::Vec3dArray* getPoints() = 0;
        //@}

        //@{
        /** Sets/gets the type of area to compute */
        virtual void setComputationType(ComputationType type) = 0;
        virtual ComputationType getComputationType() = 0;
        //@}

        virtual double getDistance() = 0;

        //! Executes the filter
        virtual void execute() = 0;

        //! Stops the execution of the filter
        virtual void stop() = 0;
    };
} // namespace SMCUI

#endif  // SMCUI_IDISTANCECALCULATIONUIHANDLER_H_
