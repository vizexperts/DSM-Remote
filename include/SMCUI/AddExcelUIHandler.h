#pragma once

/******************************************************************************
* File              : AddExcelUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : AddExcelUIHandler Implementation Class
* Author            : Abhinav Goyal
* Author email      : abhinav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
* Copyright (c) 2013-2014, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <VizUI/UIHandler.h>

#include <SMCUI/IAddExcelUIHandler.h>

namespace SMCUI {

       // XXX - should not be exported
    class SMCUI_DLL_EXPORT AddExcelUIHandler : public IAddExcelUIHandler, 
                                               public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
       
        public:
            
            AddExcelUIHandler();

            void readFile(const std::string &layerName, const std::string &url);

        protected:
    };
} 
// namespace SMCUI

