#pragma once

/******************************************************************************
* File              : IVectorLayerQueryUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Database Layer GUI Handler
* Author            : dharmendra
* Author email      : dharmendra.bhakar@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <DB/ReaderWriter.h>
#include <ogrsf_frmts.h>
namespace SMCUI
{
    /*!
    * \class IVectorLayerQueryUIHandler
    *
    * \brief UIHandler interface of class implementing loading vector layers base on Query from GUI
    *
    * \note 
    *
    * \author dharmendra bhakar
    *
    * \version 1.0
    *
    * Contact: dharmendra.bhakar@vizexperts.com
    * \Date July 2016
    */
    class IVectorLayerQueryUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IVectorLayerQueryUIHandler);
        /** 
        * Gets the attribute 
        * \param url a string argument denoting url from ModelObject layer
        *|return String Vector of Feature Attibute of Vector Shp file
        */
        virtual  std::vector<std::string> getFeatureAtribute(std::string url)=0;
        /**
        * set shape File path 
        * \param pathOfShapeFileName a string argument denoting url/shp File Name from ModelObject layer
        *|return None
        */
        virtual  void setShapeFilePathName(const std::string& pathOfShapeFileName) = 0;
        /**
        * set layer Name
        * \param layerName a string argument denoting layer Name
        *|return None
        */
        virtual  void setLayerName(const std::string& layerName) = 0;
        /**
        * get OGR layer Name
        * 
        *\return string, ogr Layer Name from URl
        */
        virtual  std::string getOGRLayerName() = 0;
        /**
        * to set sql query
        * \param query a string argument denoting sql query like Select * from where **** 
        */
        virtual  void setSQLQuery(const std::string& query) = 0;
        /**
        * to Execute sql query
        * \param runSpatialQuery a boolean argument denoting whether run Spaticial Query or Not
        *|return OGRLayer
        */
        virtual  OGRLayer* executeQueryStatement(bool runSpatialQuery = false) = 0;
        /**
        * execute Query Statement and extract shp file and reload in Project
        * \param runSpatialQuery a boolean argument denoting whether run Spaticial Query or Not
        */
        virtual  bool execute(bool runSpatialQuery = false)=0;
        /**
        * set extents for geometry area boundation
        * \param extents a Vec4d argument denoting Extens of Area
        */
        virtual  void setExtents(const osg::Vec4d& extents) = 0;
        /**
        * set style sheet name
        * \param  argument denoting style sheet name
        */
        virtual void setStyleSheet(const std::string&) = 0;
        /***
        * set output layer name
        * \param  outputName a string argument denoting output layer name
        */
        virtual void setVectorQueryOutputName(const std::string& outputName)=0;
        /**
        * reset member variable
        * \param None
        */
        virtual void reset()=0;
        /**
        * reset folder object
        * \param None
        */
        virtual void resetFolderObject()=0;
    };
}
