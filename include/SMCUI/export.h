#ifndef SMCUI_EXPORT_H_
#define SMCUI_EXPORT_H_

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#   if defined(SMCUI_LIBRARY)
#      define SMCUI_DLL_EXPORT __declspec(dllexport)
#   else
#      define SMCUI_DLL_EXPORT __declspec(dllimport)
#   endif 
#else
#   if defined(SMCUI_LIBRARY)
#      define SMCUI_DLL_EXPORT __attribute__ ((visibility("default")))
#   else
#      define SMCUI_DLL_EXPORT
#   endif 
#endif

#endif // SMCUI_EXPORT_H_
