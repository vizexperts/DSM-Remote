#ifndef SMCUI_IELEVATIONPROFILEUIHANDLER_H_
#define SMCUI_IELEVATIONPROFILEUIHANDLER_H_

/*****************************************************************************
* File              : IElevationProfileUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for surface area filter
* Author            : Rahul Srivastava
* Author email      : rahul@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <Core/ILine.h>

namespace SMCUI
{
    //! UIHandler for surface area ui handler
    class IElevationProfileUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IElevationProfileUIHandler);

        public:

            // Surface area message
            static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> ProfileCalculatedMessageType;;

        public:

            //@{
            /** Sets/gets the points of the line to be processed */
            virtual void setPoints(osg::Vec3dArray* points) = 0;
            virtual const osg::Vec3dArray* getPoints() const = 0;
            virtual const osg::Vec2dArray* getDistanceAltitudePoints() const = 0;
            //@}

            //! Returns the generate graph filename
            virtual const std::string& getGraphFilename() = 0;

            //@{
            //! Elevation profile statistics
            virtual osg::Vec3d getMinimumElevationPoint() const = 0;
            virtual osg::Vec3d getMaximumElevationPoint() const = 0;
            virtual double getAverageElevation() const = 0;
            //@}

            //! Executes the filter
            virtual void execute() = 0;

            //! Clears the line
            virtual void clearLine() = 0;

            virtual void stop() = 0;

            //! getLine
            virtual CORE::RefPtr<CORE::ILine> getLine() const = 0;

            //! set marking state
            virtual void setMarking(bool value) = 0;

            //! set select line state
            virtual void selectLine(bool value) = 0;

            virtual std::vector<std::string> getColorVector() = 0;
    };
} // namespace SMCUI

#endif  // SMCUI_IELEVATIONPROFILEUIHANDLER_H_
