#ifndef SMCUI_ITERRAINEXPORTSTATUSMESSAGE_H
#define SMCUI_ITERRAINEXPORTSTATUSMESSAGE_H

/******************************************************************************
 * File              : ITerrainExportStatusMessage.h
 * Version           : 1.0
 * Module            : SMCUI
 * Description       : ITerrainExportStatusMessage class declaration
 * Author            : Kamal Grover
 * Author email      : kamal@vizexperts.com
 * Reference         : 
 * Inspected by      : 
 * Inspection date   :  
 
 *******************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *******************************************************************************
 *
 * Revision log (latest on the top): 
 *
 ******************************************************************************/

#include <SMCUI/export.h>
#include <Core/CoreMessage.h>
#include <Core/IMessageType.h>

namespace SMCUI
{
    //! IFilter Status message type
    class ITerrainExportStatusMessage : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ITerrainExportStatusMessage);
        public:

            // Add Content message types
            static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> TerrainExportStatusMessageType;

            enum LayerStatus
            {
                IDLE,
                SUCCESS,
                FAILURE,
                PENDING,
                STOP_INITIATED
            };

        public:

            //@{
            /** Sets/Gets the add content status */
            virtual void setStatus(LayerStatus status) = 0;
            virtual LayerStatus getStatus() const = 0;
            //@}

            virtual void setProgress(unsigned int progress) = 0;
            virtual unsigned int getProgress() = 0;

            virtual void setLayerName(std::string layername) = 0;
            virtual std::string getLayerName() = 0;

        protected:

            ~ITerrainExportStatusMessage(){}
    };
}

#endif // SMCUI_ITERRAINEXPORTSTATUSMESSAGE_H
