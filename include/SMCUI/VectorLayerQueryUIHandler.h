#pragma once

/******************************************************************************
* File              : VectorLayerQueryUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Vector Layer Query Handler
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <SMCUI/IVectorLayerQueryUIHandler.h>
#include <SMCUI/IAddContentStatusMessage.h>
#include <VizUI/UIHandler.h>


namespace SMCUI
{
    /*!
    * \class VectorLayerQueryUIHandler
    *
    * \brief UIHandler for loading vector layers from Databse
    *
    * \note 
    *
    * \author Nitish Puri
    *
    * \version 1.0
    *
    * Contact: nitish@vizexperts.com
    *
    */
    class VectorLayerQueryUIHandler : public SMCUI::IVectorLayerQueryUIHandler, 
        public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public :

        VectorLayerQueryUIHandler();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage & message);
        // set url of Shp file
        void setShapeFilePathName(const std::string& pathOfShapeFile);
        //return url of shp file
        std::string getShapeFilePathName() const;
        // use to set extents of geometry
        void setExtents(const osg::Vec4d& extents);
        // execution query statement and return ogr Layer After Filter
        OGRLayer* executeQueryStatement(bool runSpatialQuery);
        // to set sql query like Select * from where **** 
        void setSQLQuery(const std::string& query);
        // set layer Name For Shp file Output
        void setLayerName(const std::string& layerName);
        // reurn ogr Layer
        OGRLayer* getOGRLayer();
        // return Feature Attibute of Vector Shp file
        std::vector<std::string> getFeatureAtribute(std::string url);
        //return ogr Layer Name from URl
        std::string getOGRLayerName();
        // execute Query Statement and extract shp file and reload in Project
        bool execute(bool runSpatialQuery = false);
        // set style sheet name
        void setStyleSheet(const std::string&);
        // output layer name
        void setVectorQueryOutputName(const std::string& outputName);
        // reset member variable
        void reset();
        // reset folder object
        void resetFolderObject();
    protected:

        void _mtAddVectorData(const std::string& layerName, const std::string& url, CORE::RefPtr<DB::ReaderWriter::Options> options);
        void _queryResultOutputFolder();
        OGRDataSource* getOGRDataSource();
        CORE::RefPtr<CORE::IObject> _objectToAdd;


        ~VectorLayerQueryUIHandler();

        //! Scoped lock for URL
        OpenThreads::Mutex _mutex;

        std::string _queryOutputName; // variable to store layerName
        std::string _layerName; // variable to store layerName
        std::string _styleSheetName; // variable to store stylesheetName
        std::string _pathOfShapeFile; // variable to store path of Shp File 
        std::string _vectorQueryOutputName; // output name
        std::string _sqlQuery; // variable to store sql query 

        //! result OGR Layer
        OGRLayer* _resultLayer; 
        //! extents
        osg::Vec4d _extents;
        //OGr Data source
        OGRDataSource *_ogrDataSource;

        osg::observer_ptr<CORE::IObject> _vectorQueryOutputFolder;
        const std::string _shapeFileStoragePath;

    };
}
