#ifndef SMCUI_ADDCONTENTSTATUSMESSAGE_H
#define SMCUI_ADDCONTENTSTATUSMESSAGE_H

/******************************************************************************
 * File              : AddContentStatusMessage
 * Version           : 1.0
 * Module            : SMCUI
 * Description       : AddContentStatusMessage class declaration
 * Author            : Gaurav Garg
 * Author email      : gaurav@vizexperts.com
 * Reference         : 
 * Inspected by      : 
 * Inspection date   :  
 
 *******************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *******************************************************************************
 *
 * Revision log (latest on the top): 
 *
 ******************************************************************************/

#include <SMCUI/export.h>
#include <Core/CoreMessage.h>
#include <SMCUI/IAddContentStatusMessage.h>

namespace SMCUI
{
    //! Filter Status message type
    class AddContentStatusMessage 
        : public CORE::CoreMessage
        , public SMCUI::IAddContentStatusMessage
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        public:

            AddContentStatusMessage();

            //@{
            /** Sets/Gets the filter status */
            void setStatus(AddContentStatus status);
            AddContentStatus getStatus() const;
            //@}

            void setProgress(unsigned int progress);
            unsigned int getProgress();

            void setSkippedFileList(std::vector<std::string>& skippedFiles);
            const std::vector<std::string>& getSkippedFileList() const;
            std::vector<std::string>& getSkippedFileList();


        protected:

            ~AddContentStatusMessage();

        protected:

            //! Filter status
            AddContentStatus _status;

            //! progress
            unsigned int _progress;

            std::vector<std::string> _skippedFiles;
    };
}

#endif // SMCUI_ADDCONTENTSTATUSMESSAGE_H
