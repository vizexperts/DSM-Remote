#ifndef SMCUI_ICOLORELEVATIONUIHANDLER_H_
#define SMCUI_ICOLORELEVATIONUIHANDLER_H_

/******************************************************************************
* File              : IColorElevationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Slope aspect filter UI Handler
* Author            : Gaurav Gar
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Elements/IColorMap.h>
#include <GISCompute/IColorCodedElevFilterVisitor.h>

namespace ELEMENTS
{
    class IColorMap;
}

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI 
{
    class IColorElevationUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IColorElevationUIHandler);

    public:

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;
        //@}
        //Set Map size
        virtual void setMapSize(int mapSize) = 0;
        //! Set Color Map Type
        virtual  void setColorMapType(ELEMENTS::IColorMap::COLORMAP_TYPE colorMapType) = 0;

        virtual void setElevationObject(TERRAIN::IElevationObject* obj) = 0;

        virtual void setOutputName(std::string& name) = 0;

        virtual void execute() = 0;

        virtual void stopFilter() = 0;
        virtual void computeMinMaxElevation() = 0;
        virtual void setColorMap(ELEMENTS::IColorMap* colorMap) = 0;
    };

} // namespace SMCUI

#endif // SMCUI_IColorElevationUIHANDLER_H_
