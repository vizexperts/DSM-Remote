#pragma once

/*****************************************************************************
*
* File             : MilitaryPathAnimationUIHandler.h
* Version          : 1.1
* Module           : SMCUI
* Description      : MilitaryPathAnimationUIHandler interface declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*****************************************************************************/

// include files

#include <SMCUI/IMilitaryPathAnimationUIHandler.h>
#include <VizUI/UIHandler.h>

#include <Elements/IPath.h>
#include <Elements/IMilitarySymbol.h>

#include <Core/ISelectionComponent.h>
#include <Core/IPoint.h>
#include <Core/Base.h>
#include <Core/IObjectFactory.h>


namespace SMCUI
{
    class SMCUI_DLL_EXPORT MilitaryPathAnimationUIHandler 
        : public IMilitaryPathAnimationUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        MilitaryPathAnimationUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        void setMode(IMilitaryPathAnimationUIHandler::AnimationMode mode);

        //!
        unsigned int getNumCheckpoints();

        double getPathLength();

        ELEMENTS::ICheckpoint* getCheckpoint(unsigned int cpID=0);

        void removeCheckPoint(ELEMENTS::ICheckpoint* cp);

        bool addCheckpoint(const boost::posix_time::ptime& arrivalTime, 
            const boost::posix_time::ptime& departureTime, double distance);

        bool addCheckpoint(const boost::posix_time::ptime& arrivalTime,
            const boost::posix_time::ptime& departureTime, double distance, std::string animationType);

        bool addCheckpoint(ELEMENTS::ICheckpoint* cp);

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        //@}

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void highlightPointAtDistance(double distance);

        //! set the trail visibility while mission playing
        void setTrailVisibility(bool value);

        //! get the trail visibility
        bool getTrailVisibility() const;

        //! Set checkpoint visibility
        void setCheckpointVisibility(bool value);

        //! Get Checkpoint visibility
        bool getCheckpointVisibility() const;

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> getUnit() const;

        // ! If file selected type is gpx than this function will give URL of the file.
        const std::string& getGpxFileURL() const;

        // ! If file selected type is gpx than this function will give URL of the file.
        CORE::RefPtr<CORE::ILine> getLineFromGpx() const;

        //! This function is called for createing when a gpx plotted path is selected in attach path
        // It can be selected in three types
        //
        CORE::IObject* createLineObjectfromFeature(const osgEarth::Features::Feature *feature);
    protected:

        ~MilitaryPathAnimationUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

        //!
        void _createPath();

        void _clampPoint(osg::Vec3d& position);

        //!
        void _showPointAtDistance(osg::Vec3d position);

    protected:

        AnimationMode _mode;

        //! selection component
        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

        CORE::RefPtr<CORE::IPoint> _unit;

        CORE::RefPtr<ELEMENTS::IMilitarySymbol> _milSymbol;

        CORE::RefPtr<ELEMENTS::IPath> _path;

        //! point
        CORE::RefPtr<CORE::IPoint> _point;
        
        CORE::RefPtr<CORE::ILine> _gpsLine;

        std::string _attachFileURL;

    };

} // namespace SMCUI

