#pragma once

/*****************************************************************************
 *
 * File             : IAlmanacUIHandler.h
 * Version          : 1.0
 * Module           : UI
 * Description      : IAlmanacUIHandler class
 * Author           : Rahul Srivastava
 * Author email     : rahul@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <App/IUIHandler.h>

#include <SMCUI/export.h>

#include <boost/date_time/posix_time/posix_time_types.hpp>

#include <osg/Vec3>


namespace CORE
{
    class Attribute;
}

namespace SMCUI 
{
    //! Almanac Data
    struct AlmanacData
    {
        boost::posix_time::ptime::time_duration_type sunriseTime;
        boost::posix_time::ptime::time_duration_type sunsetTime;
        boost::posix_time::ptime::time_duration_type moonriseTime;
        boost::posix_time::ptime::time_duration_type moonsetTime;

        double moonPhase;
        double timeToNewMoon;
    };

    //! Generates almanac data containing times for sunset, sunrise
    //! moonset, moonrise and moon phase
    class IAlmanacUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAlmanacUIHandler);

        public:
            
            //! Sets the position at which almanac needs to be calculated
            virtual void setPosition(const osg::Vec3& longlatAlt) = 0;

            //! Gets the position at which almanac needs to be calculated
            virtual const osg::Vec3 getPosition() const = 0;

            //! Sets the date and time at which almanac needs to be calculated
            virtual void setDateTime(const boost::posix_time::ptime& stime) = 0;

            //! Gets the date and time at which almanac needs to be calculated
            virtual const boost::posix_time::ptime& getDateTime() const = 0;

            //! Computes the almanac parameters
            virtual void computeAlmanac() = 0;

            //! Returns the alamanc parameters
            virtual const AlmanacData& getAlmanacData() const = 0;

            //! Set/Get GMT Time
            virtual void setGMTTime(const double&) = 0;

            virtual double getGMTTime() = 0;
    };
} // namespace UI

