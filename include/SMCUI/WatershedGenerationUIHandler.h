#ifndef SMCUI_WATERSHEDGENERATIONUIHANDLER_H_
#define SMCUI_WATERSHEDGENERATIONUIHANDLER_H_


/******************************************************************************
* File              : WatershedGenerationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class 
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/IWatershedGenerationUIHandler.h>
#include <GISCompute/ITauDEMWatershedVisitor.h>
#include <VizUI/UIHandler.h>
#include <Terrain/IElevationObject.h>


namespace SMCUI
{
    class SMCUI_DLL_EXPORT WatershedGenerationUIHandler 
        : public IWatershedGenerationUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        WatershedGenerationUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        virtual bool getFocus() const;

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //! get/set stream shapefile
        void setStreamOutputName(std::string& name);
        //! get watershed raster
        void setWatershedOutputName(std::string& name);


         //! set area extent in longitude/latitude
        void setExtents(osg::Vec4d extents);
        osg::Vec4d getExtents() const;

        //@{
        /** Set the extents of the outlet area */
        void setOutletExtents(osg::Vec4d extents) ;
        osg::Vec4d getOutletExtents() const ;
        //@}

        //! To specify the Threshold
        void setThreshold(double value);
        double getThreshold() const;

        //! start on ok
        virtual void execute();

        //!
        void stopFilter();

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //!
        void setElevationObject(TERRAIN::IElevationObject* obj);

    protected:

        ~WatershedGenerationUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

    protected:

        //! minmax visitor
        CORE::RefPtr<CORE::IBaseVisitor> _visitor;

        //! area extent
        osg::ref_ptr<osg::Vec2Array> _areaExtent;

        //! outlet region area extent
        osg::ref_ptr<osg::Vec2Array> _outletExtents;

        //! Parameter for stream network generation
        double _threshold;

        //!
        CORE::RefPtr<TERRAIN::IElevationObject> _elevObj;

        //! watershed raster output name
        std::string _watershedname;

        //! stream network output name
        std::string _streamname;

    };

} // namespace SMCUI

#endif // SMCUI_WATERSHEDGENERATIONUIHANDLER_H_

