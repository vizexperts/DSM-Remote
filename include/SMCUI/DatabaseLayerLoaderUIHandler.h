#pragma once

/******************************************************************************
* File              : DatabaseLayerLoaderUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Database Layer GUI Handler
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <SMCUI/IDatabaseLayerLoaderUIHandler.h>
#include <SMCUI/IAddContentStatusMessage.h>
#include <VizUI/UIHandler.h>
#include <Database/IDatabase.h>
#include <DB/ReaderWriter.h>


namespace SMCUI
{
    /*!
    * \class DatabaseLayerLoaderUIHandler
    *
    * \brief UIHandler for loading vector layers from Databse
    *
    * \note 
    *
    * \author Nitish Puri
    *
    * \version 1.0
    *
    * Contact: nitish@vizexperts.com
    *
    */
    class DatabaseLayerLoaderUIHandler : public SMCUI::IDatabaseLayerLoaderUIHandler, 
        public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public :

        static const std::string IncidentLayerName;

        DatabaseLayerLoaderUIHandler();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage & message);

        /**
        * Load layer with given options
        */
        void loadLayer(CORE::RefPtr<DB::ReaderWriter::Options> options, const DBType dbType);

        /**
        * Execute SQL query with specified connected data source
        * TBD: User defined Data sources to be included
        */
        CORE::RefPtr<DATABASE::ISQLQueryResult> executeSQLQuery(const std::string& queeryStatement, const DBType dbType);

        void initializeAttributes();

        bool connectToDB(const DBType dbType);

        // add value in Symbol type map
        void setServer(const CORE::NamedGroupAttribute& groupattr);

        CORE::RefPtr<CORE::NamedGroupAttribute> getServer() const;

        void setOracleDBName(const std::string& dbName);
        std::string getOracleDBName() const;

        void setMssqlDBName(const std::string& dbName);
        std::string getMssqlDBName() const;


        static void setProgress(int value);

    protected:

        void _notifyStatusToObservers(IAddContentStatusMessage::AddContentStatus status, unsigned int progress);

        OpenThreads::Mutex      _featuresToAddMutex;
        //std::vector<CORE::RefPtr<CORE::IObject>> _addToWorld;
        CORE::RefPtr<CORE::IObject> _objectToAdd;

        void (*_progressFunctPtr)(int);

        //void _mtLoadBPLayer(std::string userspaceName, std::string tableName);
        void _mtLoadBPLayer(CORE::RefPtr<DB::ReaderWriter::Options> options);
        void _mtLoadBPLayerMssql(CORE::RefPtr<DB::ReaderWriter::Options> options);


        ~DatabaseLayerLoaderUIHandler();

        bool _isInitializedOracle;
        bool _isInitializedMssql;

        static int _progressValue;
        static bool _progressChanged;

        std::string _serverNameOracle;
        std::string _serverNameMssql;

        CORE::RefPtr<DATABASE::IDatabase> _databaseOracle;
        CORE::RefPtr<DATABASE::IDatabase> _databaseMssql;

    };
}
