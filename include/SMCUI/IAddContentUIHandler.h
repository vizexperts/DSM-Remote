#pragma once

/*****************************************************************************
*
* File             : IAddContentUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : IAddContentUIHandler class
* Author           : Gaurav Garg
* Author email     : gaurav@vizexperts.com
* Reference        : SMCUI module
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/


#include <App/IUIHandler.h>
#include <SMCUI/export.h>
#include <Util/BaseExceptions.h>
#include<Util/ShpFileUtils.h> 
#include <Core/IMessageType.h>
#include <Core/IObject.h>
#include <osg/Vec2d>
#include <osg/Vec4d>
#include <osg/Vec4>
#include <Terrain/IRasterObject.h>
#include <DB/ReaderWriter.h>

namespace CORE
{
    class ITerrain;
}

//! \brief  Handles add content gui events from the GUI
namespace SMCUI
{
    class IAddContentUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAddContentGUIHandler);
    public:

        // Progress message
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> DataLoadProgressUpdateType;

        //! AddContent exceptions
        typedef UTIL::Exception AddRasterException;
        typedef UTIL::Exception AddVectorException;

        enum GoogleDataType
        {
            GOOGLE_STREETS,
            GOOGLE_HYBRID
        };

    public:

        virtual void addImageOverlay(const std::string &url, osg::Vec4d &extents, std::string rotation, int subdataset) = 0;
        virtual void getGdalSubsets(const std::string &url, std::vector<std::string> &list) = 0;

        //@{
        /** Handler functions for different types of data */


        virtual void addMultibandHDFData(const std::string &layerName,
            const std::string &url,
            osg::Vec4d &extents,
            bool override_extents,
            unsigned int minLevel,
            unsigned int maxLevel,
            std::vector<int> subdatasetVector, bool hasNoData, double noDataValue) = 0;

        virtual void addMultibandRasterData(std::string layerName, std::string url1, std::string url2, std::string url3,
            osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel, bool hasNoData, double noDataValue) = 0;

        virtual void addRasterData(const std::string &layerName, const std::string &url) = 0;
        virtual void addRasterData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel
            , int subdataset, const std::string &enhanceType, const std::string &projectionType) = 0;

        virtual void addVectorData(const std::string &layerName, const std::string &url, CORE::RefPtr<DB::ReaderWriter::Options> options) = 0;

        virtual void addElevationData(const std::string &layerName, const std::string &url) = 0;
        virtual void addElevationData(const std::string &layerName, const std::string &url, osg::Vec4d &extents, bool override_extents, unsigned int minLevel, unsigned int maxLevel) = 0;

        virtual void addDGNData(const std::string &layerName, const std::string &url, const std::string &orclUrl, osg::Vec2d extents, bool reloadCache) = 0;
        virtual void computeMinMaxLOD(std::string fileName, unsigned int minLevel, unsigned int maxLevel) = 0;

        //! Get Extents of given file
        virtual osg::Vec4 defaultExtentsQuery(const std::string& fileName) = 0;

        virtual void setImageDraggerVisibility(bool value) = 0;
        virtual void setImageRotateVisibility(bool value) = 0;

        virtual void addOSMWebData(std::string url) = 0;
        virtual void removeOSMWebData() = 0;

        virtual void addYahooWebData(std::string datasetOption) = 0;
        virtual void removeYahooWebData() = 0;

        virtual void addWorldWindWebData(std::string url) = 0;
        virtual void removeWorldWindWebData() = 0;

        virtual void addBingWebData(std::string url) = 0;
        virtual void removeBingWebData() = 0;

        virtual bool addGoogleData(const GoogleDataType& type) = 0;
        virtual void removeGoogleData(const GoogleDataType& type) = 0;

        virtual void setProgressValue(int value) = 0;
        virtual int getProgressValue() = 0;
        virtual void reorderRasterBands(CORE::IObject* object, const std::vector<int>& bandOrder) = 0;

        virtual void changeRasterEnhancementType(CORE::IObject* object, const TERRAIN::IRasterObject::EnhancementType enhanceType) = 0;

        //! \brief updates modelObject with new style Template
        //! \param IObject
        //! \param template Name 
        virtual void updateModelObject(CORE::IObject* object, std::string templateName) = 0; 
        //@}

        virtual void updateMarkingsObject(CORE::IObject *object, std::string jsonFile) = 0;
    };
} // namespace SMCUI

