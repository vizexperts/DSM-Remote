#ifndef SMCUI_DISTANCECALCULATIONUIHANDLER_H_
#define SMCUI_DISTANCECALCULATIONUIHANDLER_H_

/*****************************************************************************
* File              : DistanceCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for surface area filter
* Author            : Sumit Pandey
* Author email      : sumitp@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <SMCUI/export.h>
#include <osg/Array>
#include <GISCompute/IDistanceCalculationVisitor.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/IDistanceCalculationUIHandler.h>

namespace SMCUI
{
    //! interface used for calculating area of a polygon
    class DistanceCalculationUIHandler : public IDistanceCalculationUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        DistanceCalculationUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Sets/gets the points of the polygons to be processed */
        void setPoints(osg::Vec3dArray* points);
        osg::Vec3dArray* getPoints();
        //@}

        //@{
        /** Sets/gets the type of area to compute */
        void setComputationType(ComputationType type);
        ComputationType getComputationType();
        //@}

        //! Returns the value of the surface's area in sq km
        double getDistance();

        //! Resets values of the visitor
        void setFocus(bool flag);

        //! Subscribe to filter messages
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Executes the filter
        void execute();

        //! Stops the exeecution of filter
        void stop();

    protected:

        ~DistanceCalculationUIHandler();

    protected:

        //! ISurfaceAreaVisitor 
        CORE::RefPtr<GISCOMPUTE::IDistanceCalculationVisitor> _visitor;
    };
} // namespace SMCUI

#endif  // SMCUI_DISTANCECALCULATIONUIHANDLER_H_
