#ifndef SMCUI_TOURUIHANDLER_H_
#define SMCUI_TOURUIHANDLER_H_

/*****************************************************************************
*
* File             : TourUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : TourUIHandler class
* Author           : Abhinav Goyal
* Author email     : abhinav@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013-2014, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/RefPtr.h>
#include <Core/UniqueID.h>

#include <SMCElements/ITourComponent.h>

#include <VizUI/ICameraUIHandler.h>
#include <VizUI/UIHandler.h>

#include <SMCUI/ITourUIHandler.h>
#include <SMCUI/export.h>
#include <osgText/Text>

namespace SMCUI 
{
    class TourUIHandler : public ITourUIHandler,
        public VizUI::UIHandler,
        public VizUI::IKeyboardHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        TourUIHandler();

        void onAddedToManager();

        void onRemovedFromManager();

        ///Record Related Functions
        bool startTourRecording() ;

        bool stopTourRecording() ;

        bool pauseTourRecording() ;

        bool saveCurrentRecordedTour(const std::string &tourName, bool overwrite=false);

        //! Save a tour sequence 
        void saveSequence(const std::string& sequenceName, const std::vector<std::string>& sequence);

        void deleteSequence(const std::string& sequencename);

        void playSequence(const std::string& sequenceName);

        void playNextTourInCurrentSequence();

        void playPreviousTourInCurrentSequence();

        void setSequenceName(const std::string& oldSequenceName, const std::string& newSequenceName);

        SMCElements::ITourComponent::RecorderState getRecordState() const;
        //End Record

        //Play Related Functions
        bool playTour(const std::string &tourName);

        // Delete tour
        bool deleteTour(const std::string& tourName);

        // Pause current playing tour
        bool pauseTour() ;

        // Stop the current playing tour
        bool stopTour() ;

        // get the duration of current loaded tour of tour component
        unsigned int getTourDuration() ;

        // set the tour duration(To provide seek functionality while tour is playing)
        void setTourCurrTime(unsigned int currTime) ;

        // to show a snap of tour at the time duration specified
        void previewAtCheckPoint(unsigned int duration); 

        //get the state of the tour in case it is in playMode
        SMCElements::ITourComponent::PlayerState getPlayerState() const;
        //End Play related functions

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //get the state of the tour
        SMCElements::ITourComponent::TourMode getTourState() const;

        const SMCElements::ITourComponent::MapTourToFile& getTourMap() const;

        //! give list of all available sequence
        const SMCElements::ITourComponent::MapSequenceToTourList &getSequenceList() const;

        std::string getCurrentTourName() const;

        std::string getCurrentSequenceName() const;

        /*! 
        \fn bool setTourName( std::string oldTourName, std::string newTourName )
        \brief This function will set the name of the tour from oldName to newName.
        \param oldName : Old name of the tour.
        \param newName : New name of the tour
        \return Returns true if the name has been changed correctly.
        \author vikram singh
        \date Oct 17th 2013
        */
        virtual bool setTourName( std::string oldTourName, std::string newTourName );

        unsigned int getTourCurrentTime() const;

        //! get the speed factor
        float getSpeedFactor() const;

        //! set the speed factor
        void setSpeedFactor(float speedFactor);

        //! double the current speed factor
        void increaseSpeedFactor();

        //! half the current speed factor
        void decreaseSpeedFactor();

        void reset();

        void resetAll();

        //! set the recorder type
        void setRecorderType(SMCElements::ITourComponent::RecorderType type);

        //! get the recorder type
        SMCElements::ITourComponent::RecorderType getRecorderType() const;

        //! handle key press
        bool handleKeyPressed(VizUI::IKeyboard* keyboard, int key);

        //! handle key release
        bool handleKeyReleased(VizUI::IKeyboard* keyboard, int key);

        //! record a frame while in User Keyframe mode
        void recordKeyframe();

        //check point related functions
        void createNewCheckPoint(unsigned int duration);
        void editCheckPoint(unsigned int oldDuration, unsigned int newDuration, double newSpeed);
        void deleteCheckPoint(unsigned int duration);
        void resetSpeedProfile();
        void saveSpeedProfile();
        SMCElements::ITourComponent::MapTimeToSpeed getCheckPoints();

    protected:

        void _reset();

        void _createHUD();

        ~TourUIHandler();

        CORE::RefPtr<SMCElements::ITourComponent> _tourComponent;

        osg::ref_ptr<osg::Camera> _hudCamera;

        osg::ref_ptr<osgText::Text> _tourNameText;
        osg::ref_ptr<osgText::Text> _tourTimeText;
        osg::ref_ptr<osgText::Text> _helpText;

        bool _handlePlaybackControls;

        //!
        bool _projectLoaded;

        //!
        bool _createHud;

    };
} // namespace SMCUI

#endif // SMCUI_IPOINTUIHANDLER_H_
