#ifndef SMCUI_ICRESTCLEARANCE_UIHANDLER_H_
#define SMCUI_ICRESTCLEARANCE_UIHANDLER_H_

/*****************************************************************************
* File              : ICrestClearanceUIHandler.h
* Version           : 1.0
* Module            : indiGISCompute
* Description       : Interface class for crest clearance filter's UI handling
* Author            : Sumit Pandey
* Author email      : sumitp@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

namespace SMCUI 
{
    //! UIHandler for surface area ui handler
    class ICrestClearanceUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ICrestClearanceUIHandler);

        public:

            // Surface area message
            static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> CrestClearanceCompletedMessageType;

        public:

            enum ComputationType 
            {
                TRAJECTORY_CALCULATION,
                ANGLE_AND_VELOCITY,
                INVALID
            };

            //@{
            /** Sets/gets the launching point */
            virtual void setLaunchingPosition(osg::Vec3d point) = 0;
            virtual osg::Vec3d getLaunchingPosition() = 0;
            //@}

            //@{
            /** Sets/gets the target point */
            virtual void setTargetPosition(osg::Vec3d point) = 0;
            virtual osg::Vec3d getTargetPosition() = 0;
            //@}

            //@{
            /** Sets/gets the projection of direction of projectile on the ground */
            virtual void setDirection(osg::Vec3d dir) = 0;
            virtual osg::Vec3d getDirection() = 0;
            //@}

            //@{
            /** Sets/gets the angle for TRAJECTORY_CALCULATION analysis */
            virtual void setLaunchingAngle(double speed) = 0;
            virtual double getLaunchingAngle() = 0;
            //@}

            //@{
            /** Sets/gets the velocity for TRAJECTORY_CALCULATION analysis */
            virtual void setLaunchingSpeed(double speed) = 0;
            virtual double getLaunchingSpeed() = 0;
            //@}

            //@{
            /** Sets/gets the stepsize for angle for ANGLE_AND_VELOCITY analysis */
            virtual void setAngleStepSize(double delta) = 0;
            virtual double getAngleStepSize() = 0;
            //@}

            //@{
            /** Sets/gets the min speed for ANGLE_AND_VELOCITY analysis */
            virtual void setMinSpeed(double minSpeed) = 0;
            virtual double getMinSpeed() = 0;
            //@}

            //@{
            /** Sets/gets the max speed for ANGLE_AND_VELOCITY analysis */
            virtual void setMaxSpeed(double maxSpeed) = 0;
            virtual double getMaxSpeed() = 0;
            //@}


            //@{
            /** Sets/gets the weapons name for ANGLE_AND_VELOCITY analysis */
            virtual void setWeaponName(std::string weaponName) = 0;
            virtual std::string getWeaponName() = 0;
            //@}

            //@{
            /** Sets/gets the min speed for ANGLE_AND_VELOCITY analysis */
            virtual void setMinAngle(double minAngle) = 0;
            virtual double getMinAngle() = 0;
            //@}

            //@{
            /** Sets/gets the max speed for ANGLE_AND_VELOCITY analysis */
            virtual void setMaxAngle(double maxSpeed) = 0;
            virtual double getMaxAngle() = 0;
            //@}

            //@{
            /** Sets/gets the type of area to compute */
            virtual void setComputationType(ComputationType type) = 0;
            virtual ComputationType getComputationType() = 0;
            //@}

            //@{
            /** Set if to add object to world */
            virtual void toAddGun(bool value) = 0;
            //@}

             //@{
            /** Set if to add object to world */
            virtual void toAddCrest(bool value) = 0;
            //@}

            //@{
            /** Sets/gets the min range of weapon*/
            virtual void setMinRange(const double& minRange) = 0;
            virtual double getMinRange() const = 0;

            //@{
            /** Sets/gets the max range of weapon*/
            virtual void setMaxRange(const double& maxRange) = 0;
            virtual double getMaxRange() const = 0;

            //@{
            /** get the calculated min max Velocity and displacement when trajectory calculation fails**/
            virtual double getMinVelocity() const = 0;
            virtual double getMaxVelocity() const = 0;
            virtual double getDisplacement() const = 0;



        //! Executes the filter
        virtual void execute() = 0;

        //! Stop the filter
        virtual void stopFilter() = 0;
    };
} // namespace indiGISCompute

#endif  // SMCUI_ICRESTCLEARANCE_UIHANDLER_H_
