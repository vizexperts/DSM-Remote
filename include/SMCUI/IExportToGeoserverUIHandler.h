#pragma once

/******************************************************************************
* File                     : IExportToGeoserverUIHandler.h
* Version               : 1.0
* Module               : SMCUI
* Description         : UIHandler Interface for publishing layers to geoserver
* Author                : Nitish Puri
* Author email       : nitish@vizexperts.com
* Reference          :
* Inspected by      :
* Inspection date  :

*******************************************************************************
* Copyright (c) 2012-2012 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <Core/IObject.h>
#include <osg/Vec4>
#include <osg/Vec3>

namespace SMCUI
{
    //! Used for publishing layers to Geoserver,
    //! Not being used anywhere right now
    class IExportToGeoserverUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IExportToGeoserverUIHandler);

    public:
        virtual bool exportGeoFeature(std::string& exportLayerName) = 0;
        virtual bool exportGeoRaster(std::string& exportLayerName) = 0;

    };

} // namespace SMCUI

