#ifndef SMCUI_LANDMARKUIHANDLER_H_
#define SMCUI_LANDMARKUIHANDLER_H_

/*****************************************************************************
*
* File             : LandmarkUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : LandmarkUIHandlerclass
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUImodule
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <VizUI/UIHandler.h>
#include <SMCUI/ILandmarkUIHandler.h>
#include <Elements/IModelFeatureObject.h>
#include <osgUtil/LineSegmentIntersector>
#include <osg/Geode>
#include <osg/Geometry>

namespace CORE
{
    class IMetadataRecord;
}

namespace SMCUI 
{
    class LandmarkUIHandler : public VizUI::UIHandler
        , public ILandmarkUIHandler
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;
    public:

        //! COnstrucotr
        LandmarkUIHandler();


        CORE::IFeatureLayer* getOrCreateLandmarkLayer(const std::string &modelName);

        //@{
        /** Sets/Gets Point */
        //            void setModelFeature(ELEMENTS::IModelFeatureObject* model);
        ELEMENTS::IModelFeatureObject* getModelFeature() const;
        //@}

        //@{
        /** Sets/Gets Unit */
        void setLandmarkName(std::string modelName);
        std::string getLandmarkName() const;

        void setLandmarkFilename(std::string fileName);
        std::string getLandmarkFilename() const;
        //@}

        //@{
        /** Sets/Gets flag to handle mouse events */
        void setProcessMouseEvents(bool flag);
        bool getProcessMouseEvents() const;
        //@}

        //! set the current mode
        void setMode(LandmarkMode mode);

        //! get the current mode
        LandmarkMode getMode();

        //! set the current selected line as current line
        void setSelectedLandmarkAsCurrent();

        //! set the metadata record to the current line
        void setMetadataRecord(CORE::IMetadataRecord* record);

        //! get the metadata record of the current line
        CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const;

        //! update the current line
        void _update();

        //! add point to current selected layer
        void addLandmarkToCurrentSelectedLayer();

        void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions) const;

        //! get current selected layer definition
        CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn();

        //! remove the created point
        void removeCreatedLandmark();

        //! set the focus of the UI handler
        void setFocus(bool value);

        //! function called when added to Manager
        void onAddedToManager();

        //! function called when it recieves a message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! set the temporary state
        void setTemporaryState(bool value);

        //! get the temporary state
        bool getTemporaryState() const;

        //             void setRepresentationMode()

        //! Create a Point and add it to the scene
        CORE::RefPtr<CORE::IObject> createLandmark(bool addToWorld=true);

        void setResourceLocationOnServer(const std::string& link);

        void addToLayer(CORE::IFeatureLayer *featureLayer);

    protected:

        //! Destructor
        ~LandmarkUIHandler();

        void _addToLayer(CORE::IFeatureLayer *featureLayer);

        //! Handles the intersection - placeholder currently
        //            void _handleButtonDraggedIntersection(int button, const osgUtil::LineSegmentIntersector::Intersection& osgIntersection);

        //! Handles the release intersection
        //            void _handleButtonReleasedIntersection(int button, const osg::Vec3d& longLatAlt);
        void _handleButtonReleasedIntersection(int button, const osgUtil::LineSegmentIntersector::Intersection& osgIntersection);

        //! Resets the handler
        void _reset();

        //!
        CORE::IFeatureLayer* _getSelectedLayer();

    private:

        //! point
        CORE::RefPtr<ELEMENTS::IModelFeatureObject> _modelFeatureObject;

        std::string _modelName;

        std::string _fileName;

        //! Flag to represent whether we are handling mouse events or not
        bool _processMouseEvents;

        //! Offset for the area
        double _offset;

        //! mode
        ILandmarkUIHandler::LandmarkMode _mode;

        //! temporary state
        bool _temporary;

        bool _handleButtonRelease;
    };
} // namespace SMCUI

#endif // SMCUI_LANDMARKUIHANDLER_H_
