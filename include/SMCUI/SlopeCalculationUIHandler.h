#ifndef SMCUI_SLOPECALCULATIONUIHANDLER_H_
#define SMCUI_SLOPECALCULATIONUIHANDLER_H_

/*****************************************************************************
* File              : SlopeCalculationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : class for slope calculation filter
* Author            : Mohan Singh
* Author email      : mohan@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <SMCUI/export.h>
#include <osg/Array>
#include <VizUI/UIHandler.h>
#include <SMCUI/ISlopeCalculationUIHandler.h>
#include <Core/ILine.h>

namespace SMCUI
{
    //! interface used for calculating slope 
    class SlopeCalculationUIHandler : public ISlopeCalculationUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        SlopeCalculationUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Set/get point1 of the line to be processed */
        void setPoint1(CORE::IPoint* point1);
        CORE::IPoint* getPoint1();
        //@}

        //@{
        /** Set/get point2 of the line to be processed */
        void setPoint2(CORE::IPoint* point2);
        CORE::IPoint* getPoint2();
        //@}

        //! Subscribe to filter messages
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
        //! reset member Variable to Initiliaze position
        void reset();

        //! Calculation of slope
        void execute();
        //return  slope in String Data Type
        std::string getSlopeText();
    protected:

        ~SlopeCalculationUIHandler();
        // create a line between two existed points
        void _createLine();

        // delete a line between two existed points
        void _deleteLine();

        //! reference of LineFeatureObject
        CORE::RefPtr<CORE::ILine> _line;
        
        //! first point
        CORE::RefPtr<CORE::IPoint> _point1;
        //! second point
        CORE::RefPtr<CORE::IPoint> _point2;

        // contains calculated slope value
        std::string _slopeInString;
    };
} // namespace SMCUI

#endif  // SMCUI_SLOPECALCULATIONUIHANDLER_H_
