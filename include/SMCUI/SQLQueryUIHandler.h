#ifndef SMCUI_SQLQUERYUIHANDLER_H_
#define SMCUI_SQLQUERYUIHANDLER_H_


/******************************************************************************
* File              : SQLQueryUIHandler.h
* Version           : 1.0
* Module            : TKUI
* Description       : SQLQueryUIHandler interface class
* Author            : Nishant Singh
* Author email      : ns@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <SMCUI/ISQLQueryUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Core/ISQLQuery.h>
#include <Core/ISQLQueryResult.h>

namespace SMCUI 
{
    class SQLQueryUIHandler 
        : public ISQLQueryUIHandler 
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        public:

            SQLQueryUIHandler();

            //! Called when UIHandler added to manager
            void onAddedToManager();

            //@{
            //** Set focus after deleting the line object */
            void setFocus(bool value);
            //@}

            //! 
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            //! 
            void initializeAttributes();

            //! run SQL query on the feature layer
            CORE::ISQLQueryResult* executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& whereClause);
            CORE::ISQLQueryResult* executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& fieldNames, 
                                                        const std::string& whereClause);
            CORE::ISQLQueryResult* executeSQLQuery(CORE::IFeatureLayer* layer, const std::string& fieldNames, 
                                                const std::string& whereClause, const osg::Vec4d& extents);
            CORE::ISQLQueryResult* executeSQLQuery2(CORE::IFeatureLayer* layer, const std::string& query1,const std::string& query2);                                                
            CORE::ISQLQueryResult* executeSQLQuery2(CORE::IFeatureLayer* layer, const std::string& query);

            //! get the current SQL query result
            CORE::ISQLQueryResult* getSQLQueryResult() const;

            //! clear the current SQL query result
            void clearSQLQueryResult();

            //! save SQL query result
            void saveSQLQueryResult();

        protected:

            //! load the SQL query component
            bool _loadSQLQuery();

            //! sql query
            CORE::RefPtr<CORE::ISQLQuery> _sqlQuery;

            //! sql query result
            CORE::RefPtr<CORE::ISQLQueryResult> _sqlResult;
    };

} // namespace SMCUI
#endif // SMCUI_SQLQUERYUIHANDLER_H_


