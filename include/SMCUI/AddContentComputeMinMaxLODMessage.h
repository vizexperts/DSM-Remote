#ifndef SMCUI_AddContentComputeMinMaxLODMessage_H
#define SMCUI_AddContentComputeMinMaxLODMessage_H

/******************************************************************************
 * File              : AddContentComputeMinMaxLODMessage
 * Version           : 1.0
 * Module            : SMCUI
 * Description       : AddContentComputeMinMaxLODMessage class declaration
 * Author            : Raghavendra Polinki
 * Author email      : rp@vizexperts.com
 * Reference         : 
 * Inspected by      : 
 * Inspection date   :  
 
 *******************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *******************************************************************************
 *
 * Revision log (latest on the top): 
 *
 ******************************************************************************/

#include <SMCUI/export.h>
#include <Core/CoreMessage.h>
#include <SMCUI/IAddContentComputeMinMaxLODMessage.h>

namespace SMCUI
{
    //! Filter Status message type
    class AddContentComputeMinMaxLODMessage 
        : public CORE::CoreMessage
        , public SMCUI::IAddContentComputeMinMaxLODMessage
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED
        public:

            AddContentComputeMinMaxLODMessage();

            //@{
            /** Sets/Gets the filter status */
            void setStatus(AddContentComputeMinMaxLODStatus status);
            AddContentComputeMinMaxLODStatus getStatus() const;
            //@}

            void setProgress(unsigned int progress);
            unsigned int getProgress();

            void setMinLOD(unsigned int minLOD);
            unsigned int getMinLOD();

            void setMaxLOD(unsigned int minLOD);
            unsigned int getMaxLOD();

        protected:

            ~AddContentComputeMinMaxLODMessage();

        protected:

            //! Filter status
            AddContentComputeMinMaxLODStatus _status;

            //! progress
            unsigned int _progress;

            unsigned int _minLOD;

            unsigned int _maxLOD;


    };
}

#endif // SCMUI_ADDCONTENTSTATUSMESSAGE_H
