#ifndef SMCUI_CUSTUMDRAGGER_H_
#define SMCUI_CUSTUMDRAGGER_H_

/*****************************************************************************
*
* File             : CustomDragger.h
* Version          : 1.1
* Module           : SMCUI
* Description      : Custom Dragger class declarations
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <osgManipulator/RotateCylinderDragger>
#include <osgManipulator/TranslateAxisDragger>
#include <osgManipulator/Translate2DDragger>
#include <osg/Camera>

#include <SMCUI/export.h>

///////////////////////////////////////////////////////////////////////////
//!
//! \file CustomDragger.h
//!
//! \brief Contains different Custom Draggers used in DraggerContainer
//!
///////////////////////////////////////////////////////////////////////////
namespace SMCUI{

    ///////////////////////////////////////////////////////////////////////
    //!
    //! Definig some colors used for creating manipulators
    //!
    ///////////////////////////////////////////////////////////////////////
    static osg::Vec4 COLORS_RED                 = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
    static osg::Vec4 COLORS_GREEN               = osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f);
    static osg::Vec4 COLORS_BLUE                = osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f);
    static osg::Vec4 COLORS_BLACK               = osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f);
    static osg::Vec4 COLORS_TRANSPARENT_BLUE    = osg::Vec4(0.0f, 0.0f, 1.0f, 0.2f);
    static osg::Vec4 COLORS_YELLOW              = osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f);

    ///////////////////////////////////////////////////////////////////////
    //!
    //! \fn osg::Geometry* createCircleGeometry(float radius, unsigned int numSegments, bool fill)
    //!
    //! \brief Utility function to create a circular geometry
    //!
    //! \param[in] radius Radius of required circle
    //!
    //! \param[in] numSegments Number of segments required in circle geometry
    //! 
    //! \param[in] fill if (fill == true) Returns a polygon geometry
    //!                 if (fill == false) Returns a line geometry
    //!
    ///////////////////////////////////////////////////////////////////////
    osg::Geometry* createCircleGeometry(float radius, unsigned int numSegments, bool fill = false);

    ///////////////////////////////////////////////////////////////////////
    //!
    //! \struct CustomTrackballDragger
    //!
    //! \brief Custom dragger class used for object rotation
    //!        Can be set to show all three axes of rotation 
    //!
    ///////////////////////////////////////////////////////////////////////
    class  CustomTrackballDragger: public osgManipulator::CompositeDragger
    {
    public: 
        //////////////////////////////////////////////////////////////////
        //!
        //! \fn     CustomTrackballDragger()
        //!
        //! \brief  Default Constructor
        //!         Initializes cild draggers
        //! 
        //////////////////////////////////////////////////////////////////
        CustomTrackballDragger();

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setupDefaultGeometry()
        //! 
        //! \brief  Overloaded Dragger::setupDefaultGeometry()
        //!         Setup geometry of dragger children
        //!
        /////////////////////////////////////////////////////////////////
        void setupDefaultGeometry();

    protected:
        ///////////////////////////////////////////////////////////////
        //!
        //! \fn     ~CustomTrackballDragger()
        //!
        //! \brief default Destructor
        //!
        ///////////////////////////////////////////////////////////////
        virtual ~CustomTrackballDragger(){};

        //////////////////////////////////////////////////////////////
        //!
        //! \var _xDragger Dragger used for rotation along x-axis(Northing)
        //!
        //////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::RotateCylinderDragger> _xDragger;

        //////////////////////////////////////////////////////////////
        //!
        //! \var _yDragger Dragger used for rotation along z-axis(Easting)
        //!
        //////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::RotateCylinderDragger> _yDragger;

        //////////////////////////////////////////////////////////////
        //!
        //! \var _zDragger Dragger used for rotation along z-axis(Up)
        //!
        //////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::RotateCylinderDragger> _zDragger;

    };

    ///////////////////////////////////////////////////////////////////////
    //!
    //! \struct CustomScaleDragger
    //!
    //! \brief Custom dragger class used for object scaling
    //!
    ///////////////////////////////////////////////////////////////////////
    class CustomScaleDragger: public osgManipulator::Dragger
    {
    public: 
        //////////////////////////////////////////////////////////////////
        //!
        //! \fn     CustomScaleDragger()
        //!
        //! \brief  Default Constructor
        //!         Initializes cild draggers
        //! 
        //////////////////////////////////////////////////////////////////
        CustomScaleDragger();

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn         handle(const osgManipulator::PointerInfo& pi, 
        //!                 const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us)
        //! 
        //! \brief      Overloaded Dragger::handle(...)
        //!             Handler for Drag event, calculates the scale to be applied on associated matrices
        //!
        //! \param pi[in]   PointerInfo of current pointer used for manipulation
        //!
        //! \param ea[in]   EventAdapter associated to event
        //! 
        //! \param us[in,out]   ActionAdapter associated to event
        //!
        /////////////////////////////////////////////////////////////////
        bool handle(const osgManipulator::PointerInfo& pi, 
            const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setupDefaultGeometry()
        //! 
        //! \brief  Overloaded Dragger::setupDefaultGeometry()
        //!         Setup geometry of dragger children
        //!
        /////////////////////////////////////////////////////////////////
        void setupDefaultGeometry();

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     double computeScale(const osg::Vec3d& startProjectedPoint, const osg::Vec3d& projectedPoint, 
        //!             const osg::Vec3d& scaleCenter)
        //! 
        //! \brief  Compute scale to be applied in MotionCommand in local coordinates
        //! 
        //! \param[in]  startProjectedPoint point in local coordinates where command started
        //!
        //! \param[in]  projectedPoint current projected point in local coordinates 
        //!
        //! \param[in]  scaleCenter Center point in local coordinates
        //!
        /////////////////////////////////////////////////////////////////////////////////
        double computeScale(const osg::Vec3d& startProjectedPoint, const osg::Vec3d& projectedPoint, 
            const osg::Vec3d& scaleCenter);

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     double computeScaleInScreenSpace(const osg::Vec3d& startProjectedPoint,
        //!             const osg::Vec3d& projectedPoint, const osg::Vec3d& scaleCenter)
        //! 
        //! \brief  Compute scale to be applied in MotionCommand in Screen Space
        //!         XXX:- Not working properly, To be checked
        //! 
        //! \param[in]  startProjectedPoint point in local coordinates where command started
        //!
        //! \param[in]  projectedPoint current projected point in local coordinates 
        //!
        //! \param[in]  scaleCenter Center point in local coordinates
        //!
        /////////////////////////////////////////////////////////////////////////////////
        double computeScaleInScreenSpace(const osg::Vec3d& startProjectedPoint, const osg::Vec3d& projectedPoint, 
            const osg::Vec3d& scaleCenter);

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setColor(const osg::Vec4& color)
        //! 
        //! \brief  Set Material color for children nodes
        //! 
        //! \param[in]  color Color to be set  
        //!
        /////////////////////////////////////////////////////////////////////////////////
        inline void setColor(const osg::Vec4& color) 
        {_color = color;  osgManipulator::setMaterialColor(_color, *this); }

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     const osg::Vec4& getColor()
        //! 
        //! \brief  Get current material color 
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        inline const osg::Vec4& getColor() const { return _color; }

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setMinScale(double min)
        //! 
        //! \brief  Set minimum scale of the dragger
        //!
        //! \param[in] min Minimum scale value
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        inline void setMinScale(double min) { _minScale = min; }

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     double getMinScale()
        //! 
        //! \brief  Get minimum scale of the dragger
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        inline double getMinScale() const{ return _minScale; }

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setMaxScale(double max)
        //! 
        //! \brief  Set maximum scale of the dragger
        //!
        //! \param[in] max Maximum scale value
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        inline void setMaxScale(double max) { _maxScale = max; }

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     double getMaxScale()
        //! 
        //! \brief  Get maximum scale of the dragger
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        inline double getMaxScale() const{ return _maxScale; }

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setPickColor(const osg::Vec4& color)
        //! 
        //! \brief  Set Pick color of dragger
        //!
        //! \param[in] color Pick color to be set
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        inline void setPickColor(const osg::Vec4& color) { _pickColor = color; }

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     const osg::Vec4& getPickColor() const
        //! 
        //! \brief  Get Pick color of dragger
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        inline const osg::Vec4& getPickColor() const { return _pickColor; }

    private:

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     _getCamera()
        //! 
        //! \brief  Initialize _camera to current camera
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        void _getCamera();

    protected:

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     ~CustomScaleDragger()
        //! 
        //! \brief  Default Destructor
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        virtual ~CustomScaleDragger(){};

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _projector CylinderPlaneProjector used to calculate projections on dragger
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::CylinderPlaneProjector> _projector;


        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _camera osg::Camera used in case Screen Space scaling is done
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osg::Camera> _camera;

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _startProjectedPoint   Variable to store the coordinates of point from where START command is send,
        //!                             world coordinates if screen space calculation is done local otherwise
        //!
        /////////////////////////////////////////////////////////////////////////////////
        osg::Vec3d _startProjectedPoint;

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _scaleCenter   Variable to store the coordinates of cylinder center on START command
        //!                     world coordinates if screen space calculation is done local otherwise
        //!
        /////////////////////////////////////////////////////////////////////////////////
        osg::Vec3d _scaleCenter;

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _color Color used in dragger ring
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        osg::Vec4 _color;

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _pickColor Color used in dragger ring while interaction
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        osg::Vec4 _pickColor;

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _minScale Minimum scale of dragger
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        double _minScale;

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \var _maxScale Maximum scale of dragger
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        double _maxScale;
    };


    ///////////////////////////////////////////////////////////////////////
    //!
    //! \struct CustomTranslateAxisDragger
    //!
    //! \brief Custom dragger class used for object Translation along the axes
    //!
    ///////////////////////////////////////////////////////////////////////
    class CustomTranslateAxisDragger: public osgManipulator::TranslateAxisDragger
    {
    public: 

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn CustomTranslateAxisDragger()
        //!
        //! \brief  Default constructor
        //!         Initializes all the child draggers
        //!
        /////////////////////////////////////////////////////////////////////
        CustomTranslateAxisDragger(){};

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setupDefaultGeometry()
        //! 
        //! \brief  Overloaded Dragger::setupDefaultGeometry()
        //!         Setup geometry of dragger children
        //!
        /////////////////////////////////////////////////////////////////
        void setupDefaultGeometry();

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setupTranslateDragger(osg::ref_ptr<osgManipulator::Translate1DDragger> dragger)
        //! 
        //! \brief  Called by setupDefaultGeometry()
        //!         Sets up the given draggers custom geometry
        //!
        //! \param[in,out] dragger Dragger to be setup with custom geometry
        //!
        /////////////////////////////////////////////////////////////////
        void setupTranslateDragger(osg::ref_ptr<osgManipulator::Translate1DDragger> dragger);

    protected:

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     ~CustomTranslateAxisDragger()
        //! 
        //! \brief  Default Destructor
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        virtual ~CustomTranslateAxisDragger(){};
    };

    ///////////////////////////////////////////////////////////////////////
    //!
    //! \struct CustomTranslatePlaneDragger
    //!
    //! \brief Custom dragger class used for object Translation along the Terrain
    //!
    ///////////////////////////////////////////////////////////////////////
    class CustomTranslatePlaneDragger: public osgManipulator::Translate2DDragger
    {
    public: 

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn CustomTranslateAxisDragger()
        //!
        //! \brief  Default constructor
        //!         Initializes all the child draggers
        //!
        /////////////////////////////////////////////////////////////////////
        CustomTranslatePlaneDragger();

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setupDefaultGeometry()
        //! 
        //! \brief  Overloaded Dragger::setupDefaultGeometry()
        //!         Setup geometry of dragger children
        //!
        /////////////////////////////////////////////////////////////////
        void setupDefaultGeometry();

    protected:

        ///////////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     ~CustomTranslatePlaneDragger()
        //! 
        //! \brief  Default Destructor
        //! 
        /////////////////////////////////////////////////////////////////////////////////
        virtual ~CustomTranslatePlaneDragger(){};
    };

}   // namespace SMCUI

#endif      // SMCUI_CUSTUMDRAGGER_H_