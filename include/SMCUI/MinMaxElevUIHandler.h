#pragma once


/******************************************************************************
* File              : MinMaxElevUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for UI handler
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2010-2011 CAIR, DRDO
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/IMinMaxElevUIHandler.h>
#include <VizUI/UIHandler.h>
#include <osgUtil/LineSegmentIntersector>
#include <SMCUI/IAreaUIHandler.h>

namespace CORE
{
    class IPoint;
}


namespace SMCUI 
{
    class MinMaxElevUpdateCallback : public osg::NodeCallback
    {
        public:

            MinMaxElevUpdateCallback(CORE::IWorld* world)
                : _world(world)
            {}

            virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);

            CORE::RefPtr<CORE::IObject> object;

            //! Lock for updating the object
            OpenThreads::Mutex  mutex;

        private:
            CORE::IWorld* _world;
    };

    class SMCUI_DLL_EXPORT MinMaxElevUIHandler 
        : public IMinMaxElevUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        public:

            //! Constructor
            MinMaxElevUIHandler();

            //! Calculates the MinMaxElevation
            virtual void computeMinMaxElevation(bool multiThreaded = true);

            //! stop executing the filter
            void stopFilter();

            //@{
            /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
            void onAddedToManager();
            void onRemovedFromManager();
            //@}

            virtual void setFocus(bool focus);

            //!
            virtual bool getFocus() const;

            //@{
            /** Set the extents of the area */
            void setExtents(osg::Vec4d extents);
            osg::Vec4d getExtents() const;
            //@}

            //@{
            //! Returns the minimum and maximum elevation values
            osg::Vec3d getMinimumElevationPoint() const;
            osg::Vec3d getMaximumElevationPoint() const;
            //@}

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        protected:

            ~MinMaxElevUIHandler();

            //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
            void _reset();

            //!
            void _setArea();

        private:

            //! add the Min and Max point to the world
            void _addMinMaxPoints(CORE::IPoint* min, CORE::IPoint* max);

            //! remove min max point from the world
            void _removeMinMaxPoints();

            //! reset the min max points
            void _resetMinMaxPoints();

            //! create Feature Layer
            void _createLayer();

            //! minmax visitor
            CORE::RefPtr<CORE::IBaseVisitor> _minmaxVisitor;

            //! feature layer
            CORE::RefPtr<CORE::IObject> _featureLayer;

            //! Flag denoting whether first point has been set or not
            bool _firstPointSet;

            //! Flag denoting whether second point has been set or not
            bool _secondPointSet;

            //! Array containing area points
            osg::ref_ptr<osg::Vec2Array> _extents;

            //! First point
            osg::Vec3d _minPoint;

            //! Second point
            osg::Vec3d _maxPoint;

            //! Area UI Handler
            CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

            // update callback used on world
            osg::ref_ptr<MinMaxElevUpdateCallback> _minMaxCallback;
    };

} // namespace SMCUI


