#ifndef _SMCUI_IAddGPSUIHandler_H_
#define _SMCUI_IAddGPSUIHandler_H_

/*****************************************************************************
 *
 * File             : IAddGPSUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : IAddGPSUIHandler class
 * Author           : Gaurav Garg
 * Author email     : gaurav@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <App/IUIHandler.h>
#include <SMCUI/export.h>
#include <osg/Vec4>

namespace CORE
{
    class Attribute;
}

namespace SMCUI 
{
    //! Handles digital compass sources
    class IAddGPSUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAddGPSUIHandler);
        public:

            enum Type
            {
                None,
                NMEATCP,
                Log
            };
            
            //! Loads the  GPS source
            virtual void loadSource(const std::string& fileName = std::string()) = 0;

            //! Sets the GPS source type
            virtual void setCurrentType(Type sourceType) = 0;

            //! Gets the named attribute of the source
            virtual CORE::Attribute* getSourceAttribute(const std::string& attrname) const = 0;
    };
} // namespace SMCUI

#endif // _SMCUI_IAddGPSUIHandler_H_
