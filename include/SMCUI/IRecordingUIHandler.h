#pragma once

/*****************************************************************************
 *
 * File             : IAddVectorGUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : IAddVectorGUIHandler class
 * Author           : Abhinav Goyal
 * Author email     : abhinav@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <SMCUI/export.h>

#include <App/IUIHandler.h>

#include <Util/BaseExceptions.h>


//! \brief  Handles recordingt gui events from the GUI
namespace SMCUI 
{
    class IRecordingUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IRecordingUIHandler);

        public:

            virtual void startRecording(std::string filename, int width, int height) = 0;

            virtual void stopRecording() = 0;

    };
} // namespace SMCUI


