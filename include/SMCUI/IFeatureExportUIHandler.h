#pragma once

/*****************************************************************************
 *
 * File             : IFeatureExportUIHandler.h
 * Version          : 1.0
 * Module           : UI
 * Description      : IFeatureExportUIHandler class
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : UI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <Core/IReferenced.h>
#include <Core/RefPtr.h>

#include <SMCUI/export.h>

#include <osg/Vec4>

namespace CORE
{
    class IPoint;
}

namespace SMCUI 
{
    class IFeatureExportUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IFeatureExportUIHandler);

        public:

           //! export the file
            virtual bool exportFeature(const std::string& filename) = 0;
            virtual bool exportRaster(const std::string& filename) = 0;
            virtual bool exportElevation(const std::string& filename) = 0;
            virtual bool exportFeatureToTin(const std::string& filename) = 0;
            virtual bool exportFeatureToGeoVrml(const std::string& filename) = 0;
            virtual bool exportFeatureToRaster(const std::string& filename, const osg::Vec4& color, const osg::Vec4& nodataColor, double resolution) = 0;
            virtual bool exportLineToCSV(const std::string& filename) = 0;
            virtual bool exportPointLayerToCSV(const std::string& filename) = 0;
            virtual void setUploadingServerPort(const std::string& portNum) = 0;
            virtual std::string getUploadingServerPort() const = 0;
            /**
            * to export layer on GeorbIS server 
            * param serverUrl string argument denoting server IP as (i.e  https://192.168.0.1 ) 
            * /return 
            */

            virtual bool exportGeoLayer(std::string& serverIp) = 0;
            /**
            * request to check capability
            * param serverUrl string argument denoting server Url(i.e  https://192.168.0.1:19091/maps )
            * /return param boolean true if georbis server otherwise false
            */
            virtual  bool georbISServerCapability(std::string& serverUrl) = 0;

            // set the flag which is used to know if ive should be converted to gltf or not
            virtual void setConvertModel(bool value) = 0;
            
            

#if  0
            // RFE1 
            virtual bool exportFeatureToOverlay(const std::string& filename) = 0;

            virtual bool exportRoute(const std::string& filename) = 0;

            virtual bool exportTrajectory(const std::string& filename) = 0;
            
            virtual bool exportFormation(const std::string& filename) = 0;
#endif
    };
} // namespace UI
