#pragma once

/*****************************************************************************
*
* File             : CreateFeatureUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : CreateFeatureUIHandler class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/IBaseUtils.h>

#include <VizUI/UIHandler.h>

#include <SMCUI/ICreateFeatureUIHandler.h>

namespace SMCUI
{
    // Interface to create a new layer
    class CreateFeatureUIHandler : public VizUI::UIHandler, 
        public ICreateFeatureUIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    private:
        struct CustomLayer
        {
            std::string layerGeometry;
            float penWidth;
            osg::Vec4 penColor;
            osg::Vec4 brushColor;
        };

        typedef std::map<std::string, CustomLayer> CustomLayerMap;


    public:

        //! Constructor
        CreateFeatureUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        CORE::RefPtr<CORE::NamedGroupAttribute> _landmarkType;

        CORE::RefPtr<CORE::IFeatureLayer> getFeatureLayer(std::string type);

        void initializeAttributes();

        void addCustomLayerType(const CORE::NamedGroupAttribute& groupattr);

        CORE::RefPtr<CORE::NamedGroupAttribute> getCustomLayerType() const;

    protected:
        CORE::RefPtr<CORE::NamedGroupAttribute> _layerType;

        void readColorFromString(std::string colorString, osg::Vec4& color);

        CustomLayerMap _customLayerMap;


    };

} // namespace UI

