#ifndef SMCUI_ISURFACEARACALCUIHANDLER_H_
#define SMCUI_ISURFACEARACALCUIHANDLER_H_

/*****************************************************************************
* File              : ISurfaceAreaCalcUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for surface area filter
* Author            : Rahul Srivastava
* Author email      : rahul@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

namespace SMCUI
{
    //! UIHandler for surface area ui handler
    class ISurfaceAreaCalcUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ISurfaceAreaCalcUIHandler);

    public:

        // Surface area message
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> AreaCalculationCompletedMessageType;

    public:

        enum ComputationType
        {
            //! for normal area of the polygon
            AERIAL,  
            //! for area of projection of polygon on the terrain
            PROJECTED,
            //! for an invalid visitor
            INVALID
        };

        //@{
        /** Sets/gets the points of the polygons to be processed */
        virtual void setPoints(osg::Vec3dArray* points) = 0;
        virtual osg::Vec3dArray* getPoints() = 0;
        //@}

        //@{
        /** Sets/gets the type of area to compute */
        virtual void setComputationType(ComputationType type) = 0;
        virtual ComputationType getComputationType() = 0;
        //@}

        virtual double getArea() = 0;

        //! Executes the filter
        virtual void execute() = 0;

        //! Stops the execution of the filter 
        virtual void stop() = 0;

    };
} // namespace SMCUI

#endif  // SMCUI_ISURFACEARACALCUIHANDLER_H_
