#ifndef SMCUI_ICUTANDFILLUIHANDLER_H_
#define SMCUI_ICUTANDFILLUIHANDLER_H_

/******************************************************************************
* File              : ICutAndFillUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Slope aspect filter UI Handler
* Author            : Gaurav Gar
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <GISCompute/ICutAndFillFilterVisitor.h>

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI 
{
    class ICutAndFillUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ICutAndFillUIHandler);

    public:
        //@{
        /** Set/get the Stream Output file */
        virtual void setOutputName(std::string& name) = 0;
        //@}

        ////@{
        ///** Set/get color */
        virtual void setColor(GISCOMPUTE::ICutAndFillFilterVisitor::COLOR color) = 0;
        //virtual GISCOMPUTE::ICutAndFillFilterVisitor::COLOR  getColor() const =0;
        ////@}

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;
        //@}
        //!
        virtual void setInputElevationObject(TERRAIN::IElevationObject* objFirst,TERRAIN::IElevationObject* objSecond)=0;
        virtual void setElevationObject(TERRAIN::IElevationObject* obj) = 0;

        //@{
        /** Set the Threshold */

        virtual void setTransparency(double transparency)=0;
        virtual double getTransparency() const=0;
        //@}


        virtual void execute() = 0;

        virtual void stopFilter() = 0;
    };

} // namespace SMCUI

#endif // SMCUI_ICUTANDFILLUIHANDLER_H_
