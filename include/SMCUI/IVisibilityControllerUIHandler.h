#ifndef SMCUI_IVISIBILITYCONTROLLERUIHANDLER_H_
#define SMCUI_IVISIBILITYCONTROLLERUIHANDLER_H_

/*****************************************************************************
*
* File             : IVisibilityControllerUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : IVisibilityControllerUIHandler class
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013-2014, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/RefPtr.h>

#include <SMCUI/export.h>
#include <Elements/IVisibilityController.h>

namespace SMCUI 
{
    class IVisibilityControllerUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IVisibilityControllerUIHandler);

        //status return by add control point function
        enum PointAddedStatus {
            SUCCESS,
            DUPLICATE,
            FAIL
        };

    public:

        virtual const ELEMENTS::IVisibilityController::ControlPointList& getControlPointList() const = 0;

        virtual unsigned int getNumControlPoints() const = 0;

        virtual PointAddedStatus addControlPoint(const boost::posix_time::ptime& time, bool visibilityState) = 0;

        virtual void removeControlPoint(unsigned int index) = 0;

        // returns weather the selecetd object has visibility controller property 
        virtual bool setSelectedObjectAsCurrent()= 0;
        
        virtual void reset() = 0;
    };
} // namespace SMCUI

#endif // SMCUI_IVISIBILITYCONTROLLERUIHANDLER_H_
