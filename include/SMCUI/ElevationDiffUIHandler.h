#ifndef SMCUI_ELEVATIONDIFFUIHANDLER_H_
#define SMCUI_ELEVATIONDIFFUIHANDLER_H_

/*****************************************************************************
* File              : ElevationDiffUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : class for Elevation Difference calculation 
* Author            : Akshay Gupta
* Author email      : akshay@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/

#include <SMCUI/export.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/IElevationDiffUIHandler.h>

namespace SMCUI
{
    //! class used for calculating elevation difference between two points  
    class ElevationDiffUIHandler : public IElevationDiffUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        ElevationDiffUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Set/get point1 of the line to be processed */
        void setPoint1(CORE::IPoint* point1);
        CORE::IPoint* getPoint1();
        //@}

        //@{
        /** Set/get point1 of the line to be processed */
        void setPoint2(CORE::IPoint* point2);
        CORE::IPoint* getPoint2();
        //@}

        //! Subscribe to filter messages
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Calculation of elevation difference
        void execute();

        //! Elevation difference value as string
        std::string getEleDiffText();
        //reset elevation
        void reset();

    protected:

        ~ElevationDiffUIHandler();

        //! first point
        CORE::RefPtr<CORE::IPoint> _point1;
        //! second point
        CORE::RefPtr<CORE::IPoint> _point2;
        //! member to store Elevation in String Data Type
        std::string _eleDiffInString;
    };
} // namespace SMCUI

#endif  // SMCUI_ELEVATIONDIFFUIHANDLER_H_
