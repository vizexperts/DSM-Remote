#pragma once

/******************************************************************************
* File              : AddCSVUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : AddCSVUIHandler Implementation Class
* Author            : Abhinav Goyal
* Author email      : abhinav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
* Copyright (c) 2013-2014, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <VizUI/UIHandler.h>

#include <SMCUI/IAddCSVUIHandler.h>

namespace SMCUI {

       // XXX - should not be exported
    class SMCUI_DLL_EXPORT AddCSVUIHandler : public IAddCSVUIHandler, 
                                             public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
       
        public:
            
            AddCSVUIHandler();

            bool readFile(const std::string &layerName, const std::string &url, std::vector<std::string>& hyperlinkList, int latIndex, int longIndex,int unitIndex);

            void getAttributesList(std::vector<std::string>& vStrings, std::string& url);

        protected:

            bool _readHeader(std::ifstream &inFile, std::vector<std::string> &vStrings);

            CORE::IObject *_createFeatureLayer();
            CORE::IObject *_createOverlayObject();
            std::vector<std::string> _headerTags;
            std::string _currLine;
            bool _headerReadingDone;
    };
} 
// namespace SMCUI

