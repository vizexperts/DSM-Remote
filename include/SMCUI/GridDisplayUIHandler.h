#pragma once

/******************************************************************************
* File              : GridDisplayUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for UI handler
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <osg/NodeCallback>
#include <SMCUI/IGridDisplayUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Core/IBaseUtils.h>
#include <Core/Base.h>

#include <Core/ITerrain.h>
#include <osgEarthUtil/GeodeticGraticule>
#include <OpenThreads/Mutex>
namespace SMCUI
{

    class GridDisplayUIHandler : public VizUI::UIHandler
                                ,public SMCUI::IGridDisplayUIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        GridDisplayUIHandler();

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        void setGridDisplay(bool value) ;
        bool getGridDisplay() const ;
        //@}
        //@{ set/Get Function for Display Indian Grid 
        void setIndianGridDisplay(bool value);
        bool getIndianGridDisplay() const;
        //@}

        void update(const CORE::IMessageType &messageType, const CORE::IMessage &message);
        
    protected:

        ~GridDisplayUIHandler();

        bool _gridDisplay;
        bool _indianGridDisplay;
        osg::ref_ptr<osgEarth::Util::GeodeticGraticule> _gridNode;
        void _mtAddVectorData(const std::string& layerName, const std::string& url, CORE::RefPtr<DB::ReaderWriter::Options> options);
        //! Scoped lock for URL
        OpenThreads::Mutex _mutex;

        CORE::RefPtr<CORE::IObject> _indianGridObject;
        const std::string _indianGridShapeFilePath;
        const std::string _indianGridStyleTemplate;

    };
} // namespace SMCUI
