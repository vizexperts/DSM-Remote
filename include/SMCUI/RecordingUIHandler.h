#pragma once

/******************************************************************************
* File              : RecordingUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : RecordingUIHandler Implementation Class
* Author            : 
* Author email      : ip@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 
*******************************************************************************
* Copyright (c) 2013-2014, VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/
#include <VizUI/UIHandler.h>

#include <SMCUI/IRecordingUIHandler.h>

#include <AVR/CameraRecording.h>

namespace SMCUI {

       // XXX - should not be exported
    class SMCUI_DLL_EXPORT RecordingUIHandler : public IRecordingUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
       
        public:
            
            RecordingUIHandler();

            void startRecording(std::string filename, int width, int height);

            void stopRecording();

        protected:

            ~RecordingUIHandler();

            CORE::RefPtr<AVR::ICameraRecording> _cameraRecording;


    };
} 
// namespace SMCUI

