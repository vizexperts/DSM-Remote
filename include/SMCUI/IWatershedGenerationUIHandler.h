#ifndef SMCUI_IWATERSHEDGENERATIONUIHANDLER_H_
#define SMCUI_IWATERSHEDGENERATIONUIHANDLER_H_

/******************************************************************************
* File              : IWatershedGenerationUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Slope aspect filter UI Handler
* Author            : Gaurav Gar
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <GISCompute/ITauDEMWatershedVisitor.h>

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI 
{
    class IWatershedGenerationUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IWatershedGenerationUIHandler);

    public:
        //@{
        /** Set/get the Stream Output file */
        virtual void setStreamOutputName(std::string& name) = 0;
        //virtual std::string getStreamOutputName() const = 0;
        //@}

        //@{
        /** Set/get the wathershed Output file */
        virtual void setWatershedOutputName(std::string& name) = 0;
       // virtual std::string getWatershedOutputName() const = 0;
        //@}

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;
        //@}

                //@{
        /** Set the Outlet extents of the area */
        virtual void setOutletExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getOutletExtents() const = 0;
        //@}


        virtual void setElevationObject(TERRAIN::IElevationObject* obj) = 0;

        //@{
        /** Set the Threshold */

        virtual void setThreshold(double value)=0;
        virtual double getThreshold() const=0;
        //@}


        virtual void execute() = 0;

        virtual void stopFilter() = 0;
    };

} // namespace SMCUI

#endif // SMCUI_IWATERSHEDGENERATIONUIHANDLER_H_
