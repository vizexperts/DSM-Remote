#ifndef SMCUI_ANIMATIONUIHANDLER_H_
#define SMCUI_ANIMATIONUIHANDLER_H_

/*****************************************************************************
*
* File             : AnimationUIHandler.h
* Version          : 1.1
* Module           : SMCUI
* Description      : AnimationUIHandler interface declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*****************************************************************************/

#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/IAnimationUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Core/IAnimationComponent.h>

namespace SMCUI
{
    class SMCUI_DLL_EXPORT AnimationUIHandler
        :public IAnimationUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public: 
        AnimationUIHandler();

        virtual void setFocus(bool focus);
        virtual bool getFocus() const;

        void onAddedToManager();
        void onRemovedFromManager();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void setStartEndDateTime(const boost::posix_time::ptime& startTime, const boost::posix_time::ptime& endTime);
        void setCurrentDateTime(const boost::posix_time::ptime& currentTime);

        const boost::posix_time::ptime& getStartDateTime();
        const boost::posix_time::ptime& getEndDateTime();
        const boost::posix_time::ptime& getCurrentDateTime();

        void play();
        void pause();
        void stop();

        double getTimeScale() const;
        void setTimeScale(double newTimeScale);

        bool isAnimationRunning() const;

    protected:
        ~AnimationUIHandler();

        void _reset();

        CORE::RefPtr<CORE::IAnimationComponent> _animationComponent;

    };
}

#endif // SMCUI_ANIMATIONUIHANDLER_H_