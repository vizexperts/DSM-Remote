#pragma once 

/******************************************************************************
* File              : IAreaUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for GUI Area Handler
* Author            : Kumar Saurabh Arora
* Author email      : saurabh@vizexperts.com
* Reference         :
* Inspected by      :
* Inspection date   :

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/IMessageType.h>
#include <osg/Vec4>
#include <osg/Vec3>
#include <osg/Vec2d>
#include <osg/Vec4d>

namespace CORE
{
    class IPolygon;
    class IMetadataRecord;
    class IMetadataTableDefn;
    class IFeatureLayer;
}

namespace SMCUI
{
    class IAreaUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAreaUIHandler);
    public:

        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> AreaCreatedMessageType;

        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> AreaUpdatedMessageType;

        enum AreaMode
        {
            AREA_MODE_NONE,
            AREA_MODE_CREATE_AREA,
            AREA_MODE_EDIT,
            AREA_MODE_CREATE_RECTANGLE,
            AREA_MODE_NO_OPERATION,
            AREA_MODE_FIXED_RECTANGLE
        };

        //! Get/Set AreaMode
        virtual bool setMode(AreaMode mode) = 0;
        virtual AreaMode getMode() const = 0;

        // Get Clicked Point
        virtual osg::Vec3d getClickedPoint() = 0;

        // Get/Set extents
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() = 0;
        virtual osg::Vec4d getExtentsUTM() = 0;
        // Get/Set widthHeihgt for Fixed Rectangle mode
        virtual void setFixedRectangleWidthHeight(osg::Vec2d widthHeight) = 0;
        virtual osg::Vec2d getFixedRectangleWidthHeight() = 0;

        //! Get/Set Color of Area
        virtual void setColor(osg::Vec4 color) = 0;
        virtual osg::Vec4  getColor() const = 0;

        virtual std::vector<int>& getSelectedVertexList() = 0;
        virtual void setSelectedVertexList(std::vector<int> _selectedPointList) = 0;

        //! Get Area
        virtual double getArea() const = 0;

        //! Returns the current area instance
        virtual CORE::IPolygon* getCurrentArea() const = 0;

        //! set the current selected Area as current Area
        virtual void setSelectedAreaAsCurrentArea() = 0;

        //! set the metadata record to the current area
        virtual void setMetadataRecord(CORE::IMetadataRecord* record) = 0;

        //! get the metadata record of the current area
        virtual CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const = 0;

        //! update the current area
        virtual void update() = 0;

        virtual void addToLayer(CORE::IFeatureLayer *featureLayer) = 0;

        //! add area to current selected layer
        virtual void addAreaToCurrentSelectedLayer() = 0;

        virtual void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions)const = 0;

        //! get current selected layer definition
        virtual CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn() = 0;

        //! remove the created area
        virtual void removeCreatedArea() = 0;

        //! delete the last point from the area
        virtual void deleteLastPointFromArea() = 0;

        //! set temporary state for the area
        virtual void setTemporaryState(bool state) = 0;

        //! get temporary state for the area
        virtual bool getTemporaryState() const = 0;

        virtual void reset() = 0;

        virtual unsigned int getNumPoints() const = 0;

        //! delete Point at index
        virtual bool _deletePointAtIndex(int index) = 0;

        //! move point at index
        virtual void _movePointAtIndex(unsigned int index, osg::Vec3 position) = 0;

        virtual void _setProcessMouseEvents(bool value) = 0;

        virtual bool _getProcessMouseEvents() const = 0;

    };

} // namespace SMCUI

