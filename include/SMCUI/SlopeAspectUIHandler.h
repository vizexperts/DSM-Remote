#ifndef SMCUI_SLOPEASPECTUIHANDLER_H_
#define SMCUI_SLOPEASPECTUIHANDLER_H_


/******************************************************************************
* File              : SlopeAspectUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : implementation class 
* Author            : Gaurav Garg
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

// include files
#include <Core/Base.h>
#include <Core/IObjectFactory.h>
#include <SMCUI/ISlopeAspectUIHandler.h>
#include <VizUI/UIHandler.h>

#include <Elements/IColorMap.h>
#include <Terrain/IElevationObject.h>
#include <Elements/IColorMapComponent.h>

namespace SMCUI
{
    class SMCUI_DLL_EXPORT SlopeAspectUIHandler 
        : public ISlopeAspectUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        //! Constructor
        SlopeAspectUIHandler();

        //! 
        virtual void setFocus(bool focus);

        //!
        virtual bool getFocus() const;

        //@{
        /** Called to subscribe/unsubscribe to ApplicationLoadedMessage */
        void onAddedToManager();
        void onRemovedFromManager();
        //@}

        //@{
        /** Set the extents of the area */
        void setExtents(osg::Vec4d extents);
        osg::Vec4d getExtents() const;
        //@}

        //! start on ok
        virtual void execute();

        //!
        void stopFilter();

        //! get message
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! set computation type
        void setComputeType(GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType type);

        //!
        void setElevationObject(TERRAIN::IElevationObject* obj);

        //!
        void setOutputName(std::string& name);
        //Set Map size
        void setMapSize(int mapSize);
        //! Set Color Map Type
        void setColorMapType(ELEMENTS::IColorMap::COLORMAP_TYPE colorMapType);
        //! 
        void setColorMap(ELEMENTS::IColorMap* colorMap);
    protected:

        ~SlopeAspectUIHandler();

        //! Resets all visitor parameters like firstPointSet, secontPointSet, clears the array and remove the line object
        void _reset();

    protected:
        void _populateColorMap(float minSize, float maxSize);
        //! minmax visitor
        CORE::RefPtr<CORE::IBaseVisitor> _visitor;

        //! Array containing area points
        osg::ref_ptr<osg::Vec2Array> _extents;

        //! computation type to be used
        GISCOMPUTE::ISlopeAspectFilterVisitor::ComputeType _computeType;

        //! 
        CORE::RefPtr<ELEMENTS::IColorMap> _colorMap;

        //!
        CORE::RefPtr<TERRAIN::IElevationObject> _elevObj;

        //!
        std::string name;
        //! Map Size of the Color Map
        int _mapSize;
        //Color Map type
        ELEMENTS::IColorMap::COLORMAP_TYPE _colorMapType;
        //! Color component
        CORE::RefPtr<ELEMENTS::IColorMapComponent> _colorMapComponent;
        
    };

} // namespace SMCUI

#endif // SMCUI_SLOPEASPECTUIHANDLER_H_

