#ifndef SMCUI_ILANDMARKUIHANDLER_H_
#define SMCUI_ILANDMARKUIHANDLER_H_

/*****************************************************************************
*
* File             : ILandmarkUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : ILandmarkUIHandler class
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

namespace CORE
{
    class IMetadataRecord;
    class IMetadataTableDefn;
    class IPoint;
    class IFeatureLayer;
}

namespace ELEMENTS
{
    class IModelFeatureObject;
}

namespace SMCUI 
{
    class ILandmarkUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ILandmarkUIHandler);
    public:

        //! Point updated message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> LandmarkUpdatedMessageType;

        //! Point created message type
        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> LandmarkCreatedMessageType;

        static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> LandmarkSelectedMessageType;

    public:

        enum LandmarkMode
        {
            LANDMARK_MODE_NONE,
            LANDMARK_MODE_CREATE,
            LANDMARK_MODE_EDIT
        };

        virtual CORE::IFeatureLayer* getOrCreateLandmarkLayer(const std::string &modelName) = 0;

        virtual void setLandmarkName(std::string modelName) = 0;
        virtual std::string getLandmarkName() const = 0;

        virtual void setLandmarkFilename(std::string milSymbol) = 0;
        virtual std::string getLandmarkFilename() const = 0;

        //@{
        /** Sets/Gets Point */
        virtual ELEMENTS::IModelFeatureObject* getModelFeature() const = 0;
        //@}

        //@{
        /** Sets/Gets flag to handle mouse events */
        virtual void setProcessMouseEvents(bool flag) = 0;
        virtual bool getProcessMouseEvents() const = 0;
        //@}

        //! set the current mode
        virtual void setMode(LandmarkMode mode) = 0;

        //! get the current mode
        virtual LandmarkMode getMode() = 0;

        //! set the current selected line as current line
        virtual void setSelectedLandmarkAsCurrent() = 0;

        //! set the metadata record to the current line
        virtual void setMetadataRecord(CORE::IMetadataRecord* record) = 0;

        //! get the metadata record of the current line
        virtual CORE::RefPtr<CORE::IMetadataRecord> getMetadataRecord() const = 0;

        //! update the current line
        virtual void _update() = 0;

        virtual void addToLayer(CORE::IFeatureLayer *featureLayer) = 0;

        //! add point to current selected layer
        virtual void addLandmarkToCurrentSelectedLayer() = 0;

        virtual void getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions)const = 0;

        //! get current selected layer definition
        virtual CORE::RefPtr<CORE::IMetadataTableDefn> getCurrentSelectedLayerDefn() = 0;

        //! remove the created point
        virtual void removeCreatedLandmark() = 0;

        //! set the temporary state
        virtual void setTemporaryState(bool value) = 0;

        //! get the temporary state
        virtual bool getTemporaryState() const = 0;

        //! Create a Point and add it to the scene
        virtual CORE::RefPtr<CORE::IObject> createLandmark(bool addToWorld=true) = 0;

    };
} // namespace SMCUI

#endif // SMCUI_ILANDMARKUIHANDLER_H_
