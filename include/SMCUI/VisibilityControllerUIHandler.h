#ifndef SMCUI_VISIBILITYCONTROLLERUIHANDLER_H_
#define SMCUI_VISIBILITYCONTROLLERUIHANDLER_H_

/*****************************************************************************
*
* File             : VisibilityControllerUIHandler.h
* Version          : 1.0
* Module           : SMCUI
* Description      : VisibilityControllerUIHandler class
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013-2014, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/RefPtr.h>

#include <VizUI/UIHandler.h>

#include <SMCUI/IVisibilityControllerUIHandler.h>
#include <SMCUI/export.h>

namespace ELEMENTS
{
    class IVisibilityController;
}

namespace SMCUI 
{
    class VisibilityControllerUIHandler : public IVisibilityControllerUIHandler, public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        VisibilityControllerUIHandler();

        void onAddedToManager();

        void onRemovedFromManager();

        const ELEMENTS::IVisibilityController::ControlPointList& getControlPointList() const;

        unsigned int getNumControlPoints() const;

        PointAddedStatus addControlPoint(const boost::posix_time::ptime& time, bool visibilityState);

        void removeControlPoint(unsigned int index);

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        // returns weather the selecetd object has visibility controller property 
        bool setSelectedObjectAsCurrent();

        void reset();

    protected:

        ~VisibilityControllerUIHandler();

        CORE::RefPtr<CORE::IObject> _currentSelectedObject;

        CORE::RefPtr<ELEMENTS::IVisibilityController> _visibilityController;

        bool _handleButtonRelease;
    };
} // namespace SMCUI

#endif // SMCUI_VISIBILITYCONTROLLERUIHANDLER_H_
