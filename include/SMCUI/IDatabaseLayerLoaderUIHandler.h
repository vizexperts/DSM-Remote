#pragma once

/******************************************************************************
* File              : IDatabaseLayerLoaderUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Database Layer GUI Handler
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/RefPtr.h>
#include <Database/ISQLQueryResult.h>
#include <DB/ReaderWriter.h>

namespace SMCUI
{
    /*!
     * \class IDatabaseLayerLoaderUIHandler
     *
     * \brief UIHandler interface of class implementing loading vector layers from Databse
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class IDatabaseLayerLoaderUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IDatabaseLayerLoaderUIHandler);

        enum DBType
        {
            DBType_Oracle = 1,
            DBType_MSSQL = 2
        };

        /**
        * Load layer with given options and dbtype
        * @param options DB::Options for loading layer
        * @param dbType data source to use for loading layer
        */
        virtual void loadLayer(CORE::RefPtr<DB::ReaderWriter::Options> options, const DBType dbType) = 0;

        /**
        * Execute SQL query with current connected data source 
        * @param queryStatement Statement to be executed
        * @param dbType data source to use
        */
        virtual CORE::RefPtr<DATABASE::ISQLQueryResult> executeSQLQuery(const std::string& queryStatement, const DBType dbType) = 0;

        /**
        * Try to connect to the given database
        * @param dbType data source to use
        * @return Connection was succesful or not
        */
        virtual bool connectToDB(const DBType dbType) = 0;
    };
}
