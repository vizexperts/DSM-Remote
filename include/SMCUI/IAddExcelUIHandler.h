#pragma once

/*****************************************************************************
 *
 * File             : IAddExcelUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : IAddExcelUIHandler class
 * Author           : Abhinav Goyal
 * Author email     : abhinav@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <SMCUI/export.h>

#include <App/IUIHandler.h>

#include <Util/BaseExceptions.h>


//! \brief  Handles add content gui events from the GUI
namespace SMCUI 
{
    class IAddExcelUIHandler : public CORE::IReferenced 
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IAddExcelUIHandler);

        public:

            virtual void readFile(const std::string &layerName, const std::string &url) = 0;
    };
} // namespace SMCUI


