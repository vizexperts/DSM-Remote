#ifndef SMCUI_IFLOODINGUIHANDLER_H_
#define SMCUI_IFLOODINGUIHANDLER_H_

/******************************************************************************
* File              : IFloodingUIHandler.h
* Version           : 1.0
* Module            : SMCUI
* Description       : Interface class for Flood filter UI Handler
* Author            : Gaurav Gar
* Author email      : gaurav@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <Core/Referenced.h>
#include <SMCUI/export.h>
#include <osg/Array>
#include <GISCompute/IFloodingVisitor.h>

namespace TERRAIN
{
    class IElevationObject;
}

namespace SMCUI 
{
    class IFloodingUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IFloodingUIHandler);

    public:

        //@{
        /** Set the extents of the area */
        virtual void setExtents(osg::Vec4d extents) = 0;
        virtual osg::Vec4d getExtents() const = 0;
        //@}

        virtual void setElevationObject(TERRAIN::IElevationObject* obj) = 0;

        virtual void setOutputName(std::string& name) = 0;
        //@{
         //! set the river shapefile

        virtual void setBreachPoint(const osg::Vec3d& point)=0;
        virtual osg::Vec3d getBreachPoint() const=0;
         //@}

        //@{
            //! To specify rise in water level
            virtual void setWaterLevelRise(double rise) = 0;
            virtual double getWaterLevelRise() const= 0;

        //@}
        //! To get filled Voulme
            virtual double getFilledVolume()=0;
         //@}
        virtual void execute() = 0;

        virtual void stopFilter() = 0;
    };

} // namespace SMCUI

#endif // SMCUI_IFLOODINGUIHANDLER_H_
