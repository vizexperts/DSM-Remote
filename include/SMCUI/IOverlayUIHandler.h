#ifndef SMCUI_IOVERLAYUIHANDLER_H_
#define SMCUI_IOVERLAYUIHANDLER_H_

/*****************************************************************************
 *
 * File             : IOverlayUIHandler.h
 * Version          : 1.0
 * Module           : SMCUI
 * Description      : IOverlayUIHandler class
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : SMCUI module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2012-2013, VizExperts India Private Limited (unpublished)
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCUI/export.h>
#include <Core/IMessageType.h>
#include <boost/date_time/posix_time/posix_time_types.hpp>

namespace ELEMENTS
{
    class IOverlay;
}


namespace SMCUI 
{
    class IOverlayUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, IOverlayUIHandler);

        public:

            static SMCUI_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> OverlayCreatedMessageType;

        public:

            //! create an overlay
            virtual ELEMENTS::IOverlay* createOverlay(const std::string& overlayName, 
                                                          const std::string& authorName, 
                                                          const std::string& operationName,
                                                          const std::string& operationTaskReference,
                                                          const std::string& operationType,
                                                          unsigned int operationNumber,
                                                          const boost::posix_time::ptime& creationTime) = 0;

            //! find the overlay in the world
            virtual ELEMENTS::IOverlay* findOverlay(const std::string& overlayName) = 0;

            //! set selected Overlay as current Overlay
            virtual void setSelectedOverlayAsCurrentOverlay() = 0;

            //! add layer to the current Overlay
            virtual bool createLayer(const std::string& layerName,
                                     const std::string& layerType,
                                     std::vector<std::string>& attrNames,
                                     std::vector<std::string>& attrTypes,
                                     std::vector<int>& attrWidths,
                                     std::vector<int>& attrPrecisions) = 0;

            //! get the current Overlay
            virtual ELEMENTS::IOverlay* getCurrentOverlay() const = 0;

            //! set the current Overlay
            virtual void setCurrentOverlay(ELEMENTS::IOverlay* overlay) = 0;

    };
} // namespace SMCUI

#endif // SMCUI_IOVERLAYUIHANDLER_H_
