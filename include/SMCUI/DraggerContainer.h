#ifndef SMCUI_DRAGGERCONTAINER_H_
#define SMCUI_DRAGGERCONTAINER_H_

/*****************************************************************************
*
* File             : DraggerContainer.h
* Version          : 1.1
* Module           : SMCUI
* Description      : Dragger Container class declarations
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCUI module
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <osgGA/GUIEventHandler>

#include <SMCUI/CustomDragger.h>
#include <SMCUI/IDraggerUIHandler.h>

///////////////////////////////////////////////////////////////////////////////
//!
//! \file DraggerContainer.h
//!
//! \brief DraggerContainer class declaration
//!
///////////////////////////////////////////////////////////////////////////////
namespace SMCUI
{
    ///////////////////////////////////////////////////////////////////////////
    //!
    //! \struct DraggerUIHandler
    //!
    //! \brief Forward Declaration
    //!
    ///////////////////////////////////////////////////////////////////////////
    class DraggerUIHandler;

    ///////////////////////////////////////////////////////////////////////////
    //!
    //! \struct DraggerContainer
    //!
    //! \brief Composit Dragger that manages all other draggers and dragger switching
    //!
    ///////////////////////////////////////////////////////////////////////////
    class  DraggerContainer : public osgManipulator::CompositeDragger
    {

        ////////////////////////////////////////////////////////////////////////////
        //! 
        //! \struct DraggerUpdateCallback
        //! 
        //! \brief Callback to receive the motion commands send by DraggerContainer and its child draggers
        //!
        ////////////////////////////////////////////////////////////////////////////
        class DraggerUpdateCallback: public osgManipulator::DraggerCallback
        {
        public:

            ////////////////////////////////////////////////////////////////////////////////
            //!
            //! \fn DraggerUpdateCallback(DraggerContainer* draggerContainer)
            //!
            //! \brief Constructor taking dragger to be associated in with as parameter
            //!
            //! \param draggerContainer DraggerContainer this callback is associated with
            //!
            ////////////////////////////////////////////////////////////////////////////////
            DraggerUpdateCallback(DraggerContainer* draggerContainer)
                : _draggerContainer(draggerContainer)
            { }

            ////////////////////////////////////////////////////////////////////////////////
            //!
            //! \fn receive(const osgManipulator::MotionCommand& command)
            //!
            //! \brief Checks the type of command received and applies it on the associated object of DraggerContainer
            //! 
            //! \param command osgManipulator::MotionCommand to be processed
            //!
            ////////////////////////////////////////////////////////////////////////////////
            bool receive(const osgManipulator::MotionCommand& command);

        protected:

            ////////////////////////////////////////////////////////////////////////////////
            //!
            //! \fn ~DraggerUpdateCallback()
            //!
            //! \brief Default Destructor
            //!
            ////////////////////////////////////////////////////////////////////////////////
            ~DraggerUpdateCallback(){}

            ////////////////////////////////////////////////////////////////////////////////
            //!
            //! \var _draggerContainer DraggerContainer this callback is assocaited with
            //!
            ////////////////////////////////////////////////////////////////////////////////
            DraggerContainer* _draggerContainer;
        };

    public: 

        //////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn DraggerContainer(DraggerUIHandler* draggerUIHandler)
        //!
        //! \brief Constructor taking a DraggerUIHandler object 
        //!
        //! \param draggerUIHandler DraggerUIHandler object this dragger is associated with
        //!
        //////////////////////////////////////////////////////////////////////////////
        DraggerContainer(DraggerUIHandler* draggerUIHandler);

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setupDefaultGeometry()
        //! 
        //! \brief  Overloaded Dragger::setupDefaultGeometry()
        //!         Setup geometry of dragger children
        //!
        /////////////////////////////////////////////////////////////////
        void setupDefaultGeometry();

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn         handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
        //! 
        //! \brief      Overloaded Dragger::handle(...)
        //!             Handler for GUI event, Switches the dragger if a button is pressed, calls Dragger::handle() otherwise
        //!
        //! \param ea[in]   EventAdapter associated to event
        //! 
        //! \param aa[in,out]   ActionAdapter associated to event
        //!
        /////////////////////////////////////////////////////////////////
        bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn         createButtons()
        //! 
        //! \brief      Create Buttons and add to dragger
        //!             Called on Construction
        //!
        /////////////////////////////////////////////////////////////////
        void createButtons();

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn         initializeDragger(osg::ref_ptr<osgManipulator::Dragger> dragger)
        //! 
        //! \brief      Initialize given dragger and add it to parent
        //!             Called on Construction for all child draggers
        //!
        //! \param[in,out]      dragger Dragger to be initialized
        //!
        /////////////////////////////////////////////////////////////////
        void initializeDragger(osg::ref_ptr<osgManipulator::Dragger> dragger);

        /////////////////////////////////////////////////////////////////
        //!
        //! \fn         createButton(osg::Vec4 color, double offsetAngle, 
        //!                 std::string objectName, std::string imageFilePath)
        //! 
        //! \brief      Create a single button with given parameters
        //!
        //! \param[in]      color Color of button to be added
        //!
        //! \param[in]      offsetAngle Offset at which button is to be placed on the disc
        //!
        //! \param[in]      objectName Name of button object
        //!
        //! \param[in]      imageFilePath   Path of button icon
        //!
        /////////////////////////////////////////////////////////////////
        osg::MatrixTransform* createButton(osg::Vec4 color, double offsetAngle, 
            std::string objectName, std::string imageFilePath);

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setDraggerSize(float size)
        //!
        //! \brief Set Dragger Size in screen
        //!
        //! \param[in]  Size of dragger
        //!
        ///////////////////////////////////////////////////////////////////////////////
        void setDraggerSize(float size); 

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     float getDraggerSize() const
        //!
        //! \brief Get dragger size in screen
        //!
        ///////////////////////////////////////////////////////////////////////////////
        float getDraggerSize() const ;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setFixedInScreen(bool f)
        //!
        //! \brief Set fixed in screen or not
        //!
        //! \param[in]  f Fixed in screen or not
        //!
        ///////////////////////////////////////////////////////////////////////////////
        void setFixedInScreen(bool f);

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     bool getFixedInScreen() const
        //!
        //! \brief Get if dragger is fixed in screen space or not
        //!
        ///////////////////////////////////////////////////////////////////////////////
        bool getFixedInScreen() const;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     DraggerUIHandler* getDraggerUIHandler()
        //!
        //! \brief Get DraggerUIHandler instance
        //!
        ///////////////////////////////////////////////////////////////////////////////
        DraggerUIHandler* getDraggerUIHandler();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     osgManipulator::Dragger* getTranslateAxisDragger()
        //!
        //! \brief Get CustomTranslateAxisDragger instance
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osgManipulator::Dragger* getTranslateAxisDragger();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     osgManipulator::Dragger* getTranslatePlaneDragger()
        //!
        //! \brief Get CustomTranslatePlaneDragger instance
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osgManipulator::Dragger* getTranslatePlaneDragger();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     osgManipulator::Dragger* getRotateDragger()
        //!
        //! \brief Get CustomTrackballDragger instance
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osgManipulator::Dragger* getRotateDragger();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     osgManipulator::Dragger* getScaleDragger()
        //!
        //! \brief Get CustomScaleDragger instance
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osgManipulator::Dragger* getScaleDragger();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     osg::MatrixTransform* getSelectionMatrix()
        //!
        //! \brief Get selection matrix
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::MatrixTransform* getSelectionMatrix();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     osg::MatrixTransform* getButtonsTransform()
        //!
        //! \brief Get Buttons Node
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::MatrixTransform* getButtonsTransform();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     double getScale()
        //!
        //! \brief Get dragger scale with respect to associated object
        //!
        ///////////////////////////////////////////////////////////////////////////////
        double getScale();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     ELEMENTS::IModelFeatureObject* getAssociatedModel()
        //!
        //! \brief Get associated object
        //!
        ///////////////////////////////////////////////////////////////////////////////
        ELEMENTS::IModelHolder* getAssociatedModel();

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setAssociatedModel(CORE::RefPtr<ELEMENTS::IModelFeatureObject> associatedModelObject)
        //!
        //! \brief set associated object
        //!
        ///////////////////////////////////////////////////////////////////////////////
        void setAssociatedModel(CORE::RefPtr<ELEMENTS::IModelHolder> associatedModelObject);

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void setDraggerMode(SMCUI::IDraggerUIHandler::DraggerMode mode)
        //!
        //! \brief Set dragger mode
        //!
        ///////////////////////////////////////////////////////////////////////////////
        void setDraggerMode(SMCUI::IDraggerUIHandler::DraggerMode mode);

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void traverse(osg::NodeVisitor& nv, osg::MatrixTransform* matrixTransform)
        //!
        //! \brief Convenience function to traverse all Associated matrix transforms 
        //!         Culling to scale matrix in screen space
        //!
        ///////////////////////////////////////////////////////////////////////////////
        void traverse(osg::NodeVisitor& nv, osg::MatrixTransform* matrixTransform);

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \fn     void traverse(osg::NodeVisitor& nv)
        //!
        //! \brief Overloaded: osg::Node::traverse(...)
        //!
        ///////////////////////////////////////////////////////////////////////////////
        void traverse(osg::NodeVisitor& nv);

    protected:

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _translateAxisDragger CustomTranslateAxisDragger
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::Dragger> _translateAxisDragger;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _translatePlaneDragger CustomTranslatePlaneDragger
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::Dragger> _translatePlaneDragger;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _rotateDragger CustomTrackballDragger
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::Dragger> _rotateDragger;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _scaleDragger CustomScaleDragger
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osgManipulator::Dragger> _scaleDragger;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _buttonsTransform MatrixTransform containing all buttons
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osg::MatrixTransform> _buttonsTransform;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _translateButton MatrixTransform containing translateButton
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osg::MatrixTransform> _translateButton;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _rotateButton MatrixTransform containing Rotate button
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osg::MatrixTransform> _rotateButton;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _scaleButton MatrixTransform containing Scale button
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osg::MatrixTransform> _scaleButton;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _selection Matrix transform that is updated by the draggers
        //!
        ///////////////////////////////////////////////////////////////////////////////
        osg::ref_ptr<osg::MatrixTransform> _selection;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _draggerSize Dragger size in screen
        //!
        ///////////////////////////////////////////////////////////////////////////////
        float _draggerSize;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _translatePlaneDragger CustomTranslatePlaneDragger
        //!
        ///////////////////////////////////////////////////////////////////////////////
        double _scale;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _fixedInScreen Dragger size fixed in screen or not
        //!
        ///////////////////////////////////////////////////////////////////////////////
        bool _fixedInScreen;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _draggerMode Current dragger mode
        //!
        ///////////////////////////////////////////////////////////////////////////////
        IDraggerUIHandler::DraggerMode _draggerMode;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _associatedModel Associated model that dragger is currently manipulating
        //!
        ///////////////////////////////////////////////////////////////////////////////
        CORE::RefPtr<ELEMENTS::IModelHolder> _associatedModel;

        ///////////////////////////////////////////////////////////////////////////////
        //!
        //! \var     _draggerUIHandler Instance of DraggerUIHandler
        //!
        ///////////////////////////////////////////////////////////////////////////////
        SMCUI::DraggerUIHandler* _draggerUIHandler;
    };

}
#endif // SMCUI_DRAGGERCONTAINER_H_