#pragma once

/*****************************************************************************
 *
 * File             : ICreateLayerUIHandler.h
 * Version          : 1.0
 * Module           : UI
 * Description      : ICreateLayerUIHandler class declaration
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : UI interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <SMCUI/export.h>

#include <Core/IBaseUtils.h>
#include <Core/RefPtr.h>

namespace CORE
{
    class IFeatureObject;
}

namespace SMCUI
{
    // Interface to create a new layer
    class ICreateLayerUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCUI_DLL_EXPORT, SMCUI, ICreateLayerUIHandler);

        public:

            //! Returns the UniqueID of the feature layer
            virtual const CORE::UniqueID* getLayerUniqueID() const = 0;

            //! check if the layer name is available or not
            virtual bool checkLayerNameAvailability(const std::string& name) = 0;

            //! create a layer
            virtual bool createLayer(const std::string& layerName,
                                     const std::string& layerType,
                                     std::vector<std::string>& attrNames,
                                     std::vector<std::string>& attrTypes,
                                     std::vector<int>& attrWidths,
                                     std::vector<int>& attrPrecisions) = 0;
    };

} // namespace UI

