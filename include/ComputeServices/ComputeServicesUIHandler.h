#pragma once

// include files
#include <Core/Base.h>
#include <VizUI/UIHandler.h>
#include <ComputeServices/IComputeServicesUIHandler.h>

#include <ComputeServices/HTTPInteractor.h>
#include <ComputeServices/ThriftInteractor.h>

namespace ComputeServices
{
    /**
    * \class ComputeServicesUIHandler
    *
    * \brief UIHandler for Listing, describing and executing GIS compute services provided by GeorbIS-Server and GeorbIS-Engine
    *
    * \author Nitish Puri
    *
    * \version 1.0
    *
    * \date September 2016
    *
    */
    class COMPUTESERVICES_DLL_EXPORT ComputeServicesUIHandler
        : public IComputeServicesUIHandler
        , public VizUI::UIHandler
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
    public:

        ComputeServicesUIHandler();

        ~ComputeServicesUIHandler();

        void setConnection(const Connection& connection);
//        Mode getMode() const;

        bool testConnection();

        void listServices(std::string& response);

        //! Called when UIHandler added to manager
        void onAddedToManager();

        //@{
        //** Set focus after deleting the line object */
        void setFocus(bool value);
        //@}

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //void _reset();

    protected:

        //! current mode of Line Handler
        Connection _currentConnection;

        HTTPInteractor* _httpInteractor;
        ThriftInteractor* _thriftInteractor;

    };

} // namespace ComputeServices

