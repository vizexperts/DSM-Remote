#pragma once

#include <Core/Referenced.h>
#include <ComputeServices/export.h>

namespace ComputeServices
{
    /**
    * \class IComputeServicesUIHandler
    *
    * \brief UIHandler Interface for Listing, describing and executing GIS compute services provided by GeorbIS-Server and GeorbIS-Engine
    *
    * \author Nitish Puri
    *
    * \version 1.0
    *
    * \date September 2016
    *
    */
    class IComputeServicesUIHandler : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(COMPUTESERVICES_DLL_EXPORT, ComputeServices, IComputeServicesUIHandler);

    public:

        enum SERVERMODE
        {
            GEORBIS_SERVER,
            GEORBIS_ENGINE
        };

        struct Connection
        {
            std::string ip;
            unsigned int port;
            SERVERMODE connectionMode;
        };

        virtual void setConnection(const Connection& connection) = 0;
        //virtual void getConnection(Connection& connection);

        //! Tests the current connection
        virtual bool testConnection() = 0;

        virtual void listServices(std::string& response) = 0;

        //! reset UIHandler
        //virtual void _reset() = 0;
    };

} // namespace ComputeServices

