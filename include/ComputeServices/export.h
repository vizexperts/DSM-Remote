#ifndef COMPUTESERVICES_EXPORT_H_
#define COMPUTESERVICES_EXPORT_H_

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#   if defined(COMPUTESERVICES_LIBRARY)
#      define COMPUTESERVICES_DLL_EXPORT __declspec(dllexport)
#   else
#      define COMPUTESERVICES_DLL_EXPORT __declspec(dllimport)
#   endif 
#else
#   if defined(COMPUTESERVICES_LIBRARY)
#      define COMPUTESERVICES_DLL_EXPORT __attribute__ ((visibility("default")))
#   else
#      define COMPUTESERVICES_DLL_EXPORT
#   endif 
#endif

#endif // COMPUTESERVICES_EXPORT_H_
