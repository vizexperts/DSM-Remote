//#include <>

#include <Core/Base.h>
#include <ComputeServices/IComputeServicesUIHandler.h>

namespace ComputeServices
{

    /**
    * \class HTTPInteractor
    *
    * \brief used in conjunction with ComputeServicesUIHandler class to communicate with GeorbIS-Server through curl HTTP calls
    *
    * \author Nitish Puri
    *
    * \version 1.0
    *
    * \date September 2016
    *
    */
    class HTTPInteractor
    {
    public:
        void sendRequest(const std::string& request, std::string& response, const IComputeServicesUIHandler::Connection& connection);

        // write to string use by curl as callback
        static  size_t write_to_string(void *ptr, size_t size, size_t count, void *stream);

    };
}