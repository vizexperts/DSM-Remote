#pragma once

/*****************************************************************************
 *
 * File             : ComputeServicesPlugin.h
 * Version          : 1.1
 * Module           : ComputeServices
 * Description      : ComputeServicesPlugin class declaration
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : Part of SMCreator Framework
 * Inspected by     :
 * Inspection date  :
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <App/IAppRegistryPlugin.h>
#include <App/IUIHandlerType.h>
#include <App/IManagerType.h>
#include <App/IUndoTransactionType.h>
#include <ComputeServices/export.h>
#include <Core/RefPtr.h>
#include <Core/ICoreRegistryPlugin.h>

namespace APP
{
    class ApplicationRegistry;
    class IUIHandlerFactory;
    class IManagerFactory;
    class IUndoTransactionFactory;
}


namespace CORE
{
    class InterfaceRegistry;
    class IMessageFactory;
    class IObjectFactory;
    class IVisitorFactory;
    class IComponentFactory;
}

namespace ComputeServices
{

    //! Registers all objects, interfaces and messages defined in CORE
    class ComputeServicesRegistryPlugin : public APP::IAppRegistryPlugin
    {
        public:

            //@{
            /** Library IDs */
            static const unsigned short ComputeServicesRegistryPluginStartId = 3001;
            //@}

            //@{
            /** UIHandler types */
            static const COMPUTESERVICES_DLL_EXPORT CORE::RefPtr<APP::IUIHandlerType> ComputeServicesUIHandlerType;

            //! Library name string
            static const std::string LibraryName;

            //! Plugin name string
            static const std::string PluginName;

        public:

            ~ComputeServicesRegistryPlugin();

             //@{
            /** Plugin information */
            const std::string& getPluginName() const;
            const std::string& getLibraryName() const;
            //@}

            //@{
            /** Load/Unload the library */
            virtual void load(const APP::ApplicationRegistry& cr);
            virtual void unload(const APP::ApplicationRegistry& cr);
            //@}

        protected:

            //@{
            /** Register/Deregister applications */
            virtual void registerUIHandlers(APP::IUIHandlerFactory& af);
            virtual void deregisterUIHandlers(APP::IUIHandlerFactory& af);
            //@}
    };
}