//#include <>

#include <Core/Base.h>
#include <ComputeServices/IComputeServicesUIHandler.h>

namespace ComputeServices
{

    /**
    * \class ThriftInteractor
    *
    * \brief used in conjunction with ComputeServicesUIHandler class to communicate with GeorbIS-Engine through thrift IPC calls
    *
    * \author Nitish Puri
    *
    * \version 1.0
    *
    * \date September 2016
    *
    */
    class ThriftInteractor
    {
    public:
        void sendRequest(const std::string& request, std::string& response, const IComputeServicesUIHandler::Connection& connection);
    };
}