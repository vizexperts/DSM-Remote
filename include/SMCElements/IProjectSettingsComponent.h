#ifndef SMCELEMENTS_IPROJECTSETTINGSCOMPONENT_H_
#define SMCELEMENTS_IPROJECTSETTINGSCOMPONENT_H_

/*****************************************************************************
 *
 * file             : IProjectSettingsComponent.h
 * version          : 1.1
 * module           : SMCElements
 * description      : IProjectSettingsComponent interface declaration
 * author           : Nitish Puri
 * author email     : nitish@vizexperts.com
 * reference        : SMCElements module
 * inspected by     : 
 * inspection date  : 
 *
 *****************************************************************************
 * Copyright 2010, Vizexperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * revision log (latest on top):
 *
 *
 *****************************************************************************/

#include <SMCElements/export.h>
#include <Elements/IRepresentation.h>

#include <Core/IReferenced.h>

#include <map>

namespace SMCElements
{
    class IProjectSettingsComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IProjectSettingsComponent)
        
    public:

        // Set/Get MilitarySymbol Representation Mode setting
        virtual ELEMENTS::IRepresentation::RepresentationMode
            getMilitarySymbolRepresentationMode() const = 0;

        virtual void setMilitarySymbolRepresentationMode(ELEMENTS::IRepresentation::RepresentationMode mode) = 0;
    };
}


#endif      // SMCELEMENTS_IPROJECTSETTINGSCOMPONENT_H_