#ifndef SMCELEMENTS_RESOURCEMANAGERCOMPONENT_H_
#define SMCELEMENTS_RESOURCEMANAGERCOMPONENT_H_

/*****************************************************************************
 *
 * file             : ResourceManagerComponent.h
 * version          : 1.1
 * module           : SMCElements
 * description      : ResourceManagerComponent interface declaration
 * author           : Nitish Puri
 * author email     : nitish@vizexperts.com
 * reference        : SMCElements module
 * inspected by     : 
 * inspection date  : 
 *
 *****************************************************************************
 * Copyright 2010, Vizexperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * revision log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/Component.h>
#include <Core/IBaseUtils.h>
#include <SMCElements/IResourceManagerComponent.h>

namespace SMCElements
{
    class ResourceManagerComponent : public CORE::Component
                                   , public IResourceManagerComponent
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;
        
    public:

        // constructor
        ResourceManagerComponent();
        
        void onAddedToWorldMaintainer();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void clearResources();

        void addResource(const std::string& fullResourceName);

        const MapResourceToInstances& getResourceMap() const;

    protected:

        virtual ~ResourceManagerComponent();

        MapResourceToInstances _mapResourceToInstances;

    };
}   // namespace SMCElements
#endif      // SMCELEMENTS_RESOURCEMANAGERCOMPONENT_H_