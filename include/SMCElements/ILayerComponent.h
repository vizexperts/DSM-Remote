#ifndef SMCELEMENTS_ILAYERCOMPONENT_H_
#define SMCELEMENTS_ILAYERCOMPONENT_H_

/*****************************************************************************
*
* File             : ILayerComponent.h
* Version          : 1.1
* Module           : SMCElements
* Description      : ILayerComponent Interface Declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, Viz Experts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCElements/export.h>
#include <Core/IReferenced.h>
#include <Core/Component.h>
#include <Core/IFeatureLayer.h>


namespace SMCElements 
{
    class ILayerComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, ILayerComponent);
    public: 
        typedef std::map<std::string, CORE::RefPtr<CORE::IFeatureLayer> > LayerMap;

        virtual CORE::RefPtr<CORE::IFeatureLayer> getOrCreateFeatureLayer(const std::string& name, bool addToWorld = true) = 0;

        virtual CORE::RefPtr<CORE::IFeatureLayer> getOrCreateFeatureLayer(const std::string& layerName,
            const std::string& layerType,
            std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes,
            std::vector<int>& attrWidths,
            std::vector<int>& attrPrecisions,
            bool addToWorld = true) = 0;

        // virtual void setLayerAsActive(std::string name)=0; 

        //! return feature layer if already exists otherwise return NULL
        virtual CORE::RefPtr<CORE::IFeatureLayer> getFeatureLayer(const std::string& name) = 0;

        virtual const LayerMap& getLayerMap() const = 0;
    };
}

#endif //SMCELEMENTS_ILAYERCOMPONENT_H_
