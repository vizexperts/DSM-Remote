#pragma once
/******************************************************************************
* File              : UserFolder.h
* Version           : 1.0
* Module            : SMCElements
* Description       : UserFolder Class
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/
#include <Core/Object.h>
#include <SMCElements/IUserFolderItem.h>
#include <SMCElements/IUserFolderComponent.h>
#include <Core/IDeletable.h>
#include <Core/ISelectable.h>
#include <Core/IVisibility.h>

#include <Core/export.h>

namespace SMCElements
{
    /*!
     * \class UserFolder
     *
     * \brief Immplements IUserFolderItem interface, object class representing a logical/user created layer grouping. 
     *        This grouping is managed by \see UserFolderComponent class.
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * \date 
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class UserFolder : public CORE::Object, 
        public IUserFolderItem,
        public CORE::IVisibility,
        public CORE::IDeletable,
        public CORE::ISelectable
    {

        DECLARE_META_BASE;
        DECLARE_IREFERENCED;        

    public:

        UserFolder();

        //! Add a child object
        //! Object should wither be a IUserFolderItem/IUserFolderObjectWrapper type
        void  addChild(CORE::IObject* child);

        //! Remove a child object
        //! Object should wither be a IUserFolderItem/IUserFolderObjectWrapper type
        bool removeChild(CORE::IObject* child);        

        //! Get child map
        const UserFolderCollection::UIDBaseMap& getChildrenList() const;

        //! Get OSG Node
        //! Returns NULL
        osg::Node* getOSGNode() const ;

        //! Set/Get Visibility
        void setVisibility(bool visibility);
        bool getVisibility() const;


        void setSelected(bool selected);
        bool getSelected() const;

        //! Remove from parent
        void remove();


    protected: 

        virtual ~UserFolder(){};

    private:

        UserFolderCollection _children;

        CORE::RefPtr<SMCElements::IUserFolderComponent> _folderComp;

        bool _visible;
        bool _selected;
    };
}