#pragma once

/*****************************************************************************
*
* File             : UserFolderComponent.h
* Version          : 1.1
* Module           : Elements
* Description      : UserFolderComponent Interface
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/Component.h>

#include <SMCElements/IUserFolderComponent.h>
#include <SMCElements/IUserFolderItem.h>
#include <Core/IMessageType.h>
#include <Core/CoreMessage.h>
#include <SMCElements/export.h>

namespace SMCElements
{
    /*!
     * \class UserFolder
     *
     * \brief Immplements IUserFolderItem interface, object class representing a logical/user created layer grouping. 
     *        This grouping is managed by \see UserFolderComponent class.
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * \date 
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class UserFolderComponent : public CORE::Component, public IUserFolderComponent
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

    public: 
        UserFolderComponent();

        //! On Added/Removed from world maintainer
        void onAddedToWorldMaintainer();
        void onRemovedFromWorldMaintainer();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Get the root folder containing all folders created by the user.
        IUserFolderItem* getRootFolder() const;

        //! Get Users folder configuration file name.
        std::string getFoldersFilename() const;

        //! Check weather to send object added/removed messages when a folder item is added to another folder
        //! \return False only when folder component is reading from configuration file and creating folder structure
        bool sendObjectMessages() const;

    protected:

        virtual ~UserFolderComponent();

        void _readFoldersFile();

        /* 
        *   \brief: Comparing hierarchy when reloading objects.
        *
        *   \params: 
        *           refenceObj:- refrence from comparing objects.
        *           srcObj:- src object for comparing objects.
        *
        *   \retval: void
        */
        void _compareAndAssignHeirarchy(CORE::RefPtr<CORE::IObject> &srcObj, const CORE::RefPtr<CORE::IObject> &refenceObj) const;

        CORE::RefPtr<IUserFolderItem> _rootFolder;

        std::string _foldersFilename;

        bool _sendMessages;
        
        // Reloading request check.
        bool _isReloading;
    };
}