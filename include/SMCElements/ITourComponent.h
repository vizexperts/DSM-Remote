#ifndef ELEMENTS_ITOURCOMPONENT_H_
#define ELEMENTS_ITOURCOMPONENT_H_

/*****************************************************************************
*
* File             : ITourComponent.h
* Version          : 1.1
* Module           : Elements
* Description      : ITourComponent Interface
* Author           : Abhinav Goyal
* Author email     : abhinav@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IReferenced.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <SMCElements/export.h>

#include <Util/Exception.h>

#include <osg/Vec3d>

#include <boost/date_time/posix_time/posix_time_types.hpp>

// Forward declaration

namespace SMCElements
{
    class ITourComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, ITourComponent)

    public:

        static SMCELEMENTS_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> SequenceTourChangedMessageType;

        /************************************************************************/
        /* Comparator for case-insensitive comparison in STL assos. containers  */
        /************************************************************************/
        struct ci_less : std::binary_function<std::string, std::string, bool>
        {
            // case-independent (ci) compare_less binary function
            struct nocase_compare : public std::binary_function<unsigned char,unsigned char,bool> 
            {
                bool operator() (const unsigned char& c1, const unsigned char& c2) const 
                {
                    return tolower (c1) < tolower (c2); 
                }
            };

            bool operator() (const std::string & s1, const std::string & s2) const 
            {
                return std::lexicographical_compare 
                    (s1.begin (), s1.end (),   // source range
                    s2.begin (), s2.end (),   // dest range
                    nocase_compare ());       // comparison
            }
        };


        //! will map tour name with the physical file present on the disk
        typedef std::map<std::string, std::string, ci_less> MapTourToFile;

        //! will map tour name for sequence with the physical file present on the disk
        typedef std::map<std::string, std::string, ci_less> MapSequenceTourToFile;

        //! will map sequence name with vector of tour name
        typedef std::map<std::string, std::vector<std::string> > MapSequenceToTourList;

        enum TourMode
        {
            Tour_Recording,
            Tour_Playing,
            //Sequence_Playing,
            Tour_None
        };

        //! state of the recorder while tour is being recorded
        enum RecorderState
        {
            Recorder_None,
            Recorder_Recording,
            Recorder_Pause,
            Recorder_Stop
        };

        enum PlayerState
        {
            Player_None,
            Player_Playing,
            Player_Pause,
            Player_Stop
        };

        enum RecorderType
        {
            CONTINUOUS,
            USER_KEYFRAME
        };

        //! Data Structure to hold the camera properties at particular instance
        struct CameraProperties
        {
            osg::Vec3d position;
            double heading;
            double pitch;
            double range;

            bool operator==(const struct CameraProperties &properties)
            {
                static const unsigned int IgnoreSquredDistance = 5;

                if( !osg::equivalent(this->heading, properties.heading) )
                    return false;

                if( !osg::equivalent(this->pitch, properties.pitch) )
                    return false;

                if( !osg::equivalent(this->range, properties.range) )
                    return false;

                if((this->position-properties.position).length2() > IgnoreSquredDistance)
                    return false;

                return true;
            }
        };

        //! Data structure to represent the frame
        struct Frame
        {
            //! configuration of the camera
            struct CameraProperties cameraProperties;

            //! configuration of the animation component
            boost::posix_time::ptime animationTime;

            bool operator==(const struct Frame &frame)
            {
                try
                {
                    if(this->cameraProperties==frame.cameraProperties)
                    {
                        if((this->animationTime-frame.animationTime).total_milliseconds() < 10 )
                        {
                            return true;
                        }
                    }
                }
                catch(...)
                {}
                return false;
            }

            bool operator !=(const struct Frame &frame)
            {
                return !(*this==frame);
            }

            void makeInvalid()
            {
                cameraProperties.position.set(0,0,0);
                cameraProperties.range = 0;
            }
        };

        //! will make the tour time to the frame at that time
        //! here time is absolute 
        typedef std::map<unsigned int, Frame>      MapTimeToFrame;
        typedef std::map<unsigned int, double> MapTimeToSpeed;

        //! will add the tour from external source
        virtual bool addExternalTour(const std::string &tourName,
            const std::string &tourFileName,
            bool overrideContentIfPresent) = 0;


        //! will reset the component (safe to use)
        //! doesn't reset sequence name/playing properties becouse this is used while a sequence is playing
        virtual void reset() = 0;

        //! will reset the component along with sequence name/playing properies
        virtual void resetAll() = 0;

        //! will start the recording from starting
        virtual bool startRecording(bool overridePreviousProgress = false) = 0;

        //! to pause the current recording
        virtual bool pauseRecording() = 0;

        //! to stop the current Recoring
        virtual bool stopRecording() = 0;

        //! to create new speed check point
        virtual bool createCheckPoint(unsigned int duration,double speedFactor) = 0;

        //! to delete a check point
        virtual bool deleteCheckPoint(unsigned int duration) = 0;

        //! to reset complete speed profile
        virtual bool resetSpeedProfile() = 0;

        virtual RecorderState getRecorderStatus() const = 0;

        virtual PlayerState   getPlayerStatus() const = 0;

        //! Name by which user has to save the tour
        //! flag to control the override action
        virtual void saveCurrentRecorded(const std::string &tourName, 
            bool overrideExisting=false) = 0;

        //! Save a tour sequence 
        virtual void saveSequence(const std::string& sequenceName, const std::vector<std::string>& sequence) = 0;
#if 1
        //This code has to be removed after sometime
        virtual void reloadOldSequence(const std::string& sequenceName, const std::vector<std::string>& sequence) = 0;
#endif 
        virtual void reloadSequence(const std::string& sequenceName, const std::vector<std::string>& sequence) = 0;

        virtual void deleteSequence(const std::string& sequencename) = 0;

        virtual void playSequence(const std::string& sequenceName) = 0;

        virtual void playNextTourInCurrentSequence() = 0;

        virtual void playPreviousTourInCurrentSequence() = 0;

        virtual void setSequenceName(const std::string& oldSequenceName, const std::string& newSequenceName) = 0;

        //! to save the current speed profile
        virtual void saveSpeedProfile() = 0;

        //! Will load the tour internally 
        virtual void loadTour(const std::string &tourName) = 0;

        //! Unload the tour and free memory 
        virtual void unloadTour() = 0;

        //! Returns the duration of the tour
        virtual unsigned int getTourDuration() const = 0;

        //! give list of all availale tour
        virtual const MapTourToFile &getTourMap() const = 0;

        //! give list of all available sequence
        virtual const MapSequenceToTourList &getSequenceList() const = 0;

        //! gives the list of current tour's checkpoints
        virtual const MapTimeToSpeed& getCurrentCheckpointMap() const = 0;

        virtual TourMode getTourMode() const =0;

        virtual void playTour(const std::string &tourName="")=0;

        virtual void deleteTour(const std::string & tourName) = 0;

        virtual bool pauseTour() =0;

        virtual bool stopTour() =0;

        virtual void setTourCurrentTime(unsigned int time)=0;

        virtual void previewSnap(unsigned int time) = 0;

        virtual std::string getCurrentTourName() const = 0;

        virtual std::string getCurrentSequenceName() const = 0;

        virtual bool setTourName( std::string oldTourName, std::string newTourName) = 0;

        virtual unsigned int getTourCurrentTime() const = 0;

        //! get the speed factor
        virtual float getSpeedFactor() const = 0;

        //! set the speed factor
        virtual void setSpeedFactor(float speedFactor) = 0;

        //! double the current speed factor
        virtual void increaseSpeedFactor() = 0;

        //! half the current speed factor
        virtual void decreaseSpeedFactor() = 0;

        //! set the recorder type
        virtual void setRecorderType(RecorderType type) = 0;

        //! get the recorder type
        virtual RecorderType getRecorderType() const = 0;

        //! record a frame while in User Keyframe mode
        virtual void recordKeyframe() = 0;

        virtual void setTourToPlayOnStart(std::string tourName) = 0;
        virtual std::string getTourNameToPlayOnStart() const = 0;
    };

}
#endif // ELEMENTS_ITOURCOMPONENT_H_
