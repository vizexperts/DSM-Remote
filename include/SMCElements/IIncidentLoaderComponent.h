#pragma once

/*****************************************************************************
*
* File             : IIncidentLoaderComponent.h
* Version          : 1.1
* Module           : Elements
* Description      : IIncidentLoaderComponent Interface
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IReferenced.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <SMCElements/export.h>

namespace SMCElements
{
    ////! Forward Declaration
    //class IUserFolderItem;

    /*!
     * \class IIncidentLoaderComponent
     *
     * \brief Class implementing IncidentLoaderComponent interface which acts as a container for all the User created Folders/Groups
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * \date 
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class IIncidentLoaderComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IIncidentLoaderComponent)

    public:
        struct IncidentType
        {
            std::string type;
            std::string tableName;
            std::string dateColName;
            std::map<std::string, std::string> select;
            std::string from;
            std::string whereClause;
            std::string skipList;
        };

        typedef std::map<std::string, IncidentType> IncidentTypeMap;

        virtual const IncidentTypeMap& getIncidentMap() const = 0;
        
    };
}