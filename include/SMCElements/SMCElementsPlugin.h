#pragma once
/*****************************************************************************
 *
 * File             : SMCElementsPlugin.h
 * Version          : 1.1
 * Module           : SMCElements
 * Description      : SMCElementsPlugin class declaration
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : Part of SMCElements
 * Inspected by     :
 * Inspection date  :
 *
 *****************************************************************************
  * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <Core/ICoreRegistryPlugin.h>
#include <Core/RefPtr.h>
#include <Core/IObjectType.h>
#include <Core/IVisitorType.h>
#include <SMCElements/export.h>
#include <Core/IWorldType.h>
#include <Core/IComponentType.h>
#include <Core/IPropertyType.h>
#include <Core/IRefType.h>

namespace CORE
{
    class InterfaceRegistry;
    class IMessageFactory;
    class IObjectFactory;
    class IVisitorFactory;
    class IComponentFactory;
    class IWorldFactory;
    class IPropertyFactory;
    class IRefFactory;
}

namespace SMCElements
{
    //////////////////////////////////////////////////////////////////////////
    //!
    //! Registers all objects, interfaces and messages defined in CORE
    //!
    //////////////////////////////////////////////////////////////////////////

    class SMCElementsRegistryPlugin : public CORE::ICoreRegistryPlugin
    {
        public:

            //@{
            /** Registration IDs */
            static const unsigned short SMCElementsRegistryPluginStartId = 1000;
            //@}

            //@{
            /** Reference types */
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IObjectType> UserFolderType;
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IObjectType> UserFolderObjectWrpperType;
            //@}


            //@{
            /** Component types */
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> LayerComponentType;
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> ResourceManagerComponentType;
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> TourComponentType;
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> PresentationComponentType;
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> ProjectLoadAndSaveComponentType;            
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> PresetComponentType;
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> UserFolderComponentType;
            static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> IncidentLoaderComponentType;


            //static const SMCELEMENTS_DLL_EXPORT CORE::RefPtr<CORE::IComponentType> CheckpointPathTourComponentType;


            //! Library name string
            static const std::string LibraryName;

            //! Plugin name string
            static const std::string PluginName;

        public:

            ~SMCElementsRegistryPlugin();

             //@{
            /** Plugin information */
            const std::string& getPluginName() const;
            const std::string& getLibraryName() const;
            //@}

            //@{
            /** Load/Unload the library */
            virtual void load(const CORE::CoreRegistry& cr);
            virtual void unload(const CORE::CoreRegistry& cr);
            //@}

        protected:

            //@{
            /** Register/Deregister interfaces */
            virtual void registerInterfaces(CORE::InterfaceRegistry& ir);
            virtual void deregisterInterfaces(CORE::InterfaceRegistry& ir);
            //@}

            //@{
            /** Register/Deregister messages */
            virtual void registerMessages(CORE::IMessageFactory& mf);
            virtual void deregisterMessages(CORE::IMessageFactory& mf);
            //@}

            //@{
            /** Register/Deregister objects */
            virtual void registerObjects(CORE::IObjectFactory& of);
            virtual void deregisterObjects(CORE::IObjectFactory& of);
            //@}

            //@{
            /** Register/Deregister visitors */
            virtual void registerVisitors(CORE::IVisitorFactory& vf);
            virtual void deregisterVisitors(CORE::IVisitorFactory& vf);
            //@}

            //@{
            /** Register/Deregister worlds */
            virtual void registerWorlds(CORE::IWorldFactory& wf);
            virtual void deregisterWorlds(CORE::IWorldFactory& wf);
            //@}

            //@{
            /** Register/Deregister components */
            virtual void registerComponents(CORE::IComponentFactory& of);
            virtual void deregisterComponents(CORE::IComponentFactory& of);
            //@}

            //@{
            /** Register/Deregister properties */
            virtual void registerProperties(CORE::IPropertyFactory& pf);
            virtual void deregisterProperties(CORE::IPropertyFactory& pf);
            //@}

            //@{
            /** Register/Deregister Refernced Object */
            virtual void registerReference(CORE::IRefFactory& rf);
            virtual void deregisterReference(CORE::IRefFactory& rf);
            //@}
    };
}
