#ifndef ELEMENTS_IPRESENTATIONCOMPONENT_H_
#define ELEMENTS_IPRESENTATIONCOMPONENT_H_

/*****************************************************************************
 *
 * File             : IPresentaionComponent.h
 * Version          : 1.1
 * Module           : Elements
 * Description      : IPresentaionComponent Interface
 * Author           : Somnath Singh
 * Author email     : Somnath@vizexperts.com
 * Reference        : SMCElements module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2013, VizExperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCElements/export.h>
#include <Util/Exception.h>
#include <map>

// Forward declaration

namespace SMCElements
{
    class IPresentationComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IPresentationComponent)

        public:

            typedef std::map<std::string, std::string> pdfMap;

            virtual void addPdfDocument(const std::string& documentName, const std::string& documentPath) = 0;

            virtual void deletePdfDocument(const std::string& documentName) = 0;

            virtual const pdfMap& getPdfMapCollection() const = 0;

            virtual const pdfMap& getPdfMapToSave() const = 0;

    };

}
#endif // ELEMENTS_IPRESENTATIONCOMPONENT_H_
