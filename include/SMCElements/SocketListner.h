#ifndef SOCKETLISTNER_H_
#define SOCKETLISTNER_H_

/*****************************************************************************
 *
 * File             : SocketListner.h>
 * Version          : 1
 * Module           : DMSNetwork
 * Description      : Core Network Manager class
 * Author           : Prakhar Jain
 * Author email     : author email id
 * Reference        : SRS Reference No
 * Inspected by     : code inspector's name
 * Inspection date  : YYYY-MM-DD
 *
 *****************************************************************************
 * Copyright (c) 2010-2011,VixExperts India Pvt. Ltd                                        
 *****************************************************************************
 *
 * Revision Log (latest on top):
 * Revision     Version     Date(YYYY-MM-DD)    Time(XXXX hrs)  Name
 * 
 *
 *
 *****************************************************************************/
#include <iostream>
#include <vector>
#include <map>
#include <string>

//osg headers
//#include <osg/Referenced>
#include <OpenThreads/Thread>

class SocketListner : public OpenThreads::Thread
{

    public:

        //contsructor
        SocketListner(std::string = "", int port = 0, std::string name = "");

        //destructor
        ~SocketListner();

        //initialize socket listner
        bool initialize();

        //push control messages to map
        void populateMap(std::string, std::string);

        //connect to mips
        bool connectToMIPS(std::string IP);

        //trigger mips if desktop resolution changes
        void TriggerMipsOnResolution();

        //is socket listner thread done
        bool isDone();

        //quit socket listner thread done
        void quit();

    protected:

        //thread mutex
        OpenThreads::Mutex _mutex;

        //main thread run function
        virtual void run();

    private:

        //server and client sockets
        unsigned int s_sock, c_sock;
        
        //ip address of host
        std::string _IP;
        
        //port for communication
        int _port;

        //socket listner thread done flag
        bool _done;

        //socket listner thread quit flag
        bool _quit;

        //buffer legth of network message
        int _bufferLength;

        //msg to application map
        std::map<std::string, std::string> _msgToAppMap;

        //name of the thread
        std::string _name;

        //previous desktop resolution
        int _prevHoriz, _prevVert;

        //mips trigger
        char *mipsTriggerEnv;
};

#endif // DMS_SOCKETLISTNER_H_