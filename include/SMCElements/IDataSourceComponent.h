#ifndef SMCELEMENTS_IDATASOURCECOMPONENT_H_
#define SMCELEMENTS_IDATASOURCECOMPONENT_H_

/*****************************************************************************
*
* File             : IDataSourceComponent.h
* Version          : 1.1
* Module           : SMCElements
* Description      : IDataSourceComponent Interface Declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, Viz Experts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCElements/export.h>
#include <Core/IReferenced.h>
#include <Elements/IFeatureDataSource.h>


namespace SMCElements 
{
    class IDataSourceComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IDataSourceComponent);
    public: 
        typedef std::map<std::string, CORE::RefPtr<ELEMENTS::IFeatureDataSource> > FeatureDataSourceMap;

        //! get or create the data source with the given name
        virtual ELEMENTS::IFeatureDataSource* getOrCreateFeatureDataSource(const std::string& name) = 0;

        //! return data source if already exists otherwise return NULL
        virtual ELEMENTS::IFeatureDataSource* getFeatureDataSource(const std::string& name) = 0;

        //! get the data source map
        virtual const FeatureDataSourceMap& getFeatureDataSourceMap() const = 0;
    };
}

#endif //SMCELEMENTS_IDATASOURCECOMPONENT_H_