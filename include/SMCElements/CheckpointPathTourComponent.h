#pragma once

/*****************************************************************************
*
* File             : CheckpointPathTourComponent.h
* Version          : 1.1
* Module           : SMCElements
* Description      : CheckpointPathTourComponent 
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/Component.h>
#include <SMCElements/export.h>
#include <Elements/IPathController.h>

#include <osg/Vec3d>
#include <osg/Node>
#include <Core/RefPtr.h>

#include <Elements/ICameraComponent.h>
#include <Core/IAnimationComponent.h>
#include <SMCElements/ICheckpointPathTourComponent.h>

namespace SMCElements
{
    class CheckpointPathTourComponent: public CORE::Component, public ICheckpointPathTourComponent
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

    public: 

        CheckpointPathTourComponent();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void onAddedToWorldMaintainer();

        void onRemovedFromWorldMaintainer();

        void reset();

        void startTourForPath(ELEMENTS::IPathController* pathController);

        //! Get total elapsed time in seconds of current Tour 
        unsigned int getElapsedTime();

        //! Get total time in seconds of current tour
        unsigned int getTotalTime();

    protected:

        ~CheckpointPathTourComponent();

    private:

        CORE::RefPtr<CORE::IAnimationComponent> _animationComponent;

        CORE::RefPtr<ELEMENTS::ICameraComponent> _cameraComponent;

        CORE::RefPtr<ELEMENTS::IPathController> _currentPathController;

        boost::posix_time::ptime _previousStartTime;
        boost::posix_time::ptime _previousEndTime;
        boost::posix_time::ptime _previousCurrentTime;

    };
}

