#ifndef ELEMENTS_PROJECTLOADANDSAVECOMPONENT_H_
#define ELEMENTS_PROJECTLOADANDSAVECOMPONENT_H_

/*****************************************************************************
 *
 * File             : ProjectLoadAndSaveComponent.h
 * Version          : 1.0
 * Module           : Elements
 * Description      : ProjectLoadAndSaveComponent Implementation
 * Author           : Somnath Singh
 * Author email     : Somnath@vizexperts.com
 * Reference        : SMCElements module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2013, VizExperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/Referenced.h>
#include <Core/Component.h>

#include <SMCElements/IProjectLoadAndSaveComponent.h>

namespace SMCElements
{
    class ProjectLoadAndSaveComponent : public CORE::Component,
                                  public IProjectLoadAndSaveComponent
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

        public:

            ProjectLoadAndSaveComponent();

            virtual void saveProject(const std::string& fileName, const std::string& projectFolder, const std::string& locationToSave, const bool& needToZip);

            virtual void loadProject(const std::string& projectName, const std::string& location);

            virtual bool isWebserverAlive(const std::string& link);

            void initializeAttributes();

            void setMode(Mode mode);

            Mode getMode();

            bool saveDefaultTemplate(const std::string& fileLocation, const std::string& link);

            void downloadServerSettingFile(const std::string& toSaveLocation, const std::string& link);

        protected:

            virtual ~ProjectLoadAndSaveComponent();

            void _zipProject(const std::string& projectName, const std::string& location);

            void _extractZippedFile(const std::string& zipFile, const std::string& location);

            void _uploadOnServer(const std::string& project, const std::string& link);

            void _downloadFromServer(const std::string& projectZip,  const std::string& link);

            void _getWebServerNameAndPassword();

            std::string _webserverUsername;
            std::string _webserverPassword;

            Mode _mode;

    };
}
#endif // ELEMENTS_PROJECTLOADANDSAVECOMPONENT_H_
