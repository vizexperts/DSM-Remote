#pragma once

/*****************************************************************************
*
* File             : IncidentLoaderComponent.h
* Version          : 1.1
* Module           : Elements
* Description      : IncidentLoaderComponent Interface
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/Component.h>

#include <SMCElements/IIncidentLoaderComponent.h>
#include <Core/IMessageType.h>
#include <Core/CoreMessage.h>
#include <SMCElements/export.h>

namespace SMCElements
{
    /*!
     * \class IncidentLoaderComponent
     *
     * \brief Immplements IncidentLoaderComponent interface, object class for creating Incident queries. 
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * \date 
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class IncidentLoaderComponent : public CORE::Component, public IIncidentLoaderComponent
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

    public: 
        IncidentLoaderComponent();

        //! Initialize properties from config
        void initializeAttributes();

        const IncidentTypeMap& getIncidentMap() const;

    protected:

        void _addIncident(const CORE::NamedGroupAttribute& groupAttr);
        CORE::RefPtr<CORE::NamedGroupAttribute> _getIncident() const;

        virtual ~IncidentLoaderComponent();

        IncidentTypeMap _incidentTypeMap;
    };
}