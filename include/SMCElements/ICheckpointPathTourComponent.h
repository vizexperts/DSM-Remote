#pragma once

/*****************************************************************************
 *
 * File             : ICheckpointPathTourComponent.h
 * Version          : 1.1
 * Module           : SMCElements
 * Description      : ICheckpointPathTourComponent Interface
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : SMCElements module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2013, VizExperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCElements/export.h>

namespace ELEMENTS
{
    class IPathController;
}

namespace SMCElements
{
    class ICheckpointPathTourComponent: public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, ICheckpointPathTourComponent);

    public: 

        static SMCELEMENTS_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> CPTourStartedMessageType;
        static SMCELEMENTS_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> CPTourEndedMessageType;

        virtual void startTourForPath(ELEMENTS::IPathController* pathController) = 0;

        //! Get total elapsed time in seconds of current Tour 
        virtual unsigned int getElapsedTime() = 0;

        //! Get total time in seconds of current tour
        virtual unsigned int getTotalTime() = 0;

        //! reset
        virtual void reset() = 0;
    };
}

