#pragma once

/*****************************************************************************
*
* File             : IUserFolderComponent.h
* Version          : 1.1
* Module           : Elements
* Description      : IUserFolderComponent Interface
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IReferenced.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>
#include <SMCElements/export.h>

namespace SMCElements
{
    //! Forward Declaration
    class IUserFolderItem;

    /*!
     * \class IUserFolderComponent
     *
     * \brief Class implementing UserFolderComponent interface which acts as a container for all the User created Folders/Groups
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * \date 
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class IUserFolderComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IUserFolderComponent)

    public:

        //! Message Type for notifying any observer that Folder configuration has been loaded from Users folder file
        //! Subscribed by ElementsListGUI to configure itself according to user folders
        static SMCELEMENTS_DLL_EXPORT const CORE::RefPtr<CORE::IMessageType> UserFolderConfigurationLoadedMessageType;

        //! Get the root folder containing all folders created by the user.
        virtual IUserFolderItem* getRootFolder() const = 0;

        //! Get Users folder configuration file name.
        virtual std::string getFoldersFilename() const = 0;

        //! Check weather to send object added/removed messages when a folder item is added to another folder
        //! \return False only when folder component is reading from configuration file and creating folder structure
        virtual bool sendObjectMessages() const = 0;
        
    };
}