#ifndef SMCELEMENTS_IRESOURCEMANAGERCOMPONENT_H_
#define SMCELEMENTS_IRESOURCEMANAGERCOMPONENT_H_

/*****************************************************************************
 *
 * file             : IResourceManagerComponent.h
 * version          : 1.1
 * module           : SMCElements
 * description      : IResourceManagerComponent interface declaration
 * author           : Nitish Puri
 * author email     : nitish@vizexperts.com
 * reference        : SMCElements module
 * inspected by     : 
 * inspection date  : 
 *
 *****************************************************************************
 * Copyright 2010, Vizexperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * revision log (latest on top):
 *
 *
 *****************************************************************************/

#include <SMCElements/export.h>

#include <Core/IReferenced.h>

#include <map>

namespace SMCElements
{
    class IResourceManagerComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IResourceManagerComponent)
        
    public:

        // map between a type of full Resource name and its instances allocated
        // resource name = affiliation/category/type        

        typedef std::map<std::string , unsigned int> MapResourceToInstances;

        //! Clear resource tree
        virtual void clearResources() = 0;

        virtual void addResource(const std::string& fullResourceName) = 0;

        virtual const MapResourceToInstances& getResourceMap() const = 0;

    };
}


#endif      // SMCELEMENTS_IRESOURCEMANAGERCOMPONENT_H_