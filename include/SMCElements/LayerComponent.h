#pragma once

/*****************************************************************************
 *
 * File             : LayerComponent.h
 * Version          : 1.1
 * Module           : SMCElements
 * Description      : LayerComponent class declaration
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : Component for managing layers
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, Viz Experts India Pvt. Ltd.                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Elements/FeatureLayer.h>
#include <SMCElements/export.h>
#include <Core/IObjectFactory.h>
#include <Core/Component.h>
#include <SMCElements/ILayerComponent.h>
#include <SMCElements/ITourComponent.h>
#include <SMCElements/IDataSourceComponent.h>
#include <map>

namespace SMCElements{

    // component for managing Layers
    class SMCELEMENTS_DLL_EXPORT LayerComponent 
        : public SMCElements::ILayerComponent,
          public SMCElements::IDataSourceComponent,
          public CORE::Component  

    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
  
    public:

        //! Constructor
        LayerComponent();        

        void onAddedToWorldMaintainer();
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Get or Create FeatureLayer
        CORE::RefPtr<CORE::IFeatureLayer> getOrCreateFeatureLayer(const std::string& name, bool addToWorld = true);
         CORE::RefPtr<CORE::IFeatureLayer> getOrCreateFeatureLayer(const std::string& layerName,
                             const std::string& layerType,
                             std::vector<std::string>& attrNames,
                             std::vector<std::string>& attrTypes,
                             std::vector<int>& attrWidths,
                             std::vector<int>& attrPrecisions,
                             bool addToWorld = true);
        //! Create Feature Layer
        CORE::RefPtr<CORE::IObject> createFeatureLayer(const std::string& name);

        CORE::RefPtr<CORE::IObject> createFeatureLayer(const std::string& layerName,
                             const std::string& layerType,
                             std::vector<std::string>& attrNames,
                             std::vector<std::string>& attrTypes,
                             std::vector<int>& attrWidths,
                             std::vector<int>& attrPrecisions);

        //! getFeatureLayer
        CORE::RefPtr<CORE::IFeatureLayer> getFeatureLayer(const std::string& name);

        //! get the layer map
        const LayerMap& getLayerMap() const;

        //! get or create the data source with the given name
        ELEMENTS::IFeatureDataSource* getOrCreateFeatureDataSource(const std::string& name);

        //! return data source if already exists otherwise return NULL
        ELEMENTS::IFeatureDataSource* getFeatureDataSource(const std::string& name);

        //! get the data source map
        const FeatureDataSourceMap& getFeatureDataSourceMap() const;

    protected:

        typedef std::map<std::string, CORE::RefPtr<CORE::UniqueID>, ITourComponent::ci_less> MapNameToID;


        LayerMap    _layerMap;
        //MapNameToID _mapNameToID;

        ~LayerComponent();

    private:

        CORE::RefPtr<CORE::IObject> _createFeatureDataSource(const std::string& name);

        //! Feature Data Source Map
        FeatureDataSourceMap _featureDataSourceMap;
                                     
    };
}
