#pragma once
/******************************************************************************
* File              : IUserFolderItem.h
* Version           : 1.0
* Module            : SMCElements
* Description       : IUserFolderItem Class
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/
#include <Core/IObject.h>
#include <Core/UIDBaseMapCollection.h>
#include <SMCElements/export.h>

namespace SMCElements
{
    /*!
     * \class IUserFolderObjectWrapper
     *
     * \brief Interface class for Wrapper object used to envelop a leaf object(Actual object added to world)
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * \date 
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class IUserFolderObjectWrapper : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IUserFolderObjectWrapper);

    public:

        //! Set/Get referenced object
        virtual void setReferencedObject(CORE::IObject* referencedObject) = 0;
        virtual CORE::IObject* getReferencedObject() const = 0;
    };

    /*!
     * \class IUserFolderItem
     *
     * \brief Interface class for Logical/user created group of data layers/other groups
     *
     * \note 
     *
     * \author Nitish Puri
     *
     * \version 1.0
     *
     * \date 
     *
     * Contact: nitish@vizexperts.com
     *
     */
    class IUserFolderItem : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IUserFolderItem);

        typedef CORE::UIDBaseMapCollection<CORE::IObject>  UserFolderCollection;
        
    public:

        //! Add a child object
        //! Object should either be a IUserFolderItem/IUserFolderObjectWrapper type
        virtual void addChild(CORE::IObject * child)  = 0;

        //! Remove a child object
        //! Object should either be a IUserFolderItem/IUserFolderObjectWrapper type
        virtual bool removeChild(CORE::IObject* child) = 0;

        //! Get child map
        virtual const UserFolderCollection::UIDBaseMap & getChildrenList() const = 0;
    };

}