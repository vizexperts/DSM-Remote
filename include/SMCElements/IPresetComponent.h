#pragma once

/*****************************************************************************
 *
 * File             : IWeather.h
 * Version          : 1.1
 * Module           : Elements
 * Description      : IWeather Interface
 * Author           : Somnath Singh
 * Author email     : somnath@vizexperts.com
 * Reference        : Elements module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/


#include <SMCElements/export.h>

#include <Core/IReferenced.h>
#include <Core/RefPtr.h>
#include <Core/IMessageType.h>

#include <string>
#include <map>

namespace SMCElements
{
    class IPresetComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IPresetComponent);

        public:
            
            //Defining the Type of Connection in ENUM
            enum ConnectionType
            {
                TCP,
                UDP
            };
            
            //Set the port Number
            virtual void setPortNumber(const std::string&)=0;

            //Start the socket connection
            virtual void start() = 0;

            //Stop the socket connection
            virtual void stop() = 0;
    };
}
