#pragma once
/******************************************************************************
* File              : UserFolderObjectWrapper.h
* Version           : 1.0
* Module            : SMCElements
* Description       : UserFolderObjectWrapper Class
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*******************************************************************************
*
* Revision log (latest on the top):
*
******************************************************************************/
#include <Core/Object.h>
#include <SMCElements/IUserFolderItem.h>
#include <Core/export.h>

namespace SMCElements
{
    class UserFolderObjectWrapper : public CORE::Object, public IUserFolderObjectWrapper
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:

        UserFolderObjectWrapper();


        //! Overriding Base::getName() and Base::setName()
        //! Delegates these calls to referenced object
        std::string getName() const;
        void setName(const std::string& name);

        //! Set/Get referenced object
        void setReferencedObject(CORE::IObject* referencedObject);
        CORE::IObject* getReferencedObject() const;

        osg::Node* getOSGNode() const;

    protected: 
        virtual ~UserFolderObjectWrapper();


    private:

        CORE::RefPtr<CORE::IObject> _object;
   };
}