#ifndef ELEMENTS_PRESENTATIONCOMPONENT_H_
#define ELEMENTS_PRESENTATIONCOMPONENT_H_

/*****************************************************************************
 *
 * File             : PresentationComponent.h
 * Version          : 1.0
 * Module           : Elements
 * Description      : PresentationComponent Implementation
 * Author           : Somnath Singh
 * Author email     : Somnath@vizexperts.com
 * Reference        : SMCElements module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2013, VizExperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/Referenced.h>
#include <Core/Component.h>

#include <Core/IMessageType.h>
#include <Core/IMessage.h>

#include <SMCElements/IPresentationComponent.h>

namespace SMCElements
{
    class PresentationComponent : public CORE::Component,
                                  public IPresentationComponent
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

        public:

            PresentationComponent();

            virtual void addPdfDocument(const std::string& documentName, const std::string& documentPath);

            virtual const IPresentationComponent::pdfMap& getPdfMapCollection() const;

            virtual const IPresentationComponent::pdfMap& getPdfMapToSave() const;

            virtual void deletePdfDocument(const std::string& documentName);

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            void onAddedToWorldMaintainer();

        protected:

            virtual ~PresentationComponent();

            bool _copyPdfDocumentInProjectFolder(const std::string& pdfPath);

            void reset();

            //Contain the map of Pdf Name and The pdfPath
            pdfMap _mapPdf;

            //This will only contain the Name of the Pdf and the PDF document name like [Tutorial <-> xyz.pdf ]
            pdfMap _mapPdfAndRelativePath;

    };
}
#endif // ELEMENTS_PRESENTATIONCOMPONENT_H_

