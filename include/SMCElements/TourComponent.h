#ifndef ELEMENTS_TOURCOMPONENT_H_
#define ELEMENTS_TOURCOMPONENT_H_

/*****************************************************************************
*
* File             : TourComponent.h
* Version          : 1.0
* Module           : Elements
* Description      : TourComponent Implementation
* Author           : Abhinav Goyal
* Author email     : abhinav@vizexperts.com
* Reference        : SMCElements module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2013, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/Referenced.h>
#include <Core/Component.h>
#include <Core/IAnimationComponent.h>

#include <VizUI/ICameraUIHandler.h>


#include <View/ICameraComponent.h>

#include <SMCElements/ITourComponent.h>

#include <Util/Exception.h>
#include <libjson.h>

//osg Headers
#include <osg/Timer>

// Forward declaration
#include <string>

namespace SMCElements
{
    class TourComponent : public CORE::Component,
        public ITourComponent
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

    public:

        const static unsigned int MAX_FILE_VERSION = 2;

        //! constuctor
        TourComponent();

        void update(const CORE::IMessageType& messageType, 
            const CORE::IMessage& message);

        bool addExternalTour(const std::string &tourName,
            const std::string &tourFileName,
            bool overrideContentIfPresent);

        void onAddedToWorldMaintainer();

        void onRemovedFromWorldMaintainer();

        //! will reset only the states  of the 
        //Recorder and player and wouldn't effet
        //internal data
        //! doesn't reset sequence name/playing properties becouse this is used while a sequence is playing
        void reset();

        //! will reset the component along with sequence name/playing properies
        void resetAll();

        //! will start the recording from startting
        bool startRecording(bool overridePreviousProgress=false);

        //! to pause the current recording
        bool pauseRecording();

        //! to stop the current Recoring
        bool stopRecording();

        //! to create new speed check point
        bool createCheckPoint(unsigned int duration, double speedFactor);

        //! to delete a check point
        bool deleteCheckPoint(unsigned int duration);

        //! to reset total speed profile
        bool resetSpeedProfile();


        //! Name by which user has to save the tour
        //! flag to control the override action
        void saveCurrentRecorded(const std::string &tourName, 
            bool overrideExisting=false);

        void saveSequence(const std::string& sequenceName, const std::vector<std::string>& sequence);
#if 1        
        //This code has to be removed after sometime
        void reloadOldSequence(const std::string& sequenceName, const std::vector<std::string>& sequence);
#endif

        void reloadSequence(const std::string& sequenceName, const std::vector<std::string>& sequence);

        void deleteSequence(const std::string& sequenceName);

        void playSequence(const std::string& sequenceName);

        void playNextTourInCurrentSequence();

        void playPreviousTourInCurrentSequence();

        void setSequenceName(const std::string& oldSequenceName, const std::string& newSequenceName);

        //! saving total speed profile
        void saveSpeedProfile();

        void loadTour(const std::string &tourName);

        void unloadTour();

        const MapTourToFile &getTourMap() const;

        const MapSequenceToTourList &getSequenceList() const;

        const MapTimeToSpeed& getCurrentCheckpointMap() const;

        //! this time is in mili seconds
        unsigned int getTourDuration() const;

        //set the time in mili seconds from starting
        void setTourCurrentTime(unsigned int time);

        // to show a snap shot at given time durataion
        void previewSnap(unsigned int time) ;

        //! set the tour mode
        bool setTourMode(TourMode tourMode);

        //! get the tour mode
        TourMode getTourMode() const;

        //! play the given tour
        void playTour(const std::string &tourName="");

        //! delete the given tour
        void deleteTour(const std::string& tourName);

        //! pause the current running tour
        bool pauseTour();

        //! stop the current running tour
        bool stopTour();

        RecorderState getRecorderStatus() const ;

        PlayerState   getPlayerStatus() const ;

        std::string getCurrentTourName() const;

        std::string getCurrentSequenceName() const;

        /*! 
        \fn bool setTourName( std::string oldTourName, std::string newTourName )
        \brief This function will set the name of the tour from oldName to newName.
        \param oldName : Old name of the tour.
        \param newName : New name of the tour
        \return Returns true if the name has been changed correctly.
        \author vikram singh
        \date Oct 17th 2013
        */
        bool setTourName( std::string oldTourName, std::string newTourName);

        unsigned int getTourCurrentTime() const ;

        //! get the speed factor
        float getSpeedFactor() const;

        //! set the speed factor
        void setSpeedFactor(float speedFactor);

        //! double the current speed factor
        void increaseSpeedFactor();

        //! half the current speed factor
        void decreaseSpeedFactor();

        //! set the recorder type
        void setRecorderType(RecorderType type);

        //! get the recorder type
        RecorderType getRecorderType() const;

        //! record a frame while in User Keyframe mode
        void recordKeyframe();

        void setTourToPlayOnStart(std::string tourName);
        std::string getTourNameToPlayOnStart() const;

    protected:

        void _setCurrentFrame(const unsigned int deltaTime);
        void _setCurrentSpeed(const unsigned int deltaTime);

        //! tour checkpoint to 
        struct TourSpeedCheckPoint
        {
            boost::posix_time::ptime animationTime;
            float speedFactor;
        };

        //! tour data for version 2
        struct FrameV2
        {
            osg::Matrixd viewport;
            boost::posix_time::ptime animationTime;

            void invalidate()
            {
                viewport.makeIdentity();
            }

            bool isInvalid()
            {
                return viewport.isIdentity();
            }

            bool operator==(const struct FrameV2 &frame)
            {
                try
                {
                    if(this->viewport == frame.viewport)
                    {
                        if((this->animationTime-frame.animationTime).total_milliseconds() < 1)
                        {
                            return true;
                        }
                    }
                }
                catch(...)
                {
                }

                return false;
            }

            bool operator !=(const struct FrameV2 &frame)
            {
                return !(*this==frame);
            }
        };

        //! Will reset the component completely
        //! will erase all the internal data
        void _hardReset();
        

        //! will check weather tour with same name present or not
        bool _isTourWithNameAlreadyPresent(const std::string &tourName);

        //! get the version number of the file
        unsigned int _readVersion(std::ifstream& ifstream);

        //! will read the header of the recorded frame
        bool _readHeaderFromFile(std::ifstream &ifstream);

        //!
        bool _readFrameFromFileV1(std::ifstream &ifstream,
            unsigned int &duration,
            Frame &frame);

        bool _readFrameFromFileV2(std::ifstream& ifstream,
            unsigned int& duration,
            FrameV2& frame);

        bool _readSpeedFromFile(std::ifstream &cpInFile, 
            unsigned int &duration,
            float &speedFactor);

        //!
        bool _writeHeaderToFileV1(std::ofstream &ofstream);

        //! 
        bool _writeHeaderToFileV2(std::ofstream &ofstream);

        //!
        bool _writeFrameToFileV1(std::ofstream &ofstream,
            unsigned int time,
            const Frame &frame,
            bool forceFul=false);

        //! write function for V2 file
        bool _writeFrameToFileV2(std::ofstream &ofstream,
            unsigned int time,
            const FrameV2 &frame,
            bool forceFull=false);


        bool _readFrameV1(unsigned int currStep, Frame &frame);

        bool _readFrameV2(unsigned int currStep, FrameV2& frame);

        bool _readSpeed( unsigned int currStep);

        void _loadCPFile(std::string fileName);

        std::string _getCPNameFromTourName(std::string tourFileName);

        //! Protected Destructor
        ~TourComponent();

        //! get the height at the given position
        double _getHeightAtPosition(const osg::Vec3d& position);

        //! to create new checkPoint file
        bool _createNewCheckPointFile(std::string fileName);
        void _writeHeaderToCPFile(std::ofstream &cpOutFile);

        std::string _getSequenceJson();
        void _getJsonObject(std::string jsonFile, JSONNode& mainNode);
        void _updateJson(std::string jsonFile, JSONNode mainNode);
        //! get the location of tour file (in current project directory)
        std::string _getTourFullNameWithPath(std::string fileName);


    private:

        typedef std::map<unsigned int, FrameV2> MapTimeToFrameV2;

        //! Animation component
        CORE::RefPtr<CORE::IAnimationComponent> _animationComponent;

        //! Camera Component
        CORE::RefPtr<VIEW::ICameraComponent> _cameraComponent;

        //! Maps name of the tour to the physical file on disk
        MapTourToFile _mapTourNameToFile;

        //! Maps Name of sequence to tour list
        MapSequenceToTourList _mapSequenceToTourList;

        //! map from time to frame and map time speed
        //! Map is kept to keep frames in order
        MapTimeToFrame _mapTimeToFrameV1;
        MapTimeToFrameV2 _mapTimeToFrameV2;
        MapTimeToSpeed _mapTimeToSpeed;

        MapTimeToFrame::iterator    _mapTimeToFrameV1Iter;
        MapTimeToFrameV2::iterator  _mapTimeToFrameV2Iter;

        //! Name of the current Active Tour
        std::string _currentActiveTourName;

        //! Name of current active sequence
        std::string _currentActiveSequenceName;
        unsigned int _currentActiveTourPositionInSequence;

        //! Duration of the current active tour
        unsigned int _tourDuration;

        //! Timer to record the time of the recording
        boost::posix_time::ptime _tourStartTime;

        ITourComponent::RecorderState _recorderState;

        //! Previous Camera Properties
        ITourComponent::Frame _prevFrameV1;

        //! Current Frame either in recording or playing 
        ITourComponent::Frame _currFrameV1;

        //! Previous Frame for version 2
        FrameV2 _prevFrameV2;

        //! Current Frame for version 2
        FrameV2 _currFrameV2;

        //!
        unsigned int _elapsedDuration;

        //! Current tour mode
        TourMode _mode;

        //! 
        ITourComponent::PlayerState _playerState;

        //! Current tour version
        unsigned int _tourReadVersion;

        //! Current tour write version
        unsigned int _tourWriteVersion;

        //! float speed factor
        float _speedFactor;

        //! Recorder Type
        RecorderType _recorderType;

        // these two variables are used for maintaining the playback speed set by the user
        bool _isSpeedChanged;
        MapTimeToSpeed::iterator _lastCheckPointItr;

        //! NP: tour to play on start.
        //! Meant to be a hack for playing tour in external viewer. To Be Removed.
        std::string _tourToPlayOnStart;

        bool _isSequencePlaying;

        //! flag only set while executing tick message, so that iterator is used only if simulation is running
        bool _useIterator;
    };
}
#endif // ELEMENTS_ILIGHTCOMPONENT_H_
