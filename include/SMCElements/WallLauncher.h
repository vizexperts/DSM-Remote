#ifndef DMS_WALLLAUNCHER_H_
#define DMS_WALLLAUNCHER_H_

#ifdef WIN32

/*****************************************************************************
 *
 * File             : WallLauncher.h
 * Version          : 1
 * Module           : DMSNetwork
 * Description      : Core Network Manager class
 * Author           : Prakhar Jain
 * Author email     : author email id
 * Reference        : SRS Reference No
 * Inspected by     : code inspector's name
 * Inspection date  : YYYY-MM-DD
 *
 *****************************************************************************
 * Copyright (c) 2010-2011,VixExperts India Pvt. Ltd                                        
 *****************************************************************************
 *
 * Revision Log (latest on top):
 * Revision     Version     Date(YYYY-MM-DD)    Time(XXXX hrs)  Name
 * 
 *
 *
 *****************************************************************************/

#include <iostream>
#include <vector>
#include <queue>

//osg headers
#include <osg/Referenced>
#include <OpenThreads/Thread>
#include <OpenThreads/Condition>

class WallLauncher : public OpenThreads::Thread
{

    public:

        //contsructor
        WallLauncher(std::string,int);

        //destructor
        ~WallLauncher();

        // Start listening on the socket
        bool startListening();

        // checks if any more reuqetss are left
        bool hasNext();

        // Fetches the next request in the queue and removes it from the queue
        std::string fetchNextRequest();

        // Method to stop the listener from listening
        void stop();

    protected:

        //initialize socket listner
        bool _initialize();

        //thread mutex
        OpenThreads::Mutex _mutex;

        //main thread run function
        virtual void run();

        void _processSocketError(int errorCode);

    private:
        
        //server and client sockets
        unsigned int s_sock, c_sock;
        
        //request queue
        std::queue<std::string> _requests;

        //frame data 
        char *_frameData;

        //ip address of host - controller or wall
        std::string _IP;
        
        //port for communication
        int _port;

        //socket listner thread done flag
        bool _done;

        //buffer legth of network message
        int _bufferLength;

        //! condition variable used to ensure complete cleanup
        OpenThreads::Condition _cleanUpCondition;
};

#endif //WIN32

#endif // DMS_SOCKETLISTNER_H_
