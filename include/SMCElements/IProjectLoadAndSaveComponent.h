#ifndef ELEMENTS_IPROJECTLOADANDSAVECOMPONENT_H_
#define ELEMENTS_IPROJECTLOADANDSAVECOMPONENT_H_

/*****************************************************************************
 *
 * File             : IProjectLoadAndSaveComponent.h
 * Version          : 1.1
 * Module           : Elements
 * Description      : IProjectLoadAndSaveComponent Interface
 * Author           : Somnath Singh
 * Author email     : Somnath@vizexperts.com
 * Reference        : SMCElements module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2013, VizExperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IReferenced.h>
#include <SMCElements/export.h>

namespace SMCElements
{
    class IProjectLoadAndSaveComponent : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCELEMENTS_DLL_EXPORT, SMCElements, IProjectLoadAndSaveComponent)

        enum Mode
        {
            SERVER,
            LOCAL,
            NONE
        };

        public:

            virtual void saveProject(const std::string& fileName, 
                                     const std::string& projectFolder, 
                                     const std::string& locationToSave, 
                                     const bool& needToZip) = 0;

            virtual void loadProject(const std::string& projectName, 
                                     const std::string& location) = 0;

            virtual bool isWebserverAlive(const std::string& link) = 0;

            virtual void setMode(Mode mode) = 0;
            virtual Mode getMode() = 0;

            // save the template in setting.json file in apache server
            virtual bool saveDefaultTemplate(const std::string& fileLocation, const std::string& link) = 0;

            // download file from server
            virtual void downloadServerSettingFile(const std::string& toSaveLocation, const std::string& link) = 0;
    };

}
#endif // ELEMENTS_IPROJECTLOADANDSAVECOMPONENT_H_
