#ifndef SMCELEMENTS_PROJECTSETTINGSCOMPONENT_H_
#define SMCELEMENTS_PROJECTSETTINGSCOMPONENT_H_

/*****************************************************************************
 *
 * file             : ProjectSettingsComponent.h
 * version          : 1.1
 * module           : SMCElements
 * description      : ProjectSettingsComponent interface declaration
 * author           : Nitish Puri
 * author email     : nitish@vizexperts.com
 * reference        : SMCElements module
 * inspected by     : 
 * inspection date  : 
 *
 *****************************************************************************
 * Copyright 2010, Vizexperts India Private Limited (unpublished)                                       
 *****************************************************************************
 *
 * revision log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/Component.h>
#include <Core/IBaseUtils.h>
#include <SMCElements/IProjectSettingsComponent.h>

namespace SMCElements
{
    class ProjectSettingsComponent : public CORE::Component
                                   , public IProjectSettingsComponent
    
    {
        DECLARE_IREFERENCED;
        DECLARE_META_BASE;

    public:

        ProjectSettingsComponent();

        void onAddedToWorldMaintainer();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        // get/set militarysymbol representation mode
        ELEMENTS::IRepresentation::RepresentationMode 
            getMilitarySymbolRepresentationMode() const;

        void setMilitarySymbolRepresentationMode(ELEMENTS::IRepresentation::RepresentationMode mode);

    protected:

        virtual ~ProjectSettingsComponent();

        ELEMENTS::IRepresentation::RepresentationMode _militarySymbolRepresentationMode;

    };
}

#endif // SMCELEMENTS_PROJECTSETTINGSCOMPONENT_H_


