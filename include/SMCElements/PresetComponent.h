#pragma once

/*****************************************************************************
 *
 * File             : WeatherSocketComponent.h
 * Version          : 1.1
 * Module           : Elements
 * Description      : WeatherSocketComponent Interface
 * Author           : Somnath Singh
 * Author email     : somnath@vizexperts.com
 * Reference        : Elements module
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

// Headers Files
#include <SMCElements/IPresetComponent.h>

#include <SMCElements/WallLauncher.h>

#include <Core/Referenced.h>
#include <Core/RefPtr.h>
#include <Core/Component.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMessageType.h>

#include <OpenThreads/Mutex>


namespace SMCElements
{
    class PresetComponent : public CORE::Component,
                            public SMCElements::IPresetComponent
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

        public:

            //!
            PresetComponent();

            void start();
            void stop();

            void setPortNumber(const std::string&);
            std::string getPortNumber();

            void setIPAddress(const std::string&);
            std::string getIPAddress();

            void initializeAttributes();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
            void onRemovedFromWorldMaintainer();
            void onAddedToWorldMaintainer();

      private:

          void _processRequest(const std::string &req);

          //! World Maintainer instance
          CORE::RefPtr<CORE::IWorldMaintainer> _wmain;

          //Set up the actual connection
          void _ConnectionSetupStatus();

          //Setup the TCP connections
          void _setupTCPConnection();

          //Setup the UDP connections
          void _setupUDPConnection(std::string,double);

          //Responsible to send Messages to the Subsribed users
          void _sendMessage(CORE::IMessageType*);

          //Check whether the established connection need to be stopped
          bool _needToDisconnect();

          //Do the clean up for the socket connection and reset the connection variables
          void _cleanUp();

           //Contain the Server IP 
          std::string _serverIP;

          //Contain the Port Number
          std::string _portNumber;

          //Variable determining whether to start or stop the TCP or UDP connection
          bool _startTCPConnection;
          bool _startUDPConnection;
          bool _stopTCPConnection;
          bool _stopUDPConnection;
          bool _connectionAllowed;

          //Variable to check whther the connection is allowed or not 
          UTIL::ThreadPool* tp;
#ifdef WIN32
          WallLauncher *_sockListner;
#endif //WIN32
          protected:

            //! A mutex for synchronization 
            OpenThreads::Mutex _mutex;

            virtual ~PresetComponent(){}

    };
}
