#ifndef SMCQT_UNDOREDOGUI_H_
#define SMCQT_UNDOREDOGUI_H_

/*****************************************************************************
*
* File             : UndoRedoGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : UndoRedoGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <App/IUIHandlerManager.h>


namespace SMCQt
{
    class UndoRedoGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        UndoRedoGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

        public slots:

            void undo();
            void redo();

    protected:

        virtual ~UndoRedoGUI();

        virtual void _loadAndSubscribeSlots();

    private:

        //! undo stack limit
        unsigned int _undoStackSize;

        //! XXX: currently UIHandler manager itself handles Undo/Redo transactions
        CORE::RefPtr<APP::IUIHandlerManager> _uihandlerManager;
    };

} // namespace SMCQt

#endif  // SMCQT_UNDOREDOGUI_H_
