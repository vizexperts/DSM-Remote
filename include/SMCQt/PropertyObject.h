#ifndef SMCQT_PROPERTYOBJECT_H_
#define SMCQT_PROPERTYOBJECT_H_

/*****************************************************************************
*
* File             : PropertyObject.h
* Version          : 1.0
* Module           : SMCQt
* Description      : PropertyObject class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : QObject derived class used for showing Object attributes
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <QObject>

namespace SMCQt
{

    class PropertyObject : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
            Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
            Q_PROPERTY(QString value READ value WRITE setValue NOTIFY valueChanged)
            Q_PROPERTY(int precesion READ precesion WRITE setPrecesion NOTIFY precesionChanged)

    public:

        PropertyObject(QObject* parent = 0);
        PropertyObject(const QString &name, const QString &type, const QString &value ,int precesion, 
            QObject *parent = 0);

        QString name() const;
        void setName(const QString &name);

        QString type() const;
        void setType(const QString &type);

        QString value() const;
        void setValue(const QString &value);

        int precesion() const;
        void setPrecesion(int precesion);

signals:
        void nameChanged();
        void typeChanged();
        void valueChanged();
        void precesionChanged();

    private:
        QString m_name;
        QString m_type;
        QString m_value;
        int m_precesion;

    };

 
}

#endif //SMCQT_PROPERTYOBJECT_H_