#pragma once

/*****************************************************************************
*
* File             : SlopeAspectGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : SlopeAspectGUI class declaration
* Author           : Rahul Srivastava
* Author email     : rahul@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>

#include <SMCUI/ISlopeAspectUIHandler.h>
#include <SMCUI/IColorMapUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
#include <Elements/IColorMap.h>
#include <Elements/IColorMapComponent.h>

namespace SMCQt
{
    // Creates a new layer
    class SlopeAspectGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        static const std::string SlopeAnalysisObjectName;

        SlopeAspectGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void  _setProgressValue(int);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void _handlePBStartClicked();
                void _handlePBStopClicked();
                void  _handleProgressValueChanged(int);
                void _handlePBAreaClicked(bool pressed);
                void _reset();
                void _hadleNewColorMapAdded(const QString& text);
				

    protected:

        virtual ~SlopeAspectGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        void _populateElevationObject();

        void _populateColorMap();
        void _populateAnalysisListMenu();
		void _updateProgressBarVisibility(bool value);
        //void _populateColorMapMenu();

    protected:

        //! Slope Aspect UIHandler
        CORE::RefPtr<SMCUI::ISlopeAspectUIHandler> _uiHandler;

        //! ColorMap UI Handler
        CORE::RefPtr<SMCUI::IColorMapUIHandler> _colorUIHandler;

        CORE::RefPtr<ELEMENTS::IColorMapComponent> _colorMapComponent;


        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

        QObject* _slopeAspectObject;

    };

} // namespace SMCQt
