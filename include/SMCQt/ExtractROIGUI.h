#ifndef EXTRACTROIGUI_H
#define EXTRACTROIGUI_H

/*****************************************************************************
*
* File             : ExtractROIGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : Image Reprojection class declaration
* Author           : Sandeep
* Author email     : sandeep@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : code inspector's name
* Inspection date  : YYYY-MM-DD
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <Stereo/export.h>
#include <Core/IBaseUtils.h>
#include <Core/IBaseVisitor.h>
#include <Core/IObject.h>

#include <VizQt/QtGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAddContentUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Stereo/StereoViewerGUI.h>
#include <Stereo/StereoSceneGenerator.h>
#include <Stereo/MainWindowGUI.h>
#include <Stereo/Image.h>



namespace SMCQt
{
    class ExtractROIGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED
        Q_OBJECT

    public:

        ExtractROIGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);


    private:


        //Last Path History
        std::string _lastPath;

        public slots:

            /** Set/Get active function */ 
            void setActive(bool active);

            void closeStereoWizard(Stereo::MainWindowGUI*);


signals:

            //! signal emmited when a filteroperation is completed
            void _updateGUIStatus();


            private slots:

                //! Slots for event handling
                void _handleInputBrowseButtonClicked();
                void _handleoutputBrowseButtonClicked();
                void _handleOkButtonClicked();
                void _handleCancelButtonClicked(); 
                void connectPopupLoader(QString);

                void _viewImage(QString);
                void _handleGUIStatus();

    protected:

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        virtual ~ExtractROIGUI();


        //! UIHandler for loading data
        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;


        std::vector<Stereo::MainWindowGUI*> _stereoWizard;
        QObject* _extractROI;

    };
}// namespace SMCQt

#endif // EXTRACTROIGUI_H
