#pragma once


#include <QWidget>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QPushButton>
#include <QToolButton>
#include <QCheckBox>

#include <osg/Vec4d>

#include <Core/IBaseUtils.h>

#include <VizQt/LayoutFileGUI.h>
#include <VizQt/QtGUI.h>

#include <SMCUI/IAddContentUIHandler.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouseMessage.h>

namespace SMCQt
{
    class WebCamGUI : public VizQt::LayoutFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED

        Q_OBJECT

        QCheckBox         *_cbYahoo;
        QCheckBox        *_cbBing;
        QCheckBox        *_cbOSM;
        QCheckBox        *_cbWorldWind;
        QCheckBox        *_cbGoogleHybrid;
        QCheckBox        *_cbGoogleStreets;
       
    public:

        WebCamGUI(QWidget *parent = 0);

        void initialize();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

    public slots:

            void setActive(bool active);
            

    private slots:
            
   protected:

       bool _isAppInstanceRunning;

       void _startWebCam();

       //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        virtual ~WebCamGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void makeConnections();

        void _loadAddContentUIHandler();
    };
}
