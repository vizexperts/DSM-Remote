#pragma once
/*****************************************************************************
*
* File             : CVAGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CVAGUI class declaration
* Author           : Sandeep
* Author email     : sandeep@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : code inspector's name
* Inspection date  : YYYY-MM-DD
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Stereo/export.h>
#include <Core/IBaseUtils.h>
#include <Core/IBaseVisitor.h>
#include <Core/IObject.h>
#include <VizUI/UIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAddContentUIHandler.h>
#include <GISCompute/IChangeDetectionCVA.h>
#include <VizQt/LayoutFileGUI.h>

#include <Stereo/StereoViewerGUI.h>
#include <Stereo/StereoSceneGenerator.h>
#include <Stereo/MainWindowGUI.h>
#include <Stereo/Image.h>

#ifdef USE_QT4
#include <QtGui/QMainWindow>
#else
#include <QtWidgets/QMainWindow>
#endif

#include <QtDeclarative/QDeclarativeView>


namespace SMCQt
{
    class CVAGUI :public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT


    public:

        static const std::string ChangeDetectionPopup;

        CVAGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);

            void closeStereoWizard(Stereo::MainWindowGUI*);
            //@}

signals:

            //! signal emmited when a filteroperation is completed
            void _updateGUIStatus();        

            private slots:

                //! Slots for event handling
                void _handleInput1BrowseButtonClicked();

                void _handleInput2BrowseButtonClicked();

                void _handleDistanceImageButtonClicked();

                void _handleAngleImageButtonClicked();

                void _viewImage(QString);

                void _handleStartButtonClicked();
                void _handleStopButtonClicked();

                void _handleGUIStatus();
                void _reset();

    protected:

        virtual ~CVAGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        void _loadAddContentUIHandler();

        //! Last path
        std::string _lastPath;

        //! Reference of AddContentUIHandler
        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;

        //! CVA Visitor
        CORE::RefPtr<GISCOMPUTE::IChangeDetectionCVA> _cva;

        CORE::RefPtr<CORE::IBaseVisitor> _visitor;
        std::vector<Stereo::MainWindowGUI*> _stereoWizard;

        Stereo::Image::EnhancementType _enhancementType;

    };

} // namespace SMCQt

