#pragma once

/*****************************************************************************
*
* File             : DatabaseLayerGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : DatabaseLayerGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IDatabaseLayerLoaderUIHandler.h>
#include <SMCElements/IIncidentLoaderComponent.h>
#include <VizQt/QtUtils.h>
#include <Core/ISelectable.h>

namespace SMCQt
{
    class DatabaseLayerGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        //! Structure used to store incident query in case of a query stack
        struct IncidentGroupedQuery
        {
            QString     _queryName;
            QString     _incidentType;
            QString     _whereClause;
            QDateTime   _fromTime;
            QDateTime   _toTime;
            QColor      _queryColor;
        };

        DatabaseLayerGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void _loadAndSubscribeSlots();
signals:
        void  _setProgressValue(int);


        public slots:

            void popupLoaded(QString type);
            void addOracleTable();
            void addIncidentLayer();
            void userspaceChanged(QString name);
            void addIncidentType(QString incidentName);
            void plottingTypeChanged(QString plottingType);
            void addAllIncidents();
            void addGroupedQuery();
            void removeGroupedQuery(int index);
            void groupedQuerySelected(int index, bool addingNew = false, bool selectionOnly = false);
            void popupClosed();
            void setProgressValue(int value);
            void renameQuery();

    protected:

        virtual ~DatabaseLayerGUI();

        void _populateUserspaceList(QString databaseName);


        bool _validateIncidentTable(const  SMCElements::IIncidentLoaderComponent::IncidentType& incidentType);

        //! update grouped query name list in the UI
        void _populateQueryNameList();

        std::string _currentDatabaseName;
        std::string _currentUserspaceName;
        std::string _currentTableName;
        std::string _selectedIncident;

        CORE::RefPtr<CORE::ISelectable> _currentSelection;
        bool _oracleUILoaded;
        bool _incidentUILoaded;


        CORE::RefPtr<SMCUI::IDatabaseLayerLoaderUIHandler> _databaseLayerLoaderUIHandler;
        CORE::RefPtr<SMCElements::IIncidentLoaderComponent> _incidentLoaderComponent;

        QList< IncidentGroupedQuery >           _incidentGroupedQueryStack;
        QList< QColor >                         _defaultPalletColors;    

    };
}
