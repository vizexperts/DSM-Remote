#ifndef SMCQT_DOWNLOADRASTERGUI_H_
#define SMCQT_DOWNLOADRASTERGUI_H_

/*****************************************************************************
 *
 * File             : DownloadRasterGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : DownloadRasterGUI class declaration
 * Author           : Lokesh Sharma
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>

#include <VizQt/LayoutFileGUI.h>

#include <osgEarthUtil/WMS>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/QMLTreeModel.h> 
#include <QtCore/QTimer>
#include <osgEarthUtil/WFS>
#include <SMCUI/IAreaUIHandler.h>

class QLineEdit;
class QLabel;

namespace SMCQt
{
    class DownloadRasterGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

        public:

            static const std::string GetTiffFromExtentsPopup;
            static const std::string GeorbisServerlistDir;

            // flag to confirm that file is written
            static bool fileWritten;
            //! Constructor
            DownloadRasterGUI();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
            
        public slots:

            //@{
            /** Set/Get active function */
            void connectPopUpLoader(QString type);
            //@}

            //! clear the GUI
            void reset();

            void _updateLayerName();

        private slots:

            //! Updates the area editbox regularly
            void _handleConnectButtonClicked();
            void _handleExportButtonClicked();
            void _handleBrowseButtonClicked();
            
            // clear selected Area
            void _handleClearAreaButton();

            // curl write function
            static  size_t write_to_File(void *ptr, size_t size, size_t nmemb, FILE *stream);
            // handle the selection of area / mark the extent
            void _handlePBAreaClicked(bool pressed);

        protected:

            virtual ~DownloadRasterGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            //! populate the WMS GUI
            void _populate(const osgEarth::Util::WMSLayer::LayerList& layerList, WmsTreeModelItem* parent);

            //! remove the child item
            void _removeChildItem(WmsTreeModelItem* item);

            //! get the selected layer names as comma seperated values
            void _getLayerNames(std::string& layers, WmsTreeModelItem* item);

            //! read all server names present in appdata and insert these in _georbISServerList
            void _readAllServers();

            //! get wms layer path from server name
            std::string _getWMSPath(std::string serverName);

        private:

            //! Qt timer
            QTimer _timer;

            //! Server Address
            std::string _serverAddress;
            
            WmsTreeModel* _getTiffTreeModel;
            WmsTreeModelItem* _childItem;

            // inMemory list for saving server names saved in appdata
            std::vector<std::string> _georbISServerList;

            std::string _layerName;

            bool _noFurtherUpdate;

            std::string _outputDirectory;

            // to mark the Extents
            CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;
    };

} // namespace SMCQt

#endif  // SMCQT_DOWNLOADRASTERGUI_H_

