#ifndef SMCQT_VIEWSHEDOBSERVEROBJECT_H_
#define SMCQT_VIEWSHEDOBSERVEROBJECT_H_

/*****************************************************************************
*
* File             : ViewshedObserverObject.h
* Version          : 1.0
* Module           : SMCQt
* Description      : Viewshed Observer Object class declaration
* Author           : Mohan Singh
* Author email     : mohan@vizexperts.com
* Reference        : QObject derived class used for showing Object attributes
*                    for Viewshed Analysis
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <QObject>
#include <QMetaObject>
#include <qcolor.h>


namespace SMCQt
{

    class ViewshedObserverObject : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
            Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
            Q_PROPERTY(double longi READ longi WRITE setLongi NOTIFY longiChanged)
            Q_PROPERTY(double lat READ lat WRITE setLat NOTIFY latChanged)
            Q_PROPERTY(double height READ height WRITE setHeight NOTIFY heightChanged)
            Q_PROPERTY(double range READ range WRITE setRange NOTIFY rangeChanged)

            Q_PROPERTY(double lookatlongi READ lookatlongi WRITE setlookatLongi NOTIFY lookatlongiChanged)
            Q_PROPERTY(double lookatlat READ lookatlat WRITE setlookatLat NOTIFY lookatlatChanged)
            Q_PROPERTY(double lookatele READ lookatele WRITE setlookatEle NOTIFY lookateleChanged)
            Q_PROPERTY(double horizontalFOV READ horizontalFOV WRITE sethorizontalFOV NOTIFY horizontalFOVChanged)
            Q_PROPERTY(double verticalFOV READ verticalFOV WRITE setverticalFOV NOTIFY verticalFOVChanged)

    public:

        ViewshedObserverObject(QObject* parent = 0);
        ViewshedObserverObject(const QString &name, const QColor &color, double longi, double lat, double height, double range, QObject *parent = 0);
        ViewshedObserverObject(const QString &name, const QColor &color, double longi, double lat, double height, double range, double lookatlongi ,double lookatlat, double lookatele, double horizontalFOV, double verticalFOV, QObject *parent = 0);

        QString name() const;
        void setName(const QString &name);

        QColor color() const;
        void setColor(const QColor &color);

        double longi() const;
        void setLongi(double longi);

        double lat() const;
        void setLat(double lat);

        double height() const;
        void setHeight(double height);

        double range() const;
        void setRange(double range);

        double lookatlongi() const;
        void setlookatLongi(double lookatlongi);

        double lookatlat() const;
        void setlookatLat(double lookatlat);

        double lookatele() const;
        void setlookatEle(double lookatele);

        double horizontalFOV() const;
        void sethorizontalFOV(double horizontalFOV);

        double verticalFOV() const;
        void setverticalFOV(double verticalFOV);
signals:
        void nameChanged();
        void colorChanged();
        void longiChanged();
        void latChanged();
        void heightChanged();
        void rangeChanged();

        void lookatlongiChanged();
        void lookatlatChanged();
        void lookateleChanged();
        void horizontalFOVChanged();
        void verticalFOVChanged();

    private:
        
        QString m_name;
        QColor m_color;
        double m_longi;
        double m_lat;
        double m_height;
        double m_range;

        double m_lookatlongi;
        double m_lookatlat;
        double m_lookatele;
        double m_horizontalFOV;
        double m_verticalFOV;
        //CORE::RefPtr<CORE::IObject> iObject;

    };

}

#endif //SMCQT_VIEWSHEDOBSERVEROBJECT_H_