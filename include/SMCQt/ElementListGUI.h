#pragma once

/*****************************************************************************
*
* File             : ElementListGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ElementListGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <Core/NamedAttribute.h>
#include <Core/IBaseUtils.h>

#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/ElementListTreeModel.h>
#include <VizUI/ISelectionUIHandler.h>
#include <SMCQt/VectorTableModel.h>

namespace CORE
{
    class IObject;
}

namespace SMCQt
{
    class SMCQT_DLL_EXPORT ElementListGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:
        static const std::string ELEMENTLISTTREEVIEW;
        static const std::string ELEMENTLIST;
        static const std::string OPENATTRIBUTETABLEPOPUP;
        static const std::string REORDERRASTERITEMPOPUP;
        static const std::string ITEMRENAMEPOPUP;
        static const std::string ADDFOLDERPOPUP;
        static const std::string TOOLBOXOBJECTNAME;
        static const std::string ITEMRELOADPOPUP;
        static const std::string DGNClassificationToolboxButton;

        ElementListGUI();

        void onRemovedFromGUIManager();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

        //@{
        /** Set/Get function for element icons group attribute */
        void setElementIcons(const CORE::NamedGroupAttribute& groupattr);
        CORE::RefPtr<CORE::NamedGroupAttribute> getElementIcons() const;
        //@}

        //@{
        /** Set/Get function for top-level icon */
        void setTopLevelIcon(const std::string& iconfile);
        const std::string& getTopLevelIcon() const;
        //@}

        public slots:
        //void populateTemplateList();
        //void populateTemplateComboList();
        //void populateStyleTemplateList(); 
        //void setJsonEditorInput(QString templateName);
        //void setJsonEditorOutput();
         void reloadModelObject();
        //void populateEnumList(QString name, QString parentName, QString value);
        //void jsonFileSelected(int index);

        //! Create empty folder object inside the current object if possible
        //! Show user error if not possible
        void createChildFolder(QString name);

        //! Reload the current proxy object
        void reloadCurrentObject();
        void connectItemReloadPopup();
        void browseNewDataSourceButtonClicked();

        private slots:

        //! handles clicking of item in tree view
        void _onItemClicked(int index);

        //! handle double click on a item
        void _onItemDoubleClicked(int index);

        //! Handle delete key pressed when Treeview has focus
        void _onItemDeleteKeyPressed(int index);

        //! handle raster item index change 
        void _handleRasterItemChanged(int itemIndex, int to);

        void _searchElements(QString stringToSearch);

        void _renameObject(int index, QString name);

        //! Signal handlers for delete object confirmation
        void deleteObject();
        void cancelDeleteObject();

        void _openRenameItemPopup(int);

        void _openAttributeTablePopup();
        void _rasterMove(int positionOption);
        void  _itemChangedSendNotify(CORE::RefPtr<const CORE::IObject>);
        void _handleBrowseButtonClicked(); 
        void _handleSaveButtonClicked();
        void _popupClosed();


    protected:
        //! Destructor
        virtual ~ElementListGUI();

        void _emptyItemAndDeleteChild(SMCQt::ElementListTreeModelItem *item);

        //! Populates the tree item using the object node
        void _populateElementTree(SMCQt::ElementListTreeModelItem* parent, SMCQt::ElementListTreeModelItem* item,
            const CORE::IObject* object);

        //! function to find SelectionUIHandler
        void _findSelectionUIHandler();

        //! function to unsubribe the message
        void _unsubscribeMessage(const CORE::IObject* object);

        //! 
        void _setShowLayerChild(bool value);

        //! 
        bool _getShowLayerChild() const;

        void _setVisualizationSpinners();
        //! Initialize all the default tree items such as LocalParentItem, RemoteParentItem, LocalLayersItem, RemoteLayersItem
        void _initializeAllDefaultItems();
        //! this Function Populate rightClick sensative List
        void _pupulateRightClickMenu(int index);

        ElementListTreeModel* _treeModel;

        QObject* _elementListTreeView;
        QObject* _openAttributeTableView;

        //! Parent Layers Item for Local Objects
        ElementListTreeModelItem* _localLayersItem;

        //! selction UI Handler
        CORE::RefPtr<VizUI::ISelectionUIHandler> _selectionUIHandler;

        //! remove element
        bool _removeElementTree(SMCQt::ElementListTreeModelItem* item, const CORE::IObject* object);

        //! find element
        SMCQt::ElementListTreeModelItem* _findElementTree(SMCQt::ElementListTreeModelItem* item,
            const CORE::IObject* object);
        //Enum for RightClick Contextual Menu uID
        enum RightClickUid
        {
            ADD_FOLDER_UID,
            DELETE_UID,
            RENAME_UID,
            SCALE_VISIBILITY_UID,
            RELOAD_UID
        };
        //! Element icons
        CORE::RefPtr<CORE::NamedGroupAttribute> _elementIcons;

        //! Top-element icon
        std::string _topElementIcon;

        //! selection component
        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

        //! current selection
        CORE::RefPtr<CORE::ISelectable> _currentSelection;

        //! flag to decide whther to show feature layer childs
        bool _showLayerChild;

        //! Index of item to be deleted
        CORE::IDeletable* _itemToBeDeleted;
        // creation of model
        VectorTableModel* _tableModel; 
        int _dgnLayerCountInWorld;
		QString _fileName;

        // check for reloading of component.
        bool _isReloading;
    };

} // namespace SMCQt
