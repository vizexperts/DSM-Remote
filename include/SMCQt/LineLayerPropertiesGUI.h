#pragma once

/*****************************************************************************
*
* File             : LineLayerPropertiesGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : LineLayerPropertiesGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/Referenced.h>

#include <SMCQt/DeclarativeFileGUI.h>

#include <Core/ISelectable.h>
#include <GIS/IConnectedFeatureStyle.h>
namespace SMCQt
{
    class LineLayerPropertiesGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE; 
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string profileTemplate;
        LineLayerPropertiesGUI();
        private slots:

            void connectPopup(QString type);

            void changeDrawTechniqueType();

            void _makeFenceFolder();
            
            void _handlePopupOkButtonPressed();
            void popupClosed();

            // browse Gate file
            void browseButtonClicked(QString val,bool vectorFilter=true);

            //void browseUnfencedButtonClicked();

            void recFenceFolderCreated(bool isAlreadyFolder=false);

            void applyInputStyle(CORE::RefPtr<CORE::IObject> obj);

            // creates new QueryGroup from existing saved ones
            void _editExistingConnectedFeature(QString selectedValue);
            void _clearConnectedFeatureStruct();
            //return false if templateName Name not Present Else populate and Fill data
            bool _getConnectedFeatureTemplate(std::string templateName);
            //delete connected Feature Template on ok click
            void _okSlotForDeleting();

            //cancel button handling slot
            void _cancelSlotForDeleting();

            //delete Connected Feature Template  while editing
            void _deleteConnectedFeatureTemplate(QString);
            // for populating Template combobox
            void _readAllTemplate();
            //check whether Template  Name Exist in JSon folder or not
            bool _isAlreadyPresent(std::string name);
            //Dump Template Data in JSON format
            void _writeTemplate(QString name);
            // decides whether to show parameters grid or not
            void _showParameterGrid(QString selectedValue);
            //function for saving the template
            void _handleSaveButtonClicked();
            bool updateTemplate();
            void templateSelected();
            void _handleApplyButtonClicked();

            // read all profile templates from appdata
            void _readAllDitchProfiles();

            void _deleteProfile(QString selectedValue);

            void _okSlotForDeletingProfile();

            void _browseButtonClicked();

            void _guiMessageSlot(QString message);

            void _saveNewProfile(QVariant pointsList);

            void _applySelectedProfileToTerrain(QVariant pointProfileMap );
            void _loadTemplatesOfSelectedProfile(QString teplateName, bool refreshTemplateList);
            void _saveTemplate(QString templateName, QVariant pointList);
            void _addNewTemplate(QString);
            void _deleteTemplate(QString); 

    signals:
            void sendFenceFolderCreated(bool);

    protected:

        virtual ~LineLayerPropertiesGUI();

        virtual void _loadAndSubscribeSlots();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message); 

        void _setBusy();

    protected:

        CORE::RefPtr<CORE::ISelectable> _currentSelection;
        CORE::RefPtr<CORE::ISelectable> _tempCurrentSelection;
        CORE::RefPtr<CORE::IObject> _folderObject;
        //CORE::RefPtr<CORE::ISelectable> _currentCreatedModel;

        bool _resetStyleOnClose;
        bool _shouldClose;

        //! Line style
        QColor          _originalColor;
        double          _originalLineWidth;
        unsigned short  _originalLineStipple;
        unsigned int    _originalLineStippleFactor;

        //! Fence style
        bool            _originalBFLFlip;

        //! IB style
        bool            _originalIBFlip;
        double          _originalIBWidth;
        bool            _originalIBAutoScale;
        QColor          _originalSingleLineIBColor;
        QColor          _originalInnerColor;
        QColor          _originalOuterColor;

        std::string     _originalGateLayerPath;
        std::string        _originalUnfencedLayerPath;

        bool            _originalOneLineIB;

        QString         _originalDrawTechnique;

        const std::string _connectedFeatureTemplateStoragePath;
        //List of String to Store tempalte  Name
        std::vector<std::string> _templateList;
        CORE::RefPtr<GIS::ConnectedFeatureData>  _connectedFeatureDataStack;

        std::vector<std::string> _profileList;

        std::string _lastPath;


        QList<QVariant> _pointsList; 
        QList<QObject*> _profileTemplateList;
    };

} // namespace SMCQt
