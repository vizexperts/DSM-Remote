#ifndef SMCQT_TIMELINEGUI_H_
#define SMCQT_TIMELINEGUI_H_

/*****************************************************************************
*
* File             : TimelineGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : TimelineGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <QDateTime>

#include <SMCUI/IAnimationUIHandler.h>
#include <SMCUI/ITourUIHandler.h>

namespace SMCQt
{
    class TimelineGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        TimelineGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void setActive(bool value);

            void setStartEndDateTime(QDateTime qStartDateTime ,QDateTime qEndDateTime);

            // slots for timeline
            void slower();
            void play();
            void pause();
            void stop();
            void faster();

            void timeSliderValueChanged(int value);

    protected:

        virtual ~TimelineGUI();

        virtual void _loadAndSubscribeSlots();

        //! populate timeline with Start/End/Current Date and time
        void _populateTimeline();

    private:

        CORE::RefPtr<SMCUI::IAnimationUIHandler> _animationUIHandler;

    };

} // namespace SMCQt

#endif  // SMCQT_TIMELINEGUI_H_
