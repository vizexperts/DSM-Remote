/*****************************************************************************
*
* File             : FloodingGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : FloodingGUI class declaration
* Author           : dharmendra
* Author email     : dharmendra@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2015-2016, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>

#include <SMCUI/IFloodingUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
#include <SMCUI/IPointUIHandler2.h>
namespace SMCQt
{
    // Creates a new layer
    class FloodingGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        QObject* FloodingObject;
    public:

        static const std::string FloodingAnalysisPopup;

        FloodingGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void  _setProgressValue(int);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void _handlePBMarkBreachAtClicked(bool pressed);
                void _handlePBStartClicked();
                void _handlePBStopClicked();
                void _handlePBRunInBackgroundClicked();
                void  _handleProgressValueChanged(int);
                void _handlePBAreaClicked(bool pressed);
                void _reset();
                //! enable/disable point handler 
                void _setPointHandlerEnabled(bool enable);

    protected:

        virtual ~FloodingGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

    protected:

        //! Height Above UIHandler
        CORE::RefPtr<SMCUI::IFloodingUIHandler> _uiHandler;

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;
        //! Point UI Handler
        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointHandler;
        
        //! point object for viewer position
        CORE::RefPtr<CORE::IObject> _viewerPoint;
        
    };

} // namespace SMCQt
