#pragma once

/*****************************************************************************
*
* File             : AddGPSGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : AddGPSGUI class declaration
* Author           : Gaurav Garg
* Author email     : gaurav@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCUI/IAddGPSUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    // Adds a GPS source
    class AddGPSGUI :  public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:
        static const std::string AddGps;
        AddGPSGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set active function */
            void setActive(bool active);
            //@}

            private slots:

                void _handleBrowseButtonClicked();
                void connectPopupLoader(QString);
                void _handleCancelButtonClicked();            
                void _handleOkButtonClicked();
                void _reset();

    protected:

        virtual ~AddGPSGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! Configures and loads NMEA source
        void _configureNMEATCPSource();

        //! Configures and loads a file source
        void _configureLogfile();

        //! UIHandler for loading digital compass data
        CORE::RefPtr<SMCUI::IAddGPSUIHandler> _agpsUIHandler;

        //Qml Object
        QObject* _gpsObject;
    };

} // namespace SMCQt

