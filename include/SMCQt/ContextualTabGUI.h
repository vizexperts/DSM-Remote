#pragma once

/*****************************************************************************
*
* File             : ContextualTabGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : Contextual tab class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/IContextualTabGUI.h>
#include <VizUI/ISelectionUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/CreatePointGUI.h>
#include <SMCQt/CreateLineGUI.h>
#include <SMCQt/CreatePolygonGUI.h>
#include <SMCQt/CreateLandmarkGUI.h>
#include <SMCQt/ResourceManagerGUI.h>
#include <SMCQt/MilitaryPathAnimationGUI.h>
#include <SMCQt/VisibilityControllerGUI.h>

namespace SMCQt
{
    // XXX - exported only for testing purpose
    class SMCQT_DLL_EXPORT ContextualTabGUI : public SMCQt::DeclarativeFileGUI ,public SMCQt::IContextualTabGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

        Q_OBJECT

    public:

        static const std::string pointHyperlinkActivePropertyName; 
        static const std::string pointAttributeActivePropertyName; 
        static const std::string TOOLBOXOBJECTNAME;

        static const std::string AddFeatureToolboxButton;
        static const std::string RasterReorderToolboxButton;
        static const std::string ExportLayerToolboxButton;
        static const std::string ExportToWebserverToolboxButton;
        static const std::string QuerySelectedDataToolboxButton;
        static const std::string NewRasterWindowWebserverToolboxButton;
        static const std::string OpacitySliderToolboxButton;
        static const std::string OverviewCreationToolboxButton;
        static const std::string GeneralInfoToolboxButton;
        static const std::string CreateTMSToolboxButton;
        static const std::string PostGISToolboxButton;
        static const std::string RouteCalculationToolboxButton;
        static const std::string ExportWebLayerToolboxButton;
        static const std::string PreviewTrackToolboxButton;
        static const std::string VectorStyleToolboxButton;
        static const std::string VectorAttributeToolboxButton;
        static const std::string HistogramLegendsToolboxButton;
        static const std::string ConnectedFeaturesToolboxButton;
        static const std::string ConvertTo3DToolboxButton;
        static const std::string FlyAroundToolboxButton;
        static const std::string ApplyDicthToTerrainToolboxButton;
        static const std::string colorLegendToolboxButton;


        ContextualTabGUI();

        //@{
        /** Called when GUI added/removed to GUI manager */
        void onAddedToGUIManager();
        //@}

        //! activates the tab by typename
        void activateByObjectTypeName(std::string name);

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        Q_INVOKABLE void closeContextualTab();

signals:
        void activateGuiByTypeName(QString type);
        public slots:

            // ! Updates position of contextual menu on screen
            void updatePosition();
            void highlightItemInMultipleSelection(int);
            void selectItemInMultipleSelection(int);
            void openContextualMenuForItemInMultipleSelection(int);
            void closeMultipleSelectionItemMenu();
            void openContextualMenuFromButton();


    protected:

        virtual ~ContextualTabGUI();

        //! Open contextual menu for the current selection
        void _selectTab();

        //! Select the object and enable/disable GUI
        void _selectObject();

        void _deactivateUIHandler();

        void _handleMultipleSelectionMessage();

        void _openContextualMenuForSelection();

        void _enableToolboxButton(const std::string& buttonName, bool enabled);
        void _setlayerTypeInToolbox(const std::string& layerType);

        std::string _identifyObject(CORE::RefPtr<CORE::IObject>);

        CORE::RefPtr<VizUI::ISelectionUIHandler> _selectionUIHandler;
        CORE::RefPtr<CORE::ISelectionComponent>_selectionComponent;

        //! type of contextual tab
        QString _tabname;

        //! type of object currently selected
        std::string _name;

        //! Current LongLatAlt of contextualGUI
        osg::Vec3 _position;

        //! flag:: Show ContextualGUI 
        bool _showGUI;

        //! Curren screenPos of contextualGUI(x, y, 0)
        osg::Vec3 _screenPos;

        // timer to fire event to update position in gui
        QTimer _timer;

        //! mouse clicked left or right
        bool _mouseClickLeft;

        //! Flag which shows whether contextual menue is opened by clicking botton or not
        bool _isOpenContextualFromButton;
               
    };

} // namespace SMCQt