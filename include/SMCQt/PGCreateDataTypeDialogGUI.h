#pragma once

#include <VizQt/GUI.h>
#include <VizDataUI/IDatabaseUIHandler.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/ISettingComponent.h>

namespace SMCQt
{

    class PGCreateDataTypeDialogGUI : public SMCQt::DeclarativeFileGUI
    {
        Q_OBJECT
       

        public:
            
            PGCreateDataTypeDialogGUI ();
         
            void _loadAndSubscribeSlots();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            void _displaySchema();

            ~PGCreateDataTypeDialogGUI();

            QStringList dataTypeList;

            bool makeConnectionWithDatabase();

            bool _checkConnectionWithDatabase();

            VizDataUI::IDatabaseUIHandler* _getDatabaseUIHandler();

            std::map<std::string, std::string> _getDatabaseCredential();

        private slots:

            void _handleCreateButtonClicked();

            void popupLoaded(QString );
        
        protected:
            
            vizCore::RefPtr<VizDataUI::IDatabaseUIHandler> _dbUIHandler;

            bool _rasterRadioButton;

            bool _vectorRadioButton;

            bool _elevationRadioButton;

            CORE::RefPtr<VizQt::IQtGUIManager> _manager; 

            bool _dbSelect;

            std::map<std::string, std::string> _dbOptions;

            QString databaseName;

            CORE::RefPtr<ELEMENTS::ISettingComponent> _globalSettingComponent;

            bool _presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap, const std::string& keyword);

            
    };
}
