#pragma once

/*****************************************************************************
*
* File             : RoadNetworkGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : RoadNetworkGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <GIS/IFeatureLayer.h>
#include <Core/IPoint.h>
#include <Elements/IMultiPoint.h>
#include <GISCompute/IShortestPathCalculationVisitor.h>
#include<Terrain/IModelObject.h>

namespace SMCQt
{
    class RoadNetworkGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        enum RoadNetworkGUIState
        {
            NONE,
            MARK_START,
            MARK_END,
            RESULT
        };

        RoadNetworkGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            // initiate GUI connection
            void connectPopup(QString type);

            // mark start point
            void markStart(bool value);

            // mark end point
            void markEnd(bool value);

            // set database instance
            void setDatabase(QString databaseInstance);

            // set style to be applied on result layer
            void setStyle(QString jsonFile);

            // compute ( calls shortest path calculation visitor )
            void compute();

            // close connection
            void popupClosed();

    protected:
        virtual ~RoadNetworkGUI();

        virtual void _loadAndSubscribeSlots();

        void _handleMousePressed(int buttonMask, osg::Vec3d longLatAlt);    

        // populates all the postgres instances available
        void _populateDatabaseList();

        // populates all the styles available
        void _populateStyleList();

        // reads postgres databases name and pushes into list to be shown in combobox
        void _readAllDatabaseInstanceName();

        // reads style names and pushes into listto be shown in combobox
        void _readAllStyle();

        // fetch selected database details and send it to visitor
        void _getDatabaseParameters(std::string pgresFileName);

        // creates point
        CORE::RefPtr<CORE::IPoint> _createPoint();

        // selected line layer
        CORE::RefPtr<TERRAIN::IModelObject> _featureLayer;

        // shortest path visitor
        CORE::RefPtr<GISCOMPUTE::IShortestPathCalculationVisitor> _visitor;

        // enum for GUI state 
        RoadNetworkGUIState _guiState;

        // shows list of all pg databases
        std::vector<std::string> _databaseInstanceList;

        // shows list of all styles
        std::vector<std::string> _styleList;

        // saves database parameters
        std::map <std::string, std::string> _dbOptions;

        // start point
        CORE::RefPtr<CORE::IPoint> _startPoint;

        // end point
        CORE::RefPtr<CORE::IPoint> _endPoint;
        
        // selected database 
        std::string _databaseName;
        
        // path of selected json styling
        std::string _styleJsonPath;
        
        // styles folder path 
        std::string _styleFolderPath;
          
        // output layer name
        std::string _outputLayerName;

        // random name used for saving shp file in visitor
        std::string _uniqueName;

        
    };
}