#pragma once

/*****************************************************************************
 *
 * File             : FeatureExportGUI.h
 * Version          : 1.0
 * Module           : Qt
 * Description      : FeatureExportGUI class declaration
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : Qt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>

#include <SMCUI/IFeatureExportUIHandler.h>

#include <VizQt/LayoutFileGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    class FeatureExportGUI :  public DeclarativeFileGUI
    {
        DECLARE_META_BASE;

        Q_OBJECT

        public:
        static const std::string ExportWindow;
        static const std::string ExportGeoWindow;
        static const std::string GeorbISServerJsonLocation;
            enum ExportMode
            {
                EXPORT_MODE_NONE = 0,
                EXPORT_MODE_FEATURE = 1,
                EXPORT_MODE_ELEVATION = 2,
                EXPORT_MODE_RASTER = 3,
                EXPORT_MODE_TIN = 4,
                EXPORT_MODE_GEOVRML = 5,
                //RFE1
                EXPORT_MODE_OVERLAY = 6,
                EXPORT_ROUTE_OVERLAY = 7,
                EXPORT_TRAJECTORY_OVERLAY = 8,
                EXPORT_FORMATION_OVERLAY = 9,
                EXPORT_MODE_POINT_FEATURE = 10
            };

        public:

            FeatureExportGUI();
            
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);


        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            void connectPopupLoader(QString);
            void setMode(int mode);
            //@}

        private slots:

            //! Updates the area editbox regularly
            void _handleExportGeoButtonClicked();
            void _handleExportButtonClicked();
            void _handleBrowseButtonClicked();
            /**
            * call when publishingServerChange called from Qml and store GerobISServer Name in _georbIsServerUrl
            * param name a QString argument denoting GerobISServer Name
            *
            */
            void _readGeorbISServerUrl(QString georbISSeverName);
        protected:

            virtual ~FeatureExportGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            //! get the feature export UI handler
            void _loadFeatureExportUIHandler();
            /**
            * Fill Georbis Server List (read from GeorbISServer jsong and Fill in Qml)
            * 
            *|return void
            */
            void _fillGeorbISServerList();
            void _executePublishLayer();


        protected:

            //! Feature Export UI Handler
            CORE::RefPtr<SMCUI::IFeatureExportUIHandler> _featureExportUIHandler;

            ExportMode _mode;
            QObject* _exportObject;
            QString fileName;
            // _serverIp parameter as required by _featureExportUIHandler
            std::string  _serverIp;
            std::string _portNum;

    };

} // namespace GUI


