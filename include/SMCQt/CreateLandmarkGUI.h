#pragma once

/*****************************************************************************
*
* File             : CreateLandmarkGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateLandmarkGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQT interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/ILandmarkUIHandler.h>
#include <Core/IPoint.h>
#include <VizUI/IKeyboard.h>
#include <VizUI/Keyboard.h>
#include <VizUI/Mouse.h>
#include <SMCUI/IDraggerUIHandler.h>
#include <Elements/IModelFeatureObject.h>
#include <GIS/IFeature.h>

#include <Curl/EasyCurl.h>
#include <Elements/ISettingComponent.h>
#include <SMCQt/QMLDirectoryTreeModel.h>
#include <QDir>
#include <map>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateLandmarkGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

            //Creating for taking the Reference of the uploaded models
        struct LandmarkUploaded
        {
            std::string name;
            std::string type;
            std::string modelFilePath;
            std::string iconFilePath;
            std::string category;
            std::string uid;
            bool selected;
        };

        static std::string LANDMARK_APPDATA_DIR;
        static const std::string LandmarkContextualObjectName;
        static const std::string MouseButtonClickedPropertyName;

        //Slider Extents QML Side for + and - directions
        static const std::string XPlaneMinExtent;
        static const std::string XPlaneMaxExtent;
        static const std::string XPlaneMinExtentOpposite;
        static const std::string XPlaneMaxExtentOpposite;
        static const std::string YPlaneMinExtent;
        static const std::string YPlaneMaxExtent;
        static const std::string YPlaneMinExtentOpposite;
        static const std::string YPlaneMaxExtentOpposite;
        static const std::string ZPlaneMinExtent;
        static const std::string ZPlaneMaxExtent;
        static const std::string ZPlaneMinExtentOpposite;
        static const std::string ZPlaneMaxExtentOpposite;

        //Slider value QML Side for + and - directions
        static const std::string XPlaneDistance;
        static const std::string YPlaneDistance;
        static const std::string ZPlaneDistance;
        static const std::string XPlaneDistanceOpposite;
        static const std::string YPlaneDistanceOpposite;
        static const std::string ZPlaneDistanceOpposite;

        //SpinBox Extents QML Side for + and - directions
        static const std::string XPlaneSpinBoxMin;
        static const std::string XPlaneSpinBoxMax;
        static const std::string XPlaneSpinBoxMinOpposite;
        static const std::string XPlaneSpinBoxMaxOpposite;
        static const std::string YPlaneSpinBoxMin;
        static const std::string YPlaneSpinBoxMax;
        static const std::string YPlaneSpinBoxMinOpposite;
        static const std::string YPlaneSpinBoxMaxOpposite;
        static const std::string ZPlaneSpinBoxMin;
        static const std::string ZPlaneSpinBoxMax;
        static const std::string ZPlaneSpinBoxMinOpposite;
        static const std::string ZPlaneSpinBoxMaxOpposite;

        //SpinBox Value QML Side for + and - directions
        static const std::string XPlaneSpinBoxDistance;
        static const std::string YPlaneSpinBoxDistance;
        static const std::string ZPlaneSpinBoxDistance;
        static const std::string XPlaneSpinBoxDistanceOpposite;
        static const std::string YPlaneSpinBoxDistanceOpposite;
        static const std::string ZPlaneSpinBoxDistanceOpposite;

        static const std::string OpacitySliderValue;
        static const std::string OpacitySpinBoxValue;

        // server suffix 
        static const std::string SERVER_URL;

        //url suffix to download model from server
        static const std::string MODEL_DOWNLOAD_URL;
        
        // georbis Server list Directory
        static const std::string GeorbisServerlistDir;

    public:

        CreateLandmarkGUI();

        void initializeList();

        void initializePlacedLandmarksList();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void _updateDragger();
        void showLandmarkAttributes();

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            void modelSelected(int index);
            void setMode(int mode);
            void setRepresentationMode(int mode);
            void search(QString qstr);
            void connectSMPMenu(QString type);
            void menuDestroyed();
            void deselectModel(QString model);

            // slots for contextual menu
            void changeName(QString newName);
            void handleLatLongAltScaleChanged(QString);
            void snapToGround();
            void reorient();
            void deleteLandmark();
            void setModel(int index);
            void openModelSelectionPane(bool value);

            //Slot to populate CrossSection UI features like x,y,z planes with plane distance slider 
            void toggleCrossSection();

            // Toggle clipping
            void toggleClipping();

            // Close Clipping from QML if clipping is still active
            void closeClippingFromGUI();

            // Limit value argument to 2 decimal places
            double getPrecision(double value);

          //  void _setActiveClippingPlanes();
            // set clipping in xplane
            void setXPlane(double xplane);
            void setXPlaneOpposite(double xplaneOpposite);
            void toggleXPlane1(bool xClip1);
            void toggleXPlane2(bool xClip2);

            // set clipping in yplane
            void setYPlane(double yplane);
            void setYPlaneOpposite(double yplaneOpposite);
            void toggleYPlane1(bool yClip1);
            void toggleYPlane2(bool yClip2);

            // set clipping in zplane
            void setZPlane(double zplane);
            void setZPlaneOpposite(double zplaneOpposite);
            void toggleZPlane1(bool zClip1);
            void toggleZPlane2(bool zClip2);

            // set opacity for x,y,z plane quads
            void setOpacity(double opacity);

            // enabling/disabling the boundary of plane quads
            void toggleQuadsBoundary(bool remove);

            void togglePersistanceCheck(bool check); 

            /*!
            \fn void setEnableDragger(bool value)
            \brief Enables or Disable Manipulator on current selected landmark
            \param value enable or disable manipulator on current selected landmark
            */
            void setEnableDragger(bool value);

            void connectContextualMenu();
            void disconnectContextualMenu();

            void popupLoaded(QString type);
            void addNewLandmark(QString);

            void browseButtonClicked();

            void browseLandmarkButtonClicked();
            void onClickedOk(QString directory);

            void addModelsToSelectedLayer(bool value);

            void _resetForDefaultHandling();
            void  doReloadFeatureQml();
            void  doReloadLandmarksQml();

            void handleClippingGUI(); 

signals:
            void landmarkCreated();

    private: 

            //Checks whether the current selected model is valid or not
            bool _isModelValid();

            // set models extents in LandmarkContextual UI by taking boundingbox from iclipping interface
            void _setPlaneExtents();

            // set the bounding box of 3d model
            void _setBoundingBox();


    protected:

        virtual ~CreateLandmarkGUI();

        virtual void _loadAndSubscribeSlots();

        void _populateContextualMenu();

         void _writeSettings();

        void _createMetadataRecord();

        void _addNewLandMark(const std::string& modelName, const std::string& modelpath,
            const std::string& IconPath, const std::string& category);

        void _copyModel(const std::string& modelPath);

        void _uploadFileToServer(const std::string& fileToUpload, const std::string& fileName);

        void _addIconToGUIList(const std::string& type, const std::string& name, const std::string& iconPath,
            std::string& uid);

        bool _getConfigFileToUpload(const std::string& configPath, std::string& configLinkOnServer);

        void _addModelAndIconToGUIList(const std::string& type, const std::string& modelPath,
            const std::string& iconPath, const std::string& category, const std::string& uid);

        void _convertSpacesIntoHTMLFormat(std::string& fileName);

        bool _populateDir(QDir dir, QMLDirectoryTreeModelItem* item = NULL, bool fillCateegories = false);

        // parse models name and uid from server
        void _parseResponse(std::string serverName , std::string response);

        // write function for server response
        static size_t write_to_string(void *ptr, size_t size, size_t count, void *stream); 


        enum LandmarkMode
        {
            MODE_CREATE_LANDMARK = 0,
            MODE_EDIT_LANDMARK = 1,
            MODE_NONE = 2,
            MODE_CONTEXTUALMENU =3
        } _mode;

        //! LandmarkUIHandler
        CORE::RefPtr<SMCUI::ILandmarkUIHandler> _landmarkUIHandler;

        //! DraggerUIHandler
        CORE::RefPtr<SMCUI::IDraggerUIHandler> _draggerUIHandler2;

        QFileInfo _currentSelectedFile;

        bool _addToDefault;

        //!
        CORE::RefPtr<CORE::IFeatureLayer> _selectedFeatureLayer;

        //This will contain the Last path Browsed in QT Browser
        std::string _lastPath;

        std::string _lastPath1;

        //Uploaded models Map
        std::map<std::string, LandmarkUploaded>  _uploadedLandmarkTypeMap;

        std::string _webServerAddress;
        std::string _webserverUsername;
        std::string _webserverPassword;
        std::string _resourceLocationOnServer;

        bool _reloadLandmarkList;

        CORE::RefPtr<ELEMENTS::ISettingComponent> _globalSettingComponent;

        QMLDirectoryTreeModel*      _qmlDirectoryTreeModel;
        QMLDirectoryTreeModelItem*  _qmlDirectoryTreeModelRootItem;

        // create tree for models list from server
        QMLDirectoryTreeModelItem* _serverModelsItem;
        QList<QObject*>             _qmlCategoriesList;

        //! 
        bool _modelSelectionPaneOpenedFromContextualMenu;
        CORE::RefPtr<GIS::IFeature> _selectedFeature;

        CORE::RefPtr<ELEMENTS::IModelFeatureObject> _model;

        //! to set whether the clipping is on/off
        bool _crossSection;

        //! to set whether the clipping is persistant or not 
        bool _crossSectionPersistant;

        //! to set the xplane clipping
        bool _xClip1;

        //! to set the xplane clipping in opposite direction
        bool _xClip2;

        //! to set the yplane clipping
        bool _yClip1;

        //! to set the yplane clipping in opposite direction
        bool _yClip2;

        //! to set the zplane clipping
        bool _zClip1;

        //! to set the zplane clipping in opposite direction
        bool _zClip2;

        //! to set the xplane distance for clipping
        double _xPlane1;

        //! to set the xplane distance for clipping in opposite direction
        double _xPlane2;

        //! to set the yplane distance for clipping
        double _yPlane1;

         //! to set the yplane distance for clipping in opposite direction
        double _yPlane2;

        //! to set the zplane distance for clipping
        double _zPlane1;

         //! to set the zplane distance for clipping in opposite direction
        double _zPlane2;

        //! to store the bounding box of the 3d model
        osg::BoundingBox _bb;

        //! to set opacity of quad
        double _opacity;

        // to store whether remove boundary is checked or not in GUI
        bool _removeBoundary;

        // to store which mouse button is clicked
        bool _mouseClickRight;

        // set the attribute list for landmark symbol
        QList<QObject*> _attributeList;

        // inMemory list for saving server names saved in appdata
        std::vector<std::string> _georbISServerList;


        // list that tracks name of server with server uid and name map
        typedef std::map<std::string ,std::map<std::string, std::string>> ServerUIDNameMap;
        ServerUIDNameMap _uidNameMap;

        //selected model uid name path and server
        std::string _selectedServer, _selectedName, _selectedUID, _selectedModelRelativeLocation;

    };
}
