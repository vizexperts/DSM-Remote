#ifndef _SMCQT_CREATETMS_GUI_H
#define _SMCQT_CREATETMS_GUI_H

/*****************************************************************************
*
* File             : CreateTMSGUI.h
* Version          : 1
* Module           : TKQt
* Description      : CreateTMSGUI Class 
* Author           : Naresh Reddy
* Author email     : author email id
* Reference        : Part of TKP Application
* Inspected by     : code inspector's name
* Inspection date  : YYYY-MM-DD
*
*****************************************************************************
* Copyright (c) 2010-2011,VizExperts India Pvt. Ltd                                        
*****************************************************************************
*
* Revision Log (latest on top):
* Revision     Version     Date(YYYY-MM-DD)    Time(XXXX hrs)  Name
* 
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>


#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IExportUIHandler.h>
#include <SMCUI/IAddContentUIHandler.h>

#include <SMCUI/IAreaUIHandler.h>
#include <QtCore/QTimer>
#include <SMCUI/ISurfaceAreaCalcUIHandler.h>
#include <Core/ISelectionComponent.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateTMSGUI :  public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED

        Q_OBJECT

    public:
        static const std::string CreateTMSPopup;

        CreateTMSGUI();

        void initialize();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

    private:

        //! Last Path History
        std::string _lastPath;
        
        void reset();

        public slots:

            void setActive(bool value);


        private slots:
            //! This slot is executed when browse button is pressed for selecting durectory. 
            void browseButtonPressedSlot();

            void popupLoaded(QString type);
            //This slot is executed when we press Ok button
            void _handleOkButtonClicked();
            
            //This slot is executed when area is marked.
            void markArea(bool value);

    protected:

        virtual ~CreateTMSGUI();

        void _loadAndSubscribeSlots();

        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;

        void _loadAddContentUIHandler();

        QString _directoryPath;

        //! UIHandler for loading  data
        CORE::RefPtr<SMCUI::IExportUIHandler> _exportUIHandler;

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

        bool _bWebRasterData;
    };
}
#endif // _SMCQT_CREATETMS_GUI_H
