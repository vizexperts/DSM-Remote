#pragma once

/*****************************************************************************
*
* File             : StyleTemplateGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : StyleTemplateGUI class declaration
* Author           : Gurpreet Singh
* Author email     : gurpreet@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/ElementListTreeModel.h>
#include <SMCQt/QMLDirectoryTreeModel.h>
#include <VizUI/ISelectionUIHandler.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT StyleTemplateGUI: public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:
        static const std::string MODELLAYERSTYLEPOPUP;
        static const std::string ITEMRELOADPOPUP;
        static const std::string TEMPLATELISTPOPUP;

        static std::string _currentSelectedJsonFile;

        public slots :

        //! Populate style template list 
        void populateStyleTemplateList();

        //! Populate Model and icon files list
        void populateModelList();

        void popupClosed();

        void jsonFileSelected(int index);
        void modelFileSelected(int index);
        bool populateEnumList(QString name, QString parentName, QString value);
        void setJsonEditorOutput();
        void handleSaveAsButtonClicked(QString fileName); 
        void setDefaultModelObject();
        void updateModelObject();

    protected: 
        
        //! used for populating style template list
        bool _populateDir(QDir dir, QMLDirectoryTreeModelItem* item = NULL);
        
        QMLDirectoryTreeModel*      _qmlDirectoryStyleTreeModel;
        QMLDirectoryTreeModelItem*  _qmlDirectoryStyleTreeModelRootItem;

        QMLDirectoryTreeModel*      _qmlDirectoryFileTreeModel;
        QMLDirectoryTreeModelItem*  _qmlDirectoryFileTreeModelRootItem;

        //! Keeps a list of all context property names set by this UI so that they can be set to NULL when menu is closed
        std::vector<std::string> _contextPropertiesSet;

        //! used for populating model and icon file names,.
        bool _populateModelDir(QDir dir, QMLDirectoryTreeModelItem* item = NULL); 
        
		//! selection component
        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

        //! current selection
        CORE::RefPtr<CORE::ISelectable> _currentSelection;
    }; 
}

