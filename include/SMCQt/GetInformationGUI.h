#pragma once

/*****************************************************************************
*
* File             : GetInformationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : GetInformationGUI class declaration
* Author           : Mohan singh
* Author email     : mohan@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <App/AccessElementUtils.h>

#include <Core/IWorldMaintainer.h>

#include <Terrain/IRasterObject.h>
#include <Core/ISelectionComponent.h>

#include <gdal_priv.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT GetInformationGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        GetInformationGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void setActive(bool active);

            private slots:

                void connectPopupLoader(QString type);

    protected:

        virtual ~GetInformationGUI();

        virtual void _loadAndSubscribeSlots();

        void _addAttributeToTable(const QString& attrName, const QString& attrValue);

        void _clearAttributeTable();

        void _readAttributesFromFile(std::string &url);

        void _getMetaData(GDALDatasetH dataset);

        void _getImageStructureMetadata(GDALDatasetH dataset);

        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

        QList<QObject *> _twAttrList;

    };

}    // namespace SMCQt

