#ifndef SMCQT_TOURGUI_H_
#define SMCQT_TOURGUI_H_

/*****************************************************************************
*
* File             : TourGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : TourGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <QDateTime>

#include <SMCUI/IAnimationUIHandler.h>
#include <SMCUI/ITourUIHandler.h>
#include <QtMultimedia/QAudioRecorder>
#include <QtMultimedia/QMediaPlayer>

namespace SMCQt
{
    class TourGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        // const string to store audiofile with extension
        static const std::string NarrationFile;
        enum MenuType
        {
            TOUR_MENU,
            SEQUENCE_MENU
        };

        // enum for storing current recording state of mic
        enum RecordingState{
            RECORDING,
            RECORDING_PAUSED,
            PLAYING_AUDIO,
            PLAYING_PAUSED,
            IDLE
        };

        TourGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

        void setOpenTourInExternalViewer(bool value);

        bool getOpenTourInExternalViewer() const;

        public slots:


            void connectTourMenu();

            // slots for tour recording
            void addNewTour();
            void playTour(QString tourName);
            void pauseTour();
            void stopTour();
            void decreaseSpeedFactor();
            void increaseSpeedFactor();
            void connectRecorder();
            void tourSliderValueChanged(int);

            void confirmTourDelete(QString tourName);
            void deleteTourOkButtonPressed();
            void deleteTourCancelButtonPressed(){}

            void populateTourList();

            void startRecording();
            void pauseRecording();
            void stopRecording();
            void saveTour(bool overwrite=false);
            void recorderClosed();
            void changeRecorderType(int);
            void editCurrentTourSpeed();
            void changeTourName( QString oldName, QString newName );
            void markKeyframe();

            // slot to enable/disable mic button in QML list view
            void enableMicInQMLModel(QString tourName);

            // called when mic button is clicked in UI
            void micClicked(QString tourName);

            // slot to delete audio file for given tour
            void deleteAudio(QString tourName);

            // utility function to delete the audio file
            void _okSlotForDeleting();

            // utility function to return back without deleting audio file
            void _cancelSlotForDeleting();

            //! mutes Narration
            void muteNarration(bool checked);

            //! tour checkpoint manipulation slots

            void connectTourSpeedEditor();
            void createCheckPoint(int timeStamp);
            void modifyCheckPoint(int oldTime,int newTime, double newSpeed);
            void deleteCheckPoint(int timeStamp);
            void resetSpeedProfile();
            void saveSpeedProfile();
            void setCurrentCheckpointProperties(int);
            void previewAtCheckPoint(int time);

            //Tour Sequence functions
            void connectPopupLoader(QString type);
            void addToSequenceList(int index);
            void removeFromSequence(int index);
            void moveInSequence(int from, int index);
            void saveSequence(QString name);
            void openBottomLoader();
            //void deleteSequence(QString name);
            //void deleteSequenceOkButtonPressed();
            //void deleteSequenceCancelButtonPressed();

            //! 0: Tour Menu
            //! 1: Sequence Menu
            void menuTypeChanged(int menuType);


            //Will close the recorder without bothering whether
            //recording is saved or not
            void closeRecorderSilently();

            //
            void openSaver();

            //!
            void overwriteSave();

            void _launchOSGView(std::string tourNameToPlay);

    protected:

        void _updateTourListGUI();

        virtual ~TourGUI();

        virtual void _loadAndSubscribeSlots();

        //! set the initial state of gui and weather mission time has been initialised or not
        void _populateGUI();

        //! to populate checkpoints at the time of opening a tour
        void _populateCheckPointList();
        void _resetCheckPointGUI();

        // internal function to check whether audio for given sequence is present or not 
        bool _isAudioPresent(std::string);

        // internal function to start playing audio
        void _playAudio(std::string);

        // internal function to get number of tours in a sequence
        void _getNumberOfToursInSequence(int& size, const std::string& sequenceName);

        // sets volume of microPhone
        void _setVolume(int volume);
    private:

        CORE::RefPtr<SMCUI::IAnimationUIHandler> _animationUIHandler;

        //Tour UIHandler for tour manipulation
        CORE::RefPtr<SMCUI::ITourUIHandler>  _tourUIHandler;

        //! variable to hold the status of save
        int _isCurrentRecordedSaved;

        QString _tourToDelete;

        // current list of checkpoints
        QList<QVariant> _checkPointList;

        bool _openInExternalViewer;

        QStringList _remainingTourList;
        QStringList _currentTourSequence;

        MenuType _menuType = TOUR_MENU;

        QAudioRecorder* _audioRecorder;
        QMediaPlayer* _audioPlayer;

        RecordingState _recordingState;
        std::string _audioToDelete;
        std::string _enableMicForTour;
        int _tourToPlay;

    };

} // namespace SMCQt

#endif  // SMCQT_TOURGUI_H_
