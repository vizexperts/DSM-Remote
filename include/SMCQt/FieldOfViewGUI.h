#pragma once
/*****************************************************************************
*
* File             : FieldOfViewGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : FieldOfViewGUI class declaration
* Author           : Priti Priya
* Author email     : priti@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/ISettingComponent.h>
#include <View/ICameraComponent.h>
#include <SMCUI/IAreaUIHandler.h>

namespace SMCQt
{
    class FieldOfViewGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:
        FieldOfViewGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:
            void handleChangeFov(double fovValue);
            void handleVerticalScale(double scaleValue);
            void handleChangeAor(bool value);
            void handleChangeOrtho(bool value);
            void handleMarkAor(bool value);
            void handleChangeSelectionState(bool value);
            void handleDisplayGrid(bool value);
            //! slot for on/off  IndianGrid on Globe
            void handleDisplayIndianGrid(bool value);

            void connectFov();
            void saveFovSetting();

    protected:
        virtual void _loadAndSubscribeSlots();
        virtual ~FieldOfViewGUI();
        void _writeSettings();

        void _getAORLayer(bool create = true);

        void _initializeAORFeature();
        void _setAorToCameraComponent();
        void _configureCreatedArea();

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

        //! drawed polygon
        CORE::RefPtr<CORE::IPolygon> _area;

        //! AOR feature layer
        CORE::RefPtr<CORE::IFeatureLayer> _aorLayer;

        CORE::RefPtr<ELEMENTS::ISettingComponent> _settingComponent;
        CORE::RefPtr<VIEW::ICameraComponent> _cameraComponent;
        QObject* _fovObject;

    };

} // namespace SMCQt
