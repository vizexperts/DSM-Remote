/*****************************************************************************
*
* File             : AddBookmarksGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : AddBookmarksGUI class declaration
* Author           : Akshay
* Author email     : akshay@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2015-2016, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <VizUI/UIHandler.h>
#include <VizUI/ICameraUIHandler.h>
#include <SMCQt/QMLDirectoryTreeModel.h>

namespace SMCQt
{
    // Creates a new layer
    class AddBookmarksGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        QObject* BookmarksGUIObject;
    public:

        // structure to store Bookmark related values under 1 entity. 
        // This way we can directly give these values to cameraUIHandler
        struct Bookmark {
            osg::Vec3d longLatAlt;
            double range;
            double heading;
            double pitch;
        };

        // object names for qml components
        static const std::string BookmarksMenuPane;
        static const std::string AddOrRenameBookmarksPopup;
        
        AddBookmarksGUI();

signals:

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // opens addBookmark popup and saves camera positions in _currentBookmark variable
                void _addBookmark();

                // saves in json file under bookmarks in project folder
                void _writeBookmark(QString name);

                // updates selected bookmark name and file path
                void _bookMarkClicked(int);

                // fetches the selected bookmark and sets camera position to this bookmark
                void _bookMarkDoubleClicked(int);

                //resets the class members
                void _reset();

                // opens renameBookmark popup 
                void _editBookmark();

                // renames the json file name with given name
                void _editExistingBookmark(QString newName);

                //deletes bookmark
                void _deleteBookmark();

                // util function for delete
                void _okSlotForDeleting();

                // util function for delete
                void _cancelSlotForDeleting();

    protected:

        virtual ~AddBookmarksGUI();

        // for populating bookmarks in tree
        void _populateBookmarksList();

        // utils function for populateBookmarkList
        bool _populateDir(QDir dir, QMLDirectoryTreeModelItem* item = NULL);

        // checks whether bookmark with given name is already present or not
        bool _isAlreadyPresent(std::string name);

    protected:

        //! Camera UI handler
        CORE::RefPtr<VizUI::ICameraUIHandler> _cameraUIHandler;

        // stores the current camera position
        Bookmark _currentBookmark;

        // currently selected index in tree
        int _currentIndexInTree;

        // currently selected Bookmark name in tree
        std::string _currentSelectedBookmarkName;

        // currently selected Bookmark path in tree
        std::string _currentSelectedBookmarkFilePath;

        QMLDirectoryTreeModel*      _qmlDirectoryBookmarksTreeModel;
        QMLDirectoryTreeModelItem*  _qmlDirectoryBookmarksTreeModelRootItem;
        
    };

} // namespace SMCQt
