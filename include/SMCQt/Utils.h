#ifndef SMCQT_UTILS_H_
#define SMCQT_UTILS_H_

/*****************************************************************************
*
* File             : Utils.h
* Version          : 1.0
* Module           : SMCQt
* Description      : Class Provides utility functionality
* Author           : Darshan Prata
* Author email     : darshan@vizexperts.com
* Reference        : 
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2012-2016, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <VizQt/QtGUI.h>
//#include <App/IUIHandlerManager.h>




namespace SMCQt
{
    class Utils
    {

    public:

        static void getModelLayerExtCenter(CORE::RefPtr<CORE::IObject> object, osg::Vec4d& extents, osg::Vec3d &center);
    };

} // namespace SMCQt

#endif  // SMCQT_UNDOREDOGUI_H_
