#ifndef SMCQT_ANIMATIONGUI_H_
#define SMCQT_ANIMATIONGUI_H_

/*****************************************************************************
*
* File             : AnimationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : AnimationGUI class declaration
* Author           : Kamal Grover
* Author email     : kamal@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <QDateTime>

#include <SMCUI/IAnimationUIHandler.h>
#include <SMCUI/ITourUIHandler.h>
#include <Terrain/IElevationObject.h>
#include <Terrain/IRasterObject.h>
#include <Util/GdalUtils.h>
#include <Core/IObject.h>
namespace SMCQt
{
    class AnimationGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
            QObject* animationGUIobj;
            QObject* addAnimationGUIobj;
    public:
        static const std::string AnimationGUIPopup;
        static const std::string AddAnimationGUIPopup;
        AnimationGUI();
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
        void _loadAndSubscribeSlots();

        public slots:
        void connectPopupLoader(QString type);
        void addToAnimationList(int index);
        void addToRampFileList(QString file);
        void removeFromAnimationList(int index);
        void moveInAnimationList(int from, int index);
        void playAnimation();
        void pauseAnimation();
        void stopAnimation();
        void loopAnimation(bool);
        void changeAnimationSpeed(double factor);
        //output map of newly generated filenames to filepaths 
        void genColorRelief(std::map<std::string, std::string>& filemap, std::string rampFile = DEFAULT_RAMP_FILE);
        void browseButtonClicked();
        //void addAnimation(QString animationName, QString filename);
        void saveAnimation(QString name);
        void loadAnimation(QString filepath);
    protected:
        virtual ~AnimationGUI();
        void toggleVisibility(int index);
        //TODO :: Move this function to UTIL 
        bool _writeMapToFile(std::string jsonfile, std::map <std::string, std::string> filemap);
        bool readFileToMap(std::string jsonfile, std::map<std::string, std::string>& filemap);
        void populateSavedAnimationFiles();
        void populateColorRampFiles();
        void addRasterToWorld(std::string name, std::string rasterfile);
        std::string getURLFromIObject(CORE::RefPtr<CORE::IObject>);
    private:
        static const std::string    DEFAULT_RAMP_FILE;
        std::map<std::string, CORE::RefPtr<CORE::IObject>> _rasterObjMap;
        QStringList _availableList;
        QStringList _animationList;
        QList<QObject*> _savedAnimationFiles;
        QList<QObject*> _rampFiles;
        int timeCount = 0;
        int currentIndex = 0;
        bool resume = false;
        bool loop = false;
        double baseChangeTime = 60;
        double changeTime = 60; // in no of frames.
                          
    };

} // namespace SMCQt

#endif  // SMCQT_ANIMATIONGUI_H_
