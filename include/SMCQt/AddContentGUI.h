#pragma once

/*****************************************************************************
*
* File             : AddContentGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : AddContentGUI class declaration
* Author           : Gaurav Garg
* Author email     : gaurav@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IObject.h>
#include <Core/WorldMaintainer.h>
#include <Core/IMessageFactory.h>
#include <Core/NamedAttribute.h>
#include <Core/GroupAttribute.h>
#include <Core/IPoint.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/QMLTreeModel.h> 

#include <SMCUI/IAddContentUIHandler.h>
#include <SMCUI/IAddCSVUIHandler.h>
#include <SMCUI/ILineUIHandler.h>

#include <VizUI/UIHandler.h>
#include <VizUI/ICameraUIHandler.h>

#include <VizDataUI/IDatabaseUIHandler.h>

#include <Elements/ISettingComponent.h>
#include <Elements/IMapSheetInfoComponent.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>
#include <Terrain/VectorObject.h>

#include <osgEarthUtil/WMS>

namespace SMCQt
{
    class SkippedTableModel : public QObject
    {
        Q_OBJECT
            Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
            Q_PROPERTY(QString srno READ srno WRITE setSrno)
    public:

        //! Constructor
        SkippedTableModel(QString srno, QString name)
        {
            _fileName = name;
            _srno = srno;
        }

        QString name() const
        {
            return _fileName;
        }
        void setName(const QString &name)
        {
            _fileName = name;
        }
        QString srno() const
        {
            return _srno;
        }
        void setSrno(const QString &name)
        {
            _srno = name;
        }

        //! Destructor
        ~SkippedTableModel(){}
    signals:
        void nameChanged();
    protected:

        //! feature layer
        QString _fileName, _srno;
    };

    class AddContentGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        AddContentGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void _loadAndSubscribeSlots();

signals:
        void  _setProgressValue(int);

        public slots:

            void popupLoaded(QString type);

        // Adding New Layer

        void addElevationData(QString fileName, QString filepath);
        void addVectorLayer(QString fileName, QString filepath, bool clamping, bool editable);
        void addRasterLayer(QString layerName, QString filePath, int minLOD, int maxLOD, bool overrideExtents, QString enhanceType,QString projectionType);
        void addNewVectorLayer(QString, QString, bool editable);
        void addDGNLayer(QString layerName, QString filePath, QString orclFilePath, bool extentsSpecified, bool reloadCache);
        void addExcelFile(QString name);
        void latlongComboboxClicked();
        void selectHyperlinkFromList(int index);
        void removeHyperlinkFromList(int index);
        void hyperlinkComboboxClicked(QString selectedHyperlink,bool add);

        void computeMinMaxLod(QString fileName);
        void browseButtonClicked(bool directoryChecked = false);
        void orclBrowseButtonClicked();
        void setProgressValue(int value);

        void _dgnMapSheetNumberChanged(QString mapSheetName);

        void _handleMinMarkClicked(bool);
        void _handleMaxMarkClicked(bool);
        void _subdatasetSelected();

        void addLayers();
        void remLayers();
        void curlRemLayer(std::string layer, bool layerGroup);
        void createCRSCombobox();


    private:

        //! Connect popup loader based on layer type
        void addRasterLayerPopupLoaded();
        void addElevationLayerPopupLoaded();
        void addVectorLayerPopupLoaded();
        void addDGNLayerPopupLoaded();
        void addExcelDataPopupLoaded();

        //! dgn helper functions
        void dgnFilePathChanged(QObject* addLayerPopup, QString filePath);
        void _getDGNExtents(QObject* addLayerPopup, const std::string& fileName);
        bool _validMapSheetName(const std::string& mapSheetName);
        osg::Vec2d _getMapCenter(const std::string& mapSheetName);
        void _addDGNDataONTerrain(const std::string& fileName, const std::string& filePath, const std::string& orclFilePath,
            const double& latitude, const double& longitude, bool reloadCache);

        //! function called by slot addRasterData 
        //!XX Need to remove this
        void _addRasterData(QString layerName, QString fileLocation, bool overrideExtend, int minlevel, int maxLevel, QString enhanceType,QString projectionType);

        //! 
        void filePathChanged(QObject*, QString file);

        void _populateGeoserverLists();
        void _populate(const osgEarth::Util::WMSLayer::LayerList& layerList, WmsTreeModelItem* _childItem);
        void _getLayerNames(std::list<WmsTreeModelItem*>& layers, WmsTreeModelItem* item);

        void _closePopup();

    protected:

        virtual ~AddContentGUI();

        //Check whether the Multiples files are been selected or not 
        bool _checkForMultipleFiles(QString files);

        QString fileSelectedFromQTBrowser;

        //Parameter for adding Raster Data
        std::string _rasterFile;
        std::string _rasterLayerName;
        std::string _enhanceType;
        std::string _projectionType;
        bool _overrideRasterExtend;
        unsigned int _currentMinLod, _currentMaxLod;

        std::string _currSelectedFullFileName;

    private:

        //Last Path History
        std::string _lastPath;

        bool _isDirectoryChecked;

        QList<QObject *> _vectorAttributeFilterList;

        //This will contain whether to add Raster , Elevation , Vector or DGN
        std::string _addLayerModeState;

        CORE::RefPtr<ELEMENTS::IMapSheetInfoComponent> _mapSheetInfoComponent;

        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;

        CORE::RefPtr<SMCUI::IAddCSVUIHandler> _cvsHandler;

        //marking for min and max coordinates
        bool _minMarking;

        bool _maxMarking;

        int _subdataset;
        int _multipleFileSelection;

        WmsTreeModel* _wmsTreeModel;
        WmsTreeModelItem* _childItem;

        std::vector<std::string> _attributeList;
        std::vector<std::string> _hyperlinkList;

        //! Skipped file list.
        QList<QObject *> _skippedList;

    };

} // namespace SMCQt