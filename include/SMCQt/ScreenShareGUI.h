#ifndef QT_SCREENSHAREGUI_H_
#define QT_SCREENSHAREGUI_H_

/*****************************************************************************
 *
 * File             : ScreenShareGUI.h
 * Version          : 1.0
 * Module           : indiQt
 * Description      : ScreenShareGUI class declaration
 * Author           : Udit Gangwani
 * Author email     : udit@vizexperts.com
 * Reference        : indiQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2010-2011, CAIR, DRDO                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

// indigis3d related headers
#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <HLA/INetworkCollaborationManager.h>

namespace SMCQt
{
    //! Add Trajectory GUI Dialog
    class ScreenShareGUI 
        : public VizQt::LayoutFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        public:
            
            // Widget names
            static const std::string LBMessage;
            static const std::string LEName;
            static const std::string PBRequest;
            static const std::string LBRequestMessage;
            static const std::string PBCancelRequest;
            static const std::string PBProgress;
            static const std::string PBTakeOwnerShip;
            static const std::string PBSubscribeScreen;

            //! Different Messages to be Displayed at different stages and with respect to current state of project            
            static const std::string MsgScreenSubscribed;
            static const std::string MsgRequestScreenSharing;

        public:

            ScreenShareGUI();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
            
            //! Initializes the following attributes:-
            //!     # Value editbox name
            //!     # Units combobox name
            //!     # Calculationtype editbox name
            void initializeAttributes();

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

        private slots:

            //! Handling Request for Screen Sharing and Subscriotion
            void _handleRequestButtonClicked();

            //! Handle Cancel Request for Screen Share and Screen Subscriotion
            void _handleCancelRequestClicked();

            void _handleOwnerShipTaken();

            void _handleSubscribeButtonClicked();

        protected:

            virtual ~ScreenShareGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            //! Set the initial GUI state
            bool _setInitialGUIState();

        protected:

            //! Network Collaboration Manager
            CORE::RefPtr<HLA::INetworkCollaborationManager> _netwrkCollaborationMgr;            

            //! boolean to indicate if request is still pending
            bool _requestPending;
    };

} // namespace indiQt

#endif  // QT_SCREENSHAREGUI_H_
