#ifndef _SMCQT_RASTERWINDOWGUI_H
#define _SMCQT_RASTERWINDOWGUI_H

#include <Stereo/export.h>
#include <VizQt/GUI.h>
#include <SMCUI/IAddContentUIHandler.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/ISettingComponent.h>
#include <Terrain/IRasterObject.h>
#include <Stereo/StereoViewerGUI.h>
#include <Stereo/StereoSceneGenerator.h>
#include <Stereo/MainWindowGUI.h>
#include <Stereo/Image.h>

#ifdef USE_QT4
#include <QtGui/QMainWindow>
#else
#include <QtWidgets/QMainWindow>
#endif

#include <QtDeclarative/QDeclarativeView>

namespace SMCQt
{
    class RasterWindowGUI : public SMCQt::DeclarativeFileGUI
    {
        Q_OBJECT

    public:
        RasterWindowGUI();
        ~RasterWindowGUI();

        void onRemovedFromGUIManager();

        void onAddedToGUIManager();

        void _loadAndSubscribeSlots();

        void _selectObject();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void rasterWindow();

            void connectSmpMenu(QString type);

            void closeStereoWizard(Stereo::MainWindowGUI*);

            void popupLoaded(QString);

            void okButtonClicked();

    protected:

        std::string url;
        std::vector<Stereo::MainWindowGUI*> _stereoWizard;

        Stereo::Image::EnhancementType _enhancementType;
    };

}
#endif
