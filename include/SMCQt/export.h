#ifndef SMCQT_DLL_EXPORT_H
#define SMCQT_DLL_EXPORT_H

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#   if defined(SMCQT_LIBRARY)
#      define SMCQT_DLL_EXPORT __declspec(dllexport)
#   else
#      define SMCQT_DLL_EXPORT __declspec(dllimport)
#   endif 
#else
#   if defined(SMCQT_LIBRARY)
#      define SMCQT_DLL_EXPORT __attribute__ ((visibility("default")))
#   else
#      define SMCQT_DLL_EXPORT
#   endif 
#endif

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#   if defined(SMCQT_LIBRARY)
#      define SMCQT_DLL_EXPORT __declspec(dllexport)
#   else
#      define SMCQT_DLL_EXPORT __declspec(dllimport)
#   endif 
#else
#   if defined(SMCQT_LIBRARY)
#      define SMCQT_DLL_EXPORT __attribute__ ((visibility("default")))
#   else
#      define SMCQT_DLL_EXPORT
#   endif 
#endif


#endif // ENV_DLL_EXPORT_H
