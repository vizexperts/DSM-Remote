#pragma once

/*****************************************************************************
*
* File             : MeasureHeightGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : MeasureHeightGUI class declaration
* Author           : Venkata Balakrushna Beesetti
* Author email     : balakrishna@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2010-2011, CAIR, DRDO                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IHeightCalculationUIHandler.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <QtCore/QTimer>

namespace SMCQt
{
    class MeasureHeightGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT


    public:

        static const std::string HeightAnalysisObjectName;

        MeasureHeightGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:
                //  Handlig Button functions 

                void _handleMarkButtonToggled(bool);


                // Updating measurement values
                void _handleCalculationTypeUpdatedTimer();
                void _updatePosition();

                void _fontSizeChanged ( QString size);
                void _fontColorChanged (QColor color);

                //! Display Position Values
                void _displayPositionValues();

    protected:

        virtual ~MeasureHeightGUI();

    protected:

        //! Qt timer
        QTimer _timer;

        bool _markingEnabled;

        bool _resetFlag;

        CORE::RefPtr<VizUI::ITerrainPickUIHandler> _tph;

        //! Height Calculation UI Handler
        CORE::RefPtr<SMCUI::IHeightCalculationUIHandler> _heightCalculationUIHandler;

        //! flag to check if a marking point was created
        bool _isPointCreated;

        //! Skip the point update once
        bool _skiponce;

        CORE::RefPtr<osg::PositionAttitudeTransform> _pat;

        osg::ref_ptr<osg::Geode> _geode;

        osg::Vec4f _fontColor;
        float  _size;
        std::string _height;

        QObject* _heightAnalysisObject;    
    };

} 
