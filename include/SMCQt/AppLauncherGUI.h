#ifdef USE_QT4

#ifndef SMPQT_APPLAUNCHERGUI_H_
#define SMPQT_APPLAUNCHERGUI_H_

/*****************************************************************************
 *
 * File             : AppLauncherGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : AppLauncherGUI class declaration
 * Author           : Raghavendra Polinki
 * Author email     : rp@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2012-2013, VizExperts India Private Limited (unpublished)
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Stereo/MainWindowGUI.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT AppLauncherGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        AppLauncherGUI();

        void onAddedToGUIManager();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

    public slots:
        void launchTerrainWizard();
        void launchStereoWizard();
        void launchStreamingWizard();
        void closeStereoWizard();

    protected:
        virtual ~AppLauncherGUI();
        
        virtual void _loadAndSubscribeSlots();

    protected:
        Stereo::MainWindowGUI* _stereoWizard;

    };
}   // namespace SMCQt

#endif  // SMPQT_LOGINGUI_H_

#endif