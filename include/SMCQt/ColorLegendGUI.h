#include <Core/IBaseUtils.h>

#include <VizQt/LayoutFileGUI.h>

#include <Elements/IColorMapComponent.h>
#include <Elements/IColorMap.h>

#include <SMCUI/IColorMapUIHandler.h>

#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    //QML Color Object
    class ColorLegendObject : public QObject
    {
        Q_OBJECT
            Q_PROPERTY(double  minVal READ  minVal WRITE setMinValue NOTIFY minValueChanged)
            Q_PROPERTY(double  maxVal READ  maxVal WRITE setMaxValue NOTIFY maxValueChanged)
            Q_PROPERTY(QColor  cellColor  READ  cellColor  WRITE setcellColor   NOTIFY  cellColorChanged)

    public:
        ColorLegendObject(QObject* parent = 0);
        ColorLegendObject(double minVal, double maxVal, QColor cellColor, QObject *parent = 0);

        double minVal() const;
        void setMinValue(double minVal);

        double maxVal() const;
        void setMaxValue(double maxVal);

        QColor cellColor()const;
        void setcellColor(QColor cellColor);

    signals:
        void minValueChanged();
        void maxValueChanged();
        void cellColorChanged();

    private:
        double min_Value;
        double max_Value;
        QColor cell_Color;

    };

    class SMCQT_DLL_EXPORT ColorLegendGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:
        static const std::string ColorLegend;
        ColorLegendGUI();

        public slots:
       
        Q_INVOKABLE 
         void handleSelectColorMap();
         void fillLegendColorTable(ELEMENTS::IColorMap* colorMap);
         
       
    protected:
        virtual ~ColorLegendGUI();
        //! Color component
        CORE::RefPtr<ELEMENTS::IColorMapComponent> _colorMapComponent;

        //! ColorMap UI handler
        CORE::RefPtr<SMCUI::IColorMapUIHandler> _uiHandler;

        //! current color map
        CORE::RefPtr<ELEMENTS::IColorMap> _colorMap;

        //!current colorList
        QList<QObject*> _colorList;
        //!current templateList
        QList<QObject*> _templateColorMapList;
    };

}