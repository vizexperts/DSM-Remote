#pragma once
/*****************************************************************************
*
* File             : EventGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : EventGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/IObject.h>
#include <SMCUI/IEventUIHandler.h>
#include <Core/IAnimationComponent.h>
#include <QAbstractListModel>
#include <Core/ISelectionComponent.h>
#include <Effects/IInfoEvent.h>

namespace SMCQt
{
    typedef CORE::RefPtr<EFFECTS::IInfoEvent> InfoEventItem; 

    class EventGUI
        : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        EventGUI();

        /** Called when GUI added/removed to GUI manager */
        void onAddedToGUIManager();
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

        // slots for global event gui
        void connectPopupLoader(QString type);
        void handleOkButtonClicked();
        void handleCancelButtonClicked();
        void attachInfoEventImageFile();
        void attachInfoEventAudioFile();
        void attachInfoEventVideoFile();
        void handleMarkButtonClicked(bool selected);
        void handlePreviewButtonClicked(bool selected);
        void handleExplosionScaleChanged(double value);
        void handleEventTypeChanged();
        void closeIconClicked();
        void showEventPreview(bool showPreview);
        void editEvent(bool enableEdit);
        void deleteEvent();
        void setActive(bool);
        void muteAudio(QString title, bool mute);
        void setInfoEvent(QObject* infoGUIObject, CORE::RefPtr<EFFECTS::IInfoEvent> infoEvent);
        void closePreview();

    signals:
        void pauseVideo();
        void playVideo();
        void load();
    protected:

        virtual ~EventGUI();
        virtual void _loadAndSubscribeSlots();

        //! reset gui params
        void _reset();
        void menuClosed();
        bool previewCreated;

    protected:

        bool configureInfoEvent();
        bool configureExplosionEvent();
        bool configureGunfireEvent();
        bool configureAlarmEvent();

        CORE::RefPtr<SMCUI::IEventUIHandler> _eventUIHandler;
        CORE::RefPtr<VizQt::IQtGUI> _addEventGUI;
        CORE::RefPtr<CORE::IAnimationComponent> _animationComponent;
        //InfoEventModel* _infoEventModel;

        // List of all the Info Event those are in playing
        QList< InfoEventItem > infoEventList;

    private :
        CORE::RefPtr<EFFECTS::IEvent> _addedEvent;
        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;
        bool editingEvent;

        // flag to check if the event is playing in preview
        bool isPreview = false;
        // flag to check if the event is playing in Edit Mode
        bool editPreview = false;
    };
} // namespace SMCQt
