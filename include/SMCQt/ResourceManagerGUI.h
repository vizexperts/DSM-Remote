#pragma once

/*****************************************************************************
*
* File             : ResourceManagerGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ResourceManagerGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCElements/IResourceManagerComponent.h>
#include <SMCUI/IMilitarySymbolPointUIHandler.h>
#include <Elements/ISymbolTypeLoader.h>

namespace SMCQt
{
    // QML Resource Object
    class ResourceObject : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(QString path READ path NOTIFY dataChanged)
            Q_PROPERTY(QString type READ type NOTIFY dataChanged)
            Q_PROPERTY(QString category READ category NOTIFY dataChanged)
            Q_PROPERTY(QString affiliation READ affiliation NOTIFY dataChanged)

            Q_PROPERTY(int allocated READ allocated WRITE setAllocated NOTIFY allocatedChanged)
            Q_PROPERTY(int deployed READ deployed WRITE setDeployed NOTIFY deployedChanged)

    public: 
        ResourceObject(const QString &path, const QString &type,
            const QString &category, const QString &affiliation, int allocated, int deployed , QObject *parent = 0)
            : QObject(parent)
            , m_path(path)
            , m_type(type)
            , m_category(category)
            , m_affiliation(affiliation)
            , m_allocated(allocated)
            , m_deployed(deployed)
        {}

        QString path() const { return m_path; }
        QString type() const { return m_type; }
        QString affiliation() const { return m_affiliation; }
        QString category() const { return m_category; }

        int allocated() const {return m_allocated; }
        void setAllocated(int allocated)
        {
            if(allocated != m_allocated)
            {
                m_allocated = allocated;
                emit allocatedChanged();
            }
        }

        int deployed() const { return m_deployed; }
        void setDeployed(int deployed)
        {
            if(deployed != m_deployed)
            {
                m_deployed = deployed;
                emit deployedChanged();
            }
        }


signals:
        void dataChanged();

        void allocatedChanged();
        void deployedChanged();

    private:
        QString m_path;
        QString m_type;
        QString m_category;
        QString m_affiliation;
        int     m_allocated;
        int     m_deployed;
    };

    /*************************************************************************************/

    class SMCQT_DLL_EXPORT ResourceManagerGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string MilitarySymbolContextualMenuObjectName;

        ResourceManagerGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void connectSMPMenu(QString type);

            void popupLoaded(QString type);

            // slots for resource allocation
            void addMilitaryResource(QString affiliation, QString categoryName, QString symbolName);

            void selectResource(QString fullName);

            //deselect Resource with given fullName
            void deselectResource(QString fullName);

            void deleteResource(QString fullName);

            //! Search(common for allocated resources and User Groups)
            void search(QString str);

            //! Menu destroyed
            void menuDestroyed();

            //! Slots for contextual menu
            void rename(QString newName);
            void handleLatLongAltChanged(QString);
            void deleteSymbol();
            void clampingChanged(bool value);
            void textVisibilityChanged(bool value);
            void sphereVisibilityChanged(bool value);
            void sphereRangeChanged( double value );
            void changeSymbolSize(QString);


            //! XXX: To be removed
            //void handleUserIdChanged(QString);
            
            void editEnabled(bool value);

            void connectContextualMenu();
            void disconnectContextualMenu();

    protected:
        virtual ~ResourceManagerGUI();

        virtual void _loadAndSubscribeSlots();

        // populate resources containing str
        void _populateResourceList(QString str = QString(""));

        void _populateContextualMenu();

        //! Configure symbol and gui after symbol is created
        void _configureSymbol();

        // Create metadata record of created symbol
        void _createMetadataRecord();

        ELEMENTS::IIcon*
            _loadIcon(std::string name, std::string fileName,
            const osg::Vec4s& color, double width, int lineType, double symbolSize);

        QList<QObject*> _resourceList;

        CORE::RefPtr<SMCElements::IResourceManagerComponent> _resourceManagerComponent;


        CORE::RefPtr<ELEMENTS::ISymbolTypeLoader> _symbolTypeLoader;

        CORE::RefPtr<SMCUI::IMilitarySymbolPointUIHandler> _unitUIHandler;

        QString _currentResourceFullName;
    };
}