#pragma once

/*****************************************************************************
*
* File             : DatabaseSettingsGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : DatabaseSettingsGUI class declaration
* Author           : Akshay Gupta
* Author email     : akshay@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/ISettingComponent.h>
#include <SMCUI/ILookUpProjectUIHandler.h>


namespace SMCQt
{
    class DatabaseSettingsGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        //structure to store database parameters
        struct DatabaseParameters
        {
            std::string dbType;
            std::string dbName = "";
            std::string userName;
            std::string passWord;
            std::string host = "";
            std::string port = "";
        };
		
        static const std::string DatabaseSettingsMenu;

        DatabaseSettingsGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            // set active to be called from qml 
            void setActive(bool value);

            // to save database with current parameters
            void saveButtonClicked();

            // delete selected database
            void deleteDatabase(QString value);

            // on comboBox clicked
            void comboBoxClicked(QString value);
            
            // to test database connection
            void testDatabaseConnection();

        private slots:

            //ok slot for deletig the file
            void _okSlotForDeleting();

            //cancel slot for deletig the file 
            void _cancelSlotForDeleting();

    protected:

        virtual ~DatabaseSettingsGUI();

        // populates available databases
        void _populateDatabases();

        // returns database of passed argument if available
        DatabaseSettingsGUI::DatabaseParameters& _getDatabase(std::string filename);

        // reads database instance and push it to _databaseInstanceList
        void _readAllDatabaseInstanceName();

        // to write database parameter in .db file at Terrain_DSM
        void _writeDatabaseFile(QString fileName, std::vector<QString>& values);

        // to check database connection
        bool _connectWithDatabase();

        // handler for IDatabase
        vizCore::RefPtr<VizDataUI::IDatabaseUIHandler> _dbUIHandler;

        //list of saved databases
        std::vector<std::string> _databaseInstanceList;

        // list of database parameter as required by _dbUIHandler
        std::map<std::string, std::string> _dbOptions;
		
        DatabaseSettingsGUI::DatabaseParameters _dbFile;

    };

} // namespace SMCQt
