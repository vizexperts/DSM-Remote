#pragma once
/*****************************************************************************
*
* File             : CreatePolygonGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreatePolygonGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
#include <VizUI/ISelectionUIHandler.h>
#include <GIS/IFeature.h>


namespace SMCQt
{
    // XXX - exported only for testing purpose
    class SMCQT_DLL_EXPORT CreatePolygonGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT


    public:

        static const std::string PolygonContextualMenuObjectName;

        CreatePolygonGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void setClamping(bool clamping);
        bool getClamping() const;
        std::string jsonFile;
        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);

            void selectOption(QString option);

            // Common slots
            void connectContextualMenu();
            void disconnectContextualMenu();

            void connectSmpMenu(QString type);

            // slots for contextual menu
            void rename(QString newName);
            void penColorChanged(QColor penColor);
            void fillColorChanged(QColor fillColor);
            void lineWidthChanged(double lineWidth);
            void deleteArea();
            void setEditMode(bool mode);
            void connectEditVerticesMenu(bool value);
            void deleteVertex();
            void toggleFillColor(bool value);
            void handleLatChanged();
            void handleLongChanged();
            void setPolygonEnable(bool value);

            void showAttributes();
            void handleAttributesOkButtonPressed();
            void handleAttributesCancelButtonPressed();

            void _populateContextualMenu();

            // slots for polygon layer
            void addPolygonToSelectedLayer(bool value);
            /*void exportSelectedLayer();*/
            void handleStyleApplyButton();
            void handleFontButton();

    protected slots:

        void _resetForDefaultHandling();

    protected:

        virtual ~CreatePolygonGUI();

        virtual void _loadAndSubscribeSlots();

        //! get the feature export UI handler
        void _loadAreaUIHandler();

        void _populateAttributes();

        void _createMetadataRecord();

        void _performHardReset();

    protected:

        //! Point UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaUIHandler;
        CORE::RefPtr<VizUI::ISelectionUIHandler> _selectionUIHandler;

        CORE::RefPtr<GIS::IFeature> _selectedFeature;

        //!
        CORE::RefPtr<CORE::IFeatureLayer> _selectedFeatureLayer;

        bool _addToDefault;
        bool _mouseClickRight;

        //! flag to keep track for proper clean up
        bool _cleanup;

        unsigned int _nextAreaCount;

        QList<QObject*> _attributeList;

        //!
        bool _clamping;

    private:
        void onVertexSelected( );
    };

} // namespace SMCQt
