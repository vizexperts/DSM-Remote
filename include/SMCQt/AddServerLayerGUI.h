#ifndef SMCQT_ADDSERVERLAYERGUI_H_
#define SMCQT_ADDSERVERLAYERGUI_H_

/*****************************************************************************
 *
 * File             : AddServerLayerGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : AddServerLayerGUI class declaration
 * Author           : Nishant Singh
 * Author email     : ns@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>

#include <VizQt/LayoutFileGUI.h>

#include <osgEarthUtil/WMS>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/QMLTreeModel.h> 
#include <QtCore/QTimer>
#include <osgEarthUtil/WFS>

class QLineEdit;
class QLabel;

namespace SMCQt
{
    class AddServerLayerGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

        public:

            static const std::string AddWMSLayerPopup;
            static const std::string AddWFSLayerPopup;
            static const std::string GeorbisServerlistDir;

            //! Constructor
            AddServerLayerGUI();
            
        public slots:

            //@{
            /** Set/Get active function */
            void connectPopUpLoader(QString type);
            //@}

            //! clear the GUI
            void reset();

            void _updateLayerName();

        private slots:

            //! Updates the area editbox regularly
            void _handleConnectButtonClicked();
            void _handleAddButtonClicked();
            void _layerNameAddedByUser(QString name);
           

        protected:

            virtual ~AddServerLayerGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            //! populate the WMS GUI
            void _populate(const osgEarth::Util::WMSLayer::LayerList& layerList, WmsTreeModelItem* parent);

            //! populate the WFS GUI
            void _populate(const std::vector<osg::ref_ptr<osgEarth::Util::WFSFeatureType>>& featureList, WmsTreeModelItem* parent);

            //! remove the child item
            void _removeChildItem(WmsTreeModelItem* item);

            //! get the selected layer names as comma seperated values
            void _getLayerNames(std::string& layers, WmsTreeModelItem* item);

            //! read all server names present in appdata and insert these in _georbISServerList
            void _readAllServers();

            //! get wms layer path from server name
            std::string _getWMSPath(std::string serverName);

        private:

            //! Qt timer
            QTimer _timer;

            //! Server Address
            std::string _serverAddress;
            
            WmsTreeModel* _wmsTreeModel;
            WmsTreeModelItem* _childItem;

            // inMemory list for saving server names saved in appdata
            std::vector<std::string> _georbISServerList;

            std::string _layerName;

            bool _noFurtherUpdate;
    };

} // namespace SMCQt

#endif  // SMCQT_ADDSERVERLAYERGUI_H_

