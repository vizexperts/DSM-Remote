#ifndef REPROJECTIMAGEGUI_H
#define REPROJECTIMAGEGUI_H

/*****************************************************************************
*
* File             : ReprojectImageGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : Image Reprojection class declaration
* Author           : Sandeep
* Author email     : sandeep@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : code inspector's name
* Inspection date  : YYYY-MM-DD
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <Stereo/export.h>
#include <Core/IBaseUtils.h>
#include <Core/IBaseVisitor.h>
#include <Core/IObject.h>

#include <VizQt/QtGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAddContentUIHandler.h>
#include <VizUI/UIHandler.h>
#include <Stereo/StereoViewerGUI.h>
#include <Stereo/StereoSceneGenerator.h>
#include <Stereo/MainWindowGUI.h>
#include <Stereo/Image.h>



namespace SMCQt
{
    class ReprojectImageGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED
        Q_OBJECT

    public:

        ReprojectImageGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

    private:

        //CORE::RefPtr<VizQt::IQtGUIManager> _manager;

        //Last Path History
        std::string _lastPath;

        public slots:

            /** Set/Get active function */ 
            void setActive(bool active);

            void closeStereoWizard(Stereo::MainWindowGUI*);

signals:

            //! signal emmited when a filteroperation is completed
            void _updateGUIStatus();

            private slots:

                //! Slots for event handling
                void _handleInputBrowseButtonClicked();
                void _handleoutputBrowseButtonClicked();
                void _handleOkButtonClicked();
                void connectPopupLoader(QString);

                void _viewImage(QString);
                void _handleGUIStatus();

    protected:

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        virtual ~ReprojectImageGUI();


        //  void _loadAddContentUIHandler();

        //! UIHandler for loading data
        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;


        std::vector<Stereo::MainWindowGUI*> _stereoWizard;
        QObject* _reprojectObject;

    };
}// namespace SMCQt

#endif // REPROJECTIMAGEGUI_H
