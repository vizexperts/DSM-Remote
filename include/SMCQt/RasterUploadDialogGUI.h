#ifndef RASTERUPLOADDIALOGGUI_H
#define RASTERUPLOADDIALOGGUI_H


#include <VizQt/GUI.h>

#include <VizDataUI/IDatabaseUIHandler.h>

#include <SMCUI/IAddContentUIHandler.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/ISettingComponent.h>

namespace SMCQt
{

    class RasterUploadDialogGUI : public SMCQt::DeclarativeFileGUI
    {
        Q_OBJECT

            QString _dataTypeSelect;

        QList<QObject*> dataTypeList;

    public:

        RasterUploadDialogGUI ();


        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        QString getTableName();

        QString getSchema();

        QString databaseName;

        std::map<std::string, std::string> _dbOptions;

        int getTileWidth();

        int getTileHeight();

        QObject* createDataType; 

        ~RasterUploadDialogGUI();


        void _loadAddContentUIHandler();

        void _loadAndSubscribeSlots();

        void _displaySchema();

        void onAddedToGUIManager();

        void onRemovedFromGUIManager();

        // for database 
        bool makeConnectionWithDatabase();

        bool _checkConnectionWithDatabase();

        VizDataUI::IDatabaseUIHandler* _getDatabaseUIHandler();

        std::map<std::string, std::string> _getDatabaseCredential();

        private slots:

            void _handleOkButtonClicked();

            void _schemaSelected();

            void popupLoaded(QString );

            void setParent();


    protected:
        //! UIHandler for loading  data
        vizCore::RefPtr<VizDataUI::IDatabaseUIHandler> _dbUIHandler;

        std::map<std::string,std::string> _dataType2Schema;

        bool _dbSelect;

        QString _dataTableName;

        QString _databaseSelect;

        int _tileWidth;

        int _tileHeight;

        bool _createCheckBox;

        bool _appendCheckBox;

        CORE::RefPtr<VizQt::IQtGUIManager> _manager; 

        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;

        CORE::RefPtr<ELEMENTS::ISettingComponent> _globalSettingComponent;

        bool _presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap, const std::string& keyword);

    };
}
#endif // RASTERUPLOADDIALOGGUI_H
