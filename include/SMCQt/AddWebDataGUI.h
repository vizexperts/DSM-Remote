#pragma once
/*****************************************************************************
*
* File             : AddImageGUI.h
* Version          : 1
* Module           : TKQt
* Description      : AddTabGUI Class of Ribbon GUI
* Author           : indira
* Author email     : author email id
* Reference        : Part of TKP Application
* Inspected by     : code inspector's name
* Inspection date  : YYYY-MM-DD
*
*****************************************************************************
* Copyright (c) 2010-2011,VizExperts India Pvt. Ltd                                        
*****************************************************************************/

#include <SMCUI/IAddContentUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    class AddWebDataGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED

        Q_OBJECT


    public:

        AddWebDataGUI();

        void initialize();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:
            void connectSmpMenu(QString type);

            private slots:

                void _handleYahooCBClicked(bool value);
                void _handleBingCBClicked(bool value);
                void _handleOSMCBClicked(bool value);
                void _handleWorldWindCBClicked(bool value);
                void _handleGoogleStreetCBClicked(bool value);
                void _handleGoogleHybridCBClicked(bool value);


    protected:

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        virtual ~AddWebDataGUI();

        //! Loads the .ui file and subscribes signals to slots
        void _loadAddContentUIHandler();

        //! UIHandler for loading data
        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;
    };
}
