#ifndef SMCQT_STEREOSETTINGSGUI_H_
#define SMCQT_STEREOSETTINGSGUI_H_

/*****************************************************************************
*
* File             : StereoSettingsGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : StereoSettingsGUI class declaration
* Author           : Gaurav Garg
* Author email     : gaurav@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/ISettingComponent.h>
#include <osg/DisplaySettings>

namespace SMCQt
{
    class StereoSettingsGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        StereoSettingsGUI();

        //! Initializes the following attributes:-
        //!     # Value editbox name
        //!     # Units combobox name
        //!     # Calculationtype editbox name
        void initializeAttributes();

        bool isToggleEnabled() const{return false;}

        bool isStereoEnabled() const{return 0.0;}

        int getStereoMode() const{return 0;}

        double getEyeSeperation() const{return 0.0;}

        double getScreenDistance() const{return 0.0;}

        double getScreenWidth() const{return 0.0;}

        double getScreenHeight() const{return 0.0;}

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            //void connectSMPMenu(QString type);
            void saveStereoSettings();
            void setMode(int mode);
            void toggleStereo(bool state);
            void toggleEyes(bool state);
            void updateEyeSeparation(double value);
            void updateScreenDistance(double value);
            void updateScreenHeight(double value);
            void updateScreenWidth(double value);
            void connectSmp();
            //@}

    protected:

        virtual ~StereoSettingsGUI();

        virtual void _loadAndSubscribeSlots();

        void _writeSettings();

        std::string _getAbsoluteSettingConfPath(const std::string& configFile);

    };

} // namespace SMCGUI

#endif  // SMCQT_STEREOSETTINGSGUI_H_
