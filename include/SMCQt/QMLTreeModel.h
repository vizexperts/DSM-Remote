#ifndef    SMCQT_QMLTREEMODEL_H_
#define    SMCQT_QMLTREEMODEL_H_

#include<QAbstractListModel>

namespace SMCQt
{
    class QMLTreeModelItem
    {
    public:
        QMLTreeModelItem();

        virtual ~QMLTreeModelItem();

        // adjust level property of all children
        void adjustChildrenLevels();

        // append item
        void addItem(QMLTreeModelItem* item);

        virtual void removeItem(QMLTreeModelItem* item);

        void clear();

        QMLTreeModelItem* parent();

        // get level
        int getLevel();

        // is item opened
        bool isOpened();

        void setOpened(bool value);

        bool isCheckable();

        void setCheckable(bool value);

        virtual Qt::CheckState getCheckState();
        virtual Qt::CheckState getManualCheckState(){return _checkState; };

        void setCheckState(Qt::CheckState state);

        const QList<QMLTreeModelItem*> & getChildren() const;

        void moveChild(int childIndex, int newPos);

        int getChildCount() const;

        bool hasChildren();

        bool draggable() const;

        void setDragEnabled(bool draggable);

    protected:

        int _level;
        bool _isOpened;
        bool _isItemCheckable;
        Qt::CheckState _checkState;
        bool _isItemDraggable;

        QMLTreeModelItem* _parent;

        QList<QMLTreeModelItem *> _children;
    };

    class SimpleTreeModelItem: public QMLTreeModelItem
    {
    public:
        SimpleTreeModelItem(QString name = QString())
            : _name(name)
        {}

        QString getName() const;

        void setName(QString name);

    private:

        QString _name;
    };

    class WmsTreeModelItem: public QMLTreeModelItem
    {
    public:
        WmsTreeModelItem(QString name = QString(), QString title = QString(),QString abstraction = QString())
            : _name(name), _title(title),_abstraction(abstraction)
        {}

        QString getName() const;

        void setName(QString name);

        QString getTitle() const;

        void setTitle(QString title);

        QString getAbstraction() const;

        void setAbstraction(QString abstraction);

    private:

        QString _name;
        QString _title;
        QString _abstraction;
    };

    class QMLTreeModel : public QAbstractListModel
    {
        Q_OBJECT
    public:
        explicit QMLTreeModel(QObject* Parent = 0);

        QVariant data(const QModelIndex & Index, int role = Qt::DisplayRole)const;

        int rowCount(const QModelIndex & Parent = QModelIndex()) const;

        void adjustChildrenLevels();

        void addItem(QMLTreeModelItem* item);

        void removeItem(QMLTreeModelItem* item);

        QMLTreeModelItem* getItem(int numIndex);

        int indexOf(QMLTreeModelItem* item) const;

        QHash<int,QByteArray> roleNames() const;

        void emitDataChanged(int index);

        void clear();

        public slots:

            void openItem(int numIndex);

            void closeItem(int numIndex);

            void toggleItem(int numIndex);

            virtual void toggleCheckedState(int numIndex);

            virtual void move(int numIndex, int parentIndex);

            int getLevel(int index);

    protected:

        Q_DISABLE_COPY(QMLTreeModel);

        QList<QMLTreeModelItem*> _items;


        enum ListMenuItemRoles
        {
            LevelRole = Qt::UserRole + 1,
            IsOpenedRole,
            HasChildrenRole,
            CountRole,
            IsCheckableRole,
            CheckedStateRole,
            IsDraggableRole,
            UserRole
        };
    };

    class SimpleTreeModel : public QMLTreeModel
    {
        QVariant data(const QModelIndex & Index, int role = Qt::DisplayRole)const;

        QHash<int,QByteArray> roleNames() const;

    private:
        enum TreeItemRoles
        {
            NameRole = QMLTreeModel::UserRole + 1
        };
    };


    class WmsTreeModel: public QMLTreeModel
    {
        QVariant data(const QModelIndex & Index, int role = Qt::DisplayRole)const;

        QHash<int,QByteArray> roleNames() const;

    private:
        enum TreeItemRoles
        {
            NameRole = QMLTreeModel::UserRole + 1,
            TitleRole,
            AbstractionRole
        };
    };

}   // namespace SMCQt

# endif    //    SMCQT_QMLTREEMODEL_H_
