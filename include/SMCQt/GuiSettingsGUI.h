#pragma once

/*****************************************************************************
*
* File             : GuiSettingsGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : GuiSettingsGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <Elements/IProjectSettingsComponent.h>

namespace SMCQt
{
    class GuiSettingsGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string GUISettingsMenuObjectName;

        GuiSettingsGUI();

        public slots:

            void connectGuiSettingsMenu();

    protected:

        virtual ~GuiSettingsGUI();

        private slots:
        //! slot fot store lanlongUnit in Setting.json
        void _latLongUnitChanged(int);
        //! slot fot store Dock/Undock type in Setting.json
        void _dockTypeChanged(bool);
        //! slot fot store IndianGrid and MGRS visibility in Setting.json

        void _grVisibilityChanged(int);
         //! slot fot store Ui Scale Value in Setting.json
        void _uiScaleChanged(double);

        //! slot for toggling multiTouch interaction from GUI settings
        void _toggleMultiTouch(bool startTouch);

        void _toggleShowZone(bool); 

        void _toggleShowLetter(bool); 

        void _setMapSheet(bool);

        void _setGRNumOfDigits(int num); 
    private:
        void _writeSettings(QString,QString);
        QObject* _guiSettingsMenu;
    };

} 
