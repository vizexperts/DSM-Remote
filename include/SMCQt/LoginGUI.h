#pragma once
/*****************************************************************************
 *
 * File             : LoginGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : LoginGUI class declaration
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright 2012-2013, VizExperts India Private Limited (unpublished)
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Database/IDatabase.h>
#include <QAbstractListModel>

namespace SMCQt
{

    ///////////////////////////////////////////////////////////////////////////
    //! \brief A basic construct to hold authorosiation roles per user.
    ///////////////////////////////////////////////////////////////////////////
    class QPermissionsObject 
    {
    public:

        QPermissionsObject(QString username)
            : _username(username)
            , _analysis(false)
            , _planning(false)
            , _add_data(false)
            , _adminUser(false)
            , _dirty(false)
        {}

        QString username() const { return _username; }
        void setUsername(const QString& name)
        {
            if(_username != name)
            {
                _username = name;
            }
        }

        bool analysis() const { return _analysis; }
        void setAnalysis(bool value)
        {
            if(_analysis != value)
            {
                _analysis = value;
            }
        }

        bool planning() const { return _planning; } 
        void setPlanning(bool value)
        {
            if(_planning != value)
            {
                _planning = value;
            }
        }

        bool add_data() const {return _add_data; }
        void setAdd_data(bool value)
        {
            if(_add_data != value)
            {
                _add_data = value;
            }
        }

        bool adminUser() const {return _adminUser; }
        void setAdminUser(bool value)
        {
            if(_adminUser != value)
            {
                _adminUser = value;
            }
        }

        bool dirty() const {return _dirty; }
        void setDirty(bool value)
        {
            if(_dirty != value)
            {
                _dirty = value;
            }
        }

    private:
        QString _username;
        bool _analysis;
        bool _planning;
        bool _add_data;
        bool _adminUser;
        bool _dirty;
    };

    ///////////////////////////////////////////////////////////////////////////
    //! \class QPermissionsModel
    //! \brief Model class for User authentication roles. Used to populate UACPopup view
    ///////////////////////////////////////////////////////////////////////////
    class QPermissionsModel : public QAbstractListModel
    {
        Q_OBJECT;

    public:
        explicit QPermissionsModel(QObject* parent = 0);

        ////////////////////////////////////////////////////////////////////////////////
        //! \brief  overloaded function from QAbstractListModel class that returns 
        //!         back data provided a given ModelIndex and a role
        //!
        //! \param Index index of item in view/model
        //!
        //! \param role Role for which data is needed
        //!
        ////////////////////////////////////////////////////////////////////////////////
        QVariant data(const QModelIndex & Index, int role = Qt::DisplayRole)const;

        int rowCount(const QModelIndex & Parent = QModelIndex())const;

        bool addItem(QPermissionsObject* item);

        QPermissionsObject* getItem(int numIndex);

        const QList<QPermissionsObject*> & getChildren() const;

        bool removeItem(int index);

        void clear();

        QHash<int,QByteArray> roleNames() const;

    public slots:

        //! Slots to change the data in the model from QML
        void analysisRoleChanged(int index, bool value);
        void planningRoleChanged(int index, bool value);
        void addDataRoleChanged(int index, bool value);
        void adminUserRoleChanged(int index, bool value);


    protected:

        bool _isValidIndex(int index) const
        {
            if(index < 0 || index >= _items.size())
                return false;
            
            return true;
        }
        
        //! Mark the obejct at given index as dirty.
        //! Only the items that are marked dirty would be updated in the database
        void markDirty(int index)
        {
            _items.at(index)->setDirty(true);
        }

        //! Explicitly disabling deep copy of model data.
        Q_DISABLE_COPY(QPermissionsModel);

        QList<QPermissionsObject*> _items;

        enum ListMenuRoleItems
        {
            UsernameRole = Qt::UserRole + 1,
            AnalysisRole,
            PlanningRole,
            AddDataRole,
            AdminUserRole,
            DirtyRole,
            UserRole
        };
    };

    class SMCQT_DLL_EXPORT LoginGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        //! QML object name for UAC menu
        static const std::string UacSettingsMenuName;

        //! Table name for Permissions table in the database
        static const std::string PermissionsTableName;

        //! Column names in the Permission table
        static const std::string PermissionsTable_Username;
        static const std::string PermissionsTable_Analysis;
        static const std::string PermissionsTable_Planning;
        static const std::string PermissionsTable_Add_Data;
        static const std::string PermissionsTable_AdminUser;

        LoginGUI();

        enum UserType {
            USERTYPE_NONE = 0,
            USERTYPE_INSTRUCTOR = 1,
            USERTYPE_MODELCREATOR = 2,
            USERTYPE_SYNDICATE = 3
        };
        struct User
        {
            std::string password;
            UserType type;
        };

        //! Name and user map
        typedef std::map<std::string, User> CredentialsMap;

        void initializeAttributes();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void addCredential(const CORE::NamedGroupAttribute& attr);
        CORE::RefPtr<CORE::NamedGroupAttribute> getCredential() const;

        void setAuthDatabase(const std::string& attr);
        std::string getAuthDatabase() const;

        static const std::string USERNAME;
        static const std::string PASSWORD;
        static const std::string TYPE;


    public slots:
        void checkPassword(QString userName, QString password);

        void connectUacPopup();
        void disconnectUacPopup();
        
        void okButtonClicked();
        void cancelButtonClicked();
        void addToActiveUsers(int index);
        void removeFromActiveUsers(int index);
        void saveIPAddress(QString ip);
        void checkLocalProject(bool value);


    protected:
        virtual ~LoginGUI();
        
        CredentialsMap _credentialsMap;

        void _populateUserList();

        // populates all the modes to be enabled in application
        void _checkLicenses();

        // checks the license for the passed string. True means license is available
        bool _isPresentInLicenseFile(std::string moduleName);

        void _createTable(const std::string& tableName, CORE::RefPtr<DATABASE::IDatabase> database);

        CORE::RefPtr<CORE::NamedGroupAttribute> _credentialType;

        std::string _currentUserName;

        //! Name of the database used for authentication
        std::string _authenticationDB;

        //! List of non active users
        QStringList _nonActiveUserList;

        //! List of active users
        QPermissionsModel* _permissionsListModel;

        std::vector<std::string> _modulesAvailableInLicense;

        bool _isVuPresent;
    };
}   // namespace SMCQt
