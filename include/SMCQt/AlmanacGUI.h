#pragma once

/*****************************************************************************
*
* File             : AlmanacGUI.h
* Version          : 1.0
* Module           : Qt
* Description      : AlmanacGUI class declaration
* Author           : Rahul Srivastava
* Author email     : rahul@vizexperts.com
* Reference        : Qt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizUI/IPointUIHandler.h>
#include <SMCUI/IAlmanacUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/VizComboBoxElement.h>
#include <QtCore/QVariant>

namespace SMCQt
{
    // GUI for almanac calculation. Takes as input position
    // and date.
    class AlmanacGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        // Property and object names
        static const std::string TFLongitude;
        static const std::string TFLatitude;
        static const std::string TFMoonrise;
        static const std::string TFMoonset;
        static const std::string TFSunrise;
        static const std::string TFSunset;
        static const std::string DTAlmanac;
        static const std::string TFMoonPhase;
        static const std::string IMGMoonPhaseViewer;
        static const std::string CBTimeZoneList;
        static const std::string AlmanacAnalysisPopup;

    public:

        AlmanacGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:
            void setActive(bool value);

        private slots:

            void _handleMarkButtonClicked(bool checked);

            void _handlePositionChangedManually();
            void _handleDateTimeChanged(const QDateTime&);
            void _handleTimeZoneChanged(QString);

    protected:

        virtual ~AlmanacGUI();

        //! Invokes UIHandler function
        void _calculateAlmanac();

        //! Updates the GUI widgets
        void _updateGUI(const SMCUI::AlmanacData&);

        void _setMoonPhaseImage(double moonPhase, unsigned int daysToNewMoon);

        //XXX Ag use argument name properly
        bool _checkForValidCharacters(std::string);
        void _getParameters(std::string, double &, double &, double &,char ch);

        void _getTimeZone();

        //! string to Hours  or Minutes conversin 
        void _stringToTimeConversion(const std::string&, double&, double&);

    protected:

        //! Point UI Handler
        CORE::RefPtr<VizUI::IPointUIHandler> _pointUIHandler;

        //! Almanac UIHandler
        CORE::RefPtr<SMCUI::IAlmanacUIHandler> _almanacUIHandler;

        //! Location
        osg::Vec3d _longLat;
        double _gmtTime;
        double _gmtZoneHour;
        double _gmtZoneMinutes;
        int _gmtZoneSeconds;

        //! flag to specify if messages are subscribed
        bool _isSubscribed;

        //! marked point
        CORE::RefPtr<CORE::IPoint> _point;

        std::vector<std::string> _moonPhaseDatabase;

        // Initialize Moon Phase Image 
        void _initializeMoonPhaseImageDB();
        void _initializeTimeZoneList();

        //XXX AG Comments are missing
        QList<QObject*> _timeZonesList;

        static int count;
    };

} // namespace GUI

