#pragma once

/*****************************************************************************
*
* File             : GeorbISServersSettingsGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : GeorbISServersSettingsGUI class declaration
* Author           : dharmendra bhakar
* Author email     : dharmendra@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IFeatureExportUIHandler.h>


namespace SMCQt
{
    class GeorbISServersSettingsGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        //structure to store georbISServer parameters
        struct GeorbISServerParameters
        {
            std::string folder = "";
            std::string host = "";
            std::string port = "";

        };
		
        static const std::string GeorbISSettingsMenu;
        static const std::string GeorbISServerJsonLocation;
        //!! constructor
        GeorbISServersSettingsGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:
            /**
            * set active to be called from qml 
            * param value a bool argument denoting popup opened
            *
            */
            void setActive(bool value);

        private slots:
            /**
            * to save georbISServer with current parameters(called from QMl)
            */
            void _saveButtonClicked();
            /**
            * delete selected georbIS server
            * param value a QString argument denoting GeorbIS server Name
            *
            */
            void _deleteGeorbISServer(QString value);
            /**
            * on comboBox clicked fill parameter according Server Name
            * param value a QString argument denoting GeorbIS server Name
            *
            */
            void _comboBoxClicked(QString value);
            /**
            * to test GeorbIS server connection
            */
            void _testGeorbISServerConnection();
            /**
            * ok slot for deletig the file
            *
            */
            void _okSlotForDeleting();
            /**
            * cancel slot for deletig the file 
            */
            void _cancelSlotForDeleting();

    protected:

        virtual ~GeorbISServersSettingsGUI();

        // populates available georbISServers
        void _populateGeorbISServers();

        // returns georbISServer of passed argument if available
        bool  _getGeorbISServer(std::string filename);

        // reads georbISServer instance and push it to _georbISServersInstanceList
        void _readAllGeorbISServerInstanceName();

        // to write georbISServer parameter in .db file at Terrain_DSM
        void _writeGeorbISServerFile(QString fileName, QString values);

        // to check georbISServer connection
        bool _connectWithGeorbISServer();

        // handler for IGeorbISServer
        vizCore::RefPtr<SMCUI::IFeatureExportUIHandler> _featureExportUIHandler;

        //list of saved georbISServers
        std::vector<std::string> _georbISServersInstanceList;

        // list of georbISServer parameter as required by _featureExportUIHandler
        std::string _serverUrl;
		
        GeorbISServerParameters _serverParameter;

    };

} // namespace SMCQt
