/*****************************************************************************
*
* File             : CutAndFillGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CutAndFillGUI class declaration
* Author           : dharmendra
* Author email     : dharmendra@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2015-2016, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <SMCUI/ICutAndFillUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
namespace SMCQt
{
    // Creates a new layer
    class CutAndFillGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        QObject* CutAndFillObject;
    public:

        static const std::string CutAndFillAnalysisPopup;

        CutAndFillGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void  _setProgressValue(int);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void _handlePBStartClicked();
                void _handlePBStopClicked();
                void _handlePBRunInBackgroundClicked();
                void  _handleProgressValueChanged(int);
                void _handlePBAreaClicked(bool pressed);
                void _reset();

    protected:

        virtual ~CutAndFillGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

    protected:

        //! Height Above UIHandler
        CORE::RefPtr<SMCUI::ICutAndFillUIHandler> _uiHandler;

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;
    };

} // namespace SMCQt
