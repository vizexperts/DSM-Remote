#pragma once

/*****************************************************************************
*
* File             : SettingsGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : SettingsGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/ISettingComponent.h>

namespace SMCQt
{
    class SettingsGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string UserSettingsMenu;

        SettingsGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        std::string rasterCurrentPath;
        std::string rasterLastPath;
        std::string elevationCurrentPath;
        std::string elevationLastPath;

        public slots:

            void populateProjectSettingsMenu();

            void okButtonClicked();

            void browseRasterData();
            void browseElevationData();

            void connectPopupLoader(QString type);

    protected:

        virtual ~SettingsGUI();

        virtual void _loadAndSubscribeSlots();

        void _executeWriteSettings();

        std::string _getAbsoluteSettingConfPath(const std::string& configFile);

        bool _checkWeatherAllSettinsArePresent();

        bool _presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap, const std::string& keyword);

        void _setProjectType();

        void _setProjectSetting();

        void _setLayerDatabaseSetting();

        void _setWebserverSetting();

        void _setLocalDataSetting();

        void _writeSettings();

        bool _readSettings();

        //Instance of the setting component
        CORE::RefPtr<ELEMENTS::ISettingComponent> _globalSettingComponent;

    };

} // namespace SMCQt
