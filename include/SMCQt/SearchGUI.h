#ifndef SMCQT_SEARCHGUI_H_
#define SMCQT_SEARCHGUI_H_

/*****************************************************************************
*
* File             : SearchGUI.h
* Version          : 1.3
* Module           : SMCQt
* Description      : SearchGUI.h class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <VizQt/IQtGUIManager.h>
#include <osg/Vec3d>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/IObject.h>
#include <Core/ICompositeObject.h>
#include <Database/IDockDbInfo.h>
#include <vector>
#include <QString>
#include <VizUI/IMouseMessage.h>
#include <VizUI/ITerrainPickUIHandler.h>
#include <SMCUI/IDatabaseLayerLoaderUIHandler.h>
#include <Elements/IMapSheetInfoComponent.h>
#include <SMCElements/ILayerComponent.h>
namespace SMCQt
{

    class SearchObj : public QObject
    {
        Q_OBJECT
            Q_PROPERTY(QString name READ name NOTIFY dataChanged)

    public: 
        SearchObj(const QString &name, double latitude, double longitude , QObject *parent = 0)
            : QObject(parent)
            , m_name(name)
            , m_latitude(latitude)
            , m_longitude(longitude)
        {}

        QString name() const { return m_name; }
        double latitude() const {return m_latitude;};
        double longitude() const {return m_longitude;};

signals:
        void dataChanged();

    private:
        QString m_name;
        double m_latitude;
        double m_longitude;
    };

    class  SearchGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;  
        DECLARE_IREFERENCED;

        Q_OBJECT


    private:

        bool _checkForDMS(QString &str,double &longitude,double &latitude);
        void _helpMessage(std::vector<std::string> &str);
        bool _checkValidString(QString qst);
        bool _extractLongLatGivenNAndE(int posEW , int posNS, std::string st,double &longitude,double &latitude);
        bool _stringContainAlphabet(QString);
        void _checkForKeywordsAndPopulateSuggestions(QString searchString);
        void _checkForMapGrAndLatLongExpr(QString searchString);//- validation and identify which type of request Map GR Lat long 
        void _addKeywordsToSuggestions(QString searchString);
        void _createSearchBookmark(); //Creating Folder Object to store searched place
        void _getDefaultLayerAttributes(std::vector<std::string>& attrNames,
            std::vector<std::string>& attrTypes, std::vector<int>& attrWidths, std::vector<int>& attrPrecisions) const;

    public:
        SearchGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Initialize properties from config
        void initializeAttributes();

        void _addKeyword(const CORE::NamedGroupAttribute& groupAttr);
        CORE::RefPtr<CORE::NamedGroupAttribute> _getKeyword() const;
        //use for read UId of reacentSearch form setting.conf of Project
        void _readUidOfRecentSearch();
        //use for Write UId of reacentSearch form setting.conf of Project
        void _writeUidOfRecentSearch();
        void _setGeoJumpRange(double value);
        double _getGeoJumpRange() const;
        //Show the icon on the GLOBE 
        void showMarker(osg::Vec3d , std::string); 
        //Show the Area Coverage by  MapSheet on the GLOBE 
        void showExtentArea(osg::Vec4d , std::string); 
        //Get the World Instance
        CORE::IWorld* worldInstance();
        //! string to add in element list 
        static const std::string BookMarkName;
        //! tag string to prevent transaction 
        static const std::string TagToPreventTransaction;
        void checkIndianGR(std::string std);
        public slots:

            //////
            void selectPlace(int index);
            void getGeoJumpInfo(QString);

    protected:

        CORE::RefPtr<DATABASE::IDockDbInfo>  _databaseComponent; 

        virtual void _loadAndSubscribeSlots();

        void clearList();

        virtual ~SearchGUI();

        CORE::RefPtr<VizUI::ITerrainPickUIHandler> _terrainPickHandler;

        CORE::RefPtr<SMCUI::IDatabaseLayerLoaderUIHandler> _databaseLayerLoaderUIHandler;

        std::map<std::string, std::string> _searchSpaceMap;

        QList<QObject*> _placesList;

        double _geoJumpRange;
        //Component for Map and GR Validation and conversion
        CORE::RefPtr<ELEMENTS::IMapSheetInfoComponent> _mapSheetComponent;
        //for show bookmark in element list
        osg::observer_ptr<CORE::IObject> _searchBookmark;
        // to read Unique iD of recentSearch from Project
        CORE::RefPtr<CORE::UniqueID> _uniqueIDOfRecentSearch;

    };
}

#endif // SMCQT_SEARCHGUI_H_
