#ifndef SMCQT_ENVIRONMENT_GUI_H_
#define SMCQT_ENVIRONMENT_GUI_H_

/*****************************************************************************
*
* File             : EnvironmentGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : EnvironmentGUI class declaration
* Author           : Gaurav Garg
* Author email     : gaurav@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/IObject.h>

#include <QtCore/QTimer>

namespace ENV{
    class IWeatherComponent;
};

namespace SMCQt
{
    class EnvironmentGUI 
        : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        EnvironmentGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void connectWeatherMenu();

            void toggleSunMoonStars(bool);
            void toggleLight(bool);
            void toggleCloudsEnable(bool);
            void setCloudsDensity(int);
            void toggleSnow(bool);
            void toggleRain(bool);
            void toggleFog(bool);
            void toggleDust(bool);
            void setFogDensity(int);

            void _update();

    protected:

        virtual ~EnvironmentGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! Utility function to get weather component
        ENV::IWeatherComponent* _getWeatherComponent();

        //! reset gui params
        void _reset();

    protected:

        //!
        bool _firstTime;

    };

} // namespace SMCQt

#endif  // SMCQT_ENVIRONMENT_GUI_H_
