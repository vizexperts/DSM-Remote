#ifndef SMCQT_LAYERTABLEMODEL_H_
#define SMCQT_LAYERTABLEMODEL_H_

/*****************************************************************************
 *
 * File             : LayerTableModel.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : LayerTableModel class declaration
 * Author           : Mohan Singh
 * Author email     : mohan@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2013-2014, CAIR, DRDO                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <QtCore/QAbstractTableModel>
#include <Core/IObject.h>
#include <Core/IFeatureLayer.h>
#include <ogrsf_frmts.h>
#include <Core/IMetadataFieldDefn.h>
#include <map>

namespace SMCQt
{
    class LayerTableModel : public QAbstractTableModel 
    {
        Q_OBJECT
        public:

            //! Constructor
            LayerTableModel(QObject* parent, CORE::IFeatureLayer* layer, OGRLayer* ogrlayer);

            QHash<int,QByteArray> roleNames() const;

            //! get the row count
            int rowCount(const QModelIndex &parent = QModelIndex()) const;

            //! get the data at the given index
            QVariant data(const QModelIndex &index, int role) const;

            //! get the column count
            int columnCount(const QModelIndex& index) const;

            //! item flags
            Qt::ItemFlags flags(const QModelIndex& index) const;

            //! Destructor
            ~LayerTableModel();


        protected:

            //! feature layer
            CORE::RefPtr<const CORE::IFeatureLayer> _layer;

            OGRLayer* _ogrlayer;
            
            typedef std::map<std::string, int> RoleMap;
            RoleMap _roleMap;

            std::vector<std::string> _roleNamesVector;

            QHash<int,QByteArray> _roleHash;

    };
}
#endif

