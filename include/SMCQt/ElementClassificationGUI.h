#pragma once

/*****************************************************************************
*
* File             : ElementClassificationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ElementClassificationGUI class declaration
* Author           : Mohan Singh
* Author email     : mohan@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2013-2014 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/NamedAttribute.h>
#include <Core/IBaseUtils.h>

#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/ElementListTreeModel.h>
#include <Elements/IDGNInfoComponent.h>

#include <VizUI/ISelectionUIHandler.h>
#include <VizUI/ICameraUIHandler.h>
#include <GIS/IFeatureLayer.h>
#include <Terrain/IModelObject.h>

namespace SMCQt
{
    class ElementListTreeModelItemDgnSpecific : public ElementListTreeModelItem
    {
    public:
        QString ElementListTreeModelItemDgnSpecific::getName() const
        {
            QString name("");

            if (_name != "")
            {
                name = _name;
            }
            else
            {
                name = QString::fromStdString(_object->getInterface<CORE::IBase>()->getName());
            }

            return name;
        }
    };
}
namespace CORE
{
    class IObject;
}

class QModelIndex;

namespace SMCQt
{
    class SMCQT_DLL_EXPORT ElementClassificationGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        ElementClassificationGUI();

        void onRemovedFromGUIManager();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //@{
        /** Set/Get function for element icons group attribute */
        void setElementIcons(const CORE::NamedGroupAttribute& groupattr);
        CORE::RefPtr<CORE::NamedGroupAttribute> getElementIcons() const;
        //@}

        public slots:

        void setActive(bool active);

        void setPrefer3D(bool value);

        void connectPopupLoader(QString);

        private slots:

        void _handleCloseButtonClicked();
        void  _itemChangedSendNotify(CORE::RefPtr<const CORE::IObject>);

        void _toggle3DFinished();

    protected:

        //! Destructor
        virtual ~ElementClassificationGUI();

        virtual void _loadAndSubscribeSlots();

        //! function to find SelectionUIHandler
        void _findSelectionUIHandler();

        /**
        * 2 passes required to _fillClassification
        * Only the layers about which DGNInfoComponent is aware are classifiable.
        * Unclassifiable layers are grouped under "Unknown".
        * Pass 1 is to classify the classifiable layers.
        * Pass 2 is to group the unclassfiable layers into "Unknown"
        * Build Tree from _dgnLayers (formed from the 2 passes).
        */
        void _fillClassification();

        void _fillFeatureMap();

        void _setCheckState(SMCQt::ElementListTreeModelItem* item, Qt::CheckState checkState);

        void _updateCheckStatus(SMCQt::ElementListTreeModelItem* item);

        struct FeatureMapAndFlag
        {
            std::vector<CORE::RefPtr<TERRAIN::IModelObject> > featureLayerVector;
            bool isProcessed;//flag to varify whether layer present in classification or not if not it will go in unknown classification
        };
        typedef std::map<std::string, FeatureMapAndFlag > FeatureMap;

        FeatureMap _featureMap;

        CORE::RefPtr<ELEMENTS::IDGNInfoComponent> _dgnInfoComponent;

        ElementListTreeModel* _treeModel;
        ElementListTreeModelItem* _dgnLayers;
        bool _setup;
        SMCQt::ElementListTreeModelItem* _findElementInTree(SMCQt::ElementListTreeModelItem* item,
            const CORE::IObject* object);

        //! Is currently toggling between 2d/3d
        bool _isToggling2d3d = false;

        /**
        * Iterates overall the children of the parent and checks for a child with the given name.
        * returns the pointer to the child, if present . 
        * returns NULL is child is not found.
        */
        ElementListTreeModelItem* _isAChild(ElementListTreeModelItem* parent, std::string name);

        /**
        * Prepares a new ElementListTreeModelItem with the given name and default settings
        * The prepared ITEM is checkable and is unchecked by default.
        */
        ElementListTreeModelItem* _prepareNewItem(QString name);

        /**
        * Iterates over all the children of the parent and checks for a child with the given name.
        * returns the pointer to the child, if present .
        * returns a new child prepared with the default settings
        * note : child is added to parent if it is newly created.
        */
        ElementListTreeModelItem* _getOrCreateChild(ElementListTreeModelItem* parent, std::string name);

        /**
        * Adds layer to the parent and sets visibility
        */
        void _processLayer(std::string layerName, FeatureMapAndFlag& featureLayer, ElementListTreeModelItem* parentItem);

        /**
        * Pass 1 of the 2 passes required to _fillClassification
        * Partially Build the Tree for _dgnLayers for classifiable layers
        * Entries from the ClassificationMap are added to the tree if a matching layer is found in _featureMap
        */
        void ElementClassificationGUI::_firstPass();
        
        /**
        * Pass 2 of the 2 passes required to _fillClassification
        * Iterate over the _featureMap for the remaining unclassified entries 
        * Put them in Major Classification "unknown" and Minor Classification "unknown" in the tree model and Process them.
        */
        void ElementClassificationGUI::_secondPass();

    };
} // namespace SMCQt
