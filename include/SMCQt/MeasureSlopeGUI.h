#pragma once

/*****************************************************************************
*
* File             : MeasureSlopeGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : MeasureSlopeGUI class declaration
* Author           : Mohan Singh
* Author email     : mohan@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IPointUIHandler2.h>
#include <SMCUI/ISlopeCalculationUIHandler.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT MeasureSlopeGUI: public SMCQt::DeclarativeFileGUI

    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:
        static const std::string SlopeAnalysisObjectName;

        MeasureSlopeGUI();
        ~MeasureSlopeGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
        public slots:

            void setSlopeEnable(bool);
             // Slots for Point1 handling
            void _handlePBMarkPointFirstClicked(bool);
            // Slots for Point2 handling
            void _handlePBMarkPointSecondClicked(bool);

        


    protected:


        //! enable/disable point handler 
        void _setPointHandlerEnabled(bool enable);
        //calculate slope through Ui handler execute Funtion
        void _slopeCalculation();
        //! Slope Calculation UI Handler
        CORE::RefPtr<SMCUI::ISlopeCalculationUIHandler> _slopeCalculationHandler;
        //! Resets the calculated Slope
        void _reset();

       //! Get the point from point UI handler and if both point Marked execute calculation
        void _processPoint();

        //! Point selection state
        enum PointSelectionState
        {
            NO_MARK,
            POINT1_MARK,
            POINT2_MARK
        };

        //keep point Selection State i.e point1_mark , Point2_mark
        PointSelectionState _state;

        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointHandler;
        //! point1 object 
        CORE::RefPtr<CORE::IObject> _point1;

        //! point2 object
        CORE::RefPtr<CORE::IObject> _point2;

    };
}
