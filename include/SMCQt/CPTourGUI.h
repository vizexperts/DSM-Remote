#pragma once

/*****************************************************************************
*
* File             : CPTourGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CPTourGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCUI/IAnimationUIHandler.h>
#include <SMCElements/ICheckpointPathTourComponent.h>


namespace SMCQt
{
    class CPTourGUI: public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:
        CPTourGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //void initializeAttributes();

        void onAddedToGUIManager();

        void onRemovedFromGUIManager();

        public slots:

            void connectTourMenu();

            void playTour();
            void pauseTour();
            void stopTour();
            void decreaseSpeedFactor();
            void increaseSpeedFactor();
            void closed();
            //void 

    protected: 

        ~CPTourGUI();

        virtual void _loadAndSubscribeSlots();

    private:

        CORE::RefPtr<SMCElements::ICheckpointPathTourComponent> _cpTourComponent;

        CORE::RefPtr<SMCUI::IAnimationUIHandler> _animationUIHandler;

    };
}