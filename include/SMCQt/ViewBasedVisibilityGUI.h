#pragma once

/*****************************************************************************
*
* File             : ViewBasedVisibilityGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ViewBasedVisibilityGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/ISelectable.h>
#include <View/ICameraComponent.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT ViewBasedVisibilityGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT;

    public:

        ViewBasedVisibilityGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void _loadAndSubscribeSlots();

    private slots:

            void popupLoaded(QString type);

            //! Signal Handlers for layer based LOD
            void _setLOD(QString type, int lod);
            void _goToLod(int lod, bool zoomin);
            void _toggleViewBasedVisProp(bool value);
            void _popupClosed();

    protected:

        ~ViewBasedVisibilityGUI();

        void _setVisualizationSpinners();

        CORE::RefPtr<CORE::ISelectable> _currentSelection;

        CORE::RefPtr<VIEW::ICameraComponent> _cameraComponent;

        //CORE::RefPtr<ELEMENTS::ISelectionCopo> _cameraComponent;

    };
}
