#pragma once

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <VizUI/IPointUIHandler.h>
#include <SMCUI/ICrestClearanceUIHandler.h>
#include <Core/IObject.h>
#include <Core/IReferenced.h>
#include <Elements/export.h>

#include <Elements/IIcon.h>
#include <Core/Property.h>


#include <osg/Geode>
#include <osg/ShapeDrawable>
#include <osg/Shape>
#include <osg/PolygonMode>
#include <osg/Depth>
#include <Database/IDatabase.h>

namespace SMCQt
{
    //! GUI
    class GunRangeGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        // crest clearance icon file path
        static const std::string gunIconPath;
        static const std::string TarIconPath;

        static const std::string GunRangeMenuObjectName;

    public:

        GunRangeGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

        private slots:

            //! Gets the point from PointUIHandler and sets the relevant GUI
            void _processPoint();
            // Slots for event handling

            void _handleStartButtonClicked();
            void _handleStopButtonClicked();
            void _handleGPMarkButtonClicked(bool);
            void _handleDirMarkButtonClicked(bool);
            void changeToDefaultStateSlot();
            void _closeGUI(); 
    protected:

        virtual ~GunRangeGUI();

        //! Removes the object from the world
        void _removeObject(CORE::IObject* obj);

        //! Sets target point
        void _setTargetPoint(CORE::IPoint* point);

        //! Sets launch point
        void _setLaunchPoint(CORE::IPoint* point);

        // this function will activate the mouse events and set the point to create events when clicked 
        void _setPointHandlerEnabled(bool pressed);

signals :
        void changeToDefaultStateSignal();

    protected:

        // different states in which the crest clearance gui will be
        // these states are used in function processPoint() to populate the corresponding textfields
        enum PointSelectionState
        {
            NO_MARK,
            LP_MARK_PPC,
            DIR_MARK_PPC,
        };

        //! Transmitter/Receiver Icon Pointer
        CORE::RefPtr<ELEMENTS::IIcon> _gunIcon;
        CORE::RefPtr<ELEMENTS::IIcon> _tarIcon;

        //! Point UI Handler
        CORE::RefPtr<VizUI::IPointUIHandler> _pointHandler;

        //! Crest Clearance UI Handler
        CORE::RefPtr<SMCUI::ICrestClearanceUIHandler> _gunRangeHandler;

        //! Point selection state
        PointSelectionState _state;

        //! target position
        osg::Vec3d _targetPos;

        //! Launch point
        CORE::RefPtr<CORE::IObject> _lpPoint;

        //! Target/Direction point
        CORE::RefPtr<CORE::IObject> _tgtPoint;

        CORE::RefPtr<osg::PositionAttitudeTransform> _spherePat;

        // qml object for crestClearance file
        QObject* _gunRangeMenuObject;

        bool _gunAdd;

    };

} // namespace SMCQt


