#ifndef SMCQT_CLASSIFICATIONOBJECT_H_
#define SMCQT_CLASSIFICATIONOBJECT_H_

/*****************************************************************************
*
* File             : VectorClassificationObject.h
* Version          : 1.0
* Module           : SMCQt
* Description      : VectorClassificationObject class declaration
* Author           : Kamal Grover
* Author email     : kamal@vizexperts.com
* Reference        : QObject derived class used for showing Object attributes
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <QObject>

namespace SMCQt
{

    class ClassificationObject : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
            Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
			Q_PROPERTY(bool state READ state WRITE setState NOTIFY stateChanged)

    public:

        ClassificationObject(QObject* parent = 0);
        ClassificationObject(const QString &name, const QString &type, const bool& state, QObject *parent = 0);

        QString name() const;
        void setName(const QString &name);

        QString type() const;
        Q_INVOKABLE void setType(const QString &type);
		
		bool state() const;
		Q_INVOKABLE void setState(const bool& state);


    signals:
        void nameChanged();
        void typeChanged();
		void stateChanged();
    private:
        QString m_name;
        QString m_type;
		bool m_state;
    };
}

#endif //SMCQT_CLASSIFICATIONOBJECT_H_