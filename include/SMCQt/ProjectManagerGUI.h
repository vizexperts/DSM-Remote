#pragma once

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/FilesListModel.h>
#include <VizQt/IQtGUIManager.h>

#include <Core/Base.h>
#include <Core/IBaseUtils.h>

#include <SMCUI/ILookUpProjectUIHandler.h>
#include <Elements/ISettingComponent.h>
#include <SMCElements/IProjectLoadAndSaveComponent.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT ProjectManagerGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        enum ActiveTab
        {
            PLANNER
        };

        static const std::string VizStyleObjectName;

        static const std::string PLANNERDIRECTORY;
        static const std::string TEMPLATEDIRECTORY;

        static const std::string PLANNERFILEEXTENSION;
        static const std::string TEMPLATEFILEEXTENSION;

        static const std::string PLANNERSERVERFOLDER;

        static const std::string SERVERPLANNERDIRECTORY;

        static const std::string SERVERTEMPLATEFILE;
        static const std::string TEMPLATESERVERFOLDER;

        // smtemplate menu string
        static const std::string smpTemplatesMenuObjectName;

        ProjectManagerGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

        //Sets the active tab of the tab frame on Desktop
        void setActiveTab(int currActiveTab);

        // slot to save project
        void projectSave();
        void projectSaveAs();

        void projectSaveAsProject(QString fileName);


        //! Slots for templates
        void projectSaveAsTemplate(QString name);
        void setDefaultTemplate(QString name);

        void loadProject(QString fileName, int type);

        void deleteProject();
        void cancelDelete();

        void loadFiles();
        void logoff();

        // Load Template QML Object
        void loadsmpTemplatesMenu();

        //Function to delete selected template.
        void deleteTemplate(QString name);

        void confirmTemplateDeletion();
        void cancelTemplateDeletion();


        // slot to create new sand model
        void newProjectName(QString projectName);

        void searchProjects(QString filter, int type);
        void searchPlanProjects(QString filter);

        //! Slot called from UI when 'Home' button is clicked
        void closeProject();

        //! Save the current project and open desktop
        void projectSaveOnClose();

        //! Open the Desktop to load a new project
        void openDesktop();

        // slots to close the application
        void confirmCloseApplication();
        void closeApplicationOkClicked();

        //! dummy function to assign to slots where no work is needed
        void emptySlot(){};

        void confirmDeleteProject(QString filename, int type);

        // called when project list has been obtained from the database
        void populateProjectList();

        // world read
        void addWorldToMaintainer();

        // Return Default Tempalte Name
        QString getDefaultTemplate();

    signals:
        void _setProgressValue(int value);

    protected:

        void _performHardReset();

        void _calcNormalWindowWidth();

        virtual ~ProjectManagerGUI();

        void _assignSignalForSaveAsAndExport();

        bool SaveFile();

        bool SaveAsFile(const std::string& file, const SMCUI::ILookUpProjectUIHandler::ProjectType& projectType = SMCUI::ILookUpProjectUIHandler::PLANNER);

        void _flushTheDirectoryContents(const std::string& folderName);

        void _clearUndoStack();

        void _assignSignalToConfirmationDialogBox();

        void _createNewProject(const std::string& projectName);

        void _setProjectType(const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType);

        void _resetPopUpGUI();

        bool _projectAlreadyProject(const std::string& projectName, const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType);

        std::string _getPresentProject() const;

        void _setPresentProject(const std::string& projectName);

        void _executeLoadFiles();

        void _executeLoadProject(QString fileName, int type);

        // save project in different thread
        void _executeProjectSave();

        void _executeSaveAs(QString fileName);

        CORE::IObject* _readProjectFile(const std::string& projectName, const int& type);

        CORE::IObject* _readLocalProject(const std::string& projectName, const int& type);

        //Suport for local project hierarchy
        bool _localProjectAlreadyPresent(const std::string& projectName, const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType);

        void _getTheAvailableLocalProjects();

        void _getLocalProject(const std::string& projectLocation, std::vector<std::string>& creatorOrPlannerList, int limit = 20);

        void _getLocalProjectDateWise(const std::string& projectLocation, int type);

        void _loadLocalProject(const std::string& fileName, const int& type);

        CORE::IObject* _readLocalProjectFile(const std::string& fileName, const int& type);

        bool _saveLocalProject();

        bool _saveAsLocalProject(const std::string& file, const SMCUI::ILookUpProjectUIHandler::ProjectType& projectType);

        void _moveOtherAssociatedFileToDestinationFolder
            (const std::string& sourcefolderName, const std::string& folderName);

        void _deleteLocalProject(const std::string& projectName, const int& type);

        void _copyDefaultProjectTo(std::string  projectFileName);

        void inline _subscribeToProjectLoaderComponentMessages();
        void inline _unsubscribeFromProjectLoaderComponentMessages();
        //read setting from setting.json file and set in VizStyle like latunit and dock/Undock
        void _setVizStyleGlobalSetting();

        // get the project settings set by setting componnet
        void _getProjectSettings();

        // this method returns true if there are database settings present in the setting component
        bool _getDatabaseCredential(const std::string& database);

        // returns true if setting avalibale in glabal map
        bool _presentInGlobalSettingMap(const std::map<std::string, std::string>& settingMap, const std::string& keyword);

        // return url of the server
        std::string _getProjectLocationOnServer();

        // this method add/ update the name to the database
        void _addEntryToDatabase(const std::string& projectName, const SMCUI::ILookUpProjectUIHandler::NewOrSaveType& type, const SMCUI::ILookUpProjectUIHandler::ProjectType &projectType);

        // this uploads the save as project to the server
        void _uploadDefaultProjectToServerNamedAs(const std::string& newName);

        // upload the files to the server
        void _uploadProjectFilesToServer(const std::string& projectFilePath, const SMCUI::ILookUpProjectUIHandler::ProjectType& planner);

        // get default template name
        std::string _getDefaultTemplateNameFromServer();

        //Copy default project files in server
        void copyDefaultTemplateToProjectLocation(std::string templateName,std::string projectLocation);

        protected slots:

        virtual void _onSetProgressValue(int);

    protected:

        virtual void _loadAndSubscribeSlots();

        std::string _localMachineSavePath;

        //! GUI Manager
        CORE::RefPtr<VizQt::IQtGUIManager> _manager;

        //Taking the instance of LookUpProject UI handler
        CORE::RefPtr<SMCUI::ILookUpProjectUIHandler> _lookUpUIHandler;

        //Enter the name of the new project name
        std::string _newCreatorProjectName;

        //Keep the Dummy UID ref
        std::string _dummyUID;

        //This will have the folder where the Folder contents are temporay been swapped in and out when each project is loaded
        std::string _temporaryFolder;

    private:

        void cleanupProjectFolder();

        /**
        *
        * \brief used to refresh template combo box and set new value to next entry in list or empty entry.
        *
        *	\retval void
        */
        void refreshTemplateComboBox();



        //Whether it is Creator Or Planner
        int _userType;

        std::string _planSchemaName, _planTableName;

        std::string _webServerAddress;
        std::string _databaseToConnect;

        std::string _projectLocationOnServer;
        std::string _presentProject;
        std::string _currentProjectUID;
        std::string _newProjectName;

        SMCUI::ILookUpProjectUIHandler::ProjectType _projectType;

        std::string _webserverUsername;
        std::string _webserverPassword;

        //This is to store the Database credential
        SMCUI::ILookUpProjectUIHandler::DatabaseCredential _databaseCredential;

        CORE::RefPtr<ELEMENTS::ISettingComponent> _globalSettingComponent;

        CORE::RefPtr<SMCElements::IProjectLoadAndSaveComponent> _projectSaveAndLoadComponent;

        FilesListModel* _plannerListModel;

        // pair of project name and creation time
        QList<QPair<QString, QString> > _plannerProjectList;
        //QList<QPair<QString, QString>>

        // list of last six modified projects
        std::vector<std::string> _recentPlannerProjects;

        std::vector<std::string> _templateProjects;

        CORE::RefPtr<CORE::IObject> _worldReadFromFile;

        QString _projectToDeleteName;
        int _projectToDeleteType;

        bool isProjectSavable;

        ActiveTab _activeTab;

        std::string _applicationName;

        //! Flag to check whether currently a project loading is already in progress
        bool _loadingProject;

        int _screenResolutionX;

        int _screenResolutionY;

        float _windowScaleFactor;

        float _aspectRatio;

        int _normalWindowWidth;

        int _normalWindowHeight;

        int _windowPosX;

        int _windowPosY;

        QString _strTemlateName;

    };
}
