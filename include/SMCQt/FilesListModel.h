#ifndef SMCQT_FILESLISTMODEL_H_
#define SMCQT_FILESLISTMODEL_H_

/******************************************************************************
* File              : FilesListModel.h
* Version           : 1.0
* Module            : SMCQt
* Description       : List Model for list of files shown on desktop
* Author            : Nitish Puri
* Author email      : nitish@vizexperts.com
* Reference         : 
* Inspected by      : 
* Inspection date   : 

*******************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*******************************************************************************
*
* Revision log (latest on the top):
*
*
******************************************************************************/

#include <QAbstractListModel>
#include <QStringList>
#include <QDateTime>

namespace SMCQt
{
    class FilesGroup 
    {
    public : 
        FilesGroup(QString name);

        void addFile(QString fileName);

        QString getName() const;
        void setName(QString name);
        
        QStringList children() const;


    private:
        QString _name;
        QStringList _children;
    };

    bool filesGroupLessThan(const FilesGroup& g1, const FilesGroup& g2);

    class FilesListModel : public QAbstractListModel
    {
        Q_OBJECT

    public:

        explicit FilesListModel(QObject* Parent = 0);

        QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

        void addFile(QString fileName, QString groupName);

        void sort();

        int rowCount(const QModelIndex & Parent = QModelIndex())const;

        QHash<int, QByteArray> roleNames() const;

        void clear();

    private:

        Q_DISABLE_COPY(FilesListModel);

        QList<FilesGroup> _groups;

        enum ListMenuItemRoles
        {
            GroupNameRole = Qt::UserRole + 1,
            ChildrenRole,
            CountRole            
        };
    };

}   // namespace SMCQt

#endif      // SMCQT_FILESLISTMODEL_H_
