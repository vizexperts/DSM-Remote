#ifndef SMCQT_CREATEOPACITYSLIDER_H_
#define SMCQT_CREATEOPACITYSLIDER_H_

/*****************************************************************************
*
* File             : CreateOpacitySliderGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateLineGUI class declaration
* Author           : vikram singh
* Author email     : vikram@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <QColor>
#include <SMCUI/IPointUIHandler2.h>
#include <Core/Base.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <App/AccessElementUtils.h>

#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>

#include <osgEarth/ImageLayer>
#include <osgEarthAnnotation/ImageOverlay>

#include <Terrain/IRasterObject.h>
#include <Elements/IOverlayImage.h> 
#include <Core/ISelectionComponent.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateOpacitySliderGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        CreateOpacitySliderGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
       
        double _getCurrentOpacity();
        
   public slots:

        void changeSliderValueHandler( double );
        void okButtonChangeSliderValueHandler(double);
        void connectSmpMenu(QString type);
        void setActive(bool);

    protected:

        virtual ~CreateOpacitySliderGUI();

        virtual void _loadAndSubscribeSlots();

        void _changeOpacity(const double& value);
        double _currentOpacityValue;//! use for store current opacity if new opacity from slider not apply then set this 
        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

    };

} // namespace SMCQt

#endif  // SMCQT_CREATEOPACITYSLIDER_H_
