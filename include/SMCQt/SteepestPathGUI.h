/*****************************************************************************
*
* File             : SteepestPathGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : SteepestPathGUI class declaration
* Author           : Akshay Gupta
* Author email     : akshay@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2015-2016, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/ISteepestPathUIHandler.h>
#include <SMCUI/IAreaUIHandler.h>
#include <SMCUI/IPointUIHandler2.h>
#include <Core/IObject.h>
#include <Core/IPolygon.h>

namespace SMCQt
{
    // Creates a new layer
    class SteepestPathGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        QObject* SteepestPathObject;
    
    public:

        static const std::string SteepestPathAnalysisPopup;

        SteepestPathGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
        
    public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

    private slots:

            // Slots for event handling
            void _handleStartButtonClicked();
            void _handleStopButtonClicked();
            void _handlePBAreaClicked(bool pressed);
            void _handlePBStartPointClicked(bool pressed);
                

    protected:

             //@{
            /** play, stop and pause Steep Path computation*/
            bool play();
            bool stop();
            //@}

            void _handleSteepPathButtonPressed(std::string& playEvent);

            //! Removes the object from the world
            void _removeObject(CORE::IObject* obj);

            //! Sets start point
            void _setStartPoint(CORE::IPoint* point);

            //! reset gui params
            void _reset();

            virtual ~SteepestPathGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

    protected:

        //! steep path UI Handler
        CORE::RefPtr<SMCUI::ISteepestPathUIHandler> _steepPathGUIHandler;

        //! point UI Handler
        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointHandler;

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;


        //! Start point
        CORE::RefPtr<CORE::IObject> _startPoint;

        //! array extent
        osg::ref_ptr<osg::Vec2dArray> _vExtent;

        //! Polygon ownership
        CORE::RefPtr<CORE::IPolygon> _iPolygon;

        //! is polygon eligible to be removed
        bool _bPolygonRemovable;
        
    };

} // namespace SMCQt
