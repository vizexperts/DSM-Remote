#pragma once
/*****************************************************************************
*
* File             : MeasureAreaGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : MeasureAreaGUI class declaration
* Author           : Rahul Srivastava
* Author email     : rahul@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
#include <QtCore/QTimer>
#include <SMCUI/ISurfaceAreaCalcUIHandler.h>
#include <Core/ISelectionComponent.h>

namespace SMCQt
{
    // XXX - exported only for testing purpose
    class SMCQT_DLL_EXPORT MeasureAreaGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        static const std::string MeasureAreaGUIPopup;

        MeasureAreaGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //@{
        /** sets/gets emableDrag attribute */
        void setEnableDrag(bool value);
        bool getEnableDrag() const;
        //@}

        public slots:

            void setActive(bool value);

            void markArea(bool value);
            void selectArea(bool value);
            void startCalculation();
            void stopCalculation();
            void changeUnit(QString unit);

            void handleAreaUpdateTimer();
            void handleCalculationCompleted();

    protected:

        virtual ~MeasureAreaGUI();

        virtual void _loadAndSubscribeSlots();

        //! Resets the calculated area 
        void _reset();

signals:

        void _calculationCompleted();

    protected:

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

        //! SurfaceArea UI Handler
        CORE::RefPtr<SMCUI::ISurfaceAreaCalcUIHandler> _surfaceAreaCalcHandler;

        // selection component
        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

        //! Qt timer
        QTimer _timer;

        QString _selectedUnit;

        bool _resetFlag;

        //! flag to indicate mark button is active
        bool _markingEnabled;

        //! drawed polygon
        CORE::RefPtr<CORE::IPolygon> _area;

        //!
        bool _enableDrag;
    };

} // namespace SMCQt
