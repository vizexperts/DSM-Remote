#ifndef QT_NETWORKJOINGUI_H_
#define QT_NETWORKJOINGUI_H_

/*****************************************************************************
 *
 * File             : NetworkJoinGUI.h
 * Version          : 1.0
 * Module           : Qt
 * Description      : NetworkJoinGUI class declaration
 * Author           : Udit Gangwani
 * Author email     : udit@vizexperts.com
 * Reference        : Qt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

// indigis3d related headers
#include <Core/IBaseUtils.h>

#include <VizQt/LayoutFileGUI.h>

#include <HLA/INetworkCollaborationManager.h>

#include <QListWidgetItem>


namespace SMCQt
{
    //! Add Trajectory GUI Dialog
    class NetworkJoinGUI 
        : public VizQt::LayoutFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        public:
            
            // Widget names
            static const std::string LWProjectList;
            static const std::string LEProjectName;
            static const std::string PBSearchProjects;
            static const std::string PBJoin;
            static const std::string PBCancel;

        public:

            NetworkJoinGUI();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
            
            //! Initializes the following attributes:-
            //!     # Value editbox name
            //!     # Units combobox name
            //!     # Calculationtype editbox name
            void initializeAttributes();

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

        private slots:

            //! Handling publish/cancel buttons
            void _handleJoinButtonClicked();
            void _handleCancelButtonClicked();
            void _handleSearchProjectsButtonClicked();
            void _handleItemClicked(QListWidgetItem*);

        protected:

            bool _saveProjectAndLoadEmptyWorld();
            bool _loadEmptyWorld();

            virtual ~NetworkJoinGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            void setSyncMode(const std::string &mode);
            std::string getSyncMode();

        protected:

            //! Boolean to indicate if projec thas been joined or not
            bool _joined;

            CORE::RefPtr<HLA::INetworkCollaborationManager> _netwrkCollaborationMgr;

            //! Name of the Joined Project
            std::string _joinedProjectNm;

            std::string _syncMode;
    };

} // namespace Qt

#endif  // QT_NETWORKJOINGUI_H_
