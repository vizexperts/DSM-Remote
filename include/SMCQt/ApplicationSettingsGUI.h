#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/ElementListTreeModel.h>
#include <SMCQt/QMLDirectoryTreeModel.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT ApplicationSettingsGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        static std::string _applicationSelectedJsonFile;
        QString file;


        ApplicationSettingsGUI();

        public slots :

        void JsonParsing();
        Q_INVOKABLE void connectSlots();
        Q_INVOKABLE void createComboBoxItem(QString name);
        Q_INVOKABLE void saveJsonFile();
    };
}