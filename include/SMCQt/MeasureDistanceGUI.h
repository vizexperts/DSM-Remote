#pragma once

/*****************************************************************************
*
* File             : MeasureDistanceGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : MeasureDistanceGUI class declaration
* Author           : Sumit Pandey
* Author email     : sumitp@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/ILineUIHandler.h>
#include <QtCore/QTimer>
#include <SMCUI/IDistanceCalculationUIHandler.h>
#include <Core/ISelectionComponent.h>
#include <Core/ILine.h>

namespace SMCQt
{
    // XXX - exported only for testing purpose
    class SMCQT_DLL_EXPORT MeasureDistanceGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        static const std::string DistanceAnalysisObjectName;

        MeasureDistanceGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void setActive(bool value);

            void markLine(bool value);
            void selectLine(bool value);
            void startCalculation();
            void stopCalculation();
            void changeUnit(QString unit);

            void handleDistanceUpdateTimer();
            void handleCalculationCompleted();

    protected:

        virtual ~MeasureDistanceGUI();

        virtual void _loadAndSubscribeSlots();

        //! Resets the calculated distance
        void _reset();

signals:

        void _calculationCompleted();

    protected:

        //! Area UI Handler
        CORE::RefPtr<SMCUI::ILineUIHandler> _lineHandler;

        //! SurfaceArea UI Handler
        CORE::RefPtr<SMCUI::IDistanceCalculationUIHandler> _distanceCalculationHandler;

        // selection component
        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

        //! Qt timer
        QTimer _timer;

        //! Temporary line object
        CORE::RefPtr<CORE::ILine> _line;

        QString _selectedUnit;

        //! 
        bool _resetFlag;

        //! 
        bool _markingEnabled;
    };

} // namespace SMCQt
