#pragma once

/*****************************************************************************
*
* File             : VectorLayerQueryGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : VectorLayerQueryGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <Core/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IVectorLayerQueryUIHandler.h>
#include <VizQt/QtUtils.h>
#include <SMCUI/IAreaUIHandler.h>
namespace SMCQt
{
    class VectorLayerQueryGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        static const std::string VECTORLAYERQUERYPOPUP;
        static const std::string VECTORQUERYBUILDEROBJECTNAME;
        //! Structure used to store Query Group Related Data
        struct VectorGroupedQuery
        {
            QString     _layerType;
            QString     _layerUniqueId;
            QString     _layer;
            QString     _whereClause; 
            QString     _queryName;
            QString     _style;
            //QString     _bottomLeftLong;
            //QString     _bottomLeftLat;
            //QString     _topRightLong;
            //QString     _topRightLat;
        };
        struct VectorGroupedQueryWithArea
        {
            QList< VectorGroupedQuery >  _vectorGroupedQueryStack;
            QString     _bottomLeftLong;
            QString     _bottomLeftLat;
            QString     _topRightLong;
            QString     _topRightLat;
        };

        VectorLayerQueryGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void _loadAndSubscribeSlots();
signals:


        public slots:

            void popupLoaded(QString type);
            void addGroupedQuery();
            void groupedQuerySelected(int index, bool addingNew = false, bool selectionOnly = true);
            void popupClosed();
            void updateGroupedQuery();


        private slots:
            //remvoe Query From List
            void _removeQueryFromList(int index);
            //save Query Group 
            void _handleSaveButtonClicked();
            //delete QueryGroup on ok click
            void _okSlotForDeleting();
            //delete query on ok click
            void _okSlotForQueryDeleting();
            //cancel button handling slot
            void _cancelSlotForDeleting();

            //delete querygroup while editing
            void _deleteQueryGroup(QString);
            //delete Query 
            void _removedQuery(QString);

            // decides whether to show parameters grid or not
            void _showParameterGrid(QString selectedValue);

            // creates new QueryGroup from existing saved ones
            void _editExistingQueryGroup(QString selectedValue);

            //Populate vector Layer list by geometryType
            void _populateVectorListByType(QString selectedValue);
            // creates new Query from existing saved ones
            void _editExistingQuery(QString);

            //for saving Query in json 
            void _queryBuiledrOkButtonClicked();
            
            //for Running Query Slot
            void _handleRunButtonClicked();
            //on close reset QueryEdit Button
            void _resetQueryEdit();
            // Slot for marking Area
            void _handlePBAreaClicked(bool pressed);
            void _vectorLayorPopupClose();
            //clear marked Area
            void _handleClearAreaButton();

    protected:

        virtual ~VectorLayerQueryGUI();
        void  _clearGroupedQueryStruct();
        //populate Existing Vector Style from Dsm style Folder and Set in Ui
        void _addVectorLayerStyleList();
        //Dump Query Group Data in JSON format
        void _writeQueryGroup(QString name);
        //Dump Query Data in JSON format
        void _writeQuery(QString name);
        // for populating query combobox
        void _readAllQuery();
        // for populating queryGroup combobox
        void _readAllQueryGroup();
        //return false if Query Name not Present Else populate and Fill data
        bool _getQuery(std::string QueryName);

        //return false if QueryGroup Name not Present Else populate and Fill data
        bool _getQueryGroup(std::string QueryGroupName);
        //! update grouped query name list in the UI
        void _populateQueryNameList();
        //check whether Query Name Exist in JSon folder or not
        bool _isQueryAlreadyPresent(std::string name);
        //check whether Query Group  Name Exist in JSon folder or not
        bool _isAlreadyPresent(std::string name);

        //for Running Query by Uihadnle
        void _executeQueryGroup();
        //Ui Hadnler for Handling Query Execution
        CORE::RefPtr<SMCUI::IVectorLayerQueryUIHandler> _vectorLayerQueryUIHandler;
        //Structure used to store Current Group Query Related Data
        VectorGroupedQueryWithArea _vectorGroupedQueryWithArea;
        //Structure used to store Current Query Related Data
        QString _currentQueryWhereClause;
        //List of String to Store Query Group  Name
        std::vector<std::string> _queryGroupList;
        //List of String to Store Query Name
        std::vector<std::string> _queryList;
        const std::string _queryStoragePath;
        const std::string _queryGroupStoragePath;

        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

    };
}
