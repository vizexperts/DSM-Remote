#ifndef SMCQT_PRESENTATIONGUI_H_
#define SMCQT_PRESENTATIONGUI_H_

/*****************************************************************************
*
* File             : PresentationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : PresentationGUI class declaration
* Author           : Somnath Singh
* Author email     : Somnath@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCElements/IPresentationComponent.h>

namespace SMCQt
{

    class SMCQT_DLL_EXPORT PresentationGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        PresentationGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);            

        void _loadAndSubscribeSlots();

        public slots:

            void connectPDFMenu();

            //void browsebuttonclicked(qurl fileurl);
            void browseButtonClicked();
            void uploadPdf(QString filePath);
            void openDocument(QString fileName);
            void showEncryptedPDFError();
            void showCorruptedPDFError();
            void confirmDocumentDelete(QString documentName);
            void deleteDocumentOkButtonPressed();
            void deleteDocumentCancelButtonPressed(){}
            void showPdfList();
            void browseForTextToSpeech();

    protected:

        //! Destructor
        virtual ~PresentationGUI();

        CORE::RefPtr<SMCElements::IPresentationComponent> _presentationComponent;

        QString _documentToDelete;

    };

} // namespace indiGUI

#endif  // SMCQT_ELEMENTLISTGUI_H_
