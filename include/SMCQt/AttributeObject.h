#ifndef SMCQT_ATTRIBUTEOBJECT_H_
#define SMCQT_ATTRIBUTEOBJECT_H_

/*****************************************************************************
*
* File             : AttributeObject.h
* Version          : 1.0
* Module           : SMCQt
* Description      : AttributeObject class declaration
* Author           : Mohan Singh
* Author email     : mohan@vizexperts.com
* Reference        : QObject derived class used for showing Object attributes
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <QObject>

namespace SMCQt
{

    class AttributeObject : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
            Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
            Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
            Q_PROPERTY(int precision READ precision WRITE setPrecision NOTIFY precisionChanged)

    public:

        AttributeObject(QObject* parent = 0);
        AttributeObject(const QString &name, const QString &type, int width = 0 ,int precesion = 0, QObject *parent = 0);

        QString name() const;
        void setName(const QString &name);

        QString type() const;
        void setType(const QString &type);

        int width() const;
        void setWidth(int width);

        int precision() const;
        void setPrecision(int precision);

signals:
        void nameChanged();
        void typeChanged();
        void widthChanged();
        void precisionChanged();

    private:
        QString m_name;
        QString m_type;
        int m_width;
        int m_precision;

    };

}

#endif //SMCQT_ATTRIBUTEOBJECT_H_