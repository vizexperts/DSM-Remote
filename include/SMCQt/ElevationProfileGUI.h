#ifndef SMCQT_ELEVATIONPROFILE_H_
#define SMCQT_ELEVATIONPROFILE_H_

/*****************************************************************************
*
* File             : ElevationProfileGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ElevationProfileGUI class declaration
* Author           : Rahul Srivastava
* Author email     : rahul@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <QtCore/QTimer>
#include <SMCUI/IElevationProfileUIHandler.h>
#include <Core/IPoint.h>

namespace SMCQt
{
    //! Elevation Profile GUI to check an elevation profile
    class SMCQT_DLL_EXPORT ElevationProfileGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT
    public:

        ElevationProfileGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);


        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            void connectMenu();
            void markLine(bool value);
            void selectLine(bool value);
            void startCalculation();
            void stopCalculation();
            void menuClosed();
            void showPointAtDistance(double distance);

signals:
            void _updateGraphDisplay();

            private slots:

                //! Updates the graph display area
                void _onUpdateGraphDisplay();

    protected:

        virtual ~ElevationProfileGUI();

        virtual void _loadAndSubscribeSlots();


        //! Creates current position point
        void _createCurrentPositionPoint();

    protected:

        //! Elevation profile ui handler instance
        CORE::RefPtr<SMCUI::IElevationProfileUIHandler> _elevationProfileHandler;


        //! Current position
        CORE::RefPtr<CORE::IPoint> _currentPosition;

        bool _markingEnabled;
    };

} // namespace SMCQt

#endif  // SMCQT_ELEVATIONPROFILE_H_
