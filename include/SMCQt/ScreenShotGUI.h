#ifndef QT_SCREENSHOTGUI_H_
#define QT_SCREENSHOTGUI_H_

/*****************************************************************************
*
* File             : ScreenshotGUI.h
* Version          : 1.0
* Module           : Qt
* Description      : ScreenshotGUI class declaration
* Author           : Rahul Srivastava
* Author email     : rahul@vizexperts.com
* Reference        : Qt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>

#include <osg/Camera>

namespace SMCQt
{
    // XXX - exported only for testing purpose
    class ScreenshotGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        ScreenshotGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void setActive(bool value);

        private slots:

            void _performCleanup();

            //XXX AG Private object members start from _(underscore)
            void connectPopupLoader(QString type);

            //! Handles browse button clicked
            void _handleBrowseButtonClicked();

            //! Handles capture button clicked
            void _handleCaptureButtonClicked();

    protected:


        virtual ~ScreenshotGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! Camera screen capture callback
        osg::ref_ptr<osg::Camera::DrawCallback> _captureCallback;

    };

} // namespace indiGUI

#endif  // QT_SCREENSHOTGUI_H_
