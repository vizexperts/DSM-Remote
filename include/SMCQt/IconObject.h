#ifndef SMCQT_ICONOBJECT_H_
#define SMCQT_ICONOBJECT_H_

/*****************************************************************************
*
* File             : IconObject.h
* Version          : 1.0
* Module           : SMCQt
* Description      : IconObject class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : QObject derived class to hold a name and an icon property
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <QObject>

namespace SMCQt
{

    class IconObject : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
            Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
            Q_PROPERTY(bool audioPresent READ audioPresent WRITE setAudioPresent NOTIFY audioPresentChanged)
            Q_PROPERTY(bool micEnable READ micEnable WRITE setMicEnable NOTIFY micEnableChanged)

    public: 
        IconObject(QObject* parent = 0);
        IconObject(const QString &name, const QString &path, QObject *parent = 0);

        // QObject holding 4 properties i.e. name , path , audioPresent and micEnable
        IconObject(const QString &name, const QString &path, bool audioPresent,bool micEnable, QObject *parent = 0);

        QString name() const;
        void setName(const QString &name);

        QString path() const;
        void setPath(const QString &path);

        bool audioPresent() const;
        void setAudioPresent(bool present);

        bool micEnable() const;
        void setMicEnable(bool present);

    signals:
        void nameChanged();
        void pathChanged();
        void audioPresentChanged();
        void micEnableChanged();

    private:
        QString m_name;
        QString m_path;
        bool m_audioPresent;
        bool m_micEnable;

    };
}

#endif //SMCQT_ICONOBJECT_H_