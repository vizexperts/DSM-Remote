#ifndef SMCQT_VECTORTABLEMODEL_H_
#define SMCQT_VECTORTABLEMODEL_H_

/*****************************************************************************
 *
 * File             : VectorTableModel.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : VectorTableModel class declaration
 * Author           : dharmendra
 * Author email     : dharmendra@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2015-2016,                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <QtCore/QAbstractTableModel>
#include <Core/IObject.h>
#include <GIS/IFeatureLayer.h>
#include <map>
#include <GIS/IFeature.h>
#include<Terrain/IModelObject.h>
#include <ogrsf_frmts.h>
namespace SMCQt
{
    class VectorTableModel : public QAbstractTableModel 
    {
         Q_OBJECT
        public:

            //! Constructor
            
            VectorTableModel(QObject* parent, std::string url);

            QHash<int,QByteArray> roleNames() const;

            //! get the row count
            int rowCount(const QModelIndex &parent = QModelIndex()) const  ;

            //! get the data at the given index
            QVariant data(const QModelIndex &index, int role) const;
            //! get the column Index by Name
            Q_INVOKABLE int getColumnNumber(QString value) const;
            //! get the column DataType by Name
            Q_INVOKABLE QString getColumnType(QString value) const;
            //! get the column count
            int columnCount(const QModelIndex& index) const;
            //! set Column data
            Q_INVOKABLE bool setData(int row,int column,QString value);
            //! get Column data
            Q_INVOKABLE QString  getData(int row,int column);
            //! item flags
            Qt::ItemFlags flags(const QModelIndex& index) const;
            //bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
            void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);
            //! Destructor
            ~VectorTableModel();
            //return OGR Layer from Memory where changed is stored
            OGRLayer* getOGRUpdateLayer();
            void closePopup();

           

        protected:

            //! OGR Layer for storage runTime Change in Feature 
            OGRLayer  *_inMemoryOGRLayer;
            
            typedef std::map<std::string, int> RoleMap;
            RoleMap _roleMap;

            std::vector<std::string> _roleNamesVector;

            QHash<int,QByteArray> _roleHash;
            //Data source pointing to In memory OGR Layer
            OGRDataSource *_ogrDSForMemory;

    };
}
#endif

