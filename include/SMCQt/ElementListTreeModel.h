#pragma once

/*****************************************************************************
*
* File             : ElementListTreeModel.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ElementListTreeModel class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/QMLTreeModel.h>
#include <Core/IObject.h>
#include <Core/IBase.h>
#include <Core/UniqueID.h>

namespace SMCQt
{
    class ElementListTreeModelItem: public QMLTreeModelItem
    {
    public:

        ElementListTreeModelItem() 
            : _object(NULL)
            , _iconPath("")
            , _changingCheckState(false)
        {
        }

        ElementListTreeModelItem(CORE::IObject* object, QString iconPath = "")
            : _object(object)
            , _iconPath(iconPath)
            , _changingCheckState(false)
        {
        }

        ~ElementListTreeModelItem()
        {
        }

        const CORE::IObject* getObject() const;

        void setObject(const CORE::IObject* object);

        virtual QString getName() const;

        void setName(QString name);

        QString getSubName() const;

        void setSubName(QString subName);

        void setIcon(QString icon);

        QString getIcon() const;

        bool changingCheckStatus() const;

        void setChangingCheckStatus(bool value);

        Qt::CheckState getCheckState();

    private:

        QString _iconPath;


        QString _subName;

        bool _changingCheckState;
    protected:
        QString _name;
        osg::observer_ptr<const CORE::IObject> _object;


    };


    class ElementListTreeModel: public QMLTreeModel
    {
        Q_OBJECT
    public: 

        QVariant data(const QModelIndex & Index, int role = Qt::DisplayRole)const;

        QHash<int,QByteArray> roleNames() const;

        bool customizeTree(QString searchString, QMLTreeModelItem* item, QMLTreeModelItem* parent);

        Q_INVOKABLE QString getItemName(int index) const;

        public slots:
            void toggleManualCheckedState(int numIndex);

            void toggleCheckedState(int numIndex);

            void move(int numIndex, int parentIndex);

signals:
            void itemChanged(int itemIndex);
            void itemChangedSendNotify(CORE::RefPtr<const CORE::IObject>);

    private:

        void setCheckState(int numIndex, Qt::CheckState state);

        void setCheckState(QMLTreeModelItem* item, Qt::CheckState state);

        void updateCheckStatus(int numIndex);

    protected:

        enum ElementListModelRoles
        {
            NameRole = QMLTreeModel::UserRole + 1,
            IconRole,
            UserRole
        };
    };

}   // SMCQt
