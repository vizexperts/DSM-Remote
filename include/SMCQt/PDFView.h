#ifndef PDFVIEW_H
#define PDFVIEW_H

#include <QtQuick/QQuickPaintedItem>
#include <QtGui/QImage>

namespace Poppler{
    class Document;
}
namespace SMCQt{

    class PDFView : public QQuickPaintedItem
    {
        Q_OBJECT
            Q_PROPERTY(int currentPage READ currentPage WRITE setCurrentPage /*NOTIFY currentPageChanged*/)
            Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
            Q_PROPERTY(qreal zoom READ zoom WRITE setZoom NOTIFY zoomChanged)
            Q_PROPERTY(QSize pageSize READ pageSize NOTIFY pageSizeChanged)
            Q_PROPERTY(int numPages READ numPages NOTIFY numPagesChanged)

    public:
        PDFView(QQuickItem *parent = 0);
        ~PDFView();

           QString source() const;
        void setSource(const QString& source);
        
        int currentPage() const;
        void setCurrentPage(int currPage);
        
        qreal zoom() const;
        void setZoom(qreal zoom);

        QSize pageSize() const;

        int numPages() const;

        void loadPDF();
        void updatePosition();

        Q_INVOKABLE void saveToFile(const QUrl& path);

    protected:
        virtual void paint(QPainter *);

    signals:

        void sourceChanged();
        void currentPageChanged();
        void pageSizeChanged();
        void zoomChanged();
        void numPagesChanged();
        void encryptedPDFError();
        void corruptedPDFError();

        public slots:

        void loadPage();

    private:
        Poppler::Document* docPDF;

        // current page
        QImage page;

        QString _source;

        int _currentPage;

        int posX, posY;
        int phX, phY;

        qreal _zoom;

        int _numPages;

        QSize _pageSize;

    };
}

//QML_DECLARE_TYPE(SMCQt::PDFView)

#endif // PDFVIEW_H

