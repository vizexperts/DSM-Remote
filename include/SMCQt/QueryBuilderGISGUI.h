#pragma once

/*****************************************************************************
*
* File             : QueryBuilderGISGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : QueryBuilderGISGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
#include <SMCUI/IDatabaseLayerLoaderUIHandler.h>
#include <SMCElements/IIncidentLoaderComponent.h>

#include <SMCQt/LayerTableModel.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT QueryBuilderGISGUI:public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:
        //! Constructor
        QueryBuilderGISGUI();

        //! 
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        std::string _getDatabaseColumnName(int index);

        public slots:

            //void setActive(bool active);
            void connectPopupLoader(QString);

            private slots:

                void _handlePBAreaClicked(bool pressed);
                void _handleOkButtonClicked();
                void _handleClearButtonClicked();
                void _handleTableOkButtonClicked();

                void _handleAddToWhereButtonClicked(int index);

                void _handleOperatorButtonClicked(QString buttonName);

                void _populateFiledDefinition();
                void _queryBuilderClosed();

                void showSampleValues(int);
                void showAllValues(int);
                void addValueToWhere(int);
                void changeText(const QString& leToSearch);


    protected: 

        virtual ~QueryBuilderGISGUI();

        //! Loads the qml file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        CORE::RefPtr<SMCUI::IDatabaseLayerLoaderUIHandler>  _databaseLayerLoaderUIHandler;

        CORE::RefPtr<SMCElements::IIncidentLoaderComponent> _incidentLoaderComponent;

        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

        // list containing all fields
        QStringList _allFieldDefinitions;

        // list containing only selected fields
        QStringList _currentFieldValues;

        std::string _currentUserspaceName;
        std::string _currentTableName;
        std::string _currentIncidentName;

        std::string _currentUIType;
        
        //used for searching the values
        std::string _toSearch;

        // creation of model
        LayerTableModel* _tableModel; 
    };
}
