#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT UserManualGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

        public slots :
        Q_INVOKABLE void openUserManual();
    };
}