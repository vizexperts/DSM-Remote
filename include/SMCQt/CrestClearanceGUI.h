/*****************************************************************************
*
* File             : CrestClearanceGUI.h
* Version          : 1.0
* Module           : indiQt
* Description      : CrestClearanceGUI class declaration
* Author           : Rahul Srivastava 
* Author email     : rahul@vizexperts.com
* Edited by        : Vikram singh ( vikram@vizexperts.com )
* Reference        : indiQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <VizUI/IPointUIHandler.h>
#include <SMCUI/ICrestClearanceUIHandler.h>
#include <Core/IObject.h>
#include <Elements/IIcon.h>

namespace SMCQt
{
    //! Crest clearance GUI
    class CrestClearanceGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        // crest clearance icon file path
        static const std::string LaunchIconPath;
        static const std::string DirIconPath;

        //! GUI object name
        static const std::string CrestClearanceMenuObject;

    public:

        CrestClearanceGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                //! Gets the point from PointUIHandler and sets the relevant GUI
                void _processPoint();
                // Slots for event handling

                void _handleVACStartButtonClicked();
                void _handleVACStopButtonClicked();
                void _handlePPCStartButtonClicked();
                void _handlePPCStopButtonClicked();
                void _handlePPCLPMarkButtonClicked(bool);
                void _handlePPCDirMarkButtonClicked(bool);
                void _handleVACLPMarkButtonClicked(bool);
                void _handleVACTPMarkButtonClicked(bool);
                void changeToDefaultStateSlot();

    protected:

        virtual ~CrestClearanceGUI();

        //! Removes the object from the world
        void _removeObject(CORE::IObject* obj);

        //! Sets target point
        void _setTargetPoint(CORE::IPoint* point);

        //! Sets launch point
        void _setLaunchPoint(CORE::IPoint* point);

        // this function will activate the mouse events and set the point to create events when clicked 
        void _setPointHandlerEnabled(bool pressed);

        signals :
                void changeToDefaultStateSignal();

    protected:

        // different states in which the crest clearance gui will be
        // these states are used in function processPoint() to populate the corresponding textfields
        enum PointSelectionState
        {
            NO_MARK,
            LP_MARK_PPC,
            DIR_MARK_PPC,
            LP_MARK_VAC,
            TP_MARK_VAC,
        };

        //! Transmitter/Receiver Icon Pointer
        CORE::RefPtr<ELEMENTS::IIcon> _launchIcon;
        CORE::RefPtr<ELEMENTS::IIcon> _dirIcon;

        //! Point UI Handler
        CORE::RefPtr<VizUI::IPointUIHandler> _pointHandler;

        //! Crest Clearance UI Handler
        CORE::RefPtr<SMCUI::ICrestClearanceUIHandler> _crestClearanceHandler;

        //! Point selection state
        PointSelectionState _state;

        //! target position
        osg::Vec3d _targetPos;

        //! Launch point
        CORE::RefPtr<CORE::IObject> _lpPoint;

        //! Target/Direction point
        CORE::RefPtr<CORE::IObject> _tgtPoint;

        // qml object for crestClearance file
        QObject* _crestClearanceMenuObject;

        bool _crestAdd;
    };

} // namespace indiQt
