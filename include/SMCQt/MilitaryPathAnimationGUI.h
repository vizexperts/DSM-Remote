#pragma once

/*****************************************************************************
*
* File             : MilitaryPathAnimationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : MilitaryPathAnimationGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IMilitaryPathAnimationUIHandler.h>
#include <SMCUI/IAnimationUIHandler.h>
#include <SMCUI/IDraggerUIHandler.h>
#include <Elements/ICheckpoint.h>
#include <osg/PositionAttitudeTransform>

#include <QtCore/QDateTime>

#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/util/XMLString.hpp>

#include <xercesc/dom/DOMNamedNodeMap.hpp>

namespace SMCQt
{
    // QML Checkpint Object

    class GPXTrackPoints;
    class GPXTrackSeg;
    class GPXTrack;

    typedef std::map<std::string, std::map<std::string, GPXTrackSeg>> MapofGpx;
    typedef std::map<std::string, GPXTrackSeg> MapofTrkSegment;
    typedef std::list <GPXTrackPoints> ListfTrackPoints;
    typedef ListfTrackPoints VecofRoutes;
    typedef std::map<std::string, GPXTrack> MapofTracks;
    typedef std::map<std::string, ListfTrackPoints> MapofWaypoint;
    typedef std::map<std::string, VecofRoutes> MapofRoutes;

    



    enum GPX_TAG_TYPE
    {
        GPX_TAG_TRACK = 0,
        GPX_TAG_ROUTE,
        GPX_TAG_WAYPOINT,
        GPX_TAG_COUNT
    };

    class GPXTrackPoints
    {


    public:
        GPXTrackPoints(const double& elevation, const std::string& time, double lon, double lat, GPX_TAG_TYPE tagType = GPX_TAG_TRACK)
            :_elevation(elevation), _time(time), _lon(lon), _lat(lat), _tagType(tagType)
        {

        }
        GPXTrackPoints() :_elevation(-1.0), _time(""), _lon(0.0), _lat(0.0), _tagType(GPX_TAG_TRACK)
        {

        }
        virtual ~GPXTrackPoints(){}

        //! Set/Get function for longitude.
        void setLongitude(const double & lon){ _lon = lon; }
        double getLongitude() const { return _lon; }

        //! Set/Get function for latitude.
        void setLatitude(const double & lat){ _lat = lat; }
        double getLatitude() const{ return _lat; }

        //! Set/Get function for Elevation.
        void setElevation(const double & elevation){ _elevation = elevation; }
        double getElevation() const{ return _elevation; }

        //! Set/Get function for TimeStamp.
        void setTime(const std::string & time){ _time = time; }
        const std::string getTime() const{ return _time; }

        //! Set/Get function for set Boost Time Stamp.
        void setBoostTime(const boost::posix_time::ptime & time){ _bstTime = time; }
        const boost::posix_time::ptime getBoostTime() const{ return _bstTime; }

        //! Set/Get function for set GPX Tag Type
        void setTagType(const GPX_TAG_TYPE & tagType){ _tagType = tagType; }
        GPX_TAG_TYPE getTagType() const{ return _tagType; }

    protected:
        double _lon, _lat;
        double _elevation;
        std::string _time;
        boost::posix_time::ptime _bstTime;

        GPX_TAG_TYPE _tagType;
    };
    class GPXTrackSeg
    {
    public:
        GPXTrackSeg(){}
        ~GPXTrackSeg(){}
    
    protected:
        ListfTrackPoints _vecTrkPt;

    public:

		//! set/get track pointer info vector.
        void setTrakPtInfoVector(const ListfTrackPoints& trkInfo){ _vecTrkPt = trkInfo; };
        ListfTrackPoints& getTrakPointList(){ return _vecTrkPt; };

    };
    class GPXTrack
    {
    public:
        GPXTrack(){}
        ~GPXTrack(){}
   
    protected:
        MapofTrkSegment _trackSegments;
    public:
		
		//! set/get track pointer info vector.
        void setTrakPtInfoVector(const MapofTrkSegment& trackSegments){ _trackSegments = trackSegments; };
        MapofTrkSegment& getTrackSegmentMap(){ return _trackSegments; };

    };
    class GPXData
    {
    public:
        GPXData(){}
        ~GPXData(){}

    protected:
        MapofTracks _tracks;
        MapofWaypoint _waypoints;
        MapofRoutes _routes;
    public:

        MapofTracks & getGPXTrackMap(){ return _tracks; }
        MapofWaypoint & getGPXWaypointsMap(){ return _waypoints; }
        MapofRoutes & getGPXRoutesMap(){ return _routes; }
    };

    class MilitaryPathAnimationGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:
        const char* CHECKPOINT_MANUAL = "Manual checkpoints";
        const char* CHECKPOINT_ATTACHFILE = "Attach Timestamp File";

        static  const CORE::RefPtr<CORE::IMessageType> GPXXMLParseComplete;

        static const std::string EventContextualMenuObjectName;

        MilitaryPathAnimationGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void setActive(bool active);
            void attachPath(bool active);

            // slot to set the Axis
            void setAxis(QString facing,QString normal);

            // Asks question weather to remove path
            void removePath();

            void showGhostAt(double position);
            void addCheckpoint(double position, QDateTime qArrivalDateTime, QDateTime qDepartureDateTime, QString animationType);
            void selectCheckpoint(QString cpID);
            void tabSelected(QString name);

            void editCheckpoint(QString cpID, double position, QDateTime arrivalDateTime,
                QDateTime departureDateTime, QString animationType);

            void deleteCheckpoint(QString cpID);

            void connectContextualMenu();
            void disconnectContextualMenu();

            // handle remove path question Ok button clicked
            void handleRemovePathOkClicked();
            // dummy function to do nothing on cancel
            void handleRemovePathCancelClicked(){};

            void setMode(int mode);

            void updateAxis(osg::Vec3d point);

            void _reset();

            //! set the trail visibility
            void setTrailVisibility(bool value);

            void setEnableAxis(bool value);

            // change the osg::Vec3d to the axis
            std::string getDirection(osg::Vec3d value);

            // select animation type 
            void selectAnimationType(QString animationType);
            

	        // This function is called when their is choice of selection between either loading checkpoint file OR
	        // Manual checkpoint.
	        void selectTimestampingMethod(QString selectedOption);


	        // Selecting GPX type from parsed list i.e Tracks,Waypoints,routes.
	        void selectGPXType(QString selectedOption);
        
	        // Selecting GPX Track segment.
	        void selectGPXTrackSegment(QString selectedOption);

	        // Selecting GPX Routes
	        void selectGPXRoutes(QString selectedOption);



	        //!
	        //! Adjustment of start and end time for event choice.
	        void adjustStartandEndTime();
	        void okAdjustmentofTime();
	        void cancelAdjustmentofTime();


    protected:

        virtual ~MilitaryPathAnimationGUI();

        virtual void _loadAndSubscribeSlots();

        void _populateCheckpointModel();

        // check if new checkpoint bounds go over mission star/end time and edit them accordingly 
        void _checkMissionStartEndTimeWith(QDateTime qArrivalDateTime, QDateTime qDepartureDateTime);

        //! Populate animated type list
        void _populateAnimatedList();


        void _parseAndAttachTimestmapNode();

        void _manualCheckpoints();

        //! Parse Xml Info structure
        void _parseXmlInfo(const std::string& fileName);
        
        //! Parse GpxTracks
        void _parseGPXTrack(xercesc::DOMNode* elementRoot, MapofTrkSegment& segmentStruct);

        //! Parse Gpx Node Info
        void _parseGPXNodeInfoTrack(xercesc::DOMNode* node, ListfTrackPoints &trackpoints);
        
        //! Parse Gpx Route Info
        void _parseGPXRoute(xercesc::DOMNode* node, ListfTrackPoints &trackpoints);
        
        //! Select Track Segment
        void _selectTrackSegment(QString selectedOption);
        
        //! Select Route Points
        void _selectRoutePoints(QString selectedOption);

        //! Select Waypoint Segment
        void _selectWaypointSegment(QString selectedOption);
		
		//! Removing milli second part from timestamp.
		void _trimMillisecondPart(std::string &trimString)  const;


    protected:
        CORE::RefPtr<SMCUI::IMilitaryPathAnimationUIHandler> _uiHandler;

        SMCUI::IMilitaryPathAnimationUIHandler::AnimationMode _mode;

        CORE::RefPtr<ELEMENTS::ICheckpoint> _cp;

        CORE::RefPtr<SMCUI::IAnimationUIHandler> _animationUIHandler;

        // group node to handle all the geodes.
        CORE::RefPtr<osg::PositionAttitudeTransform> patNode;

        // selected axis
        std::string selectedNormalAxis, selectedFacingAxis;

        // Current selected file.
        std::string _currentFile;
        
        QString _selectedGpxType;

        MapofTrkSegment* _selectedGPXNodeType;

        // This pointer to point to selected track segment so when we select a segment we don't have to resiterator whole tracksegment again.
        ListfTrackPoints* _selectedGPXNodeSegment;

        GPXData _gpxData;


        //! variable denoting whether manual check point is selected or not
        bool _bAttachedCheckpointsFromFile;

        //! Saved start time and end time
        boost::posix_time::ptime _startTime, _endTime;

        //! stages backup from EventContexualUI
        int _stages;

    };
}   // namespace SMCQt
