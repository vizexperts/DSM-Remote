#pragma once
/*****************************************************************************
*
* File             : VisibilityControllerGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : VisibilityControllerGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCUI/IVisibilityControllerUIHandler.h>

#include <QtCore/QDateTime>

namespace SMCQt
{
    class VisibilityControllerGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string EventContextualMenuObjectName;

        VisibilityControllerGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:
            void connectContextualMenu();
            void disconnectContextualMenu();

            void selectControlPoint(QString cpID);
            void addControlPoint(QDateTime cpTime, bool cpVisibilityState);
            void editControlPoint(QString cpID, QDateTime cpTime, bool cpVisibilityState);
            void deleteControlPoint(QString cpID);
            void tabSelected(QString name);

    protected:

        void _populateControlPoints();

        virtual ~VisibilityControllerGUI();

        CORE::RefPtr<SMCUI::IVisibilityControllerUIHandler> _vcUIHandler;
    };
}   // namespace SMCQt
