#pragma once
/*****************************************************************************
*
* File             : SensorRangeAnalysisGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : SensorRangeAnalysisGUI class declaration
* Author           : Kumar Saurabh Arora
* Author email     : saurabh@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/WorldMaintainer.h>
#include <VizQt/LayoutFileGUI.h>
#include <SMCUI/IPointUIHandler2.h>
#include <SMCUI/IAreaUIHandler.h>
#include <osg/Vec3d>
#include <osg/Vec4d>
#include <Core/IBaseVisitor.h>
#include <Core/IObject.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Elements/IMapSheetInfoComponent.h>

namespace SMCQt
{
    // Point layer properties GUI
    class SensorRangeAnalysisGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        static const std::string SensorRangeAnalysisObjectName;

        SensorRangeAnalysisGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:
            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

signals:
            void _updateSuccessStatus();

            private slots:
                void _handleStartButtonClicked();
                void _handleStopButtonClicked();
                void _handleAddButtonClicked();
                void _handleRemoveButtonClicked(int);
                void _handlePBmarkPositionClicked(bool);
                void _handleSuccessStatus();
    protected:
        //@{
        /** play and stop Path computation*/
        bool play();
        bool stop();
        //@}

        void _handleSensorRangeAnalysisButtonPressed(const std::string& playEvent);
        void _addAttributeToTable(const QString& leName, const QColor& rectColor, double leLong, double leLat, double leHeight, double leRange );

    protected:

        virtual ~SensorRangeAnalysisGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! Sets current point
        void _setCurrentPoint(CORE::IPoint* point);

        void _reset();

        //! SensorRange filter
        typedef std::map<std::string, CORE::RefPtr<CORE::IBaseVisitor> > VisitorList;
        VisitorList _visitorList;

        //! point UI Handler
        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointHandler;

        //! mapshhet info component for mapsheet gr to lat long
        CORE::RefPtr<ELEMENTS::IMapSheetInfoComponent> _mapSheetInfoComponent;

        ObserverList _observerList;

        //! Vector of objects
        std::map<std::string, CORE::RefPtr<CORE::IObject> > _objectList;

        //! Current point
        CORE::RefPtr<CORE::IObject> _currentPoint;

        //XXX Comments missing

        //XXX class protected/private data/function members should start from _(underscore)
        int noOfObjects;
        QList<QObject *> _twObserverList;
    };

} // namespace SMCQt
