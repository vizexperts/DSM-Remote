
#include <Core/IBaseUtils.h>

#include <VizQt/LayoutFileGUI.h>

#include <Elements/IColorMapComponent.h>
#include <Elements/IColorMap.h>

#include <SMCUI/IColorMapUIHandler.h>

#include <SMCQt/DeclarativeFileGUI.h>


namespace SMCQt
{
    //QML Color Object
    class ColorObject : public QObject
    {
        Q_OBJECT
            Q_PROPERTY(double  minVal READ  minVal WRITE setMinValue NOTIFY minValueChanged)
            Q_PROPERTY(double  maxVal READ  maxVal WRITE setMaxValue NOTIFY maxValueChanged)
            Q_PROPERTY(QColor  cellColor  READ  cellColor  WRITE setcellColor   NOTIFY  cellColorChanged)

    public:
        ColorObject(QObject* parent = 0);
        ColorObject(double minVal, double maxVal, QColor cellColor, QObject *parent = 0);

        double minVal() const;
        void setMinValue(double minVal);

        double maxVal() const;
        void setMaxValue(double maxVal);

        QColor cellColor()const;
        void setcellColor(QColor cellColor);

    signals:
        void minValueChanged();
        void maxValueChanged();
        void cellColorChanged();

    private:
        double min_Value;
        double max_Value;
        QColor cell_Color;

    };
    // Creates a new layer
    class ColorPaletteGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:
        static const std::string ColorMap;

    public:

        ColorPaletteGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

        //@{
        /** Set/Get active function */
        void setActive(bool active);
        //@}

    signals:

        void newColorMapAdded(const QString&);

        private slots:

        // Slots for event handling
        void _handleDefaultColorButtonClicked(QColor);
        void _handleCBTemplatesIndexChanged();
        void _handleGenerateButtonClicked();
        void _handleInsertRowButtonClicked();
        void _handleDeleteRowButtonClicked();
        void _handleOkButtonClicked();
        void _handleCancelButtonClicked();
        void _handleClampChecked(bool);
        void deleteColorMap();

    protected:

        virtual ~ColorPaletteGUI();

        void _populateColorMap();

        void _resetColorTable();

        void _resetButton();

        void _fillColorTable(ELEMENTS::IColorMap* colorMap);

    protected:

        //! Color component
        CORE::RefPtr<ELEMENTS::IColorMapComponent> _colorMapComponent;

        //! ColorMap UI handler
        CORE::RefPtr<SMCUI::IColorMapUIHandler> _uiHandler;

        //! current color map
        CORE::RefPtr<ELEMENTS::IColorMap> _colorMap;

        //! current default color
        osg::Vec4 _defaultColor;
        //!current colorList
        QList<QObject*> _colorList;
        //!current templateList
        QList<QObject*> _templateColorMapList;

        bool _isClamp;
        double _minSize;
        double _maxSize;
        double _mapSize;
        int _alphaValue;
    };

} // namespace SMCQt
