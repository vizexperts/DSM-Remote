#pragma once

 #include<QAbstractListModel>
#include <SMCQt/QMLTreeModel.h>
#include <QDir>

 namespace SMCQt
 {
     class QMLDirectoryTreeModelItem : public QMLTreeModelItem
     {
     public:
         QMLDirectoryTreeModelItem(QFileInfo fileInfo = QFileInfo())
             : _fileInfo(fileInfo)
         {}

         QFileInfo getFileInfo() const
         {
             return _fileInfo;
         }

     private:

         QFileInfo _fileInfo;
     };

     class QMLDirectoryTreeModel : public QMLTreeModel
     {
         Q_OBJECT

     public:

         QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

         QHash<int, QByteArray> roleNames() const;

         bool customizeTree(QString searchString, QMLTreeModelItem* item, QMLTreeModelItem* parent);

         //! Default is false
         void showExtensions(bool value){ _showExtensions = value; }

         //QDir::Filters filter() const;
         //void setFilter(QDir::Filters filters);

         //QDir::SortFlags sorting() const;
         //void setSorting(QDir::SortFlags sortFlags);

         //QStringList nameFilters() const;
         //void setNameFilters(const QStringList &nameFilters);

         //void intialize(QDir directory);

     private:
         enum TreeItemRoles
         {
             NameRole = QMLTreeModel::UserRole + 1,
             IsDirRole
         };

         bool _showExtensions = false;

         //void _populateDir(QDir dir, QMLDirectoryTreeModelItem* parentItem, bool fillCategories);

         //QDir _directory;
     };

 }   // namespace SMCQt
