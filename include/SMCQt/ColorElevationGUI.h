/*****************************************************************************
*
* File             : ColorElevationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ColorElevationGUI class declaration
* Author           : Naman Gupta
* Author email     : naman@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>

#include <SMCUI/IColorElevationUIHandler.h>
#include <SMCUI/IColorMapUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
#include <Elements/IColorMap.h>
#include <Elements/IColorMapComponent.h>

namespace SMCQt
{
    // Creates a new layer
    class ColorElevationGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        QObject* ColorElevationObject;
    public:

        static const std::string ColorEleAnalysisPopup;

        ColorElevationGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void  _setProgressValue(int);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void _handlePBStartClicked();
                void _handlePBStopClicked();
                void _handlePBRunInBackgroundClicked();
                void  _handleProgressValueChanged(int);
                void _hadleNewColorMapAdded(const QString&);
                void _handlePBAreaClicked(bool pressed);
                void _reset();

    protected:

        virtual ~ColorElevationGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        void _populateColorMap();

    protected:

        //! Height Above UIHandler
        CORE::RefPtr<SMCUI::IColorElevationUIHandler> _uiHandler;

        //! ColorMap UI Handler
        CORE::RefPtr<SMCUI::IColorMapUIHandler> _colorUIHandler;

        CORE::RefPtr<ELEMENTS::IColorMapComponent> _colorMapComponent;


        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;
        
    };

} // namespace SMCQt
