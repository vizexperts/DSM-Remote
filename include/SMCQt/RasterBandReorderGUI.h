#ifndef SMCQT_RASTERBANDREORDER_H_
#define SMCQT_RASTERBANDREORDER_H_

/*****************************************************************************
*
* File             : CreateOpacitySliderGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateLineGUI class declaration
* Author           : vikram singh
* Author email     : vikram@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <App/AccessElementUtils.h>

#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>


#include <Terrain/IRasterObject.h>
#include <Core/ISelectionComponent.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT RasterBandReorderGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        RasterBandReorderGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void connectSmpMenu(QString type);
            void handleApplyButton();
            void handleCancelButton();

    protected:

        virtual ~RasterBandReorderGUI();

        virtual void _loadAndSubscribeSlots();

        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

    };

} // namespace SMCQt

#endif  // SMCQT_CREATEOPACITYSLIDER_H_
