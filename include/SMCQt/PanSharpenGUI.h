#pragma once
/*****************************************************************************
*
* File             : PanSharpenGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : PanSharpen class declaration
* Author           : Sandeep
* Author email     : sandeep@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : code inspector's name
* Inspection date  : YYYY-MM-DD
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <Stereo/export.h>
#include <Core/IBaseUtils.h>
#include <Core/IBaseVisitor.h>
#include <Core/IObject.h>

#include <VizQt/QtGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAddContentUIHandler.h>
#include <VizUI/UIHandler.h>
#include <GISCompute/IPanSharpenVisitor.h>
#include <Stereo/StereoViewerGUI.h>
#include <Stereo/StereoSceneGenerator.h>
#include <Stereo/MainWindowGUI.h>
#include <Stereo/Image.h>

#ifdef USE_QT4
#include <QtGui/QMainWindow>
#else
#include <QtWidgets/QMainWindow>
#endif

#include <QtDeclarative/QDeclarativeView>


namespace SMCQt
{
    class PanSharpenGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED
        Q_OBJECT

    public:

        static const std::string PanSharpenGUIObjectName;

        PanSharpenGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

    private:

        //Last Path History
        std::string _lastPath;

        public slots:

            /** Set/Get active function */ 
            void setActive(bool active);

            void closeStereoWizard(Stereo::MainWindowGUI*);

signals:

            //! signal emmited when a filteroperation is completed
            void _updateGUIStatus();

            private slots:

                //! Slots for event handling
                void _handleBrowsePanImage();

                void _handleBrowseMSImage();

                void _handleBrowseBand1Image();

                void _handleBrowseBand2Image();

                void _handleBrowseBand3Image();

                void _handleBrowseOutputImage();

                void _viewImage(QString);
                void _showMSImage();

                void _fuseImages(QString, QString, QString, QString, QString);        

                void _handleStopButtonClicked();
                void _handleGUIStatus();
                void _reset();


    protected:

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        virtual ~PanSharpenGUI();


        void _loadAddContentUIHandler();

        //! UIHandler for loading data
        CORE::RefPtr<SMCUI::IAddContentUIHandler> _addContentUIHandler;

        //! PanSharpen Visitor
        CORE::RefPtr<GISCOMPUTE::IPanSharpenVisitor> _panVisitor;

        CORE::RefPtr<CORE::IBaseVisitor> _visitor;

        std::vector<Stereo::MainWindowGUI*> _stereoWizard;

    };
}// namespace SMCQt
