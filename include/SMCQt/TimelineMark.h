#ifndef SMCQT_TIMELINEMARKERELEMENT_H_
#define SMCQT_TIMELINEMARKERELEMENT_H_

/*****************************************************************************
*
* File             : TimelineMarkerElement.h
* Version          : 1.0
* Module           : SMCQt
* Description      : TimelineMarkerElement class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : QObject derived class used to show Timeline markers
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <QObject>
#include <QColor>

namespace SMCQt
{

    class TimelineMovementMark : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(double posX READ posX WRITE setPosX NOTIFY posXChanged)
            Q_PROPERTY(QColor profileColor READ profileColor WRITE setProfileColor NOTIFY profileColorChanged)
            Q_PROPERTY(double deltaX READ deltaX WRITE setDeltaX NOTIFY deltaXChanged)
            Q_PROPERTY(double distance READ distance WRITE setDistance NOTIFY distanceChanged)

    public: 
        TimelineMovementMark(QObject* parent = 0);
        TimelineMovementMark( double posX, QColor profileColor, double deltaX, double distance, QObject *parent = 0);

        double posX() const;
        void setPosX(double posX);

        QColor profileColor() const;
        void setProfileColor(QColor profileCOlor);

        double deltaX() const;
        void setDeltaX(double deltaX);

        double distance() const;
        void setDistance(double distance);
signals:
        void posXChanged();
        void profileColorChanged();
        void deltaXChanged();
        void distanceChanged();

    private:
        double m_posX;
        QColor m_profileColor;
        double m_deltaX;
        double m_distance;
    };

    class TimelineVisibilityMark : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(double posX READ posX WRITE setPosX NOTIFY posXChanged)
            Q_PROPERTY(QColor profileColor READ profileColor WRITE setProfileColor NOTIFY profileColorChanged)
            Q_PROPERTY(double deltaX READ deltaX WRITE setDeltaX NOTIFY deltaXChanged)
            Q_PROPERTY(bool visibility READ visibility WRITE setVisibility NOTIFY visibilityChanged)

    public: 
        TimelineVisibilityMark(QObject* parent = 0);
        TimelineVisibilityMark( double posX, QColor profileColor, double deltaX, bool visibility, QObject *parent = 0);

        double posX() const;
        void setPosX(double posX);

        QColor profileColor() const;
        void setProfileColor(QColor profileCOlor);

        double deltaX() const;
        void setDeltaX(double deltaX);

        bool visibility() const;
        void setVisibility(bool visibility);
signals:
        void posXChanged();
        void profileColorChanged();
        void deltaXChanged();
        void visibilityChanged();

    private:
        double m_posX;
        QColor m_profileColor;
        double m_deltaX;
        bool m_visibility;
    };
}

#endif //SMCQT_TIMELINEMARKERELEMENT_H_
