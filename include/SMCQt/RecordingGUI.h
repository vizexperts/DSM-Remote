#ifndef RECORDING_GUI_H_
#define RECORDING_GUI_H_

/*****************************************************************************
 *
 * File             : RecordingGUI.h
 * Version          : 1.0
 * Module           : Qt
 * Description      : RecordingGUI class declaration
 * Author           : Indira
 * Author email     : indira@vizexperts.com
 * Reference        : SMCQt
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCUI/RecordingUIHandler.h>

namespace SMCQt
{
    class RecordingGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED
        Q_OBJECT

        public:

            RecordingGUI();
            
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        private slots:

            //void _performCleanup();

            //! Handles browse button clicked
            void _handleBrowseButtonClicked();

            //! Handles capture button clicked
            void _startCaptureClicked(QString filename);

            //! Handle stop button clicked
            void _stopCaptureClicked();

            //! when popup is loaded
            void popupLoaded(QString type);

            //! when resolution is changed
            void _changeResolution(QString resolution);


        protected:

            virtual ~RecordingGUI();

            void _loadRecordingUIHandler();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            //! Browse button name
            std::string _browseButtonName;

            //! LineEdit box name
            std::string _lineEditName;

            //! Capture button name
            std::string _captureButtonName;

            //! 
            bool _startedCapture;

            CORE::RefPtr<SMCUI::IRecordingUIHandler> _recordingUIHandler;

            int _width;
            int _height;
    };

} 

#endif  // RECORDING_GUI_H_
