#pragma once

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>

//#include <SMCQt/QMLTreeModel.h>

#include <ComputeServices/IComputeServicesUIHandler.h>

#include <QDir>
#include <map>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT ComputeServicesGUI: public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:
        static const std::string ComputeServicesPopupObjectName;

        ComputeServicesGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

    public slots:

    void popupLoaded();

    protected:

        virtual ~ComputeServicesGUI();        

        CORE::RefPtr<ComputeServices::IComputeServicesUIHandler> _computeServicesUIHandler;

    };
}