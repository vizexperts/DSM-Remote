#ifndef SMCQT_CREATEMILITARYSYMBOLGUI_H_
#define SMCQT_CREATEMILITARYSYMBOLGUI_H_

/*****************************************************************************
*
* File             : CreateMilitarySymbolGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateMilitarySymbolGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <Elements/ISymbolTypeLoader.h>

#include <SMCUI/IMilitarySymbolPointUIHandler.h>


namespace SMCQt
{

    // Class to populate MilitarySymbolType GUI
    class CreateMilitarySymbolGUI 
        : public SMCQt::DeclarativeFileGUI
    {
        Q_OBJECT

    public:

        CreateMilitarySymbolGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void popupLoaded(QString type);
            void showAffiliation(QString affiliation);
            void showCategory(QString affiliation, QString category);
            void setSymbolSize(QString symbolSize);
            //@}


    protected:

        //! Destructor
        virtual ~CreateMilitarySymbolGUI();

        virtual void _loadAndSubscribeSlots();


        //! 
        void _populateGUI();

        void _populateList(const std::string& qAffiliation, const std::string& category = "");

    protected:

        CORE::RefPtr<ELEMENTS::ISymbolTypeLoader> _symbolLoaderComp;

        CORE::RefPtr<SMCUI::IMilitarySymbolPointUIHandler> _uihandler;

        bool _uniqueAcrossAffiliation;

    };

} // namespace SMCQt

#endif  // SMCQT_CREATEMILITARYSYMBOLGUI_H_
