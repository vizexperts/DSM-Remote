#ifndef SMCQT_CREATEFEATUREGUI_H_
#define SMCQT_CREATEFEATUREGUI_H_

/*****************************************************************************
*
* File             : CreateFeatureGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateFeatureGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/ILineUIHandler.h>
#include <SMCUI/IAreaUIHandler.h>
#include <SMCUI/ICreateFeatureUIHandler.h>
#include <SMCUI/IPointUIHandler2.h>
#include <QColor>


namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateFeatureGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        CreateFeatureGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:


            // Common slots
            void connectSmpMenu(QString type);

            void createLineFeatureType(QString type);

            void createAreaFeatureType(QString type);

    protected:

        virtual ~CreateFeatureGUI();

        virtual void _loadAndSubscribeSlots();

        //! get the feature export UI handler
        void _loadUIHandlers();

        void _populateAttributes();

        void _createMetadataRecord();

    protected:

        //! Line UI Handler
        CORE::RefPtr<SMCUI::ILineUIHandler> _lineUIHandler;

        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaUIHandler;

        CORE::RefPtr<SMCUI::ICreateFeatureUIHandler> _createFeatureUIHandler;

        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointUIHandler;

        std::string _selectedFeatureGeometry;

        std::string _selectedFeatureType;
    };

} // namespace SMCQt

#endif  // SMCQT_CREATEFEATUREGUI_H_
