#pragma once
/*****************************************************************************
*
* File             : MinMaxElevGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : MinMaxElevGUI class declaration
* Author           : Akshay Gupta
* Author email     : akshay@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCUI/IMinMaxElevUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>


namespace SMCQt
{
    // Creates a new layer
    class MinMaxElevGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        static const std::string MinMaxElevGUIObject;

        MinMaxElevGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void _updateMinMax();
        
        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void _handlePBStartClicked();
                void _handlePBStopClicked();
                void _handlePBAreaClicked(bool pressed);
                void _onUpdateMinMax();
                void _onReset();

    protected:

        virtual ~MinMaxElevGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

    protected:

        //! Slope Aspect UIHandler
        CORE::RefPtr<SMCUI::IMinMaxElevUIHandler> _uiHandler;

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;
        osg::Vec3d _minPoint;
        osg::Vec3d _maxPoint;

         CORE::RefPtr<CORE::IPolygon> _area;
        
        QObject* _minMaxElevMenuObject;

    };

} // namespace SMCQt
