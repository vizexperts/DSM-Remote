#ifndef SMCQT_RESOURCESLIST_H_
#define SMCQT_RESOURCESLIST_H_

#include <QAbstractListModel>
#include <SMCQt/QMLTreeModel.h>
#include <QStringList>

namespace SMCQt
{

    class ResourceCategory 
    {
    public : 
        ResourceCategory(QString name, QString path = "");

        //void addSymbol(QObject* symbol);
        void addSymbol(QString symbolName);

        QString getName() const;
        void setName(QString name);

        QString getPath() const;
        void setPath(QString path);
        
        //QList<QObject*> children() const;
        QStringList children() const;


    private:
        QString _name;
        //QList<QObject*> _children;
        QStringList _children;
        QString _path;
    };


    class ResourcesList : public QAbstractListModel
    {
        Q_OBJECT

    public:

        explicit ResourcesList(QObject* Parent = 0);

        QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

        void addCategory(ResourceCategory* category);

//        ResourceCategory* getCategory(int numIndex);

        int rowCount(const QModelIndex & Parent = QModelIndex())const;

        QHash<int, QByteArray> roleNames() const;

        void clear();

    private:

        Q_DISABLE_COPY(ResourcesList);

        QList<ResourceCategory*> _categories;

        enum ListMenuItemRoles
        {
            CategoryNameRole = Qt::UserRole + 1,
            ChildrenRole,
            CountRole,
            CategoryPathRole
        };
    };

}   // namespace SMCQt

#endif      // SMCQT_RESOURCESLIST_H_
