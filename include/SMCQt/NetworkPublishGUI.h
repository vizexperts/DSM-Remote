#ifndef QT_NETWORKPUBLISHGUI_H_
#define QT_NETWORKPUBLISHGUI_H_

/*****************************************************************************
 *
 * File             : NetworkPublishGUI.h
 * Version          : 1.0
 * Module           : Qt
 * Description      : NetworkPublishGUI class declaration
 * Author           : Udit Gangwani
 * Author email     : udit@vizexperts.com
 * Reference        : Qt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

// indigis3d related headers
#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <HLA/INetworkCollaborationManager.h>

namespace SMCQt
{
    //! Add Trajectory GUI Dialog
    class NetworkPublishGUI 
        : public VizQt::LayoutFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        public:
            
            // Widget names
            static const std::string LEName;
            static const std::string PBPublish;
            static const std::string PBCancel;
            static const std::string PBResign;
            static const std::string LBMessage;
            static const std::string PBProgress;

        public:

            NetworkPublishGUI();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);
            
            //! Initializes the following attributes:-
            //!     # Value editbox name
            //!     # Units combobox name
            //!     # Calculationtype editbox name
            void initializeAttributes();

            void setSyncMode(const std::string &mode);
            std::string getSyncMode();

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

        private slots:

            //! Handling publish/cancel buttons
            void _handlePublishButtonClicked();
            void _handleCancelButtonClicked();
            void _handleResignButtonClicked();

        protected:

            virtual ~NetworkPublishGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            CORE::RefPtr<HLA::INetworkCollaborationManager> _netwrkCollaborationMgr;

        protected:
            
            //! Boolean to indicate if project is published or not
            bool _published;

            //! Boolean to indicate if project is Currently destroying the project or not
            bool _destroying;

            //! Boolean to indicate if proper cleanup is done or not afetr the GUI is closed
            bool _cleanup;

            //! Synchronization Mode 
            std::string _syncMode;
    };

} // namespace Qt

#endif  // QT_NETWORKPUBLISHGUI_H_
