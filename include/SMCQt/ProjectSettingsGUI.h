#ifndef SMCQT_PROJECTSETTINGSGUI_H_
#define SMCQT_PROJECTSETTINGSGUI_H_

/*****************************************************************************
*
* File             : ProjectSettingsGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ProjectSettingsGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <Elements/IProjectSettingsComponent.h>

namespace SMCQt
{
    class ProjectSettingsGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        ProjectSettingsGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void connectProjectSettingsMenu();

            void militarySymbolModeChanged(int mode);
            void militarySymbolSizeChanged(QString size);
            

    protected:

        virtual ~ProjectSettingsGUI();

        virtual void _loadAndSubscribeSlots();

        //Instance of the setting component
        CORE::RefPtr<ELEMENTS::IProjectSettingsComponent> _projectSettingComponent;

    private:
        int _currentMode;
        float _symbolSize;
        QObject* _projectSettingsMenu;
        void _setCurrentMode();

    };

} // namespace SMCQt

#endif  // SMCQT_PROJECTSETTINGSGUI_H_
