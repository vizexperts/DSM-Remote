#ifndef SMCQT_CREATECURSORGUI_H_
#define SMCQT_CREATECURSORGUI_H_

/*****************************************************************************
*
* File             : CreateCursorGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : Class to manage custom cursor shapes for application
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>

//////////////////////////////////////////////////////////////////////////////
//!
//! \file CreateCursorGUI.h
//!
//! \brief Custom cursor implementation 
//!
//////////////////////////////////////////////////////////////////////////////

namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateCursorGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        CreateCursorGUI();

        ///////////////////////////////////////////////////////////////////////////
        //!
        //! \enum   CursorImplementation
        //!
        //! \brief  Type of cursor implementation
        //!         Set from config file on application startup
        //!
        ///////////////////////////////////////////////////////////////////////////
        enum CursorImplementation
        {
            CURSORIMPL_DEFAULT,
            CURSORIMPL_QML,
            CURSORIMPL_QT
        };

        //@}
        //@{
        /** Called when GUI added/removed to GUI manager */
        void onAddedToGUIManager();
        //@}

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn setCursorPackPath(const std::string& path)
        //!
        //! \brief Set cursor pack path for Qt implementation
        //!
        /////////////////////////////////////////////////////////////////////
        void setCursorPackPath(const std::string& path);

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn getCursorPackPath()
        //!
        //! \brief Get cursor pack path for Qt implementation
        //!
        /////////////////////////////////////////////////////////////////////
        std::string getCursorPackPath();

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn getCursorPackPathWrtQML()
        //!
        //! \brief Get cursor pack path for QML implementation
        //!
        /////////////////////////////////////////////////////////////////////
        std::string getCursorPackPathWrtQML();

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn setCursorPackPathWrtQML(const std::string& path)
        //!
        //! \brief Set cursor pack path for QML implementation
        //!
        /////////////////////////////////////////////////////////////////////
        void setCursorPackPathWrtQML(const std::string& path);

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn setCursorImplementation(int impl)
        //!
        //! \brief Set cursor implementation
        //!
        /////////////////////////////////////////////////////////////////////
        void setCursorImplementation(int impl);


        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn getCursorImplementation()
        //!
        //! \brief Get cursor implementation
        //!
        /////////////////////////////////////////////////////////////////////
        int getCursorImplementation();

        public slots:

            /////////////////////////////////////////////////////////////////////
            //!
            //! \fn changeCursor(int shape)
            //!
            //! \brief Change cursor
            //!
            /////////////////////////////////////////////////////////////////////
            void changeCursor(int shape);

            /////////////////////////////////////////////////////////////////////
            //!
            //! \fn updateMouseCursorPosition()
            //!
            //! \brief Update the position of QML rendered Cursor
            //!
            /////////////////////////////////////////////////////////////////////
            void updateMouseCursorPosition();

    protected:

        virtual ~CreateCursorGUI();

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn     _setCursor(const std::string& path, int hotspotX = -1, int hotspotY = -1)
        //!
        //! \brief  Set the cursor with give path of file, 
        //!         By default hotSpot coordinates are invalid(Center of the image is taken as offset)
        //!
        /////////////////////////////////////////////////////////////////////
        void _setCursor(const std::string& path, int hotspotX = -1, int hotspotY = -1);

    private:

        /////////////////////////////////////////////////////////////////////
        //!
        //! \fn     getCursorPath(Qt::CursorShape shape)
        //!
        //! \brief  Get image path for cursor
        //!
        /////////////////////////////////////////////////////////////////////
        std::string getCursorPath(Qt::CursorShape shape);

        /////////////////////////////////////////////////////////////////////
        //!
        //! \var     _cursorPackPath
        //!
        //! \brief  Cursor pack path for Qt implementation
        //!
        /////////////////////////////////////////////////////////////////////
        std::string _cursorPackPath;

        /////////////////////////////////////////////////////////////////////
        //!
        //! \var     _cursorPackPathWrtQML
        //!
        //! \brief  Cursor pack path for QML implementation
        //!
        /////////////////////////////////////////////////////////////////////
        std::string _cursorPackPathWrtQML;

        /////////////////////////////////////////////////////////////////////
        //!
        //! \var     _cursorImpl
        //!
        //! \brief  Current cursor implementation
        //!
        /////////////////////////////////////////////////////////////////////
        CursorImplementation _cursorImpl;

        /////////////////////////////////////////////////////////////////////
        //!
        //! \var     _cursorObject
        //!
        //! \brief  QML Cursor object
        //!
        /////////////////////////////////////////////////////////////////////
        QObject* _cursorObject;

        /////////////////////////////////////////////////////////////////////
        //!
        //! \var     _layoutView
        //!
        //! \brief  Main QML Window
        //!
        /////////////////////////////////////////////////////////////////////
        QQuickView* _layoutView;
    };

} // namespace SMCQt

#endif  // SMCQT_CREATECURSORGUI_H_
