/*****************************************************************************
*
* File             : TerrainExportGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : Terrain Export class declaration
* Author           : kamal
* Author email     : kamal@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright 2015-2016, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>

#include <SMCUI/ITerrainExportUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
#include <osgEarth/SpatialReference>
#include <Util/CPUWallTimer.h>
namespace SMCQt
{
    // Creates a new layer
    class TerrainExportGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
            QObject* TerrainExportObject;
    public:

        /** Chosen for performance optimizations for unreal .
         *  Other Sizes that we can chose from are 127,253,505,1009,2017,4033,8129.
         *  i.e. size[0] = 64 and size(i) = 2*size[i] - 1 forall i in [1,8];
         *  For info refer : https://docs.unrealengine.com/latest/INT/Engine/Landscape/TechnicalGuide/
         **/
        const int TILE_SIZE = 2017;

        static const std::string TerrainExportPopup;
        static const std::string SnowConvertPopup;
        static const std::string ClassificationJson;
        static const std::string TempDir;
        TerrainExportGUI();
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

    signals:

        void  _setProgressValue(int);

        public slots:

        //@{
        /** Set/Get active function */
        void setActive(bool active);
        //@}
        void _browseButtonClicked();
        void _cancelButtonClicked(bool hasUserCanceled);
        void _populateVectorAttributes(QString file);
        void _generateRasterButtonClicked(); 

        private slots:

        //! Signal handler Terrain Export Complete  
        void postTerrainExport();

        // Slots for event handling
        void _handleProgressValueChanged(int value , std::string message);
        void _handleExportPressed(bool pressed);
        void _handleMarkButtonPressed(bool pressed);
        void _reset();

    protected:

        virtual ~TerrainExportGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();
        void _readPreviousDumpInfo(std::string extentsFile);
        double _getOptimisedSideLength();

        //! Returns extents in ULX, ULY, BRX, BRY format
        osg::Vec4d _transform(osg::Vec4d extents, const osgEarth::SpatialReference* in_sref, const osgEarth::SpatialReference* out_sref);

    protected:
        QList<QObject*>vectorList;
        QList<QObject*>rasterList;
        QList<QObject*> vectorClassificationTypeList;
        QList<QObject*> rasterClassificationTypelist;

        int _totalLayers = 0;
        int _layersProcessed = 0;
        int _progressStatus = 0; 

        //! Height Above UIHandler
        CORE::RefPtr<SMCUI::ITerrainExportUIHandler> _uiHandler;

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;
        UTIL::CPUWallTimer _timer;

    };

} // namespace SMCQt
