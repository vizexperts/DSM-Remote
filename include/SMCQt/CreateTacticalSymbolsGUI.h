#pragma once

/*****************************************************************************
*
* File             : CreateTacticalSymbolGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateTacticalSymbolGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQT interface
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/
#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCQt/QMLTreeModel.h>

#include <SMCUI/ITacticalSymbolsUIHandler.h>
#include <TacticalSymbols/ITacticalSymbol.h>
#include <QDir>
#include <map>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateTacticalSymbolsGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string TacticalSymbolsPopupObjectName;
        static const std::string TacticalSymbolsContextualTabObjectName;

        CreateTacticalSymbolsGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

        void connectContextualMenu();
        void disconnectContextualMenu();

        // slots for contextual menu
        void rename(QString newName);
        void deleteSymbol();
        void setEditMode(bool mode);

        void popupLoaded();
        void markTacticalSymbol(bool value, QString type);

    protected:

        virtual ~CreateTacticalSymbolsGUI();

        CORE::RefPtr<SMCUI::ITacticalSymbolsUIHandler> _tacticalSymbolsUIHandler;       

        //! Symbol data directory path
        std::string _symbolDataDir;

    };
}
