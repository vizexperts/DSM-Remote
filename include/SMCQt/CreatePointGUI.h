#pragma once

/*****************************************************************************
*
* File             : CreatePointGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateLineGUI class declaration
* Author           : Nitish Puti
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <QColor>
#include <SMCUI/IPointUIHandler2.h>
#include <Core/Base.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>
#include <GIS/IFeature.h>
#include <VizUI/ISelectionUIHandler.h>
#include <QAbstractListModel>
#include <Elements/IMapSheetInfoComponent.h>
#include <SMCQt/QMLDirectoryTreeModel.h>
namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreatePointGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string PointContextualMenuObjectName;
        static const std::string ContextualMenuObjectName; 
        static const std::string MouseButtonClickedPropertyName;
        static const std::string pointHyperlinkActivePropertyName; 
        static const std::string pointAttributeActivePropertyName; 
        static const std::string HyperlinkContextualMenuObjectName;
        static const std::string FeatureContextualObjectName; 
        std::string jsonFile;

        CreatePointGUI();

        void CreatePointGUI::showMultipleImages();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:
        void play(QString);

        public slots:

            // Slots to connect to furthur menus
            void connectSMPMenu(QString type);

            void connectContextualMenu();
            void disconnectContextualMenu();

            // 
            void setPointEnable(bool value);

            // if the Label is selected
            void setLabelEnable(bool value);

            //Slots for Contextual menu
            void rename(QString newName);
            void setTextActive(bool state);
            void handleLatLongAltChanged(QString);
            void deletePoint();
            void clampingChanged(bool value);
            void showPointAttributes();
            void editEnabled(bool value);

            // slots for point layer
            void addPointsToSelectedLayer(bool value);

            // slots for Hyperlink options
            void attachFile(QString type);

            void playAudio();

            void playVideo();

            void showImage(QString fileName); 

            //For playing the video or audio 
            void playAudioOrVideo(QString fileName);

            void  openUrl(QString fileName);

            void handleAttributesOkButtonPressed();
            void handleAttributesCancelButtonPressed();

            //Ths slot for changing the Text size and color
            void colorChanged(QColor color);
            void changeTextSize(double textSize);

            // slot for toggling outline
            void outlineChanged(bool value);

            // slot for toggling billboard
            void billboardChanged(bool value);

            // slot for changing outline color
            void outlineColorChanged(QColor color);

            // slot for changing billboard color
            void billboardColorChanged(QColor color);

            // slot for changing font file for text
            void fontChanged(QString font);

            // slot for changing alignment
            void alignmentChanged(int alignment);

            // slot for changing layout
            void layoutChanged(int layout);

            // slot for chaning font files directory
            void handleBrowseButtonClicked();

            // saving text hyperlink for a point
            void saveTextPoint(QString pointText);
            void handleStyleApplyButton();

            void populateSymbolList();
            void symbolFileSelected(int index);

            void handleFontButton();

            protected slots:
                void _resetForDefaultHandling();

    protected:

        void setActive(bool active);


    protected:

        void _performHardReset();

        virtual ~CreatePointGUI();

        virtual void _loadAndSubscribeSlots();

        void _loadPointUIHandler2();

        void _populateAttributes();

        void _populateContextualMenu();

        void _createMetadataRecord();

        // populates all the fonts(.ttf extensions) present in _fontDir
        void _populateFontList();

        std::string _getProjectLocation();

        // used by attachFiles() slot for copying files attached 
        void _copyAttachedFiles(std::string fileChosen);


        //! Point UI Handler
        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointUIHandler;
        CORE::RefPtr<VizUI::ISelectionUIHandler> _selectionUIHandler;

        CORE::RefPtr<GIS::IFeature> _selectedFeature;

        //!
        CORE::RefPtr<CORE::IFeatureLayer> _selectedFeatureLayer;
        //Mapsheet component  for conversion  lat long to GR
        CORE::RefPtr<ELEMENTS::IMapSheetInfoComponent> _mapSheetComponent;

        QList<QObject*> _attributeList;

        bool _addToDefault;

        bool _mouseClickRight;

        bool _cleanup;

        unsigned int _pointNumber;

        QStringList _imageList;
        
        QStringList _docList;

        QStringList _urlList;

        std::string _audioVideoFileBrowsed;

        // flag to check if Point is selected or Label
        bool _pointEnabled;

        // stores the list of fonts present in _fontDir
        std::vector<std::string> _fontsList;

        // current font directory
        std::string _fontDir;
        QMLDirectoryTreeModel*      _qmlDirectoryFileTreeModel;
        QMLDirectoryTreeModelItem*  _qmlDirectoryFileTreeModelRootItem;
        //! Keeps a list of all context property names set by this UI so that they can be set to NULL when menu is closed
        std::vector<std::string> _contextPropertiesSet;

        //! used for populating model and icon file names,.
        bool _populateModelDir(QDir dir, QMLDirectoryTreeModelItem* item = NULL);
    };

} // namespace SMCQt
