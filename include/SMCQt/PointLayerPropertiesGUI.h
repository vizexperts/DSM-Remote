#pragma once
/*****************************************************************************
*
* File             : PointLayerPropertiesGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : PointLayerPropertiesGUI class declaration
* Author           : Somnath Sngh
* Author email     : somnath@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/Referenced.h>

#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCUI/IPointUIHandler2.h>
#include <Core/ISelectable.h>

#include <QtCore/QStringList>

namespace SMCQt
{
    class QLegendColorListObject : public QObject
    {
        Q_OBJECT
            Q_PROPERTY(QColor legendColor READ legendColor WRITE setlegendColor NOTIFY legendColorChanged)
            Q_PROPERTY(QString legendName READ legendName NOTIFY legendNameChanged)
            Q_PROPERTY(int legendIndex READ legendIndex NOTIFY legendIndexChanged)

    public:
        QLegendColorListObject(const QString& name, const QColor& color, int index, QObject* parent = 0)
            : QObject(parent)
            , m_name(name)
            , m_color(color)
            , m_index(index)
        {}

        QColor legendColor() const { return m_color; }
        QString legendName() const { return m_name; }
        int legendIndex() const { return m_index; }

        void setlegendColor(QColor color)
        {
            if(color != m_color)
            {
                m_color = color;
                emit legendColorChanged();
            }
        }

        void setlegendName(QString name)
        {
            if(name != m_name)
            {
                m_name = name;
                emit legendNameChanged();
            }
        }

        void setlegendIndex(int index)
        {
            if (index != m_index)
            {
                m_index = index;
                emit legendIndexChanged();
            }
        }

    signals:
        void legendColorChanged();
        void legendNameChanged();
        void legendIndexChanged();

    private:
        QColor m_color;
        QString m_name;
        int m_index;
    };

    // Point layer properties GUI
    //! TODO: Class needs to be refactored.
    //! TODO: Only Histogram functionality using this class.
    class PointLayerPropertiesGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE; 
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        PointLayerPropertiesGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

        public slots:
            void connectContextualMenu();
            void disconnectContextualMenu();


        private slots:

            //! Updates the area editbox regularly
            void _handleOkButtonClicked();
            void _handleCancelButtonClicked();
            void _handleBrowseButtonClicked();
            void _handleApplyButtonClicked();
            void _showIconMenu(bool value);


            void _annotationChanged(QString value);
            void _changeHistColor(QColor newColor, int index);
            void _changeColorTransparency(int alphaValue);
            void _changeHistWidth(double width);
            void connectPopup(QString type);
            void _fontSizeChanged ( QString size);
            void _fontColorChanged (QColor color);
            void _fontStyleChanged(int fontfile);
            
            //! Show legend window for currently selected layer
            //! Applicable only if Histogram property is available
            void showLayerLegend();

            void changeDrawTechniqueType(QString drawTechnique, QString modelType);

            void _handlePopupOkButtonPressed();
            void popupClosed();

            void _setBusy();

    
    protected:

        virtual ~PointLayerPropertiesGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! fill the table widget
        void _fillSymbolTable();

        void _resetQMLIconProperty();

        void _uploadNewImage(const std::string& iconName);

        void addIcon(const CORE::NamedGroupAttribute &credential);
        CORE::RefPtr<CORE::NamedGroupAttribute> getIcon();

    protected:

        //! Point UI Handler
        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointUIHandler;

        std::string _lastBrowsedpath;

        std::string _parentObjectName;

        CORE::RefPtr<CORE::ISelectable> _currentSelection;

        bool                _resetStyleOnClose;

        std::string         _originalIcon;
        std::string         _originalColumnName;
        QString             _originalDrawTechnique;
        QString             _originaModelType;
        QColor              _color;
        int                 _size;
        std::string         _fontfile;
        QList<QObject*>     _originalHistColorArray;
        double              _originalHistWidth;
    };

} // namespace SMCQt
