#ifndef SMCQT_ICONTEXTUALTABGUI_H_
#define SMCQT_ICONTEXTUALTABGUI_H_

/*****************************************************************************
*
* File             : IContextualTabGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : IContextualTabGUI interface
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <VizQt/GUI.h>
#include <SMCQt/export.h>
#include <Core/Base.h>
#include <Core/IBaseUtils.h>
#include <VizQt/IQtGUIManager.h>
#include <VizUI/ICameraUIHandler.h>

#include <SMCUI/ILineUIHandler.h>

#include <QtCore/QTimer>

namespace SMCQt
{
    class IContextualTabGUI : public CORE::IReferenced
    {
        DECLARE_META_INTERFACE(SMCQT_DLL_EXPORT, SMCQt, IContextualTabGUI);

    public:

        //! activates the tab by typename
        virtual void activateByObjectTypeName(std::string name) = 0;

        virtual void closeContextualTab() = 0;
    };

} // namespace SMCQt

#endif  // SMCQT_ICONTEXTUALTABGUI_H_
