#ifndef SMCQT_QUERYBUILDERGUI_H_
#define SMCQT_QUERYBUILDERGUI_H_

/*****************************************************************************
*
* File             : QueryBuilderGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : QueryBuilderGUI class declaration
* Author           : Mohan Singh
* Author email     : mohan@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2013-2014, CAIR, DRDO                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/ISelectable.h>
#include <Core/ISQLQuery.h>
#include <ogrsf_frmts.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>

#include <Core/IMessageType.h>
#include <App/AccessElementUtils.h>
#include <Core/IWorldMaintainer.h>
#include <Core/ISelectionComponent.h>
#include <SMCUI/ISQLQueryUIHandler.h>
//#include <Elements/IQueryComponent.h>
#include <SMCQt/LayerTableModel.h>

#include <gdal_priv.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT QueryBuilderGUI:public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:
        //! Constructor
        QueryBuilderGUI();

        //! 
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void setActive(bool active);
            void connectPopupLoader(QString);

            private slots:

                void _handlePBAreaClicked(bool pressed);
                void _handleQueryButtonClicked();
                void _handleOkButtonClicked();
                void _handleClearButtonClicked();
                void _handleCancelButtonClicked();
                void _handleTableOkButtonClicked();

                void _handleAddToSelectButtonClicked(int index);
                void _handleAddToWhereButtonClicked(int index);
                void _handleDeleteFromSelectedFields(int index);

                void _handleOperatorButtonClicked(QString buttonName);

    protected: 

        virtual ~QueryBuilderGUI();

        //! Loads the qml file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! get the current selected Layer
        void _getSelectedLayer();

        //!
        void _fillTableDefinitionFields();

        //!
        CORE::RefPtr<CORE::IObject> _createFeatureLayer(OGRLayer* inputLayer);

        //! SQL Query UI Handler
        CORE::RefPtr<SMCUI::ISQLQueryUIHandler> _sqlQueryUIHandler;

        //! 
        CORE::RefPtr<CORE::IFeatureLayer> _parentLayer;

        //! feature layer currently selected
        CORE::RefPtr<CORE::IFeatureLayer> _currentLayer;
        OGRLayer* _currentOGRLayer;

        //!
        CORE::RefPtr<CORE::ISelectable> _previousSelection;

        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;

        // list containing all fields
        QStringList _allFieldDefinitions;

        // list containing only selected fields
        QStringList _selectedFieldDefinitions;

        // creation of model
        LayerTableModel* _tableModel; 
    };
}

#endif
