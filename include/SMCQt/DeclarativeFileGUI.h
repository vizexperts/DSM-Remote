#pragma once

/*****************************************************************************
*
* File             : DeclarativeFileGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : DeclarativeFileGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)                                       
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <VizQt/QtGUI.h>
#include <Core/IBaseUtils.h>
#include <SMCQt/export.h>
#include <Util/ThreadPool.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT DeclarativeFileGUI : public VizQt::QtGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        //! Object name strings for various commonly used menu items.
        static const std::string SMP_RightMenu;
        static const std::string SMP_FileMenu;
        static const std::string SMP_QuickLaunchGUI; 

        DeclarativeFileGUI();

        void initializeAttributes();

        //@{
        /** Called when GUI added to GUI manager */
        void onAddedToGUIManager();
        void onRemovedFromGUIManager();
        //@}

        QWidget* getWidget() const;

    private slots:

        void connectErrorMessage();
        void _hideBusyBar();

        // error message with provided slots
        void _criticalQuestion(QString title, QString text,bool closeVisible = false, 
            QString okSlot = "", QString cancelSlot = "");

        // Warning message
        void _question(QString title, QString text, bool closeVisible = false, 
            QString okSlot = "", QString cancelSlot = "");

        // shows error to user with just ok button, furthur interaction possible if required
        void _showError(QString title, QString text, bool critical = true, QString okSlot = "");

        void errorWindowClosed();

signals:

        void executed();

        void showError(QString title, QString text, bool critical = true, QString okSlot = "");

        // error message with provided slots
        void criticalQuestion(QString title, QString text, bool closeVisible = false, 
            QString okSlot = "", QString cancelSlot = "");

        // Warning message
        void question(QString title, QString text, bool closeVisible = false,
            QString okSlot = "", QString cancelSlot = "");

    protected:            

        virtual void _loadAndSubscribeSlots();

        // returns the child objectName of root qml item
        QObject*  _findChild(const std::string& objectName);

        void jumpToObject(CORE::RefPtr<CORE::IObject> object);

        // sets the context property of the root context
        void _setContextProperty(const std::string& propertyName , const QVariant& var);

        void _loadBusyBar(const UTIL::ThreadPool::Functor0_T& threadFunc, std::string message, bool autoHide = true);

        void _executeBusyBar();

        virtual ~DeclarativeFileGUI();

        QString _errorTitle;

        QString _errorDescription;

        QString _okSlot;

        QString _cancelSlot;

        bool _critical;

        bool _buttonText;

        bool _closeVisible;

        UTIL::ThreadPool::Functor0_T _busyFunction;
    };

} // namespace SMCGUI
