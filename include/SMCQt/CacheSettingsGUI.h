#pragma once

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    /**
    * class CacheSettingsGUI
    * \brief This class is intended to handle the clear cache settings 
    * \note currently only DGN Cache clear is implemented.
    */
    class CacheSettingsGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:
        CacheSettingsGUI();

        public slots:
        
        /**
        * connects SIGNAL clearDGNCache()  ---> SLOT handleClearDGNCache()
        */
        void connectCacheSettingsMenu();
        
        /**
        * confirms from user to clear cache
        */
        void handleClearDGNCache();
        
        /**
        * clears the cache for all the DGN that have been created previously
        * except for the ones that are currently loaded
        */
        void clearCache();

    protected:

        virtual void _loadAndSubscribeSlots();
        virtual ~CacheSettingsGUI();

        QObject* _cacheSettingsObject;

    };

} // namespace SMCQt
