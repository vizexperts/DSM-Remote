#pragma once

#include <QObject>

namespace SMCQt
{

    struct VizProfileObject : public QObject
    {
        Q_OBJECT
    public:
        Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged); 
        Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged);
        Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged);

        VizProfileObject(const QString& name, const QString& color, const bool active);
        VizProfileObject(); 
        VizProfileObject(const VizProfileObject& obj);

        QString name() const;
        void setName(const QString& name);

        QString color() const;
        void setColor(const QString& color);

        bool active() const; 
        void setActive(bool active); 
            
    signals: 
        void nameChanged(); 
        void colorChanged(); 
        void activeChanged(); 

     public: 
        QString _name; 
        QString _color; 
        bool _active; 
    };
}