#pragma once
/*****************************************************************************
*
* File             : CreateLineGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateLineGUI class declaration
* Author           : Nitish Puri
* Author email     : ns@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/ILineUIHandler.h>
#include <QColor>
#include <GIS/IFeature.h>
#include <VizUI/ISelectionUIHandler.h>


namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateLineGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string LineContextualMenuObjectName;

        CreateLineGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void setClamping(bool clamping);
        bool getClamping() const;
        std::string jsonFile;

    private:
        void onVertexSelected( );

    public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);

            // Common slots
            void connectContextualMenu();
            void disconnectContextualMenu();

            void connectSmpMenu(QString type);

            //Slots for contextual menu
            void colorChanged(QColor color);
            void lineWidthChanged(double lineWidth);
            void rename(QString newName);
            void setTextActive(bool value);
            void deleteLine();
            void setEditMode( bool mode );
            void connectEditVerticesMenu(bool value);
            void deleteVertex( );
            void handleLatChanged();
            void handleLongChanged();
            void showLineAttributes();
            void setLineEnable(bool value);

            // slot for toggling outline
            void outlineChanged(bool value);

            // slot for toggling billboard
            void billboardChanged(bool value);

            // slot for changing outline color
            void outlineColorChanged(QColor color);

            // slot for changing billboard color
            void billboardColorChanged(QColor color);

            // slot for changing font file for text
            void fontChanged(QString font);

            // slot for changing alignment
            void alignmentChanged(int alignment);

            // slot for changing layout
            void layoutChanged(int layout);

            // slot for chaning font files directory
            void handleBrowseButtonClicked();

            // slot for changing text color
            void textColorChanged(QColor color);
            
            // slot for changing text size
            void changeTextSize(double textSize);

            //@}

            // slots for line layer

            //! \see LineLayerTab.qml
            void addLineToSelectedLayer(bool value);

            void exportLine();

            void handleAttributesOkButtonPressed();
            void handleAttributesCancelButtonPressed();

            void _populateContextualMenu();

            void handleStyleApplyButton();
            void handleFontButton();
    protected slots:

        void _resetForDefaultHandling();

    protected:

        virtual ~CreateLineGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! get the feature export UI handler
        void _loadLineUIHandler();

        void _populateAttributes();

        void _createMetadataRecord();

        void _performHardReset();

        // populates all the fonts(.ttf extensions) present in _fontDir
        void _populateFontList();

    protected:

        //! Line UI Handler
        CORE::RefPtr<SMCUI::ILineUIHandler> _lineUIHandler;
        CORE::RefPtr<VizUI::ISelectionUIHandler> _selectionUIHandler;

        //!
        CORE::RefPtr<CORE::IFeatureLayer> _selectedFeatureLayer;

        bool _addToDefault;

        unsigned int _currLineNumber;
        bool _mouseClickRight;
        QList<QObject*> _attributeList;
        
        //!
        bool _clamping;

        CORE::RefPtr<GIS::IFeature> _selectedFeature;

        // stores the list of fonts present in _fontDir
        std::vector<std::string> _fontsList;

        // current font directory
        std::string _fontDir;

    };

} // namespace SMCQt
