#ifndef SMCQT_ProfileEditor_H
#define SMCQT_ProfileEditor_H

#include <QtQuick/QQuickPaintedItem>
#include <QtQuick/QQuickItem>
#include <QtGui/QPainter>
#include <QObject>
#include <QPointF>
#include <SMCQt/VizProfileObject.h> 

namespace SMCQt{

    class ProfileObject : public QQuickPaintedItem
    {
        Q_OBJECT
            Q_PROPERTY(QString objectName READ objectName WRITE setObjectName)
            Q_PROPERTY(QString xProp READ xProp WRITE setXProp NOTIFY xPropChanged)
            Q_PROPERTY(QString yProp READ yProp WRITE setYProp NOTIFY yPropChanged)

    public:
        
        static const std::string ditchProfilePopup;
        static const std::string ditchPopup;
        static const std::string profileEditorPopup; 

        ProfileObject(QQuickItem* parent = 0);
        ~ProfileObject() {};

        void paint(QPainter* painter);
        Q_INVOKABLE void setLineData(QList<QVariant> lineData);
        Q_INVOKABLE void clearGraph();
        Q_INVOKABLE void startDrawing(bool);
        Q_INVOKABLE QList<QVariant> getLineData();
        Q_INVOKABLE QList<QVariant> getCurrentPositionAlongLine();
        Q_INVOKABLE void setVizProfileObject(QString name, QString color, bool active);
        Q_INVOKABLE void setVisible(QString name, QString color, bool checked);
        Q_INVOKABLE QMap<QString, QVariant> getPointProfileMap(); 
        Q_INVOKABLE void deleteTemplate(QString);
        Q_INVOKABLE void editGraph(bool set);
        Q_INVOKABLE void setGraphX(QString x); 
        Q_INVOKABLE void setProfileXY(QString x, QString y); 

        void setProfileXY(QPointF point);

        QString xProp() const;
        void setXProp(const QString& x);

        QString yProp() const; 
        void setYProp(const QString& y); 
    signals: 
        void xPropChanged(); 
        void yPropChanged(); 

    protected:
        void mousePressEvent(QMouseEvent* event);
        void mouseReleaseEvent(QMouseEvent* event); 
        void mouseMoveEvent(QMouseEvent *event);
        void setObjectName(QString objectName);
        void showPointstext(QVector<QPointF> points,QPainter* painter);

    private:

        QVector<QPointF> _line;
        bool _clear = false;
        bool _startDrawing = false;
        QString _objectName  = "";

        bool _simpleClickPressed = false; 
        int _selectedPointIndex = -1;

        QString _currentVizProfileObject; 
        std::map<std::pair<float, float>, QString> _pointProfileMap;
        std::map<QString, VizProfileObject> _nameProfileMap;
        QPointF _selectedPoint; 

        std::pair<float, float> _selectedPointPair;
        bool _editGraph = false; 

        QString _x = "0"; 
        QString _y = "0";
    };
}


#endif 