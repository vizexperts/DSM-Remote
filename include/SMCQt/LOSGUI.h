#pragma once
/*****************************************************************************
*
* File             : LOSGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : LOSGUI class declaration
* Author           : Sumit Pandey
* Author email     : sumitp@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <GISCompute/LOSVisitor.h>
#include <SMCUI/IPointUIHandler2.h>
#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    // Creates a new layer
    class LOSGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        static const std::string LOSAnalysisObjectName;

        LOSGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void _startButtonEnable(bool);
        void _setProgressValue(int);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void _handlePBMarkViewPointClicked(bool);
                void _handlePBMarkLookAtClicked(bool);
                void _changeLosType(int losType);
                void _changeCircularFill(bool value);
                void _changeCircularSectorAngle(double value);
                void _changeTransperancy(double value);
                void _changeViewerHeight(double value);
                void _changeLookAtHeight(double value);
                void _changeViewerLat(double value);
                void _changeViewerLong(double value);
                void _changeLookAtLat(double value);
                void _changeLookAtLong(double value);
                void _numberOfSpokes(int);

    protected:

        virtual ~LOSGUI();

        //! Get the point from point UI handler and fills the edit box, depending on the state
        void _processPoint();

        //! starts the filter
        void _execute();

        // calculates aerial distance between lookAt and Viewer point
        void _calculateDistance();

        void _setViewPoint(CORE::IPoint* point);

        void _setLookAtPoint(CORE::IPoint* point);

        //! enable/disable point handler 
        void _setPointHandlerEnabled(bool enable);

        //! reset params
        void _reset();

        //void _calculateAngle();


    protected:

        enum PointSelectionState
        {
            NO_MARK,
            VIEWPOINT_MARK,
            DIR_MARK
        };

        enum LOSType
        {
            NONE,
            LINEAR,
            CIRCULAR
        };

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointHandler;

        //! LOS Visitor
        CORE::RefPtr<GISCOMPUTE::ILOSVisitor> _visitor;

        //! Point selection state
        PointSelectionState _state;

        //! target position
        osg::Vec3d _targetPos;

        //! point object for viewer position
        CORE::RefPtr<CORE::IObject> _viewerPoint;

        //! point object for viewing position
        CORE::RefPtr<CORE::IObject> _vieweingPoint;

        QObject* _losMenuObject;
        double _viwerActualAlt;//! Variable to Store Viewer Actual Height

        //! set what is current type of LOS which on which update is working if it's first time then NONE will create new
        LOSType _currentLOSMode;

        //state, which track whether first time object created or not
        bool _firstObjectCreated;

        //is slot is called via user's line editing or via application 
        bool _isFromUser;
    };

} // namespace SMCQt
