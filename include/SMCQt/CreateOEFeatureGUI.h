#pragma once

/*****************************************************************************
*
* File             : CreateOEFeatureGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateOEFeatureGUI class declaration
* Author           : Gurpreet Singh
* Author email     : gurpreet@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCQt/export.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <VizUI/ISelectionUIHandler.h>
#include <SMCQt/ElementListTreeModel.h>
#include <SMCQt/QMLDirectoryTreeModel.h>
#include <Terrain/IOEFeatureObject.h>
namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateOEFeatureGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT
      
    static const std::string FeatureContextualMenuObjectName;
      std::string styleFilePath;
      CORE::RefPtr<TERRAIN::IOEFeatureObject> oeFeature;
    public: 
        CreateOEFeatureGUI();
    public slots :
        void handleFeatureContextualClose();
        void handleFeatureContextualOpen();
        void handleFeatureContextualSave();
        bool populateEnumList(QString name, QString parentName, QString value);
        
    protected: 
        void _populateContextualMenu(); 

        std::vector<std::string> _contextPropertiesSet;
        //attribute list 
        QList<QObject*> _attributeList;

        QStringList _imageList;

        QStringList _docList;

        QStringList _urlList;

    }; 
}