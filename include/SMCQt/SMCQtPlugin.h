#pragma once

/*****************************************************************************
*
* File             : SMCQtPlugin.h
* Version          : 1.1
* Module           : SMCQt
* Description      : SMCQtPlugin class declaration
* Author           : Gaurav Garg
* Author email     : gaurav@vizexperts.com
* Reference        : Part of SMCQt
* Inspected by     :
* Inspection date  :
*
*****************************************************************************
* Copyright (c) 2011-2012, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <App/IApplicationPlugin.h>
#include <App/IAppRegistryPlugin.h>
#include <SMCQt/export.h>

namespace APP
{
    class IApplication;
    class ApplicationRegistry;
    class IManagerFactory;
    class IApplicationFactory;
    class IGUIFactory;
}

namespace SMCQt
{
    //////////////////////////////////////////////////////////////////////////
    //!
    //! Registers all objects, interfaces and messages defined in CORE
    //!
    //////////////////////////////////////////////////////////////////////////

    class SMCQtAppRegistryPlugin : public APP::IAppRegistryPlugin
    {
    public:

        //@{
        /** Registration IDs */
        static const unsigned short StartID = 600;
        //@}

        //@{
        /** GUI */
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AddContentGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> EnvironmentGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> BottomPaneGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AddGPSGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateLineGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreatePointGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ElementListGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> SearchGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CalendarGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> EventManagerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ContextualTabGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateMilitarySymbolGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreatePolygonGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateLandmarkGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ProjectManagerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> StereoSettingsGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> LoginGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ResourceManagerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> MilitaryPathAnimationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> UndoRedoGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> TimelineGUIType;
#ifdef USE_QT4
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AppLauncherGUIType;
#endif

        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> NetworkJoinGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> NetworkPublishGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ScreenShareGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> PresentationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> TourGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> SettingsGUIType;

        // gui's for filters
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> MeasureDistanceGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> MeasureSlopeGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> MeasureAreaGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ElevationProfileGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> LOSGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> SlopeAspectGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ColorElevationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AlmanacGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ViewshedAnalysisGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> SensorRangeAnalysisGUIType;
        //@}

        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> RecordingGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ScreenshotGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateLayerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> FeatureExportGUIType;

        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AddWebDataGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> PointLayerPropertiesGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> EventGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ProjectSettingsGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> GuiSettingsGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AddServerLayerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> DownloadRasterGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateFeatureGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CrestClearanceGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> RadioLOSGUIType;
#ifdef WIN32
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> TTSGUIType;
#endif//WIN32
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> WebCamGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateOpacitySliderGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> VisibilityControllerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateTMSGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> RasterUploadDialogGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> PGCreateDataTypeDialogGUIType;
#ifdef WIN32
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateCursorGUIType;
#endif //WIN32
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> PanSharpenGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CVAGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> KMeanGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> FieldOfViewGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CacheSettingsGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> RasterBandReorderGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> RasterEnhancementChangerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> RasterWindowGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ReprojectImageGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ExtractROIGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> GunRangeGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> GetInformationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> QueryBuilderGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> QueryBuilderGISGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ElementClassificationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> RoadNetworkGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> DatabaseLayerGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ViewBasedVisibilityGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> LineLayerPropertiesGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> MinMaxElevGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> MeasureHeightGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ElevationDiffGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> MeasurePerimeterGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> SteepestPathGUIType;


        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ContoursGenerationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> HillshadeCalculationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> TerrainExportGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AnimationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> FloodingGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> WatershedGenerationGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CutAndFillGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> FlyAroundGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> DatabaseSettingsGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> GeorbISServersSettingsGUIType;

        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> StyleTemplateGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateOEFeatureGUIType; 

        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> VectorLayerQueryGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> VectorLayerQueryBuilderGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> CreateTacticalSymbolsGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ComputeServicesGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> AddBookmarksGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ApplicationSettingsGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ColorPaletteGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> ColorLegendGUIType;
        static const SMCQT_DLL_EXPORT CORE::RefPtr<APP::IGUIType> UserManualGUIType;

        //! Library name string
        static const std::string LibraryName;

        //! Plugin name string
        static const std::string PluginName;

    public:

        ~SMCQtAppRegistryPlugin();

        //@{
        /** Plugin information */
        const std::string& getPluginName() const;
        const std::string& getLibraryName() const;
        //@}

        //@{
        /** Load/Unload the library */
        virtual void load(const APP::ApplicationRegistry& cr);
        virtual void unload(const APP::ApplicationRegistry& cr);
        //@}

    protected:

        //@{
        /** Register/Deregister managers */
        virtual void registerManagers(APP::IManagerFactory& mf);
        virtual void deregisterManagers(APP::IManagerFactory& mf);
        //@}

        //@{
        /** Register/Deregister applications */
        virtual void registerApplications(APP::IApplicationFactory& af);
        virtual void deregisterApplications(APP::IApplicationFactory& af);
        //@}

        //@{
        /** Register/Deregister applications */
        virtual void registerGUIs(APP::IGUIFactory& factory);
        virtual void deregisterGUIs(APP::IGUIFactory& factory);
        //@}
    };

    //////////////////////////////////////////////////////////////////////////
    //!
    //! Adds GUI instances at runtime to the application
    //!
    //////////////////////////////////////////////////////////////////////////

    class SMCQtApplicationPlugin : public APP::IApplicationPlugin
    {
    public:

        //! Library name string
        static const std::string LibraryName;

        //! Plugin name string
        static const std::string PluginName;

    public:

        ~SMCQtApplicationPlugin();

        //@{
        /** Plugin information */
        const std::string& getPluginName() const;
        const std::string& getLibraryName() const;
        //@}

        //@{
        /** Load/Unload the library */
        virtual void load(const APP::IApplication& app);
        virtual void unload(const APP::IApplication& app);
        //@}
    };
}
