#ifndef SMCQT_TTSGUI_H_
#define SMCQT_TTSGUI_H_

/*****************************************************************************
*
* File             : TTSGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : TTSGUI class declaration
* Author           : Nishant Singh
* Author email     : ns@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
* Updated by       : Mohan Singh ( mohan@vizexperts.com)
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <Audio/ITextToSpeechComponent.h>
#include <SMCQt/DeclarativeFileGUI.h>

namespace SMCQt
{
    class TTSGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

    public:

        TTSGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                void connectPopupLoader(QString type);
                void _handleBrowseButtonClicked();
                void _handlePlayButtonClicked();
                void _handleStopButtonClicked();
                void _handleCloseButtonClicked();

    protected:

        virtual ~TTSGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! clear the GUI
        void _reset();

    protected:

        //! Animation Component
        CORE::RefPtr<AUDIO::ITextToSpeechComponent> _ttsComponent;

        enum TTSStatus
        {
            STOPPED,
            PLAYING,
            PAUSED
        };

        TTSStatus _ttsStatus;
    };

} // namespace SMCQt


#endif  // SMCQT_TTSGUI_H_
