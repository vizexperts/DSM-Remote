#pragma once

/*****************************************************************************
*
* File             : CreateLayerGUI.h
* Version          : 1.0
* Module           : Qt
* Description      : CreateLayerGUI class declaration
* Author           : Mohan Singh
* Author email     : mohan@vizexperts.com
* Reference        : Qt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <SMCUI/ICreateLayerUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>


namespace SMCQt
{
    // Creates a new layer
    class CreateLayerGUI  : public SMCQt::DeclarativeFileGUI
    {

        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

        Q_OBJECT

    public:

        CreateLayerGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void connectPopupLoader(QString type);
                void _handleAddButtonClicked();
                void _handleUpdateButtonClicked(int);
                void _handleRemoveButtonClicked(int);
                void _handleOkButtonClicked();
                void _handleCancelButtonClicked();

    protected:

        virtual ~CreateLayerGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

        //! clear attribute params
        void _clearAttributeParams();

        //! add attribute to the table
        void _addAttributeToTable(const QString& attrName, const QString& attrType, int attrWidth, int attrPrecision);

        //! clear the attribute table
        void _clearAttributeTable();

        //! clear the whole GUI
        void _clear();

    protected:

        //XXX AG Comments Missing from class variables
        CORE::RefPtr<SMCUI::ICreateLayerUIHandler> _createLayerUIHandler;


        //XXX AG Wrong Comment
        std::string _cbLayerTemplateName;

        //! current layer type
        std::string _currentLayerType;

        QList<QObject *> _twAttrList;
    };

} // namespace GUI

