#pragma once
/*****************************************************************************
*
* File             : ViewshedAnalysisGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : ViewshedAnalysisGUI class declaration
* Author           : Kumar Saurabh Arora
* Author email     : saurabh@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/ISelectionComponent.h>

#include <VizQt/LayoutFileGUI.h>
#include <SMCUI/IPointUIHandler2.h>
#include <SMCUI/IAreaUIHandler.h>
#include <osg/Vec3d>
#include <osg/Vec4d>
#include <Core/IBaseVisitor.h>
#include <Core/IObject.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Database/IDatabase.h>

#include <ogrsf_frmts.h>

namespace SMCQt
{
    // Point layer properties GUI
    class ViewshedAnalysisGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

        public:

            static const std::string ViewshedAnalysisObjectName;

            ViewshedAnalysisGUI();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:
            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

signals:
            void _updateSuccessStatus();

        private slots:
            void connectAnalysisLoader();

            void _handleStartButtonClicked();
            void _handleStopButtonClicked();
            void _handleAddButtonClicked();
            void _handleRemoveButtonClicked(int);
            void _handlePBmarkPositionClicked(bool);
            void _handlePBPointPositionClicked(bool);
            void _handleSuccessStatus();
            void _handleBrowseButtonClicked();
            void _handleBrowseBuildingShpButtonClicked();
            void _handleSaveTifPathButton();
            void _handleNameChanged(QString);
            void _handleHeightChanged(QString);
            void _handleRangeChanged(QString);
            void _handleSensorChanged();

            void _handleLookatLatChanged(QString);
            void _handleLookatLongChanged(QString);
            void _handleLookatEleChanged(QString);

            void _handleVerticalFOVChanged(QString);
            void _handleHorizontalFOVChanged(QString);

            void _handlefixedfov(bool checked);

            void _handletabChanged(bool);
            void _handleMarklookatLatLongButtonClicked(bool);
            void _disableMarking();

            void _viewShedClosed();
        protected:
            //@{
            /** play and stop Path computation*/
            bool play();
            bool stop();
            //@}

            void _handleViewshedAnalysisButtonPressed(const std::string& playEvent);
            void _addAttributeToTable(const QString& leName, const QColor& rectColor, double leLong, double leLat, double leHeight, double leRange,
                                    osg::Vec3f lookatPos, double leHorizontalFOV, double leVerticalFOV);
        protected:

            virtual ~ViewshedAnalysisGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            //! Sets viewer point
            void _setViewerPoint(CORE::IPoint* point);

            //! Sets lookat point
            void _setLookatPoint(CORE::IPoint* point);

            void _reset();

            //! Viewshed filter
            typedef std::map<std::string, CORE::RefPtr<CORE::IBaseVisitor> > VisitorList;
            VisitorList _visitorList;

            //! point UI Handler
            CORE::RefPtr<SMCUI::IPointUIHandler2> _pointHandler;
    
            //! point UI Handler
            CORE::RefPtr<CORE::ISelectionComponent> _pointHandlerObj;

            ObserverList _observerList;

            //! Vector of objects
            std::map<std::string, CORE::RefPtr<CORE::IObject> > _objectList;
            
            //! point shape file path
            std::string _originalLayerPath;

            //! building shape file path
            std::string _originalBuidingLayerPath;

            //! point shape file path
            std::string _saveTifFilePath;

            //! viewer point
            CORE::RefPtr<CORE::IObject> _viewerPoint;

            //! lookat point
            CORE::RefPtr<CORE::IObject> _lookatPoint;

            //XXX Comments missing
           
            //XXX class protected/private data/function members should start from _(underscore)
            int noOfObjects;
            QList<QObject *> _twObserverList;

            bool _tab;

            bool _nolookat;

            int _nameIndex;
            int _heightIndex;
            int _rangeIndex;

            int _lookatLatIndex;
            int _lookatLongIndex;
            int _lookatEleIndex;

            int _horizontalFOVIndex;
            int _verticalFOVIndex;

            bool _fixedfov;

            OGRDataSource *_viewShedPoints;
    };

} // namespace SMCQt
