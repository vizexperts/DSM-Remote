/*****************************************************************************
*
* File             : WatershedGenerationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : WatershedGenerationGUI class declaration
* Author           : dharmendra
* Author email     : dharmendra@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2015-2016, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <SMCUI/IWatershedGenerationUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAreaUIHandler.h>
namespace SMCQt
{
    // Creates a new layer
    class WatershedGenerationGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        QObject* WatershedGenerationObject;
    public:

        static const std::string WatershedAnalysisPopup;

        WatershedGenerationGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void  _setProgressValue(int);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling
                void _handlePBStartClicked();
                void _handlePBStopClicked();
                void _handlePBRunInBackgroundClicked();
                void  _handleProgressValueChanged(int);
                void _handlePBAreaClicked(bool pressed);
                void _handlePBOutletAreaClicked(bool pressed);
                void _reset();

    protected:

        virtual ~WatershedGenerationGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();
        enum AreaSelectionState
        {
            NO_MARK,
            AREA_MARK,
            OUTLETREGION_MARK
        };

    protected:

        //! Height Above UIHandler
        CORE::RefPtr<SMCUI::IWatershedGenerationUIHandler> _uiHandler;

        //! Area UI Handler
        CORE::RefPtr<SMCUI::IAreaUIHandler> _areaHandler;
                //! Point selection state
        AreaSelectionState _state;
        
    };

} // namespace SMCQt
