#ifndef SMCGUI_BOTTOMPANE_H_
#define SMCGUI_BOTTOMPANE_H_

/*****************************************************************************
 *
 * File             : LatLongGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : LatLongGUI class declaration
 * Author           : Gaurav Garg
 * Author email     : gaurav@vizexperts.com
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <VizQt/GUI.h>
#include <Core/Base.h>
#include <Core/IBaseUtils.h>
#include <VizQt/IQtGUIManager.h>
#include <VizUI/ICameraUIHandler.h>
#include <View/ICameraComponent.h>
#include <QtCore/QTimer>
#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/export.h>
#include <Elements/IMapSheetInfoComponent.h>
#include <Util/GdalUtils.h>

namespace SMCQt
{
    // XXX - exported only for testing purpose
    class SMCQT_DLL_EXPORT BottomPaneGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        
        Q_OBJECT
        Q_PROPERTY(QString positionText READ positionText)
        Q_PROPERTY(QString mgrsText READ mgrsText)
        Q_PROPERTY(QString gridText READ GRText)
        Q_PROPERTY(QString eyePositionText READ eyePositionText)
        Q_PROPERTY(QString mapSheetText READ mapSheetText)

        public:

            BottomPaneGUI();

            QString positionText();

            QString mgrsText();

            QString gridText();

            QString gridText1();

            QString GRText();
            QString eyePositionText();

            QString mapSheetText();

            void fillGridMat(); 
            void initializeZoneSRS(); 
            void fillMapSheetMat(); 

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            /*class SpatialSRS{
            public: 
                OGRSpatialReference oGRSpatialReference;
                float south;
                float north;
                float west;
                float east;
            };


            void inline fillJsonBounds(SpatialSRS& spatialSRS, QJsonObject&  boundsObject);
            bool inline isWithinBounds(osg::Vec2d pos, SpatialSRS spatialSRS);*/
            std::string inline FixLengthOfInteger(int input);

        protected:

            /*SpatialSRS targetSRS_0;
            SpatialSRS targetSRS_1A;
            SpatialSRS targetSRS_1B;
            SpatialSRS targetSRS_2A;
            SpatialSRS targetSRS_2B;
            SpatialSRS targetSRS_3A;
            SpatialSRS targetSRS_3B;
            SpatialSRS targetSRS_4A;
            SpatialSRS targetSRS_4B;*/

            OGRSpatialReference _sourceSRS;

            std::vector<std::vector<std::string>> gridMat; 
            std::vector<std::vector<int>> _mapSheetMat; 

            virtual ~BottomPaneGUI();

            //! basemap UI Handler
            CORE::RefPtr<VizUI::ICameraUIHandler> _cameraUIHandler;
            //! Camera component
            CORE::RefPtr<VIEW::ICameraComponent> _cameraComponent;
            CORE::RefPtr<ELEMENTS::IMapSheetInfoComponent> _mapSheetComponent;
    };

} // namespace SMCQt

#endif  // SMCGUI_BOTTOMPANE_H_
