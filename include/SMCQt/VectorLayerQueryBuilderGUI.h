#pragma once

/*****************************************************************************
*
* File             : VectorLayerQueryBuilderGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : VectorLayerQueryBuilderGUI class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IVectorLayerQueryUIHandler.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT VectorLayerQueryBuilderGUI:public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:
        static const std::string VECTORQUERYBUILDEROBJECTNAME;
        //! Constructor
        VectorLayerQueryBuilderGUI();

        //! 
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);


        public slots:



            private slots:

                void _handleOkButtonClicked();
                void _handleClearButtonClicked();
                void _handleTableOkButtonClicked();

                void _handleAddToWhereButtonClicked(int index);

                void _handleOperatorButtonClicked(QString buttonName);

                void _populateFieldDefinition();
                void _queryBuilderClosed();

                void showSampleValues(int);
                void showAllValues(int);
                void addValueToWhere(int);
                void changeText(const QString& leToSearch);
                //@{
                /** Set active function */
                void setActive(bool active);
                //@}


    protected: 

        virtual ~VectorLayerQueryBuilderGUI();


        CORE::RefPtr<SMCUI::IVectorLayerQueryUIHandler>  _vectorLayerQueryUIHandler;

        // list containing all fields
        QStringList _allFieldDefinitions;

        // list containing only selected fields
        QStringList _currentFieldValues;



        std::string _currentVectorShapeName;

        std::vector<std::string> _shapeFileAttributeList;
        
        //used for searching the values
        std::string _toSearch;
    };
}
