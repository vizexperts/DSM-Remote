/*****************************************************************************
*
* File             : FlyAroundGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : FlyAroundGUI class declaration
* Author           : Akshay
* Author email     : akshay@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2015-2016, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <VizQt/LayoutFileGUI.h>
#include <SMCUI/IFlyAroundUIHandler.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <VizUI/UIHandler.h>
#include <VizUI/ICameraUIHandler.h>
#include <Core/ISelectable.h>
namespace SMCQt
{
    // Creates a new layer
    class FlyAroundGUI :public DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
        QObject* FlyAroundObject;
    public:

        static const std::string FlyAroundAnalysisPopup;

        FlyAroundGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            private slots:

                // Slots for event handling

                // slot for starting flyAround for existing stored data
                void _startExistingFlyaroundComboBoxClicked(QString selectedValue);

                //saves current flyAround
                void _handleSaveButtonClicked();

                //stops current flyAround
                void _handlePBStopClicked();

                // previews the flyAround with current inserted values without saving
                void _handlePreviewClicked();

                // stops the flyAround with current inserted values without saving
                void _handleStopFlyAroundPreview();

                // pauses the flyAround
                void _handlePauseClicked();

                //apply changes
                void _applyChanges();

                //resets the class members
                void _reset();

                // creates new flyAround from existing saved ones
                void _editExistingFlyaround(QString selectedValue,bool checked);

                // decides whether to show parameters grid or not
                void _showParameterGrid(QString selectedValue);

                // for visualising the selected flyAround
                void _visualise(QString selectedValue);

                // doubles the flyAround speed
                void _increaseSpeed();

                // divides the flyAround speed by 2
                void _decreaseSpeed();

                //delete flyAround while editing
                void _delete(QString);

                //ok slot for overWriting the file while saving
                void _okSlotForOverWriting();

                //cancel slot for overWriting the file while saving
                void _cancelSlotForOverWriting();

                //ok slot for deletig the file
                void _okSlotForDeleting();

                //cancel slot for deletig the file 
                void _cancelSlotForDeleting();                

    protected:

        virtual ~FlyAroundGUI();

        // retruns IPoint for current selected POint/model
        CORE::RefPtr<CORE::IPoint> _getSelectedPointFeature() ;
        
        // for populating flyAround combobox
        void _readAllFlyAround();

        // retruns flyAround struct for given flyAround name
        SMCUI::IFlyAroundUIHandler::FlyAround _getFlyaround(std::string flyAroundName);

        // writes in json file while saving
        void _writeFlyAround(QString name, QString flyaroundValues);

        // 2 flyAround can't have the same name
        bool isAlreadyPresent(std::string name);

    protected:

        //! flyAround UI Handler
        CORE::RefPtr<SMCUI::IFlyAroundUIHandler> _uiHandler;

        //! Camera UI handler
        CORE::RefPtr<VizUI::ICameraUIHandler> _cameraUIHandler;

        //! current selection
        CORE::RefPtr<CORE::ISelectable> _currentSelection;

        std::vector<std::string> _flyaroundList;

        bool _start;

        bool _preview;

        QString _lastFlyaroundName;
        
    };

} // namespace SMCQt
