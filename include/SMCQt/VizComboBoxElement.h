#pragma once
/*****************************************************************************
*
* File             : VizComboBoxElement.h
* Version          : 1.0
* Module           : SMCQt
* Description      : VizComboBoxElement class declaration
* Author           : Nitish Puri
* Author email     : nitish@vizexperts.com
* Reference        : QObject derived class used in CustomComboBox component
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <QObject>

namespace SMCQt
{

    class VizComboBoxElement : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(QString uID READ uID WRITE setUID NOTIFY isUIDChanged)

    public:

        VizComboBoxElement(const QString& name, const QString& uID, QObject *parent = 0);

        QString name() const;
        void setName(const QString &name);

        QString uID() const;
        void setUID(const QString &uID);

    signals:
        void nameChanged();
        void isUIDChanged();

    private:
        QString m_name;
        QString m_uID;
    };
}
