#ifndef SMCQT_RASTERENHANCEMENTCHANGER_H_
#define SMCQT_RASTERENHANCEMENTCHANGER_H_

/*****************************************************************************
*
* File             : RasterEnhancementChangerGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : RasterEnhancementChangerGUI class declaration
* Author           : Raghavendra Polinki
* Author email     : rp@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <App/AccessElementUtils.h>

#include <Core/IWorldMaintainer.h>
#include <Core/IRasterData.h>


#include <Terrain/IRasterObject.h>
#include <Core/ISelectionComponent.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT RasterEnhancementChangerGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        RasterEnhancementChangerGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            void connectSmpMenu(QString type);
            void handleApplyButton();
            void handleCancelButton();

    protected:

        virtual ~RasterEnhancementChangerGUI();

        virtual void _loadAndSubscribeSlots();

        CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;

    };

} // namespace SMCQt

#endif  // SMCQT_CREATEOPACITYSLIDER_H_
