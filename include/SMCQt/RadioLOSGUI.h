#pragma once
/*****************************************************************************
*
* File             : RadioLOSGUI.h
* Version          : 1.0
* Module           : indiQt
* Description      : RadioLOSGUI class declaration
* Author           : Kumar Saurabh Arora
* Author email     : saurabh@vizexperts.com
* Reference        : indiQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2010, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCUI/IRadioLOSUIHandler.h>
#include <VizUI/IPointUIHandler.h>
#include <Core/IObject.h>
#include <Elements/IIcon.h>
#include <SMCQt/DeclarativeFileGUI.h>


namespace SMCQt
{
    class RadioLOSGUI : public DeclarativeFileGUI
    {
        Q_OBJECT
            DECLARE_META_BASE;
        DECLARE_IREFERENCED;

    public:
        // radar icon file path
        static const std::string TransIconPath;
        static const std::string RecvIconPath;

        static const std::string RadioLOSAnalysisObjectName;

    public:

        RadioLOSGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

signals:

        void _startButtonEnable(bool);
        void _stopButtonEnable(bool);
        void _transPosMarkButtonEnable(bool);
        void _recvPosMarkButtonEnable(bool);
        void _updateOutputFields(bool);
        void _getDecisionButtonEnable(bool);
        void _showProgressBar(bool);


        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

            void connectRadioAnalysisLoader();

            private slots:

                // Slots for event handling
                void _handlePBMarkTransPosClicked(bool);
                void _handlePBMarkRecvPosClicked(bool);
                void _handlePBStartClicked();
                void _handlePBStopClicked();
                void _handlePBGetDecisionButtonClicked();
                void _onUpdateOutputFields(bool);

                void _setStartButtonEnable(bool);
                void _setStopButtonEnable(bool);
                void _setGetDecisionButtonEnable(bool);
                void _setTransPosMarkButtonEnable(bool);
                void _setRecvPosMarkButtonEnable(bool);
                void _onShowProgressBar(bool);


    protected:

        virtual ~RadioLOSGUI();

        //! Removes the object from the world
        void _removeObject(CORE::IObject* obj);

        //! Sets Trans/Recv Postion
        void _setTransPos(CORE::IPoint* point);
        void _setRecvPos(CORE::IPoint* point);

        //! Get the point from point UI handler and fills the edit box, depending on the state
        void _processPoint();

        //! copy points
        CORE::IPoint* _copyPoint(CORE::IPoint* point);

        //! enable/disable point handler 
        void _setPointHandlerEnabled(bool enable);

    protected:

        enum PointSelectionState
        {
            NO_POS_MARK,
            TRANS_POS_MARK,
            RECV_POS_MARK
        };


        //! Radio LOS UI Handler
        CORE::RefPtr<SMCUI::IRadioLOSUIHandler> _uiHandler;

        //! Point UI Handler
        CORE::RefPtr<VizUI::IPointUIHandler> _pointHandler;

        //! Transmitter/Receiver Position
        CORE::RefPtr<CORE::IObject> _transPos;
        CORE::RefPtr<CORE::IObject> _recvPos;

        //! Transmitter/Receiver Icon Pointer
        CORE::RefPtr<ELEMENTS::IIcon> _transIcon;
        CORE::RefPtr<ELEMENTS::IIcon> _recvIcon;

        //! Point selection state
        PointSelectionState _state;

        QObject* _radioLOSObject;
    };

} // namespace SMCQt
