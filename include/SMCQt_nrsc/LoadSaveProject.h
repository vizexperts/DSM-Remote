#ifndef SMCQT_LOADSAVEPROJECT_H_
#define SMCQT_LOADSAVEPROJECT_H_

/*****************************************************************************
*
* File             : LoadSaveProject.h
* Version          : 1.0
* Module           : SMCQt
* Description      : LoadSaveProject class declaration
* Author           : Somnath Singh
* Author email     : somnath@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*****************************************************************************/

#include <VizQt/GUI.h>
#include <Core/Base.h>
#include <Core/IBaseUtils.h>
#include <VizQt/IQtGUIManager.h>
#include <VizQt/QtGUI.h>
#include <SMCQt/IAddContentGUI.h>
#include <SMCQt/QMLTreeModel.h>
#include <SMCUI/IAddContentGUIHandler.h>

#include <Database/IDatabase.h>
#include <Core/IObject.h>
#include <VizUI/UIHandler.h>
#include <SMCUI/ILineUIHandler.h>
#include <CORE/WorldMaintainer.h>
#include <Core/IMessageFactory.h>
#include <Core/IBaseUtils.h>

#include <VizDataUI/IDatabaseUIHandler.h>



namespace SMCQt
{
    class LoadSaveProject: public VizQt::QtGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        LoadSaveProject();

        //@{
        /** Called when GUI added/removed to GUI manager */
        void onAddedToGUIManager();
        void onRemovedFromGUIManager();
        //@}

        //! XXX - should create these UIHandlers from factory
        void setAddContentGUIHandler(SMCUI::IAddContentGUIHandler* addContentGUIHandler){_addContentGUIHandler = addContentGUIHandler; }
        SMCUI::IAddContentGUIHandler * getIAddContentGUIHandler();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void initializeAttributes();

        void _loadAndSubscribeSlots();

        void addCredential(const CORE::NamedGroupAttribute& attr);
        CORE::RefPtr<CORE::NamedGroupAttribute> setCredential() ;

        QWidget* getWidget() const{return NULL;};

    signals:
        void selectedItemChanged(QVariant);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            bool getActive() const;
            void loadFiles();
            //@}

            //Signals from QML
            void smpSearch(QString layerName);

         // Contains the functions related to the Database
         public:

            bool makeConnectionWithDatabase();
            void closeDatabaseConnection();

        private:
            

    protected:

        virtual ~LoadSaveProject();

        bool _checkConnectionWithDatabase();

        VizDataUI::IDatabaseUIHandler* _getDatabaseUIHandler();

        void _createProjectSchema(const std::string& schemaName,const std::string& tableName);

        bool _checkForExistingSchema(const std::string& schemaName);

        void _addProjectNameAndPath(const std::string& projectName, const std::string& path,const std::string& schemaName, const std::string& tableName);

        void _getPresentProjects(const std::string& schemaName, const std::string& tableName);

        QStringList _getProjectQListFromProjectMap(std::map<std::string, std::string> projectNameAndPath);

        //! GUI Manager
        CORE::RefPtr<VizQt::IQtGUIManager> _manager;

        //! Reference of AddContentUIHandler
        CORE::RefPtr<SMCUI::IAddContentGUIHandler> _addContentGUIHandler;

        //! status of gui
        bool _active;

        //! context property name
        std::string _contextPropertyName;

    private:

        //TAking the instance of database UI handler
        CORE::RefPtr<VizDataUI::IDatabaseUIHandler> _dbUIHandler;

        // Maintaining the Map of the Schema for the Raster and Vector Layers
        std::map<std::string, std::vector<std::string> > _vectorSchemaMap;
        std::map<std::string, std::vector<std::string> > _rasterSchemaMap;

        //Maintaining the Objects which are added to the Terrain currently
        std::map<std::string, vizCore::RefPtr<CORE::IObject> > _objectAdded;


        std::map<std::string, vizCore::RefPtr<CORE::IObject> > _rasterObjectAdded;
        std::map<std::string, vizCore::RefPtr<CORE::IObject> > _vectorObjectAdded;
        std::map<std::string, vizCore::RefPtr<CORE::IObject> > _elevationObjectAdded;

        //Contain the List of the Layers
        QStringList _layerList;

        //Mode :- Elevation / Vector / Raster
        std::string _currentMode;

        //Contain the Database Schema currently been used
        std::string _currentDatabaseSchema;

        //Hav the reference of the currently selected Item from the layer Type
        std::string _currentSelectedItem;

        // Options for new connection
        std::map<std::string, std::string> _dbOptions;

        QList<QObject*> _selectionLayerList;

        std::string _tableNameSaveLoad;
        std::string _schemaNameSaveLoad;
        std::map<std::string, std::string> _projectNameAndPath;
    };

} // namespace SMCQt

#endif // SMCQT_ADDCONTENTGUI_H_
