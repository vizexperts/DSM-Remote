#ifndef SMCQT_RASTERSERIESEVENTGUI_H_
#define SMCQT_RASTERSERIESEVENTGUI_H_

/*****************************************************************************
 *
 * File             : VisibilityControllerGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : VisibilityControllerGUI class declaration
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCUI/IRasterSeriesEventUIHandler.h>

#include <QtCore/QDateTime>

namespace SMCQt
{
    class RasterSeriesEventGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
		DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        RasterSeriesEventGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void onAddedToGUIManager();

		void onRemovedFromGUIManager();

        public slots:
            void popupLoaded(QString type);

            //! to select raster from raster series
            void selectRaster(QString cpID);

            //! edit the raster in raster series
            void editRasterControlPoint(QString uid, QDateTime qTime);

            //!delete the raster from raster series
            void deleteRaster(QString uid);
           

    protected:

        //! populate the dropdown list
        void _populateRasterList();

        virtual ~RasterSeriesEventGUI();

        void _loadAndSubscribeSlots();   

        CORE::RefPtr<SMCUI::IRasterSeriesEventUIHandler> _rsEventUIHandler;
    };
}   // namespace SMCQt

#endif  // SMCQT_VISIBILITYCONTROLLERGUI_H_
