#ifndef SMCQT_CreateVideoGUI_H_
#define SMCQT_CreateVideoGUI_H_

/*****************************************************************************
*
* File             : CreateVideoGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CreateLineGUI class declaration
* Author           : Nitish Puti
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <QColor>
#include <SMCUI/IPointUIHandler2.h>
#include <Core/Base.h>
#include <Core/ILine.h>
#include <Core/IPoint.h>

namespace SMCQt
{
    class SMCQT_DLL_EXPORT CreateVideoGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        CreateVideoGUI();

        //! Initializes attributes
        void initializeAttributes();
        //@}

        /** Called when GUI added/removed to GUI manager */
        void onAddedToGUIManager();
        void onRemovedFromGUIManager();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        signals:
            void play(QString);

        public slots:

            // Slots to connect to furthur menus
            void connectSMPMenu(QString type);
            void connectContextualMenu(QString type);

            // 
            void setVideoEnable(bool value);

            //Slots for Contextual menu
            void rename(QString newName);
            void setTextActive(bool state);
            void handleLatLongAltChanged(QString);
            void deletePoint();
            void clampingChanged(bool value);
            void showPointAttributes();
            void editEnabled(bool value);

            // slots for point layer
            void addPointsToSelectedLayer(bool value);

            // slots for Hyperlink options
            void attachFile(QString type);

            void playVideo();

            //For playing the video or audio 
            void playAudioOrVideo(QString fileName);

            void handleAttributesOkButtonPressed();
            void handleAttributesCancelButtonPressed();

            //Ths slot for changing the Text size and color
            void colorChanged(QColor color);
            void changeTextSize(double textSize);


    protected slots:
            void _resetForDefaultHandling();

    protected:

            void setActive(bool active);


    protected:

        void _performHardReset();

        virtual ~CreateVideoGUI();

        virtual void _loadAndSubscribeSlots();

        void _loadPointUIHandler2();

        void _populateAttributes();

        void _populateContextualMenu();

        void _createMetadataRecord();

        std::string _getProjectLocation();

        //! Point UI Handler
        CORE::RefPtr<SMCUI::IPointUIHandler2> _pointUIHandler;


        //!
        CORE::RefPtr<CORE::IFeatureLayer> _selectedFeatureLayer;

        QList<QObject*> _attributeList;

        bool _addToDefault;

        bool _cleanup;

        unsigned int _pointNumber;

        unsigned int _objNumber;

        std::string _audioVideoFileBrowsed;
    };

} // namespace SMCQt

#endif  // SMCQT_CreateVideoGUI_H_
