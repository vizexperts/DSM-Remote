#ifndef SMCQT_LAYERPROPERTIESGUI_H_
#define SMCQT_LAYERPROPERTIESGUI_H_

/*****************************************************************************
*
* File             : LayerPropertiesGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : LayerPropertiesGUI class declaration
* Author           : Nitish Puti
* Author email     : nitish@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <QColor>
#include <Core/Base.h>
#include <Core/IFeatureLayer.h>


namespace SMCQt
{
    class SMCQT_DLL_EXPORT LayerPropertiesGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        LayerPropertiesGUI();

        //! Initializes attributes
        void initializeAttributes();
        //@}

        /** Called when GUI added/removed to GUI manager */
        void onAddedToGUIManager();
        void onRemovedFromGUIManager();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);


        public slots:

            // Slots to connect to furthur menus
            void connectSMPMenu(QString type);

            //Ths slot for changing the color
            void colorChanged(QColor color);

            //Ths slot for changing the fill color
            void fillColorChanged(QColor color);

    protected:

			virtual void _loadAndSubscribeSlots();
			virtual ~LayerPropertiesGUI();


    protected:


        //!
        CORE::RefPtr<CORE::IFeatureLayer> _selectedFeatureLayer;

    };

} // namespace SMCQt

#endif  // SMCQT_CreateImageGUI_H_
