#ifndef SMCQt_WMSGUI_H_
#define SMCQt_WMSGUI_H_

/*****************************************************************************
 *
 * File             : WMSGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : WMSGUI class declaration
 * Author           : Abhishek Bansal
 * Author email     : abhishek@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, CAIR, DRDO
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <VizQt/LayoutFileGUI.h>
#include <SMCQT/export.h>

#include <SMCUI/IWMSUIHandler.h>
#include <UTIL/WMSUtils.h>
//Qt Headers

#ifndef USE_QT4
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTreeWidget>
#endif

namespace SMCQt
{
	class SMCQT_DLL_EXPORT WMSGUI : public VizQt::LayoutFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

        public:
            // Widget Names

            static const std::string LEServerUrl; // lineEdit
            static const std::string PBAddLayers;   //Push button for starting animation
            static const std::string TRLayer; // layer tree
            static const std::string PBConnect; // Push Button for connecting to WMS Server
            

        public:

            WMSGUI();

            //@{
            /** Called when GUI added/removed to GUI manager */
            void onRemovedFromGUIManager();
            //@}
            
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            void initializeAttributes();


        public slots:
            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

        private slots:
            void _handleConnectButtonClicked(bool);
            void _handleAddLayersButtonClicked();
            void _handleTreeItemDoubleClicked(QTreeWidgetItem*, int);

        signals:


        private slots:


        private:
            void _showAllLayers(const UTIL::Layer::LayerList &);
            bool _isConnectedToSomeServer ;

        protected:
            virtual ~WMSGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            //! Cleans UP after the dialog is closed
            void _cleanUP();

            //! Reset the GUI
            void _reset();

            void _getTreeWidgetItem(const UTIL::Layer::LayerList &list, QTreeWidgetItem *parentWidget);

        protected:

            //! XXX : NS Member variables names must start with small letters
            QLineEdit   *_serverUrlLE;
            QPushButton *_connectPB;
            QPushButton *_addLayersPB;
            QTreeWidget *_layerTR;


            CORE::RefPtr<SMCUI::IWMSUIHandler> _WMSUIHandler;
    };

} // namespace SMCQt

#endif // SMCQt_WMSGUI_H_