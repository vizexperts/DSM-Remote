#pragma once

/*****************************************************************************
 *
 * File             : CompassGUI.h
 * Version          : 1.0
 * Module           : indiQt
 * Description      : CompassGUI class declaration
 * Author           : Abhinav
 * Author email     : 
 * Reference        : 
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2011-2012, CAIR, DRDO                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <CORE/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/VizComboBoxElement.h>



#include <osgViewer/View>

namespace SMCQt
{
    // GUI for almanac calculation. Takes as input position
    // and date.
    class CompassGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;

        Q_OBJECT
           
        public:

            CompassGUI();

			void onAddedToGUIManager();
            
            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        protected:

			virtual ~CompassGUI();
			
			
            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();
         
        protected:

			QObject *_QMLObject;

			osg::ref_ptr<osgViewer::View> _view;
    };

} // namespace indiGUI

