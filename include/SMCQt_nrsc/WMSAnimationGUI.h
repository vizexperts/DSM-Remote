#ifndef INDIQT_WMSANIMATIONGUI_H_
#define INDIQT_WMSANIMATIONGUI_H_

/*****************************************************************************
 *
 * File             : WMSGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : WMSAnimationGUI class declaration
 * Author           : Abhishek Bansal
 * Author email     : abhishek@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, CAIR, DRDO
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <VizQt/LayoutFileGUI.h>

#include <SMCQT/DeclarativeFileGUI.h>

#include <SMCQt/export.h>

#include <SMCUI/IWMSUIHandler.h>

#include <UTIL/WMSUtils.h>

//Qt Headers
#ifndef USE_4QT4
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QSlider>
#endif

#include <CORE/IAnimationComponent.h>

#include <APP/IManager.h>

namespace SMCQt
{

	class CustomSlider : public QObject
	{
		public:
			void setSlider(QSlider *slider);
			void setGUIManager(APP::IManager *guiManager) { _guiManager = guiManager; }

			bool eventFilter(QObject *obj, QEvent *event);
		private:
			QSlider *_slider;
			APP::IManager *_guiManager;
	};

	class SMCQT_DLL_EXPORT WMSAnimationGUI : public VizQt::LayoutFileGUI
    {
        Q_OBJECT

        DECLARE_META_BASE;

        public:

            // Widget Names
            static const std::string SeekBarProgress;       //Push button for starting animation
            static const std::string LineEditProgress;      //Push button for starting animation
            static const std::string PBPlayPause;           //Push button for starting animation
            static const std::string PBStop;                //Push button for starting animation

            WMSAnimationGUI();

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            void initializeAttributes();

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}

        signals:
            void _sliderConfigurationUpdatedSignal();

        private slots:

            void _changeSliderConfiguration();
            void _handlePlayAnimationButtonClicked(bool);
            void _setInitailGUIState();
            void _handleSliderTimeChanged(int elapsedTime);
            void _handlePBPauseClicked();
            void _handlePBStopClicked();
			void _handleSeekBarClicked();


        protected:

            virtual ~WMSAnimationGUI();
            virtual void _initializeEndTime();
            inline void _setProgressTimeIndicator();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

            QPushButton *_playPausePB;
            QPushButton *_stopButton;
            QSlider     *_seekBar;
            QLineEdit   *_seekBarLE;

			CORE::RefPtr<SMCUI::IWMSUIHandler> _WMSUIHandler;
            
            QString  _endTime;
            QString  _currTime;
            QString  _timeFormatString;

            std::stringstream _currTimeStream;

            boost::posix_time::ptime _startTimeBoost;
            boost::posix_time::ptime _endTimeBoost;
            boost::posix_time::ptime _currentTimeBoost;

			CustomSlider *_customSlider;
    };

} // namespace SMCQt



#endif // INDIQT_WMSANIMATIONGUI_H_