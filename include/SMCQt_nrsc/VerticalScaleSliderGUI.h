#ifndef SMCQT_VERTICALSCALESLIDER_H_
#define SMCQT_VERTICALSCALESLIDER_H_

/*****************************************************************************
*
* File             : VerticalScaleSliderGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : VerticalScaleSliderGUI class declaration
* Author           : Naresh Sankapelly
* Author email     : naresh@vizexperts.com
* Reference        : SMCQt module
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright 2012-2013, VizExperts India Private Limited (unpublished)
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <Core/IMessageType.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/Base.h>
#include <App/AccessElementUtils.h>
#include <CORE/IWorldMaintainer.h>

namespace SMCUI
{
    class IElevationEditingUIHandler;
}

namespace SMCQt
{
    class SMCQT_DLL_EXPORT VerticalScaleSliderGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        //! 
        //! \brief Constructor
        //! 
        VerticalScaleSliderGUI();

        //! 
        //! \brief called every time a message is receieved. 
        //! \param messageType Type of the message received. 
        //! \param message Message object. 
        //! 
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! 
        //! \brief Initializes attributes
        //! 
        void initializeAttributes();
       
        //! 
        //! \brief Called when GUI added to GUI manager 
        //! 
        void onAddedToGUIManager();
        
        //! 
        //! \brief Called when GUI added from GUI manager
        //! 
        void onRemovedFromGUIManager();
        
   public slots:

        //! 
        //! \brief Slot that gets executed whenever the user changes vertical scale value
        //! \param verticalScaleValue New vertical scale value
        //! 
        void changeVerticalScalevalue(const double &verticalScaleValue);
        
        
        void connectVerticalScaleMenu();

    protected:

        virtual ~VerticalScaleSliderGUI();

        virtual void _loadAndSubscribeSlots();

        //! 
        //! \brief Changes the vertical scale value on terrain
        //! 
        void _changeVerticalScale(const double& value);

        //! 
        //! \brief Returns the current vertical scale applied on Terrain
        //! 
        double VerticalScaleSliderGUI::_getCurrentVerticalScale();

    private:

        //! ElevationEditingUIHandler for all elevation related modifications
        CORE::RefPtr<SMCUI::IElevationEditingUIHandler>  _elevationEditingUIHandler;
    };

} // namespace SMCQt

#endif  // SMCQT_VERTICALSCALESLIDER_H_
