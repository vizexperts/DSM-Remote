#ifndef SMCQT_BUILDINGEXTRUSIONGUI_H
#define SMCQT_BUILDINGEXTRUSIONGUI_H

#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCUI/BuildingExtrusionUIHandler.h>


namespace SMCQt
{

    class SMCQT_DLL_EXPORT BuildingExtrusionGUI: public SMCQt::DeclarativeFileGUI
    {

        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        
        Q_OBJECT

    public:
        //! Constructor
        BuildingExtrusionGUI();

        //! destructor
        ~BuildingExtrusionGUI();

        //! Called when GUI added/removed to GUI manager 
        void onAddedToGUIManager();
        void onRemovedFromGUIManager();
        
        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! get the map between url and IBuildingExtrusion
       // CORE::ITerrain::mapUrlToBuildingExtrusion getMapUrlToBuilding();

        //! get the map between url and IBuildingExtrusion
       // void setMapUrlToBuilding(const  CORE::ITerrain::mapUrlToBuildingExtrusion&);

        public slots:

             //! this function is called when the contextual menu is opened
             void connectSmpMenu(QString);
             
             //! this will extrude the building , if the argument is true
             //! then building will extrude , if the argument is false then 
             //! then the extruded building will be undone
             void extrudeBuilding();

             //! remove the extruded building
             void intrudeBuilding();

    protected:
        //! stores the url for the shape file
        std::string _url;

        //! load the slots
        virtual void _loadAndSubscribeSlots();

        //! compute the url of the selected object
        void _selectObject(const std::string& name);

        
        //! map between url and IBuildingExtrusion , this will be helpfull in removing 
        //! the extruded building 
        // could be helpful in saving extruded building while saving project
        //CORE::ITerrain::mapUrlToBuildingExtrusion _mapUrlToBuilding;

        //! buildingExtrusionUIHandler
        CORE::RefPtr<SMCUI::IBuildingExtrusionUIHandler> _buildingExtrusionUIHandler;

        //! 
        CORE::RefPtr<ELEMENTS::IVectorObject> _vectorObject;

    };
}
#endif // SMCQT_BUILDINGEXTRUSIONGUI_H