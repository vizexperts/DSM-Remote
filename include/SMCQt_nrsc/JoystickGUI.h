#ifndef SMCQT_JOYSTYICK_GUI_H_
#define SMCQT_JOYSTYICK_GUI_H_

/*****************************************************************************
 *
 * File             : JoystickGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : JoystickGUI class declaration
 * Author           : Somnath Singh
 * Author email     : Somnath@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013 VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/QMLTreeModel.h>

#include "VizOSGQtQuick/OSGViewport.h"

#include <Core/IBaseUtils.h>
#include <Core/Base.h>

#include <VizQt/GUI.h>
#include <VizQt/IQtGUIManager.h>

#include <QtGui>

#include <osg/PositionAttitudeTransform>
#include <osgViewer/CompositeViewer>
#include <Touch/IMultiTouchManager.h>

#include <Joystick/JoystickManager.h>
#include <Joystick/JoystickEventHandler.h>
#include <Joystick/JoystickPlugin.h>


namespace osgQtQuick
{
    class OSGViewport;
}

namespace SMCQt
{

    class SMCQT_DLL_EXPORT JoystickGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;

        Q_OBJECT

        public:

            JoystickGUI();

            //@{
            /** Called when GUI removed to GUI manager */
            void onAddedToGUIManager();
            //@}

            //@{
            /** Called when GUI added/removed to GUI manager */
            void onRemovedFromGUIManager();
            //@}

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

            void _loadAndSubscribeSlots();

            void initializeAttributes();

            //JOYSTICKCONTROLLER::JoystickEventHandler* _getJoystickEventHandler();

           
        public slots:
            void connectToQML(QString menuName);
            void startJoystickController();
            void stopJoystickController();
            void isJoystickPresent();

        signals:
            void showError(QString title, QString text, bool critical = true);


        protected:

            //! Destructor
            virtual ~JoystickGUI();

           // JOYSTICKCONTROLLER::JoystickEventHandler* _getJoystickEventHandler();
            JOYSTICKCONTROLLER::IJoystickManager* _getJoystickManager();
            bool _checkIfJoystickEnabledInOtherWorld();
           
            CORE::RefPtr<TOUCH::IMTManager> _multiTouchManager;
            void _disableTouch();
            void _configureEventHandling(bool);

        private:

            CORE::RefPtr<JOYSTICKCONTROLLER::IJoystickManager> _jsManager;
            osg::ref_ptr<JOYSTICKCONTROLLER::JoystickEventHandler> _joystickEventHandler;
            double checkHeight();
    };

} // namespace indiGUI

#endif  // SMCQT_JOYSTYICK_GUI_H_
