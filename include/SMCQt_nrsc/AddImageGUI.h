#ifndef AddImageGUI_H
#define AddImageGUI_H
/*****************************************************************************
 *
 * File             : AddImageGUI.h
 * Version          : 1
 * Module           : TKQt
 * Description      : AddTabGUI Class of Ribbon GUI
 * Author           : indira
 * Author email     : author email id
 * Reference        : Part of TKP Application
 * Inspected by     : code inspector's name
 * Inspection date  : YYYY-MM-DD
 *
 *****************************************************************************
 * Copyright (c) 2010-2011,VizExperts India Pvt. Ltd                                        
 *****************************************************************************
 *
 * Revision Log (latest on top):
 * Revision     Version     Date(YYYY-MM-DD)    Time(XXXX hrs)  Name
 * 
 *
 *
 *****************************************************************************/
#include <osg/Vec4d>

#include <Core/IBaseUtils.h>

#include <VizQt/QtGUI.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCUI/IAddContentGUIHandler.h>

#include <VizUI/ITerrainPickUIHandler.h>
#include <VizUI/IMouseMessage.h>

namespace SMCQt
{
    class AddImageGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE
        DECLARE_IREFERENCED
        Q_OBJECT

    public:

        AddImageGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void onAddedToGUIManager();

		void onRemovedFromGUIManager();

        //@{
        /** Set/Get GUI manager*/
        void setGUIManager(APP::IGUIManager* manager);
        APP::IGUIManager* getGUIManager() const;
        //@}

    private:

        //! GUI Manager
        CORE::RefPtr<VizQt::IQtGUIManager> _manager;

         //Last Path History
        std::string _lastPath;

    private slots:
            
        void _handleBrowseButtonClicked();
        void _addImageLayer(QString, QString, QString, QString, QString, QString);
        void _handleMinMarkClicked();
        void _handleMaxMarkClicked();
        void popupLoaded(QString type);
        void _subdatasetSelected();

   protected:

       //! Loads the .ui file and subscribes signals to slots
        void _loadAndSubscribeSlots();

        virtual ~AddImageGUI();

        void _loadAddContentUIHandler();

        //! UIHandler for loading data
        CORE::RefPtr<SMCUI::IAddContentGUIHandler> _addContentUIHandler;

        osg::Vec4d _extents;

        //marking for min and max coordinates
        bool _minMarking;

        bool _maxMarking;

        int _subdataset;
    };
}
#endif // AddImageGUI_H
