#pragma once

/*****************************************************************************
 *
 * File             : CameraSyncGUI.h
 * Version          : 1.0
 * Module           : indiQt
 * Description      : CameraSyncGUI class declaration
 * Author           : Abhinav
 * Author email     : 
 * Reference        : 
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2011-2012, CAIR, DRDO                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <CORE/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <QtCore/QVariant>

namespace SMCQt
{
    // GUI for almanac calculation. Takes as input position
    // and date.
    class CameraSyncGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;

        Q_OBJECT

            // Widget names
            static const std::string ViewComboBox;
            static const std::string AttachDeattachButton;
           
        public:

            CameraSyncGUI();
            
            //! Initializes attributes
            void initializeAttributes();
            //@}

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set active function */
            void setActive(bool active);
            //@}

			void connectToQML(QString menuName);
			void disconnectFromQML(QString menuName);

        private slots:
            
			void _handleAttachButtonClicked();
			void _handleDettachButtonClicked();
            

        protected:

			void	_populateComboBox();
			void	_setGUIState();
            virtual ~CameraSyncGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();
         
        protected:

          
            QList<QObject*> _viewList;

			QObject *_QMLCameraSyncObject;
    };

} // namespace indiGUI

