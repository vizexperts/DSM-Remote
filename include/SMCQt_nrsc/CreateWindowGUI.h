#pragma once

/*****************************************************************************
 *
 * File             : CreateWindowGUI.h
 * Version          : 1.0
 * Module           : indiQt
 * Description      : CreateWindowGUI class declaration
 * Author           : Abhinav
 * Author email     : 
 * Reference        : 
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2011-2012, CAIR, DRDO                                       
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <CORE/IBaseUtils.h>

#include <SMCQt/DeclarativeFileGUI.h>
#include <SMCQt/VizComboBoxElement.h>

#include <QtCore/QVariant>
#include <SMCQt/CreateWindowClient.h>
#include <SMCQt/CreateWindowService.h>
#include <RemoteSignal/IRemoteSignalComponent.h>

namespace SMCQt
{
    // GUI for almanac calculation. Takes as input position
    // and date.
    class CreateWindowGUI : public DeclarativeFileGUI
    {
        DECLARE_META_BASE;

        Q_OBJECT
           
        public:

            CreateWindowGUI();
            
            //! Initializes attributes
            void initializeAttributes();
            //@}

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set active function */
            void setActive(bool active);
            //@}

			void connectToQML(QString menuName);
			void disconnectFromQML(QString menuName);

        private slots:
            void _createDefaultWindowSlot(QString subWindowConfig, QString windowName);
            void _createCopyWindowSlot(QString subWindowConfig, QString windowName);
			void _handleButtonClicked();

        signals:
            void _createDefaultWindowSignal(QString subWindowConfig, QString windowName);
            void _createCopyWindowSignal(QString subWindowConfig, QString windowName);

        protected:
			
			void _setGUIState();
            virtual ~CreateWindowGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();
         
        protected:

            QObject *_QMLObject;

            /// RemoteSignal Component -- Used in accessing remote signal services 
            CORE::RefPtr<vizRemoteSignal::IRemoteSignalComponent> _remoteSignalComponent;

            /// CreateWindowGUI remote signal service's Client
            vizRemoteSignal::CreateWindowClient *_createWindowClient;

            /// CreateWindowGUI remote signal service's Server
            vizRemoteSignal::CreateWindowService *_createWindowService;
    };

} // namespace indiGUI

