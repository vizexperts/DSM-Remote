#ifndef SMCQT_WEATHERCONTROLLERGUI_H_
#define SMCQT_WEATHERCONTROLLERGUI_H_

/*****************************************************************************
 *
 * File             : VisibilityControllerGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : VisibilityControllerGUI class declaration
 * Author           : Nitish Puri
 * Author email     : nitish@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <SMCQt/DeclarativeFileGUI.h>

#include <SMCUI/IWeatherControllerUIHandler.h>

#include <QtCore/QDateTime>

namespace SMCQt
{
    class WeatherControllerGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
		DECLARE_IREFERENCED;
        Q_OBJECT

    public:

        WeatherControllerGUI();

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        void onAddedToGUIManager();

		void onRemovedFromGUIManager();

        public slots:
            void popupLoaded(QString type);
            void selectControlPoint(QString cpID);
            void addControlPoint(QDateTime cpTime, bool rainState, bool snowState);
            void editControlPoint(QString cpID, QDateTime cpTime, bool rainState, bool snowState);
            void deleteControlPoint(QString cpID);
            //void tabSelected(QString name);

    protected:

        void _populateControlPoints();

        virtual ~WeatherControllerGUI();

        void _loadAndSubscribeSlots();   

        CORE::RefPtr<SMCUI::IWeatherControllerUIHandler> _wcUIHandler;
    };
}   // namespace SMCQt

#endif  // SMCQT_VISIBILITYCONTROLLERGUI_H_
