#ifndef SMCQT_TIMEVARYINGRASTERGUI_H_
#define SMCQT_TIMEVARYINGRASTERGUI_H_

/*****************************************************************************
 *
 * File             : TimeVaryingRasterGUI.h
 * Version          : 1.0
 * Module           : SMCQt
 * Description      : TimeVaryingRasterGUI class declaration
 * Author           : Naresh Sankapelly
 * Author email     : naresh@vizexperts.com
 * Reference        : SMCQt interface
 * Inspected by     : 
 * Inspection date  : 
 *
 *****************************************************************************
 * Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
 *****************************************************************************
 *
 * Revision Log (latest on top):
 *
 *
 *****************************************************************************/

#include <Core/IBaseUtils.h>

#include <VizQt/LayoutFileGUI.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Terrain/IWMSComponent.h>
#include <Core/ISelectionComponent.h>
#include <Terrain/ITimeVaryingRasterComponent.h>
#include <osgEarthUtil/WMS>
#include <SMCUI/ITimeVaryingRasterUIHandler.h>
#include <QTreeWidget>

class QLineEdit;
class QLabel;

namespace SMCQt
{
    class TimeVaryingRasterGUI : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        Q_OBJECT

        public:

            //! Constructor
            TimeVaryingRasterGUI();
            
            //! Initializes attributes
            void initializeAttributes();
            //@}

            void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        public slots:

            //@{
            /** Set/Get active function */
            void setActive(bool active);
            //@}
            
            void popupLoaded(QString type);
            void addRasterFile();
            
			

        private slots:

        protected:

            virtual ~TimeVaryingRasterGUI();

            //! Loads the .ui file and subscribes signals to slots
            virtual void _loadAndSubscribeSlots();

        private:

            CORE::RefPtr<CORE::ISelectionComponent> _selectionComponent;
            			
			//Tour UIHandler for tour manipulation
			CORE::RefPtr<SMCUI::ITimeVaryingRasterUIHandler>  _timeVaryingRasterUIHandler;

			//Current QML Item for this GUI
			QObject *_guiObject;

    };

} // namespace SMCQt

#endif  // SMCQT_TIMEVARYINGRASTERGUI_H_

