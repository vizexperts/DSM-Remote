#ifndef SMCQT_COLLABORATION_GUI_H_
#define SMCQT_COLLABORATION_GUI_H_

/*****************************************************************************
*
* File             : CollaborationGUI.h
* Version          : 1.0
* Module           : SMCQt
* Description      : CollaborationGUI class declaration
* Author           : Naresh Sankapelly
* Author email     : naresh@vizexperts.com
* Reference        : SMCQt interface
* Inspected by     : 
* Inspection date  : 
*
*****************************************************************************
* Copyright (c) 2012-2013, VizExperts India Pvt. Ltd.
*****************************************************************************
*
* Revision Log (latest on top):
*
*
*****************************************************************************/

#include <Core/IBaseUtils.h>
#include <SMCQt/DeclarativeFileGUI.h>
#include <Core/IObject.h>

#include <QtCore/QTimer>
#include <HLA/INetworkCollaborationManager.h>
#include <RemoteSignal/IRemoteSignalComponent.h>

namespace SMCQt
{
    class CollaborationGUI 
        : public SMCQt::DeclarativeFileGUI
    {
        DECLARE_META_BASE;
        DECLARE_IREFERENCED;
        Q_OBJECT
    public:

        CollaborationGUI();

        //@{
        /** Called when GUI added/removed to GUI manager */
        void onAddedToGUIManager();
        void onRemovedFromGUIManager();
        //@}

        void update(const CORE::IMessageType& messageType, const CORE::IMessage& message);

        //! Initializes the following attributes:-
        //!     # Value editbox name
        //!     # Units combobox name
        //!     # Calculationtype editbox name
        void initializeAttributes();

        public slots:

            void connectCollaborationMenu();
            void startWall(bool value);
            void startController(bool value);
            void stopCollaboration(bool value);
        signals:
            
            
    protected:

        virtual ~CollaborationGUI();

        //! Loads the .ui file and subscribes signals to slots
        virtual void _loadAndSubscribeSlots();

    protected:
        /// Used in handling all HLA collaboration specific tasks
        CORE::RefPtr<HLA::INetworkCollaborationManager> _netwrkCollaborationMgr;

        /// To check Whether wall is running or not
        bool _isWallRunning;

        /// To check Whether Controller is running or not
        bool _isControllerRunning;

        /// To check whether the colloboration is stopped or not
        bool _isCollobarationStopped;

        /// RemoteSignal Component -- Used in accessing remote signal services 
        CORE::RefPtr<vizRemoteSignal::IRemoteSignalComponent> _remoteSignalComponent;
    };

} // namespace SMCQt

#endif  // SMCQT_COLLABORATION_GUI_H_
